﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoidDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.VoidDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Void Diary</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    
    <!--Rakhel ML Change - Start !-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <!--Rakhel ML Change - End !-->

    <script language="JavaScript">
					function SetFromToDate()
					{
						
							document.forms[0].txtFromDate.disabled="True";
							//document.forms[0].txtFrombtn.disabled="True"; //Rakhel ML Changes
							document.forms[0].txtToDate.disabled="True";
							//document.forms[0].txtTobtn.disabled="True";//Rakhel ML Changes
							document.forms[0].chkVoidSelected.checked = "True";
							//Rakhel ML Changes - Start
							var btnfrom = $("#txtFromDate"); var btnto = $("#txtToDate");
							$.datepicker._disabledInputs = [btnfrom[0], btnto[0]];
							//Rakhel ML Changes - End

						//Set focus to the first field
						var i;
						for (i=0;i<document.forms[0].length;i++)			
						{	 				
                            if((document.forms[0][i].type=="text") || (document.forms[0][i].type=="select-one")|| (document.forms[0][i].type=="textarea"))
						    {
							    if(document.forms[0][i].disabled==false){
							    document.forms[0][i].focus();
							    break;
							    }
						    }
						}
                    }
					
					function chkChange()
					{
						var b= document.forms[0].chkVoidSelected.checked;
						document.forms[0].txtFromDate.disabled = b;
						document.forms[0].txtToDate.disabled = b;
                        //Rakhel ML changes - Start	
						//document.forms[0].txtFrombtn.disabled=b;	
						//document.forms[0].txtTobtn.disabled=b;
						if (b) {
						    var btnfrom = $("#txtFromDate"); var btnto = $("#txtToDate");
						    $.datepicker._disabledInputs = [btnfrom[0], btnto[0]];
						}
						else {
						    $.datepicker._disabledInputs = [];
						}
                    }
                    //Rakhel ML changes - End
                    function ValForm() 
					{			    
					    //Raman MITS 31181: If void selected checkbox is checked then we do not need to validate from data
					    if (document.forms[0].chkVoidSelected.checked == false) {
					        if (replace(document.forms[0].txtFromDate.value, " ", "") == "") {
					            //alert("Please enter the From date.");  //Rakhel ML changes
					            alert(VoidDiaryValidations.EnterFromDate);
					            document.forms[0].txtFromDate.focus();
					            return false;
					        }

					        if (replace(document.forms[0].txtToDate.value, " ", "") == "") {
					            //alert("Please enter the To date."); //Rakhel ML changes
					            alert(VoidDiaryValidations.EnterToDate);
					            document.forms[0].txtToDate.focus();
					            return false;
					        }
					        //Aman MITS 31225 --Start
					        //                        if(document.forms[0].txtFromDate!=null)
					        //                        {
					        //                        var strfd=document.forms[0].txtFromDate.value;
					        //                        }
					        //                        if(document.forms[0].txtToDate!=null)
					        //                        {
					        //                        var strtd=document.forms[0].txtToDate.value;
					        //                        }
					        //                        var arrfd=strfd.split("/");
					        //                        var arrtd=strtd.split("/");

					        //                        if(arrfd[0].length!=2)
					        //                        {
					        //                        arrfd[0]='0'+arrfd[0];
					        //                        }

					        //                        if(arrfd[1].length!=2)
					        //                        {
					        //                        arrfd[1]='0'+arrfd[1];
					        //                        }

					        //                        if(arrtd[0].length!=2)
					        //                        {
					        //                        arrtd[0]='0'+arrtd[0];
					        //                        }

					        //                        if(arrtd[1].length!=2)
					        //                        {
					        //                        arrtd[1]='0'+arrtd[1];
					        //                        }
					        //                        var strfd=arrfd[2]+arrfd[0]+arrfd[1];
					        //                        var strtd=arrtd[2]+arrtd[0]+arrtd[1];
					        //                        if(strfd>strtd)
					        //                        {
					        //                            //alert("From date is greater than To date"); //Rakhel ML changes
					        //                            alert(VoidDiaryValidations.ValidDate);
					        //                            return false;
					        //                        }
					        var target = $("#txtFromDate");
					        var sectarget = $("#txtToDate");
					        var inst = $.datepicker._getInst(target[0]);
					        var secinst = $.datepicker._getInst(sectarget[0]);

					        fdate = inst.selectedDay;
					        fmonth = inst.selectedMonth;
					        fyear = inst.selectedYear;
                                      
                        //mbahl3 Mits 31638 start
                        if (document.forms[0].txtFromDate.value != "" && (fdate=="0")) {
                            var format = target.datepicker('option', 'dateFormat');
                            var format1 = format.substr(0, 2).toUpperCase();
                            var format2 = format.substr(3, 2).toUpperCase();
                            var format3 = format.substr(6, 4).toUpperCase();
                            var sDate = document.forms[0].txtFromDate.value;
                            var sDateSeparator = format.substr(2, 1);

                            var sArr = sDate.split(sDateSeparator);
                            var dArr = new Array();

                            if (format1.substring(0, 1) === "D") {
                                dArr[0] = new String(parseInt(sArr[1], 10));
                                if (format3.substring(0, 1) === "Y") { // ddmmyy
                                    dArr[1] = new String(parseInt(sArr[0], 10));
                                    dArr[2] = new String(parseInt(sArr[2], 10));
                                }
                                else { //ddyymm
                                    dArr[1] = new String(parseInt(sArr[2], 10));
                                    dArr[2] = new String(parseInt(sArr[0], 10));
                                }
                            }

                            if (format1.substring(0, 1) === "M") {
                                dArr[0] = new String(parseInt(sArr[0], 10));
                                if (format3.substring(0, 1) === "Y") { // mmddyy
                                    dArr[1] = new String(parseInt(sArr[1], 10));
                                    dArr[2] = new String(parseInt(sArr[2], 10));
                                }
                                else { //mmyydd
                                    dArr[1] = new String(parseInt(sArr[2], 10));
                                    dArr[2] = new String(parseInt(sArr[1], 10));
                                }
                            }

                            if (format1.substring(0, 1) === "Y") {
                                dArr[0] = new String(parseInt(sArr[2], 10));
                                if (format3.substring(0, 1) === "M") { // yyddmm
                                    dArr[1] = new String(parseInt(sArr[0], 10));
                                    dArr[2] = new String(parseInt(sArr[1], 10));
                                }
                                else { //yymmdd
                                    dArr[1] = new String(parseInt(sArr[1], 10));
                                    dArr[2] = new String(parseInt(sArr[0], 10));
                                }
                            }
                            dArr[2] = Get4DigitYear(dArr[2]);
                            fdate = dArr[1];
                            fmonth = dArr[0]-1;
                            fyear = dArr[2];
        
                        }
                             

					        secdate = secinst.selectedDay;
					        secmonth = secinst.selectedMonth;
					        secyear = secinst.selectedYear;
					        if (document.forms[0].txtToDate.value != "" && (secdate == "0")) {
					            var format = target.datepicker('option', 'dateFormat');
					            var format1 = format.substr(0, 2).toUpperCase();
					            var format2 = format.substr(3, 2).toUpperCase();
					            var format3 = format.substr(6, 4).toUpperCase();
					            var sDate = document.forms[0].txtToDate.value;
					            var sDateSeparator = format.substr(2, 1);

					            var sArr = sDate.split(sDateSeparator);
					            var dArr = new Array();

					            if (format1.substring(0, 1) === "D") {
					                dArr[0] = new String(parseInt(sArr[1], 10));
					                if (format3.substring(0, 1) === "Y") { // ddmmyy
					                    dArr[1] = new String(parseInt(sArr[0], 10));
					                    dArr[2] = new String(parseInt(sArr[2], 10));
					                }
					                else { //ddyymm
					                    dArr[1] = new String(parseInt(sArr[2], 10));
					                    dArr[2] = new String(parseInt(sArr[0], 10));
					                }
					            }

					            if (format1.substring(0, 1) === "M") {
					                dArr[0] = new String(parseInt(sArr[0], 10));
					                if (format3.substring(0, 1) === "Y") { // mmddyy
					                    dArr[1] = new String(parseInt(sArr[1], 10));
					                    dArr[2] = new String(parseInt(sArr[2], 10));
					                }
					                else { //mmyydd
					                    dArr[1] = new String(parseInt(sArr[2], 10));
					                    dArr[2] = new String(parseInt(sArr[1], 10));
					                }
					            }

					            if (format1.substring(0, 1) === "Y") {
					                dArr[0] = new String(parseInt(sArr[2], 10));
					                if (format3.substring(0, 1) === "M") { // yyddmm
					                    dArr[1] = new String(parseInt(sArr[0], 10));
					                    dArr[2] = new String(parseInt(sArr[1], 10));
					                }
					                else { //yymmdd
					                    dArr[1] = new String(parseInt(sArr[1], 10));
					                    dArr[2] = new String(parseInt(sArr[0], 10));
					                }
					            }
					            dArr[2] = Get4DigitYear(dArr[2]);
					            secdate = dArr[1];
					            secmonth = dArr[0]-1;
					            secyear = dArr[2];

					        }
							//mbahl3 mits 31638 end
					        var fromDate = new Date();
					        fromDate.setFullYear(fyear, fmonth, fdate);

					        var toDate = new Date();
					        toDate.setFullYear(secyear, secmonth, secdate);

					        if (fromDate > toDate ) {
					            alert(VoidDiaryValidations.ValidDate);
					            return false;
					        }
					    }
					return true;
					//Aman MITS 31225 --End
					}

                    function replace(sSource, sSearchFor, sReplaceWith)
                    {
	                    var arr = new Array();
	                    arr=sSource.split(sSearchFor);
	                    return arr.join(sReplaceWith);
	                }
	                //Rakhel ML changes - Start
	                function Disp() {
	                    $.datepicker._disabledInputs = [];
	                }
	                //Rakhel ML changes - End
				</script>
</head>
<body onload="SetFromToDate()" onunload="Disp()">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <table border="0" align="center">
     <tr>
                 <td colspan="2" align="left">
                   <asp:ImageButton ID="btnVoid" ImageUrl="~/Images/tb_diaryhistoryvoid_active.png" class="bold" ToolTip="<%$ Resources:ttVoid %>"
                        runat="server" OnClientClick="return ValForm();" OnClick="btnVoid_Click" />
                   <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
                 </td>
            </tr>
     
     <tr>
        <td colspan="2" class="ctrlgroup">
          <asp:Label ID="lblVoidDiaryResrc" runat="server" Text="<%$ Resources:lblVoidDiaryResrc %>"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td class="required">
          <asp:Label ID="lblFromDateResrc" class="required" runat="server" Text="<%$ Resources:lblFromDateResrc %>"></asp:Label> 
           
        </td>
        <td  class="required">
            <input type="text" value="" runat="server" id="txtFromDate" size="15" onblur="dateLostFocus(this.id);"/>
           
            <!-- Rakhel ML Changes -Start !-->
            <script type="text/javascript">
                $(function () {
                    $("#txtFromDate").datepicker({
                        showOn: "button",
                        buttonImage: "../../Images/calendar.gif",
                        buttonImageOnly: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeYear: true
                    });
                });
            </script>
            <!-- Rakhel ML Changes - End !-->
        </td>
    </tr>
     <tr>
         <td  class="required">
          <asp:Label ID="lblToDateResrc" class="required" runat="server" Text="<%$ Resources:lblToDateResrc %>"></asp:Label>   
          <%--   tmalhotra3 MITS:30998--%>
        </td>
         <td  class="required">
            <input type="text" value="" id="txtToDate" size="15" runat="server" onblur="dateLostFocus(this.id);"/>
             
            <!-- Rakhel ML Changes -Start !-->
            <script type="text/javascript">
                $(function () {
                    $("#txtToDate").datepicker({
                        showOn: "button",
                        buttonImage: "../../Images/calendar.gif",
                        buttonImageOnly: true,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeYear: true
                    });
                });
            </script>
            <!-- Rakhel ML Changes - End !-->
         </td>
    </tr>
    <tr>
     <td colspan="2" class="ctrlgroup">
        <asp:Label ID="lblVoidSeleResrc" class="required" runat="server" Text="<%$ Resources:lblVoidSeleResrc %>"></asp:Label> 
    </td>
    </tr>
    <tr>
     <td>
         <asp:Label  ID="lblVoidSelectedDiary" runat="server" Text=""></asp:Label>
         <input type="checkbox" runat="server" id="chkVoidSelected" onclick="chkChange()"/>
      </td>
    </tr>
    <%--<tr>
        <td colspan="2" align="center">
            <asp:Button ID="btnVoid" CssClass="button" Text="Void" runat="server" 
                OnClientClick="return ValForm();" onclick="btnVoid_Click" />
            <asp:Button ID="btnCancel" CssClass="button" Text="Cancel" runat="server" 
                onclick="btnCancel_Click" />
        </td> 
    </tr>--%>
    </table> 
    </form>
</body>
</html>
