﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Telerik.Web.UI;
using Riskmaster.RMXResourceManager;
using System.Globalization;
using System.IO;//WWIG GAP20A - agupta298 - MITS 36804

namespace Riskmaster.UI.Diaries
{
    public partial class DiaryList : System.Web.UI.Page
    {
        //Mits 36708
        int claimantLength = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            string previousPage = string.Empty;
            try
            {
                //Rakhel ML Changes
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryList.aspx"), "DiaryListValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DiaryListValidations", sValidationResources, true);
                //Rakhel ML Changes

                if (!IsPostBack)
                {
                    //nnithiyanand - 33561 - ActiveDiaries check box getting disabled - Starts
                    if (!object.ReferenceEquals(Session["GlobalActiveDiaryChecked"], null))
                    {
                        if (Session["GlobalActiveDiaryChecked"].ToString() == "1")
                        {
                            chkActiveDiaryChecked.Checked = true;
                        }
                        else if (Session["GlobalActiveDiaryChecked"].ToString() == "0")
                        {
                            chkActiveDiaryChecked.Checked = false;
                        }
                    }
                    if (Session["ShowAllDiariesRelatedToClaimchecked"] != null && Session["ShowAllDiariesRelatedToClaimchecked"].ToString()=="yes")
                    {
                        chkShowAllDiariesRelatedToClaim.Checked = true;
                        
                    }
                    else
                    {
                        chkShowAllDiariesRelatedToClaim.Checked = false;
                        
                    }
                    //nnithiyanand - 33561 - ActiveDiaries check box getting disabled - Ends

                    // divGrid.Attributes.Add("style", "overflow:auto;height:245px;width:99.5%;border-width:'0'");
                    // divGrid.Attributes.Add("style", "overflow:auto;height:315px;width:99.5%;border-width:'0'");//Changed by Amitosh for mits 23655 (05/02/2011)
                    //divGrid.Attributes.Add("style", "height:auto;width:99.5%;border-width:'0'");// Changed by Ishan (22/12/2011)
                    //divGrid.Attributes.Add("class", "divScroll");

                    //Ankit Start : Working on MITS - 30730 (RO)
                    Session["activediarychecked"] = null;
                    Session["claimactivediarychecked"] = null;
                    //Ankit End
                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

					//pkandhari Jira 6412 starts
                    if (Request.QueryString["assignedgroup"] != null)
                    {
                        ViewState["assignedgroup"] = Request.QueryString["assignedgroup"];
                    }
                    else
                    {
                        ViewState["assignedgroup"] = "";
                    }
					//pkandhari Jira 6412 ends

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";
                    }
                    //Indu - Mits 33843 - for showing the Parent record of the diary in Diary list
                    if (Request.QueryString["parentrecord"] != null)
                    {
                        ViewState["parentrecord"] = Request.QueryString["parentrecord"];
                    }
                    else
                    {
                        ViewState["parentrecord"] = "";
                    }
                    //smishra54 1 Sep 2011: MITS 25643
                    if (Request.QueryString["attachrecname"] != null)
                    {
                        ViewState["attachrecname"] = Request.QueryString["attachrecname"];
                    }
                    else
                    {
                        ViewState["attachrecname"] = "";
                    }
                    //smishra54: End 

                    if (Request.QueryString["attachfdmdetail"] != null)
                    {
                        ViewState["attachfdmdetail"] = Request.QueryString["attachfdmdetail"];
                    }
                    else
                    {
                        ViewState["attachfdmdetail"] = "";
                    }

                    if (Request.QueryString["attachlob"] != null)
                    {
                        ViewState["attachlob"] = Request.QueryString["attachlob"];
                    }
                    else
                    {
                        ViewState["attachlob"] = "";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }

                    //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                    if (Request.QueryString["usersortorder"] != null)
                    {
                        ViewState["usersortorder"] = Request.QueryString["usersortorder"];
                    }
                    else
                    {
                        ViewState["usersortorder"] = "0";
                    }
                    //end Mits 23477
                    //MITS 36519
                    if (Request.QueryString["sortexpression"] != null)
                    {
                        ViewState["sortexpression"] = Request.QueryString["sortexpression"];
                    }
                    else
                    {
                        ViewState["sortexpression"] = "0";
                    }
                    //Neha Mits 23586 05/16/2011
                    if (ViewState["taskname"] == null)
                    {
                        ViewState["taskname"] = "";
                    }
                    if (ViewState["taskfilter"] == null)
                    {
                        ViewState["taskfilter"] = "";
                    }//End

                    ViewState["sortorder"] = "";//Deb: MITS 32961 
                    ViewState["shownotes"] = "";      // csingh7 MITS 18435
                    //ViewState["shownotes"] = "1";                    
                    //aanandpraka2:Start changes for MITS 27074
                    //ViewState["activediarychecked"] = "";
                    if (Session["activediarychecked"] == null)
                            Session.Add("activediarychecked", "");                       
                    if (Session["claimactivediarychecked"] == null)
                            Session.Add("claimactivediarychecked", "");                                              
                    //aanandpraka2:End changes
                    if (Session["ShowAllDiariesRelatedToClaimchecked"] == null)
                        Session.Add("ShowAllDiariesRelatedToClaimchecked", "");//vkumar258          
                    ViewState["FilterExpr"] = "";
                    ViewState["reqactdiary"] = "0";
					//rkulavil: RMA-18019 starts
                    //if (Session["Showalldiariesforactiverecord"] != null && Session["Showalldiariesforactiverecord"].ToString() == "yes")//vkumar258
                    //{
                    //    chkShowActiveDiariesForRecordOnly.Checked = true;
                    //}
                    //else
                    //{
                    //    chkShowActiveDiariesForRecordOnly.Checked = false;
                    //}
                    if (!string.IsNullOrEmpty(Page.Request.UrlReferrer.ToString()))
                    {
                        previousPage=Page.Request.UrlReferrer.ToString();
                    }
                    if (object.ReferenceEquals(Session["Showalldiariesforactiverecord"], null) || previousPage.Contains("/RiskmasterUI/MDI/Default.aspx"))
                    {
                        ViewState["Showalldiariesforactiverecord"] = "yes";
                        Session["Showalldiariesforactiverecord"] = ViewState["Showalldiariesforactiverecord"];
                    }
                    else
                    {
                        ViewState["Showalldiariesforactiverecord"] = Session["Showalldiariesforactiverecord"];
                    }
                    //ViewState["Showalldiariesforactiverecord"] = Session["Showalldiariesforactiverecord"];// "yes";
                    chkShowActiveDiariesForRecordOnly.Checked = ViewState["Showalldiariesforactiverecord"] == "yes" ? true : false;
					//rkulavil: RMA-18019 ends
                    ViewState["ShowAllDiariesRelatedToClaim"] = "no";
                    ViewState["showregarding"] = "";  
                    objDiaryCurrentAction = new DiaryListCurrentAction();

                    objDiaryCurrentAction.Pagenumber = "1";
                    if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                        objDiaryCurrentAction.Duedate = "today";
                    else
                        objDiaryCurrentAction.Duedate = string.Empty;
                    //sshrikrishna:Start Changes for MITS 27074                  
                    if (Request.QueryString.Count == 0)
                    {
                        objDiaryCurrentAction.Diarysource = "D";
                        //skarandhama2-MITS 29913-25/04/2013-Start here
                        lstDiary.Height = System.Web.UI.WebControls.Unit.Pixel(450);
                        //skarandhama2-MITS 29913-25/04/2013-End here
                    }
                    else
                    {
                        objDiaryCurrentAction.Diarysource = "C";
                        //skarandhama2-MITS 29913-25/04/2013-Start here
                        lstDiary.Height = System.Web.UI.WebControls.Unit.Pixel(415);
                        //skarandhama2-MITS 29913-25/04/2013-End here

                    }
                        objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                        objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();//vkumar258
                    if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                    //sshrikrishna:End Changes
                    objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                    if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                    {
                    objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                    }
                    //Ankit Start : Worked on MITS - 29939
                    //if (!chkActiveDiaryChecked.Checked)
                    //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                    //Ankit End
                    objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                    objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                    objDiaryCurrentAction.Sortorder = "";//Deb: MITS 32961 
                    //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                    objDiaryCurrentAction.Usersortorder = ViewState["usersortorder"].ToString();
                    //End Mits 23477
                    objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                    GridViewBinder(objDiaryCurrentAction, true);

                    if (chkActiveDiaryChecked.Checked == false)
                    {
                        //txtdatedue.Text = System.DateTime.Now.Date.ToShortDateString();
                    }

                    //Commented by Amitosh because paging is through Grid 
                    //    btnFirstPage.Enabled = false;
                    // btnPrev.Enabled = false;

             //       GridViewBinder(objDiaryCurrentAction,true);

//MITS 27074 Start changes

                    if (ViewState["ispeekdiary"].ToString() == "true")
                    {
                        //set peek style of buttons
                        SetPeekStyle();
                    }
                    else
                    {
                        if (ViewState["attachrecordid"].ToString() == "")
                        {                            
                            SetDefaultStyle();
                        }
                        else
                        {                         
                            SetAttachDiaryStyle();
                        }
                    }                    
//MITS 27074 end changes
                }
                //smishra54 1 Sep 2011: MITS 25643
                else
                {
                    if (Request.Form["__EVENTTARGET"] != null && string.Equals(Request.Form["__EVENTTARGET"], "attachdiary"))
                    {
                        objDiaryCurrentAction = new DiaryListCurrentAction();

                        objDiaryCurrentAction.Pagenumber = ViewState["pagenumber"].ToString();
                        if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                            objDiaryCurrentAction.Duedate = "today";
                        else
                            objDiaryCurrentAction.Duedate = string.Empty;
                        //sshrikrishna:Start Changes for MITS 27074                  
                        //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                        if (Request.QueryString.Count == 0)
                        {
                            objDiaryCurrentAction.Diarysource = "D";
                            //skarandhama2-MITS 29913-25/04/2013-Start here
                            lstDiary.Height = System.Web.UI.WebControls.Unit.Pixel(450);
                            //skarandhama2-MITS 29913-25/04/2013-End here
                        }
                        else
                        {
                            objDiaryCurrentAction.Diarysource = "C";
                            //skarandhama2-MITS 29913-25/04/2013-Start here
                            lstDiary.Height = System.Web.UI.WebControls.Unit.Pixel(415);
                            //skarandhama2-MITS 29913-25/04/2013-End here
                        }
                        if (Session["activediarychecked"] != null)
                            objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                        if (Session["claimactivediarychecked"] != null)
                            objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                        //sshrikrishna:End Changes
                        objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                        if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                        objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                        //Ankit Start : Worked on MITS - 29939
                        //if (!chkActiveDiaryChecked.Checked)
                        //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                        //Ankit End
                        if(Session["ShowAllDiariesRelatedToClaimchecked"]!=null)//vkumar258
                            objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                        if(Session["ShowAllDiariesRelatedToClaimchecked"]!=null)//vkumar258
                            objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                        objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                        objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                        objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                        objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                        objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                        objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                        GridViewBinder(objDiaryCurrentAction,true);
                    }
                }
                //smishra54 :End
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

           //protected void lstDiary_RowDataBound(object sender, GridViewRowEventArgs e)
        protected void lstDiary_ItemDataBound(object sender, GridItemEventArgs e)//Changed the ID of the Grid
        {
            string imageurl = @"../../Images/";
            string imageSortUpUrl = imageurl + "arrow_up_white.gif";
            string imageSortDownUrl = imageurl + "arrow_down_white.gif";

            try
            {

                //Ankit Start : Working on MITS 30730
                if (e.Item is GridItem)
                {
                    RadDatePicker txtToOrderDatePicker = ((GridItem)e.Item).FindControl("filterRadDatePicker") as RadDatePicker;
                    if (txtToOrderDatePicker != null)
                    {
                        if (DateTime.Compare(DateTime.Today.AddYears(-1), Convert.ToDateTime(this.startDate)) != 0)
                            txtToOrderDatePicker.SelectedDate = this.startDate;
                    }
                }
                //Ankit End
                //Added by Amitosh to hide the pager combobox from the pagertemplate   


                if (e.Item is GridPagerItem)
                {
                    GridPagerItem pager = (GridPagerItem)e.Item;
                    Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                    lbl.Visible = false;

                    RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                    combo.Visible = false;
                }
                //end Amitosh
                //setting sorting Images as per user action
                if (e.Item is GridHeaderItem)
                {

                    //Ankit-Mits 30085-Start changes                    
                    ((GridHeaderItem)e.Item)["attachrecord"].ToolTip = RMXResourceProvider.GetSpecificObject("ttAttachRecord", RMXResourceProvider.PageId("DiaryList.aspx"), ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());

                    //Ankit-Mits 30085 end changes
                    //Ashish Ahuja: Mits 34105 start - Commenting the code which is not required.
                    //HtmlImage imgSort = new HtmlImage();
                    //imgSort.ID = "imgSort";
                    //if (ViewState["sortcolumnindex"] != null)
                    //{
                    //    int colIndex = Convert.ToInt32(ViewState["sortcolumnindex"]);

                    //    if (ViewState["sortdirection"].ToString() == "ASC")
                    //    {
                    //        imgSort.Src = imageSortUpUrl;
                    //    }
                    //    else
                    //    {
                    //        imgSort.Src = imageSortDownUrl;
                    //    }

                    //    e.Item.Cells[colIndex].Controls.Add(imgSort);
                    //}
                    //else
                    //{
                    //    imgSort.Src = imageSortDownUrl;
                    //    e.Item.Cells[3].Controls.Add(imgSort);
                    //}
                    //Ashish Ahuja: Mits 34105 End
                    SetHeaderCofig(e.Item);
                    //MITS 36519
                    string columnName = Convert.ToString(ViewState["sortexpression"]);
                    ((GridHeaderItem)e.Item)[columnName].Style.Add("BackGround", "#FFCA5E");
                }

                //setting priority images
                if (e.Item is GridDataItem)
                {



                    string txtRegarding = string.Empty;
                    string txtNotes = string.Empty;
                    string txtCreatedOn = string.Empty;
                    string txtAssignedTo = string.Empty;
                    string txtAssignedFrom = string.Empty;
                    string txtStatus = string.Empty;
                    string txtEntryId = string.Empty;
                    string txtAttachPrompt = string.Empty;
                    string txtTaskName = string.Empty;
                    string txtWorkAct = string.Empty;
                    string txtAttachTable = string.Empty;
                    string txtAttachedRecordId = string.Empty;
                    string txtAttachedRecordCategory = string.Empty;
                    //igupta3 jira 439
                    string txtNotRoutable = string.Empty;
                    string txtNotRollable = string.Empty;


                    //rupal:start,r8 auto diary enh
                    string txtAttachTableForNav = string.Empty;
                    string txtAttachedRecordIdForNav = string.Empty;
                    //rupal:end


                    Label gridLblDueDate = null;
                    Label gridNotesLabel = null;
                    Label gridClaimantLabel = null;
                    Label gridClaimStatusLabel = null;
                    Label gridDepartmentLabel = null;
                    Label gridLblTaskSubjectLabel = null;
                    Label gridWrokActivityLabel = null;
                    Label gridAttachRecordLabel = null;
                    Label gridToLabel = null;
                    Label gridFromLabel = null;
                    Label gridParentRecord = null;
                    //igupta3 jira 439
                    Label gridNonRoute = null;
                    Label gridNonRoll = null;

                    string javascriptShowDetailsMethod = string.Empty;

                    //setting duedate value in gridview
                    gridLblDueDate = (Label)e.Item.Cells[3].FindControl("lblDueDate");
                    //Deb : e.Item should be used instead of e.Row, e.Item is telerik property and e.Row is .NET Gridview property
                    DataRowView rowData = e.Item.DataItem as DataRowView;

                    //START srajindersin MITS 32606 dt:05/14/2013 
                    //try
                    //{
                    //    gridLblDueDate.Text = DataBinder.Eval(e.Row.DataItem, "complete_date").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    gridLblDueDate.Text = "";
                    //}
                    if (rowData.Row.Table.Columns.Contains("complete_date") && DataBinder.Eval(e.Item.DataItem, "complete_date") != null)
                    {
                        gridLblDueDate.Text = AppHelper.GetDate(DataBinder.Eval(e.Item.DataItem, "complete_date").ToString());
                    }
                    else
                    {
                        gridLblDueDate.Text = "";
                    }

                    //try
                    //{
                    //    txtTaskName = DataBinder.Eval(e.Row.DataItem, "tasksubject").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    txtTaskName = "";
                    //}
                    if (rowData.Row.Table.Columns.Contains("tasksubject") && DataBinder.Eval(e.Item.DataItem, "tasksubject") != null)
                    {
                        txtTaskName = DataBinder.Eval(e.Item.DataItem, "tasksubject").ToString();
                    }
                    else
                    {
                        txtTaskName = "";
                    }

                    //txtRegarding = DataBinder.Eval(e.Row.DataItem, "regarding").ToString();
                    if (rowData.Row.Table.Columns.Contains("regarding") && DataBinder.Eval(e.Item.DataItem, "regarding") != null)
                    {
                        txtRegarding = DataBinder.Eval(e.Item.DataItem, "regarding").ToString();
                    }
                    else
                    {
                        txtRegarding = "";
                    }

                    //try
                    //{
                    //    txtNotes = DataBinder.Eval(e.Row.DataItem, "diary_Text").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    txtNotes = "";
                    //}
                    if (rowData.Row.Table.Columns.Contains("diary_Text") && DataBinder.Eval(e.Item.DataItem, "diary_Text") != null)
                    {
                        txtNotes = DataBinder.Eval(e.Item.DataItem, "diary_Text").ToString();
                    }
                    else
                    {
                        txtNotes = "";
                    }

                    //try
                    //{
                    //    txtAttachTable  = DataBinder.Eval(e.Row.DataItem, "attach_table").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    txtAttachTable = "";
                    //}
                    if (rowData.Row.Table.Columns.Contains("attach_table") && DataBinder.Eval(e.Item.DataItem, "attach_table") != null)
                    {
                        txtAttachTable = DataBinder.Eval(e.Item.DataItem, "attach_table").ToString();
                    }
                    else
                    {
                        txtAttachTable = "";
                    }
                    //rsharma220 MITS 34479 Start
                    if (rowData.Row.Table.Columns.Contains("attach_table_for_nav") && DataBinder.Eval(e.Item.DataItem, "attach_table_for_nav") != null)
                    {
                        txtAttachTableForNav = DataBinder.Eval(e.Item.DataItem, "attach_table_for_nav").ToString();
                    }
                    else
                    {
                        txtAttachTableForNav = "";
                    }
                    //try
                    //{
                    //    txtAttachedRecordId = DataBinder.Eval(e.Row.DataItem, "attach_recordid").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    txtAttachedRecordId = "";
                    //}

                    if (rowData.Row.Table.Columns.Contains("attach_recordid_for_nav") && DataBinder.Eval(e.Item.DataItem, "attach_recordid_for_nav") != null)
                    {
                        txtAttachedRecordIdForNav = DataBinder.Eval(e.Item.DataItem, "attach_recordid_for_nav").ToString();
                    }
                    else
                    {
                        txtAttachedRecordIdForNav = "";
                    }
                    //rsharma220 MITS 34479 End
                    if (rowData.Row.Table.Columns.Contains("attach_recordid") && DataBinder.Eval(e.Item.DataItem, "attach_recordid") != null)
                    {
                        txtAttachedRecordId = DataBinder.Eval(e.Item.DataItem, "attach_recordid").ToString();
                    }
                    else
                    {
                        txtAttachedRecordId = "";
                    }
                    //END srajindersin MITS 32606 dt:05/14/2013 

                    // akaushik5 Added for MITS 37900 Starts
                    bool IsAttachedRecordAccessible = rowData.Row.Table.Columns.Contains("attachedrecordaccessible")
                        && DataBinder.Eval(e.Item.DataItem, "attachedrecordaccessible") != null
                        && DataBinder.Eval(e.Item.DataItem, "attachedrecordaccessible").ToString().ToLower().Equals("true");
                    // akaushik5 Added for MITS 37900 Ends


                    txtCreatedOn = DataBinder.Eval(e.Item.DataItem, "create_date").ToString();
                    txtAssignedTo = DataBinder.Eval(e.Item.DataItem, "assigned_user").ToString();
                    txtAssignedFrom = DataBinder.Eval(e.Item.DataItem, "assigning_user").ToString();
                    txtStatus = DataBinder.Eval(e.Item.DataItem, "status").ToString();
                    txtEntryId = DataBinder.Eval(e.Item.DataItem, "entry_id").ToString();
                    txtAttachPrompt = DataBinder.Eval(e.Item.DataItem, "attachprompt").ToString();
                    txtWorkAct = DataBinder.Eval(e.Item.DataItem, "work_activity").ToString();
                    //igupta3 jira 439
                    txtNotRoutable = DataBinder.Eval(e.Item.DataItem, "notroutablecode").ToString();
                    txtNotRollable = DataBinder.Eval(e.Item.DataItem, "notrollablecode").ToString();

                    if (txtRegarding == string.Empty)
                    {
                        txtRegarding = "***";
                    }
                    else
                    {
                        if (txtRegarding.IndexOf("'") != -1)
                        {
                            txtRegarding = txtRegarding.Replace("'", "`");
                        }
                    }

                    if (txtNotes.IndexOf("'") != -1)
                    {
                        txtNotes = txtNotes.Replace("'", "`");
                    }

                    if (txtCreatedOn == string.Empty)
                    {
                        txtCreatedOn = "***";
                    }

                    if (txtAssignedTo == string.Empty)
                    {
                        txtAssignedTo = "***";
                    }

                    if (txtAssignedFrom == string.Empty)
                    {
                        txtAssignedFrom = "***";
                    }

                    if (txtStatus == string.Empty)
                    {
                        txtStatus = "***";
                    }

                    //added by Nitin for Mits 15568
                    txtNotes = txtNotes.Replace("\r\n", " ");
                    //end by Nitin for Mits 15568

                    //javascriptShowDetailsMethod = "showDiaryDetails('" + txtRegarding.Replace("'", "&apos;") + "','" + txtNotes.Replace("'", "&apos;") + "','" + txtCreatedOn + "','" + txtAssignedTo.Replace("'", "&apos;") + "','" + txtAssignedFrom.Replace("'", "&apos;") + "','" + txtStatus + "','" + txtEntryId + "','" + txtAttachPrompt.Replace("'", "&apos;") + "','" + gridLblDueDate.Text + "','" + txtTaskName.Replace("'", "&apos;") + "')";
                    //Deb : single qoute issue fixed as claim number may contain single qoute
                    javascriptShowDetailsMethod = "showDiaryDetails('" + txtRegarding.Replace("'", "&apos;") + "','" + txtNotes.Replace("'", "&apos;") + "','" + txtCreatedOn + "','" + txtAssignedTo.Replace("'", "&apos;") + "','" + txtAssignedFrom.Replace("'", "&apos;") + "','" + txtStatus + "','" + txtEntryId + "',\"" + txtAttachPrompt.Replace("'", "&apos;") + "\",'" + gridLblDueDate.Text + "','" + txtTaskName.Replace("'", "&apos;") + "','" + txtNotRoutable + "','" + txtNotRollable + "')"; //igupta3 jira 439
                    //showing details of a particular row , after on click it at ,client side
                    e.Item.Attributes.Add("onclick", javascriptShowDetailsMethod);
                    e.Item.Attributes.Add("txtNotRoutable", txtNotRoutable);
                    e.Item.Attributes.Add("txtNotRollable", txtNotRollable);
                    //tanwar2 - mits 30533
                    //if (DataBinder.Eval(e.Item.DataItem, "priority").ToString() != "1")
                    if (!(DataBinder.Eval(e.Item.DataItem, "priority").ToString() == "1" || DataBinder.Eval(e.Item.DataItem, "priority").ToString() == "0"))
                    {
                        System.Web.UI.WebControls.Image imgPriority = (System.Web.UI.WebControls.Image)e.Item.FindControl("PriorityImage");
                        if (imgPriority != null)
                            imgPriority.ImageUrl = GetPrioiryUrl(DataBinder.Eval(e.Item.DataItem, "priority").ToString());
                        switch (DataBinder.Eval(e.Item.DataItem, "priority").ToString())
                        {
                            case "1":
                                imgPriority.ToolTip = "Optional";
                                break;
                            case "2":
                                imgPriority.ToolTip = "Important";
                                break;
                            case "3":
                                imgPriority.ToolTip = "Required";
                                break;

                        }

                        //  imgPriority.Src = GetPrioiryUrl(DataBinder.Eval(e.Item.DataItem, "priority").ToString());
                        //   e.Item.Cells[1].Controls.Add(imgPriority);
                    }
                    else
                    {
                        System.Web.UI.WebControls.Image imgPriority = (System.Web.UI.WebControls.Image)e.Item.FindControl("PriorityImage");
                        if (imgPriority != null)
                            imgPriority.Visible = false;
                    }

                    //pkandhari Jira 6412 starts
                    System.Web.UI.WebControls.Image imgAssignedTo = (System.Web.UI.WebControls.Image)e.Item.FindControl("AssignedToImage");
                    if ((DataBinder.Eval(e.Item.DataItem, "assigned_group").ToString() == "0"))
                    {
                        e.Item.Attributes.Add("grpflag", "false");
                        imgAssignedTo.ToolTip = "Diary Assigned To User";
                        imgAssignedTo.ImageUrl = "../../Images/diary_assigned2individual.gif";
                    }
                    else
                    {
                        e.Item.Attributes.Add("grpflag","true");
                        ViewState["assignedgroup"] = DataBinder.Eval(e.Item.DataItem, "assigned_user").ToString();
                        imgAssignedTo.ToolTip = "Diary Assigned To Group";
                        imgAssignedTo.ImageUrl = "../../Images/diary_assigned2group.gif";
                    }
                    //pkandhari Jira 6412 ends

                    //         GridDataItem item = (GridDataItem)e.Item;

                    //    strtxt = item["ImageURL"].Text;
                    // if (!string.IsNullOrEmpty(strtxt))
                    /// {
                    //     Image img = (Image)item.FindControl("image");
                    //     if (img != null)
                    //         img.ImageUrl = strtxt;
                    // }
                    // else
                    // {
                    //     Image img = (Image)item.FindControl("image");
                    //     if (img != null)
                    //         img.Visible = false;
                    // }

                    // string sPrioritylevel = item["PriorityLevel"].Text;
                    // if (sPrioritylevel != null)
                    // {
                    //     sPriorityURL = GetPrioiryUrl(sPrioritylevel);
                    // }

                    // if (!string.IsNullOrEmpty(sPriorityURL))
                    // {
                    //     Image imgPriority = (Image)item.FindControl("PriorityImage");
                    //     if (imgPriority != null)
                    //         imgPriority.ImageUrl = sPriorityURL;
                    // }
                    // else
                    // {
                    //     Image imgPriority = (Image)item.FindControl("PriorityImage");
                    //     if (imgPriority != null)
                    //         imgPriority.Visible = false;
                    // }

                    string imgName = string.Empty;
                    //HtmlImage imgUser = new HtmlImage();
                    //imgUser.ID = "imgUser";
                    imgName = DataBinder.Eval(e.Item.DataItem, "img").ToString();
                    //    imgUser.Src = imgName;
                    System.Web.UI.WebControls.Image imgUser = (System.Web.UI.WebControls.Image)e.Item.FindControl("imageUser");
                    if (imgUser != null)
                        imgUser.ImageUrl = imgName;

                    //  e.Item.Cells[2].Controls.Add(imgUser);


                    gridLblTaskSubjectLabel = (Label)e.Item.Cells[4].FindControl("lbltaskSubject");
                    //gridLblTaskSubjectLabel.Attributes.Add("onclick", "DisplayDiaryDetails('" + txtEntryId + "','" + txtAssignedFrom + "','" + txtAttachPrompt + "','" + txtCreatedOn + "','" + ViewState["assigneduser"].ToString() + "','" + ViewState["ispeekdiary"].ToString() + "','" + ViewState["attachtable"].ToString() + "','" + ViewState["attachrecordid"].ToString() + "')");
                    //Deb : single qoute issue fixed as claim number may contain single qoute
                    gridLblTaskSubjectLabel.Attributes.Add("onclick", "DisplayDiaryDetails('" + txtEntryId + "','" + txtAssignedFrom + "',\"" + txtAttachPrompt + "\",'" + txtCreatedOn + "','" + ViewState["assigneduser"].ToString() + "','" + ViewState["ispeekdiary"].ToString() + "','" + ViewState["attachtable"].ToString() + "','" + ViewState["attachrecordid"].ToString() + "','" + txtNotRoutable + "','" + txtNotRollable + "','" + txtAssignedTo + "')"); //igupta3 jira 439
                    //Mits 36708
                    if (claimantLength > 0 && gridLblTaskSubjectLabel.Text.Trim().Length > claimantLength)
                    {
                        string originalTaskSubject = gridLblTaskSubjectLabel.Text;
                        gridLblTaskSubjectLabel.Text = gridLblTaskSubjectLabel.Text.Substring(0, claimantLength) + "...";
                        gridLblTaskSubjectLabel.ToolTip = originalTaskSubject;
                    }
                    else
                        gridLblTaskSubjectLabel.Text = gridLblTaskSubjectLabel.Text;
                    //setting notes in gridview
                    gridNotesLabel = (Label)e.Item.Cells[4].FindControl("lblGrdNotes");
                    //gridRegardingLabel = (Label)e.Item.Cells[4].FindControl("lblRegarding");



                    //Added by Amitosh for Terelik Grid

                    if ((ViewState["showregarding"].ToString() == "1") && (txtRegarding != "***"))
                    {
                        // gridRegardingLabel.Visible = true;

                        //if ((txtRegarding != "***") && (txtNotes != ""))
                        //{
                        //Mits 14637 starts by Nitin 13-04-2009
                        //Mits 36708
                        if (claimantLength != 0 && txtRegarding.Trim().Length > claimantLength)
                        {
                            gridNotesLabel.Text = txtRegarding.Substring(0, Convert.ToInt32(claimantLength)) + "...";
                            gridNotesLabel.ToolTip = txtRegarding;
                        }
                        else
                            gridNotesLabel.Text = txtRegarding;
                        //Mits 14637 ends by Nitin 13-04-2009
                        //}
                        //else if ((txtRegarding != "***") && (txtNotes == ""))
                        //{
                        //    gridNotesLabel.Text = txtRegarding;
                        //}
                        //else
                        //{
                        //    gridNotesLabel.Text = txtNotes;
                        //}
                    }
                    else
                    {
                        // gridRegardingLabel.Text = "";
                        gridNotesLabel.Text = "";
                        // gridRegardingLabel.Visible = false;
                        // gridNotesLabel.Visible = false;
                    }
                    //End Amitosh
                    if ((ViewState["shownotes"].ToString() == "1") && (txtNotes != ""))
                    {
                        // gridRegardingLabel.Visible = true;

                        //if ((txtRegarding != "***") && (txtNotes != ""))
                        //{
                        //    //Mits 14637 starts by Nitin 13-04-2009
                        if (string.IsNullOrEmpty(gridNotesLabel.Text))
                        {
                            //Mits 36708
                            if (claimantLength != 0 && txtNotes.Trim().Length > claimantLength)
                            {
                                gridNotesLabel.Text = txtNotes.Substring(0, Convert.ToInt32(claimantLength)) + "...";
                                gridNotesLabel.ToolTip = txtNotes;
                            }
                            else
                                gridNotesLabel.Text = txtNotes;
                        }
                        else
                        {
                            //Mits 36708
                            if (claimantLength != 0 && txtNotes.Trim().Length > claimantLength)
                            {
                                gridNotesLabel.Text = (txtRegarding + "<br/>" + txtNotes).Substring(0, Convert.ToInt32(claimantLength)) + "...";
                                gridNotesLabel.ToolTip = txtRegarding + "<br/>" + txtNotes;
                            }
                            else
                                gridNotesLabel.Text = txtRegarding + "<br/>" + txtNotes;
                        }
                        //    //Mits 14637 ends by Nitin 13-04-2009
                        //}
                        //else if ((txtRegarding != "***") && (txtNotes == ""))
                        //{
                        //    gridNotesLabel.Text = txtRegarding;
                        //}
                        //else
                        //{
                        //    gridNotesLabel.Text = txtNotes;
                        //}
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(gridNotesLabel.Text))
                        {
                            // gridRegardingLabel.Text = "";
                            gridNotesLabel.Text = "";
                            // gridRegardingLabel.Visible = false;
                            gridNotesLabel.Visible = false;
                        }
                    }




                    //if (ViewState["showregarding"].ToString() == "1")
                    //{
                    //    // gridRegardingLabel.Visible = true;

                    //    if ((txtRegarding != "***") && (txtNotes != ""))
                    //    {
                    //        //Mits 14637 starts by Nitin 13-04-2009
                    //        gridNotesLabel.Text = txtRegarding + "<br/>" + txtNotes;
                    //        //Mits 14637 ends by Nitin 13-04-2009
                    //    }
                    //    else if ((txtRegarding != "***") && (txtNotes == ""))
                    //    {
                    //        gridNotesLabel.Text = txtRegarding;
                    //    }
                    //    else
                    //    {
                    //        gridNotesLabel.Text = txtNotes;
                    //    }
                    //}
                    //else
                    //{
                    //    // gridRegardingLabel.Text = "";
                    //    gridNotesLabel.Text = "";
                    //    // gridRegardingLabel.Visible = false;
                    //    gridNotesLabel.Visible = false;
                    //}
                    ////End Amitosh
                    //if (ViewState["shownotes"].ToString() == "1")
                    //{
                    //    // gridRegardingLabel.Visible = true;

                    //    if ((txtRegarding != "***") && (txtNotes != ""))
                    //    {
                    //        //Mits 14637 starts by Nitin 13-04-2009
                    //        gridNotesLabel.Text = txtRegarding + "<br/>" + txtNotes;
                    //        //Mits 14637 ends by Nitin 13-04-2009
                    //    }
                    //    else if ((txtRegarding != "***") && (txtNotes == ""))
                    //    {
                    //        gridNotesLabel.Text = txtRegarding;
                    //    }
                    //    else
                    //    {
                    //        gridNotesLabel.Text = txtNotes;
                    //    }
                    //}
                    //else
                    //{
                    //    // gridRegardingLabel.Text = "";
                    //    gridNotesLabel.Text = "";
                    //    // gridRegardingLabel.Visible = false;
                    //    gridNotesLabel.Visible = false;
                    //}

                    //setting attachrecord field
                    gridAttachRecordLabel = (Label)e.Item.Cells[5].FindControl("lblAttachRecord");
                    //if (txtAttachPrompt != "") Commented by Amitosh because txtAttachPrompt will not be empty
                    if (txtAttachPrompt != "No")
                    {
                        //setting open attchment details through MDI
                        //rupal:start, r8 auto diary enh
                        if (txtAttachTableForNav == string.Empty)
                            txtAttachedRecordCategory = GetAttachedTableCategory(txtAttachTable);
                        else
                            txtAttachedRecordCategory = GetAttachedTableCategory(txtAttachTableForNav);
                        //rupal:end
                        //rupal:if condition added
                        if (txtAttachedRecordIdForNav == string.Empty)
                            txtAttachedRecordCategory = txtAttachedRecordCategory.Replace("{jobid}", txtAttachedRecordId);
                        txtAttachedRecordCategory = txtAttachedRecordCategory.Replace("{modulename}", txtAttachTable);      //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch
                        //starts by Nitin on 22-04-2009 in order to open some FDM screens as popups for Mits 15272
                        if (txtAttachedRecordCategory != string.Empty)
                        {
                            // akaushik5 Added for MITS 37900 Starts
                            if (IsAttachedRecordAccessible)
                            {
                                // akaushik5 Added for MITS 37900 Ends

                                //rupal:start, r8 auto diary enh
                                if (txtAttachedRecordIdForNav != string.Empty)
                                    txtAttachedRecordId = txtAttachedRecordIdForNav;
                                //rupal:end
                                if (IsOpenAttachedRecordInPopup(txtAttachedRecordCategory))
                                {
                                    gridAttachRecordLabel.Attributes.Add("onclick", "OpenAttachedRecordAsPopUp('" + txtAttachedRecordCategory + "','" + txtAttachedRecordId + "')");
                                }
                                else
                                {
                                    gridAttachRecordLabel.Attributes.Add("onclick", "parent.MDIShowScreen('" + txtAttachedRecordId + "','" + txtAttachedRecordCategory + "')");
                                }
                            }
                        }
                        //ended by Nitin on 22-04-2009 in order to open some FDM screens as popups for Mits 15272
                        // akaushik5 Added for MITS 37900 Starts
                        if (IsAttachedRecordAccessible)
                        {
                            gridAttachRecordLabel.Attributes.Add("style", "cursor:pointer");
                            gridAttachRecordLabel.Font.Underline = true;
                        }
                        else
                        {
                            txtAttachPrompt = string.Format("{0}{1}", txtAttachPrompt, RMXResourceProvider.GetSpecificObject("lblResourceNotAccessible", RMXResourceProvider.PageId("DiaryList.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()));
                        }
                        // akaushik5 Added for MITS 37900 Ends
                        gridAttachRecordLabel.CssClass = "LightBlue";
                        gridAttachRecordLabel.Font.Bold = true;
                    }
                    //else //Commented by Amitosh because else block will never execute
                    //{
                    //    txtAttachPrompt = "No";
                    //}

                    //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch Start
                    if(txtAttachPrompt.Contains("DIS") && txtAttachTable=="DIS")
                    {
                        txtAttachPrompt = txtAttachPrompt.Substring(0, 3);
                    }
                    if (txtAttachPrompt.Contains("DDS") && txtAttachTable=="DDS")
                    {
                        txtAttachPrompt = txtAttachPrompt.Substring(0, 3);
                    }
                    //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch End
                    gridAttachRecordLabel.Text = txtAttachPrompt;

                    //settting workactivity field
                    gridWrokActivityLabel = (Label)e.Item.Cells[6].FindControl("lblWorkActivity");
                    /*
                    if (txtWorkAct == "")
                    {
                        txtWorkAct = "No Work Activity";
                    }
                     */
                    //Mits 36708
                    if (claimantLength != 0 && txtWorkAct.Trim().Length > claimantLength)
                    {
                        gridWrokActivityLabel.Text = txtWorkAct.Substring(0, Convert.ToInt32(claimantLength)) + "...";
                        gridWrokActivityLabel.ToolTip = txtWorkAct;
                    }
                    else
                        gridWrokActivityLabel.Text = txtWorkAct;


                    //setting claimant field 
                    gridClaimantLabel = (Label)e.Item.Cells[7].FindControl("lblGrdClaimant");
                    gridDepartmentLabel = (Label)e.Item.Cells[8].FindControl("lblDepartment");
                    gridClaimStatusLabel = (Label)e.Item.Cells[9].FindControl("lblClaimStatus");
                    gridToLabel = (Label)e.Item.Cells[10].FindControl("lblTo");
                    gridFromLabel = (Label)e.Item.Cells[11].FindControl("lblFrom");
					//Indu - Mits 33843
                    gridParentRecord = (Label)e.Item.Cells[12].FindControl("lblParentRecord");
                    //igupta3 jira 439
                    gridNonRoute = (Label)e.Item.Cells[13].FindControl("lblNotRoutable");
                    gridNonRoll = (Label)e.Item.Cells[14].FindControl("lblNotRollable");

                    //START srajindersin MITS 32606 dt:05/14/2013 
                    //try
                    //{
                    //   gridClaimantLabel.Text =  DataBinder.Eval(e.Row.DataItem, "claimant").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    gridClaimantLabel.Text = "";
                    //}
                    if (rowData.Row.Table.Columns.Contains("claimant") && DataBinder.Eval(e.Item.DataItem, "claimant") != null)
                    {
                        //Mits 36708                  
                        if (claimantLength != 0 && DataBinder.Eval(e.Item.DataItem, "claimant").ToString().Trim().Length > Convert.ToInt32(claimantLength))
                        {
                            gridClaimantLabel.Text = DataBinder.Eval(e.Item.DataItem, "claimant").ToString().Substring(0, Convert.ToInt32(claimantLength)) + "...";
                            gridClaimantLabel.ToolTip = DataBinder.Eval(e.Item.DataItem, "claimant").ToString();
                        }
                        else
                        {
                            gridClaimantLabel.Text = DataBinder.Eval(e.Item.DataItem, "claimant").ToString();
                        }
                    }
                    else
                    {
                        gridClaimantLabel.Text = "";
                    }

                    //try
                    //{
                    //    gridDepartmentLabel.Text = DataBinder.Eval(e.Row.DataItem, "department").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    gridDepartmentLabel.Text = "";
                    //}
                    if (rowData.Row.Table.Columns.Contains("department") && DataBinder.Eval(e.Item.DataItem, "department") != null)
                    {
                        //Mits 36708
                        if (claimantLength != 0 && DataBinder.Eval(e.Item.DataItem, "department").ToString().Trim().Length > Convert.ToInt32(claimantLength))
                        {
                            gridDepartmentLabel.Text = DataBinder.Eval(e.Item.DataItem, "department").ToString().Substring(0, claimantLength) + "...";
                            gridDepartmentLabel.ToolTip = DataBinder.Eval(e.Item.DataItem, "department").ToString();
                        }
                        else
                            gridDepartmentLabel.Text = DataBinder.Eval(e.Item.DataItem, "department").ToString();
                    }
                    else
                    {
                        gridDepartmentLabel.Text = "";
                    }

                    //try
                    //{
                    //    gridClaimStatusLabel.Text = DataBinder.Eval(e.Row.DataItem, "claimstatus").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    gridClaimStatusLabel.Text = ""; 
                    //}
                    if (rowData.Row.Table.Columns.Contains("claimstatus") && DataBinder.Eval(e.Item.DataItem, "claimstatus") != null)
                    {
                        gridClaimStatusLabel.Text = DataBinder.Eval(e.Item.DataItem, "claimstatus").ToString();
                    }
                    else
                    {
                        gridClaimStatusLabel.Text = "";
                    }
                    //END srajindersin MITS 32606 dt:05/14/2013 
                    //igupta3 non route/roll check jira 439
                    if (rowData.Row.Table.Columns.Contains("routable") && DataBinder.Eval(e.Item.DataItem, "routable") != null)
                    {
                        gridNonRoute.Text = DataBinder.Eval(e.Item.DataItem, "routable").ToString();
                    }
                    else
                    {
                        gridNonRoute.Text = "";
                    }
                    if (rowData.Row.Table.Columns.Contains("rollable") && DataBinder.Eval(e.Item.DataItem, "rollable") != null)
                    {
                        gridNonRoll.Text = DataBinder.Eval(e.Item.DataItem, "rollable").ToString();
                    }
                    else
                    {
                        gridNonRoll.Text = "";
                    }
                    //igupta3 ends

                    if (rowData.Row.Table.Columns.Contains("assigned_user") && DataBinder.Eval(e.Item.DataItem, "assigned_user") != null)
                    {
                        gridToLabel.Text = DataBinder.Eval(e.Item.DataItem, "assigned_user").ToString();
                    }
                    else
                    {
                        gridToLabel.Text = "";
                    }
                    if (rowData.Row.Table.Columns.Contains("assigning_user") && DataBinder.Eval(e.Item.DataItem, "assigning_user") != null)
                    {
                        gridFromLabel.Text = DataBinder.Eval(e.Item.DataItem, "assigning_user").ToString();
                    }
                    else
                    {
                        gridFromLabel.Text = "";
                    }
                    //Indu - Mits 33843 
                    try
                    {
                        if (!string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "parentrecord").ToString()))
                        {
                            string tableName = DataBinder.Eval(e.Item.DataItem, "parentrecord").ToString().Split('Æ')[2].Trim();
                            gridParentRecord.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(tableName) + ":" + DataBinder.Eval(e.Item.DataItem, "parentrecord").ToString().Split('Æ')[1];
                            //gridParentRecord.Attributes.Add("onclick", "parent.MDIShowScreen('" + DataBinder.Eval(e.Item.DataItem, "parentrecord").ToString().Split('Æ')[0] + "','" + DataBinder.Eval(e.Item.DataItem, "attach_table").ToString().ToLower().Trim() + "')");
                            gridParentRecord.Attributes.Add("onclick", "parent.MDIShowScreen('" + DataBinder.Eval(e.Item.DataItem, "parentrecord").ToString().Split('Æ')[0] + "','" + tableName + "')");
                         }

                    }                    catch (System.Web.HttpException)
                    {
                        gridParentRecord.Text = "";

                    }
                    
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

      
        public string GetPrioiryUrl(string priorityData)
        {
            string imageurl = @"../../Images/";

            if (priorityData == "1")
            {
                return "";
            }
            else if (priorityData == "2")
            {
                imageurl = imageurl + "important.gif";
            }
            else if (priorityData == "3")
            {
                imageurl = imageurl + "required.gif";
            }
            else
            {
                return "";
            }

            return imageurl;
        }

        private void SetViewState(DataTable dTable)
        {
            DataRow dRow = null;
            dRow = dTable.Rows[0];
           
            string pageNumberValue = string.Empty;
            string nextPageValue = string.Empty;
            string lastPageValue = string.Empty;
            string dueDateValue = string.Empty;
            string assignedUser = string.Empty;
            string pageCountValue = string.Empty;
            //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
            string sUserSortOrder = string.Empty;
            //end
            try
            {
                ViewState["maxrecord"] = dRow["maxrecord"].ToString();
            }
            catch
            {
            }
            try
            {
                pageNumberValue = dRow["pagenumber"].ToString();
            }
            catch
            {
                pageNumberValue = "";
            }

            try
            {
                pageCountValue = dRow["pagecount"].ToString();
            }
            catch
            {
                pageCountValue = "";
            }

            try
            {
                dueDateValue = dRow["datedue"].ToString();
            }
            catch
            {
                dueDateValue = "";
            }

            try
            {
                nextPageValue = dRow["nextpage"].ToString();
            }
            catch
            {
                nextPageValue = "";
            }

            try
            {
                lastPageValue = dRow["lastpage"].ToString();
            }
            catch
            {
                lastPageValue = "";
            }

            try
            {
                assignedUser = dRow["user"].ToString();
            }
            catch
            {
                assignedUser = "";
            }
            //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
            try
            {
                sUserSortOrder = dRow["UserSortOrder"].ToString();
            }
            catch
            {
                sUserSortOrder = "0";
            }
            //end Mits 23477
            if (pageNumberValue == pageCountValue) //last page
            {
                ViewState["pagenumber"] = pageNumberValue;
                //ViewState["datedue"] = dueDateValue;
                ViewState["nextpage"] = "";
                ViewState["lastpage"] = "";

                //Commented by Amitosh because Paging is in Grid
                //// case when pagecount is only 1
                //if (pageNumberValue == "1")
                //{
                //   // btnFirstPage.Enabled = false;
                //    //btnPrev.Enabled = false;
                //}
                //else
                //{
                //    btnFirstPage.Enabled = true;
                //    btnPrev.Enabled = true;
                //}
                //btnNext.Enabled = false;
                //btnLast.Enabled = false;
            }
            else if (pageNumberValue == "1")
            { //Commented by Amitosh because Paging is in Grid
                //btnFirstPage.Enabled = false;
                //btnPrev.Enabled = false;
                //btnNext.Enabled = true;
                //btnLast.Enabled = true;

                //ViewState["datedue"] = dueDateValue;
                ViewState["nextpage"] = nextPageValue;
                ViewState["lastpage"] = lastPageValue;
                ViewState["pagenumber"] = pageNumberValue;
            }
            else
            {
                //ViewState["datedue"] = dueDateValue;
                ViewState["nextpage"] = nextPageValue;
                ViewState["lastpage"] = lastPageValue;
                ViewState["pagenumber"] = pageNumberValue;
                //Commented by Amitosh because Paging is in Grid
                //btnFirstPage.Enabled = true;
                //btnPrev.Enabled = true;
                //btnNext.Enabled = true;
                //btnLast.Enabled = true;
            }

            ViewState["assigneduser"] = assignedUser;
            //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
            ViewState["usersortorder"] = sUserSortOrder;
            //added by nitin for R6 implementation fo Header Config setttings in GridView starts
            try
            {
                ViewState["chkpriority"] = dRow["Priority"].ToString();
            }
            catch
            {
                ViewState["chkpriority"] = "";
            }

            try
            {
                ViewState["chktext"] = dRow["Text"].ToString();
            }
            catch
            {
                ViewState["chktext"] = "";
            }

            try
            {
                ViewState["chkdue"] = dRow["Due"].ToString();
            }
            catch
            {
                ViewState["chkdue"] = "";
            }

            try
            {
                ViewState["chkworkactivity"] = dRow["WorkActivity"].ToString();
            }
            catch
            {
                ViewState["chkworkactivity"] = "";
            }
            //igupta3 jira 439
            try
            {
                ViewState["chknotroutable"] = dRow["NotRoutable"].ToString();
            }
            catch
            {
                ViewState["chknotroutable"] = "";
            }
            try
            {
                ViewState["chknotrollable"] = dRow["NotRollable"].ToString();
            }
            catch
            {
                ViewState["chknotrollable"] = "";
            }
            try
            {
                ViewState["chkattachedrecord"] = dRow["AttachedRecord"].ToString();
            }
            catch
            {
                ViewState["chkattachedrecord"] = "";
            }
            //Indu - Mits 33843
            try
            {
                ViewState["chkparentrecord"] = dRow["ParentRecord"].ToString();
            }
            catch
            {
                ViewState["chkparentrecord"] = "";
            }

            try
            {
                ViewState["chkclaimant"] = dRow["Claimant"].ToString();
            }
            catch
            {
                ViewState["chkclaimant"] = "";
            }

            try
            {
                ViewState["chkclaimstatus"] = dRow["ClaimStatus"].ToString();
            }
            catch
            {
                ViewState["chkclaimstatus"] = "";
            }

            try
            {
                ViewState["chkdepartment"] = dRow["Department"].ToString();
            }
            catch
            {
                ViewState["chkdepartment"] = "";
            }

            try
            {
                ViewState["priorityheader"] = dRow["PriorityHeader"].ToString();
            }
            catch
            {
                ViewState["priorityheader"] = "";
            }

            try
            {
                ViewState["textheader"] = dRow["TextHeader"].ToString();
            }
            catch
            {
                ViewState["textheader"] = "";
            }

            try
            {
                ViewState["dueheader"] = dRow["DueHeader"].ToString();
            }
            catch
            {
                ViewState["dueheader"] = "";
            }

            try
            {
                ViewState["workactivityheader"] = dRow["WorkActivityHeader"].ToString();
            }
            catch
            {
                ViewState["workactivityheader"] = "";
            }
            //igupta3 jira 439
            try
            {
                ViewState["notroutableheader"] = dRow["NotRoutableHeader"].ToString();
            }
            catch
            {
                ViewState["notroutableheader"] = "";
            }
            try
            {
                ViewState["notrollableheader"] = dRow["NotRollableHeader"].ToString();
            }
            catch
            {
                ViewState["notrollableheader"] = "";
            }
            try
            {
                ViewState["claimantheader"] = dRow["ClaimantHeader"].ToString();
            }
            catch
            {
                ViewState["claimantheader"] = "";
            }

            try
            {
                ViewState["claimstatusheader"] = dRow["ClaimStatusHeader"].ToString();
            }
            catch
            {
                ViewState["claimstatusheader"] = "";
            }

            try
            {
                ViewState["departmentheader"] = dRow["DepartmentHeader"].ToString();
            }
            catch
            {
                ViewState["departmentheader"] = "";
            }

            try
            {
                ViewState["noheaderselected"] = dRow["NoHeaderSelected"].ToString();
            }
            catch
            {
                ViewState["noheaderselected"] = "";
            }

            try
            {
                ViewState["attachedrecordheader"] = dRow["AttachedRecordHeader"].ToString();
            }
            catch
            {
                ViewState["attachedrecordheader"] = "";
            }
            //Indu - Mits 33843
            try
            {
                ViewState["parentrecordheader"] = dRow["parentrecordheader"].ToString();
            }
            catch
            {
                ViewState["parentrecordheader"] = "";
            }

            //added by nitin for R6 implementation fo Header Config setttings in GridView ends

            //Added by Amitosh for Diary UI Change
            try
            {
                ViewState["chkAssignedUser"] = dRow["AssignedUser"].ToString();
            }
            catch
            {
                ViewState["chkAssignedUser"] = "";
            }

            try
            {
                ViewState["chkAssigningUser"] = dRow["AssigningUser"].ToString();
            }
            catch
            {
                ViewState["chkAssigningUser"] = "";
            }
            try
            {
                ViewState["AssigningUserHeader"] = dRow["AssigningUserHeader"].ToString();
            }
            catch
            {
                ViewState["AssigningUserHeader"] = "";
            }

            try
            {
                ViewState["AssignedUserheader"] = dRow["AssignedUserHeader"].ToString();
            }
            catch
            {
                ViewState["AssignedUserheader"] = "";
            }
            //End Amitosh
			 //Deb: MITS 32961 
            try
            {
                ViewState["sortorder"] = dRow["OrderBy"].ToString();
            }
            catch { }
            //MITS 36519
            try
            {
                ViewState["sortexpression"] = dRow["SortExpression"].ToString();
            }
            catch { }
            //Deb: MITS 32961 
        }
        
        /// <summary>
        /// Added by Nitin,in order to open attachment through Mdi Navigation tree
        /// </summary>
        /// <param name="attachTable"></param>
        /// <returns></returns>
        private string GetAttachedTableCategory(string attachTable)
        {
            string attachedTableCategory = string.Empty;
            string[] arrAdmTrack = null;
            char unitSeparator = (char)31;

            //Added by Nitin for Mits 14882 on 08-Apr-09
            if (attachTable.ToLower().Contains("admintracking|"))
            {
                arrAdmTrack = attachTable.Split('|');

                //parent.MDIShowScreen(lRecordID, "admintrackinglist" + del + "Admin Tracking (" + admtable + ")" + del + "UI/FDM/admintracking" + admtable + ".aspx" + del + "?recordID=(NODERECORDID)");

                if (arrAdmTrack.GetUpperBound(0) > 0)
                {
                    attachedTableCategory = "admintrackinglist" + unitSeparator + "Admin Tracking (" + arrAdmTrack[1] + ")" + unitSeparator + "UI/FDM/admintracking" + arrAdmTrack[1] + ".aspx" + unitSeparator + "?recordID=(NODERECORDID)"; // nnorouzi; no further items (SELECETD/TOP, another Root)are provided in the categoryArg, otherwise it will be considered as an injected node.
                    return attachedTableCategory;
                }
            }
            //Ended by Nitin for Mits 14882 on 08-Apr-09

            //cases are changed by Nitin for Mits 15044 on 07th April 09
            switch (attachTable)
            {
                case "LEAVEPLAN":
                    attachedTableCategory = "Leaveplan";
                    break;
                case "ADJUST_DATED_TEXT":
                    attachedTableCategory = "adjusterdatedtext";
                    break;
                case "POLICY_ENH":
                    attachedTableCategory = "policyenh";
                    break;
                // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                case "POLICY_ENH_AL":
                    attachedTableCategory = "policyenhal";
                    break;
                case "POLICY_ENH_GL":
                    attachedTableCategory = "policyenhgl";
                    break;
                case "POLICY_ENH_PC":
                    attachedTableCategory = "policyenhpc";
                    break;
                case "POLICY_ENH_VA":
                    attachedTableCategory = "policyenhva";
                    break;
                case "POLICY_ENH_WC":
                    attachedTableCategory = "policyenhwc";
                    break;
                // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries
                case "DISABILITY_PLAN":
                    attachedTableCategory = "plan";
                    break;
                case "MED_STAFF":
                    attachedTableCategory = "staff";
                    break;
                case "BILL_X_INSTLMNT":
                    attachedTableCategory = "policybilling";
                    break;
                case "BILL_X_BILL_ITEM":
                    attachedTableCategory = "policybilling";
                    break;
                //rsushilaggar 12-Jul-2010 Modified file to call User vaerfication screen for DA
                    //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch Start
                //case "DA_DDS":
                //case "DA_DIS":
                case "DIS":
                    //attachedTableCategory = "UserVerification" + unitSeparator + "User Verification" + unitSeparator + "UI/Utilities/ToolsDesigners/TaskManager/" + unitSeparator + "?jobid={jobid}&modulename=" + attachTable.Substring(3) + unitSeparator + "SELECTED" + unitSeparator + "Others";
                    attachedTableCategory = "UserVerification" + unitSeparator + "User Verification" + unitSeparator + "UI/Utilities/ToolsDesigners/TaskManager/" + unitSeparator + "?jobid={jobid}&modulename={modulename}";
                    break;
                case "DDS":
                    attachedTableCategory = "UserVerification" + unitSeparator + "User Verification" + unitSeparator + "UI/Utilities/ToolsDesigners/TaskManager/" + unitSeparator + "?jobid={jobid}&modulename={modulename}";
                    break;
                    //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch end
                case "TM_JOB_LOG":
                    attachedTableCategory = "taskmanagerjob";
                    break;
                //11/14/2010 - mcapps2 - MITS 23090 Start
                case "FUNDS_AUTO":
                case "FUNDS_AUTO_BATCH": //rupal:r8 auto diary enh
                    attachedTableCategory = "autoclaimchecks";
                    break;      
                //rupal:start, r8 auto diary enh
                case "CLAIM_X_LITIGATION":
                    attachedTableCategory = "litigation";
                    break;
                case "CM_X_TREATMENT_PLN": 
                    attachedTableCategory = "cmxtreatmentpln";
                    break;
                //rupal:end
                default:
                    attachedTableCategory = attachTable.ToLower();
                    break;
            }
            return attachedTableCategory;
        }

        /// <summary>
        /// Added by Nitin on 22-04-2009 in order to open some 
        /// FDM screens as popups for Mits 15272
        /// </summary>
        /// <param name="attachedScreen"></param>
        /// <returns></returns>
        private bool IsOpenAttachedRecordInPopup(string attachedTableCategory)
        {
            //cases for medwatch,medwatchtest,fallinfo,concomitant have been added by Nitin 
            //for Mits no 16940 on 15/June/2009 

            switch (attachedTableCategory)
            {
                case "adjuster":
                case "adjusterdatedtext":
                case "claimant":
                case "defendant":
                case "litigation":
                case "expert":
                case "piemployee":
                case "pidependent":
                case "pirestriction":
                case "piworkloss":
                case "pimedstaff":
                case "pimedstaffprivilege":
                case "pimedstaffcertification":
                case "piother":
                case "pipatient":
                case "piprocedure":
                case "piphysician":
                case "piprivilege":
                case "picertification":
                case "pieducation":
                case "piprevhospital":
                case "piwitness":
                case "cmxcmgrhist":
                case "cmxaccommodation":
                case "cmxvocrehab":
                case "cmxtreatmentpln":
                case "cmxmedmgtsavings":
                case "leave":
                case "unit":
                case "osha":
                case "eventdatedtext":
                case "casemgrnotes":
                case "dependent":
                case "medwatch":
                case "medwatchtest":
                case "fallinfo":
                case "concomitant":
                case "taskmanagerjob":
// akaushik5 Added for MITS 31972 Starts
                case "propertyloss":
                case "salvage":
                case "subrogation":
// akaushik5 Added for MITS 31972 Ends
                case "policycoverage":                    //avipinsrivas Start : Worked for JIRA - 1333
                    return true;
                default:
                    return false;
            }
        }

        protected void btnFirstPage_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;

            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = "1";
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();

                // objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                //sshrikrishna:End changes                
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                //GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;

            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = (Convert.ToInt32(ViewState["pagenumber"]) - 1).ToString();
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                // objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();                
                //sshrikrishna:End changes
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
             //   GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = (Convert.ToInt32(ViewState["pagenumber"]) + 1).ToString();
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                //sshrikrishna:End changes
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Activediarychecked = "";
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
               // GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        protected void btnLast_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = ViewState["lastpage"].ToString();
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();               \
                //sshrikrishna:End changes
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
               // GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

                //protected void lstDiary_Sorting(object sender, GridViewSortEventArgs e)
        protected void lstDiary_Sorting(object sender, GridSortCommandEventArgs e) //Changed by Amitosh because ID of  Grid is changed
        {
            //Ashish Ahuja - Mits 33926
            lstDiary.CurrentPageIndex = 0;
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = "1";
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();

                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                //sshrikrishna:End changes for MITS 27074
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
               //Deb: MITS 32961 
                //if (ViewState["sortexpression"] != null)
                //{
                //    //MITS 30373-Start
                //    if (e.SortExpression != ViewState["sortexpression"].ToString() && (ViewState["sortexpression"].ToString() != ""))
                //    {
                //        ViewState["sortdirection"] = "ASC";
                //        ViewState["sortexpression"] = e.SortExpression;
                //        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "ASC").ToString();
                //    }
                //    //MITS 30373-End
                //    else if (e.SortExpression == ViewState["sortexpression"].ToString())
                //    {
                //        ViewState["sortdirection"] = "DEC";
                //        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "DEC").ToString();
                //        //added by Nitin for Mits 14632 on 9-Apr-09
                //        ViewState["sortexpression"] = "";
                //    }
                //    //MITS 30373-Start
                //    else if ((ViewState["sortexpression"].ToString() == "") && (ViewState["sortdirection"].ToString() == "DEC"))
                //    {
                //        ViewState["sortdirection"] = "ASC";
                //        if (ViewState["oldsortexpression"].ToString() == e.SortExpression)
                //            ViewState["sortexpression"] = "";
                //        else
                //            ViewState["sortexpression"] = e.SortExpression;
                //        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "ASC").ToString();
                //    }
                //    //MITS 30373- End
                //    else
                //    {
                //        ViewState["sortdirection"] = "ASC";
                //        ViewState["sortexpression"] = e.SortExpression;
                //        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "ASC").ToString();
                //    }
                //}
                //else
                //{
                if (Convert.ToString(ViewState["sortexpression"]) != e.SortExpression.ToString())
                {
                    ViewState["sortexpression"] = e.SortExpression;
                    try
                    {

                        objDiaryCurrentAction.Sortorder = Convert.ToString(DiariesHelper.GetSortOrder(e.SortExpression, Convert.ToInt32(ViewState["sortorder"].ToString())));
                        //srajindersin MITS 32355 01/06/2014
                        if (string.IsNullOrEmpty(e.SortExpression) && objDiaryCurrentAction.Sortorder == Convert.ToString(ViewState["sortorder"]))
                        {
                            objDiaryCurrentAction.Sortorder = Convert.ToString(DiariesHelper.GetSortOrder(string.Empty, Convert.ToInt32(ViewState["sortorder"].ToString())));
                        }
                    }
					catch {  }
                }
                else
                {
                    ViewState["sortexpression"] = e.SortExpression;
                    try
                    {
                        objDiaryCurrentAction.Sortorder = Convert.ToString(DiariesHelper.GetSortOrder(string.Empty, Convert.ToInt32(ViewState["sortorder"].ToString())));
                    }
                    catch {  }
                }
                //MITS 36519
                objDiaryCurrentAction.SortExpression = Convert.ToString(ViewState["sortexpression"]);
                ViewState["sortdirection"] = DiariesHelper.GetSortDirection(Convert.ToInt32(objDiaryCurrentAction.Sortorder));
                //}
                ViewState["sortcolumnindex"] = DiariesHelper.GetSortColumnIndex(e.SortExpression).ToString();
                ViewState["sortorder"] = objDiaryCurrentAction.Sortorder;
                //ViewState["oldsortexpression"] = e.SortExpression;
                //Deb: MITS 32961
				
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString();
                GridViewBinder(objDiaryCurrentAction,true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }


        }


        private DataSet GetDiaryList(DiaryListCurrentAction objCurrentAction)
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            XmlNode retDiaryNode = null;
            DataSet dSet = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                //setting inputs to get diarylist from webservice
                dHelper = new DiariesHelper();

                inputDocNode = dHelper.InputDocForListAndCalender(objCurrentAction);

                serviceMethodToCall = "WPAAdaptor.GetDiariesDOM";

                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                retDiaryNode = diaryDoc.SelectSingleNode("ResultMessage/Document/diaries");
                if (retDiaryNode != null)
                {
                    dSet = new DataSet();
                    dSet.ReadXml(new XmlNodeReader(diaryDoc));
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

            return dSet;
        }

        private void SetDefaultStyle()
        {
            //lblMsg.Visible = false;
            //Deb : Commneted for ML changes
            lblUserDiariesDisplay.Text = ViewState["assigneduser"].ToString();// +"'s Diaries";
            cmdCreate.ImageUrl = "~/Images/tb_diarycreate_active.png";
            cmdDiaryList.Visible = true;
            cmdDiaryCalendar.Visible = true;
            cmdPeek.ImageUrl = "~/Images/tb_diarypeek_active.png";
            //smishra54 1 Sep 2011: MITS 25643
            btnHome.Visible = false;
            //smishra54: End
            chkShowActiveDiariesForRecordOnly.Visible = false;
            chkShowAllDiariesRelatedToClaim.Visible = false;
        }

        private void SetPeekStyle()
        {
            lblMsg2.Visible = true;
            //Deb : Commneted for ML changes
            lblUserDiariesDisplay.Text = ViewState["assigneduser"].ToString();// +"'s Diaries";
            //lblMsg2.Text = "Note: You are currently peeking at another user's diaries. <br /> Press the 'Home' button to see your own diaries.";
            //cmdPeek.ImageUrl = "~/Images/tb_savemapping_active.png";
            cmdPeek.ImageUrl = "~/Images/tb_previous_active.png";
            cmdPeek.ToolTip = "Home";
            //smishra54 1 Sep 2011: MITS 25643
            btnHome.Visible = false;
            //smishra54: End
            chkShowActiveDiariesForRecordOnly.Visible = false;
            chkShowAllDiariesRelatedToClaim.Visible = false;
        }

        private void SetAttachDiaryStyle()
        {
            lblMsg1.Visible = true;
            //lblMsg1.Text = "Note: You are currently looking at the list of all diaries attached to a record. <br /> Press the 'Home' button to see your own diaries.";
            //Deb : Commneted for ML changes
            //lblUserDiariesDisplay.Text = "Diaries[" + ViewState["attachlob"].ToString() + ": " + ViewState["attachfdmdetail"].ToString() + " Attachments Only]";
            //Aman MITS 31225
            lblUserDiariesDisplay.Text = RMXResourceProvider.GetSpecificObject("lblDiariesResrc", RMXResourceProvider.PageId("DiaryList.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + ViewState["attachlob"].ToString() + ": " + ViewState["attachfdmdetail"].ToString() + " " + RMXResourceProvider.GetSpecificObject("lblAttachonlyResrc", RMXResourceProvider.PageId("DiaryList.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            //smishra54 1 Sep 2011: MITS 25643
            //cmdCreate.Text = "Home";
            cmdCreate.Attributes.Add("onclick", "Javascript:return OnCreateDiaryClick('" + ViewState["attachtable"] + "','" + ViewState["attachrecordid"] + "','" + ViewState["attachrecname"] + "');");
            //smishra54 -End
            cmdDiaryList.Visible = false;
            cmdDiaryCalendar.Visible = false;
            //vkumar258 RMA-10872 Start:
          //  chkShowActiveDiariesForRecordOnly.Visible = true;//vkumar258 RMA-10782
           // chkShowActiveDiariesForRecordOnly.Checked = true;
            //if (ViewState["attachtable"].ToString() == "CLAIM" && !chkActiveDiaryChecked.Checked) //Ankit Start : Worked on MITS - 29939
            if (ViewState["attachtable"].ToString() == "CLAIM" && chkShowActiveDiariesForRecordOnly.Checked)
            {
                chkShowAllDiariesRelatedToClaim.Visible = true;
            }
            else
            {
                chkShowAllDiariesRelatedToClaim.Visible = false;
            }
            //vkumar258 End
        }

        private void GridViewBinder(DiaryListCurrentAction objDiaryCurrentAction,bool p_bBindGrid)
        {
            DataSet diaryRecordsSet = null;

            string sDueDate = string.Empty;            
            string sDiarySource = string.Empty;//sachin MITS 27074
            objDiaryCurrentAction.User = ViewState["assigneduser"].ToString();
            objDiaryCurrentAction.Attachtable = ViewState["attachtable"].ToString();
            objDiaryCurrentAction.Attachrecordid = ViewState["attachrecordid"].ToString();
            //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
            objDiaryCurrentAction.Usersortorder = ViewState["usersortorder"].ToString();
            //MITS 36519
            objDiaryCurrentAction.SortExpression =Convert.ToString(ViewState["sortexpression"]);
            //End mits 2377
            if (ViewState["ispeekdiary"].ToString() == "true")
            {
                objDiaryCurrentAction.Diaryfunction = "peek";
            }

            diaryRecordsSet = GetDiaryList(objDiaryCurrentAction);
            //WWIG GAP20A - agupta298 - MITS 36804 - Adding column(s) to Rad grid dynamically - Start
            if (diaryRecordsSet != null)
                AddDiarySuppColumn(diaryRecordsSet);
            //WWIG GAP20A - agupta298 - MITS 36804- Adding column(s) to Rad grid dynamically - End
            //sDueDate = diaryRecordsSet.Tables[2].Rows[0]["datedue"].ToString();
            sDueDate = diaryRecordsSet.Tables["diaries"].Rows[0]["datedue"].ToString();//agupta298 - WWIG GAP20A-removed hardcoded table id as there are now some new tables in dataset.
            //sDiarySource = diaryRecordsSet.Tables[2].Rows[0]["diarysource"].ToString(); //27074
            sDiarySource = diaryRecordsSet.Tables["diaries"].Rows[0]["diarysource"].ToString(); //27074//agupta298 - WWIG GAP20A-removed hardcoded table id
            //Mits 36708
            //claimantLength = Convert.ToInt32(diaryRecordsSet.Tables[2].Rows[0]["claimantlength"]);
            claimantLength = Convert.ToInt32(diaryRecordsSet.Tables["diaries"].Rows[0]["claimantlength"]);
            lstDiary.MasterTableView.FilterExpression = "";//Ankit-This is required because Telerik looks to apply its own filter post custom filtering.
            //Ankit-MITS 27074
            //int iPageSize = Convert.ToInt32(diaryRecordsSet.Tables[2].Rows[0][""]);
            //int iPageSize = Convert.ToInt32(diaryRecordsSet.Tables[2].Rows[0]["pagesize"]);
            //int iVCount = Convert.ToInt32(diaryRecordsSet.Tables[2].Rows[0]["maxrecord"]);
            int iPageSize = Convert.ToInt32(diaryRecordsSet.Tables["diaries"].Rows[0]["pagesize"]);
            int iVCount = Convert.ToInt32(diaryRecordsSet.Tables["diaries"].Rows[0]["maxrecord"]);
            lstDiary.PageSize = iPageSize;
            lstDiary.VirtualItemCount = iVCount;
            ////Ankit MITS 27074
            if (sDueDate == "")
            {
                ViewState["reqactdiary"] = "1";
                //ViewState["datedue"] = "";
                //to set visibility of date textbox and refresh textbox
              //  lblDueDateDisplay.Visible = false;
             //   txtdatedue.Visible = false;
              //  btnPickDate.Attributes.Add("style", "DISPLAY: none");
                //sgoel6 04/06/2009 MITS 14187 | Show Refresh button when Show All Active Diaries option in selected
                btnRefresh.Visible = true;
              //  chkShowAllDiariesRelatedToClaim.Visible = false; //vkumar258 RMA-10872                                          
                //sshrikrishna:Start changes for MITS 27074
                //chkActiveDiaryChecked.Checked = true;
                if ((sDiarySource.CompareTo("D") == 0))
                {
                    if (Request.QueryString.Count == 0)
                    {
                        chkActiveDiaryChecked.Checked = true;
                        Session["activediarychecked"] = "1";
                    }
                }
                else if ((sDiarySource.CompareTo("C") == 0))
                {
                    if (Request.QueryString.Count != 0)
                    {
                        Session["claimactivediarychecked"] = "1";
                        chkActiveDiaryChecked.Checked = true;
                    }
                }
                else if ((sDiarySource.CompareTo("DC") == 0))
                {
                    Session["activediarychecked"] = "1";
                    Session["claimactivediarychecked"] = "1";
                    chkActiveDiaryChecked.Checked = true;
                }
                //sshrikrishna:End changes
            }
            //ViewState["shownotes"] = diaryRecordsSet.Tables[2].Rows[0]["shownotes"].ToString(); //csingh7 MITS 18435    
            //ViewState["showregarding"] = diaryRecordsSet.Tables[2].Rows[0]["showregarding"].ToString(); //csingh7 MITS 18435    
            ViewState["shownotes"] = diaryRecordsSet.Tables["diaries"].Rows[0]["shownotes"].ToString(); //csingh7 MITS 18435  //agupta298 - WWIG GAP20A-removed hardcoded table id  
            hdnMainUserGroup.Value = diaryRecordsSet.Tables["diaries"].Rows[0]["loggedinusergroup"].ToString();//asharma326 jira 11081
            hdnMainUser.Value = diaryRecordsSet.Tables["diaries"].Rows[0]["loggedinuser"].ToString();//asharma326 jira 11081
            hdnOtherDiariesEditPerm.Value = diaryRecordsSet.Tables["diaries"].Rows[0]["allowactionsonothersdiariesperm"].ToString();//asharma326 jira 11081
            hdnUser.Value = diaryRecordsSet.Tables["diaries"].Rows[0]["user"].ToString();//asharma326 jira 11081
            ViewState["showregarding"] = diaryRecordsSet.Tables["diaries"].Rows[0]["showregarding"].ToString(); //csingh7 MITS 18435    //agupta298 - WWIG GAP20A-removed hardcoded table id
            if (ViewState["shownotes"].ToString() == "0")
            {
                chkShownotes.Checked = false;
            }                                                                               //csingh7 MITS 18435 : end
        //    lblRecords.Text = "Displaying " + diaryRecordsSet.Tables[2].Rows[0]["lowercount"].ToString() + "-" + diaryRecordsSet.Tables[2].Rows[0]["uppercount"].ToString() + " of " + diaryRecordsSet.Tables[2].Rows[0]["maxrecord"].ToString();

            if (ViewState["showregarding"].ToString() == "0")
            {
                chkShowregarding.Checked = false;
            }
            else
            {
                chkShowregarding.Checked = true;
            }
            // npadhy RMA-13644 As we now have another node in Diary for Supplementals, so count of Table can increase
            // So checking is the record count in Diary table is == 0 then also show No Record displayed
            //START PSARIN2 R8 :FIX when no record is present
            if (diaryRecordsSet.Tables.Count <= 3 || (diaryRecordsSet.Tables.Count > 3 && diaryRecordsSet.Tables[2].Rows.Count ==  0)
                || ((diaryRecordsSet.Tables.Count > 3 && diaryRecordsSet.Tables[2].Rows.Count > 0 && diaryRecordsSet.Tables[2].Rows[0]["maxrecord"].ToString() == "0")))
            {
                if (ViewState["diaryresultset"] != null)
                {
                    DataTable dtDummyTable = new DataTable();
                    DataTable dtDummytbl = new DataTable();                    
                    dtDummyTable = (DataTable)ViewState["diaryresultset"];
                    dtDummytbl = dtDummyTable.Clone();
                    //DataRow dr = dtDummytbl.NewRow(); //zmohammad MITS 31429
                    //dtDummytbl.Rows.Add(dr);          //zmohammad MITS 31429          
                    lstDiary.DataSource = dtDummytbl;                    
                }
                ViewState["pagenumber"] = "1";
                //ViewState["datedue"] = sDueDate;
                ViewState["reqactdiary"] = "0";
                ViewState["Showalldiariesforactiverecord"] = "yes";
                ViewState["ShowAllDiariesRelatedToClaim"] = "no";
                ViewState["priorityheader"] = ""; //zmohammad MITS 31429
                ViewState["chkpriority"] = "0"; //zmohammad MITS 31429
                ViewState["shownotes"] = "";
               // ViewState["taskname"] = "";
               // ViewState["taskfilter"] = "";
                ViewState["showregarding"] = "";
                ViewState["usersortorder"] = "5";
                    divGrid.Visible = false; //Kuladeep:MITS 29089-Uncommented code to set grid' visiblity to false.
                    // lstDiary.Visible = false;
                    tblNoRecords.Visible = true;
                    //lstDiary.DataSource = null;//Ankit
                    try
                    {
                        if (ViewState["diaryresultset"] != null)
                        {
                            //SetViewState(diaryRecordsSet.Tables[3]); //Adding diaries viewstate so that headers and filter are still visible.
                            SetViewState(diaryRecordsSet.Tables["diary"]);//agupta298 - WWIG GAP20A-removed hardcoded table id
                        }
                        else 
                        {
                            //SetViewState(diaryRecordsSet.Tables[2]);
                            SetViewState(diaryRecordsSet.Tables["diaries"]); //Adding diaries viewstate so that headers and filter are still visible.//agupta298 - WWIG GAP20A-removed hardcoded table id
                        }
                    }
                    catch
                    {
                    }
               return;
            }
            //END PSARIN2 R8 :FIX when no record is present
          //  tblTxtArea.Visible = true;
            divGrid.Visible = true;
            tblNoRecords.Visible = false;
            try
            {
                //SetViewState(diaryRecordsSet.Tables[2]);
                SetViewState(diaryRecordsSet.Tables["diaries"]);//agupta298 - WWIG GAP20A-removed hardcoded table id
            }
            catch (IndexOutOfRangeException)
            {
                tblNoRecords.Visible = true;
               // tblTxtArea.Visible = false;
            }



            //Added by amitosh for R8 Terelik Grid implementation



            try
            {
            //ViewState["diaryresultset"] = diaryRecordsSet.Tables[3];
            ViewState["diaryresultset"] = diaryRecordsSet.Tables["diary"];//agupta298 - WWIG GAP20A-removed hardcoded table id
            lstDiary.VirtualItemCount = Convert.ToInt32(ViewState["maxrecord"].ToString());
            ViewState["diaryDataSet"] = diaryRecordsSet;
            lstDiary.Columns[1].HeaderText = ViewState["priorityheader"].ToString();
            lstDiary.Columns[2].HeaderText = ViewState["dueheader"].ToString();
            lstDiary.Columns[3].HeaderText = ViewState["textheader"].ToString();
            lstDiary.Columns[4].HeaderText = ViewState["attachedrecordheader"].ToString();
            //Indu - Mits 33843 Addinf Parent Record in header
            lstDiary.Columns[5].HeaderText = ViewState["parentrecordheader"].ToString();
            lstDiary.Columns[6].HeaderText = ViewState["workactivityheader"].ToString();
            lstDiary.Columns[7].HeaderText = ViewState["claimantheader"].ToString();
            lstDiary.Columns[9].HeaderText = ViewState["claimstatusheader"].ToString();
            lstDiary.Columns[8].HeaderText = ViewState["departmentheader"].ToString();
            lstDiary.Columns[10].HeaderText = ViewState["AssignedUserheader"].ToString();
            lstDiary.Columns[11].HeaderText = ViewState["AssigningUserHeader"].ToString();
            //igupta3 jira 439
            lstDiary.Columns[12].HeaderText = ViewState["notroutableheader"].ToString();
            lstDiary.Columns[13].HeaderText = ViewState["notrollableheader"].ToString();
            //lstDiary.DataSource = diaryRecordsSet.Tables[3];            
            lstDiary.DataSource = diaryRecordsSet.Tables["diary"];//agupta298 - WWIG GAP20A-removed hardcoded table id
			if(p_bBindGrid)
            {                
                lstDiary.DataBind();
            }
            //end Amitosh
             
            }
            catch (IndexOutOfRangeException)
            {
                divGrid.Visible = false;
                tblNoRecords.Visible = true;
              //  tblTxtArea.Visible = false;
                //lstDiary.DataBind();
            }
        }

        private void SetHeaderCofig(GridItem grdRow)
        {
            //setting visibility of columns on the basis of configure settings
            if (ViewState["chkpriority"].ToString() == "1")
            {
                //  lstDiary.Columns[1].Visible = true;
                lstDiary.Columns[1].Visible = true;//Grid ID changed by Amitosh

            }
            else
            {
                //    lstDiary.Columns[1].Visible = false;
                lstDiary.Columns[1].Visible = false;//Grid ID changed by Amitosh
            }

            if (ViewState["chktext"].ToString() == "1")
            {
                // lstDiary.Columns[4].Visible = true;
                lstDiary.Columns[3].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //lstDiary.Columns[4].Visible = false;
                lstDiary.Columns[3].Visible = false;//Grid ID changed by Amitosh
            }

            if (ViewState["chkdue"].ToString() == "1")
            {
                //lstDiary.Columns[3].Visible = true;
                lstDiary.Columns[2].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //lstDiary.Columns[3].Visible = false;
                lstDiary.Columns[2].Visible = false;//Grid ID changed by Amitosh
            }

            if (ViewState["chkworkactivity"].ToString() == "1")
            {
                //lstDiary.Columns[6].Visible = true;
                lstDiary.Columns[6].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //lstDiary.Columns[6].Visible = false;
                lstDiary.Columns[6].Visible = false;//Grid ID changed by Amitosh
            }
            //igupta3 jira 439
            if (ViewState["chknotroutable"].ToString() == "1")
            {
                //lstDiary.Columns[6].Visible = true;
                lstDiary.Columns[12].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //lstDiary.Columns[6].Visible = false;
                lstDiary.Columns[12].Visible = false;//Grid ID changed by Amitosh
            }
            if (ViewState["chknotrollable"].ToString() == "1")
            {
                //lstDiary.Columns[6].Visible = true;
                lstDiary.Columns[13].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //lstDiary.Columns[6].Visible = false;
                lstDiary.Columns[13].Visible = false;//Grid ID changed by Amitosh
            }
            if (ViewState["chkattachedrecord"].ToString() == "1")
            {

                //lstDiary.Columns[5].Visible = true;
                lstDiary.Columns[4].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {

                //lstDiary.Columns[5].Visible = false;
                lstDiary.Columns[4].Visible = false;//Grid ID changed by Amitosh
            }
            //Indu - Mits 33843
            if (ViewState["chkparentrecord"].ToString() == "1")
            {

                //    lstDiary.Columns[5].Visible = true;
                lstDiary.Columns[5].Visible = true;//
            }
            else
            {

                //    lstDiary.Columns[5].Visible = false;
                lstDiary.Columns[5].Visible = false;
            }

            if (ViewState["chkclaimant"].ToString() == "1")
            {

                //lstDiary.Columns[7].Visible = true;
                lstDiary.Columns[7].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {

                //lstDiary.Columns[7].Visible = false;
                lstDiary.Columns[7].Visible = false;//Grid ID changed by Amitosh
            }

            if (ViewState["chkclaimstatus"].ToString() == "1")
            {
                //lstDiary.Columns[9].Visible = true;
                lstDiary.Columns[9].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //lstDiary.Columns[9].Visible = false;
                lstDiary.Columns[9].Visible = false;//Grid ID changed by Amitosh
            }

            if (ViewState["chkdepartment"].ToString() == "1")
            {

                //lstDiary.Columns[8].Visible = true;
                lstDiary.Columns[8].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //  lstDiary.Columns[8].Visible = false;
                lstDiary.Columns[8].Visible = false;//Grid ID changed by Amitosh
            }
            if (ViewState["chkAssignedUser"].ToString() == "1")
            {

                //lstDiary.Columns[8].Visible = true;
                lstDiary.Columns[10].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //  lstDiary.Columns[8].Visible = false;
                lstDiary.Columns[10].Visible = false;//Grid ID changed by Amitosh
            }

            if (ViewState["chkAssigningUser"].ToString() == "1")
            {

                //lstDiary.Columns[8].Visible = true;
                lstDiary.Columns[11].Visible = true;//Grid ID changed by Amitosh
            }
            else
            {
                //  lstDiary.Columns[8].Visible = false;
                lstDiary.Columns[11].Visible = false;//Grid ID changed by Amitosh
            }

            lstDiary.Columns[1].HeaderText = ViewState["priorityheader"].ToString();

            lstDiary.Columns[2].HeaderText = ViewState["dueheader"].ToString();

            lstDiary.Columns[3].HeaderText = ViewState["textheader"].ToString();

            lstDiary.Columns[4].HeaderText = ViewState["attachedrecordheader"].ToString();

            lstDiary.Columns[5].HeaderText = ViewState["parentrecordheader"].ToString();

            lstDiary.Columns[6].HeaderText = ViewState["workactivityheader"].ToString();

            lstDiary.Columns[7].HeaderText = ViewState["claimantheader"].ToString();

            lstDiary.Columns[9].HeaderText = ViewState["claimstatusheader"].ToString();

            lstDiary.Columns[8].HeaderText = ViewState["departmentheader"].ToString();

            lstDiary.Columns[10].HeaderText = ViewState["AssignedUserheader"].ToString();
            lstDiary.Columns[11].HeaderText = ViewState["AssigningUserHeader"].ToString();
            //igupta3 jira 439
            lstDiary.Columns[12].HeaderText = ViewState["notroutableheader"].ToString();
            lstDiary.Columns[13].HeaderText = ViewState["notrollableheader"].ToString();
            //((LinkButton)grdRow.Cells[4].Controls[0]).Text = ViewState["noheaderselected"].ToString();


            if (ViewState["noheaderselected"].ToString() == "True")
            {
                //Changed for Mits 19134
                //lstDiary.Visible = false;
                divGrid.Visible = false;
                tblNoRecords.Visible = true;
                //Changed for Mits 19134
            }
            else
            {
                //Changed for Mits 19134
                //lstDiary.Visible = true;
                divGrid.Visible = true;
                tblNoRecords.Visible = false;
                //Changed for Mits 19134
            }
        }


        protected void chkActiveDiaryChecked_CheckedChanged(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            
            try
            {
                if (chkActiveDiaryChecked.Checked == true)
                {
                    Session["GlobalActiveDiaryChecked"] = "1"; //nnithiyanand - 33561 - ActiveDiaries check box getting disabled.

                    //aanandpraka2:Start changes for MITS 27074
                    //ViewState["activediarychecked"] = "1";
                    if (Request.QueryString.Count == 0)
                    {
                        Session["activediarychecked"] = "1";
                    }
                    else
                        Session["claimactivediarychecked"] = "1";
                    //aanandpraka2:End Changes
                    ViewState["reqactdiary"] = "1";
                    //ViewState["datedue"] = "";
                    //to set visibility of date textbox and refresh textbox
                   // lblDueDateDisplay.Visible = false;
                   // txtdatedue.Visible = false;
                   // btnPickDate.Attributes.Add("style", "DISPLAY: none");
                    //sgoel6 04/06/2009 MITS 14187 | Show Refresh button when Show All Active Diaries option in selected
                    btnRefresh.Visible = true;
                    //chkShowAllDiariesRelatedToClaim.Visible = false;
                }
                else
                {
                    Session["GlobalActiveDiaryChecked"] = "0"; //nnithiyanand - 33561 - ActiveDiaries check box getting disabled.

                    //aanandpraka2:Start changes for MITS 27074
                    //ViewState["activediarychecked"] = "";
                    if (Request.QueryString.Count == 0)
                        Session["activediarychecked"] = "";
                    else
                        Session["claimactivediarychecked"] = "";
                    //aanandpraka2:End Changes
                    ViewState["reqactdiary"] = "1";
                    //ViewState["datedue"] = "today";
                   // txtdatedue.Text = System.DateTime.Now.ToShortDateString();
                    //to set visibility of date textbox and refresh textbox
                  //  lblDueDateDisplay.Visible = true;
                  //  txtdatedue.Visible = true;
                  //  btnPickDate.Attributes.Add("style", "DISPLAY:''");
                  //  btnPickDate.Visible = true;
                    btnRefresh.Visible = true;

                    //if (ViewState["ispeekdiary"].ToString() == "false")
                    //{
                     //   if (ViewState["attachtable"].ToString() == "CLAIM")
                       // {
                           // chkShowAllDiariesRelatedToClaim.Visible = true;
                       // }
                       // else
                       // {
                            //chkShowAllDiariesRelatedToClaim.Visible = false;
                        //}
                    //}
                    //else
                    //{
                       // chkShowAllDiariesRelatedToClaim.Visible = false;
                    //}
                }

                objDiaryCurrentAction = new DiaryListCurrentAction();
                //ViewState["FilterExpr"] = "";
                objDiaryCurrentAction.Pagenumber = "1";
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                // akaushik5 Added for MITS 32450 Starts
                if (!object.ReferenceEquals(Session["activediarychecked"], null))
                {
                    // akaushik5 Added for MITS 32450 Ends
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                    // akaushik5 Added for MITS 32450 Starts
                }

                if (!object.ReferenceEquals(Session["claimactivediarychecked"], null))
                {
                    // akaushik5 Added for MITS 32450 Ends
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                    // akaushik5 Added for MITS 32450 Starts
                }
                // akaushik5 Added for MITS 32450 Ends
                
                //    objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";
                //sshrikrishna:End Changes
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"]!=null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString();
                GridViewBinder(objDiaryCurrentAction,true);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }




        protected void chkShownotes_CheckedChanged(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;

            try
            {
                if (chkShownotes.Checked == true)
                {
                    ViewState["shownotes"] = "1";
                }
                else
                {
                    //ViewState["shownotes"] = "";
                    ViewState["shownotes"] = "0";   // csingh7 MITS 18435
                }
   

                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = ViewState["pagenumber"].ToString();
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;   
                //sshrikrishna:Star changes for MITS 27074
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";

                // akaushik5 Added for MITS 32450 Starts
                if (!object.ReferenceEquals(Session["activediarychecked"], null))
                {
                    // akaushik5 Added for MITS 32450 Ends
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                    // akaushik5 Added for MITS 32450 Starts
                }

                if (!object.ReferenceEquals(Session["claimactivediarychecked"], null))
                {
                    // akaushik5 Added for MITS 32450 Ends
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                    // akaushik5 Added for MITS 32450 Starts
                }
                // akaushik5 Added for MITS 32450 Ends
                //  objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                //sshrikrishna:End changes
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();//MITS 27074
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                GridViewBinder(objDiaryCurrentAction,true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void chkShowActiveDiariesForRecordOnly_CheckedChanged(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;

            try
            {
                if (chkShowActiveDiariesForRecordOnly.Checked == true)
                {
                    ViewState["Showalldiariesforactiverecord"] = "yes";
                    cmdCreate.Attributes.Add("onclick", "Javascript:return OnCreateDiaryClick('" + ViewState["attachtable"] + "','" + ViewState["attachrecordid"] + "','" + ViewState["attachrecname"] + "');");
                    if (ViewState["attachtable"].ToString() == "CLAIM")
                    {
                        chkShowAllDiariesRelatedToClaim.Visible = true;
                        chkShowAllDiariesRelatedToClaim.Checked = false;//vkumar258
                        // ViewState["ShowAllDiariesRelatedToClaim"] = "no";  //vkumar258 Start:RMA-10872
                        //Session["ShowAllDiariesRelatedToClaimchecked"] = "no";
                    }
                    else
                    {
                        chkShowAllDiariesRelatedToClaim.Visible = false;
                        chkShowAllDiariesRelatedToClaim.Checked = false;//vkumar258
                        Session["ShowAllDiariesRelatedToClaimchecked"] = "no";
                    }
                }
                else
                {
                    ViewState["Showalldiariesforactiverecord"] = "no";
                    cmdCreate.Attributes.Add("onclick", "Javascript:return OnCreateDiaryClick('','','');");
                    chkShowAllDiariesRelatedToClaim.Visible = false;
                    chkShowAllDiariesRelatedToClaim.Checked = false;//vkumar258
                    //ViewState["ShowAllDiariesRelatedToClaim"] = "no";  //vkumar258 Start:RMA-10872
                    Session["ShowAllDiariesRelatedToClaimchecked"] = "no";
                }

                Session["Showalldiariesforactiverecord"] = ViewState["Showalldiariesforactiverecord"];//vkumar258
                objDiaryCurrentAction = new DiaryListCurrentAction();
                ViewState["FilterExpr"] = "";
                objDiaryCurrentAction.Pagenumber = "1";
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;        
                //sshrikrishna:Start changes for MITS 27074
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";
                //sshrikrishna:End changes
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();//MITS 27074
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                GridViewBinder(objDiaryCurrentAction,true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }


        protected void chkShowAllDiariesRelatedToClaim_CheckedChanged(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;

            try
            {
                if (chkShowAllDiariesRelatedToClaim.Checked == true)
                {
                   // ViewState["ShowAllDiariesRelatedToClaim"] = "yes";
                    Session["ShowAllDiariesRelatedToClaimchecked"] = "yes";
                    //chkActiveDiaryChecked.Checked = false;
                    //ViewState["reqactdiary"] = "0";
                    //Session["activediarychecked"] = "";
                }
                else
                {
                   // ViewState["ShowAllDiariesRelatedToClaim"] = "no";
                    Session["ShowAllDiariesRelatedToClaimchecked"] = "no";
                }

                objDiaryCurrentAction = new DiaryListCurrentAction();
                ViewState["FilterExpr"] = "";
                objDiaryCurrentAction.Pagenumber = "1";
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";
                //sshrikrishna:End changes
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                    if (ViewState["Showalldiariesforactiverecord"]!=null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();//MITS 27074
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                GridViewBinder(objDiaryCurrentAction,true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }


        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
          
            try
            {
              //  ViewState["datedue"] = txtdatedue.Text;

                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = "1";
                //zmohammad MITs 36477 : Changing back index to Page 1 on Refresh.
                lstDiary.CurrentPageIndex = 0;
                //sgoel6 04/06/2009 MITS 14187 | Set the Due Date based on selection of Show All Active Diaries option                 
                //sshrikrishna:Start changes for MITS 27074       
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();                                                
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                if (Request.QueryString.Count == 0)
                {
                    objDiaryCurrentAction.Diarysource = "D";
                }
                else
                {
                    objDiaryCurrentAction.Diarysource = "C";
                }
                //sshrikrishna:End changes
                if (chkActiveDiaryChecked.Checked)
                    objDiaryCurrentAction.Duedate = "";
                else
                    objDiaryCurrentAction.Duedate = "today";
                //Ankit Start : Working on MITS - 30730 (RO)
                //ViewState["FilterExpr"] = "";
                //Ankit End
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                {
                    objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                }
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();//MITS 27074
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                GridViewBinder(objDiaryCurrentAction,true);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void cmdComplete_Click(object sender, EventArgs e)
        {
            //igupta3 Mits:32846            
            string sChkDateSetting = null;
            sChkDateSetting = AppHelper.ReadCookieValue("CurrentDateSetting");
            if (sChkDateSetting == "-1")
            {
                XmlDocument diaryDoc = null;
                try
                {
                    diaryDoc = CompleteDiaryCurrentDate();

                    //if diarydoc has success message then redirect to diarylist 
                    //else throw new execption with error messge in it

                    if (diaryDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                    }
                    else
                    {
                        // Changed by Amitosh For MITS 24229 (03/04/2011)
                        //  throw new ApplicationException(diaryDoc.InnerXml); 
                        throw new ApplicationException(diaryDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText);
                    }

                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                finally
                {
                    diaryDoc = null;
                }
            }
            else
                Server.Transfer("CompleteDiary.aspx?entryid=" + hdnEntry_id.Value + "&assigninguser=" + hdnAssigningUser.Value + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
        }

        protected void cmdCreate_Click(object sender, EventArgs e)
        {
            //RMA-4691
            //Server.Transfer("CreateDiary.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
            Response.Redirect("/RiskmasterUI/UI/FDM/creatediary.aspx?assignedusername=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecord=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&CalledBy=creatediary", false);
        }

        //smishra54 1 Sep 2011: MITS 25643
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Server.Transfer("DiaryList.aspx", false);
        }
        //smishra54: End 

        protected void cmdEdit_Click(object sender, EventArgs e)
        {
            //RMA - 4691
            //Server.Transfer("EditDiary.aspx?entryid=" + hdnEntry_id.Value + "&assigninguser=" + hdnAssigningUser.Value + "&attachprompt=" + hdnAttachPrompt.Value + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
            Response.Redirect("/RiskmasterUI/UI/FDM/creatediary.aspx?entryid=" + hdnEntry_id.Value + "&recordID=" + hdnEntry_id.Value + "&assigninguser=" + hdnAssigningUser.Value + "&attachprompt=" + hdnAttachPrompt.Value + "&assignedusername=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecord=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&CalledBy=creatediary", false);
        }

        protected void cmdDiaryCalendar_Click(object sender, EventArgs e)
        {
            Server.Transfer("DiaryListCalendar.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
        }

        protected void cmdDiaryList_Click(object sender, EventArgs e)
        {

            // Server.Transfer("DiaryListing.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
            //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
            Server.Transfer("DiaryListing.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&usersortorder=" + ViewState["usersortorder"].ToString(), false);
            //end Mits 23477
        }
        //Added by amitosh for R8 enhancement.Replacing asp grid with Terelik Grid
        protected void lstDiary_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                if (IsPostBack)
                {
                objDiaryCurrentAction = new DiaryListCurrentAction();
                //try
                //{
                //    objDiaryCurrentAction.Duedate = ViewState["datedue"].ToString();
                //}
                //catch
                //{
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //}               
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();

                try
                {
                        //sshrikrishna:Start changes for MITS 27074
                        if (Session["activediarychecked"] != null)
                            objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                        if (Session["claimactivediarychecked"] != null)
                            objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                        if (Request.QueryString.Count == 0)
                            objDiaryCurrentAction.Diarysource = "D";
                        else
                            objDiaryCurrentAction.Diarysource = "C";

                        //    objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                        //sshrikrishna:End changes for MITS 27074
                }
                catch
                {
                    objDiaryCurrentAction.Activediarychecked = "";
                        objDiaryCurrentAction.Claimactivediarychecked = ""; //MITS 27074
                }                
                try
                {
                    objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                }
                catch
                {
                    //if (ViewState["sortexpression"] != null)
                    //{
                    //    if (e.SortExpression == ViewState["sortexpression"].ToString())
                    //    {
                    //        ViewState["sortdirection"] = "DEC";
                    //        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "DEC").ToString();
                    //        //added by Nitin for Mits 14632 on 9-Apr-09
                    //        ViewState["sortexpression"] = "";
                    //    }
                    //    else
                    //    {
                    //        ViewState["sortdirection"] = "ASC";
                    //        ViewState["sortexpression"] = e.SortExpression;
                    //        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "ASC").ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    ViewState["sortdirection"] = "ASC";
                    //    ViewState["sortexpression"] = e.SortExpression;
                    //    objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "ASC").ToString();
                    //}

                    //ViewState["sortcolumnindex"] = DiariesHelper.GetSortColumnIndex(e.SortExpression).ToString();
                    //ViewState["sortorder"] = objDiaryCurrentAction.Sortorder;

                    objDiaryCurrentAction.Sortorder = "5";
                }
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //try
                //{
                //    objDiaryCurrentAction.Pagenumber = ViewState["pagenumber"].ToString();
                //}
                //catch
                //{
                    objDiaryCurrentAction.Pagenumber = "1";
                //}
                //Praveen ML Changes- FilterExpression change for COMPLETE_DATE
                if (lstDiary.MasterTableView.FilterExpression.IndexOf("COMPLETE_DATE") > 0)
                {
                    //Ankit Start : Working on MITS 30730
                    //int startInd = lstDiary.MasterTableView.FilterExpression.IndexOf("'") + 1;
                    //int endInd = lstDiary.MasterTableView.FilterExpression.LastIndexOf("'");
                    //string chkCompDate = lstDiary.MasterTableView.FilterExpression.Substring(startInd, endInd - startInd);
                    string chkCompDate = DateTime.Parse(this.startDate.ToString()).ToShortDateString();
                    //Ankit End
                    lstDiary.MasterTableView.FilterExpression = lstDiary.MasterTableView.FilterExpression.Replace(chkCompDate, AppHelper.GetRMDate(chkCompDate));
                }
                //Praveen ML Changes
                objDiaryCurrentAction.FilterExpression = lstDiary.MasterTableView.FilterExpression.Replace("tasksubject", "ENTRY_NAME");
                ViewState["FilterExpr"] = lstDiary.MasterTableView.FilterExpression.Replace("tasksubject", "ENTRY_NAME");
                lstDiary.MasterTableView.FilterExpression = ViewState["FilterExpr"].ToString(); //Ankit-The RadGrid needs the filterexpression value populated till it binds itself to the datasource. 
                GridViewBinder(objDiaryCurrentAction,false);
                if (ViewState["ispeekdiary"].ToString() == "true")
                {
                    //set peek style of buttons
                    SetPeekStyle();
                }
                else
                {
                    if (ViewState["attachrecordid"].ToString() == "")
                    {
                        //set defualt style
                        SetDefaultStyle();
                    }
                    else
                    {
                        //set attach diary button style
                        SetAttachDiaryStyle();
                    }
                }

            }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //end Amitosh
        protected void cmdHistory_Click(object sender, EventArgs e)
        {
            Server.Transfer("DiaryHistory.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
        }

        protected void cmdPeek_Click(object sender, EventArgs e)
        {

            //starts changes for Mits 14729 by Nitin on 08-April-09
            //if (cmdPeek.ImageUrl.ToLower() != "~/Images/tb_previous_active.png")
            if (string.Compare(cmdPeek.ImageUrl, "~/Images/tb_previous_active.png", true) != 0) // cmdPeek.ImageUrl.ToLower() != "~/Images/tb_previous_active.png")
            {
                Server.Transfer("PeekDiary.aspx?attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            else
            {
                Server.Transfer("DiaryList.aspx?attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            //end changes for Mits 14729 by Nitin on 08-April-09

        }

        protected void cmdRoute_Click(object sender, EventArgs e)
        {
            Server.Transfer("RouteDiary.aspx?entryid=" + hdnEntry_id.Value + "&assigninguser=" + hdnAssigningUser.Value + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
        }

        protected void cmdRoll_Click(object sender, EventArgs e)
        {
            Server.Transfer("RollDiary.aspx?entryid=" + hdnEntry_id.Value + "&assigninguser=" + hdnAssigningUser.Value + "&creationdate=" + hdnCreationDate.Value + "&assigneduser=" + (Convert.ToBoolean(hdnGroupAssigned.Value) ? string.Empty : ViewState["assigneduser"].ToString()) + "&assignedgroup=" + (Convert.ToBoolean(hdnGroupAssigned.Value) ? ViewState["assignedgroup"].ToString() : string.Empty) + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);//pkandhari Jira 6412
        }

        protected void cmdVoid_Click(object sender, EventArgs e)
        {
            //igupta3 Mits:32846
            ViewState["entryid"] = hdnEntry_id.Value.ToString();
            if (Convert.ToString(ViewState["entryid"]).IndexOf(',') == -1)
            {
            Server.Transfer("VoidDiary.aspx?entryid=" + hdnEntry_id.Value + "&assingneduser=" + ViewState["assigneduser"].ToString() + "&completedate=" + hdnCompletionDate.Value + "&taskname=" + hdnTaskName.Value + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
            }
            else
            {
                XmlDocument responseDoc = null;
                try
                {
                    responseDoc = VoidDiaryCurrentDate();

                    if (responseDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                    }
                    else
                    {
                        throw new ApplicationException(responseDoc.InnerXml);
                    }
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                finally
                {
                    responseDoc = null;
                }
            }
        }

        //Changed by gagan for MITS 19259 : Start
        protected void ShowActiveDiariesDue_OnTextChanged(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = ViewState["pagenumber"].ToString();
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();
                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";
                //sshrikrishna:End changes for MITS 27074
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();//MITS 27074
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString(); //MITS 27074
                GridViewBinder(objDiaryCurrentAction,true);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //Commented by Amitosh.Because the filtering property is available in Terelik Grid
        //Changed by gagan for MITS 19259 : End
        //Neha Mits 23586 05/16/2011 added filter criteria on the page
        //protected void btnFilter_Click(object sender, EventArgs e)
        //{
        //    DiaryListCurrentAction objDiaryCurrentAction = null;
        //    try
        //    {
        //        string sTaskName = "";
        //        sTaskName = txtTaskName.Text;
        //        if (string.Compare(sTaskName, "") != 0)
        //        {
        //            sTaskName = sTaskName.Trim();
        //            sTaskName = sTaskName.Replace(" \r\n", "");
        //            sTaskName = sTaskName.Replace("\r\n ", "");
        //            sTaskName = sTaskName.Replace("\r\n", "");
        //        }
        //        objDiaryCurrentAction = new DiaryListCurrentAction();

        //        objDiaryCurrentAction.Pagenumber = "1";
        //        objDiaryCurrentAction.Duedate = ViewState["datedue"].ToString();
        //        objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();
        //        objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
        //        objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
        //        objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
        //        objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
        //        objDiaryCurrentAction.TaskNameFilter = drpdwnFilter.SelectedValue;
        //        ViewState["taskname"] = sTaskName;
        //        ViewState["taskfilter"] = drpdwnFilter.SelectedValue;
        //        objDiaryCurrentAction.TaskNameText = sTaskName;
        //        GridViewBinder(objDiaryCurrentAction);

        //    }
        //    catch (Exception ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        //        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
        //    }
        //}

        /// <summary>
        /// Added for Terelik Grid
        /// </summary>
        /// <Author>Amitosh</Author>
        /// <param name="sender"></param>
        /// <param name="e"></param>

      
        protected void lstDiary_PreRender(object sender, EventArgs e)
        {
            GridFilterMenu menu = lstDiary.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull" || menu.Items[i].Text == "GreaterThan" || menu.Items[i].Text == "LessThan" || menu.Items[i].Text == "GreaterThanOrEqualTo" || menu.Items[i].Text == "LessThanOrEqualTo" || menu.Items[i].Text == "IsEmpty" || menu.Items[i].Text == "NotIsEmpty")
                {
                    menu.Items.RemoveAt(i);
                }
                else
                {
                    i = (i + 1);
                }
            }
        }

        protected void lstDiary_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            string sCommandArg = null;
            try
            {
                try
                {
                    sCommandArg = ((System.Web.UI.WebControls.LinkButton)(((Telerik.Web.UI.GridCommandEventArgs)(e)).CommandSource)).CommandArgument;
                }
                catch
                {
                    sCommandArg = ((System.Web.UI.WebControls.Button)(((Telerik.Web.UI.GridCommandEventArgs)(e)).CommandSource)).CommandArgument;
                }
                DiaryListCurrentAction objDiaryCurrentAction = null;

                objDiaryCurrentAction = new DiaryListCurrentAction();


             //averma62 - MITS 28971  objDiaryCurrentAction.Duedate = ViewState["datedue"].ToString();                
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();

                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";

                //    objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();  
                //sshrikrishna:End changes for MITS 27074
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                //End Neha Mits 23586
               

                switch (sCommandArg)
                {
                    case "Next":
                        objDiaryCurrentAction.Pagenumber = (Convert.ToInt32(ViewState["pagenumber"]) + 1).ToString();
                        break;
                    case "Previous":
                        objDiaryCurrentAction.Pagenumber = (Convert.ToInt32(ViewState["pagenumber"]) - 1).ToString();
                        break;
                    case "First":
                        objDiaryCurrentAction.Pagenumber = "1";
                        break;
                    case "Last":
                        objDiaryCurrentAction.Pagenumber = ViewState["lastpage"].ToString();
                        break;
                    default:
                        int iPageIndex = e.NewPageIndex + 1;
                        objDiaryCurrentAction.Pagenumber = iPageIndex.ToString();
                        break;
                }
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString();

                GridViewBinder(objDiaryCurrentAction,true);

            }
            catch
            {
            }
        }

        protected void chkShowregarding_CheckedChanged(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;

            try
            {
                if (chkShowregarding.Checked == true)
                {
                    ViewState["showregarding"] = "1";
                }
                else
                {
                    //ViewState["shownotes"] = "";
                    ViewState["showregarding"] = "0";   // csingh7 MITS 18435
                }

                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = ViewState["pagenumber"].ToString();
                if (chkActiveDiaryChecked.Checked == false)//Change by kuladeep for mits:29089
                    objDiaryCurrentAction.Duedate = "today";
                else
                    objDiaryCurrentAction.Duedate = string.Empty;
                //sshrikrishna:Start changes for MITS 27074
                if (Request.QueryString.Count == 0)
                    objDiaryCurrentAction.Diarysource = "D";
                else
                    objDiaryCurrentAction.Diarysource = "C";

                if (Session["activediarychecked"] != null)
                    objDiaryCurrentAction.Activediarychecked = Session["activediarychecked"].ToString();

                if (Session["claimactivediarychecked"] != null)
                    objDiaryCurrentAction.Claimactivediarychecked = Session["claimactivediarychecked"].ToString();
                //objDiaryCurrentAction.Activediarychecked = ViewState["activediarychecked"].ToString();       
                //sshrikrishna:End changes for MITS 27074
                objDiaryCurrentAction.Reqactdiary = ViewState["reqactdiary"].ToString();
                if (ViewState["Showalldiariesforactiverecord"] != null)//RMA-16478
                objDiaryCurrentAction.Showalldiariesforactiverecord = ViewState["Showalldiariesforactiverecord"].ToString();
                //Ankit Start : Worked on MITS - 29939
                //if (!chkActiveDiaryChecked.Checked)
                //    objDiaryCurrentAction.Showallclaimdiaries = ViewState["ShowAllDiariesRelatedToClaim"].ToString();
                //Ankit End
                if (Session["ShowAllDiariesRelatedToClaimchecked"] != null)
                    objDiaryCurrentAction.Showallclaimdiaries = Session["ShowAllDiariesRelatedToClaimchecked"].ToString();
                objDiaryCurrentAction.Shownotes = ViewState["shownotes"].ToString();
                objDiaryCurrentAction.Showregarding = ViewState["showregarding"].ToString();
                //Neha Mits 23586 05/16/2011--aaply filter on the page
                objDiaryCurrentAction.TaskNameText = ViewState["taskname"].ToString();
                objDiaryCurrentAction.TaskNameFilter = ViewState["taskfilter"].ToString();
                objDiaryCurrentAction.FilterExpression = ViewState["FilterExpr"].ToString();
                //End Neha Mits 23586
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();//MITS 27074
                GridViewBinder(objDiaryCurrentAction,true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        // end Amitosh    

        protected DateTime? startDate
        {
            set
            {
                ViewState["strD"] = value;
            }
            get
            {
                if (ViewState["strD"] != null)
                    return (DateTime)ViewState["strD"];
                else
                {
                    return DateTime.Today.AddYears(-1);
                }
            }
        }
        //protected DateTime? endDate
        //{
        //    set
        //    {
        //        ViewState["endD"] = value;
        //    }
        //    get
        //    {
        //        if (ViewState["endD"] != null)
        //            return (DateTime)ViewState["endD"];
        //        else
        //        {
        //            return DateTime.Today;
        //        }
        //    }
        //}

        protected void lstDiary_ItemCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.FilterCommandName)
                {
                    Pair filterPair = (Pair)e.CommandArgument;

                    switch (filterPair.Second.ToString())
                    {
                        case "COMPLETEDATE":
                            this.startDate = ((e.Item as GridFilteringItem)[filterPair.Second.ToString()].FindControl("filterRadDatePicker") as RadDatePicker).SelectedDate;
                        //    this.endDate = ((e.Item as GridFilteringItem)[filterPair.Second.ToString()].FindControl("txtToOrderDatePicker") as RadDatePicker).SelectedDate;
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //Deb MITS 31489
        protected void grdlstDiary_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridItem)
            {
                string sCulture = AppHelper.GetCulture().ToString();
                RadDatePicker txtToOrderDatePicker = ((GridItem)e.Item).FindControl("filterRadDatePicker") as RadDatePicker;
                if (txtToOrderDatePicker != null)
                {
                    txtToOrderDatePicker.Culture = new CultureInfo(sCulture);
                }
            }
        }
        //igupta3 Mits:32846 Changes starts
        private XmlDocument VoidDiaryCurrentDate()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.VoidDiary";

                inputDocNode = GetCurrentDateInputForVoidDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                diaryDoc = null;
                dHelper = null;
                inputDocNode = null;
            }
        }
        private XmlNode GetCurrentDateInputForVoidDiary()
        {
            XmlDocument voidDiaryDoc = null;
            XmlNode voidDiaryNode = null;
            XmlAttribute atbAttachRecordId = null;
            XmlAttribute atbAttachTable = null;
            XmlAttribute atbAssignedUser = null;
            XmlAttribute atbDiaryId = null;
            XmlAttribute atbFromDate = null;
            XmlAttribute atbToDate = null;
            XmlAttribute activityStringNode = null;
            XmlAttribute currentDate = null;

            try
            {
                voidDiaryDoc = new XmlDocument();

                voidDiaryNode = voidDiaryDoc.CreateElement("VoidDiary");

                atbAttachRecordId = voidDiaryDoc.CreateAttribute("AttachRecordId");
                atbAttachRecordId.Value = "";
                voidDiaryNode.Attributes.Append(atbAttachRecordId);

                atbAttachTable = voidDiaryDoc.CreateAttribute("AttachTable");
                atbAttachTable.Value = "";
                voidDiaryNode.Attributes.Append(atbAttachTable);

                atbAssignedUser = voidDiaryDoc.CreateAttribute("assigneduser");
                atbAssignedUser.Value = ViewState["assigneduser"].ToString();
                voidDiaryNode.Attributes.Append(atbAssignedUser);

                atbDiaryId = voidDiaryDoc.CreateAttribute("diaryid");
                atbFromDate = voidDiaryDoc.CreateAttribute("fromdate");
                atbToDate = voidDiaryDoc.CreateAttribute("todate");

                atbDiaryId.Value = hdnEntry_id.Value.ToString();
                atbFromDate.Value = "";
                atbToDate.Value = "";

                voidDiaryNode.Attributes.Append(atbDiaryId);
                voidDiaryNode.Attributes.Append(atbFromDate);
                voidDiaryNode.Attributes.Append(atbToDate);

                //currentDate = voidDiaryDoc.CreateAttribute("CurrentDate");
                //currentDate.Value = DateTime.Today.ToShortDateString();
                //voidDiaryNode.Attributes.Append(currentDate);

                voidDiaryDoc.AppendChild(voidDiaryNode);

                return voidDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                voidDiaryDoc = null;
                voidDiaryNode = null;
                atbAttachRecordId = null;
                atbAttachTable = null;
                atbAssignedUser = null;
                atbDiaryId = null;
                atbFromDate = null;
                atbToDate = null;
                activityStringNode = null;
                currentDate = null;
            }
        }

        private XmlDocument CompleteDiaryCurrentDate()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; 
            string[] arrEntryIds;
            int iIteratorVar = 0;
            XmlDocument saveDiaryDoc = null;
            XmlNode saveDiaryNode = null;
            string sSaveDiaryInnerXML = string.Empty;
            //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; 

            try
            {
                //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; added ifthenelse in case multiple diaries are selected
                ViewState["entryid"] = hdnEntry_id.Value.ToString();
                if (Convert.ToString(ViewState["entryid"]).IndexOf(',') == -1)
                {
                    serviceMethodToCall = "WPAAdaptor.SaveDiary";

                    //to be fill up by viewstate
                    inputDocNode = GetCurrentDateInputDocForCompleteDiary();

                    dHelper = new DiariesHelper();
                    diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(diaryServiceOutput);
                    return diaryDoc;
                }
                else
                {
                    serviceMethodToCall = "WPAAdaptor.SaveDiary";

                    saveDiaryDoc = new XmlDocument();
                    saveDiaryNode = saveDiaryDoc.CreateElement("MultipleDiaries");

                    arrEntryIds = Convert.ToString(ViewState["entryid"]).Split(new char[] { ',' });
                    for (iIteratorVar = 0; iIteratorVar < arrEntryIds.Length; iIteratorVar++)
                    {
                        ViewState["entryid"] = arrEntryIds[iIteratorVar];

                        //to be fill up by viewstate
                        inputDocNode = GetCurrentDateInputDocForCompleteDiary();
                        sSaveDiaryInnerXML = sSaveDiaryInnerXML + inputDocNode.OuterXml;

                        //saveDiaryNode.AppendChild(inputDocNode);
                    }
                    saveDiaryNode.InnerXml = sSaveDiaryInnerXML;

                    dHelper = new DiariesHelper();
                    diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(saveDiaryNode, serviceMethodToCall);

                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(diaryServiceOutput);
                    return diaryDoc;
                }
                //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/27/2012; added ifthenelse in case multiple diaries are selected

            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                diaryDoc = null;
                dHelper = null;
                inputDocNode = null;
                saveDiaryDoc = null;
                saveDiaryNode = null;
            }
        }

        private XmlNode GetCurrentDateInputDocForCompleteDiary()
        {
            XmlDocument saveDiaryDoc = null;
            XmlNode saveDiaryNode = null;
            XmlNode entryIdNode = null;
            XmlNode assigningUserNode = null;
            XmlNode dueDateNode = null;
            XmlNode responseDateNode = null;
            XmlNode responseNode = null;
            XmlNode teTrackedNode = null;
            XmlNode teTotalHoursNode = null;
            XmlNode teExpensesNode = null;
            XmlNode teEndTimeNode = null;
            XmlNode startTimeNode = null;
            XmlNode statusOpenNode = null;
            XmlNode autoConfirmNode = null;
            XmlNode activityStringNode = null;
            XmlNode currentDateSetting = null;

            try
            {
                saveDiaryDoc = new XmlDocument();
                saveDiaryNode = saveDiaryDoc.CreateElement("SaveDiary");

                entryIdNode = saveDiaryDoc.CreateElement("EntryId");
                entryIdNode.InnerText = ViewState["entryid"].ToString();
                saveDiaryNode.AppendChild(entryIdNode);

                assigningUserNode = saveDiaryDoc.CreateElement("AssigningUser");
                assigningUserNode.InnerText = "";
                saveDiaryNode.AppendChild(assigningUserNode);

                dueDateNode = saveDiaryDoc.CreateElement("DueDate");
                dueDateNode.InnerText = "";
                saveDiaryNode.AppendChild(dueDateNode);

                responseDateNode = saveDiaryDoc.CreateElement("ResponseDate");
                responseDateNode.InnerText = DateTime.Today.ToShortDateString();
                saveDiaryNode.AppendChild(responseDateNode);

                responseNode = saveDiaryDoc.CreateElement("Response");
                responseNode.InnerText = "";
                saveDiaryNode.AppendChild(responseNode);

                teTrackedNode = saveDiaryDoc.CreateElement("TeTracked");
                teTrackedNode.InnerText = "";

                saveDiaryNode.AppendChild(teTrackedNode);

                teTotalHoursNode = saveDiaryDoc.CreateElement("TeTotalHours");
                teTotalHoursNode.InnerText = "";
                saveDiaryNode.AppendChild(teTotalHoursNode);

                teExpensesNode = saveDiaryDoc.CreateElement("TeExpenses");
                teExpensesNode.InnerText = "";
                saveDiaryNode.AppendChild(teExpensesNode);

                teEndTimeNode = saveDiaryDoc.CreateElement("TeEndTime");
                teEndTimeNode.InnerText = "";
                saveDiaryNode.AppendChild(teEndTimeNode);

                startTimeNode = saveDiaryDoc.CreateElement("TeStartTime");
                startTimeNode.InnerText = "";
                saveDiaryNode.AppendChild(startTimeNode);

                statusOpenNode = saveDiaryDoc.CreateElement("StatusOpen");
                statusOpenNode.InnerText = "False";
                saveDiaryNode.AppendChild(statusOpenNode);

                autoConfirmNode = saveDiaryDoc.CreateElement("AutoConfirm");
                autoConfirmNode.InnerText = "0";
                saveDiaryNode.AppendChild(autoConfirmNode);


                activityStringNode = saveDiaryDoc.CreateElement("ActivityString");
                activityStringNode.InnerText = "";
                saveDiaryNode.AppendChild(activityStringNode);

                currentDateSetting = saveDiaryDoc.CreateElement("CurrentDateSetting");
                currentDateSetting.InnerText = "TRUE";
                saveDiaryNode.AppendChild(currentDateSetting);

                saveDiaryDoc.AppendChild(saveDiaryNode);

                return saveDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                saveDiaryDoc = null;
                saveDiaryNode = null;
                entryIdNode = null;
                assigningUserNode = null;
                dueDateNode = null;
                responseDateNode = null;
                responseNode = null;
                teTrackedNode = null;
                teTotalHoursNode = null;
                teExpensesNode = null;
                teEndTimeNode = null;
                startTimeNode = null;
                statusOpenNode = null;
                autoConfirmNode = null;
                activityStringNode = null;
                currentDateSetting = null;
            }
        }
        //igupta3 Mits:32846 Changes ends    
        //WWIG GAP20A - agupta298 - MITS 36804 - Start

        /// <summary>
        /// Function for adding column(s) to grid dynamically
        /// </summary>
        /// <Author>agupta298 - WWIG GAP20A</Author>
        private void AddDiarySuppColumn(DataSet p_diaryRecordsSet)
        {
            GridBoundColumn objGridBoundColumn;
            string sSortingColumnName = string.Empty;
            bool IsColumnExists = false;

            try
            {
                if (p_diaryRecordsSet.Tables.Contains("option") && p_diaryRecordsSet.Tables["option"].Rows.Count > 0 && p_diaryRecordsSet.Tables.Contains("diary"))
                {
                    foreach (DataRow dr in p_diaryRecordsSet.Tables["option"].Rows)
                    {
                        GridColumn col = lstDiary.MasterTableView.Columns.FindByUniqueNameSafe(Convert.ToString(dr["ColumnName"]));
                        if (col != null)
                        {
                            //Need to Delete the column as we have to maintain the sequence of dynamic column.
                            this.lstDiary.MasterTableView.Columns.Remove(col);
                        }

                        //Important: first Add column to the collection 
                        objGridBoundColumn = new GridBoundColumn();
                        this.lstDiary.MasterTableView.Columns.Add(objGridBoundColumn);
                        //Then set properties 
                        objGridBoundColumn.UniqueName = Convert.ToString(dr["ColumnName"]);
                        objGridBoundColumn.DataField = Convert.ToString(dr["ColumnName"]);
                        objGridBoundColumn.HeaderText = Convert.ToString(dr["HeaderText"]);
                        objGridBoundColumn.SortExpression = Convert.ToString(dr["ColumnName"]);
                        objGridBoundColumn.AllowFiltering = false;
                        objGridBoundColumn.AllowSorting = false;
                        objGridBoundColumn.ItemStyle.Width = 140;
                        objGridBoundColumn.HeaderStyle.Width = 140;
                        objGridBoundColumn.ItemStyle.CssClass = "SuppColumn";

                        if (p_diaryRecordsSet.Tables["diary"].Rows.Count > 0)
                        {
                            if (Convert.ToString(dr["FieldType"]) == "SuppTypeHyperlink")
                            {
                                #region For Creating anchor tag for SuppTypeHyperlink
                                foreach (DataRow dtrow in p_diaryRecordsSet.Tables["diary"].Rows)
                                {
                                    //<a id='link_" + p_iFieldId + "_Diary_supp' href='" + p_Data + "' class='LightBlue' style='display:inline-block;color:#330099;font-weight:bold;text-decoration:underline;width:5px;cursor:pointer' target='_blank'>" + arrData[0] + "</a>";
                                    string sURL = Convert.ToString(dtrow[Convert.ToString(dr["ColumnName"])]);
                                    string sFormattedURL = string.Empty;
                                    if (!string.IsNullOrEmpty(sURL))
                                    {


                                        // Initialize StringWriter instance.
                                        StringWriter stringWriter = new StringWriter();
                                        // Put HtmlTextWriter in using block because it needs to call Dispose.
                                        using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
                                        {
                                            writer.AddAttribute(HtmlTextWriterAttribute.Href, sURL);
                                            writer.AddAttribute(HtmlTextWriterAttribute.Id, "link_" + Convert.ToString(dr["FieldID"]) + "_Diary_supp");
                                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "LightBlue");
                                            writer.AddAttribute(HtmlTextWriterAttribute.Style, "display:inline-block;color:#330099;font-weight:bold;text-decoration:underline;width:100%;cursor:pointer");
                                            writer.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");
                                            writer.RenderBeginTag(HtmlTextWriterTag.A);
                                            writer.Write(Convert.ToString(dr["WebLinkName"]));
                                            writer.RenderEndTag();
                                        }
                                        sFormattedURL = stringWriter.ToString();
                                    }

                                    dtrow[Convert.ToString(dr["ColumnName"])] = sFormattedURL;
                                }
                                #endregion
                            }
                        }
                    }
                }
                //case for removing the Rad grid column in case of No DiarySupplemental Column exit after updating config in same session.
                else if (!p_diaryRecordsSet.Tables.Contains("option") && p_diaryRecordsSet.Tables.Contains("diary"))
                {
                    GridColumnCollection objGridColumnCollection = lstDiary.MasterTableView.Columns;
                    if (objGridColumnCollection != null && objGridColumnCollection.Count > 0)
                    {
                        foreach (GridColumn item in objGridColumnCollection)
                        {
                            if (item.ColumnType == "GridBoundColumn")
                                this.lstDiary.MasterTableView.Columns.Remove(item);
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }

}
