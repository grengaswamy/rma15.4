﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.Diaries
{
    public partial class DiaryHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryHistory.aspx"), "DiaryHistoryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DiaryHistoryValidations", sValidationResources, true);
                if (!IsPostBack)
                {
                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }


                    if (ViewState["ispeekdiary"].ToString() == "true")
                    {
                        //set peek style of buttons
                        SetPeekStyle();
                    }
                    else
                    {
                        if (ViewState["attachrecordid"].ToString() == "")
                        {
                            //set defualt style
                            SetDefaultStyle();
                        }
                        else
                        {
                            //set attach diary button style
                            SetAttachDiaryStyle();
                        }
                    }





                    ViewState["sortorder"] = "5";
                    
                    

                    objDiaryCurrentAction = new DiaryListCurrentAction();

                    objDiaryCurrentAction.Pagenumber = "1";
                    objDiaryCurrentAction.Duedate = "today";
                    objDiaryCurrentAction.Sortorder = "5";


                    //if (chkActiveDiaryChecked.Checked == false)
                    //{
                    //    txtdatedue.Text = System.DateTime.Now.Date.ToShortDateString();
                    //}

                    btnFirstPage.Enabled = false;
                    btnPrev.Enabled = false;
                    //skhare7 For Multilang commented
                   // lblDiaries.Text = ViewState["assigneduser"].ToString() + "'s Diary History";

                    GridViewBinder(objDiaryCurrentAction);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string imageurl = @"../../Images/";
            string imageSortUpUrl = imageurl + "arrow_up_white.gif";
            string imageSortDownUrl = imageurl + "arrow_down_white.gif";

            try
            {
                //setting sorting Images as per user action
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    HtmlImage imgSort = new HtmlImage();
                    imgSort.ID = "imgSort";
                    if (ViewState["sortcolumnindex"] != null)
                    {
                        int colIndex = Convert.ToInt32(ViewState["sortcolumnindex"]);

                        if (ViewState["sortdirection"].ToString() == "ASC")
                        {
                            imgSort.Src = imageSortUpUrl;
                        }
                        else
                        {
                            imgSort.Src = imageSortDownUrl;
                        }

                        e.Row.Cells[colIndex].Controls.Add(imgSort);
                    }
                    else
                    {
                        imgSort.Src = imageSortDownUrl;
                        e.Row.Cells[3].Controls.Add(imgSort);
                    }
                }

                //setting priority images
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label gridLblDueDate = null;
                    Label gridClaimStatusLabel = null;
                    Label gridAssigningUser = null; //tmalhotra3-MITS 25091
                    Label gridAssignedUser = null;
                    Label gridNotesLabel = null;
                    Label gridRegardingLabel = null;
                    Label gridLblTaskSubjectLabel = null;
                    

                    string txtNotes = string.Empty;
                    string txtRegarding = string.Empty;

                    //Start by Shivendu for MITS 21606
                    Label gridAttachRecordLabel = null;
                    string txtAttachPrompt = string.Empty;
                    string txtAttachTable = string.Empty;
                    string txtAttachedRecordId = string.Empty;
                    string txtAttachedRecordCategory = string.Empty;
                    //End by Shivendu for MITS 21606
                    
                    //setting duedate value in gridview
                    gridLblDueDate = (Label)e.Row.Cells[3].FindControl("lblDueDate");

                    try
                    {
                        gridLblDueDate.Text = DataBinder.Eval(e.Row.DataItem, "complete_date").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridLblDueDate.Text = "";
                    }

                    //tanwar2 - mits 30533 - missing image when priority = 0
                    //if (DataBinder.Eval(e.Row.DataItem, "priority").ToString() != "1")
                    if (!(DataBinder.Eval(e.Row.DataItem, "priority").ToString() == "1" || DataBinder.Eval(e.Row.DataItem, "priority").ToString() == "0"))
                    {
                        HtmlImage imgPriority = new HtmlImage();
                        imgPriority.ID = "imgPriority";

                        imgPriority.Src = GetPrioiryUrl(DataBinder.Eval(e.Row.DataItem, "priority").ToString());
                        e.Row.Cells[1].Controls.Add(imgPriority);
                    }

                    string imgName = string.Empty;
                    HtmlImage imgUser = new HtmlImage();
                    imgUser.ID = "imgUser";

                    imgName = DataBinder.Eval(e.Row.DataItem, "img").ToString();
                    imgName = imgName.Substring(imgName.LastIndexOf("/") + 1);
                    imgUser.Src = imageurl + imgName;

                    e.Row.Cells[2].Controls.Add(imgUser);

                    gridLblTaskSubjectLabel = (Label)e.Row.Cells[4].FindControl("lbltaskSubject");
                    gridLblTaskSubjectLabel.Attributes.Add("onclick", "DisplayDiaryDetails('" + DataBinder.Eval(e.Row.DataItem, "entry_id").ToString() + "','" + ViewState["ispeekdiary"].ToString() + "','" + ViewState["assigneduser"].ToString() + "','" + ViewState["attachtable"].ToString() + "','" + ViewState["attachrecordid"].ToString() + "')");
					
					//pkandhari Jira 6412 starts
                    HtmlImage imgAssignedTo = new HtmlImage();

                    if (DataBinder.Eval(e.Row.DataItem, "assigned_group").ToString() == "0")
                    {
                        imgAssignedTo.ID = "imgAssignedToUser";
                        imgAssignedTo.Src = "../../Images/diary_assigned2individual.gif";
                    }
                    else
                    {
                        imgAssignedTo.ID = "imgAssignedToGroup";
                        imgAssignedTo.Src = "../../Images/diary_assigned2group.gif";
                    }
                    e.Row.Cells[1].Controls.Add(imgAssignedTo);
                    //pkandhari Jira 6412 ends

                    txtRegarding = DataBinder.Eval(e.Row.DataItem, "regarding").ToString();
                    try
                    {
                        txtNotes = DataBinder.Eval(e.Row.DataItem, "diary_Text").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        txtNotes = "";
                    }
                    //Start by Shivendu for MITS 21606
                    txtAttachPrompt = DataBinder.Eval(e.Row.DataItem, "attachprompt").ToString();
                    try
                    {
                        txtAttachTable = DataBinder.Eval(e.Row.DataItem, "attach_table").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        txtAttachTable = "";
                    }

                    try
                    {
                        txtAttachedRecordId = DataBinder.Eval(e.Row.DataItem, "attach_recordid").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        txtAttachedRecordId = "";
                    }
                    gridAttachRecordLabel = (Label)e.Row.Cells[5].FindControl("lblAttachRecord");
                    if (txtAttachPrompt != string.Empty)
                    {
                        //setting open attchment details through MDI
                        txtAttachedRecordCategory = GetAttachedTableCategory(txtAttachTable);
                        //starts by Nitin on 22-04-2009 in order to open some FDM screens as popups for Mits 15272
                        if (txtAttachedRecordCategory != string.Empty)
                        {
                            if (IsOpenAttachedRecordInPopup(txtAttachedRecordCategory))
                            {
                                gridAttachRecordLabel.Attributes.Add("onclick", "OpenAttachedRecordAsPopUp('" + txtAttachedRecordCategory + "','" + txtAttachedRecordId + "')");
                            }
                            else
                            {
                                gridAttachRecordLabel.Attributes.Add("onclick", "parent.MDIShowScreen('" + txtAttachedRecordId + "','" + txtAttachedRecordCategory + "')");
                            }
                        }
                    
                    }
                   
                    //End by Shivendu for MITS 21606

                    gridNotesLabel = (Label)e.Row.Cells[4].FindControl("lblGrdNotes");
                    gridNotesLabel.Text = txtNotes;

                    gridRegardingLabel = (Label)e.Row.Cells[4].FindControl("lblRegarding");
                    gridRegardingLabel.Text = txtRegarding;

                    gridClaimStatusLabel = (Label)e.Row.Cells[6].FindControl("lblClaimStatus");
                    try
                    {
                        gridClaimStatusLabel.Text = DataBinder.Eval(e.Row.DataItem, "claimstatus").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridClaimStatusLabel.Text = "";
                    }
				//tmalhotra3-MITS 25091 start
                    gridAssigningUser = (Label)e.Row.Cells[8].FindControl("lblAssigningUser");
                    try
                    {
                        gridAssigningUser.Text = DataBinder.Eval(e.Row.DataItem, "assigning_user").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridAssigningUser.Text = "";
                    }
                   
                    gridAssignedUser = (Label)e.Row.Cells[7].FindControl("lblAssignedUser");
                    try
                    {
                        gridAssignedUser.Text = DataBinder.Eval(e.Row.DataItem, "assigned_user").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridAssignedUser.Text = "";
                    }
                  //tmalhotra3-MITS 25091 end
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        public string GetPrioiryUrl(string priorityData)
        {
            string imageurl = @"../../Images/";

            if (priorityData == "1")
            {
                return "";
            }
            else if (priorityData == "2")
            {
                imageurl = imageurl + "important.gif";
            }
            else if (priorityData == "3")
            {
                imageurl = imageurl + "required.gif";
            }
            else
            {
                return "";
            }

            return imageurl;
        }

        private void SetViewState(DataTable dTable)
        {
            DataRow dRow = null;
            dRow = dTable.Rows[0];

            if (dRow["pagenumber"].ToString() == dRow["pagecount"].ToString()) //last page
            {
                ViewState.Add("pagenumber", dRow["pagenumber"]);
                ViewState.Add("nextpage", "");
                ViewState.Add("lastpage", "");

                // case when pagecount is only 1
                if (dRow["pagenumber"].ToString() == "1")
                {
                    btnFirstPage.Enabled = false;
                    btnPrev.Enabled = false;
                }
                else
                {
                    btnFirstPage.Enabled = true;
                    btnPrev.Enabled = true;
                }
                btnNext.Enabled = false;
                btnLast.Enabled = false;
            }
            else if (dRow["pagenumber"].ToString() == "1")
            {
                btnFirstPage.Enabled = false;
                btnPrev.Enabled = false;
                btnNext.Enabled = true;
                btnLast.Enabled = true;


                if (dRow["nextpage"] != null)
                {
                    ViewState.Add("nextpage", dRow["nextpage"]);
                }
                else
                {
                    ViewState.Add("nextpage", "");
                }

                if (dRow["lastpage"] != null)
                {
                    ViewState.Add("lastpage", dRow["lastpage"]);
                }
                else
                {
                    ViewState.Add("lastpage", "");
                }

                if (dRow["pagenumber"] != null)
                {
                    ViewState.Add("pagenumber", dRow["pagenumber"]);
                }
                else
                {
                    ViewState.Add("pagenumber", "");
                }
            }
            else
            {

                if (dRow["nextpage"] != null)
                {
                    ViewState.Add("nextpage", dRow["nextpage"]);
                }
                else
                {
                    ViewState.Add("nextpage", "");
                }

                if (dRow["lastpage"] != null)
                {
                    ViewState.Add("lastpage", dRow["lastpage"]);
                }
                else
                {
                    ViewState.Add("lastpage", "");
                }

                if (dRow["pagenumber"] != null)
                {
                    ViewState.Add("pagenumber", dRow["pagenumber"]);
                }
                else
                {
                    ViewState.Add("pagenumber", "");
                }
                
                btnFirstPage.Enabled = true;
                btnPrev.Enabled = true;
                btnNext.Enabled = true;
                btnLast.Enabled = true;
            }

            ViewState["user"] = dRow["user"].ToString();
        }

        protected void btnFirstPage_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = "1";
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();

                GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = (Convert.ToInt32(ViewState["pagenumber"]) - 1).ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();

                GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = (Convert.ToInt32(ViewState["pagenumber"]) + 1).ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();

                GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void btnLast_Click(object sender, EventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = ViewState["lastpage"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();

                GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;
            try
            {
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = ViewState["pagenumber"].ToString();
                if (ViewState["sortexpression"] != null)
                {
                    if (e.SortExpression == ViewState["sortexpression"].ToString())
                    {
                        ViewState["sortdirection"] = "DEC";
                        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "DEC").ToString();
                        //added by Nitin for Mits 14632 on 9-Apr-09
                        ViewState["sortexpression"] = "";
                    }
                    else
                    {
                        ViewState["sortdirection"] = "ASC";
                        ViewState["sortexpression"] = e.SortExpression;
                        objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "ASC").ToString();
                    }
                }
                else
                {
                    ViewState["sortdirection"] = "ASC";
                    ViewState["sortexpression"] = e.SortExpression;
                    objDiaryCurrentAction.Sortorder = DiariesHelper.GetSortOrder(e.SortExpression, "ASC").ToString();
                }



                ViewState["sortcolumnindex"] = DiariesHelper.GetSortColumnIndexforDiaryHistory(e.SortExpression).ToString(); //tmalhotra3- MITS 25091
               
                ViewState["sortorder"] = objDiaryCurrentAction.Sortorder;

                GridViewBinder(objDiaryCurrentAction);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


        }

        private void SetPeekStyle()
        {
            btnHome.Visible = true;
            //skhare7 Multilang comment
          //  lblDiaries.Text = ViewState["assigneduser"].ToString() + "'s Diary History";
            lblActionMessage.Visible = true;
          //  lblActionMessage.Text = "Note: You are currently peeking at another user's diaries. <br/>" + "Press the 'Home' button to see your own diaries.";
        }

        private void SetDefaultStyle()
        {
            btnHome.Visible = false;
            //skhare7 Multilang comment
           // lblDiaries.Text = ViewState["assigneduser"].ToString() + "'s Diary History";
            lblActionMessage.Visible = false;
        }

        private void SetAttachDiaryStyle()
        {
            btnHome.Visible = true;
            //skhare7 Multilang comment
           // lblDiaries.Text = "Diary History[" + ViewState["attachtable"].ToString() + " " + ViewState["attachrecordid"].ToString() + " Attachments Only]";
           // lblActionMessage.Visible = true;
          //  lblActionMessage.Text = "Note: You are currently looking at the list of all diaries attached to a record. <br/> Press the 'Home' button to see your own diaries.";
            lblMessage.Visible = true;
          //  lblMessage.Text = "Note: You are currently looking at the list of all diaries attached to a record. <br/> Press the 'Home' button to see your own diaries.";
        }
        

        private enum DIARYSORTORDER : int
        {
            NONE,
            ASC_COMPLETE_DATE,
            ASC_ATTACH_TABLE,
            ASC_ENTRY_NAME,
            ASC_PRIORITY,
            DEC_COMPLETE_DATE,
            DEC_ATTACH_TABLE,
            DEC_ENTRY_NAME,
            DEC_PRIORITY,
            ASC_WORK_ACTIVITY,
            DEC_WORK_ACTIVITY = 13
        }

        private DataSet GetDairyList(DiaryListCurrentAction objCurrentAction)
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            XmlNode retDiaryNode = null;
            DataSet dSet = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                //setting inputs to get dairylist from webservice
                dHelper = new DiariesHelper();

                inputDocNode = GetInputDocForDiaryHistory(objCurrentAction);

                serviceMethodToCall = "WPAAdaptor.GetDiaryHistoryDom";

                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                retDiaryNode = diaryDoc.SelectSingleNode("ResultMessage/Document/diaries");

                if (retDiaryNode != null)
                {
                    dSet = new DataSet();
                    dSet.ReadXml(new XmlNodeReader(diaryDoc));
                }
                return dSet;

            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {

            }
        }

        private void GridViewBinder(DiaryListCurrentAction objDiaryCurrentAction)
        {
            DataSet diaryRecordsSet = null;
            
            objDiaryCurrentAction.User = ViewState["assigneduser"].ToString();
            objDiaryCurrentAction.Attachtable = ViewState["attachtable"].ToString();
            objDiaryCurrentAction.Attachrecordid = ViewState["attachrecordid"].ToString();

            diaryRecordsSet = GetDairyList(objDiaryCurrentAction);
            lblRecords.Text = "Page " + diaryRecordsSet.Tables[2].Rows[0]["pagenumber"].ToString() + " of " + diaryRecordsSet.Tables[2].Rows[0]["pagecount"].ToString();

            try
            {
                SetViewState(diaryRecordsSet.Tables[2]);
            }
            catch (IndexOutOfRangeException)
            {
                GridView1.Visible = false;
                btnRefresh.Visible = false;
                tblNoRecords.Visible = true;
            }


            try
            {
                GridView1.DataSource = diaryRecordsSet.Tables[3];
                GridView1.DataBind();
            }
            catch (IndexOutOfRangeException)
            {
                GridView1.Visible = false; 
                btnRefresh.Visible = false;
                tblNoRecords.Visible = true;
            }
        }
        /// <summary>
        /// Added by Nitin,in order to open attachment through Mdi Navigation tree
        /// </summary>
        /// <param name="attachTable"></param>
        /// <returns></returns>
        private string GetAttachedTableCategory(string attachTable)
        {
            string attachedTableCategory = string.Empty;
            string[] arrAdmTrack = null;
            char unitSeparator = (char)31;

            //Added by Nitin for Mits 14882 on 08-Apr-09
            if (attachTable.ToLower().Contains("admintracking|"))
            {
                arrAdmTrack = attachTable.Split('|');

                //parent.MDIShowScreen(lRecordID, "admintrackinglist" + del + "Admin Tracking (" + admtable + ")" + del + "UI/FDM/admintracking" + admtable + ".aspx" + del + "?recordID=(NODERECORDID)");

                if (arrAdmTrack.GetUpperBound(0) > 0)
                {
                    attachedTableCategory = "admintrackinglist" + unitSeparator + "Admin Tracking (" + arrAdmTrack[1] + ")" + unitSeparator + "UI/FDM/admintracking" + arrAdmTrack[1] + ".aspx" + unitSeparator + "?recordID=(NODERECORDID)"; // nnorouzi; no further items (SELECETD/TOP, another Root)are provided in the categoryArg, otherwise it will be considered as an injected node.
                    return attachedTableCategory;
                }
            }
            //Ended by Nitin for Mits 14882 on 08-Apr-09

            //cases are changed by Nitin for Mits 15044 on 07th April 09
            switch (attachTable)
            {
                case "LEAVEPLAN":
                    attachedTableCategory = "Leaveplan";
                    break;
                case "ADJUST_DATED_TEXT":
                    attachedTableCategory = "adjusterdatedtext";
                    break;
                case "POLICY_ENH":
                    attachedTableCategory = "policyenh";
                    break;
                // npadhy Start MITS 22028 Policies opening up in Maintenance when opened from Diaries
                case "POLICY_ENH_AL":
                    attachedTableCategory = "policyenhal";
                    break;
                case "POLICY_ENH_GL":
                    attachedTableCategory = "policyenhgl";
                    break;
                case "POLICY_ENH_PC":
                    attachedTableCategory = "policyenhpc";
                    break;
                case "POLICY_ENH_VA":
                    attachedTableCategory = "policyenhva";
                    break;
                case "POLICY_ENH_WC":
                    attachedTableCategory = "policyenhwc";
                    break;
                // npadhy End MITS 22028 Policies opening up in Maintenance when opened from Diaries
                case "DISABILITY_PLAN":
                    attachedTableCategory = "plan";
                    break;
                case "MED_STAFF":
                    attachedTableCategory = "staff";
                    break;
                case "BILL_X_INSTLMNT":
                    attachedTableCategory = "policybilling";
                    break;
                case "BILL_X_BILL_ITEM":
                    attachedTableCategory = "policybilling";
                    break;
                case "TM_JOB_LOG":
                    attachedTableCategory = "taskmanagerjob";
                    break;
                default:
                    attachedTableCategory = attachTable.ToLower();
                    break;
            }
            return attachedTableCategory;
        }

        /// <summary>
        /// Added by Nitin on 22-04-2009 in order to open some 
        /// FDM screens as popups for Mits 15272
        /// </summary>
        /// <param name="attachedScreen"></param>
        /// <returns></returns>
        private bool IsOpenAttachedRecordInPopup(string attachedTableCategory)
        {
            //cases for medwatch,medwatchtest,fallinfo,concomitant have been added by Nitin 
            //for Mits no 16940 on 15/June/2009 

            switch (attachedTableCategory)
            {
                case "adjuster":
                case "adjusterdatedtext":
                case "claimant":
                case "defendant":
                case "litigation":
                case "expert":
                case "piemployee":
                case "pidependent":
                case "pirestriction":
                case "piworkloss":
                case "pimedstaff":
                case "pimedstaffprivilege":
                case "pimedstaffcertification":
                case "piother":
                case "pipatient":
                case "piprocedure":
                case "piphysician":
                case "piprivilege":
                case "picertification":
                case "pieducation":
                case "piprevhospital":
                case "piwitness":
                case "cmxcmgrhist":
                case "cmxaccommodation":
                case "cmxvocrehab":
                case "cmxtreatmentpln":
                case "cmxmedmgtsavings":
                case "leave":
                case "unit":
                case "osha":
                case "eventdatedtext":
                case "casemgrnotes":
                case "dependent":
                case "medwatch":
                case "medwatchtest":
                case "fallinfo":
                case "concomitant":
                case "taskmanagerjob":
                    return true;
                default:
                    return false;
            }
        }
        private XmlNode GetInputDocForDiaryHistory(DiaryListCurrentAction objCurrentAction)
        {
            XmlDocument diaryHistoryDoc = null;
            XmlNode diaryHistoryNode = null;
            XmlAttribute atbAttachRcdIdNode = null;
            XmlAttribute atbAttachTableNode = null;
            XmlAttribute atbPageCountNode = null;
            XmlAttribute atbPageNumberNode = null;
            XmlAttribute atbPageSizeNode = null;
            XmlAttribute atbRecordCountNode = null;
            XmlAttribute atbSortOrderNode = null;
            XmlAttribute atbUserNameNode = null;

            
                diaryHistoryDoc = new XmlDocument();
                diaryHistoryNode = diaryHistoryDoc.CreateElement("GetDiaryHistoryDom");

                atbAttachRcdIdNode = diaryHistoryDoc.CreateAttribute("attachrecordid");
                atbAttachRcdIdNode.Value = objCurrentAction.Attachrecordid; 
                diaryHistoryNode.Attributes.Append(atbAttachRcdIdNode);

                atbAttachTableNode = diaryHistoryDoc.CreateAttribute("attachtable");
                atbAttachTableNode.Value = objCurrentAction.Attachtable; 
                diaryHistoryNode.Attributes.Append(atbAttachTableNode);

                atbPageCountNode = diaryHistoryDoc.CreateAttribute("pagecount");
                atbPageCountNode.Value = objCurrentAction.Pagecount;
                diaryHistoryNode.Attributes.Append(atbPageCountNode);

                atbPageNumberNode = diaryHistoryDoc.CreateAttribute("pagenumber");
                atbPageNumberNode.Value = objCurrentAction.Pagenumber;
                diaryHistoryNode.Attributes.Append(atbPageNumberNode);

                atbPageSizeNode = diaryHistoryDoc.CreateAttribute("pagesize");
                atbPageSizeNode.Value = "10";
                diaryHistoryNode.Attributes.Append(atbPageSizeNode);

                atbRecordCountNode = diaryHistoryDoc.CreateAttribute("recordcount");
                atbRecordCountNode.Value = objCurrentAction.Recordcount;
                diaryHistoryNode.Attributes.Append(atbRecordCountNode);

                atbSortOrderNode = diaryHistoryDoc.CreateAttribute("sortorder");
                atbSortOrderNode.Value = objCurrentAction.Sortorder;
                diaryHistoryNode.Attributes.Append(atbSortOrderNode);

                atbUserNameNode = diaryHistoryDoc.CreateAttribute("username");
                atbUserNameNode.Value = ViewState["assigneduser"].ToString();
                diaryHistoryNode.Attributes.Append(atbUserNameNode);

                diaryHistoryDoc.AppendChild(diaryHistoryNode);

                return diaryHistoryNode; 
            
        }

        private XmlNode GetInputDocForDeleteDiary()
        {
            XmlDocument deleteDiaryDoc = null;
            XmlNode deleteNode = null;
            XmlAttribute diaryId = null;

           
                deleteDiaryDoc = new XmlDocument();

                deleteNode = deleteDiaryDoc.CreateElement("DeleteDiary");

                diaryId = deleteDiaryDoc.CreateAttribute("diaryid");
                diaryId.Value = hdnEntryId.Value;
                deleteNode.Attributes.Append(diaryId);

                deleteDiaryDoc.AppendChild(deleteNode);

                return deleteNode;
            
        }

        private void RefreshDiary()
        {
            DiaryListCurrentAction objDiaryCurrentAction = null;

           
                objDiaryCurrentAction = new DiaryListCurrentAction();

                objDiaryCurrentAction.Pagenumber = ViewState["pagenumber"].ToString();
                objDiaryCurrentAction.Sortorder = ViewState["sortorder"].ToString();

                GridViewBinder(objDiaryCurrentAction);
            
        }

        private void DeleteDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            
                serviceMethodToCall = "WPAAdaptor.DeleteDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForDeleteDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
            
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshDiary();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteDiary();
                RefreshDiary();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("DiaryList.aspx", false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }        
    }
}
