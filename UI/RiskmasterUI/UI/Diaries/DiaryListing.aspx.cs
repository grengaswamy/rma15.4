﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class DiaryListing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            try
            {
                //Rakhel ML Changes - start 
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryListing.aspx"), "DiaryListingValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DiaryListingValidations", sValidationResources, true);
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
               //Rakhel ML Changes - end

                if (!IsPostBack)
                {
                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }
                    //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                    if (Request.QueryString["usersortorder"] != null)
                    {
                        ViewState["usersortorder"] = Request.QueryString["usersortorder"];
                    }
                    else
                    {
                        ViewState["usersortorder"] = "0";
                    }
                    //End Mits 23477

                    //MITS 32210 Raman Bhatia
                    //MITS 36764 srajindersin 5/23/2014
                    diaryDoc = LoadDiary();

                    PopulateFormControls(diaryDoc.SelectSingleNode("ResultMessage/Document"));

                    PopulateComboBox();

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
                
        }

        private void PopulateFormControls(XmlNode documentNode)
        {
            XmlNodeList taskDescNodeList = null;
            XmlNode taskDescVisbleAttr = null;
            taskDescVisbleAttr = documentNode.SelectSingleNode("TaskDescList/@visible");
            if (taskDescVisbleAttr.InnerText == "true")
            {
                cboTaskDesc.Visible = true;
                chkUseTaskDesc.Visible = true;
                //taskDescNodeList = documentNode.SelectNodes("TaskDescList/option");

                //MITS 32210 Raman Bhatia
                cboTaskDesc.Text = "";
                //added by Nitin for Mits 15800
                //cboTaskDesc.Items.Clear();
                //cboTaskDesc.Items.Add("");

                /*
                foreach (XmlNode taskDescNode in taskDescNodeList)
                {
                    cboTaskDesc.Items.Add(taskDescNode.InnerText);
                }
                 */
            }
            else
            {
                cboTaskDesc.Visible = false;
                chkUseTaskDesc.Visible = false;
            }
            //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
            //PopulateComboBox();
        }

        
        /// <summary>
        /// Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
        /// </summary>
        private void PopulateComboBox()
        {
            string sUserSortOrder = ViewState["usersortorder"].ToString();
            string[] stempValue = new string[4];
            if (string.Compare(sUserSortOrder, "0") != 0)
            {
                //zmohammad MITS 31457 start
                stempValue = sUserSortOrder.Split(',');
                // aaggarwal29 : MITS 36691 Used sort direction to set sort direction values along with sort by.
                if (stempValue.Length > 0 && stempValue[0].Split(' ').Length == 2)
                {
                    selSortBy1.SelectedValue = stempValue[0].Split(' ')[0];
                    selSortOrder1.SelectedValue = stempValue[0].Split(' ')[1];
                }
                if (stempValue.Length > 1 && stempValue[1].Split(' ').Length == 2)
                {
                    selSortBy2.SelectedValue = stempValue[1].Split(' ')[0];
                    selSortOrder2.SelectedValue = stempValue[1].Split(' ')[1];
                }
                if (stempValue.Length > 2 && stempValue[2].Split(' ').Length == 2)
                {
                    selSortBy3.SelectedValue = stempValue[2].Split(' ')[0];
                    selSortOrder3.SelectedValue = stempValue[2].Split(' ')[1];
                }
                if (stempValue.Length > 3 && stempValue[3].Split(' ').Length == 2)
                {
                    selSortBy4.SelectedValue = stempValue[3].Split(' ')[0];
                    selSortOrder4.SelectedValue = stempValue[3].Split(' ')[1];
                }
                //zmohammad MITS 31457 end
            }


        } 

        //MITS 32210 Raman Bhatia
        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.GetTaskDescSetting";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForLoadDiary()
        {
            XmlDocument loadDiaryDoc = null;

            XmlNode loadDiaryNode = null;

            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("AssignedUser");
                loadDiaryNode.InnerText = ViewState["assigneduser"].ToString();

                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
        
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string assignedUser = string.Empty;
            string diarySel = string.Empty;
            string fromDate = string.Empty;
            string sortBy = string.Empty;
            string taskDesc = string.Empty;
            string toDate = string.Empty;
            string useTaskDesc = string.Empty;
            string sortOrder = string.Empty;


            try
            {
                assignedUser = ViewState["assigneduser"].ToString();

                if (selDiary1.Checked)
                {
                    diarySel = "0";
                }
                else if (selDiary2.Checked)
                {
                    diarySel = "1";
                }
                else if (selDiary3.Checked)
                {
                    diarySel = "2";
                }
                else if (selDiary4.Checked)
                {
                    diarySel = "3";
                }
                else if (selDiary5.Checked)
                {
                    diarySel = "4";
                }

                fromDate = txtFromDate.Value;
                toDate = txtToDate.Value;
                sortBy = sSortBy.Value;
                sortOrder = sSortOrder.Value;
                //Mits 23477 Neha
                //aaggarwal29 : MITS 36691 Added sort direction along with sort by clause
                ViewState["usersortorder"] = selSortBy1.SelectedValue + " " + selSortOrder1.SelectedValue + "," + selSortBy2.SelectedValue + " " + selSortOrder2.SelectedValue + "," + selSortBy3.SelectedValue + " " + selSortOrder3.SelectedValue + "," + selSortBy4.SelectedValue + " " + selSortOrder4.SelectedValue;

                // MITS 32210 Raman Bhatia
                //if (cboTaskDesc.SelectedItem != null)
                //{
                //taskDesc = cboTaskDesc.SelectedItem.Text;
                taskDesc = cboTaskDesc.Text;
                //sgoel6 04/06/2009 | MITS 14636 Use escape sequence
                taskDesc = taskDesc.Replace("'", "\\'");
                //encoded by Nitin for Mits 15798 on 24-04-2009
                taskDesc = Server.UrlEncode(taskDesc);
                //ended by Nitin for Mits 15798 on 24-04-2009
                //}
                //else
                //{
                //    taskDesc = "";
                //}

                useTaskDesc = chkUseTaskDesc.Checked.ToString();
                //aaggarwal29: MITS 36691 , added sort direction to the query string
                string script = "PrintListing.aspx?assigneduser=" + assignedUser + "&diarysel=" + diarySel + "&fromdate=" + fromDate + "&sortby=" + sortBy + "&taskdesc=" + taskDesc + "&todate=" + toDate + "&usetaskdesc=" + useTaskDesc+"&sortorder="+sortOrder;
                ClientScript.RegisterStartupScript(this.GetType(), "popup", "window.open('" + script + "','DiaryList','width=800,height=800' + ',top=' + (screen.availHeight - 800) / 2 + ',left=' + (screen.availWidth - 800) / 2 + ',resizable=yes,scrollbars=yes')", true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
               // Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                //Neha Mits 23477 User sort order will only be sent if print button is clicked. else it will not be part of the URL.
                if (ViewState["usersortorder"] != null)
                {
                    Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&usersortorder=" + ViewState["usersortorder"], false);
                }
                else
                {
                    Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
