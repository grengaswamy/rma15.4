﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryHistory.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryHistory" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DiaryList</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        
        function SetSelectDiary(entry_id)
        {
            document.getElementById("isDiarySelected").value = 'true';
            document.getElementById("hdnEntryId").value = entry_id;
        }
        
        function ValidateSelectedDiary()
        {
            if(document.getElementById("isDiarySelected").value == 'false')
            {
                //alert("Please select any diary to proceed");
                alert(DiaryHistoryValidations.SelectDiary);
                return false;
            }
            else
            {
                return  true;
            }
        }
        
        function DisplayDiaryDetails(entryId,isPeekDiary,assignedUser,attachTable,attachRecordId)
        {
            window.location = "/RiskmasterUI/UI/FDM/diarydetails.aspx?entryid=" + entryId + "&recordID=" + entryId + "&assignedusername=" + assignedUser + "&ispeekdiary=" + isPeekDiary  + "&attachtable=" + attachTable + "&attachrecord=" + attachRecordId + "&CalledBy=diarydetails";  
        }
        //Start by Shivendu for MITS 21606
        function OpenAttachedRecordAsPopUp(attachedScreen, attachedRecordId) {
            var wnd = null;
            if (attachedScreen == "taskmanagerjob") {
                wnd = window.open("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMStatusDetails.aspx?JobId=" + attachedRecordId, "AttachedRecord",
                      "width=750,height=500,top=" + (screen.availHeight - 500) / 2 + ",left=" + (screen.availWidth - 750) / 2 + ",resizable=yes,scrollbars=yes");
            }
            else {
                wnd = window.open("/RiskmasterUI/UI/FDM/" + attachedScreen + ".aspx?recordID=" + attachedRecordId, "AttachedRecord",
			    "width=900,height=650,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
            }
        }
        //End by Shivendu for MITS 21606
    </script> 
</head>
<body>
    <form id="form1" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <table width="100%" cellspacing="0" cellpadding="0" border="0">                                        
            <tr>
             <td colspan="6" align="left" class="PadLeft4">
               <asp:ImageButton ID="btnDelete" ImageUrl="~/Images/tb_delete_active.png" class="bold" ToolTip="<%$ Resources:ttDelete %>"
                     runat="server"  OnClientClick = "javascript:return ValidateSelectedDiary()" 
                     onclick="btnDelete_Click" />
               <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
               <%-- <asp:Button runat="server" ID="btnHome" Text="<%$ Resources:btnHome %>" Visible="false" CssClass="button"
                      Width="400px" onclick="btnHome_Click" />--%>
                  <asp:ImageButton ID="btnHome" visible="false" ImageUrl="~/Images/tb_previous_active.png" class="bold"
                ToolTip="<%$ Resources:btnHome %>" runat="server" OnClick="btnHome_Click" />
                 <!--     
                 <asp:LinkButton CssClass= "LightBlue" ID="btnRefresh" runat="server"  Text="<%$ Resources:btnRefresh %>" Font-Underline="true"  onclick="btnRefresh_Click">
                      </asp:LinkButton>
                     -->
                </td>
            </tr>                     
    </table>
    <div  class="divScroll" style="border-width:"0">   
        <table  id="tblGrid" width="100%" runat="server" cellspacing="0" cellpadding="0" border="0">
            <tr>
				<td colspan="10" class="msgheader" bgcolor="#D5CDA4">
				  <asp:Label ID="lblDiaries" runat="server"></asp:Label>
				</td> 
			</tr> 
			<tr>
			    <td colspan="10" class="PadLeft4">
			        <asp:Label ID="lblActionMessage" visible="false" runat="server" Text="<%$ Resources:lblActionMessage %>"></asp:Label>
                     <asp:Label ID="lblMessage" visible="false" runat="server" Text="<%$ Resources:lblMessage %>"></asp:Label>
			    </td> 
			</tr>
			<tr bgcolor="#330099">
			    <td colspan="5" class="headertext2">
                <%--Richa: MITS 30996 Start--%>
				    <asp:Label ID="lblRecords" runat="server" Text="" ></asp:Label>
				<%--Richa: MITS 30996 End--%>
                </td>
				<td colspan="5" align="right" class="headertext2">
                    <table>
                     <tr>
                       <td>
                            <span id="firstSpan"  class="headerlink1">| </span>
				            <asp:LinkButton  ID="btnFirstPage" runat="server" CssClass="headerlink2" Text="<%$ Resources:btnFirstPage %>" onclick="btnFirstPage_Click" ></asp:LinkButton>
                        </td>
                        <td>
                            <span id="Span1"  class="headerlink1"> | </span>
				            <asp:LinkButton  ID="btnPrev" runat="server" CssClass="headerlink2" Text="<%$ Resources:btnPrev %>" onclick="btnPrev_Click"></asp:LinkButton>   
                        </td>
                        <td>
                            <span id="Span2"  class="headerlink1"> | </span>
				            <asp:LinkButton  ID="btnNext" runat="server"  CssClass="headerlink2" Text="<%$ Resources:btnNext %>" onclick="btnNext_Click" ></asp:LinkButton>
                        </td>   
                        <td align="center">
                            <span id="Span3"  class="headerlink1"> | </span>
				            <asp:LinkButton  ID="btnLast" runat="server" CssClass="headerlink2" Text="<%$ Resources:btnLast %>" onclick="btnLast_Click"></asp:LinkButton>   
                        </td>
                     </tr>
                    </table>
				</td> 
			</tr>
			<tr>
			    <td  colspan="10" >
			        <%--<div  class="divScroll" style="border-width:"0">--%>
			            <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%" AllowSorting="true" 
                            DataKeyNames="entry_id" AutoGenerateColumns="False" 
                            GridLines="None" PageSize="5" EnableViewState="true"
                              onrowdatabound="GridView1_RowDataBound" onsorting="GridView1_Sorting" >
                            
                            <%--Richa: MITS 30996 Start--%>
                            <PagerSettings FirstPageText="" LastPageText="" Mode="NextPreviousFirstLast" 
                                Position="Top" NextPageText="" />
                               <%-- Richa: MITS 30996 End--%>
                            <HeaderStyle CssClass="ctrlgroup" />
                           <AlternatingRowStyle CssClass="data2" /> 
                            <Columns>
                                <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id="selectrdo"  name="gvRadio" onclick="javascript:SetSelectDiary('<%# DataBinder.Eval(Container.DataItem, "entry_id") %>');" />
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="<%$ Resources:gvhdrPriority %>" ItemStyle-CssClass="data" HeaderStyle-CssClass ="headerlink2" HeaderStyle-ForeColor="White" SortExpression="priority">
                                    <ItemTemplate>
                                       <%-- <img id="img124" runat="server" src='<%# GetPrioiryUrl(DataBinder.Eval(Container, "DataItem.priority")) %>' />--%>
                                    </ItemTemplate> 
                                    
                                </asp:TemplateField> 
                                <asp:TemplateField    HeaderStyle-CssClass="headerlink1" >
                                    <ItemTemplate>
                                     <%-- <img id="img123" src= '<%# DataBinder.Eval(Container, "DataItem.img")%>' />--%>
                                    </ItemTemplate> 
                                    
                                </asp:TemplateField> 
                               
                                <asp:TemplateField ItemStyle-CssClass="data" HeaderText="<%$ Resources:gvhdrDue %>" ItemStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="White" SortExpression="duedate" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblDueDate" CssClass="data" runat="server" Text=""></asp:Label>
                                    </ItemTemplate> 
                                    
                                </asp:TemplateField> 
                                
                                <asp:TemplateField HeaderText="<%$ Resources:gvhdrTaskName %>" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White" SortExpression="tasksubject">
                                    <ItemTemplate>
                                        <asp:Label ID="lbltaskSubject" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasksubject")%>' style="cursor:pointer" CssClass="LightBlue" ForeColor="#330099" Font-Bold="true" Font-Underline="True"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblRegarding" runat="server" Text="" CssClass="LightBlue"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblGrdNotes" runat="server" Text="" CssClass="LightBlue"></asp:Label>
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="<%$ Resources:gvhdrAttachedRecord %>" HeaderStyle-CssClass="PadLeft4" ItemStyle-CssClass="data" HeaderStyle-ForeColor="White" SortExpression="attachprompt">
                                    <ItemTemplate >
                                        <asp:Label ID="lblAttachRecord" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.attachprompt")%>' style="cursor:pointer" CssClass="LightBlue" ForeColor="#330099" Font-Bold="true" Font-Underline="True"></asp:Label>
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="<%$ Resources:gvhdrClaimStatus %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" >
                                    <ItemTemplate >
                                        <asp:Label ID="lblClaimStatus" runat="server" Text="" ></asp:Label>
                                    </ItemTemplate> 
                                </asp:TemplateField>  
                                 <asp:TemplateField HeaderText="<%$ Resources:gvhdrAssignedUser %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" HeaderStyle-ForeColor="White" SortExpression="assigned_user">
                                    <ItemTemplate >
                                        <asp:Label ID="lblAssignedUser" runat="server" Text="" ></asp:Label>
                                    </ItemTemplate> 
                               </asp:TemplateField> 
                                <asp:TemplateField HeaderText="<%$ Resources:gvhdrAssigningUser %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" HeaderStyle-ForeColor="White" SortExpression="assigning_user">
                                    <ItemTemplate >
                                        <asp:Label ID="lblAssigningUser" runat="server" Text="" ></asp:Label>
                                    </ItemTemplate> 
                               </asp:TemplateField> 
                            </Columns>
                        </asp:GridView>
                    <%--</div>--%>
			    </td>
			</tr>
        </table>
         <table id="tblNoRecords" runat="server" width="100%"  visible="false" cellspacing="0" cellpadding="1" border="0">
        <tr>
			<td colspan="6" align="center" class="PadLeft4"><br /><br /><br /><br /> <asp:Label ID="lblNoDiary" runat="server" Text="<%$ Resources:lblNoDiary %>"></asp:Label><br /><br /><br />&#160;</td>
		</tr>
     </table>
        <%--<table width="100%" cellspacing="0" cellpadding="0" border="0">
             <tr>
                <td colspan="6" class="PadLeft4">
                    <asp:LinkButton CssClass= "LightBlue" runat="server"  Text="Refresh" Font-Underline="true" ID="btnRefresh" onclick="btnRefresh_Click"></asp:LinkButton>
                </td>
            </tr>--%>
            <%--<tr>
             <td colspan="6" align="left" class="PadLeft4">
                <asp:Button runat="server" ID="btnDelete" Text="Delete"  CssClass="button"
                     OnClientClick = "javascript:return ValidateSelectedDiary()" 
                     onclick="btnDelete_Click" Width="41px" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel"  CssClass="button"
                     onclick="btnCancel_Click" Width="45px" />
                <asp:Button runat="server" ID="btnHome" Text="Home" Visible="false" CssClass="button"
                      Width="41px" onclick="btnHome_Click" />
                </td> 
            </tr>--%>
        <%--</table>--%> 
        <input type="hidden" id="isDiarySelected" value="false" /> 
        <input type="hidden" id="hdnEntryId" runat="server" />
    </div>
    </form>
</body>
</html>
