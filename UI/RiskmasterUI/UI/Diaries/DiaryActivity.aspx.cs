﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class DiaryActivity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument userListDoc = null;
            XmlNode diariesNode = null;
            //Rakhel ML Changes
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryActivity.aspx"), "DiaryActivityValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "DiaryActivityValidations", sValidationResources, true);
            //Rakhel ML Changes
            try
            {
                userListDoc = GetActivityUserList();

                diariesNode = userListDoc.SelectSingleNode("ResultMessage/Document/Diaries");

                if (diariesNode != null)
                {
                    PopulateActivityDropDown(diariesNode);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlNode GetInputDocForAcitivity()
        {
            XmlDocument activityDoc = null;
            XmlNode activityNode = null;

            activityDoc = new XmlDocument();
            activityNode = activityDoc.CreateElement("Diary");
            activityDoc.AppendChild(activityNode);
            return activityNode;
        }

        private XmlDocument GetActivityUserList()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.GetActivityCodes";

                inputDocNode = GetInputDocForAcitivity();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private void PopulateActivityDropDown(XmlNode diariesNode)
        {
            XmlNodeList  activtiyNodeList = null;
            activtiyNodeList = diariesNode.SelectNodes("ActivityCodes/Activity");

            lstActivity.Items.Add("");
            foreach (XmlNode activityNode in activtiyNodeList)
            {
                lstActivity.Items.Add(new ListItem(activityNode.InnerText,activityNode.Attributes["codeid"].Value));
            }
        }
    }
}
