﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryDetails.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryDetails" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Diary Details</title>
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />

    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
    
    
    <script type="text/javascript">
        function Validate(btnName)
        {
            if (document.getElementById('txtNotRouteableFlag') != null)
                var RouteableFlag = document.getElementById('txtNotRouteableFlag').value;
            if (document.getElementById('txtNotRollableFlag') != null)
                var RollableFlag = document.getElementById('txtNotRollableFlag').value;
            if(btnName=="btnRoute")
            {
                if(RouteableFlag=="-1")
                {
                    alert(DiaryDetailsValidations.NotRoutablePerm);
                    return false;
                }
            }
            else if (btnName == "btnRoll")
            {
                
                if (RollableFlag == "-1")
                {
                    alert(DiaryDetailsValidations.NotRollablePerm);
                    return false;
                }
            }
            return true;
        }
    </script>
</head>
<body onload="parent.MDIScreenLoaded()">
    <form id="frmData" runat="server">
    <table border="0" width="98%" style="margin-left: 0px">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <tr>
            <td colspan="2" class="ctrlgroup">
                  <asp:Label ID="lblDiaryDetResrc"  runat="server" Text="<%$ Resources:lblDiaryDetResrc %>"></asp:Label>  
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTaskResrc"  runat="server" Text="<%$ Resources:lblTaskResrc %>"></asp:Label> 
            </td>
            <td class="required" width="100%">
                <asp:Label ID="lblTaskName" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="lblCreatedonResrc"  runat="server" Text="<%$ Resources:lblCreatedonResrc %>"></asp:Label> 
            </td>
            <td class="required">
                <asp:Label ID="lblCreatedOn" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="lblDueDateResrc"  runat="server" Text="<%$ Resources:lblDueDateResrc %>"></asp:Label>   
            </td>
            <td class="required">
                <asp:Label ID="lblDueDate" runat="server" Text=""></asp:Label>
            
                <asp:Label ID="lblDueTime" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                  <asp:Label ID="lblWorkActResrc"  runat="server" Text="<%$ Resources:lblWorkActResrc %>"></asp:Label>     
            </td>
            <td class="required">
                <asp:Label ID="lblWorkActivities" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="lblEstTimeResrc"  runat="server" Text="<%$ Resources:lblEstTimeResrc %>"></asp:Label>   
            </td>
            <td class="required">
                <asp:Label ID="lblEstimateTime" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
             <asp:Label ID="lblPriorityResrc"  runat="server" Text="<%$ Resources:lblPriorityResrc %>"></asp:Label>      
            </td>
            <td class="required">
                <img id="imgPriority" runat="server" />
                <asp:Label ID="lblPriority" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="lblNotesResrc"  runat="server" Text="<%$ Resources:lblNotesResrc %>"></asp:Label>  
            </td>
            <td class="required">
                <asp:Label ID="lblNotes" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="lblRegardingResrc"  runat="server" Text="<%$ Resources:lblRegardingResrc %>"></asp:Label>   
            </td>
            <td class="required">
                <asp:Label ID="lblRegarding" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="lblNotRoutableResrc"  runat="server" Text="<%$ Resources:lblNotRoutableResrc %>"></asp:Label>   
            </td>
            <td class="required">
                <asp:Label ID="lblNotRoutable" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="lblNotRollableResrc"  runat="server" Text="<%$ Resources:lblNotRollableResrc %>"></asp:Label>   
            </td>
            <td class="required">
                <asp:Label ID="lblNotRollable" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td bgcolor="#D5CDA4" colspan="2">
                <asp:Label ID="lblDiaryStatusMsg" runat="server" Text="<%$ Resources:lblDiaryStatusMsg %>"></asp:Label>
                   <asp:Label ID="lblStatusMsg" runat="server" Text="<%$ Resources:lblStatusMsg %>"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblCompleteDiary" runat="server">
                    <tr>
                        <td class="datatd">
                          <asp:Label ID="lblComponResrc"  runat="server" Text="<%$ Resources:lblComponResrc %>"></asp:Label>     
                        </td>
                        <td class="datatd">
                            <asp:Label ID="lblCompletedOn" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd">
                         <asp:Label ID="lblCompResResrc"  runat="server" Text="<%$ Resources:lblCompResResrc %>"></asp:Label>    
                        </td>
                        <td class="datatd">
                            <asp:Label ID="lblCompletionResponse" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd">
                            <asp:Label ID="lblCompUserResrc"  runat="server" Text="<%$ Resources:lblCompUserResrc %>"></asp:Label> 
                        </td>
                        <td class="datatd">
                            <asp:Label ID="lblCompletedByUser" runat="server" Text=""></asp:Label>
                        </td>
                        </tr>
                        <tr>
                            <td class="datatd">
                            <asp:Label ID="lblAssignUserResrc"  runat="server" Text="<%$ Resources:lblAssignUserResrc %>"></asp:Label> 
                        </td>
                         <td class="datatd">
                            <asp:Label ID="lblAssigningUser" runat="server" Text=""></asp:Label>
                        </td>
                        </tr>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblTeTracked" runat="server">
                    <tr>
                        <td bgcolor="#D5CDA4" colspan="2">
                            <asp:Label ID="lblTimeExpResrc"  runat="server" Text="<%$ Resources:lblTimeExpResrc %>"></asp:Label>  
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd">
                          <asp:Label ID="lblStartTimeResrc"  runat="server" Text="<%$ Resources:lblStartTimeResrc %>"></asp:Label>   
                        </td>
                        <td class="datatd">
                            <asp:Label ID="lblTeStartTime" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd">
                          <asp:Label ID="lblEndTimeResrc"  runat="server" Text="<%$ Resources:lblEndTimeResrc %>"></asp:Label>    
                        </td>
                        <td class="datatd">
                            <asp:Label ID="lblTeEndTime" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd">
                           <asp:Label ID="lblTotTimeResrc"  runat="server" Text="<%$ Resources:lblTotTimeResrc %>"></asp:Label>   
                        </td>
                        <td class="datatd">
                            <asp:Label ID="lblTotalTimeSpent" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="datatd">
                          <asp:Label ID="lblExpResrc"  runat="server" Text="<%$ Resources:lblExpResrc %>"></asp:Label>  
                        </td>
                        <td class="datatd">
                            <asp:Label ID="lblTeExpenses" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <asp:Button ID="btnEdit" runat="server" Text="<%$ Resources:btnEdit %>" CssClass="button" OnClick="btnEdit_Click" />
                <asp:Button ID="btnComplete" runat="server" Text="<%$ Resources:btnComplete %>" CssClass="button" OnClick="btnComplete_Click" />
                <%--igupta3 Jira 439--%>
                <asp:Button ID="btnRoute" runat="server" Text=" <%$ Resources:btnRoute %>  " CssClass="button" OnClientClick="return Validate('btnRoute');" OnClick="btnRoute_Click" />
                <asp:Button ID="btnRoll" runat="server" Text=" <%$ Resources:btnRoll %>  " CssClass="button" OnClick="btnRoll_Click" OnClientClick="return Validate('btnRoll');"/>
                <asp:Button ID="btnCancel" runat="server" Text=" <%$ Resources:btnCancel %>  " CssClass="button" OnClick="btnCancel_Click" />
                <asp:Button ID="btnClose" runat="server" Text="  <%$ Resources:btnClose %> " CssClass="button" OnClick="btnClose_Click" />
                <asp:Button ID="btnPrint" runat="Server" Text=  "<%$ Resources:btnPrint %>" CssClass="button" OnClick="btnPrint_Click" />
                <asp:TextBox ID="txtNotRouteableFlag" runat="server" style="display:none"></asp:TextBox>
                <asp:TextBox ID="txtNotRollableFlag" runat="server" style="display:none"></asp:TextBox>
              <%--  asingh263 mits 34874--%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
