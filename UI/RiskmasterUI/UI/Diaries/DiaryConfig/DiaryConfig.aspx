﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryConfig.aspx.cs" Inherits="Riskmaster.UI.UI.Diaries.DiaryConfig.DiaryConfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Diary Header Configuration</title>
    <script src="../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../Scripts/ExecutiveSummary.js" type="text/javascript"></script>
    <script language="Javascript">
        function setDefaultValue(name, value) 
        {
            document.getElementById(name).value = value;
            return false;
        }
    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <input type="hidden" value="" id="hiddenSelectedUser" />
    <input type="hidden" value="DiaryConfig" id="hTabName" />
    <div class="msgheader" id="formtitle">
        <asp:Label ID="lblDiaryHeader" runat="server" Text="<%$ Resources:lblDiaryHeader %>"></asp:Label>        
    </div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0">
                    <tr>
                        <td class="Selected">
                            <a class="Selected" name="DiaryConfig"><span style="text-decoration: none">
                            <asp:Label ID="lblDiaryConfig" runat="server" Text="<%$ Resources:lblDiaryConfig %>"></asp:Label>
                            </span>
                            </a>
                        </td>
                        <td style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td valign="top">
                <div style="position: relative; left: 0; top: 0; width: 950px; height: 280px; overflow: auto;valign:top" class="singletopborder">
                    <table border="0" cellspacing="0" celpadding="0" width="100%" valign="top" align="center">
                        <tr valign="top">
                            <td valign="top">
                                <table border="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkPriority" runat="server" checked ="checked" />
                                            <asp:Label ID="lblPriority" runat="server" Text="<%$ Resources:lblPriority %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Priority" runat="server" id="txtPriority" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPriority" runat="server" value="<%$ Resources:btnPriority %>" tabindex="3" onclick="return setDefaultValue('txtPriority','Priority');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkDue" checked ="checked"/>
                                            <asp:Label ID="lblDue" runat="server" Text="<%$ Resources:lblDue %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" id="txtDue" runat="server"  value="Due" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnDue" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtDue','Due');" class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkText" checked ="checked" />
                                            <asp:Label ID="lblText" runat="server" Text="<%$ Resources:lblText %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Task Name" id="txtText" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnText" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtText','Task Name');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkAttachedRecord" checked ="checked" />
                                            <asp:Label ID="lblAttachRecord" runat="server" Text="<%$ Resources:lblAttachRecord %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Attached Record" runat="server" id="txtAttachedRecord" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAttachedRecord" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtAttachedRecord','Attached Record');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkWorkActivity" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblWorkAct" runat="server" Text="<%$ Resources:lblWorkAct %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Work Activity" runat="server" id="txtWorkActivity" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnWorkActivity" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtWorkActivity','Work Activity');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkClaimant" onclick="CheckboxClick()" runat="server" checked ="checked" />
                                            <asp:Label ID="lblClaimant" runat="server" Text="<%$ Resources:lblClaimant %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Claimant" id="txtClaimant" runat="server"/>
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnClaimant" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtClaimant','Claimant');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkDepartment" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblDept" runat="server" Text="<%$ Resources:lblDept %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Department" id="txtDepartment" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnDepartment" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtDepartment','Department');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkClaimStatus" runat="server" checked ="checked" />
                                            <asp:Label ID="lblClaimStatus" runat="server" Text="<%$ Resources:lblClaimStatus %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Claim Status" id="txtClaimStatus" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnClaimStatus" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtClaimStatus','Claim Status');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkAssignedUser" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblTo" runat="server" Text="<%$ Resources:lblTo %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="To" id="txtAssignedUser" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAssignedUser" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtAssignedUser','To');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkAssigningUser" runat="server" checked ="checked" />
                                            <asp:Label ID="lblFrom" runat="server" Text="<%$ Resources:lblFrom %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="From" id="txtAssigningUser" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAssigningUser" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtAssigningUser','From');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                <asp:Label ID="lblMsg1" runat="server" Text="<%$ Resources:lblMsg1 %>"></asp:Label>
                                <br>
                                <asp:Label ID="lblMsg2" runat="server" Text="<%$ Resources:lblMsg2 %>"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" celpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveConfiguration" runat="server" Text="<%$ Resources:btnSaveConfiguration %>" 
                                CssClass="button" onclick="btnSaveConfiguration_Click" />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnSelectAll %>" id="btnSelectAll" class="button"
                                onclick="return SelectAll();; " />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnUnselectAll %>" id="btnUnselectAll" class="button"
                                onclick="return UnselectAll();; " />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
