﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.Diaries.DiaryConfig
{
    public partial class DiaryConfig : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDiaryConfigDetails();
            }
        }

        protected void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            SetDiaryConfigDetails();
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>WPAAdaptor.GetDiaryConfig</Function>
              </Call>
              <Document>
                 <DiaryConfig>
                    <IsAdminUser>False</IsAdminUser> 
                    <UserId /> 
                  </DiaryConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement SetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>WPAAdaptor.SetDiaryConfig</Function>
              </Call>
              <Document>
                 <DiaryConfig>
                          <Priority></Priority> 
                          <Text /> 
                          <WorkActivity /> 
                          <Department></Department> 
                          <Due></Due> 
                          <AttachedRecord></AttachedRecord> 
                          <Claimant></Claimant> 
                          <ClaimStatus></ClaimStatus> 
                          <AssignedUser></AssignedUser>
                          <AssigningUser></AssigningUser>
                          <PriorityHeader></PriorityHeader> 
                          <TextHeader></TextHeader> 
                          <WorkActivityHeader></WorkActivityHeader> 
                          <DepartmentHeader></DepartmentHeader> 
                          <DueHeader></DueHeader> 
                          <AttachedRecordHeader></AttachedRecordHeader> 
                          <ClaimantHeader></ClaimantHeader> 
                          <ClaimStatusHeader></ClaimStatusHeader> 
                          <AssignedUserHeader></AssignedUserHeader> 
                          <AssigningUserHeader></AssigningUserHeader> 
                    </DiaryConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }

        private XmlDocument GetDairyConfigDetails()
        {
            XElement messageElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = GetConfigMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument SetDiaryConfigDetails()
        {
            XElement messageElement = null;
            XElement tempElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = SetConfigMessageTemplate();

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/Priority");
                if (chkPriority.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/Text");
                if (chkText.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/WorkActivity");
                if (chkWorkActivity.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/Department");
                if (chkDepartment.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/Due");
                if (chkDue.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/AttachedRecord");
                if (chkAttachedRecord.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/Claimant");
                if (chkClaimant.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/ClaimStatus");
                if (chkClaimStatus.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/PriorityHeader");
                tempElement.Value = txtPriority.Value;
                
                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/TextHeader");
                tempElement.Value = txtText.Value;
                
                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/WorkActivityHeader");
                tempElement.Value = txtWorkActivity.Value;

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/DepartmentHeader");
                tempElement.Value = txtDepartment.Value;

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/DueHeader");
                tempElement.Value =txtDue.Value;

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/AttachedRecordHeader");
                tempElement.Value = txtAttachedRecord.Value;

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/ClaimantHeader");
                tempElement.Value = txtClaimant.Value; 

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/ClaimStatusHeader");
                tempElement.Value = txtClaimStatus.Value;
                //Added by Amitosh for Diary UI Changes
                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/AssignedUser");
                if (chkAssignedUser.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/AssigningUser");
                if (chkAssigningUser.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/AssignedUserHeader");
                tempElement.Value = txtAssignedUser.Value;

                tempElement = messageElement.XPathSelectElement("./Document/DiaryConfig/AssigningUserHeader");
                tempElement.Value = txtAssigningUser.Value;

                //End Amitosh
                
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private void LoadDiaryConfigDetails()
        {
            XmlDocument resultDoc = null;
            XmlNode diaryConfigNode = null;

           resultDoc = GetDairyConfigDetails(); 

            if (resultDoc != null)
            {
                //set obtained values of Config
                diaryConfigNode = resultDoc.SelectSingleNode("//Document/DiaryConfig");

                if (diaryConfigNode != null)
                {
                    if (diaryConfigNode.SelectSingleNode("Priority").InnerText == "True")
                    {
                        chkPriority.Checked = true;
                    }
                    else
                    {
                        chkPriority.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("Text").InnerText == "True")
                    {
                        chkText.Checked = true;
                    }
                    else
                    {
                        chkText.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("WorkActivity").InnerText == "True")
                    {
                        chkWorkActivity.Checked = true;
                    }
                    else
                    {
                        chkWorkActivity.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("Department").InnerText == "True")
                    {
                        chkDepartment.Checked = true;
                    }
                    else
                    {
                        chkDepartment.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("Due").InnerText == "True")
                    {
                        chkDue.Checked = true;
                    }
                    else
                    {
                        chkDue.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("AttachedRecord").InnerText == "True")
                    {
                        chkAttachedRecord.Checked = true;
                    }
                    else
                    {
                        chkAttachedRecord.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("Claimant").InnerText == "True")
                    {
                        chkClaimant.Checked = true;
                    }
                    else
                    {
                        chkClaimant.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("ClaimStatus").InnerText == "True")
                    {
                        chkClaimStatus.Checked = true;
                    }
                    else
                    {
                        chkClaimStatus.Checked = false;
                    }

                    txtPriority.Value = diaryConfigNode.SelectSingleNode("PriorityHeader").InnerText;
                    txtText.Value = diaryConfigNode.SelectSingleNode("TextHeader").InnerText;
                    txtWorkActivity.Value = diaryConfigNode.SelectSingleNode("WorkActivityHeader").InnerText;
                    txtDepartment.Value = diaryConfigNode.SelectSingleNode("DepartmentHeader").InnerText;
                    txtDue.Value = diaryConfigNode.SelectSingleNode("DueHeader").InnerText;
                    txtAttachedRecord.Value = diaryConfigNode.SelectSingleNode("AttachedRecordHeader").InnerText;
                    txtClaimant.Value = diaryConfigNode.SelectSingleNode("ClaimantHeader").InnerText;
                    txtClaimStatus.Value = diaryConfigNode.SelectSingleNode("ClaimStatusHeader").InnerText;

                    //Added by Amitosh for Diary UI Changes
                    if (diaryConfigNode.SelectSingleNode("AssignedUser").InnerText == "True")
                    {
                        chkAssignedUser.Checked = true;
                    }
                    else
                    {
                        chkAssignedUser.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("AssigningUser").InnerText == "True")
                    {
                        chkAssigningUser.Checked = true;
                    }
                    else
                    {
                        chkAssigningUser.Checked = false;
                    }

                    txtAssignedUser.Value = diaryConfigNode.SelectSingleNode("AssignedUserHeader").InnerText;
                    txtAssigningUser.Value = diaryConfigNode.SelectSingleNode("AssigningUserHeader").InnerText;

                    //End Amitosh
                }

            }
        }

    }
}
