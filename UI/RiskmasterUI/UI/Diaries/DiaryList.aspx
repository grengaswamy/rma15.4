﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryList.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryList" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DiaryList</title>
    <style type="text/css">
        .SelectedItem {
            background: none repeat scroll 0 0 #6699FF !important;
        }

        .SuppColumn {
            word-break: break-all;
        }

        .radGrid {
            word-wrap: break-word;
        }

    </style>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
   	<script src="../../Scripts/jquery/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function SetSelectDiary() {
            document.getElementById("isDiarySelected").value = 'true';
        }
        function ValidateSelectedDiary(sFieldName) {
            if (document.getElementById("isDiarySelected").value == 'false') {
                alert(DiaryListValidations.SelectDiary);
                return false;
            }
            if (sFieldName == 'Route' || sFieldName == 'Roll' || sFieldName == 'Edit') {
                if (document.getElementById("isMultipleDiarySelected") != null) {
                    if (document.getElementById("isMultipleDiarySelected").value == 'true') {
                        alert(DiaryListValidations.MultipleDiarySelect);  //kkaur8 ML Changes
                        return false;
                    }
                }
            }
            if (sFieldName == 'Route') {
                if (document.getElementById("hdnNotRoutable") != null) {
                    if (document.getElementById("hdnNotRoutable").value == '-1') {
                        alert(DiaryListValidations.NotRoutablePerm);
                        return false;
                    }
                }
            }
            if (sFieldName == 'Roll') {
                if (document.getElementById("hdnNotRollable") != null) {
                    if (document.getElementById("hdnNotRollable").value == '-1') {
                        alert(DiaryListValidations.NotRollablePerm);
                        return false;
                    }
                }
            }
            if (sFieldName == 'Void' || sFieldName == 'Edit' || sFieldName == 'Roll' || sFieldName == 'Route' || sFieldName == 'Complete') {
                var otherDiariesEditPermission = document.getElementById("hdnOtherDiariesEditPerm");
                if ((otherDiariesEditPermission && otherDiariesEditPermission.value == "1")) {
                    if ((document.getElementById("hdnUser").value == document.getElementById("hdnMainUser").value
                        && (document.getElementById("hdnUser").value != document.getElementById("hdnMainUserGroup").value))) {
                        return true;
                    }
                    else if 
                        ((document.getElementById("hdnUser").value != document.getElementById("hdnMainUser").value
                        && (document.getElementById("hdnUser").value == document.getElementById("hdnMainUserGroup").value)))
                        return true;
                    else {
                        alert(DiaryListValidations.OtherDiariesEditPermission);
                        return false;
                    }
                }
            }
            //End: 10/12/2012; MITS 29791 BRD 5.1.18
            return true;
        }

        function DisplayDiaryDetails(entryId, assigningUser, attachPrompt, creationDate, assignedUser, isPeekDiary, attachTable, attachRecordId, notRoute, notRoll, txtAssignedTo) {
            var permission = "0";
            var otherDiariesEditPermission = document.getElementById("hdnOtherDiariesEditPerm");
            if ((otherDiariesEditPermission && otherDiariesEditPermission.value == "1")) {
                if ((txtAssignedTo == document.getElementById("hdnMainUser").value
                      && (txtAssignedTo != document.getElementById("hdnMainUserGroup").value))) {
                    permission = "0";
                }
                else if ((txtAssignedTo != document.getElementById("hdnMainUser").value
                    && (txtAssignedTo == document.getElementById("hdnMainUserGroup").value)))
                    permission = "0";
                else {
                    permission = "1";
                }
            }
            //window.location = "/RiskmasterUI/UI/FDM/DiaryDetails.aspx?entryid=" + entryId + "&assigninguser" + assigningUser + "&attachprompt=" + attachPrompt + "&creationdate=" + creationDate + "&assigneduser=" + assignedUser + "&ispeekdiary=" + isPeekDiary + "&attachtable=" + attachTable + "&attachrecordid=" + attachRecordId + "&Routeableflag=" + notRoute + "&Rollableflag=" + notRoll; //igupta3 JIRA-439
            window.location = "/RiskmasterUI/UI/FDM/DiaryDetails.aspx?entryid=" + entryId + "&recordID=" + entryId + "&assigninguser=" + assigningUser + "&attachprompt=" + attachPrompt + "&creationdate=" + creationDate + "&assignedusername=" + assignedUser + "&ispeekdiary=" + isPeekDiary + "&attachtable=" + attachTable + "&attachrecord=" + attachRecordId + "&Routeableflag=" + notRoute + "&Rollableflag=" + notRoll + "&CalledBy=diarydetails" + "&perm=" + permission;

        }
        function CheckMDIScreenLoaded() {
            try {
                parent.MDIScreenLoaded();
            }
            catch (e) { }
        }
        function OpenAttachedRecordAsPopUp(attachedScreen, attachedRecordId) {
            var wnd = null;
            if (attachedScreen == "taskmanagerjob") {
                wnd = window.open("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMStatusDetails.aspx?JobId=" + attachedRecordId, "AttachedRecord",
                      "width=750,height=500,top=" + (screen.availHeight - 500) / 2 + ",left=" + (screen.availWidth - 750) / 2 + ",resizable=yes,scrollbars=yes");
            }
            else {
                wnd = window.open("/RiskmasterUI/UI/FDM/" + attachedScreen + ".aspx?recordID=" + attachedRecordId, "AttachedRecord",
			    "width=900,height=650,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
            }
        }
        function OnCreateDiaryClick(sAttachTableName, sAttachRecordId, sAttachedRecordNumber) {
            var sAttRecName = '';
            var sAttSubject = '';
            var sTableName = '';

            if (sAttachRecordId == '' || sAttachTableName == '') {
                window.open("/RiskmasterUI/UI/FDM/creatediary.aspx?RtnClaimScreen=true&CalledBy=creatediary", "CreateDiary", "width=900,height=600,top=" + (screen.availHeight - 600) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
                return false;
            }

            sTableName = sAttachTableName;
            sAttRecName = sAttachedRecordNumber;

            if (sAttachTableName == 'policyenhal' || sAttachTableName == 'policyenhgl' || sAttachTableName == 'policyenhpc' || sAttachTableName == 'policyenhwc') {
                sTableName = 'POLICY_ENH';
                switch (sAttachTableName) {
                    case 'policyenhal':
                        sAttSubject = "POLICY MANAGEMENT (AL)";
                        break;
                    case 'policyenhgl':
                        sAttSubject = "POLICY MANAGEMENT (GL)";
                        break;
                    case 'policyenhpc':
                        sAttSubject = "POLICY MANAGEMENT (PC)";
                        break;
                    case 'policyenhwc':
                        sAttSubject = "POLICY MANAGEMENT (WC)";
                        break;
                }
            }
            sTableName = sTableName.toUpperCase();
            window.open("/RiskmasterUI/UI/FDM/attachdiary.aspx?AttachTable=" + sTableName
			    + "&AttachRecord=" + sAttachRecordId + "&AttachRecordName=" + sAttRecName + "&AttachSubject=" + sAttSubject, "AttachDiary",
		        "width=600,height=600,top=" + (screen.availHeight - 600) / 2 + ",left=" + (screen.availWidth - 600) / 2 + ",resizable=yes,scrollbars=yes");
            return false;
        }
        function btnExport_click() {
            tableToExcel();
            return false;
        }
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
              , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
              , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
              , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function () {
                var headerWidth = $('#lstDiary_ctl00_Header colgroup').clone().html().replace(/<A[^>]*>|<\/A>/g, "").replace(/<a[^>]*>|<\/a>/g, "").replace(/<input[^>]*>|<\/input>/gi, "");
                var header = $('#lstDiary_ctl00_Header tr').clone().html().replace(/<A[^>]*>|<\/A>/g, "").replace(/<a[^>]*>|<\/a>/g, "").replace(/<input[^>]*>|<\/input>/gi, "");
                var tableHTML = $('#lstDiary_ctl00').clone();
                tableHTML.find('img').map(function () {
                    $div = $('<div>').html($(this).attr('title'));
                    $(this).replaceWith($div);
                });
                var table = headerWidth
                        + "<thead>" + header + "</thead>"
                        + tableHTML.html().replace(/<a[^>]*>|<\/a>/g, "").replace(/<input[^>]*>|<\/input>/gi, "");
                
                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                {
                    gridArea.document.open("txt/html", "replace");
                    gridArea.document.write("<table border='1px'>" + table.replace(/<table[^>]*>/gi, "<table border='1px'") + "</table>");
                    gridArea.document.close();
                    gridArea.focus();
                    sa = gridArea.document.execCommand("SaveAs", true, "Diary List WorkSheet.xls");
                }
                else {
                    var ctx = {
                        worksheet: 'Diary List WorkSheet', table: table
                    }
                    window.location.href = uri + base64(format(template, ctx))
                }
            }
        })()
</script>
</head>
<body onload="CheckMDIScreenLoaded()">
    <form id="frmDiaryList" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
            <div class="toolBarButton" runat="server" id="div_home" xmlns="">
                <asp:ImageButton ID="btnHome" ImageUrl="~/Images/tb_previous_active.png" class="bold"
                    ToolTip="<%$ Resources:ttHome %>" runat="server" OnClick="btnHome_Click" style="display:None" />
            </div>
            <div class="toolBarButton" runat="server" id="div_complete" xmlns="">
                <asp:ImageButton ID="cmdComplete" ImageUrl="~/Images/tb_diarycomplete_active.png"
                    class="bold" ToolTip="<%$ Resources:ttComplete %>" runat="server" OnClientClick="return ValidateSelectedDiary('Complete');"
                    OnClick="cmdComplete_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_edit" xmlns="">
                <asp:ImageButton ID="cmdEdit" ImageUrl="~/Images/tb_diaryedit_active.png" class="bold"
                    ToolTip="<%$ Resources:ttEdit %>" runat="server" OnClientClick="return ValidateSelectedDiary('Edit');"
                    OnClick="cmdEdit_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_route" xmlns="">
                <asp:ImageButton ID="cmdRoute" ImageUrl="~/Images/tb_diaryroute_active.png" class="bold"
                    ToolTip="<%$ Resources:ttRoute %>" runat="server" OnClientClick="return ValidateSelectedDiary('Route');"
                    OnClick="cmdRoute_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_roll" xmlns="">
                <asp:ImageButton ID="cmdRoll" ImageUrl="~/Images/tb_diaryroll_active.png" class="bold"
                    ToolTip="<%$ Resources:ttRoll %>" runat="server" OnClientClick="return ValidateSelectedDiary('Roll');"
                    OnClick="cmdRoll_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_create" xmlns="">
                <asp:ImageButton ID="cmdCreate" ImageUrl="~/Images/tb_diarycreate_active.png" class="bold"
                    ToolTip="<%$ Resources:ttCreate %>" runat="server" OnClick="cmdCreate_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_diarylist" xmlns="">
                <asp:ImageButton ID="cmdDiaryList" ImageUrl="~/Images/tb_diary_active.png" class="bold"
                    ToolTip="<%$ Resources:ttDiaryList %>" runat="server" OnClick="cmdDiaryList_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_diarycalendar" xmlns="">
                <asp:ImageButton ID="cmdDiaryCalendar" ImageUrl="~/Images/tb_diarycalendar_active.png"
                    class="bold" ToolTip="<%$ Resources:ttDiaryCalendar %>" runat="server" OnClick="cmdDiaryCalendar_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_peek" xmlns="">
                <asp:ImageButton ID="cmdPeek" ImageUrl="~/Images/tb_diarypeek_active.png" class="bold"
                    ToolTip="<%$ Resources:ttPeek %>" runat="server" OnClick="cmdPeek_Click" />
            </div>
            <div class="toolBarButton" runat="server" id="div_void" xmlns="">
                <asp:ImageButton ID="cmdVoid" ImageUrl="~/Images/tb_diaryhistoryvoid_active.png"
                    class="bold" ToolTip="<%$ Resources:ttVoid %>" runat="server" OnClick="cmdVoid_Click" OnClientClick="return ValidateSelectedDiary('Void');" />
            </div>
            <div class="toolBarButton" runat="server" id="div_history" xmlns="">
                <asp:ImageButton ID="cmdHistory" ImageUrl="~/Images/tb_diaryhistory_active.png" class="bold"
                    ToolTip="<%$ Resources:ttHistory %>" runat="server" OnClick="cmdHistory_Click" />
            </div>
        <div class="toolBarButton" runat="server" id="div1" xmlns="">
            <asp:ImageButton ID="cmdExporttoExcel" ImageUrl="~/Images/tb_exportexcel_active.png" class="bold" OnClientClick="return btnExport_click();"
                ToolTip="<%$ Resources:ttExport %>" runat="server"/>
            <iframe id="gridArea" style="display:none"></iframe>
        </div>
            <asp:HiddenField ID="hdnEntry_id" runat="server" Value="" />
            <asp:HiddenField ID="hdnAssigningUser" runat="server" Value="" />
            <asp:HiddenField ID="hdnAttachPrompt" runat="server" Value="" />
            <asp:HiddenField ID="hdnCreationDate" runat="server" Value="" />
            <asp:HiddenField ID="hdnCompletionDate" runat="server" Value="" />
            <asp:HiddenField ID="hdnTaskName" runat="server" Value="" />
            <asp:HiddenField ID="hdnNotRoutable" runat="server" Value="" />
            <asp:HiddenField ID="hdnNotRollable" runat="server" Value="" />
        <asp:HiddenField ID="hdnMainUserGroup" runat="server" Value="" />
        <asp:HiddenField ID="hdnMainUser" runat="server" Value="" />
        <asp:HiddenField ID="hdnUser" runat="server" Value="" />
        <asp:HiddenField ID="hdnOtherDiariesEditPerm" runat="server" Value="0" />
            <input type="hidden" id="isDiarySelected" value="false" />
            <input type="hidden" id="isMultipleDiarySelected" value="false" />
            <asp:HiddenField ID="hdnGroupAssigned" runat="server" Value="" />
        </div>
        <div>
            <asp:CheckBox runat="server" ID="chkActiveDiaryChecked" AutoPostBack="true" Checked="false"
                OnCheckedChanged="chkActiveDiaryChecked_CheckedChanged" />
            <asp:Label ID="lblActiveDiary" runat="server" Text="<%$ Resources:lblActiveDiaryResrc %>"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:LinkButton CssClass="LightBlue" runat="server" Text="<%$ Resources:btnRefresh %>" Font-Underline="true"
            ID="btnRefresh" OnClick="btnRefresh_Click"></asp:LinkButton>
            <asp:CheckBox runat="server" ID="chkShownotes" Checked="true" AutoPostBack="true"
                OnCheckedChanged="chkShownotes_CheckedChanged" />
            <asp:Label ID="lblShowNotes" runat="server" Text="<%$ Resources:lblShowNotesResrc %>"></asp:Label>
            <asp:CheckBox runat="server" ID="chkShowregarding" Checked="true" AutoPostBack="true"
                OnCheckedChanged="chkShowregarding_CheckedChanged" />
            <asp:Label ID="lblShowRegdResrc" runat="server" Text="<%$ Resources:lblShowRegdResrc %>"></asp:Label>
            <br />
            <asp:CheckBox runat="server" ID="chkShowActiveDiariesForRecordOnly" Checked="true"
                AutoPostBack="true" Text="<%$ Resources:chkShowActiveDiariesForRecordOnly %>" OnCheckedChanged="chkShowActiveDiariesForRecordOnly_CheckedChanged" />
            &nbsp;<asp:CheckBox runat="server" ID="chkShowAllDiariesRelatedToClaim" Checked="false"
                AutoPostBack="true" Text="<%$ Resources:chkShowAllDiariesRelatedToClaim %>" OnCheckedChanged="chkShowAllDiariesRelatedToClaim_CheckedChanged" />
        </div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td colspan="10" class="PadLeft4">
                    <asp:Label ID="lblMsg1" Visible="false" runat="server" Text="<%$ Resources:lblMsg1 %>"></asp:Label>
                    <asp:Label ID="lblMsg2" Visible="false" runat="server" Text="<%$ Resources:lblMsg2 %>"></asp:Label>
                </td>
            </tr>
        </table>
        <div id="divGrid" runat="server">
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td colspan="10" class="msgheader" bgcolor="#D5CDA4">
                        <asp:Label ID="lblUserDiariesDisplay" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr bgcolor="white">
                    <td colspan="5" class="headertext2"></td>
                </tr>
                <tr>
                    <td colspan="10">
                    </td>
                </tr>
            </table>
            <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
            <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
            <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
                <script type="text/javascript">
                    var selected = {};
                    function OnRowClick(sender, e) {
                        document.getElementById("isDiarySelected").value = 'true';
                        var x;
                        var iSelectdItemsCnt = 0;
                        var sEntryIds = "";
                        var iEntryId = e.getDataKeyValue("entry_id");
                        var masterTable = sender.get_masterTableView();
                        var selectedRows;
                        var row;
                        var labelRoute;
                        var labelRoll;
                        var sTaskNames = "";
                        var sNotRoute = "";
                        var sNotRoll = "";

                        if (!selected[iEntryId]) {
                            selected[iEntryId] = true;
                        }
                        for (x in selected) {
                            if (selected[x]) {
                                iSelectdItemsCnt = iSelectdItemsCnt + 1;
                                if (sEntryIds == "") {
                                    sEntryIds = x;
                                }
                                else {
                                    sEntryIds = sEntryIds + "," + x;
                                }
                            }
                        }
                        if (masterTable != null) {
                            selectedRows = masterTable.get_selectedItems();
                            for (i = 0; i < selectedRows.length; i++) {
                                row = selectedRows[0];
                                if (row != null) {
                                    labelRoute = row.get_element().getAttribute("txtNotRoutable");
                                    labelRoll = row.get_element().getAttribute("txtNotRollable");
                                    if (labelRoute != null) {


                                        if (sNotRoute == "") {
                                            sNotRoute = labelRoute;
                                        }
                                        else {
                                            sNotRoute = sNotRoute + ", " + labelRoute;
                                        }

                                    }
                                    if (labelRoll != null) {

                                        if (sNotRoll == "") {
                                            sNotRoll = labelRoll;
                                        }
                                        else {
                                            sNotRoll = sNotRoll + ", " + labelRoll;
                                        }

                                    }

                                    if (document.getElementById("hdnGroupAssigned") != null) {
                                        document.getElementById("hdnGroupAssigned").value = row.get_element().getAttribute("grpflag");
                                    }
                                }
                            } //end for
                            if (document.getElementById("hdnNotRoutable") != null) {

                                document.getElementById("hdnNotRoutable").value = sNotRoute;
                            }
                            if (document.getElementById("hdnNotRollable") != null) {

                                document.getElementById("hdnNotRollable").value = sNotRoll;
                            }
                        }
                        if (iSelectdItemsCnt > 1) {
                            if (document.getElementById("isMultipleDiarySelected") != null) {
                                document.getElementById("isMultipleDiarySelected").value = 'true';
                            }
                            if (document.getElementById("hdnTaskName") != null) {
                                document.getElementById("hdnTaskName").value = "";
                            }
                        }
                        else {
                            if (document.getElementById("isMultipleDiarySelected") != null) {
                                document.getElementById("isMultipleDiarySelected").value = 'false';
                            }
                        }

                        if (document.getElementById("hdnEntry_id") != null) {

                            document.getElementById("hdnEntry_id").value = sEntryIds;
                        }
                        return false;
                    }
                    function OnRowDeselect(sender, e) {

                        var x;
                        var iSelectdItemsCnt = 0;
                        var iEntryId = e.getDataKeyValue("entry_id");
                        var sEntryIds = "";
                        var masterTable = sender.get_masterTableView();
                        var selectedRows;
                        var row;
                        var labelRoute;
                        var labelRoll;
                        var sTaskNames = "";
                        var sNotRoute = "";
                        var sNotRoll = "";;

                        if (iEntryId != null) {
                            if (selected[iEntryId]) {
                                selected[iEntryId] = null;
                            }
                        }
                        for (x in selected) {
                            if (selected[x]) {
                                iSelectdItemsCnt = iSelectdItemsCnt + 1;
                                if (sEntryIds == "") {
                                    sEntryIds = x;
                                }
                                else {
                                    sEntryIds = sEntryIds + "," + x;
                                }
                            }
                        }
                        if (iSelectdItemsCnt > 1) {
                            if (document.getElementById("isMultipleDiarySelected") != null) {
                                document.getElementById("isMultipleDiarySelected").value = 'true';
                            }
                            if (document.getElementById("hdnTaskName") != null) {
                                document.getElementById("hdnTaskName").value = "";
                            }
                        }
                        else if (iSelectdItemsCnt == 1) {
                            if (document.getElementById("isMultipleDiarySelected") != null) {
                                document.getElementById("isMultipleDiarySelected").value = 'false';
                            }
                        }
                        else {
                            if (document.getElementById("isDiarySelected") != null) {
                                document.getElementById("isDiarySelected").value = 'false';
                            }
                            if (document.getElementById("isMultipleDiarySelected") != null) {
                                document.getElementById("isMultipleDiarySelected").value = 'false';
                            }
                        }
                        if (document.getElementById("hdnEntry_id") != null) {

                            document.getElementById("hdnEntry_id").value = sEntryIds;
                        }

                        if (masterTable != null) {
                            selectedRows = masterTable.get_selectedItems();
                            for (i = 0; i < selectedRows.length; i++) {
                                row = selectedRows[i];
                                if (row != null) {
                                    labelRoute = row.get_element().getAttribute("txtNotRoutable");
                                    labelRoll = row.get_element().getAttribute("txtNotRollable");
                                    if (labelRoute != null) {


                                        if (sNotRoute == "") {
                                            sNotRoute = labelRoute;
                                        }
                                        else {
                                            sNotRoute = sNotRoute + ", " + labelRoute;
                                        }

                                    }
                                    if (labelRoll != null) {

                                        if (sNotRoll == "") {
                                            sNotRoll = labelRoll;
                                        }
                                        else {
                                            sNotRoll = sNotRoll + ", " + labelRoll;
                                        }

                                    }

                                    if (document.getElementById("hdnGroupAssigned") != null) {
                                        document.getElementById("hdnGroupAssigned").value = row.get_element().getAttribute("grpflag");
                                    }
                                }
                            } //end for
                            if (document.getElementById("hdnNotRoutable") != null) {

                                document.getElementById("hdnNotRoutable").value = sNotRoute;
                            }
                            if (document.getElementById("hdnNotRollable") != null) {

                                document.getElementById("hdnNotRollable").value = sNotRoll;
                            }
                        }
                        //Comment-end;
                        return false;
                    }
                    function RadGrid1_RowCreated(sender, args) {
                        return false;
                    }
                    function GridCreated(sender, eventArgs) {
                        var x;
                        for (x in selected) {
                            selected[x] = null;
                        }
                        document.getElementById("isDiarySelected").value = 'false';
                        return false;
                    }
                    function GetSelectedRow(rowindex, rdbtn) {
                        var i, obj;
                        checkUnCheckRadio(rdbtn);
                        document.getElementById("isDiarySelected").value = 'true';
                    }
                </script>
            </telerik:RadCodeBlock>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="lstDiary">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="lstDiary" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadGrid style="Height:72vh;width:auto" runat="server" ID="lstDiary" AutoGenerateColumns="false" OnNeedDataSource="lstDiary_NeedDataSource"
                OnItemCommand="lstDiary_ItemCommand" PageSize="10" OnItemDataBound="lstDiary_ItemDataBound"
                AllowSorting="True" AllowPaging="True" OnSortCommand="lstDiary_Sorting" AllowFilteringByColumn="true"
                GridLines="None" OnPageIndexChanged="lstDiary_PageIndexChanged" EnableLinqExpressions="false"
                Skin="Office2007" OnPreRender="lstDiary_PreRender" AllowCustomPaging="true" AllowMultiRowSelection="true" MasterTableView-AllowNaturalSort="false">
                <SelectedItemStyle CssClass="SelectedItem" />
                <ItemStyle Wrap="true"></ItemStyle>
                <ClientSettings AllowKeyboardNavigation="true">
                    <Scrolling  AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True"  ScrollHeight="600px">
                    </Scrolling>
                    <ClientEvents OnRowSelected="OnRowClick" OnRowDeselected="OnRowDeselect" OnGridCreated="GridCreated" OnRowCreated="RadGrid1_RowCreated" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <SortingSettings SortToolTip="" />
                <MasterTableView Width="100%" AllowCustomSorting="true" ClientDataKeyNames="entry_id" TableLayout="Fixed">
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridClientSelectColumn ItemStyle-Width="40px" HeaderStyle-Width="40px">
                        </telerik:GridClientSelectColumn>
                        <telerik:GridTemplateColumn AllowFiltering="false" ItemStyle-Width="90px" HeaderStyle-Width="90px" HeaderText="<%$ Resources:gvHdrPriority %>"
                            UniqueName="Priority" SortExpression="priority">
                            <ItemTemplate>
                                <asp:Image ID="PriorityImage" runat="server" />
                                <asp:Image ID="imageUser" runat="server" />
                                <asp:Image ID="AssignedToImage" runat="server" />
                                <asp:Label ID="lbltest" runat="server" Text="" Visible="false"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-CssClass="data" ItemStyle-Width="140px" HeaderStyle-Width="140px" HeaderText="Due"
                            DataField="COMPLETE_DATE" UniqueName="COMPLETEDATE" SortExpression="COMPLETEDATE" FilterControlWidth="100px">
                            <ItemTemplate>
                                <asp:Label ID="lblDueDate" CssClass="data" runat="server" Text="" Width="90px"></asp:Label>
                            </ItemTemplate>
                            <FilterTemplate>
                                <asp:Label ID="lblTill" CssClass="data" runat="server" Text="<%$ Resources:lblTill %>"></asp:Label>
                                <telerik:RadDatePicker ID="filterRadDatePicker" runat="server" Width="100px" ClientEvents-OnDateSelected="DateSelected" />
                                <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                    <script type="text/javascript">
                                        function DateSelected(sender, args) {
                                            var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        var date = FormatSelectedDate(sender);
                                        tableView.filter("COMPLETEDATE", date, "LessThanOrEqualTo");
                                    }
                                    function FormatSelectedDate(picker) {
                                        var date = picker.get_selectedDate();
                                        var dateInput = picker.get_dateInput();
                                        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());

                                        return formattedDate;
                                    }
                                    </script>
                                </telerik:RadScriptBlock>
                            </FilterTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-CssClass="radGrid" HeaderText="<%$ Resources:gvHdrTaskName %>" FilterControlWidth="100px" AllowFiltering="true" ItemStyle-Width="160px" HeaderStyle-Width="160px"
                            DataField="tasksubject" SortExpression="tasksubject" UniqueName="tasksubject">
                            <ItemTemplate>
                                <asp:Label ID="lbltaskSubject" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasksubject")%>'
                                    Style="cursor: pointer" CssClass="LightBlue" ForeColor="#330099" Font-Bold="true"
                                    Font-Underline="True"></asp:Label>
                                <br />
                                <asp:Label ID="lblGrdNotes" runat="server" Text="" CssClass="LightBlue"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-CssClass="radGrid" HeaderText="Attached Record" ItemStyle-Width="140px" HeaderStyle-Width="140px" FilterControlWidth="28px"
                            AllowFiltering="false" DataField="ATTACH_PROMPT" SortExpression="attachrecord" UniqueName="attachrecord">
                            <ItemTemplate>
                                <asp:Label ID="lblAttachRecord" runat="server" Text="" ForeColor="#330099"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-CssClass="radGrid" HeaderText="Parent Record" ItemStyle-Width="140px" HeaderStyle-Width="140px" FilterControlWidth="28px"
                            AllowFiltering="false" DataField="parentrecord" SortExpression="parentrecord" UniqueName="parentrecord">
                            <ItemTemplate>
                                <asp:Label ID="lblParentRecord" runat="server" ForeColor="#330099" Width="130px" Style="cursor: pointer;" CssClass="LightBlue" Font-Bold="true"
                                    Font-Underline="True"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-CssClass="radGrid" HeaderText="<%$ Resources:gvHdrWorkActivity %>" ItemStyle-Width="150px" HeaderStyle-Width="150px" FilterControlWidth="35px"
                            AllowFiltering="false" DataField="ACT_TEXT" SortExpression="work_activity" UniqueName="work_activity">
                            <ItemTemplate>
                                <asp:Label ID="lblWorkActivity" runat="server" Text="" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-CssClass="radGrid" HeaderText="Claimant" ItemStyle-Width="100px" HeaderStyle-Width="100px" FilterControlWidth="35px"
                            DataField="claimant" AllowFiltering="false" SortExpression="claimant" UniqueName="claimant">
                            <ItemTemplate>
                                <asp:Label ID="lblGrdClaimant" runat="server" Text="" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-CssClass="radGrid" HeaderText="<%$ Resources:gvHdrDepartment %>" ItemStyle-Width="100px" HeaderStyle-Width="100px" FilterControlWidth="80px"
                            DataField="department" AllowFiltering="false" SortExpression="department" UniqueName="department">
                            <ItemTemplate>
                                <asp:Label ID="lblDepartment" runat="server" Text="" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrClaimStatus %>" ItemStyle-Width="100px" HeaderStyle-Width="100px" FilterControlWidth="80px"
                            DataField="claimstatus" AllowFiltering="false" SortExpression="claimstatus" UniqueName="claimstatus">
                            <ItemTemplate>
                                <asp:Label ID="lblClaimStatus" runat="server" Text="" Width="5"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrTo %>" ItemStyle-Width="90px" HeaderStyle-Width="90px" FilterControlWidth="50px"
                            DataField="ASSIGNED_USER" SortExpression="assigned_user" UniqueName="assigned_user">
                            <ItemTemplate>
                                <asp:Label ID="lblTo" runat="server" Text="" Width="5"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrFrom %>" ItemStyle-Width="90px" HeaderStyle-Width="90px" FilterControlWidth="50px"
                            DataField="ASSIGNING_USER" SortExpression="assigning_user" UniqueName="assigning_user">
                            <ItemTemplate>
                                <asp:Label ID="lblFrom" runat="server" Text="" Width="5"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-Width="70px" HeaderStyle-Width="70px" FilterControlWidth="60px" AllowFiltering="false"
                            DataField="notroutable" SortExpression="notroutable" UniqueName="notroutable">
                            <ItemTemplate>
                                <asp:Label ID="lblNotRoutable" runat="server" Text="" Width="10px"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn ItemStyle-Width="70px" HeaderStyle-Width="70px" FilterControlWidth="60px" AllowFiltering="false"
                            DataField="notrollable" SortExpression="notrollable" UniqueName="notrollable">
                            <ItemTemplate>
                                <asp:Label ID="lblNotRollable" runat="server" Text="" Width="10px"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <ItemStyle CssClass="datatd1" />
                    <AlternatingItemStyle CssClass="datatd" />
                    <HeaderStyle CssClass="msgheader" />
                </MasterTableView>
                <PagerStyle Mode="NextPrevAndNumeric" NextPageToolTip="<%$ Resources:ttNext %>" PrevPageToolTip="<%$ Resources:ttPrev %>"
                    FirstPageToolTip="<%$ Resources:ttFirst %>" LastPageToolTip="<%$ Resources:ttLast %>" Position="Top" AlwaysVisible="true" />
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Duration="200" Type="OutQuint" />
                </FilterMenu>
            </telerik:RadGrid>
        </div>
        <table id="tblNoRecords" runat="server" width="100%" visible="false" cellspacing="0"
            cellpadding="0" border="1" bordercolor="#23238E">
            <tr>
                <td colspan="6" align="center" class="PadLeft4">
                    <br />
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="lblNoDryResrc" runat="server" Text="<%$ Resources:lblNoDryResrc %>"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
