﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintCalendar.aspx.cs" Inherits="Riskmaster.UI.Diaries.PrintCalendar" ValidateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Diary Calendar</title>  <%--csingh7 for mits 14633--%>
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <div  runat="server" id="diaryCalendarDiv">
        </div>
    </form>
</body>
</html>
