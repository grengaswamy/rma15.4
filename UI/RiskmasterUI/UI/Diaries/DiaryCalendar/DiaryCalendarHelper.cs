﻿/********************************************************************************
    Class Name: DiaryCalenderHelper.cs 
    Author    : Nitin 
    Created on: 27-Aug-2008
 *  Purpose   :  This class contains genral methods to fetch data from WebService 
 *           and to populate data in Schedule controls
 ********************************************************************************
*/

using System;
using System.Text;
using System.Web;
using System.Xml;
using Infragistics.WebUI.Shared;
using Infragistics.WebUI.WebSchedule;
using System.Globalization;
using Riskmaster.Models; //Ash - cloud / REST webservice
//using Riskmaster.UI.DiaryCalendar; //Ash - cloud / REST webservice

namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    /// <summary>
    /// Summary description for DBDataClass
    /// </summary>
    /// 
    public class DiaryCalendarHelper
    {
        private const string m_ColorCode1 = "green";
        private const string m_ColorCode2 = "purple";
        private const string m_ColorCode3 = "red";
        private string m_sPageID = string.Empty; //MITS 34492 hlv
        public string s_User { get; set; }//asharma326 jira 11081
        public bool bCreateHyperlink = true;//asharma326 jira 11081
        public bool bShowCloseImg = true;//asharma326 jira 11081
        public DiaryCalendarHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DiaryCalendarHelper(string p_sPageID)
        {
            //
            // TODO: Add constructor logic here
            //
            this.m_sPageID = p_sPageID;
        }

        #region Public Methods
        /// <summary>
        /// This method is created by Nitin 
        /// It fetches diaries from DataBase by hitting Webservice
        /// </summary>
        /// <param name="currentSelectedControl"></param>
        /// <returns></returns>
        public XmlDocument GetDiaryList(CurrentWebSchedule currentSelectedControl)
        {
            //DiaryCalendarClient objDiaryService = null;  //calling webservice
            DiaryInput oDiaryInput = null;    //declaring ModelInput 
            DiaryOutput objOutputModel = null; //declaring ModelOutput
            XmlDocument diaryListDoc = null;

            try
            {
                //setting inputs to get diarylist from webservice
                oDiaryInput = new DiaryInput();
                oDiaryInput.GetDiaryInput = InputDoc(currentSelectedControl).OuterXml;

                
                oDiaryInput.Token = AppHelper.ReadCookieValue("SessionId");
                
                //setting variable , will contain output from webservice
                objOutputModel = new DiaryOutput();

                // Start : Ash - Cloud / REST webservice call
                //setting webservice object to fetch diary list
                //objDiaryService = new DiaryCalendarClient();
                //objOutputModel = objDiaryService.GetDiaryList(oDiaryInput);

                oDiaryInput.ClientId = AppHelper.ClientId;
                objOutputModel = AppHelper.GetResponse<DiaryOutput>("RMService/Diary/list", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDiaryInput);
                // End : Ash - Cloud / REST webservice call

                diaryListDoc = new XmlDocument();
                diaryListDoc.LoadXml(objOutputModel.GetDiaryOutput);
                
                return diaryListDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            //finally
            //{
            //    objDiaryService.Close();
            //}
        }

        /// <summary>
        /// Added by paggarwal2-4
        /// Gets the View in which the Calendar should get displayed
        /// </summary>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetCalendarView()
        {
            //DiaryCalendarClient objDiaryService = null;  //calling webservice
            DiaryInput oDiaryInput = null;  //declaring ModelInput 
            DiaryOutput objOutputModel = null; //declaring ModelOutput
            XmlDocument diaryListDoc = null;

            try
            {
                //setting inputs to get diarylist from webservice
                oDiaryInput = new DiaryInput();
                oDiaryInput.GetDiaryInput = "";

                oDiaryInput.Token = AppHelper.ReadCookieValue("SessionId");

                //setting variable , will contain output from webservice
                objOutputModel = new DiaryOutput();

                // Start : Ash - Cloud / REST webservice call
                //setting webservice object to fetch diary list
                //objDiaryService = new DiaryCalendarClient();
                //objOutputModel = objDiaryService.GetCalendarView(oDiaryInput);

                oDiaryInput.ClientId = AppHelper.ClientId;
                objOutputModel = AppHelper.GetResponse<DiaryOutput>("RMService/Diary/calendarview", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDiaryInput);
                // End : Ash - Cloud / REST webservice call

                diaryListDoc = new XmlDocument();
                diaryListDoc.LoadXml(objOutputModel.GetDiaryOutput);

                return diaryListDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            //finally
            //{
            //    objDiaryService.Close();
            //}
        }

        /// <summary>
        /// Added by paggarwal2-4
        /// Gets the information required when loading the Diary Calendar
        /// </summary>
        /// <param name="userName">ref string</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetOnLoadInformation(ref string userName)
        {
            //DiaryCalendarClient objDiaryService = null;  //calling webservice //Ash - cloud/Rest
            DiaryInput oDiaryInput = null;  //declaring ModelInput 
            DiaryOutput objOutputModel = null; //declaring ModelOutput
            XmlDocument diaryListDoc = null;

            try
            {
                //setting inputs to get diarylist from webservice
                oDiaryInput = new DiaryInput();
                oDiaryInput.GetDiaryInput = "";
                oDiaryInput.Token = AppHelper.ReadCookieValue("SessionId");

                //setting variable , will contain output from webservice
                objOutputModel = new DiaryOutput();

                // Start : Ash - Cloud / REST webservice call
                //setting webservice object to fetch diary list
                //objDiaryService = new DiaryCalendarClient();
                //objOutputModel = objDiaryService.GetOnLoadInformation(oDiaryInput);

                oDiaryInput.ClientId = AppHelper.ClientId;
                objOutputModel = AppHelper.GetResponse<DiaryOutput>("RMService/Diary/info", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDiaryInput);
                // End : Ash - Cloud / REST webservice call
                
                diaryListDoc = new XmlDocument();
                diaryListDoc.LoadXml(objOutputModel.GetDiaryOutput);

                userName = objOutputModel.UserName;

                return diaryListDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            //finally
            //{
            //    objDiaryService.Close();
            //}
        }
        /// <summary>
        /// This method is created by Nitin ,to populated diaries as per desired action
        /// </summary>
        /// <param name="diaryScheduleInfo">ref WebScheduleInfo</param>
        /// <param name="diaryListDoc">XmlDocument</param>
        /// <param name="action">string</param>
        public void GetDiaryActivities(ref WebScheduleInfo diaryScheduleInfo, XmlDocument diaryListDoc, string action)
        {
            try
            {
                if (diaryListDoc != null)
                {

                    switch (action)
                    {
                        case "peek":
                            SetActivitiesForPeek(ref diaryScheduleInfo, diaryListDoc);
                            break;
                        case "processingoffice":
                            SetActivitiesForProcessingOffice(ref diaryScheduleInfo, diaryListDoc);
                            break;
                        default:
                            //in case of default , user will be current logged in
                            //SetDiaryActivityDetails(ref diaryScheduleInfo, diaryListDoc, "currentloggedinuser");
                            SetLimitedDiaryActivityDetailsForCurrentLoggedInUser(ref diaryScheduleInfo, diaryListDoc, "monthview"); 
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// This method is created by Nitin ,to populated diaries for Peek functionality
        /// </summary>
        /// <param name="diaryScheduleInfo">ref WebScheduleInfo</param>
        /// <param name="diaryListDoc">XmlDocument</param>
        private void SetActivitiesForPeek(ref WebScheduleInfo diaryScheduleInfo, XmlDocument diaryListDoc)
        {
            XmlNodeList userNodeList = null;
            string userName = string.Empty;
            XmlDocument diaryDoc = null;
            try
            {
                userNodeList = diaryListDoc.SelectNodes("PeekList/User/diaries");

                foreach (XmlNode diaryNode in userNodeList)
                {
                    userName = diaryNode.ParentNode.Attributes["FirstName"].Value;
                    userName = userName + "," + diaryNode.ParentNode.Attributes["LastName"].Value;
                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(diaryNode.OuterXml);
                    SetDiaryActivityDetails(ref diaryScheduleInfo, diaryDoc, userName);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// This method is created by Nitin ,to populated diaries for Processingoffice functionality
        /// </summary>
        /// <param name="diaryScheduleInfo">ref WebScheduleInfo</param>
        /// <param name="diaryListDoc">XmlDocument</param>
        private void SetActivitiesForProcessingOffice(ref WebScheduleInfo diaryScheduleInfo, XmlDocument diaryListDoc)
        {
            XmlNodeList userNodeList = null;
            string userName = string.Empty;
            XmlDocument diaryDoc = null;
            try
            {
                userNodeList = diaryListDoc.SelectNodes("PeekProcessingOffice/User/diaries");

                //this is incorrect check since, user count may be zero even when BES is enabled
                //if (userNodeList.Count == 0)
                //{
                //    if (diaryListDoc.InnerText == "0")
                //    {
                //        throw new ApplicationException("Please Enable Business Entity Security in your DataBase");
                //    }
                //}

                foreach (XmlNode diaryNode in userNodeList)
                {
                    userName = diaryNode.ParentNode.Attributes["FirstName"].Value;
                    userName = userName + "," + diaryNode.ParentNode.Attributes["LastName"].Value;
                    diaryDoc = new XmlDocument();
                    diaryDoc.LoadXml(diaryNode.OuterXml);
                    SetDiaryActivityDetails(ref diaryScheduleInfo, diaryDoc, userName);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        /// <summary>
        /// Method is created by Nitin
        /// Purpose is to create input xml string ,in order to fetch diaries from WebService
        /// </summary>
        /// <param name="currentSelectedControl">CurrentWebSchedule</param>
        /// <returns>XmlDocument</returns>
        private XmlDocument InputDoc(CurrentWebSchedule currentSelectedControl)
        {
            XmlDocument xDoc = null;
            XmlNode xNode = null;
            XmlAttribute atbActiveDiaryChecked = null;
            XmlAttribute atbAttachRecordId = null;
            XmlAttribute atbAttachTable = null;
            XmlAttribute atbDiaryFunction = null;
            XmlAttribute atbDueDate = null;
            XmlAttribute atbOpen = null;
            XmlAttribute atbPageCount = null;
            XmlAttribute atbPageNumber = null;
            XmlAttribute atbPageSize = null;
            XmlAttribute abtRecordCount = null;
            XmlAttribute abtReqActDiary = null;
            XmlAttribute atbShowAllClaim = null;
            XmlAttribute atbShowAllDiariesForAR = null;
            XmlAttribute atbShowNotes = null;
            XmlAttribute atbSortOrder = null;
            XmlAttribute atbUser = null;
            XmlAttribute atbCurrentScheduleControl = null;
            XmlAttribute atbFromDate = null;
            XmlAttribute atbToDate = null;
            XmlAttribute atbAction = null;
            XmlAttribute xmlAttribute = null;

            try
            {
                xDoc = new XmlDocument();

                xNode = xDoc.CreateElement("GetDiaryDom");


                atbActiveDiaryChecked = xDoc.CreateAttribute("activediarychecked");
                atbActiveDiaryChecked.Value = "";

                xNode.Attributes.Append(atbActiveDiaryChecked);


                atbAttachRecordId = xDoc.CreateAttribute("attachrecordid");
                atbAttachRecordId.Value = "";
                xNode.Attributes.Append(atbAttachRecordId);

                atbAttachTable = xDoc.CreateAttribute("attachtable");
                atbAttachTable.Value = "";
                xNode.Attributes.Append(atbAttachTable);



                atbDiaryFunction = xDoc.CreateAttribute("diaryfunction");
                atbDiaryFunction.Value = "";
                xNode.Attributes.Append(atbDiaryFunction);

                atbDueDate = xDoc.CreateAttribute("duedate");
                atbDueDate.Value = "";
                xNode.Attributes.Append(atbDueDate);

                atbOpen = xDoc.CreateAttribute("open");
                atbOpen.Value = "1";
                xNode.Attributes.Append(atbOpen);

                atbPageCount = xDoc.CreateAttribute("pagecount");
                atbPageCount.Value = "";
                xNode.Attributes.Append(atbPageCount);

                atbPageNumber = xDoc.CreateAttribute("pagenumber");
                atbPageNumber.Value = "1";
                xNode.Attributes.Append(atbPageNumber);

                atbPageSize = xDoc.CreateAttribute("pagesize");
                atbPageSize.Value = "10";
                xNode.Attributes.Append(atbPageSize);

                abtRecordCount = xDoc.CreateAttribute("recordcount");
                abtRecordCount.Value = "";
                xNode.Attributes.Append(abtRecordCount);

                abtReqActDiary = xDoc.CreateAttribute("reqactdiary");
                abtReqActDiary.Value = "0";
                xNode.Attributes.Append(abtReqActDiary);

                atbShowAllClaim = xDoc.CreateAttribute("showallclaimdiaries");
                atbShowAllClaim.Value = "";
                xNode.Attributes.Append(atbShowAllClaim);

                atbShowAllDiariesForAR = xDoc.CreateAttribute("showalldiariesforactiverecord");
                atbShowAllDiariesForAR.Value = "yes";
                xNode.Attributes.Append(atbShowAllDiariesForAR);

                atbShowNotes = xDoc.CreateAttribute("shownotes");
                atbShowNotes.Value = "0";
                xNode.Attributes.Append(atbShowNotes);

                atbSortOrder = xDoc.CreateAttribute("sortorder");
                atbSortOrder.Value = "";
                xNode.Attributes.Append(atbSortOrder);

                atbUser = xDoc.CreateAttribute("user");
                atbUser.Value = s_User;
                xNode.Attributes.Append(atbUser);

                atbCurrentScheduleControl = xDoc.CreateAttribute("currentschedulecontrol");
                atbCurrentScheduleControl.Value = currentSelectedControl.CurrentControl;
                xNode.Attributes.Append(atbCurrentScheduleControl);


                if (currentSelectedControl.CurrentControl == "webweekview")
                {
                    atbFromDate = xDoc.CreateAttribute("fromdate");
                    atbFromDate.Value = currentSelectedControl.FromDate;
                    xNode.Attributes.Append(atbFromDate);

                    atbToDate = xDoc.CreateAttribute("todate");
                    atbToDate.Value = currentSelectedControl.ToDate;
                    xNode.Attributes.Append(atbToDate);

                    xmlAttribute = xDoc.CreateAttribute("Code");
                    xmlAttribute.Value = "1";
                    xNode.Attributes.Append(xmlAttribute);
                }
                else if (currentSelectedControl.CurrentControl == "webdayview")
                {
                    atbFromDate = xDoc.CreateAttribute("currentactivedate");
                    atbFromDate.Value = currentSelectedControl.CurrentActiveDate;
                    xNode.Attributes.Append(atbFromDate);
                    xmlAttribute = xDoc.CreateAttribute("Code");
                    xmlAttribute.Value = "2";
                    xNode.Attributes.Append(xmlAttribute);

                }
                else
                {
                    atbFromDate = xDoc.CreateAttribute("navigationyear");
                    atbFromDate.Value = currentSelectedControl.NavigationYear;
                    xNode.Attributes.Append(atbFromDate);

                    atbToDate = xDoc.CreateAttribute("navigationmonth");
                    atbToDate.Value = currentSelectedControl.NavigationMonth;
                    xNode.Attributes.Append(atbToDate);

                    xmlAttribute = xDoc.CreateAttribute("Code");
                    xmlAttribute.Value = "0";
                    xNode.Attributes.Append(xmlAttribute);
                }

                atbAction = xDoc.CreateAttribute("action");
                atbAction.Value = currentSelectedControl.Action;
                xNode.Attributes.Append(atbAction);

                xmlAttribute = xDoc.CreateAttribute("FromStyle");
                xmlAttribute.Value = currentSelectedControl.FromStyle.ToString();
                xNode.Attributes.Append(xmlAttribute);

                xDoc.AppendChild(xNode);
                return xDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// This method is created by Nitin 
        /// to populate webscheduleinfo control with diaries.
        /// </summary>
        /// <param name="diaryScheduleInfo">ref WebScheduleInfo</param>
        /// <param name="diaryListDoc">XmlDocument</param>
        /// <param name="userName">string</param>
        private void SetDiaryActivityDetails(ref WebScheduleInfo diaryScheduleInfo, XmlDocument diaryListDoc, string userName)
        {
            XmlNodeList distinctListByDueDate = null;
            XmlNodeList selectedDiaryList = null;
            Appointment currentDiaryActivity = null;
            string diaryDueDate = string.Empty;
            StringBuilder descHtmlMakrup = null;

            string diaryTaskName = string.Empty;
            string diaryAttRecordPrompt = string.Empty;
            string diaryParentRecord = string.Empty;
            string diaryAttTable = string.Empty;
            string diaryAttRecodId = string.Empty;
            string diaryEntryId = string.Empty;
            string diaryWorkActivity = string.Empty;
            string diaryClaimant = string.Empty;
            bool isDueDateOver = false;
            string taskCode = string.Empty;
            string userImage = string.Empty;
            DateTime dtDatetime = new DateTime();
            try
            {
                //fetching distinct dariy from diarylist
                distinctListByDueDate = diaryListDoc.SelectNodes("/diaries/diary[not(@complete_date=preceding-sibling::diary/@complete_date)]");

                foreach (XmlNode distinctDiaryNode in distinctListByDueDate)
                {
                    diaryDueDate = distinctDiaryNode.Attributes["complete_date"].Value.ToString();

                    //selecting DiaryList having complete date diaryduedate
                    selectedDiaryList = diaryListDoc.SelectNodes("/diaries/diary[@complete_date='" + diaryDueDate + "']");
                    currentDiaryActivity = new Appointment(diaryScheduleInfo);
                    if (userName == "currentloggedinuser")  //incase of current logged in 
                    {
                        currentDiaryActivity.Subject = selectedDiaryList.Count.ToString();
                    }
                    else
                    {
                        currentDiaryActivity.Subject = userName + "(" + selectedDiaryList.Count.ToString() + ")";
                    }
                    descHtmlMakrup = new StringBuilder();
                    dtDatetime = DateTime.Parse(diaryDueDate, new CultureInfo(diaryScheduleInfo.CultureInfo.Name, false));
                    currentDiaryActivity.StartDateTime = new SmartDate(dtDatetime).AddHours(3);
                    currentDiaryActivity.EndDateTime = new SmartDate(dtDatetime).AddHours(6);
                    if (DateTime.Now > Convert.ToDateTime(diaryDueDate))
                    {
                        isDueDateOver = true;
                    }
                    else
                    {
                        isDueDateOver = false;
                    }
                    currentDiaryActivity.Key = userName + currentDiaryActivity.StartDateTime.ToLongDateString();
                    currentDiaryActivity.DataKey = userName + currentDiaryActivity.StartDateTime.ToLongDateString();
                    descHtmlMakrup.Append(GetHtmlTableHeaderMarkup());
                    foreach (XmlNode selectedDiary in selectedDiaryList)
                    {
                        diaryParentRecord = "";
                        diaryTaskName="";
                        diaryAttRecordPrompt = "";
                        diaryWorkActivity="";
                        diaryClaimant = "";
                        diaryEntryId = "";
                        diaryAttTable="";
                        diaryAttRecodId = "";
                        taskCode = "";
                        userImage="";
                        if (selectedDiary.Attributes["tasksubject"].Value != null)
                        {
                            diaryTaskName = selectedDiary.Attributes["tasksubject"].Value;
                        }

                        if (selectedDiary.Attributes["attachprompt"].Value != null)
                        {
                            diaryAttRecordPrompt = selectedDiary.Attributes["attachprompt"].Value;
                        }

                        if (selectedDiary.Attributes["parentrecord"] != null)
                        {
                            diaryParentRecord = selectedDiary.Attributes["parentrecord"].Value;
                        }

                        if (selectedDiary.Attributes["work_activity"].Value != null)
                        {
                            diaryWorkActivity = selectedDiary.Attributes["work_activity"].Value;
                        }

                        if (selectedDiary.Attributes["claimant"] != null)
                        {
                            diaryClaimant = selectedDiary.Attributes["claimant"].Value.Trim();
                            //MITS 13735, 13736
                            if (diaryClaimant != null && diaryClaimant != string.Empty &&
                            diaryClaimant.Length > 0 && diaryClaimant[diaryClaimant.Length - 1] == ',')
                            {
                                diaryClaimant = diaryClaimant.Substring(0, diaryClaimant.Length - 1);
                            }
                        }

                        if (selectedDiary.Attributes["entry_id"] != null)
                        {
                            diaryEntryId = selectedDiary.Attributes["entry_id"].Value;
                        }

                        if (selectedDiary.Attributes["attach_table"] != null)
                        {
                            diaryAttTable = selectedDiary.Attributes["attach_table"].Value;
                        }

                        if (selectedDiary.Attributes["attach_recordid"] != null)
                        {
                            diaryAttRecodId = selectedDiary.Attributes["attach_recordid"].Value;
                        }
                        if (selectedDiary.Attributes["TaskCode"] != null)
                        {
                            taskCode = selectedDiary.Attributes["TaskCode"].Value;
                        }

                        if (selectedDiary.Attributes["img"] != null)
                        {
                            userImage = selectedDiary.Attributes["img"].Value;
                        }

                        descHtmlMakrup.Append(GetHtmlMarkupForDiaryDesc(diaryEntryId, diaryTaskName, diaryAttRecordPrompt,
                            diaryAttRecodId, diaryWorkActivity, diaryClaimant, diaryAttTable, isDueDateOver, taskCode, userImage, diaryParentRecord));
                    }
                    descHtmlMakrup.Append(GetHtmlTableFooterMarkup());
                    currentDiaryActivity.Description = descHtmlMakrup.ToString();
                    diaryScheduleInfo.Activities.Add(currentDiaryActivity);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        /// <summary>
        /// This method is created by Nitin 
        /// to populate webscheduleinfo control with diaries.
        /// </summary>
        /// <param name="diaryScheduleInfo">ref WebScheduleInfo</param>
        /// <param name="diaryListDoc">XmlDocument</param>
        /// <param name="userName">string</param>
        private void SetLimitedDiaryActivityDetailsForCurrentLoggedInUser(ref WebScheduleInfo diaryScheduleInfo, XmlDocument diaryListDoc, string currentControl)
        {
            XmlNodeList distinctListByDueDate = null;
            XmlNodeList selectedDiaryList = null;
            Appointment currentDiaryActivity = null;
            string diaryDueDate = string.Empty;
            StringBuilder descHtmlMakrup = null;

            string diaryTaskName = string.Empty;
            string diaryAttRecordPrompt = string.Empty, diaryParentRecord=string.Empty;
            string diaryAttTable = string.Empty;
            string diaryAttRecodId = string.Empty;
            string diaryEntryId = string.Empty;
            string diaryWorkActivity = string.Empty;
            string diaryClaimant = string.Empty;
            bool isDueDateOver = false;
            string taskCode = string.Empty;
            string userImage = string.Empty;
            SmartDate diaryStartDate;
            SmartDate diaryEndDate;
            int diaryCount = 0;
            DateTime dtDatetime= new DateTime();
            try
            {
                //fetching distinct dariy from diarylist
                distinctListByDueDate = diaryListDoc.SelectNodes("/diaries/diary[not(@complete_date=preceding-sibling::diary/@complete_date)]");

                foreach (XmlNode distinctDiaryNode in distinctListByDueDate)
                {
                    diaryDueDate = distinctDiaryNode.Attributes["complete_date"].Value.ToString();

                    dtDatetime=DateTime.Parse(diaryDueDate, new CultureInfo(diaryScheduleInfo.CultureInfo.Name,false));
                    diaryStartDate = new SmartDate(dtDatetime).AddHours(3);
                    diaryEndDate = new SmartDate(dtDatetime).AddHours(6);

                    //selecting DiaryList having complete date diaryduedate
                    selectedDiaryList = diaryListDoc.SelectNodes("/diaries/diary[@complete_date='" + diaryDueDate + "']");

                    diaryCount = 0;

                    foreach (XmlNode selectedDiary in selectedDiaryList)
                    {
                        diaryParentRecord = "";
                        diaryTaskName = "";
                        diaryAttRecordPrompt = "";
                        diaryWorkActivity = "";
                        diaryClaimant = "";
                        diaryEntryId = "";
                        diaryAttTable = "";
                        diaryAttRecodId = "";
                        taskCode = "";
                        userImage = "";
                        if (diaryListDoc.SelectSingleNode("//diaries").Attributes["currentControl"].Value.Equals("webdayview"))
                        {
                            SetAllActivityDetailsForCurrentLoggedInUser(ref diaryScheduleInfo, selectedDiaryList, diaryStartDate, diaryEndDate);
                            break;
                        }
                        if (diaryCount == 3)
                        {
                            SetAllActivityDetailsForCurrentLoggedInUser(ref diaryScheduleInfo,selectedDiaryList, diaryStartDate, diaryEndDate);
                            break;
                        }
                        else
                        {
                            diaryCount++;
                        }
                        

                        if (selectedDiary.Attributes["tasksubject"].Value != null)
                        {
                            diaryTaskName = selectedDiary.Attributes["tasksubject"].Value;
                        }

                        if (selectedDiary.Attributes["attachprompt"].Value != null)
                        {
                            diaryAttRecordPrompt = selectedDiary.Attributes["attachprompt"].Value;
                        }

                        if (selectedDiary.Attributes["work_activity"].Value != null)
                        {
                            diaryWorkActivity = selectedDiary.Attributes["work_activity"].Value;
                        }

                        if (selectedDiary.Attributes["claimant"] != null)
                        {
                            diaryClaimant = selectedDiary.Attributes["claimant"].Value.Trim();
                            //MITS 13735, 13736
                            if (diaryClaimant != null && diaryClaimant != string.Empty &&
                            diaryClaimant.Length > 0 && diaryClaimant[diaryClaimant.Length - 1] == ',')
                            {
                                diaryClaimant = diaryClaimant.Substring(0, diaryClaimant.Length - 1);
                            }
                        }

                        if (selectedDiary.Attributes["entry_id"] != null)
                        {
                            diaryEntryId = selectedDiary.Attributes["entry_id"].Value;
                        }

                        if (selectedDiary.Attributes["parentrecord"] != null)
                        {
                            diaryParentRecord = selectedDiary.Attributes["parentrecord"].Value;
                        }

                        if (selectedDiary.Attributes["attach_table"] != null)
                        {
                            diaryAttTable = selectedDiary.Attributes["attach_table"].Value;
                        }

                        if (selectedDiary.Attributes["attach_recordid"] != null)
                        {
                            diaryAttRecodId = selectedDiary.Attributes["attach_recordid"].Value;
                        }
                        if (selectedDiary.Attributes["TaskCode"] != null)
                        {
                            taskCode = selectedDiary.Attributes["TaskCode"].Value;
                        }
                        
                        
                        currentDiaryActivity = new Appointment(diaryScheduleInfo);

                        currentDiaryActivity.Subject = "  " + diaryCount.ToString() + ".)  "  + diaryTaskName   + ".....";
                        
                        descHtmlMakrup = new StringBuilder();

                        diaryStartDate = diaryStartDate.AddMinutes(1);
                        diaryEndDate = diaryEndDate.AddMinutes(1);

                        currentDiaryActivity.StartDateTime = diaryStartDate;
                        currentDiaryActivity.EndDateTime = diaryEndDate;

                        if (DateTime.Now > Convert.ToDateTime(diaryDueDate))
                        {
                            isDueDateOver = true;
                        }
                        else
                        {
                            isDueDateOver = false;
                        }
                        currentDiaryActivity.Key = currentDiaryActivity.StartDateTime.ToString();
                        currentDiaryActivity.DataKey = currentDiaryActivity.StartDateTime.ToString();
                        descHtmlMakrup.Append(GetHtmlTableHeaderMarkup());

                        if (selectedDiary.Attributes["img"] != null)
                        {
                            userImage = selectedDiary.Attributes["img"].Value;
                        }

                        descHtmlMakrup.Append(GetHtmlMarkupForDiaryDesc(diaryEntryId, diaryTaskName, diaryAttRecordPrompt,
                            diaryAttRecodId, diaryWorkActivity, diaryClaimant, diaryAttTable, isDueDateOver, taskCode, userImage, diaryParentRecord));

                        descHtmlMakrup.Append(GetHtmlTableFooterMarkup());
                        currentDiaryActivity.Description = descHtmlMakrup.ToString();
                        //currentDiaryActivity.Style.CustomRules = "text-indent: 10px;background: url(D:/DotNet/ConnectDemo/Images/user.gif);background-repeat: no-repeat;background-position: 5px;cursor: hand;";
                        diaryScheduleInfo.Activities.Add(currentDiaryActivity);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private void SetAllActivityDetailsForCurrentLoggedInUser(ref WebScheduleInfo diaryScheduleInfo,XmlNodeList selectedNodeList, SmartDate selectedStartDate,SmartDate selectedEndDate)
        {
            Appointment currentDiaryActivity = null;
            StringBuilder descHtmlMakrup = null;

            string diaryTaskName = string.Empty;
            string diaryAttRecordPrompt = string.Empty, diaryParentRecord=string.Empty;
            string diaryAttTable = string.Empty;
            string diaryAttRecodId = string.Empty;
            string diaryEntryId = string.Empty;
            string diaryWorkActivity = string.Empty;
            string diaryClaimant = string.Empty;
            bool isDueDateOver = false;
            string taskCode = string.Empty;
            string userImage = string.Empty;
            
            try
            {
                    currentDiaryActivity = new Appointment(diaryScheduleInfo);
                    currentDiaryActivity.Subject = "    more....";
                    
                    descHtmlMakrup = new StringBuilder();
                    currentDiaryActivity.StartDateTime = selectedStartDate.AddMinutes(1);
                    currentDiaryActivity.EndDateTime = selectedEndDate.AddMinutes(2);
                    if (DateTime.Now > Convert.ToDateTime(selectedStartDate.ToString()))
                    {
                        isDueDateOver = true;
                    }
                    else
                    {
                        isDueDateOver = false;
                    }

                    currentDiaryActivity.Key = currentDiaryActivity.StartDateTime.ToString();
                    currentDiaryActivity.DataKey = currentDiaryActivity.StartDateTime.ToString();
                    descHtmlMakrup.Append(GetHtmlTableHeaderMarkup());

                    foreach (XmlNode selectedDiary in selectedNodeList)
                    {
                        diaryParentRecord = "";
                        diaryTaskName = "";
                        diaryAttRecordPrompt = "";
                        diaryWorkActivity = "";
                        diaryClaimant = "";
                        diaryEntryId = "";
                        diaryAttTable = "";
                        diaryAttRecodId = "";
                        taskCode = "";
                        userImage = "";
                        if (selectedDiary.Attributes["tasksubject"].Value != null)
                        {
                            diaryTaskName = selectedDiary.Attributes["tasksubject"].Value;
                        }

                        if (selectedDiary.Attributes["attachprompt"].Value != null)
                        {
                            diaryAttRecordPrompt = selectedDiary.Attributes["attachprompt"].Value;
                        }

                        if (selectedDiary.Attributes["work_activity"].Value != null)
                        {
                            diaryWorkActivity = selectedDiary.Attributes["work_activity"].Value;
                        }
                        if (selectedDiary.Attributes["parentrecord"] != null)
                        {
                            diaryParentRecord = selectedDiary.Attributes["parentrecord"].Value;
                        }

                        if (selectedDiary.Attributes["claimant"] != null)
                        {
                            diaryClaimant = selectedDiary.Attributes["claimant"].Value.Trim();
                            
                            if (diaryClaimant != null && diaryClaimant != string.Empty &&
                            diaryClaimant.Length > 0 && diaryClaimant[diaryClaimant.Length - 1] == ',')
                            {
                                diaryClaimant = diaryClaimant.Substring(0, diaryClaimant.Length - 1);
                            }
                        }

                        if (selectedDiary.Attributes["entry_id"] != null)
                        {
                            diaryEntryId = selectedDiary.Attributes["entry_id"].Value;
                        }

                        if (selectedDiary.Attributes["attach_table"] != null)
                        {
                            diaryAttTable = selectedDiary.Attributes["attach_table"].Value;
                        }

                        if (selectedDiary.Attributes["attach_recordid"] != null)
                        {
                            diaryAttRecodId = selectedDiary.Attributes["attach_recordid"].Value;
                        }
                        if (selectedDiary.Attributes["TaskCode"] != null)
                        {
                            taskCode = selectedDiary.Attributes["TaskCode"].Value;
                        }

                        if (selectedDiary.Attributes["img"] != null)
                        {
                            userImage = selectedDiary.Attributes["img"].Value;
                        }


                        descHtmlMakrup.Append(GetHtmlMarkupForDiaryDesc(diaryEntryId, diaryTaskName, diaryAttRecordPrompt,
                            diaryAttRecodId, diaryWorkActivity, diaryClaimant, diaryAttTable, isDueDateOver, taskCode, userImage, diaryParentRecord));
                    }
                    descHtmlMakrup.Append(GetHtmlTableFooterMarkup());
                    currentDiaryActivity.Description = descHtmlMakrup.ToString();
                   
                
                diaryScheduleInfo.Activities.Add(currentDiaryActivity);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        /// <summary>
        /// Constructs the html for display
        /// </summary>
        /// <param name="diaryEntryId">string</param>
        /// <param name="taskName">string</param>
        /// <param name="attachedRecordPrompt">string</param>
        /// <param name="attachedRecordId">string</param>
        /// <param name="workActivity">string</param>
        /// <param name="claimant">string</param>
        /// <param name="attachTable">string</param>
        /// <param name="isDueDateOver">string</param>
        /// <param name="taskCode">string</param>
        /// <returns>string</returns>
        private string GetHtmlMarkupForDiaryDesc(string diaryEntryId, string taskName, string attachedRecordPrompt,
            string attachedRecordId, string workActivity, string claimant, string attachTable, bool isDueDateOver, string taskCode,string userImagePath, string sParentRecord)
        {
            StringBuilder strBulder = null;
            IntegrationHelper objIntegration = null;
            string taskNameUrl = string.Empty;
            string attachRecordUrl = string.Empty;

            try
            {
                objIntegration = new IntegrationHelper();
                attachRecordUrl = objIntegration.GetAttachedTableCategory(attachTable);

                strBulder = new StringBuilder();
                
                strBulder.Append("<tr style='color: #330099;font-weight:bold'>");
                
                if (userImagePath == string.Empty)
                {
                    userImagePath = "/RiskmasterUI/Images/user_task.png";
                }

                strBulder.Append("<td width='10%'><img src='" + userImagePath + "'/></td>");

                if (taskName == string.Empty)
                {
                    strBulder.Append("<td width='15%' style='text-decoration: underline;'>" + taskName + "</td>");
                }
                else
                {
                    //asharma326 jira 11081
                    if(this.bCreateHyperlink)
                        strBulder.Append("<td width='15%' onclick=EditDiary('" + diaryEntryId + "','" + attachTable + "') style='cursor: pointer;text-decoration: underline'>" + taskName + "</td>");
                    else
                        strBulder.Append("<td width='15%'>" + taskName + "</td>");

                }
                if (attachTable == string.Empty)
                {
                    strBulder.Append("<td width='25%' style='text-decoration: underline'>" + attachedRecordPrompt + "</td>");
                }
                else
                {
                    //asharma326 jira 11081
                    if (this.bCreateHyperlink)
                        strBulder.Append("<td width='25%' onclick=NavigateToSelectedFDM('" + attachedRecordId + "','" + attachRecordUrl + "')  style='cursor: pointer;text-decoration: underline'>"
                        + attachedRecordPrompt + "</td>");
                    else
                        strBulder.Append("<td width='25%' >"+ attachedRecordPrompt + "</td>");
                }

                //asharma326 jira 11081
                if (sParentRecord == string.Empty)
                {
                    strBulder.Append("<td width='25%' style='text-decoration: underline'>" + sParentRecord + "</td>");
                }
                else
                {
                    string sParentRecordtext = string.Empty;
                    string tableName = sParentRecord.Split('Æ')[2].Trim();
                    sParentRecordtext = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(tableName) + ":" + sParentRecord.Split('Æ')[1];
                    if (this.bCreateHyperlink)
                        strBulder.Append("<td width='25%' onclick=parent.parent.MDIShowScreen('" + sParentRecord.Split('Æ')[0] + "','" + tableName + "')  style='cursor: pointer;text-decoration: underline'>"
                        + sParentRecordtext + "</td>");
                    else
                        strBulder.Append("<td width='25%' >" + sParentRecordtext + "</td>");
                }

                strBulder.Append("<td width='25%'>" + claimant + "</td>");
                strBulder.Append("</tr>");
                return strBulder.ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        /// <summary>
        /// This method is created by Nitin to create Html table header,to be displayed on Div present on UI form
        /// </summary>
        /// <returns>html markup string for table header</returns>
        private string GetHtmlTableHeaderMarkup()
        {
            StringBuilder tableHeader = null;

            tableHeader = new StringBuilder();
            tableHeader.Append("<div>");
            tableHeader.Append("<table border='0' cellpadding='0' cellspacing='0' style='border-color: Black;width:100%;'>");
            tableHeader.Append("<tr  class='ctrlgroup' >");
            tableHeader.Append("<td width='10%'></td>");

            //MITS 34492 - hlv begin
            if (this.m_sPageID.Length == 0)
            {
                tableHeader.Append("<td width='15%'>Task Name</td>");
                tableHeader.Append("<td width='25%'>Attached Record</td>");
                tableHeader.Append("<td width='25%'>Parent record</td>"); 
                tableHeader.Append("<td width='25%'>Claimant</td>");
            }
            else
            {

                tableHeader.Append("<td width='10%'>" + AppHelper.GetResourceValue(this.m_sPageID, "lblTaskName", "0") + "</td>");
                tableHeader.Append("<td width='25%'>" + AppHelper.GetResourceValue(this.m_sPageID, "lblAttache", "0") + "</td>");
                tableHeader.Append("<td width='25%'>" + AppHelper.GetResourceValue(this.m_sPageID, "lblParentRecord", "0") + "</td>");
                tableHeader.Append("<td width='25%'>" + AppHelper.GetResourceValue(this.m_sPageID, "lblClaimant", "0") + "</td>");
            }
            //MITS 34492 - hlv end
            //asharma326 jira 11081
            if(this.bShowCloseImg)
                tableHeader.Append("<td align='right' onclick='HideDiv()' style='cursor: hand'><img src='../../../Images/removeicon.gif'/></td>");
            else
                tableHeader.Append("<td> </td>");

            tableHeader.Append("</tr>");

            return tableHeader.ToString();
        }

        /// <summary>
        /// This method is created by Nitin to create Html table footer,to be displayed on Div present on UI form
        /// </summary>
        /// <returns>html markup string for table footer</returns>
        private string GetHtmlTableFooterMarkup()
        {
            return "</table>";
        }


        /// <summary>
        /// Added by paggarwal2-4
        /// Set the new Calendar View xml
        /// </summary>
        /// <param name="code">string</param>
        /// <returns>XmlDocument</returns>
        private XmlDocument CreateSetCalendarViewXML(string code)
        {
            XmlDocument xDoc = null;
            XmlNode xNode = null;
            XmlAttribute xmlAttributeCode = null;
            try
            {
                xDoc = new XmlDocument();
                xNode = xDoc.CreateElement("CalendarView");

                xmlAttributeCode = xDoc.CreateAttribute("Code");
                xmlAttributeCode.Value = code;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            return xDoc;
        }
        #endregion
    }
}