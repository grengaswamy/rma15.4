﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryCalendarStyle.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryCalendar.DiaryCalendarStyle" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Diary Calender Style</title>
         <script language="javascript" type="text/javascript"  >
         function selectStyle(obj)
         {
            if(obj == 2)
            {
                document.getElementById('hdnSelectedValue').value = 'DayView.aspx';
            }
            else if(obj == 1)
            {
                document.getElementById('hdnSelectedValue').value = 'WeekView.aspx';
            }
            else if(obj == 0)
            {
                document.getElementById('hdnSelectedValue').value = 'MonthView.aspx';
            }
         }
         function Save() 
         {
            var isReplaced = false;
            if(!window.opener.closed)
             {
                 var rdButton= document.getElementById('rdDaily');
                 var diaryAction = window.opener.document.getElementById("hdnDiaryAction").value;
                 var reloadString = window.opener.location.href;
                 var isHome='';
                 if(window.opener.document.getElementById("btnPeek")!=null)
                     isHome = window.opener.document.getElementById("btnPeek").value;
                 if(rdButton.checked)
                 {
                    window.opener.document.getElementById('hdnSelectedControl').value = 'DayView.aspx';
                    var diaryPageString = window.opener.location.href;
                    var querystrings = diaryPageString.split('&');
                    if(querystrings.length >0)
                    {
                        for(var i =0;i<querystrings.length;i++)
                        {
                            if(querystrings[i].search('CalendarView')!=-1)
                            {
                                var splitString = querystrings[i].split('=');
                                reloadString = reloadString.replace(splitString[1], 'DayView.aspx');
                                isReplaced = true;
                            }
                            if(querystrings[i].search('action')==0)
                            {
                                var splitString = querystrings[i].split('=');
                                reloadString = reloadString.replace(splitString[1], diaryAction);
                                isReplaced = true;
                            }
                            if(querystrings[i].search('isHome')==0)
                            {
                                var splitString = querystrings[i].split('=');
                                reloadString = reloadString.replace(splitString[1], encodeURIComponent(isHome)); //ksahu5 ML Change
                                isReplaced = true;
                            }
                        }                       
                    }
                    if(!isReplaced)
                    {
                        if (window.opener.location.href.indexOf('?') == -1) 
                        {
                                 window.opener.location.href = window.opener.location.href +
                                 "?CalendarView=DayView.aspx&action=" + diaryAction + "&isHome=" + encodeURIComponent(isHome);//ksahu5 ML Change
                        }
                        else 
                        {
                            window.opener.location.href = window.opener.location.href +
                            "&CalendarView=DayView.aspx&action=" + diaryAction + "&isHome=" + encodeURIComponent(isHome);//ksahu5 ML Change
                        }
                    }
                    else
                     {
                        window.opener.location.href = reloadString;
                     }
                 }
                 rdButton= document.getElementById('rdWeekly');
                 if(rdButton.checked)
                 {
                     window.opener.document.getElementById('hdnSelectedControl').value = 'WeekView.aspx';
                     var diaryPageString = window.opener.location.href;
                     var querystrings = diaryPageString.split('&');            
                     if(querystrings.length >0)
                     {
                        for(var i =0;i<querystrings.length;i++)
                        {
                            if(querystrings[i].search('CalendarView')!=-1)
                            {
                                var splitString = querystrings[i].split('=');
                                reloadString = reloadString.replace(splitString[1],'WeekView.aspx');
                                isReplaced = true;
                            }
                            if(querystrings[i].search('action')==0)
                            {
                                var splitString = querystrings[i].split('=');
                                reloadString = reloadString.replace(splitString[1], diaryAction);
                                isReplaced = true;
                            }
                           if(querystrings[i].search('isHome')==0)
                            {
                                var splitString = querystrings[i].split('=');
                                reloadString = reloadString.replace(splitString[1], encodeURIComponent(isHome));//ksahu5 ML Change
                                isReplaced = true;
                            }
                        }                       
                     }
                         if (!isReplaced) {
                             if (window.opener.location.href.indexOf('?') == -1) {
                                 window.opener.location.href = window.opener.location.href +
                                "?CalendarView=WeekView.aspx&action=" + diaryAction + "&isHome=" + encodeURIComponent(isHome);//ksahu5 ML Change
                             }
                             else {
                                 window.opener.location.href = window.opener.location.href +
                                "&CalendarView=WeekView.aspx&action=" + diaryAction + "&isHome=" + encodeURIComponent(isHome);//ksahu5 ML Change
                             }
                         }
                         else {
                             window.opener.location.href = reloadString;
                         }
                     }
                     rdButton = document.getElementById('rdMonthly');
                     if (rdButton.checked) {
                         window.opener.document.getElementById('hdnSelectedControl').value = 'MonthView.aspx';
                         var diaryPageString = window.opener.location.href;
                         var querystrings = diaryPageString.split('&');
                         if (querystrings.length > 0) {
                             for (var i = 0; i < querystrings.length; i++) {
                                 if (querystrings[i].search('CalendarView') != -1) {
                                     var splitString = querystrings[i].split('=');
                                     reloadString = reloadString.replace(splitString[1], 'MonthView.aspx');
                                     isReplaced = true;
                                 }
                                 if (querystrings[i].search('action') == 0) {
                                     var splitString = querystrings[i].split('=');
                                     reloadString = reloadString.replace(splitString[1], diaryAction);
                                     isReplaced = true;
                                 }
                                 if (querystrings[i].search('isHome') == 0) {
                                     var splitString = querystrings[i].split('=');
                                     reloadString = reloadString.replace(splitString[1], encodeURIComponent(isHome));//ksahu5 ML Change
                                     isReplaced = true;
                                 }
                             }
                         }
                         if (!isReplaced) {
                             if (window.opener.location.href.indexOf('?') == -1) {
                                 window.opener.location.href = window.opener.location.href +
                                "?CalendarView=MonthView.aspx&action=" + diaryAction + "&isHome=" + encodeURIComponent(isHome);//ksahu5 ML Change
                             }
                             else {
                                 window.opener.location.href = window.opener.location.href +
                                "&CalendarView=MonthView.aspx&action=" + diaryAction + "&isHome=" + encodeURIComponent(isHome);//ksahu5 ML Change
                             }
                         }
                         else {
                             window.opener.location.href = reloadString;
                         }
                     }
                 }

                 window.close();
             }
        </script>
</head>
<body >
    <form id="form1" runat="server">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <div style="width: 301px">
     <table align="center" style="width: 302px; color: #000080;">
         <tr style="width:100%">
            <td width="100%" align="left" class="style1" 
                 style="font-weight: bold; text-decoration: underline; color: #000080;">
                <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <input name="select" type="radio" value="0" id="rdDaily" onclick="selectStyle(2)" runat="server"/>
                <asp:Label ID="lblDiary" runat="server" Text="<%$ Resources:lblDiary %>"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <input name="select" type="radio" value="1" id="rdWeekly" onclick="selectStyle(1)" runat="server"/>
                <asp:Label ID="lblWeekly" runat="server" Text="<%$ Resources:lblWeekly %>"></asp:Label>
            </td>
        </tr>
        <tr>
            <td >
                <input name="select" type="radio" value="2" id="rdMonthly" onclick="selectStyle(0)" runat="server"/>
                <asp:Label ID="lblMonthly" runat="server" Text="<%$ Resources:lblMonthly %>"></asp:Label>
            <input type="hidden" value="MonthView.aspx" id="hdnSelectedValue" runat="server"/>
            </td>
            
        </tr>
        <tr>
        <td>
            <!--<input type="button" class="button" onclick="Save();" value="Save"/>-->
            <asp:Button ID="btnSave" Text="<%$ Resources:btnSave %>" class="button" OnClientClick="Save();return false;" runat="server" />
        </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>