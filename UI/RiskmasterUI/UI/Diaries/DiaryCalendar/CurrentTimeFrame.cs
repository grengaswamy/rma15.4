﻿/********************************************************************************
    Class Name: CurrentTimeFrame.cs
    Author: Nitin 
    Created on: 04-Sept-2008
 *  Purpose:Class contains functions and methods to get value of current month\week
 ********************************************************************************
*/

using System;

/// <summary>
/// Summary description for CurrentTimeFrame
/// </summary>
/// 
namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public class CurrentTimeFrame
    {
        public CurrentTimeFrame()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Public Methods
        /// <summary>
        /// This method is created by Nitin to get CurrentMonth for Monthview,on the basis of currentactviedate and previousdate
        /// </summary>
        /// <param name="previousActiveDate"></param>
        /// <param name="currentActiveDate"></param>
        /// <param name="previousMonth"></param>
        /// <returns></returns>
        public void CalculateCurrentMonthandYearForWebMonthView(string monthViewNavigationDirection,string dateStringFromWebMonth,ref string currentMonth,ref string currentYear)
        {
            string[] dateDataArray = null;
            string currentDay = null;
            DateTime currentDate;

            try
            {
                //Wed Dec 10 00:00:00 UTC+0530 2008
                dateDataArray = dateStringFromWebMonth.Split(' ');

                if (dateDataArray.GetUpperBound(0) > 0)
                {
                    currentMonth = dateDataArray[1].ToString();

                    currentDay = dateDataArray[2].ToString();

                    //currentYear = dateDataArray[5].ToString();
                    currentYear = dateDataArray[3].ToString();

                    currentDate = Convert.ToDateTime(currentMonth + "-" + currentDay + "-" + currentYear);

                    if (monthViewNavigationDirection.ToLower() == "prev")
                    {
                        currentDate = currentDate.AddMonths(-1);
                    }
                    else if (monthViewNavigationDirection.ToLower() == "next")
                    {
                        currentDate = currentDate.AddMonths(1);
                    }
                    else
                    {
                        currentDate = currentDate;
                    }

                    currentMonth = currentDate.Month.ToString();
                    currentYear = currentDate.Year.ToString();
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        

        /// <summary>
        /// Method is created by Nitin,to get FromDate of the week,to which active date belongs to 
        /// for WeekView
        /// </summary>
        /// <param name="activeDate"></param>
        /// <returns></returns>
        public string GetFromDate(DateTime activeDate)
        {
            DateTime datFrom;
            try
            {
                datFrom = activeDate;

                while (datFrom.DayOfWeek.ToString() != "Monday")
                    datFrom = datFrom.AddDays(-1);

                return datFrom.ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        /// <summary>
        /// Method is created by Nitin,to get ToDate of the week,to which active date belongs to 
        /// for WeekView
        /// </summary>
        /// <param name="activeDate"></param>
        /// <returns></returns>
        public string GetToDate(DateTime activeDate)
        {
            DateTime datTo;
            try
            {
                datTo = activeDate;

                while (datTo.DayOfWeek.ToString() != "Sunday")
                    datTo = datTo.AddDays(1);

                return datTo.ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        

        #endregion
    }
}
