﻿/********************************************************************************
    Class Name: CurrentWebSchedule.cs 
    Author    : Nitin 
    Created on: 10-Sept-2008
 *  Purpose   :  This class cotains properties,which are filled from UI ,to decide behaviour
 *               action of current selected control
 ********************************************************************************
*/

using System;

/// <summary>
/// Summary description for CurrentWebSchedule
/// </summary>
/// 
namespace Riskmaster.UI.Diaries.DiaryCalendar
{
    public class CurrentWebSchedule
    {
        public CurrentWebSchedule()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string CurrentControl
        {
            get;
            set;
        }

        public string NavigationMonth
        {
            get;
            set;
        }

        public string NavigationYear
        {
            get;
            set;
        }

        public string FromDate
        {
            get;
            set;
        }

        public string ToDate
        {
            get;
            set;
        }

        public string CurrentActiveDate
        {
            get;
            set;
        }

        public string Action
        {
            get;
            set;
        }

        public bool checkBESenablity
        {
            get;
            set;
        }
        public bool FromStyle
        {
            get;
            set;
        }
    }
}