﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class PeekDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            XmlNodeList avialableUserList = null;

            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PeekDiary.aspx"), "PeekDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "PeekDiaryValidations", sValidationResources, true);

                if (!IsPostBack)
                {
                    //Starts changed for Mits 14729 by Nitin on 08-April-09

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";
                    }
                    //Ended changed for Mits 14729 by Nitin on 08-April-09
                    
                    diaryDoc = LoadDiary();
                    avialableUserList = diaryDoc.SelectNodes("ResultMessage/Document/diaries/Users/User");

                    if (avialableUserList.Count == 0)
                    {
                        lblPermission.Visible = true;
                       // lblPermission.Text = "You do not have sufficient rights to peek at any user diaries.";

                        txtAssignedUser.Visible = false;
                        cmdAddCustomizedUser.Visible = false;
                    }
                    else
                    {
                        lblPermission.Visible = false;
                        txtAssignedUser.Visible = true;
                        cmdAddCustomizedUser.Visible = true;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "WPAAdaptor.GetAvailablePeekList";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForLoadDiary()
        {
            XmlDocument loadDiaryDoc = null;
            XmlNode loadDiaryNode = null;

            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("Diary");

                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //changed for Mits 14729 by Nitin on 08-April-09
                Server.Transfer("DiaryList.aspx?attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                //changed for Mits 14729 by Nitin on 08-April-09
                Server.Transfer("DiaryList.aspx?assigneduser=" + hdnAssigneduser.Value + "&ispeekdiary=true&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


    }
}
