﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttachDiarySuccessful.aspx.cs" Inherits="Riskmaster.UI.Diaries.AttachDiarySuccessful" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <%--Changed for MITS 14770--%>
      <script type="text/javascript">
          function SetOpenDiaryFlag() {              
          if(window.opener.document.getElementById('containsopendiaries')!=null)
            window.opener.document.getElementById('containsopendiaries').value = true;
           }
	    </script>		
    
</head>
<body method="post" onload="SetOpenDiaryFlag();">
    <br /><br /><br />
        <h3 align="center">
          <asp:Label ID="lblAttachdiaryResrc"  runat="server" Text="<%$ Resources:lblAttachdiaryResrc %>"></asp:Label> 
       </h3>
	<br /><br />
	
        <form>
                <table width="100%">
                    <tr>
                        <td align="center">
                             <input type="button" class="button" value="<%$ Resources:btnClose %>" onclick="self.close();" />
                        </td>
                    </tr>
                </table>            
        </form>
    
</body>
</html>
