﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class DiaryDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;
            XmlNode loadDiaryNode = null;
            XmlNode loggedInUser = null;

            try
            {
                //Rakhel ML Changes //igupta3 jira 439
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DiaryDetails.aspx"), "DiaryDetailsValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DiaryDetailsValidations", sValidationResources, true);
                //Rakhel ML Changes

                if (!IsPostBack)
                {
                    // divGrid.Attributes.Add("style", "overflow:auto;height:245px;width:99.5%;border-width:'0'");
                    // divGrid.Attributes.Add("style", "overflow:auto;height:315px;width:99.5%;border-width:'0'");//Changed by Amitosh for mits 23655 (05/02/2011)
                    //divGrid.Attributes.Add("style", "height:auto;width:99.5%;border-width:'0'");// Changed by Ishan (22/12/2011)
                    //divGrid.Attributes.Add("class", "divScroll");
                    //igupta3 jira 439
                    if (Request.QueryString["Routeableflag"] != null)
                    {
                        txtNotRouteableFlag.Text = Request.QueryString["Routeableflag"];
                    }
                    else
                    {
                        txtNotRouteableFlag.Text = "";
                    }
                    if (Request.QueryString["Rollableflag"] != null)
                    {
                        txtNotRollableFlag.Text = Request.QueryString["Rollableflag"];
                    }
                    else
                    {
                        txtNotRollableFlag.Text = "";
                    }


                    if (Request.QueryString["entryid"] != null)
                    {
                        ViewState["entryid"] = Request.QueryString["entryid"];
                    }
                    else
                    {
                        ViewState["entryid"] = "";
                    }


                    if (Request.QueryString["assigninguser"] != null)
                    {
                        ViewState["assigninguser"] = Request.QueryString["assigninguser"];
                    }
                    else
                    {
                        ViewState["assigninguser"] = "";
                    }


                    if (Request.QueryString["attachprompt"] != null)
                    {
                        ViewState["attachprompt"] = Request.QueryString["attachprompt"];
                    }
                    else
                    {
                        ViewState["attachprompt"] = "";
                    }

                    if (Request.QueryString["creationdate"] != null)
                    {
                        ViewState["creationdate"] = Request.QueryString["creationdate"];
                    }
                    else
                    {
                        ViewState["creationdate"] = "";
                    }

                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }


                    diaryDoc = LoadDiary();

                    loadDiaryNode = diaryDoc.SelectSingleNode("ResultMessage/Document/LoadDiary");

                    if (loadDiaryNode != null)
                    {
                        SetPageDisplayStyle(loadDiaryNode);
                    }


                    loggedInUser = loadDiaryNode.SelectSingleNode("//LoggedInUser");
                    if (String.IsNullOrEmpty(ViewState["assigneduser"].ToString()) && (loggedInUser != null))
                    {
                        if (!String.IsNullOrEmpty(loggedInUser.InnerText))
                        {
                            ViewState["assigneduser"] = loggedInUser.InnerText;
                        }
                    }
                    
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlNode GetInputDocForLoadDiary()
        {
            //<LoadDiary base="" entryid="128" notify="false" root="" />

            XmlDocument loadDiaryDoc = null;

            XmlNode loadDiaryNode = null;
            XmlAttribute atbBase = null;
            XmlAttribute atbEntryId = null;
            XmlAttribute atbNotify = null;
            XmlAttribute atbRoot = null;

            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("LoadDiary");

                atbBase = loadDiaryDoc.CreateAttribute("base");
                atbBase.Value = "";

                atbEntryId = loadDiaryDoc.CreateAttribute("entryid");
                atbEntryId.Value = ViewState["entryid"].ToString();

                atbNotify = loadDiaryDoc.CreateAttribute("notify");
                atbNotify.Value = "";

                atbRoot = loadDiaryDoc.CreateAttribute("root");
                atbRoot.Value = "";

                loadDiaryNode.Attributes.Append(atbBase);
                loadDiaryNode.Attributes.Append(atbEntryId);
                loadDiaryNode.Attributes.Append(atbNotify);
                loadDiaryNode.Attributes.Append(atbRoot);

                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.LoadDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);

                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private void SetPageDisplayStyle(XmlNode loadDiaryNode)
        {
            XmlNode taskNameNode = null;
            XmlNode createdOnNode = null;
            XmlNode dueDateNode = null;
            XmlNode dueTimeNode = null; //tmalhotra2 mits 25540
            XmlNodeList workActivitiesNodeList = null;
            XmlNode statusOpenNode = null;
            XmlNode estimateTimeNode = null;
            XmlNode priorityNode = null;
            XmlNode notesNode = null;
            XmlNode regardingNode = null;
            XmlNode completeOnNode = null;
            XmlNode completionResponseNode = null;
            XmlNode teTrackNode = null;
            XmlNode teStartTimeNode = null;
            XmlNode teEndTimeNode = null;
            XmlNode teTotalTimeSpendNode = null;
            XmlNode teExpensesNode = null;
            XmlNode completedbyUserNode = null;
            XmlNode assigningUserNode = null; //tmalhotra3- MITS 25091
            //igupta3 jira 439
            XmlNode notroute = null;
            XmlNode notroll = null;

            string imageUrl = "";
            
            taskNameNode = loadDiaryNode.SelectSingleNode("TaskSubject");
            if (taskNameNode != null)
            {
                lblTaskName.Text = taskNameNode.InnerText;
            }

            createdOnNode = loadDiaryNode.SelectSingleNode("CreateDate");
            if (createdOnNode != null)
            {
                lblCreatedOn.Text = createdOnNode.InnerText;
            }

            dueDateNode = loadDiaryNode.SelectSingleNode("CompleteDate");
            if (dueDateNode != null)
            {
                lblDueDate.Text = dueDateNode.InnerText;
            }
            //tmalhotra2 mits 25540
            dueTimeNode = loadDiaryNode.SelectSingleNode("CompleteTime");
            if (dueTimeNode != null)
            {
                lblDueTime.Text = dueTimeNode.InnerText;
            }
            //tmalhtora2 end
            //igupta3 jira 439
            notroute = loadDiaryNode.SelectSingleNode("NonRoutableFlag");
            if (notroute != null)
            {
                lblNotRoutable.Text = notroute.InnerText;

            }
            notroll = loadDiaryNode.SelectSingleNode("NonRollableFlag");
            if (notroll != null)
            {
                lblNotRollable.Text = notroll.InnerText;
            }
            

            workActivitiesNodeList = loadDiaryNode.SelectNodes("Activities/Activity/ActivityName");
            foreach (XmlNode activityNode in workActivitiesNodeList)
            {
                lblWorkActivities.Text = lblWorkActivities.Text + activityNode.InnerText + "<br/>";
            }

            estimateTimeNode  = loadDiaryNode.SelectSingleNode("EstimateTime");
            if (estimateTimeNode != null)
            {
               lblEstimateTime.Text = estimateTimeNode.InnerText;
            }

            priorityNode = loadDiaryNode.SelectSingleNode("Priority");
            //tmalhotra2 15/11/2011 mits 26248 start:
           
            
            if (priorityNode != null)
            {
                //Aman MITS 31225
                if (priorityNode.InnerText == "2")
                {   
                    //imgPriority.Src = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Images\\important.gif";
                    imgPriority.Src = "~/Images/important.gif";
                    //lblPriority.Text = "Important";                    
                    lblPriority.Text = RMXResourceProvider.GetSpecificObject("Important", RMXResourceProvider.PageId("DiaryDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                }
                else if (priorityNode.InnerText == "3")
                {
                   // imgPriority.Src = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Images\\required.gif";
                    imgPriority.Src = "~/Images/required.gif";
                    //lblPriority.Text = "Required";
                    lblPriority.Text = RMXResourceProvider.GetSpecificObject("Required", RMXResourceProvider.PageId("DiaryDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                }
                //tmalhotra2 15/11/2011 mits 26248 end:
                else
                {
                    imgPriority.Visible = false;
                    //lblPriority.Text = "Optional";
                    lblPriority.Text = RMXResourceProvider.GetSpecificObject("Optional", RMXResourceProvider.PageId("DiaryDetails.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                }
                //Aman MITS 31225
            }

            notesNode = loadDiaryNode.SelectSingleNode("TaskNotes");
            if (notesNode != null)
            {
                lblNotes.Text = notesNode.InnerText;
            }

            regardingNode = loadDiaryNode.SelectSingleNode("Regarding ");
            if (regardingNode != null)
            {
                lblRegarding.Text = regardingNode.InnerText;
            }

            statusOpenNode = loadDiaryNode.SelectSingleNode("StatusOpen");
            if (statusOpenNode != null)
            {
                if (statusOpenNode.InnerText.ToLower() == "false")
                {
                    //Ashish ahuja - Mits 33984 start
                    //lblStatusMsg.Visible = true;
                    lblDiaryStatusMsg.Visible = true;
                    lblStatusMsg.Visible = false;
                    //Ashish ahuja - Mits 33984 end
                   // lblDiaryStatusMsg.Text = "This diary has been completed. Completion details below.";
                    tblCompleteDiary.Visible = true;

                    completeOnNode = loadDiaryNode.SelectSingleNode("ResponseDate");
                    if (completeOnNode != null)
                    {
                        lblCompletedOn.Text = completeOnNode.InnerText;
                    }

                    completionResponseNode = loadDiaryNode.SelectSingleNode("Response");
                    if (completionResponseNode != null)
                    {
                        lblCompletionResponse.Text = completionResponseNode.InnerText;
                    }
                    completedbyUserNode = loadDiaryNode.SelectSingleNode("CompletedByUser");
                    if (completedbyUserNode != null)
                    {
                        lblCompletedByUser.Text = completedbyUserNode.InnerText;
                    }
                    assigningUserNode = loadDiaryNode.SelectSingleNode("AssigningUser"); //tmalhotra3- MITS 25091
                    if (assigningUserNode != null)
                    {
                        lblAssigningUser.Text = assigningUserNode.InnerText;
                    }
                    btnClose.Visible = true;
                    
                    btnEdit.Visible = false;
                    btnComplete.Visible = false;
                    btnRoll.Visible = false;
                    btnRoute.Visible = false;
                    btnCancel.Visible = false;
                    btnPrint.Visible = false;//asingh263 mits 34874
                }
                else
                {
                    //Ashish ahuja - Mits 33984 start
                    //lblDiaryStatusMsg.Visible = true;
                    lblStatusMsg.Visible = true;
                    lblDiaryStatusMsg.Visible = false;
                    //Ashish ahuja - Mits 33984 end
                   // lblDiaryStatusMsg.Text = "This is an Open Diary.";
                    tblCompleteDiary.Visible = false;

                    btnClose.Visible = false;

                    btnEdit.Visible = true;
                    btnComplete.Visible = true;
                    btnRoll.Visible = true;
                    btnRoute.Visible = true;
                    btnCancel.Visible = true;
                    btnPrint.Visible = true;//asingh263 mits 34874
                }
            }
            else
            {
                tblCompleteDiary.Visible = false;
                btnClose.Visible = false;
            }


            teTrackNode = loadDiaryNode.SelectSingleNode("TeTracked");
            if (teTrackNode != null)
            {
                if (teTrackNode.InnerText.ToLower() == "true")
                {
                    tblTeTracked.Visible = true;

                    teStartTimeNode = loadDiaryNode.SelectSingleNode("TEStartTime");
                    if (teStartTimeNode != null)
                    {
                        lblTeStartTime.Text = teStartTimeNode.InnerText;
                    }

                    teEndTimeNode = loadDiaryNode.SelectSingleNode("TEEndTime");
                    if (teEndTimeNode != null)
                    {
                        lblTeEndTime.Text = teEndTimeNode.InnerText;
                    }

                    teTotalTimeSpendNode = loadDiaryNode.SelectSingleNode("TeTotalHours");
                    if (teTotalTimeSpendNode != null)
                    {
                        lblTotalTimeSpent.Text = teTotalTimeSpendNode.InnerText;
                    }

                    teExpensesNode = loadDiaryNode.SelectSingleNode("TeExpenses");
                    if (teExpensesNode != null)
                    {
                        lblTeExpenses.Text = teExpensesNode.InnerText; // Modified by csingh7 MITS 14639
                    }
                }
                else
                {
                    tblTeTracked.Visible = false;
                }
            }
            else
            {
                tblTeTracked.Visible = false;
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("EditDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&attachprompt=" + ViewState["attachprompt"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("CompleteDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnRoute_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("RouteDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnRoll_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("RollDiary.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&creationdate=" + ViewState["creationdate"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("DiaryHistory.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        //asingh263 mits 34874 starts
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            
            try
            {
                //Server.Transfer("PrintListing.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&creationdate=" + ViewState["creationdate"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() +"&screen=selected", false);

                string script = "PrintListing.aspx?entryid=" + ViewState["entryid"].ToString() + "&assigninguser=" + ViewState["assigninguser"].ToString() + "&creationdate=" + ViewState["creationdate"].ToString() + "&assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString() + "&screen=selected";
                ClientScript.RegisterStartupScript(this.GetType(), "popup", "window.open('" + script + "','DiaryList','width=800,height=800' + ',top=' + (screen.availHeight - 800) / 2 + ',left=' + (screen.availWidth - 800) / 2 + ',resizable=yes,scrollbars=yes')", true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //asingh263 mits 34874 ends
    }
}
