﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PeekDiary.aspx.cs" Inherits="Riskmaster.UI.Diaries.PeekDiary" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Peek Diary</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/cul.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery/jquery-1.8.0.js"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
          function Validate()
          {
            if(document.forms[0].txtAssignedUser != null)
            {
              if (document.forms[0].txtAssignedUser.value == "")
              {
                  //window.alert("No Peek Target was selected.\n Please select a user and try again or press 'Cancel' to return to your own messages.");
                  window.alert(PeekDiaryValidations.NoPeekTarget); 
                return false;
              }
              
            }
            else if(document.forms[0].txtAssignedUser == null)
            {
                //window.alert("No Peek Target was selected.\n Please select a user and try again or press 'Cancel' to return to your own messages.");
                window.alert(PeekDiaryValidations.NoPeekTarget); 
              return false;
            }
          }
          
          function OnPeek(TargetUID, TargetUserName)
          {
            document.forms[0].tid.value=TargetUID;
            document.forms[0].tun.value=TargetUserName;
            //document.forms[0].submit();
            return true;
          }
    </script>
    
</head>
<body>
<p class="formtitle"> <asp:Label ID="lblDiaryPeekTargetResrc"  runat="server" Text="<%$ Resources:lblDiaryPeekTargetResrc %>"></asp:Label> 
</p>
<asp:Label ID="lblDiaryPeekingResrc"  runat="server" Text="<%$ Resources:lblDiaryPeekingResrc %>"></asp:Label> 
<br/><br/>

<br/><i><asp:Label ID="lblDiaryNoteResrc"  runat="server" Text="<%$ Resources:lblDiaryNoteResrc %>"></asp:Label> 

</i>
<form id="frmData"  runat="server">
    <table border="0">
        <tr>
            <td colspan="3" >
                <uc1:ErrorControl ID="ErrorControl" runat="server" />
            </td>
        </tr>
        <%--<tr>
		<!--Ankit Start : Worked on MITS - 31646-->
        	<td colspan="2" align="left">
            <asp:ImageButton ID="btnOk" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip="<%$ Resources:ttSave %>"
                        runat="server" OnClientClick="return Validate();" OnClick="btnOk_Click" />
                   <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:ttCancel %>"
                        runat="server" OnClick="btnCancel_Click" />
            </td>
        </tr>--%>
        <!--Ankit End-->
		<tr>
        <td colspan="3" class="ctrlgroup">
         <asp:Label ID="lblDiaryPeekResrc"  runat="server" Text="<%$ Resources:lblDiaryPeekResrc %>"></asp:Label> 
           <%-- <td colspan="3" class="ctrlgroup">Diary Peek Target Selection</td>--%>
           </td>
        </tr>
        <tr>
             <td width="10%"></td>
             <td width="1%"></td>
             <td width="89%"></td>
        </tr>
        <tr>
         
            <td colspan="3">  <asp:Label ID="lblAvlUsersResrc"  runat="server" Text="<%$ Resources:lblAvlUsersResrc %>"></asp:Label> </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <input type="text" runat="server" value="" id="txtAssignedUser"/>               
                <input type="text"  runat="server" value="" id="hdnAssigneduser" style="display:none" enableviewstate="true"/>  
                <input type="button"  runat="server" class="CodeLookupControl" value="" id="cmdAddCustomizedUser" onclick="AddCustomizedListUser('peekdiary', 'hdnAssigneduser', '', 'txtAssignedUser')"/>
                <asp:Label ID="lblPermission" runat="server" Visible="false"  Font-Italic ="true" Text="<%$ Resources:lblPermission %>"></asp:Label>
            </td>
        </tr>
        <!--Ankit Start : Worked on MITS - 31646-->
       <tr>
            <td colspan="3" align="left">
            <asp:Button ID="btnOk" Text="Show Diaries" class="bold" runat="server" OnClientClick="return Validate();" OnClick="btnOk_Click" />
            <asp:Button ID="btnCancel" Text="Cancel" class="bold" runat="server" OnClick="btnCancel_Click" />
            </td>
       </tr>
       <!--Ankit End-->
       </table>
</form>
</body>
</html>
