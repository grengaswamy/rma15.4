﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.Diaries
{
    public partial class RouteDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("RouteDiary.aspx"), "RouteDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "RouteDiaryValidations", sValidationResources, true);

                if (!IsPostBack)
                {
                    //07/26/2011 SMISHRA54: WPA Email Notification
                     //08/16/2012 SGUPTA243: MITS 28766
                    //chkEmailNotification.Checked = true;
                    chkEmailNotification.Checked = false;
                    //07/26/2011 SMISHRA54: WPA Email Notification

                    if (Request.QueryString["entryid"] != null)
                    {
                        ViewState["entryid"] = Request.QueryString["entryid"];
                    }
                    else
                    {
                        ViewState["entryid"] = "";
                    }

                    if (Request.QueryString["assigninguser"] != null)
                    {
                        ViewState["currentuser"] = Request.QueryString["assigninguser"];
                    }
                    else
                    {
                        ViewState["currentuser"] = "";
                    }

                    if (Request.QueryString["assigneduser"] != null)
                    {
                        ViewState["assigneduser"] = Request.QueryString["assigneduser"];
                    }
                    else
                    {
                        ViewState["assigneduser"] = "";
                    }

                    if (Request.QueryString["attachtable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["attachtable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";//"EVENT";
                    }

                    if (Request.QueryString["attachrecordid"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["attachrecordid"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";// "1";
                    }

                    if (Request.QueryString["ispeekdiary"] != null)
                    {
                        ViewState["ispeekdiary"] = Request.QueryString["ispeekdiary"];
                    }
                    else
                    {
                        ViewState["ispeekdiary"] = "false";
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlDocument RouteCurrentDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.RouteDiary";

                inputDocNode = GetInputDocForRouteDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForRouteDiary()
        {
            XmlDocument routeDairyDoc = null;
            XmlNode routeDiaryNode = null;
            XmlNode entryIdNode = null;
            XmlNode assignedUserNode = null;
            XmlNode assignedGroupNode = null;//pkandhari Jira 6412
            XmlNode taskNotesNode = null;
            XmlNode activityStringNode = null;
            XmlNode actionNode = null;
            //07/26/2011 SMISHRA54 : WPA Email Notification
            XmlNode notifyByMailNode = null;
            //07/26/2011 SMISHRA54 : WPA Email Notification End
            routeDairyDoc = new XmlDocument();

            routeDiaryNode = routeDairyDoc.CreateElement("RouteDiary");

            entryIdNode = routeDairyDoc.CreateElement("EntryId");
            entryIdNode.InnerText = ViewState["entryid"].ToString();
            routeDiaryNode.AppendChild(entryIdNode);

            assignedUserNode = routeDairyDoc.CreateElement("AssignedUser");
            //assignedUserNode.InnerText = hdnAssigneduser.Value;
            assignedUserNode.InnerText = hdnDiaryLstUsers.Value.ToString();//pkandhari Jira 6412
            routeDiaryNode.AppendChild(assignedUserNode);

            //pkandhari Jira 6412 starts
            assignedGroupNode = routeDairyDoc.CreateElement("AssignedGroup");
            assignedGroupNode.InnerText = hdnDiaryLstGroups.Value.ToString();
            routeDiaryNode.AppendChild(assignedGroupNode);
            //pkandhari Jira 6412 ends  

            taskNotesNode = routeDairyDoc.CreateElement("TaskNotes");
            //Mits 14637 starts by Nitin 13-04-2009
            taskNotesNode.InnerText = " Routed from " + ViewState["assigneduser"].ToString() + " on " + System.DateTime.Now.ToShortDateString();
            //Mits 14637 ends by Nitin 13-04-2009
            routeDiaryNode.AppendChild(taskNotesNode);

            activityStringNode = routeDairyDoc.CreateElement("ActivityString");
            routeDiaryNode.AppendChild(activityStringNode);

            actionNode = routeDairyDoc.CreateElement("Action");
            actionNode.InnerText = "Route";
            routeDiaryNode.AppendChild(actionNode);

            //07/26/2011 SMISHRA54 : WPA Email Notification
            notifyByMailNode = routeDairyDoc.CreateElement("EmailNotifyFlag");
            if (chkEmailNotification.Checked)
            {
                notifyByMailNode.InnerText = "true";
            }
            else
            {
                notifyByMailNode.InnerText = "";
            }
            routeDiaryNode.AppendChild(notifyByMailNode);

            //07/26/2011 SMISHRA54 : WPA Email Notification End

            routeDairyDoc.AppendChild(routeDiaryNode);  

            return routeDiaryNode;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnRoute_Click(object sender, EventArgs e)
        {
            XmlDocument responseDoc = null;
            string sDiaryError = string.Empty;

            try
            {
                responseDoc = RouteCurrentDiary();

                if (responseDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    Server.Transfer("DiaryList.aspx?assigneduser=" + ViewState["assigneduser"].ToString() + "&ispeekdiary=" + ViewState["ispeekdiary"].ToString() + "&attachtable=" + ViewState["attachtable"].ToString() + "&attachrecordid=" + ViewState["attachrecordid"].ToString(), false);
                }
                else
                {
                    sDiaryError = responseDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText;
                    if (!string.IsNullOrEmpty(sDiaryError))
                    {
                        throw new ApplicationException(sDiaryError);
                    }
                    else
                    {
                        throw new ApplicationException(responseDoc.InnerXml);
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
