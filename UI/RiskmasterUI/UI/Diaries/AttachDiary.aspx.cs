﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Diaries
{
    public partial class AttachDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //Rakhel ML Changes
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AttachDiary.aspx"), "AttachDiaryValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "AttachDiaryValidations", sValidationResources, true);
            //Rakhel ML Changes
            //Praveen: ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture,this);
                }
            //Praveen: ML Changes

           
            XmlDocument diaryDoc = null;
            XmlNodeList wpaTaskNodeList = null;
   

            try
            {
                if (!IsPostBack)
                {
                    txtDueDate.Value = AppHelper.GetDate(System.DateTime.Today.ToString());
                    txtDueTime.Value = AppHelper.GetTime(System.DateTime.Now.TimeOfDay.ToString());

                    if (Request.QueryString["AttachTable"] != null)
                    {
                        ViewState["attachtable"] = Request.QueryString["AttachTable"];
                    }
                    else
                    {
                        ViewState["attachtable"] = "";
                    }

                    if (Request.QueryString["AttachRecordId"] != null)
                    {
                        ViewState["attachrecordid"] = Request.QueryString["AttachRecordId"];
                    }
                    else
                    {
                        ViewState["attachrecordid"] = "";//"EVENT";
                    }

                    if (Request.QueryString["AttSecRecId"] != null)
                    {
                        ViewState["attsecrecid"] = Request.QueryString["AttSecRecId"];
                    }
                    else
                    {
                        ViewState["attsecrecid"] = "";//"EVENT";
                    }
                    //Parijat : 19392 -Diaries  shud be with the claim numbetr and not id

                    if (Request.QueryString["AttachRecordName"] != null)
                    {
                        ViewState["AttachRecordName"] = Request.QueryString["AttachRecordName"];
                    }
                    else
                    {
                        ViewState["AttachRecordName"] = "";//"EVENT";
                    }

                    // npadhy Start MITS 23013 Attach Table Name for Policy Diaries when saved by system is different from when a user manually saves a diary.
                    if (Request.QueryString["AttachSubject"] != null)
                    {
                        ViewState["AttachSubject"] = Request.QueryString["AttachSubject"];
                    }
                    else
                    {
                        //"EVENT";
                        ViewState["AttachSubject"] = string.Empty;
                    }

                    //MITS 13345 - MWC - This is not a quick diary so we should not put in comments stating that it is a quick diary.
                    //txtSubject.Value = "QD for " + ViewState["attachtable"].ToString() + " " + ViewState["attachrecordid"].ToString();
                    //txtNotes.Value = "Quick Diary";
                    //Start: Neha Suresh Jain, 03/26/2010, MITS:20117
                    // txtSubject.Value = "Diary for " + ViewState["attachtable"].ToString() + " " + ViewState["attachrecordid"].ToString();
                    // npadhy Start MITS 23013 Attach Table Name for Policy Diaries when saved by system is different from when a user manually saves a diary.
                    if (ViewState["AttachSubject"].ToString() == string.Empty)
                    {
                        if (ViewState["AttachRecordName"].ToString() == string.Empty)
                        {
                            txtSubject.Value = ViewState["attachtable"].ToString() + " " +ViewState["attachrecordid"].ToString();
                        }
                        else//Parijat : 19392 -Diaries  shud be with the claim numbetr and not id
                        {
                            txtSubject.Value = ViewState["attachtable"].ToString() +" : "+ ViewState["AttachRecordName"].ToString();
                        }
                    }
                    else
                    {
                        if (ViewState["AttachRecordName"].ToString() == string.Empty)
                        { 
                            txtSubject.Value = ViewState["AttachSubject"].ToString() +" "+ ViewState["attachrecordid"].ToString();
                        }
                        else//Parijat : 19392 -Diaries  shud be with the claim numbetr and not id
                        {
                            txtSubject.Value = ViewState["AttachSubject"].ToString()+ " : " + ViewState["AttachRecordName"].ToString();
                        }
                    }
                    // npadhy End MITS 23013 Attach Table Name for Policy Diaries when saved by system is different from when a user manually saves a diary.
                    //end:Neha Suresh Jain    
                    diaryDoc = LoadDiary();

                    wpaTaskNodeList = diaryDoc.SelectNodes("ResultMessage/Document/GetUsersResult/WpaTaskList/WpaTask");

                    if (wpaTaskNodeList.Count > 0)
                    {
                        foreach (XmlNode wpaTaskNode in wpaTaskNodeList)
                        {
                            txtSubjectDcboBox.Items.Add(wpaTaskNode.Attributes["code_desc"].Value);
                        }

                        this.Page.ClientScript.RegisterStartupScript(typeof(string), "WpaTaskIsSelect", "_isSelect = true;", true);
                    }
                    else
                    {
                        txtSubjectDcboBox.Visible = false;
                    }

                    //07/26/2011 SMISHRA54: WPA Email Notification
                    //  chkEmailNotification.Checked = true  //Commented by Amitosh for MITS 27485  
                    //07/26/2011 SMISHRA54: WPA Email Notification
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlDocument LoadDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            int iIterator = 0;
            try
            {
                serviceMethodToCall = "WPAAdaptor.GetUsers";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForLoadDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                LoginID.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@LoginID").InnerText;
                LoginUserName.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@LoginUser").InnerText;
                DefaultAssignedTo.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@DefaultAssignedTo").InnerText;

                //zmohammad MITs 33655 Diary Initialisation script : Start
                if (!string.IsNullOrEmpty(DefaultAssignedTo.Value) && DefaultAssignedTo.Value == "True" && LoginUserName.Value != "")
                {
                    if (lstUsers.Items.Count == 0)
                    {
                        UserStr.Value = LoginUserName.Value;
                        UserIdStr.Value = LoginID.Value;
                    }
                }

                // rrachev JIRA 4607
                //XmlNode mailDisabled = diaryDoc.SelectSingleNode("//GetUsersResult/@MAIL_DISABLED");
                //if (mailDisabled != null && (mailDisabled.InnerText.ToLower() == "true"))
                //{
                //    chkAutoConfrim.Disabled = true;
                //}
                //dvatsa-JIRA 11627(start)
                XmlNode mailEnabled = diaryDoc.SelectSingleNode("//GetUsersResult/@MAIL_ENABLED");
                if (mailEnabled != null && (mailEnabled.InnerText.ToLower() == "true"))
                {
                    chkAutoConfrim.Disabled = false;
                    chkEmailNotification.Disabled = false;
                    chkDiaryOverDueNotify.Disabled = false;
                }
                else
                {
                    chkAutoConfrim.Disabled = true;
                    chkEmailNotification.Disabled = true;
                    chkDiaryOverDueNotify.Disabled = true;
                }
               //dvatsa-JIRA 11627(end)
                if (diaryDoc.SelectSingleNode("//GetUsersResult/@ScriptFired").InnerText == "true")
                {
                    if (!string.IsNullOrEmpty(diaryDoc.SelectSingleNode("//GetUsersResult/@TaskSubject").InnerText))
                    { txtSubject.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@TaskSubject").InnerText; }

                    txtNotes.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@TaskNotes").InnerText;

                    if (!string.IsNullOrEmpty(diaryDoc.SelectSingleNode("//GetUsersResult/@CompleteDate").InnerText))
                    { txtDueDate.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@CompleteDate").InnerText; }

                    if (!string.IsNullOrEmpty(diaryDoc.SelectSingleNode("//GetUsersResult/@CompleteTime").InnerText))
                    { txtDueTime.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@CompleteTime").InnerText; }

                    ddlPriority.SelectedValue = diaryDoc.SelectSingleNode("//GetUsersResult/@Priority").InnerText;

                    if (!string.IsNullOrEmpty(diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUser").InnerText))
                    {

                        if (string.IsNullOrEmpty(UserStr.Value))
                        {
                            UserStr.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUser").InnerText;
                        }
                        else
                        {
                            UserStr.Value = UserStr.Value + ' ' + diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUser").InnerText;
                        }
                    }
                    if (!string.IsNullOrEmpty(diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUserID").InnerText))
                    {
                        if (string.IsNullOrEmpty(UserIdStr.Value))
                        {
                            UserIdStr.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUserID").InnerText;
                        }
                        else
                        {
                            UserIdStr.Value = UserIdStr.Value + ' ' + diaryDoc.SelectSingleNode("//GetUsersResult/@AssignedUserID").InnerText;
                        }
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@EstimateTime").InnerText != "0")
                    {
                        txtEstTime.Value = diaryDoc.SelectSingleNode("//GetUsersResult/@EstimateTime").InnerText;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@NotifyFlag").InnerText.ToLower() == "true")
                    {
                        chkEmailNotification.Checked = true;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@AutoConfirm").InnerText.ToLower() == "true")
                    {
                        chkAutoConfrim.Checked = true;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@OverDueDiaryNotify").InnerText.ToLower() == "true")
                    {
                        chkDiaryOverDueNotify.Checked = true;
                    }
                    //igupta3 jira 439
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@NonRoutableFlag").InnerText.ToLower() == "true")
                    {
                        chkNotRoutable.Checked = true;
                    }
                    if (diaryDoc.SelectSingleNode("//GetUsersResult/@NonRollableFlag").InnerText.ToLower() == "true")
                    {
                        chkNotRollable.Checked = true;
                    }
                }

                string[] arr = UserStr.Value.Split(new char[] { ' ' });
                for (iIterator = 0; iIterator < arr.Length; iIterator++)
                {
                    if (!string.IsNullOrWhiteSpace(arr[iIterator]))
                    {
                        lstUsers.Items.Add(arr[iIterator]);
                    }
                }
                //zmohammad MITs 33655 Diary Initialisation script : End
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument SaveDiary()
        {
            string diaryServiceOutput = string.Empty;
            XmlDocument diaryDoc = null;
            DiariesHelper dHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            
            try
            {
                serviceMethodToCall = "WPAAdaptor.SaveDiary";

                //to be fill up by viewstate
                inputDocNode = GetInputDocForSaveDiary();

                dHelper = new DiariesHelper();
                diaryServiceOutput = dHelper.CallCWSFunctionForDiaries(inputDocNode, serviceMethodToCall);

                diaryDoc = new XmlDocument();
                diaryDoc.LoadXml(diaryServiceOutput);
                return diaryDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForSaveDiary()
        {
            XmlDocument saveDiaryDoc = null;
            XmlNode saveDiaryNode = null;

            XmlNode entryIdNode = null;
            XmlNode taskSubjectNode = null;
            XmlNode assigningUserNode = null;
            XmlNode assignedUserNode = null;
            XmlNode statusOpenNode = null;
            XmlNode dueDateNode = null;
            XmlNode completeTimeNode = null;
            XmlNode responseDateNode = null;
            XmlNode responseNode = null;
            XmlNode teTrackedNode = null;
            XmlNode teTotalHoursNode = null;
            XmlNode teExpensesNode = null;
            XmlNode teEndTimeNode = null;
            XmlNode startTimeNode = null;
            XmlNode taskNotesNode = null;
            XmlNode actionNode = null;
            XmlNode estimateTimeNode = null;
            XmlNode priorityNode = null;
            XmlNode issueCompletionNode = null;
            XmlNode issueDateTextNode = null;
            XmlNode notifyFlagNode = null;
            XmlNode attachRecordIdNode = null;
            XmlNode attachTableNode = null;
            XmlNode autoConfirmNode = null;
            //07/07/2011 MDHAMIJA : WPA Email Notification
            XmlNode notifyByMailNode = null;
            //07/07/2011 MDHAMIJA : WPA Email Notification End
            XmlNode activityStringNode = null;
            XmlNode sendOverDueDiaryNotification = null;
            //igupta3 jira 439
            XmlNode nonRoutable = null;
            XmlNode nonRollable = null;

            saveDiaryDoc = new XmlDocument();
            saveDiaryNode = saveDiaryDoc.CreateElement("SaveDiary");

            entryIdNode = saveDiaryDoc.CreateElement("EntryId");
            entryIdNode.InnerText = "";
            saveDiaryNode.AppendChild(entryIdNode);

            taskSubjectNode = saveDiaryDoc.CreateElement("TaskSubject");
            taskSubjectNode.InnerText = txtSubject.Value;
            saveDiaryNode.AppendChild(taskSubjectNode);

            assigningUserNode = saveDiaryDoc.CreateElement("AssigningUser");
            assigningUserNode.InnerText = "currentuser";
            saveDiaryNode.AppendChild(assigningUserNode);

            assignedUserNode = saveDiaryDoc.CreateElement("AssignedUser");
            assignedUserNode.InnerText = UserStr.Value.ToString();  
            saveDiaryNode.AppendChild(assignedUserNode);

            statusOpenNode = saveDiaryDoc.CreateElement("StatusOpen");
            statusOpenNode.InnerText = "1";
            saveDiaryNode.AppendChild(statusOpenNode);


            teTrackedNode = saveDiaryDoc.CreateElement("TeTracked");
            teTrackedNode.InnerText = "";
            saveDiaryNode.AppendChild(teTrackedNode);

            teTotalHoursNode = saveDiaryDoc.CreateElement("TeTotalHours");
            teTotalHoursNode.InnerText = "";
            saveDiaryNode.AppendChild(teTotalHoursNode);

            teExpensesNode = saveDiaryDoc.CreateElement("TeExpenses");
            teExpensesNode.InnerText = "";
            saveDiaryNode.AppendChild(teExpensesNode);

            startTimeNode = saveDiaryDoc.CreateElement("TeStartTime");
            startTimeNode.InnerText = "";
            saveDiaryNode.AppendChild(startTimeNode);

            teEndTimeNode = saveDiaryDoc.CreateElement("TeEndTime");
            teEndTimeNode.InnerText = "";
            saveDiaryNode.AppendChild(teEndTimeNode);

            responseDateNode = saveDiaryDoc.CreateElement("ResponseDate");
            responseDateNode.InnerText = "";
            saveDiaryNode.AppendChild(responseDateNode);

            responseNode = saveDiaryDoc.CreateElement("Response");
            responseNode.InnerText = "";
            saveDiaryNode.AppendChild(responseNode);

            taskNotesNode = saveDiaryDoc.CreateElement("TaskNotes");
            taskNotesNode.InnerText = txtNotes.Value;
            saveDiaryNode.AppendChild(taskNotesNode);

            dueDateNode = saveDiaryDoc.CreateElement("DueDate");
            dueDateNode.InnerText = AppHelper.GetRMDate(txtDueDate.Value);//mbahl3 Mits 31486
            saveDiaryNode.AppendChild(dueDateNode);

            completeTimeNode = saveDiaryDoc.CreateElement("CompleteTime");
            completeTimeNode.InnerText = txtDueTime.Value;
            saveDiaryNode.AppendChild(completeTimeNode);

            estimateTimeNode = saveDiaryDoc.CreateElement("EstimateTime");
            estimateTimeNode.InnerText = txtEstTime.Value;
            saveDiaryNode.AppendChild(estimateTimeNode);

            priorityNode = saveDiaryDoc.CreateElement("Priority");
            priorityNode.InnerText = ddlPriority.SelectedItem.Value;
            saveDiaryNode.AppendChild(priorityNode);


            activityStringNode = saveDiaryDoc.CreateElement("ActivityString");
            activityStringNode.InnerText = actstring.Value;
            saveDiaryNode.AppendChild(activityStringNode);

            if (optIssue2.Checked == true)
            {
                issueCompletionNode = saveDiaryDoc.CreateElement("IssueCompletion");
                issueCompletionNode.InnerText = "2";
                saveDiaryNode.AppendChild(issueCompletionNode);

                issueDateTextNode = saveDiaryDoc.CreateElement("IssueDateText");
                issueDateTextNode.InnerText = txtIssuePeriod.Value;
                saveDiaryNode.AppendChild(issueDateTextNode);
            }

            autoConfirmNode = saveDiaryDoc.CreateElement("AutoConfirm");
            notifyFlagNode = saveDiaryDoc.CreateElement("NotifyFlag");
            //07/07/2011 MDHAMIJA : WPA Email Notification
            notifyByMailNode = saveDiaryDoc.CreateElement("EmailNotifyFlag");
            //07/07/2011 MDHAMIJA : WPA Email Notification End

            if (chkAutoConfrim.Checked == true)
            {
                autoConfirmNode.InnerText = "true";
                notifyFlagNode.InnerText = "true";
            }
            else
            {
                autoConfirmNode.InnerText = "";
                notifyFlagNode.InnerText = "";
            }

            saveDiaryNode.AppendChild(autoConfirmNode);
            saveDiaryNode.AppendChild(notifyFlagNode);
            //07/07/2011 MDHAMIJA : WPA Email Notification
            if (chkEmailNotification.Checked)
            {
                notifyByMailNode.InnerText = "true";
            }
            else
            {
                notifyByMailNode.InnerText = "";
            }
            saveDiaryNode.AppendChild(notifyByMailNode);
            //07/07/2011 MDHAMIJA : WPA Email Notification End
            //added by amitosh for QBE Enhancement of OverDueDiary Notification
            sendOverDueDiaryNotification = saveDiaryDoc.CreateElement("OverDueDiaryNotify");
            if (chkDiaryOverDueNotify.Checked)
            {
                sendOverDueDiaryNotification.InnerText = "true";
            }
            else
            {
                sendOverDueDiaryNotification.InnerText = "";
            }
            saveDiaryNode.AppendChild(sendOverDueDiaryNotification);
            //igupta3 jira 439
            nonRoutable = saveDiaryDoc.CreateElement("NonRoutableFlag");
            if (chkNotRoutable.Checked)
            {
                nonRoutable.InnerText = "true";
            }
            else
            {
                nonRoutable.InnerText = "";
            }
            saveDiaryNode.AppendChild(nonRoutable);
            nonRollable = saveDiaryDoc.CreateElement("NonRollableFlag");
            if (chkNotRollable.Checked)
            {
                nonRollable.InnerText = "true";
            }
            else
            {
                nonRollable.InnerText = "";
            }
            saveDiaryNode.AppendChild(nonRollable);
            //igupta3 ends
            //End Amitosh
            if (ViewState["attachrecordid"].ToString() != "")
            {
                attachRecordIdNode = saveDiaryDoc.CreateElement("AttachRecordId");
                attachRecordIdNode.InnerText = ViewState["attachrecordid"].ToString();
                saveDiaryNode.AppendChild(attachRecordIdNode);

                attachTableNode = saveDiaryDoc.CreateElement("AttachTable");
                attachTableNode.InnerText = ViewState["attachtable"].ToString();
                saveDiaryNode.AppendChild(attachTableNode); 
            }

            saveDiaryDoc.AppendChild(saveDiaryNode);

            return saveDiaryNode;
        }

        private XmlNode GetInputDocForLoadDiary()
        {
            XmlDocument loadDiaryDoc = null;
            XmlNode loadDiaryNode = null;

            try
            {
                loadDiaryDoc = new XmlDocument();
                loadDiaryNode = loadDiaryDoc.CreateElement("Diary");

                loadDiaryDoc.AppendChild(loadDiaryNode);
                return loadDiaryNode;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XmlDocument diaryDoc = null;

            try
            {
                diaryDoc = SaveDiary();
                //Start MITS 31546 averma62 - To persist the value of work activity and User list box after the post back if any validation failure happens.
                string[] sUserIdList = UserIdStr.Value.Split(' '); // persist value for user list
                string[] sUserList = UserStr.Value.Split(' ');
                lstUsers.Items.Clear();
                for (int i = 0; i < sUserIdList.Length; i++)
                  {
                      lstUsers.Items.Add(new ListItem(sUserList[i],sUserIdList[i]));
                  }

                string[] sWorkIdList = actstring.Value.Split('|'); // persist value for work acitivity list
                ArrayList sWorkId = new ArrayList();
                ArrayList sWorkTest = new ArrayList();
                    
                lstActivities.Items.Clear();
                for (int i = 0; i < sWorkIdList.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        sWorkId.Add(sWorkIdList[i]);
                    }
                    else
                    {
                        sWorkTest.Add(sWorkIdList[i]);
                    }
                }

                for (int j = 0; j < sWorkTest.Count; j++)
                {
                    lstActivities.Items.Add(new ListItem(sWorkTest[j].ToString(), sWorkId[j].ToString()));
                }
                sWorkId = null;
                sWorkTest = null;
                //End  MITS 31546 averma62 
                
                //if diarydoc has success message then redirect to diarylist 
                //else throw new execption with error messge in it

                if (diaryDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    //commented by Nitin for R6 implementation
                        //Server.Transfer("AttachDiarySuccessful.aspx", false);
               
                    //added by Nitin for R6 implementation
                    //Changed by Saurabh Arora for MITS 19061:Start
                    //this.Page.ClientScript.RegisterStartupScript(typeof(string), "AttachedDiary"," self.close();", true);
                    //MITS 25643: smishra54, 1 Sep 11
                    this.Page.ClientScript.RegisterStartupScript(typeof(string), "AttachedDiary", " if(window.opener.document.getElementById('containsopendiaries')!=null) window.opener.document.getElementById('containsopendiaries').value = true; if(window.opener.document.forms[0].id=='frmDiaryList') {window.opener.document.forms[0].__EVENTTARGET.value = 'attachdiary';window.opener.document.forms[0].submit();} self.close();", true);
                    //smishra54: End
                    //Changed by Saurabh Arora for MITS 19061:End
                }
                else
                {
                    //srajindersin MITS 26319 04/25/2012
                    ErrorControl.errorDom = diaryDoc.InnerXml;
                    //throw new ApplicationException(diaryDoc.InnerXml);
                    //srajindersin MITS 26319 04/25/2012

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    
    }
}
