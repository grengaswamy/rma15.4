﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManualPaymentDetails.aspx.cs" Inherits="Riskmaster.UI.NonOccPayments.ManualPaymentDetails" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head runat="server">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">   
        <title>Process Manual Check</title>
        <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css">
        <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css">
        <script language="JavaScript" SRC="../../Scripts/form.js"></script>
        <script language="JavaScript" SRC="../../Scripts/nonocc.js"></script>
        <script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
        <script src="../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
        <script src="../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
        <script type="text/javascript" src="../../Scripts/calendar-alias.js"></script>  
  </head>
 <body onload="ClosePopupIfPaymentProcessed('');">
 <form id="frmData" name= "frmData" runat="server"> 
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td>
      From Account:      
     </td>
     <td>
        <input type="text" name="chksetupaccount" id="chksetupaccount" disabled="true" runat="server"/>
     </td>     
     <td>
         <asp:Button class="button" runat="server" Text="Process" ID="cmdProcess" OnClientClick = "return processManualPayments();" onclick="cmdProcess_Click"/>
    </td>
    </tr>
    <tr>
     <td>
      Check Number      
     </td>
     <td>
     <!--pmital5 Mits 14848 04/20/09 - Changed onblur function -->
        <input type="text" name="chknumber" id="chknumber" onblur="checkNumLostFocus(this)" runat="server" maxlength="18"/>
     </td>
     <td>
       <asp:Button class="button" runat="server" Text="Cancel" ID="cmdCancel" OnClientClick = "window.close();"/>     
     </td>
    </tr>
    <tr>
     <td>
      Check Date      
     </td>
     <td>
        <input type="text" name="chkDate" id="chkDate" disabled="true" runat="server"/>
     </td>
    </tr>
   </table>  
    <input type="text" style="display:none" runat="server" id="autotransid" />
    <input type="text" style="display:none" runat="server" id="accountid" />
    <input type="text" style="display:none" runat="server" id="manualpaymentprocessedflag" />   
    <asp:textbox style="display:none" runat="server" id="hdnautosplitid" rmxref="Instance/Document/ProcessManualPayment/autosplitid" rmxtype="hidden" />           
    <asp:textbox style="display:none" runat="server" id="hdnaccountid" rmxref="Instance/Document/ProcessManualPayment/AccountID" rmxtype="hidden" />           
    <asp:textbox style="display:none" runat="server" id="hdnaccountname" rmxref="Instance/Document/ProcessManualPayment/AccountName" rmxtype="hidden" />           
    <asp:textbox style="display:none" runat="server" id="hdnchecknumber" rmxref="Instance/Document/ProcessManualPayment/CheckNumber" rmxtype="hidden" />           
    <asp:textbox style="display:none" runat="server" id="hdncheckdate" rmxref="Instance/Document/ProcessManualPayment/CheckDate" rmxtype="hidden" />           
    <asp:textbox style="display:none" runat="server" id="hdnautotransid" rmxref="Instance/Document/ProcessManualPayment/AutoTransID" rmxtype="hidden" />           
    <asp:textbox style="display:none" runat="server" id="hdnmanpaymentprocessed" rmxref="Instance/Document/ProcessManualPayment/ManualPaymentProcessed" rmxtype="hidden" />           
    <uc1:ErrorControl ID="ErrorControl" runat="server" />  
    <input type="text" style="display:none" runat="server" id="transiddetail" /> <!--igupta3 Mits 28566-->
   </form>    
 </body>
</html>
