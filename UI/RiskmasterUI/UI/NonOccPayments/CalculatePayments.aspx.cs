﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;

namespace Riskmaster.UI.NonOccPayments
{
    public partial class CalculatePayments : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        public bool bReturnStatus = false;
        public string sreturnValue = "";
        public string sClaimantTitle = "";
        public string sClaimNumber = "";
        public string sBencalcpaystart = "";
        public string sBencalcpayto = "";
        public string sClaimid = "";
        public string sTranstypecode = "";
        public string sAccountid = "";
        public string sTranstype = "";
        public string sAccount = "";
        public string sSysCmd = "";
        public string sBatchid = "";
        public bool sRecordExists = false;
        public bool sNegativePayments = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdnbenst.Text = AppHelper.GetQueryStringValue("benst");
                hdnbento.Text = AppHelper.GetQueryStringValue("bento");
                hdndWeeklyBenefit.Text = AppHelper.GetQueryStringValue("dWeeklyBenefit");
                hdnilblDaysIncluded.Text = AppHelper.GetQueryStringValue("ilblDaysIncluded");
                hdniNetPayment.Text = AppHelper.GetQueryStringValue("iNetPayment").Replace('-', '(').Replace("^^", ")");//skhare7 MITS 27497
                hdndSuppRate.Text = AppHelper.GetQueryStringValue("dSuppRate");
                hdnpensionoffset.Text = AppHelper.GetQueryStringValue("pensionoffset");
                hdnssoffset.Text = AppHelper.GetQueryStringValue("ssoffset");
                hdnoioffset.Text = AppHelper.GetQueryStringValue("oioffset");
                hdniGrossCalculatedPayment.Text = AppHelper.GetQueryStringValue("iGrossCalculatedPayment");
                hdniGrossCalculatedSupplement.Text = AppHelper.GetQueryStringValue("iGrossCalculatedSupplement");
                hdniGrossTotalNetOffsets.Text = AppHelper.GetQueryStringValue("iGrossTotalNetOffsets");
                hdnpayst.Text = AppHelper.GetQueryStringValue("payst");
                hdnpayto.Text = AppHelper.GetQueryStringValue("payto");
                hdniFederalTax.Text = AppHelper.GetQueryStringValue("iFederalTax");
                hdniSocialSecurityAmount.Text = AppHelper.GetQueryStringValue("iSocialSecurityAmount");
                hdniMedicareAmount.Text = AppHelper.GetQueryStringValue("iMedicareAmount");
                hdniStateAmount.Text = AppHelper.GetQueryStringValue("iStateAmount");
                hdnclaimid.Text = AppHelper.GetQueryStringValue("claimid");
                hdniDaysWorkingInWeek.Text = AppHelper.GetQueryStringValue("iDaysWorkingInWeek");
                hdniDaysWorkingInMonth.Text = AppHelper.GetQueryStringValue("iDaysWorkingInMonth");
                hdniOffsetCalc.Text = AppHelper.GetQueryStringValue("iOffsetCalc");
                hdnpieid.Text = AppHelper.GetQueryStringValue("pieid");
                hdntaxarray.Text = AppHelper.GetQueryStringValue("taxarray");
                hdntranstype.Text = AppHelper.GetQueryStringValue("transtype");
                hdnacct.Text = AppHelper.GetQueryStringValue("acct");
                hdndailyamount.Text = AppHelper.GetQueryStringValue("dailyamount");
                hdndailysuppamount.Text = AppHelper.GetQueryStringValue("dailysuppamount");
                hdntaxflags.Text = AppHelper.GetQueryStringValue("taxflags");
                hdndOther.Text = AppHelper.GetQueryStringValue("dOther");
                hdndSS.Text = AppHelper.GetQueryStringValue("dSS");
                hdndPension.Text = AppHelper.GetQueryStringValue("dPension");
                claimid.Value = AppHelper.GetQueryStringValue("claimid");
                taxarray.Value = AppHelper.GetQueryStringValue("taxarray");
                //pmittal5 Mits 14841 04/27/09 - GHS Enhancements
                hdnothoffset1.Text = AppHelper.GetQueryStringValue("othoffset1");
                hdnothoffset2.Text = AppHelper.GetQueryStringValue("othoffset2");
                hdnothoffset3.Text = AppHelper.GetQueryStringValue("othoffset3");
                hdnpsttaxded1.Text = AppHelper.GetQueryStringValue("psttaxded1");
                hdnpsttaxded2.Text = AppHelper.GetQueryStringValue("psttaxded2");
                hdndoffset1.Text = AppHelper.GetQueryStringValue("doffset1");
                hdndoffset2.Text = AppHelper.GetQueryStringValue("doffset2");
                hdndoffset3.Text = AppHelper.GetQueryStringValue("doffset3");
                hdndPostTaxDed1.Text = AppHelper.GetQueryStringValue("dPostTaxDed1");
                hdndPostTaxDed2.Text = AppHelper.GetQueryStringValue("dPostTaxDed2");
                hdnDistributionType.Text = AppHelper.GetQueryStringValue("distributiontype");
            }
            //End - pmittal5
            try
            {
                sSysCmd = AppHelper.GetQueryStringValue("syscmd");
                hdnsyscmd.Text = sSysCmd;

                if (!Page.IsPostBack || syscmd.Value == "6")
                {
                    hdnsyscmd.Text = syscmd.Value;
                    LoadCalculatePayments();
                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }       
      
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            hdnsyscmd.Text = syscmd.Value;
            LoadCalculatePayments();
        }

        private void LoadCalculatePayments()
        {
            XmlDocument objCalcPayments = new XmlDocument();
            XElement objTitle = null;     

            bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.CalculatePayments", out sreturnValue);

            if (bReturnStatus)
            {
                objCalcPayments.LoadXml(sreturnValue);
                rootElement = XElement.Parse(objCalcPayments.OuterXml);
                objTitle = rootElement.XPathSelectElement("//table");
                sSysCmd = objTitle.Attribute("syscmd").Value;

                if (sSysCmd == "1" || sSysCmd == "6")
                {
                    sClaimantTitle = objTitle.Attribute("claimant").Value;
                    sClaimNumber = objTitle.Attribute("claimnumber").Value;                   
                    sClaimid = objTitle.Attribute("claimid").Value;
                    sTranstypecode = objTitle.Attribute("transtypecode").Value;
                    sAccountid = objTitle.Attribute("accountid").Value;
                    sTranstype = objTitle.Attribute("transtype").Value;
                    sAccount = objTitle.Attribute("account").Value;
                    sBatchid = objTitle.Attribute("batchid").Value;
                    claimnumber.Value = sClaimNumber;
                    batchid.Value = sBatchid;
                    if(objTitle.Attribute("zerorecords")!=null)
                    {
                        sNegativePayments = Convert.ToBoolean(objTitle.Attribute("zerorecords").Value);  
                    }

                }
                if (rootElement.XPathSelectElement("//table").Nodes() != null)
                {
                    result = from objNode in rootElement.XPathSelectElement("//table").Nodes()
                             select objNode;
                    sRecordExists = true;
                }
                else
                {
                    sRecordExists = false;
                }
            }
        }
    }
}
