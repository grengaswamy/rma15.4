﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalculatePayments.aspx.cs" Inherits="Riskmaster.UI.NonOccPayments.CalculatePayments" %>

<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head runat="server">
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>   
          <title>Disability Payments</title>
          <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css"/>
          <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
          <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
          <script language="JavaScript" src="../../Scripts/nonocc.js" type="text/javascript"></script>
          <script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
          <script src="../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
          <script src="../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
          <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
          <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
          <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
          <script type="text/javascript" src="../../Scripts/calendar-alias.js"></script>
          <script type ="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">{ var i; } </script>  
</head>
 <body>
 <form id="frmData" runat="server"> 
  <input type="hidden" name="syscmd" value="1" id="syscmd" runat="server"/>
  <input type="hidden" name="claimnumber" id="claimnumber" runat="server"/>
  <input type="hidden" name="batchid" id="batchid" runat="server"/>
  <input type="hidden" name="claimid" id="claimid" runat="server"/>
  <input type="hidden" name="batchrestrict" id="batchrestrict" runat="server"/>
  <input type="hidden" name="taxarray" id="taxarray" runat="server"/>
  <input type="hidden" name="amountchange" id="amountchange" runat="server"/>
  
  <%if (sSysCmd == "6")
  {%>
  <script language="javascript" type="text/javascript">
    
    var lClaimId=document.forms[0].claimid.value;
    var taxarray=document.forms[0].taxarray.value;
    var claimnumber = document.forms[0].claimnumber.value;
    var batchid = document.forms[0].batchid.value;  

    window.close();
    m_Popup=window.open("../NonOccPayments/NonOccFuturePayments.aspx?claimid="+lClaimId+"&taxarray="+taxarray+"&syscmd="+SysCmd_SavePayments+"&claimnumber="+claimnumber+"&batchrestrict=true"+"&batchid="+batchid,"future",
			'width='+m_Width+',height='+m_Height+',top='+(screen.availHeight-m_Height)/2+',left='+(screen.availWidth-m_Width)/2+',resizable=yes,scrollbars=yes');
		
 </script>   
  <%}%>
  <%else if (sSysCmd == "5")
  {%>
  <div class="errtextheader" runat="server">Possible duplicate payments</div>  
   <div id="divForms" class="divScroll">
   <table width="100%" cellspacing="0" cellpadding="1" border="1">  
   <%if(sRecordExists) {%> 
       <%int i = 0; foreach (XElement item in result)
         {
             string rowclass = "";
             if (i == 0) rowclass = "msgheader";
             else if (i % 2 == 0) rowclass = "datatd";
             else rowclass = "datatd1";
             i++;                   
        %>  
        <tr class="<%=rowclass%>">                  
        <td>
                <%=item.Attribute("startdate").Value%>
        </td>
        <td>
                <%=item.Attribute("enddate").Value%>
        </td>
        <td>
                <%=item.Attribute("payee").Value%>
        </td>
        <td>
                <%=item.Attribute("claimnumber").Value%>
        </td>
        <td>
                <%=item.Attribute("numofpayments").Value%>
        </td>
        <td>
                <%=item.Attribute("voucher").Value%>
        </td>
        <td>
                <%=item.Attribute("batch").Value%>
        </td>                   
    </tr>
<%}%>
<%}%>
  </table>
  </div>
  <br />
 <input type="button" class="button" name="btnClose" value="Cancel" onclick="window.close();"/>      
 <asp:Button runat="server" Text="Continue" ID="btnContinue" class="button" OnClientClick="return Continue();" onclick="btnContinue_Click" />  
<%}%>
<%else if (sSysCmd == "1")
{%>
   <div class="formtitle" id="formtitle">Calculated payments</div>
   <div class="formsubtitle">[<%=sClaimNumber%><%=sClaimantTitle%>]</div>	
   <table>
    <tr>
     <td>Press 'Save Payments' to save these payments to database.  You will be able to edit or delete individual payments after you
      save them.
     </td>
    </tr>
   </table>
   <table>
    <tr>
     <td><b>Transaction Type:</b></td>
     <td><%=sTranstype%></td>
    </tr>
    <tr>
     <td><b>Account:</b></td>
     <td><%=sAccount%></td>
    </tr>
   </table>
    <div id="div1" class="divScroll">
    <table width="100%" cellspacing="0" cellpadding="1" border="1">
    
    <%if (!sNegativePayments)
          {%>
          <%if(sRecordExists) {%> 
      
       <%int i = 0; foreach (XElement item in result)
         {
             string rowclass = "";
             if (i == 0) rowclass = "msgheader";
             else if (i % 2 == 0) rowclass = "datatd";
             else rowclass = "datatd1";
             i++;                   
        %>  
        <tr class="<%=rowclass%>">                  
        <td>
                <%=item.Attribute("paymentnumber").Value%>
        </td>
        <td>
                <%=item.Attribute("fromdate").Value%>
        </td>
        <td>
                <%=item.Attribute("todate").Value%>
        </td>
        <td>
                <%=item.Attribute("printdate").Value%>
        </td>
        <td>
                <%=item.Attribute("gross").Value%>
        </td>
        <td>
                <%=item.Attribute("supppayment").Value%>
        </td>
        <td>
                <%=item.Attribute("net").Value%>
        </td>  
         <td>
                <%=item.Attribute("dayspaid").Value%>                
        </td>
        <td>
                <%=item.Attribute("fed").Value%>  
        </td>   
         <td>
                <%=item.Attribute("ss").Value%> 
        </td> 
         <td>
                <%=item.Attribute("med").Value%> 
        </td> 
         <td>
                <%=item.Attribute("state").Value%> 
        </td> 
         <td>
                <%=item.Attribute("pension").Value%>  
        </td> 
         <td>
                <%=item.Attribute("ssoffset").Value%> 
        </td>   
         <td>
                <%=item.Attribute("oi").Value%>    
        </td>      
        <!-- pmittal5 Mits 14841 04/27/09 - GHS Enhancements-->
        <td>
                <%=item.Attribute("othoffset1").Value%>    
        </td> 
        <td>
                <%=item.Attribute("othoffset2").Value%>    
        </td> 
        <td>
                <%=item.Attribute("othoffset3").Value%>    
        </td>    
        <td>
                <%=item.Attribute("psttaxded1").Value%>    
        </td>    
        <td>
                <%=item.Attribute("psttaxded2").Value%>    
        </td>    
        <td>
                <%=item.Attribute("distributiontype").Value%>    
        </td>       
        <!--End - pmittal5 -->          
    </tr>    
<%}
      }
      }%>
      <%else { %>
                    
                <tr> <td class="errtextheader">All payments of this claim are Negative.</td></tr>
              
      <%}%>
</table>
</div>
<br />
<input type="button" class="button" name="btnClose" value="Cancel" onclick="window.close();"/>
<%if (!sNegativePayments)
  { %>
<asp:Button runat="server" Text="Save Payments" ID="btnSave" class="button" OnClientClick="return SavePayments();"/>  
<%} %>
<%else { %>
<asp:Button runat="server" Text="Save Payments" ID="btnSave1" class="button" OnClientClick="return SavePayments();" Enabled="false"/> 
<%}%>
<%}%>

 <br/> 
 <asp:textbox style="display:none" runat="server" id="hdnclaimid" rmxref="Instance/Document/NonOccPayments/claimid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpieid" rmxref="Instance/Document/NonOccPayments/pieid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnsyscmd" rmxref="Instance/Document/NonOccPayments/syscmd" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntaxarray" rmxref="Instance/Document/NonOccPayments/taxarray" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnacct" rmxref="Instance/Document/NonOccPayments/acct" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchid" rmxref="Instance/Document/NonOccPayments/batchid" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpaymentnum" rmxref="Instance/Document/NonOccPayments/paymentnum" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchrestrict" rmxref="Instance/Document/NonOccPayments/batchrestrict" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnrecalc" rmxref="Instance/Document/NonOccPayments/recalc" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnsysCmdQueue" rmxref="Instance/Document/NonOccPayments/sysCmdQueue" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbenst" rmxref="Instance/Document/NonOccPayments/benst" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbento" rmxref="Instance/Document/NonOccPayments/bento" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpayst" rmxref="Instance/Document/NonOccPayments/payst" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnpayto" rmxref="Instance/Document/NonOccPayments/payto" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntranstype" rmxref="Instance/Document/NonOccPayments/transtype" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntaxflags" rmxref="Instance/Document/NonOccPayments/taxflags" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdndeletelist" rmxref="Instance/Document/NonOccPayments/deletelist" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdnbatchDelete" rmxref="Instance/Document/NonOccPayments/batchDelete" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdngross" rmxref="Instance/Document/NonOccPayments/gross" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdnfromdate" rmxref="Instance/Document/NonOccPayments/fromdate" rmxtype="hidden" />           
 <asp:textbox style="display:none" runat="server" id="hdntodate" rmxref="Instance/Document/NonOccPayments/todate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnsave" rmxref="Instance/Document/NonOccPayments/save" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnclaimnumber" rmxref="Instance/Document/NonOccPayments/claimnumber" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndailyamount" rmxref="Instance/Document/NonOccPayments/dailyamount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndailysuppamount" rmxref="Instance/Document/NonOccPayments/dailysuppamount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniFederalTax" rmxref="Instance/Document/NonOccPayments/iFederalTax" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniSocialSecurityAmount" rmxref="Instance/Document/NonOccPayments/iSocialSecurityAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniMedicareAmount" rmxref="Instance/Document/NonOccPayments/iMedicareAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniStateAmount" rmxref="Instance/Document/NonOccPayments/iStateAmount" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniNetPayment" rmxref="Instance/Document/NonOccPayments/iNetPayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossCalculatedPayment" rmxref="Instance/Document/NonOccPayments/iGrossCalculatedPayment" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossCalculatedSupplement" rmxref="Instance/Document/NonOccPayments/iGrossCalculatedSupplement" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniGrossTotalNetOffsets" rmxref="Instance/Document/NonOccPayments/iGrossTotalNetOffsets" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnilblDaysIncluded" rmxref="Instance/Document/NonOccPayments/ilblDaysIncluded" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnautosplitid" rmxref="Instance/Document/NonOccPayments/autosplitid" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnamountchange" rmxref="Instance/Document/NonOccPayments/amountchange" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnprintdate" rmxref="Instance/Document/NonOccPayments/printdate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdncheckmemo" rmxref="Instance/Document/NonOccPayments/checkmemo" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnselectpaymentrecord" rmxref="Instance/Document/NonOccPayments/selectpaymentrecord" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndSuppRate" rmxref="Instance/Document/NonOccPayments/dSuppRate" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndWeeklyBenefit" rmxref="Instance/Document/NonOccPayments/dWeeklyBenefit" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdniOffsetCalc" rmxref="Instance/Document/NonOccPayments/iOffsetCalc" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndPension" rmxref="Instance/Document/NonOccPayments/dPension" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndSS" rmxref="Instance/Document/NonOccPayments/dSS" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdndOther" rmxref="Instance/Document/NonOccPayments/dOther" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniDaysWorkingInWeek" rmxref="Instance/Document/NonOccPayments/iDaysWorkingInWeek" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdniDaysWorkingInMonth" rmxref="Instance/Document/NonOccPayments/iDaysWorkingInMonth" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnpensionoffset" rmxref="Instance/Document/NonOccPayments/pensionoffset" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnssoffset" rmxref="Instance/Document/NonOccPayments/ssoffset" rmxtype="hidden" />  
 <asp:textbox style="display:none" runat="server" id="hdnoioffset" rmxref="Instance/Document/NonOccPayments/oioffset" rmxtype="hidden" /> 
 <!-- pmittal5 Mits 14841 04/27/09 -GHS enhancements-->
 <asp:textbox style="display:none" runat="server" id="hdnothoffset1" rmxref="Instance/Document/NonOccPayments/othoffset1" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnothoffset2" rmxref="Instance/Document/NonOccPayments/othoffset2" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnothoffset3" rmxref="Instance/Document/NonOccPayments/othoffset3" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnpsttaxded1" rmxref="Instance/Document/NonOccPayments/psttaxded1" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdnpsttaxded2" rmxref="Instance/Document/NonOccPayments/psttaxded2" rmxtype="hidden" /> 
 <asp:textbox style="display:none" runat="server" id="hdndoffset1" rmxref="Instance/Document/NonOccPayments/doffset1" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndoffset2" rmxref="Instance/Document/NonOccPayments/doffset2" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndoffset3" rmxref="Instance/Document/NonOccPayments/doffset3" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndPostTaxDed1" rmxref="Instance/Document/NonOccPayments/dPostTaxDed1" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdndPostTaxDed2" rmxref="Instance/Document/NonOccPayments/dPostTaxDed2" rmxtype="hidden" />
 <asp:textbox style="display:none" runat="server" id="hdnDistributionType" rmxref="Instance/Document/NonOccPayments/DistributionType" rmxtype="hidden" />
 <!-- End - pmittal5 --> 
 <uc1:ErrorControl ID="ErrorControl" runat="server" />  
 <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />
 </form>
 </body>
</html>