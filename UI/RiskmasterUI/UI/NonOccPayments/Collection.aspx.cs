﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;

namespace Riskmaster.UI.NonOccPayments
{
    public partial class Collection : NonFDMBasePageCWS
    {
        public IEnumerable result = null;        
        public XElement rootElement = null;       
        public string sClaimId = "";
        bool bReturnStatus = false;
        string sreturnValue = "";

        protected void Page_Load(object sender, EventArgs e)
        {          
            XmlDocument objCollection = new XmlDocument();
            XElement objTitle = null;            
            XmlNodeList objCollectionNodes = null;

            sClaimId = AppHelper.GetQueryStringValue("ClaimId");
            hdnclaimid.Text = sClaimId;
            ClaimNumber.Text = AppHelper.GetQueryStringValue("ClaimNumber");

            if (!IsPostBack)
            {
                try
                {
                    bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.GetCollectionScreenInformation", out sreturnValue);
                    if (bReturnStatus)
                    {
                        objCollection.LoadXml(sreturnValue);                        
                        reservetypearray.Text   = objCollection.SelectSingleNode("//ReserveTypeArray").InnerText;
                        TaxablePercent.Text = objCollection.SelectSingleNode("//TaxablePercent").InnerText;
                        FED_TAX_RATE.Text = objCollection.SelectSingleNode("//FED_TAX_RATE").InnerText;
                        SS_TAX_RATE.Text = objCollection.SelectSingleNode("//SS_TAX_RATE").InnerText;
                        MEDICARE_TAX_RATE.Text = objCollection.SelectSingleNode("//MEDICARE_TAX_RATE").InnerText;
                        STATE_TAX_RATE.Text = objCollection.SelectSingleNode("//STATE_TAX_RATE").InnerText;
                        
                        //Fetch the list of AccountList
                        objCollectionNodes = objCollection.SelectNodes("//Collection/AccountList/option");

                        for (int i = 0; i < objCollectionNodes.Count; i++)
                        {
                            lstBankAccounts.Items.Add(new ListItem(objCollectionNodes[i].InnerText, (((XmlElement)objCollectionNodes[i]).GetAttribute("value"))));
                        }

                        //Fetch the list of TransactionTypes
                        objCollectionNodes = objCollection.SelectNodes("//Collection/TransactionType/option");

                        for (int i = 0; i < objCollectionNodes.Count; i++)
                        {
                            lstTransTypes.Items.Add(new ListItem(objCollectionNodes[i].InnerText, (((XmlElement)objCollectionNodes[i]).GetAttribute("value"))));
                        }  

                    }
                }

                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
        }

        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {            
            hdnclaimid.Text = sClaimId;
            hdnbankacc.Text = lstBankAccounts.SelectedItem.Value;
            hdntranstype.Text = lstTransTypes.SelectedItem.Value;
            hdnreservetype.Text = reservetype.Value;
            hdnstartdate.Text = txtStart.Value;
            hdnenddate.Text = txtEnd.Value;
            hdnamt.Text = txtAmount.Value;         
            hdnfedamt.Text = txtFed.Value;          
            hdnssamt.Text = txtSS.Value;            
            hdnmedamt.Text = txtMed.Value;            
            hdnstateamt.Text = txtState.Value;
            hdnnetamt.Text = txtNet.Value;            
            hdnformmode.Text = "close";

            bReturnStatus = CallCWSFunction("NonOccPaymentsAdaptor.SaveAllCollections", out sreturnValue);                 
        }
     
    }
}
