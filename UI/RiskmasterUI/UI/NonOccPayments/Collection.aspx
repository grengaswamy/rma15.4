﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Collection.aspx.cs" Inherits="Riskmaster.UI.NonOccPayments.Collection" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head runat="server">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>   
  <title>Short Term Disability - Collection</title>
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css"/>
  <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
  <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
  <script language="JavaScript" src="../../Scripts/nonocc.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
  <script src="../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
  <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
  <script type="text/javascript" src="../../Scripts/calendar-alias.js"></script>
  </head>
 <body onload="CheckForWindowClosing();">
  <form id="frmData" runat="server">
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
     <td width="5%">&nbsp;</td>
     <td width="35%">Collection for Claim Number:</td>
     <td width="60%"><b><asp:Label ID="ClaimNumber" runat="server"></asp:Label></b></td>
    </tr>
    <tr>
     <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
     <td></td>
     <td>Bank Account:</td>
     <td>   
       <asp:DropDownList ID="lstBankAccounts" runat="server"></asp:DropDownList>
      </td>
    </tr>
   <tr>
     <td></td>
     <td>
     	Transaction Type:      							
     </td>
     <td>    
      <asp:DropDownList ID="lstTransTypes" runat="server" onchange="return setReserveType('lstTransTypes');" ></asp:DropDownList>
     </td>      
    </tr>
    <tr>
     <td></td>
     <td>
       Reserve Type:      							
     </td>
     <td><input type="text" value="" id="reservetype" disabled="true" runat="server"/></td>
    </tr>
    <tr>
     <td></td>
     <td>From:    							
     </td>
     <td>
     <input type="text" value="" size="30" onblur="dateLostFocus(this.id);" id="txtStart" runat="server"/>
     <input type="button" class="DateLookupControl" id="txtStartbtn" runat="server"/>
     <script type="text/javascript">
	  Calendar.setup(
	  {
		inputField : "txtStart",
		ifFormat : "%m/%d/%Y",
		button : "txtStartbtn"
	  }
	);
	 </script>
	</td>
    </tr>
    <tr>
     <td></td>
     <td>To:      					
     </td>
     <td>
        <input type="text" value="" size="30" onblur="dateLostFocus(this.id);&#xA;" id="txtEnd" runat="server"/>
        <input type="button" class="DateLookupControl" id="txtEndbtn"/>     
        <script type="text/javascript">
	        Calendar.setup
	        (
	            {
		            inputField : "txtEnd",
		            ifFormat : "%m/%d/%Y",
		            button : "txtEndbtn"
	            }
	        );
	</script></td>
    </tr>
    <tr>
     <td colspan="3">
        <hr/>
     </td>
    </tr>
    <tr>
     <td></td>
     <td>
       Collection Amount:      							
     </td>
     <td>
        <input type="text" value="" size="30" onblur="numLostFocus(this);storeAmount();CalculateTaxes();"  runat="server" id="txtAmount"/>
     </td>
    </tr>
    <tr>
     <td></td>
     <td>Federal Tax:   							
     </td>
     <td>
        <input type="checkbox" runat="server" id="Fed" onclick="TaxCheckBoxClicked('Fed');"/>&nbsp;&nbsp;
        <input type="text" value="" size="30" runat="server" onblur="numLostFocus(this);CalculateTaxes();" id="txtFed"/>
      </td>
    </tr>
    <tr>
     <td></td>
     <td>
      Social Security Tax:      							
     </td>
     <td>
        <input type="checkbox" id="SS" onclick="TaxCheckBoxClicked('SS');" runat="server"/>&nbsp;&nbsp;
        <input type="text" value="" size="30" runat="server" onblur="numLostFocus(this);CalculateTaxes();" id="txtSS"/>
     </td>
    </tr>
    <tr>
     <td></td>
     <td>
       Medicare Tax:      							
     </td>
     <td>
        <input type="checkbox" id="Med"  runat="server" onclick="TaxCheckBoxClicked('Med');"/>&nbsp;&nbsp;
        <input type="text" value="" size="30" runat="server" onblur="numLostFocus(this);CalculateTaxes();" id="txtMed"/></td>
    </tr>
    <tr>
     <td></td>
     <td>
      State Tax:      							
     </td>
     <td>
        <input type="checkbox" id="State" runat="server" onclick="TaxCheckBoxClicked('State');"/>&nbsp;&nbsp;
        <input type="text" value="" size="30" onblur="numLostFocus(this);CalculateTaxes();" runat="server" id="txtState"/></td>
    </tr>
    <tr>
     <td></td>
     <td>
      	Net Collection:      							
     </td>
     <td><input type="text"  size="30" runat="server" onblur="" id="txtNet" disabled="true" /></td>
    </tr>
    <tr>
    <td></td>
    <td>
        Manual Entry of Tax Amounts:      							
    </td>
    <td>
        <input type="checkbox" runat="server" id="chkManual" onclick="ManualEntryOfTaxes();"/>
     </td>
    </tr>
    <tr>
     <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="3" align="center">
        <input type="submit" value="Cancel" onclick="javascript:window.close();return false;" class="button"/>&nbsp;&nbsp;&nbsp;
        <asp:Button Text="Save Changes" id="btnSaveChanges" class="button" runat="server" OnClientClick="return CheckCollectionFields();" onclick="btnSaveChanges_Click"/>
      </td>     
    </tr>
   </table>
      <asp:textbox style="display:none" runat="server" id="hdnclaimid" rmxref="Instance/Document/Collection/ClaimId" rmxtype="hidden" />           
      <asp:textbox style="display:none" runat="server" id="hdnclaimnumber" rmxref="Instance/Document/Collection/ClaimNumber" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnbankacc" rmxref="Instance/Document/Collection/BankAccount" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdntranstype" rmxref="Instance/Document/Collection/TransactionType" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnreservetype" rmxref="Instance/Document/Collection/ReserveType" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnstartdate" rmxref="Instance/Document/Collection/StartDate" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnenddate" rmxref="Instance/Document/Collection/EndDate" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnamt" rmxref="Instance/Document/Collection/Amount" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnFed" rmxref="Instance/Document/Collection/Fed" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnfedamt" rmxref="Instance/Document/Collection/FedAmount" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnSS" rmxref="Instance/Document/Collection/SS" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnssamt" rmxref="Instance/Document/Collection/SSAmount" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnMed" rmxref="Instance/Document/Collection/Med" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnmedamt" rmxref="Instance/Document/Collection/MedAmount" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnState" rmxref="Instance/Document/Collection/State" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnstateamt" rmxref="Instance/Document/Collection/StateAmount" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnnetamt" rmxref="Instance/Document/Collection/NetAmount" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnManulaentry" rmxref="Instance/Document/Collection/ManualEntry" rmxtype="hidden" />    
      <asp:textbox style="display:none" runat="server" id="hdnformmode" rmxref="Instance/Document/Collection/FormMode" rmxtype="hidden" /> 
      <asp:textbox style="display:none" runat="server" id="reservetypearray" rmxtype="hidden" /> 
      <asp:textbox style="display:none" runat="server" id="TaxablePercent" rmxtype="hidden" /> 
      <asp:textbox style="display:none" runat="server" id="FED_TAX_RATE" rmxtype="hidden" /> 
      <asp:textbox style="display:none" runat="server" id="SS_TAX_RATE" rmxtype="hidden" /> 
      <asp:textbox style="display:none" runat="server" id="MEDICARE_TAX_RATE" rmxtype="hidden" /> 
      <asp:textbox style="display:none" runat="server" id="STATE_TAX_RATE" rmxtype="hidden" />                          
      <input type="hidden" value="" id="FormMode"/>      
      <uc1:ErrorControl ID="ErrorControl" runat="server" /> 
   </form>
   <script language="javascript">
     document.forms[0].txtFed.disabled=true;
   	 document.forms[0].txtSS.disabled=true;
	 document.forms[0].txtMed.disabled=true;
	 document.forms[0].txtState.disabled=true;
	 document.forms[0].txtAmount.value=0.00;
	 document.forms[0].txtFed.value=0.00;
	 document.forms[0].txtSS.value=0.00;
	 document.forms[0].txtMed.value=0.00;
	 document.forms[0].txtState.value=0.00;
	 document.forms[0].txtNet.value=0.00;
 </script>
 </body>
</html>
