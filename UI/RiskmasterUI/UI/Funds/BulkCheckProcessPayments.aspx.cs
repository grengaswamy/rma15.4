﻿using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Riskmaster.UI.UI.Funds
{
    /// <summary>
    /// Author: Animesh Sahai
    /// Date: 2/10/2010
    /// Class to display the list of payments processed using the Bulk Check Release Utility.
    /// </summary>
    public partial class BulkCheckProcessPayments : NonFDMBasePageCWS
    {
        public XmlDocument Model = null;
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function></Function></Call><Document><Data><selectedtransid/><checkstatus/></Data></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            string sCWSresponse = string.Empty;
            try
            {

                Model = new XmlDocument();
                if (Page.IsPostBack)
                {
                    BindBackData();
                    XElement XmlTemplate = null;
                    XmlTemplate = XElement.Parse(MergeMessageTemplate);
                    CallCWS("FundManagementAdaptor.ProcessBulkPayments", XmlTemplate, out sCWSresponse, false, false);
                    Model.LoadXml(sCWSresponse);
                    BindpageControls(sCWSresponse);
                }
                else
                {
                    divFailure.Visible = false;
                    divSuccess.Visible = false;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        /// <summary>
        /// Method to bind the data to the Screen XML.
        /// </summary>
        /// <returns></returns>
        private string BindBackData()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(MergeMessageTemplate);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//selectedtransid", hdnselectedTransID.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//checkstatus", hdnCheckStatus.Value);
                return doc.OuterXml;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Method to bind the search result to the Datagrid control
        /// Author: Animesh Sahai
        /// Date: 2/5/2010
        /// </summary>
        /// <param name="sreturnValue">string that contains the data XML</param>
        private void BindpageControls(string sreturnValue)
        {
            DataSet usersRecordsSet = null;
            try
            {
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                //**************************
                //Populating successful payments.
                //***************************
                usersRecordsSet = ConvertSuccessXmlDocToDataSet(usersXDoc);
                if (usersRecordsSet != null)
                {
                    if (usersRecordsSet.Tables.Count > 0)
                    {
                        gvSuccessPayments.DataSource = usersRecordsSet.Tables[0].DefaultView;
                        gvSuccessPayments.DataBind();
                        divSuccess.Visible = true;
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        usersRecordsSet.Tables.Add(dt);
                        gvSuccessPayments.DataBind();
                        divSuccess.Visible = false;
                    }
                }
                usersRecordsSet = null;
                //**************************
                //Populating successful payments.
                //***************************
                usersRecordsSet = ConvertUnSuccessXmlDocToDataSet(usersXDoc);
                if (usersRecordsSet != null)
                {
                    if (usersRecordsSet.Tables.Count > 0)
                    {
                        gvUnsuccessfulPayments.DataSource = usersRecordsSet.Tables[0].DefaultView;
                        gvUnsuccessfulPayments.DataBind();
                        divFailure.Visible = true;
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        usersRecordsSet.Tables.Add(dt);
                        gvUnsuccessfulPayments.DataBind();
                        divFailure.Visible = false;
                    }
                }


            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
            finally
            {
                if (usersRecordsSet != null)
                {
                    usersRecordsSet.Dispose();
                    usersRecordsSet = null;
                }
            }

        }

        /// <summary>
        /// Method to extract the search result.
        /// </summary>
        /// <param name="diaryDoc">Result returned from the business layer based on the search criteria</param>
        /// <returns>returns the Dataset obtained from the supplied XML</returns>
        public DataSet ConvertSuccessXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//success")));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        /// <summary>
        /// Method to extract the search result.
        /// </summary>
        /// <param name="diaryDoc">Result returned from the business layer based on the search criteria</param>
        /// <returns>returns the Dataset obtained from the supplied XML</returns>
        public DataSet ConvertUnSuccessXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//unsuccess")));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
    }
}
