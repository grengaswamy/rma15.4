﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.Funds
{
    public partial class reissue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScript(sCulture, this);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }

            string sDate = DateTime.Now.ToShortDateString();
            ReissueDate.Text = AppHelper.GetDate(sDate);
            ReissueDate.Enabled = false;
            ReissuedBy.Text = AppHelper.GetUserLoginName();
            ReissuedBy.Enabled = false;
            //Rakhel ML Changes 
            //ReissueDatebtn.Enabled = false; 
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            isReissuePayment.Text = "false";
        }

        protected void btnReissue_Click(object sender, EventArgs e)
        {
            isReissuePayment.Text = "true";
        }
    }
}
