﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BulkCheckRelease.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.BulkCheckRelease" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc5" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <style type="text/css">
        .buttonA
        {
        	display: inline; 
            FONT-WEIGHT: bold;
            FONT-SIZE: 8pt;
            FONT-FAMILY: Arial, Helvetica, sans-serif;
            BACKGROUND-IMAGE: url(/RiskmasterUI/Images/button.gif);    
            width: 128px;                	
        }
		.checkbox
		{
			width:15px;
		}
    </style>
    <title></title>
    
<link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript">{var i;}  </script>
     <!-- Rakhel ML Changes - Start !-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
     <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">         { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
     <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">         { var i; } </script>			
	<!-- Rakhel ML Changes - End !-->
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript">
       var ns, ie, ieversion;
       var browserName = navigator.appName;                   // detect browser 
       var browserVersion = navigator.appVersion;
       if (browserName == "Netscape") {
           ie = 0;
           ns = 1;
       }
       else		//Assume IE
       {
           ieversion = browserVersion.substr(browserVersion.search("MSIE ") + 5, 1);
           ie = 1;
           ns = 0;
       }
       function onPageLoaded() {
           if (ie) {
               if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                   eval("document.all.divForms").style.height = 400
               }
           }
           else {
               var o_divforms;
               o_divforms = document.getElementById("divForms");
               if (o_divforms != null) {
                   o_divforms.style.height = window.frames.innerHeight * 0.70;
                   o_divforms.style.width = window.frames.innerWidth * 0.995;
               }
           }
       }
       function DisableClients()
       {
            if (document.forms[0].chkAllClients.checked==true)
            {
                document.forms[0].Org.value="";
                document.forms[0].hdnAll.value="ALL" 
                document.forms[0].Org.readOnly=true;
                document.forms[0].Orgbtn.disabled=true;
                document.forms[0].Org_cid.value="0";
                document.forms[0].OrgId.value="0";
            } 
            else
            {
                document.forms[0].Org.value="";
                document.forms[0].Org.readOnly=false;
                document.forms[0].Orgbtn.disabled=false;
                document.forms[0].Org_cid.value="";
                document.forms[0].OrgId.value="";
                document.forms[0].hdnAll.value=""
            }
       }
       function SelectAll() {
           for (var i = 0; i < document.forms[0].elements.length; i++) {
               if (document.forms[0].elements[i].type == 'checkbox') {
                   if(document.forms[0].elements[i].id == "chkAllClients"){
                        continue;  
                   } 
                   if(document.forms[0].elements[i].checked == false){
                   document.forms[0].elements[i].checked = true;
                   Calculate(document.forms[0].elements[i].id) 
                   }
                   else{
                   document.forms[0].elements[i].checked = true;
                   }
               }
           }
       }
       function UnSelectAll() {
           for (var i = 0; i < document.forms[0].elements.length; i++) {
               if (document.forms[0].elements[i].type == 'checkbox') {
                   if(document.forms[0].elements[i].id == "chkAllClients"){
                        continue;  
                   }  
                   document.forms[0].elements[i].checked = false;
                   document.forms[0].txtTotal.value = formatCurrency(0.00);
               }
           }
           document.forms[0].txtUnsAmount.value = formatCurrency(Number(document.forms[0].hdnUnselectedAmount.value));  
       }
       function Calculate(sCtrlName)
       {
            var objFormElem = eval('document.forms[0].' + sCtrlName);
            var objTotalElemName = eval('document.forms[0].txtTotal');
            var strKeyValue =  objFormElem.value;
            var arrTemp=strKeyValue.split(";");
            var dblAmt=parseFloat(arrTemp[2]).toFixed(2);
            var dblTotalAmt = eval('document.forms[0].txtTotal.value').replace('$','');
            dblTotalAmt = dblTotalAmt.replace(',','');
            dblTotalAmt=parseFloat(dblTotalAmt).toFixed(2);
            if (objFormElem.checked == true)
            {
                var dblGrandTotal = Number(dblTotalAmt) + Number(dblAmt);
            }
            else
            {
                var dblGrandTotal = Number(dblTotalAmt) - Number(dblAmt);
            }
            document.forms[0].txtTotal.value = formatCurrency(dblGrandTotal);      
            var chgn_value=(parseFloat(document.forms[0].hdnUnselectedAmount.value).toFixed(2)) - (parseFloat(dblGrandTotal).toFixed(2));
            document.forms[0].txtUnsAmount.value = formatCurrency(Number(chgn_value));
       }
       
       function ProcessPayments()
       {
            var selected = "";
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].id == "chkAllClients")
                   continue;
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].checked == true) {
                       var strValue=document.forms[0].elements[i].value; 
                       var arrTemp=strValue.split(";");     
                       if (selected == "") {
                           selected = arrTemp[0];
                       }
                       else {
                           selected = selected + "," + arrTemp[0];
                       }
                   }
                }   
            }
           document.forms[0].hdnSelected.value = selected;
           if (selected != "") {
               m_codeWindow = window.open('BulkCheckProcessPayments.aspx', 'codeWnd',
						'width=800,height=500' + ',top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 800) / 2 + ',resizable=yes,scrollbars=yes,toolbar=no,menubar=yes');
               return false;
           }
           if (selected == "") {
               //alert("There are no payments selected, cannot process!");
               alert(BulkCheckReleaseValidations.ValidNoPaymentSelection);
               return false;
           }
           return false;
       }
       function ClearSearchCriteria()
       {
	       document.forms[0].hdnSelected.value="";
	       document.forms[0].txtTransactionFromDate.value="";
	       document.forms[0].txtTransactionToDate.value="";
	       document.forms[0].Provlastfirstname.value="";
	       document.forms[0].Provlastfirstname_creatable.value="";
	       document.forms[0].Proventityid.value="";
	       //ddhiman
	       document.forms[0].txtClaimNumber.value="";
	       document.forms[0].txtClaimNumber_creatable.value="";
	       document.forms[0].txtClaimNumber_cid.value="";
	       document.forms[0].txtAmount.value="";
	       document.forms[0].txtAccountName.value="";
	       document.forms[0].txtFromDateStart.value="";
	       document.forms[0].txtToDateStart.value="";
	       document.forms[0].txtFromDateEnd.value="";
	       document.forms[0].txtToDateEnd.value="";
	       document.forms[0].txtInvoieFromDate.value="";
	       document.forms[0].txtInvoiceToDate.value="";
	       document.forms[0].txtInvoiceNumber.value="";
	       document.forms[0].ReserveTypeID_codelookup_cid.value="";
	       document.forms[0].ReserveTypeID_codelookup.value="";
	       document.forms[0].lstTransTypes_multicode_lst.value="";
	       var formObj = document.forms['frmData']; // your form
	       var OptionCount = 0;
	       OptionCount = formObj.lstTransTypes_multicode.options.length;
	       for (var loop=0; loop < OptionCount; loop++) {
            formObj.lstTransTypes_multicode.options[0] = null; // remove the option
            }
            //ddhiman
	       document.forms[0].Org.value="";
	       document.forms[0].OrgId.value="";
	       document.forms[0].Org_cid.value="";
	       document.forms[0].chkAllClients.checked=false;
	       return false;
       } 
       function VerifySearchCriteria()
       {
            var StartDate = new Date(document.forms[0].txtTransactionFromDate.value);   
            var EndDate= new Date(document.forms[0].txtTransactionToDate.value);	
            if (StartDate !="" && EndDate !="")
            {
                if (StartDate > EndDate)
                {
                    //alert('To date cannot be prior to From date');
                    alert(BulkCheckReleaseValidations.ValidToDateValidation);
	                document.forms[0].txtTransactionFromDate.value="";
	                document.forms[0].txtTransactionToDate.value="";
                    return false;
                }
            }
            //ddhiman 12/15/2010
            var fStartDate = new Date(document.forms[0].txtFromDateStart.value);   
            var fEndDate= new Date(document.forms[0].txtFromDateEnd.value);	
            if (fStartDate !="" && fEndDate !="")
            {
                if (fStartDate > fEndDate)
                {
                    //alert('From Date (End) cannot be prior to From Date (Start)');
                    alert(BulkCheckReleaseValidations.ValidFromDateValidation);
                    document.forms[0].txtFromDateStart.value="";
	                document.forms[0].txtFromDateEnd.value="";
                    return false;
                }
            }
            var tStartDate = new Date(document.forms[0].txtToDateStart.value);   
            var tEndDate= new Date(document.forms[0].txtToDateEnd.value);	
            if (tStartDate !="" && tEndDate !="")
            {
                if (tStartDate > tEndDate)
                {
                    //alert('To Date (End) cannot be prior to To Date (Start)');
                    alert(BulkCheckReleaseValidations.ValidToDateEndValidation);
	                document.forms[0].txtToDateStart.value="";
	                document.forms[0].txtToDateEnd.value="";
                    return false;
                }
            }
            var iStartDate = new Date(document.forms[0].txtInvoieFromDate.value);   
            var iEndDate= new Date(document.forms[0].txtInvoiceToDate.value);	
            if (iStartDate !="" && iEndDate !="")
            {
                if (iStartDate > iEndDate)
                {
                    //alert('Invoice To Date cannot be prior to Invoice From Date');
                    alert(BulkCheckReleaseValidations.ValidInvoiceDateValidation);
                    document.forms[0].txtInvoieFromDate.value="";
	                document.forms[0].txtInvoiceToDate.value="";
                    return false;
                }
            }
            //ddhiman 12/15/2010
            pleaseWait.Show();            
            document.forms[0].hdnSearchHit.value="true";
			return true;         
       }
       function chgCurrencyFormat()
       {
            if (document.forms[0].txtUnsAmount!=null)
            {
                document.forms[0].txtUnsAmount.value = formatCurrency(document.forms[0].txtUnsAmount.value);
            }
        }

    </script>
    <style type="text/css">
        .style1
        {
            width: 142px;
        }
        .style2
        {
        }
        .style3
        {
            height: 11px;
        }
        .style4
        {
            width: 142px;
            height: 16px;
        }
        .style5
        {
            width: 142px;
            height: 11px;
        }
        .style6
        {
            height: 9px;
        }
        .style7
        {
            width: 142px;
            height: 9px;
        }
    </style>

</head>
<body onload="parent.MDIScreenLoaded(); chgCurrencyFormat();">
    <form id="frmData" runat="server">
        <table width="100%">
            <tr>
                <td class="msgheader">                    
                    <asp:Label ID="lblPageHdr" runat="server" Text="<%$ Resources:lblPageHdrResrc %>" />
                </td>
            </tr>
            <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
        </table>
        <input type="hidden" id="hdnSelected" runat="server"/>
        <input type="hidden" id="hdnAll" runat="server"/>
        <asp:HiddenField ID="hdnSortExpression" runat="server" Value="1" />
        <input type="hidden" id="hdnSearchHit" runat="server" />
        <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"   CustomMessage="_" />
        <br />
        <table width="100%">
            <tr>
                <td class="msgheader">                    
                    <asp:Label ID="lblSearchCriteria" runat="server" Text="<%$ Resources:lblSearchCriteriaResrc %>" />
                </td>
            </tr>        
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="20%" align="right">
                    <asp:Label ID="lblTransactionFrom" runat="server" Text="<%$ Resources:lblTransactionFromResrc %>" />
                </td>
                <td width="30%" align="left" >
                    <asp:Label Width="10" runat="server"  id= "lblFromDateSpace" Text=""/> 
                    <asp:TextBox runat="server" FormatAs="date" ID="txtTransactionFromDate" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='FromDate']" 
                    TabIndex="1" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" RMXType="date" Width="139px"/>
                    <!-- Rakhel ML Changes - Start !-->
                    
                    <script type="text/javascript">
                        $(function () {
                            $("#txtTransactionFromDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                    </script>
                   <!-- Rakhel ML Changes - End !-->
                    <asp:Label Width="10" runat="server"  id= "lblSpace" Text=""/> 								
                </td>		
                <td width="10%" align="right">
                    <asp:Label runat="server" class="" ID="lblProvider" Text="<%$ Resources:lblProviderResrc %>" />
                </td>
                <td width="40%" align="left">
                    <span>
                        <asp:Label Width="10" runat="server"  id= "lblProviderSpace" Text=""/> 
                        <asp:Textbox runat="server" ReadOnly="false" Width = "200"  onchange="lookupTextChanged(this);setDataChanged(true);" tabindex="1" onblur="lookupCustomProvField(this);" RMXType="entitylookup" id="Provlastfirstname" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ProviderEID']" />
                        <asp:button runat="server" class="EllipsisControl" id="Provlastfirstnamebtn" tabindex="2" onclientclick="return lookupData('Provlastfirstname','',4,'Prov',1);" />
                        <asp:Textbox runat="server" style="display:none" Text="0" id="Provlastfirstname_creatable" />                    
                        <asp:TextBox style="display:none" runat="server" onchange="setDataChanged(true);" id="Proventityid" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ProviderEID']/@codeid" RMXType="id" />            
                    </span>
                </td>
            </tr>
            <tr>		
                <td width="20%" align="right">
                    <asp:Label ID="lblTransactionTo" runat="server" Text="<%$ Resources:lblTransactionToResrc %>" />
                </td>        
                <td width="30%" align="left" >
                    <asp:Label Width="10" runat="server"  id= "lblToDateSpace" Text=""/> 
                    <asp:TextBox runat="server" FormatAs="date" ID="txtTransactionToDate" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ToDate']" 
                    TabIndex="2" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" RMXType="date" Width="139px"/>
                     <!-- Rakhel ML Changes - Start !-->
                    
                    <script type="text/javascript">
                        $(function () {
                            $("#txtTransactionToDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                     </script>
                   <!-- Rakhel ML Changes - End !-->
                </td>
                <td width="20%" align="right">
                    <asp:Label ID="lblClaimNo" runat="server" Text="<%$ Resources:lblClaimNumberResrc %>" />
                </td>
                <td>
                    <asp:Label ID="lblClaimNumber" runat="server" Width="10" Text="" />
                    <asp:TextBox runat="server" ReadOnly="false" Width="200" onchange="lookupTextChanged(this);setDataChanged(true);"
                    onblur="lookupCustomClear(this);" RMXType="claimnumberlookup" ID="txtClaimNumber"
                    RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='dClaimNumber']" />
                    <asp:Button runat="server" class="EllipsisControl" ID="txtClaimNumberbtn" OnClientClick="return lookupData('txtClaimNumber','claim',1,'txtClaimNumber',6)" />
                    <asp:TextBox runat="server" Style="display: none" Text="0" ID="txtClaimNumber_creatable" />
                    <asp:TextBox Style="display: none" runat="server" onchange="setDataChanged(true);"
                    ID="txtClaimNumber_cid" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='dClaimNumber']/@codeid" RMXType="id" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblReserveType" runat="server" Text="<%$ Resources:lblReserveTypeResrc %>" />
                </td>
                <td align="left">
                    <asp:Label ID="lblResTyp" runat="server" Width="10px" />
                    <uc5:CodeLookUp runat="server" ID="ReserveTypeID" CodeTable="RESERVE_TYPE"
                    ControlName="ReserveTypeID" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ReserveTypeID']"
                    type="code" Required="true" />
                </td>
                <td align="right">
                    <asp:Label ID="lblAmont" runat="server" Text="<%$ Resources:lblAmountResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblComboAmt" />
                    <asp:DropDownList ID="ddlAmountCond" runat="server" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ddlAmountCond']">
                        <asp:ListItem Value="0">=</asp:ListItem>
                        <asp:ListItem Value="1">&lt;=</asp:ListItem>
                        <asp:ListItem Value="2">&gt;=</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label Width="10px" runat="server" ID="lblAmountBCR" />
                    <asp:TextBox runat="server" Width="143" onchange="setDataChanged(true);" ID="txtAmount"
                    RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='txtAmount']" RMXType="currency"
                    onblur="currencyLostFocus(this);" max="9999999999.99" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblFromDate" runat="server" Text="<%$ Resources:lblFromDateResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblFrmDatStart" />
                    <asp:TextBox ID="txtFromDateStart" runat="server" onchange="setDataChanged(true);"
                    onblur="dateLostFocus(this.id);" RMXType="date" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='FromDateStart']"
                    Width="139px" />
                     <!-- Rakhel ML Changes - Start !-->
                   
                    <script type="text/javascript">
                        $(function () {
                            $("#txtFromDateStart").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                     </script>
                 <!-- Rakhel ML Changes - End !-->
                </td>
                <td align="right">
                    <asp:Label ID="lblAccName" runat="server" Text="<%$ Resources:lblAccountNameResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblAccountName" />
                    <asp:TextBox runat="server" onchange="setDataChanged(true);" ID="txtAccountName"
                    RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='AccountName']" RMXType="text"
                    Width="200px" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblFromDtEnd" runat="server" Text="<%$ Resources:lblFromDtEndResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblFrmDatEnd" />
                    <asp:TextBox ID="txtFromDateEnd" runat="server" onchange="setDataChanged(true);"
                    onblur="dateLostFocus(this.id);" RMXType="date" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='FromDateEnd']"
                    Width="139px" />
                     <!-- Rakhel ML Changes - Start !-->
                    
                    <script type="text/javascript">
                        $(function () {
                            $("#txtFromDateEnd").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                    </script>
                   <!-- Rakhel ML Changes - End !-->
                </td>
                <td align="right" rowspan="4">        
                    <asp:Label ID="lblTransactionType" runat="server" Text="<%$ Resources:lblTransactionTypeResrc %>" />
                </td>
                <td rowspan="4">
                    <span id="Span1" style="display:inline-block;width:10px;"></span>                
                    <asp:listbox  ToolTip=""  ID="lstTransTypes_multicode" runat="server" 
                    SelectionMode ="Multiple" Rows ="3" width = "206px" Height="88px"                     
                    RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='lstTransTypes']"></asp:listbox>
                    <asp:Button class="CodeLookupControl"  runat="server" ID="lstTransTypes_multicodebtn" OnClientClick="return selectCode('TRANS_TYPES','lstTransTypes_multicode');"/>
                    <asp:Button ToolTip="<%$ Resources:ttRemove %>"  class="BtnRemove" Text="-"  runat="server" ID="lstTransTypes_multicodebtndel" OnClientClick="return deleteSelCode('lstTransTypes_multicode');"/>
                    <asp:TextBox ID="lstTransTypes_multicode_lst" style="display:none" runat ="server" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='lstTransTypes']/@codeid"/>                              
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblToDate" runat="server" Text="<%$ Resources:lblToDateResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblToDatStart" />
                    <asp:TextBox ID="txtToDateStart" runat="server" Width="139px" onchange="setDataChanged(true);"
                    onblur="dateLostFocus(this.id);" RMXType="date" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ToDateStart']" />
                     <!-- Rakhel ML Changes - Start !-->
                    
                    <script type="text/javascript">
                         $(function () {
                            $("#txtToDateStart").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                    </script>
                    <!-- Rakhel ML Changes - End !-->
                </td>
            </tr>
        
            <tr>
                <td align="right">
                    <asp:Label ID="lblToDtEnd" runat="server" Text="<%$ Resources:lblToDtEndResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblToDatEnd" />
                    <asp:TextBox ID="txtToDateEnd" runat="server" Width="139px" onchange="setDataChanged(true);"
                    onblur="dateLostFocus(this.id);" RMXType="date" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ToDateEnd']" />
                   <!-- Rakhel ML Changes - Start !-->
                    
                    <script type="text/javascript">
                       $(function () {
                            $("#txtToDateEnd").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                     </script>
                   <!-- Rakhel ML Changes - End !-->
                </td>        
            </tr>
        
            <tr>
                <td align="right">
                    <asp:Label ID="lblInvoiceFromDate" runat="server" Text="<%$ Resources:lblInvoiceFromDateResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblInvoiceFrmDat" />
                    <asp:TextBox ID="txtInvoieFromDate" runat="server" onchange="setDataChanged(true);"
                    onblur="dateLostFocus(this.id);" RMXType="date" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='InvoiceFromDate']"
                    Width="139px" />
                    <!-- Rakhel ML Changes - Start !-->
                   
                    <script type="text/javascript">
                        $(function () {
                            $("#txtInvoieFromDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                     </script>
                   <!-- Rakhel ML Changes - End !-->
                </td>          
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblInvoiceToDate" runat="server" Text="<%$ Resources:lblInvoiceToDateResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblInvoiceToDat" />
                    <asp:TextBox ID="txtInvoiceToDate" runat="server" onchange="setDataChanged(true);"
                    onblur="dateLostFocus(this.id);" RMXType="date" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='InvoiceToDate']"
                    Width="139px" />
                     <!-- Rakhel ML Changes - Start !-->
                    
                    <script type="text/javascript">
                        $(function () {
                            $("#txtInvoiceToDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                    </script>
                   <!-- Rakhel ML Changes - End !-->
                </td>
                <td width="10%" align="right">
                    <asp:Label runat="server" class="" ID="lblClientResrc" Text="<%$ Resources:lblClientResrc %>" />
                </td>
                <td align ="left" >
                    <span>
                        <asp:Label Width="10" runat="server"  id= "lblClientSpace" Text=""/> 
                        <asp:TextBox runat="server" ReadOnly="false" Width = "200" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);setDataChanged(true);" id="Org" RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='ClientEID']" RMXType="orgh" name="Org" cancelledvalue="" />
                        <asp:Button ID="Orgbtn" class="CodeLookupControl" OnClientClick="return selectCode('orgh','Org','ALL')" Text="" runat="server" />
                        <asp:TextBox Style="display: none" onchange="setDataChanged(true);" ID="OrgId" runat="server"  /> 
                        <asp:TextBox Style="display: none" onchange="setDataChanged(true);" ID="Org_cid" runat="server" RMXRef = "/Instance/Document/BulkCheckRelease/control[@name ='ClientEID']/@codeid"/>
                    </span>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label runat="server" class="" ID="lblInvoice" Text="<%$ Resources:lblInvoiceResrc %>" />
                </td>
                <td>
                    <asp:Label Width="10px" runat="server" ID="lblInvoiceNum" />
                    <asp:TextBox ID="txtInvoiceNumber" runat="server" Width="139px" onchange="setDataChanged(true);"
                    RMXRef="/Instance/Document/BulkCheckRelease/control[@name ='InvoiceNumber']" RMXType="text" />
                </td>
                <td></td>
                <td>
                    <asp:Label Width="10" runat="server"  id= "lblCheckAllSpace" Text=""/> 
                    <asp:CheckBox ID="chkAllClients" runat="server" Text="<%$ Resources:chkAllClientsResrc %>" OnClick="javascript:DisableClients();"/>
                </td>
            </tr>
            <tr style="height:10px" >
                <td></td>
                <td align="left">
                &nbsp;</td>
                <td></td><td></td>
                </tr>
                <tr>
                <td></td><td></td>        
            </tr>  
            <tr>
                <td  colspan="4" align="center">
                    <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:btnSearchResrc %>"  CssClass="buttonA" OnClientClick="javascript:return VerifySearchCriteria();"/>
                    <asp:Label Width="10" runat="server"  id= "lblSpace1" Text=""/> 								
                    <asp:Button ID="btnClear" runat="server" Text="<%$ Resources:btnClearResrc %>" OnClientClick="javascript:return ClearSearchCriteria();"  CssClass="buttonA" />
                </td>
            </tr>          
        </table> 
        <br />
        <div id="DivDefault" style="font-family:Arial; font-size:small" runat="server">
            <asp:Label runat="server" class="" ID="lblSpecifyCriteria" Text="<%$ Resources:lblSpecifyCriteriaResrc %>" />
        </div> 
        <div id="DivProcess" runat="server" >
            <table width="100%">    
                <tr>
                    <td class="msgheader">
                        <asp:Label runat="server" class="" ID="lblSearchResults" Text="<%$ Resources:lblSearchResultsResrc %>" />
                    </td>
                </tr>
            </table>
            <!-- Grid Control to display the search results -->
            <asp:Panel ID="Panel1" runat="server" Style="width: 100%; height: 80%; overflow: auto; left: 0px;" ScrollBars="Vertical">
                <asp:GridView ID="gvSelectPayments" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                AllowSorting="true" Width="1050px" ShowHeader="true" EnableViewState="false"
                EmptyDataText="<%$ Resources:gvNoRecordsToDisplay %>"
                OnRowDataBound="gvSelectPayments_RowDataBound" OnSorting="gvSelectPayments_Sorting">
                    <PagerStyle CssClass="gridLockedHeader" ForeColor="" />
                    <HeaderStyle CssClass="colheader6"  />
                    <AlternatingRowStyle CssClass="data2" /> 
                    <FooterStyle CssClass ="colheader6" />
                    <Columns>              
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader" ControlStyle-Width="10px">
                            <ItemTemplate>
                                <input type="checkbox" onclick="javascript:Calculate(this.id);" id='chkRecord<%# DataBinder.Eval(Container.DataItem, "key")%>'
                                name='chkRecord' value='<%# DataBinder.Eval(Container.DataItem, "id")%>' class="checkbox" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrClient %>" DataField="client" ControlStyle-Width="60px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="1">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrTransactionDate %>" DataField="transactiondate" ControlStyle-Width="70px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="2">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrPayeeName %>" DataField="payeename" ControlStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="3">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrClaimNumber %>" DataField="claimnumber" ControlStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="4">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrAmount %>" DataField="amount" ControlStyle-Width="70px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="5">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrTransType %>" DataField="transactiontype" ControlStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="6">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrAccountName %>" DataField="accountname" ControlStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="7">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrReserveType %>" DataField="reservetype" ControlStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="8">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrFromDate %>" DataField="fromdate" ControlStyle-Width="80px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="9">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrToDate %>" DataField="todate" ControlStyle-Width="80px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="10">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrInvoice %>" DataField="invoicenumber" ControlStyle-Width="100px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="11">
                        </asp:BoundField>

                        <asp:BoundField HeaderText="<%$ Resources:gvHdrInvoiceDate %>" DataField="invoicedate" ControlStyle-Width="70px"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="gridLockedHeader"
                        FooterStyle-CssClass="headertext2" SortExpression="12">
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <table width="100%">    
                <tr>
                    <td class="style6">
                    </td>
                    <td align="right" class="style6">
                    </td>
                    <td align="left" class="style7">
                    </td>
                    <td align="right" class="style7">
                    </td>
                    <td align="left" class="style6">
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <input type="button" class="buttonA" name="btnAll" runat="server" value="<%$ Resources:btnAllResrc %>" onclick="SelectAll();" align="left" />
                        <input type="button" class="buttonA" name="btnDeselectAll" runat="server" value="<%$ Resources:btnDeselectAllResrc %>" onclick="UnSelectAll();" />
                    </td>
                    <td align="right">
                        <asp:Label ID="Label1" Text="<%$ Resources:lbl1Resrc %>" runat="server" Font-Bold="true" />
                    </td>
                    <td align="left" class="style4">
                        <asp:TextBox ID="txtUnsAmount" runat="server" ReadOnly="True" Width="128px" value="$0.00" />
                    </td>
                    <td align="right" class="style1">
                        <asp:Label ID="lblAmount" Text="<%$ Resources:lblAmtResrc %>" runat="server" Font-Bold="True" />
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtTotal" runat="server" ReadOnly="True" Width="128px" value="$0.00" />
                    </td>
                </tr>
                <tr>
                    <td class="style3"></td>
                    <td align="right" class="style3"></td>
                    <td align="left" class="style3"></td>
                    <td align="right" class="style5"></td>
                    <td align="left" class="style3"></td>
                </tr>
                <tr>
                    <td align="left" class="style2">
                        <asp:Label ID="Label2" Text="<%$ Resources:lbl2Resrc %>" runat="server" />
                        <asp:DropDownList ID="cboCheckStatus" type="combobox" rmxref="/Instance/Document/BulkCheckRelease/control[@name ='checkstatus']/@value"
                        itemsetref="/Instance/Document/BulkCheckRelease/control[@name ='checkstatus']" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:HiddenField ID="hdnUnselectedAmount" runat="server" Value="0" />
                    </td>
                    <td align="right" colspan="3">
                        <asp:Button ID="btnProcess" UseSubmitBehavior="true" runat="server" Text="<%$ Resources:btnProcessResrc %>"
                        CssClass="buttonA" OnClientClick="return ProcessPayments();" />
                        <asp:Label ID="lblBlnkSp1" runat="server" Height="16px" Width="18px"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>   
        <div id="DivMessage" style="font-family:Arial; font-size:small" runat="server">
            <br />
            <asp:Label ID="lblFooter" runat="server" Text="<%$ Resources:lblFooterResrc %>" />
        </div>    
        <!-- End -->
    </form>
</body>
</html>
