﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SplitInfoRender.aspx.cs"
    Inherits="Riskmaster.UI.Funds.SplitInfoRender" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }</script>
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript">        { var i; }</script>
    <script language="javascript" type="text/javascript">
        function haveProperty(obj, sPropName) {
            for (p in obj) {
                if (p == sPropName)
                    return true;
            }
            return false;
        }

        function onDataLoaded() {
            obj = eval("document.forms[0].ExchangeRate");
            if (obj != null)
                obj2 = eval("window.opener.document.forms[0].ExchangeRate");
            if (obj2 != null) {
                obj2.value = obj.value;
                obj2.cancelledvalue = obj.value;
            }
            obj = eval("document.forms[0].ExchangeRateDate");
            if (obj != null)
                obj2 = eval("window.opener.document.forms[0].ExchangeRateDate");
            if (obj2 != null) {
                obj2.value = obj.value;
                obj2.cancelledvalue = obj.value;
            }
            obj = eval("document.forms[0].SplitToPaymentCurrRate");
            if (obj != null)
                obj2 = eval("window.opener.document.forms[0].SplitToPaymentCurrRate");
            if (obj2 != null) {
                obj2.value = obj.value;
                obj2.cancelledvalue = obj.value;

            }
            window.opener.document.forms[0].submit();
            if (haveProperty(window.opener, "lookupCallback")) {
                if (window.opener.lookupCallback != null) {

                    eval("window.opener." + window.opener.lookupCallback + "(-1,'',document.forms[0].hdDataChanged.value)");
                }
                return true;
            }
            return true;
        }

    </script>
</head>
<body onload="onDataLoaded();" style="display: none">
    <form id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </div>
    <asp:TextBox Style="display: none" runat="server" ID="ExchangeRate" rmxref="" rmxtype="id" />
    <asp:TextBox Style="display: none" runat="server" ID="ExchangeRateDate" rmxref=""
        rmxtype="id" />
    <asp:TextBox Style="display: none" runat="server" ID="SplitToPaymentCurrRate" rmxref=""
        rmxtype="id" />
    <asp:Label Style="display: none" runat="server" class="label" ID="pmtcurrencytypetext"
        RMXRef="/Instance/Funds/PmtCurrencyType" RMXType="labelonly"></asp:Label>
    <asp:HiddenField ID="hdDataChanged" runat="server" />
    </form>
</body>
</html>
    