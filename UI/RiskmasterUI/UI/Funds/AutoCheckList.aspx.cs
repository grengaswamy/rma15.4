﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Data;
using System.Collections;
using System.Web.UI.HtmlControls;


namespace Riskmaster.UI.UI.Funds
{
    public partial class AutoCheckList : NonFDMBasePageCWS
    {


        string sCWSresponse = string.Empty;
        private XElement XmlTemplate = null;
        private int iRows = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!Page.IsPostBack)
                {
                    ClaimId.Text = AppHelper.GetQueryStringValue("ClaimId");
                    ClaimantEid.Text = AppHelper.GetQueryStringValue("ClaimantEid");
                    UnitId.Text = AppHelper.GetQueryStringValue("UnitId");
                    UnitRowId.Text = AppHelper.GetQueryStringValue("UnitRowId");
                    FormTitle.Text = AppHelper.GetQueryStringValue("FormTitle");
                    SubTitle.Text = AppHelper.GetQueryStringValue("SubTitle");
                    Caption.Text = AppHelper.GetQueryStringValue("Caption");
                    //skhare7 r8 Combined Payment
                    EntityId.Text = AppHelper.GetQueryStringValue("EntityId");
                    //skhare7 end
                    BOBMode.Text = AppHelper.GetQueryStringValue("CheckMode");
                    ClaimantListClaimantEID.Text = AppHelper.GetQueryStringValue("ClaimantListClaimantEID");//RMA-8490
                    //lblcaption.Text = "Payment History (" + Caption.Text + ")";
                    lblcaption.Text = GetResourceValue("lblcaptionResrc", "0") + " (" + Caption.Text + ")";
                    this.hdCurrencyType.Text = AppHelper.GetQueryStringValue("CurrencyType");
                    CallCWS("FundManagementAdaptor.GetAutoCheckList", XmlTemplate, out sCWSresponse, true, true);
                    BindDataToGrid(sCWSresponse);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }



        }
        private void BindDataToGrid(string sCWSresponse)
        {

            try
            {
                XElement objAutoCheckList = XElement.Parse(sCWSresponse);
                if (objAutoCheckList.XPathSelectElements("//row") != null)
                {
                    grdAutoCheckList.HeaderStyle.CssClass = "colheader3";
                    grdAutoCheckList.HeaderStyle.ForeColor = System.Drawing.Color.White;
                    grdAutoCheckList.RowStyle.CssClass = "rowlight";
                    grdAutoCheckList.AlternatingRowStyle.CssClass = "rowdark2";

                    DataRow drAutoCheckList = null;
                    DataTable dtAutoCheckList = new DataTable();


                    IEnumerable result = null;

                    //dtAutoCheckList.Columns.Add("Check Date");
                    //dtAutoCheckList.Columns.Add("Control #");
                    //dtAutoCheckList.Columns.Add("Payee");
                    //dtAutoCheckList.Columns.Add("Claim Number");
                    //dtAutoCheckList.Columns.Add("Amount");
                    //dtAutoCheckList.Columns.Add("AutoBatchId");
                    dtAutoCheckList.Columns.Add(GetResourceValue("gvHdrCheckDate","0"));
                    dtAutoCheckList.Columns.Add(GetResourceValue("gvHdrControl", "0"));
                    dtAutoCheckList.Columns.Add(GetResourceValue("gvHdrPayee", "0"));
                    dtAutoCheckList.Columns.Add(GetResourceValue("gvHdrClaimNumber", "0"));
                    dtAutoCheckList.Columns.Add(GetResourceValue("gvHdrAmount", "0"));
                    dtAutoCheckList.Columns.Add(GetResourceValue("gvAutoBatchId", "0"));
                    
                    

                    result = from row in objAutoCheckList.XPathSelectElements("//row")
                             select row;
                    foreach (XElement item in result)
                    {
                        drAutoCheckList = dtAutoCheckList.NewRow();

                        drAutoCheckList[0] = item.XPathSelectElement("PrintDate").Value;
                        drAutoCheckList[1] = item.XPathSelectElement("ControlNumber").Value;
                        drAutoCheckList[2] = item.XPathSelectElement("Name").Value;
                        drAutoCheckList[3] = item.XPathSelectElement("ClaimNumber").Value;
                        drAutoCheckList[4] = item.XPathSelectElement("Amount").Value;
                        drAutoCheckList[5] = item.XPathSelectElement("PrintDate").Attribute("AutoBatchId").Value;

                        dtAutoCheckList.Rows.Add(drAutoCheckList);
                        iRows++;
                    }
                    drAutoCheckList = dtAutoCheckList.NewRow();
                    dtAutoCheckList.Rows.Add(drAutoCheckList);
                    grdAutoCheckList.DataSource = dtAutoCheckList;
                    grdAutoCheckList.DataBind();
                }
				//RMA-6086
                //if (drdCurrencytype.Items.Count == 1)
                //{
                //    lblCurrencytype.Visible = false;
                //    drdCurrencytype.Visible = false;
                //}
				//RMA-6086
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
		//RMA-6086
        //protected void drdCurrencytype_onchange(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        CallCWS("FundManagementAdaptor.GetAutoCheckList", XmlTemplate, out sCWSresponse, true, true);
        //        BindDataToGrid(sCWSresponse);
        //    }
        //    catch (Exception ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        //        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
        //    }
        //}
		//RMA-6086
        protected void grdAutoCheckList_RowDataBound(object Sender, GridViewRowEventArgs e)
        {
            try
            {
                string imageurl = @"../../Images/";
                string imageSortUpUrl = imageurl + "arrow_up_white.gif";
                string imageSortDownUrl = imageurl + "arrow_down_white.gif";

                if (e.Row.RowType == DataControlRowType.Header)
                {


                    int iCol = GetSortColumnIndex(SortCol.Text);
                    if (-1 == iCol)
                    {
                        return;
                    }
                    HtmlImage imgSort = new HtmlImage();
                    if (Ascending.Text.ToLower() == "false")
                    {
                        imgSort.Src = imageSortDownUrl;
                    }
                    else
                    {
                        imgSort.Src = imageSortUpUrl;
                    }

                    e.Row.Cells[iCol].Controls.Add(imgSort);
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex == iRows)
                {

                    e.Row.Visible = false;
                }
                else if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (!String.IsNullOrEmpty(e.Row.Cells[0].Text) && e.Row.Cells[0].Text != "&quot;")
                    {
                        e.Row.Cells[0].Text = "<a href='/RiskmasterUI/UI/FDM/autoclaimchecks.aspx?SysFormId=" + e.Row.Cells[e.Row.Cells.Count - 1].Text + "&SysCmd=0" + "&ClaimantListClaimantEID=" + ClaimantListClaimantEID.Text + "'>" + e.Row.Cells[0].Text + "</a>";
                    }
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        protected void grdAutoCheckList_Sorting(object Sender, GridViewSortEventArgs e)
        {
            try
            {
                string m_strSortExp = GetSortColumn(e.SortExpression);
                if (String.Empty != SortCol.Text)
                {
                    if (String.Compare(SortCol.Text, m_strSortExp, true) == 0)
                    {
                        if (Ascending.Text.ToLower() == "false")
                        {

                            Ascending.Text = "True";
                        }
                        else
                        {
                            Ascending.Text = "False";
                        }
                    }
                    else
                    {
                        Ascending.Text = "False";
                        SortCol.Text = m_strSortExp;
                    }
                }
                CallCWS("FundManagementAdaptor.GetAutoCheckList", XmlTemplate, out sCWSresponse, true, true);
                BindDataToGrid(sCWSresponse);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        private string GetSortColumn(string sortExpression)
        {
            switch (sortExpression)
            {
                case "Check Date":
                    return "PRINT_DATE";
                case "Control #":
                    return "CTL_NUMBER";
                case "Payee":
                    return "LAST_NAME";
                case "Claim Number":
                    return "CLAIM_NUMBER";
                case "Amount":
                    //return "AMOUNT";
                    return "AMT";                 
                default:
                    return "";
            }

        }
        private int GetSortColumnIndex(string sortColumn)
        {
            switch (sortColumn)
            {
                case "PRINT_DATE":
                    return 0;
                case "CTL_NUMBER":
                    return 1;
                case "LAST_NAME":
                    return 2;
                case "CLAIM_NUMBER":
                    return 3;
                //case "AMOUNT":
                case "AMT":
                    return 4;
                default:
                    return -1;
            }

        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("AutoCheckList.aspx"), strResourceType).ToString();
        }
    }
}
