﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReservesInfoRender.aspx.cs" Inherits="Riskmaster.UI.Funds.ReservesInfoRender" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reserves Info Render</title>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">{var i;}</script>
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript">{var i;}</script>
    <script language="javascript" type="text/javascript">
        

					function haveProperty(obj, sPropName)
					{
						for(p in obj)
						{
							if(p==sPropName)
								return true;
						}
						return false;
					}


					function onDataLoaded() {					  
					    //skhare7 R8 supervisory approval
					    obj = eval("document.forms[0].hdnReserveStatus");
					    if (obj != null)
					        var sReservestatus = obj.value;
					    if (sReservestatus == 'H' || sReservestatus == 'R') { //Add OR "sReservestatus == 'R'" condition by kuladeep  for mits:34035 
					        obj2 = eval("window.opener.document.forms[0].ResTypeStatus");
					        if (obj2 != null) {
					            obj2.value = obj.value;
					            obj2.cancelledvalue = obj.value;
					         
					        } 
					    }
					    //skhare7 R8 supervisory approval End
						obj=eval("document.forms[0].reservetypecode");
						if(obj!=null)
						obj2=eval("window.opener.document.forms[0].ReserveTypeCode_codelookup");
						if(obj2!=null)
						{
							obj2.value=obj.value;
							obj2.cancelledvalue=obj.value; 
							
						}
						//IC Retrofit for REM Transaction Code , MITS 20093
						//Added by asingh264 on 12 March 2010 REM-MITS #19821 To fetch the reservetype value along with code and put it in the corresponding textbox
						if(obj!=null)
						obj3=eval("window.opener.document.forms[0].ReserveTypeCodeFt_codelookup");
						if(obj3!=null)
						{
							obj3.value=obj.value;
							obj3.cancelledvalue=obj.value; 
						}
			//End of REM-MITS #19821 asingh264
                        //code was Commented skhare7
						obj=eval("document.forms[0].reservebalance");
						if(obj!=null)
						obj2=eval("window.opener.document.forms[0].reservebalance");
						if(obj2!=null)
						{
							obj2.value=formatCurrency(obj.value, "");
							obj2.cancelledvalue=formatCurrency(obj.value, "");
			            }
			            //Deb Multi Currency
			            obj = eval("document.forms[0].ClaimCurrReserveBal");
			            if (obj != null)
			                obj2 = eval("window.opener.document.forms[0].ClaimCurrReserveBal");
			            if (obj2 != null) {
			                obj2.value = formatCurrency(obj.value, "");
			                obj2.cancelledvalue = formatCurrency(obj.value, "");
			            } 
			            //Deb Multi Currency
						obj=eval("document.forms[0].reservetypecode_cid");
						if(obj!=null)
						obj2=eval("window.opener.document.forms[0].ReserveTypeCode_codelookup_cid");
						if(obj2!=null)
						{
							obj2.value=obj.value;
							obj2.cancelledvalue=obj.value;
		           	}
			      //code was Commented skhare7
						//IC Retrofit for REM Transaction Code , MITS 20093
						//Added by asingh264 on 12 March 2010 REM-MITS #19821 To fetch the reservetype value along with code and put it in the corresponding textbox
						if(obj!=null)
						obj3=eval("window.opener.document.forms[0].ReserveTypeCodeFt_codelookup_cid");
						if(obj3!=null)
						{
							obj3.value=obj.value;
							obj3.cancelledvalue=obj.value; 
						}
						//End of REM-MITS #19821 asingh264
						if(haveProperty(window.opener,"lookupCallback"))
						{
							if(window.opener.lookupCallback!=null) {

							    eval("window.opener." + window.opener.lookupCallback + "(-1,'',document.forms[0].hdDataChanged.value)");
							    //akaur9 11/04/2011 MITS 26532 
							    obj = eval("document.forms[0].hdnReserveStatus");
							    if (obj != null)
							        var sResstatus = obj.value;
							    if (sResstatus == 'H') {
							        obj2 = eval("window.opener.document.forms[0].reservebalance");
							        if (obj2 != null) {
							            obj2.value = "";
							            obj2.cancelledvalue = "";
							        }
							    }   //akaur9 11/04/2011 MITS 26532 
								return true;
							}
						}
						return true;
					}
	</script>
</head>
<body onload="onDataLoaded();" style="display: none">
    <form id="frmData" runat="server">


    <div>
    
       <uc1:ErrorControl ID="ErrorControl" runat="server"/>
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />

    </div>
        <asp:textbox style="display: none" runat="server" id="claimanteid" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="unitid" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="transid" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="claimid" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="transtypecodeid" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="reservetypecode" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="reservetypecode_cid" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="reservebalance" rmxref="" rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="OK_Button" rmxref="" rmxtype="id" />
         <%-- skhare7   R8 superviaory Approval--%>
        <asp:textbox style="display: none" runat="server" id="hdnReserveStatus" rmxref="" rmxtype="id" />
         <%-- skhare7   R8 superviaory Approval End--%>
        
        <asp:HiddenField ID="hdDataChanged" runat="server" />
        <%--Deb Multi Currency--%>
        <asp:textbox style="display: none" runat="server" id="ClaimCurrReserveBal" rmxref="" rmxtype="id" />
        <%--Deb Multi Currency--%>
    </form>
</body>
</html>

