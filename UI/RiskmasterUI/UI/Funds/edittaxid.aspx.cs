﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml;
using System.Xml.Linq;
namespace Riskmaster.UI.Funds
{
    public partial class edittaxid : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblName.Text = AppHelper.GetQueryStringValue("PayeeName");            
            EntityID.Text = AppHelper.GetQueryStringValue("EntityID");
            // prashbiharis MITS  35027
            if (!IsPostBack)
            {
                inTaxID.Text = AppHelper.GetQueryStringValue("TaxId");
                TaxId.Text = AppHelper.GetQueryStringValue("TaxId");
            }
            try
            {
                XElement XmlTemplate = null;
                bool bReturnStatus = false;
                string CWSRes= string.Empty;
                XmlDocument resXML = new XmlDocument();
               
                if (IsPostBack && SSNFormat.Text == "1")
                {
                    XmlTemplate = GetMessageTemplate("EntitySSNCheckAdaptor.CheckSSN");
                    bReturnStatus = CallCWSFunction("EntitySSNCheckAdaptor.CheckSSN", out CWSRes, XmlTemplate);
                    if (bReturnStatus)
                    {
                        resXML.LoadXml(CWSRes);                        
                        if (resXML.SelectSingleNode("//Entities").Attributes["dupexists"] != null && resXML.SelectSingleNode("//Entities").Attributes["dupexists"].Value == "0")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Update", "EditTaxID()", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "dupTaxID", "DupTaxIDExists()", true);
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }
            // prashbiharis MITS  35027
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            isUpdateTaxId.Text = "True";

        }
        private XElement GetMessageTemplate(string sFunctionName)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append(sFunctionName);
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><DupSSN><TaxId>");
            sXml = sXml.Append(TaxId.Text);
            sXml = sXml.Append("</TaxId><EntityId>");
            sXml = sXml.Append(EntityID.Text);
            sXml = sXml.Append("</EntityId><IgnoreForFuture>");
            sXml = sXml.Append("False");
            sXml = sXml.Append("</IgnoreForFuture><FormMode/>");
            sXml = sXml.Append("<temp/></DupSSN></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }      

    }
}
