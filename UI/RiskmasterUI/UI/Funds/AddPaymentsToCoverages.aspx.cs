﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Funds
{
    public partial class AddPaymentsToCoverages : System.Web.UI.Page
    {
        public XmlDocument Model = null;
	//Changed by Gagan for MITS 22238 : Start
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.Get</Function></Call><Document><Policies><FromDate></FromDate><ToDate></ToDate><PolicyName></PolicyName><PolicyNumber></PolicyNumber><PolicyType></PolicyType></Policies></Document></Message>";
	//Changed by Gagan for MITS 22238 : End
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AddPaymentsToCoverages.aspx"), "AddPaymentsToCoveragesValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "AddPaymentsToCoveragesValidationScripts", sValidationResources, true);
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }

                if (!IsPostBack)
                {
                    //rupal:start, r8 enh
                    //if carrier claim is on, disable policy management radio button
                    EnableDisablePolicyMgt();
                    //rupal:end
	                //Changed by Gagan for MITS 22238 : Start
                    divForms.Attributes.Add("style", "overflow:scroll;height:65%;width:100%;border-width:'0'");
	                //Changed by Gagan for MITS 22238 : End
                    divForms.Attributes.Add("class", "divScroll");
                    Model = new XmlDocument();
                    MergeMessageTemplate = BindBackData();
                    string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    Model.LoadXml(sCWSresponse);
                    BindpageControls(sCWSresponse);
                }
                else
                {
                   
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
                   
        private string BindBackData()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeMessageTemplate);
            doc.SelectSingleNode("//ToDate").InnerText = txtToDate.Value;
            doc.SelectSingleNode("//FromDate").InnerText =txtFromDate.Value;
	        //Changed by Gagan for MITS 22238 : Start
            doc.SelectSingleNode("//PolicyName").InnerText = txtPolicyName.Text;
            doc.SelectSingleNode("//PolicyNumber").InnerText = txtPolicyNumber.Text;
            if (rbtnPolicyTracking.Checked == true)
            {
                doc.SelectSingleNode("//PolicyType").InnerText = "Policy Tracking";
            }
            else
            {
                doc.SelectSingleNode("//PolicyType").InnerText = "Policy Management";
            }
	        //Changed by Gagan for MITS 22238 : End
            return doc.OuterXml;
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="diaryDoc"></param>
        /// <returns></returns>
        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
	//Changed by Gagan for MITS 22238 : Start
                dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//Policies")));
	//Changed by Gagan for MITS 22238 : End
                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        private void BindpageControls(string sreturnValue)
        {
            try
            {
                DataSet usersRecordsSet = null;
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
                if (usersRecordsSet != null)
                {
                    if (usersRecordsSet.Tables.Count > 0)
                    {
                        gvSelectClaims.DataSource = usersRecordsSet.Tables[0].DefaultView;
                        gvSelectClaims.DataBind();
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        usersRecordsSet.Tables.Add(dt);
                        gvSelectClaims.DataBind();

                    }
                }

            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                MergeMessageTemplate = BindBackData();
                Model = new XmlDocument();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                Model.LoadXml(sCWSresponse);
                BindpageControls(sCWSresponse);               
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
             
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        //start:rupal, r8 enh
        private void EnableDisablePolicyMgt()
        {   
            string sCWSresponse = AppHelper.CallCWSService(GetMessageTemplate());
            XmlDocument ObjDoc = new XmlDocument();
            ObjDoc.LoadXml(sCWSresponse);
            XmlNode objCarrierClaimsNode = ObjDoc.SelectSingleNode("//UseCarrierClaims");
            if (objCarrierClaimsNode != null && objCarrierClaimsNode.InnerText == "True")
            {
                rbtnPolicyManagement.Visible = false;
            }
            else
            {
                rbtnPolicyManagement.Visible = true;
            }
        }

        private string GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>FundManagementAdaptor.CheckUtility</Function></Call><Document><UseCarrierClaims></UseCarrierClaims></Document></Message>");

            return sXml.ToString();
        }
        //end:rupal
    }
}
