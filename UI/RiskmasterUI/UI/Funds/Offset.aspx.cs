﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Data;
using System.Web.UI.HtmlControls;
using Riskmaster.Common;
using System.Globalization;
using System.Threading;
using Riskmaster.RMXResourceManager;
using System.Text;

namespace Riskmaster.UI.Funds
{
    public partial class Offset : System.Web.UI.Page
    {
        XElement oThisMessageElement = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            iTransId.Text = AppHelper.GetQueryStringValue("TransId");            
        }

        private XElement GetOffsetReserveXml()
        {
            XElement objOffsetReserveXml = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>ReserveFundsAdaptor.OffsetReserves</Function> 
                        </Call>
                    <Document>
						<ReserveFunds>
							<TransId /> 
							<ParentSecurityId /> 
							<SecurityId /> 
						</ReserveFunds>
                    </Document>
                </Message>
            ");

            return objOffsetReserveXml;
        }

        protected void btnOffset_Click_old(object sender, EventArgs e)
        {
            XmlDocument objXml = null;
            XElement objInsufficientReserves = null;
            XElement objTempElement = null;

            oThisMessageElement = GetOffsetReserveXml();

            if (HttpContext.Current.Request.Cookies["SessionId"] != null)
            {
                objTempElement = oThisMessageElement.XPathSelectElement("./Authorization");
                objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
            }

            objTempElement = oThisMessageElement.XPathSelectElement("Document/ReserveFunds/TransId");
            objTempElement.Value = iTransId.Text;

            string sReturn = AppHelper.CallCWSService(oThisMessageElement.ToString());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void btnOffset_Click(object sender, EventArgs e)
        {
            XmlDocument objXml = null;
            XElement objInsufficientReserves = null;
            XElement objTempElement = null;

            oThisMessageElement = GetOffsetReserveXml();

            if (HttpContext.Current.Request.Cookies["SessionId"] != null)
            {
                objTempElement = oThisMessageElement.XPathSelectElement("./Authorization");
                objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
            }

            objTempElement = oThisMessageElement.XPathSelectElement("Document/ReserveFunds/TransId");
            objTempElement.Value = iTransId.Text;

            string sReturn = AppHelper.CallCWSService(oThisMessageElement.ToString());


            Refresh_Page(sReturn);
        }

        /// <summary>
        /// This function is used to refresh the data
        /// </summary>
        private void Refresh_Page(string sPreviousData)
        {
            try
            {
                
                XmlDocument xmlReturnErrorTest = new XmlDocument();
                xmlReturnErrorTest.LoadXml(sPreviousData);
                if (xmlReturnErrorTest.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    ErrorControl1.errorDom = sPreviousData;
                }
                else
                {
                    lbl_OffsetDesc.Text = "The Offset Process Completed Successfully";
                    btnOffset.Enabled = false;
                    btnCancel.Text = "Close";
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }// method: Refresh_Page

    }
}
