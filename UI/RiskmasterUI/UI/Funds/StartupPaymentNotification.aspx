﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StartupPaymentNotification.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.StartupPaymentNotification" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
        <title>Payment Notification</title>    
        <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    </head>
    <body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
        <form id="frmData" runat="server">
            <div id="maindiv" style="height:100%;width:99%;overflow:auto">
                <table width="100%">    
                    <tr>
                        <td class="msgheader">
                            <asp:Label runat="server" ID="lblChecks"></asp:Label> 
                            <asp:Label ID="lblPrintedChecks" runat="server" Text="<%$ Resources:lblPrintedChecksResrc %>" /> 
                        </td>
                    </tr>    
                    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
                </table>
                <asp:Panel ID="divForms" runat="server">
                    <asp:GridView ID="gvSelectChecks" runat="server" AutoGenerateColumns="false" 
                    AllowPaging="false" AllowSorting="true" Width="100%" ShowHeader="true"  EnableViewState="false"
                    EmptyDataText="<%$ Resources:gvNoChecks %>">
          
                    <PagerStyle CssClass="headertext2"  ForeColor="White" />
                    <HeaderStyle CssClass="colheader6" />
                    <AlternatingRowStyle CssClass="data2" /> 
                    <FooterStyle  CssClass ="colheader6" />
                    <Columns>   
                        <asp:BoundField  HeaderText="<%$ Resources:gvHdrCheckDate %>"  DataField="CheckDate" 
                        ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                        
                        <asp:BoundField   HeaderText="<%$ Resources:gvHdrControl %>"  DataField="ControlNumber"  ControlStyle-Width="20%"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                               
                        <asp:BoundField   HeaderText="<%$ Resources:gvHdrPayee %>"  DataField="Payee"  ControlStyle-Width="20%"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                        
                        <asp:BoundField   HeaderText="<%$ Resources:gvHdrClaimNumber %>"  DataField="ClaimNumber"  ControlStyle-Width="20%"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                
                        <asp:BoundField   HeaderText="<%$ Resources:gvHdrAmount %>"  DataField="Amount"  ControlStyle-Width="20%"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                    </Columns>
                    </asp:GridView>            
                </asp:Panel>
                <table>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <td align="right" width="75%" class="finlabel">
                                <asp:Label runat="server" ID="lblfinlabel" Text="<%$ Resources:lblTotalPayAmountResrc %>" />
                            </td>
                            <td align="left" width="25%" class="fintotals">
                                <asp:Label id="lblTotal" runat="server" />
                            </td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnPrint" UseSubmitBehavior="true" runat="server" Text="<%$ Resources:btnPrintResrc %>" CssClass="button" onclick="btnPrint_Click"/>
                        </td>
                    </tr>
                </table>
                <table border="0">
                    <tr>
                        <td></td> 
                    </tr>      
                </table>
            </div>
        </form>
    </body>
</html>
