﻿/**********************************************************************************************
 *   Date     |  Jira   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using System.Text;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Funds
{
    public partial class ControlRequest : System.Web.UI.Page
    {
        XElement oMessageElement = null;
        XElement oSessionCmdElement = null;
        string sTransId = string.Empty;
        XElement XmlTemplate = null;
        bool bReturnStatus = false;
        enum eMessageTemplate { GetSearchResult, CheckUtility, ProcessControlRequest, GetSearchResultAfterRequestProcessed };  //mkaran2 - Control Request Functionlity  - MITS 34569
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument objXml = null;
            try
            {
                // Fetch the request parameters and set the corresponding XElement nodes.
                successctrlreq.Text = string.Empty;
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ControlRequest.aspx"), "ControlRequestValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "ControlRequestValidationScripts", sValidationResources, true);

                if (!Page.IsPostBack)
                {
                    oMessageElement = GetMessageTemplate(eMessageTemplate.CheckUtility);
                    MakeWebServiceCall();
                    objXml = GetDummyXmlForGrid();
                    TransSplitGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");//rupal:mits 33600
                    TransSplitGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");//rupal:mits 33600                    
                    TransSplitGrid.BindData(objXml);
                    objXml = GetDummyXmlForDestGrid();
                    DestFinKeysGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");//rupal:mits 33600
                    DestFinKeysGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");//rupal:mits 33600                    
                    DestFinKeysGrid.BindData(objXml);
                    //DisableGrids();
                    EnableDisbleReserveTypeFt();
                    objXml = null;
                    oMessageElement = null;
                }
                else
                {
                    EnableDisbleReserveTypeFt();
                    if (string.Compare(this.functiontocall.Text, "GetDataForSelectedFund") == 0)
                    {
                        this.functiontocall.Text = string.Empty;
                        oMessageElement = GetMessageTemplate(eMessageTemplate.GetSearchResult);
                        if (!string.IsNullOrEmpty(pay_transid.Text))
                        {
                            sTransId = pay_transid.Text;
                            oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/EntityTableId");
                            oSessionCmdElement.Value = sTransId;
                        }
                        objXml = GetDataForSelectedFund();
                        //Deb: MITS 31004  
                        //rupal:start,mits 33600
                        // need lineofbuscode before getting the xml template for the grid
                        SetServiceData(objXml);                        
                        //rupal:end
                        XmlDocument objXmlTemp = GetDummyXmlForGrid();
                        objXml.SelectSingleNode("//listhead").InnerXml = objXmlTemp.SelectSingleNode("//listhead").InnerXml;
                        //Deb: MITS 31004  
                        if (objXml.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                        {
                            XmlNode oInstanceNode = objXml.SelectSingleNode("/ResultMessage/Document");
                            objXml.LoadXml(oInstanceNode.InnerXml);
                            //rupal:start,mits 33600
                            if (lineofbusinesscode.Text.Trim() != "243")
                            {
                                TransSplitGrid.HideColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                TransSplitGrid.ShowColumns = GetResourceValue("GridHdrLossType", "0");
                            }
                            else
                            {
                                TransSplitGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");
                                TransSplitGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                            }                           
                            
                            //rupal:end,mits 33600
                            TransSplitGrid.BindData(objXml);
                            if (usecarrierclaim != null)
                            {
                                if (usecarrierclaim.Text == "True")
                                {
                                    objXmlTemp = GetDummyXmlForDestGrid();
                                    objXml.SelectSingleNode("//DestFinKeysGrid/listhead").InnerXml = objXmlTemp.SelectSingleNode("//DestFinKeysGrid/listhead").InnerXml;
                                    //rupal:start,mits 33600
                                    if (lineofbusinesscode.Text.Trim() != "243")
                                    {
                                        DestFinKeysGrid.HideColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                        DestFinKeysGrid.ShowColumns = GetResourceValue("GridHdrLossType", "0");
                                    }
                                    else
                                    {
                                        DestFinKeysGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");
                                        DestFinKeysGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                    }                                    
                                    //rupal:end,mits 33600
                                    DestFinKeysGrid.BindData(objXml);
                                }
                            }
                            SetServiceData(objXml);
                            
                        }
                        else
                        {
                            //rupal:start,mits 33600
                            if (lineofbusinesscode.Text.Trim() != "243")
                            {
                                TransSplitGrid.HideColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                TransSplitGrid.ShowColumns = GetResourceValue("GridHdrLossType", "0");
                            }
                            else
                            {
                                TransSplitGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");
                                TransSplitGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                            }
                            
                            //rupal:end,mits 33600
                            objXml = GetDummyXmlForGrid();
                            TransSplitGrid.BindData(objXml);
                            if (usecarrierclaim.Text == "True")
                            {
                                //rupal:start,mits 33600
                                if (lineofbusinesscode.Text.Trim() != "243")
                                {
                                    DestFinKeysGrid.HideColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                    DestFinKeysGrid.ShowColumns = GetResourceValue("GridHdrLossType", "0");
                                }
                                else
                                {
                                    DestFinKeysGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");
                                    DestFinKeysGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                }
                                
                                //rupal:end,mits 33600
                                objXml = GetDummyXmlForDestGrid();
                                DestFinKeysGrid.BindData(objXml);
                            }
                            //throw new ApplicationException(objXml.InnerXml);
                            
                        }
                        ClearFormFields();
                        oMessageElement = null;
                    }
                    else
                    {
                        if (string.Compare(this.functiontocall.Text, "ProcessControlRequest") == 0)
                        {
                            oMessageElement = GetMessageTemplate(eMessageTemplate.ProcessControlRequest);
                            objXml = ProcessControlRequest();
                            
                            this.functiontocall.Text = string.Empty;
                            if (objXml.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                            {
                                successctrlreq.Text = "True";
                                //oMessageElement = GetMessageTemplate(eMessageTemplate.GetSearchResult);                               
                                oMessageElement = GetMessageTemplate(eMessageTemplate.GetSearchResultAfterRequestProcessed);//mkaran2 - Control Request Functionlity   - MITS 34569                   
                                if (!string.IsNullOrEmpty(pay_transid.Text))
                                {
                                    sTransId = pay_transid.Text;
                                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/EntityTableId");
                                    oSessionCmdElement.Value = sTransId;
                                }
                                objXml = GetDataForSelectedFund();
                                //Deb: MITS 31004  
                                //rupal:start,mits 33600
                                // need lineofbuscode before getting the xml template for the grid
                                SetServiceData(objXml);
                                //rupal:end
                                XmlDocument objXmlTemp = GetDummyXmlForGrid();
                                objXml.SelectSingleNode("//listhead").InnerXml = objXmlTemp.SelectSingleNode("//listhead").InnerXml;
                                //Deb: MITS 31004  
                                if (objXml.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                                {
                                    XmlNode oInstanceNode = objXml.SelectSingleNode("/ResultMessage/Document");
                                    objXml.LoadXml(oInstanceNode.InnerXml);
                                    //rupal:start,mits 33600
                                    if (lineofbusinesscode.Text.Trim() != "243")
                                    {
                                        TransSplitGrid.HideColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                        TransSplitGrid.ShowColumns = GetResourceValue("GridHdrLossType", "0");
                                    }
                                    else
                                    {
                                        TransSplitGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");
                                        TransSplitGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                    }
                                    //JIRA-857
                                    hdIsCollectionSource.Text = objXml.SelectSingleNode("//CheckCollMove").Value;
                                    //rupal:end,mits 33600
                                    TransSplitGrid.BindData(objXml);
                                    if (usecarrierclaim != null)
                                    {
                                        if (usecarrierclaim.Text == "True")
                                        {
                                            objXmlTemp = GetDummyXmlForDestGrid();
                                            objXml.SelectSingleNode("//DestFinKeysGrid/listhead").InnerXml = objXmlTemp.SelectSingleNode("//DestFinKeysGrid/listhead").InnerXml;
                                            //rupal:start,mits 33600
                                            if (lineofbusinesscode.Text.Trim() != "243")
                                            {
                                                DestFinKeysGrid.HideColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                                DestFinKeysGrid.ShowColumns = GetResourceValue("GridHdrLossType", "0");
                                            }
                                            else
                                            {
                                                DestFinKeysGrid.HideColumns = GetResourceValue("GridHdrLossType", "0");
                                                DestFinKeysGrid.ShowColumns = GetResourceValue("GridHdrDisabilityCatCode", "0") + "|" + GetResourceValue("GridHdrDisabilityTypeCode", "0");
                                            }

                                            //rupal:end,mits 33600
                                            DestFinKeysGrid.BindData(objXml);
                                        }
                                    }
                                    SetServiceData(objXml);
                                    ClearFormFields();
                                    //objXml.InnerXml = string.Empty;
                                }

                            }
                            oMessageElement = null;

                        }
                    }
                    BindData2ErrorControl(objXml);
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private void SetServiceData(XmlDocument objXml)
        {
            //rupal:mits 3600, added try block
            try
            {
            XElement oTemplate = XElement.Parse(objXml.InnerXml.ToString());
            orgEid.Text = oTemplate.XPathSelectElement("//orgEid").Value;
            clm_entityid.Text = oTemplate.XPathSelectElement("//clm_entityid").Value;
            claimid.Text = oTemplate.XPathSelectElement("//claimid").Value;
            lineofbusinesscode.Text = oTemplate.XPathSelectElement("//lineofbusinesscode").Value;
            transid.Text = oTemplate.XPathSelectElement("//transid").Value;
            transdate.Text = oTemplate.XPathSelectElement("//transdate").Value;

            //Asharma326 MITS 32734 Starts set Policy_ID
            if (oTemplate.XPathSelectElement("//Policy_ID") != null)
            Policy.Text = oTemplate.XPathSelectElement("//Policy_ID").Value;
            //Asharma326 MITS 32734 Ends set Policy_ID
            }
            catch (Exception e)
            {
                ErrorHelper.logErrors(e);
            }
        }

        private XElement GetMessageTemplate(eMessageTemplate eMessageTemplate)
        {
            XElement oTemplate = null;
            switch (eMessageTemplate)
            {
                case eMessageTemplate.CheckUtility:
                    StringBuilder sXml = new StringBuilder("<Message>");
                    sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
                    sXml = sXml.Append("<Call><Function>FundManagementAdaptor.CheckUtility</Function></Call><Document><Settings><UseResFilter></UseResFilter><UseCarrierClaims></UseCarrierClaims></Settings></Document></Message>");
                    oTemplate = XElement.Parse(sXml.ToString());
                    break;
                case eMessageTemplate.GetSearchResult:
                    oTemplate = XElement.Parse(@"
                    <Message>
                        <Authorization></Authorization> 
                            <Call>
                                <Function>ControlRequestAdaptor.GetSearchResultsForSelectedFund</Function> 
                            </Call>
                            <Document>
                                <GetEntityData>
                                    <EntityTableId></EntityTableId> 
                                </GetEntityData>
                            </Document>
                    </Message>
                    ");
                    break;
                //mkaran2 - Control Request Functionlity - start  - MITS 34569
                case eMessageTemplate.GetSearchResultAfterRequestProcessed:
                    oTemplate = XElement.Parse(@"
                    <Message>
                        <Authorization></Authorization> 
                            <Call>
                                <Function>ControlRequestAdaptor.GetSearchResultsForSelectedFund</Function> 
                            </Call>
                            <Document>
                                <GetEntityData>
                                    <EntityTableId></EntityTableId> 
                                    <RequestProcessed>true</RequestProcessed> 
                                </GetEntityData>
                            </Document>
                    </Message>
                    ");
                    break;
                //mkaran2 - Control Request Functionlity - end  - MITS 34569
                case eMessageTemplate.ProcessControlRequest:
                    oTemplate = XElement.Parse(@"
                    <Message>
                        <Authorization></Authorization> 
                            <Call>
                                <Function>ControlRequestAdaptor.ProcessControlRequest</Function> 
                            </Call>
                            <Document>
                                <ProcessControl>
                                    <transid></transid> 
                                    <splitid></splitid>
                                    <transtypecode></transtypecode>
                                    <reservetypecode></reservetypecode>
                                    <rcrowid></rcrowid>
                                    <reasontypecode></reasontypecode>
                                </ProcessControl>
                            </Document>
                    </Message>
                    ");
                    SetValues(ref oTemplate);
                    break;
            }
            return oTemplate;
        }

        private void SetValues(ref XElement oTemplate)
        {
            TextBox objTxtReserveTypeFt = (TextBox)this.FindControl("ReserveTypeCodeFt$codelookup_cid");
            TextBox objTxtReserveType = (TextBox)this.FindControl("ReserveTypeCode$codelookup_cid");
            TextBox objTxtTransactionType = (TextBox)this.FindControl("TransTypeCode$codelookup_cid");
            TextBox objTxtTransactionTypeFt = (TextBox)this.FindControl("TransTypeCodeFt$codelookup_cid");
            TextBox objTxtReasonType = (TextBox)this.FindControl("ChangeTypeCode$codelookup_cid");
            oTemplate.XPathSelectElement("//transid").Value = transid.Text;
            oTemplate.XPathSelectElement("//splitid").Value = transsplitid.Text;
            if (useresfilter.Text == "True")
            {
                oTemplate.XPathSelectElement("//reservetypecode").Value = objTxtReserveTypeFt.Text;
            }
            else
            {
                if (objTxtReserveType != null)
                {
                    oTemplate.XPathSelectElement("//reservetypecode").Value = objTxtReserveType.Text;
                }
            }
            if (usecarrierclaim != null)
            {
                if (usecarrierclaim.Text == "True")
                {
                    oTemplate.XPathSelectElement("//transtypecode").Value = objTxtTransactionTypeFt.Text;
                }
                else
                {
                    oTemplate.XPathSelectElement("//transtypecode").Value = objTxtTransactionType.Text;
                }
            }
            //oTemplate.XPathSelectElement("//transtypecode").Value = objTxtTransactionType.Text;
            oTemplate.XPathSelectElement("//rcrowid").Value = rcrowid.Text;
            oTemplate.XPathSelectElement("//reasontypecode").Value = objTxtReasonType.Text;

        }

        private XmlDocument GetDummyXmlForGrid()
        {
            //string strXML = string.Empty;
            //rupal:mits 33600
            //changed the code for performance optimization, replaced string concatanation with string builder
            StringBuilder sbXML = new StringBuilder();
            XmlDocument oTemplate = new XmlDocument();
            if (string.Compare(usecarrierclaim.Text, "True", true) != 0)
            {
                #region
//                    "<FundsSplits> " +
//                        "<listhead> " +
//                            "<TransTypeCode>" + GetResourceValue("GridHdrTransactionType", "0") + "</TransTypeCode> " +
//                            "<ReserveTypeCode>" + GetResourceValue("GridHdrReserveType", "0") + "</ReserveTypeCode> " +
//                            "<Amount>" + GetResourceValue("GridHdrAmount", "0") + "</Amount> " +
//                        "</listhead> " +
//                        "<option> " +
//                            "<TransTypeCode></TransTypeCode> " +
//                            "<SplitRowId></SplitRowId> " +
//                            "<ReserveTypeCode></ReserveTypeCode> " +
//                            "<Amount></Amount> " +
//                        "</option> " +
//                    "</FundsSplits> " +
//                "</Funds>";

//                oTemplate.LoadXml(@strXML);
////                oTemplate.LoadXml(@"
////                <Funds>
////                    <FundsSplits>
////                        <listhead>
////                            <TransTypeCode>Transaction Type</TransTypeCode>
////                            <ReserveTypeCode>Reserve Type</ReserveTypeCode>
////                            <Amount>Amount</Amount>
////                        </listhead>
////                        <option>
////                            <TransTypeCode></TransTypeCode> 
////                            <SplitRowId></SplitRowId> 
////                            <ReserveTypeCode></ReserveTypeCode> 
////                            <Amount></Amount>
////                        </option>
////                    </FundsSplits>
////                </Funds>
////                ");
//            }
//            else
//            {
//                strXML = "<Funds> " +
//                    "<FundsSplits> " +
//                        "<listhead> " +
//                            "<claimantname>" + GetResourceValue("GridHdrClaimantName", "0") + "</claimantname> " +
//                            "<PolicyID>" + GetResourceValue("GridHdrPolicy", "0") + "</PolicyID> " +
//                            "<UnitID>" + GetResourceValue("GridHdrUnit", "0") + "</UnitID> " +
//                            "<CoverageTypeCode>" + GetResourceValue("GridHdrCoverageType", "0") + "</CoverageTypeCode> " +
//                            "<ReserveTypeCode>" + GetResourceValue("GridHdrReserveType", "0") + "</ReserveTypeCode> " +
//                            "<Amount>" + GetResourceValue("GridHdrAmount", "0") + "</Amount> " +
//                        "</listhead> " +
//                        "<option> " +
//                            "<claimantname></claimantname> " +
//                            "<PolicyID></PolicyID> " +
//                            "<UnitID></UnitID> " +
//                            "<CoverageTypeCode></CoverageTypeCode> " +
//                            "<ReserveTypeCode></ReserveTypeCode> " +
//                            "<Amount></Amount> " +
//                            "<SplitRowId></SplitRowId> " +
//                        "</option> " +
//                    "</FundsSplits> " +
//                "</Funds>";

//                oTemplate.LoadXml(@strXML);

////                oTemplate.LoadXml(@"
////                <Funds>
////                    <FundsSplits>
////                        <listhead>
////                            <claimantname>Claimant Name</claimantname>
////                            <PolicyID>Policy</PolicyID>
////                            <UnitID>Unit</UnitID>
////                            <CoverageTypeCode>Coverage Type</CoverageTypeCode>
////                            <ReserveTypeCode>Reserve Type</ReserveTypeCode>
////                            <Amount>Amount</Amount>
////                        </listhead>
////                        <option>
////                            <claimantname></claimantname> 
////                            <PolicyID></PolicyID>
////                            <UnitID></UnitID>
////                            <CoverageTypeCode></CoverageTypeCode>
////                            <ReserveTypeCode></ReserveTypeCode>
////                            <Amount></Amount>
////                            <SplitRowId></SplitRowId> 
////                        </option>
////                    </FundsSplits>
////                </Funds>
                ////                ");
                #endregion
                sbXML.Append("<Funds> ");
                sbXML.Append("<FundsSplits>");
                sbXML.Append("<listhead> ");
                sbXML.Append("<TransTypeCode>" + GetResourceValue("GridHdrTransactionType", "0") + "</TransTypeCode> ");
                sbXML.Append("<ReserveTypeCode>" + GetResourceValue("GridHdrReserveType", "0") + "</ReserveTypeCode> ");
                sbXML.Append("<Amount>" + GetResourceValue("GridHdrAmount", "0") + "</Amount> ");
                sbXML.Append("</listhead> ");
                sbXML.Append("<option> ");
                sbXML.Append("<TransTypeCode></TransTypeCode> ");
                sbXML.Append("<SplitRowId></SplitRowId> ");
                sbXML.Append("<ReserveTypeCode></ReserveTypeCode> ");
                sbXML.Append("<Amount></Amount> ");
                sbXML.Append("</option> ");
                sbXML.Append("</FundsSplits> ");
                sbXML.Append("</Funds>");

                oTemplate.LoadXml(@sbXML.ToString());
                
            }
            else
            {
                sbXML.Append("<Funds> " );
                sbXML.Append("<FundsSplits> " );
                sbXML.Append("<listhead> " );
                sbXML.Append("<claimantname>" + GetResourceValue("GridHdrClaimantName", "0") + "</claimantname> " );
                sbXML.Append("<PolicyID>" + GetResourceValue("GridHdrPolicy", "0") + "</PolicyID> " );
                sbXML.Append("<UnitID>" + GetResourceValue("GridHdrUnit", "0") + "</UnitID> " );
                sbXML.Append("<CoverageTypeCode>" + GetResourceValue("GridHdrCoverageType", "0") + "</CoverageTypeCode> " );
                //rupal:start, mits 33600
                sbXML.Append("<LossTypeCode>" + GetResourceValue("GridHdrLossType", "0") + "</LossTypeCode> ");
                sbXML.Append("<DisabilityCatCode>" + GetResourceValue("GridHdrDisabilityCatCode", "0") + "</DisabilityCatCode> ");
                sbXML.Append("<DisabilityTypeCode>" + GetResourceValue("GridHdrDisabilityTypeCode", "0") + "</DisabilityTypeCode> ");
                //rupal:end
                sbXML.Append("<ReserveTypeCode>" + GetResourceValue("GridHdrReserveType", "0") + "</ReserveTypeCode> ");
                sbXML.Append("<Amount>" + GetResourceValue("GridHdrAmount", "0") + "</Amount> " );
                sbXML.Append("</listhead> " );
                sbXML.Append("<option> " );
                sbXML.Append("<claimantname></claimantname> " );
                sbXML.Append("<PolicyID></PolicyID> " );
                sbXML.Append("<UnitID></UnitID> " );
                sbXML.Append("<CoverageTypeCode></CoverageTypeCode> " );
                //rupal:start, mits 33600               
                sbXML.Append("<LossTypeCode></LossTypeCode> ");
                sbXML.Append("<DisabilityCatCode></DisabilityCatCode> ");
                sbXML.Append("<DisabilityTypeCode></DisabilityTypeCode> ");                
                //rupal:end
                sbXML.Append("<ReserveTypeCode></ReserveTypeCode> " );
                sbXML.Append("<Amount></Amount> " );
                sbXML.Append("<SplitRowId></SplitRowId> " );
                sbXML.Append("</option> " );
                sbXML.Append("</FundsSplits> " );
                sbXML.Append("</Funds>");
                oTemplate.LoadXml(@sbXML.ToString());               
            }
            sbXML = null;
            return oTemplate;
        }       

        private XmlDocument GetDummyXmlForDestGrid()
        {
            //string strXML = string.Empty;
            //string strXML = string.Empty;
            //rupal:mits 33600
            //changed the code for performance optimization, replaced string concatanation with string builder
            StringBuilder sbXML = new StringBuilder();
            XmlDocument oTemplate = new XmlDocument();

            sbXML.Append("<Funds> ");
            sbXML.Append("<DestFinKeysGrid> ");
            sbXML.Append("<listhead> ");
            sbXML.Append("<claimantname>" + GetResourceValue("GridHdrClaimantName", "0") + "</claimantname> ");
            sbXML.Append("<PolicyID>" + GetResourceValue("GridHdrPolicy", "0") + "</PolicyID> ");
            sbXML.Append("<UnitID>" + GetResourceValue("GridHdrUnit", "0") + "</UnitID> ");
            sbXML.Append("<CoverageTypeCode>" + GetResourceValue("GridHdrCoverageType", "0") + "</CoverageTypeCode> ");
            //rupal:start.mits 33600
			sbXML.Append("<LossTypeCode>" + GetResourceValue("GridHdrLossType", "0") + "</LossTypeCode> ");
            sbXML.Append("<DisabilityCatCode>" + GetResourceValue("GridHdrDisabilityCatCode", "0") + "</DisabilityCatCode> ");
            sbXML.Append("<DisabilityTypeCode>" + GetResourceValue("GridHdrDisabilityTypeCode", "0") + "</DisabilityTypeCode> ");         
            //rupal:end          
            sbXML.Append("<ReserveTypeCode>" + GetResourceValue("GridHdrReserveType", "0") + "</ReserveTypeCode> ");            
            sbXML.Append("</listhead> ");
            sbXML.Append("<option> ");
            sbXML.Append("<claimantname></claimantname> ");
            sbXML.Append("<PolicyID></PolicyID> ");
            sbXML.Append("<UnitID></UnitID> ");
            sbXML.Append("<CoverageTypeCode></CoverageTypeCode> ");
            //rupal:start, mits 33600           
            sbXML.Append("<LossTypeCode></LossTypeCode> ");
            sbXML.Append("<DisabilityCatCode></DisabilityCatCode> ");
            sbXML.Append("<DisabilityTypeCode></DisabilityTypeCode> ");           
            //rupal:end 
            sbXML.Append("<ReserveTypeCode></ReserveTypeCode> ");
            sbXML.Append("<rcrowid></rcrowid> ");
            sbXML.Append("<SplitRowId></SplitRowId> ");
            sbXML.Append("</option> ");
            sbXML.Append("</DestFinKeysGrid> ");
            sbXML.Append("</Funds>");
            oTemplate.LoadXml(@sbXML.ToString());
            sbXML = null;
            #region commented

            //strXML = "<Funds> " +
            //           "<DestFinKeysGrid> " +
            //            "<listhead> " +
            //                "<claimantname>" + GetResourceValue("GridHdrClaimantName", "0") + "</claimantname> " +
            //                "<PolicyID>" + GetResourceValue("GridHdrPolicy", "0") + "</PolicyID> " +
            //                "<UnitID>" + GetResourceValue("GridHdrUnit", "0") + "</UnitID> " +
            //                "<CoverageTypeCode>" + GetResourceValue("GridHdrCoverageType", "0") + "</CoverageTypeCode> " +
            //                "<ReserveTypeCode>" + GetResourceValue("GridHdrReserveType", "0") + "</ReserveTypeCode> " +
            //            "</listhead> " +
            //            "<option> " +
            //                "<claimantname></claimantname> " +
            //                "<PolicyID></PolicyID> " +
            //                "<UnitID></UnitID> " +
            //                "<CoverageTypeCode></CoverageTypeCode> " +
            //                "<ReserveTypeCode></ReserveTypeCode> " +
            //                "<rcrowid></rcrowid> " +
            //            "</option> " +
            //          "</DestFinKeysGrid> " +
            //         "</Funds>";
            
            //oTemplate.LoadXml(@strXML);
            
//            oTemplate.LoadXml(@"
//                <Funds>
//                    <DestFinKeysGrid>
//                        <listhead>
//                            <claimantname>Claimant Name</claimantname>
//                            <PolicyID>Policy</PolicyID>
//                            <UnitID>Unit</UnitID>
//                            <CoverageTypeCode>Coverage Type</CoverageTypeCode>
//                            <ReserveTypeCode>Reserve Type</ReserveTypeCode>
//                        </listhead>
//                        <option>
//                            <claimantname></claimantname>
//                            <PolicyID></PolicyID>
//                            <UnitID></UnitID>
//                            <CoverageTypeCode></CoverageTypeCode>
//                            <ReserveTypeCode></ReserveTypeCode>
//                            <rcrowid></rcrowid>
//                        </option>
//                    </DestFinKeysGrid>
//                </Funds>
            //            ");
            #endregion
            return oTemplate;
        }

        private XmlDocument GetDataForSelectedFund()
        {
            //XmlDocument objXml = new XmlDocument();
            //XmlElement xmlIn = null;

            //Call WCF wrapper for cws
            //CommonWCFService.CommonWCFServiceClient oWCF = new CommonWCFService.CommonWCFServiceClient();
            //string sReturn = oWCF.ProcessRequest(oMessageElement.ToString());

            //string sReturn = AppHelper.CallWCFService(oMessageElement.ToString());
            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);

            return oFDMPageDom;
            //XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");

            //objXml.LoadXml(oInstanceNode.InnerXml);
            //return objXml;
        }

        private XmlDocument ProcessControlRequest()
        {
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;

            //Call WCF wrapper for cws
            //CommonWCFService.CommonWCFServiceClient oWCF = new CommonWCFService.CommonWCFServiceClient();
            //string sReturn = oWCF.ProcessRequest(oMessageElement.ToString());

            //string sReturn = AppHelper.CallWCFService(oMessageElement.ToString());
            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            //XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");

            //objXml.LoadXml(oInstanceNode.InnerXml);
            return oFDMPageDom;
        }

        private void MakeWebServiceCall()
        {
            XmlTemplate = GetMessageTemplate(eMessageTemplate.CheckUtility);
            //bReturnStatus = CallCWSFunction("SplitForm.GetUtilityUseResFilterValue", out sreturnValue, XmlTemplate);

            //bReturnStatus = CallCWS("FundManagementAdaptor.CheckUtility", XmlTemplate, out sreturnValue, false, false);
            string sReturn = AppHelper.CallCWSService(XmlTemplate.ToString());
            XmlTemplate = XElement.Parse(sReturn.ToString());

            useresfilter.Text = XmlTemplate.XPathSelectElement("//UseResFilter").Value;
            usecarrierclaim.Text = XmlTemplate.XPathSelectElement("//UseCarrierClaims").Value;

        }

        private void EnableDisbleReserveTypeFt()
        {
            //XmlDocument objXmlDoc = new XmlDocument();

            //objXmlDoc.LoadXml(sXml);
            //XmlNode UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/UseResFilter");
            
            Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
            Control objDivReserveTypeCode = this.FindControl("div_ReserveTypeCode");
            Control objReserveTypeCodeFt = this.FindControl("ReserveTypeCodeFt");
            Control objDivReserveTypeCodeFt = this.FindControl("div_ReserveTypeCodeFt");
            
            if (useresfilter != null)
            {
                if (useresfilter.Text == "True")
                {
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = true;
                    }
                    if (objDivReserveTypeCode != null)
                    {
                        div_ReserveTypeCode.Attributes.Add("style", "visibility:hidden;display:none");
                    }

                    if (Request.QueryString["mode"] == "edit")
                    {
                        TransTypeCode.Enabled = true;
                    }
                    else
                    {
                        TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
                        if (objTxt != null)
                        {
                            objTxt.Enabled = false;
                            objTxt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                        }
                        Button objBtn = (Button)this.FindControl("TransTypeCode$codelookupbtn");
                        if (objBtn != null)
                        {
                            objBtn.Enabled = false;
                        }
                    }
                }
                else
                {
                    if (objDivReserveTypeCode != null)
                    {
                        objDivReserveTypeCode.Visible = true;
                    }
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = false;
                    }
                    if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                    {
                        ((CodeLookUp)objReserveTypeCode).Enabled = false;
                    }
                }
            }
            if (usecarrierclaim != null)
            {
                if (usecarrierclaim.Text == "True")
                {
                    if (objDivReserveTypeCode != null)
                    {
                        objDivReserveTypeCode.Visible = false;
                    }
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = false;
                    }
                    Control objDivTransTypeCode = this.FindControl("div_TransTypeCode");
                    if (objDivTransTypeCode != null)
                    {
                        objDivTransTypeCode.Visible = false;
                    }
                }
                else
                {
                    Control objDivDestFinGrid = this.FindControl("div_DestFinKeysGrid");
                    if (objDivDestFinGrid != null)
                    {
                        objDivDestFinGrid.Visible = false;
                    }
                    Control objDivTransTypeCode = this.FindControl("div_TransTypeCodeFt");
                    if (objDivTransTypeCode != null)
                    {
                        objDivTransTypeCode.Visible = false;
                    }
                }
            }
            
        }

        //private void DisableGrids()
        //{

        //    Control objDivDestFinGrid = this.FindControl("div_DestFinKeysGrid");
        //    if (objDivDestFinGrid != null)
        //    {
        //        objDivDestFinGrid.Visible = false;
        //    }
        //    Control objDivTransSplitGrid = this.FindControl("div_TransSplitGrid");
        //    if (objDivTransSplitGrid != null)
        //    {
        //        objDivTransSplitGrid.Visible = false;
        //    }
        //}


        private void ClearFormFields()
        {
            //ddhiman: 2011/01/20: MITS 27007
            TextBox objTxtDesResTyp = (TextBox)this.FindControl("ReserveTypeCodeFt$codelookup");
            //End ddhiman
            TextBox objTxtReserveTypeFt = (TextBox)this.FindControl("ReserveTypeCodeFt$codelookup_cid");
            TextBox objTxtReserveType = (TextBox)this.FindControl("ReserveTypeCode$codelookup_cid");
            TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
            TextBox objTxtTransactionType = (TextBox)this.FindControl("TransTypeCode$codelookup_cid");
            TextBox objTxtReason = (TextBox)this.FindControl("ChangeTypeCode$codelookup");
            TextBox objTxtReasonCode = (TextBox)this.FindControl("ChangeTypeCode$codelookup_cid");
            TextBox objTxtFt = (TextBox)this.FindControl("TransTypeCodeFt$codelookup");
            TextBox objTxtTransactionTypeFt = (TextBox)this.FindControl("TransTypeCodeFt$codelookup_cid");

            //ddhiman: 2011/01/20: MITS 27007
            if (objTxtDesResTyp != null)
            {
                objTxtDesResTyp.Text = string.Empty;
            }
            //End ddhiman

            if (objTxt != null)
            {
                objTxt.Text = string.Empty;
                objTxtTransactionType.Text = string.Empty;
            }
            if (objTxtReserveType != null)
            {
                objTxtReserveType.Text = string.Empty;
            }
            if (objTxtReserveTypeFt != null)
            {
                objTxtReserveTypeFt.Text = string.Empty;
            }
            if (objTxtReason != null)
            {
                objTxtReason.Text = string.Empty;
                objTxtReasonCode.Text = string.Empty;
            }
            if (objTxtFt != null)
            {
                objTxtFt.Text = string.Empty;
                objTxtTransactionTypeFt.Text = string.Empty;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMessageElement"></param>
        internal void BindData2ErrorControl(XmlDocument objXmlDoc)
        {
            Label lblError;
            
            lblError = (Label)this.FindControl("lblError");

            //MJP - if the label is inside the user control (ascx), it would result in a null exception error
            //      the code below fixes that and accesses the label inside the user control
            if (lblError == null)
            {
                UserControl objUserControl = (UserControl)this.FindControl("ErrorControl1");

                if (objUserControl != null)
                {
                    lblError = (Label)objUserControl.FindControl("lblError");
                } // if
            }
            if (objXmlDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd") != null)
            {
                if (lblError != null)
                {
                    lblError.Text = ErrorHelper.FormatServiceErrors(objXmlDoc.InnerXml);
                }
            }
            else
            {
                lblError.Text = string.Empty;
            }
        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("ControlRequest.aspx"), strResourceType).ToString();
        }
    }
}