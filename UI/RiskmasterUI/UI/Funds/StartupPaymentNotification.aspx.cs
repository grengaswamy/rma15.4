﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;

namespace Riskmaster.UI.UI.Funds
{
    public partial class StartupPaymentNotification : System.Web.UI.Page
    {
        public XmlDocument Model = null;
        string MergeMessageTemplate = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.NotifyUserOfFuturePayments</Function></Call><Document><PaymentNotification/></Document></Message>";
        string MessageTemplatePrint = "<Message><Authorization>af42dc54-6527-49ba-8e42-f1628512a043</Authorization><Call><Function>FundManagementAdaptor.CreateStartupPaymentNotificationReport</Function></Call><Document><PaymentNotification/></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                divForms.Attributes.Add("style", "overflow:scroll;height:78%;width:100%;border-width:'0'");
                divForms.Attributes.Add("class", "divScroll");
                Model = new XmlDocument();
                string sCWSresponse = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                Model.LoadXml(sCWSresponse);
                BindpageControls(sCWSresponse);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="diaryDoc"></param>
        /// <returns></returns>
        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                DataTable dt = new DataTable();
                dt.Columns.Add("CheckDate");
                dt.Columns.Add("ControlNumber");
                dt.Columns.Add("Payee");
                dt.Columns.Add("ClaimNumber");
                dt.Columns.Add("Amount");
                //dSet.ReadXml(new XmlNodeReader(diaryDoc.SelectSingleNode("//PaymentNotification/Row")));
                foreach(XmlNode node in diaryDoc.SelectNodes("//PaymentNotification/Row"))
                {
                    DataRow dr=dt.NewRow();
                    dr["CheckDate"] = node.SelectSingleNode("CheckDate").InnerText;
                    dr["ControlNumber"] = node.SelectSingleNode("ControlNumber").InnerText;
                    dr["Payee"] = node.SelectSingleNode("Payee").InnerText;
                    dr["ClaimNumber"] = node.SelectSingleNode("ClaimNumber").InnerText;
                    dr["Amount"] = node.SelectSingleNode("Amount").InnerText;
                    dt.Rows.Add(dr);
                }
                dSet.Tables.Add(dt);
                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        private void BindpageControls(string sreturnValue)
        {
            try
            {
                DataSet usersRecordsSet = null;
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                lblTotal.Text = AppHelper.GetInnerXmlFromDOM(usersXDoc, "TotalAmount");
                lblChecks.Text = AppHelper.GetInnerXmlFromDOM(usersXDoc, "TotalNumChecks");
                usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
                if (usersRecordsSet.Tables[0] != null)
                {
                    if (usersRecordsSet.Tables[0].Rows.Count == 0)
                    {
                        lblChecks.Text = "0";
                        lblTotal.Visible = false;
                        btnPrint.Visible = false;
                        lblfinlabel.Visible = false;
                    }
                }
                else
                {
                    lblChecks.Text = "0";
                    lblfinlabel.Visible = false;
                    btnPrint.Visible = false;
                }
                gvSelectChecks.DataSource = usersRecordsSet.Tables[0].DefaultView;
                gvSelectChecks.DataBind();
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string sCWSresponse = AppHelper.CallCWSService(MessageTemplatePrint.ToString());
            Model = new XmlDocument();
            Model.LoadXml(sCWSresponse);
            byte[] pdfbytes = Convert.FromBase64String(AppHelper.GetInnerXmlFromDOM(Model,"File"));
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=checks.pdf"));
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }
     
    }
}
