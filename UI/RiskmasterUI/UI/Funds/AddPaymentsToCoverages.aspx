﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPaymentsToCoverages.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.AddPaymentsToCoverages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>

     <script src="../../Scripts/form.js" type="text/javascript"></script>

    <!-- Rakhel ML Changes - Start !-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
     <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">         { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
     <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">         { var i; } </script>				
	 <!-- Rakhel ML Changes - End!-->		
   <uc4:CommonTasks ID="CommonTasks1" runat="server" />
   <script type="text/javascript">
       var ns, ie, ieversion;
       var browserName = navigator.appName;                   // detect browser 
       var browserVersion = navigator.appVersion;
       if (browserName == "Netscape") {
           ie = 0;
           ns = 1;
       }
       else		//Assume IE
       {
           ieversion = browserVersion.substr(browserVersion.search("MSIE ") + 5, 1);
           ie = 1;
           ns = 0;
       }
       function onPageLoaded() {
           if (ie) {
               if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                   eval("document.all.divForms").style.height = 400
               }
           }
           else {
               var o_divforms;
               o_divforms = document.getElementById("divForms");
               if (o_divforms != null) {
                   o_divforms.style.height = window.frames.innerHeight * 0.70;
                   o_divforms.style.width = window.frames.innerWidth * 0.995;
               }
           }
       }
       function dateSelected(sDay, sMonth, sYear) {
           var objCtrl = null;
           if (m_codeWindow != null)
               m_codeWindow.close();
           objCtrl = eval('document.forms[0].' + m_sFieldName);
           if (objCtrl != null) {
               sDay = new String(sDay);
               if (sDay.length == 1)
                   sDay = "0" + sDay;
               sMonth = new String(sMonth);
               if (sMonth.length == 1)
                   sMonth = "0" + sMonth;
               sYear = new String(sYear);
               objCtrl.value = formatRMDate(sYear + sMonth + sDay);
               objCtrl.focus();
           }
           m_sFieldName = "";
           m_codeWindow = null;
           setDataChanged(true);
           document.forms[0].submit();
       }
       function submitFromDate() {
           if (document.forms[0].txtFromDate.value != '') {
               //__doPostback("btnRefresh","daterefresh" );
               document.getElementById("hdnAll").value = "";
               //Start: Neha Suresh Jain, MITS 20806, 05/18/2010
               dateLostFocus(document.forms[0].txtFromDate.id);
               //End: Neha Suresh Jain              
               document.getElementById('btnRefresh').click();

           }
       }
       function submitToDate() {
           if (document.forms[0].txtToDate.value != '') {
               document.getElementById("hdnAll").value = "";
               //Start: Neha Suresh Jain, MITS 20806, 05/18/2010
               dateLostFocus(document.forms[0].txtToDate.id);
               //End: Neha Suresh Jain 
               document.getElementById('btnRefresh').click();
           }
       }
       function EnableDisable() {
           
           //Rakhel ML Changes
            var datebtn1 = $("#txtFromDate");
            var datebtn2 = $("#txtToDate");
            //  if (document.forms[0].chkFromDate.checked) {
            //      alert(document.forms[0].chkFromDate.checked + "1");
            //      document.forms[0].txtFromDate.disabled = false;
            //      //document.forms[0].datebtn1.disabled = false; //Rakhel - ML Changes
            //      $.datepicker._disabledInputs = [datebtn2[0]];
            //  }
            //  else {
            //      //document.forms[0].txtFromDate.value="";
            //      alert(document.forms[0].chkFromDate.checked + "2");
            //      document.forms[0].txtFromDate.disabled = true;
            //      //document.forms[0].datebtn1.disabled = true;  //Rakhel - ML Changes
            //      $.datepicker._disabledInputs = [datebtn1[0]];
            //  }
            //  if (document.forms[0].chkToDate.checked) {
            //      alert(document.forms[0].chkFromDate.checked + "3");
            //      document.forms[0].txtToDate.disabled = false;
            //      //document.forms[0].datebtn2.disabled = false;  //Rakhel - ML Changes 
            //      $.datepicker._disabledInputs = [datebtn1[0]]; 
            //  }
            //  else {
            //      //document.forms[0].txtToDate.value="";
            //      alert(document.forms[0].chkFromDate.checked + "4");
            //      document.forms[0].txtToDate.disabled = true;
            //      //document.forms[0].datebtn2.disabled = true;  //Rakhel - ML Changes 
            //      $.datepicker._disabledInputs = [datebtn2[0]];
            //  }

           if ((document.forms[0].chkFromDate.checked) && (document.forms[0].chkToDate.checked)) {
               document.getElementById('txtFromDate').disabled = false;
               document.getElementById('txtToDate').disabled = false;
               $.datepicker._disabledInputs = [];
           }
           if ((document.forms[0].chkFromDate.checked) && !(document.forms[0].chkToDate.checked)) {
               document.getElementById('txtFromDate').disabled =   false;
                document.getElementById('txtToDate').disabled  = true;
               $.datepicker._disabledInputs = [datebtn2[0]];
           }
           if (!(document.forms[0].chkFromDate.checked) && (document.forms[0].chkToDate.checked)) {
               document.getElementById('txtFromDate').disabled = true;
                document.getElementById('txtToDate').disabled  = false;
               $.datepicker._disabledInputs = [datebtn1[0]];
           }
           if (!(document.forms[0].chkFromDate.checked) && !(document.forms[0].chkToDate.checked)) {
               document.getElementById('txtFromDate').disabled = true;
                document.getElementById('txtToDate').disabled  = true;
               $.datepicker._disabledInputs = [datebtn1[0], datebtn2[0]];
           }
           //Rakhel ML Changes
       }
       function ProcessClaims() {
           var selected = "";
           for (var i = 0; i < document.forms[0].elements.length; i++) {
               if (document.forms[0].elements[i].id == "chkFromDate" || document.forms[0].elements[i].id == "chkToDate")
                   continue;
               if (document.forms[0].elements[i].type == 'checkbox') {
                   if (document.forms[0].elements[i].checked == true) {

                       if (selected == "") {
                           selected = document.forms[0].elements[i].value;
                       }
                       else {
                           selected = selected + " " + document.forms[0].elements[i].value;
                       }
                   }
               }
           }
           document.forms[0].hdnSelected.value = selected;
           if (selected != "") {
               m_codeWindow = window.open('ProcessClaims.aspx', 'codeWnd',
						'width=700,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 700) / 2 + ',resizable=yes,scrollbars=yes');
               return false;
           }

           if (document.forms[0].hdnAll.value == "Y") {
               document.getElementById("hdnAll").value = "";
               m_codeWindow = window.open('ProcessClaims.aspx', 'codeWnd',
						'width=700,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 700) / 2 + ',resizable=yes,scrollbars=yes');
               return false;
           }
           if (selected == "") {
           //Changed for MITS 22238 : Start
               //alert("There are no policies selected, cannot process!");
               alert(AddPaymentsToCoveragesValidations.ValidNoPoliciesSelection);
           //Changed for MITS 22238 : End
               return false;
           }
           return false;
       }
//       function dateLostFocus(sCtrlName) {
//           var sDateSeparator;
//           var iDayPos = 0, iMonthPos = 0;
//           var d = new Date(1999, 11, 22);
//           var s = d.toLocaleString();
//           var sRet = "";
//           var objFormElem = eval('document.forms[0].' + sCtrlName);

//           var sDate = new String(objFormElem.value);
//           //Start: Neha Suresh Jain, MITS 20806, 05/18/2010
//           if (sDate.indexOf('/') == -1) {
//               sDate = sDate.substr(0, 2) + '/' + sDate.substr(2, 2) + '/' + sDate.substr(4, 4);
//           }
//           //End: Neha Suresh Jain
//           var iMonth = 0, iDay = 0, iYear = 0;
//           var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
//           if (sDate == "")
//               return "";
//           iDayPos = s.indexOf("22");
//           iMonthPos = s.indexOf("11");
//           //if(IE4)
//           //	sDateSeparator=s.charAt(iDayPos+2);
//           //else
//           sDateSeparator = "/";
//           var sArr = sDate.split(sDateSeparator);
//           if (sArr.length == 3) {
//               sArr[0] = new String(parseInt(sArr[0], 10));
//               sArr[1] = new String(parseInt(sArr[1], 10));
//               sArr[2] = new String(parseInt(sArr[2], 10));
//               // Classic leap year calculation
//               if (((parseInt(sArr[2], 10) % 4 == 0) && (parseInt(sArr[2], 10) % 100 != 0)) || (parseInt(sArr[2], 10) % 400 == 0))
//                   monthDays[1] = 29;
//               if (iDayPos < iMonthPos) {
//                   // Date should be as dd/mm/yyyy
//                   if (parseInt(sArr[1], 10) < 1 || parseInt(sArr[1], 10) > 12 || parseInt(sArr[0], 10) < 0 || parseInt(sArr[0], 10) > monthDays[parseInt(sArr[1], 10) - 1])
//                       objFormElem.value = "";
//               }
//               else {
//                   // Date is something like mm/dd/yyyy
//                   if (parseInt(sArr[0], 10) < 1 || parseInt(sArr[0], 10) > 12 || parseInt(sArr[1], 10) < 0 || parseInt(sArr[1], 10) > monthDays[parseInt(sArr[0], 10) - 1])
//                       objFormElem.value = "";
//               }
//               // Check the year
//               if (parseInt(sArr[2], 10) < 10 || (sArr[2].length != 4 && sArr[2].length != 2))
//                   objFormElem.value = "";
//               // If date has been accepted
//               if (objFormElem.value != "") {
//                   // Format the date
//                   if (sArr[0].length == 1)
//                       sArr[0] = "0" + sArr[0];
//                   if (sArr[1].length == 1)
//                       sArr[1] = "0" + sArr[1];
//                   if (sArr[2].length == 2)
//                       sArr[2] = "19" + sArr[2];
//                   if (iDayPos < iMonthPos)
//                       objFormElem.value = formatRMDate(sArr[2] + sArr[1] + sArr[0]);
//                   else
//                       objFormElem.value = formatRMDate(sArr[2] + sArr[0] + sArr[1]);
//               }
//           }
//           else
//               objFormElem.value = "";
//         
//       }
       function SelectAll() {
           for (var i = 0; i < document.forms[0].elements.length; i++) {
               if (document.forms[0].elements[i].id == "chkFromDate" || document.forms[0].elements[i].id == "chkToDate")
                   continue;
               if (document.forms[0].elements[i].type == 'checkbox') {
                   document.forms[0].elements[i].checked = true;
               }
           }
       }
       function UnSelectAll() {
           for (var i = 0; i < document.forms[0].elements.length; i++) {
               if (document.forms[0].elements[i].id == "chkFromDate" || document.forms[0].elements[i].id == "chkToDate")
                   continue;
               if (document.forms[0].elements[i].type == 'checkbox') {
                   document.forms[0].elements[i].checked = false;
               }
           }
       }

       //Rakhel ML Changes - Start
       function Disp() {
           $.datepicker._disabledInputs = [];
       }
       //Rakhel ML Changes - End
    </script>
		

    <style type="text/css">
        .style1
        {
            width: 12%;
        }
        .style2
        {
            width: 18%;
        }
    </style>
      <style>
       .divScroll
{
    BORDER-RIGHT: darkblue 1px solid;
    BORDER-TOP: darkblue 1px solid;
    OVERFLOW: auto;
    BORDER-LEFT: darkblue 1px solid;
    WIDTH: 99%;
    BORDER-BOTTOM: darkblue 1px solid;
    BACKGROUND-COLOR: #ffffff;
    height:250px !important
}
    </style>
</head>
    <body onload="parent.MDIScreenLoaded();" onunload="Disp();">
        <form id="frmData" runat="server">
            <table width="100%">    
                <tr>
                    <td class="msgheader">
                        <asp:Label ID="lblApplyPaymentHdr" runat="server" Text="<%$ Resources:lblApplyPaymentHdrResrc %>" />
                    </td>
                </tr>    
                <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <!-- Changed for MITS 22238 : Start -->
                <input type="hidden" id="hdnSelected" runat="server" />
                <input type="hidden" id="hdnAll" runat="server" />
                <tr>
                    <td width="5%"><input type="checkbox" id="chkFromDate" value="True" onclick="EnableDisable();" runat="server" /></td>
                    <td width="8%">
                        <asp:Label ID="lblFromDate" runat="server" Text="<%$ Resources:lblFromDateResrc %>" />
                    </td>
                    <td width="12%">
                        <input type="text" value="" size="12" onblur="dateLostFocus(this.id);" onchange="setDataChanged(true);submitFromDate();" id="txtFromDate" runat="server"/>
                    </td>
                    <td width="5%">
                        <!--Rakhel ML Changes -Start !-->
                             <script type="text/javascript">
                             $(function () {
                                    $("#txtFromDate").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../Images/calendar.gif",
                                        buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    });
                            });
                            </script>
                            <!--Rakhel ML Changes-End !-->
                    </td>
                    <td width="3%">&nbsp;</td>
                    <td width="5%"><input type="checkbox" id="chkToDate" value="True" onclick="EnableDisable();" runat="server" /></td>
                    <td width="8%">
                        <asp:Label ID="lblToDate" runat="server" Text="<%$ Resources:lblToDateResrc %>" />
                    </td>
                    <td width="12%">
                        <input type="text"   value="" size="12" onblur="dateLostFocus(this.id);" onchange="setDataChanged(true);submitToDate();" id="txtToDate" runat="server"/>
                    </td>
                    <td width="5%">
                    <!--Rakhel ML Changes -Start !-->
                      
                         <script type="text/javascript">
                            $(function () {
                                $("#txtToDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                    buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                });
                            });
                            </script>
                        <!--Rakhel ML Changes - End !-->
                    </td>
                    <td class="style2">
                        <asp:RadioButton ID="rbtnPolicyTracking" runat="server" GroupName="PolicyType" Text="<%$ Resources:lblPolicyTrackingResrc %>" Checked="True" OnCheckedChanged="btnRefresh_Click" AutoPostBack="true" />
                    </td>
                    <td class="style1">&nbsp;</td>
                    <td width="*">&nbsp;</td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="8%">
                        <asp:Label ID="lblPolicyName" runat="server" Text="<%$ Resources:lblPolicyNameResrc %>" />
                    </td>
                    <td width="10%" align="left">
                        <asp:TextBox ID="txtPolicyName" Width="92" runat ="server"/>
                    </td>
                    <td width="5%"></td>
                    <td width="3%">&nbsp;</td>
                    <td width="5%"></td>
                    <td width = "8%">
                        <asp:Label ID="lblPolicyNumber" runat="server" Text="<%$ Resources:lblPolicyNumberResrc %>" />
                    </td>
                    <td width="10%"><asp:TextBox ID="txtPolicyNumber" Width="92" runat="server"/></td>    
                    <td width = "5%"></td>
                    <td class="style2">
                        <asp:RadioButton ID="rbtnPolicyManagement" runat="server" OnCheckedChanged="btnRefresh_Click" GroupName="PolicyType" Text="<%$ Resources:lblPolicyManagementResrc %>" AutoPostBack="true"/>
                    </td>
                    <td class="style1">
                        <asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:btnRefreshResrc %>"  CssClass="button" onclick="btnRefresh_Click"/>
                    </td>
                    <td width="*">&nbsp;</td>
                </tr>

            </table>    
            <!--Changed for MITS 22238 : End -->
         
            <asp:Panel ID="divForms" runat="server" class="divScroll">
                <%-- Changed by gagan fo MITS 22238 : start   --%>

                <asp:GridView ID="gvSelectClaims" runat="server" AutoGenerateColumns="false" 
                AllowPaging="false" AllowSorting="true" Width="100%" ShowHeader="true"  EnableViewState="true"
                EmptyDataText="<%$ Resources:gvClaimsEmptyDataTextResrc %>">
                <PagerStyle CssClass="headertext2"  ForeColor="White"/>
                <HeaderStyle CssClass="colheader6" />
                <AlternatingRowStyle CssClass="data2" /> 
                <FooterStyle  CssClass ="colheader6" />
                    <Columns>              
                        <asp:TemplateField  HeaderStyle-HorizontalAlign ="Left" HeaderStyle-CssClass="headertext2" ControlStyle-Width="5%">
                            <ItemTemplate>
                                <input type="checkbox"  style='visibility:<%# DataBinder.Eval(Container.DataItem, "visible")%>' id='chkPolicy' name='chkPolicy' value='<%# DataBinder.Eval(Container.DataItem, "id")%>' />                                        
                            </ItemTemplate>
                        </asp:TemplateField> 

                        <asp:BoundField  HeaderText="<%$ Resources:gvFieldPolicyNameResrc %>"  DataField="policyname" 
                        ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                
                        <asp:BoundField  HeaderText="<%$ Resources:gvFieldPolicyNumberResrc %>"  DataField="policynumber" 
                        ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                
                        <asp:BoundField   HeaderText="<%$ Resources:gvFieldEffectiveDateResrc %>"  DataField="effectivedate"  ControlStyle-Width="20%"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                
                        <asp:BoundField   HeaderText="<%$ Resources:gvFieldExpirationDateResrc %>"  DataField="expirationdate"  ControlStyle-Width="20%"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                        </asp:BoundField>
                        <%-- Changed by gagan for MITS 22238 : end   --%>
               
                    </Columns>
                </asp:GridView>            
            </asp:Panel>
            <table border="0">
                <tr>
                    <td>
                        <!--Changed for MITS 22238 : Start -->
                        <b>
                            <asp:Label ID="lblProcessInfo" runat="server" Text="<%$ Resources:lblProcessInfoResrc %>" />
                        </b>
                        <!--Changed for MITS 22238 : End -->
                    </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td>
                        <input type="button" class="button" name="btnAll" runat="server" value="<%$ Resources:btnAllResrc %>" onclick="SelectAll();" />
                    </td>
                    <td>
                        <input type="button" class="button" name="btnDeselectAll" runat="server" value="<%$ Resources:btnDeselectAllResrc %>" onclick="UnSelectAll();" />
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnProcess" onclick="btnProcess_Click" UseSubmitBehavior="true" runat="server" Text="<%$ Resources:btnProcessResrc %>"  CssClass="button" OnClientClick="return ProcessClaims();"/>
                    </td> 
 
                </tr>
                <tr>
                <td>
       
                <script language="javascript" type="text/javascript">
                    EnableDisable();
                </script>           
                    <%if (Model!=null){%>
					
                        <%if (Model.SelectSingleNode("//Policies/Policy[1]")!= null){%>
                            <%if (Model.SelectSingleNode("//Policies/Policy[1]").Attributes["policynumber"].Value == "<ALL MATCHING POLICIES>")   {%>           
                                <script language="javascript" type="text/javascript">
                                    document.getElementById("hdnAll").value = "Y";
                                    //alert("The filter criteria given has generated more than 500 policy matches. Please filter the policies again");
                                    alert(AddPaymentsToCoveragesValidations.ValidReFilter);
                                    document.getElementById('btnProcess').disabled = true;
                                </script>
                            <%} %>
                            <%else %>
                            <%{ %>
                                <script language="javascript" type="text/javascript">
                                    document.getElementById('btnProcess').disabled = false;
                                </script>
                            <%} %>
                        </td>
                        <%} %>
                    <%} %>
					
                </tr>
            </table>
        </form>
    </body>
</html>