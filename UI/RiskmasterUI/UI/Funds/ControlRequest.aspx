﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ControlRequest.aspx.cs"
    Inherits="Riskmaster.UI.UI.Funds.ControlRequest" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="cl" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Control Request</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script language="JavaScript" type="text/javascript">

        function ReadOnlyScreenControls() {
            objSuccess = document.getElementById('successctrlreq');
            if (objSuccess != null) {
                if (objSuccess.value == "True") {
                    //alert("Control Request Successfully Completed");
                    alert(ControlRequestValidations.ValidRequestCompleted);
                }
            }
            objCheckNumber = document.getElementById('pay_checknumber');
            if (objCheckNumber != null) {
                objCheckNumber.readOnly = "readonly";
                objCheckNumber.style.backgroundColor = "#f2f2f2";
            }
            objFirstName = document.getElementById('pay_firstname');
            if (objFirstName != null) {
                objFirstName.readOnly = "readonly";
                objFirstName.style.backgroundColor = "#f2f2f2";
            }
            objLastName = document.getElementById('pay_lastname');
            if (objLastName != null) {
                objLastName.readOnly = "readonly";
                objLastName.style.backgroundColor = "#f2f2f2";
            }
        }

        function ProcessControlRequest() {
            document.forms[0].functiontocall.value = "ProcessControlRequest";
        }

        function GetDataForSelectedFund() {
            document.forms[0].functiontocall.value = "GetDataForSelectedFund";
        }

        function checkListSelection() {
            Evaluate();
            return Validate();
        }

        function Evaluate() {
            inputs = document.getElementsByTagName("input");
            for (i = 0; i < inputs.length; i++) {
                if ((inputs[i].type == "radio") && (inputs[i].checked == true) && (inputs[i].name == "TransSplitGrid")) {
                    document.forms[0].transsplitid.value = inputs[i].value;
                    break;
                }
                else {
                    document.forms[0].transsplitid.value = "";
                }
            }
            for (i = 0; i < inputs.length; i++) {
                if ((inputs[i].type == "radio") && (inputs[i].checked == true) && (inputs[i].name == "DestFinKeysGrid")) {
                    document.forms[0].rcrowid.value = inputs[i].value;
                    break;
                }
                else {
                    document.forms[0].rcrowid.value = "";
                }
            }
        }

        function Validate() {            
            if (document.forms[0].transsplitid.value == 0 || document.forms[0].transsplitid.value == "") {
                //alert("Please select Source Financial Key.");
                alert(ControlRequestValidations.ValidSourceKeySelection);
                return false;
            }
            if (document.forms[0].usecarrierclaim.value == "True") {
                if (document.forms[0].rcrowid.value == 0 || document.forms[0].rcrowid.value == "") {
                    //alert("Please select Destination Financial Key.");
                    alert(ControlRequestValidations.ValidDestinationKeySelection);
                    return false;
                }
                //Aman MITS 31815
                
                if (document.forms[0].TransTypeCodeFt_codelookup != null) {
                    if ((document.forms[0].TransTypeCodeFt_codelookup_cid.value == "") || (document.forms[0].TransTypeCodeFt_codelookup_cid.value == "0")) {                            
                            //alert("Please select Transaction Type.");
                            alert(ControlRequestValidations.ValidTransactionTypeSelection);
                            return false;                            
                    }
                }                    
                
                //Aman MITS 31815

            }
            else {
                if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
                    if ((document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "") || (document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "0")) {
                        if (document.forms[0].ReserveTypeCode_codelookup != null) {
                            if ((document.forms[0].ReserveTypeCode_codelookup_cid.value == "") || (document.forms[0].ReserveTypeCode_codelookup_cid.value == "0")) {
                                if (parseInt(document.forms[0].ReserveTypeCode_codelookup_cid.value, 10) <= 0) {
                                    //alert("Please select Reserve Type.");
                                    alert(ControlRequestValidations.ValidReserveTypeSelection);
                                    return false;
                                }
                            }
                        }
                    }
                }
                if ((document.forms[0].TransTypeCode_codelookup_cid.value == "") || (document.forms[0].TransTypeCode_codelookup_cid.value == "0")) {
                    //alert("Please select Transaction Type.");
                    alert(ControlRequestValidations.ValidTransactionTypeSelection);
                    return false;
                }

                if (parseInt(document.forms[0].TransTypeCode_codelookup_cid.value, 10) <= 0 && document.forms[0].TransTypeCode_codelookup_cid.value == "") {
                    //alert("Please select Transaction Type.");
                    alert(ControlRequestValidations.ValidTransactionTypeSelection);
                    return false;
                }
            }
            return true;
        }
        //JIRA-857 Starts
        function validatecollectionmove() {
            var inputs = document.getElementsByTagName("input");
            if (document.getElementById('hdIsCollectionSource').value == 0) {
                for (i = 0; i < inputs.length; i++) {
                    if ((inputs[i].type == "radio") && (inputs[i].checked == true) && (inputs[i].name == "DestFinKeysGrid")) {
                        var dest = inputs[i + 1].value;
                        var result = dest.substr(dest.indexOf("preventcollonres") + 21, 1);
                        if (result == 1) {
                            alert(ControlRequestValidations.CollectionNotAllowed);
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        //JIRA-857 Ends
    </script>
    
</head>
<body onload="parent.MDIScreenLoaded();ReadOnlyScreenControls();">
    <form id="frmData" runat="server">
    <br />
    <div>
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:lbl1Resrc %>" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
    </div>
    <br />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                    <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                        <%--JIRA-857 Starts <asp:ImageButton runat="server" src="../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="btnControlRequest" AlternateText="<%$ Resources:AlternateTextSave %>" ValidationGroup="vgSave"
                            onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'"
                            OnClientClick="ProcessControlRequest();return checkListSelection();" />--%>
                        <asp:ImageButton runat="server" src="../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="btnControlRequest" AlternateText="<%$ Resources:AlternateTextSave %>" ValidationGroup="vgSave"
                            onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'"
                            OnClientClick="if(validatecollectionmove()){ProcessControlRequest();return checkListSelection();}else{return false;}" />
                        <%--JIRA-857 Ends--%>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="NotSelected" nowrap="nowrap" name="TABScorrectpayment" id="TABScorrectpayment">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="correctpayment" id="LINKTABScorrectpayment">
                    <asp:Label ID="lblCorrectPayment" style="text-decoration: none" runat="server" Text="<%$ Resources:lblCorrectPaymentResrc %>" />
                </a>
            </td>
            <td nowrap="nowrap" style="border-bottom: none; border-left: none; border-right: none;
                border-top: none;">
                &nbsp;&nbsp;
            </td>
            <td valign="top" nowrap="nowrap">
            </td>
        </tr>
    </table>
    <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 98%;
        height: 600px; overflow: auto">
        <table border="0" cellspacing="0" cellpadding="0" width="95%">
            <tr>
                <td>
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" name="FORMTABcorrectpayment"
                        id="FORMTABcorrectpayment">
                        <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
                        <tr>
                            <td width="95%">
                                <table cellspacing="10" width="100%">
                                    <tr>
                                        <td>
                                            <div id="div_ctlnumber" class="half">
                                                <asp:Label ID="lblControlNumber" CssClass="label" runat="server" Text="<%$ Resources:lblControlNumberResrc %>" />
                                                <span class="formw">
                                                    <asp:TextBox MaxLength="30" ID="pay_ctlnumber" runat="server" RMXType="search"></asp:TextBox>
                                                    <asp:Button ID="searchlookupbtn" class="EllipsisControl" runat="server" RMXType="search"
                                                        OnClientClick="GetDataForSelectedFund();return lookupData('pay_ctlumber', 'payment', '-1', 'pay_', '13');" />
                                                </span>
                                            </div>
                                            <div id="div_checknumber" class="half">
                                                <asp:Label ID="lblCheckNumber" CssClass="label" runat="server" Text="<%$ Resources:lblCheckNumberResrc %>" />
                                                <span class="formw">
                                                    <asp:TextBox MaxLength="30" ID="pay_checknumber" runat="server"></asp:TextBox>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div_firstname" class="half">
                                                <asp:Label ID="lblPayeeFirstName" CssClass="label" runat="server" Text="<%$ Resources:lblPayeeFirstNameResrc %>" />
                                                <span class="formw">
                                                    <asp:TextBox MaxLength="30" ID="pay_firstname" runat="server"></asp:TextBox>
                                                </span>
                                            </div>
                                            <div id="div_lastname" class="half">
                                                <asp:Label ID="lblPayeeLastName" CssClass="label" runat="server" Text="<%$ Resources:lblPayeeLastNameResrc %>" />
                                                <span class="formw">
                                                    <asp:TextBox MaxLength="30" ID="pay_lastname" runat="server"></asp:TextBox>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="800px" colspan="4">
                                            <div id="div_TransSplitGrid" style="overflow: auto; width: 100%; position: relative; height: 200px" runat="server">
                                                <span class="formw">
                                                    <dg:UserControlDataGrid runat="server" ID="TransSplitGrid" GridName="TransSplitGrid"
                                                        GridTitle="<%$ Resources:GridTitle1 %>" Target="Funds/FundsSplits" Ref="" Unique_Id="SplitRowId"
                                                        ShowRadioButton="true" ShowCheckBox="False" Width="680px" Height="150px" ShowHeader="True"
                                                        LinkColumn="" PopupWidth="470" PopupHeight="170" Type="Grid" HideButtons="" />
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="800px" colspan="4">
                                            <div id="div_DestFinKeysGrid" style="overflow: auto; width: 100%; position: relative;
                                                height: 200px" runat="server">
                                                <span class="formw">
                                                    <dg:UserControlDataGrid runat="server" ID="DestFinKeysGrid" GridName="DestFinKeysGrid"
                                                        GridTitle="<%$ Resources:GridTitle2 %>" Target="Funds/DestFinKeysGrid" Ref="" Unique_Id="rcrowid"
                                                        ShowRadioButton="true" ShowCheckBox="False" Width="680px" Height="150px" ShowHeader="True"
                                                        LinkColumn="" PopupWidth="470" PopupHeight="170" Type="Grid" HideButtons="" />
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div_ReserveTypeCodeFt" class="half" runat="server">
                                                <asp:Label ID="lblDestinationReserveType" CssClass="required" runat="server" Text="<%$ Resources:lblDestinationReserveTypeResrc %>" />
                                                <span class="formw">
                                                    <cl:CodeLookUp CodeTable="RESERVE_TYPE" runat="server" ID="ReserveTypeCodeFt" type="code"
                                                        ControlName="ReserveTypeCodeFt" Required="true"/>
                                                </span>
                                            </div>
                                            <div id="div_TransTypeCode" class="half" runat="server">
                                                <asp:Label ID="lblDestinationTransactionType" CssClass="required" runat="server" Text="<%$ Resources:lblDestinationTransactionTypeResrc %>" />
                                                <span class="formw">
                                                    <cl:CodeLookUp CodeTable="TRANS_TYPES" runat="server" ID="TransTypeCode" type="code"
                                                        ControlName="TransTypeCode" Required="true"/>
                                                </span>
                                            </div>
                                            <div id="div_ReserveTypeCode" class="half" runat="server">
                                                <asp:Label ID="lblDestinationReserveType2" CssClass="required" runat="server" Text="<%$ Resources:lblDestinationReserveTypeResrc %>" />
                                                <span class="formw">
                                                    <cl:CodeLookUp CodeTable="" runat="server" ID="ReserveTypeCode" type="code"
                                                        ControlName="ReserveTypeCode" Required="true"/>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div_TransTypeCodeFt" class="half" runat="server">
                                                <asp:Label ID="lblDestinationTransactionType2" CssClass="required" runat="server" Text="<%$ Resources:lblDestinationTransactionTypeResrc %>" />
                                                <span class="formw">
                                                    <cl:CodeLookUp CodeTable="TRANS_TYPES" runat="server" ID="TransTypeCodeFt" type="code"
                                                        ControlName="TransTypeCodeFt" Required="true"/>
                                                </span>
                                            </div>
                                            <div id="div_reasonchange" class="half">
                                                <asp:Label ID="lblReasonForChange" CssClass="label" runat="server" Text="<%$ Resources:lblReasonForChangeResrc %>" />
                                                <span class="formw">
                                                    <cl:CodeLookUp CodeTable="CR_REASON_CODE" runat="server" ID="ChangeTypeCode" type="code"
                                                        ControlName="ChangeTypeCode" />
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <asp:TextBox runat="server" ID="pay_transid" Style="display: none" />
            <asp:TextBox Style="display: none" runat="server" ID="TransSplitSelectedId" />
            <asp:TextBox Style="display: none" runat="server" ID="TransSplitGrid_Action" />
            <asp:TextBox Style="display: none" runat="server" ID="TransSplitGrid_RowAddedFlag" />
            <asp:TextBox Style="display: none" runat="server" ID="TransSplitGrid_RowDeletedFlag" />
            <asp:TextBox Style="display: none" runat="server" ID="TransSplitGrid_RowEditFlag" />
            <asp:TextBox Style="display: none" runat="server" ID="clm_entityid" />
            <asp:TextBox Style="display: none" runat="server" ID="Policy" />
            <asp:TextBox Style="display: none" runat="server" ID="Unit" />
            <asp:TextBox Style="display: none" runat="server" ID="transid" />
            <asp:TextBox Style="display: none" runat="server" ID="orgEid" />
            <asp:TextBox Style="display: none" runat="server" ID="claimid" />
            <asp:TextBox Style="display: none" runat="server" ID="lineofbusinesscode" />
            <asp:TextBox Style="display: none" runat="server" ID="transdate" />
            <asp:TextBox Style="display: none" runat="server" ID="transsplitid" Text="" />
            <asp:TextBox Style="display: none" runat="server" ID="rcrowid" Text="" />
            <asp:TextBox Style="display: none" runat="server" ID="hdIsCollectionSource" Text="" />
            <asp:TextBox Style="display: none" runat="server" ID="SysFormName" Text="controlrequest" />
            <asp:TextBox type="text" runat="server" value="" Style="display: none"
                ID="functiontocall" />
            <asp:TextBox type="text" runat="server" value="" Style="display: none" ID="useresfilter" />
            <asp:TextBox type="text" runat="server" value="" Style="display: none" ID="usecarrierclaim" />
            <asp:TextBox type="text" runat="server" value="" Style="display: none" ID="successctrlreq" />
             

        </table>
    </div>
    </form>
</body>
</html>
