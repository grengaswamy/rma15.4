﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BulkCheckProcessPayments.aspx.cs" Inherits="Riskmaster.UI.UI.Funds.BulkCheckProcessPayments" %>
<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Process Payments</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script language='javascript' type="text/javascript">
        function submitPage() {
            if (document.forms[0].hdnFirstTime != null) {
                if (document.forms[0].hdnFirstTime.value == "1") {
                    document.forms[0].hdnFirstTime.value = "2";
                }
                if (document.forms[0].hdnFirstTime.value == "0") {
                    document.forms[0].hdnFirstTime.value = "1";
                    document.forms[0].hdnselectedTransID.value = self.parent.window.opener.document.forms[0].hdnSelected.value;
                    document.forms[0].hdnCheckStatus.value = self.parent.window.opener.document.forms[0].cboCheckStatus.value;
                    document.forms[0].submit();
                    pleaseWait.Show();
                }
            }
        }
        function submitParentPage() {
            if (document.forms[0].hdnFirstTime.value == "2") {
                self.parent.window.opener.document.forms[0].hdnSearchHit.value = "true";
                self.parent.window.opener.document.forms[0].submit();
            }

        }
    </script> 
</head>
<body onload="javascript:submitPage();" onunload="javascript:submitParentPage();"  marginwidth="0" marginheight="0" bottommargin="0" leftmargin="0" rightmargin="0"
				scrolltop="0" scrollleft="0" topmargin="0">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" /> 
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"   CustomMessage="_" />
    <div id="divSuccess" runat="server" >
    <table width="100%">    
        <tr>
            <td class="msgheader">
                <asp:Label ID="lblMsgSuccessHdr" runat="server" Text="<%$ Resources:lblMsgSuccessHdrResrc %>" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblSuccess" runat="server" Font-Bold="true" Font-Names ="Arial" Font-Size="Small" Text="<%$ Resources:lblSuccessResrc %>"/> 
<br />
    <!-- ************************************************ -->
    <!-- Grid Control to display the successfully processed payments -->
    <asp:Panel ID="divForms" runat="server" >
        <asp:GridView ID="gvSuccessPayments" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true" Width="100%" ShowHeader="true"  EnableViewState="false" 
        EmptyDataText="<%$ Resources:gvPaymentsEmptyText %>">
            <PagerStyle CssClass="headertext2"  ForeColor="white"/>
            <HeaderStyle CssClass="colheader6" />
            <AlternatingRowStyle CssClass="data2" /> 
            <FooterStyle CssClass ="colheader6" />
            <Columns>              
                <asp:BoundField  HeaderText="<%$ Resources:gvPaymentsHdrClient %>"  DataField="client" ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrTransactionDate %>"  DataField="transactiondate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrPayeeName %>"  DataField="payeename"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrClaimNumber %>"  DataField="claimnumber"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrAmount %>"  DataField="amount"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrTransType %>"  DataField="transactiontype"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrAccountName %>"  DataField="accountname"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrReserveType %>"  DataField="reservetype"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrCurrentAdjuster %>"  DataField="currentadjuster"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
               
                <%--<asp:BoundField   HeaderText="From Date"  DataField="fromdate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="To Date"  DataField="todate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="Invoice#"  DataField="invoicenumber"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="Invoice Date"  DataField="invoicedate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>--%>
               
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <!-- ************************************************* -->
    </div>
    <br />
    <div id="divFailure" runat="server">
    <table width="100%">    
        <tr>
            <td class="msgheader">
                <asp:Label ID="lblMsgFailureHdr" runat="server" Text="<%$ Resources:lblMsgFailureHdrResrc %>" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblFailure" runat="server" Font-Bold="true" Font-Names ="Arial" Font-Size="Small" Text="<%$ Resources:lblFailureResrc %>"/> 
<br />
<!-- ************************************************ -->
    <!-- Grid Control to display the successfully processed payments -->
    <asp:Panel ID="Panel1" runat="server" >
        <asp:GridView ID="gvUnsuccessfulPayments" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true" Width="100%" ShowHeader="true"  EnableViewState="false" 
        EmptyDataText="<%$ Resources:gvPaymentsEmptyText %>">
            <PagerStyle CssClass="headertext2"  ForeColor="white"/>
            <HeaderStyle CssClass="colheader6" />
            <AlternatingRowStyle CssClass="data2" /> 
            <FooterStyle CssClass ="colheader6" />
            <Columns>              
                <asp:BoundField  HeaderText="<%$ Resources:gvPaymentsHdrClient %>"  DataField="client" ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrTransactionDate %>"  DataField="transactiondate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrPayeeName %>"  DataField="payeename"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrClaimNumber %>"  DataField="claimnumber"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrAmount %>"  DataField="amount"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrTransType %>"  DataField="transactiontype"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrAccountName %>"  DataField="accountname"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrReserveType %>"  DataField="reservetype"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="<%$ Resources:gvPaymentsHdrCurrentAdjuster %>"  DataField="currentadjuster"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <%--<asp:BoundField   HeaderText="From Date"  DataField="fromdate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="To Date"  DataField="todate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="Invoice#"  DataField="invoicenumber"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>
                <asp:BoundField   HeaderText="Invoice Date"  DataField="invoicedate"  ControlStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign = "Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                </asp:BoundField>--%>
               
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <!-- ************************************************* -->
    </div>
    <br />
    <div>
        <table id="tblButton" width="100%">
            <tr>
                <td></td>
                <td align="center">
                    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:btnCloseResrc %>"  CssClass="button" OnClientClick="javascript:self.close();"/>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <!-- Hidden Fields -->
    <asp:HiddenField ID="hdnClose" runat="server"  Value="false"/>
    <asp:HiddenField ID="hdnselectedTransID" runat="server"  Value=""/>
    <asp:HiddenField ID="hdnCheckStatus" runat="server"  Value=""/>
    <asp:HiddenField ID="hdnFirstTime" runat="server"  Value="0"/>
    </form>
</body>
</html>
