<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="InsufficientReserves.aspx.cs" Inherits="Riskmaster.UI.Funds.InsufficientReserves" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Insufficient Reserve</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/InsufResrvAmnt.js" type="text/javascript"></script>

    <script language="JavaScript" type="text/javascript">
        function checkforsufficientreservesupdated() {
            var Reason = document.forms[0].reason.value;
            if (Reason == "AUTO-ADJUST") {
                window.opener.document.forms[0].SysCmd.value = "9";
                window.opener.document.forms[0].submit();

                window.close();
            }
        }
    </script>

    <script type="text/javascript" src="../../Scripts/rmx-common-ref.js"></script>

</head>

<body onload="checkforsufficientreservesupdated();this.focus();">
    <form id="frmData" runat="server">
    <table align="center">
                <tr>
                    <td align="left" colspan="4">
                        <font size="2" color="red">
                            <asp:Label ID="lblInfo1Resrc" runat="server" Text="<%$ Resources:lblInfo1Resrc %>" />
                            <b>
                            <asp:label id="lblReserveType" runat="server"></asp:label>
                            </b> 
                            <asp:Label ID="lblInfo2" runat="server" Text="<%$ Resources:lblInfo2Resrc %>" />
                        </font>
                    </td>
                </tr>
                <tr>
                    <td width="30%" align="right">
                        <asp:Label ID="lblCurrentBlnc" runat="server" Text="<%$ Resources:lblCurrentBlncResrc %>" />&nbsp;&nbsp;
                    </td>
                    <td id="curbalance" width="10%" align="left">
                        <b>
                        <asp:label id="lblCurrentBalance" runat="server"></asp:label>
                        </b>
                        <asp:TextBox style="display:none" rmxref="/Instance/Document/reserves" id="txtres" runat="server"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:TextBox rmxref="/Instance/Document/SetReserveAmount" id="txtsetres" runat="server" MaxLength="15"></asp:TextBox>
                    </td>
                    <td>
                		<asp:Button runat="server" class="button" OnClientClick="return clickReserve('1');" OnClick="ModifyReserves" Text="<%$ Resources:btnsetresResrc %>" id="btnsetres" />
                    </td>
                </tr>
                <tr>
                    <td width="30%" align="right">
                        <asp:Label ID="lblCurrentRsrv" runat="server" Text="<%$ Resources:lblCurrentRsrvResrc %>" />&nbsp;&nbsp;
                    </td>
                    <td id="curreserve" width="10%" align="left">
                        <b>
                            <asp:label id="lblCurrentReserve" runat="server"></asp:label>
                        </b>
                    </td>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblOr" runat="server" Text="<%$ Resources:lblOrResrc %>" />
                    </td>
                </tr>
                <tr>
                    <td width="30%" align="right">
                        <asp:Label ID="lblPaymentAmnt" runat="server" Text="<%$ Resources:lblPaymentAmntResrc %>" />&nbsp;&nbsp;
                    </td>
                    <td width="10%">
                        <b>
                            <asp:label id="lblPaymentAmount" runat="server"></asp:label>
                        </b>
                    </td>
                    <td align="right">
                        <asp:TextBox rmxref="/Instance/Document/AddReserveAmount" id="txtaddres" runat="server" MaxLength="15"></asp:TextBox>
                    </td>
                    <td>
                		<asp:Button runat="server" class="button" OnClientClick="return clickReserve('0');" OnClick="ModifyReserves" Text="<%$ Resources:btnaddresResrc %>" id="btnaddres" />
                    </td>
                </tr>
            </table>
    <table width="100%">
        <tr>
            <td align="right">
        		<asp:Button runat="server" class="button" OnClientClick="return clickReserve('2');" OnClick="ModifyReserves" Text="<%$ Resources:btncontResrc %>" id="btncont" />
            </td>
            <td align="left">
        		<asp:Button runat="server" class="button" OnClientClick="return clickReserve('-1');" Text="<%$ Resources:btncloseResrc %>" id="btnclose" />
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    
        <asp:HiddenField ID="hdInsuffResXml" runat="server"/>
        <asp:HiddenField ID="skipreservetypes" runat="server"/>
        <asp:HiddenField ID="newamount" runat="server"/>
        <asp:HiddenField ID="reason" runat="server"/>
        <asp:HiddenField ID="isreslimit" runat="server"/>
        <asp:HiddenField ID="reslimit" runat="server"/>
       <%-- // Jira 6385- Incurred Limit--%>
         <asp:HiddenField ID="isclminclimit" runat="server"/>
        <asp:HiddenField ID="clminclimit" runat="server"/>
        <asp:HiddenField ID="currclminclimit" runat="server"/>
        <%--// Jira 6385- Incurred Limit--%>
        <asp:TextBox id="ClaimCurrency" runat="server" style="display:none"></asp:TextBox>

        </form>
</body>
</html>
