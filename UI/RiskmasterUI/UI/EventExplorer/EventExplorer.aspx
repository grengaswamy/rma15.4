﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventExplorer.aspx.cs" Inherits="Riskmaster.UI.EventExplorer.EventExplorer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" Tagname="ErrorControl" tagprefix="uc3" %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Event Explorer</title>
    
  
</head>
<body>
   
      <iframe id="EventExplorerTree" name="EventExplorerTree" width="30%"  frameborder="0" marginheight="0" marginwidth="0" border="0" src="EventExplorerTree.aspx?EventId=<%=EventId%>&Lob=<%=Lob%>&ClaimId=<%=ClaimId%>">
      </iframe>
      <iframe id="EventExplorerData" name="EventExplorerData" width="69%"  frameborder="0" marginheight="0" marginwidth="0" border="0" src="EventExplorerData.aspx?EventId=<%=EventId%>&Lob=<%=Lob%>&ClaimId=<%=ClaimId%>">
      </iframe>
      <!--This script is used to Resize the Iframe to Full Screen , it was not done automatically with height=100%-->
  <script type="text/javascript">
    function resizeIframe() {
        var height = document.documentElement.clientHeight;
        height -= document.getElementById('EventExplorerTree').offsetTop;
        height -= 10; 
        document.getElementById('EventExplorerTree').style.height = height +"px";
        document.getElementById('EventExplorerData').style.height = height +"px";
    
    };
    document.getElementById('EventExplorerTree').onload = resizeIframe;
    document.getElementById('EventExplorerData').onload = resizeIframe;
          window.onresize = resizeIframe;

      //MITS 27964 hlv 4/10/2012 begin
      if (window.opener != null) {
          window.opener.window.parent.parent.iwintype = document.title;
      }

      window.onunload = function () {
          if (window.opener != null) {
              window.opener.window.parent.parent.iwintype = "";
          }
      }
      //MITS 27964 hlv 4/10/2012 end
</script>

</body>
</html>
