<xsl:transform	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:xforms="http://www.w3.org/2002/xforms" 
				xmlns:xxforms="http://orbeon.org/oxf/xml/xforms" 
				xmlns:xhtml="http://www.w3.org/1999/xhtml" 
				xmlns:app="http://csc.com/Riskmaster/Webservice/Common" version="2.0" >
				
	<xsl:template match="/">
		<xsl:apply-templates select="/ResultMessage/Document"/>
	</xsl:template>
				
	<xsl:template match="//Document">
		<!--<html>
			<head>
				<title>Explorer Data (<xsl:value-of select="data/@title"/>)</title>
				<script language="javascript" src="/RiskmasterUI/UI/EventExplorer/Scripts/eventexplorer.js"> </script>
        <link rel="Stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" />


    </head>-->
			<body class="9pt" style="width: 95%; margin: 0px; padding: 0px;" ><!--Modified by Scott MITS 20137-->
				<form name="frmData">
				<h3 class="Blue" align="left"><xsl:value-of select="data/@title"/></h3>
				<input type="hidden" id="EventId" name="EventId"></input>
          <input type="hidden" id="Key" name="Key"></input>

          <!--Added Rakhi for Mits 18520:In quick summary, headings and data are squished-->
          <p align="left">
          <table cellspacing="4" cellpadding="4" width="100%">  <!--Incorporated Parijat Changes(Parijat:19023 -changes done for formatting) of cellspacing -->
            <xsl:apply-templates select="data/header"/>
            <xsl:apply-templates select="data/rows"/>
          </table>
            <!--Added Rakhi for Mits 18520:In quick summary, headings and data are squished-->
            
          <!--Commented Rakhi-Approach changed for Mits 18520 
          //Added for Mits 18520
          <xsl:choose>
            <xsl:when test="substring(//Document/data/@title , 1 , 25) = 'Payment history for claim'" >
              <table cellspacing="4" cellpadding="4" width="1200px">	//Parijat:19023 -changes done for formatting
                <xsl:apply-templates select="data/header"/>
                <xsl:apply-templates select="data/rows"/>
              </table>
            </xsl:when>
            <xsl:when test="substring(//Document/data/@title , 1 , 31) = 'Payment history by reserve type'" >
              <table cellspacing="0" cellpadding="4" width="1500px">
                <xsl:apply-templates select="data/header"/>
                <xsl:apply-templates select="data/rows"/>
              </table>
            </xsl:when>
            <xsl:when test="substring(//Document/data/@title , 1 , 24) = 'Payment History By Payee'" >
              <table cellspacing="0" cellpadding="4" width="1000px">
                <xsl:apply-templates select="data/header"/>
                <xsl:apply-templates select="data/rows"/>
              </table>
            </xsl:when>
            <xsl:otherwise>
              <table cellspacing="0" cellpadding="2" width="100%"> 
                <xsl:apply-templates select="data/header"/>
                <xsl:apply-templates select="data/rows"/>
              </table>
            </xsl:otherwise>
          </xsl:choose>
          //Added for Mits 18520	-->		
				</p>
				</form>
			</body>
		<!--</html>-->
	</xsl:template>

	<xsl:template match="header">
		<tr class="datah">
      <!-- Changed by Gagan for MITS 7658 : Start -->
      <!--
      </xsl:apply-templates>
      -->      
      <xsl:apply-templates select="col"></xsl:apply-templates>
      <!-- Changed by Gagan for MITS 7658 : End -->
		</tr>
	</xsl:template>
	
	<xsl:template match="rows">
    <xsl:choose><!--psharma41: MITS 19725 Enhanced notes does not need to be sorted as they are by default sorted thru activity date -->
      <xsl:when test="@title[.!='']"> <!--Added case Rakhi for display of multiple addresses-->
        <tr height="10px"></tr>
        <tr>
          <td colspan="2">
            <h4 class="datah">
              <xsl:value-of select="@title"/>
            </h4>
          </td>
        </tr>
        <xsl:apply-templates />
      </xsl:when>
    <xsl:when test="substring(//Document/data/@title , 1 , 14) = 'Enhanced Notes'" >
      <xsl:apply-templates/>
      </xsl:when>
    <xsl:otherwise><!--psharma41: END MITS 19725 Enhanced notes does not need to be sorted as they are by default sorted thru activity date -->
      <xsl:apply-templates>
        <xsl:sort select="@ClaimNumber"/>
        <!--abansal23: MITS 17595 Claims not sorted by Claim Number in Quick Summary -->
      </xsl:apply-templates>
    </xsl:otherwise>
  </xsl:choose>
  </xsl:template>

	


  <xsl:template match="col">
    <xsl:choose>
      <xsl:when test="@rid[.!='']">
        <td class="9pt">
          <a href="#">
            <xsl:attribute name="onclick">
              javascript:getdetail('<xsl:value-of select="@rid"/>','<xsl:value-of select="@id"/>');
            </xsl:attribute>
            <xsl:value-of select="text()"/>
          </a>
        </td>
      </xsl:when>
      <xsl:otherwise>
        <td>        
          <xsl:value-of select="text()"/>
        </td>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>




  <xsl:template match="row">
    <!-- Changed by Gagan for MITS 7658 : Start -->
    <xsl:choose>
      <xsl:when test="substring(//Document/data/@title , 1 , 14) = 'Enhanced Notes'" >
        <xsl:variable name="pos" select="position()"></xsl:variable>       
        <xsl:choose>
          <xsl:when test="(position() mod 2) = 0">
            <tr bgcolor="WhiteSmoke"><!--Parijat:19023 -changes done for formatting-->
              <xsl:for-each select="col">
                <xsl:choose>
                  <xsl:when test="position()=4">
                    <input type="hidden">
                      <xsl:attribute name="id">value<xsl:value-of select="$pos"/></xsl:attribute>
                      <xsl:attribute name="value">
                        <xsl:value-of select="text()"/>
                      </xsl:attribute>
                    </input>                    
                    <!--MGaba2:MITS 20137:Bottom scroll bar should not come on this screen-->
                    <!--<td style = "word-break:break-all" class="9pt">-->
                      <td class="9pt">
                      <xsl:attribute name="id">row<xsl:value-of select="$pos"/></xsl:attribute>
                      <script>
                        fnInnerHtml("<xsl:value-of select="$pos"/>");
                      </script>
                    </td>    
                  </xsl:when>
                  <xsl:otherwise>
                    <td>
                      <xsl:value-of select="text()"></xsl:value-of>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <tr bgcolor="White">
              <xsl:for-each select="col">
                <xsl:choose>
                  <xsl:when test="position()=4">
                    <input type="hidden">
                      <xsl:attribute name="id">value<xsl:value-of select="$pos"/></xsl:attribute>
                      <xsl:attribute name="value">
                        <xsl:value-of select="text()"/>
                      </xsl:attribute>
                    </input>
                    <!--MGaba2:MITS 20137:Bottom scroll bar should not come on this screen-->
                    <!--<td style = "word-break:break-all" class="9pt">-->
                      <td class="9pt">
                      <xsl:attribute name="id">row<xsl:value-of select="$pos"/></xsl:attribute>
                      <script>
                        fnInnerHtml("<xsl:value-of select="$pos"/>");
                      </script>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td>
                      <xsl:value-of select="text()"></xsl:value-of>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:when>
      <xsl:when test="substring(//Document/data/@title , 1 , 15) = 'Notes for claim'" >
        <xsl:variable name="pos" select="position()"></xsl:variable>
        <xsl:choose>
          <xsl:when test="(position() mod 2) = 0">
            <tr bgcolor="WhiteSmoke">
              <!--Parijat:19023 -changes done for formatting-->
              <xsl:for-each select="col">
                <xsl:choose>
                  <xsl:when test="position()=4">
                    <input type="hidden">
                      <xsl:attribute name="id">value<xsl:value-of select="$pos"/></xsl:attribute>
                      <xsl:attribute name="value">
                        <xsl:value-of select="text()"/>
                      </xsl:attribute>
                    </input>
                    <!--MGaba2:MITS 20137:Bottom scroll bar should not come on this screen-->
                    <!--<td style = "word-break:break-all" class="9pt">-->
                    <td class="9pt">
                      <xsl:attribute name="id">row<xsl:value-of select="$pos"/></xsl:attribute>
                      <script>
                        fnInnerHtml("<xsl:value-of select="$pos"/>");
                      </script>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td>
                      <xsl:value-of select="text()"></xsl:value-of>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <tr bgcolor="White">
              <xsl:for-each select="col">
                <xsl:choose>
                  <xsl:when test="position()=4">
                    <input type="hidden">
                      <xsl:attribute name="id">value<xsl:value-of select="$pos"/></xsl:attribute>
                      <xsl:attribute name="value">
                        <xsl:value-of select="text()"/>
                      </xsl:attribute>
                    </input>
                    <!--MGaba2:MITS 20137:Bottom scroll bar should not come on this screen-->
                    <!--<td style = "word-break:break-all" class="9pt">-->
                    <td class="9pt">
                      <xsl:attribute name="id">row<xsl:value-of select="$pos"/></xsl:attribute>
                      <script>
                        fnInnerHtml("<xsl:value-of select="$pos"/>");
                      </script>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td>
                      <xsl:value-of select="text()"></xsl:value-of>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:otherwise>
      <!-- Changed by Gagan for MITS 7658 : End -->
        <xsl:choose>
          <xsl:when test="(position() mod 2) = 0">
            <tr bgcolor="WhiteSmoke">
              <xsl:apply-templates select="col"/>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <tr bgcolor="White">
              <xsl:apply-templates select="col"/>
            </tr>
          </xsl:otherwise>
        </xsl:choose>
    <!-- Changed by Gagan for MITS 7658 : Start -->
      </xsl:otherwise>
    </xsl:choose>
    <!-- Changed by Gagan for MITS 7658 : End -->



  </xsl:template>

</xsl:transform>
