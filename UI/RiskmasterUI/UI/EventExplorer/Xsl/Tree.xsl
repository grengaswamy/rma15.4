<xsl:transform	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:xforms="http://www.w3.org/2002/xforms" 
				xmlns:xxforms="http://orbeon.org/oxf/xml/xforms" 
				xmlns:xhtml="http://www.w3.org/1999/xhtml" 
				xmlns:app="http://csc.com/Riskmaster/Webservice/Common" version="2.0" >
				
	<xsl:template match="/">
		<xsl:apply-templates select="/ResultMessage/Document"/>
	</xsl:template>
				
	<xsl:template match="//Document">
		<!--<html xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:f="http://orbeon.org/oxf/xml/formatting" xmlns:xhtml="http://www.w3.org/1999/xhtml"  >-->
			<!--<head>
				<title>Event Explorer</title>
        <link rel="Stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" />
			</head>-->
			<body class="explorer" >
				<h3 class="Blue" align="center">Quick Summary</h3>
				<xsl:for-each select="eventsummary">
					<xsl:apply-templates/>
				</xsl:for-each>							
			</body>
		<!--</html>-->
	</xsl:template>
	
	<xsl:template match="node">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td class="9pt" width="1"> <img src="" width="22" height="1" alt="" />
</td>
				<td class="9pt" align="left">
					<xsl:choose>
						<xsl:when test="count(child::*) != 0 ">
							<a class="White" href="#"><xsl:attribute name="onClick">return GetData("<xsl:value-of select="@key"/>")</xsl:attribute> <xsl:value-of select="@text"/></a>
						</xsl:when>
						<xsl:otherwise>
			      			<a class="White" href="#"><xsl:attribute name="onClick">return GetData("<xsl:value-of select="@key"/>")</xsl:attribute> <xsl:value-of select="@text"/></a>
			      		</xsl:otherwise>
					</xsl:choose>
				</td>				
			</tr>
			<tr>
				<td class="9pt"></td>
				<td class="9pt"><xsl:apply-templates/></td>
			</tr>
		</table>
	</xsl:template>			
</xsl:transform>