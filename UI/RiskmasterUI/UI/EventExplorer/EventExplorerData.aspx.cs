﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using System.IO;
using Riskmaster.Common;
using System.Text.RegularExpressions;
namespace Riskmaster.UI.EventExplorer
{
    public partial class EventExplorerData : System.Web.UI.Page
    {
        public string Display = "";
        string GetDataMessageTemplate = "<Message><Authorization></Authorization><Call>" +
            "<Function>EventExplorerAdaptor.GetData</Function></Call><Document><EventExplorer><EventId>1</EventId><Key />" +
            "<ClaimId>1</ClaimId></EventExplorer></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            XslCompiledTransform xslt = null;
            StringWriter sw = new StringWriter();
            try
            {
                if (AppHelper.GetFormValue("Key") == "")
                {
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/EventId", AppHelper.GetQueryStringValue("EventId"));
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/Key", AppHelper.GetQueryStringValue("Key"));
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/ClaimId", AppHelper.GetQueryStringValue("ClaimId"));
                }
                else
                {
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/EventId", AppHelper.GetFormValue("EventId"));
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/Key", AppHelper.GetFormValue("Key"));
                }
                //tmalhotra2 MITS-29787 Re# 5.1.13
                if (AppHelper.GetQueryStringValue("ClaimId") != string.Empty)
                {
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/ClaimId", AppHelper.GetQueryStringValue("ClaimId"));
                }
                string sNoteKey = (AppHelper.GetFormValue("Key") == "") ? AppHelper.GetQueryStringValue("Key") : AppHelper.GetFormValue("Key");
                string sReturn = AppHelper.CallCWSService(GetDataMessageTemplate);

                // rrachev JIRA RMA-9421 Begin
                //XmlDocument Model = new XmlDocument();
                //Model.LoadXml(sReturn);
                xslt = new XslCompiledTransform();
                xslt.Load(Server.MapPath("./Xsl/Data.xsl"));

                //// Transform the file.
                //xslt.Transform(Model, null, sw);
                XmlWriter transformXmlWriter = XmlWriter.Create(sw, xslt.OutputSettings);
                bool tryAgain = false;
                do
                {
                    tryAgain = false;
                    XmlDocument Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    try
                    {
                        xslt.Transform(Model, transformXmlWriter);
                    }
                    catch (ArgumentException aex)
                    {
                        if (aex.Message.IndexOf("hexadecimal value", StringComparison.InvariantCultureIgnoreCase) > -1 &&
                            aex.Message.IndexOf("0x", StringComparison.InvariantCultureIgnoreCase) > -1 &&
                            aex.Message.IndexOf("invalid character", StringComparison.InvariantCultureIgnoreCase) > -1)
                        {
                            // Remove invalid characters
                            string r = "[\x00-\x08\x0B\x0C\x0E-\x1F]";
                            sReturn = Regex.Replace(sReturn, r, "", RegexOptions.Compiled);
                            // To avoid this type of exception it is nessesary "&" to be removed also.  
                            // But the XML contains HTML encoded symbols so removing of "&" will broke the right presentation of the data.
                            // So it is needed to skip characters check.                        
                            XmlWriterSettings settings = xslt.OutputSettings.Clone();
                            settings.CheckCharacters = false;
                            sw = new StringWriter();
                            transformXmlWriter = XmlWriter.Create(sw, settings);
                            tryAgain = true;
                        }
                        else
                        {
                            throw;
                        }
                    }


                } while (tryAgain);
                // rrachev JIRA RMA-9421 End
                Display = sw.ToString();
                //mdhamija MITS 28062--Start
                //rsharma220 JIRA 3841
                if (!string.IsNullOrEmpty(sNoteKey))
                {
                    if (sNoteKey.Contains("name=note"))
                        Display = Server.HtmlDecode(Display);
                }
                //Display = Server.HtmlDecode(Display); //rkotak:mits 28930, this line is supposed to be commnted but was uncommented during the merge on 5/3/2012
                //mdhamija MITS 28062
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
