﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Executivesummary
{
    public partial class ExecSummary : NonFDMBasePageCWS
    {
        /// <summary>
        /// Returns the PDF for the specified EventID or ClaimID or AddTracking
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <author>Rahul Solanki</author>        
        protected void Page_Load(object sender, EventArgs e)
        {
            string sAdaptorName = string.Empty;
            string sRecordID = string.Empty;
            string sReportType = string.Empty;
            string sResponse = "";

            try
            {
                //starts changes by Nitin for Mits 15312 
                if (Request.QueryString["recid"] != null)
                {
                    sRecordID = Request.QueryString["recid"].ToString();
                }
                else
                {
                    throw new ApplicationException("record id missing from querystring.");
                }

                if (Request.QueryString["type"] != null)
                {
                    sReportType = Request.QueryString["type"].ToString().ToUpper();
                }
                else
                {
                    throw new ApplicationException("record type missing from querystring");
                }
                
                if (sReportType=="EVENT")
                {
                    sAdaptorName = "ExecutiveSummaryAdaptor.EventExecSumm";
                    txtRecordId.Attributes.Add("rmxref", "Instance/Document/ExecutiveSummaryReport/EventId");
                }
                else if (sReportType == "CLAIM")
                {
                    sAdaptorName = "ExecutiveSummaryAdaptor.ClaimExecSumm";
                    txtRecordId.Attributes.Add("rmxref", "Instance/Document/ExecutiveSummaryReport/ClaimId");
                }
                else if (sReportType == "ADMTRACKING")
                {
                    sAdaptorName = "ExecutiveSummaryAdaptor.AdmTrackingExecSumm";
                    txtRecordId.Attributes.Add("rmxref", "Instance/Document/ExecutiveSummaryReport/RecordId");
                    txtTableName.Attributes.Add("rmxref", "Instance/Document/ExecutiveSummaryReport/TableName");

                    if (Request.QueryString["admtable"] != null)
                    {
                        txtTableName.Text = Request.QueryString["admtable"].ToString();
                    }
                    else
                    {
                        throw new ApplicationException("admin tracking table name is missing in querystring");
                    }
                }
                else
                {
                    throw new Exception("report type missing from querystring.");
                }

                txtRecordId.Text = sRecordID;

                //ends changes by Nitin for Mits 15312 
                TextBox txtPdfFile = (TextBox)this.Form.FindControl("File");

                //MITS 26441 : Client requests change to custom print process for Exec Summary/Large Notes 
                //modified by Raman Bhatia on 10/21/2011
                string control = Request.Form["__EVENTTARGET"]; 
                if (IsPostBack && !String.IsNullOrEmpty(control) && control == "btnChoices")
                {
                    Response.Clear();
                    if (rdr1.Checked)
                    {
                        //Deb : MITS 31227 
                        string strArr = JavaScriptResourceHandler.ProcessSpecificKeyRequest(this.Context,RMXResourceManager.RMXResourceProvider.PageId("ExecSummary.aspx"),((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString(),"OnSuccessMessage", "ExeSummaryValidations");
                        string str = "alert('" + strArr.Substring(strArr.IndexOf("\"") + 1).Substring(0, strArr.Substring(strArr.IndexOf("\"") + 1).IndexOf("\"")) + "');self.close();";
                        //string str = "alert('Your request has been scheduled successfully.');self.close();";
                        //Deb : MITS 31227 
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "messageKey", str , true);
                    }
                    else if(rdr3.Checked)
                    {
                        ClientScript.RegisterClientScriptBlock(this.GetType() , "closeKey" , "self.close();" , true);
                    }
                    //Response.Flush();
                    //Response.End();

                }

                //MITS 26441 : Client requests change to custom print process for Exec Summary/Large Notes 
                //modified by Raman Bhatia on 10/21/2011
                //if (!IsPostBack)
                //{
                    //Raman: We do not need to run BindData2Control
                    //NonFDMCWSPageLoad(sAdaptorName);
                    CallCWS(sAdaptorName, null, out sResponse, true, false);

                    XElement oReturntemp = XElement.Parse(sResponse);
                    string sMsgStatusCode2 = oReturntemp.XPathSelectElement("//MsgStatusCd").Value;
                    if (!sMsgStatusCode2.Equals("error", StringComparison.OrdinalIgnoreCase))                    
                    //if (!ErrorHelper.IsAnyErrorInCWSCall(sResponse))
                    {
                        divChoices.Visible = false;
                        XElement objOuterXml = XElement.Parse(sResponse);
                        XElement oFileEle = objOuterXml.XPathSelectElement("./Document/ExecutiveSummaryReport/File");
                        if (oFileEle != null)
                        {
                            txtPdfFile.Text = oFileEle.Value;
                        }

                        byte[] pdfbytes = Convert.FromBase64String(txtPdfFile.Text);

                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Charset = "";
                        Response.AppendHeader("Content-Encoding", "none;");
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", string.Format("inline; filename=ExecutiveSummary_{0}_{1}.PDF", sReportType, sRecordID));
                        Response.AddHeader("Accept-Ranges", "bytes");
                        Response.BinaryWrite(pdfbytes);
                        Response.Flush();
                        //Response.Close(); //igupta3 Mits: 33301
                    }
                    else
                    {
                        XElement oReturn = XElement.Parse(sResponse);
                        string sMsgStatusCode = oReturn.XPathSelectElement("//ExtendedStatusCd").Value;
                        //if (sMsgStatusCode.Contains"PrintPDF.ValidateCriteria.ThresholdExceeded", StringComparison.OrdinalIgnoreCase))
                        if (sMsgStatusCode.Contains("InvalidCriteria") || sMsgStatusCode.Contains("ThresholdExceeded"))
                        {
                            //MITS 26441 : Client requests change to custom print process for Exec Summary/Large Notes 
                            //modified by Raman Bhatia on 10/21/2011

                            /*
                            Response.Clear();
                            Response.Write("Executive Summary threshold limit was exceeded."
                                + "ExecSummary creation has been scheduled.<br/>"
                                + "Once generated, it will be accessible from the Attachments list.<br/><br/>"
                                + "<input type='button' value='OK' style='align:center;' onclick='javascript:window.close()'/>"
                                );

                            Response.Flush();
                            Response.End();
                             * 
                             */
                            rdr1.Checked = true;
                            divChoices.Visible = true;
                            ErrorControl.Visible = false;
                        }
                        
                        //throw new InvalidCriteriaException(Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded"), p_objException);
                  //  }
                }
            }
            catch (Exception ex)
            {
                //error.Text = "ERROR: " + ex.Message.ToString();
                //throw new Exception("Pdf Error", ex.GetBaseException());
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


        }
      


    }
}
