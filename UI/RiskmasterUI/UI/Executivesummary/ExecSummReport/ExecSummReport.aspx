﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExecSummReport.aspx.cs" Inherits="Riskmaster.UI.ExecSummReport.ExecSummReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Executive Summary Configuration</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
   <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">
  </script>
  <script type="text/javascript" language="JavaScript" src="../../../Scripts/ExecutiveSummary.js">
  </script>
</head>
<body onload="OnLoad();loadTabList();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="formtitle">Executive Summary Configuration</div>
   <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td>
      <table border="0" cellspacing="0" celpadding="0">
       <tr>
        <td class="Selected" nowrap="true" name="TABSevents" id="TABSevents"><a class="Selected" HREF="#" onClick="tabChange(this.name);" name="events" id="LINKTABSevents"><span style="text-decoration:none">Events</span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="true" name="TABSallclaims" id="TABSallclaims"><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="allclaims" id="LINKTABSallclaims"><span style="text-decoration:none">Claims (All)</span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="true" name="TABSgcclaims" id="TABSgcclaims"><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="gcclaims" id="LINKTABSgcclaims"><span style="text-decoration:none">Claims (GC)</span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="true" name="TABSvaclaims" id="TABSvaclaims"><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="vaclaims" id="LINKTABSvaclaims"><span style="text-decoration:none">Claims (VA)</span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="true" name="TABSwcclaims" id="TABSwcclaims"><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="wcclaims" id="LINKTABSwcclaims"><span style="text-decoration:none">Claims (WC)</span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="true" name="TABSdiclaims" id="TABSdiclaims"><a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="diclaims" id="LINKTABSdiclaims"><span style="text-decoration:none">Claims (DI)</span></a></td>
        <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
       </tr>
      </table>
     </td>
    </tr>
    <tr valign="top">
     <td valign="top">
      <div style="position:relative;left:0;top:0;width:950px;height:280px;overflow:auto;valign:top" class="singletopborder">
       <table border="0" cellspacing="0" celpadding="0" width="100%" valign="top" ALIGN="center">
        <tr valign="top">
         <td valign="top">
          <table border="0" cellspacing="0" celpadding="0" width="100%" ALIGN="center" name="FORMTABevents" id="FORMTABevents">
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EventInformation" id="chkEventInformation" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="1" tab="events"/>Event Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/MedwatchInfo" id="chkMedWatchInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="9" tab="events"/>MedWatch Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/PersonInvolved" id="chkPersonInvolved"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="17" tab="events"/>Person Involved
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EventDetail" id="chkEventDetail"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="2" tab="events"/>Event Detail
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EquipIndicator" id="chkEquipmentIndicator"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="10" tab="events"/>Equipment Indicator
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Patient" id="chkPatientInvolved"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="18" tab="events"/>Patient Involved
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EventDatedText" id="chkEventDatedText"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="3" tab="events"/>Event Dated Text
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/MedicationIndicator" id="chkMedicationIndicator"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="11" tab="events"/>Medication Indicator
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Physicians" id="chkPhysicians"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="19" tab="events"/>Physicians
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/ReportedInfo" id="chkReportedInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="4" tab="events"/>Reported Information
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/ConcomitantProduct" id="chkConcomitantProduct"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="12" tab="events"/>Concomitant Product
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OtherMedicalStaff" id="chkOtherMedicalStaff"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="20" tab="events"/>Other Medical Staff
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/FollowupInfo" id="chkFollowUpInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="5" tab="events"/>Follow Up Information
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/MedwatchTest" id="chkMedWatchTest"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="13" tab="events"/>MedWatch Test
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OtherPersons" id="chkOtherPersons"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="21" tab="events"/>Other Persons
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/OshaInfo" id="chkOSHAInformation1" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="6" tab="events"/>OSHA Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/DiaryDetails" id="chkDiaryDetails1" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="14" tab="events"/>Diary Details
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/QualityManagement" id="chkQualityManagement"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="22" tab="events"/>Quality Management
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/ClaimInfo" id="chkClaimInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="7" tab="events"/>Claim Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Supp" id="chkSupplementalFields1" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="15" tab="events"/>Supplemental Fields
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Intervention" id="chkIntervention"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="23" tab="events"/>Intervention
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/FallInfo" id="chkFallInformation" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="8" tab="events"/>Fall Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/Comment" id="chkComments1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="16" tab="events"/>Comments
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/Event/EnhancedNotes" id="chkEnhancedNotes1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="16" tab="events"/>EnhancedNotes
            </td>
           </tr>
          </table>
          <table border="0" cellspacing="0" celpadding="0" width="100%" ALIGN="center" name="FORMTABallclaims" id="FORMTABallclaims" style="display:none;">
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ClaimInformation" id="chkClaimInformation1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="1" tab="allclaims"/>Claim Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/LitigationInformation" id="chkLitigationInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="11" tab="allclaims"/>Litigation Information
            </td>
           </tr>
           <tr>
           <%-- added by rkaur7 for MITS 16668--%>
           <td width="40%" valign="top"><asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/PolicyStates" id="chkPolicyStatesAllClaims"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="2" tab="allclaims"/>Policy States            
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/AttorneyInformation" id="chkAttorneyInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="12" tab="allclaims"/>Attorney Information
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top"><asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ClaimLevelReserves" id="chkClaimLevelReserves"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="2" tab="allclaims"/>Claim Level Reserves            
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ExpertWitness" id="chkExpertWitness"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="13" tab="allclaims"/>Expert Witness
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/PaymentHistory" id="chkPaymentHistory"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="3" tab="allclaims"/>Payment History            
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/DefendantInformation" id="chkDefendantInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="14" tab="allclaims"/>Defendant Information
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/FundsManagement" id="chkFundsManagement"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="4" tab="allclaims"/>Funds Management            
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/DefAttorneyInformation" id="chkDefAttorneyInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="15" tab="allclaims"/>Attorney Information
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/TransactionDetail" id="chkTransactionDetail"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="5" tab="allclaims"/>Transaction Detail            
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/ClaimInvolvementHistory" id="chkClaimInvolvementHistory1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="16" tab="allclaims"/>Claim Involvement History
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top"><asp:checkbox runat="server" name="$node^114" value="True" appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/EventDetail" id="chkAllClaimsDetail"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="6" tab="allclaims"/>Event Detail            
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  appearance="full" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/DiaryDetails" id="chkDiaryDetails2" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="17" tab="allclaims"/>Diary Details
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/PersonsInvolved" id="chkPersonsInvolved" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="7" tab="allclaims"/>Persons Involved            
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/SupplementalFields" id="chkSupplementalFields2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="18" tab="allclaims"/>Supplemental Fields
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/AdjusterInformation" id="chkAdjusterInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="8" tab="allclaims"/>Adjuster Information            
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/Comments" id="chkComments2" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="19" tab="allclaims"/>Comments
            </td>
           </tr>
           <tr>
           <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/AdjusterDatedText" id="chkAdjusterDatedText"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="9" tab="allclaims"/>Adjuster Dated Text
           </td>
           </tr>
           <tr>           
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/EnteredUserFullName" id="chkEnteredUserFullName"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="10" tab="allclaims"/>Entered User Full Name
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/AllClaims/EnhancedNotes" id="chkEnhancedNotes2" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="20" tab="allclaims"/>Enhanced Notes
            </td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" width="100%" ALIGN="center" name="FORMTABgcclaims" id="FORMTABgcclaims" style="display:none;">
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantInformation" id="chkClaimantInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="1" tab="gcclaims"/>Claimant Information
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/AttorneyInformation" id="chkAttorneyInformation1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="2" tab="gcclaims"/>Attorney Information
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimInvolvementHistory" id="chkClaimInvolvementHistory3" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="3" tab="gcclaims"/>Claim Involvement History
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantLevelReserves" id="chkClaimantLevelReserves1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="4" tab="gcclaims"/>Claimant Level Reserves
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantPaymentHistory" id="chkCClaimantPaymentHistory" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="5" tab="gcclaims"/>Claimant Payment History
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantFundsManagement" id="chkClaimantFundsManagement1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="6" tab="gcclaims"/>Claimant Funds Management
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/TransactionDetail" id="chkTransactionDetail1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="7" tab="gcclaims"/>Transaction Detail
            </td>
           </tr>
            <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantUnitInfo" id="chkUnitInfo"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="8" tab="gcclaims"/>Claimant Unit Information
            </td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" width="100%" ALIGN="center" name="FORMTABvaclaims" id="FORMTABvaclaims" style="display:none;">
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantInformation" id="chkClaimantInformation1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="1" tab="vaclaims"/>Claimant Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/VehicleAccident" id="chkVehicleAccident" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="8" tab="vaclaims"/>Vehicle Accident
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/AttorneyInformation" id="chkAttorneyInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="2" tab="vaclaims"/>Attorney Information
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/UnitInformation" id="chkUnitInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="9" tab="vaclaims"/>Unit Information
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimInvolvementHistory" id="chkClaimInvolvementHistory4"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="3" tab="vaclaims"/>Claim Involvement History
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimInvolvementHistoryUnit" id="chkClaimInvolvementHistory5"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="10" tab="vaclaims"/>Claim Involvement History
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantLevelReserves" id="chkClaimantLevelReserves2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="4" tab="vaclaims"/>Claimant Level Reserves
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/UnitLevelReserves" id="chkUnitLevelReserves"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="11" tab="vaclaims"/>Unit Level Reserves
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantPaymentHistory" id="chkClaimantPaymentHistory"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="5" tab="vaclaims"/>Claimant Payment History
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/PaymentHistoryForUnit" id="chkPaymentHistoryForUnit"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="12" tab="vaclaims"/>Payment History for Unit
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/ClaimantFundsManagement" id="chkClaimantFundsManagement2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="6" tab="vaclaims"/>Claimant Funds Management
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/UnitLevelFunds" id="chkUnitLevelFunds" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="13" tab="vaclaims"/>Unit Level Funds
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/TransactionDetail" id="chkTransactionDetail2" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="7" tab="vaclaims"/>Transaction Detail
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/VAClaims/TransactionDetailUnitLevel" id="chkTransactionDetailUnit"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="3" index="14" tab="vaclaims"/>Transaction Detail
            </td>
           </tr>
            <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/GCClaims/ClaimantUnitInfo" id="chkUnitInfo1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="8" tab="gcclaims"/>Claimant Unit Information
            </td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABwcclaims" id="FORMTABwcclaims" style="display:none;">
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EmployeeInformation" id="chkEmployeeInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="1" tab="wcclaims"/>Employee Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/CaseManagementInformation" id="chkCaseManagementInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="10" tab="wcclaims"/>Case Management Information
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EmploymentInformation" id="chkEmploymentInformation"   onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="2" tab="wcclaims"/>Employment Information
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/CaseManagerInformation" id="chkCaseManagerInformation1"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="11" tab="wcclaims"/>Case Manager Information
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/DependentInformation" id="chkDependentInformation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="3" tab="wcclaims"/>Dependent Information
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/CaseManagerNotes" id="chkCaseManagerNotes2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="12" tab="wcclaims"/>Case Manager Notes
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/RestrictedDays" id="chkRestrictedDays2" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="4" tab="wcclaims"/>Restricted Days
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/RestrictedWork" id="chkRestrictedWork2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="13" tab="wcclaims"/>Restricted Work
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/LostDays" id="chkLostDays2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="5" tab="wcclaims"/>Lost Days
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/WorkLoss" id="chkWorkLoss2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="14" tab="wcclaims"/>Work Loss
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/EmployeeEventDetail" id="chkEmployeeEventDetail2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="6" tab="wcclaims"/>Employee Event Detail
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/TreatmentPlan" id="chkTreatmentPlan2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="15" tab="wcclaims"/>Treatment Plan
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/ClaimInvolvementHistory" id="chkClaimInvolvementHistory2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="7" tab="wcclaims"/>Claim Involvement History
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/MedicalManagementSavings" id="chkMedicalManagementSavings2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="16" tab="wcclaims"/>Medical Management Savings
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/OSHAInformation" id="chkOSHAInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="8" tab="wcclaims"/>OSHA Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/Accommodations" id="chkAccommodations2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="17" tab="wcclaims"/>Accommodations
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/JurisdictionalSupplementals" id="chkJurisdictionalSupplementals"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="9" tab="wcclaims"/>Jurisdictional Supplementals
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/WCClaims/VocationalRehabilitation" id="chkVocationalRehabilitation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="18" tab="wcclaims"/>Vocational Rehabilitation
            </td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" name="FORMTABdiclaims" id="FORMTABdiclaims" style="display:none;">
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/EmployeeInformation" id="chkEmployeeInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="1" tab="diclaims"/>Employee Information
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/CaseManagementInformation" id="chkCaseManagementInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="9" tab="diclaims"/>Case Management Information
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/EmploymentInformation" id="chkEmploymentInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="2" tab="diclaims"/>Employment Information
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/CaseManagerInformation" id="chkCaseManagerInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="10" tab="diclaims"/>Case Manager Information
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/DependentInformation" id="chkDependentInformation2"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="3" tab="diclaims"/>Dependent Information
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/CaseManagerNotes" id="chkCaseManagerNotes"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="11" tab="diclaims"/>Case Manager Notes
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/RestrictedDays" id="chkRestrictedDays"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="4" tab="diclaims"/>Restricted Days
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/RestrictedWork" id="chkRestrictedWork"   onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="12" tab="diclaims"/>Restricted Work
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/LostDays" id="chkLostDays"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="5" tab="diclaims"/>Lost Days
            </td>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/WorkLoss" id="chkWorkLoss"   onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="13" tab="diclaims" />Work Loss
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/EmployeeEventDetail" id="chkEmployeeEventDetail" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="6" tab="diclaims"/>Employee Event Detail
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/TreatmentPlan" id="chkTreatmentPlan" onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="14" tab="diclaims"/>Treatment Plan
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server" rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/LeaveInfo" id="chkLeaveInfo"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="2" index="7" tab="diclaims"/>Leave Info
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/ClaimInvolvementHistory" id="chkClaimInvolvementHistory"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="1" index="8" tab="diclaims"/>Claim Involvement History
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/MedicalManagementSavings" id="chkMedicalManagementSavings"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="15" tab="diclaims"/>Medical Management Savings
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/PlanInfo" id="chkPlanInfo"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="9" tab="diclaims"/>Plan Info
            </td>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/Accommodations" id="chkAccommodations"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="16" tab="diclaims"/>Accommodations
            </td>
           </tr>
           <tr>
            <td width="40%" valign="top"><asp:checkbox runat="server"  rmxref="/Instance/Document/ExecutiveSummaryConfig/DIClaims/VocationalRehabilitation" id="chkVocationalRehabilitation"  onchange="ApplyBool(this);" onclientclick="CheckboxClick()" depth="0" index="10" tab="diclaims"/>Vocational Rehabilitation
            </td>
           </tr>
          </table>
         </td>
        </tr>
       </table>
      </div>
     </td>
    </tr>
    <tr>
     <td>
      <%--<table border="0" cellspacing="0" cellpadding="0" width="30%" valign="top">
       <tr>
        <td>Available Users:</td>
        <td>Users To Be Configured:</td>
       </tr>
       <tr>
        <td valign="top"><asp:dropdownlist runat="server" id="cboAllUsersGroups" rmxtext="/Instance/ExecutiveSummaryConfig/Users/User/Name" rmxvalue="/Instance/ExecutiveSummaryConfig/Users/User/UserID"/>
          <asp:button UseSubmitBehavior ="false" runat="server" Text="Add" id="btnAddUser" class="button" onclientclick="if(!AddUserGroup()) return false;" onclick="AddUserGroup"/></td>
        <td><asp:listbox runat="server" id="lstUsersGroups" rmxretainvalue="true"/><asp:ImageButton runat="server" UseSubmitBehavior ="false" src="../../../Images/delete3.gif" width="20" height="20" border="0" alt="Delete" title="Remove" OnClientClick = "if(!DeleteUserGroup()) return false;" OnClick="DeleteUserGroup" /></td>
       </tr>
      </table>--%>
     </td>
    </tr>
    <tr>
     <td>
      <table border="0" cellspacing="0" cellpadding="0" width="80%" valign="top">
       <tr>
        <td>
         <div class="small">
          												Use this page to select the information you want to see in Executive Summary Reports.
          												<br/>
          												Once you have made all of your selections, click the "Save Configuration" button to store the settings for use.																
          											
         </div>
        </td>
       </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="0" width="30%" valign="top">
       <tr>
        <td><asp:button runat="server" UseSubmitBehavior="false" Text="Save Configuration" id="btnSaveConfiguration" class="button" onclientclick="if(! Save()) return false; " OnClick="Save"/></td>
        <td><asp:button runat="server" Text="Select All" id="btnSelectAll" class="button" onclientclick="return SelectAll();; "/></td>
        <td><asp:button runat="server" Text="Unselect All" id="btnUnselectAll" class="button" onclientclick="return UnselectAll();; "/></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
    <asp:textbox style="display: none" runat="server" id="hiddenSelectedUser" rmxretainvalue="true" rmxref="/Instance/Document/ExecutiveSummaryConfig/SelectedUser"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="IsAdminUser" rmxretainvalue="true" rmxref="/Instance/Document/ExecutiveSummaryConfig/IsAdminUser"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="hTabName" rmxref="/Instance/Document/ExecutiveSummaryConfig/SelectedTab"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="functiontocall" rmxref="/Instance/Document/ExecutiveSummaryConfig/FunctionToCall"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="UserId" rmxref="/Instance/Document/ExecutiveSummaryConfig/UserId"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="UserIDs" rmxref="/Instance/Document/ExecutiveSummaryConfig/UserIDs"
                 Text=""   rmxtype="hidden" />
    </form>
</body>
</html>
