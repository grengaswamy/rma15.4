﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExecSummary.aspx.cs" Inherits="Riskmaster.UI.Executivesummary.ExecSummary" EnableViewState="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Executive summary</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <%--color to be made red here--%>
    <%--<asp:textbox style="display: none;" runat="server" id="error" rmxref="Instance/Document/ExecutiveSummaryReport/EventId" Text="15" rmxtype="hidden" />--%>
    </div>
	<%--controls changed by Nitin for Mits 15312 on 16-Apr-2009 --%>
	<asp:textbox style="display: none;" runat="server" id="txtRecordId" Text="" rmxtype="hidden" />
	<asp:TextBox style="display: none;"  runat="server" ID="txtTableName"  Text="" rmxtype="hidden" />
	<asp:textbox style="display: none;" runat="server" id="File" rmxref="Instance/Document/ExecutiveSummaryReport/File" Text="" rmxtype="hidden" />
    <div>
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <div id="divChoices" runat="server">
            <table>
            <tr>
                <td><asp:Label runat="server" id="txtLabel" class="required" Text="<%$ Resources:lblText %>" ></asp:Label></td>
        
            </tr>
            <tr>
                <td><asp:RadioButton GroupName="Choices" runat="server" Text="<%$ Resources:rdr1 %>" id="rdr1" rmxref="/Instance/Document/ExecutiveSummaryReport/SelectedChoiceThreshholdExceeded" value="2"/></td>
            </tr>
            <!-- 11/3/2011 MITS 26441 Raman Bhatia: Now REM DOES NOT want this choice to be displayed!!! I am leaving all 
            code written for this choice in there.. in future if we need this option then just uncomment this section-->
            <!--
            <tr>
            <td><asp:RadioButton GroupName="Choices" runat="server" Text="Proceed with executive summary generation. (not recommended)" id="rdr2" rmxref="/Instance/Document/ExecutiveSummaryReport/SelectedChoiceThreshholdExceeded" value="1"/></td>
            </tr>
            -->
            <tr>
            <td><asp:RadioButton GroupName="Choices" runat="server" Text="<%$ Resources:rdr3 %>" id="rdr3" rmxref="/Instance/Document/ExecutiveSummaryReport/SelectedChoiceThreshholdExceeded" value="0"/>
            </td>
            </tr>
            
            
            </table>
            <asp:Button id="btnChoices" runat="server" Text="<%$ Resources:btnChoices %>" UseSubmitBehavior="false"/>
        </div>
        
    </div>
    </form>
</body>
</html>
