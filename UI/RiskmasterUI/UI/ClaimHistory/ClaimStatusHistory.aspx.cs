﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.ClaimHistory
{
    public partial class ClaimStatusHistory : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                string sCurstatus="";
                string sCurdate = "";
                string sCurapprovedby = "";
                string sCurreason = "";
                XmlDocument XmlDoc = new XmlDocument();
                DataTable objTable = new DataTable();
                DataTable ddTable = new DataTable();
                XmlNode xNode;
                DataRow objRow;
                //Start - Yukti-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                if (!IsPostBack)
                { 
                    
                    claimid.Text = Request.QueryString["claimid"];
                    triggerdate.Text  = Request.QueryString["triggerdate"];
                    sCurstatus = Request.QueryString["curstatus"];
                    sCurdate = Request.QueryString["curdate"];
                    sCurapprovedby = Request.QueryString["curapprovedby"];
                    sCurreason = Request.QueryString["curreason"];
                   
                     XmlTemplate = GetMessageTemplate(claimid.Text, triggerdate.Text, sCurstatus );
                    bReturnStatus = CallCWSFunction("ClaimHistoryAdaptor.GetClaimStatusHistory", out sreturnValue, XmlTemplate);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);
                        //Added by Shivendu for MITS 16148
                        //nadim-17282-start
                        if (XmlDoc.SelectSingleNode("//ClaimStatusHistory/ClaimantName").InnerText == null || XmlDoc.SelectSingleNode("//ClaimStatusHistory/ClaimantName").InnerText == "")
                        {
                            lblClaimStatusHistTitle.Text = "Claim Status History [ " + XmlDoc.SelectSingleNode("//ClaimStatusHistory/ClaimNumber").InnerText + " ]";
                        }
                        else
                        {
                            
                            lblClaimStatusHistTitle.Text = "Claim Status History [ " + XmlDoc.SelectSingleNode("//ClaimStatusHistory/ClaimNumber").InnerText
                                                           + "*" + XmlDoc.SelectSingleNode("//ClaimStatusHistory/ClaimantName").InnerText + "* ]";
                        }
                        //nadim-17282-end
                        changedby.Text = XmlDoc.SelectSingleNode("//ClaimStatusHistory/ChangedBy").InnerText;
                        XmlNodeList statustypelist = XmlDoc.SelectNodes("//ClaimStatusHistory/StatusType/level");
                        foreach (XmlNode xStatusCode in statustypelist)
                        {
                            searchtype.Items.Add(new ListItem(xStatusCode.SelectSingleNode("@name").Value, xStatusCode.SelectSingleNode("@id").Value ));
                        }
                        
                        XmlNodeList statuslist = XmlDoc.SelectNodes("//ClaimStatusHistory/Row");
                        objTable.Columns.Add("Status");
                        objTable.Columns.Add("Date");
                        objTable.Columns.Add("ChangedBy");
                        objTable.Columns.Add("Reason");
                        for (int i = 0; i < statuslist.Count; i++)
                        {
                            xNode = statuslist[i];
                            objRow = objTable.NewRow();
                            objRow["Status"] = xNode.ChildNodes[0].InnerText;
                            objRow["Date"] = xNode.ChildNodes[1].InnerText;
                            objRow["ChangedBy"] = xNode.ChildNodes[2].InnerText;
                            objRow["Reason"] = xNode.ChildNodes[3].InnerText;
                            objTable.Rows.Add(objRow);

                        }
                        objTable.AcceptChanges();
                        GridViewClmStatus.Visible = true;
                        GridViewClmStatus.DataSource = objTable;
                        GridViewClmStatus.DataBind();

                    }
					//gmallick MITS 12965 Start
                    string upateflagval = "";

                   
                    upateflagval = Request.QueryString["updateflag"];

                    if(upateflagval == "false")
                    {
                        DropDownList searchtyp = this.Form.FindControl("searchtype") as DropDownList;
                        TextBox changedate = this.Form.FindControl("changedate") as TextBox;
                       // Button changedatebutton = this.Form.FindControl("changedatebtn") as Button;
                        TextBox changedbyuser = this.Form.FindControl("changedby") as TextBox;
                        TextBox approvedby = this.Form.FindControl("approvedby") as TextBox;
                        TextBox reason = this.Form.FindControl("reason") as TextBox;
                        Button btnOK = this.Form.FindControl("btnOK") as Button;

                        divChangeDate.Visible = false;   //gmallick JIRA 12965  
                        if (searchtyp != null)
                        {
                            searchtyp.Enabled = false;
                            searchtyp.Attributes.Add("disabled", "true");
                        }
                        if (changedate != null)
                        {
                            changedate.Enabled = false;
                            changedate.Attributes.Add("disabled", "true");
                        }
                        
                        if (changedbyuser != null)
                        {
                            changedbyuser.Enabled = false;
                            changedbyuser.Attributes.Add("disabled", "true");
                        }
                        if (approvedby != null)
                        {
                            approvedby.Enabled = false;
                            approvedby.Attributes.Add("disabled", "true");
                        }
                        if (reason != null)
                        {
                            reason.Enabled = false;
                            reason.Attributes.Add("disabled", "true");
                        }
                        if (btnOK != null)
                        {
                            btnOK.Enabled = false;
                            btnOK.Attributes.Add("disabled", "true");
                        }
                   }
                    //gmallick MITS 12965 End
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate(string sClaimId, string sTriggerDate,string sCodeId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimStatusHistory><ClaimNumber/><ClaimantName/><ClaimId>");
            sXml = sXml.Append(sClaimId);
            sXml = sXml.Append("</ClaimId><CodeId>");
            sXml = sXml.Append(sCodeId);
            sXml = sXml.Append("</CodeId><ChangedBy /><ApprovedBy /><Reason /><StatusType /><ChangeDate /><triggerdate>");
            sXml = sXml.Append(sTriggerDate);
            sXml = sXml.Append("</triggerdate></ClaimStatusHistory></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
