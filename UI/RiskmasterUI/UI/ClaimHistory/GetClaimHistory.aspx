﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetClaimHistory.aspx.cs" Inherits="Riskmaster.UI.ClaimHistory.GetClaimHistory" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"/>
    <title>Claim Involvement History</title>
    
</head>
<body>
			
    <form id="frmData" runat="server">
    <div>
        <asp:textbox ID="eventnumber" runat="server" style="display:none" rmxref="/Instance/Document/ClaimHistory/option/EventNumber"></asp:textbox>
        <asp:textbox ID="SubTitle" runat="server" style="display:none" rmxref="/Instance/Document/ClaimHistory/@SubTitle"></asp:textbox>
        <%--Yukti-ML Changes Start,MITS 34089--%>
        <%--<div class="formtitle" id="formtitle">Claim Involvement History</div> --%>
        <div class="formtitle" id="formtitle"><asp:Label runat="server" ID="lblClaimInvHist" Text="<%$ Resources:lblClaimInvHist %>" /></div>
        <%--Yukti-ML Changes End--%>
		<div class="formsubtitle"><%=SubTitle.Text %></div> 
		<br />
		<table border="1" width="100%" cellspacing="0" cellpadding="2">
							<tr>
                            <%--Yukti-ML Changes Start,MITS 34089--%>
								<%--<td class="ctrlgroup">Claim Number</td>
								<td class="ctrlgroup" nowrap="1">Claim Date</td>
								<td class="ctrlgroup" nowrap="1">Line of Business</td>
								<td class="ctrlgroup" nowrap="1">Claim Type</td>
								<td class="ctrlgroup" nowrap="1">Claim Status</td>
								<td class="ctrlgroup" nowrap="1">Date/Time Closed</td>--%>

                                <td class="ctrlgroup"><asp:Label runat="server" ID="lblClaimNum" Text="<%$ Resources:lblClaimNum %>"/></td>
								<td class="ctrlgroup" nowrap="1"><asp:Label runat="server" ID="lblClaimDt" Text="<%$ Resources:lblClaimDt %>"/></td>
								<td class="ctrlgroup" nowrap="1"><asp:Label runat="server" ID="lblLOB" Text="<%$ Resources:lblLOB %>"/></td>
								<td class="ctrlgroup" nowrap="1"><asp:Label runat="server" ID="lblClaimType" Text="<%$ Resources:lblClaimType %>"/></td>
								<td class="ctrlgroup" nowrap="1"><asp:Label runat="server" ID="lblClaimStatus" Text="<%$ Resources:lblClaimStatus %>"/></td>
								<td class="ctrlgroup" nowrap="1"><asp:Label runat="server" ID="lblDtTimeClosed" Text="<%$ Resources:lblDtTimeClosed %>"/></td>
                            <%--Yukti-ML Changes End--%>
								
							</tr>
							<% int i = 0; foreach (XElement item in result)
                            {
                                string rowclass = "";
                                if ((i % 2) == 1) rowclass = "datatd";
                                else rowclass = "datatd1";
                                i++;%>
							
								<tr>
									
									<td class="<%=rowclass %>"><%
                                lnkClaimant.ID = "lnkClaimant" + i;
                                lnkClaimant.Text = item.Element("ClaimNumber").Value;
                                lnkClaimant.OnClientClick = "window.opener.parent.MDIShowScreen(" + item.Element("ClaimId").Value + ",'claim');window.close();return false;";%>
                              
                             <asp:LinkButton runat="server" id="lnkClaimant"></asp:LinkButton>  
                             <%--Start:code commented and new added for displaying property info Neha Suresh Jain--%>
										
									</td>
									<td class="<%=rowclass %>">
										<%= AppHelper.GetUIDate(item.Element("DateOfClaim").Value)%>
									</td>
									<td class="<%=rowclass %>">
									    <%=item.Element("LineOfBusCode").Value%>
									</td>
									<td class="<%=rowclass %>">
									    <%=item.Element("ClaimTypeCode").Value%>
									</td>
									<td class="<%=rowclass %>">
									    <%=item.Element("ClaimStatusCode").Value%>
									</td>
									<td class="<%=rowclass %>">
										<%=  AppHelper.GetUIDatetTime(item.Element("DttmClosed").Value)%>
									</td>
								</tr>
                                <%} %>
								 
						</table>
						<br/>
                        <%--Yukti-ML Changes Start,MITS 34089--%>
					<%--	<input class="button" type="button" id="btnCancel" value="Cancel" onclick="window.close();"/>&#160;&#160;&#160;&#160;
						<input class="button" type="button" id="btnPrint" value="Print" onclick="window.print();"/>--%>
                        <asp:Button runat="server" ID="btnCancel" Text="<%$ Resources:btnCancel %>" OnClientClick="window.close();" />&#160;&#160;&#160;&#160;
                        <asp:Button runat="server" ID="btnPrint" Text="<%$ Resources:btnPrint %>" OnClientClick="window.print();" />
                        <%--Yukti-ML Changes End--%>

						<!-- Dummy nodes for form.js -->
						<input style="display:none" id="SysFormIdName" />
						<input style="display:none" id="SysFormPIdName" />
						<input style="display:none" id="SysFormName" />
						
    </div>
    </form>
</body>
</html>
