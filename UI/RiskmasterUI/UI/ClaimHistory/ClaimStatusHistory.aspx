﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimStatusHistory.aspx.cs" Inherits="Riskmaster.UI.ClaimHistory.ClaimStatusHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Claim Status History</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/form.js">        { var i; } </script>
    
    <script type="text/javascript">
     function fillDate()
            {
              //Mgaba2:Mits 10241: Current Date shouls be taken from server:Start
              //var today=new Date();
              //document.getElementById('changedate').value = today.getMonth() + 1 + "/" + today.getDate() + "/" + (today.getYear());-->
              document.getElementById('changedate').value=window.opener.document.getElementById('systemcurrentdate').value;
              //Mgaba2:Mits 10241:End 

            }

            function Print()
            {
            //Defect 001259 Fixed by Neelima Jan 2006
            //Not a good way
            document.getElementById('changedate').parentElement.parentElement.style.display = "none";
            document.getElementById('changedby').parentElement.parentElement.style.display = "none";
            document.getElementById('reason').parentElement.parentElement.style.display = "none";
            document.getElementById('approvedby').parentElement.parentElement.style.display = "none";
            document.getElementById('searchtype').parentElement.parentElement.style.display = "none";
            document.getElementById('btnOK').style.display = "none";
            document.getElementById('btnCancel').style.display = "none";
            document.getElementById('btnPrint').style.display = "none";
            self.print();
            document.getElementById('changedate').parentElement.parentElement.style.display = "";
            document.getElementById('changedby').parentElement.parentElement.style.display = "";
            document.getElementById('reason').parentElement.parentElement.style.display = "";
            document.getElementById('approvedby').parentElement.parentElement.style.display = "";
            document.getElementById('searchtype').parentElement.parentElement.style.display = "";
            document.getElementById('btnOK').style.display = "";
            document.getElementById('btnCancel').style.display = "";
            document.getElementById('btnPrint').style.display = "";
            return false;
            }

            function SaveValues()
            {
            var closeClaim="";
            var objCtrlAllCodes;
            var element=document.getElementById('searchtype');
            if (element.options[element.selectedIndex].value=="0")
            {
                alert('Please select some Claim Status');
                return false;
            }
            window.opener.document.getElementById('statuschangeapprovedby').value = document.getElementById('approvedby').value;
                //33144 - nkaranam2
            if (window.opener.document.getElementById('dttmclosed') != null)
            window.opener.document.getElementById('dttmclosed').value = document.getElementById('changedate').value;
            // ABhateja, 06.29.2007
            // MITS 9946, Make sure that 'statuschangedate' gets updated.
            window.opener.document.getElementById('statuschangedate').value = document.getElementById('changedate').value;
            window.opener.document.getElementById('statuschangereason').value = document.getElementById('reason').value;
            window.opener.document.getElementById('claimstatuscode_codelookup').value = document.getElementById('searchtype').options[document.getElementById('searchtype').selectedIndex].text;
            window.opener.document.getElementById('claimstatuscode_codelookup_cid').value = document.getElementById('searchtype').value;

            //MITS 10241:Applying Module Security:Allow Backdating in Claim Status History-Start
            //BackdateClaimSetting is 0 if backdating is not allowed
            //if (window.opener.document.getElementById('BackdateClaimSetting') != null) { Jira : 9130
                if (window.opener.document.getElementById('BackdateClaimSetting') != null && window.opener.document.getElementById('BackdateClaimSetting').value == "0") {
                objcurdate = new Date(window.opener.document.getElementById('systemcurrentdate').value);
                objchangedate = new Date(window.opener.document.getElementById('statuschangedate').value);
                datediff = new Date();
                datediff = objcurdate - objchangedate;
                if (datediff > 0) {
                    alert("Changed Date " + window.opener.document.getElementById('statuschangedate').value + " is not permitted to be lesser than Today's Date " + window.opener.document.getElementById('systemcurrentdate').value);
                    document.forms[0].changedate.focus();
                    return false;
                    }
                }
            
            //MITS 10241-End            
            
            objCtrlAllCodes=window.opener.document.getElementById('AllCodes');
            if (objCtrlAllCodes!=null)
            {
              arrCodes=objCtrlAllCodes.value.split(',');
              for(i=0;i<arrCodes.length-1;i++)
				  {
						  if (arrCodes[i]==document.getElementById('searchtype').value)
						  {
							  closeClaim="y";
							  break;
						  }
					  }
				  }
			  if (closeClaim=="y")
				  window.opener.setClaimClosedDate();
			  else
				  window.opener.clearClaimClosedDate();
			  window.opener.EnableCloseMethod();
			  window.opener.setDataChanged(true);
			  window.close();
			  return false;
        }
       
    </script>
    <%--gmallick - RMA-12965 - Starts --%>
    <style type="text/css">
        .sameline {
    display: inline;
}
    </style>
    <%--gmallick - RMA-12965 - End --%>
</head>
<body onload="fillDate()">
    <form id="frmData" runat="server">
    <asp:TextBox ID="claimid" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="triggerdate" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="curParams" runat="server" style="display:none"></asp:TextBox>
    <asp:scriptmanager id="SMgr" runat="server" />
     <table width="100%" cellspacing="0" cellpadding="0">
		<tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr>
		<tr class="msgheader">
		<!-- Label control added by Shivendu for MITS 16148 -->
		    <td colspan="2"><asp:Label ID="lblClaimStatusHistTitle" runat="server" Text="Label"></asp:Label></td>
		</tr>
	</table>
	<br/>
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td>	
			    <asp:label runat="server" class="label" id="lbl_status" text="Status:" />
			</td>
			<td>
				<asp:DropDownList ID="searchtype" runat="server" rmxref="Instance/Document/ClaimStatusHistory/StatusType"></asp:DropDownList>
 			</td> 
		</tr>
		<tr>
		    <td><asp:label runat="server" class="label" id="lbl_changedate" text="Date: " /></td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" id="changedate" RMXRef="Instance/Document/ClaimStatusHistory/ChangeDate" RMXType="date"  onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                <%--gmallick - RMA-12965 - Starts --%>
                 <div id="divChangeDate" runat="server" class="sameline">
               <%-- <input type="button" class="DateLookupControl" name="changedatebtn" />                 
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					    {
					        inputField: "changedate",
					        ifFormat: "%m/%d/%Y",
					        button: "changedatebtn"
					    }
					    );
			    </script>--%>

                <script type="text/javascript">
                    $(function () {
                        $("#changedate").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                    });
                    </script>
                      </div>
                    <%--Yukti-Ml Changes End--%>
				 <%--gmallick - RMA-12965 - End --%>
					
			</td> 
		</tr>

		<tr>
		    <td><asp:label runat="server" class="label" id="lbl_changedby" text="Changed By: " /></td>
			<td>
			    <asp:textbox runat="server" id="changedby" rmxref="Instance/Document/ClaimStatusHistory/ChangedBy"  disabled="True"/>				
			</td>
		</tr>
		<tr>
		    <td><asp:label runat="server" class="label" id="lbl_approvedby" text="Approved By: " /></td>
			<td>
			    <asp:textbox runat="server" id="approvedby" rmxref="Instance/Document/ClaimStatusHistory/ApprovedBy"  />				
			</td>
		</tr>
		<tr>
		    <td><asp:label runat="server" class="label" id="lbl_reason" text="Reason: " /></td>
			<td>
			    <asp:textbox runat="server" id="reason" rmxref="Instance/Document/ClaimStatusHistory/Reason"  />				
			</td>
		</tr>
    </table> 
    <br/>
    <div class="divScroll" style="width: 100%; height: 42%;">
    <table width="100%" cellspacing="0" cellpadding="0">
		<tr class="ctrlgroup">
		    <asp:GridView ID="GridViewClmStatus" AutoGenerateColumns="False" runat="server" Font-Bold="True" 
		    CellPadding="0" GridLines="None" CellSpacing="0" Width="100%"  EmptyDataText ="No Status History Available" >
		    <HeaderStyle CssClass="ctrlgroup"/>
            <rowstyle CssClass ="datatd" HorizontalAlign="Left" Font-Bold="false"  />
            <alternatingrowstyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />

                <Columns>
                    <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("Status")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("Date")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Changed By" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("ChangedBy")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reason" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("Reason")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns> 
            </asp:GridView>     
		</tr>
	</table> 
    </div>
    
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
			    <asp:Button  class="button" id="btnOK" Text =" OK " runat="server" CssClass="button" UseSubmitBehavior="false" OnClientClick ="javascript: SaveValues();return false;" Width ="50px" />
				&#160;
				<asp:Button  class="button" id="btnCancel" Text =" Cancel " runat="server" OnClientClick ="window.close();" Width ="50px" />
				&#160;
				<asp:Button  class="button" id="btnPrint" Text =" Print " runat="server" OnClientClick ="Print();return false;" Width ="50px" />
                <asp:TextBox runat="server" id="hdnCurrentDate" Style="display: none"></asp:TextBox>
			</td>
		</tr>
	</table>
    </form>
</body>
</html>
