﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Data;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.AutoChecks
{
    public partial class SelectBatch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            XElement objXml = null;
            XElement objAutoCheckBatchCriteria = null;
            XElement objTempElement = null;
            
            if (!IsPostBack)
            {

                objAutoCheckBatchCriteria = GetMessageTemplate();

                if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
                {
                    objTempElement = objAutoCheckBatchCriteria.XPathSelectElement("./Authorization");
                    objTempElement.Value = AppHelper.ReadCookieValue("SessionId");
                }

                objTempElement = objAutoCheckBatchCriteria.XPathSelectElement("./Document/GetAllAutoChecksBatch/ClaimID");
                if (Request.QueryString["ClaimID"] != null)
                {
                    objTempElement.Value = Request.QueryString["ClaimID"];
                }

                // Call CWS and get the result xml.
                objXml = GetAutoCheckBatches(objAutoCheckBatchCriteria);

                BindGridToXml(objXml);
            }


        }

        private XElement GetAutoCheckBatches(XElement p_objAutoCheckBatchesIn)
        {
            XmlDocument objXmlCriteriaDom = new XmlDocument();
            objXmlCriteriaDom.LoadXml(p_objAutoCheckBatchesIn.ToString());

            string sReturn = AppHelper.CallCWSService(objXmlCriteriaDom.InnerXml.ToString());

            //Binding Error Control
            ErrorControl.errorDom = sReturn;

            objXmlCriteriaDom.LoadXml(sReturn);
            
            XmlNode oInstanceNode = objXmlCriteriaDom.SelectSingleNode("/ResultMessage/Document");

            XElement objAutoCheckBatchesOut = XElement.Parse(oInstanceNode.InnerXml.ToString());

            return objAutoCheckBatchesOut;
        }

        private void BindGridToXml(XElement p_objAutoChecksBatchXml)
        {
            DataRow drAutoCheckBatchRow = null;
            DataTable dtAutoCheckBatch = new DataTable();
            int iPageSize = 0;

            var objAutoCheckBatchXml = from cols in p_objAutoChecksBatchXml.Descendants("AutoCheckBatch")

                                    select cols;

            if (objAutoCheckBatchXml.Count() != 0)
            {
                iPageSize = objAutoCheckBatchXml.Count();

                XElement objHeaderRow = objAutoCheckBatchXml.First();

                for (int iCount = 0; iCount < objHeaderRow.Elements().Count(); iCount++)
                {
                    dtAutoCheckBatch.Columns.Add(new DataColumn(objHeaderRow.Elements().ElementAt(iCount).Name.ToString()));
                }

                foreach (XElement autochecksbatch in objAutoCheckBatchXml)
                {
                    drAutoCheckBatchRow = dtAutoCheckBatch.NewRow();

                    for (int iCount = 0; iCount < autochecksbatch.Elements().Count(); iCount++)
                    {
                        drAutoCheckBatchRow[objHeaderRow.Elements().ElementAt(iCount).Name.ToString()] = autochecksbatch.Elements().ElementAt(iCount).Value;
                    }
                    dtAutoCheckBatch.Rows.Add(drAutoCheckBatchRow);
                }
            }
            else
            {
                lblNoRecord.InnerText = "There are no records to show.";
                return;
            }

            // Bind DataTable to GridView.
            grdAutoChecksBatch.Visible = true;
            grdAutoChecksBatch.PageSize = iPageSize;
            grdAutoChecksBatch.DataSource = dtAutoCheckBatch;
            grdAutoChecksBatch.DataBind();
        }

        protected void grdAutoChecksBatch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string sBatchNumber = e.Row.Cells[0].Text;
                    e.Row.Cells[0].Text = "<a href='#'>" + sBatchNumber + "</a>";
                    e.Row.Cells[0].Attributes["onclick"] = String.Format("MoveToAutoBatchId({0})", sBatchNumber);

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    else
                    {
                        e.Row.CssClass = "rowlight2";
                    }

                    for (int i = 0; i < e.Row.Cells.Count; i++)
                        e.Row.Cells[i].Wrap = false;
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    foreach (TableCell cell in e.Row.Cells)
                    {
                        cell.Wrap = false;
                    }
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                    <Call>
                        <Function>FundManagementAdaptor.GetAllAutoChecksBatch</Function> 
                    </Call>
                    <Document>
                        <GetAllAutoChecksBatch>
                            <ClaimID></ClaimID> 
                        </GetAllAutoChecksBatch>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }
    }
}
