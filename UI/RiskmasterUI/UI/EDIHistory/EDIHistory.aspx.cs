﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.EDIHistory
{
    /// <summary>
    /// EDI History Module
    /// Coded by : Akashyap3
    /// 21-Apr-2009
    /// </summary>
    public partial class EDIHistory : NonFDMBasePageCWS
    {
        #region Private Variables

        /// <summary>
        /// Returned response string from CallCWS
        /// </summary>
        string _strResponse = "";

        /// <summary>
        /// The Status of the Web Server Hit
        /// </summary>
        bool bReturnStatus = false;


        /// <summary>
        /// XML representation of _strResponse
        /// </summary>
        XmlDocument oResponse = null;

        /// <summary>
        /// The XMLin for the Function is created using the StringBuilder
        /// </summary>
        StringBuilder sbRequest = null;

        /// <summary>
        /// The Row Object for creating the Rows out of EDIHistory Data and bind to Grid
        /// </summary>
        DataRow objRow;

        #endregion

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            _strResponse = "";

            try
            {
                if (!Page.IsPostBack || !Page.IsCallback)
                {
                    // Get the Value of ClaimId
                    string sClaimId = AppHelper.GetQueryStringValue("ClaimId");

                    //Preparing XML to send to service
                    XElement oMessageElement = GetMessageTemplate(sClaimId);

                    // Call the CWS service to Get the EDI History
                    bReturnStatus = CallCWS("ClaimHistoryAdaptor.GetEDIHistory", oMessageElement, out _strResponse, false, false);

                    if (bReturnStatus)
                    {

                        oResponse = new XmlDocument();
                        //parse CallCWS response to bind it with the Grid
                        oResponse.LoadXml(_strResponse);

                        DataTable dtGridData = new DataTable();

                        // Create the columns for in the DataColumn
                        dtGridData.Columns.Add("Ak1Date");
                        dtGridData.Columns.Add("Ak1DocId");
                        dtGridData.Columns.Add("AddedByUser");
                        dtGridData.Columns.Add("AttachmentId");
                        dtGridData.Columns.Add("ClaimId");
                        dtGridData.Columns.Add("ClaimNumber");
                        dtGridData.Columns.Add("EDIRowId");
                        dtGridData.Columns.Add("FilingDate");
                        dtGridData.Columns.Add("FilingMTC");
                        dtGridData.Columns.Add("FilingStatus");
                        dtGridData.Columns.Add("FilingType");
                        dtGridData.Columns.Add("Jurisdiction");
                        dtGridData.Columns.Add("LateReason");
                        dtGridData.Columns.Add("MTCCorrection");
                        dtGridData.Columns.Add("MTCCorrectDate");
                        dtGridData.Columns.Add("SuspensionEffDate");
                        dtGridData.Columns.Add("SuspensionNarr");

                        XmlNodeList ediHistoryData = oResponse.SelectNodes("//EDIHistory/option");

                        foreach (XmlNode objNodes in ediHistoryData)
                        {
                            objRow = dtGridData.NewRow();
                            for (int i = 0; i < objNodes.ChildNodes.Count; i++)
                            {
                                objRow[i] = objNodes.ChildNodes[i].InnerText;
                            }
                            dtGridData.Rows.Add(objRow);
                        }

                        objRow = dtGridData.NewRow();
                        dtGridData.Rows.Add(objRow);

                        gvEDIHistory.DataSource = dtGridData;

                        gvEDIHistory.DataBind();

                    }


                    // Pass the XElement oResponse to BindData2ErrorControl
                    BindData2ErrorControl(_strResponse);

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        #endregion

        # region Grid Data Bound

        protected void gvEDIHistory_DataBound(object sender, EventArgs e)
        {
            gvEDIHistory.Rows[gvEDIHistory.Rows.Count - 1].CssClass = "hiderowcol";
        }
        # endregion

        #region GetMessageTemplate()
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate(string p_sClaimId)
        {
            sbRequest = new StringBuilder();
            sbRequest.Append(@"<Message>
                                <Authorization>d484b0b4-11bd-4281-835f-63d9a527422d</Authorization>
                                <Call>
                                    <Function>ClaimHistoryAdaptor.GetEDIHistory</Function>
                                </Call>
                                <Document>
                                    <EDIHistory>
                                        <ClaimId>");
            sbRequest.Append(p_sClaimId);
            sbRequest.Append(@" </ClaimId>
                               </EDIHistory>
                            </Document>
                        </Message>");
            XElement oTemplate = XElement.Parse(sbRequest.ToString());
            
            return oTemplate;
        }
        #endregion

    }
}
