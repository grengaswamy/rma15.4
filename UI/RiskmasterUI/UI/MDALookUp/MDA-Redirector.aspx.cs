﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;

namespace Riskmaster.UI.UI.MDALookUp
{
    public partial class MDA_Redirector : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                string sClaimID = "";
                XmlDocument XmlDoc = new XmlDocument();
                XmlNode xNode = null;
                XmlNode xDiagcode = null;
                DataRow objRow;

                if (!IsPostBack)
                {
                    txtMedicalCode.Text = AppHelper.GetQueryStringValue("medicalcode");
                    txtJobID.Text = AppHelper.GetQueryStringValue("jobclassid");
                    MDABusinessHelper helper = new MDABusinessHelper();
                    string retData = helper.GetMDATopics(txtMedicalCode.Text, txtJobID.Text);
                    XmlDoc.LoadXml(retData);
                    SetMDAMessage(XmlDoc);

                    if (XmlDoc.SelectSingleNode("//RelatedMonographUrls/RelatedMonographUrl/MonographUrl") != null)
                    {
                        string mdaURL = XmlDoc.SelectSingleNode("//RelatedMonographUrls/RelatedMonographUrl/MonographUrl").InnerText;
                        Response.Redirect(mdaURL);
                       
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private void SetMDAMessage(XmlDocument doc)
        {
            string message = "";
            if (doc != null)
            {
                if (doc.SelectSingleNode("//TransactionMessage/Message") != null)
                {
                    message = doc.SelectSingleNode("//TransactionMessage/Message").InnerText;
                    if (message.ToLower().Equals("license is current."))
                    {
                        message = "";
                    }
                }
            }
            if (!message.Equals(""))
            {
                ErrorControl1.Text = ErrorHelper.FormatErrorsForUI(message,true);
            }
        }
    }
}
