﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentList.aspx.cs" Inherits="Riskmaster.UI.PaperVision.DocumentList" %>

<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Document Management</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script language="javascript" type="text/javascript">
                  
					
					
					function CheckF(ctrl) {

					    getCheckedDocFolders();
					  
					}
					function CheckD(ctrl) {

					    getCheckedDocFolders();
					   
					}
					
				
					
					function delCheck() {
					    
						var fId = document.forms[0].hfolderId.value;
						var dId = document.forms[0].hdocId.value;	
						if (fId=='')
						{
							if (dId=='')
							{
								alert("Please select files and/or folders to delete.");
								return false;
							}
							if (dId=='')
							{
								alert("Please select files and/or folders to delete.");
								return false;
							}
						}						

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
									alert("LSS document(s) can not be deleted from RMX. Please delete it from LSS.");
									return false;								
								}
							}
						}	
					    
					    
					    return true;
											
					}
					
					function copyCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
							alert("Please select files you would like to copy.");
							return false;
						}
						if (dId=='')
						{
							alert("Please select files you would like to copy.");
							return false;
						}
					
						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
									alert("LSS document(s) can not be copied in RMX.");
									return false;								
								}
							}
						}
							 var screen='CopyDocument.aspx';
                         document.forms[0].action=screen+ "?Psid="+document.all.Psid.value+"&operation=c";
                        document.forms[0].method="post";
                        document.forms[0].submit();
					}
					
					function transferCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
							alert("Please select files you would like to transfer.");
							return false;
						}
						if (dId=='')
						{
							alert("Please select files you would like to transfer.");
							return false;
						}

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
									alert("LSS document(s) can not be transferred in RMX.");
									return false;								
								}
							}
						}
						
						 var screen='TransferDocument.aspx';
                         document.forms[0].action=screen+ "?Psid="+document.all.Psid.value+"&operation=t";
                        document.forms[0].method="post";
                        document.forms[0].submit();
					}
					
					function emailCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
							alert("Please select files you would like to e-mail.");
							return false;
						}
						if (dId=='')
						{
							alert("Please select files you would like to e-mail.");
							return false;
						}

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							if(objElem.checked==true)
							{
								if(objElem.docinternaltype=='3')
								{
									alert("LSS document(s) can not be e-mailed in RMX.");
									return false;								
								}
							}
						}
						   var screen='EmailDocuments.aspx';
                           document.forms[0].action=screen+ "?Psid="+document.all.Psid.value+"&operation=t";
                           document.forms[0].method="post";
                           document.forms[0].submit();
					}
					
					function moveCheck()
					{
						var dId = document.forms[0].hdocId.value;										
						if (dId=='0')
						{
							alert("Please select files you would like to move.");
							return false;
						}
						if (dId=='')
						{
							alert("Please select files you would like to move.");
							return false;
						}

						//Manoj - LSS Documents
						for (var i=0; i < document.forms[0].length; i++)
          {
          var objElem = document.forms[0].elements[i];
          if(objElem.checked==true)
          {
              if(objElem.docinternaltype=='3')
              {
              alert("LSS document(s) can not be moved in RMX.");
              return false;
              }
          }
          
          }
          
           var screen='MoveDocuments.aspx';
           document.forms[0].action=screen;
           document.forms[0].method="post";
           document.forms[0].submit();
          }
          function Trim(sString) {
              while (sString.substring(0, 1) == ' ') {
                  sString = sString.substring(1, sString.length);
              }
              while (sString.substring(sString.length - 1, sString.length) == ' ') {
                  sString = sString.substring(0, sString.length - 1);
              }
              return sString;
          }

          function createfolderCheck() {
              var fname = Trim(document.forms[0].newfoldername.value);
              if (fname=='')
              {
                  alert("Please enter folder name to create.");
                  document.forms[0].newfoldername.value = "";
                  document.forms[0].newfoldername.focus();
              return false;
          }
              if (fname.match(/[%*|\\:\/?<>]+/))
              {
                  alert("An invalid name was specified for creating a new folder.");
                  return false;
              }
           var screen='';
         
           return true;
          }
        
          function AddNewIntegate(folderid,psid,pvactive)
          {

              var screen = '/RiskmasterUI/UI/PaperVision/PVAddDocument.aspx';
           //document.forms[0].action=screen+ "?folderid="+folderid+"&psid="+psid+"&integrate=0";Animesh Modified the defination MITS 18345
           document.forms[0].action=screen+ "?folderid="+folderid+"&psid="+psid+"&integrate=0&papervisionactive="+pvactive;
           document.forms[0].method="post";
           document.forms[0].submit();
          }
          function getCheckedMCMDocFolders() {
             
          for (var i=0; i < document.forms[0].length; i++)
						{
							var objElem = document.forms[0].elements[i];
							var sID = new String(objElem.id);
							if ((sID.substring(0, 6) == "optD") && (objElem.checked == true)) {
							    document.all.hdocId.value += '|' + objElem.value + '|';

							}
							else if ((sID.substring(0, 9) == "optF")) {
							    if (objElem.checked == true)
							        document.all.hfolderId.value += '|' + objElem.value + '|';

							    if (objElem.value == "-1" || objElem.value == "-922")
							        objElem.style.visibility = 'hidden';
							}
							else if ((sID.substring(0, 11) == "optUpfolder"))
							    objElem.style.visibility = 'hidden';
							else if ((sID.substring(0, 3) == "opt")) {
							    if (objElem.value == "")
							        objElem.style.visibility = 'hidden';
							}
							    
						}
					}
					function getCheckedDocFolders() {
					    document.all.hdocId.value = "";
					    document.all.hfolderId.value = "";
					    for (var i = 0; i < document.forms[0].length; i++) {
					        var objElem = document.forms[0].elements[i];
					        var sID = new String(objElem.id);

					        if ((sID.substring(0, 6) == "optD") && (objElem.checked == true)) {
					            document.all.hdocId.value += +objElem.value + ',';
					           
					        }
					        else if ((sID.substring(0, 9) == "optF") && (objElem.checked == true)) {
                            if (objElem.value!='-1')
					        document.all.hfolderId.value += objElem.value + ',';
					       
					      
					        }
					    }
					   
					}
					function GetDocumentsInformation(pagenumber,parentfolderid,parentfoldername,folderid)
					{
					   
		            if (folderid=='undefined' || folderid==null)
					            folderid=0;
			            document.forms[0].FolderId.value=folderid;
			            document.forms[0].action = "/RiskmasterUI/UI/PaperVision/PVDocumentList.aspx?FolderId=" + folderid;
					    document.forms[0].hdnIsMoveToDiffFolder.value="move";
					    document.forms[0].method="post";
					    document.forms[0].submit();
					    return false;
					}
					function Refresh() {
					    document.forms[0].submit();
					    return false;
					}
					function GetDocument(id)
					{
					    //Merge 13013
					    if ('<%#Model.ViewAttachment%>' != "false") {
					        document.forms[0].action = "/RiskmasterUI/UI/PaperVision/PVDisplayDocument.aspx?id=" + id;
					        document.forms[0].method = "post";
					        document.forms[0].submit();
					        return false;
					    }
					    else {
					        alert('You do not have permission to complete requested operation. (No View / Download Permission in Attached Documents)');
					        return false;
					    }
					}
										//zalam 08/26/2008 Mits:-13137 Start
					function CheckCheckAll() {
					    inputs = document.getElementsByTagName("input");

					    for (i = 0; i < inputs.length; i++) {
					        if (inputs[i].type == "checkbox") {
					            inputs[i].checked = true;
					        }
					    }
					    getCheckedDocFolders();
					}
					

					function UnselectCheckAll()
					{
					    inputs = document.getElementsByTagName("input");
						for (i = 0; i < inputs.length; i++)
						{
							if (inputs[i].type=="checkbox")
							{
								inputs[i].checked = false;
							}
			            }
			            document.all.hfolderId.value = '';
			            document.all.hdocId.value = '';
					}
					//zalam 08/26/2008 Mits:-13137 End

					function PageLoaded() {
					
					    try {
					        if (parent!=null)
					      parent.MDIScreenLoaded();
					    }
					    catch(e){}
				   
					 document.forms[0].method="post";
						//Jump to Acrosoft if enabled..Raman Bhatia . 08/03/2006
						if('<%#Model.JumpToAcrosoft%>' =="1")
						{	
							if('<%#Model.JumpDestination%>'=="Attachments")
							{
								sTableName = '<%#Model.AttachTable%>';
								sFormName =  '<%#Model.FormName%>';
								sRecordId = '<%#Model.AttachRecordId%>';
								sid = '<%#Model.AcrosoftSession%>';
								sIndexKey = '<%#Model.AcrosoftAttachmentsTypeKey%>';
								sEventFolderFriendlyName = '<%#Model.EventFolderFriendlyName%>';
								sClaimFolderFriendlyName = '<%#Model.ClaimFolderFriendlyName%>';
								sAsAnywhereLink = '<%#Model.AsAnywhereLink%>';
								sAcrosoftSkin = '<%#Model.AcrosoftSkin%>';
								sAcrosoftPath = "";
								switch(sTableName)
								{
								    case "claim": if (window.opener.document.forms[0].claimnumber) {
								            sClaimNumber = window.opener.document.forms[0].claimnumber.value;
								            sEventNumber = window.opener.document.forms[0].ev_eventnumber.value;
								            sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=" + sClaimNumber + "&sid=" + sid + "&app_func=selectFolder";
								        }
								        break; 
									
													
									case "claimant"   : 
									case "defendant"  :
									case "funds"	  : 
									case "unit"	  : sClaimNumber = '<%#Model.ClaimNumber%>';
															sEventNumber = '<%#Model.EventNumber%>';
															sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=" + sClaimNumber + "&sid=" + sid + "&app_func=selectFolder";
															break; 
												      
									case "event"		:								
									case "piemployee"	: 
									case "pipatient"	: 
									case "piwitness"	:
									case "eventdatedtext"   :
												
														if(window.opener.document.forms[0].eventnumber.value!="")
														{	
															sEventNumber = window.opener.document.forms[0].eventnumber.value;
															sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sEventFolderFriendlyName + "=" + sEventNumber + "&" + sClaimFolderFriendlyName + "=0&sid=" + sid + "&app_func=selectFolder";
														}
														break;
									
									case "policy" :     if(window.opener.document.forms[0].policyname.value!="")
														{
															sPolicyName = window.opener.document.forms[0].policyname.value;
															sIndexKey = '<%#Model.AcrosoftPolicyTypeKey%>' 
															sPolicyFolderFriendlyName = '<%#Model.PolicyFolderFriendlyName%>';
															sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sPolicyFolderFriendlyName + "=" + sPolicyName + "&sid=" + sid + "&app_func=selectFolder";
                            }
                            break;
                  // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
                  case "policyenh" :     if(window.opener.document.forms[0].PolicyQuoteName.value!="")
                            {
                              sPolicyName = window.opener.document.forms[0].PolicyQuoteName.value;
                              sIndexKey = '<%#Model.AcrosoftPolicyTypeKey%>';
                              sPolicyFolderFriendlyName = '<%#Model.PolicyFolderFriendlyName%>';
                              sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sPolicyFolderFriendlyName + "=" + sPolicyName + "&sid=" + sid + "&app_func=selectFolder";
                            }
                            break;
                            // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan


                            default:	sAcrosoftPath = "";
                            break;
                            }

          if (sAcrosoftPath!="") {
           var wnd=self.parent.open(sAcrosoftPath,'DocManagement',
          'width=800,height=600'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-800)/2+',resizable=yes,scrollbars=yes');
          window.close();

          }
          }

          if('<%#Model.JumpDestination%>'=="Users")
							{
								sIndexKey = '<%#Model.AcrosoftUsersTypeKey %>';
								sAsAnywhereLink = '<%#Model.AsAnywhereLink%>';
								sAcrosoftSkin = '<%#Model.AcrosoftSkin%>';
								sLoginName = '<%#Model.AcrosoftLoginName%>';
								sUsersFolderFriendlyName = '<%#Model.UsersFolderFriendlyName%>';
								sid = '<%#Model.AcrosoftSession%>';
																								
								sAcrosoftPath = sAsAnywhereLink + "?IndexKey=" + sIndexKey + "&" + sUsersFolderFriendlyName + "=" + sLoginName + "&sid=" + sid + "&app_func=selectFolder";
								
								window.location.href = sAcrosoftPath;
								
							}
						}
						
										
							if (document.getElementById('newfoldername')!=null)
							document.forms[0].newfoldername.value = "";
							document.forms[0].hdocId.value = "";
							document.forms[0].hfolderId.value = "";
							getCheckedMCMDocFolders();
							return false;
				
					
				
					}
			
    </script>
   
</head>
<body onload="PageLoaded();">
    <form id="frmData" runat="server">
   <%if (Model.JumpToAcrosoft != "1")
     { %>
    <div>
    
     <asp:HiddenField ID="ParentFolder" runat="server"/>
       <asp:HiddenField ID="hdnIsMoveToDiffFolder" runat="server"/>
     <table width="100%" cellspacing="0" cellpadding="0" border="0">
     <tr>
       <td>
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
      </tr>
     	<tr>
									<td   colspan="8"  class="msgheader" bgcolor="#D5CDA4">
										 <asp:Label ID="lblFolder" runat="server"></asp:Label>
									</td>
								</tr>
								<tr>
  <td class="headertext2">
      <td nowrap="" align="left" class="headertext2"><asp:Label ID="lblPageRangeTop" runat="server"></asp:Label><asp:Label ID="lblPagerTop" runat="server"></asp:Label></td>

 <td colspan="8" width="95%" nowrap="" align="right" class="headertext2">
 <asp:LinkButton id="lnkRefresh"  CssClass="headerlink" OnCommand = "LinkCommand_Refresh" runat="server">Refresh</asp:LinkButton>
 <asp:Label ID="lblSpace" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;</asp:Label>
  <asp:LinkButton id="lnkFirstTop"  runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> <asp:LinkButton id="lnkPrevTop" runat="server" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNextTop" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLastTop" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
</td>
</td>
</tr>
		<tr>
		
		<td colspan="8">
		
		
		
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" 
            Width="100%" onrowdatabound="GridView1_RowDataBound" AllowPaging="false" 
            AllowSorting="True" 
            PagerSettings-Mode=NumericFirstLast onsorting="GridView1_Sorting" 
            onrowcreated="GridView1_RowCreated"  ShowHeader="true"
            GridLines=None PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign=Right PageSize=20>
            
            <PagerStyle CssClass="headertext2"  ForeColor=White/>
            <HeaderStyle CssClass="colheader6" />
            <AlternatingRowStyle CssClass="data2" /> 
            <FooterStyle  CssClass ="colheader6" />
            <Columns>
            
            <asp:TemplateField HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                <ItemTemplate>
                   <input type="checkbox" id='opt<%# DataBinder.Eval(Container.DataItem,"Type") %>'  value='<%# DataBinder.Eval(Container.DataItem,"Id")%>' onclick="Check<%# DataBinder.Eval(Container.DataItem,"Type") %>(this)"/>
                     <asp:CheckBox ID="chkDoc"  runat="server" Visible=false />
                    <asp:HiddenField ID="hdnId" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Id") %> '/>
                    <asp:HiddenField ID="hdnType" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Type") %> '/>
                </ItemTemplate>

<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6"></HeaderStyle>
             </asp:TemplateField>    
            <asp:TemplateField HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader" >
                <ItemTemplate>
                                
                               <asp:ImageButton ID="img" runat="server"/>
                               <asp:Label ID="lbl" runat=server></asp:Label>
                </ItemTemplate>

<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6"></HeaderStyle>
            </asp:TemplateField>     
                   
            
            
            <asp:TemplateField SortExpression="1" HeaderText="Name"  HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader"  FooterStyle-HorizontalAlign=Left>
            <ItemTemplate>
            <%# DataBinder.Eval(Container.DataItem,"Message") %>
            <asp:HiddenField ID="hdnTitle" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Title") %> '/>
             <asp:HiddenField ID="hdnName" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem,"Name") %> '/>
            
                <asp:HyperLink ID="linkName"    NavigateUrl="#" runat="server"></asp:HyperLink>
            </ItemTemplate>

<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6" HorizontalAlign=Left></HeaderStyle>
            </asp:TemplateField>  
            
            
            <asp:BoundField  SortExpression="2" HeaderText="Subject"  DataField="Subject" 
                    HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader" >
<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6" HorizontalAlign=Left></HeaderStyle>
                </asp:BoundField>
                    <asp:BoundField  SortExpression="3" HeaderText="Type"  DataField="DocumentType" 
                    HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader" >
<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6" HorizontalAlign=Left></HeaderStyle>
                </asp:BoundField>
            <asp:BoundField  SortExpression="4" HeaderText="Class"  DataField="Class" 
                    HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader" >
<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6" HorizontalAlign=Left></HeaderStyle>
                </asp:BoundField>
            <asp:BoundField  SortExpression="5" HeaderText="Category" DataField="Category" 
                    HeaderStyle-CssClass="msgheader" FooterStyle-CssClass="msgheader" >
<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6" HorizontalAlign=Left></HeaderStyle>
                </asp:BoundField>
             <asp:BoundField  SortExpression="6" HeaderText="Date Created " 
                    DataField="CreateDate" HeaderStyle-CssClass="msgheader" 
                    FooterStyle-CssClass="msgheader" >
<FooterStyle CssClass="colheader6"></FooterStyle>

<HeaderStyle CssClass="colheader6" HorizontalAlign=Left></HeaderStyle>
                </asp:BoundField>
            </Columns>
            <HeaderStyle CssClass="colheader6" Font-Bold="True" ForeColor="White" />
          
        </asp:GridView>
       
        </td>
		</tr>
		
<tr>
<td align="left" colspan="2" class="headertext2"><asp:Label ID="lblPageRangeDown" runat="server"></asp:Label>  <asp:Label ID="lblPagerDown" runat="server"></asp:Label></td>

 <td colspan="6" width="100%" nowrap="" align="right" class="headertext2">

  <asp:LinkButton id="lnkFirstDown"   runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> <asp:LinkButton id="lnkPrevDown" runat="server" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNextDown" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLastDown" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
</td>
</tr>
		</table>
		
		<table>
		
		<tr>
		  <td class="PadLeft4"><input type="button" class="button" onclick="CheckCheckAll();" value="Select All"></td>
      <td class="PadLeft4"><input type="button" class="button" onclick="UnselectCheckAll();" value="Unselect All"></td>
		  <% if (Model.ScreenFlag == "Files")
       {%>
     <%if (Model.Create_allowed == "1")
       {%>
      <td class="PadLeft4">
      
      <input type="button" onserverclick="GotoAddNew" value="Add New" class="button" onclick="return AddNewIntegate('<%#Model.Pid%>','<%#Model.Psid %>','<%#Model.PaperVisionActive%>');">&nbsp;</td> <!--Animesh Modified MITS 18345 -->
      <%} %>
       <%} %>
        <% if (Model.ScreenFlag != "Files")
           {%>
     <%if (Model.Att_create_allowed == "1")
       {%>
      <td class="PadLeft4">
      
      <input type="button" value="Add New" class="button" onclick="return AddNewIntegate('<%#Model.Pid%>','<%#Model.Psid %>','<%#Model.PaperVisionActive%>');">&nbsp;</td> <!--Animesh Modified MITS 18345 -->
      <%} %>
       <%} %>
       
     
     <%if (Model.Move_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="   Move   " class="button" onclick="return moveCheck();; "></td>
      <%} %>
     
     
        
     <%if (Model.Email_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  E-Mail  " class="button" onclick="return emailCheck();; "></td>
      <%} %>
       
       
         <% if (Model.ScreenFlag == "Files")
            {%>
     <%if (Model.Delete_allowed == "1")
       {%>
      <td class="PadLeft4">
      <asp:Button 
              ID="Button2" Text="  Delete  " CssClass="button" runat="server"
               onclick="btnDelete_Click" UseSubmitBehavior="true"  OnClientClick="return delCheck()"/>
      </td>
      <%} %>
       <%} %>
       
         <% if (Model.ScreenFlag != "Files")
            {%>
     <%if (Model.Att_delete_allowed == "1")
       {%>
      <td class="PadLeft4">      <asp:Button 
              ID="Button1" Text="  Delete  " CssClass="button" runat="server"
               onclick="btnDelete_Click" UseSubmitBehavior="true"  OnClientClick="return delCheck()"/></td>
      <%} %>
       <%} %>
       
         <% if (Model.ScreenFlag == "Files")
            {%>
     <%if (Model.Transfer_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  Transfer  " class="button" onclick="return transferCheck();; "></td>
      <%} %>
       <%} %>
       
         
       
         <% if (Model.ScreenFlag == "Files")
            {%>
     <%if (Model.Copy_allowed == "1")
       {%>
      <td class="PadLeft4"><input type="button" name="" value="  Copy  " class="button" onclick="return copyCheck();; "></td>
      <%} %>
       <%} %>
       
        
     </tr>
    </table>
   	<table border="0">
  <% if (Model.ScreenFlag == "Files")
     {%>
     <%if (Model.Createfolder_allowed == "1")
       {%>
	<%if (Model.TopFolder.FolderID == "0")
   { %>
	<%if (Model.Pid != "-1")
   { %>
     <tr>
      <td class="PadLeft4">Enter Folder Name to Create here:<br><input type="text" name="newfoldername" value="" id="newfoldername" runat="server">&nbsp;<asp:Button 
              ID="btnAddFolder" Text="Create Folder" CssClass="button" runat=server 
               onclick="btnAddFolder_Click" UseSubmitBehavior="true"  OnClientClick="return createfolderCheck();"/></td>
         
     </tr>
     <%} %>
     <%} %>
		<%} %>
     <%} %>						
					
    </div>
    <%} %>
    
    <asp:HiddenField ID="hdnPageNumber" runat="server" Value="0" />
    <asp:HiddenField ID="hdnOrderBy" runat="server" />
     <asp:HiddenField ID="hdocId" runat="server"/>
     <asp:HiddenField ID="hfolderId" runat="server"/>
     <asp:HiddenField ID="Psid" runat="server" Value="50" />
     <asp:HiddenField ID="flag" runat="server"/>
     <asp:HiddenField ID="FolderId" runat="server" />
      <asp:HiddenField ID="FolderName" runat="server"/>
     <asp:HiddenField ID="Regarding" runat="server" />
     <asp:HiddenField ID="AttachTableName" runat="server" />
     <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
     <asp:HiddenField ID="hdnSortExpression" runat="server" value="1"/>
       <asp:HiddenField ID="hdTotalPages" runat="server"/>
    <asp:HiddenField ID="hdCurrentPage" runat="server"/>
      <asp:HiddenField ID="NonMCMFormName" runat="server" />
      <!-- Animesh Inserted MITS 18345 -->
      <asp:HiddenField ID="hdnPaperVisionActive" runat="server" />
      <!-- Animesh Insertion Ends -->
     </form>
     
    </div>
    
</body>
</html>

