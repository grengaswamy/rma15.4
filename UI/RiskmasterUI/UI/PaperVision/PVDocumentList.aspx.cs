﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.DocumentService;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.Linq;
using System.Data;
using Riskmaster.Models;
namespace Riskmaster.UI.PaperVision
{
    public class DocumentDatas
    {
        public string Name
        {
            get;
            set;

        }
      
        public string Type
        {
            get;
            set;

        }
        public string CreateDate
        {
            get;
            set;

        }
        public string Message
        {
            get;
            set;

        }
        public string Category
        {
            get;
            set;

        }
        public string Title
        {
            get;
            set;

        }
        public string Class
        {
            get;
            set;

        }
        public string DocumentType
        {
            get;
            set;

        }
        public string Subject
        {
            get;
            set;

        }
        public int Id
        {
            get;
            set;

        }


    }
    public partial class DocumentList : System.Web.UI.Page
    {
        private string m_SortDirection = "";
        private bool bIsEmptyMessage = false;
        private string m_strSortExp = "";
        private int m_iPagesize = 10;
        public Riskmaster.Models.DocumentList Model = new Riskmaster.Models.DocumentList();
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
            NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
        }
        private List<DocumentDatas> BindData(string exp,string pagenum)
        {
            List<DocumentDatas> doc = new List<DocumentDatas>();
            DocumentBusinessHelper docHelper = new DocumentBusinessHelper();
            try
            {
                if (exp == hdnSortExpression.Value)
                {
                    exp = (6 + Conversion.ConvertObjToInt(exp)).ToString();
                }

                if (exp != "")
                    hdnSortExpression.Value = exp;
                else
                    exp = hdnSortExpression.Value;


                if (Conversion.ConvertStrToInteger(exp) >= 1 && Conversion.ConvertStrToInteger(exp) <= 6)
                {
                    //bganta - correcting the direction of the triangle
                    //m_SortDirection = "Ascending";
                    m_SortDirection = "Descending";
                }
                else
                {
                    //bganta - correcting the direction of the triangle
                    //m_SortDirection = "Descending";
                    m_SortDirection = "Ascending";
                }

                if (Page.PreviousPage != null)
                {
                    exp = AppHelper.GetFormValue("hdnSortExpression");
                    hdnSortExpression.Value = exp;
                }
                m_strSortExp = exp;
                string screenflag = AppHelper.GetValue("flag");
                string psid = AppHelper.GetValue("Psid");
                //Added:Yukti,Dt:11/13/2013, MITS 30990
                //Model = docHelper.List(exp, screenflag, psid, pagenum);
                Model = docHelper.List(exp, screenflag, psid, pagenum, false);
                //Ended:Yukti,DT:11/13/2013
                if (Model.JumpToAcrosoft != "1")
                {
                    hdTotalPages.Value = Model.Pagecount;
                    hdnPageNumber.Value = Model.Pagenumber;
                    //Animesh Inserted MITS 18345 
                    hdnPaperVisionActive.Value = Model.PaperVisionActive;   
                    //Animesh insertion ends
                    Model.ScreenFlag = screenflag;
                    flag.Value = screenflag;
                    if (screenflag == "Files")
                        ParentFolder.Value = Model.TopFolder.FolderID;

                    DocumentDatas dcUpfolder = null;
                    if (screenflag == "Files")
                    {
                        if (Model.Pid != "0")
                        {
                            dcUpfolder = new DocumentDatas();

                            dcUpfolder.Name = "";
                            dcUpfolder.Id = -922;
                            dcUpfolder.Type = "Upfolder";

                        }
                    }

                    var query2 = from s1 in Model.Folders
                                 select new DocumentDatas
                                 {
                                     Name = s1.FolderName,
                                     Id = (int)Convert.ToInt64(s1.FolderID),
                                     Type = "F",
                                     Message = ""


                                 };
                    var query1 = from s in Model.Documents
                                 select new DocumentDatas
                                 {
                                     Name = s.FileName,
                                     Id = (int)Convert.ToInt64(s.Pid
                                     ),
                                     Type = "D",
                                     Subject = s.Subject,
                                     Class = s.DocumentClass,
                                     DocumentType = s.DocumentsType,
                                     Category = s.DocumentCategory,
                                     CreateDate = s.CreateDate,
                                     Title = s.Title,
                                     Message = ""

                                 };


                    doc = new List<DocumentDatas>();
                    if (dcUpfolder != null)
                    {
                        doc.Add(dcUpfolder);
                    }
                    doc.AddRange(query2.ToList());
                    doc.AddRange(query1.ToList());
                    Psid.Value = Model.Psid;
                    FolderId.Value = Model.Pid;
                    FolderName.Value = Model.Name;
                    lblFolder.Text = Model.Name;
                    if (Model.Documents.Count == 0 && Model.Folders.Count == 0)
                    {
                        lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                        lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;
                        DataTable dt = new DataTable();
                        DataColumn dc = new DataColumn();
                        dc.ColumnName = "Id";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Type";
                        dt.Columns.Add(dc);


                        dc = new DataColumn();
                        dc.ColumnName = "Message";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Title";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Name";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Subject";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "DocumentType";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Class";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Category";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "CreateDate";
                        dt.Columns.Add(dc);

                        DataRow dr = dt.NewRow();
                        if (dcUpfolder != null)
                        {
                            dr["Type"] = "Upfolder";
                            dr["Id"] = "-922";

                            if (screenflag != "Files")
                            {
                                dr["Message"] = "<i>No documents attached. Click Add Document to add new documents.</i>";
                            }
                            else if (screenflag == "Files" && Model.Folders.Count == 0)
                            {
                                dr["Message"] = "<i>This folder is empty. Click Add New to add new documents.</i>";
                            }
                            dt.Rows.Add(dr);
                            GridView1.DataSource = dt;
                            GridView1.DataBind();

                        }
                        if (screenflag != "Files")
                        {
                            dr["Message"] = "<i>No documents attached. Click Add Document to add new documents.</i>";
                            dt.Rows.Add(dr);
                            bIsEmptyMessage = true;
                            GridView1.DataSource = dt;
                            GridView1.DataBind();
                        }

                    }
                    else
                    {
                        GridView1.DataSource = doc;
                        GridView1.DataBind();

                    }

                }
            }

            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            Page.DataBind();
            return doc;
        }
        void AddSortImage(GridViewRow headerRow)
        {
            if ((Conversion.ConvertStrToInteger(m_strSortExp) >= 7))
                m_strSortExp = ((Conversion.ConvertStrToInteger(m_strSortExp) - 6)).ToString();
            Int32 iCol = GetSortColumnIndex(m_strSortExp);
            if (-1 == iCol)
            {
                return;
            }
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if ("Ascending" == m_SortDirection)
            {
                sortImage.ImageUrl = "~/Images/arrow_down_white.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "~/Images/arrow_up_white.gif";
                sortImage.AlternateText = "Descending Order";
            }

            // Add the image to the appropriate header cell.
            headerRow.Cells[iCol].Controls.Add(sortImage);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                if (AppHelper.GetValue("flag") == "")
                {
                    //Commented by Nitin for Mits 21612 for adding Paging for Attachments
                    //lnkFirstTop.Visible=false;
                    //lnkLastTop.Visible=false;
                    //lnkPrevTop.Visible=false;
                    //lnkNextTop.Visible=false;
                    //lnkFirstDown.Visible=false;
                    //lnkLastDown.Visible=false;
                    //lnkPrevDown.Visible=false;
                    //lnkNextDown.Visible=false;
                    //lblPageRangeTop.Visible = false;
                    //lblPageRangeDown.Visible = false;
                    //lblSpace.Visible = false;
                    //lnkRefresh.Visible = false;
                }
                if (!Page.IsPostBack)
                {
                    SetInitialValues();

                    List<DocumentDatas> doc = new List<DocumentDatas>();                    
                    doc = BindData("",AppHelper.GetFormValue("hdnPageNumber"));
                    if (Model.JumpToAcrosoft != "1")
                    {
                        if (!Page.IsPostBack)
                        {
                            lnkFirstTop.Enabled = true;
                            lnkLastTop.Enabled = true;
                            lnkPrevTop.Enabled = true;
                            lnkNextTop.Enabled = true;
                            lnkFirstDown.Enabled = true;
                            lnkLastDown.Enabled = true;
                            lnkPrevDown.Enabled = true;
                            lnkNextDown.Enabled = true;
                            if (Page.PreviousPage != null)
                            {
                                lblPageRangeTop.Text = "Page " + hdnPageNumber.Value + " of " + hdTotalPages.Value;
                                lblPageRangeDown.Text = "Page " + hdnPageNumber.Value + " of " + hdTotalPages.Value;
                            }
                            else
                            {
                                lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                                lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;


                                if (hdTotalPages.Value == "1") // There is only 1 page.
                                {
                                    lnkFirstTop.Enabled = false;
                                    lnkLastTop.Enabled = false;
                                    lnkPrevTop.Enabled = false;
                                    lnkNextTop.Enabled = false;
                                    lnkFirstDown.Enabled = false;
                                    lnkLastDown.Enabled = false;
                                    lnkPrevDown.Enabled = false;
                                    lnkNextDown.Enabled = false;
                                }
                                else
                                {
                                    lnkFirstTop.Enabled = false;
                                    lnkPrevTop.Enabled = false;
                                    lnkFirstDown.Enabled = false;
                                    lnkPrevDown.Enabled = false;
                                }
                            }
                            if (Page.PreviousPage != null)
                            {
                                if (Conversion.ConvertStrToInteger(hdnPageNumber.Value) == 1)
                                {
                                    lnkFirstTop.Enabled = false;
                                    lnkPrevTop.Enabled = false;
                                    lnkFirstDown.Enabled = false;
                                    lnkPrevDown.Enabled = false;
                                    
                                }
                                if (Conversion.ConvertStrToInteger(hdnPageNumber.Value) == Conversion.ConvertStrToInteger(hdTotalPages.Value))
                                {
                                    lnkLastTop.Enabled = false;
                                    lnkNextTop.Enabled = false;
                                    lnkLastDown.Enabled = false;
                                    lnkNextDown.Enabled = false;
                                }
                            }
                        }
                    }

                }
                else
                {
                    ProcessPostBack();
                    
                }
               
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
           
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
             
                HiddenField id = (HiddenField)e.Row.Cells[0].FindControl("hdnId");
                HiddenField Type = (HiddenField)e.Row.Cells[0].FindControl("hdnType");
                ImageButton img = (ImageButton)e.Row.Cells[1].FindControl("img");
                if (Conversion.ConvertStrToInteger(id.Value) == -1 && Type.Value == "F")
                {

                    img.ImageUrl = "~/Images/Document/recycle.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                 
                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = Name.Value;

                }
                else if (Conversion.ConvertStrToInteger(id.Value) > 0 && Type.Value == "F")
                {

                    img.ImageUrl = "~/Images/Document/folder.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = Name.Value;
                }
                if (Type.Value == "D")
                {

                    img.ImageUrl = "~/Images/Document/doc.gif";
                    img.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "')");
                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = title.Value + "(" + Name.Value + ")";

                    //sgupta243 MITS 22213 Date 09/14/2012
                    if (title.Value == "SROI")
                    {
                        link.Text = Name.Value;
                    }


                    link.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "')");
                }

                if (Type.Value == "Upfolder")
                {

                    img.ImageUrl = "~/Images/Document/upfolder.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + ParentFolder.Value + "')");
                

                }
                if (Type.Value == "F")
                {
                    HyperLink link1 = (HyperLink)e.Row.FindControl("linkName");
                    link1.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                }
                if (bIsEmptyMessage == true)
                {
                    img.Visible = false;
                }
            }


        }

       

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            m_strSortExp = e.SortExpression;
            BindData(e.SortExpression,hdnPageNumber.Value);

        }

        private int GetSortColumnIndex(String strCol)
        {
            foreach (DataControlField field in GridView1.Columns)
            {
                if (field.SortExpression == strCol)
                {
                    return GridView1.Columns.IndexOf(field);
                }
            }

            return -1;
        }
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (String.Empty != m_strSortExp)
                {
                    AddSortImage(e.Row);
                }
            }
        }
        private void ProcessPostBack()
        {
            if (hdnIsMoveToDiffFolder.Value == "move")
            {
                List<DocumentDatas> doc = new List<DocumentDatas>();
                GridView1.PageIndex = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdnPageNumber"));
                BindData("", "1");
                lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;
                lnkFirstTop.Enabled = true;
                lnkLastTop.Enabled = true;
                lnkPrevTop.Enabled = true;
                lnkNextTop.Enabled = true;
                lnkFirstDown.Enabled = true;
                lnkLastDown.Enabled = true;
                lnkPrevDown.Enabled = true;
                lnkNextDown.Enabled = true;
                if (hdTotalPages.Value == "1") // There is only 1 page.
                {
                    lnkFirstTop.Enabled = false;
                    lnkLastTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkNextTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkLastDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                    lnkNextDown.Enabled = false;
                }
                else
                {
                    lnkFirstTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                }
                hdnIsMoveToDiffFolder.Value = "";
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Server.Transfer("/RiskmasterUI/UI/PaperVision/PVConfirmDelete.aspx", true);
        }
        protected void btnAddFolder_Click(object sender, EventArgs e)
        {
            try
            {
                if (FolderId.Value == "")
                    FolderId.Value = "0";
                DocumentBusinessHelper dc = new DocumentBusinessHelper();
                dc.CreateFolder(newfoldername.Value, FolderId.Value);
                List<DocumentDatas> doc = new List<DocumentDatas>();
                doc = BindData("",hdnPageNumber.Value);
                lblPageRangeTop.Text = "Page " +hdnPageNumber.Value+ " of " + hdTotalPages.Value;
                lblPageRangeDown.Text = "Page " +hdnPageNumber.Value+ " of " + hdTotalPages.Value;
                lnkFirstTop.Enabled = true;
                lnkLastTop.Enabled = true;
                lnkPrevTop.Enabled = true;
                lnkNextTop.Enabled = true;
                lnkFirstDown.Enabled = true;
                lnkLastDown.Enabled = true;
                lnkPrevDown.Enabled = true;
                lnkNextDown.Enabled = true;
                if (hdTotalPages.Value == "1") // There is only 1 page.
                {
                    lnkFirstTop.Enabled = false;
                    lnkLastTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkNextTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkLastDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                    lnkNextDown.Enabled = false;
                }
                else
                {
                    lnkFirstTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                }
                if (Conversion.ConvertStrToInteger(hdnPageNumber.Value) < Conversion.ConvertStrToInteger(hdTotalPages.Value))
                {
                    lnkFirstTop.Enabled = true;
                    lnkLastTop.Enabled = true;
                    lnkPrevTop.Enabled = true;
                    lnkNextTop.Enabled = true;
                    lnkFirstDown.Enabled = true;
                    lnkLastDown.Enabled = true;
                    lnkPrevDown.Enabled = true;
                    lnkNextDown.Enabled = true;
                }

            }

            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void LinkCommand_Refresh(object sender, CommandEventArgs e)
        {
            BindData("", hdnPageNumber.Value);
        }
        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {

     
            int iCurrentPage = 0;
            int iPreviousPage = 0;
            string pagenum="";
           


            lnkFirstTop.Enabled = true;
            lnkLastTop.Enabled = true;
            lnkPrevTop.Enabled = true;
            lnkNextTop.Enabled = true;
            lnkFirstDown.Enabled = true;
            lnkLastDown.Enabled = true;
            lnkPrevDown.Enabled = true;
            lnkNextDown.Enabled = true;

            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {

                        iCurrentPage = 0;

                        lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                        lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;

                    

                        lnkFirstTop.Enabled = false;
                        lnkPrevTop.Enabled = false;
                        lnkFirstDown.Enabled = false;
                        lnkPrevDown.Enabled = false;

                        pagenum="1";
                        break;
                    }
                case "LAST":
                    {

                        iCurrentPage = Conversion.ConvertStrToInteger(hdTotalPages.Value) - 1;

                        lblPageRangeTop.Text = "Page " + hdTotalPages.Value + " of " + hdTotalPages.Value;
                        lblPageRangeDown.Text = "Page " + hdTotalPages.Value + " of " + hdTotalPages.Value;

                    

                        lnkLastTop.Enabled = false;
                        lnkNextTop.Enabled = false;
                        lnkLastDown.Enabled = false;
                        lnkNextDown.Enabled = false;

                        pagenum=hdTotalPages.Value;
                        break;
                    }
                case "PREV":
                    {
         
                        iCurrentPage = Conversion.ConvertStrToInteger(hdnPageNumber.Value);


                        iPreviousPage = iCurrentPage - 1;

                        if (iPreviousPage == 1)
                        {
                            lnkFirstTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                            lnkFirstDown.Enabled = false;
                            lnkPrevDown.Enabled = false;
                        }


                        lblPageRangeTop.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Value;
                        lblPageRangeDown.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Value;



                        pagenum = Conversion.ConvertObjToStr(iCurrentPage - 1);
                        break;
                    }
                case "NEXT":
                    {
 
                        iCurrentPage = Conversion.ConvertStrToInteger(hdnPageNumber.Value);

                        if ((iCurrentPage + 1) == Conversion.ConvertObjToInt(hdTotalPages.Value))
                        {
                            lnkLastTop.Enabled = false;
                            lnkNextTop.Enabled = false;
                            lnkLastDown.Enabled = false;
                            lnkNextDown.Enabled = false;
                        }

                        lblPageRangeTop.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Value;
                        lblPageRangeDown.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Value;



                        pagenum = Conversion.ConvertObjToStr(iCurrentPage + 1);
                        break;
                    }
            } 

           
            BindData("", pagenum);
        }


    }
}
