﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;


namespace Riskmaster.UI.LeaveHistory
{
    public partial class LeaveHistory : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public IEnumerable resultSummary = null;
        public XElement rootElement = null;
        public string sLeaveHistoryTitle = "";
        public string sLeaveSummaryTitle = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument SysUsersXmlDoc = new XmlDocument();
            XElement objTitle = null;
            string sClaimantRowId = "";     

            sClaimantRowId = Convert.ToString(Request.QueryString["ClaimantRowId"]);
            claimantrowid.Text = sClaimantRowId;

            if (!IsPostBack)
            {
                try
                {
                    bReturnStatus = CallCWSFunction("LeavePlanAdaptor.GetLeaveHistory", out sreturnValue);
                    if (bReturnStatus)
                    {
                        SysUsersXmlDoc.LoadXml(sreturnValue);
                        rootElement = XElement.Parse(SysUsersXmlDoc.OuterXml);

                        objTitle = rootElement.XPathSelectElement("//LeaveHistory");
                        sLeaveHistoryTitle = objTitle.Attribute("LeaveHistoryTitle").Value ;
                        sLeaveSummaryTitle = objTitle.Attribute("LeaveSummaryTitle").Value;

                        result = from c in rootElement.XPathSelectElements("//LeaveHistory/option")                                 
                                 select c;

                        resultSummary = from c in rootElement.XPathSelectElements("//LeaveSummary")
                                 select c;
                    }
                }

                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);                    

                }
            }
        }
    }
}
