﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeaveHistory.aspx.cs" Inherits="Riskmaster.UI.LeaveHistory.LeaveHistory" %>

<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=sLeaveHistoryTitle%></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function gotoclaim(claim) {
            //window.opener.document.location.href='fdm?SysFormName=claim&SysFormId='+claimid+'&SysViewType='+sSysViewType+'&SysCmd=0';
            //            window.opener.parent.document.location.href = "../FDM/claimdi.aspx?SysFormId=" + claimid + "&SysCmd=0";
            //abansal23: MITS 15925 on 4/27/2009
            window.opener.document.location.href = "../FDM/claimdi.aspx?SysFormName=claimdi&SysCmd=0&SysViewType=controlsonly&SysFormIdName=claimid&SysFormId=" + claim;
            window.close();
            return false;
        }
    </script>

</head>
<body class="rowlight">
    <form id="frmData" runat="server">
    <div class="datatd" id="detailtitle">
        <b>Detailed Leave History</b></div>
    <table border="1" width="100%" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <td class="msgheader">
                    Claim No
                </td>
                <td class="msgheader" nowrap="1">
                    Plan Code
                </td>
                <td class="msgheader" nowrap="1">
                    Start Date
                </td>
                <td class="msgheader" nowrap="1">
                    End Date
                </td>
                <td class="msgheader" nowrap="1">
                    Reason
                </td>
                <td class="msgheader" nowrap="1">
                    Type
                </td>
                <td class="msgheader" nowrap="1">
                    Hours
                </td>
                <td class="msgheader" nowrap="1">
                    Authorization
                </td>
                <td class="msgheader" nowrap="1">
                    Status
                </td>
            </tr>
        </thead>
        <%int i = 0; foreach (XElement item in result)
          {
              string colclass = "";
              if ((i % 2) == 1) colclass = "datatd1";
              else colclass = "datatd";
              i++;
        %>
        <tr>
            <%--abansal23: MITS 15925 on 4/27/2009 Starts--%>
            <td class="<%=colclass%>">
                
                <% lnkClaimNumber.PostBackUrl = "../FDM/claimdi.aspx?SysFormName=claimdi&SysCmd=0&SysViewType=controlsonly&SysFormIdName=claimid&SysFormId=" + item.Element("ClaimId").Value;
                   lnkClaimNumber.Text = item.Element("ClaimNumber").Value;
                   lnkClaimNumber.OnClientClick = "gotoclaim(" + item.Element("ClaimId").Value + ")";%>
                <asp:LinkButton runat="server" ID="lnkClaimNumber"></asp:LinkButton>
                <%-- <a href="#" onclick="gotoclaim(<%=item.Element("ClaimId").Value%>)"><%=item.Element("ClaimNumber").Value%></a>--%>
                <%--abansal23: MITS 15925 on 4/27/2009 Ends--%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("PlanCode").Value%>
            </td>
            <td class="<%=colclass%>">
                <%=AppHelper.GetUIDate(item.Element("DateLeaveStart").Value)%>
            </td>
            <td class="<%=colclass%>">
                <%=AppHelper.GetUIDate(item.Element("DateLeaveEnd").Value)%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("LeaveReasonCode").Value%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("LeaveTypeCode").Value%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("HoursLeave").Value%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("LeaveAuthCode").Value%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("LeaveStatusCode").Value%>
            </td>
        </tr>
        <%}%>
    </table>
    <div class="datatd" id="summarytitle">
        <b>
            <%=sLeaveSummaryTitle%></b></div>
    <table border="1" width="50%" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <td class="msgheader" nowrap="1">
                    Plan Code
                </td>
                <td class="msgheader" nowrap="1">
                    Leave Type
                </td>
                <td class="msgheader" nowrap="1">
                    Hours
                </td>
            </tr>
        </thead>
        <%int j = 0; foreach (XElement item in resultSummary)
          {
              string colclass = "";
              if ((j % 2) == 1) colclass = "datatd1";
              else colclass = "datatd";
              j++;
        %>
        <tr>
            <td class="<%=colclass%>">
                <%=item.Element("PlanCode").Value%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("LeaveType").Value%>
            </td>
            <td class="<%=colclass%>">
                <%=item.Element("Hours").Value%>
            </td>
        </tr>
        <%}%>
    </table>
   <%--abansal23: MITS 15923 on 4/27/2009 Starts --%>
    <br />
    <br />
    <div>
        <asp:Button ID="btnClose" Text="Close" runat="server" OnClientClick="window.close();" />    
        <asp:Button runat="server" ID="btnPrint" Text="Print" OnClientClick="window.print();return false;" />
        <asp:TextBox Style="display: none" runat="server" ID="claimantrowid" rmxref="Instance/Document/LeaveHistory/ClaimantRowId"
            rmxtype="hidden" />
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
    </div>
<%--    abansal23: MITS 15923 on 4/27/2009 Ends--%>
    </form>
</body>
</html>
