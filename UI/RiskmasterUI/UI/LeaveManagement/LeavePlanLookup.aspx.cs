﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;

namespace Riskmaster.UI.LeavePlanLookup
{
    public partial class LeavePlanLookup : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public IEnumerable resultSummary = null;
        public XElement rootElement = null;
        public string sLeaveHistoryTitle = "";
        public string sLeaveSummaryTitle = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument SysUsersXmlDoc = new XmlDocument();
            XElement objTitle = null;
            string sEmpEid = "";
            string sEligDate = "";

            sEmpEid = Convert.ToString(Request.QueryString["EmpEid"]);
            empid.Text = sEmpEid;
            sEligDate = Convert.ToString(Request.QueryString["Eligdate"]);
            eligdate.Text = sEligDate;            

            if (!IsPostBack)
            {               
                try
                {
                    bReturnStatus = CallCWSFunction("LeavePlanAdaptor.GetLeavePlans", out sreturnValue);
                    if (bReturnStatus)
                    {
                        SysUsersXmlDoc.LoadXml(sreturnValue);
                        rootElement = XElement.Parse(SysUsersXmlDoc.OuterXml);
                        //MITS 14649 : abansal23 starts
                        if (rootElement.XPathSelectElement("//LeavePlans") != null)
                        {
                            result = from c in rootElement.XPathSelectElement("//LeavePlans").Nodes()
                                     select c;
                            MainDiv.Visible = true;
                            ChildDiv.Visible = false;
                        }
                        else
                        {
                            MainDiv.Visible = false;
                            ChildDiv.Visible = true;
                        }
                        //MITS 14649 : abansal23 ends
                    }
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
        }

        //abansal23 on 4/23/2009 : for cases of plan name containing (')
        protected string escapeStrings(string sCode)
        {

            sCode = sCode.Replace("'", "\\'");
            return sCode;

        }
    }
}
