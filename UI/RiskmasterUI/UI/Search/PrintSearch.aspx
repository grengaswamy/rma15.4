﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintSearch.aspx.cs" Inherits="Riskmaster.UI.UI.Search.PrintSearch" ValidateRequest="false"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Search</title>
    <script language="javascript" type="text/javascript">
        function PrintSearchLoad() 
        {
            document.forms[0].hdCriteriaXml.value = window.opener.document.forms[0].hdCriteriaXml.value;
            document.forms[0].submit();
        }
    </script>
</head>
<body onload="PrintSearchLoad();">
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div>
    
    </div>
    <asp:HiddenField ID="hdCriteriaXml" runat="server" />
    </form>
</body>
</html>
