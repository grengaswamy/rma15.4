﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.Search
{
    public partial class SearchRender : System.Web.UI.Page
    {
        XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oSessionCmdElement = null;
            XmlDocument objXml = null;
            XmlDocument objEntityXml = new XmlDocument();
            //XslCompiledTransform objXsl = new XslCompiledTransform();
            XslTransform objXsl = new XslTransform();
            string sEntityId = string.Empty;
            string sType = string.Empty;
            string sLookupType = string.Empty;
            string sFormName = string.Empty;
            string sLastName = string.Empty;
            // Start : MITS No : 29409 ngupta73
            string sFirstName = string.Empty;
            // End : MITS No : 29409 ngupta73

            try
            {
                // Fetch the request parameters and set the corresponding XElement nodes.//skhare7 MITs 26969
                if (Request.QueryString["newentity"] != null && Request.QueryString["newentity"] == "AddNew")
                {
                    oMessageElement = GetAddNewMessageTemplate();
                    if (Request.QueryString["lastname"] != null)
                    {
                        sLastName = Request.QueryString["lastname"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/AddEntityData/LastName");
                        oSessionCmdElement.Value = sLastName;

                    }
                    if (Request.QueryString["formname"] != null)
                    {
                        sFormName = Request.QueryString["formname"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/AddEntityData/FormName");
                        oSessionCmdElement.Value = sFormName;
                    }
                    // Start : MITS No : 29409 ngupta73
                    if (Request.QueryString["firstname"] != null)
                    {
                        sFirstName = Request.QueryString["firstname"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/AddEntityData/FirstName");
                        oSessionCmdElement.Value = sFirstName;
                    }
                    // End : MITS No : 29409 ngupta73



                }
                else
                {
                    oMessageElement = GetMessageTemplate();

                    if (Request.QueryString["entityid"] != null)
                    {
                        sEntityId = Request.QueryString["entityid"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/EntityTableId");
                        oSessionCmdElement.Value = sEntityId;
                    }
                    if (Request.QueryString["type"] != null)
                    {
                        sType = Request.QueryString["type"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/Type");
                        oSessionCmdElement.Value = sType;
                    }
                    if (Request.QueryString["lookuptype"] != null)
                    {
                        sLookupType = Request.QueryString["lookuptype"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/LookUpType");
                        oSessionCmdElement.Value = sLookupType;
                    }
                    if (Request.QueryString["formname"] != null)
                    {
                        sFormName = Request.QueryString["formname"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/FormName");
                        oSessionCmdElement.Value = sFormName;
                    }

                }

                if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
                {
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                    oSessionCmdElement.Value = AppHelper.ReadCookieValue("SessionId");
                }

                // Call the webservice to get the data for the selected entity.

                objXml = GetDataForSelectedEntity();

                XElement objEntityData = XElement.Parse(objXml.OuterXml.ToString());

                // Get the Xml for the selected entity eg, entity.xml, employee.xml etc.

                //objEntityXml.Load(@"C:\DotNet\Main RMX Source\Riskmaster\Tools\DbUpgrade\views\entity.xml");

                // Put the data received for the selected entity into the xml.
                objEntityXml = FillXmlDom(objEntityData, sLookupType);


                // Render hidden controls using the formhidden.xsl.
                //objXsl.Load(Server.MapPath("../../Content/Xsl/formhidden.xsl"));
                objXsl.Load(GetFormHiddenXsl());

                // Get the transformed result
                StringWriter sw = new StringWriter();
                objXsl.Transform(objEntityXml, null, sw);
                string result = sw.ToString();

                Control ctrl = Page.ParseControl(result);
                Page.Controls.Add(ctrl);
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XmlDocument GetDataForSelectedEntity()
        {
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;

            //Call WCF wrapper for cws
            //CommonWCFService.CommonWCFServiceClient oWCF = new CommonWCFService.CommonWCFServiceClient();
            //string sReturn = oWCF.ProcessRequest(oMessageElement.ToString());

            //string sReturn = AppHelper.CallWCFService(oMessageElement.ToString());
            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");

            objXml.LoadXml(oInstanceNode.InnerXml);
            return objXml;
        }

        private XmlDocument FillXmlDom(XElement p_objEntityData, string p_sLookUpType)
        {
            string sRef = string.Empty;
            string sXml = string.Empty;
            XmlDocument objXml = new XmlDocument();
            XElement objEntityXml = null;

            switch (p_sLookUpType.ToLower())
            {
                case "6":
                    //sXml = "claims.xml";
                    objEntityXml = GetClaimXml();
                    break;
                case "10":
                case "8":
                    //sXml = "vehicle.xml";
                    objEntityXml = GetVehicleXml();
                    break;
                case "11":
                case "7":
                    //sXml = "event.xml";
                    objEntityXml = GetEventXml();
                    break;
                case "4":
                    //sXml = "employee.xml";
                    objEntityXml = GetEmployeeXml();
                    break;
                case "9":
                    //sXml = "policy.xml";
                    objEntityXml = GetPolicyXml();
                    break;
                case "12":
                    //sXml = "Funds.xml";
                    objEntityXml = GetFundsXml();
                    break;
                case "13":
                    //sXml = "ControlRequest.xml";
                    objEntityXml = GetControlRequestXml();
                    break;
                case "20":
                    //sXml = "policyenh.xml";
                    objEntityXml = GetPolicyEnhXml();
                    break;
                //smahajan6 12/09/09 MITS:18230 :Start
                case "25":
                    //sXml = "propertyunit.xml";
                    objEntityXml = GetPropertyUnitXml();
                    break;
                //smahajan6 12/09/09 MITS:18230 :End
                //RMA-8753 nshah28 start
                case "1102":
                    objEntityXml = GetAddressXml();
                    break; 
                //RMA-8753 nshah28 end
                default:
                    //sXml = "entity.xml";
                    objEntityXml = GetEntityXml();
                    break;
            }

            //XDocument objEntityXml = XDocument.Load(Server.MapPath("./Xml/" + sXml));
            
            var objControlXml = from controls in objEntityXml.Descendants("control")

                                select controls;

            foreach (XElement control in objControlXml)
            {
                if (control.Attribute("ref") != null)
                {
                    sRef = control.Attribute("ref").Value;
                    if (!string.IsNullOrEmpty(sRef))
                    {
                        sRef = GetResponseRefPath(sRef);

                        GetEntityElementValue(p_objEntityData, control, sRef);
                    }
                }
            }

            objXml.LoadXml(objEntityXml.ToString());

            return objXml;
        }

        private void GetEntityElementValue(XElement oMessageElement, XElement objControl, string sPath)
        {
            //check if XPath is for an attribute
            string sValue = string.Empty;
            string sCodeId = string.Empty;
            XElement oElement = null;
            oElement = oMessageElement.XPathSelectElement(sPath);
            if (oElement != null)
            {
                sValue = oElement.Value;

                if (oElement.Attribute("codeid") != null)
                    sCodeId = oElement.Attribute("codeid").Value;

            }
            objControl.Value = sValue;
            objControl.SetAttributeValue("codeid", sCodeId);
        }


        /// <summary>
        /// Convert RMXRef to XPath of CWS response message
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private string GetResponseRefPath(string sPath)
        {

            sPath = sPath.Replace("Instance/", string.Empty);

            if (sPath.StartsWith("/"))
            {
                sPath = sPath.Substring(1);
                sPath = sPath.Substring(sPath.IndexOf("/"), sPath.Length - sPath.IndexOf("/"));
                sPath = "." + sPath;
            }

            return sPath;
        }


        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>SearchAdaptor.GetSearchResultsForSelectedEntity</Function> 
                    </Call>
                    <Document>
                        <GetEntityData>
                            <EntityTableId></EntityTableId> 
                            <Type></Type> 
                            <LookUpType></LookUpType> 
                            <FormName></FormName> 
                        </GetEntityData>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }
        //skhare7 R8 26969
        // Changed By: ngupta73 MITS No : 29409 
        private XElement GetAddNewMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>SearchAdaptor.AddNewEntity</Function> 
                    </Call>
                    <Document>
                        <AddEntityData>
                            <LastName></LastName> 
                            <FirstName></FirstName>
                            <FormName></FormName> 
                        </AddEntityData>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }
        //--------------------FormHidden Xsl------------
        //        <?xml version='1.0'?>
        //<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

        //  <xsl:template match="/">

        //    <form name="frmData">
        //            <xsl:for-each select="form/group">
        //                <xsl:apply-templates select="displaycolumn/control" />
        //            </xsl:for-each>
        //        <xsl:apply-templates select="//internal"/>
        //        <input type="hidden" name="sys_formname"><xsl:attribute name="value"><xsl:for-each select="form"><xsl:value-of select="@name"/></xsl:for-each></xsl:attribute></input>
        //    </form>
        //</xsl:template>



        //<xsl:template match="control">


        //<xsl:choose>
        //    <xsl:when test="@type[.='id']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value">
        //    <xsl:value-of select="text()"/>
        //  </xsl:attribute></input></xsl:when>
        //    <xsl:otherwise>
        //        <xsl:choose>
        //            <xsl:when test="@type[.='text']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='hidden']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='textml']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='code']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_codelookup</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            <input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_codelookup_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='codewithdetail']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            <input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='freecode']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='date']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='time']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='orgh']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            <input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='label']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='memo']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='checkbox']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='zip']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='numeric']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='currency']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='entitylookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            <input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='eidlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            <input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
        //            </xsl:when>

        //      <!-- xsl:when test="@type[.='codelist']">
        //                <input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
        //                <xsl:attribute name="value">
        //                        <xsl:apply-templates select="option">
        //                            <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
        //                    </xsl:apply-templates>
        //                </xsl:attribute>
        //                </input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='entitylist']">
        //                <input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
        //                <xsl:attribute name="value">
        //                        <xsl:apply-templates select="option">
        //                            <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
        //                    </xsl:apply-templates>
        //                </xsl:attribute>
        //                </input>
        //            </xsl:when -->

        //            <xsl:when test="@type[.='policylookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='claimnumberlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='eventnumberlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='vehiclenumberlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='employeelookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='eventlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='buttonscript']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='phone']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='ssn']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //            <xsl:when test="@type[.='href']">
        //            </xsl:when>
        //            <xsl:when test="@type[.='readonly']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
        //            </xsl:when>
        //      <xsl:when test="@type[.='customizedlistlookup']">
        //        <input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_rmsyslookup</xsl:attribute>
        //          <xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
        //        </input>
        //        <input type="hidden">
        //          <xsl:attribute name="id"><xsl:value-of select="@name"/>_rmsyslookup_cid</xsl:attribute>
        //          <xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
        //        </input>
        //      </xsl:when>
        //            <xsl:otherwise>Unknown Element Type: <xsl:value-of select="@type"/>
        //            </xsl:otherwise>
        //        </xsl:choose>
        //    </xsl:otherwise>
        //</xsl:choose>
        //</xsl:template>

        //<xsl:template match="internal">
        //    <xsl:choose>
        //        <xsl:when test="@type[.='hidden']">
        //            <input type="hidden"><xsl:attribute name="id">sys_<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
        //        </xsl:when>
        //    </xsl:choose>
        //</xsl:template>
        //</xsl:stylesheet>
        #region Xml Content
        private XmlDocument GetFormHiddenXsl()
        {
            XmlDocument objDoc = new XmlDocument();
            //Formatted Xsl is hardcoded above
            objDoc.LoadXml("<?xml version=\"1.0\"?><xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\"><xsl:template match=\"/\"><form name=\"frmData\"><xsl:for-each select=\"form/group\"><xsl:apply-templates select=\"displaycolumn/control\" /></xsl:for-each><xsl:apply-templates select=\"//internal\" /><input type=\"hidden\" name=\"sys_formname\"><xsl:attribute name=\"value\"><xsl:for-each select=\"form\"><xsl:value-of select=\"@name\" /></xsl:for-each></xsl:attribute></input></form></xsl:template><xsl:template match=\"control\"><xsl:choose><xsl:when test=\"@type[.='id']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:otherwise><xsl:choose><xsl:when test=\"@type[.='text']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='hidden']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='textml']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='code']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_codelookup</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_codelookup_cid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='codewithdetail']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_cid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='freecode']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='date']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='time']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='orgh']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_cid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='label']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='memo']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='checkbox']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='zip']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='numeric']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='currency']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='entitylookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_cid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='eidlookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_cid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></input></xsl:when><!-- xsl:when test=\"@type[.='codelist']\">\r\n\t\t\t\t<input type=\"hidden\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/>_lst</xsl:attribute>\r\n         \t\t<xsl:attribute name=\"value\">\r\n\t\t\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n         \t\t\t</xsl:apply-templates>\r\n         \t\t</xsl:attribute>\r\n\t\t\t\t</input>\r\n\t\t\t</xsl:when>\r\n\t\t\t<xsl:when test=\"@type[.='entitylist']\">\r\n\t\t\t\t<input type=\"hidden\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/>_lst</xsl:attribute>\r\n         \t\t<xsl:attribute name=\"value\">\r\n\t\t\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n         \t\t\t</xsl:apply-templates>\r\n         \t\t</xsl:attribute>\r\n\t\t\t\t</input>\r\n\t\t\t</xsl:when --><xsl:when test=\"@type[.='policylookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='claimnumberlookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='eventnumberlookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='vehiclenumberlookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='employeelookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='eventlookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='buttonscript']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='phone']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='ssn']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='href']\"></xsl:when><xsl:when test=\"@type[.='readonly']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input></xsl:when><xsl:when test=\"@type[.='customizedlistlookup']\"><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_rmsyslookup</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"text()\" /></xsl:attribute></input><input type=\"hidden\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />_rmsyslookup_cid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></input></xsl:when><xsl:otherwise>Unknown Element Type: <xsl:value-of select=\"@type\" /></xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:template><xsl:template match=\"internal\"><xsl:choose><xsl:when test=\"@type[.='hidden']\"><input type=\"hidden\"><xsl:attribute name=\"id\">sys_<xsl:value-of select=\"@name\" /></xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@value\" /></xsl:attribute></input></xsl:when></xsl:choose></xsl:template></xsl:stylesheet>");
            return objDoc;
        }
        private XElement GetEntityXml()
        {
            XElement doc = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8' ?><?xml-stylesheet type='text/xsl' href='form2.xsl'?>
            <form name='entity' title='Entity Maintenance' topbuttons='1'>
              <toolbar>
                <button type='new' title='New' />
                <button type='save' title='Save' />
                <button type='movefirst' title='Move First' />
                <button type='moveprevious' title='Move Previous' />
                <button type='movenext' title='Move Next' />
                <button type='movelast' title='Move Last' />
                <button type='attach' title='Attach Documents' />
                <button type='search' title='Search' />
                <button type='diary' title='Diary' />
                <button type='mailmerge' title='Mail Merge' />
                <button type='recordsummary' title='Record Summary' />
              </toolbar>
              <group selected='1' name='entity' title='Entity'>
                <displaycolumn>
                  <control name='entityid' tabindex='0' type='id' ref='/Instance/Entity/EntityId' />
                  <control name='entitytableid' tabindex='1' type='id' ref='/Instance/Entity/EntityTableId' />
                  <control name='lastname' firstfield='1' type='text' tabindex='2' title='Name' maxlength='255'
				            required='yes' ref='/Instance/Entity/LastName' />
                  <control name='middlename' type='text' title='Middle Name' ref='/Instance/Entity/MiddleName' maxlength='255'/>

                  <control name='firstname' type='text' tabindex='3' title='First Name' maxlength='255' ref='/Instance/Entity/FirstName' />
                </displaycolumn>
                <displaycolumn>
                  <control name='contact' type='text' tabindex='4' title='Contact' maxlength='50' ref='/Instance/Entity/Contact' />
                  <control name='alsoknownas' type='text' tabindex='5' title='DBA' maxlength='50' ref='/Instance/Entity/AlsoKnownAs' />
                </displaycolumn>
                <displaycolumn>
                  <control name='abbreviation' type='text' tabindex='6' title='Abbreviation' maxlength='50'
				            required='yes' ref='/Instance/Entity/Abbreviation' />
                  <control name='birthdate' type='date' tabindex='7' title='Date Of Birth' ref='/Instance/Entity/BirthDate' />
                  <!--Added by Amitosh for Mits 23186 (02/17/2011)-->
                  <control name='age' type='text' tabindex='7' title='Age' ref='/Instance/Entity/Age' />
                </displaycolumn>
                <displaycolumn>
                  <control name='addr1' type='text' tabindex='8' title='Address' maxlength='100' ref='/Instance/Entity/Addr1' />
                </displaycolumn>
                <displaycolumn>
                  <control name='addr2' type='text' tabindex='9' title='' maxlength='100' ref='/Instance/Entity/Addr2' />
                </displaycolumn>
                 <displaycolumn>
                  <control name='addr3' type='text' tabindex='10' title='' maxlength='100' ref='/Instance/Entity/Addr3' />
                </displaycolumn>
                <displaycolumn>
                  <control name='addr4' type='text' tabindex='11' title='' maxlength='100' ref='/Instance/Entity/Addr4' />
                </displaycolumn>
                <displaycolumn>
                  <control name='city' type='text' tabindex='12' title='City' maxlength='50' ref='/Instance/Entity/City' />
                  <control name='stateid' type='code' codetable='states' tabindex='13' title='State' ref='/Instance/Entity/StateId' />
                </displaycolumn>
                <displaycolumn>
                  <control name='zipcode' type='zip' tabindex='14' title='Zip/Postal Code' ref='/Instance/Entity/ZipCode' />
                  <control name='county' type='text' tabindex='15' title='County' maxlength='50' ref='/Instance/Entity/County' />
                </displaycolumn>
                <displaycolumn>
                  <control name='countrycode' type='code' codetable='COUNTRY' tabindex='16' title='Country'
				            ref='/Instance/Entity/CountryCode' />
                  <control name='sexcode' type='code' codetable='SEX_CODE' title='Sex' ref='/Instance/Entity/SexCode'/>
                  <control name='taxid' type='ssn' tabindex='17' title='Tax Id' maxlength='50' ref='/Instance/Entity/TaxId' />
                </displaycolumn>
                <displaycolumn>
                  <control name='phone1' type='phone' tabindex='18' title='Office Phone' maxlength='50' ref='/Instance/Entity/Phone1' />
                  <control name='phone2' type='phone' tabindex='19' title='Alt. Phone' maxlength='50' ref='/Instance/Entity/Phone2' />
                </displaycolumn>
                <displaycolumn>
                  <control name='faxnumber' type='phone' tabindex='20' title='Fax' maxlength='50' ref='/Instance/Entity/FaxNumber' />
                  <control name='emailtypecode' type='code' codetable='EMAIL_TYPE' tabindex='21' title='EMail Type'
				            ref='/Instance/Entity/EmailTypeCode' />
                </displaycolumn>
                <displaycolumn>
                  <control name='emailaddress' type='text' tabindex='22' title='EMail' maxlength='25' ref='/Instance/Entity/EmailAddress' />
                  <control name='parenteid' type='eidlookup' tabindex='23' title='Parent EID' tableid='' ref='/Instance/Entity/ParentEid' />
                  <control name='lastfirstname' type='hidden' title='' ref='/Instance/Entity/LastFirstName'/>
                </displaycolumn>
                <displaycolumn>
                  <control name='rmsysuser' type='customizedlistlookup' codetable='RM_SYS_USERS' tabindex='24' title='RISKMASTER Login'  ref='/Instance/Entity/RMUserId' />
                  <control name='title' type='text' tabindex='25' title='Title' maxlength='25' ref='/Instance/Entity/Title' />
		            <!--Added buy Amitosh for EFt Payments-->
		            <control name='hasEFtbankInfo' type='text' tabindex='23' title='Title' maxlength='25' ref='/Instance/Entity/hasEFtbankInfo' />
                </displaycolumn>
                <!-- abansal23 MITS 18759 : Not able to save Adjuster for any Claim Starts -->
                <displaycolumn>
                  <control name='dttmrcdadded' type='id' ref='/Instance/Entity/DttmRcdAdded' />
                  <control name='dttmrcdlastupd' type='id' ref='/Instance/Entity/DttmRcdLastUpd' />
                  <control name='updatedbyuser' type='id' ref='/Instance/Entity/UpdatedByUser' />
                  <control name='addedbyuser' type='id' ref='/Instance/Entity/AddedByUser' />
                  <control name='PrimaryAddressExpired' type='text'  ref='/Instance/Funds/PrimaryAddressExpired' /> <!--RMA-8753 nshah28(For Expired primary address)-->
                </displaycolumn>
                <!-- abansal23 MITS 18759 : Not able to save Adjuster for any Claim Ends -->
                <displaycolumn>
                  <control name='iscombinedPayee' type='text' tabindex='29' title='IsCombinedPayee' maxlength='25' ref='/Instance/Entity/iscombinedPayee'  />
                  <control name='bankaccntid' type='text' tabindex='29' title='BankAccountId' maxlength='25' ref='/Instance/Entity/bankaccntid'  />
                  <control name='currencytypecode' type='text' tabindex='29' title='CurrencyType' maxlength='25' ref='/Instance/Entity/currencytypecode'  />
                </displaycolumn>
                <!--Ankit Start : Financial Enhancement - Prefix and Suffix Changes-->
                <displaycolumn>
                    <control name='prefix' type='code' codetable='ENTITY_PREFIX' tabindex='30'  title='Prefix' ref='/Instance/Entity/Prefix' />
                    <control name='suffixcommon' type='code' codetable='ENTITY_SUFFIX' tabindex='31'  title='SuffixCommon' ref='/Instance/Entity/SuffixCommon' />
                    <control name='suffixlegal' type='text' tabindex='32' title='SuffixLegal' ref='/Instance/Entity/SuffixLegal'  />
                </displaycolumn>
                <!--Ankit End-->
                <displaycolumn>
                <control name='referencenumber' type='text' tabindex='30' title='Reference Number' maxlength='50' ref='/Instance/Entity/ReferenceNumber' />
                <!--<control name='hdEntityRoleIds' type='hidden' title='' ref='/Instance/Entity/EntityRoleID'/>-->
                </displaycolumn>
                  <displaycolumn>
      <control name='nametype' type='code' codetable='ENTITY_NAME_TYPE' title='Name Type' ref='/Instance/Entity/NameType' tabindex='31' />
    </displaycolumn>
              </group>
            </form>");
            return doc;
        }
        private XElement GetPropertyUnitXml()
        {
            XElement objEl=XElement.Parse(@"<?xml version='1.0'?>
                        <form name='property' title='Property' topbuttons='1' supp='property_unit_SUPP' onload='pageLoaded();'>
                        <toolbar>
                        <button type='save' title='Save'/>
                        <button type='delete' title='Delete Record'/>
                        <button type='lookup' title='Lookup Data'/>
                        <button type='attach' title='Attach Documents' name='attachdocuments'/>
                        <button type='diary' title='Diary'/>
                        <button type='mailmerge' title='Mail Merge'/>
                        <button type='recordsummary' title='Record Summary'/>
                        </toolbar>
                        <!--Property Info Tab-->11/9/2009
                        <group selected='1' name='propertyinfo' title='Property Info'>
                        <displaycolumn>
                            <control name='propertyid_cid' type='id' tabindex='0' ref='/Instance/PropertyUnit/PropertyId'/>
                            <control name='propertyid' type='text' firstfield='1' tabindex='1' title='Property ID' required='yes' ref='/Instance/PropertyUnit/Pin'/>
                            <control name='territorycode' type='code' codetable='TERRITORY'  tabindex='10' title='Territory Code' ref='/Instance/PropertyUnit/TerritoryCode'/>
                        </displaycolumn>
                        <displaycolumn>
	                        <!--aarya4 - MITS#18230 02/10/2010 (Description field changed from memo to text type:Start-->
                            <!--Neha Suresh Jain, 06/09/2010, added maxlength for description,City,Address1,Address2-->
                            <control name='description' type='text' tabindex='2' title='Description' maxlength='50' ref='/Instance/PropertyUnit/Description'/>
	                        <!--aarya4 - MITS#18230 02/10/2010 :End-->
                            <control name='categorycode' type='code' codetable='CATEGORY'  tabindex='11' title='Category Code' ref='/Instance/PropertyUnit/CategoryCode'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='addr1' type='text' tabindex='3' title='Address 1' required='yes' ref='/Instance/PropertyUnit/Addr1' maxlength='50'/>
                            <control name='appraisedvalue' type='currency' min='0' max='9999999999.99' tabindex='12' title='Appraised Value' ref='/Instance/PropertyUnit/AppraisedValue'/>      
                        </displaycolumn>
                        <displaycolumn>
                            <control name='addr2' type='text' tabindex='4' title='Address 2' ref='/Instance/PropertyUnit/Addr2' maxlength='50'/>
                            <control name='appraiseddate' type='date' tabindex='13' title='Appraised Date' ref='/Instance/PropertyUnit/AppraisedDate'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='addr3' type='text' tabindex='5' title='Address 3' ref='/Instance/PropertyUnit/Addr3' maxlength='50'/>                            
                        </displaycolumn>
                        <displaycolumn>
                            <control name='addr4' type='text' tabindex='6' title='Address 4' ref='/Instance/PropertyUnit/Addr4' maxlength='50'/>                            
                        </displaycolumn>
                        <displaycolumn>
                            <control name='city' type='text' tabindex='7' title='City' required='yes' ref='/Instance/PropertyUnit/City' maxlength='50'/>
                            <!--Start: Neha Suresh Jain,06/09/2010, change Appraisal Source to Appraised Source -->
                            <!-- <control name='appraisalsource' type='code' codetable='APPRAISAL_SOURCE' tabindex='12' title='Appraisal Source' ref='/Instance/PropertyUnit/AppraisalSourceCode'/> -->
                            <control name='appraisalsource' type='code' codetable='APPRAISAL_SOURCE' tabindex='13' title='Appraised Source' ref='/Instance/PropertyUnit/AppraisalSourceCode'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='stateid' type='code' codetable='states' tabindex='8' title='State' required='yes' ref='/Instance/PropertyUnit/StateId'/>
                            <control name='landvalue' type='currency' min='0' max='9999999999.99' tabindex='15' title='Land Value' ref='/Instance/PropertyUnit/LandValue'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='zipcode' type='zip' tabindex='9' title='Zip/Postal Code' required='yes' ref='/Instance/PropertyUnit/ZipCode'/>
                            <control name='replacementvalue' type='currency' min='0' max='9999999999.99' tabindex='14' title='Replacement Value' ref='/Instance/PropertyUnit/ReplacementValue'/>      
                        </displaycolumn>
                        <!--Neha Suresh Jain,06/08/2010, Country Control added and tab index changed accordingly for all controls-->
                        <displaycolumn>
                            <control name='countrycode' type='code' codetable='COUNTRY' tabindex='10' title='Country' ref='/Instance/PropertyUnit/CountryCode' />
                        </displaycolumn>
                        </group>
                        <!--COPE Data-->
                        <group name='copeinfo' title='COPE Data'>
                        <displaycolumn>
                            <control name='classofconstruction' type='code' codetable='CLASS_OF_CONSTRUCTION' tabindex='17' title='Class of Construction' ref='/Instance/PropertyUnit/ClassOfConstruction'/>
                            <control name='heatingsyscode' type='code' codetable='HEATING_SYSTEM' tabindex='24' title='Heating System' ref='/Instance/PropertyUnit/HeatingSysCode'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='yearofconstruction' type='numeric' min='1000' max='2147483647' tabindex='18' title='Year of Construction' ref='/Instance/PropertyUnit/YearOfConstruction'/>
                            <control name='coolingsyscode' type='code' codetable='COOLING_SYSTEM' tabindex='25' title='Cooling System' ref='/Instance/PropertyUnit/CoolingSysCode'/>      
                        </displaycolumn>
                        <displaycolumn>
                            <control name='squarefootage' type='numeric' min='0' max='2147483647' tabindex='19' title='Square Footage' ref='/Instance/PropertyUnit/SquareFootage'/>
                            <control name='firealarmcode' type='code' codetable='FIRE_ALARM' tabindex='26' title='Fire Alarm' ref='/Instance/PropertyUnit/FireAlarmCode'/>  
                        </displaycolumn>
                        <displaycolumn>
                            <control name='noofstories' type='numeric' min='0' max='2147483647' tabindex='20' title='No. of Stories' ref='/Instance/PropertyUnit/NoOfStories'/>
                            <control name='sprinklerscode' type='code' codetable='SPRINKLERS' tabindex='27' title='Sprinklers' ref='/Instance/PropertyUnit/SprinklersCode'/>  
                        </displaycolumn>
                        <displaycolumn>
                            <control name='avgstoryheight' type='numeric' min='0' tabindex='21' title='Avg. Story Height' ref='/Instance/PropertyUnit/AvgStoryHeight'/>
                            <control name='entryalarmcode' type='code' codetable='ENTRY_ALARM' tabindex='28' title='Entry Alarm' ref='/Instance/PropertyUnit/EntryAlarmCode'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='wallconstructioncode' type='code' codetable='WALL_CONSTRUCTION'  tabindex='22' title='Wall Construction' ref='/Instance/PropertyUnit/WallConstructionCode'/>
                            <control name='roofanchoringcode' type='code' codetable='ROOF_ANCHORING' tabindex='29'  title='Roof Anchoring' ref='/Instance/PropertyUnit/RoofAnchoringCode'/>      
                        </displaycolumn>
                        <displaycolumn>
                            <control name='roofconstructioncode' type='code' codetable='ROOF_CONSTRUCTION' tabindex='23' title='Roof Construction' ref='/Instance/PropertyUnit/RoofConstructionCode'/>
                            <control name='glassstrengthcode' type='code' codetable='GLASS_STRENGTH' tabindex='30' title='Strength of Glass' ref='/Instance/PropertyUnit/GlassStrengthCode'/>      
                        </displaycolumn>
                        </group>
                        <!--Other COPE Data, Optional-->
                        <group name='othercopeinfo' title='Optional COPE Data'>
                        <displaycolumn>
                            <control name='plotplanscode' type='code' codetable='PLOT_PLANS' tabindex='31' title='Plot Plans' ref='/Instance/PropertyUnit/PlotPlansCode'/>
                            <control name='gpsaltitude' min='0' max='2147483647' type='numeric' tabindex='34' title='GPS Altitude' ref='/Instance/PropertyUnit/GPSAltitude'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='floodzonecertCode' type='code' codetable='FLOOD_ZONE_CERT' tabindex='32' title='Flood Zone' ref='/Instance/PropertyUnit/FloodZoneCertCode'/>
                            <control name='gpslatitude' type='numeric' max='90.0' min='-90.0' tabindex='35' title='GPS Latitude' ref='/Instance/PropertyUnit/GPSLatitude'/>
                        </displaycolumn>
                        <displaycolumn>
                            <control name='earthquakezonecode' type='code' codetable='EARTHQUAKE_ZONE' tabindex='33' title='EarthQuake Zone' ref='/Instance/PropertyUnit/EarthquakeZoneCode'/>
                            <control name='gpslongitude' type='numeric' max='180.0' min='-180.0' tabindex='36' title='GPS Longitude' ref='/Instance/PropertyUnit/GPSLongitude'/>
                        </displaycolumn>
                        </group>
                        <section name='suppdata'/>
                        <internal name='SysCmd' type='hidden' ref='Instance/UI/FormVariables/SysCmd' value=''/>
                        <internal name='SysCmdConfirmSave' type='hidden' ref='Instance/UI/FormVariables/SysCmdConfirmSave' value=''/>
                        <internal name='SysCmdQueue' type='hidden' ref='Instance/UI/FormVariables/SysCmdQueue' value=''/>
                        <internal name='SysCmdText' type='hidden' ref='Instance/UI/FormVariables/SysCmdText' value='Navigate'/>
                        <internal name='SysClassName' type='hidden' ref='Instance/UI/FormVariables/SysClassName' value='PropertyUnit'/>
                        <internal name='SysSerializationConfig' type='hidden' ref='Instance/UI/FormVariables/SysSerializationConfig'>
                        <PropertyUnit>
                            <Supplementals/>
                        </PropertyUnit>
                        </internal>
                        <internal type='hidden' name='SysFormPForm' ref='Instance/UI/FormVariables/SysFormPForm' value=''/>
                        <internal type='hidden' name='SysFormPIdName' ref='Instance/UI/FormVariables/SysFormPIdName' value=''/>
                        <internal type='hidden' name='SysFormPId' ref='Instance/UI/FormVariables/SysFormPId' value=''/>
                        <internal type='hidden' name='SysPSid' ref='Instance/UI/FormVariables/SysPSid' value=''/>
                        <internal type='hidden' name='SysEx' ref='Instance/UI/FormVariables/SysEx' value=''/>
                        <internal type='hidden' name='SysFormName' ref='Instance/UI/FormVariables/SysFormName' value='propertyunit'/>
                        <internal type='hidden' name='SysFormIdName' ref='Instance/UI/FormVariables/SysFormIdName' value='propertyid'/>
                        <internal type='hidden' name='SysFormId' ref='Instance/UI/FormVariables/SysFormId' value=''/>
                        <internal type='hidden' name='SysSid' ref='Instance/UI/FormVariables/SysSid' value='24500'/>
                        <internal type='hidden' name='SysNotReqNew' ref='Instance/UI/FormVariables/SysNotReqNew' value=''/>
                        <internal name='SysViewType' type='hidden' ref='Instance/UI/FormVariables/SysViewType' value=''/>
                        <!--smahajan6 - Retrofit Base MITS #18134 in VACo code - 02/17/2010 :Start-->
                        <internal type='hidden' name='TabNameList' value=''/>
                        <internal type='hidden' name='GroupAssocFieldList' value='' />
                        <!--smahajan6 - Retrofit Base MITS #18134 in VACo code - 02/17/2010 :End-->
                        </form>");
            return objEl;
        }
        private XElement GetPolicyEnhXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8'?><?xml-stylesheet type='text/xsl' href='form.xsl'?>
                                            <form name='enhancepolicy' title='Policy Management' topbuttons='1' includefilename='csc-Theme/riskmaster/common/javascript/EnhPolicy.js'>
                                            <group selected='1' name='policyinfo'>	  
                                                <displaycolumn>
                                                  <control name='policyid' tabindex='0' type='id' ref='/Instance/PolicyEnh/PolicyId' />
	                                            <control name='PolicyQuoteName' firstfield='1' type='text' ref='/Instance/PolicyEnh/PolicyName' />
                                                </displaycolumn>
                                            </group>
                                            </form>");
            return obj;
        }
        private XElement GetControlRequestXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8' ?><?xml-stylesheet type='text/xsl' href='form2.xsl'?>
                <form name='Funds' title='Funds' topbuttons='1'>
	                <toolbar>
		                <button type='new' title='New' />
		                <button type='save' title='Save' />
		                <button type='movefirst' title='Move First' />
		                <button type='moveprevious' title='Move Previous' />
		                <button type='movenext' title='Move Next' />
		                <button type='movelast' title='Move Last' />
		                <button type='attach' title='Attach Documents' />
		                <button type='search' title='Search' />
		                <button type='diary' title='Diary' />
		                <button type='mailmerge' title='Mail Merge' />
		                <button type='recordsummary' title='Record Summary' />
	                </toolbar>
	                <group selected='1' name='Funds' title='Funds'>
		                <displaycolumn>
                      <control name='transid' type='id' value='0' ref='/Instance/Funds//TransId' />
			                <control name='ctlnumber' type='text' title='' ref='/Instance/Funds//CtlNumber'/>
                      <control name='checknumber' type='text' title='' ref='/Instance/Funds//TransNumber'/>
		                </displaycolumn>
                    <displaycolumn>
                      <control name='firstname' type='text' title='' ref='/Instance/Funds//FirstName'/>
                      <control name='lastname' type='text' title='' ref='/Instance/Funds//LastName'/>
                    </displaycolumn>
	                </group>
                </form>");
            return obj;
        }
        private XElement GetFundsXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8' ?><?xml-stylesheet type='text/xsl' href='form2.xsl'?>
                <form name='Funds' title='Funds' topbuttons='1'>
	                <toolbar>
		                <button type='new' title='New' />
		                <button type='save' title='Save' />
		                <button type='movefirst' title='Move First' />
		                <button type='moveprevious' title='Move Previous' />
		                <button type='movenext' title='Move Next' />
		                <button type='movelast' title='Move Last' />
		                <button type='attach' title='Attach Documents' />
		                <button type='search' title='Search' />
		                <button type='diary' title='Diary' />
		                <button type='mailmerge' title='Mail Merge' />
		                <button type='recordsummary' title='Record Summary' />
	                </toolbar>
	                <group selected='1' name='Funds' title='Funds'>
		                <displaycolumn>
			                <control name='txtSearch' type='text' title='' ref='/Instance/Funds//CtlNumber'/>
		                </displaycolumn>
	                </group>
                </form>
            ");
            return obj;
        }
        private XElement GetPolicyXml()
        { //Govind- 34144
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8'?><?xml-stylesheet type='text/xsl' href='form.xsl'?>
                <form name='policy' title='Policy Maintenance' sid='9000' topbuttons='1' supp='POLICY_SUPP'>
                              <toolbar>
                                <button type='new' title='New'/>
                                <button type='save' title='Save'/>
                                <button type='movefirst' title='Move First'/>
                                <button type='moveprevious' title='Move Previous'/>
                                <button type='movenext' title='Move Next'/>
                                <button type='movelast' title='Move Last'/>
                                <button type='delete' title='Delete Record'/>
                                <button type='lookup' title='Lookup Data'/>
                                <button type='attach' title='Attach Documents'/>
                                <button type='search' title='Search'/>
                                <button type='diary' title='Diary'/>
                                <button type='comments' title='Comments'/>
                                <button type='mailmerge' title='Mail Merge'/>
                                <button type='recordsummary' title='Record Summary'/>
                              </toolbar>
                              <group selected='1' name='policyinfo' title='Policy Information'>
                                <displaycolumn>
                                  <control name='policyid' type='id'  ref='/Instance/Policy/PolicyId'/>
                                  <control name='policyname' type='text' firstfield='1' tabindex='1' title='Policy Name' maxlength='20' required='yes' ref='/Instance/Policy/PolicyName'/>
                                  <control name='issuedate' type='date' tabindex='9' title='Issue Date' ref='/Instance/Policy/IssueDate'/>
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='policynumber' type='text' tabindex='2' title='Policy Number' maxlength='20' required='no' ref='/Instance/Policy/PolicyNumber'/>
                                  <control name='reviewdate' type='date' tabindex='11' title='Review Date' ref='/Instance/Policy/ReviewDate'/>
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='policystatuscode' type='code' codetable='POLICY_STATUS' tabindex='3' title='Policy Status' required='yes' ref='/Instance/Policy/PolicyStatusCode'/>
                                  <control name='renewaldate' type='date' tabindex='13' title='Renewal Date' ref='/Instance/Policy/RenewalDate'/>
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='premium' type='currency' tabindex='5' title='Premium' ref='/Instance/Policy/Premium'/>
                                  <control name='effectivedate' type='date' tabindex='15' title='Effective Date' required='yes' ref='/Instance/Policy/EffectiveDate'/>
                                </displaycolumn>
                                <displaycolumn>
	                              <control name='expirationdate' type='date' tabindex='17' title='Expiration Date' required='yes' ref='/Instance/Policy/ExpirationDate'/>
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='primarypolicyflg' type='checkbox' tabindex='7' title='Primary Policy' ref='/Instance/Policy/PrimaryPolicyFlg'/>
                                  <control name='canceldate' type='date' tabindex='19' title='Cancel Date' ref='/Instance/Policy/CancelDate'/>
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='triggerclaimflag' type='checkbox' tabindex='8' title='Claims-Made Coverage' ref='/Instance/Policy/TriggerClaimFlag'/>
                                </displaycolumn>
                              </group>
                             </form>
                            ");
            return obj;
        }
        private XElement GetEmployeeXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8'?>
                    <form name='employee' title='Employee' sid='14500' topbuttons='1' supp='EMP_SUPP'>
                      <toolbar>
                        <button type='new' title='New'/>
                        <button type='save' title='Save'/>
                        <button type='movefirst' title='Move First'/>
                        <button type='moveprevious' title='Move Previous'/>
                        <button type='movenext' title='Move Next'/>
                        <button type='movelast' title='Move Last'/>
                        <button type='delete' title='Delete Record'/>
                        <button type='lookup' title='Lookup Data'/>
                        <button type='search' title='Search'/>
                        <button type='attach' title='Attach Documents'/>
                        <button type='diary' title='Diary'/>

                        <button type='comments' title='Comments'/>
                        <button type='mailmerge' title='Mail Merge'/>
                        <button type='recordsummary' title='Record Summary'/>
                      </toolbar>
                      <group selected='1' name='employee' title='Employee' id='1'>
                        <displaycolumn>
                          <control id='1' name='employeeeid' tabindex='0' type='id' ref='/Instance/Employee/EmployeeEid'/>
                          <control id='2' name='entityid' tabindex='1' type='id' ref='/Instance/Employee/EmployeeEntity/EntityId'/>
                          <control id='100' name='entitydttmrcdadded' tabindex='0' type='text' ref='/Instance/Employee/EmployeeEntity/DttmRcdAdded'/>
                          <control id='101' name='entitydttmrcdupdated' tabindex='0' type='text' ref='/Instance/Employee/EmployeeEntity/DttmRcdLastUpd'/>
                          <control id='102' name='entityupdatedbyuser' tabindex='0' type='text' ref='/Instance/Employee/EmployeeEntity/UpdatedByUser'/>
                          <control id='103' name='entityaddedbyuser' tabindex='0' type='text' ref='/Instance/Employee/EmployeeEntity/AddedByUser'/>

                          <control id='3' name='employeenumber' type='text' firstfield='1' tabindex='2' title='Employee Number' maxlength='50' required='yes' ref='/Instance/Employee/EmployeeNumber'/>
                          <control id='7' name='abbreviation' type='text' tabindex='6' title='Abbreviation' maxlength='50' ref='/Instance/Employee/EmployeeEntity/Abbreviation'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='4' name='lastname' type='text' tabindex='3' title='Last Name' maxlength='255' required='yes' ref='/Instance/Employee/EmployeeEntity/LastName'/>
                          <control id='6' name='alsoknownas' type='text' tabindex='5' title='Also Known as' maxlength='50' ref='/Instance/Employee/EmployeeEntity/AlsoKnownAs'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='5' name='firstname' type='text' tabindex='4' title='First Name' maxlength='255' ref='/Instance/Employee/EmployeeEntity/FirstName'/>
                          <control id='15' name='taxid' type='ssn' tabindex='16' title='Soc.Sec No.' maxlength='50' ref='/Instance/Employee/EmployeeEntity/TaxId'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='16' name='birthdate' type='date' tabindex='17' title='Date of Birth' ref='/Instance/Employee/EmployeeEntity/BirthDate'/>
                          <!--Added by Shivendu for MITS 11635-->
                          <control id = '111' name='middlename' type='text' tabindex='5' title='Middle Name' maxlength='255' ref='/Instance/Employee/EmployeeEntity/MiddleName' />
                        </displaycolumn>
                        <displaycolumn>
                          <control id='8' name='addr1' type='text' tabindex='7' title='Address' maxlength='100' ref='/Instance/Employee/EmployeeEntity/Addr1'/>
                          <control id='17' name='phone1' type='phone' tabindex='18' title='Office Phone' maxlength='50' ref='/Instance/Employee/EmployeeEntity/Phone1'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='9' name='addr2' type='text' tabindex='8' title='' maxlength='100' ref='/Instance/Employee/EmployeeEntity/Addr2'/>
                          <control id='18' name='phone2' type='phone' tabindex='19' title='Home Phone' maxlength='50' ref='/Instance/Employee/EmployeeEntity/Phone2'/>
                        </displaycolumn>
                         <displaycolumn>
                          <control id='9' name='addr3' type='text' tabindex='9' title='' maxlength='100' ref='/Instance/Employee/EmployeeEntity/Addr3'/>                          
                        </displaycolumn>
                        <displaycolumn>
                          <control id='9' name='addr4' type='text' tabindex='10' title='' maxlength='100' ref='/Instance/Employee/EmployeeEntity/Addr4'/>                          
                        </displaycolumn>
                        <displaycolumn>
                          <control id='10' name='city' type='text' tabindex='11' title='City' maxlength='50' ref='/Instance/Employee/EmployeeEntity/City'/>
                          <control id='19' name='faxnumber' type='phone' tabindex='20' title='Fax' maxlength='50' ref='/Instance/Employee/EmployeeEntity/FaxNumber'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='11' name='stateid' type='code' codetable='states' tabindex='12' title='State' ref='/Instance/Employee/EmployeeEntity/StateId'/>
                          <control id='12' name='zipcode' type='zip' tabindex='13' title='Zip/Postal Code' ref='/Instance/Employee/EmployeeEntity/ZipCode'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='14' name='countrycode' type='code' codetable='COUNTRY' tabindex='15' title='Country' ref='/Instance/Employee/EmployeeEntity/CountryCode'/>
                          <control id='20' name='sexcode' type='code' codetable='SEX_CODE' tabindex='21' title='Sex' ref='/Instance/Employee/EmployeeEntity/SexCode'/>
                          <!--Added by Shivendu for MITS 11635-->
                          <control id = '112' name='title' type='text' tabindex='28' title='Title' maxlength='25' ref='/Instance/Employee/EmployeeEntity/Title' />
                        </displaycolumn>
                        <displaycolumn>
                          <control id='13' name='county' type='text' tabindex='14' title='County' maxlength='50' ref='/Instance/Employee/EmployeeEntity/County'/>
                          <control id='21' name='maritalstatcode' type='code' codetable='MARITAL_STATUS' tabindex='22' title='Marital Status' ref='/Instance/Employee/MaritalStatCode'/>
                          <control id='21' name='maritalstatcode_empinfo' type='code' codetable='MARITAL_STATUS' tabindex='22' title='Marital Status' ref='/Instance/Employee/MaritalStatCode'/>
                        </displaycolumn>
                      </group>
                      <group name='employmentinfo' title='Employment Info' id='2'>
                        <displaycolumn>
                          <control id='22' name='datehired' type='date' firstfield='1' tabindex='0' title='Date Hired' ref='/Instance/Employee/DateHired'/>
                          <control id='22' name='datehired1' type='date' firstfield='1' tabindex='0' title='Date Hired' ref='/Instance/Employee/DateHired'/>
                          <control id='22' name='datehired2' type='date' firstfield='1' tabindex='0' title='Date Hired' ref='/Instance/Employee/DateHired'/>
                          <control id='23' name='termdate' type='date' tabindex='1' title='Termination Date' ref='/Instance/Employee/TermDate'/>
                          <control id='23' name='termdate1' type='date' tabindex='1' title='Termination Date' ref='/Instance/Employee/TermDate'/>
                          <control id='23' name='termdate2' type='date' tabindex='1' title='Termination Date' ref='/Instance/Employee/TermDate'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='24' name='positioncode' type='code' codetable='POSITIONS' tabindex='2' title='Position Code' ref='/Instance/Employee/PositionCode'/>
                          <control id='24' name='positioncode1' type='code' codetable='POSITIONS' tabindex='2' title='Position Code' ref='/Instance/Employee/PositionCode'/>
                          <control id='24' name='positioncode2' type='code' codetable='POSITIONS' tabindex='2' title='Position Code' ref='/Instance/Employee/PositionCode'/>
                          <control id='32' name='activeflag' type='checkbox' tabindex='10' title='Active' ref='/Instance/Employee/ActiveFlag'/>
                          <control id='32' name='activeflag1' type='checkbox' tabindex='10' title='Active' ref='/Instance/Employee/ActiveFlag'/>
                          <control id='32' name='activeflag2' type='checkbox' tabindex='10' title='Active' ref='/Instance/Employee/ActiveFlag'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='25' name='deptassignedeid' type='orgh' tabindex='3' title='Department' required='yes' ref='/Instance/Employee/DeptAssignedEid'/>
                          <control id='25' name='deptassignedeid1' type='orgh' tabindex='3' title='Department' required='yes' ref='/Instance/Employee/DeptAssignedEid'/>
                          <control id='25' name='deptassignedeid2' type='orgh' tabindex='3' title='Department' required='yes' ref='/Instance/Employee/DeptAssignedEid'/>
                          <control id='33' name='fulltimeflag' type='checkbox' tabindex='11' title='Full Time Employee' ref='/Instance/Employee/FullTimeFlag'/>
                          <control id='33' name='fulltimeflag1' type='checkbox' tabindex='11' title='Full Time Employee' ref='/Instance/Employee/FullTimeFlag'/>
                          <control id='33' name='fulltimeflag2' type='checkbox' tabindex='11' title='Full Time Employee' ref='/Instance/Employee/FullTimeFlag'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='26' name='supervisoreid' type='eidlookup' tableid='EMPLOYEES' tabindex='4' title='Supervisor' eid='0' ref='/Instance/Employee/SupervisorEid'/>
                          <control id='26' name='supervisoreid1' type='eidlookup' tableid='EMPLOYEES' tabindex='4' title='Supervisor' eid='0' ref='/Instance/Employee/SupervisorEid'/>
                          <control id='26' name='supervisoreid2' type='eidlookup' tableid='EMPLOYEES' tabindex='4' title='Supervisor' eid='0' ref='/Instance/Employee/SupervisorEid'/>
                          <control id='34' name='exemptstatusflag' type='checkbox' tabindex='12' title='Exempt' ref='/Instance/Employee/ExemptStatusFlag'/>
                          <control id='34' name='exemptstatusflag1' type='checkbox' tabindex='12' title='Exempt' ref='/Instance/Employee/ExemptStatusFlag'/>
                          <control id='34' name='exemptstatusflag2' type='checkbox' tabindex='12' title='Exempt' ref='/Instance/Employee/ExemptStatusFlag'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='27' name='paytypecode' type='code' codetable='PAY_TYPES' tabindex='5' title='Pay Type' ref='/Instance/Employee/PayTypeCode'/>
                          <control id='27' name='paytypecode1' type='code' codetable='PAY_TYPES' tabindex='5' title='Pay Type' ref='/Instance/Employee/PayTypeCode'/>
                          <control id='27' name='paytypecode2' type='code' codetable='PAY_TYPES' tabindex='5' title='Pay Type' ref='/Instance/Employee/PayTypeCode'/>
                          <control id='35' name='noofexemptions' type='numeric' tabindex='13' title='No of Exemptions' ref='/Instance/Employee/NoOfExemptions'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='28' name='payamount' type='text' tabindex='6' title='Pay Amount' ref='/Instance/Employee/PayAmount'/>
                          <control id='30' name='weeklyhours' type='numeric' tabindex='8' title='Hours Per Week' ref='/Instance/Employee/WeeklyHours'/>
                          <control id='30' name='weeklyhours1' type='numeric' tabindex='8' title='Hours Per Week' ref='/Instance/Employee/WeeklyHours'/>
                          <control id='30' name='weeklyhours2' type='numeric' tabindex='8' title='Hours Per Week' ref='/Instance/Employee/WeeklyHours'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='29' name='hourlyrate' type='text' tabindex='7' title='Hourly Rate' ref='/Instance/Employee/HourlyRate'/>
                          <control id='29' name='hourlyrate1' type='text' tabindex='7' title='Hourly Rate' ref='/Instance/Employee/HourlyRate'/>
                          <control id='29' name='hourlyrate2' type='text' tabindex='7' title='Hourly Rate' ref='/Instance/Employee/HourlyRate'/>
                          <control id='31' name='weeklyrate' type='text' tabindex='9' title='Weekly Rate' ref='/Instance/Employee/WeeklyRate'/>
                          <control id='31' name='weeklyrate1' type='text' tabindex='9' title='Weekly Rate' ref='/Instance/Employee/WeeklyRate'/>
                          <control id='31' name='weeklyrate2' type='text' tabindex='9' title='Weekly Rate' ref='/Instance/Employee/WeeklyRate'/>
                          <!--Added by Shivendu for MITS 11635-->
                          <control id='113' name='monthlyrate' type='currency' tabindex='43' title='Monthly Rate' ref='/Instance/Employee/MonthlyRate' />
                          <!--Added by Shivendu for MITS 11635-->
                          <control id= '114' name='jobclassification' type='code' codetable='JOB_CLASS' tabindex='57' title='Job Class' ref='/Instance/Employee/JobClassCode' />
                        </displaycolumn>
                        <displaycolumn>
                          <control id='36' name='worksunflag' type='checkbox' tabindex='15' title='Sunday' ref='/Instance/Employee/WorkSunFlag'/>
                          <control id='36' name='worksunflag1' type='checkbox' tabindex='15' title='Sunday' ref='/Instance/Employee/WorkSunFlag'/>
                          <control id='36' name='worksunflag2' type='checkbox' tabindex='15' title='Sunday' ref='/Instance/Employee/WorkSunFlag'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='37' name='workmonflag' type='checkbox' tabindex='16' title='Monday' ref='/Instance/Employee/WorkMonFlag'/>
                          <control id='37' name='workmonflag1' type='checkbox' tabindex='16' title='Monday' ref='/Instance/Employee/WorkMonFlag'/>
                          <control id='37' name='workmonflag2' type='checkbox' tabindex='16' title='Monday' ref='/Instance/Employee/WorkMonFlag'/>
                          <control id='38' name='worktueflag' type='checkbox' tabindex='17' title='Tuesday' ref='/Instance/Employee/WorkTueFlag'/>
                          <control id='38' name='worktueflag1' type='checkbox' tabindex='17' title='Tuesday' ref='/Instance/Employee/WorkTueFlag'/>
                          <control id='38' name='worktueflag2' type='checkbox' tabindex='17' title='Tuesday' ref='/Instance/Employee/WorkTueFlag'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='39' name='workwedflag' type='checkbox' tabindex='18' title='Wednesday' ref='/Instance/Employee/WorkWedFlag'/>
                          <control id='39' name='workwedflag1' type='checkbox' tabindex='18' title='Wednesday' ref='/Instance/Employee/WorkWedFlag'/>
                          <control id='39' name='workwedflag2' type='checkbox' tabindex='18' title='Wednesday' ref='/Instance/Employee/WorkWedFlag'/>
                          <control id='40' name='workthuflag' type='checkbox' tabindex='19' title='Thursday' ref='/Instance/Employee/WorkThuFlag'/>
                          <control id='40' name='workthuflag1' type='checkbox' tabindex='19' title='Thursday' ref='/Instance/Employee/WorkThuFlag'/>
                          <control id='40' name='workthuflag2' type='checkbox' tabindex='19' title='Thursday' ref='/Instance/Employee/WorkThuFlag'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='41' name='workfriflag' type='checkbox' tabindex='20' title='Friday' ref='/Instance/Employee/WorkFriFlag'/>
                          <control id='41' name='workfriflag1' type='checkbox' tabindex='20' title='Friday' ref='/Instance/Employee/WorkFriFlag'/>
                          <control id='41' name='workfriflag2' type='checkbox' tabindex='20' title='Friday' ref='/Instance/Employee/WorkFriFlag'/>
                          <control id='42' name='worksatflag' type='checkbox' tabindex='21' title='Saturday' ref='/Instance/Employee/WorkSatFlag'/>
                          <control id='42' name='worksatflag1' type='checkbox' tabindex='21' title='Saturday' ref='/Instance/Employee/WorkSatFlag'/>
                          <control id='42' name='worksatflag2' type='checkbox' tabindex='21' title='Saturday' ref='/Instance/Employee/WorkSatFlag'/>
                        </displaycolumn>
                      </group>
                      <group selected='1' name='employeeinfo' title='Employee Info' id='3'>
                        <displaycolumn>
                          <control id='43' name='driverslicno' type='text' firstfield='1' tabindex='0' title='License Number' maxlength='50' ref='/Instance/Employee/DriversLicNo'/>
                          <control id='52' name='emailtypecode' type='code' codetable='EMAIL_TYPE' tabindex='9' title='EMail Type' maxlength='50' ref='/Instance/Employee/EmployeeEntity/EmailTypeCode'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='44' name='driverslictypecode' type='code' codetable='LICENSE_TYPE_CODE' tabindex='1' title='License Type' maxlength='50' ref='/Instance/Employee/Driverslictypecode'/>
                          <control id='53' name='emailaddress' type='text' tabindex='10' title='EMail Address' maxlength='50' ref='/Instance/Employee/EmployeeEntity/EmailAddress'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='45' name='drivlicstate' type='code' codetable='STATES' tabindex='2' title='License Jurisdiction' maxlength='50' ref='/Instance/Employee/DrivlicState'/>
                          <control id='54' name='costcentercode' type='code' codetable='COST_CENTER' tabindex='11' title='Cost Center' ref='/Instance/Employee/EmployeeEntity/CostCenterCode'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='46' name='datedriverslicexp' type='date' tabindex='3' title='Expiration Date' ref='/Instance/Employee/DateDriverslicexp'/>
                          <control id='55' name='workpermitnumber' type='text' tabindex='12' title='Work Permit #' maxlength='50' ref='/Instance/Employee/WorkPermitNumber'/>
                          <control id='55' name='workpermitnumber_empinfo' type='text' tabindex='12' title='Work Permit #' maxlength='50' ref='/Instance/Employee/WorkPermitNumber'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='47' name='drivlicrstrctcode' type='code' codetable='LIC_RESTRICTION' tabindex='4' title='Restrictions' maxlength='50' ref='/Instance/Employee/DrivlicRstrctcode'/>
                          <control id='56' name='workpermitdate' type='date' tabindex='13' title='Work Permit Date' ref='/Instance/Employee/WorkPermitDate'/>
                          <control id='56' name='workpermitdate_empinfo' type='date' tabindex='13' title='Work Permit Date' ref='/Instance/Employee/WorkPermitDate'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='48' name='ncciclasscode' type='code' codetable='NCCI_CLASS_CODE' tabindex='5' title='NCCI Class' maxlength='50' ref='/Instance/Employee/NcciClassCode'/>
                          <control id='48' name='ncciclasscode_empinfo' type='code' codetable='NCCI_CLASS_CODE' tabindex='5' title='NCCI Class' maxlength='50' ref='/Instance/Employee/NcciClassCode'/>
                          <control id='57' name='numofviolations' type='numeric' tabindex='14' title='Number of Violations' ref='/Instance/Employee/NumOfViolations'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='49' name='lastverifieddate' type='date' tabindex='6' title='Date Last Verified' ref='/Instance/Employee/LastVerifiedDate'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='50' name='dateofdeath' type='date' tabindex='7' title='Date of Death' ref='/Instance/Employee/DateOfDeath'/>
                          <control id='50' name='dateofdeath_empinfo' type='date' tabindex='7' title='Date of Death' ref='/Instance/Employee/DateOfDeath'/>
                        </displaycolumn>
                        <displaycolumn>
                          <control id='51' name='insurableflag' type='checkbox' tabindex='8' title='Insurable' ref='/Instance/Employee/InsurableFlag'/>
                          <control name='lastfirstname' type='hidden' title='' ref='/Instance/Employee/EmployeeEntity/LastFirstName'/>
                        </displaycolumn>
                      </group>
                      <section name='suppdata'/>
                      <security sid='14500' parentid='1'>
                        <viewcontrol vid='14530' parentid='14500' groupid='2' controlids='26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42'/>
                      </security>
                      <button param='SysFormName=violation&amp;SysCmd=1&amp;SysEx=/Instance/Employee/EmployeeEid' title='Violations' name='btnViol' countref='/Instance/Employee/EmpXViolationList/@count'/>
                      <button param='SysFormName=dependent&amp;SysCmd=1&amp;SysEx=/Instance/Employee/EmployeeEid' title='Dependents' name='btnDep' countref='/Instance/Employee/EmpXDependentList/@count'/>
                    </form>
");
            return obj;
        }
        private XElement GetEventXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8'?><?xml-stylesheet type='text/xsl' href='form2.xsl'?><form name='event' title='Event' sid='11000' topbuttons='1' supp='EVENT_SUPP'>
                <subtitle valuenodepath='/Instance/UI/FormVariables/SysExData/SubTitle'/>
	                <toolbar>
		                <button type='new' title='New'/>
		                <button type='save' title='Save'/>
		                <button type='movefirst' title='Move First'/>
		                <button type='moveprevious' title='Move Previous'/>
		                <button type='movenext' title='Move Next'/>
		                <button type='movelast' title='Move Last'/>
		                <button type='delete' title='Delete Record'/>
		                <button type='attach' title='Attach Documents'/>
		                <button type='esumm' title='Executive Summary'/>
		                <button type='search' title='Search'/>
		                <button type='lookup' title='Lookup Data'/>
		                <button type='filtereddiary' title='View Record Diaries'/>
		                <button type='diary' title='Diary'/>
		                <button type='comments' title='Comments'/>
		                <button type='mailmerge' title='Mail Merge'/>
		                <button type='recordsummary' title='Record Summary'/>
		                <button type='commentsummary' title='Claim Comment Summary'/>
		                <button type='eventexplorer' title='Quick Summary'/>
		                <button type='sendmail' title='Send Mail'/>
	                </toolbar>

	                <group name='eventinfo' title='Event Info' selected='1'>
		                <displaycolumn>
			                <control name='eventid' tabindex='0' type='id' ref='/Instance/Event/EventId'/>
			                <control name='eventnumber' type='text' tabindex='10' firstfield='1' title='Event Number' maxlength='50' required='yes' value='0' ref='/Instance/Event/EventNumber'/>
			                <control name='datereported' type='date' tabindex='1' title='Date Reported' required='yes' ref='/Instance/Event/DateReported'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='eventtypecode' type='code' tabindex='2' codetable='EVENT_TYPE' title='Event Type' required='yes' codeid='' ref='/Instance/Event/EventTypeCode'/>
			                <control name='timereported' type='time' tabindex='11' title='Time Reported' required='yes' ref='/Instance/Event/TimeReported'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='dateofevent' type='date' tabindex='3' title='Date Of Event' required='yes' ref='/Instance/Event/DateOfEvent'/>
			                <control name='injfromdate' type='date' tabindex='12' title='Injury From' ref='/Instance/Event/InjuryFromDate'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='timeofevent' type='time' tabindex='4' title='Time Of Event' required='yes' ref='/Instance/Event/TimeOfEvent'/>
			                <control name='injtodate' type='date' tabindex='13' title='Injury To' ref='/Instance/Event/InjuryToDate'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='eventstatuscode' type='code' tabindex='5' codetable='EVENT_STATUS' title='Event Status' required='yes' codeid='' ref='/Instance/Event/EventStatusCode'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='eventindcode' type='code' tabindex='6' codetable='EVENT_INDICATOR' title='Event Indicator' codeid='' ref='/Instance/Event/EventIndCode'/>
		                </displaycolumn>
		                <displaycolumn>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='depteid' type='orgh' tabindex='8' title='Department' required='yes' codeid='' ref='/Instance/Event/DeptEid'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='deptinvolvedeid' type='orgh' tabindex='9' title='Department Involved' codeid='' ref='/Instance/Event/DeptInvolvedEid'/>
		                </displaycolumn>
	                </group>
	                <group name='eventdetail' title='Event Detail'>
		                <displaycolumn>
			                <control name='locationareadesc' type='memo' tabindex='0' firstfield='1' title='Location Description' cols='30' rows='5' ref='/Instance/Event/LocationAreaDesc'/>
			                <control name='eventdescription' type='memo' tabindex='16' title='Event Description' cols='30' rows='5' readonly='true' ref='/Instance/Event/EventDescription'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='addr1' type='text' tabindex='2' title='Location Address' ref='/Instance/Event/Addr1' maxlength='100'/>
			                <control name='onpremiseflag' type='checkbox' tabindex='1' title='Event On Premise' ref='/Instance/Event/OnPremiseFlag'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='addr2' type='text' tabindex='3' title='' ref='/Instance/Event/Addr2' maxlength='100'/>
			                <control name='primaryloccode' type='code' tabindex='11' codetable='PRIMARY_LOCATION' title='Primary Location' codeid='' ref='/Instance/Event/PrimaryLocCode'/>
		                </displaycolumn>
                       <displaycolumn>
			                <control name='addr3' type='text' tabindex='4' title='' ref='/Instance/Event/Addr3' maxlength='100'/>
                        </displaycolumn>
                        <displaycolumn>
			                <control name='addr4' type='text' tabindex='5' title='' ref='/Instance/Event/Addr4' maxlength='100'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='city' type='text' tabindex='6' title='City' ref='/Instance/Event/City'/>
			                <control name='locationtypecode' type='code' tabindex='12' codetable='LOCATION_TYPE' title='Location Type' codeid='' ref='/Instance/Event/LocationTypeCode'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='stateid' type='code' tabindex='7' codetable='states' title='State' codeid='' ref='/Instance/Event/StateId'/>
			                <control name='causecode' type='code' tabindex='13' codetable='CAUSE_CODE' title='Cause Code' ref='/Instance/Event/CauseCode'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='zipcode' type='zip' tabindex='8' title='Zip' ref='/Instance/Event/ZipCode'/>
			                <control name='noofinjuries' type='numeric' tabindex='14' title='Number of Injuries' ref='/Instance/Event/NoOfInjuries'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='countyofinjury' type='text' tabindex='9' title='County of Injury' ref='/Instance/Event/CountyOfInjury'/>
			                <control name='nooffatalities' type='numeric' tabindex='15' title='Number Of Fatalities' ref='/Instance/Event/NoOfFatalities'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='countrycode' type='code' codetable='COUNTRY' tabindex='10' title='Country' codeid='' ref='/Instance/Event/CountryCode'/>
		                </displaycolumn>
	                </group>
	                <group name='followup' title='Follow Up'>
		                <displaycolumn>
			                <control name='datetofollowup' type='date' tabindex='0' firstfield='1' title='Follow up Date' ref='/Instance/Event/DateToFollowUp'/>
			                <control name='treatmentgiven' type='checkbox' tabindex='5' title='Treatment Given' ref='/Instance/Event/TreatmentGiven'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='datephysadvised' type='date' tabindex='1' title='Date Physician Advised' ref='/Instance/Event/DatePhysAdvised'/>
			                <control name='relsigned' type='checkbox' tabindex='6' title='Release Signed' ref='/Instance/Event/ReleaseSigned'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='timephysadvised' type='time' tabindex='2' title='Time Physician Advised' ref='/Instance/Event/TimePhysAdvised'/>
			                <control name='deptheadadvised' type='checkbox' tabindex='7' title='Department Head Advised' ref='/Instance/Event/DeptHeadAdvised'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='datecarriernotif' type='date' tabindex='3' title='Carrier Notified Date' ref='/Instance/Event/DateCarrierNotif'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='physnotes' type='memo' tabindex='4' title='Physician Notes' cols='30' rows='5' ref='/Instance/Event/PhysNotes'/>
			                <control name='actionslist' type='codelist' tabindex='8' codetable='ACTION_CODE' title='Actions' ref='/Instance/Event/EventXAction'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='outcomelist' type='codelist' tabindex='9' codetable='OUTCOME_CODE' title='Outcome' ref='/Instance/Event/EventXOutcome'/>
		                </displaycolumn>
	                </group>
	                <group name='qualitymanagement' title='Quality Management'>
		                <displaycolumn>
			                <control name='medcasenumber' type='text' tabindex='0' firstfield='1' title='Case Number' ref='/Instance/Event/EventQM/MedCaseNumber'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='medfilecode' type='code' tabindex='1' codetable='QM_MED_FILES' title='Med. File/Category' ref='/Instance/Event/EventQM/MedFileCode'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='medindcode' type='code' tabindex='2' codetable='QM_MED_INDICATORS' title='Med. Indicator' ref='/Instance/Event/EventQM/MedIndCode'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='initialreview' type='href' tabindex='3' param='SysFormName=qminitialreview&amp;SysFormIdName=eventid&amp;SysEx=/Instance/Event/EventId&amp;SysCmd=1' title='Initial Review' style='LightBold'/>
			                </displaycolumn>
		                <displaycolumn>
			                <control name='physicianadvisorreview' type='href' tabindex='4' param='SysFormName=qmphysadvreview&amp;SysEx=/Instance/Event/EventId&amp;SysCmd=1' title='Physician Advisor Review' style='LightBold'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='committeedepartmentreview' type='href' tabindex='5' param='SysFormName=qmcommdeptreview&amp;SysEx=/Instance/Event/EventId&amp;SysCmd=1' title='Committee/Department Review' style='LightBold'/>
		                </displaycolumn>
		                <displaycolumn>
			                <control name='qualitymanagerreview' type='href' tabindex='6' param='SysFormName=qmqualmgrreview&amp;SysEx=/Instance/Event/EventId&amp;SysCmd=1' title='Quality Manager Review' style='LightBold'/>
		                </displaycolumn>
	                </group>
	                <section name='suppdata'/>
                  <button param='SysFormName=personinvolvedlist&amp;SysCmd=1&amp;SysViewType=controlsonly&amp;SysEx=/Instance/Event/EventId | /Instance/Event/EventNumber' title='Persons Involved' name='btnPI' countref='/Instance/Event/PiList/@count'/>
	                <button param='SysFormName=osha&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=eventid&amp;SysEx=/Instance/Event/EventId | /Instance/Claim/ClaimNumber' title='OSHA' name='btnOSHA'/>
	                <button param='SysFormName=eventdatedtext&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=evdtrowid&amp;SysEx=/Instance/Event/EventId | /Instance/Event/EventNumber' title='Dated Text' name='btnDatedText' countref='/Instance/Event/EventXDatedTextList/@count'/>
                  <button param='SysFormName=claimlist&amp;SysCmd=1&amp;SysViewType=controlsonly&amp;SysFormIdName=eventid&amp;SysEx=/Instance/Event/EventId | /Instance/Event/EventNumber' title='Claims' name='btnClaim' countref='/Instance/Event/ClaimList/@count'/>
	                <button param='SysFormName=fallinfo&amp;SysEx=/Instance/Event/EventNumber | /Instance/Event/EventId&amp;SysCmd=1' title='Fall Info' name='btnFallInfo'/>
	                <button param='SysFormName=medwatch&amp;SysEx=/Instance/Event/EventNumber | /Instance/Event/EventId&amp;SysCmd=1' title='MED Watch' name='btnMedWatch'/>
	                <button param='SysFormName=sentinel&amp;SysEx=/Instance/Event/EventNumber | /Instance/Event/EventId&amp;SysCmd=1' title='Sentinel' name='btnSentinel'/>
                </form>
            ");
            return obj;
        }
        private XElement GetVehicleXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8'?>
                        <form name='vehicle' title='Vehicle' topbuttons='1' supp='VEHICLE_SUPP' onload='pageLoaded();onLeaseVehicle();'>
                          <toolbar>
                            <button type='new' title='New'/>
                            <button type='save' title='Save'/>
                            <button type='movefirst' title='Move First'/>
                            <button type='moveprevious' title='Move Previous'/>
                            <button type='movenext' title='Move Next'/>
                            <button type='movelast' title='Move Last'/>
                            <button type='delete' title='Delete Record'/>
                            <button type='lookup' title='Lookup Data'/>
                            <button type='attach' title='Attach Documents'/>
                            <button type='search' title='Search'/>
                            <button type='diary' title='Diary'/>
                            <button type='mailmerge' title='Mail Merge'/>
                            <button type='recordsummary' title='Record Summary'/>
                          </toolbar>
                          <group selected='1' name='vehicleinfo' title='Vehicle Info'>
                            <displaycolumn>
                              <control name='unitid' tabindex='0' type='id' ref='/Instance/Vehicle/UnitId'/>
                              <control name='vin' type='text' firstfield='1' tabindex='1' title='Vehicle ID' required='yes' ref='/Instance/Vehicle/Vin'/>
                              <control name='leaseflag' type='checkbox' tabindex='9' title='Lease' onclick='onLeaseVehicle();' ref='/Instance/Vehicle/LeaseFlag'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='vehiclemake' type='text' tabindex='2' title='Vehicle Make' required='yes' ref='/Instance/Vehicle/VehicleMake'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='vehiclemodel' type='text' tabindex='3' title='Vehicle Model' required='yes' ref='/Instance/Vehicle/VehicleModel'/>
                              <control name='leasenumber' type='text' tabindex='11' title='Lease Number' ref='/Instance/Vehicle/LeaseNumber'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='vehicleyear' type='numeric' tabindex='4' title='Vehicle Year' ref='/Instance/Vehicle/VehicleYear'/>
                              <control name='leaseterm' type='numeric' tabindex='12' title='Lease Term (Months)' ref='/Instance/Vehicle/LeaseTerm'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='staterowid' type='code' codetable='states' tabindex='5' title='State' ref='/Instance/Vehicle/StateRowId'/>
                              <control name='leaseexpiredate' type='date' tabindex='13' title='Lease Expiration' ref='/Instance/Vehicle/LeaseExpireDate'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='licensenumber' type='text' tabindex='6' title='License Number' ref='/Instance/Vehicle/LicenseNumber'/>
                              <control name='leaseamount' type='currency' tabindex='14' title='Lease Amount' ref='/Instance/Vehicle/LeaseAmount'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='licensernwldate' type='date' tabindex='7' title='Renewal Date' ref='/Instance/Vehicle/LicenseRnwlDate'/>
                              <control name='originalcost' type='currency' tabindex='15' title='Original Cost' ref='/Instance/Vehicle/OriginalCost'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='purchasedate' type='date' tabindex='8' title='Purchase Date' ref='/Instance/Vehicle/PurchaseDate'/>
                            </displaycolumn>
                          </group>
                          <group  name='vehicledetailinfo' title='Vehicle Detail'>
                            <displaycolumn>
                              <control name='unittypecode' type='code' codetable='UNIT_TYPE_CODE' firstfield='1' tabindex='0' title='Unit Type' ref='/Instance/Vehicle/UnitTypeCode'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='homedepteid' type='orgh' tabindex='1' title='Department' ref='/Instance/Vehicle/HomeDeptEid'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='grossweight' type='numeric' tabindex='2' title='Weight' ref='/Instance/Vehicle/GrossWeight'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='insurancecoverage' type='memo' tabindex='3' title='Insurance Info' cols='30' rows='5' ref='/Instance/Vehicle/InsuranceCoverage'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='deductible' type='currency' tabindex='4' title='Deductible' ref='/Instance/Vehicle/Deductible'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='disposaldate' type='date' tabindex='5' title='Disposal Date' ref='/Instance/Vehicle/DisposalDate'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='lastservicedate' type='date' tabindex='6' title='Last Service Date' ref='/Instance/Vehicle/LastServiceDate'/>
                            </displaycolumn>
                            <displaycolumn>
                              <control name='typeofservice' type='memo' tabindex='7' title='Type Of Service' cols='30' rows='5' ref='/Instance/Vehicle/TypeOfService'/>
                            </displaycolumn>
                          </group><section name='suppdata'/>
                        </form>
            ");
            return obj;
        }
        private XElement GetClaimXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' encoding='UTF-8'?>
                            <form name='claimgc' title='General Claim' topbuttons='1' supp='CLAIM_SUPP'>
                              <group name='claiminfo' title='Claim Info' selected='1'>
                                <displaycolumn>
                                  <control name='eventid' type='id' tabindex='0' value='0' ref='/Instance/Claim/EventId' />
                                  <control name='claimid' type='id' tabindex='1' value='0' ref='/Instance/Claim/ClaimId' />
                                </displaycolumn>
                                 <displaycolumn>
                                  <control name='claimnumber' type='text' tabindex='3' title='Claim Number' maxlength='50' required='yes' ref='/Instance/Claim/ClaimNumber' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='claimtypecode' type='code' codetable='CLAIM_TYPE' tabindex='4' title='Claim Type' required='yes' ref='/Instance/Claim/ClaimTypeCode' />
                                  <control name='dateofclaim' type='date' tabindex='12' title='Date Of Claim' required='yes' ref='/Instance/Claim/DateOfClaim' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='timeofclaim' type='time' tabindex='13' title='Time Of Claim' required='yes' ref='/Instance/Claim/TimeOfClaim' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='claimstatuscode' type='codewithdetail' detailtype='1' codetable='CLAIM_STATUS' tabindex='6' title='Claim Status' required='yes' ref='/Instance/Claim/ClaimStatusCode' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='dttmclosed' type='label' tabindex='7' title='Date Closed' ref='/Instance/Claim/DttmClosed' />
                                  <control name='estcollection' type='currency' tabindex='18' title='Est. Collection' ref='/Instance/Claim/EstCollection' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='methodclosedcode' type='code' codetable='CLOSE_METHOD' tabindex='8' title='Close Method' required='no' ref='/Instance/Claim/MethodClosedCode' />
                                  <control name='estcollection' type='currency' tabindex='16' title='Est. Collection' ref='/Instance/Claim/EstCollection' />
                                  <control name='mcoeid' type='eidlookup' tableid='MCO' tabindex='21' title='Policy MCO' ref='/Instance/Claim/McoEid' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='filenumber' type='text' tabindex='17' title='File Number' ref='/Instance/Claim/FileNumber' />
                                  <control name='filingstateid' type='code' codetable='states' tabindex='24' title='Jurisdiction'
				                            required='yes' ref='/Instance/Claim/FilingStateId' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='primarypolicyid' type='policylookup' tabindex='18' title='Policy Name' ref='/Instance/Claim/PrimaryPolicyId' />
                                  <control name='servicecode' type='code' codetable='SERVICE_CODE' tabindex='19' title='Service Code' ref='/Instance/Claim/ServiceCode' />
                                </displaycolumn>
                                <displaycolumn>
                                  <control name='paymntfrozenflag' type='checkbox' tabindex='20' title='Payments Frozen' ref='/Instance/Claim/PaymntFrozenFlag' />
                                </displaycolumn>
                              </group>
                             </form>
            ");
            return obj;
        }
        //JIRA RMA-8753 nshah28 start
        private XElement GetAddressXml()
        {
            XElement obj = XElement.Parse(@"<?xml version='1.0' ?>
               <form name='address' title='Address Maintenance' topbuttons='1' supp='ADDR_SUPP'>
                      <subtitle valuenodepath='/Instance/UI/FormVariables/SysExData/SubTitle' />
                      <toolbar>
                <button type='save' title='Save' />
    
                <button name='AddressLookup' type='addresssearchlookup' title='Address Lookup' />
              </toolbar>
              <group selected='1' name='Addressinfo' title='Address'>
                <displaycolumn>
                  <control name='AddressId' type='id' ref='/Instance/Address/AddressId' />
                  <control name='Addr1' type='text' firstfield='1' tabindex='2' title='Address1' maxlength='255' required='yes' entitylock='true' ref='/Instance/Address/Addr1' onchange='fillinitials()' />
                  <control name='Addr2' type='text' tabindex='2' title='Address2' maxlength='50' entitylock='true' ref='/Instance/Address/Addr2' />
                </displaycolumn>
                <displaycolumn>
                  <control name='Addr3' type='text' tabindex='3' title='Address3' maxlength='255' entitylock='true' ref='/Instance/Address/Addr3' onchange='fillinitials()' />
                  <control name='Addr4' type='text' tabindex='4' title='Address4' ref='/Instance/Address/Addr4' />
                </displaycolumn>
                <displaycolumn>
                  <control name='City' type='text' tabindex='5' title='City' maxlength='255' entitylock='true' ref='/Instance/Address/City' />
                  <control name='State' type='code' codetable='states' tabindex='6' title='State' entitylock='true' ref='/Instance/Address/State' />
                </displaycolumn>
                <displaycolumn>
                  <control name='County' type='text' tabindex='7' title='County' maxlength='50' entitylock='true' ref='/Instance/Address/County' />
                  <control name='Country' type='code' codetable='COUNTRY' tabindex='8' title='Country' entitylock='true' ref='/Instance/Address/Country' />
                </displaycolumn>
                <displaycolumn>
                  <control name='ZipCode' type='zip' tabindex='9' title='Zip/Postal Code' entitylock='true' ref='/Instance/Address/ZipCode' />
                </displaycolumn>
              </group>
              <internal name='SysCmd' type='hidden' ref='Instance/UI/FormVariables/SysCmd' value='' />
              <internal name='SysCmdConfirmSave' type='hidden' ref='Instance/UI/FormVariables/SysCmdConfirmSave' value='' />
              <internal name='SysCmdQueue' type='hidden' ref='Instance/UI/FormVariables/SysCmdQueue' value='' />
              <internal name='SysCmdText' type='hidden' ref='Instance/UI/FormVariables/SysCmdText' value='Navigate' />
              <internal name='SysClassName' type='hidden' ref='Instance/UI/FormVariables/SysClassName' value='Address' />
              <internal name='SysSerializationConfig' type='hidden' ref='Instance/UI/FormVariables/SysSerializationConfig'>
                <Address>
                  <AddressEntity />
                  <Supplementals />
                </Address>
              </internal>
              <internal type='hidden' name='SysPSid' ref='Instance/UI/FormVariables/SysPSid' value='' />
              <internal type='hidden' name='SysSid' ref='Instance/UI/FormVariables/SysSid' value='' />
              <internal type='hidden' name='SysFormPId' ref='Instance/UI/FormVariables/SysFormPId' value='' />
              <internal type='hidden' name='SysFormId' ref='Instance/UI/FormVariables/SysFormId' value='' />
              <internal type='hidden' name='SysViewType' ref='Instance/UI/FormVariables/SysViewType' value='' />
              <internal type='hidden' name='SysFormIdName' ref='Instance/UI/FormVariables/SysFormIdName' value='AddressId' />
              <internal type='hidden' name='SysFormPIdName' ref='Instance/UI/FormVariables/SysFormPIdName' value='' />
              <internal type='hidden' name='SysFormPForm' ref='Instance/UI/FormVariables/SysFormPForm' value='' />
              <internal type='hidden' name='SysEx' ref='Instance/UI/FormVariables/SysEx' value='' />
              <internal type='hidden' name='SysFormName' ref='Instance/UI/FormVariables/SysFormName' value='address' />
              <internal type='hidden' name='TabNameList' value='' />
              <internal type='hidden' name='GroupAssocFieldList' value='' />
              <internal type='hidden' name='HdnIsDupAddr' ref='Instance/UI/FormVariables/SysExData/HdnIsDupAddr' value='' />
              <internal type='hidden' name='HdnDupAddrId' ref='Instance/UI/FormVariables/SysExData/HdnDupAddrId' value='0' />
              <internal type='hidden' name='dupeoverride' ref='Instance/UI/FormVariables/SysExData/dupeoverride' value='' />
            </form>");

            return obj;
        }
        //JIRA RMA-8753 nshah28 end
        #endregion
    }
}



