﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchMenu.aspx.cs" Inherits="Riskmaster.UI.Search.SearchMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Search Menu</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
</head>
<body>
    <table border="0" cellspacing="0" cellpadding="0">
	<tr><td class="td8" colspan="3"><img src="../../Images/navigationtop.gif" width="119"/></td></tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchclaims.gif" width="25" height="25" title="Claims Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=claim">Claims</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchevents.gif" width="25" height="25" title="Events Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=event">Events</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchemployees.gif" width="25" height="25" title="Employees Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=employee">Employees</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchentities.gif" width="25" height="25" title="Entity Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=entity">Entities</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchvehicles.gif" width="25" height="25" title="Vehicles Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=vehicle">Vehicles</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchpolicies.gif" width="25" height="25" title="Policy Tracking" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=policy">Policy Tracking</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchpolicies.gif" width="25" height="25" title="Policy Management" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=policyenh">Policy Management</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchfunds.gif" width="25" height="25" title="Funds Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=payment">Funds</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchfunds.gif" width="25" height="25" title="Admin Tracking" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=at">Admin Tracking</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchpatients.gif" width="25" height="25" title="Patients Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=patient">Patients</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchphysicians.gif" width="25" height="25" title="Physicians Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=physician">Physicians</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchphysicians.gif" width="25" height="25" title="Med Staff Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=medstaff">Med Staff</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchphysicians.gif" width="25" height="25" title="Disability Plan Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=dp">Disability Plan</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td class="td8" height="20" width="5" VALIGN="middle" align="right" background="../../Images/navigationmid1.gif"></td>
		<td class="td8" height="20" width="107" VALIGN="middle" background="../../Images/navigationmid2.gif"><img src="../../Images/searchphysicians.gif" width="25" height="25" title="Leave Plan Search" border="0">&nbsp;<a class="LightBold" href="searchmain.aspx?formname=leaveplan">Leave Plan</a></td>
		<td class="td8" HEIGHT="20" width="7" background="../../Images/navigationmid3.gif"></td>
	</tr>
	<tr>
		<td colspan="3"><img src="../../Images/navigationbottom.gif" width="119"/></td>
	</tr>
</table>
</body>
</html>
