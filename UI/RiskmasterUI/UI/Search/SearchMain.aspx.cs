﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 11/20/2014 | RMA-345         | achouhan3  | Check for Browser and redirect accordingly on search result screen
 **********************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using Riskmaster.Common; //JIRA-RMA-1535 
using Riskmaster.Models;
namespace Riskmaster.UI.Search
{
    public partial class SearchMain : System.Web.UI.Page
    {
        XElement m_objMessageElement = null;
		//JIRA-RMA-1535 start
        string sLangId = AppHelper.GetLanguageCode();
        string sSearchMainPageId = RMXResourceManager.RMXResourceProvider.PageId("SearchMain.aspx");
        string sSearchPageId = RMXResourceManager.RMXResourceProvider.PageId("SearchLocalization.aspx");
		//JIRA-RMA-1535 end
        //RMA-345 starts   achouhan3    Added to hold redirect page name by checking browser compatibility
        string strSearchFrmName = String.Empty;
        //RMA-345 ends   achouhan3    Added to hold redirect page name by checking browser compatibility
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument objXml = null;
            XElement objTempElement = null;
            string sSearchType = string.Empty;
            string sViewID = string.Empty;
            string sTableRestrict = string.Empty;
            string sScreenFlag = string.Empty;
            string sSetToDefault = string.Empty;
            //Pen testing :atavaragiri mits 27794
            string sFrmName = string.Empty; 
            string sSubmitQuery = string.Empty;
            // END :Pen testing :atavaragiri mits 27794
            //Tushar:MITS#18229
            string sCodeFieldRestrict = string.Empty;
            //End
            string sFullEntitySearch = string.Empty;//skhare7 JIRa 340
            string sViewEntityFlag = string.Empty;

            string sCulture = AppHelper.GetCulture().ToString();
            bool bHideGlobalSearch = false;
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScript(sCulture, this);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //skhare7 JIRa 340 Entity Role start
            if ((Request.QueryString["SetAsDefault"] != null && Request.QueryString["SetAsDefault"] != "") || (Request.QueryString["fullentitysearch"] != null && Request.QueryString["fullentitysearch"] != ""))
            {
                SetUserPrefNode();

            }
            //Deb Pen Testing

            //skhare7 JIRa 340 Entity Role end
             // pen testing changes :atavaragiri mits  27794

            /* if (Request.QueryString["btnSubmitQry"] == "true")
                    
             {
               Server.Transfer("SearchResults.aspx");
              } */

            string showAddNew = Request.QueryString["showaddnew"];
            //RMA-345 starts     Achouhan3        Check for IEbrowser and according decide to open search result page
            if (Request.Browser.Browser != null && String.Compare(Request.Browser.Browser, "IE", true) == 0 && Request.Browser.MajorVersion < 11)
                strSearchFrmName = "SearchResults.aspx?showaddnew=" + showAddNew;
            else
                strSearchFrmName = "AsyncSearchResult.aspx?showaddnew=" + showAddNew;
            //RMA-345 ends     Achouhan3        Check for IEbrowser and according decide to open search result page

            if(!String.IsNullOrEmpty(Request.QueryString["btnSubmitQry"])) 
                sSubmitQuery = AppHelper.HTMLCustomEncode(Request.QueryString["btnSubmitQry"]);
                 
                        
           
            if(sSubmitQuery=="true")
            {
                //RMA-345 starts     Achouhan3        Check for IEbrowser and according decide to open search result page
                //Server.Transfer("SearchResults.aspx");
                Server.Transfer(strSearchFrmName);
                //RMA-345 ends     Achouhan3        Check for IEbrowser and according decide to open search result page
            }

            //END:Pen testing changes mits 27794
            //Deb Pen Testing
            XslTransform objXsl = new XslTransform();
            try
            {
                m_objMessageElement = GetMessageTemplate();

                //Pen testing :atavaragiri mits 27794

                /* if (Request.QueryString["formname"] != null)
                  {
                     //sSearchType = Request.QueryString["formname"];
                     sSearchType = Request.QueryString["formname"];
                
                     objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/FormName");
                     objTempElement.Value = sSearchType;
                 } */

                 if(!String.IsNullOrEmpty(Request.QueryString["formname"]))
                 {
                     sFrmName = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
                     //sSearchType = Request.QueryString["formname"]; 
                    sSearchType = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
                       //END :pen testing changes:atavaragiri mits 27794
                    //Start rsushilaggar JIRA 11730
                    GetSysSettingsRequest oGetSysSettingsRequest = new GetSysSettingsRequest();
                    GetSysSettingsResponse oResponse = new GetSysSettingsResponse();
                    //MDINavigationTreeNodeProfileServiceClient oClient = new MDINavigationTreeNodeProfileServiceClient();
                    oGetSysSettingsRequest.Token = AppHelper.ReadCookieValue("SessionId");
                    List<String> oReqList = new List<String>();
                    oReqList.Add("UseEntityRole");
                    oReqList.ToArray();
                    //String[] reqList = new String[5];
                    //reqList[0] = "MultiCovgPerClm";
                    oGetSysSettingsRequest.oSysSettingsReqList = oReqList.ToList<String>();
                    oGetSysSettingsRequest.ClientId = AppHelper.ClientId;
                    oGetSysSettingsRequest.Token = AppHelper.ReadCookieValue("SessionId");
                    //oResponse = oClient.GetSysSettings(oRequest);
                    oResponse = AppHelper.GetResponse<GetSysSettingsResponse>("RMService/MDI/settings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oGetSysSettingsRequest);
                    Dictionary<String, Object> oDTResponse = new Dictionary<String, Object>();
                    oDTResponse = oResponse.oSysSettingsList;

                    if (Request.QueryString["hideglobalsearch"] != null && !string.IsNullOrEmpty(Request.QueryString["hideglobalsearch"]))
                        bHideGlobalSearch = string.Equals(Request.QueryString["hideglobalsearch"].ToUpper(), "TRUE");

                    int iUseEntRole = (int)oDTResponse["UseEntityRole"];
                    if (iUseEntRole != 0)
                    {
                        if (Enum.IsDefined(typeof(Globalization.PersonInvolvedGlossaryTableNames), sFrmName.ToUpper()) && !bHideGlobalSearch)
                        {
                            sFrmName = "entitymaint";
                            sSearchType = "entitymaint";
                        }
                    }
                    //End rsushilaggar JIRA 11730             
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/FormName");
                    objTempElement.Value = sSearchType;
                }

                if (Request.QueryString["setdefault"] != null)
                {
                    sSetToDefault = Request.QueryString["setdefault"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/SettoDefault");
                    objTempElement.Value = sSetToDefault;
                }

                if (Request.QueryString["viewid"] != null)
                {
                    sViewID = Request.QueryString["viewid"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/ViewID");
                    objTempElement.Value = sViewID;
                }

                if (String.IsNullOrEmpty(Request.QueryString["screenflag"]))
                {
                    sScreenFlag = "1";
                }
                else
                {
                    sScreenFlag = Request.QueryString["screenflag"];
                }
                //skhare7 JIRA 340 start
                
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/screenflag");
                    if (objTempElement != null)
                    {
                        objTempElement.Value = sScreenFlag;
                    }
                
                //skhare7 JIRA 340 end
                if (sScreenFlag == "1" || sScreenFlag == "4" || sScreenFlag == "6")//MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                    sTableRestrict = Request.QueryString["tableid"];
                else  
                    //Pen testing :atavaragiri mits 27794
                    
                    if(!String.IsNullOrEmpty(Request.QueryString["formname"]))
                        //sTableRestrict = Request.QueryString["formname"];
                    sTableRestrict = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
                   //END :Pen testing :atavaragiri mits 27794
                
                //Neha R8 CC Injury Enhance ment
                
                if (Request.QueryString["from"] != null)
                {
                    if (string.Compare(Request.QueryString["from"], "pidriveri") == 0)
                    {
                        sTableRestrict = "driver_insured";
                    }
                    else if (string.Compare(Request.QueryString["from"], "pidrivero") == 0)
                    {
                        sTableRestrict = "driver_other";  //mbahl3
                    }
                    // akaushik5 Added for MITS 30805 Starts
                    // akaushik5 Reverted the Changes for MITS 30805 Starts
                    //else if (!string.IsNullOrEmpty(Request.QueryString["from"]) && Request.QueryString["from"].Equals("patient"))
                    //{
                    //    sTableRestrict = "PATIENTS";
                    //}
                    // akaushik5 Reverted the Changes for MITS 30805 Ends
                    else if (!string.IsNullOrEmpty(Request.QueryString["from"]) && Request.QueryString["from"].Equals("MDI"))
                    {
                        sTableRestrict = "MDI";
                    }
                    // akaushik5 Added for MITS 30805 Ends

                }
                //Tushar:MITS#18229
                if (Request.QueryString["CodeFieldRestrict"] != null)
                {
                    sCodeFieldRestrict = Request.QueryString["CodeFieldRestrict"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrict");
                    if (objTempElement != null)
                    {
                        objTempElement.Value = sCodeFieldRestrict;
                    }
                }
                if (Request.QueryString["CodeFieldRestrictID"] != null)
                {
                    sCodeFieldRestrict = Request.QueryString["CodeFieldRestrictID"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrictid");
                    if (objTempElement != null)
                    {
                        objTempElement.Value = sCodeFieldRestrict;
                    }
                }
                //End MITS#18229
                //skhare7 JIRa 340 start        
                //avipinsrivas Start : Worked for 7545 (Issue of 4634 - Epic 340)
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/HideGlobalSearch");                 
                if (objTempElement != null)
                {
                    if (bHideGlobalSearch)
                        objTempElement.Value = "true";
                    else
                        objTempElement.Value = "false";
                }
                //avipinsrivas End
                //skhare7 JIRa 340 End
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/TableRestrict");

                if (!string.IsNullOrEmpty(sTableRestrict))
                {
                    objTempElement.Value = sTableRestrict;
                }

                if (! string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                    objTempElement.Value = AppHelper.GetSessionId();
                }//if
				//JIRA-RMA-1535 start
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/LangId");
                objTempElement.Value = sLangId;

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/PageId");
                objTempElement.Value = sSearchPageId;
				//JIRA-RMA-1535 end
                    objXml = GetSearchXml();

                    if (objXml != null)
                    {
                        XmlElement objXmlElement = (XmlElement)objXml.GetElementsByTagName("search").Item(0);
                        if (objXmlElement != null)
                        {
                             if(objXmlElement.Attributes.Item(9)!=null)//skhare7 JIRA 340
                        sViewEntityFlag = objXmlElement.Attributes.Item(9).Value;
                          if(objXmlElement.Attributes.Item(10)!=null)
                              sFullEntitySearch = objXmlElement.Attributes.Item(10).Value;
                        //skhare7 JIRA 340 End
                            objXmlElement.SetAttribute("screenflag", sScreenFlag);
                            objXmlElement.SetAttribute("formname", sSearchType);
                            //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                            if ( Request.QueryString["tablename"]!= null)
                                objXmlElement.SetAttribute("tablename", Request.QueryString["tablename"]);
                            if (Request.QueryString["rowid"] != null)
                                 objXmlElement.SetAttribute("rowid", Request.QueryString["rowid"]);
                            if (Request.QueryString["eventdate"] != null)
                                objXmlElement.SetAttribute("eventdate", Request.QueryString["eventdate"]);
                            if (Request.QueryString["claimdate"] != null)
                                objXmlElement.SetAttribute("claimdate", Request.QueryString["claimdate"]);
                            if (Request.QueryString["policydate"] != null)
                                objXmlElement.SetAttribute("policydate", Request.QueryString["policydate"]);
                            if (Request.QueryString["filter"] != null)
                                objXmlElement.SetAttribute("filter", Request.QueryString["filter"]);

                            //Tushar MITS#18229
                            if (Request.QueryString["CodeFieldRestrict"] != null)
                            {
                                objXmlElement.SetAttribute("codefieldrestrict", Request.QueryString["codefieldrestrict"]);
                                objXmlElement.SetAttribute("codefieldrestrictid", Request.QueryString["codefieldrestrictid"]);


                               // objXml = SetFieldRestriction(objXml);
                            }
                            //End:MITS#18229

                        }
                    }

                    //objXsl.Load(Server.MapPath("../../Content/Xsl/SearchCriteria.xsl"));
                    //avipinsrivas Start : Worked for 7545 (Issue of 4634 - Epic 340)
                    objXsl.Load(GetXsl(sViewEntityFlag, sSearchType));
                    //avipinsrivas End

                    // Get the transformed result
                    StringWriter objStrWriter = new StringWriter();
                    objXsl.Transform(objXml, null, objStrWriter);  
                  
                    string sResult =objStrWriter.ToString();
                    // parse the control(s) and add it to the page
                    Control objControl = Page.ParseControl(sResult);
                    Page.Controls.Add(objControl);

                    XmlNode objNode = objXml.SelectSingleNode("//searchfields");

                    XElement objXmlCriteria = XElement.Parse(objNode.OuterXml.ToString());

                    Control ctrlCriteriaXML = Page.FindControl("hdCriteriaXML");

                    if (ctrlCriteriaXML != null)
                        ((HiddenField)(ctrlCriteriaXML)).Value = Server.HtmlEncode(objXmlCriteria.ToString());

                    HiddenField hdFormName = (HiddenField)this.Page.FindControl("hdFormName");
                    if (hdFormName != null)
                        //Pen testing :atavaragiri mits 27794
                        
                        if(!String.IsNullOrEmpty(Request.QueryString["formname"]))
                         // hdFormName.Value = Request.QueryString["formname"]; 
                        hdFormName.Value = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
                    //END :Pen testing :atavaragiri mits 27794
                        
                HiddenField hdSysFormName = (HiddenField)this.Page.FindControl("hdSysFormName");  // csingh7 R5 Merge : 12545
                if (hdSysFormName != null)
                    hdSysFormName.Value = Request.QueryString["sysformname"];

                SetPageTitle(sFullEntitySearch);  // csingh7 : R5 Merge mits 12781

                //abansal23 MITS 17648 Starts

                HiddenField hdSelectedOrgLevels = (HiddenField)this.Page.FindControl("hdSelectedOrgLevels");
                if (hdSelectedOrgLevels != null && Request.QueryString["SelOrgLevels"] != null)
                    hdSelectedOrgLevels.Value = Request.QueryString["SelOrgLevels"];

                //abansal23 MITS 17648 Starts

                //Start - averma62 MITS 25163- Policy Interface Implementation
                Control hdAllowPolicySearch = this.Page.FindControl("hdAllowPolicySearch");
                if(hdAllowPolicySearch!=null)
                   ((HiddenField)hdAllowPolicySearch).Value=Request.QueryString["allowpolicysearch"];
                //End - averma62 MITS 25163- Policy Interface Implementation

				//JIRA-RMA-1535 start
                Control hdLangId = this.Page.FindControl("hdLangId");
                if (hdLangId != null)
                    ((HiddenField)hdLangId).Value = AppHelper.GetLanguageCode();

                Control hdPageId = this.Page.FindControl("hdPageId");
                if (hdPageId != null)
                    ((HiddenField)hdPageId).Value = sSearchPageId;
				//JIRA-RMA-1535 end

                //avipinsrivas Start : Worked for JIRAs - 9610 (Issue-4634 / Epic-340)
                HiddenField hdhideglobalsearch = (HiddenField)this.Page.FindControl("hdhideglobalsearch");
                if (hdhideglobalsearch != null)
                    hdhideglobalsearch.Value = Request.QueryString["hideglobalsearch"];
                //avipinsrivas End
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XmlDocument GetSearchXml()
        {
            XmlDocument objXml = new XmlDocument();
            
            //Call WCF wrapper for cws
            //CommonWCFService.CommonWCFServiceClient oWCF = new CommonWCFService.CommonWCFServiceClient();
            //string sReturn = oWCF.ProcessRequest(m_objMessageElement.ToString());

            
            //string sReturn = AppHelper.CallWCFService(m_objMessageElement.ToString());
            string sReturn = AppHelper.CallCWSService(m_objMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");

            objXml.LoadXml(oInstanceNode.InnerXml);

            return objXml;
        }

        /// <summary>
        /// CWS request message template
        /// CodeFieldRestrict field added by Tushar:MITS#18229
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>SearchAdaptor.CreateSearchView</Function>
              </Call>
              <Document>
                <CreateSearchView>
                  <CatId />
                  <ViewID>-1</ViewID>
                  <TableRestrict />
                  <SysEx />
                  <Settings />
                  <TableID />
                  <tablename/>
                  <rowid/>
                  <eventdate/>
                  <claimdate/>
                  <policydate/>
                  <filter/>
                  <codefieldrestrict />
                  <codefieldrestrictid />
                  <FormName>payment</FormName>
                  <SettoDefault>-1</SettoDefault>
                  <LangId></LangId>
                  <PageId></PageId>
                  <SetasDefault/>
                  <screenflag/>
                 <UseFullEntitySearch>1</UseFullEntitySearch>
                 <HideGlobalSearch>true</HideGlobalSearch>
                </CreateSearchView>
              </Document>
            </Message>
            ");

            return oTemplate;
        }

        //skhare7 JIRa 340 Entity Role start
        private void SetUserPrefNode()
        {
            string sFullEntitySearch = string.Empty;//skhare7 JIRa 340
            //XslCompiledTransform objXsl = new XslCompiledTransform();
            XElement objTempElement = null;
            m_objMessageElement = GetMessageTemplate();
            //avipinsrivas start : Worked for JIRA - 7767
            //skhare7 JIRa 340 start

           //if( Request.QueryString["fullentitysearch"] != null && Request.QueryString["fullentitysearch"] != "")

           // {

           //     sFullEntitySearch = Request.QueryString["fullentitysearch"];
           //     if (m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/UseFullEntitySearch") != null)
           //     {
           //         objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/UseFullEntitySearch");
           //         objTempElement.Value = sFullEntitySearch;
           //     }
           // }
            //avipinsrivas end
            if ((Request.QueryString["SetAsDefault"] != null && Request.QueryString["SetAsDefault"] != ""))
                SetAsDefaultClick(objTempElement);
            //skhare7 JIRa 340 end
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                objTempElement.Value = AppHelper.GetSessionId();
            }

            // HiddenField hdnSetDefault = (HiddenField)Page.FindControl("hdnIsSetAsDfltClick");
            objTempElement = m_objMessageElement.XPathSelectElement("./Call/Function");
            objTempElement.Value = "SearchAdaptor.SetUserPrefNode";

            if (Request.QueryString["type"] != null)
            {
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/CatId");
                objTempElement.Value = Request.QueryString["type"];
            }

            string sReturn = AppHelper.CallCWSService(m_objMessageElement.ToString());


        }
        //skhare7 JIRa 340 Entity Role end
        private void SetAsDefaultClick(XElement objTempElement)
        {
            string sSearchType = string.Empty;
            string sViewID = string.Empty;
            string sTableRestrict = string.Empty;
            string sScreenFlag = string.Empty;
            string sSetToDefault = string.Empty;
            // pen testing changes :atavaragiri mits 27794
            string sFrmName = string.Empty;
            // END :pen testing changes :atavaragiri mits 27794
            //Tushar:MITS 18229
            string sCodeFieldRestrict = string.Empty;
            //End MITS#18229
            //skhare7 JIRA 340 entity role
            string sSetAsDefault = string.Empty;
            //JIRA 340 end

            //XslCompiledTransform objXsl = new XslCompiledTransform();

            m_objMessageElement = GetMessageTemplate();

            //Pen testing atavaragiri mits 27794

            /* if (Request.QueryString["formname"] != null) 
            
            {

                sSearchType = Request.QueryString["formname"]; 
               objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/FormName");
                objTempElement.Value = sSearchType;
            } */
            // END :Pen testing atavaragiri mits 27794


            //Pen testing :atavaragiri mits 27794

            if(!String.IsNullOrEmpty(Request.QueryString["formname"]))
          
            {

                //Pen testing :atavaragiri mits 27794
                sFrmName = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
                //sSearchType = Request.QueryString["formname"];
                sSearchType = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
                //END :Pen testing :atavaragiri mits 27794
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/FormName");
                objTempElement.Value = sSearchType;
            }

            if (Request.QueryString["setdefault"] != null)
            {
                sSetToDefault = Request.QueryString["setdefault"];
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/SettoDefault");
                objTempElement.Value = sSetToDefault;
            }
            //skhare7 JIRA 340 Entity Role

                    sSetAsDefault = Request.QueryString["SetAsDefault"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/SetasDefault");
                    objTempElement.Value = sSetAsDefault;

                //skhare7 JIRA 340 Entity Role end


            if (Request.QueryString["viewid"] != null)
            {
                sViewID = Request.QueryString["viewid"];
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/ViewID");
                objTempElement.Value = sViewID;
            }
            //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
            if (Request.QueryString["tablename"] != null)
                m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/tablename").Value = Request.QueryString["tablename"];
            if (Request.QueryString["rowid"] != null)
                m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/rowid").Value = Request.QueryString["rowid"];
            if (Request.QueryString["eventdate"] != null)
                m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/eventdate").Value = Request.QueryString["eventdate"];
            if (Request.QueryString["claimdate"] != null)
                m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/claimdate").Value = Request.QueryString["claimdate"];
            if (Request.QueryString["policydate"] != null)
                m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/policydate").Value = Request.QueryString["policydate"];
            if (Request.QueryString["filter"] != null)
                m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/filter").Value = Request.QueryString["filter"];

            //Tushar: MITS#18229    
            if (Request.QueryString["CodeFieldRestrict"] != null)
            {
                if (m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrict") != null)
                {
                    m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrict").Value = Request.QueryString["codefieldrestrict"];
                }

                if (m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrictid") != null)
                {
                    m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrictid").Value = Request.QueryString["codefieldrestrictid"];
                }
            }
            //End:MITS#18229
            if (Request.QueryString["screenflag"] != null)
            {
                sScreenFlag = Request.QueryString["screenflag"];

                if (sScreenFlag == "1" || sScreenFlag == "4")
                    sTableRestrict = Request.QueryString["tableid"];
                else
                    //Pen testing :atavaragiri mits 27794
                    if (!String.IsNullOrEmpty(Request.QueryString["formname"]))
                        //sTableRestrict = Request.QueryString["formname"];  
                        sTableRestrict = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
                //END :Pen testing :atavaragiri mits 27794

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/TableRestrict");

                //PJS 04-28-09 : MITS 15932
                if (!string.IsNullOrEmpty(sTableRestrict))
                {
                    objTempElement.Value = sTableRestrict;
                }

                //Tushar:MITS#18229
                if (Request.QueryString["CodeFieldRestrict"] != null)
                {
                    sCodeFieldRestrict = Request.QueryString["CodeFieldRestrict"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrict");
                    objTempElement.Value = sCodeFieldRestrict;
                }
                if (Request.QueryString["CodeFieldRestrictid"] != null)
                {
                    sCodeFieldRestrict = Request.QueryString["CodeFieldRestrictid"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/codefieldrestrictid");
                    objTempElement.Value = sCodeFieldRestrict;
                }
                //End MITS#18229




            }

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                objTempElement.Value = AppHelper.GetSessionId();
            }

            // HiddenField hdnSetDefault = (HiddenField)Page.FindControl("hdnIsSetAsDfltClick");
            objTempElement = m_objMessageElement.XPathSelectElement("./Call/Function");
            objTempElement.Value = "SearchAdaptor.SetUserPrefNode";

            if (Request.QueryString["type"] != null)
            {
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/CatId");
                objTempElement.Value = Request.QueryString["type"];
            }
            //JIRA-RMA-1535 start
            objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/LangId");
            objTempElement.Value = sLangId;

            objTempElement = m_objMessageElement.XPathSelectElement("./Document/CreateSearchView/PageId");
            objTempElement.Value = sSearchPageId;
            //JIRA-RMA-1535 end
            string sReturn = AppHelper.CallCWSService(m_objMessageElement.ToString());
        }

        private void SetPageTitle(string sFullEntitySearch) // csingh7 : R5 Merge mits 12781//skhare7 JIRA 340
        {string sFormName=string.Empty;
            //skhare7 JIRa 340 End
            string strProper = string.Empty;
            //Pen testing :atavaragiri mits 27794
            //string sFormName = Request.QueryString["formname"]; 
            //skhare7 JIRa 340 entity Role start
            if (sFullEntitySearch == "-1")
                sFormName = "Entity";
            else

                sFormName = AppHelper.HTMLCustomEncode(Request.QueryString["formname"]);
            //END :Pen testing :atavaragiri mits 27794
            //skhare7 JIRa 340 entity Role end
            if (sFormName == "-1" || sFormName == "0" || sFormName == "-4" || sFormName == "dp")
                sFormName = Request.QueryString["sysformname"];

            // bkuzhanthaim RMA-9664 : Since SFormName is null so search result lookup coming empty
            if (!string.IsNullOrEmpty(sFormName))
            {
                sFormName = sFormName.Replace("_", " ");
                strProper = sFormName.Substring(0, 1).ToUpper();
                sFormName = sFormName.Substring(1).ToLower();
                string strPrev = "";

                for (int iIndex = 0; iIndex < sFormName.Length; iIndex++)
                {
                    if (iIndex > 1)
                        strPrev = sFormName.Substring(iIndex - 1, 1);
                    if (strPrev.Equals(" ") || strPrev.Equals("."))
                        strProper += sFormName.Substring(iIndex, 1).ToUpper();
                    else
                        strProper += sFormName.Substring(iIndex, 1);
                }
            }
            if (strProper.ToUpper() == "MMSEA CLMPRTY TYPE") //Added for MMSEA Party Data Screen
            {
                strProper = "MMSEA Claim Party Individual";
            }
            else if (strProper.ToUpper() == "MMSEA PRTYENT TYPE") //Added for MITS 17762
            {
                strProper = "MMSEA Claim Party Entity";
            }
            else if (strProper.ToUpper() == "POWOFATTORNEY TYPE") //Added for MITS 17760
            {
                strProper = "Power of Attorney";
            }
            Head1.Title = strProper+ " Search";          
            
        }
        #region SearchCriteris.xsl
        //-----------------------------SearchCriteria.xsl-----------------------------------------------------------
        //        <?xml version='1.0'?>
        //  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="MyNS" version="1.0">
        //    <xsl:template match="/">
        //      <html xmlns="http://www.w3.org/1999/xhtml" >
        //        <head>

        //        </head>
        //        <body>
        //          <form id="frmData" name="frmData" runat="server" method="post">

        //            <xsl:if test="search/@screenflag[.='4']">
        //              <table border="0" cellspacing="0" cellpadding="2">
        //                <tr>
        //                  <td colspan="3" class="msgheader">Executive Summary Report (Step 1 of 2)</td>
        //                </tr>
        //                <br/>
        //                <tr>
        //                  <td class="small">
        //                    Use this search screen to find the <xsl:value-of select="search/@formname" /> record for which to generate an Executive Summary Report.<br/>
        //                    Note: The current Executive Summary Configuration will be used when generating this report.
        //                    Use the Configuration menu item at the left to change\customize these settings.
        //                  </td>
        //                </tr>
        //              </table>
        //            </xsl:if>
            
        //            <table border="0" cellspacing="0" cellpadding="2">
        //              <xsl:if test="search/@catid[.!='nosubmit']">
        //                <tr>
        //                  <td align="left" colspan="3">

        //                    <!-- Integration with Nav tree : Check if the search screen is being opened as a popup -->
        //                    <xsl:choose>
        //                      <xsl:when test="search/@screenflag[.='1']">
        //                        <input id="btnSubmit1" type="button" value="Submit Query" class="button" onclick="parent.MDISearchResult(this.form)" tabindex="1"></input> <!-- csingh7 : MITS 18830 Added id attribute-->
        //                      </xsl:when>
        //                      <xsl:otherwise>
        //                        <asp:Button ID="btnSubmit1" class="button" runat="server" Text="Submit Query" PostBackUrl="~/UI/Search/SearchResults.aspx" tabindex="1"/> <!-- csingh7 : MITS 18830 Added id attribute-->
        //                      </xsl:otherwise>
        //                    </xsl:choose>
        //            <br /><img src="/RiskmasterUI/Images/empty.gif" width="1px" height="2px" />

        //                  </td>
        //                </tr>
        //              </xsl:if>
        //              <xsl:apply-templates select="search"/>
        //              <tr>
        //                <td>
        //                  <BR/>
        //                </td>
        //              </tr>
        //              <tr>
        //                <td colspan="3" class="ctrlgroup">Search Criteria</td>
        //              </tr>
        //              <xsl:apply-templates select="search/searchfields/field" />
        //            </table>
        //            <BR/>
        //            <table border="0">
        //              <tr>
        //                <td colspan="3" class="ctrlgroup">Sort By</td>
        //              </tr>
        //              <xsl:apply-templates select="search/displayfields" />
        //            </table>
        //            <table border="0">
        //              <tr>
        //                <td align="left" colspan="3">
        //                  <input type="checkbox" name="soundex" id="soundex" value="1" tabindex="500">
        //                    <xsl:if test="search/@soundex[.='-1' or .='1']">
        //                      <xsl:attribute name="checked">checked</xsl:attribute>
        //                    </xsl:if>Use Sound-Alike (Soundex) Match on Last Names
        //                  </input>
        //                </td>
        //              </tr>
        //              <tr>
        //                <td align="left" colspan="3">
        //                  <input type="checkbox" name="setasdefault" value="1" onclick="SetDefaultView()" tabindex="500" /> Default Search View
        //                </td>
        //              </tr>
        //              <xsl:if test="search/@catid[.!='nosubmit']">
        //                <tr>
        //                  <td align="left" colspan="3">


        //                    <!-- Integration with Nav tree : Check if the search screen is being opened as a popup -->
        //                    <xsl:choose>
        //                      <xsl:when test="search/@screenflag[.='1']">
        //                        <input id="btnSubmit" type="button" value="Submit Query" class="button" onclick="parent.MDISearchResult(this.form)" tabindex="500"></input>
        //                      </xsl:when>
        //                      <xsl:otherwise>
        //                        <asp:Button ID="btnSubmit" class="button" runat="server" Text="Submit Query" PostBackUrl="~/UI/Search/SearchResults.aspx" tabindex="500"/>
        //                      </xsl:otherwise>
        //                    </xsl:choose>
        //                    <xsl:if test="search/@screenflag[.='2']">
        //                      <asp:Button ID="btnCancel" class="button" runat="server" Text="Cancel" onClientClick="window.close();return false" tabindex="500"/>
        //                    </xsl:if>
        //                    <asp:Button ID="btnClear" class="button" runat="server" Text="Clear" onClientClick="this.form.reset();OnFormClear();return false" tabindex="500"/>
        //                  </td>
        //                </tr>
        //              </xsl:if>
        //            </table>
	   
        //            <asp:HiddenField id="viewid" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@viewid"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="dsnid" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@dsnid"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="catid" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@catid"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="tablerestrict" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@tablerestrict"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="codefieldrestrict" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@codefieldrestrict"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="codefieldrestrictid" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@codefieldrestrictid"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>

        //            <asp:HiddenField id="defaultviewid" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@defaultviewid"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="hdScreenFlag" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@screenflag"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="hdtablename" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@tablename"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="hdrowid" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@rowid"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="hdeventdate" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@eventdate"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="hdclaimdate" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@claimdate"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="hdpolicydate" runat="server">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="search/@policydate"/>
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField id="hdfilter" runat="server">
        //                <xsl:attribute name="value">
        //                  <xsl:value-of select="search/@filter"/>
        //                </xsl:attribute>
        //              </asp:HiddenField>            
        //            <asp:HiddenField ID="hdFormName" Value="" runat="server" />
        //            <asp:HiddenField ID="hdCriteriaXML" Value="" runat="server" />            
        //            <asp:HiddenField ID="hdSysFormName" Value="" runat="server" />  <!--Added by csingh7 : R5 merge 12545-->
        //            <asp:HiddenField ID="hdnInProcess" runat="server" />
        //            <asp:HiddenField ID="hdSelectedOrgLevels" Value="" runat="server" /><!--Added by abansal23 : MITS 17648-->
        //            <asp:HiddenField ID="SysFormName" Value="search" runat="server" /><!--Added by rsushilaggar : MITS 20309-->
        //            <asp:HiddenField ID="hdAllowPolicySearch" Value="" runat="server" /><!-- - averma62 MITS 25163- Policy Interface Implementation-->
        //             </form>
        //        </body>
        //      </html>
        //</xsl:template>

        //<xsl:template match="search">
        //    <tr>
        //    <td colspan="2" class="ctrlgroup">
        //      <xsl:value-of select="@name" />
        //    </td>

        //        <td colspan="2" class="ctrlgroup" align="right">
        //          <xsl:apply-templates select="views" />
        //        </td>
        //    <!-- PJS-MITS #14973-->
        //    <!--<input type="button" class="button" runat="server" value="Set as Default" id="btnsetasdefault"  onclick="return btnSetAsDefaultClick();"></input>-->
        //  </tr>
        //</xsl:template>

        //<xsl:template match="views">
        //  <!-- asp:DropDownList runat="server" id="cboViews" AutoPostBack="True" >
        //    <xsl:for-each select="view">
        //      <asp:ListItem />
        //      <asp:ListItem>
        //        <xsl:attribute name="value">
        //        <xsl:value-of select="@id"/>
        //        </xsl:attribute>
        //        <xsl:value-of select="@name"/>
        //      </asp:ListItem>
        //    </xsl:for-each>
        //  </asp:DropDownList -->

        //  <select name="cboViews" size="1" onchange="OnViewChange('searchmain.aspx')">
        //        <option />
        //        <xsl:for-each select="view">
        //      <xsl:sort select="@name"/> <!-- Mits 15728:Asif -->
        //        <option><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="@name"/></option>
        //        </xsl:for-each>
        //    </select>
        //  <input type="button" class="button" runat="server" value="Set as Default" id="btnsetasdefault"  onclick="return btnSetAsDefaultClick();"></input>
        //</xsl:template>

        //<xsl:template match="displayfields">
        //    <TR>
	
        //    <TD colSpan="1"><select id="orderby1" name="orderby1" size="1" tabindex="500">
        //        <option value=""></option>
		
        //    <xsl:for-each select="field">
        //        <!--gagnihotri MITS 19281 Disabling sorting for Comments column-->
        //        <xsl:choose>
        //      <!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in "Sort By" combo box.-->
        //      <!--<xsl:when test="@type!='textml'">-->
        //      <xsl:when test="@type[.!='textml' and .!='freecode']">
        //                <option>
        //                    <xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="text()"/>
        //                </option>
        //            </xsl:when>
        //        </xsl:choose>
        //    </xsl:for-each>

        //    </select></TD>

        //    <TD colSpan="1"><select id="orderby2" name="orderby2" size="1" tabindex="500">
        //        <option value=""></option>
	
        //    <xsl:for-each select="field">
        //        <!--gagnihotri MITS 19281 Disabling sorting for Comments column-->
        //        <xsl:choose>
        //      <!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in "Sort By" combo box.-->
        //      <!--<xsl:when test="@type!='textml'">-->
        //      <xsl:when test="@type[.!='textml' and .!='freecode']">
        //                <option>
        //                    <xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="text()"/>
        //                </option>
        //            </xsl:when>
        //        </xsl:choose>
        //    </xsl:for-each>

        //    </select></TD>

        //    <TD colSpan="1"><select id="orderby3" name="orderby3" size="1" tabindex="500">
        //        <option value=""></option>

        //    <xsl:for-each select="field">
        //        <!--gagnihotri MITS 19281 Disabling sorting for Comments column-->
        //        <xsl:choose>
        //      <!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in "Sort By" combo box.-->
        //      <!--<xsl:when test="@type!='textml'">-->
        //      <xsl:when test="@type[.!='textml' and .!='freecode']">
        //                <option>
        //                    <xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="text()"/>
        //                </option>
        //            </xsl:when>
        //        </xsl:choose>
        //    </xsl:for-each>

        //    </select></TD>
	
        //    </TR>

        //</xsl:template>

        //<xsl:template match="searchfields/field">
        //    <tr>
        //        <td>
        //            <xsl:value-of select="text()"/>:
        //        </td>
        //        <xsl:choose>
        //            <xsl:when test="@type[.='text']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //          <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;">&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;=">&gt;=</asp:ListItem>
        //            <asp:ListItem value="&lt;">&lt;</asp:ListItem>
        //            <asp:ListItem value="&lt;=">&lt;=</asp:ListItem>
        //          </asp:DropDownList>
        //                </td>
        //                <td>
        //          <asp:TextBox runat="server" size="50">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //                </td>
        //            </xsl:when>
        //            <xsl:when test="@type[.='ssn']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;">&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;=">&gt;=</asp:ListItem>
        //            <asp:ListItem value="&lt;">&lt;</asp:ListItem>
        //            <asp:ListItem value="&lt;=">&lt;=</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:TextBox runat="server" size="50" onblur="ssnLostFocus(this);">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //                </td>
        //            </xsl:when>
        //      <xsl:when test="@type[.='freecode']">
        //        <!--MGaba2:MITS 16476: Added support for Multi Text/Codes  -->
        //        <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //        <td>
        //          <asp:TextBox runat="server" TextMode="MultiLine">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="Columns">
        //              <xsl:value-of select="@cols"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="Rows">
        //              <xsl:value-of select="@rows"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //          <input type="button" class="EllipsisControl">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>btn
        //            </xsl:attribute>
        //            <xsl:attribute name="onClick">
        //              CodeSelect('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@id"/>','Search','')
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="number(@tabindex)+1" />
        //            </xsl:attribute>
        //          </input>
        //        </td>
        //      </xsl:when>
        //            <xsl:when test="@type[.='textml']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:TextBox runat="server" TextMode="MultiLine">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="Columns">
        //              <xsl:value-of select="@cols"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="Rows">
        //              <xsl:value-of select="@rows"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //                </td>
        //            </xsl:when>
        //            <xsl:when test="@type[.='code']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:TextBox runat="server" size="30">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="onchange">
        //              javascript:return datachanged('true');
        //            </xsl:attribute>
        //            <xsl:attribute name="onkeydown">
        //              javascript:return onEnter('<xsl:value-of select="@codetable" />','<xsl:value-of select="@id" />');
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">
        //              javascript:return onLostFocus('<xsl:value-of select="@codetable" />','<xsl:value-of select="@id" />','Search');
        //            </xsl:attribute>
        //            <xsl:if test="@value">
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="@value" />
        //              </xsl:attribute>
        //            </xsl:if >
        //            <xsl:choose>
        //              <xsl:when test="@name[.='LINE_OF_BUS_CODE']">
        //                <xsl:choose>
        //                  <xsl:when test="/search/@tablerestrict[.='gc']">
        //                    <xsl:attribute name="value">GC General Claims</xsl:attribute>
        //                  </xsl:when>
        //                  <xsl:when test="/search/@tablerestrict[.='va']">
        //                    <xsl:attribute name="value">VA Vehicle Accident Claims</xsl:attribute>
        //                  </xsl:when>
        //                  <xsl:when test="/search/@tablerestrict[.='wc']">
        //                    <xsl:attribute name="value">WC Workers' Compensation</xsl:attribute>
        //                  </xsl:when>
        //                </xsl:choose>
        //              </xsl:when>
        //            </xsl:choose>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //                <input type="button" class="EllipsisControl">
        //                  <xsl:attribute name="id"><xsl:value-of select="@id"/>btn</xsl:attribute>
        //                  <xsl:attribute name="onClick">CodeSelect('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@id"/>','Search','')</xsl:attribute>
        //          <xsl:attribute name="tabindex">
        //            <xsl:value-of select="@tabindex"/>
        //          </xsl:attribute>
        //        </input>
        //                <xsl:choose>
        //                    <xsl:when test="@name[.='LINE_OF_BUS_CODE']">
        //                        <xsl:choose>
        //                            <xsl:when test="/search/@tablerestrict[.='gc']">
        //                <asp:HiddenField Value="241" runat="server">
        //                  <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //                </asp:HiddenField>

        //                            </xsl:when>
        //                            <xsl:when test="/search/@tablerestrict[.='va']">
        //                <asp:HiddenField Value="242" runat="server">
        //                  <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //                </asp:HiddenField>
        //              </xsl:when>
        //                            <xsl:when test="/search/@tablerestrict[.='wc']">
        //                <asp:HiddenField Value="243" runat="server">
        //                  <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //                </asp:HiddenField>
        //              </xsl:when>
        //                            <xsl:when test="/search/@tablerestrict[.='di']">
        //                <asp:HiddenField Value="844" runat="server">
        //                  <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //                </asp:HiddenField>
        //              </xsl:when>
        //                            <xsl:otherwise>
        //                <asp:HiddenField Value="0" runat="server">
        //                  <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //                </asp:HiddenField>
        //              </xsl:otherwise>
        //                        </xsl:choose>
        //                    </xsl:when>
        //                    <xsl:otherwise>
        //            <asp:HiddenField runat="server">
        //              <xsl:attribute name="id"><xsl:value-of select="@id"/>_tableid</xsl:attribute>
        //              <xsl:attribute name="value">
        //                <xsl:value-of select="@table" />
        //              </xsl:attribute>
        //            </asp:HiddenField>
        //            <asp:HiddenField runat="server">
        //              <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //              <xsl:if test="@codeid">
        //                <xsl:attribute name="value">
        //                  <xsl:value-of select="@codeid" />
                
        //              </xsl:attribute>
        //              </xsl:if>
        //            </asp:HiddenField>
        //          </xsl:otherwise>					
        //                </xsl:choose>
        //                </td>
        //            </xsl:when>
        //            <xsl:when test="@type[.='date']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <xsl:attribute name="onchange">return BetweenOption('<xsl:value-of select="@type"/>','<xsl:value-of select="@id"/>');</xsl:attribute>
        //            <asp:ListItem value="between">Between</asp:ListItem>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;">&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;=">&gt;=</asp:ListItem>
        //            <asp:ListItem value="&lt;">&lt;</asp:ListItem>
        //            <asp:ListItem value="&lt;=">&lt;=</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:TextBox runat="server">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>start</xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="RMXRef">
        //              <xsl:value-of select="@ref"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="RMXType">
        //              <xsl:value-of select="@type"/>
        //            </xsl:attribute>
        //            <xsl:if test="@readonly[.='true']">
        //              <xsl:attribute name="readonly">true</xsl:attribute>
        //              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
        //            </xsl:if>
        //            <xsl:attribute name="onchange">
        //              <xsl:if test="@onchangefunctionname">
        //                <xsl:value-of select="@onchangefunctionname" />
        //              </xsl:if>datachanged('true')<xsl:if test="@onchangepost">
        //                <xsl:value-of select="@onchangepost" />
        //              </xsl:if>
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">dateLostFocus(this.id);</xsl:attribute>
        //          </asp:TextBox>
        //          <asp:button class="DateLookupControl" runat="server">
        //            <xsl:attribute name="id">btnone<xsl:value-of select="@id"/></xsl:attribute>
        //            <xsl:if test="@tabindex">
        //              <xsl:attribute name="tabindex">
        //                <xsl:value-of select="number(@tabindex)+1" />
        //              </xsl:attribute>
        //              <xsl:if test="@readonly">
        //                <xsl:attribute name="disabled">
        //                  <xsl:value-of select="@readonly" />
        //                </xsl:attribute>
        //              </xsl:if>
        //            </xsl:if>
        //          </asp:button>
        //          <script type="text/javascript">
        //            Zapatec.Calendar.setup(
        //            {
        //            inputField : "<xsl:value-of select="@id" />start",
        //            ifFormat : "%m/%d/%Y",
        //            button : "btnone<xsl:value-of select="@id" />"
        //            }
        //            );
        //          </script>


        //          <asp:TextBox runat="server">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>end</xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="RMXRef">
        //              <xsl:value-of select="@ref"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="RMXType">
        //              <xsl:value-of select="@type"/>
        //            </xsl:attribute>
        //            <xsl:if test="@readonly[.='true']">
        //              <xsl:attribute name="readonly">true</xsl:attribute>
        //              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
        //            </xsl:if>
        //            <xsl:attribute name="onchange">
        //              <xsl:if test="@onchangefunctionname">
        //                <xsl:value-of select="@onchangefunctionname" />
        //              </xsl:if>datachanged('true')<xsl:if test="@onchangepost">
        //                <xsl:value-of select="@onchangepost" />
        //              </xsl:if>
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">dateLostFocus(this.id);</xsl:attribute>
        //          </asp:TextBox>
        //          <asp:button class="DateLookupControl" runat="server">
        //            <xsl:attribute name="id">btntwo<xsl:value-of select="@id"/></xsl:attribute>
        //            <xsl:if test="@tabindex">
        //              <xsl:attribute name="tabindex">
        //                <xsl:value-of select="number(@tabindex)+1" />
        //              </xsl:attribute>
        //              <xsl:if test="@readonly">
        //                <xsl:attribute name="disabled">
        //                  <xsl:value-of select="@readonly" />
        //                </xsl:attribute>
        //              </xsl:if>
        //            </xsl:if>
        //          </asp:button>
        //          <script type="text/javascript">
        //            Zapatec.Calendar.setup(
        //            {
        //            inputField : "<xsl:value-of select="@id" />end",
        //            ifFormat : "%m/%d/%Y",
        //            button : "btntwo<xsl:value-of select="@id" />"
        //            }
        //            );
        //          </script>
        //        </td>
        //            </xsl:when>
        //            <xsl:when test="@type[.='time']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <xsl:attribute name="onchange">return BetweenOption('<xsl:value-of select="@type"/>','<xsl:value-of select="@id"/>');</xsl:attribute>
        //            <asp:ListItem value="between">Between</asp:ListItem>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;">&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;=">&gt;=</asp:ListItem>
        //            <asp:ListItem value="&lt;">&lt;</asp:ListItem>
        //            <asp:ListItem value="&lt;=">&lt;=</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:TextBox runat="server" size="15">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>start
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">timeLostFocus(this.id);</xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //          <asp:TextBox runat="server" size="15">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>end
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">timeLostFocus(this.id);</xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //            <!--input type="text" size="15" onblur="timeLostFocus(this.name);"><xsl:attribute name="name"><xsl:value-of select="@id"/>start</xsl:attribute></input>
        //                <input type="text" size="15" onblur="timeLostFocus(this.name);"><xsl:attribute name="name"><xsl:value-of select="@id"/>end</xsl:attribute></input -->
        //          </td>
        //            </xsl:when>
        //            <xsl:when test="@type[.='orgh']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:TextBox runat="server" size="30">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="onchange">
        //              javascript:return datachanged('true');
        //            </xsl:attribute>
        //            <xsl:attribute name="onkeydown">
        //              javascript:return onEnter('orgh','<xsl:value-of select="@id" />');
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">
        //                javascript:return onOrgLostFocus('orgh','<xsl:value-of select="@id" />','<xsl:value-of select ="@level"/>');                
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //          <input type="button" class="EllipsisControl">
        //            <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
        //            <xsl:attribute name="onClick">javascript:return selectOrg('orgh','<xsl:value-of select="@id" />','<xsl:value-of select="@level" />','<xsl:value-of select="@table" />');</xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </input>
        //          <asp:HiddenField Value="" runat="server">
        //            <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //          </asp:HiddenField>
        //          <!-- input type="hidden" value="0">
        //            <xsl:attribute name="id"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //            <xsl:attribute name="name"><xsl:value-of select="@id" />_cid</xsl:attribute>
        //          </input -->
        //                </td>
        //            </xsl:when>
      
        //            <xsl:when test="@type[.='checkbox']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //        <xsl:choose>
        //          <xsl:when test="/search/@defaultcheckboxvalue[.='True']">
        //            <td>
        //              <asp:CheckBox runat="server" value="1">
        //                <xsl:attribute name="id">
        //                  <xsl:value-of select="@id"/>
        //                </xsl:attribute>
        //                <xsl:attribute name="tabindex">
        //                  <xsl:value-of select="@tabindex"/>
        //                </xsl:attribute>
        //                <xsl:attribute name="checked">true</xsl:attribute>
        //              </asp:CheckBox>
        //            </td>
        //          </xsl:when>
        //          <xsl:otherwise>
        //        <td>
        //          <asp:CheckBox runat="server" value="0">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="checked">false</xsl:attribute>
        //          </asp:CheckBox>
        //        </td>
        //          </xsl:otherwise>
        //        </xsl:choose>
        //            </xsl:when>
      
        //            <xsl:when test="@type[.='numeric' or .='double']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;">&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;=">&gt;=</asp:ListItem>
        //            <asp:ListItem value="&lt;">&lt;</asp:ListItem>
        //            <asp:ListItem value="&lt;=">&lt;=</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:TextBox runat="server" size="30">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="onKeyPress">
        //              javascript:return checkEnterNum(event,this);
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">
        //              javascript:return numLostFocus(this);
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //                </td>
        //            </xsl:when>
        //            <xsl:when test="@type[.='currency']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <xsl:attribute name="onchange">return BetweenOption('<xsl:value-of select="@type"/>','<xsl:value-of select="@id"/>');</xsl:attribute>
        //            <asp:ListItem value="between">Between</asp:ListItem>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;">&gt;</asp:ListItem>
        //            <asp:ListItem value="&gt;=">&gt;=</asp:ListItem>
        //            <asp:ListItem value="&lt;">&lt;</asp:ListItem>
        //            <asp:ListItem value="&lt;=">&lt;=</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td><!--ybhaskar: Code added in currency-->
        //          <asp:TextBox runat="server" size="30">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>start</xsl:attribute>
        //            <!--<xsl:attribute name="onKeyPress">
        //              javascript:return checkEnterCurrency(event,this);
        //            </xsl:attribute>-->
        //            <!--<xsl:attribute name="onblur">
        //              javascript:return currencyLostFocus(this);
        //            </xsl:attribute>-->
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="RMXRef">
        //              <xsl:value-of select="@ref"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="RMXType">
        //              <xsl:value-of select="@type"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //          <asp:TextBox runat="server" size="30">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>end</xsl:attribute>
        //            <!--<xsl:attribute name="onKeyPress">
        //              javascript:return checkEnterCurrency(event,this);
        //            </xsl:attribute>
        //            <xsl:attribute name="onblur">
        //              javascript:return currencyLostFocus(this);
        //            </xsl:attribute>-->
        //            <xsl:attribute name="RMXRef">
        //              <xsl:value-of select="@ref"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="RMXType">
        //              <xsl:value-of select="@type"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //                </td>
        //            </xsl:when>
        //            <xsl:when test="@type[.='tablelist']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">=</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">&lt;&gt;</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //                <select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute>
        //              <xsl:attribute name="onchange">
        //                javascript:return AssignValue(this.options[this.selectedIndex].value,'<xsl:value-of select="@id"/>');
        //              </xsl:attribute>
        //                    <option value=""></option>
        //                    <xsl:for-each select="tablevalue">
        //                        <option><xsl:attribute name="value"><xsl:value-of select="@tableid"/></xsl:attribute>
        //                            <xsl:value-of select="text()"/>
        //                        </option>
        //                    </xsl:for-each>
        //                </select>
        //                </td>
        //        <asp:HiddenField Value="" runat="server">
        //          <xsl:attribute name="id"><xsl:value-of select="@id" />_lst</xsl:attribute>
        //        </asp:HiddenField>
        //            </xsl:when>

        //      <!-- Need to uncomment the 'codelist' and 'entitylist' controls and find a way to create options differently -->
        //      <xsl:when test="@type[.='codelist']">
        //                <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">in</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">not in</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //                <td>
        //          <asp:ListBox runat="server" Rows="3">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //           <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:ListBox>
        //          <!-- select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
        //                <xsl:apply-templates select="option">
        //                    <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
        //                </xsl:apply-templates>
        //                </select -->
        //          <input type="button" class="CodeLookupControl">
        //            <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
        //            <xsl:attribute name="onClick">
        //              javascript:return selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@id"/>','','');
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </input>
        //          <input type="button" class="button" value=" - ">
        //            <xsl:attribute name="id"><xsl:value-of select="@name"/>btndel</xsl:attribute>
        //            <xsl:attribute name="onClick">
        //              javascript:return deleteSelCode('<xsl:value-of select="@id"/>');
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </input>

        //          <asp:HiddenField Value="" runat="server">
        //            <xsl:attribute name="id"><xsl:value-of select="@id" />_lst</xsl:attribute>
        //          </asp:HiddenField>
        //          <!-- xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="context()[not(end())]">,</xsl:if></xsl:for-each></xsl:attribute -->
        //                </td>
        //         </xsl:when>

        //      <xsl:when test="@type[.='entitylist']">
        //        <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //            <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <asp:ListItem value="=">in</asp:ListItem>
        //            <asp:ListItem value="&lt;&gt;">not in</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //        <td>
        //          <asp:ListBox runat="server" Rows="3">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:ListBox>
        //          <!-- select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
        //                <xsl:apply-templates select="option">
        //                    <xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
        //                </xsl:apply-templates>
        //                </select -->
        //          <input type="button" class="CodeLookupControl">
        //            <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
        //            <xsl:attribute name="onClick">
        //              javascript:return selectOrg('orgh','<xsl:value-of select="@id" />','<xsl:value-of select="@level" />','<xsl:value-of select="@table" />');
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </input>
        //          <input type="button" class="button" value=" - ">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@name"/>btndel
        //            </xsl:attribute>
        //            <xsl:attribute name="onClick">
        //              javascript:return deleteSelCode('<xsl:value-of select="@id"/>');
        //            </xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </input>

        //          <asp:HiddenField Value="" runat="server">
        //            <xsl:attribute name="id"><xsl:value-of select="@id" />_lst</xsl:attribute>
        //          </asp:HiddenField>
        //          <!-- xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="context()[not(end())]">,</xsl:if></xsl:for-each></xsl:attribute -->
        //        </td>
        //      </xsl:when>
        //      <!--skhare7-->
        //      <xsl:when test="@type[.='attachedrecord']">
        //        <td>
        //          <asp:DropDownList runat="server" tabindex="500">
        //              <xsl:attribute name="id"><xsl:value-of select="@id"/>op</xsl:attribute>
        //            <xsl:attribute name="onchange"> return SelectAttachedOption('<xsl:value-of select="@type"/>','<xsl:value-of select="@id"/>');
        //            </xsl:attribute>
        //            <asp:ListItem value=" ">  </asp:ListItem>
        //            <asp:ListItem value="claim">Claim</asp:ListItem>
        //            <asp:ListItem value="event">Event</asp:ListItem>
        //            <asp:ListItem value="policy">Policy</asp:ListItem>
        //            <asp:ListItem value="policyenh">Enhanced Policy</asp:ListItem>
        //            <asp:ListItem value="entity">Entity</asp:ListItem>
        //            <asp:ListItem value="funds">Funds</asp:ListItem>
        //            <asp:ListItem value="pi">Person Involved</asp:ListItem>
        //            <asp:ListItem value="claimant">Claimant</asp:ListItem>
        //          </asp:DropDownList>
        //        </td>
        //        <td>
        //          <asp:TextBox runat="server" size="18">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/>Label</xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //            <xsl:attribute name="readonly">
        //              <xsl:value-of select="true"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //          <asp:TextBox runat="server" size="28">
        //            <xsl:attribute name="id">
        //              <xsl:value-of select="@id"/></xsl:attribute>
        //            <xsl:attribute name="tabindex">
        //              <xsl:value-of select="@tabindex"/>
        //            </xsl:attribute>
        //          </asp:TextBox>
        //        </td>
      
        //      </xsl:when>
        //            <xsl:otherwise>
        //                <td>
        //                Unknown Element Type: <xsl:value-of select="@type"/>
        //                </td>
        //            </xsl:otherwise>
        //        </xsl:choose>
        //    </tr>
        //</xsl:template>

        //</xsl:stylesheet>
        //-----------------------------SearchCriteria.xsl-----------------------------------------------------------
        #endregion
        private XmlDocument GetXsl(string sViewEntityFlag, string sFormName)
        {
            XmlDocument objDoc = new XmlDocument();
            //avipinsrivas Start : Worked for Jira-340
            //bool blnIsFromPersonInvolved = false;
            //blnIsFromPersonInvolved = Enum.IsDefined(typeof(Globalization.PersonInvolvedGlossaryTableNames), sFormName.ToUpper());
            //avipinsrivas End
            //Formatted Xsl is hardcoded above 
            //nnithiyanand MITS: 23637 DT04/23/2013
            //bjDoc.LoadXml("<?xml version=\"1.0\"?><xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:asp=\"MyNS\" version=\"1.0\"><xsl:template match=\"/\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head></head><body><form id=\"frmData\" name=\"frmData\" runat=\"server\" method=\"post\"><xsl:if test=\"search/@screenflag[.='4']\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td colspan=\"3\" class=\"msgheader\">Executive Summary Report (Step 1 of 2)</td></tr><br /><tr><td class=\"small\">\r\n                    Use this search screen to find the <xsl:value-of select=\"search/@formname\" /> record for which to generate an Executive Summary Report.<br />\r\n                    Note: The current Executive Summary Configuration will be used when generating this report.\r\n                    Use the Configuration menu item at the left to change\\customize these settings.\r\n                  </td></tr></table></xsl:if><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit1\" type=\"button\" value=\"Submit Query\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"1\"></input><!-- csingh7 : MITS 18830 Added id attribute--></xsl:when><xsl:otherwise><asp:Button ID=\"btnSubmit1\" class=\"button\" runat=\"server\" Text=\"Submit Query\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"1\" /><!-- csingh7 : MITS 18830 Added id attribute--></xsl:otherwise></xsl:choose><br /><img src=\"/RiskmasterUI/Images/empty.gif\" width=\"1px\" height=\"2px\" /></td></tr></xsl:if><xsl:apply-templates select=\"search\" /><tr><td><BR /></td></tr><tr><td colspan=\"3\" class=\"ctrlgroup\">Search Criteria</td></tr><xsl:apply-templates select=\"search/searchfields/field\" /></table><BR /><table border=\"0\"><tr><td colspan=\"3\" class=\"ctrlgroup\">Sort By</td></tr><xsl:apply-templates select=\"search/displayfields\" /></table><table border=\"0\"><tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"soundex\" id=\"soundex\" value=\"1\" tabindex=\"500\"><xsl:if test=\"search/@soundex[.='-1' or .='1']\"><xsl:attribute name=\"checked\">checked</xsl:attribute></xsl:if>Use Sound-Alike (Soundex) Match on Last Names\r\n                  </input></td></tr><tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"setasdefault\" value=\"1\" onclick=\"SetDefaultView()\" tabindex=\"500\" /> Default Search View\r\n                </td></tr><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit\" type=\"button\" value=\"Submit Query\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"500\"></input></xsl:when><xsl:otherwise><asp:Button ID=\"btnSubmit\" class=\"button\" runat=\"server\" Text=\"Submit Query\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"500\" /></xsl:otherwise></xsl:choose><xsl:if test=\"search/@screenflag[.='2']\"><asp:Button ID=\"btnCancel\" class=\"button\" runat=\"server\" Text=\"Cancel\" onClientClick=\"window.close();return false\" tabindex=\"500\" /></xsl:if><asp:Button ID=\"btnClear\" class=\"button\" runat=\"server\" Text=\"Clear\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /></td></tr></xsl:if></table><asp:HiddenField id=\"viewid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@viewid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"dsnid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@dsnid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"catid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@catid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"tablerestrict\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@tablerestrict\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"codefieldrestrict\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@codefieldrestrict\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"codefieldrestrictid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@codefieldrestrictid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"defaultviewid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@defaultviewid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdScreenFlag\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@screenflag\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdtablename\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@tablename\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdrowid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@rowid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdeventdate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@eventdate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdclaimdate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@claimdate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdpolicydate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@policydate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdfilter\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@filter\" /></xsl:attribute></asp:HiddenField><asp:HiddenField ID=\"hdFormName\" Value=\"\" runat=\"server\" /><asp:HiddenField ID=\"hdCriteriaXML\" Value=\"\" runat=\"server\" /><asp:HiddenField ID=\"hdSysFormName\" Value=\"\" runat=\"server\" /><!--Added by csingh7 : R5 merge 12545--><asp:HiddenField ID=\"hdnInProcess\" runat=\"server\" /><asp:HiddenField ID=\"hdSelectedOrgLevels\" Value=\"\" runat=\"server\" /><!--Added by abansal23 : MITS 17648--><asp:HiddenField ID=\"SysFormName\" Value=\"search\" runat=\"server\" /><!--Added by rsushilaggar : MITS 20309--><asp:HiddenField ID=\"hdAllowPolicySearch\" Value=\"\" runat=\"server\" /><!-- - averma62 MITS 25163- Policy Interface Implementation--></form></body></html></xsl:template><xsl:template match=\"search\"><tr><td colspan=\"2\" class=\"ctrlgroup\"><xsl:value-of select=\"@name\" /></td><td colspan=\"2\" class=\"ctrlgroup\" align=\"right\"><xsl:apply-templates select=\"views\" /></td><!-- PJS-MITS #14973--><!--<input type=\"button\" class=\"button\" runat=\"server\" value=\"Set as Default\" id=\"btnsetasdefault\"  onclick=\"return btnSetAsDefaultClick();\"></input>--></tr></xsl:template><xsl:template match=\"views\"><!-- asp:DropDownList runat=\"server\" id=\"cboViews\" AutoPostBack=\"True\" >\r\n    <xsl:for-each select=\"view\">\r\n      <asp:ListItem />\r\n      <asp:ListItem>\r\n        <xsl:attribute name=\"value\">\r\n        <xsl:value-of select=\"@id\"/>\r\n        </xsl:attribute>\r\n        <xsl:value-of select=\"@name\"/>\r\n      </asp:ListItem>\r\n    </xsl:for-each>\r\n  </asp:DropDownList --><select name=\"cboViews\" size=\"1\" onchange=\"OnViewChange('searchmain.aspx')\"><option /><xsl:for-each select=\"view\"><xsl:sort select=\"@name\" /><!-- Mits 15728:Asif --><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"@name\" /></option></xsl:for-each></select><input type=\"button\" class=\"button\" runat=\"server\" value=\"Set as Default\" id=\"btnsetasdefault\" onclick=\"return btnSetAsDefaultClick();\"></input></xsl:template><xsl:template match=\"displayfields\"><TR><TD colSpan=\"1\"><select id=\"orderby1\" name=\"orderby1\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD><TD colSpan=\"1\"><select id=\"orderby2\" name=\"orderby2\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD><TD colSpan=\"1\"><select id=\"orderby3\" name=\"orderby3\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD></TR></xsl:template><xsl:template match=\"searchfields/field\"><tr><td><xsl:value-of select=\"text()\" />:\r\n\t\t</td><xsl:choose><xsl:when test=\"@type[.='text']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"50\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='ssn']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"50\" onblur=\"ssnLostFocus(this);\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='freecode']\"><!--MGaba2:MITS 16476: Added support for Multi Text/Codes  --><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" TextMode=\"MultiLine\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"Columns\"><xsl:value-of select=\"@cols\" /></xsl:attribute><xsl:attribute name=\"Rows\"><xsl:value-of select=\"@rows\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />btn\r\n            </xsl:attribute><xsl:attribute name=\"onClick\">\r\n              CodeSelect('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search','')\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute></input></td></xsl:when><xsl:when test=\"@type[.='textml']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" TextMode=\"MultiLine\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"Columns\"><xsl:value-of select=\"@cols\" /></xsl:attribute><xsl:attribute name=\"Rows\"><xsl:value-of select=\"@rows\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='code']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute><xsl:attribute name=\"onkeydown\">\r\n              javascript:return onEnter('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n              javascript:return onLostFocus('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search');\r\n            </xsl:attribute><xsl:if test=\"@value\"><xsl:attribute name=\"value\"><xsl:value-of select=\"@value\" /></xsl:attribute></xsl:if><xsl:choose><xsl:when test=\"@name[.='LINE_OF_BUS_CODE']\"><xsl:choose><xsl:when test=\"/search/@tablerestrict[.='gc']\"><xsl:attribute name=\"value\">GC General Claims</xsl:attribute></xsl:when><xsl:when test=\"/search/@tablerestrict[.='va']\"><xsl:attribute name=\"value\">VA Vehicle Accident Claims</xsl:attribute></xsl:when><xsl:when test=\"/search/@tablerestrict[.='wc']\"><xsl:attribute name=\"value\">WC Workers' Compensation</xsl:attribute></xsl:when></xsl:choose></xsl:when></xsl:choose><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">CodeSelect('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search','')</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><xsl:choose><xsl:when test=\"@name[.='LINE_OF_BUS_CODE']\"><xsl:choose><xsl:when test=\"/search/@tablerestrict[.='gc']\"><asp:HiddenField Value=\"241\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='va']\"><asp:HiddenField Value=\"242\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='wc']\"><asp:HiddenField Value=\"243\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='di']\"><asp:HiddenField Value=\"844\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:otherwise><asp:HiddenField Value=\"0\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><asp:HiddenField runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_tableid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@table\" /></xsl:attribute></asp:HiddenField><asp:HiddenField runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute><xsl:if test=\"@codeid\"><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></xsl:if></asp:HiddenField></xsl:otherwise></xsl:choose></td></xsl:when><xsl:when test=\"@type[.='date']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">Between</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:if test=\"@readonly[.='true']\"><xsl:attribute name=\"readonly\">true</xsl:attribute><xsl:attribute name=\"style\">background-color: #F2F2F2;</xsl:attribute></xsl:if><xsl:attribute name=\"onchange\"><xsl:if test=\"@onchangefunctionname\"><xsl:value-of select=\"@onchangefunctionname\" /></xsl:if>datachanged('true')<xsl:if test=\"@onchangepost\"><xsl:value-of select=\"@onchangepost\" /></xsl:if></xsl:attribute><xsl:attribute name=\"onblur\">dateLostFocus(this.id);</xsl:attribute></asp:TextBox><asp:button class=\"DateLookupControl\" runat=\"server\"><xsl:attribute name=\"id\">btnone<xsl:value-of select=\"@id\" /></xsl:attribute><xsl:if test=\"@tabindex\"><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute><xsl:if test=\"@readonly\"><xsl:attribute name=\"disabled\"><xsl:value-of select=\"@readonly\" /></xsl:attribute></xsl:if></xsl:if></asp:button><script type=\"text/javascript\">\r\n            Zapatec.Calendar.setup(\r\n            {\r\n            inputField : \"<xsl:value-of select=\"@id\" />start\",\r\n            ifFormat : \"%m/%d/%Y\",\r\n            button : \"btnone<xsl:value-of select=\"@id\" />\"\r\n            }\r\n            );\r\n          </script><asp:TextBox runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:if test=\"@readonly[.='true']\"><xsl:attribute name=\"readonly\">true</xsl:attribute><xsl:attribute name=\"style\">background-color: #F2F2F2;</xsl:attribute></xsl:if><xsl:attribute name=\"onchange\"><xsl:if test=\"@onchangefunctionname\"><xsl:value-of select=\"@onchangefunctionname\" /></xsl:if>datachanged('true')<xsl:if test=\"@onchangepost\"><xsl:value-of select=\"@onchangepost\" /></xsl:if></xsl:attribute><xsl:attribute name=\"onblur\">dateLostFocus(this.id);</xsl:attribute></asp:TextBox><asp:button class=\"DateLookupControl\" runat=\"server\"><xsl:attribute name=\"id\">btntwo<xsl:value-of select=\"@id\" /></xsl:attribute><xsl:if test=\"@tabindex\"><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute><xsl:if test=\"@readonly\"><xsl:attribute name=\"disabled\"><xsl:value-of select=\"@readonly\" /></xsl:attribute></xsl:if></xsl:if></asp:button><script type=\"text/javascript\">\r\n            Zapatec.Calendar.setup(\r\n            {\r\n            inputField : \"<xsl:value-of select=\"@id\" />end\",\r\n            ifFormat : \"%m/%d/%Y\",\r\n            button : \"btntwo<xsl:value-of select=\"@id\" />\"\r\n            }\r\n            );\r\n          </script></td></xsl:when><xsl:when test=\"@type[.='time']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">Between</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"15\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">timeLostFocus(this.id);</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"15\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">timeLostFocus(this.id);</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><!--input type=\"text\" size=\"15\" onblur=\"timeLostFocus(this.name);\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\"/>start</xsl:attribute></input>\r\n\t\t\t\t<input type=\"text\" size=\"15\" onblur=\"timeLostFocus(this.name);\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\"/>end</xsl:attribute></input --></td></xsl:when><xsl:when test=\"@type[.='orgh']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute><xsl:attribute name=\"onkeydown\">\r\n              javascript:return onEnter('orgh','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n                javascript:return onOrgLostFocus('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />');                \r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField><!-- input type=\"hidden\" value=\"0\">\r\n            <xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute>\r\n            <xsl:attribute name=\"name\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute>\r\n          </input --></td></xsl:when><xsl:when test=\"@type[.='checkbox']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><xsl:choose><xsl:when test=\"/search/@defaultcheckboxvalue[.='True']\"><td><asp:CheckBox runat=\"server\" value=\"1\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"checked\">true</xsl:attribute></asp:CheckBox></td></xsl:when><xsl:otherwise><td><asp:CheckBox runat=\"server\" value=\"0\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"checked\">false</xsl:attribute></asp:CheckBox></td></xsl:otherwise></xsl:choose></xsl:when><xsl:when test=\"@type[.='numeric' or .='double']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterNum(event,this);\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n              javascript:return numLostFocus(this);\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='currency']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">Between</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><!--ybhaskar: Code added in currency--><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>--><!--<xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>\r\n            <xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='tablelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><select size=\"1\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n                javascript:return AssignValue(this.options[this.selectedIndex].value,'<xsl:value-of select=\"@id\" />');\r\n              </xsl:attribute><option value=\"\"></option><xsl:for-each select=\"tablevalue\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@tableid\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:for-each></select></td><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField></xsl:when><!-- Need to uncomment the 'codelist' and 'entitylist' controls and find a way to create options differently --><xsl:when test=\"@type[.='codelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectCode('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','','');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><xsl:when test=\"@type[.='entitylist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel\r\n            </xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><!--skhare7--><xsl:when test=\"@type[.='attachedrecord']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\"> return SelectAttachedOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><asp:ListItem value=\" \"></asp:ListItem><asp:ListItem value=\"claim\">Claim</asp:ListItem><asp:ListItem value=\"event\">Event</asp:ListItem><asp:ListItem value=\"policy\">Policy</asp:ListItem><asp:ListItem value=\"policyenh\">Enhanced Policy</asp:ListItem><asp:ListItem value=\"entity\">Entity</asp:ListItem><asp:ListItem value=\"funds\">Funds</asp:ListItem><asp:ListItem value=\"pi\">Person Involved</asp:ListItem><asp:ListItem value=\"claimant\">Claimant</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"18\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />Label</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"readonly\"><xsl:value-of select=\"true\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"28\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:otherwise><td>\r\n\t\t\t\tUnknown Element Type: <xsl:value-of select=\"@type\" /></td></xsl:otherwise></xsl:choose></tr></xsl:template></xsl:stylesheet>");

            //objDoc.LoadXml("<?xml version=\"1.0\"?><xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:asp=\"MyNS\" version=\"1.0\"><xsl:template match=\"/\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head></head><body><form id=\"frmData\" name=\"frmData\" runat=\"server\" method=\"post\"><xsl:if test=\"search/@screenflag[.='4']\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td colspan=\"3\" class=\"msgheader\">Executive Summary Report (Step 1 of 2)</td></tr><br /><tr><td class=\"small\">\r\n                    Use this search screen to find the <xsl:value-of select=\"search/@formname\" /> record for which to generate an Executive Summary Report.<br />\r\n                    Note: The current Executive Summary Configuration will be used when generating this report.\r\n                    Use the Configuration menu item at the left to change\\customize these settings.\r\n                  </td></tr></table></xsl:if><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit1\" type=\"button\" value=\"Submit Query\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"1\"></input><asp:Button ID=\"btnClear2\" class=\"button\" runat=\"server\" Text=\"Clear\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /><!-- csingh7 : MITS 18830 Added id attribute--></xsl:when><xsl:otherwise><asp:Button ID=\"btnSubmit1\" class=\"button\" runat=\"server\" Text=\"Submit Query\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"1\" /><asp:Button ID=\"btnClear3\" class=\"button\" runat=\"server\" Text=\"Clear\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /><!-- csingh7 : MITS 18830 Added id attribute--></xsl:otherwise></xsl:choose><br /><img src=\"/RiskmasterUI/Images/empty.gif\" width=\"1px\" height=\"2px\" /></td></tr></xsl:if><xsl:apply-templates select=\"search\" /><tr><td><BR /></td></tr><tr><td colspan=\"3\" class=\"ctrlgroup\">Search Criteria</td></tr><xsl:apply-templates select=\"search/searchfields/field\" /></table><BR /><table border=\"0\"><tr><td colspan=\"3\" class=\"ctrlgroup\">Sort By</td></tr><xsl:apply-templates select=\"search/displayfields\" /></table><table border=\"0\"><tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"soundex\" id=\"soundex\" value=\"1\" tabindex=\"500\"><xsl:if test=\"search/@soundex[.='-1' or .='1']\"><xsl:attribute name=\"checked\">checked</xsl:attribute></xsl:if>Use Sound-Alike (Soundex) Match on Last Names\r\n                  </input></td></tr><tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"setasdefault\" value=\"1\" onclick=\"SetDefaultView()\" tabindex=\"500\" /> Default Search View\r\n                </td></tr><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit\" type=\"button\" value=\"Submit Query\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"500\"></input></xsl:when><xsl:otherwise><asp:Button ID=\"btnSubmit\" class=\"button\" runat=\"server\" Text=\"Submit Query\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"500\" /></xsl:otherwise></xsl:choose><xsl:if test=\"search/@screenflag[.='2']\"><asp:Button ID=\"btnCancel\" class=\"button\" runat=\"server\" Text=\"Cancel\" onClientClick=\"window.close();return false\" tabindex=\"500\" /></xsl:if><asp:Button ID=\"btnClear\" class=\"button\" runat=\"server\" Text=\"Clear\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /></td></tr></xsl:if></table><asp:HiddenField id=\"viewid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@viewid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"dsnid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@dsnid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"catid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@catid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"tablerestrict\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@tablerestrict\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"codefieldrestrict\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@codefieldrestrict\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"codefieldrestrictid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@codefieldrestrictid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"defaultviewid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@defaultviewid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdScreenFlag\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@screenflag\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdtablename\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@tablename\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdrowid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@rowid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdeventdate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@eventdate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdclaimdate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@claimdate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdpolicydate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@policydate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdfilter\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@filter\" /></xsl:attribute></asp:HiddenField><asp:HiddenField ID=\"hdFormName\" Value=\"\" runat=\"server\" /><asp:HiddenField ID=\"hdCriteriaXML\" Value=\"\" runat=\"server\" /><asp:HiddenField ID=\"hdSysFormName\" Value=\"\" runat=\"server\" /><!--Added by csingh7 : R5 merge 12545--><asp:HiddenField ID=\"hdnInProcess\" runat=\"server\" /><asp:HiddenField ID=\"hdSelectedOrgLevels\" Value=\"\" runat=\"server\" /><!--Added by abansal23 : MITS 17648--><asp:HiddenField ID=\"SysFormName\" Value=\"search\" runat=\"server\" /><!--Added by rsushilaggar : MITS 20309--><asp:HiddenField ID=\"hdAllowPolicySearch\" Value=\"\" runat=\"server\" /><!-- - averma62 MITS 25163- Policy Interface Implementation--></form></body></html></xsl:template><xsl:template match=\"search\"><tr><td colspan=\"2\" class=\"ctrlgroup\"><xsl:value-of select=\"@name\" /></td><td colspan=\"2\" class=\"ctrlgroup\" align=\"right\"><xsl:apply-templates select=\"views\" /></td><!-- PJS-MITS #14973--><!--<input type=\"button\" class=\"button\" runat=\"server\" value=\"Set as Default\" id=\"btnsetasdefault\"  onclick=\"return btnSetAsDefaultClick();\"></input>--></tr></xsl:template><xsl:template match=\"views\"><!-- asp:DropDownList runat=\"server\" id=\"cboViews\" AutoPostBack=\"True\" >\r\n    <xsl:for-each select=\"view\">\r\n      <asp:ListItem />\r\n      <asp:ListItem>\r\n        <xsl:attribute name=\"value\">\r\n        <xsl:value-of select=\"@id\"/>\r\n        </xsl:attribute>\r\n        <xsl:value-of select=\"@name\"/>\r\n      </asp:ListItem>\r\n    </xsl:for-each>\r\n  </asp:DropDownList --><select name=\"cboViews\" size=\"1\" onchange=\"OnViewChange('searchmain.aspx')\"><option /><xsl:for-each select=\"view\"><xsl:sort select=\"@name\" /><!-- Mits 15728:Asif --><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"@name\" /></option></xsl:for-each></select><input type=\"button\" class=\"button\" runat=\"server\" value=\"Set as Default\" id=\"btnsetasdefault\" onclick=\"return btnSetAsDefaultClick();\"></input></xsl:template><xsl:template match=\"displayfields\"><TR><TD colSpan=\"1\"><select id=\"orderby1\" name=\"orderby1\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD><TD colSpan=\"1\"><select id=\"orderby2\" name=\"orderby2\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD><TD colSpan=\"1\"><select id=\"orderby3\" name=\"orderby3\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD></TR></xsl:template><xsl:template match=\"searchfields/field\"><tr><td><xsl:value-of select=\"text()\" />:\r\n\t\t</td><xsl:choose><xsl:when test=\"@type[.='text']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"50\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='ssn']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"50\" onblur=\"ssnLostFocus(this);\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='freecode']\"><!--MGaba2:MITS 16476: Added support for Multi Text/Codes  --><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" TextMode=\"MultiLine\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"Columns\"><xsl:value-of select=\"@cols\" /></xsl:attribute><xsl:attribute name=\"Rows\"><xsl:value-of select=\"@rows\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />btn\r\n            </xsl:attribute><xsl:attribute name=\"onClick\">\r\n              CodeSelect('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search','')\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute></input></td></xsl:when><xsl:when test=\"@type[.='textml']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" TextMode=\"MultiLine\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"Columns\"><xsl:value-of select=\"@cols\" /></xsl:attribute><xsl:attribute name=\"Rows\"><xsl:value-of select=\"@rows\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='code']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute><xsl:attribute name=\"onkeydown\">\r\n              javascript:return onEnter('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n              javascript:return onLostFocus('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search');\r\n            </xsl:attribute><xsl:if test=\"@value\"><xsl:attribute name=\"value\"><xsl:value-of select=\"@value\" /></xsl:attribute></xsl:if><xsl:choose><xsl:when test=\"@name[.='LINE_OF_BUS_CODE']\"><xsl:choose><xsl:when test=\"/search/@tablerestrict[.='gc']\"><xsl:attribute name=\"value\">GC General Claims</xsl:attribute></xsl:when><xsl:when test=\"/search/@tablerestrict[.='va']\"><xsl:attribute name=\"value\">VA Vehicle Accident Claims</xsl:attribute></xsl:when><xsl:when test=\"/search/@tablerestrict[.='wc']\"><xsl:attribute name=\"value\">WC Workers' Compensation</xsl:attribute></xsl:when></xsl:choose></xsl:when></xsl:choose><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">CodeSelect('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search','')</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><xsl:choose><xsl:when test=\"@name[.='LINE_OF_BUS_CODE']\"><xsl:choose><xsl:when test=\"/search/@tablerestrict[.='gc']\"><asp:HiddenField Value=\"241\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='va']\"><asp:HiddenField Value=\"242\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='wc']\"><asp:HiddenField Value=\"243\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='di']\"><asp:HiddenField Value=\"844\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:otherwise><asp:HiddenField Value=\"0\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><asp:HiddenField runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_tableid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@table\" /></xsl:attribute></asp:HiddenField><asp:HiddenField runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute><xsl:if test=\"@codeid\"><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></xsl:if></asp:HiddenField></xsl:otherwise></xsl:choose></td></xsl:when><xsl:when test=\"@type[.='date']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">Between</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:if test=\"@readonly[.='true']\"><xsl:attribute name=\"readonly\">true</xsl:attribute><xsl:attribute name=\"style\">background-color: #F2F2F2;</xsl:attribute></xsl:if><xsl:attribute name=\"onchange\"><xsl:if test=\"@onchangefunctionname\"><xsl:value-of select=\"@onchangefunctionname\" /></xsl:if>datachanged('true')<xsl:if test=\"@onchangepost\"><xsl:value-of select=\"@onchangepost\" /></xsl:if></xsl:attribute><xsl:attribute name=\"onblur\">dateLostFocus(this.id);</xsl:attribute></asp:TextBox><asp:button class=\"DateLookupControl\" runat=\"server\"><xsl:attribute name=\"id\">btnone<xsl:value-of select=\"@id\" /></xsl:attribute><xsl:if test=\"@tabindex\"><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute><xsl:if test=\"@readonly\"><xsl:attribute name=\"disabled\"><xsl:value-of select=\"@readonly\" /></xsl:attribute></xsl:if></xsl:if></asp:button><script type=\"text/javascript\">\r\n            Zapatec.Calendar.setup(\r\n            {\r\n            inputField : \"<xsl:value-of select=\"@id\" />start\",\r\n            ifFormat : \"%m/%d/%Y\",\r\n            button : \"btnone<xsl:value-of select=\"@id\" />\"\r\n            }\r\n            );\r\n          </script><asp:TextBox runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:if test=\"@readonly[.='true']\"><xsl:attribute name=\"readonly\">true</xsl:attribute><xsl:attribute name=\"style\">background-color: #F2F2F2;</xsl:attribute></xsl:if><xsl:attribute name=\"onchange\"><xsl:if test=\"@onchangefunctionname\"><xsl:value-of select=\"@onchangefunctionname\" /></xsl:if>datachanged('true')<xsl:if test=\"@onchangepost\"><xsl:value-of select=\"@onchangepost\" /></xsl:if></xsl:attribute><xsl:attribute name=\"onblur\">dateLostFocus(this.id);</xsl:attribute></asp:TextBox><asp:button class=\"DateLookupControl\" runat=\"server\"><xsl:attribute name=\"id\">btntwo<xsl:value-of select=\"@id\" /></xsl:attribute><xsl:if test=\"@tabindex\"><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute><xsl:if test=\"@readonly\"><xsl:attribute name=\"disabled\"><xsl:value-of select=\"@readonly\" /></xsl:attribute></xsl:if></xsl:if></asp:button><script type=\"text/javascript\">\r\n            Zapatec.Calendar.setup(\r\n            {\r\n            inputField : \"<xsl:value-of select=\"@id\" />end\",\r\n            ifFormat : \"%m/%d/%Y\",\r\n            button : \"btntwo<xsl:value-of select=\"@id\" />\"\r\n            }\r\n            );\r\n          </script></td></xsl:when><xsl:when test=\"@type[.='time']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">Between</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"15\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">timeLostFocus(this.id);</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"15\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">timeLostFocus(this.id);</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><!--input type=\"text\" size=\"15\" onblur=\"timeLostFocus(this.name);\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\"/>start</xsl:attribute></input>\r\n\t\t\t\t<input type=\"text\" size=\"15\" onblur=\"timeLostFocus(this.name);\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\"/>end</xsl:attribute></input --></td></xsl:when><xsl:when test=\"@type[.='orgh']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute><xsl:attribute name=\"onkeydown\">\r\n              javascript:return onEnter('orgh','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n                javascript:return onOrgLostFocus('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />');                \r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField><!-- input type=\"hidden\" value=\"0\">\r\n            <xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute>\r\n            <xsl:attribute name=\"name\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute>\r\n          </input --></td></xsl:when><xsl:when test=\"@type[.='checkbox']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><xsl:choose><xsl:when test=\"/search/@defaultcheckboxvalue[.='True']\"><td><asp:CheckBox runat=\"server\" value=\"1\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"checked\">true</xsl:attribute></asp:CheckBox></td></xsl:when><xsl:otherwise><td><asp:CheckBox runat=\"server\" value=\"0\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"checked\">false</xsl:attribute></asp:CheckBox></td></xsl:otherwise></xsl:choose></xsl:when><xsl:when test=\"@type[.='numeric' or .='double']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterNum(event,this);\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n              javascript:return numLostFocus(this);\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='currency']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">Between</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><!--ybhaskar: Code added in currency--><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>--><!--<xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>\r\n            <xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='tablelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><select size=\"1\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n                javascript:return AssignValue(this.options[this.selectedIndex].value,'<xsl:value-of select=\"@id\" />');\r\n              </xsl:attribute><option value=\"\"></option><xsl:for-each select=\"tablevalue\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@tableid\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:for-each></select></td><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField></xsl:when><!-- Need to uncomment the 'codelist' and 'entitylist' controls and find a way to create options differently --><xsl:when test=\"@type[.='codelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectCode('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','','');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><xsl:when test=\"@type[.='entitylist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel\r\n            </xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><!--skhare7--><xsl:when test=\"@type[.='attachedrecord']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\"> return SelectAttachedOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><asp:ListItem value=\" \"></asp:ListItem><asp:ListItem value=\"claim\">Claim</asp:ListItem><asp:ListItem value=\"event\">Event</asp:ListItem><asp:ListItem value=\"policy\">Policy</asp:ListItem><asp:ListItem value=\"policyenh\">Enhanced Policy</asp:ListItem><asp:ListItem value=\"entity\">Entity</asp:ListItem><asp:ListItem value=\"funds\">Funds</asp:ListItem><asp:ListItem value=\"pi\">Person Involved</asp:ListItem><asp:ListItem value=\"claimant\">Claimant</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"18\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />Label</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"readonly\"><xsl:value-of select=\"true\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"28\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:otherwise><td>\r\n\t\t\t\tUnknown Element Type: <xsl:value-of select=\"@type\" /></td></xsl:otherwise></xsl:choose></tr></xsl:template></xsl:stylesheet>");
            //End //nnithiyanand MITS: 23637 DT04/23/2013

            //MITS ID: 33230 - nkaranam2
            //JIRA-RMA-1535 start           
            // objDoc.LoadXml("<?xml version=\"1.0\"?><xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:asp=\"MyNS\" version=\"1.0\"><xsl:template match=\"/\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head></head><body><form id=\"frmData\" name=\"frmData\" runat=\"server\" method=\"post\"><xsl:if test=\"search/@screenflag[.='4']\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td colspan=\"3\" class=\"msgheader\">Executive Summary Report (Step 1 of 2)</td></tr><br /><tr><td class=\"small\">\r\n                    Use this search screen to find the <xsl:value-of select=\"search/@formname\" /> record for which to generate an Executive Summary Report.<br />\r\n                    Note: The current Executive Summary Configuration will be used when generating this report.\r\n                    Use the Configuration menu item at the left to change\\customize these settings.\r\n                  </td></tr></table></xsl:if><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit1\" type=\"button\" value=\"Submit Query\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"1\"></input><asp:Button ID=\"btnClear2\" class=\"button\" runat=\"server\" Text=\"Clear\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /><!-- csingh7 : MITS 18830 Added id attribute--></xsl:when><xsl:otherwise><asp:Button ID=\"btnSubmit1\" class=\"button\" runat=\"server\" Text=\"Submit Query\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"1\" /><asp:Button ID=\"btnClear3\" class=\"button\" runat=\"server\" Text=\"Clear\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /><!-- csingh7 : MITS 18830 Added id attribute--></xsl:otherwise></xsl:choose><br /><img src=\"/RiskmasterUI/Images/empty.gif\" width=\"1px\" height=\"2px\" /></td></tr></xsl:if><xsl:apply-templates select=\"search\" /><tr><td><BR /></td></tr><tr><td colspan=\"3\" class=\"ctrlgroup\">Search Criteria</td></tr><xsl:apply-templates select=\"search/searchfields/field\" /></table><BR /><table border=\"0\"><tr><td colspan=\"3\" class=\"ctrlgroup\">Sort By</td></tr><xsl:apply-templates select=\"search/displayfields\" /></table><table border=\"0\"><tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"soundex\" id=\"soundex\" value=\"1\" tabindex=\"500\"><xsl:if test=\"search/@soundex[.='-1' or .='1']\"><xsl:attribute name=\"checked\">checked</xsl:attribute></xsl:if>Use Sound-Alike (Soundex) Match on Last Names\r\n                  </input></td></tr><tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"setasdefault\" value=\"1\" onclick=\"SetDefaultView()\" tabindex=\"500\" /> Default Search View\r\n                </td></tr><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit\" type=\"button\" value=\"Submit Query\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"500\"></input></xsl:when><xsl:otherwise><asp:Button ID=\"btnSubmit\" class=\"button\" runat=\"server\" Text=\"Submit Query\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"500\" /></xsl:otherwise></xsl:choose><xsl:if test=\"search/@screenflag[.='2']\"><asp:Button ID=\"btnCancel\" class=\"button\" runat=\"server\" Text=\"Cancel\" onClientClick=\"window.close();return false\" tabindex=\"500\" /></xsl:if><asp:Button ID=\"btnClear\" class=\"button\" runat=\"server\" Text=\"Clear\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /></td></tr></xsl:if></table><asp:HiddenField id=\"viewid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@viewid\" 

            StringBuilder sXml = new StringBuilder("<?xml version=\"1.0\"?><xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:asp=\"MyNS\" version=\"1.0\"><xsl:template match=\"/\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head></head><body><form id=\"frmData\" name=\"frmData\" runat=\"server\" method=\"post\"><xsl:if test=\"search/@screenflag[.='4']\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td colspan=\"3\" class=\"msgheader\">Executive Summary Report (Step 1 of 2)</td></tr><br /><tr><td class=\"small\">\r\n                    Use this search screen to find the <xsl:value-of select=\"search/@formname\" /> record for which to generate an Executive Summary Report.<br />\r\n                    Note: The current Executive Summary Configuration will be used when generating this report.\r\n                    Use the Configuration menu item at the left to change\\customize these settings.\r\n                  </td></tr></table></xsl:if><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit1\" type=\"button\" value=\"");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnSubmitQuery", "4"));
            sXml = sXml.Append("\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"1\"></input><asp:Button ID=\"btnClear2\" class=\"button\" runat=\"server\" Text=\"");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnClear", "4"));
            sXml = sXml.Append("\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /><!-- csingh7 : MITS 18830 Added id attribute--></xsl:when> <xsl:otherwise><asp:Button ID=\"btnSubmit1\" class=\"button\" runat=\"server\" Text=\"");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnSubmitQuery", "4"));
            //RMA-345 Starts    achouhan3   Redirect on the Search Result page as per the Browser Compatibality
            //sXml = sXml.Append("\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"1\" /><asp:Button ID=\"btnClear3\" class=\"button\" runat=\"server\" Text=\"");
            sXml = sXml.Append("\" PostBackUrl=\"~/UI/Search/");
            sXml.Append(strSearchFrmName);
            sXml = sXml.Append("\" tabindex=\"1\" /><asp:Button ID=\"btnClear3\" class=\"button\" runat=\"server\" Text=\"");
            //RMA-345 Ends    achouhan3   Redirect on the Search Result page as per the Browser Compatibality
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnClear", "2"));
            sXml = sXml.Append("\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /><!-- csingh7 : MITS 18830 Added id attribute--></xsl:otherwise></xsl:choose><br /><img src=\"/RiskmasterUI/Images/empty.gif\" width=\"1px\" height=\"2px\" /></td></tr></xsl:if><xsl:apply-templates select=\"search\" /><tr><td><BR /></td></tr><tr><td colspan=\"3\" class=\"ctrlgroup\">");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "lblSearchCriteria", "0"));
            sXml = sXml.Append("</td></tr><xsl:apply-templates select=\"search/searchfields/field\" /></table><BR /><table border=\"0\"><tr><td colspan=\"3\" class=\"ctrlgroup\">");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "lblSortBy", "0"));

            sXml = sXml.Append("</td></tr><xsl:apply-templates select=\"search/displayfields\" /></table><table border=\"0\"><tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"soundex\" id=\"soundex\" value=\"1\" tabindex=\"500\"><xsl:if test=\"search/@soundex[.='-1' or .='1']\"><xsl:attribute name=\"checked\">checked</xsl:attribute></xsl:if>");
            //sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "chkUseSoundAlike", "0"));
            //AA start Address Master
                sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "chkUseSoundAlike", "0"));
            //avipinsrivas start : Worked for JIRA - 7767
                //if (sViewEntityFlag.CompareTo("-1") == 0 && !bHideGlobalSearch)//skhare7 JIRA 340 Entity role start
                //{
                //    sXml = sXml.Append("\r\n                  </input></td></tr>  <tr><td align=\"left\" colspan=\"3\"><input type=\"checkbox\" name=\"fullentitysearch\" id=\"fullentitysearch\" value=\"1\" onclick=\"SetUsefullEntitySearch()\" tabindex=\"500\"><xsl:if test=\"search/@fullentitysearch[.='-1' or .='1']\"><xsl:attribute name=\"checked\">checked</xsl:attribute></xsl:if>");
                //    sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "chkUseGlobalSearch", "0"));
                //}
                sXml = sXml.Append("<asp:HiddenField ID=\"hdhideglobalsearch\" Value=\"\" runat=\"server\" />");            //avipinsrivas Start : Worked for JIRAs - 9610 (Issue-4634 / Epic-340)
                //skhare7 JIRA 340 Entity role end
            //avipinsrivas end
            sXml = sXml.Append("\r\n                  </input></td></tr><tr><td align=\"left\" colspan=\"3\"><asp:CheckBox ID=\"setasdefault\" value=\"1\" onclick=\"SetDefaultView()\" tabindex=\"500\" runat=\"server\" Text=\"");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "chkDefaultSearchView", "0"));
            sXml = sXml.Append("\" /> \r\n </td></tr><xsl:if test=\"search/@catid[.!='nosubmit']\"><tr><td align=\"left\" colspan=\"3\"><!-- Integration with Nav tree : Check if the search screen is being opened as a popup --><xsl:choose><xsl:when test=\"search/@screenflag[.='1']\"><input id=\"btnSubmit\" type=\"button\" value=\"");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnSubmitQuery", "4"));
            sXml = sXml.Append("\" class=\"button\" onclick=\"parent.MDISearchResult(this.form)\" tabindex=\"500\"></input></xsl:when><xsl:otherwise><asp:Button ID=\"btnSubmit\" class=\"button\" runat=\"server\" Text=\"");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnSubmitQuery", "4"));           
            //RMA-345 Starts    achouhan3   Redirect on the Search Result page as per the Browser Compatibality
            //sXml = sXml.Append("\" PostBackUrl=\"~/UI/Search/SearchResults.aspx\" tabindex=\"500\" /></xsl:otherwise></xsl:choose><xsl:if test=\"search/@screenflag[.='2']\"><asp:Button ID=\"btnCancel\" class=\"button\" runat=\"server\" Text=\"Cancel\" onClientClick=\"window.close();return false\" tabindex=\"500\" /></xsl:if><asp:Button ID=\"btnClear\" class=\"button\" runat=\"server\" Text=\"");
            sXml = sXml.Append("\" PostBackUrl=\"~/UI/Search/");
            sXml.Append(strSearchFrmName);
            sXml = sXml.Append("\" tabindex=\"500\" /></xsl:otherwise></xsl:choose><xsl:if test=\"search/@screenflag[.='2']\"><asp:Button ID=\"btnCancel\" class=\"button\" runat=\"server\" Text=\"Cancel\" onClientClick=\"window.close();return false\" tabindex=\"500\" /></xsl:if><asp:Button ID=\"btnClear\" class=\"button\" runat=\"server\" Text=\"");
            //RMA-345 Ends    achouhan3   Redirect on the Search Result page as per the Browser Compatibality
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnClear", "4"));
            sXml = sXml.Append("\" onClientClick=\"this.form.reset();OnFormClear();return false\" tabindex=\"500\" /></td></tr></xsl:if></table><asp:HiddenField id=\"viewid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@viewid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"dsnid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@dsnid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"catid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@catid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"tablerestrict\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@tablerestrict\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"codefieldrestrict\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@codefieldrestrict\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"codefieldrestrictid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@codefieldrestrictid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"defaultviewid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@defaultviewid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdScreenFlag\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@screenflag\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdtablename\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@tablename\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdrowid\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@rowid\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdeventdate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@eventdate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdclaimdate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@claimdate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdpolicydate\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@policydate\" /></xsl:attribute></asp:HiddenField><asp:HiddenField id=\"hdfilter\" runat=\"server\"><xsl:attribute name=\"value\"><xsl:value-of select=\"search/@filter\" /></xsl:attribute></asp:HiddenField><asp:HiddenField ID=\"hdFormName\" Value=\"\" runat=\"server\" /><asp:HiddenField ID=\"hdCriteriaXML\" Value=\"\" runat=\"server\" /><asp:HiddenField ID=\"hdSysFormName\" Value=\"\" runat=\"server\" /><!--Added by csingh7 : R5 merge 12545--><asp:HiddenField ID=\"hdnInProcess\" runat=\"server\" /><asp:HiddenField ID=\"hdSelectedOrgLevels\" Value=\"\" runat=\"server\" /><!--Added by abansal23 : MITS 17648--><asp:HiddenField ID=\"SysFormName\" Value=\"search\" runat=\"server\" /><!--Added by rsushilaggar : MITS 20309--><asp:HiddenField ID=\"hdAllowPolicySearch\" Value=\"\" runat=\"server\" /><!-- - averma62 MITS 25163- Policy Interface Implementation--><asp:HiddenField ID=\"hdLangId\" Value=\"\" runat=\"server\" /><asp:HiddenField ID=\"hdPageId\" Value=\"\" runat=\"server\" /></form></body></html></xsl:template><xsl:template match=\"search\"><tr><td colspan=\"2\" class=\"ctrlgroup\"><xsl:value-of select=\"@name\" /></td><td colspan=\"2\" class=\"ctrlgroup\" align=\"right\"><xsl:apply-templates select=\"views\" /></td><!-- PJS-MITS #14973--><!--<input type=\"button\" class=\"button\" runat=\"server\" value=\"Set as Default\" id=\"btnsetasdefault\"  onclick=\"return btnSetAsDefaultClick();\"></input>--></tr></xsl:template><xsl:template match=\"views\"><!-- asp:DropDownList runat=\"server\" id=\"cboViews\" AutoPostBack=\"True\" >\r\n    <xsl:for-each select=\"view\">\r\n      <asp:ListItem />\r\n      <asp:ListItem>\r\n        <xsl:attribute name=\"value\">\r\n        <xsl:value-of select=\"@id\"/>\r\n        </xsl:attribute>\r\n        <xsl:value-of select=\"@name\"/>\r\n      </asp:ListItem>\r\n    </xsl:for-each>\r\n  </asp:DropDownList -->");            //avipinsrivas Start : Worked for Jira-340
            //if (!blnIsFromPersonInvolved)
            //{
            sXml = sXml.Append("<select name=\"cboViews\" id=\"cboViews\" size=\"1\" onchange=\"OnViewChange('searchmain.aspx')\"><option /><xsl:for-each select=\"view\"><xsl:sort select=\"@name\" /><!-- Mits 15728:Asif --><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"@name\" /></option></xsl:for-each></select><input type=\"button\" class=\"button\" runat=\"server\" value=\"");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "btnSetAsDefault", "4"));
            sXml = sXml.Append("\" id=\"btnsetasdefault\" onclick=\"return btnSetAsDefaultClick();\"></input>");
            //}
            ////avipinsrivas end
            sXml = sXml.Append("</xsl:template><xsl:template match=\"displayfields\"><TR><TD colSpan=\"1\"><select id=\"orderby1\" name=\"orderby1\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD><TD colSpan=\"1\"><select id=\"orderby2\" name=\"orderby2\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD><TD colSpan=\"1\"><select id=\"orderby3\" name=\"orderby3\" size=\"1\" tabindex=\"500\"><option value=\"\"></option><xsl:for-each select=\"field\"><!--gagnihotri MITS 19281 Disabling sorting for Comments column--><xsl:choose><!--MGaba2:MITS 19947:Multi Text/Codes supplemental field should not be present in \"Sort By\" combo box.--><!--<xsl:when test=\"@type!='textml'\">--><xsl:when test=\"@type[.!='textml' and .!='freecode']\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:when></xsl:choose></xsl:for-each></select></TD></TR></xsl:template><xsl:template match=\"searchfields/field\"><tr><td><xsl:value-of select=\"text()\" />:\r\n\t\t</td><xsl:choose><xsl:when test=\"@type[.='text']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"50\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='ssn']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"50\" onblur=\"ssnLostFocus(this);\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='freecode']\"><!--MGaba2:MITS 16476: Added support for Multi Text/Codes  --><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" TextMode=\"MultiLine\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"Columns\"><xsl:value-of select=\"@cols\" /></xsl:attribute><xsl:attribute name=\"Rows\"><xsl:value-of select=\"@rows\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />btn\r\n            </xsl:attribute><xsl:attribute name=\"onClick\">\r\n              CodeSelect('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search','')\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute></input></td></xsl:when><xsl:when test=\"@type[.='textml']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" TextMode=\"MultiLine\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"Columns\"><xsl:value-of select=\"@cols\" /></xsl:attribute><xsl:attribute name=\"Rows\"><xsl:value-of select=\"@rows\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='code']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute><xsl:attribute name=\"onkeydown\">\r\n              javascript:return onEnter('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n              javascript:return onLostFocus('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search');\r\n            </xsl:attribute><xsl:if test=\"@value\"><xsl:attribute name=\"value\"><xsl:value-of select=\"@value\" /></xsl:attribute></xsl:if><xsl:choose><xsl:when test=\"@name[.='LINE_OF_BUS_CODE']\"><xsl:choose><xsl:when test=\"/search/@tablerestrict[.='gc']\"><xsl:attribute name=\"value\">GC General Claims</xsl:attribute></xsl:when><xsl:when test=\"/search/@tablerestrict[.='va']\"><xsl:attribute name=\"value\">VA Vehicle Accident Claims</xsl:attribute></xsl:when><xsl:when test=\"/search/@tablerestrict[.='wc']\"><xsl:attribute name=\"value\">WC Workers' Compensation</xsl:attribute></xsl:when></xsl:choose></xsl:when></xsl:choose><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">CodeSelect('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','Search','')</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><xsl:choose><xsl:when test=\"@name[.='LINE_OF_BUS_CODE']\"><xsl:choose><xsl:when test=\"/search/@tablerestrict[.='gc']\"><asp:HiddenField Value=\"241\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='va']\"><asp:HiddenField Value=\"242\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='wc']\"><asp:HiddenField Value=\"243\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:when test=\"/search/@tablerestrict[.='di']\"><asp:HiddenField Value=\"844\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:when><xsl:otherwise><asp:HiddenField Value=\"0\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><asp:HiddenField runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_tableid</xsl:attribute><xsl:attribute name=\"value\"><xsl:value-of select=\"@table\" /></xsl:attribute></asp:HiddenField><asp:HiddenField runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute><xsl:if test=\"@codeid\"><xsl:attribute name=\"value\"><xsl:value-of select=\"@codeid\" /></xsl:attribute></xsl:if></asp:HiddenField></xsl:otherwise></xsl:choose></td></xsl:when><xsl:when test=\"@type[.='date']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "lblBetween", "0"));
            sXml = sXml.Append("</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:if test=\"@readonly[.='true']\"><xsl:attribute name=\"readonly\">true</xsl:attribute><xsl:attribute name=\"style\">background-color: #F2F2F2;</xsl:attribute></xsl:if><xsl:attribute name=\"onchange\"><xsl:if test=\"@onchangefunctionname\"><xsl:value-of select=\"@onchangefunctionname\" /></xsl:if>datachanged('true')<xsl:if test=\"@onchangepost\"><xsl:value-of select=\"@onchangepost\" /></xsl:if></xsl:attribute><xsl:attribute name=\"onblur\">dateLostFocus(this.id);</xsl:attribute></asp:TextBox>");
            sXml = sXml.Append("<script type=\"text/javascript\">\r\n            ");
            sXml = sXml.Append(" $(function ()\r\n            ");
            sXml = sXml.Append("{\r\n            ");
            sXml = sXml.Append("$(\"#<xsl:value-of select=\"@id\" />start\").datepicker({\r\n            ");
            sXml = sXml.Append(" showOn: \"button\",\r\n            ");
            sXml = sXml.Append("buttonImage: \"../../Images/calendar.gif\",\r\n            ");
      
            sXml = sXml.Append("showOtherMonths: true,\r\n            ");
            sXml = sXml.Append("selectOtherMonths: true,\r\n            ");
            sXml = sXml.Append("changeYear: true");
            sXml = sXml.Append(" }).next('button.ui-datepicker-trigger').css({border: 'none', background:'none'})\r\n            ");
            sXml = sXml.Append("});\r\n          ");
            sXml = sXml.Append("</script>");
            sXml = sXml.Append("<asp:TextBox runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:if test=\"@readonly[.='true']\"><xsl:attribute name=\"readonly\">true</xsl:attribute><xsl:attribute name=\"style\">background-color: #F2F2F2;</xsl:attribute></xsl:if><xsl:attribute name=\"onchange\"><xsl:if test=\"@onchangefunctionname\"><xsl:value-of select=\"@onchangefunctionname\" /></xsl:if>datachanged('true')<xsl:if test=\"@onchangepost\"><xsl:value-of select=\"@onchangepost\" /></xsl:if></xsl:attribute><xsl:attribute name=\"onblur\">dateLostFocus(this.id);</xsl:attribute></asp:TextBox>");
            sXml = sXml.Append("<script type=\"text/javascript\">\r\n            ");
            sXml = sXml.Append(" $(function ()\r\n            ");
            sXml = sXml.Append("{\r\n            ");
            sXml = sXml.Append("$(\"#<xsl:value-of select=\"@id\" />end\").datepicker({\r\n            ");
            sXml = sXml.Append(" showOn: \"button\",\r\n            ");
            sXml = sXml.Append("buttonImage: \"../../Images/calendar.gif\",\r\n            ");
           
            sXml = sXml.Append("showOtherMonths: true,\r\n            ");
            sXml = sXml.Append("selectOtherMonths: true,\r\n            ");
            sXml = sXml.Append("changeYear: true");
            sXml = sXml.Append(" }).next('button.ui-datepicker-trigger').css({border: 'none', background:'none'})\r\n            ");
            sXml = sXml.Append("});\r\n          ");
            sXml = sXml.Append("</script>");
            sXml = sXml.Append("</td></xsl:when><xsl:when test=\"@type[.='time']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "lblBetween", "0"));
            sXml = sXml.Append("</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"15\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">timeLostFocus(this.id);</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"15\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">timeLostFocus(this.id);</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><!--input type=\"text\" size=\"15\" onblur=\"timeLostFocus(this.name);\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\"/>start</xsl:attribute></input>\r\n\t\t\t\t<input type=\"text\" size=\"15\" onblur=\"timeLostFocus(this.name);\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\"/>end</xsl:attribute></input --></td></xsl:when><xsl:when test=\"@type[.='orgh']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute><xsl:attribute name=\"onkeydown\">\r\n              javascript:return onEnter('orgh','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n                javascript:return onOrgLostFocus('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />');                \r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField><!-- input type=\"hidden\" value=\"0\">\r\n            <xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute>\r\n            <xsl:attribute name=\"name\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute>\r\n          </input --></td></xsl:when><xsl:when test=\"@type[.='checkbox']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><xsl:choose><xsl:when test=\"/search/@defaultcheckboxvalue[.='True']\"><td><asp:CheckBox runat=\"server\" value=\"1\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"checked\">true</xsl:attribute></asp:CheckBox></td></xsl:when><xsl:otherwise><td><asp:CheckBox runat=\"server\" value=\"0\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"checked\">false</xsl:attribute></asp:CheckBox></td></xsl:otherwise></xsl:choose></xsl:when><xsl:when test=\"@type[.='numeric' or .='double']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterNum(event,this);\r\n            </xsl:attribute><xsl:attribute name=\"onblur\">\r\n              javascript:return numLostFocus(this);\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='currency']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\">return BetweenOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');</xsl:attribute><asp:ListItem value=\"between\">");
            sXml = sXml.Append(AppHelper.GetResourceValue(this.sSearchMainPageId, "lblBetween", "0"));
            //sXml = sXml.Append("</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><!--ybhaskar: Code added in currency--><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>--><!--<xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>\r\n            <xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='tablelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><select size=\"1\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n                javascript:return AssignValue(this.options[this.selectedIndex].value,'<xsl:value-of select=\"@id\" />');\r\n              </xsl:attribute><option value=\"\"></option><xsl:for-each select=\"tablevalue\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@tableid\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:for-each></select></td><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField></xsl:when><!-- Need to uncomment the 'codelist' and 'entitylist' controls and find a way to create options differently --><xsl:when test=\"@type[.='codelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectCode('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','','');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><xsl:when test=\"@type[.='entity']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute>      <xsl:attribute name=\"onkeydown\">        \r\n              javascript:return onEnter('orgh','<xsl:value-of select=\"@id\" />');\r\n      </xsl:attribute>      <xsl:attribute name=\"onblur\">        \r\n                javascript:return onOrgLostFocus('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />');                \r\n      </xsl:attribute>      <xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">        javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></td></xsl:when><xsl:when test=\"@type[.='entitylist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel\r\n            </xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><!--skhare7--><xsl:when test=\"@type[.='attachedrecord']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\"> return SelectAttachedOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><asp:ListItem value=\" \"></asp:ListItem><asp:ListItem value=\"claim\">Claim</asp:ListItem><asp:ListItem value=\"event\">Event</asp:ListItem><asp:ListItem value=\"policy\">Policy</asp:ListItem><asp:ListItem value=\"policyenh\">Enhanced Policy</asp:ListItem><asp:ListItem value=\"entity\">Entity</asp:ListItem><asp:ListItem value=\"funds\">Funds</asp:ListItem><asp:ListItem value=\"pi\">Person Involved</asp:ListItem><asp:ListItem value=\"claimant\">Claimant</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"18\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />Label</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"readonly\"><xsl:value-of select=\"true\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"28\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:otherwise><td>\r\n\t\t\t\tUnknown Element Type: <xsl:value-of select=\"@type\" /></td></xsl:otherwise></xsl:choose></tr></xsl:template></xsl:stylesheet>");
            sXml = sXml.Append("</asp:ListItem><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem><asp:ListItem value=\"&gt;\">&gt;</asp:ListItem><asp:ListItem value=\"&gt;=\">&gt;=</asp:ListItem><asp:ListItem value=\"&lt;\">&lt;</asp:ListItem><asp:ListItem value=\"&lt;=\">&lt;=</asp:ListItem></asp:DropDownList></td><td><!--ybhaskar: Code added in currency--><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />start</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>--><!--<xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />end</xsl:attribute><!--<xsl:attribute name=\"onKeyPress\">\r\n              javascript:return checkEnterCurrency(event,this);\r\n            </xsl:attribute>\r\n            <xsl:attribute name=\"onblur\">\r\n              javascript:return currencyLostFocus(this);\r\n            </xsl:attribute>--><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\" /></xsl:attribute><xsl:attribute name=\"RMXType\"><xsl:value-of select=\"@type\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='tablelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><select size=\"1\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n                javascript:return AssignValue(this.options[this.selectedIndex].value,'<xsl:value-of select=\"@id\" />');\r\n              </xsl:attribute><option value=\"\"></option><xsl:for-each select=\"tablevalue\"><option><xsl:attribute name=\"value\"><xsl:value-of select=\"@tableid\" /></xsl:attribute><xsl:value-of select=\"text()\" /></option></xsl:for-each></select></td><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField></xsl:when><!-- Need to uncomment the 'codelist' and 'entitylist' controls and find a way to create options differently --><xsl:when test=\"@type[.='codelist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectCode('<xsl:value-of select=\"@codetable\" />','<xsl:value-of select=\"@id\" />','','');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><xsl:when test=\"@type[.='entity']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"30\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"onchange\">\r\n              javascript:return datachanged('true');\r\n            </xsl:attribute>      <xsl:attribute name=\"onkeydown\">        \r\n              javascript:return onEnter('orgh','<xsl:value-of select=\"@id\" />');\r\n      </xsl:attribute>      <xsl:attribute name=\"onblur\">        \r\n                javascript:return onOrgLostFocus('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />');                \r\n      </xsl:attribute>      <xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox><input type=\"button\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">        javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_cid</xsl:attribute></asp:HiddenField></td></xsl:when><xsl:when test=\"@type[.='entitylist']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></td><td><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:ListBox><!-- select size=\"3\"><xsl:attribute name=\"name\"><xsl:value-of select=\"@name\"/></xsl:attribute>\r\n\t\t\t\t<xsl:apply-templates select=\"option\">\r\n\t\t\t\t\t<xsl:template><xsl:copy><xsl:apply-templates select=\"@* | * | comment() | pi() | text()\"/></xsl:copy></xsl:template>\r\n\t\t\t\t</xsl:apply-templates>\r\n\t\t\t\t</select --><input type=\"button\" class=\"CodeLookupControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btn</xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return selectOrg('orgh','<xsl:value-of select=\"@id\" />','<xsl:value-of select=\"@level\" />','<xsl:value-of select=\"@table\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><input type=\"button\" class=\"button\" value=\" - \"><xsl:attribute name=\"id\"><xsl:value-of select=\"@name\" />btndel\r\n            </xsl:attribute><xsl:attribute name=\"onClick\">\r\n              javascript:return deleteSelCode('<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></input><asp:HiddenField Value=\"\" runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />_lst</xsl:attribute></asp:HiddenField><!-- xsl:attribute name=\"value\"><xsl:for-each select=\"option\"><xsl:value-of select=\"@value\"/><xsl:if test=\"context()[not(end())]\">,</xsl:if></xsl:for-each></xsl:attribute --></td></xsl:when><!--skhare7--><xsl:when test=\"@type[.='attachedrecord']\"><td><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><xsl:attribute name=\"onchange\"> return SelectAttachedOption('<xsl:value-of select=\"@type\" />','<xsl:value-of select=\"@id\" />');\r\n            </xsl:attribute><asp:ListItem value=\" \"></asp:ListItem><asp:ListItem value=\"claim\">Claim</asp:ListItem><asp:ListItem value=\"event\">Event</asp:ListItem><asp:ListItem value=\"policy\">Policy</asp:ListItem><asp:ListItem value=\"policyenh\">Enhanced Policy</asp:ListItem><asp:ListItem value=\"entity\">Entity</asp:ListItem><asp:ListItem value=\"funds\">Funds</asp:ListItem><asp:ListItem value=\"pi\">Person Involved</asp:ListItem><asp:ListItem value=\"claimant\">Claimant</asp:ListItem></asp:DropDownList></td><td><asp:TextBox runat=\"server\" size=\"18\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />Label</xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"readonly\"><xsl:value-of select=\"true\" /></xsl:attribute></asp:TextBox><asp:TextBox runat=\"server\" size=\"28\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></asp:TextBox></td></xsl:when><xsl:when test=\"@type[.='userlookup']\"><td><xsl:choose><xsl:when test=\"@multiselect ='-1'\"><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">in</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">not in</asp:ListItem></asp:DropDownList></xsl:when><xsl:otherwise><asp:DropDownList runat=\"server\" tabindex=\"500\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />op</xsl:attribute><asp:ListItem value=\"=\">=</asp:ListItem><asp:ListItem value=\"&lt;&gt;\">&lt;&gt;</asp:ListItem></asp:DropDownList></xsl:otherwise></xsl:choose></td><td><xsl:choose><xsl:when test=\"@multiselect='-1'\"><asp:ListBox runat=\"server\" Rows=\"3\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" /></xsl:attribute><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute><xsl:attribute name=\"multiselect\"><xsl:value-of select=\"@multiselect\"/></xsl:attribute><xsl:attribute name=\"usergroups\"><xsl:value-of select=\"@usergroups\"/></xsl:attribute></asp:ListBox></xsl:when><xsl:otherwise><asp:TextBox runat=\"server\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\"/></xsl:attribute><xsl:attribute name=\"onchange\">RemoveExistingValues('<xsl:value-of select=\"@id\"/>');</xsl:attribute><xsl:attribute name=\"multiselect\"><xsl:value-of select=\"@multiselect\"/></xsl:attribute><xsl:attribute name=\"usergroups\"><xsl:value-of select=\"@usergroups\"/></xsl:attribute><xsl:if test=\"@tabindex\"><xsl:attribute name=\"Tabindex\"><xsl:value-of select=\"@tabindex\" /></xsl:attribute></xsl:if></asp:TextBox> </xsl:otherwise></xsl:choose><asp:button runat=\"server\" class=\"EllipsisControl\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\"/>_usrgrpbtn</xsl:attribute><xsl:if test=\"@tabindex\"><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+1\" /></xsl:attribute></xsl:if><xsl:attribute name=\"onclientclick\">return AddSuppCustomListUserLookup(this.name,'<xsl:value-of select=\"@id\"/>','UserIdStr','UserStr','<xsl:value-of select=\"@multiselect\"/>','<xsl:value-of select=\"@usergroups\"/>','true');</xsl:attribute><xsl:if test=\" (@PowerViewReadOnly[.='true'])\"><xsl:attribute name=\"disabled\">true</xsl:attribute></xsl:if></asp:button><xsl:if test=\"@multiselect ='-1'\"><asp:button runat=\"server\" class=\"BtnRemove\"><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\" />btndel</xsl:attribute><xsl:if test=\"@tabindex\"><xsl:attribute name=\"tabindex\"><xsl:value-of select=\"number(@tabindex)+2\" /></xsl:attribute></xsl:if><xsl:attribute name=\"onclientclick\">return deleteSelectedUserGroup('<xsl:value-of select=\"@id\"/>')</xsl:attribute><xsl:attribute name=\"Text\">-</xsl:attribute><xsl:if test=\" (@PowerViewReadOnly[.='true'])\"><xsl:attribute name=\"disabled\">true</xsl:attribute></xsl:if></asp:button></xsl:if><asp:TextBox runat=\"server\"><xsl:attribute name=\"RMXRef\"><xsl:value-of select=\"@ref\"/>/@codeid</xsl:attribute><xsl:attribute name=\"style\">display:none;</xsl:attribute><xsl:attribute name=\"id\"><xsl:value-of select=\"@id\"/>_lst</xsl:attribute></asp:TextBox></td></xsl:when><xsl:otherwise><td>\r\n\t\t\t\tUnknown Element Type: <xsl:value-of select=\"@type\" /></td></xsl:otherwise></xsl:choose></tr></xsl:template></xsl:stylesheet>");
            objDoc.LoadXml(sXml.ToString());
            //JIRA-RMA-1535 end
            return objDoc;

        }

    }

}
