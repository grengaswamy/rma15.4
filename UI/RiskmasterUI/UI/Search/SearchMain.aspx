﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Riskmaster.UI.Search.SearchMain" Codebehind="SearchMain.aspx.cs" ValidateRequest="false" %>
<%--<%@ OutputCache CacheProfile="RMXSearchCacheProfile" %>MITS 26195 Raman : Commenting caching till we get it right--%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>			
<script language="javaScript" src="../../Scripts/search.js" type="text/javascript"></script>			

<link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>

<title>Search</title>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();OnPageLoad();" onkeydown="SubmitQuery(event);">
<uc1:ErrorControl ID="ErrorControl" runat="server" />
</body>
</html>
