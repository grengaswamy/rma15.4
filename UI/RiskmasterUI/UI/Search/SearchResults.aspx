﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Riskmaster.UI.Search.SearchResults" Codebehind="SearchResults.aspx.cs" ValidateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Search Results</title>

	<script language="javascript" src="../../Scripts/searchresults.js" type="text/javascript"></script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div>
        <div class="image">
			  <asp:ImageButton runat="server" OnClientClick="PrintSearch();return false;" ID="btnPrint" 
                    AlternateText="Print" ImageUrl="../../Images/tb_print_active.png" ToolTip="<%$ Resources:ttPrint %>" 
                    width="28" height="28" border="0" alt="" 
                    onMouseOver="this.src='../../Images/tb_print_mo.png';this.style.zoom='110%'" 
                    onMouseOut="this.src='../../Images/tb_print_active.png';this.style.zoom='100%'" 
                    />
		</div>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
<tr>
<td><asp:Label ID="lblPagerTop" runat="server"></asp:Label></td>

<td align="right">
<asp:Label ID="lblPageRangeTop" runat="server"></asp:Label>
  <asp:LinkButton id="lnkFirstTop"   ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First" Text="<%$ Resources:lnkFirst %>"></asp:LinkButton> 
  <asp:LinkButton id="lnkPrevTop" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev" Text="<%$ Resources:lnkPrevious %>"></asp:LinkButton>
    <asp:LinkButton id="lnkNextTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next" Text="<%$ Resources:lnkNext %>"></asp:LinkButton>
    <asp:LinkButton id="lnkLastTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last" Text="<%$ Resources:lnkLast %>"></asp:LinkButton>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="4">
									<tr>
										<td class="msgheader" nowrap="" colspan="">
									    <asp:Label ID="lblSearch" runat="server" Text="<%$ Resources:lblSearch %>"></asp:Label>
										</td>
									</tr>
						
</table>

    <asp:GridView ID="grdSearchResults" runat="server" AllowSorting="true"
        onrowdatabound="grdSearchResults_RowDataBound" onsorting="grdSearchResults_Sorting" 
        CellPadding="4"
         >
        <HeaderStyle CssClass="colheader6" ForeColor="White"/>


    </asp:GridView>


								


<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td><asp:Label ID="lblPagerDown" runat="server"></asp:Label></td>

<td align="right">
<asp:Label ID="lblPageRangeDown" runat="server"></asp:Label>  
  <asp:LinkButton id="lnkFirstDown"   ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First" Text="<%$ Resources:lnkFirst %>"></asp:LinkButton> 
    <asp:LinkButton id="lnkPrevDown" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev" Text="<%$ Resources:lnkPrevious %>"></asp:LinkButton>
    <asp:LinkButton id="lnkNextDown" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next" Text="<%$ Resources:lnkNext %>"></asp:LinkButton>
    <asp:LinkButton id="lnkLastDown" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last" Text="<%$ Resources:lnkLast %>"></asp:LinkButton>
</td>
</tr>
</table>

    <br></br>
    
    <%if (hdScreenFlag.Value == "2")
      { %>
    <div align="center"> 
        <asp:Button ID="btnReturn" Text="<%$ Resources:btnReturn %>" CssClass="button" onclientclick="self.history.back();return false;" runat="server"  />
        <asp:Button ID="btnCancel" class="button" runat="server" Text="<%$ Resources:btnCancel %>" onClientClick="window.close();return false"/>
    </div>
    <%} %>
    <asp:HiddenField ID="hdSortOrder" runat="server"/>
    <asp:HiddenField ID="hdSortColumn" runat="server"/>
    <asp:HiddenField ID="searchcat" runat="server"/>
    <asp:HiddenField ID="admtable" runat="server"/>
    <asp:HiddenField ID="hdCriteriaXml" runat="server"/>
    <asp:HiddenField ID="hdTotalPages" runat="server"/>
    <asp:HiddenField ID="hdCurrentPage" runat="server"/>
    <asp:HiddenField ID="hdPageSize" runat="server"/>
    <asp:HiddenField ID="hdTotalRows" runat="server"/>
    <asp:HiddenField ID="hdHeaderText" runat="server"/>
    <asp:HiddenField ID="hdScreenFlag" runat="server"/>
    <asp:HiddenField ID="hdNoSortIndex" runat="server" />
    <asp:HiddenField ID="hdtablename" runat="server" />
    <asp:HiddenField ID="hdrowid" runat="server" />
    <asp:HiddenField ID="hdeventdate" runat="server" />
    <asp:HiddenField ID="hdclaimdate" runat="server" />
    <asp:HiddenField ID="hdpolicydate" runat="server" />
    <asp:HiddenField ID="hdfilter" runat="server" />    
  <%--  skhare7 --%>
   <asp:HiddenField ID="hdnAttachedPrompt" runat="server" /> 
    </ContentTemplate>
    </asp:UpdatePanel>
    
    
    </form>
</body>
</html>
