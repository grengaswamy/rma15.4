﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchRender.aspx.cs" Inherits="Riskmaster.UI.Search.SearchRender" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Search Render</title>
    <script language="javascript" type="text/javascript">
		function haveProperty(obj, sPropName)
		{
			for(p in obj)
			{
				if(p==sPropName)
					return true;
			}
			return false;
		}
		function onDataLoaded()
		{
			if(haveProperty(window.opener,"lookupCallback"))
			{
				if(window.opener.lookupCallback!=null)
				{
					eval("window.opener."+window.opener.lookupCallback+"(-1)");
					return true;
				}
			}
			self.close();
			return true;
		}    
	</script>
</head>
<body onload="onDataLoaded();">
    <div>
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <table align="center" width="100%" border="0">
        <tr>
	        <td valign="middle" align="center">
		        <img  src="../../Images/loading1.gif" />
	        </td>

        </tr>
    </table>

    </div>
</body>
</html>
