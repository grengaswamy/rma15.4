﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common; //mbahl3 mits 30224 for multilingual changes
using Riskmaster.RMXResourceManager;
using System.Xml.XPath;
using Riskmaster.Common.Extensions;
using System.Web.Script.Services;
using Riskmaster.Common;
using System.Dynamic;
using System.Data;
using System.Xml;
using Newtonsoft.Json;
using Riskmaster.BusinessHelpers;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;



namespace Riskmaster.UI.UI.Search
{
 
    public partial class AsyncSearchResult : System.Web.UI.Page
    {
        XElement m_objMessageElement = null;
        string Const_Width = "150";
        XElement objXmlCriteria = null;
        XDocument messageElement = null;
        string sReturn = string.Empty;
        XElement resultDoc = null;
        string strSortField = String.Empty;
        string strSortDirection = String.Empty;
        XElement objElement = null;//dnehe
        string strSearch = string.Empty; //dnehe
        protected void Page_Load(object sender, EventArgs e)
        {
            #region javascript registration
           
            string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);
            string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

            string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);
           
            #endregion
            XElement xmlIn = null;
            XmlElement objUserPrefElement = null;
            string sSortDir = ""; string sSortColName = "";
            //start rsushilaggar JIRA 7767
            string showAddNew = Request.QueryString["showaddnew"];
            Control ctrl = this.FindControl("btnAddNew");
            if (ctrl != null)
            {
                if (!string.IsNullOrEmpty(showAddNew) && showAddNew == "true")
                {

                    ctrl.Visible = true;
                }
                else
                {
                    ctrl.Visible = false;
                }
            }
            //end rsushilaggar
            if (Request.IsAjaxRequest())
            {
                if (Request.QueryString["call"] != null)
                {
                    switch (Request.QueryString["call"].ToLower())
                    {
                        case "getdata":
                            GetData();
                            break;
                        case "savepreferences":
                            SavePreferences();
                            break;
                    }
                }
            }
            try
            {
                if (!Page.IsPostBack)
                {
                    //mbahl3 for multilingual mits 30224 
                    string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("SearchResults.aspx"), "CreateEntityValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "CreateEntityValidationsScripts", sValidationResources, true);
                    string sValResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("SearchResults.aspx"), "SearchValidations", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "SearchValidations", sValidationResources, true);
                    string strValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AsyncSearchResult.aspx"), "CreateAsyncSearchLabel", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "CreateAsyncSearchLabel", strValidationResources, true);
                    string strToolTipResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AsyncSearchResult.aspx"), "CreateAsyncSearchTT", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "CreateAsyncSearchTT", strToolTipResources, true);
                    
                    //mbahl3 for multilingual mits 30224 

                    if (Request.Form["hdCriteriaXML"] != null)
                        objXmlCriteria = XElement.Parse(Server.HtmlDecode(Request.Form["hdCriteriaXML"]));

                    if (Request.Form["hdScreenFlag"] != null)
                    {
                        hdScreenFlag.Value = Request.Form["hdScreenFlag"];
                    }
                    PrepareCriteriaXml(objXmlCriteria);
                    objXmlCriteria = UpdateCriteriaXml(objXmlCriteria);

                    //Get User Preference XML
                    messageElement = GetUserPrefMessageTemplate();
                    sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return user preference xml (saved setting) for the search.

                    if (sReturn != "")
                    {
                        resultDoc = XElement.Parse(sReturn);
                    }

                    hdUserPref.Value = Server.UrlEncode(Server.HtmlEncode(resultDoc.ToString()));
                    hdCriteriaXml.Value = Server.UrlEncode(Server.HtmlEncode(objXmlCriteria.ToString()));
                    //RMA-345 Start
                    resultDoc = null;
                    messageElement = null;
                    //RMA-345 End
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                // ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts
            objElement = objXmlCriteria.XPathSelectElement("./Document/Search/SearchMain/FormName");
            strSearch = objElement.Value;
            if (!String.IsNullOrEmpty(hdScreenFlag.Value) && hdScreenFlag.Value.Trim() == "2")
            {
                if (Request.Form["viewid"] != null && !String.IsNullOrEmpty(Request.Form["viewid"]))
                {
                    switch (Request.Form["viewid"].Trim())
                    {
                        case "4":
                            strSearch = "entity";
                            break;
                    }
                }
            }
            hdnEEFormName.Value = strSearch;
        //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality ends
        }

        /// <summary>
        /// Dev Signature       : achouhan3
        /// Date                : 09/09/2014
        /// Description         : Method to Prepare search Criteria XML by extracting Request object keys
        /// </summary>
        /// <param name="p_objCriteriaXML"></param>
        private void PrepareCriteriaXml(XElement p_objCriteriaXML)
        {
            var objFieldXml = from fields in p_objCriteriaXML.Descendants("field")

                              select fields;

            string sFieldID = string.Empty;
            string sOrderBy = string.Empty;

            foreach (XElement field in objFieldXml)
            {
                XText txtNode = field.Nodes().OfType<XText>().First();
                txtNode.Value = string.Empty;
                switch (field.Attribute("type").Value.ToLower())
                {
                    case "text":
                    case "numeric":

                    case "ssn":
                    case "textml":
                    case "freecode": //MITS 16476
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        // Set the value of the Operation XElement.
                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;
                    case "checkbox":
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        // Set the value of the Operation XElement.
                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        // Set the XText of XElement.
                        if (Request.Form[sFieldID] != null)
                            if (Request.Form[sFieldID].ToLower() == "on")
                                field.Add(new XText("-1"));
                        break;

                    case "orgh":
                    case "code":
                    // akaushik5 Added for MITS 38161 Starts
                    case "entity":
                        // akaushik5 Added for MITS 38161 Ends
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("_cid", Request.Form[sFieldID + "_cid"]));
                        field.Add(new XElement("_tableid", Request.Form[sFieldID + "_tableid"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;
                    case "currency":
                        //Code added by ybhaskar for MITS 14684
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("StartCurrency", Request.Form[sFieldID + "start"]));
                        field.Add(new XElement("EndCurrency", Request.Form[sFieldID + "end"]));

                        // Set the XText of XElement.
                        //field.Add(new XText(Request.Form[sFieldID]));

                        break;
                    case "date":
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("StartDate", Request.Form[sFieldID + "start"]));
                        field.Add(new XElement("EndDate", Request.Form[sFieldID + "end"]));

                        // Set the XText of XElement.
                        //field.Add(new XText(Request.Form[sFieldID]));
                        break;

                    case "time":
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("StartTime", Request.Form[sFieldID + "start"]));
                        field.Add(new XElement("EndTime", Request.Form[sFieldID + "end"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;

                    case "tablelist":
                    case "codelist":
                    case "entitylist":
                    // npadhy JIRA 6415 - Handling the search for User Lookup
                    case "userlookup":
                        // Get the field id from the XElement.
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        field.Add(new XElement("_lst", Request.Form[sFieldID + "_lst"]));

                        // Set the XText of XElement.
                        //field.Add(new XText(Request.Form[sFieldID]));
                        break;
                    case "attachedrecord": //skhare7
                        // Get the field id from the XElement.
                        sFieldID = field.Attribute("id").Value;

                        // Set the value of the Operation XElement.
                        field.Add(new XElement("Operation", Request.Form[sFieldID + "op"]));

                        // Set the XText of XElement.
                        field.Add(new XText(Request.Form[sFieldID]));
                        break;

                    default:
                        break;
                }

            }
        }

        private XElement UpdateCriteriaXml(XElement p_objCriteriaXML)
        {
            try
            {
                // npadhy Start MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                string sSysFormName = string.Empty;
                // npadhy End MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                var objFieldXml = from fields in p_objCriteriaXML.Descendants("field")
                                  select fields;


                m_objMessageElement = GetMessageTemplate();
                XElement objTempElement = null;

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                    objTempElement.Value = AppHelper.GetSessionId();
                }//if

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/ViewId");
                if (Request.Form["viewid"] != null)
                    objTempElement.Value = Request.Form["viewid"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/FormName");
                if (Request.Form["hdFormName"] != null)
                    objTempElement.Value = Request.Form["hdFormName"];

                //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/tablename");
                if (Request.Form["hdtablename"] != null)
                    objTempElement.Value = Request.Form["hdtablename"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/rowid");
                if (Request.Form["hdrowid"] != null)
                    objTempElement.Value = Request.Form["hdrowid"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/eventdate");
                if (Request.Form["hdeventdate"] != null)
                    objTempElement.Value = Request.Form["hdeventdate"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/claimdate");
                if (Request.Form["hdclaimdate"] != null)
                    objTempElement.Value = Request.Form["hdclaimdate"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/policydate");
                if (Request.Form["hdpolicydate"] != null)
                    objTempElement.Value = Request.Form["hdpolicydate"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/filter");
                if (Request.Form["hdfilter"] != null)
                    objTempElement.Value = Request.Form["hdfilter"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/TableRestrict");
                if (Request.Form["tablerestrict"] != null)
                {
                    objTempElement.Value = Request.Form["tablerestrict"];

                    // npadhy Start MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                    if (string.Compare(objTempElement.Value, "policyenh", true) == 0)
                    {
                        sSysFormName = AppHelper.GetFormValue("hdSysFormName");
                        if (sSysFormName.StartsWith("policyenh") && AppHelper.GetFormValue("hdScreenFlag") == "2")
                        {
                            objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/filter");
                            objTempElement.Value = sSysFormName.Substring(9).ToUpper();
                        }
                    }
                    // npadhy End MITS 22029 To restrict the Policy records of another LOB when searched from a Policy Screen
                }

                //Tushar"MITS#18229
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/codefieldrestrict");
                if (Request.Form["codefieldrestrict"] != null)
                    objTempElement.Value = Request.Form["codefieldrestrict"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/codefieldrestrictid");
                if (Request.Form["codefieldrestrictid"] != null)
                    objTempElement.Value = Request.Form["codefieldrestrictid"];

                //End:MITS#18229


                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/DisplayFields/OrderBy1");
                if (Request.Form["orderby1"] != null)
                    objTempElement.Value = Request.Form["orderby1"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/DisplayFields/OrderBy2");
                if (Request.Form["orderby2"] != null)
                    objTempElement.Value = Request.Form["orderby2"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/DisplayFields/OrderBy3");
                if (Request.Form["orderby3"] != null)
                    objTempElement.Value = Request.Form["orderby3"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/SysEx");  // csingh7 R5 Merge : 12545
                if (Request.Form["hdSysFormName"] == "funds")
                    objTempElement.Value = "bFunds";

                //abansal23 MITS 17648 10/09/2009 Starts
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/SelOrgLevels");  // csingh7 R5 Merge : 12545
                if (Request.Form["hdSelectedOrgLevels"] != null)
                    objTempElement.Value = Request.Form["hdSelectedOrgLevels"];
                //abansal23 MITS 17648 10/09/2009 Starts

                // Handle Soundex for Search
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/SoundEx");
                string sSoundexChecked = Request.Form["soundex"];

                if (sSoundexChecked != null)
                {
                    objTempElement.Value = "-1";
                }
                else
                {
                    objTempElement.Value = "";
                }
                //Start - averma62 MITS 25163- Policy Interface Implementation
                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/AllowPolicySearch");
                if (Request.Form["hdAllowPolicySearch"] != null)
                    objTempElement.Value = Request.Form["hdAllowPolicySearch"];
                //End  - averma62 MITS 25163- Policy Interface Implementation

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/LangId");
                if (objTempElement != null)
                    objTempElement.Value = Request.Form["hdLangId"];

                objTempElement = m_objMessageElement.XPathSelectElement("./Document/Search/SearchMain/PageId");
                if (objTempElement != null)
                    objTempElement.Value = Request.Form["hdPageId"];

                var objSearchField = from searchfields in m_objMessageElement.Descendants("SearchFields")

                                     select searchfields;


                XElement objSearchFieldElem = objSearchField.ElementAt(0);

                foreach (XElement field in objFieldXml)
                {
                    objSearchFieldElem.Add(field);
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
            }

            return m_objMessageElement;
        }

        /// <summary>
        /// CWS request message template
        /// <CodeFieldRestrict /> added by Tushar:MITS:18229
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            //Abansal23 : MITS 17648 : SelOrgLevels added as node in template
            //averma62 MITS 25163- Policy Interface Implementation
            XElement objTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>SearchAdaptor.SearchRun</Function>
              </Call>
              <Document>
                <Search>
                  <PageNo />
                  <TotalRecords />
                  <SortColPass />
                  <LookUpId>0</LookUpId>
                  <PageSize>0</PageSize>
                  <MaxResults></MaxResults>
                  <SortColumn />
                  <Order>ascending</Order>
                  <IfBack/>
                  <SearchMain>
                    <IfLookUp>0</IfLookUp>
                    <InProcess>0</InProcess>
                    <ScreenFlag>1</ScreenFlag>
                    <ViewId>-1</ViewId>
                    <FormName></FormName>
                    <FormNameTemp />
                    <TableRestrict />
                    <codefieldrestrict />
                    <codefieldrestrictid />
                    <tablename/>
                    <rowid/>
                    <eventdate/>
                    <claimdate/>
                    <policydate/>
                    <filter/>
                    <SysEx />
                    <Settings />
                    <TableID />
                    <SoundEx>-1</SoundEx>
                    <DefaultSearch />
                    <DefaultViewId />
                    <Views />
                    <CatId>2</CatId>
                    <DisplayFields>
                      <OrderBy1 />
                      <OrderBy2 />
                      <OrderBy3 />
                    </DisplayFields>
                    <SearchCat />
                    <AdmTable />
                    <EntityTableId />
                    <Type />
                    <LookUpType />
                    <SearchFields>
                    </SearchFields>
                    <SelOrgLevels />
                    <SettoDefault>-1</SettoDefault>
                    <AllowPolicySearch />
                    <LangId />
                    <PageId />
                  </SearchMain>
                </Search>
              </Document>
            </Message>
            ");

            return objTemplate;
        }

        /// <summary>
        /// Dev Signature      : achouhan3  
        /// Date               : 09/09/2014
        /// Description        : Ajax Method to get Data and pass to client for binding in JSON format
        /// <returns>JSON Object</returns>
        /// </summary>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetData()
        {
            ServiceResponseHelper objResponse = null;
            try
            {
                XElement xmlIn = null;
                XElement objXmlCriteria = null;
                XElement objTempElement = null;
                JavaScriptSerializer reqJson = new JavaScriptSerializer();
                string strCurrentPage = String.Empty;
                string strPageSize = String.Empty;
                string strCriteriaXml = String.Empty;              
                int TotalRecords = 0;
                string strSearchCategory = String.Empty;
                string strViewID = String.Empty;
                bool IsPageIndex = false;
                if (Request.Form["criteriaXML"] != null)
                    strCriteriaXml = Convert.ToString(Request.Form["criteriaXML"]);
                if (Request.Form["sortField"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortField"])))
                    strSortField = Convert.ToString(Request.Form["sortField"]);
                if (Request.Form["sortDirection"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortDirection"])))
                    strSortDirection = Convert.ToString(Request.Form["sortDirection"]) == "desc" ? "descending" : "ascending";
                if (Request.Form["totalRecords"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["totalRecords"])))
                    TotalRecords = Convert.ToInt32(Request.Form["totalRecords"]);
                if (Request.Form["pageNumber"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["pageNumber"])))
                    strCurrentPage = Convert.ToString(Request.Form["pageNumber"]);
                if (Request.Form["pageSize"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["pageSize"])))
                    strPageSize = Convert.ToString(Request.Form["pageSize"]);
                if (Request.Form["IsPageindex"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["IsPageindex"])))
                    IsPageIndex = Convert.ToBoolean(Request.Form["IsPageindex"]);
                if (Request.Form["UserPref"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["UserPref"])))
                    sReturn = Server.UrlDecode(Server.HtmlDecode(Request.Form["UserPref"]));
            
                if (!String.IsNullOrEmpty(sReturn))
                {
                    resultDoc = XElement.Parse(sReturn);

                }
                if (strCriteriaXml != String.Empty)
                {
                    objXmlCriteria = XElement.Parse(Server.UrlDecode(Server.HtmlDecode(strCriteriaXml)));                    
                    objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/SearchMain/FormName");
                    strSearchCategory = objTempElement.Value;

                    //RMA-7158      achouhan3       Entity Look Up blank screen issue starts
                    if (Request.Form["screenFlag"] != null && !String.IsNullOrEmpty(Request.Form["screenFlag"]) && Request.Form["screenFlag"].Trim() == "2")
                    {
                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/SearchMain/ViewId");
                        strViewID = objTempElement.Value;
                        //rkatyal4 : JIRA 19773 start | JIRA 19902 and 19880
                        switch (strViewID.Trim())
                        {
                            case "4":
                                strSearchCategory = "entity";
                                break;
                        }
                        switch (strSearchCategory.Trim())
                        {
                            case "-1":
                                strSearchCategory = "entity";
                                break;
                            case "-4":
                                //rkatyal4 : JIRA 19773 end | JIRA 19902 and 19880
                                strSearchCategory = "entity";
                                break;
                        }
                    }
                    //RMA-7158      achouhan3       Entity Look Up blank screen issue ends
                    XElement xUserSearchNode = resultDoc.XPathSelectElement("//UserPrefSearch/" + strSearchCategory + "search");
                    if (xUserSearchNode != null && String.IsNullOrEmpty(strSortField))
                    {
                        if (xUserSearchNode.Attribute("SortType") != null)
                            strSortDirection = xUserSearchNode.Attribute("SortType").Value;
                        if (xUserSearchNode.Attribute("SortColumn") != null)
                            strSortField = xUserSearchNode.Attribute("SortColumn").Value;
                        if(String.IsNullOrEmpty(strPageSize) && xUserSearchNode.Attribute("PageSize")!=null)
                            strPageSize=xUserSearchNode.Attribute("PageSize").Value;
                    }
                    objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/PageNo");
                    objTempElement.Value = strCurrentPage;
                    if (!IsPageIndex && !String.IsNullOrEmpty(strPageSize))
                    {
                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/PageSize");
                        objTempElement.Value = strPageSize;
                    }
                  
                    if (TotalRecords != 0)
                    {
                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/TotalRecords");
                        objTempElement.Value = TotalRecords.ToString();
                    }
                    if (!String.IsNullOrEmpty(strSortField))
                    {
                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/SortColumn");
                        objTempElement.Value = strSortField;
                    }
                    if (!String.IsNullOrEmpty(strSortDirection))
                    {
                        objTempElement = objXmlCriteria.XPathSelectElement("./Document/Search/Order");
                        objTempElement.Value = strSortDirection;
                    }

                    xmlIn = GetSearchResults(objXmlCriteria);
                    //Get Criteria XML in Response then
                    if (!String.IsNullOrEmpty(strPageSize) && xmlIn.Attribute("pagesize") != null)
                    {
                        xmlIn.Attribute("pagesize").SetValue(strPageSize);
                    }
                    objResponse = BindDataToGrid(xmlIn);
                }
            }
            catch (Exception ex)
            {
                objResponse = new ServiceResponseHelper(false, ex.Message, null);
            }
            var json = JsonConvert.SerializeObject(objResponse);
            Response.Write(json);
            try
            {
                Response.Flush();
                Response.End();
            }
            catch (System.Threading.ThreadAbortException threadEx) { }
        }

        private ServiceResponseHelper BindDataToGrid(XElement p_objResultXml)
        {
            int iNoSortIndex = 0;
            int colIndex = 0;
            string strKeyColumn = String.Empty;
            bool isKey = false;
            const string DATE_STRING = "date";
            const string CURRENCY_STRING = "currency";  //34278
            //MITS#34276  Starts  Entity ID Type and ID Type Vendor permission Starts
            Boolean bEntIDTypePermission = true;
            Boolean bIDTypeVendorPermission = true;
            int indxEntIDType = 0;
            int indxEntIDNum = 0;
            bool isEntityIdnumber = false;
            string SearchCategory = String.Empty;
            string admtableval = String.Empty;//RMA-11641 - msampathkuma
            bool bUseEntityRole = false;        //avipinsrivas start : Worked for JIRA - 7767
            if (p_objResultXml.Attribute("isentidtypeview") != null)
                bEntIDTypePermission = Convert.ToBoolean(p_objResultXml.Attribute("isentidtypeview").Value);
            if (p_objResultXml.Attribute("isidtypevendorview") != null)
                bIDTypeVendorPermission = Convert.ToBoolean(p_objResultXml.Attribute("isidtypevendorview").Value);
            //avipinsrivas start : Worked for JIRA - 7767
            if (p_objResultXml.Attribute("useentityrole") != null && string.Equals(p_objResultXml.Attribute("useentityrole").Value.ToUpper(), "TRUE"))
                bUseEntityRole = true;
            //avipinsrivas end
            //MITS#34276   Ends   Entity ID Type and ID Type Vendor permission Ends
            List<IDictionary<String, object>> lstResponse = new List<IDictionary<String, object>>();
            List<ColumnDefHelper> lstColDef = new List<ColumnDefHelper>();
            XDocument objSearchDoc = XDocument.Load(p_objResultXml.CreateReader());
            //RMA-6138 achouhan3  Starts  Added to support ML
            string strColumnField = String.Empty;
            Regex regexColField = new Regex("[^a-zA-Z0-9_]+");
            int ColCount=1;
            //RMA-6138 achouhan3  Ends  Added to support ML
            if (p_objResultXml!=null && p_objResultXml.Attribute("searchcat") != null)
                SearchCategory = p_objResultXml.Attribute("searchcat").Value;
            //RMA-11641 - msampathkuma
            if (p_objResultXml != null && p_objResultXml.Attribute("admtable") != null)
                admtableval = p_objResultXml.Attribute("admtable").Value;
            List<UserPreferences> lstUserPreference = new List<UserPreferences>();
            if (resultDoc != null)
            {

                IEnumerable<XElement> xSearchNode = resultDoc.XPathSelectElements("//UserPrefSearch/" + SearchCategory + "search/Column");
                foreach (XElement PrefNode in xSearchNode)
                {
                    //RMA-7560  achouhan3       When PageSize is bigger than actual total page size
                    if (PrefNode.Attribute("Name") != null)
                    {
                        lstUserPreference.Add(new UserPreferences
                        {
                            Name = Convert.ToString(PrefNode.Attribute("Name").Value),
                            ColOrder = Convert.ToString(PrefNode.Attribute("ColOrder").Value) == String.Empty ? 0 : Convert.ToInt32(PrefNode.Attribute("ColOrder").Value),
                            ColWidth = Convert.ToString(PrefNode.Attribute("ColWidth").Value),
                            Key = PrefNode.Attribute("Key") != null ? Convert.ToString(PrefNode.Attribute("Key").Value) : "False"
                        });
                    }
                }
            }

            //searchcat.Value = SearchCategory;
            var objColXml = from cols in objSearchDoc.Descendants("column")

                            select cols;

            foreach (XElement column in objColXml)
            {
                //lstColDef.Add(new ColumnDefHelper { field = column.Value.Replace(" ", "_"), displayName = column.Value, visible = true, width = Const_Width
                //, headerCellTemplate="<div ng-click=\"col.sort($event)\" ng-class=\"\'colt\' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ \'width\' : col.width - 14 + \'px\' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>"});
                //RMA-6138 achouhan3 Starts   Added to support ML
                strColumnField = Common.Conversion.ReplaceSpecialCharacterToWords(column.Value);
                if (regexColField.IsMatch(strColumnField))
                    strColumnField = ColCount.ToString();
                //RMA-6138 achouhan3 Ends   Added to support ML

                var PrefColumn = lstUserPreference.Where(X => X.Name == column.Value);
                if (PrefColumn != null && PrefColumn.FirstOrDefault() != null)
                {
                    Const_Width = PrefColumn.FirstOrDefault().ColWidth;
                    colIndex = PrefColumn.FirstOrDefault().ColOrder;
                    if (!String.IsNullOrEmpty(PrefColumn.FirstOrDefault().Key))
                    {
                        isKey = Convert.ToBoolean(PrefColumn.FirstOrDefault().Key);
                        strKeyColumn = column.Value;
                    }
                    //if (PrefColumn.FirstOrDefault().SortOrder)
                    //{
                    //    strSortColumn = PrefColumn.FirstOrDefault().Name.Replace(" ","_");
                    //    strSortDirection = PrefColumn.FirstOrDefault().SortType;
                    //}
                }
                else
                {
                    Const_Width = "150";
                    colIndex = 0;
                    isKey = false;
                }

                //rkotak: rma 11739 starts
                if (column.Attribute("nosort") != null && column.Attribute("nosort").Value.ToLower() == "on")
                {
                    lstColDef.Add(new ColumnDefHelper
                    {
                        field = strColumnField,
                        displayName = column.Value,
                        visible = true,
                        width = Const_Width,
                        Position = colIndex,
                        Key = isKey,
                        
                        headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\"  ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
                        "<div class=\"ngHeaderText\">{{col.displayName}}</div>" +
                        "</div>" +                        
                        "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
                        "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>"
                    });
                }
                else
                {
                    lstColDef.Add(new ColumnDefHelper
                    {
                        field = strColumnField,
                        displayName = column.Value,
                        visible = true,
                        width = Const_Width,
                        Position = colIndex,
                        Key = isKey,
                        headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
                        "<div ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
                        "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
                            //"<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 24 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>"+
                            //"<a class=\"k-grid-filter\" href=\"#\" tabindex=\"-1\" ng-click=\"filterData(col)\"><span class=\"k-icon k-filter\"></span></a>"+
                        "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
                        "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>"
                    });
                }
                //rkotak: rma 11739 ends
                //34278 Start
                //dtSearch.Columns.Add(GetDataColumn(column.Value,dtSearch));

                //dtSearch.Columns.Add(GetDataColumn(column.Value, dtSearch, column.Attribute("type").Value));
                //34278 End
                if (column.Value.ToLower() == "entity id type")
                    indxEntIDType = iNoSortIndex;
                else if (column.Value.ToLower() == "entity id number")
                {
                    isEntityIdnumber = true;
                    indxEntIDNum = iNoSortIndex;
                }
                ColCount++;
            }

            //added by neha swati for MITS # 33409 wwig gap 10
            if (string.Compare(SearchCategory, "claim", StringComparison.OrdinalIgnoreCase) == 0)
            {
                //dtSearch.Columns.Add("restrictedclaimflag");
                lstColDef.Add(new ColumnDefHelper { field = "restrictedclaimflag", width = Const_Width });
            }
            //end
            lstColDef.Add(new ColumnDefHelper { field = "pid", displayName = "pid", visible = false, width = Const_Width });
            lstColDef.Add(new ColumnDefHelper { field = "boolpid", displayName = "boolpid", visible = false, width = Const_Width });

            //Mridul. 11/26/09. MITS:18229
            //if (searchcat.Value.ToLower() == "entity")
            if (string.Compare(SearchCategory, "entity", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(SearchCategory, "policyenh", StringComparison.OrdinalIgnoreCase) == 0)
            {
                lstColDef.Add(new ColumnDefHelper { field = "entityformname", visible = false, width = Const_Width });

            }

            var objRowXml = from rows in objSearchDoc.Descendants("row")

                            select rows;

            string[] arrPid = new string[1];

            foreach (XElement row in objRowXml)
            {

                var objFieldXml = from fields in row.Descendants("field")
                                  select fields;

                int iCount = 0;

                //drSearchRow = dtSearch.NewRow();
                dynamic dynamicExpando = new ExpandoObject();
                var objResponseJson = dynamicExpando as IDictionary<String, object>;
                // Bijender has Modified the code for MIts 14532
                // there is wrong mapping while adding the row to dtSearch table in case of same column name 
                foreach (XElement column in objColXml)
                {
                    if (column.Attribute("type").Value == DATE_STRING)
                    {
                        if (objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value == "")
                        {
                            //drSearchRow[column.Value] = System.DBNull.Value;
                            // drSearchRow[dtSearch.Columns[iCount].ColumnName] = System.DBNull.Value;   
                            //string strcolname= lstColDef[iCount].field;
                            objResponseJson[lstColDef[iCount].field] = System.DBNull.Value;
                        }
                        else
                            //drSearchRow[column.Value] = objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value;
                            //drSearchRow[dtSearch.Columns[iCount].ColumnName] = objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value;
                            objResponseJson[lstColDef[iCount].field] = objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value == String.Empty ? null : objFieldXml.ElementAt(iCount).Attribute("formatedtext").Value;
                    }
                    //34278 Start
                    else if (column.Attribute("type").Value == CURRENCY_STRING)
                    {
                        string sValue = objFieldXml.ElementAt(iCount).Value.Replace(",", "");
                        if (objFieldXml.ElementAt(iCount).Value.Substring(0, 1) == "-")
                        {
                            // drSearchRow[dtSearch.Columns[iCount].ColumnName] = Conversion.ConvertStrToDouble("-" + sValue.Substring(2));
                            objResponseJson[lstColDef[iCount].field] = Conversion.ConvertStrToDouble("-" + sValue.Substring(2));
                        }
                        else
                        {
                            // drSearchRow[dtSearch.Columns[iCount].ColumnName] = Conversion.ConvertStrToDouble(sValue.Substring(1));
                            objResponseJson[lstColDef[iCount].field] = Conversion.ConvertStrToDouble(sValue.Substring(1));
                        }
                        //if (column.Attribute("culture") != null)
                        //{
                            // hdcultureName.Value = column.Attribute("culture").Value;
                        //}
                    }
                    //34278 End
                    else
                    {
                        //MITS#34276   Starts   Entity ID type and ID type Vendor View Permission
                        if ((lstColDef[iCount].displayName.ToLower() == "entity id number" || lstColDef[iCount].field.ToLower() == "entity id type")
                                && !String.IsNullOrEmpty(objFieldXml.ElementAt(iCount).Value))
                        {
                            if (!bEntIDTypePermission)
                                objResponseJson[lstColDef[iCount].field] = "######";
                            else if (!bIDTypeVendorPermission)
                            {
                                string[] arrVendor = objFieldXml.ElementAt(indxEntIDType).Value.Split(' ');
                                if (arrVendor[0] != null && arrVendor[0].ToString().ToLower() == "vendor")
                                {
                                    objFieldXml.ElementAt(indxEntIDType).Value = "######";
                                    if (isEntityIdnumber)
                                        objFieldXml.ElementAt(indxEntIDNum).Value = "######";
                                }

                                objResponseJson[lstColDef[iCount].field] = objFieldXml.ElementAt(iCount).Value == String.Empty ? null : objFieldXml.ElementAt(iCount).Value;
                            }
                            else
                                objResponseJson[lstColDef[iCount].field] = objFieldXml.ElementAt(iCount).Value == String.Empty ? null : objFieldXml.ElementAt(iCount).Value;

                        }
                        //MITS#34276   Ends   Entity ID type and ID type Vendor View Permission
                        else
                            objResponseJson[lstColDef[iCount].field] = objFieldXml.ElementAt(iCount).Value == String.Empty ? null : objFieldXml.ElementAt(iCount).Value;

                    }
                    if (!isKey && iCount == 0)
                    {
                        if (String.IsNullOrEmpty(Convert.ToString(objResponseJson[lstColDef[iCount].field])))
                        {
                            objResponseJson[lstColDef[iCount].field] = "<No Data>";
                        }
                    }
                    else if (!String.IsNullOrEmpty(strKeyColumn) && String.IsNullOrEmpty(Convert.ToString(objResponseJson[lstColDef[iCount].field])) && String.Equals(lstColDef[iCount].displayName, strKeyColumn))
                    {
                        objResponseJson[lstColDef[iCount].field] = "<No Data>";
                    }

                    iCount++;
                }
                //Bijender End Mits 14532
                if (row.Attribute("pid").Value != String.Empty && row.Attribute("pid").Value.Contains("|"))
                {
                    objResponseJson["pid"] = row.Attribute("pid").Value.Split('|')[0].ToString();
                    objResponseJson["boolpid"] = row.Attribute("pid").Value.Split('|')[1].ToString();
                }
                else
                    objResponseJson["pid"] = row.Attribute("pid").Value;

                //added by neha swati for MITS # 33409 wwig gap 10
                if (string.Compare(SearchCategory, "claim", StringComparison.OrdinalIgnoreCase) == 0 && row.Attribute("restrictedclaimflag") != null && objResponseJson.ContainsKey("restrictedclaimflag"))
                    objResponseJson["restrictedclaimflag"] = row.Attribute("restrictedclaimflag").Value;
                //end
                //Mridul. 11/26/09. MITS:18229
                //if (searchcat.Value.ToLower() == "entity")
                if (string.Compare(SearchCategory, "entity", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(SearchCategory, "policyenh", StringComparison.OrdinalIgnoreCase) == 0)
                    objResponseJson["entityformname"] = row.Attribute("entityformname").Value;

                lstResponse.Add(objResponseJson);
            }
            string strFunctionClick = String.Empty;
            //avipinsrivas start : Worked on Code Review Comments for JIRA - 7767
            string sIsActiveEntity = "true";      //avipinsriavs start : Worked for JIRA - 7767
            //List<ColDef> _colDefPID = lstColDef.Where(X => X.field == "pid").ToList<ColDef>();
            if (String.Compare(SearchCategory, "entity", StringComparison.OrdinalIgnoreCase) == 0 || String.Compare(SearchCategory, "policyenh", StringComparison.OrdinalIgnoreCase) == 0)
            {
                //ColDef _colDefs = (ColDef)lstColDef.Where(X => X.field == "entityformname");
                if (lstResponse.FirstOrDefault() != null && lstResponse.FirstOrDefault().ContainsKey("pid"))
                {
                    if ((lstResponse.FirstOrDefault().ContainsKey("boolpid")) && (lstResponse.FirstOrDefault()["boolpid"].ToString() != String.Empty)) //RMA-19606 & RMA-19639
                        sIsActiveEntity = "{{row.getProperty(\'" + lstColDef.Where(X => X.field == "boolpid").FirstOrDefault().field + "\')}}";

                    strFunctionClick = String.Format("selRecord({0},\"{1}\",'{2}','{3}'); return false;", "{{row.getProperty(\'" + lstColDef.Where(X => X.field == "pid").FirstOrDefault().field + "\')}}", "{{row.getProperty(\'" + lstColDef.Where(X => X.field == "entityformname").FirstOrDefault().field + "\')}}", sIsActiveEntity, bUseEntityRole);
                    //avipinsrivas end
                }
            }
            else if (String.Compare(SearchCategory, "diary", StringComparison.OrdinalIgnoreCase) == 0)
            {
                //Diary case neeed to implement with complete logic
                strFunctionClick = String.Format("selRecord({0},'{1}'); return false;", "{{row.getProperty(\'" + lstColDef.Where(X => X.field == "pid").FirstOrDefault().field + "\')}}", String.Empty);
            }
            else
            {
                if (lstResponse.FirstOrDefault() != null && lstResponse.FirstOrDefault().ContainsKey("pid"))
                {
                    if ((lstResponse.FirstOrDefault().ContainsKey("boolpid")) && (lstResponse.FirstOrDefault()["boolpid"].ToString() != String.Empty)) //RMA-19606 & RMA-19639
                        sIsActiveEntity = "{{row.getProperty(\'" + lstColDef.Where(X => X.field == "boolpid").FirstOrDefault().field + "\')}}";

                    strFunctionClick = String.Format("selRecord({0},\"{1}\",'{2}','{3}'); return false;", "{{row.getProperty(\'" + lstColDef.Where(X => X.field == "pid").FirstOrDefault().field + "\')}}", "{{row.getProperty(\'" + lstColDef.FirstOrDefault().field + "\')}}", sIsActiveEntity, bUseEntityRole);
                }
            }
            if (lstColDef.Where(X => X.Key == true).FirstOrDefault() != null)
                lstColDef.Where(X => X.Key == true).FirstOrDefault().cellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><a href=\"#\" onclick=" + strFunctionClick + ">{{row.getProperty(\'" + lstColDef.Where(X => X.Key == true).FirstOrDefault().field + "\')}}</a></div>,";
            else if (lstColDef.FirstOrDefault()!=null)
                lstColDef.FirstOrDefault().cellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><a href=\"#\" onclick=" + strFunctionClick + ">{{row.getProperty(\'" + lstColDef.FirstOrDefault().field + "\')}}</a></div>,";
            //sorting of Data
            GridDataHelper objGridData = new GridDataHelper();
            //int iSortIndex = 0;
            //string strSortColumn = "";
            //if (!String.IsNullOrEmpty(strSortField))
            //{
            //    iSortIndex = Convert.ToInt32(strSortField);
            //    if (lstColDef != null && lstColDef[iSortIndex - 1] != null)
            //        strSortColumn = lstColDef[iSortIndex - 1].field;
            //    if (!String.IsNullOrEmpty(strSortField) && strSortDirection == "descending")
            //    {
            //        var sortedList = (from dict in lstResponse orderby dict[strSortColumn] select dict);
            //        objGridData.Data = lstResponse.OrderByDescending(dict => dict[strSortColumn]).ToList();
            //    }
            //    else
            //    {
            //        var sortedList = (from dict in lstResponse orderby dict[strSortColumn] select dict);
            //        objGridData.Data = lstResponse.OrderBy(dict => strSortColumn).ToList();
            //    }
            //}
            //else
            objGridData.Data = lstResponse;
            objGridData.ColumnPosition = lstColDef;
            lstColDef = lstColDef.OrderBy(X => X.Position).ToList<ColumnDefHelper>();
            objGridData.Data = lstResponse;
            objGridData.ColumnDef = lstColDef;
            objGridData.TotalCount = Conversion.ConvertObjToInt(p_objResultXml.Attribute("totalrows").Value);
            objGridData.PageSize = Conversion.ConvertObjToInt(p_objResultXml.Attribute("pagesize").Value);
            objGridData.SearchCat = SearchCategory;
            objGridData.admtable = admtableval; //RMA-11641 - msampathkuma
            ServiceResponseHelper objResponse = new ServiceResponseHelper(true, null, objGridData);
            return objResponse;
        }

        private DataColumn GetDataColumn(string p_sColumnName, DataTable p_objDataTable, string sDatatype)
        //34278 End
        {
            DataColumn objColumn = new DataColumn();
            int iColumnCount = 1;
            string sColumnName = string.Empty;
            const string DATE_STRING = "date"; //34278
            const string CURRENCY_STRING = "currency";  //34278

            objColumn.Caption = p_sColumnName;

            if (p_objDataTable.Columns.Contains(p_sColumnName))
            {
                sColumnName = p_sColumnName + iColumnCount.ToString();
                while (p_objDataTable.Columns.Contains(sColumnName))
                {
                    iColumnCount++;
                    sColumnName = p_sColumnName + iColumnCount.ToString();
                }
                objColumn.ColumnName = sColumnName;
            }
            else
            {
                objColumn.ColumnName = p_sColumnName;
            }
            //RMA-345 Start
            if (sDatatype == DATE_STRING)
            {
                objColumn.DataType = typeof(System.DateTime);
            }
            if (sDatatype == CURRENCY_STRING)
            {
                objColumn.DataType = typeof(System.Double);
            }
            //RMA-345 End
            return objColumn;
        }


        private XElement GetSearchResults(XElement p_objCriteriaXML)
        {
            XmlDocument objXmlCriteriaDom = new XmlDocument();
            XElement objTempElement = null;
            objXmlCriteriaDom.LoadXml(p_objCriteriaXML.ToString());

            string sReturn = AppHelper.CallCWSService(objXmlCriteriaDom.InnerXml.ToString());

            objXmlCriteriaDom.LoadXml(sReturn);
            XmlNode oInstanceNode = objXmlCriteriaDom.SelectSingleNode("/ResultMessage/Document");

            XElement objResultXml = null;
            if (oInstanceNode.InnerXml != string.Empty)
            {
                objResultXml = XElement.Parse(oInstanceNode.InnerXml.ToString());
            }
            return objResultXml;
        }
        /// <summary>
        /// Dev Signature      : achouhan3  
        /// Date               : 09/09/2014
        /// Description        : Ajax Method to get save user  preference in XML
        /// <returns>JSON Object</returns>
        /// </summary>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SavePreferences()
        {
            ServiceResponseHelper objResponse = null;
            try
            {
                XDocument messageElement = null;
                XmlElement objUserPrefElement = null;
                XElement objXmlCriteria = null;
                XElement xmlIn;
                string strSearchCat = String.Empty;
                string strPageSize = String.Empty;
                string strSortDireection = String.Empty;
                string strSortFieldIndex = String.Empty;
                string strSortField = String.Empty;
                if (Request.Form["grdPageSize"] != null && !String.IsNullOrEmpty(Request.Form["grdPageSize"]))
                    strPageSize = Request.Form["grdPageSize"];
                if (Request.Form["searchCat"] != null && !String.IsNullOrEmpty(Request.Form["searchCat"]))
                    strSearchCat = Request.Form["searchCat"] + "search";
                if (Request.Form["sortDirection"] != null && !String.IsNullOrEmpty(Request.Form["sortDirection"]))
                    strSortDireection = Request.Form["sortDirection"] == "desc" ? "descending" : "ascending";
                if (Request.Form["sortField"] != null && !String.IsNullOrEmpty(Request.Form["sortField"]))
                    strSortField = Request.Form["sortField"];
                if (Request.Form["sortFieldIndex"] != null && !String.IsNullOrEmpty(Request.Form["sortFieldIndex"]))
                    strSortFieldIndex = Request.Form["sortFieldIndex"];
                JavaScriptSerializer reqJson = new JavaScriptSerializer();
                List<UserPreferences> gridColumn = new List<UserPreferences>();
                List<object> gridSortColumn = new List<object>();
                int iCount = 1;
                if (!String.IsNullOrEmpty(strSearchCat))
                {
                    XElement objTempElement = null;
                    XDocument xmlUserPref = null;

                    objTempElement = new XElement(strSearchCat);
                    objTempElement.SetAttributeValue("PageSize", strPageSize);
                    objTempElement.SetAttributeValue("SortType", strSortDireection);
                    objTempElement.SetAttributeValue("SortColumn", strSortFieldIndex);

                    xmlUserPref = new XDocument(objTempElement);
                    if (Request.Form["columnObject"] != null && !String.IsNullOrEmpty(Request.Form["columnObject"]))
                        gridColumn = reqJson.Deserialize<List<UserPreferences>>(Request.Form["columnObject"]);

                    foreach (var objColumn in gridColumn)
                    {
                        objTempElement = XElement.Parse("<Column/>");
                        objTempElement.SetAttributeValue("Name", objColumn.colDef.displayName); 
                        if (!String.IsNullOrEmpty(objColumn.colDef.cellTemplate))
                        {
                            objTempElement.SetAttributeValue("Key", "True");
                        }
                        objTempElement.SetAttributeValue("ColOrder", iCount);
                        objTempElement.SetAttributeValue("ColWidth", objColumn.width);                      
                        xmlUserPref.Element(strSearchCat).Add(objTempElement);
                        iCount++;
                    }

                    XElement tempElement = null;
                    string sReturn = String.Empty;

                    messageElement = SetUserPrefMessageTemplate(xmlUserPref.ToString());

                    sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return bool.

                    //Get User Pref XML to store and communicate later on
                    messageElement = GetUserPrefMessageTemplate();
                    sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return user preference xml (saved setting) for the search.

                    if (sReturn != "")
                    {
                        resultDoc = XElement.Parse(sReturn);

                    }
                    Dictionary<string, string> objUserPrefDict = new Dictionary<string, string>();
                    string strUserPref = Server.UrlEncode(Server.HtmlEncode(resultDoc.ToString()));
                    objUserPrefDict.Add("userPrefXML", strUserPref);
                    objResponse = new ServiceResponseHelper(true, null, objUserPrefDict);
                }
            }
            catch (Exception ex)
            {
                objResponse = new ServiceResponseHelper(false, ex.Message, null);
            }
            var json = JsonConvert.SerializeObject(objResponse);
            Response.Write(json);
            try
            {
                Response.Flush();
                Response.End();
            }
            catch (System.Threading.ThreadAbortException threadex) { }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XDocument SetUserPrefMessageTemplate(string strXml)
        {
            string str = "<Message><Authorization></Authorization><Call><Function>SearchAdaptor.SetUserPrefSearch</Function></Call><Document><UserPrefSearch>" + strXml + "</UserPrefSearch></Document></Message>";

            XElement oTemplate = XElement.Parse(@str);

            return new XDocument(oTemplate);
        }
        private XDocument GetUserPrefMessageTemplate()
        {
            string str = "<Message><Authorization></Authorization><Call><Function>SearchAdaptor.GetUserPrefSearchXML</Function></Call><Document><UserPrefSearch></UserPrefSearch></Document></Message>";

            XElement oTemplate = XElement.Parse(@str);

            return new XDocument(oTemplate);
        }
       
    }
}
