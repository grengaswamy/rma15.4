﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ISOClaimSubHistory.aspx.cs" Inherits="Riskmaster.UI.UI.ISOClaimSubHistory.ISOClaimSubHistory" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %> <%-- //Mits id: 34975 - Design change - Govind --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server"> 
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">   
  <title>ISO Claim Submission</title>
  <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
  <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
  <script language="JavaScript" src="../../scripts/form.js"></script>  
  <script type="text/javascript" src="../../scripts/rmx-common-ref.js"></script>
  <script language="JavaScript" type="text/javascript" src="../../Scripts/WaitDialog.js">{var i;}</script> <%-- //Mits id: 34975 - Design change - Govind --%>
</head>
<body onload="Onload()">
        <script type="text/javascript" language="javascript">

            function Onload() {
                if (document.getElementById('SubmitToISOFlagStatus').value == "True") {
                    document.getElementById('chkSubmitToISOFlag').checked = true;
                }
                else {
                    document.getElementById('chkSubmitToISOFlag').checked = false;
                }

                if (document.getElementById('subtype').value == "I") {
                    document.getElementById("rdInitialSubmit").checked = true;
                }
                else {
                    document.getElementById("rdReplacement").checked = true;
                }

            }

            function SaveValues() {            
                if (document.getElementById('chkSubmitToISOFlag').checked == true) {
                    document.getElementById('SubmitToISOFlagStatus').value = "True";
                }
                else {
                    document.getElementById('SubmitToISOFlagStatus').value = "False";
                }

                if (document.getElementById("rdInitialSubmit").checked == true) {
                    document.getElementById('subtype').value = "I";
                }
                else {
                    document.getElementById('subtype').value = "R";
                }

                //window.close();
                pleaseWait.Show(); //Mits id: 34975 - Design change - Govind
            }

			</script>
	        <form id="frmData" runat="server" >
	           
	            <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                         <td>
                            <asp:ImageButton id="Save" runat="server" src="../../Images/save.gif" alt="save" class="LightBold" height="28" width="28" border="0" title="Save" OnClientClick="Javascript:return SaveValues();" OnClick="SaveISOClaimSubHistory"
                            onmouseover="javascript: document.all['Save'].src = '../../Images/save2.gif';" 
                            onmouseout="javascript: document.all['Save'].src = '../../Images/save.gif';javascript: document.all['Save'].style.zoom='100%';" 
                             />                                                          
                        </td>
                    </tr>
                </table></br>
               <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" value="" appearance="full" id="chkSubmitToISOFlag" onchange="setDataChanged(true);" />Submit To ISO
                            <asp:TextBox runat="server" id ="SubmitToISOFlagStatus" rmxref="/Instance/Document/ISOClaimSubHistory/SubmitToISOFlag/@value" style="display:none"/>
                            <asp:HiddenField runat="server" id="claimid"  value="" />
                            <asp:TextBox runat="server" id="subtype"  value="" rmxref="/Instance/Document/ISOClaimSubHistory/SubType/@value" style="display:none"/>
                        </td>
                    </tr>
               </table></br>
               <table width="50%" cellspacing="0" cellpadding="0">
                    <tr>
                         <td><asp:RadioButton id="rdInitialSubmit" runat="server" GroupName="rdSubmit" name="rdSubmit" value="InitialSubmit" /><label for="rdInitialSubmit">Initial Submit</label></td>
                         <td><asp:RadioButton id="rdReplacement" runat="server" GroupName="rdSubmit" name="rdSubmit" value="Replacement" /><label for="rdReplacement" >Replacement</label></td>
                    </tr>
               </table></br>
               <table width="100%" cellspacing="0" cellpadding="0" class="singleborder">
                    <tr class="msgheader">
                          <td colspan="2">ISO Submission History</td>
                    </tr>
               </table>
               <table width="100%" cellspacing="0" cellpadding="0" class="singleborder">
                    <tr class="ctrlgroup">
                         <td>Date</td>
                         <td>Type</td>
                         <td>Description</td>
                    </tr>
                    <%int j = 0; foreach (XElement item in result)
                      {
                          string colclass = "";
                          if ((j % 2) == 1) colclass = "datatd1";
                          else colclass = "datatd";
                          j++;
                    %>
                    <tr>
                        <td class="<%=colclass%>">
                            <%=item.Element("Date").Value%>
                        </td>
                        <td class="<%=colclass%>">
                            <%=item.Element("Type").Value%>
                        </td>
                        <td class="<%=colclass%>">
                            <%=item.Element("Description").Value%>
                        </td>
                    </tr>
                    <%}%>
                 
               </table>
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />	<%-- //Mits id: 34975 - Design change - Govind --%>	
        </form>
 </body>
</html>
