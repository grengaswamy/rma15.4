﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MedInfo.aspx.cs" Inherits="Riskmaster.UI.UI.PSOForm.MedInfo" ValidateRequest="false"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register tagprefix="uc2" Tagname="CodeLookUp"  src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register tagprefix="uc"  tagname="MultiCode" src="~/UI/Shared/Controls/MultiCodeLookup.ascx"   %>
<%@ Register tagprefix="uc3" tagname="PleaseWaitDialog" src="~/UI/Shared/Controls/PleaseWaitDialog.ascx"   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MEDICATION OR OTHER SUBSTANCE</title>    
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
</head>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; } </script>   
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script> 
    <script type="text/javascript" language="JavaScript" src="../../Scripts/pso.js"></script> 
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>  
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script> 
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script> 
<body onload="CheckHiddenContrls();HideControlsGrid();">
    <form id="frmData" runat="server" name="frmData">
<table>
   <tr>
      <td colspan="2">
         <uc1:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
  </tr>
</table>
<div class="msgheader" id="pnlMedicationInfo_Header" runat="server">Medication Or Other Substance</div>
<div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" EnableTheming="True" ShowMessageBox="false" />
</div>
<div id="pnlMedicationInfo" runat="server">
  <table border="0" cellspacing="0" cellpadding="0" id="pnlMedicationInfo_table" runat="server" width="100%">        
    <tr id="txtDrugName_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtDrugName_Title" runat="server" Text="Generic name or investigational drug name" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtDrugName" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/MedicationInfo/DrugName" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <tr id="txtBrandName_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtBrandName_Title" runat="server" Text="Brand name (if known)" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtBrandName" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/MedicationInfo/BrandName" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <tr id="txtManufacturerName_ctlRow" runat="server" >
        <td>
           <asp:Label ID="txtManufacturerName_Title" runat="server" Text="Manufacturer (if known)" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtManufacturerName" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/MedicationInfo/ManufacturerName" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>    
    <tr id="txtProductStrength_ctlRow" runat="server" >
        <td>
           <asp:Label ID="txtProductStrength_Title" runat="server" Text="Strength or concentration of product" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtProductStrength" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/MedicationInfo/ProductStrength" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <tr id="txtDosage_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtDosage_Title" runat="server" Text="Dosage form of product" class=""></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtDosage" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/MedicationInfo/Dosage" MaxLength="100"></asp:TextBox>
        </td>              
    </tr>
    <tr id="cdWasMedPresc_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdWasMedPresc_Title" runat="server" Text="Was this medication/ substance prescribed for this patient?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdWasMedPresc" CodeTable="PSO_YES_NO" ControlName="cdWasMedPresc" RMXRef="/Instance/Document/MedicationInfo/MedicationPrescribedCode" RMXType="code"  />             
        </td>
    </tr>
    <tr id="cdWasMedGiven_ctlRow" runat="server">
        <td>
            <asp:Label ID="cdWasMedGiven_Title" runat="server" Text="Was this medication/ substance given to this patient?" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="cdWasMedGiven" CodeTable="PSO_YES_NO" ControlName="cdWasMedGiven" RMXRef="/Instance/Document/MedicationInfo/MedicationGivenCode" RMXType="code"  />             
        </td>
    </tr>     
  </table>
</div>
<div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnSave">
            <asp:button class="button" runat="server" id="btnMedicationOk" RMXRef="" Text="OK" width="75px" onClientClick="return fnMedInfoOk();" onclick="btnMedInfoOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:button class="button" runat="server" id="btnMedInfoCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return fnMedInfoCancel();" />
        </div>
 </div>
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/Med/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="txtMedInfoSessionId" RMXRef="/Instance/Document/MedicationInfo/txtMedInfoSessionId"></asp:TextBox>
    <asp:TextBox style="display: none" runat="server" id="MedRowId" RMXRef="/Instance/Document/MedicationInfo/MedRowId" RMXType="hidden"/>
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="txtmode" />
    <asp:TextBox style="display: none" runat="server" ID="formname" value="MedicationInfo" />    
    <asp:TextBox Style="display: none" runat="server" ID="SessionId" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="txtgridname"  />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />    
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" /> 
    <asp:TextBox Style="display: none" runat="server" ID="txtFunctionToCall" /> 
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="hiddencontrols"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="requiredcontrols"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="pnlhiddencontrols"></asp:TextBox>
 </form>
 <uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
</body>
</html>

