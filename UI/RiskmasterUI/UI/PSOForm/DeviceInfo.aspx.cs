﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common.Extensions;

namespace Riskmaster.UI.UI.PSOForm
{
    public partial class DeviceInfo : NonFDMBasePageCWS
    {
        XmlDocument XMLDocument = new XmlDocument();
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        bool bReturnStatus = false;
        string m_FUNCTION_SAVE_DEVICE = "PSOFormAdaptor.SetDeviceInfo";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes 
                XMLDocument.Load(Server.MapPath("~/App_Data/PSOForm/DeviceInfo.xml"));
                AppHelper.CreateControl(this.Page, XMLDocument);
                if (!IsPostBack)
                {

                    string sSessionId = string.Empty;
                    if (AppHelper.GetQueryStringValue("sessionid") != "")
                    {
                        sSessionId = AppHelper.GetQueryStringValue("sessionid");
                        txtDeviceInfoSessionId.Text = sSessionId;
                    }
                    if (AppHelper.GetQueryStringValue("mode") != "")
                    {
                        txtmode.Text = AppHelper.GetQueryStringValue("mode");
                    }
                    if (AppHelper.GetQueryStringValue("selectedid") != "")
                    {
                        DeviceRowId.Text = AppHelper.GetQueryStringValue("selectedid");
                    }
                }
                string sFunctionToCall = txtFunctionToCall.Text;
                if (string.Compare(sFunctionToCall, m_FUNCTION_SAVE_DEVICE, true) != 0)
                {
                    XmlTemplate = null;
                    bReturnStatus = CallCWS("PSOFormAdaptor.GetDeviceInfo", XmlTemplate, out sCWSresponse, true, true);
                    if (bReturnStatus)
                    {
                        ModifyControl_Hide(sCWSresponse);
                    }
                    else
                    {
                        btnDeviceOk.Enabled = false;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }

        }


        private void ModifyControl_Hide(string sCWSresponse)
        {
            XmlDocument xmlResult = new XmlDocument();
            string sHiddenFieldList = string.Empty;
            string sRequiredFieldList = string.Empty;
            string sPnlHiddenCtrlsList = string.Empty;
            bool bFailure = false;
            string sValue = string.Empty;
            xmlResult.LoadXml(sCWSresponse);

            if (xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/MsgStatusCd") != null)
            {
                sValue = xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/MsgStatusCd").InnerText;
                if (string.Compare(sValue, "Error", true) == 0)
                {
                    bFailure = true;
                }
            }
            if (bFailure)
            {
                btnDeviceOk.Enabled = false;
                return;
            }
            btnDeviceOk.Enabled = true;

            if (xmlResult.SelectSingleNode("//DeviceInfo/HiddenFieldList") != null)
            {
                sHiddenFieldList = xmlResult.SelectSingleNode("//DeviceInfo/HiddenFieldList").InnerText;
            }
            hiddencontrols.Text = sHiddenFieldList;
            if (xmlResult.SelectSingleNode("//DeviceInfo/RequiredFieldList") != null)
            {
                sRequiredFieldList = xmlResult.SelectSingleNode("//DeviceInfo/RequiredFieldList").InnerText;
            }
            if (xmlResult.SelectSingleNode("//DeviceInfo/pnlhiddencontrols") != null)
            {
                sPnlHiddenCtrlsList = xmlResult.SelectSingleNode("//DeviceInfo/pnlhiddencontrols").InnerText;
            }
            requiredcontrols.Text = sRequiredFieldList;
            pnlhiddencontrols.Text = sPnlHiddenCtrlsList;
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            string sFunctionToCall = txtFunctionToCall.Text;
            XMLDocument.Load(Server.MapPath("~/App_Data/PSOForm/DeviceInfo.xml"));
            if (string.Compare(sFunctionToCall, m_FUNCTION_SAVE_DEVICE, true) != 0)
            {
                XElement xmlEvent = Xelement.XPathSelectElement("./Document/DeviceInfo");
                XElement xmlForm = new XElement("FormValues");
                xmlForm.Value = XMLDocument.SelectSingleNode("//form").OuterXml;
                if (xmlEvent != null)
                {
                    xmlEvent.Add(xmlForm);
                }
            }
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("PSOFormAdaptor.GetDeviceInfo");
            sXml = sXml.Append("</Function></Call><Document>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }

        protected void btnDeviceOk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = string.Empty;
            TextBox txtSessionId;
            bReturnStatus = CallCWS("PSOFormAdaptor.SetDeviceInfo", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                // Set the Session Id
                XmlDoc.LoadXml(sreturnValue);
                txtSessionId = (TextBox)this.FindControl("SessionId");
                txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetDevices/SessionId").InnerText;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "DeviceOkClick", "<script>fnRefreshParentDeviceOk();</script>");
            }
        }

    }
}