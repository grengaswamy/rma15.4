﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.UI.PSOForm
{
    public partial class MedInfo : NonFDMBasePageCWS
    {
        XmlDocument XMLDocument = new XmlDocument();
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        bool bReturnStatus = false;
        string m_FUNCTION_SAVE_MEDICATION = "PSOFormAdaptor.SetMedInfo";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sGridName = string.Empty;
                XMLDocument.Load(Server.MapPath("~/App_Data/PSOForm/MedInfo.xml"));
                AppHelper.CreateControl(this.Page, XMLDocument);
                if (!IsPostBack)
                {
                    
                    if (AppHelper.GetQueryStringValue("gridname") != "")
                    {
                        sGridName = AppHelper.GetQueryStringValue("gridname");
                        txtgridname.Text = sGridName;
                    }
                    string sSessionId = string.Empty;
                    if (AppHelper.GetQueryStringValue("sessionid") != "")
                    {
                        sSessionId = AppHelper.GetQueryStringValue("sessionid");
                        txtMedInfoSessionId.Text = sSessionId;
                    }
                    if (AppHelper.GetQueryStringValue("mode") != "")
                    {
                        txtmode.Text = AppHelper.GetQueryStringValue("mode");
                    }
                    if (AppHelper.GetQueryStringValue("selectedid") != "")
                    {
                        MedRowId.Text = AppHelper.GetQueryStringValue("selectedid");
                    } 
                }

                string sFunctionToCall = txtFunctionToCall.Text;
                if (string.Compare(sFunctionToCall, m_FUNCTION_SAVE_MEDICATION, true) != 0)
                {
                    XmlTemplate = null;
                    bReturnStatus = CallCWS("PSOFormAdaptor.GetMedInfo", XmlTemplate, out sCWSresponse, true, true);
                    if (bReturnStatus)
                    {
                        ModifyControl_Hide(sCWSresponse);
                    }
                    else
                    {
                        btnMedicationOk.Enabled = false;
                    }
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }

        }
        private void ModifyControl_Hide(string sCWSresponse)
        {
            XmlDocument xmlResult = new XmlDocument();
            string sPnlHiddenCtrlsList = string.Empty;
            string sHiddenFieldList = string.Empty;
            string sRequiredFieldList = string.Empty;
            bool bFailure = false;
            string sValue = string.Empty;
            xmlResult.LoadXml(sCWSresponse);


            if (xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/MsgStatusCd") != null)
            {
                sValue = xmlResult.SelectSingleNode("//ResultMessage/MsgStatus/MsgStatusCd").InnerText;
                if (string.Compare(sValue, "Error", true) == 0)
                {
                    bFailure = true;
                }
            }
            if (bFailure)
            {
                btnMedicationOk.Enabled = false;
                return;
            }
            btnMedicationOk.Enabled = true;
            if (xmlResult.SelectSingleNode("//MedicationInfo/HiddenFieldList") != null)
            {
                sHiddenFieldList = xmlResult.SelectSingleNode("//MedicationInfo/HiddenFieldList").InnerText;
            }
            hiddencontrols.Text = sHiddenFieldList;
            if (xmlResult.SelectSingleNode("//MedicationInfo/RequiredFieldList") != null)
            {
                sRequiredFieldList = xmlResult.SelectSingleNode("//MedicationInfo/RequiredFieldList").InnerText;
            }
            if (xmlResult.SelectSingleNode("//MedicationInfo/pnlhiddencontrols") != null)
            {
                sPnlHiddenCtrlsList = xmlResult.SelectSingleNode("//MedicationInfo/pnlhiddencontrols").InnerText;
            }
            requiredcontrols.Text = sRequiredFieldList;
            pnlhiddencontrols.Text = sPnlHiddenCtrlsList;  
        }
        public override void ModifyXml(ref XElement Xelement)
        {
            string sFunctionToCall = txtFunctionToCall.Text;
            XMLDocument.Load(Server.MapPath("~/App_Data/PSOForm/MedInfo.xml"));
            if (string.Compare(sFunctionToCall, m_FUNCTION_SAVE_MEDICATION, true) != 0)
            {
                XElement xmlEvent = Xelement.XPathSelectElement("./Document/MedicationInfo");
                XElement xmlForm = new XElement("FormValues");
                xmlForm.Value = XMLDocument.SelectSingleNode("//form").OuterXml;
                if (xmlEvent != null)
                {
                    xmlEvent.Add(xmlForm);
                }
            }
        }
        protected void btnMedInfoOk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = string.Empty;
            TextBox txtSessionId;
            bReturnStatus = CallCWS("PSOFormAdaptor.SetMedInfo", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                // Set the Session Id
                XmlDoc.LoadXml(sreturnValue);
                txtSessionId = (TextBox)this.FindControl("SessionId");
                txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetMedInfo/SessionId").InnerText;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MedInfoOkClick", "<script>fnRefreshParentMedInfoOk();</script>");
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
  
    }
}