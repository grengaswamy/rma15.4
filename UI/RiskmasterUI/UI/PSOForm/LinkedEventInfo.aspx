﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LinkedEventInfo.aspx.cs" Inherits="Riskmaster.UI.UI.PSOForm.LinkedEventInfo" ValidateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register tagprefix="uc2" Tagname="CodeLookUp"  src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register tagprefix="uc3" tagname="PleaseWaitDialog" src="~/UI/Shared/Controls/PleaseWaitDialog.ascx"   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SUMMARY OF INITIAL REPORT</title>    
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
</head>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; } </script>   
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script> 
    <script type="text/javascript" language="JavaScript" src="../../Scripts/pso.js"></script> 
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>  
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>  
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>  
<body onload="CheckHiddenContrls();">
<form id="frmData" runat="server" name="frmData">
<table>
   <tr>
      <td colspan="2">
         <uc1:ErrorControl ID="ErrorControl1" runat="server" />
      </td>
  </tr>
</table>
<div class="msgheader" id="pnlLinkEvent_Header" runat="server">Summary Of Initial Report(SIR)</div>
<div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" EnableTheming="True" ShowMessageBox="false" />
</div>  
<div id="pnlLinkEventInfo" runat="server">
  <table border="0" cellspacing="0" cellpadding="0" id="pnlLinkEventInfo_table" runat="server" width="100%">
    <tr id="txtManufacturerName_ctlRow" runat="server">
        <td>
           <asp:Label ID="txtLinkEventID_Title" runat="server" Text="Linked event ID" class="required"></asp:Label>
        </td>
        <td> 
           <asp:TextBox ID="txtLinkEventID" runat="server" RMXType="text" size="" RMXRef="/Instance/Document/LinkedEventInfo/LnkEventNumber" MaxLength="16"></asp:TextBox>
        </td>              
    </tr> 
    <tr>
        <td>
            <asp:Label ID="psocdLinkReason_Title" runat="server" Text="Reason for linking" class=""></asp:Label>
        </td>
        <td> 
            <uc2:CodeLookUp runat="server" ID="psocdLinkReason" CodeTable="PSO_LINK_REASON" ControlName="psocdLinkReason" RMXRef="/Instance/Document/LinkedEventInfo/EventLnkReasonCode" RMXType="code"  />             
        </td>
    </tr> 
    <tr id="txtOtherLinkReason_ctlRow" runat="server">
        <td>
            <asp:Label ID="txtOtherLinkReason_Title" runat="server" Text="If Other, Please Specify"></asp:Label>
        </td>
        <td> 
            <span class="formw">
            <asp:TextBox runat="Server" id="txtOtherLinkReason" RMXRef="/Instance/Document/LinkedEventInfo/EventLnkReasonText" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
            <input type="button" class="button" value="..." name="btnOtherLinkReason"  id="btnOtherLinkReason" onclick="EditMemo('txtOtherLinkReason','')" />
            </span>
        </td>              
    </tr>        
  </table>
</div>
 <div id="div1" class="formButtonGroup">
        <div class="formButton" id="div_btnSave">
            <asp:button class="button" runat="server" id="btnLnkEvtOk" RMXRef="" Text="OK" width="75px" onClientClick="confirmSubmit();return fnLnkEvtOk();" onclick="btnLnkEvtOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:button class="button" runat="server" id="btnLnkEvtCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return fnLnkEvtCancel();" />
        </div>
 </div>
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/LinkedEvents/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="txtLnkEvtInfoSessionId" RMXRef="/Instance/Document/LinkedEventInfo/txtLnkEvtInfoSessionId"></asp:TextBox>
    <asp:TextBox style="display:none" runat="server" id="LnkEvtRowId" RMXRef="/Instance/Document/LinkedEventInfo/LnkEventRowId" RMXType="hidden"/>
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="SessionId" />
    <asp:TextBox Style="display: none" runat="server" ID="txtmode" />
    <asp:TextBox Style="display: none" runat="server" ID="txtFunctionToCall" /> 
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />    
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />  
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="hiddencontrols"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="requiredcontrols"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="formname" Text="LinkedEventInfo"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="pnlhiddencontrols"></asp:TextBox>
</form>
<uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
</body>
</html>

