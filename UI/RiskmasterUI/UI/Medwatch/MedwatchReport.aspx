﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MedwatchReport.aspx.cs" Inherits="Riskmaster.UI.UI.pdf_forms.Medwatch" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MedWatch</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
    <script language="javascript" type="text/javascript">
        function MedWatchReport3500() {
            // var sQueryString = 'EventId=' + document.forms[0].all("EventId").value;
            document.forms[0].EventId.value = window.opener.document.forms[0].eventid.value;
            //var sQueryString =document.forms[0].all("EventId").value;
            var sQueryString = document.getElementById("EventId").value; //Jira 9143
            var m_windowCallToXPL = window.open('/RiskmasterUI/UI/Medwatch/MedPdf3500.aspx?EventId=' + sQueryString, 2, 'width=500,height=300,top=' + (screen.availHeight - 300) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }

        function MedWatchReport3500A() {
            /// var sQueryString = 'EventId=' + document.forms[0].all("EventId").value;
            document.forms[0].EventId.value = window.opener.document.forms[0].eventid.value;
            //var sQueryString =document.forms[0].all("EventId").value;
            var sQueryString = document.getElementById("EventId").value; //Jira 9143
            var m_windowCallToXPL = window.open('/RiskmasterUI/UI/Medwatch/MedPdf3500a.aspx?EventId=' + sQueryString, 2, 'width=500,height=300,top=' + (screen.availHeight - 300) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }

    </script>
</head>
<body>
    <form id="frmData" runat="server">
     <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
   
    	<table width="100%" cellspacing="0" cellpadding="2" border="0">
        <asp:TextBox ID="FunctionToCall" style="display:none" rmxref="/Instance/Document/FunctionToCall" runat="server"></asp:TextBox>
        <asp:TextBox ID="EventId" style="display:none" rmxref="/Instance/Document/EventId" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnFrmName" style="display:none" runat="server"></asp:TextBox>
						<tr>
							<td>
                               <asp:LinkButton ID="lnkBtnMedwatch3500"  class="LightBold" OnClientClick="return MedWatchReport3500()" Text="FDA-3500: Medwatch-Voluntary" Font-Names="Arial" Font-Size="Small"  runat="server"></asp:LinkButton>	
							</td>
						</tr>
						<tr>
							<td>
							<asp:LinkButton ID="lnkMedWatch3500A" class="LightBold" OnClientClick="return MedWatchReport3500A();" Text="FDA-3500A: Medwatch-Mandatory" Font-Names="Arial" Font-Size="Small" runat="server"></asp:LinkButton>	
							</td>
						</tr>
					</table>
    </div>
    </form>
</body>
</html>
