﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.OSHA301
{
    public partial class OSHA301 : System.Web.UI.Page
    {
        private XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";
            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                    ErrorControl1.errorDom = sReturn;
                    oFDMPageDom.LoadXml(sReturn);
                    XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    rootElement = XElement.Parse(oFDMPageDom.OuterXml);

                    if (!ErrorControl1.errorFlag)
                    {
                        XElement oEle = rootElement.XPathSelectElement("//Osha/File");
                        if (oEle != null)
                        {
                            string sFileContent = oEle.Value;
                            string sFileName = oEle.Attribute("Name").Value;
                            
                            byte[] byteOrg = Convert.FromBase64String(sFileContent);

                            Response.Clear();
                            Response.Charset = "";
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Disposition", "inline;");
                            Response.BinaryWrite(byteOrg);
                            Response.End();
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

            
           
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                 <Authorization>2a5f2503-9484-49b0-8bfc-e45f4eb38821</Authorization> 
                 <Call>
                  <Function>OSHAAdaptor.RunReport</Function> 
                 </Call>
                 <Document>
                     <report type='4'>
                     <form name='oshacriteria' sid='12500' title='OSHA Report Criteria' topbuttons='0'>
                     <group name='Preparer Information' title='Preparer Information'>
                      <control name='preparertitle' title='Preparer Title' type='text' value='' /> 
                      <control name='preparername' title='Preparer Name' type='text' value='' /> 
                      <control name='preparerphone' title='Preparer Phone' type='text' value='' /> 
                      </group>
                     <group name='osha' title='OSHA Information [By OSHA Establishment]'>
                      <control name='byoshaestablishmentflag' title='By OSHA Establishment' type='radio' value='true' /> 
                      </group>
                     <group name='osha' title='OSHA Information [By Organizational Hierarchy]'>
                      <control name='byoshaestablishmentflag' title='By OrgHierarchy' type='radio' value='false' /> 
                     <control codeid='1012' name='usereportlevel' title='Use Reports at Level' type='combobox' value='1005'>
                      <option value='1005'>Client</option> 
                      <option value='1006'>Company</option> 
                      <option value='1007'>Operation</option> 
                      <option value='1008'>Region</option> 
                      <option value='1009'>Division</option> 
                      <option value='1010'>Location</option> 
                      <option value='1011'>Facility</option> 
                      <option value='1012'>Department</option> 
                      </control>
                     <control codeid='1005' name='reportlevel' title='Report At Level' type='combobox' value='1005'>
                      <option value='1005'>Client</option> 
                      <option value='1006'>Company</option> 
                      <option value='1007'>Operation</option> 
                      <option value='1008'>Region</option> 
                      <option value='1009'>Division</option> 
                      <option value='1010'>Location</option> 
                      <option value='1011'>Facility</option> 
                      <option value='1012'>Department</option> 
                      </control>
                      </group>
                     <group name='Date Information' title='OSHA Reporting Date Information [Explicit]'>
                      <control codeid='' name='yearofreport' title='Year of Report' type='combobox' /> 
                      <control checked='' name='datemethod' title='Use Year' type='radio' value='useyear'>useyear</control> 
                      <control name='datemethod' title='Use Date Range' type='radio' value='userange'>useyear</control> 
                      <control name='begindate' title='Beginning Date' type='date' value='' /> 
                      <control name='enddate' title='Ending Date' type='date' value='' /> 
                      </group>
                     <group name='Date Information' title='OSHA Reporting Date Information [Relative]'>
                      <control name='datemethod' title='Use Relative' type='radio' value='userelative'>useyear</control> 
                      <control name='timescalar' title='Previous' type='numeric' value='1'>0</control> 
                     <control codeid='d' name='timeunit' title='Time Unit:' type='combobox' value='d'>
                      <option value='d'>Days</option> 
                      <option value='m'>Months</option> 
                      <option value='yyyy'>Years</option> 
                      </control>
                      </group>
                     <group name='Data Options' title='OSHA Reporting Data Options'>
                      <control name='printsofterrlog' title='Print Problem Log' type='checkbox'>1</control> 
                      <control checked='' name='eventbasedflag' title='Event Based Report' type='checkbox'>1</control> 
                      <control name='printoshadescflag' title='Use OSHA Description' type='checkbox'>1</control> 
                      <control name='primarylocationflag' title='Use Primary Location' type='checkbox'>1</control> 
                      <control checked='true' name='allentitiesflag' title='Use All Entities' type='radio' value='true'>true</control> 
                      <control name='allentitiesflag' title='Use Selected Entities' type='radio' value='false'>true</control> 
                      <control name='selectedentities' title='Selected Entities' type='entitylist' /> 
                      <control name='filterclaimid' type='hidden' value='' /> 
                      <control name='filtereventid' type='hidden' value='' /> 
                      </group>
                      <internal name='reporttype' type='hidden' value='4' /> 
                      <internal name='sys_formidname' type='hidden' value='eventid' /> 
                      <internal name='sys_formpidname' type='hidden' value='eventid,claimid' /> 
                      <internal name='sys_ex' type='hidden' value='eventnumber,claimnumber' /> 
                      <internal name='sys_formpform' type='hidden' value='event.asp' /> 
                      <internal name='sys_notreqnew' type='hidden' value='' /> 
                      </form>
                      </report>
                 </Document>
             </Message>
            ");

            return oTemplate;
        }
        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/report/form/group/control[@name='filterclaimid']");
            if (oElement != null)
            {
                oElement.Attribute("value").Value = AppHelper.GetQueryStringValue("ClaimId");
            }

            oElement = oMessageElement.XPathSelectElement("./Document/report/form/group/control[@name='filtereventid']");
            if (oElement != null)
            {
                oElement.Attribute("value").Value = AppHelper.GetQueryStringValue("EventId");
            }

            //MITS 19659 : Raman Bhatia: 02/01/2010
            //if "Event Osha Recordable" checkbox is checked then all employees involved data should be visible in OSHA 301 report
            //if "Event Osha Recordable" checkbox is unchecked the all employees involved whose "OSHA recordable" flag is set should be visible in OSHA 301 report.
            oElement = oMessageElement.XPathSelectElement("./Document/report/form/group/control[@name='eventbasedflag']");
            if (oElement != null)
            {
                oElement.Attribute("checked").Value = AppHelper.GetQueryStringValue("eventbasedflag");
            }
           
           
        }
    }
}
