﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.BenefitCalcTable
{
    public partial class TableDataDetail : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Read the Values from Querystring
                string sClassId = AppHelper.GetQueryStringValue("classid");
                string sTdRowId = AppHelper.GetQueryStringValue("TdRowId");

                ClassId.Text = sClassId;
                RowId.Text = sTdRowId;
                //**ksahu5-ML-MITS34114 Start ** 
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("TableDataDetail.aspx"), "TableDetailDataValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "TableDetailDataValidations", sValidationResources, true);
                //**ksahu5-ML-MITS34114 End ** 
                // Get Values for Edit from Database for the first time only
                if (!IsPostBack)
                {
                    if (sClassId != "" && sTdRowId != "")
                    {
                        bool bReturnStatus = false;
                        XElement XmlTemplate = null;
                        string sReturnValue = "";

                        XmlTemplate = GetEditMessageTemplate(sTdRowId);

                        bReturnStatus = CallCWS("DisClassTdManagerAdaptor.GetBenefitData", XmlTemplate, out sReturnValue, false, true);

                        if (!bReturnStatus)
                        {
                            ecErrorControl.errorDom = sReturnValue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetSaveMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message> ");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization> ");
            sXml = sXml.Append("<Call><Function>DisClassTdManagerAdaptor.SaveBenefitTableData</Function></Call><Document><BenefitCalcTable> ");
            sXml = sXml.Append("<BenefitCalTableData> ");
            sXml = sXml.Append("<rowid> ");
            sXml = sXml.Append(RowId.Text);
            sXml = sXml.Append("</rowid> ");
            sXml = sXml.Append("<CLASS_ID> ");
            sXml = sXml.Append(ClassId.Text);
            sXml = sXml.Append("</CLASS_ID> ");
            sXml = sXml.Append("<WAGES_FROM>");

            // Get the Code Id for Calendar Work
            sXml = sXml.Append(wages_from.Text);
            sXml = sXml.Append(" </WAGES_FROM> ");
            sXml = sXml.Append("<WAGES_TO>");
            sXml = sXml.Append(wages_to.Text);
            sXml = sXml.Append(" </WAGES_TO> ");
            sXml = sXml.Append(" <WEEKLY_BENEFIT> ");
            sXml = sXml.Append(week_benefits.Text);
            sXml = sXml.Append(" </WEEKLY_BENEFIT> ");
            sXml = sXml.Append("<SUPPLEMENT>");
            sXml = sXml.Append(supplement.Text);
            sXml = sXml.Append("</SUPPLEMENT> ");
            sXml = sXml.Append("</BenefitCalTableData> ");
            sXml = sXml.Append("</BenefitCalcTable></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetEditMessageTemplate(string sTdRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message> ");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization> ");
            sXml = sXml.Append("<Call><Function>DisClassTdManagerAdaptor.GetBenefitData</Function></Call><Document><BenefitCalTable>");
            sXml = sXml.Append("<InputXml> ");
            sXml = sXml.Append("<rowid> ");
            sXml = sXml.Append(sTdRowId);
            sXml = sXml.Append("</rowid> ");
            sXml = sXml.Append("</InputXml> ");
            sXml = sXml.Append(" </BenefitCalTable> ");
            sXml = sXml.Append("</Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sReturnValue = "";

                XmlTemplate = GetSaveMessageTemplate();

                bReturnStatus = CallCWS("DisClassTdManagerAdaptor.SaveBenefitTableData", XmlTemplate, out sReturnValue, false, false);

                if (bReturnStatus)
                {
                    string script = "<script>RefreshParent();</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
                }
                else
                {
                    ecErrorControl.errorDom = sReturnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnSubmit_Click(sender, e);
        }
    }
}
