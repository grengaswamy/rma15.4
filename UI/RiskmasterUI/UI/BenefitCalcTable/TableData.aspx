﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TableData.aspx.cs" Inherits="Riskmaster.UI.BenefitCalcTable.TableData" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Table Driven Benefits</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function Refresh()
    {
        document.forms[0].submit();
    }
    function SelectonEdit()
    {
        var radioSelect = document.getElementById('hdnSelectedId').value;
        if(radioSelect != "")
        {
            var inputs = document.all.tags("input");
	        for (i = 0; i < inputs.length; i++)
	        {
		        if ((inputs[i].type=="radio"))
		        {
			        selectedValue=inputs[i].value;
				    
			        if(selectedValue==radioSelect)
			        {
			            inputs[i].checked = true;
			            document.getElementById('hdnSelectedId').value = "";
			        }
		        }
	        }
	    }
    }
    </script>
</head>
<body onload="SelectonEdit();">
    <form id="frmData" runat="server">
        <table width="100%" cellspacing="0" cellpadding="0">
		    <tr>
		        <td>
			        <uc1:ErrorControl ID="ecErrorControl" runat="server" />
			    </td>
			</tr>
			    
			<tr>
			    <td class="ctrlgroup">
                <!--**ksahu5-ML-MITS34092 Start **-->   
				   <%-- Table Driven Benefits--%>
                     <asp:Label ID="lblTableDrivenBenefits" runat="server" Text="<%$ Resources:lblTableDrivenBenefits %>"></asp:Label>
                <!--**ksahu5-ML-MITS34092 End **-->  
			    </td>
		    </tr>
	    </table>
	    <table width="100%">
		    <tr>
			    <td width="80%">
				    <div style="width:100%;height:150px;overflow:auto">
                       <!--**ksahu5-ML-MITS34092 Start **-->
                        <asp:GridView ID="gvBenefitCalGrid" runat="server"  AutoGenerateColumns="False" 



                            Font-Names="Tahoma" Font-Size="Smaller" 
                            GridLines="None" CssClass="singleborder" 
                            Width="403px" ondatabound="gvBenefitCalGrid_DataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <input  id="rdoBenefitCalGrid" name="BenefitCalGrid" type="radio" value='<%# Eval("TdRowId")%>'/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:BoundField DataField="TdRowId" HeaderText="RowId"/>
                                <asp:BoundField DataField="Earnings" HeaderText="<%$ Resources:gvHdrEarnings %>"/>
                                <asp:BoundField DataField="Benefits" HeaderText="<%$ Resources:gvHdrBenefits %>"/>
                                <asp:BoundField DataField="Supplement" HeaderText="<%$ Resources:gvHdrSupplement %>"/>
                            
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>
                         <%--<asp:GridView ID="gvBenefitCalGrid" runat="server"  AutoGenerateColumns="False" 
                            Font-Names="Tahoma" Font-Size="Smaller" 
                            GridLines="None" CssClass="singleborder" 
                            Width="403px" ondatabound="gvBenefitCalGrid_DataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <input  id="rdoBenefitCalGrid" name="BenefitCalGrid" type="radio" value='<%# Eval("TdRowId")%>'/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TdRowId" HeaderText="RowId"/>
                                <asp:BoundField DataField="Earnings" HeaderText="Earnings"/>
                                <asp:BoundField DataField="Benefits" HeaderText="Benefits"/>
                                <asp:BoundField DataField="Supplement" HeaderText="Supplement"/>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>--%>
				    </div>
			    </td>
			    <td width="20%" valign="top">
                 <!--**ksahu5-ML-MITS34092 Start **-->  
			     <%--   <asp:ImageButton ID="gvBenefitCalGrid_New" runat="server" Text="New" 
                        ImageUrl="~/Images/new.gif" ImageAlign="Middle"/>--%>
                        <asp:ImageButton ID="gvBenefitCalGrid_New" runat="server" title="<%$ Resources:ttNew %>" 
                        ImageUrl="~/Images/new.gif" ImageAlign="Middle"/>
                    <br />
        
            <%--        <asp:ImageButton ID="gvBenefitCalGrid_Edit" runat="server" Text="Edit" 
                        ImageUrl="~/Images/edittoolbar.gif" ImageAlign="Middle"/>--%>
                        <asp:ImageButton ID="gvBenefitCalGrid_Edit" runat="server" title="<%$ Resources:ttEdit %>"
                        ImageUrl="~/Images/edittoolbar.gif" ImageAlign="Middle"/>
                    <br />
        
                  <%--  <asp:ImageButton ID="gvBenefitCalGrid_Delete" runat="server" Text="Delete" 
                        ImageUrl="~/Images/delete.gif" ImageAlign="Middle" 
                        onclick="gvBenefitCalGrid_Delete_Click"/>--%>
                         <asp:ImageButton ID="gvBenefitCalGrid_Delete" runat="server" title="<%$ Resources:ttDelete %>"
                        ImageUrl="~/Images/delete.gif" ImageAlign="Middle" 
                        onclick="gvBenefitCalGrid_Delete_Click"/>
                         <!--**ksahu5-ML-MITS34092 End **-->
			    </td>
		    </tr>
	    </table>
	     <asp:TextBox ID="hdnclassid" runat="server" style="display:none" RMXType="id"></asp:TextBox>
	    <asp:TextBox ID="hdnTdRowId" runat="server" style="display:none" RMXType="id"></asp:TextBox>
	    <asp:TextBox ID="hdnSelectedId" runat="server" style="display:none" RMXType="id"></asp:TextBox>
	    <table>
	        <tr>
	            <td>
                 <!--**ksahu5-ML-MITS34092 Start **-->  
	               <%-- <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();return false;" Text="Close" />--%>
                    <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();return false;" Text="<%$ Resources:btnClose %>" />
                     <!--**ksahu5-ML-MITS34092 End **-->  
	            </td>
	        </tr>
	    </table>
    </form>
</body>
</html>
