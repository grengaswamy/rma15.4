﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="LookUpNavigation.aspx.cs" Inherits="Riskmaster.UI.LookupData.LookUpNavigation"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" rel="stylesheet" /><link href="../../App_Themes/RMX_Default/round-button.css" type="text/css" rel="stylesheet" /><link href="../../App_Themes/RMX_Default/Tabs.css" type="text/css" rel="stylesheet" />
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <uc4:CommonTasks ID="CommonTasks1" runat="server" />
  <title>Lookup Data</title>
    <%--<link href="../../Scripts/jquery/themes/base/jquery-ui.css" rel="stylesheet" />--%>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">
  </script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/jquery/jquery-1.8.0.js"></script>
    <%--<script type="text/javascript" src="../../Scripts/jquery/ui/jquery-ui.js"></script>--%>
  <script type="text/javascript" language="javascript">
        function getPage(toPage)
		{
			document.getElementById('pagenumber').value = toPage;
			document.getElementById('hdnaction').value = 'GoToAnotherPage';
			document.forms[0].submit();
			return false;
		}
		function OpenRecord(pid, rowid) {
			//NEW VERSION FOR MDI (R5)
			var category = document.getElementById('form').value;

			//Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
			if (category == "otherunitloss") {
				var claimid = document.getElementById('parentid');
				var filteredParams = "?attachtable=claim" + "&recordID=" + rowid;
				var del = parent.unitSeparator;
				category = "entitymaint";
				if (claimid != null && Trim(claimid.value) != '') {
				    parent.MDIShowScreen(rowid, category + del + "" + del + "UI/FDM/entitymaint.aspx" + del + filteredParams + "&claimid=" + claimid.value + "&NavigatedFormName=entitymaint");     //avipinsrivas start : Worked on JIRA - 15220
					return;
				}
			}
			//Ankit End
			parent.MDIShowScreen(rowid, category);
			return;
		}

      //Added:yukti, DT:05/27/2014, MITS 35772
		$(document).ready(function () {

		    $('td:nth-child(10)').each(function () {
		        var toolTipValue = $(this).closest('tr').attr('pid5');
		        if (toolTipValue != null && toolTipValue != '') {
		            toolTipValue = toolTipValue.replace(/,/g, '\n');
		            $(this).attr('data-title', toolTipValue);
		            $(this).addClass('tip');
		        }

		    });
		    
		});
		
	  function changemode()
	  {
		if(document.forms[0].ordermode.value=="" || document.forms[0].ordermode.value=="DESC")
	    	document.forms[0].ordermode.value="ASC";
		else
			document.forms[0].ordermode.value="DESC";
		return true;
      }
      
  </script>
    <%--Added:yukti, MITS 35772--%>
  <style type="text/css">
      /*.ui-tooltip {
	    padding: 8px;
	    position: absolute;
	    z-index: 9999;
	    max-width: 300px;
	    -webkit-box-shadow: 0 0 5px #aaa;
	    box-shadow: 0 0 5px #aaa;
    }
    body .ui-tooltip {
	    border-width: 2px;
    }*/

    .tip {

    position: relative;
}

.tip:hover {
    text-decoration: none;
}

.tip:hover:after {
    background: #111;
    background-color: whitesmoke;
    border-color: black;
    border-width: 0.4em 0.4em 0.4em 0.4em;
    border: solid;
    border-radius: .1em;
    bottom: 2.35em;
    color: #000;
    content: attr(data-title);
    display: block;
    left: 1em;
    padding: .3em 1em;
    position: absolute;
    text-shadow: 0 1px 0 #000;
    white-space: pre;
    z-index: 98;
}

.tip:hover:before {
    border: inset;
    border-color: #111 transparent;
    border-width: .4em .4em 0 .4em;
    bottom: 2em;
    content: "";
    display: block;
    left: 2em;
    position: absolute;
    z-index: 99;
}
  </style>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <%if (string.Compare(form.Text, "unit", true) == 0 || string.Compare(form.Text, "propertyloss", true) == 0)
          {%>
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift">        
    <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
     <tr>
      <td align="center" valign="middle" height="32">
      <asp:ImageButton id="UnitCovLst" ImageUrl="~/Images/tb_UnitCovSummary_active.png" class="bold" ToolTip="Unit Coverage Summary" 
              OnClientClick=" UnitCovSummary();"  runat="server" />
      </td>
     </tr>
    </table>
    
   </div>
   <%} %>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
    <tr class="msgheader">
     <td class="msgheader" colspan="">Lookup results</td>
    </tr>
   </table>
   

    <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
<tr class="headertext2">
<td align="left" class="headertext2">
<asp:dropdownlist id="ddlPage" runat="server" AutoPostBack="True"  Font-Size="Small" > </asp:dropdownlist>
</td>
<td align="right" class="headertext2">
  <asp:LinkButton id="lnkFirst" ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> 
  <asp:LinkButton id="lnkPrev" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNext" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLast" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
</td>
</tr>
</table>

    <asp:GridView ID="grdLookUp" runat="server"
        onpageindexchanging="grdLookUp_PageIndexChanging" 
        onrowdatabound="grdLookUp_RowDataBound"
        CellPadding="4 "  Width="100%" AllowSorting="true" onsorting="grdLookUp_Sorting" 
         >
        
<PagerStyle CssClass="headertext2"  ForeColor=White />
            <HeaderStyle CssClass="colheader6" />
            <AlternatingRowStyle CssClass="data2" /> 
            <FooterStyle  CssClass ="colheader6" />
    </asp:GridView>


     <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
<tr class="headertext2">
<td align="left" class="headertext2">
<asp:dropdownlist id="ddlSort" runat="server" AutoPostBack="True"  Font-Size="Small" > </asp:dropdownlist>
<asp:dropdownlist id="ddlOrderMode" runat="server" AutoPostBack="True"  Font-Size="Small" > 
<asp:ListItem Text="ASCENDING" Value="ASC">
</asp:ListItem>
<asp:ListItem Text="DESCENDING" Value="DESC">
</asp:ListItem>
</asp:dropdownlist>
</td>
<td align="right" class="headertext2">
  
</td>
</tr>
</table>
   
     <asp:textbox style="display: none" runat="server" id="orderBy" rmxretainvalue="true" rmxref="Instance/Document/Data/OrderBy"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="ordermode" rmxretainvalue="true" rmxref="Instance/Document/Data/OrderMode"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="showAll" rmxretainvalue="true" rmxref="Instance/Document/Data/ShowAll"
                 Text=""   rmxtype="hidden" />
        	    <%-- dvatsa - JIRA 11108(start) --%>
                 <asp:textbox style="display: none" runat="server" id="ContentType" rmxretainvalue="true" rmxref="Instance/Document/Data/ContentType"
                 Text=""   rmxtype="hidden" />
                <%-- dvatsa -JIRA 11108(end) --%>
                 <asp:textbox style="display: none" runat="server" id="form" rmxretainvalue="true" rmxref="Instance/Document/Data/@form"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="fpid" rmxretainvalue="true" rmxref="Instance/Document/Data/@fpid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="recordcount"  rmxref="Instance/Document/Data/@recordcount"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagecount"  rmxref="Instance/Document/Data/@pagecount"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="fpidname" rmxretainvalue="true" rmxref="Instance/Document/Data/@fpidname"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="sid" rmxretainvalue="true" rmxref="Instance/Document/Data/@sid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="psid" rmxretainvalue="true" rmxref="Instance/Document/Data/@psid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="sys_ex" rmxretainvalue="true" rmxref="Instance/Document/Data/@sys_ex"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagenumber" rmxref="Instance/Document/Data/@pagenumber"
                 Text=""   rmxtype="hidden" />
                  <asp:textbox style="display: none" runat="server" id="thispage" rmxref="Instance/Document/Data/@thispage"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagesize" rmxref="Instance/Document/Data/@pagesize"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="parentid" rmxref="Instance/Document/Data/@ParentID" rmxignoreget="true"
                 Text=""   rmxtype="hidden" />
                 <%-- 10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet--%>
                 <asp:textbox style="display: none" runat="server" id="parentsysformname" rmxretainvalue="true" rmxref="Instance/Document/Data/@parentsysformname"
                 Text=""   rmxtype="hidden" />
                <%--avipinsrivas start : Worked on JIRA - 15220--%>
                <asp:textbox style="display: none" runat="server" id="NavigatedFormName" rmxretainvalue="true" rmxref="Instance/UI/FormVariables/SysExData/NavigatedFormName"
                 Text=""   rmxtype="hidden" />
                 <%--avipinsrivas end--%>
                 
    </form>
</body>
</html>
