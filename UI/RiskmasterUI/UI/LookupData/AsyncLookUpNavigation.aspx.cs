﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;//dvatsa-11108
using Newtonsoft.Json;//dvatsa-11108

namespace Riskmaster.UI.UI.LookupData
{
    public partial class AsyncLookUpNavigation : NonFDMBasePageCWS
    {
        ArrayList arrIndex = null;
        string m_sData = "";
        static string sPageName = "AsyncLookUpNavigation.aspx";//dvatsa-JIRA 11108
        static string sSuccess = "Success";//dvatsa-JIRA 11108
        public static string formName = "";
        override protected void OnInit(EventArgs e)
        {

            XmlDocument resultDoc = new XmlDocument();//dvatsa -JIRA 11108
            XmlElement xmlIn;//dvatsa -JIRA 11108
            bool bReturnStatus = false;
            string sreturnValue = "";
         
            try
            {
                //dvatsa-JIRA 11108(start)
                string FDHToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId(sPageName.ToUpper()), "FDHToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "FDHToolTips", FDHToolTips, true);

                string FDHLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId(sPageName.ToUpper()), "FDHLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "FDHLabels", FDHLabels, true);

                string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);

                string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

                string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);

               //dvatsa- JIRA 11108(end)

                if (!IsPostBack)
                {
                    form.Text = AppHelper.GetQueryStringValue("SysFormName");
                    hdnFormName.Value = form.Text;//dvatsa- JIRA 11108 (needed as we are giving dynamic grid id)
                    ContentType.Text = "JSON";
                    parentid.Text = AppHelper.GetQueryStringValue("ParentID");
                    //10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet
                    parentsysformname.Text = AppHelper.GetQueryStringValue("parentsysformname");

                    TextBox txtpagecount = (TextBox)this.Form.FindControl("pagecount");
                    bReturnStatus = CallCWSFunctionBind("LookupDataAdaptor.GetLookupData", out sreturnValue);
                   if(bReturnStatus!= null)
                     resultDoc.LoadXml(sreturnValue);
                         xmlIn = (XmlElement)resultDoc.SelectSingleNode("//LookUpNavigation//Data");
                         if (xmlIn != null)
                         hdnJsonData.Value = xmlIn.InnerText;
                         xmlIn = (XmlElement)resultDoc.SelectSingleNode("//LookUpNavigation//UserPref");
                         if (xmlIn != null)
                         hdnJsonUserPref.Value = xmlIn.InnerText;
                         xmlIn = (XmlElement)resultDoc.SelectSingleNode("//LookUpNavigation//AdditionalData");
                         if (xmlIn != null)
                         hdnJsonAdditionalData.Value = xmlIn.InnerText;
                    
                  }
          }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [System.Web.Services.WebMethod]
        public static string SavePreferences(string gridPreference, string gridId)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            string sReturnResponse = "";
            Dictionary<string, string> dicResponse = new Dictionary<string, string>();
            try
            {   //remove hyper link cell template from columns while save 
                Dictionary<string, object> l_dict = new Dictionary<string, object>();
                JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(gridPreference)), l_dict);
                List<Dictionary<string, object>> l_dict2 = new List<Dictionary<string, object>>();
                JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(l_dict["colDef"].ToString())), l_dict2);
                l_dict["colDef"] = l_dict2;
                gridPreference = JsonConvert.SerializeObject(l_dict);

                gridPreference = AppHelper.HtmlEncodeString(gridPreference);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>GridPreferenceAdaptor.SaveUserHeaderAndPreference</Function>");
                sbMessageTemplate.Append("</Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<Lookup>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(gridId);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>AsyncLookUpNavigation.aspx</PageName>");
                sbMessageTemplate.Append("<UserPref>" + gridPreference + "</UserPref>");
                sbMessageTemplate.Append("</Lookup>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString()); //return bool.   
                XmlDocument objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(sReturn);


                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    dicResponse.Add("error", "true");
                    dicResponse.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
                }
                //else                    
                //    dicResponse.Add("response", sSuccess);                
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            catch (Exception ex)
            {
                dicResponse.Add("error", "true");
                dicResponse.Add("errorMessage", ex.Message);
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            finally
            {
                sbMessageTemplate = null;
            }
            return sReturnResponse;

        }

        [System.Web.Services.WebMethod]
        public static string RestoreDefault(string inputData)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            XmlDocument objXmlDoc = new XmlDocument();
            Dictionary<string, string> dicInputData;
            string sResponseData = "";
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>();
            try
            {
                dicInputData = new Dictionary<string, string>();
                dicResponseData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(inputData, dicInputData);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>LookupDataAdaptor.RestoreDefaults</Function>");
                sbMessageTemplate.Append(" </Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<RestoreDefault>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(dicInputData["GridId"]);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>AsyncLookUpNavigation.aspx</PageName>");
                sbMessageTemplate.Append("</RestoreDefault>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString());
                objXmlDoc.LoadXml(sReturn);
                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    xmlIn = (XmlElement)objXmlDoc.SelectSingleNode("//UserPref");
                    if (xmlIn != null)
                    {
                        sResponseData = xmlIn.InnerText;
                    }
                }
                else
                {//error handling
                    dicResponseData.Add("error", "true");
                    dicResponseData.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
                }
            }
            catch (Exception ex)
            {
                dicResponseData.Add("error", "true");
                dicResponseData.Add("errorMessage", ex.Message);
                sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
            }

            finally
            {
                sbMessageTemplate = null;
                objXmlDoc = null;
                dicInputData = null;
                xmlIn = null;
                dicResponseData = null;
            }
            return sResponseData;
        }

       
        

                 }
}
