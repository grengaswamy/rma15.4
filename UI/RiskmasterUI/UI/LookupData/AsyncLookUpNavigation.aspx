<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest ="false" CodeBehind="AsyncLookUpNavigation.aspx.cs" Inherits="Riskmaster.UI.UI.LookupData.AsyncLookUpNavigation" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" rel="stylesheet" /><link href="../../App_Themes/RMX_Default/round-button.css" type="text/css" rel="stylesheet" /><link href="../../App_Themes/RMX_Default/Tabs.css" type="text/css" rel="stylesheet" />
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <uc4:CommonTasks ID="CommonTasks1" runat="server" />
  <title>Lookup Data</title>
    <%--<link href="../../Scripts/jquery/themes/base/jquery-ui.css" rel="stylesheet" />--%>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">
  </script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/jquery/jquery-1.8.0.js"></script>
    <%--<script type="text/javascript" src="../../Scripts/jquery/ui/jquery-ui.js"></script>--%>
    <%-- dvatsa(start)--%>
     <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <script src="../../Scripts/angularjs/angular.min.js"></script>    
     <script type="text/javascript" src="../../Scripts/angularjs/Directives/rma-ng-grid.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ng-grid.debug.js" ="../../Scripts/angularjs/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>    
    <script type="text/javascript" src="../../Scripts/angularjs/Controllers/LookUpController.js"></script>   
    <script type="text/javascript" src="../../Scripts/angularjs/Directives/NumericInputDirective.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
<%--dvatsa(end) --%>
  <script type="text/javascript" language="javascript">
      //Added:yukti, DT:05/27/2014, MITS 35772
      $(document).ready(function () {

          $('td:nth-child(10)').each(function () {
              var toolTipValue = $(this).closest('tr').attr('pid5');
              if (toolTipValue != null && toolTipValue != '') {
                  toolTipValue = toolTipValue.replace(/,/g, '\n');
                  $(this).attr('data-title', toolTipValue);
                  $(this).addClass('tip');
              }

          });
      });
      function OpenRecord(pid, rowid) {
          //NEW VERSION FOR MDI (R5)
          var category = document.getElementById('form').value;

          //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
          if (category == "otherunitloss") {
              var claimid = document.getElementById('parentid');
              var filteredParams = "?attachtable=claim" + "&recordID=" + rowid;
              var del = parent.unitSeparator;
              category = "entitymaint";
              if (claimid != null && Trim(claimid.value) != '') {
                  parent.MDIShowScreen(rowid, category + del + "" + del + "UI/FDM/entitymaint.aspx" + del + filteredParams + "&claimid=" + claimid.value + "&NavigatedFormName=entitymaint");     //avipinsrivas start : Worked on JIRA - 15220
                  return;
              }
          }
          //Ankit End
          parent.MDIShowScreen(rowid, category);
          return;
      }
      //RMA-8490 START
      function NavigateToReserve() {
          var Claimanteid = document.getElementById("hdnClaimanteid").value;
          if (Claimanteid == '') {
              alert('Please select a row');
              return false;
          }
          else {
              //OpenRecord(document.getElementById("hdnpid2").value, document.getElementById("hdnrowid").value);
              //NEW VERSION FOR MDI (R5)
              //var category = "claimant";
              
              var pid = document.getElementById("hdnpid").value;
              var category = "reservelistingclaimant";
              //var filteredParams = "&openreserves=true";
              //var del = parent.unitSeparator;              
              parent.MDIShowScreen(pid, category);
              return;
          }
          return false;
      }
      //RMA-8490 END
</script>
    <%--Added:yukti, MITS 35772--%>
  <style type="text/css">
      /*.ui-tooltip {
	    padding: 8px;
	    position: absolute;
	    z-index: 9999;
	    max-width: 300px;
	    -webkit-box-shadow: 0 0 5px #aaa;
	    box-shadow: 0 0 5px #aaa;
    }
    body .ui-tooltip {
	    border-width: 2px;
    }*/

    .tip {

    position: relative;
}

.tip:hover {
    text-decoration: none;
}

.tip:hover:after {
    background: #111;
    background-color: whitesmoke;
    border-color: black;
    border-width: 0.4em 0.4em 0.4em 0.4em;
    border: solid;
    border-radius: .1em;
    bottom: 2.35em;
    color: #000;
    content: attr(data-title);
    display: block;
    left: 1em;
    padding: .3em 1em;
    position: absolute;
    text-shadow: 0 1px 0 #000;
    white-space: pre;
    z-index: 98;
}

.tip:hover:before {
    border: inset;
    border-color: #111 transparent;
    border-width: .4em .4em 0 .4em;
    bottom: 2em;
    content: "";
    display: block;
    left: 2em;
    position: absolute;
    z-index: 99;
}
/* dvatsa- JIRA RMA-17679(start)*/
.ngHeaderText {
 font-size :smaller !important;
 font-family:Arial;
}
.ngCellText {
  font-size :smaller !important;
  font-family:Arial;
 }
.ngFooterPanel {
    font-size :smaller !important;
  font-family:Arial;
}
      img {
          cursor: pointer;
          cursor: hand;
      }
/* dvatsa- JIRA RMA-17679(end)*/
</style>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <%if (string.Compare(form.Text, "unit", true) == 0 || string.Compare(form.Text, "propertyloss", true) == 0)
          {%>
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift">        
    <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
     <tr>
      <td align="center" valign="middle" height="32">
      <img id="UnitCovLst" src="~/Images/tb_UnitCovSummary_active.png" class="bold" alt="Unit Coverage Summary" title="Unit Coverage Summary" 
              onclick="UnitCovSummary();"  runat="server" />
      </td>
     </tr>
    </table>
    
   </div>
   <%} %>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
    <tr class="msgheader">
     <td class="msgheader" colspan="">Lookup results</td>
    </tr>
   </table>
        <div id="divSummary" runat="server">
     <img runat="server" onclick="NavigateToReserve();"  src="../../Images/tb_finsummary_active.png" 
                            width="28" height="28" border="0" id="Summary" title="<%$ Resources:ttFinancialSummary %>" onmouseover="this.src='../../Images/tb_finsummary_mo.png';this.style.zoom='110%'"
                            onmouseout="this.src='../../Images/tb_finsummary_active.png';this.style.zoom='100%'" /><%--RMA-8490--%>
            </div>
 <asp:textbox style="display: none" runat="server" id="orderBy" rmxretainvalue="true" rmxref="Instance/Document/Data/OrderBy"
                 Text=""   rmxtype="hidden" />
                <asp:textbox style="display: none" runat="server" id="hdnpid" rmxretainvalue="true"   Text=""   rmxtype="hidden" />
                
         <asp:textbox style="display: none" runat="server" id="hdnCarrierClaims" rmxref="Instance/Document/Data/@carrierclaims"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="ordermode" rmxretainvalue="true" rmxref="Instance/Document/Data/OrderMode"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="showAll" rmxretainvalue="true" rmxref="Instance/Document/Data/ShowAll"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="form" rmxretainvalue="true" rmxref="Instance/Document/Data/@form"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="fpid" rmxretainvalue="true" rmxref="Instance/Document/Data/@fpid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="recordcount"  rmxref="Instance/Document/Data/@recordcount"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagecount"  rmxref="Instance/Document/Data/@pagecount"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="fpidname" rmxretainvalue="true" rmxref="Instance/Document/Data/@fpidname"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="sid" rmxretainvalue="true" rmxref="Instance/Document/Data/@sid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="psid" rmxretainvalue="true" rmxref="Instance/Document/Data/@psid"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="sys_ex" rmxretainvalue="true" rmxref="Instance/Document/Data/@sys_ex"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagenumber" rmxref="Instance/Document/Data/@pagenumber"
                 Text=""   rmxtype="hidden" />
                  <asp:textbox style="display: none" runat="server" id="thispage" rmxref="Instance/Document/Data/@thispage"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="pagesize" rmxref="Instance/Document/Data/@pagesize"
                 Text=""   rmxtype="hidden" />
                 <asp:textbox style="display: none" runat="server" id="parentid" rmxref="Instance/Document/Data/@ParentID" rmxignoreget="true"
                 Text=""   rmxtype="hidden" />
                <%--dvatsa -JIRA 11108(start)--%>
                <asp:textbox style="display: none" runat="server" id="ContentType" rmxretainvalue="true" rmxref="Instance/Document/Data/ContentType"
                 Text=""   rmxtype="hidden" />
                <%--dvatsa - JIRA 11108 (end) --%>
                 <%-- 10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet--%>
                 <asp:textbox style="display: none" runat="server" id="parentsysformname" rmxretainvalue="true" rmxref="Instance/Document/Data/@parentsysformname"
                 Text=""   rmxtype="hidden" />
                
                 <%--dvatsa(start) --%>

        <input type="hidden" id="hdnFormName" runat="server" />
          <asp:TextBox Style="display: none" ID="hdnClaimanteid" runat="server" ClientIDMode="Static"></asp:TextBox><%--RMA-8490--%>
        <div ng-app="rmaApp" id="ng-app">
        <div ng-controller="LookUpNavigationController">
           
           <div style="clear: both"></div>
           <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldata">
            <tr></tr>
                <tr>
                <td style="width:4%">
                    <span id="Span1" runat="server" width="100%"></span>
                </td>
                <td>
                    <uc1:ErrorControl id="ErrorControl2" runat="server" /> 
                </td>
                    <td></td>
            </tr>
                <tr  Width="100%" class="msgheader">
              <td colspan="2" >
                   <span id="lblcaption" Width="100%"></span>
              </td
            </tr>
              <tr><td><td>&nbsp;</td></tr>
            <tr>
                <td style="width:4%">
                    <span id="lblLevel" runat="server" width="100%"></span>
                </td>
               
            </tr>
        </table>
            <div style="clear: both"></div>

            <!-- GRID Template-->
            <input type="hidden" id="hdnJsonData" runat="server" />
            <input type="hidden" id="hdnJsonUserPref" runat="server" />
            <input type="hidden" id="hdnJsonAdditionalData" runat="server" />
            <rma-grid id = "{{gridid}}"  name="LookUpNavigation"  multiselect="false"  showselectioncheckbox="false" enablerowselection="true" ></rma-grid>
            <!-- GRID Template-->
             <div style="text-align:center;" >
             <label style="display: none;" id="lblEmptyData" />
             </div>
            <!-- End CUSTOMIZATION-->
            <div style="clear: both"></div>
            
            <div class="footer">
                <iframe id="csvDownloadFrame" style="display:none" />
            </div>
      </div>
    </div>
                <%--dvatsa(end) --%>
                 
    </form>
</body>
</html>

