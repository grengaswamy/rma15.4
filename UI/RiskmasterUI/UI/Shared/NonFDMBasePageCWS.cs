﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared.Controls;
using MultiCurrencyCustomControl;
using System.Threading;
using System.Globalization;
//Add by kuladeep for rmA14.1 performance
//using Riskmaster.UI.CodesService;
using System.Collections.Generic;
using Riskmaster.BusinessHelpers;
using Riskmaster.Models;

namespace Riskmaster.UI
{
    public class NonFDMBasePageCWS : System.Web.UI.Page
    {
        private string g_sXPathDelimiter = "|";
        public XmlDocument Data = new XmlDocument();
        private ArrayList sMissingRefs = new ArrayList();
        bool bReturnStatusCWSCall = false;
        private Hashtable m_RadioValues = new Hashtable();

        public string[] m_sCustomizablememofields = { "memo", "readonlymemo", "textml", "freecode" };

        protected void NonFDMCWSPageLoad(string functionName)
        {
            NonFDMCWSLoadInitialPage();
            if(functionName != null && functionName != "")
               bReturnStatusCWSCall = CallCWSFunction(functionName);
            //Deb Multi Currency
            TextBox currencytype = (TextBox)this.Form.FindControl("currencytype");
            if (currencytype != null)
            {
                if (!string.IsNullOrEmpty(currencytype.Text))
                {
                    Culture = currencytype.Text.Split('|')[1];
                    UICulture = currencytype.Text.Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currencytype.Text.Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(currencytype.Text.Split('|')[1]);
                }
            }
        }
        //Deb Multi Currency
        protected override void InitializeCulture()
        {
            if (Request.Form["currencytype"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Form["currencytype"]))
                {
                    Culture = Request.Form["currencytype"].Split('|')[1];
                    UICulture = Request.Form["currencytype"].Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["currencytype"].Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["currencytype"].Split('|')[1]);
                }
            }
            base.InitializeCulture();
        }
        private void NonFDMCWSLoadInitialPage()
        {
        }

        /// <summary>
        /// Page-level error handling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Error(Object sender, EventArgs e)
        {
            Server.Transfer("~/GenericErrorPage.aspx");
        }
                    
        /// <summary>
        /// Calls a cws function and binds data to controls
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        protected bool CallCWSFunction(string functionName)
        {
            string sCWSresponse = string.Empty;

            return CallCWS(functionName, null, out sCWSresponse, true, true);
        }

        /// <summary>
        /// Calls CWS function and returns the CWS response
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="sCWSresponse"></param>
        /// <returns></returns>
        protected bool CallCWSFunction(string functionName , out string sCWSresponse)
        {
            return CallCWS(functionName, null, out sCWSresponse, true, false);
        }

        /// <summary>
        /// Calls CWS function,returns output response and binds data to cotrols
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="sCWSresponse"></param>
        /// <returns></returns>
        protected bool CallCWSFunctionBind(string functionName, out string sCWSresponse)
        {
            return CallCWS(functionName, null, out sCWSresponse, true, true);
        }

        /// <summary>
        /// Calls CWS function,returns output response and binds data to cotrols
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="sCWSresponse"></param>
        /// <returns></returns>
        protected bool CallCWSFunctionBind(string functionName, out string sCWSresponse, XElement oMessageElement)
        {
            return CallCWS(functionName, oMessageElement, out sCWSresponse, true, true);
        }

        /// <summary>
        /// Calls CWS function according to a template and binds data to control
        /// </summary>
        /// <param name="functionName"></param>
         /// <param name="oMessageElement"></param>
        /// <returns></returns>
        protected bool CallCWSFunction(string functionName, XElement oMessageElement)
        {
            string sCWSresponse = string.Empty;
            return CallCWS(functionName, oMessageElement, out sCWSresponse, false, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="sCWSresponse"></param>
        /// <param name="oMessageElement"></param>
        /// <returns></returns>
        protected bool CallCWSFunction(string functionName, out string sCWSresponse, XElement oMessageElement)
        {
            return CallCWS(functionName, oMessageElement, out sCWSresponse, true, false);
        }
        ///this method is used to change the input xml as per requirement
        ///
        /// 
        /// <summary>
      public virtual void ModifyXml(ref XElement Xelement)
      {
      
      }

        /// <summary>
        /// This method is used to change the Attributes of Controls on the basis of XML Out
        /// </summary>
      public virtual void ModifyControl(XElement Xelement)
      {
      }
     

      
        /// This function is a wrapper for calling CWS and created for reduing duplicated codes.
        /// </summary>
        /// <param name="functionName">CWS BusinessAdaptor function name</param>
        /// <param name="oMessageElement">CWS request message template</param>
        /// <param name="sCWSresponse">CWS response</param>
        /// <param name="bGetControlData">if need to get values from controls and put them into request message</param>
        /// <param name="bSetControlData">if need to put values from response message to controls</param>
        /// <returns></returns>
        protected bool CallCWS(string functionName, XElement oMessageElement, out string sCWSresponse, bool bGetControlData, bool bSetControlData)
        {
            sCWSresponse = string.Empty;

            try
            {
                //If cws request message template is not ps
                if (oMessageElement == null)
                    oMessageElement = GetNonFDMCWSMessageTemplate();

                oMessageElement.XPathSelectElement("./Call/Function").Value = functionName;

                //If needs to get values from server controls and put into the cws request message
                if( bGetControlData)
                    BindNonFDMCWSControlCollection2Data(this.Form.Controls, oMessageElement);
                //For Modifying XML
                ModifyXml(ref oMessageElement);
                //Call WCF wrapper for cws
                string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                XmlDocument oFDMPageDom = new XmlDocument();
                oFDMPageDom.LoadXml(sReturn);
                XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                oFDMPageDom.LoadXml(oInstanceNode.OuterXml);
                Data = oFDMPageDom;

                ModifyControl(oMessageRespElement);
                //If the response values from cws need to be set to controls
                if( bSetControlData )
                    BindData2Control(oMessageRespElement, this.Form.Controls);

                sCWSresponse = sReturn;

                BindData2ErrorControl(sReturn);

                UpdateControls(oFDMPageDom);
                //rupal:start, r8 multicurrency
                try
                {
                    Control oControl = this.Form.FindControl("currencytype");
                    if ((oControl != null) && (oControl is WebControl))
                    {
                        if (!string.IsNullOrEmpty(((TextBox)oControl).Text) && ((TextBox)oControl).Text.IndexOf("|") > -1)
                        {
                            Culture = ((TextBox)oControl).Text.Split('|')[1];
                            UICulture = ((TextBox)oControl).Text.Split('|')[1];
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(((TextBox)oControl).Text.Split('|')[1]);
                            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(((TextBox)oControl).Text.Split('|')[1]);
                        }
                    }
                    else
                    {
                        XElement xElm = oMessageRespElement.XPathSelectElement("//BaseCurrencyType");
                        if (xElm != null)
                        {
                            Culture = xElm.Value;
                            UICulture = xElm.Value;
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(xElm.Value);
                            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(xElm.Value);
                            TextBox currencytype = new TextBox();
                            currencytype.ID = "currencytype";
                            currencytype.Text = "|" + xElm.Value;
                            currencytype.Style.Add("display", "none");
                            this.Form.Controls.Add(currencytype);
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                //rupal:end, r8 multicurrency

                return true;
             
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="diaryDoc"></param>
        /// <returns></returns>
        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Bind data from CWS response to ASP.NET server controls
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oControls"></param>
        protected void BindData2Control(XElement oMessageElement, ControlCollection oControls)
        {
            //Asharma326 25824 Check permissions Starts
            bool varEnableDisable = false;
            if (oMessageElement.Element("ClaimantMMSEA") != null)
            {
                if (oMessageElement.Element("ClaimantMMSEA").Element("IsReadOnly") != null)
                {
                    var IsReadOnly = (from temp in oMessageElement.Element("ClaimantMMSEA").Elements("IsReadOnly")
                                      select temp.Value).FirstOrDefault().ToString();
                    if ((IsReadOnly != null) && (IsReadOnly == "true"))
                    {
                        varEnableDisable = true;
                    }
                }
            }
            //Asharma326 25824 Check permissions Ends
            foreach (Control oCtrl in oControls)
            {
                if (oCtrl is WebControl && oCtrl.Controls.Count == 0)
                {
                    //Asharma326 25824 Check permissions Start 
                    if (varEnableDisable)
                    {
                        if (oCtrl.ClientID == oCtrl.ID)
                        {
                            if (((WebControl)oCtrl).Attributes["rmxtype"] != "id" && ((WebControl)oCtrl).Attributes["rmxtype"] != "hidden")
                                DatabindingHelper.DisableControls(oCtrl.ClientID, this.Page);
                        }
                        else
                        {
                            string sControlId = oCtrl.ClientID.Substring(0, (oCtrl.ClientID.Length - oCtrl.ID.Length - 1));
                            DatabindingHelper.DisableControls(sControlId, this.Page);
                        }

                    }
                    //Asharma326 25824 Check permissions Ends

                    // We need to find the type of the Control. The implementation changes for Controls like Combobox
                    Type controlType = oCtrl.GetType();
                    string sType = controlType.ToString();
                    int index = sType.LastIndexOf(".");
                    sType = sType.Substring(index + 1);
                    string sRMXRef;
                    string sItemSetRef;
                    string sRMXType;
                    string sSelectedValue = string.Empty;
                     string sValueCollection;
                    string sRMXRetainValue = ((WebControl)oCtrl).Attributes["rmxretainvalue"];
                    if (!string.IsNullOrEmpty(sRMXRetainValue))
                    {
                        if (sRMXRetainValue == "true")
                            continue;
                    }
                   
                    //Binding functions should ignore such controls
                    if (((WebControl)oCtrl).Attributes["rmxignorevalue"] != null)
                        continue;
                    string sRMXIgnoreGet = ((WebControl)oCtrl).Attributes["rmxignoreget"];

                    if (!string.IsNullOrEmpty(sRMXIgnoreGet))
                    {
                        if (sRMXIgnoreGet == "true")
                            continue;
                    }
					
                    sRMXRef = ((WebControl)oCtrl).Attributes["RMXRef"];
                    if (string.IsNullOrEmpty(sRMXRef))
                        continue;

                    switch (sType)
                    {
                        // For Combobox we need to keep the Collection of elements also which is bound to itemsetref node.
                        case "DropDownList":
                        case "ListBox":
                           sRMXRef = ((WebControl)oCtrl).Attributes["RMXRef"];
                           sItemSetRef = ((WebControl)oCtrl).Attributes["ItemSetRef"];
                           sRMXRef = GetResponseRefPath(sRMXRef);
                           //The same value needs to be set to multiple node in the xml string
                           if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                           {
                               string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                               sRMXRef = lstRMXRef[0];
                           }

                           if (!string.IsNullOrEmpty(sItemSetRef))
                           {
                               sItemSetRef = GetResponseRefPath(sItemSetRef);
                               if (sItemSetRef.IndexOf(g_sXPathDelimiter) > 0)
                               {
                                   string[] lstRMXRef = sItemSetRef.Split(g_sXPathDelimiter.ToCharArray());
                                   sItemSetRef = lstRMXRef[0];
                               }
                               // npadhy If the Path ends with option, the ItemSetRef does not point to a Collection
                               // Removed the Option from the ItemSetRef
                               if (sItemSetRef.EndsWith("option"))
                                   sItemSetRef = sItemSetRef.Replace("/option", "");

                               sValueCollection = GetReturnValue(oMessageElement, sItemSetRef);
                           }
                           else
                           {
                               sValueCollection = "";
                           }
                           if (!string.IsNullOrEmpty(sRMXRef))
                           {
                               sSelectedValue = GetReturnValue(oMessageElement, sRMXRef);
                           }
                           else
                           {
                               sSelectedValue = "";
                           }

                           if (!string.IsNullOrEmpty(sValueCollection))
                               DatabindingHelper.SetValue2Control((WebControl)oCtrl, sSelectedValue, sValueCollection);

                           else
                           {
                               if(sSelectedValue != "RETAINRMXVALUE")
                               {
                                   DatabindingHelper.SetValue2Control((WebControl)oCtrl, sSelectedValue);
                               } // if
                           } // else
                           break;
                        case "CheckBox":
                            //MGaba2: setDataChanged was not called in case of checkbox 
                           ((CheckBox)oCtrl).InputAttributes.Add("onchange", "return setDataChanged(true);");
                           sRMXRef = GetResponseRefPath(sRMXRef);
                           //The same value needs to be set to multiple node in the xml string
                           if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                           {
                               string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                               sRMXRef = lstRMXRef[0];
                           }

                           string sValue = GetReturnValue(oMessageElement, sRMXRef);
                           if (sValue == "RETAINRMXVALUE")
                           {
                               continue;
                           } // if
                           else
                           {
                               DatabindingHelper.SetValue2Control((WebControl)oCtrl, sValue);
                           } // else
                           break;
                        case "CurrencyTextbox":
                            sRMXRef = GetResponseRefPath(sRMXRef);
                            if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                            {
                                string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                                sRMXRef = lstRMXRef[0];
                            }
                            string sCurrencyMode = ((WebControl)oCtrl).Attributes["CurrencyMode"];
                            XElement xElm = oMessageElement.XPathSelectElement("//BaseCurrencyType");
                            if (xElm != null)
                            {
                                if (string.IsNullOrEmpty(sCurrencyMode))
                                {
                                    ((CurrencyTextbox)oCtrl).SetProperties(xElm.Value, xElm.Value);
                                }
                                else
                                {
                                    ((CurrencyTextbox)oCtrl).SetProperties(null, xElm.Value);
                                }
                            }
                            sValue = GetReturnValue(oMessageElement, sRMXRef);
                            if (sValue == "RETAINRMXVALUE")
                            {
                              continue;
                            } // if
                            else
                            {
                                DatabindingHelper.SetValue2Control((CurrencyTextbox)oCtrl, sValue);
                            } // else
                            break;
                        default:
                            sRMXRef = GetResponseRefPath(sRMXRef);
                            //The same value needs to be set to multiple node in the xml string
                            if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                            {
                                string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                                sRMXRef = lstRMXRef[0];
                            }

                            sValue = GetReturnValue(oMessageElement, sRMXRef);
                                if (sValue == "RETAINRMXVALUE")
                                {
                                    continue;
                                } // if
                                else
                                {
                                    DatabindingHelper.SetValue2Control((WebControl)oCtrl, sValue);
                                } // else
                            break;
                    }

                    //Raman Bhatia: Update customizations for rows and cols in freecode,memo,textml and readonly memo controls
                    sRMXType = ((WebControl)oCtrl).Attributes["RMXType"];
                    if (m_sCustomizablememofields.Contains(sRMXType))
                    {
                        DatabindingHelper.UpdateCustomizedControls(oMessageElement, sRMXType, oCtrl);
                    }
                }
                else if (oCtrl is UserControlDataGrid)
                {
                    //Asharma326 25824 Check permissions Starts
                    if (varEnableDisable)
                    {
                        ((UserControlDataGrid)oCtrl).HideButtons = "New|Edit|Delete|Clone";
                    }
                    //Asharma326 25824 Check permissions Ends
                    XmlDocument objXml = new XmlDocument();
                    XmlReader objReader;
                    objReader = oMessageElement.CreateReader();
                    objXml.Load(objReader);
                    ((UserControlDataGrid)oCtrl).BindData(objXml);
                    // rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                    if (oCtrl.Controls.Count > 0)
                        BindData2Control(oMessageElement, oCtrl.Controls);
                }
                else if (oCtrl is UserControlDataGridTelerik)
                {
                    XmlDocument objXml = new XmlDocument();
                    XmlReader objReader;
                    objReader = oMessageElement.CreateReader();
                    objXml.Load(objReader);
                    ((UserControlDataGridTelerik)oCtrl).BindData(objXml);
                }
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        BindData2Control(oMessageElement, oCtrl.Controls);
                }
            }
        }
        
        private void UpdateControls(XmlDocument objXmlDocument)
        {
            string[] sReadOnlyNodes = new String[50];
            XmlNode objXmlNode = null;
            XmlNodeList objNodeList = null;

            try
            {
                objXmlNode = objXmlDocument.SelectSingleNode("//DisableControls");
                if (objXmlNode != null)
                {
                    objNodeList = objXmlNode.SelectNodes("Item");

                    foreach (XmlNode objNode1 in objNodeList)
                    {
                        DisableControls(objNode1.InnerText);
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private void DisableControls(string sControlId)
        {
            Control ctrlTemp = null;
            ctrlTemp = this.FindControl(sControlId);
            switch (ctrlTemp.GetType().ToString())
            {
                case "ASP.ui_shared_controls_codelookup_ascx":
                    ((CodeLookUp)ctrlTemp).Enabled = false;
                    break;
                case "ASP.ui_shared_controls_multicodelookup_ascx":
                    ((MultiCodeLookUp)ctrlTemp).Enabled = false;
                    break;
                default:
                    //rsushilaggar 06/02/2010 MITS 20073,20036,20938,20606
                    ctrlTemp = this.FindControl(sControlId);
                    if (ctrlTemp != null)
                    {
                        switch (ctrlTemp.GetType().ToString())
                        {
                            case "System.Web.UI.WebControls.TextBox":
                                ((TextBox)ctrlTemp).ReadOnly = true;
                                ((TextBox)ctrlTemp).Style.Add(HtmlTextWriterStyle.BackgroundColor, "Silver");
                                break;
                            case "System.Web.UI.WebControls.ImageButton":
                                ((ImageButton)ctrlTemp).Enabled = false;
                                break;
                            case "System.Web.UI.WebControls.Button":
                                ((Button)ctrlTemp).Enabled = false;
                                break;
                            case "System.Web.UI.WebControls.CheckBox":
                                ((CheckBox)ctrlTemp).Enabled = false;
                                break;
                            case "System.Web.UI.WebControls.DropDownList":
                                ((DropDownList)ctrlTemp).Enabled = false;
                                break;

                        }
                        
                    }
                    else
                    {
                        ctrlTemp = this.FindControl("div_" + sControlId);
                        if (ctrlTemp != null)
                        {
                            foreach (Control ctl in ctrlTemp.Controls)
                            {
                                switch (ctl.GetType().ToString())
                                {
                                    case "System.Web.UI.WebControls.TextBox":
                                        ((TextBox)ctl).ReadOnly = true;
                                        ((TextBox)ctl).Style.Add(HtmlTextWriterStyle.BackgroundColor, "Silver");
                                        break;
                                    case "System.Web.UI.WebControls.ImageButton":
                                        ((ImageButton)ctl).Enabled = false;
                                        break;
                                    case "System.Web.UI.WebControls.Button":
                                        ((Button)ctl).Enabled = false;
                                        break;
                                    case "System.Web.UI.WebControls.CheckBox":
                                        ((CheckBox)ctl).Enabled = false;
                                        break;
                                    case "System.Web.UI.WebControls.DropDownList":
                                        ((DropDownList)ctl).Enabled = false;
                                        break;

                                }
                            }

                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private string GetReturnValue(XElement oMessageElement, string sPath)
        {
            sPath = sPath.Replace("./Document/", "./");
            //check if XPath is for an attribute
            string sValue = string.Empty;
            XElement oElement = null;
            int index = sPath.LastIndexOf("/");
            string sName = sPath.Substring(index + 1);
            if (sName.StartsWith("@"))
            {
               
                sName = sName.Substring(1);
                string sElementName = sPath.Substring(0, index);
                oElement = oMessageElement.XPathSelectElement(sElementName);
                if (oElement != null)
                {
                    if(oElement.Attribute(sName)!=null)
                    sValue = oElement.Attribute(sName).Value;
                }
            }
            else
            {
                oElement = oMessageElement.XPathSelectElement(sPath);
                if (oElement != null)
                {
                    //For muti-code data type
                    if (oElement.HasElements)
                    {
                        string sChildName = ((XElement)(oElement.FirstNode)).Name.ToString();
                        switch (sChildName)
                        {
                            // npadhy Added a case for option to cater the combobox control,
                                //asif added Types for lob parameter setup screen
                            // which has the item collection in option node.
                                // amrit added cases Transaction,state,Offset for Tax and Offset Mapping screen
                            case "Item":
                            case "option":
                            case "Types":
                            case "User":
                            case "Group":
                            case "Transaction":
                            case "State":
                            case "Name":
                            case "Offset":
                            case "Entity":
                                sValue = oElement.ToString();
                                break;
                            default:
                                sValue = oElement.FirstNode.ToString();
                                break;
                        }
                    }
                    else
                    {
                        sValue = oElement.Value;
                    }
                }
                else
                {
                    sMissingRefs.Add(sPath);

                    if (sPath.Contains("SysPropertyStore"))
                    {
                        sPath = sPath.Replace("Param[@name='SysPropertyStore']", "Param[@name='SysPropertyStore']/Instance");
                    }
                    sMissingRefs.Add(sPath);
                    sValue = "RETAINRMXVALUE";
                }
            }
            ViewState["sMissingRefs"] = sMissingRefs;
            return sValue;
        }

        //Done by Psarin2 for Navigation ..
        //Created a new function to retrieve QueryStringValues bcoz we needed a centralized function for the future 
        //All changes related to retrieval would be done here
        private string GetValues(string value)
        {
            string act = "";
            if (Request.QueryString[value] != null)
            {
                act = Request.QueryString[value];
            }
            return act;
        }//Done by Psarin2 for Navigation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMessageElement"></param>
        internal void BindData2ErrorControl(string oMessageElement)
        {
            Label lblError;
            
            lblError = (Label)this.FindControl("lblError");

            //MJP - if the label is inside the user control (ascx), it would result in a null exception error
            //      the code below fixes that and accesses the label inside the user control
            if (lblError == null)
            {
                UserControl objUserControl = (UserControl)this.FindControl("ErrorControl1");

                if (objUserControl != null)
                {
                    lblError = (Label)objUserControl.FindControl("lblError");
                } // if
            }

            if (lblError != null)
            {
                lblError.Text = ErrorHelper.FormatServiceErrors(oMessageElement);
            }
        }

        /// <summary>
        /// Get values from server controls to create CWS request message
        /// </summary>
        /// <param name="oControls"></param>
        /// <param name="oMessageElement"></param>
        private void BindControlCollection2Data(ControlCollection oControls, XElement oMessageElement)
        {
            foreach (Control oCtrl in oControls)
            {
                //if (oCtrl is WebControl)
                //{
                    string sRMXRef = ((WebControl)oCtrl).Attributes["RMXRef"];
                    string sRMXIgnoreValue = ((WebControl)oCtrl).Attributes["rmxignorevalue"];

                    if (!string.IsNullOrEmpty(sRMXIgnoreValue))
                    {
                        if (sRMXIgnoreValue == "true")
                            continue;
                    }

                    if (!string.IsNullOrEmpty(sRMXRef))
                    {
                        string sValue = DatabindingHelper.GetControlValue((WebControl)oCtrl, m_RadioValues);
                        //The same value needs to be set to multiple node in the xml string
                        if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                        {
                            string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                            foreach (string sXPath in lstRMXRef)
                            {
                                string sRefPath = GetRequestRefPath(sXPath);
                                DatabindingHelper.SetValue2Message(oMessageElement, sRefPath, sValue);
                            }
                        }
                        else
                        {
                            sRMXRef = GetRequestRefPath(sRMXRef);
                            DatabindingHelper.SetValue2Message(oMessageElement, sRMXRef, sValue);
                        }
                    }
                //}
                //else
                //{
                //    if (oCtrl.Controls.Count > 0)
                //        BindControlCollection2Data(oCtrl.Controls, oMessageElement);
                //}
            }
        }

        /// <summary>
        /// Get values from server controls to create CWS request message
        /// </summary>
        /// <param name="oControls"></param>
        /// <param name="oMessageElement"></param>
        protected void BindNonFDMCWSControlCollection2Data(ControlCollection oControls, XElement oMessageElement)
        {
            foreach (Control oCtrl in oControls)
            {
                if (oCtrl is WebControl)
                {
                    string sRMXRef = ((WebControl)oCtrl).Attributes["RMXRef"];
                    string sRMXType = ((WebControl)oCtrl).Attributes["type"];
                    if (!string.IsNullOrEmpty(sRMXType))
                    {
                        DatabindingHelper.CurrentControlType = sRMXType;
                        
                    }
                    else if (!string.IsNullOrEmpty(((WebControl)oCtrl).Attributes["RMXType"]))
                    {
                        DatabindingHelper.CurrentControlType = ((WebControl)oCtrl).Attributes["RMXType"];
                    }

                    string sRMXIgnoreSet = ((WebControl)oCtrl).Attributes["rmxignoreset"];

                    if (!string.IsNullOrEmpty(sRMXIgnoreSet))
                    {
                        if (sRMXIgnoreSet == "true")
                            continue;
                    }

                    if (!string.IsNullOrEmpty(sRMXRef))
                    {
                        string sValue = DatabindingHelper.GetControlValue((WebControl)oCtrl, m_RadioValues);

                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        //sValue = AppHelper.HTMLCustomEncode(sValue.Replace("<", "").Replace(">", ""));
                        //MGaba2: Merging it from Riskmaster branch: BRS screen crashing
                        //sValue = AppHelper.HTMLCustomEncode(sValue);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656

                        //The same value needs to be set to multiple node in the xml string
                        if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                        {
                            string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                            foreach (string sXPath in lstRMXRef)
                            {
                                string sRefPath = GetRequestRefPath(sXPath);
                                DatabindingHelper.SetValue2Message(oMessageElement, sRefPath, sValue);
                            }
                        }
                        else
                        {
                            sRMXRef = GetRequestRefPath(sRMXRef);
                            DatabindingHelper.SetValue2Message(oMessageElement, sRMXRef, sValue, true);
                        }
                    }
                }
                else if (oCtrl is UserControlDataGrid)
                {

                    TextBox txtGenerateXml =(TextBox) this.FindControl("txtGenerateXml");
                    if (txtGenerateXml != null && txtGenerateXml.Text == "true")
                    {

                        XmlDocument objXml = ((UserControlDataGrid)oCtrl).GenerateXml();
                        XElement objElem = XElement.Parse(objXml.InnerXml);
                        string sPath = ((UserControlDataGrid)oCtrl).Target;
                        sPath = sPath.Substring(0, sPath.LastIndexOf("/"));
                        sPath = GetRequestRefPath(sPath);
                        DatabindingHelper.SetObject2Message(oMessageElement, sPath, objElem);
                    }
                    // rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                    if (oCtrl.Controls.Count > 0)
                        BindNonFDMCWSControlCollection2Data(oCtrl.Controls, oMessageElement);
                   
                }
                else if (oCtrl is UserControlDataGridTelerik)
                {

                    TextBox txtGenerateXml = (TextBox)this.FindControl("txtGenerateXml");
                    if (txtGenerateXml != null && txtGenerateXml.Text == "true")
                    {

                        XmlDocument objXml = ((UserControlDataGridTelerik)oCtrl).GenerateXml();
                        XElement objElem = XElement.Parse(objXml.InnerXml);
                        string sPath = ((UserControlDataGridTelerik)oCtrl).Target;
                        sPath = sPath.Substring(0, sPath.LastIndexOf("/"));
                        sPath = GetRequestRefPath(sPath);
                        DatabindingHelper.SetObject2Message(oMessageElement, sPath, objElem);
                    }

                }
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        BindNonFDMCWSControlCollection2Data(oCtrl.Controls, oMessageElement);
                }
            }
        }

        /// <summary>
        /// Get values from server controls to create CWS request message
        /// </summary>
        /// <param name="oControls"></param>
        /// <param name="oMessageElement"></param>
        private void BindSingleControl2Data(Control oControl, XElement oMessageElement)
        {
            if (oControl == null)
                return;

            string sRMXRef = ((WebControl)oControl).Attributes["RMXRef"];
            if (!string.IsNullOrEmpty(sRMXRef))
            {
                string sValue = DatabindingHelper.GetControlValue((WebControl)oControl, m_RadioValues);
                //The same value needs to be set to multiple node in the xml string
                if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                {
                    string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                    foreach (string sXPath in lstRMXRef)
                    {
                        string sRefPath = GetRequestRefPath(sXPath);
                        DatabindingHelper.SetValue2Message(oMessageElement, sRefPath, sValue);
                    }
                }
                else
                {
                    sRMXRef = GetRequestRefPath(sRMXRef);
                    DatabindingHelper.SetValue2Message(oMessageElement, sRMXRef, sValue);
                }
            }
        }

        /// <summary>
        /// Get the Riskmaster required date format: yyyymmdd from mm/dd/yyyy
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        private string GetRMDate(string sValue)
        {
            string sRMDate = string.Empty;
            if (sValue.Length == 10)
            {
                sValue = sValue.Replace("/", string.Empty);
                sRMDate = sValue.Substring(4, 4) + sValue.Substring(0, 2) + sValue.Substring(2, 2);
            }

            return sRMDate;
        }

        /// <summary>
        /// Get the time in format of hhmm from hh:mm
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        private string GetRMTime(string sValue)
        {
            string sRMTime = string.Empty;

            if (sValue.Length == 8)
            {
                sRMTime = sValue.Replace(":", string.Empty);
            }

            return sRMTime;
        }

        /// <summary>
        /// Convert RMXRef value to XPath of CWS request message
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private string GetRequestRefPath(string sPath)
        {
            string sRMXRef = string.Empty;

            //If the path start with Instance/UI/FormVariables, it's should be param[@name='SysFormVariables']
            if (sPath.StartsWith("/option/"))
            {
                sPath = sPath.Replace("/option/", "./");
            }
            else if (sPath.StartsWith("/"))
            {
                    sPath = sPath.Substring(1);
            }

            if (sPath.StartsWith("Instance/UI/"))
            {
                sRMXRef = sPath.Replace("Instance/UI/FormVariables/", "./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/");
            }
            if(sPath.StartsWith("Instance/Document/"))
            {
                sRMXRef = sPath.Replace("Instance/Document/", "./Document/");
            }
            //else
            //{
            //    sRMXRef = "./Document/ParamList/Param[@name='SysPropertyStore']/" + sPath;
            //}
            //If it's not any of the above case, just return the sPath value back
            if (string.IsNullOrEmpty(sRMXRef))
            {
                sRMXRef = sPath;
            }


            return sRMXRef;
        }

        /// <summary>
        /// Convert RMXRef to XPath of CWS response message
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private string GetResponseRefPath(string sPath)
        {
            string sRMXRef = string.Empty;

            //If the path start with Instance/UI/FormVariables, it's should be param[@name='SysFormVariables']
            if (sPath.StartsWith("/option/"))
            {
                sPath = sPath.Replace("/option/", "./");
            }
            else if (sPath.StartsWith("/"))
            {
                    sPath = sPath.Substring(1);
            }

            //if (sPath.StartsWith("Instance/UI/"))
            //{
            //    sRMXRef = sPath.Replace("Instance/UI/FormVariables/", "./ParamList/Param[@name='SysFormVariables']/FormVariables/");
            //}
            //else
            //{
            //    sPath = sPath.Replace("Instance/", string.Empty);
            //    sRMXRef = "./ParamList/Param[@name='SysPropertyStore']/" + sPath;
            //}
            if (sPath.StartsWith("Instance/Document/"))
            {
                sRMXRef = sPath.Replace("Instance/Document/", "./Document/");
            }
            else
            {
                sRMXRef = sPath;
            }
            return sRMXRef;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetNonFDMCWSMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        //copy function from FDMBasePage.cs for the Non FDM Page--stara 05/18/09

        /// <summary>
        /// This function will set rmxignorevalue attribute to a control and all controls in it.
        /// Binding functions would ignore such controls
        /// </summary>
        /// <param name="oControl"></param>
        protected void SetIgnoreValueAttribute(Control oControl)
        {
            // Add the Attribute in the Control Itself
            if (oControl is WebControl)
            {
                ((WebControl)oControl).Attributes.Add("rmxignorevalue", "true");
            }
            // Add attribute to all the Children
            foreach (Control oCtrl in oControl.Controls)
            {
                if (oCtrl is WebControl)
                {
                    ((WebControl)oCtrl).Attributes.Add("rmxignorevalue", "true");
                }
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        SetIgnoreValueAttribute(oCtrl);
                }
            }
        }
        
        
        
        
        /// <summary>
        /// This function will remove rmxignorevalue attribute in a control and all controls in it.
        /// Binding functions would no longer ignore such controls
        /// </summary>
        /// <param name="oControl"></param>
        protected void RemoveIgnoreValueAttribute(Control oControl)
        {
            // Remove the Attribute from the Control Itself
            if (oControl is WebControl)
            {
                ((WebControl)oControl).Attributes.Remove("rmxignorevalue");
            }
            // Remove attribute from all the Children
            foreach (Control oCtrl in oControl.Controls)
            {
                if (oCtrl is WebControl)
                {
                    if (((WebControl)oCtrl).Attributes["rmxignorevalue"] != null)
                    {
                        ((WebControl)oCtrl).Attributes.Remove("rmxignorevalue");
                    }

                }
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        RemoveIgnoreValueAttribute(oCtrl);
                }
            }

        }

        //ends here--stara 05/18/09

        //Add by kuladeep for rmA14.1 Performance Start
        /// <summary>
        ///  //TODO --Currently it is working for Split (Reserve/Transaction) quick lookup.
        /// //We have to add some condition also for make it generic in future.
        /// </summary>
        /// <param name="sCodetype"></param>
        /// <param name="sLookupstring"></param>
        /// <param name="sDescSearch"></param>
        /// <param name="sFilter"></param>
        /// <param name="sLOB"></param>
        /// <param name="sSessionlob"></param>
        /// <param name="sDeptEid"></param>
        /// <param name="sFormname"></param>
        /// <param name="sTriggerDate"></param>
        /// <param name="sEventdate"></param>
        /// <param name="sTitle"></param>
        /// <param name="sSessionClaimId"></param>
        /// <param name="sOrgLevel"></param>
        /// <param name="sInsuredEid"></param>
        /// <param name="sEventId"></param>
        /// <param name="sParentCodeid"></param>
        /// <param name="iCurrentPageNumber"></param>
        /// <param name="sPolicyId"></param>
        /// <param name="sCovTypeCodeId"></param>
        /// <param name="sTransId"></param>
        /// <param name="sClaimantEid"></param>
        /// <param name="sIsFirstFinal"></param>
        /// <param name="sPolUnitRowId"></param>
        /// <param name="sPolicyLOB"></param>
        /// <param name="sCovgSeqNum"></param>
        /// <param name="sRsvStatusParent"></param>
        /// <param name="sTransSeqNum"></param>
        /// <param name="sCoverageKey"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static List<Riskmaster.Models.CodeType> GetQuickLookUpData(
           string sCodetype,
           string sLookupstring,
           string sDescSearch,
           string sFilter,
           string sLOB,
           string sJurisdiction,    //added by swati MITS # 36929
           string sSessionlob,
           string sDeptEid,
           string sFormname,
           string sTriggerDate,
           string sEventdate,
           string sTitle,
           string sSessionClaimId,
           string sOrgLevel,
           string sInsuredEid,
           string sEventId,
           string sParentCodeid,
           int iCurrentPageNumber,
           string sPolicyId,
           string sCovTypeCodeId,
           string sLossTypeCodeId,       //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
           string sTransId,
           string sClaimantEid,
           string sIsFirstFinal,
           string sPolUnitRowId,
           string sPolicyLOB,
           string sCovgSeqNum,
           string sRsvStatusParent,
           string sTransSeqNum, string sFieldName, string sCoverageKey,string sPayCol)
        {
            List<CodeType> codes = null;
            CodeListType objList = null;
            CodesBusinessHelper cb = new CodesBusinessHelper();
            //sCodetype = "code." + sCodetype;
            //sJurisdiction added by swati MITS # 36929
            //objList = cb.QuickLookup(sCodetype, sLookupstring, sDescSearch, sFilter, sLOB, sSessionlob, sDeptEid, sFormname, sTriggerDate, sEventdate, sTitle,
            objList = cb.QuickLookup(sCodetype, sLookupstring, sDescSearch, sFilter, sLOB, sJurisdiction, sSessionlob, sDeptEid, sFormname, sTriggerDate, sEventdate, sTitle,
                                        sSessionClaimId, sOrgLevel, sInsuredEid, sEventId, sParentCodeid, iCurrentPageNumber, sPolicyId, sCovTypeCodeId, sTransId,
                                        sClaimantEid, sIsFirstFinal, sPolUnitRowId, sPolicyLOB, sCovgSeqNum, sRsvStatusParent, sTransSeqNum, sFieldName, sCoverageKey, sLossTypeCodeId,sPayCol);
            codes = objList.Codes.ToList();
            System.Web.HttpContext.Current.Response.AddHeader("PageCount", objList.PageCount);
            return codes;

        }

        //Add by kuladeep for rmA14.1 Performance End
    }
}
