﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.ServiceModel;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Common;
using System.Drawing;
using Riskmaster.UI.OrganisationHierarchy;
using Riskmaster.BusinessAdaptor.Common;
//Deb: Performance changes
//using Riskmaster.UI.PageCacheService;
using System.Collections.Generic;
using Riskmaster.Models;
using Riskmaster.Cache;
//Deb: Performance changes

namespace Riskmaster.Views.Shared.Controls
{
    public partial class OrgTree : System.Web.UI.UserControl
    {
        
        /// <summary>
        /// This nodeList would maintain list of all eligible nodes in order to set their dynamic population property 
        /// </summary>
        ArrayList arlstDynamicPopulatedNodesList = null;
        /// <summary>
        /// A counter
        /// </summary>
        public int m_iCounter = 0;
        /// <summary>
        /// Used for Child Nodes Data manipulation
        /// </summary>
        private string[] m_sNameDelimiter = new string[] { "@%@%" };
        /// <summary>
        /// Used for Child Nodes Data manipulation
        /// </summary>
        private string[] m_sSecondLevelDelimiter = new string[] { "@%" };
        /// <summary>
        /// Used for Child Nodes Data manipulation
        /// </summary>
        private char m_sInsideIdDelimiter = ',';
        /// <summary>
        /// Used for Child Nodes Data manipulation
        /// </summary>
        private char m_sIdDelimiter = '|';
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        //Deb: Performance changes
        private bool bCacheClear = false;
        private bool bRetOrgXmlExists = false;
        //Deb: Performance changes
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                arlstDynamicPopulatedNodesList = new ArrayList();
                oFDMPageDom = new XmlDocument();
                string sReturn = "";
                

                if (!Page.IsPostBack || !Page.IsCallback)
                {
                    //registring onclick event for treeview
                    //this event would handle all kinds of client side manipulations 
                    OTV.Attributes.Add("onclick", "return OnTreeClick(event)");
                    OTV.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { OnTreeClick(event) }");//MITS:35039
                    //Deb: Performance changes
                    Dictionary<string, string> dictUserProp = new Dictionary<string, string>();
                    string sCurrentUser = string.Empty;
                    string sContext = string.Empty;
                    string sCmbLevel = string.Empty;
                    string timestamp = string.Empty;
                    string sOrgHLastUpdated = string.Empty;
                    string controlName = Request.Params.Get("__EVENTTARGET");
                    string sDsnName = AppHelper.ReadCookieValue("DsnName").ToString();
                    //PageCacheServiceClient oClient = new PageCacheServiceClient();
                    //dictUserProp = oClient.GetUserLoginInfo(HttpContext.Current.User.Identity.Name, sDsnName,AppHelper.ClientId);
                    PageData oPageData = new PageData();
                    oPageData.ClientId = AppHelper.ClientId;
                    oPageData.User = HttpContext.Current.User.Identity.Name;
                    oPageData.DsnName = sDsnName;
                    dictUserProp = AppHelper.GetResponse<Dictionary<string, string>>("RMService/Page/logininfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPageData);
                    if (dictUserProp["iOrgSecFlag"] == "True")
                    {
                        sContext = dictUserProp["iBesGroupId"];
                    }
                    //timestamp = oClient.GetTimestampForOrgHierarchy(dictUserProp["connectionString"]);
                    oPageData.ConnKey = dictUserProp["connectionString"];
                    timestamp = AppHelper.GetResponse<string>("RMService/Page/timestampfororg", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPageData);
                    sOrgHLastUpdated = "iOrgHLastUpdated" + dictUserProp["iDsnId"];
                    object iLastUpdated = CacheCommonFunctions.RetreiveValueFromCache<object>(sOrgHLastUpdated, AppHelper.ClientId); //HttpContext.Current.Cache.Get(sOrgHLastUpdated);
                    sCmbLevel = cmb_dlevel.SelectedValue;
                    string sCacheName = "RetOrgXml" + sContext + timestamp + dictUserProp["iDsnId"] + sCmbLevel;
                    if (CacheCommonFunctions.CheckIfKeyExists(sCacheName, AppHelper.ClientId) != null && iLastUpdated != null && controlName != "OT$chkFilter" && !string.IsNullOrEmpty(controlName) && (!string.IsNullOrEmpty(sCmbLevel) || controlName == "OT$cmb_dlevel"))
                    {
                        oFDMPageDom = (XmlDocument)CacheCommonFunctions.RetreiveValueFromCache<object>(sCacheName, AppHelper.ClientId);
                        ModifyTemplate(XElement.Parse(oFDMPageDom.InnerXml));
                    }
                    else
                    {
                        if (iLastUpdated != null)
                        {
                            if (string.Compare(timestamp, iLastUpdated.ToString(), true) != 0)
                            {
                                //foreach (DictionaryEntry entry in Cache)
                                //{
                                //    if (((string)(entry.Key)).StartsWith("RetOrgXml" + sContext + iLastUpdated + dictUserProp["iDsnId"]))
                                //    {
                                //        Cache.Remove(entry.Key.ToString());
                                //    }
                                //}
                                if (CacheCommonFunctions.CheckIfKeyExists("RetOrgXml" + sContext + iLastUpdated + dictUserProp["iDsnId"] + sCmbLevel, AppHelper.ClientId))
                                {
                                    CacheCommonFunctions.RemoveValueFromCache("RetOrgXml" + sContext + iLastUpdated + dictUserProp["iDsnId"] + sCmbLevel, AppHelper.ClientId);
                                }
                                bCacheClear = true;
                            }
                        }
                        if (controlName == null && cmb_dlevel.SelectedIndex == -1 && !bCacheClear && CacheCommonFunctions.CheckIfKeyExists("RetOrgXml" + sContext + iLastUpdated + dictUserProp["iDsnId"] + sCmbLevel, AppHelper.ClientId))
                        {
                            //foreach (DictionaryEntry entry in Cache)
                            //{
                            //    if (((string)(entry.Key)).StartsWith("RetOrgXml" + sContext + timestamp + dictUserProp["iDsnId"]))
                            //    {
                            //        oFDMPageDom = (XmlDocument)HttpContext.Current.Cache.Get(entry.Key.ToString());
                            //        ModifyTemplate(XElement.Parse(oFDMPageDom.InnerXml));
                            //        bRetOrgXmlExists = true;
                            //        break;
                            //    }
                            //}
                            if (CacheCommonFunctions.CheckIfKeyExists("RetOrgXml" + sContext + iLastUpdated + dictUserProp["iDsnId"] + sCmbLevel, AppHelper.ClientId))
                            {
                                oFDMPageDom = (XmlDocument)CacheCommonFunctions.RetreiveValueFromCache<object>("RetOrgXml" + sContext + iLastUpdated + dictUserProp["iDsnId"] + sCmbLevel, AppHelper.ClientId); 
                                ModifyTemplate(XElement.Parse(oFDMPageDom.InnerXml));
                                bRetOrgXmlExists = true;
                            }
                        }
                        if (!bRetOrgXmlExists)
                        {
                            //Preparing XML to send to servicet
                            XElement oMessageElement = GetMessageTemplate();
                            //Modify XML if needed
                            ModifyTemplate(oMessageElement);
                            //Calling Service to get all PreBinded Data 
                            CommonFunctions.CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                            //timestamp = oClient.GetTimestampForOrgHierarchy(dictUserProp["connectionString"]);
                            oPageData.ConnKey = dictUserProp["connectionString"];
                            timestamp = AppHelper.GetResponse<string>("RMService/Page/timestampfororg", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPageData);
                            sCmbLevel = oFDMPageDom.SelectSingleNode("//control[@cmblevel != ' ']").Attributes["cmblevel"].Value;
                            string pCacheName = "RetOrgXml" + sContext + timestamp + dictUserProp["iDsnId"] + sCmbLevel;
                            if (string.IsNullOrEmpty(oMessageElement.XPathSelectElement("./Document/OrgHierarchy/EntityId").Value))
                            {
                                //Raman 1/27/2012 :Disabling Cache till we have a better solution
                                //Even when a default expansion level is changed system does not update cache.. also different users can have different default expansion levels
                                //HttpContext.Current.Cache.Insert(pCacheName, oFDMPageDom);
                            }
                            sOrgHLastUpdated = "iOrgHLastUpdated" + dictUserProp["iDsnId"];
                            //Raman 1/27/2012 :Disabling Cache till we have a better solution.. 
                            //Even when a default expansion level is changed system does not update cache.. also different users can have different default expansion levels
                            //HttpContext.Current.Cache.Insert(sOrgHLastUpdated, timestamp);
                        }
                    }
                    //Deb: Performance changes
                    //Binding Error Control
                    ErrorControl.errorDom = sReturn;

                    //Display any error which came in XML
                    XmlElement oErrorEle = (XmlElement)oFDMPageDom.SelectSingleNode("//error");
                    if (oErrorEle != null)
                    {
                        lblErrorInfo.Text = oErrorEle.Attributes["desc"].Value;
                        if (oErrorEle.Attributes["name"].Value == "norecord")
                        {
                            ViewState.Remove("SearchLevel");
                        }
                        oFDMPageDom.SelectSingleNode("//org").RemoveChild((XmlNode)oErrorEle);
                    }
                    else
                    {
                        lblErrorInfo.Text = "";
                    }

                    //Update Tree Bindings to populate data
                    if (ErrorControl.errorFlag == false)
                    {
                        //Populate Default Expansion drop down list
                        XmlNodeList cmbData = oFDMPageDom.SelectNodes("//levellist/option");
                        cmb_dlevel.Items.Clear();
                        foreach (XmlNode oNode in cmbData)
                        {
                            ListItem oItem = new ListItem(oNode.InnerText, oNode.Attributes["value"].Value);
                            cmb_dlevel.Items.Add(oItem);
                            if (oNode.Attributes["selected"] != null)
                            {
                                if (oNode.Attributes["selected"].Value == "1")
                                {
                                    oItem.Selected = true;
                                }
                            }
                        }

                        //rsushilaggar MITS 18959 Date 04/05/2011
                        XmlNode objFilter = oFDMPageDom.SelectSingleNode("//chkFilter");
                        if (objFilter != null)
                        {
                            chkFilter.Checked = Riskmaster.Common.Conversion.ConvertStrToBool(objFilter.InnerText);
                        }

                        UpdateTreeBindings();

                        //Set Hidden fields needed to populate search criteria in Search Criteria user control
                        XmlElement oSearchEle = (XmlElement)oFDMPageDom.SelectSingleNode("//control[@name='orgTree']");
                        if (oSearchEle != null)
                        {
                            if (oSearchEle.Attributes["srchstr"] != null)
                            {
                                hd_txtsorg.Value = oSearchEle.Attributes["srchstr"].Value;
                            }
                            if (oSearchEle.Attributes["cmblevel"] != null)
                            {
                                hd_slevel.Value = oSearchEle.Attributes["cmblevel"].Value;
                            }
                        }
                        
                        //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                        XmlElement oEle = (XmlElement)oFDMPageDom.SelectSingleNode("//control[@name='hd_tablename']");
                        if (oEle != null)
                            hd_tablename.Value = oEle.InnerText;

                        oEle = (XmlElement)oFDMPageDom.SelectSingleNode("//control[@name='hd_rowid']");
                        if (oEle != null)
                            hd_rowid.Value = oEle.InnerText;

                        oEle = (XmlElement)oFDMPageDom.SelectSingleNode("//control[@name='hd_eventdate']");
                        if (oEle != null)
                            hd_eventdate.Value = oEle.InnerText;

                        oEle = (XmlElement)oFDMPageDom.SelectSingleNode("//control[@name='hd_claimdate']");
                        if (oEle != null)
                            hd_claimdate.Value = oEle.InnerText;

                        //Recursively Iterate over all nodes to prepare an arraylist of all nodes which need to dynamically populated
                        foreach (TreeNode objNode in OTV.Nodes)
                        {
                            UpdateDynamicNodeList(objNode);
                        }
                        //Update properties of node for dynamic population from Service
                        UpdateDynamicNodes();

                        //Emptying hidden fields for search criteria
                        hd_txtsorg.Value = "";

                        //MITS 16771- Pankaj 5/29/09
                        hd_txtcity.Value = "";
                        hd_txtstate.Value = "";
                        hd_txtzip.Value = "";
                        hd_entityid.Value = ""; //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search

                        //Emptying ViewState for search result level
                        ViewState.Remove("SearchLevel");

                        OTV.Nodes[0].SelectAction = TreeNodeSelectAction.None;

                        chkTreeNode.Checked = false;

                    }
                }
                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
         /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                  <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
                  <Call>
                  <Function>OrgHierarchyAdaptor.GetOrgHierarchyXml</Function> 
                  </Call>
                  <Document>
                  <OrgHierarchy>
                  <Expand>true</Expand> 
                  <Level>0</Level> 
                  <Node /> 
                  <NodCat /> 
                  <Search />
                  <City />
                  <State />
                  <Zip />
                  <EntityId/>
                  <Parent /> 
                  <parentFuncId /> 
                  <ReturnXml/>
                  <codeid /> 
                  <TableName /> 
                  <EventDate/>
                  <ClaimDate/>
                  <PolicyDate/>
                  <RowId>0</RowId> 
                  <RetVal />
                  <TreeNode/>
                  <InputXml>
                  <form bcolor='white' cid='orgtree' name='frmData' title='' topbuttons='1'>
                  <toolbar>
                  <button title='New' type='new' /> 
                  <button title='Edit' type='edit' /> 
                  </toolbar>
                  <body1 func='windowstatus_new()' req_func='yes' /> 
                  <group method='post' name='table_detail' title='Table Detail'>
                  <control name='orgTree' num_level='8' type='orgTree'>
                  <org child_name='client' ischild='' level='' name='Organization Hierarchy' parent='' /> 
                  </control>
                  <control name='dlevel' type='hidden' userid='2' ></control>
                  <control name='hd_tablename' type='hidden'/>
                  <control name='hd_rowid' type='hidden'/>
                  <control name='hd_eventdate' type='hidden'/>
                  <control name='hd_claimdate' type='hidden'/>
                  </group>
                  <levellist name='cmb_dlevel'>
                  <option value='C'>Client</option> 
                  <option value='CO'>Company</option> 
                  <option value='O'>Operation</option> 
                  <option value='R'>Region</option> 
                  <option value='D'>Division</option> 
                  <option value='L'>Location</option> 
                  <option value='F'>Facility</option> 
                  <option value='DT'>Department</option> 
                  </levellist>
                  <chkFilter/>
                  </form>
                  </InputXml>
                  </OrgHierarchy>
                  </Document>
                  </Message>

            ");
            return oTemplate;
        }
        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>
        private XElement GetChildNodePopulateMessageTemplate()
        {
            //Shivendu:Changing the template to add tablename,EventDate,ClaimDate and rowid for MITS 17249
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
            <Call>
              <Function>OrgHierarchyAdaptor.GetChildNodes</Function> 
              </Call>
            <Document>
            <form>
              <ReturnXml /> 
              <parentFuncId></parentFuncId> 
              <childnodecounter/>
              <PopulateRemainingNodes/>
              <codeid></codeid> 
              <RowId></RowId>
              <tablename></tablename>
              <EventDate></EventDate>
              <ClaimDate></ClaimDate>
              <PolicyDate></PolicyDate>
              <chkFilter/>
              <RetVal /> 
              <SuccessMsg /> 
              </form>
              </Document>
              </Message>
          
            ");
            return oTemplate;
        }
        /// <summary>
        /// This function would be used to modify the template as and when necessary
        /// </summary>
        /// <param name="oMessageElement"></param>
        private void ModifyTemplate(XElement oMessageElement)
        {
            if (cmb_dlevel.SelectedValue != String.Empty)
            {
                XElement oExpansionLevelElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/InputXml/form/group/control[@name='dlevel']");
                if (oExpansionLevelElement != null)
                {
                    oExpansionLevelElement.Value = cmb_dlevel.SelectedValue;
                }
                //rsushilaggar MITS 18959 Date 04/05/2011
                XElement oChkFilterElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/InputXml/form/chkFilter");
                if (oChkFilterElement != null)
                {
                    oChkFilterElement.Value = Riskmaster.Common.Conversion.ConvertBoolToInt(chkFilter.Checked).ToString();
                }
            }
            
            
            //Reading QueryString and modifying XML accordingly
            string sLob = HttpContext.Current.Request.QueryString["lob"];
            if (sLob != null && sLob == "")
            {
                sLob = "8";
            }
            if (!String.IsNullOrEmpty(sLob))
            {
                XElement oLevelElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Level");
                if (oLevelElement != null)
                {
                    oLevelElement.Value = sLob;
                }
                hd_lob.Value = sLob;
            }
            //Storing lob information in ViewState
            if (ViewState["SelectableLevel"] == null)
            {
                ViewState["SelectableLevel"] = sLob;
            }

            //We may have a requirement to be able to select org levels b/w 2 levels
            //These values can be used in such a scenario
            string sInitialLevel = HttpContext.Current.Request.QueryString["initiallevel"];
            if (ViewState["InitialSelectableLevel"] == null)
            {
                ViewState["InitialSelectableLevel"] = sInitialLevel;
            }

            string sFinalLevel = HttpContext.Current.Request.QueryString["finallevel"];
            if (ViewState["FinalSelectableLevel"] == null)
            {
                ViewState["FinalSelectableLevel"] = sFinalLevel;
            }


            //Search Criteria may arrive from querystring 

            if (ViewState["SearchCriteriaInvalid"] == null)
            {
                string slevel = HttpContext.Current.Request.QueryString["level"];
                string stxtsorg = HttpContext.Current.Request.QueryString["txtorg"];
                //MITS 16771- Pankaj 5/29/09
                string scity = HttpContext.Current.Request.QueryString["txtcity"];
                string sstate = HttpContext.Current.Request.QueryString["txtstate"];
                string sstatetext = HttpContext.Current.Request.QueryString["txtstatetext"];
                string stxtzip = HttpContext.Current.Request.QueryString["txtzip"];
                //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                string stablename = HttpContext.Current.Request.QueryString["tablename"];
                string srowid = HttpContext.Current.Request.QueryString["rowid"];
                string seventdate = HttpContext.Current.Request.QueryString["eventdate"];
                string sclaimdate = HttpContext.Current.Request.QueryString["claimdate"];

                if (!String.IsNullOrEmpty(slevel) && hd_slevel.Value == string.Empty)
                {
                    hd_slevel.Value = slevel;
                    ViewState["SearchCriteriaInvalid"] = "true";
                }
                if(!String.IsNullOrEmpty(stxtsorg) && hd_txtsorg.Value == String.Empty)
                {
                    hd_txtsorg.Value = stxtsorg;
                    ViewState["SearchCriteriaInvalid"] = "true";
                }
                if (!String.IsNullOrEmpty(scity) && hd_txtcity.Value == String.Empty)
                {
                    hd_txtcity.Value = scity;
                    ViewState["SearchCriteriaInvalid"] = "true";
                }
                if (!String.IsNullOrEmpty(sstate) && hd_txtstate.Value == String.Empty)
                {
                    hd_txtstate.Value = sstate;
                    hd_txtstatetext.Value = sstatetext;
                    ViewState["SearchCriteriaInvalid"] = "true";
                }
                if (!String.IsNullOrEmpty(stxtzip) && hd_txtzip.Value == String.Empty)
                {
                    hd_txtzip.Value = stxtzip;
                    ViewState["SearchCriteriaInvalid"] = "true";
                }
            }
            //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
            if (hd_entityid.Value != string.Empty)
            {
                XElement objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/EntityId");
                if (objEle != null)
                {
                    objEle.Value = hd_entityid.Value;
                }
            }

            //If Search Criteria's are set in hidden fields then they need to be updated in XML
            if (hd_txtsorg.Value != String.Empty || hd_txtcity.Value != String.Empty || hd_txtstate.Value != String.Empty || hd_txtzip.Value != String.Empty || hd_entityid.Value != string.Empty)
            {

                XElement objEle = oMessageElement.XPathSelectElement("./Call/Function");
                if (objEle != null)
                {
                    objEle.Value = "OrgHierarchyAdaptor.GetOrgHierarchybyLevel";
                }
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/NodCat");
                if (objEle != null)
                {
                    objEle.Value = hd_slevel.Value;
                }
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Search");
                if (objEle != null)
                {
                    objEle.Value = hd_txtsorg.Value;
                }
                //MITS 16771- Pankaj 5/29/09
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/City");
                if (objEle != null)
                {
                    objEle.Value = hd_txtcity.Value;
                }
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/State");
                if (objEle != null)
                {
                    objEle.Value = hd_txtstate.Value;
                }
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Zip");
                if (objEle != null)
                {
                    objEle.Value = hd_txtzip.Value;
                }

                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/EntityId");
                if (objEle != null)
                {
                    objEle.Value = hd_entityid.Value;
                }

                //Set Search Level in ViewState
                ViewState["SearchLevel"] = CommonFunctions.GetLevel(hd_slevel.Value);

            }
            //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
            if (hd_tablename.Value != String.Empty)
            {
                XElement objEle;
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/TableName");
                if (objEle != null)
                {
                    objEle.Value = hd_tablename.Value;
                }
            }

            if (hd_rowid.Value != String.Empty)
            {
                XElement objEle;
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/RowId");
                if (objEle != null)
                {
                    objEle.Value = hd_rowid.Value;
                }
            }

            if (hd_eventdate.Value != String.Empty)
            {
                XElement objEle;
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/EventDate");
                if (objEle != null)
                {
                    objEle.Value = hd_eventdate.Value;
                }
            }

            if (hd_claimdate.Value != String.Empty)
            {
                XElement objEle;
                objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/ClaimDate");
                if (objEle != null)
                {
                    objEle.Value = hd_claimdate.Value;
                }
            }
 

            //Reading "Search within Selected branch checkbox" and setting appropriate node value in XML
            if (chkTreeNode != null)
            {
                if (chkTreeNode.Checked)
                {
                    XElement objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Node");
                    if (objEle != null)
                    {
                       objEle.Value = hd_selectednodevalue.Value;
                    }

                    objEle = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/TreeNode");
                    if (objEle != null)
                    {
                        objEle.Value = CommonFunctions.GetCatFromLevel(Riskmaster.Common.Conversion.ConvertStrToLong(hd_selectednodevaluelevel.Value));
                    }


                }
            }
            
            XElement objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/TableName");
            if (objElem != null)
            {
                objElem.Value = AppHelper.GetQueryStringValue("tablename");
                //Added by Shivendu for MITS 17249 to store table name in a hidden control
               hd_tablename.Value = objElem.Value;
            }
            //Start by Shivendu to send rowid which contains policy id for MITS 17249
            if (objElem != null && (objElem.Value == "policy" || objElem.Value == "policyenh" || objElem.Value == "entity"))
            {
                //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/RowId");
                if (objElem != null)
                {
                    objElem.Value = AppHelper.GetQueryStringValue("rowid");
                    if (objElem.Value == "" || objElem.Value == null)
                    {
                        objElem.Value = "0";
                    }
                    //Added by Shivendu for MITS 17249 to store row id in a hidden control
                    hd_rowid.Value = objElem.Value;

                }
                objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/PolicyDate");
                if (objElem != null)
                {
                    objElem.Value = AppHelper.GetQueryStringValue("policydate");
                                   
                    //Added by Shivendu for MITS 17249 to store row id in a hidden control
                    hd_policydate.Value = objElem.Value;

                }
            }
            //End by Shivendu to send rowid which contains policy id for MITS 17249

            objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/EventDate");
            if (objElem != null)
            {
                objElem.Value = AppHelper.GetQueryStringValue("eventdate");
                //Added by Shivendu to send EventDate for MITS 17249
                hd_eventdate.Value = objElem.Value;
            }

            objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/ClaimDate");
            if (objElem != null)
            {
                objElem.Value = AppHelper.GetQueryStringValue("claimdate");
                //Added by Shivendu to send ClaimDate for MITS 17249
                hd_claimdate.Value = objElem.Value;
            }
            //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
            objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/InputXml/form/group/control[@name='hd_tablename']");
            if (objElem != null)
                objElem.Value = AppHelper.GetQueryStringValue("tablename");

            objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/InputXml/form/group/control[@name='hd_rowid']");
            if (objElem != null)
                objElem.Value = AppHelper.GetQueryStringValue("rowid");

            objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/InputXml/form/group/control[@name='hd_eventdate']");
            if (objElem != null)
                objElem.Value = AppHelper.GetQueryStringValue("eventdate");

            objElem = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/InputXml/form/group/control[@name='hd_claimdate']");
            if (objElem != null)
                objElem.Value = AppHelper.GetQueryStringValue("claimdate");
        }
        /// <summary>
        /// This function would prepare an arraylist of all nodes which need to dynamically populated
        /// </summary>
        /// <param name="objNode"></param>
        private void UpdateDynamicNodeList(TreeNode objNode)
        {
            //All Nodes Depth would be compared to the Org Level Selected 
            //This information is already present in viewstate
            UpdateSelectableLevels(objNode, objNode.Depth);
            
            //Mark last viewed entityid
            if (objNode.Value == AppHelper.GetQueryStringValue("lastviewedentityid"))
            {
                hd_lastviewedentityid.Value = objNode.Value;
                objNode.Selected = true;
            }
            if (objNode.ChildNodes.Count == 0 && objNode.Depth != ((int)CommonFunctions.enumOrgHierarchyLevels.DEPARTMENT) && objNode.Depth >= CommonFunctions.GetLevel(cmb_dlevel.SelectedValue))
            {
                arlstDynamicPopulatedNodesList.Add(objNode);

            }
            else if(ViewState["SearchLevel"] != null)
            {
                if (objNode.Depth >= Riskmaster.Common.Conversion.ConvertStrToInteger(ViewState["SearchLevel"].ToString()) && objNode.Depth != ((int)CommonFunctions.enumOrgHierarchyLevels.DEPARTMENT))
                {
                    arlstDynamicPopulatedNodesList.Add(objNode);
                }
                else
                {
                    foreach (TreeNode objChildNode in objNode.ChildNodes)
                    {
                        UpdateDynamicNodeList(objChildNode);
                    }
                }

            }
            
            else
            {
                foreach (TreeNode objChildNode in objNode.ChildNodes)
                {
                    UpdateDynamicNodeList(objChildNode);
                }
            }
        }
        /// <summary>
        /// This function would update properties of node for dynamic population from Service
        /// </summary>
        private void UpdateDynamicNodes()
        {
            foreach (TreeNode objNode in arlstDynamicPopulatedNodesList)
            {
                if (objNode != null)
                {
                    TreeNode currentNodeReplica = new TreeNode(objNode.Text, objNode.Value);
                    currentNodeReplica.PopulateOnDemand = true;
                    currentNodeReplica.Expanded = false;
                    TreeNode parentNode = objNode.Parent;
                    
                    
                    //Nodes Depth would be compared to the Org Level Selected 
                    //This information is already present in viewstate
                    UpdateSelectableLevels(currentNodeReplica, objNode.Depth);
                    
                    //If a selected node is removed then replica needs to be selected
                    if (objNode.Selected == true)
                    {
                        currentNodeReplica.Selected = true;
                    }
                    parentNode.ChildNodes.Remove(objNode);
                    parentNode.ChildNodes.Add(currentNodeReplica);
                }
            }
        }
        /// <summary>
        /// This function gets called dynamically from server using AJAX to populate child nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OTV_GetChildNodes(object sender, TreeNodeEventArgs e)
        {
                XElement oMessageElement = GetChildNodePopulateMessageTemplate();
                string sReturn = "";
                XElement oCmdElement = oMessageElement.XPathSelectElement("./Call/Function");
                if (oCmdElement != null)
                {
                    oCmdElement.Value = "OrgHierarchyAdaptor.GetChildNodes";
                }

                oCmdElement = oMessageElement.XPathSelectElement("./Document/form/parentFuncId");
                if (oCmdElement != null)
                {
                    oCmdElement.Value = e.Node.Value;
                }

                oCmdElement = oMessageElement.XPathSelectElement("./Document/form/codeid");
                if (oCmdElement != null)
                {
                    oCmdElement.Value = CommonFunctions.GetCatFromLevel(e.Node.Depth);
                }
                //Start by Shivendu for MITS 17249,to pass the filter checkbox,table name,row id values
                oCmdElement = oMessageElement.XPathSelectElement("./Document/form/chkFilter");
                if (oCmdElement != null)
                {
                    oCmdElement.Value = Riskmaster.Common.Conversion.ConvertBoolToInt(chkFilter.Checked).ToString();
                }
                if (hd_rowid != null && hd_rowid.Value != "")
                {
                    oCmdElement = oMessageElement.XPathSelectElement("./Document/form/RowId");
                    if (oCmdElement != null)
                    {
                        oCmdElement.Value = hd_rowid.Value;
                    }
                }
                if (hd_policydate != null && hd_policydate.Value != "")
                {
                    oCmdElement = oMessageElement.XPathSelectElement("./Document/form/PolicyDate");
                    if (oCmdElement != null)
                    {
                        oCmdElement.Value = hd_policydate.Value;
                    }
                }
                if (hd_tablename != null && hd_tablename.Value != "")
                {
                    oCmdElement = oMessageElement.XPathSelectElement("./Document/form/tablename");
                    if (oCmdElement != null)
                    {
                        oCmdElement.Value = hd_tablename.Value;
                    }
                }
                if (hd_eventdate != null && hd_eventdate.Value != "")
                {
                    oCmdElement = oMessageElement.XPathSelectElement("./Document/form/EventDate");
                    if (oCmdElement != null)
                    {
                        oCmdElement.Value = hd_eventdate.Value;
                    }
                }
                if (hd_claimdate != null && hd_claimdate.Value != "")
                {
                    oCmdElement = oMessageElement.XPathSelectElement("./Document/form/ClaimDate");
                    if (oCmdElement != null)
                    {
                        oCmdElement.Value = hd_claimdate.Value;
                    }
                }
                //End by Shivendu for MITS 17249,to pass the filter checkbox,table name,row id values

                //Call WCF wrapper for cws
                CommonFunctions.CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                //Binding Error Control
                ErrorControl.errorDom = sReturn;

                XElement oMessageRespElement = XElement.Parse(oFDMPageDom.OuterXml);
                XElement oChildEle = oMessageRespElement.XPathSelectElement("./form/RetVal");
                if (oChildEle != null)
                {
                    if (oChildEle.Attribute("count") != null)
                    {
                        int iCounter = Riskmaster.Common.Conversion.ConvertStrToInteger(oChildEle.Attribute("count").Value.ToString()) + 1;
                        for (int i = 1; i <= iCounter; i++)
                        {
                            oCmdElement = oMessageElement.XPathSelectElement("./Document/form/childnodecounter");
                            if (oCmdElement != null)
                            {
                                oCmdElement.Value = i.ToString();
                            }

                            //Call WCF wrapper for cws
                            CommonFunctions.CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                            //Binding Error Control
                            ErrorControl.errorDom = sReturn;

                            oMessageRespElement = XElement.Parse(oFDMPageDom.OuterXml);
                            oChildEle = oMessageRespElement.XPathSelectElement("./form/RetVal");
                            if (oChildEle != null)
                            {
                                populateChildNodes(e.Node, oChildEle.Value);
                            }
                        }
                    }
                    else
                    {
                        populateChildNodes(e.Node, oChildEle.Value);
                    }
                }
                cmb_dlevel.BackColor = Color.BlanchedAlmond;
            
        }
        /// <summary>
        /// populate all the child nodes for the current node
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        void populateChildNodes(TreeNode oParentNode, string p_sReturnVal)
        {
            string sFuncIDNames = p_sReturnVal;
            if (sFuncIDNames.Length < 2 * m_sNameDelimiter.Length)
            {
                //No Child Nodes are present.. hence collapse this node.. also remove "+" sign
                //		oParentNode.collapse();
                return;
            }

            var arFuncIDNames = sFuncIDNames.Split(m_sNameDelimiter, StringSplitOptions.RemoveEmptyEntries);
            var arFuncIDs = arFuncIDNames[0].Split(m_sIdDelimiter);
            var arFuncNames = arFuncIDNames[1].Split(m_sSecondLevelDelimiter, StringSplitOptions.RemoveEmptyEntries);
            var s = "";
            int id = 1;
            string sCode = "";
            bool bHasChild = false;
            string sName = "";


            for (var i = 0; i < arFuncIDs.Length; i++)
            {
                var arIdParts = arFuncIDs[i].Split(m_sInsideIdDelimiter);
                id = Riskmaster.Common.Conversion.ConvertStrToInteger(arIdParts[0]);

                bHasChild = false;
                if (Riskmaster.Common.Conversion.ConvertStrToInteger(arIdParts[1]) > 0)
                    bHasChild = true;

                if (arIdParts.Length > 2)
                    sCode = arIdParts[2];

                sName = arFuncNames[i];

                AddChildNode(oParentNode, id, sName, sCode, bHasChild);
            }

        }
        /// <summary>
        /// This function adds a child node to parent
        /// </summary>
        /// <param name="oParentNode"></param>
        /// <param name="p_Id"></param>
        /// <param name="p_sName"></param>
        /// <param name="p_sCode"></param>
        /// <param name="p_bHasChild"></param>
        protected void AddChildNode(TreeNode oParentNode, int p_Id, string p_sName, string p_sCode, bool p_bHasChild)
        {
            TreeNode childNode = null;
            try
            {
                childNode = new TreeNode(p_sName, p_Id.ToString());
                if (p_bHasChild)
                {
                    childNode.PopulateOnDemand = true;
                }
                else
                {
                    childNode.PopulateOnDemand = false;
                }
                childNode.Target = "_self";

                //Nodes Depth would be compared to the Org Level Selected 
                //This information is already present in viewstate
                UpdateSelectableLevels(childNode, (oParentNode.Depth + 1));
                
                oParentNode.ChildNodes.Add(childNode);

            }
            catch (Exception e)
            {
            }
        }
        /// <summary>
        /// This function adds a child node to parent
        /// </summary>
        /// <param name="oParentNode"></param>
        /// <param name="p_Id"></param>
        /// <param name="p_sName"></param>
        /// <param name="p_sCode"></param>
        /// <param name="p_bHasChild"></param>
        protected TreeNode AddChildNode(TreeNode oParentNode, long p_Id, string p_sName)
        {
            TreeNode childNode = null;
            try
            {
                childNode = new TreeNode(p_sName, p_Id.ToString());
                oParentNode.ChildNodes.Add(childNode);
               // oParentNode.Expand();

            }
            catch (Exception e)
            {
            }
            return childNode;
        }
        /// <summary>
        /// Update All Tree Bindings to display data
        /// </summary>
        private void UpdateTreeBindings()
        {
            string NewXmlText = oFDMPageDom.SelectSingleNode("//org").OuterXml;
            
            //Clearing All nodes
            OTV.Nodes.Clear();
            
            //Adding root node
            TreeNode oRootNode = new TreeNode("Organization Hierarchy", "");
            OTV.Nodes.Add(oRootNode);
            
            XElement oOrgEle = XElement.Parse(NewXmlText);

            PopulateChildNodes(OTV.Nodes[0], oOrgEle);
            
        }

        private void PopulateChildNodes(TreeNode oParentNode, XElement oParentEle)
        {
            if (oParentEle.Elements().Count() == 0 && oParentNode.Depth != ((int)CommonFunctions.enumOrgHierarchyLevels.DEPARTMENT) && oParentNode.Depth >= CommonFunctions.GetLevel(cmb_dlevel.SelectedValue))
            {
                oParentNode.PopulateOnDemand = true;
            }
            else
            {
                oParentNode.PopulateOnDemand = false;
                oParentNode.Expanded = true;
            }
            
            //Iterating in All Child Nodes
            foreach (XElement oChildEle in oParentEle.Elements())
            {
                long lId = 0;
                string sName = "";
                string sCode = "";
                bool bHasChild = false;

                lId = Riskmaster.Common.Conversion.ConvertStrToLong(oChildEle.Attribute("id").Value);
                sName = oChildEle.Attribute("name").Value;

                //Create the current client node
                TreeNode oChildNode = AddChildNode(oParentNode, lId, sName);
                if (oChildNode != null)
                {
                    PopulateChildNodes(oChildNode, oChildEle);
                }
            }
        }
        
        /// <summary>
        /// Updates the style for selectable nodes
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="p_iCurrentNodeLevel"></param>
        private void UpdateSelectableLevels(TreeNode oNode , int p_iCurrentNodeLevel)
        {
            if (ViewState["SelectableLevel"] != null)
            {
                //Check for "ALL" condition
                if (ViewState["SelectableLevel"].ToString().ToLower() == "all")
                {
                    if (p_iCurrentNodeLevel != 0)
                    {
                        oNode.SelectAction = TreeNodeSelectAction.Select;
                        OTV.LevelStyles[p_iCurrentNodeLevel].Font.Bold = true;
                        OTV.LevelStyles[p_iCurrentNodeLevel].BackColor = Color.LightBlue;
                    }
                }
                else
                {
                    int iSelectableLevel = Riskmaster.Common.Conversion.ConvertStrToInteger(ViewState["SelectableLevel"].ToString());
                    if (iSelectableLevel == p_iCurrentNodeLevel)
                    {
                        oNode.SelectAction = TreeNodeSelectAction.Select;
                        OTV.LevelStyles[p_iCurrentNodeLevel].Font.Bold = true;
                        OTV.LevelStyles[p_iCurrentNodeLevel].BackColor = Color.LightBlue;
                    }
                    else
                    {
                        oNode.SelectAction = TreeNodeSelectAction.Select;
                    }
                }
            }
            else
            {
                oNode.SelectAction = TreeNodeSelectAction.Select;
            }

            if (ViewState["SearchLevel"] != null)
            {
                int iSelectableLevel = Riskmaster.Common.Conversion.ConvertStrToInteger(ViewState["SearchLevel"].ToString());
                if (iSelectableLevel == p_iCurrentNodeLevel && p_iCurrentNodeLevel != 0)
                {
                    oNode.SelectAction = TreeNodeSelectAction.Select;
                    OTV.LevelStyles[p_iCurrentNodeLevel].Font.Bold = true;
                    OTV.LevelStyles[p_iCurrentNodeLevel].BackColor = Color.LightBlue;
                }
                else
                {
                    oNode.SelectAction = TreeNodeSelectAction.Select;
                }
                
            }


        }
    }
}