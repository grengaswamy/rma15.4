﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Riskmaster.UI.UI.Shared.Controls
{
    public partial class UploadFile : System.Web.UI.UserControl
    {
        #region Properties

        private short m_stabIndex;
        public short TabIndex
        {
            get
            {
                return m_stabIndex;
            }
            set
            {
                m_stabIndex = value;
                txtFileUpload.TabIndex = (short)(value++);
                btnUpload.TabIndex = (short)(value++);
            }
        }

        private string m_sButtonText;
        public string ButtonText
        {
            get
            {
                return btnUpload.Text;
            }
            set
            {
                btnUpload.Text = value;
            }
        }

        public Stream FileStreamData
        {
            get
            {
                return (Stream)ViewState["FileStreamData"];
            }
            set
            {
                ViewState["FileStreamData"] = value;
            }
        }

        //private string m_PostedFile;
        public string FilePath
        {
            get
            {
                return (string)ViewState["FilePath"];
            }
            set
            {
                ViewState["FilePath"] = value;
                txtFileUpload.Text = value;
            }
        }

        public string FileName
        {
            get
            {
                return txtFileUpload.Text.Substring(txtFileUpload.Text.LastIndexOf("\\") + 1);
            }
        }

        public bool Enabled
        {
            get
            {
                return txtFileUpload.Enabled;
            }
            set
            {
                txtFileUpload.Enabled = value;
                btnUpload.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return txtFileUpload.Visible;
            }
            set
            {
                txtFileUpload.Visible = value;
                btnUpload.Visible = value;
            }
        }

        public bool HasFile
        {
            get
            {
                 if ((string)ViewState["FilePath"] != string.Empty) 
                 { 
                    if (File.Exists((string)ViewState["FilePath"]))
                        return true;
                    else
                        return false;
                }
                return false;
            }
        }
     
        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                using(FileStream fs = new FileStream(txtFileUpload.Text, FileMode.Open, FileAccess.Read))
                {
                    // Create a byte array of file stream length
                    byte[] ImageData = new byte[fs.Length];
                    
                    //Read block of bytes from stream into the byte array
                    fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

                    //using (MemoryStream stream = new MemoryStream())
                    MemoryStream stream = new MemoryStream();
                        stream.Write(ImageData, 0, System.Convert.ToInt32(fs.Length));
                        FileStreamData = stream;

                        FilePath = txtFileUpload.Text;
                    
                    //Close the File Stream
                    fs.Close();
                }
            }
            catch (Exception excep)
            {

            }
        }
    }
}