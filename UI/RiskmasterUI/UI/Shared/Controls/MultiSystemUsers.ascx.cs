﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.UI.Shared.Controls
{
    /// <summary>
    /// Neha goel MITS# 34287 Swiss re gap 29 Restricted claims, control defined for the multi user system wide control
    /// </summary>
    [ValidationProperty("CodeId")]
    public partial class MultiSystemUsers : System.Web.UI.UserControl
    {
        //private string m_sCodeTable;
        //public string CodeTable
        //{
           // get
            //{
                //return m_sCodeTable;
            //}
            //set
            //{
                //m_sCodeTable = value;
            //}
       // }

        private string m_sRMXRef;
        public string RMXRef
        {
            set
            {
                m_sRMXRef = value;                
            }
        }

        private string m_sControlName;
        public string ControlName
        {
            get
            {
                return m_sControlName;
            }
            set
            {
                m_sControlName = value;
            }
        }

        private string m_sParentNode;
        public string ParentNode
        {
            get
            {
                return m_sParentNode;
            }
            set
            {
                m_sParentNode = value;
            }
        }

        private bool m_bRequired = false;
        public bool Required
        {
            get
            {
                return m_bRequired;
            }
            set
            {
                m_bRequired = value;
            }
        }
        private string m_sValidationGroup = string.Empty;
        public string ValidationGroup
        {
            get
            {
                return m_sValidationGroup;
            }
            set
            {
                m_sValidationGroup = value;
            }
        }
        private string m_sCodeId;
        public string CodeId
        {
            get
            {
                m_sCodeId = UserId.Text;
                return m_sCodeId;
            }
            set
            {
                m_sCodeId = value;
                lstUsers.Text = m_sCodeId;               
            }
        }
        private string m_sCodeText;
        public string CodeText
        {
            get
            {
                m_sCodeText = lstUsers.Text;
                return m_sCodeText;               
            }
            set
            {
                m_sCodeText = value;
                UserId.Text = value;
            }
        }

        private string m_sPageName;
        public string PageName
        {
            get
            {
                return m_sPageName;
            }
            set
            {
                m_sPageName = value;
            }
        }

        private string m_sTabIndex;
        public string TabIndex
        {
            get
            {
                return m_sTabIndex;
            }
            set
            {
                m_sTabIndex = value;
            }
        }
        
        // Public Property exposed to disable/enable the UserControl
        public bool Enabled
        {
            set
            {
                if (value == true)
                {
                    lstUsers.Enabled = true;
                    lstUsers.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ffffff");                    
                    cmdAddCustomizedListUser.Attributes.Add("style", "DISPLAY: inline");
                    cmdDelCustomizedListUser.Attributes.Add("style", "DISPLAY: inline");
                }
                else
                {
                    lstUsers.Enabled = false;
                    lstUsers.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");                    
                    cmdAddCustomizedListUser.Attributes.Add("style", "DISPLAY: none");
                    cmdDelCustomizedListUser.Attributes.Add("style", "DISPLAY: none");
                }

                
            }
        }

        public string ListCodes
        {
            get
            {
                return UserId.Text;
            }

        }

        public string AddListCodes
        {
            set
            {
                UserId.Text = UserId.Text + " " + value;
            }
        }


        public ListItem AddItems
        {
            set
            {
                lstUsers.Items.Add(value);
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(m_sRMXRef))
                {
                    lstUsers.Attributes.Add("RMXRef", m_sRMXRef);
                    UserId.Attributes.Add("RMXRef", m_sRMXRef + "/@codeid");
                    UserName.Attributes.Add("RMXRef", m_sRMXRef);                    
                }
                if (!string.IsNullOrEmpty(m_sCodeId))
                {
                     UserId.Text = m_sCodeId;
                }
                if (!string.IsNullOrEmpty(m_sCodeText))
                {
                    lstUsers.Text = m_sCodeText;
                } 

                //Code Added for TabIndex - RMA - 4691
                if (!string.IsNullOrEmpty(TabIndex))
                {
                    lstUsers.TabIndex = Convert.ToInt16(TabIndex);
                    cmdAddCustomizedListUser.Attributes.Add("tabindex", (Convert.ToInt32(TabIndex) + 1).ToString());
                    cmdDelCustomizedListUser.Attributes.Add("tabindex", (Convert.ToInt32(TabIndex) + 2).ToString());
                }
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            cmdAddCustomizedListUser.Attributes["onclick"] = " AddCustomizedListUser('"+ m_sParentNode +"','" + ControlName + "_lstUsers', '" + ControlName + "_UserId','" + ControlName + "_UserName')";
            cmdDelCustomizedListUser.Attributes["onclick"] = " DelCustomizedListUser('" + m_sParentNode + "','" + ControlName + "_lstUsers', '" + ControlName + "_UserId','" + ControlName + "_UserName')";
        }        
    }
}