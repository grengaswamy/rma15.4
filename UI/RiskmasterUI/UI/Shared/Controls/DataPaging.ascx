﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataPaging.ascx.cs" Inherits="Riskmaster.UI.Shared.Controls.DataPaging" %>
<script type="text/javascript">
    function Linkbutton(commandName, obj) {
        //debugger;
        var cntrlId = obj.id.toString();
        cntrlId = cntrlId.substr(0, cntrlId.lastIndexOf("_") + 1);
        var CurrentPageId = cntrlId + "hdCurrentPage";
        var TotalPagesId = cntrlId + "hdTotalPages";
        //var iTotalRows = Conversion.ConvertObjToInt(hdTotalRows.Value);
        var cntrl = document.getElementById(CurrentPageId);
        var cntrl2 = document.getElementById(TotalPagesId);
        var value = parseInt(cntrl.value);
        switch (commandName.toUpperCase()) {
            case "NEXT":
                if (cntrl.value != cntrl2.value) {
                    cntrl.value = value + 1;
                }
                break;
            case "PREV":
                if (value > 1) {
                    cntrl.value = value - 1;
                }
                break;
            case "FIRST":
                cntrl.value = 1;
                break;
            case "LAST":
                cntrl.value = cntrl2.value;
                break;
        }

    }
</script>
<table>
<tr>
    <td>
        Page <asp:Label ID="lblCurrentPage" runat="server" Text="1"> </asp:Label>&nbsp;of <asp:Label ID="lblTotalPage" runat="server" Text="20"></asp:Label>&nbsp;&nbsp;
    <asp:LinkButton id="lnkFirst"   ForeColor="black" runat="server"  OnClientClick="javascript:Linkbutton('FIRST',this);" OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> 
    <asp:LinkButton id="lnkPrev" runat="server" ForeColor="black" OnClientClick="javascript:Linkbutton('PREV',this);" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNext" ForeColor="black" runat="server" OnClientClick="javascript:Linkbutton('NEXT',this);" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLast" ForeColor="black" runat="server" OnClientClick="javascript:Linkbutton('LAST',this);" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
    
    <asp:TextBox ID="hdCurrentPage" runat="server" Text="1" rmxignoreget="true" style="display:none"   ></asp:TextBox>
    <asp:TextBox ID="hdPageSize" runat="server" rmxignoreget="true"  style="display:none" ></asp:TextBox>
    <asp:TextBox ID="hdTotalPages" runat="server" style="display:none" ></asp:TextBox>
    <%--<asp:HiddenField ID="hdCurrentPage" runat="server"/>
    <asp:HiddenField ID="hdPageSize" runat="server"/>--%>
    <asp:HiddenField ID="hdTotalRows" runat="server"/>
    </td>
</tr>
</table>
