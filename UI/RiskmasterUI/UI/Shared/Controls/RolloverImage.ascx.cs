﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.Shared.Controls
{
    public partial class RolloverImage : System.Web.UI.UserControl
    {
        //Protected WithEvents hlImage As System.Web.UI.WebControls.HyperLink
        //Protected WithEvents imgImage As System.Web.UI.WebControls.Image

        string _NormalImage= String.Empty;
        public string NormalImageSrc
        {
            get {return _NormalImage;}
            set {_NormalImage = value;}
        }

        string _RolloverImage = String.Empty;
        public string RolloverImageSrc
        {
            get { return _RolloverImage; }
            set { _RolloverImage = value; }
        }

        string _LinkURL = String.Empty;
        public string LinkURL
        {
            get { return _LinkURL; }
            set { _LinkURL = value; }
        }

        string _StatusText = String.Empty;
        public string StatusText
        {
            get { return _StatusText; }
            set { _StatusText = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string OverEvent = String.Empty;
            string OutEvent = String.Empty;

            //mouseover javascript
            OverEvent = imgImage.ClientID + ".src='" + _RolloverImage + "'; window.status='" + _StatusText + "'; return true;";

            //mouseout javascript
            OutEvent = imgImage.ClientID + ".src='" + _NormalImage + "'; window.status=' '; return true;";

            imgImage.ImageUrl = _NormalImage;
            imgImage.Attributes.Add("name", imgImage.ClientID);

            hlImage.NavigateUrl = _LinkURL;
            hlImage.Attributes.Add("onMouseOver", OverEvent);
            hlImage.Attributes.Add("onMouseOut", OutEvent);
        }
    }
}