﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riskmaster.UI.Shared.Controls
{
    public partial class ErrorControl : System.Web.UI.UserControl
    {
        private bool m_ErrorOccured = false;

        /// <summary>
        /// Gets and sets the Text property
        /// for the embedded Label control
        /// </summary>
        public string Text
        {
            get
            {
                return lblError.Text;
            }//get
            set
            {
                lblError.Text = value;
            }//set
        }//property: Text

        /// <summary>
        /// Display an Error message MITS 24745
        /// </summary>
        /// <param name="sErrorMessage"></param>
        public void DisplayError(string sErrorMessage)
        {
            string sError = string.Empty;
            sError = @"<font class='errortext'>Following warning(s) reported:</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                                          "<tr>" +
                                            "<td valign='top'>" +
                                              "<img src='/RiskmasterUI/Images/error-large.gif'/>" +
                                            "</td>" +
                                            "<td valign='top' style='padding-left: 1em'>" +
                                              "<ul>" + ErrorHelper.UpdateErrorMessage(sErrorMessage) +
                                               "</ul></td></tr></table>";
            lblError.Text = sError;
        }

        public string errorDom
        {
            set
            {
                lblError.Text = string.Empty;
                XmlDocument objDoc = new XmlDocument();
                //apeykov RMA-9719 Start
                //if (! string.IsNullOrEmpty(value.Trim()))
                if (value != null && value.Trim() != "")
                //apeykov RMA-9719 End
                {
                    objDoc.LoadXml(value.ToString());
                    //Explicity remove Error as TempData is stored in session 

                    if ((objDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
                    {
                        if (objDoc.SelectSingleNode("//ExtendedMsgType") != null)
                        {
                            if ((objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "Warning"))
                            {
                                errorhandle = @"<font class='errortext'>Following warning(s) reported:</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                                          "<tr>" +
                                            "<td valign='top'>" +
                                              "<img src='/RiskmasterUI/Images/error-large.gif'/>" +
                                            "</td>" +
                                            "<td valign='top' style='padding-left: 1em'>" +
                                              "<ul>";
                                foreach (XmlNode node in objDoc.SelectNodes("//ExtendedStatus"))
                                {
                                    if ((node.SelectSingleNode("ExtendedMsgType").InnerText == "Warning"))
                                    {
                                        errorhandle = String.Format("{0}<li class='warntext'>{1}</li>", errorhandle, ErrorHelper.UpdateErrorMessage(node.SelectSingleNode("ExtendedStatusDesc").InnerText));
                                    }
                                }
                                errorhandle += "</td></tr></table>";

                            }
                        }
                    }
                    else
                    {

                        if (objDoc.SelectSingleNode("//ExtendedMsgType") != null)
                        {
                            if ((objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "Message") ||
                                (objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "Error") ||
                                (objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "SystemError"))
                            {

                                errorhandle = @"<font class='errortext'>Following Errors have been reported:</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                                          "<tr>" +
                                            "<td valign='top'>" +
                                              "<img src='/RiskmasterUI/Images/error-large.gif'/>" +
                                            "</td>" +
                                            "<td valign='top' style='padding-left: 1em'>";
                                //+"<ul>"; RMA-6360
                                foreach (XmlNode node in objDoc.SelectNodes("//ExtendedStatus"))
                                {
                                    if ((node.SelectSingleNode("ExtendedMsgType").InnerText == "Error") ||
                                        (node.SelectSingleNode("ExtendedMsgType").InnerText == "Message") ||
                                        (node.SelectSingleNode("//ExtendedMsgType").InnerText == "SystemError"))
                                    {
                                        errorhandle = String.Format("{0}<li class='warntext'>{1}</li>", errorhandle, ErrorHelper.UpdateErrorMessage(node.SelectSingleNode("ExtendedStatusDesc").InnerText));
                                    }
                                }
                                errorhandle += "</td></tr></table>";
                                m_ErrorOccured = true;

                            }
                            else
                            {
                                if ((objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "PopupMessage"))
                                {
                                    errorhandle = "<script language='javascript'>alert('";
                                    foreach (XmlNode node in objDoc.SelectNodes("//ExtendedStatus/ExtendedMsgType[.='PopupMessage']/parent::node()"))
                                    {
                                        //if ((node.SelectSingleNode("ExtendedMsgType").InnerText == "Warning"))
                                        //{
                                            errorhandle = errorhandle + ErrorHelper.UpdateErrorMessage(node.SelectSingleNode("ExtendedStatusDesc").InnerText) + @"\n";
                                        //}
                                    }
                                    errorhandle += "')</script>";
                                }
                            }
                        }
                    }
                    lblError.Text = errorhandle;
                }
            }
        }

        public string errorhandle = "";
        public bool errorFlag
        {
            get
            {
                return(m_ErrorOccured);
            }
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //Start Explicitly setting the errorDom incase of CWS errors
            if (HttpContext.Current.Items["Error"] != null && HttpContext.Current.Items["Error"].ToString() != "")
            {
                errorDom = HttpContext.Current.Items["Error"].ToString();
                HttpContext.Current.Items["Error"] = null;
            }
            //End Explicitly setting the errorDom incase of CWS errors

        }
    }
}

