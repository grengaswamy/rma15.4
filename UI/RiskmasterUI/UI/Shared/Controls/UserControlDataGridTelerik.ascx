﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserControlDataGridTelerik.ascx.cs" Inherits="Riskmaster.UI.Shared.Controls.UserControlDataGridTelerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<table id="UserControl" width="100%">
<tr>
    <td>
        <asp:Label ID="lblGridTitle" runat="server" Font-Bold="True" 
            Font-Names="Tahoma" Font-Size="Small"></asp:Label><br />
    </td>
</tr>
<tr valign="top">
    <td width="93%">
        <div id="GridDiv" runat="server" style="overflow:auto" >
            <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
                    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
                    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
                        <script type="text/javascript">

                            function OnRowClick(sender, e) {
                            
                                var row = $find('<%=gvData.ClientID %>').get_masterTableView().get_dataItems()[e._itemIndexHierarchical];
                                     setControlValues(row);
                                return false;
                            }

                            function GetSelectedRow(rowindex, rdbtn) {
                                var i, obj;
                                checkUnCheckRadio(rdbtn);
                                var row = $find('<%=gvData.ClientID %>').get_masterTableView().get_dataItems()[rowindex];
                                setControlValues(row);

                            }

                            function setControlValues(row) {
                                if (document.getElementById("AdjusterScreensSelectedId") != null) {
                                    document.getElementById("AdjusterScreensSelectedId").value = $find('AdjusterScreensGrid_gvData').get_masterTableView().getCellByColumnUniqueName(row, "UniqueId").innerHTML;//innerText; RMA-15551-msampathkuma
                                }
                                else if (document.getElementById("MergeEmailDetailSelectedId") != null) {
                                    document.getElementById("MergeEmailDetailSelectedId").value = $find('MergeEmailDetailGrid_gvData').get_masterTableView().getCellByColumnUniqueName(row, "UniqueId").innerHTML;//innerText;
                                }
                            }
                            
            </script>
                    </telerik:RadCodeBlock>
                         <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="gvData">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="gvData" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
            <%--Pradyumna 11/17/2014 MITS 36930: Added OnPreRender event to allow column resize --%>        
                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                        <telerik:RadGrid runat="server" ID="gvData" AutoGenerateColumns="false" OnSortCommand="gvData_SortCommand" OnDataBound="gvData_DataBound"
                            PageSize="10" OnItemDataBound="gvData_ItemDataBound"  AllowSorting="True" AllowPaging="True"  OnNeedDataSource="gvData_NeedDataSource"
                             GridLines="None" OnPreRender="gvData_PreRender" 
                            Skin="Office2007"  EnableViewState="true" >
                       
                            <SelectedItemStyle CssClass="SelectedItem"  />

                            <ClientSettings AllowKeyboardNavigation="true" >                    
                                <ClientEvents OnRowSelected="OnRowClick" />
                                <Scrolling SaveScrollPosition="true"  AllowScroll="false" />
                                
                                <Selecting AllowRowSelect ="true" />
                            </ClientSettings>
                            <MasterTableView  >
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn >
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                   <telerik:GridClientSelectColumn UniqueName="Select" >
                                      
                                    </telerik:GridClientSelectColumn >
                                       <telerik:GridTemplateColumn  HeaderText="TextColumn" >
                <ItemTemplate>
                    <asp:HiddenField  id="hfGrid" runat="server" Value='<%# TextColumn != null && TextColumn != "" ? Eval(TextColumn):"" %>' />
                    </ItemTemplate>
               </telerik:GridTemplateColumn>
                                </Columns>
                                <ItemStyle CssClass="datatd1" />
                                <AlternatingItemStyle CssClass="datatd" />
                                <HeaderStyle CssClass="msgheader" />
                               

                            </MasterTableView>
                          <%--  <HeaderStyle CssClass="msgheader" />--%>
                            <PagerStyle  Mode="NextPrevAndNumeric" Position="Top"  AlwaysVisible ="true" />
                            <FilterMenu >
                               <CollapseAnimation  Duration="10"/> 
                            </FilterMenu>
                        </telerik:RadGrid>
                    </telerik:RadAjaxPanel>
            <asp:HiddenField ID="hdListHead" runat="server" />
        </div>
    </td>
    <td width="2%" >
        <asp:TextBox id="hdSelectedID" style="display:none" runat ="server" Text=""/>
        <asp:TextBox id="OtherParams" style="display:none" runat ="server" Text=""/>
        <asp:ImageButton ID="New" runat="server" ToolTip="New" 
            ImageUrl="~/Images/tb_new_mo.png" ImageAlign="Middle"/>
        <asp:ImageButton ID="Edit" runat="server" ToolTip="Edit" 
            ImageUrl="~/Images/tb_edit_mo.png" ImageAlign="Middle"/>
        <asp:ImageButton ID="Clone" runat="server" ToolTip="Clone" 
            ImageUrl="~/Images/tb_clone_mo.png" ImageAlign="Middle" Visible="false"/>
        <asp:ImageButton ID="Delete" runat="server" ToolTip="Delete" 
            ImageUrl="~/Images/tb_delete_mo.png" ImageAlign="Middle"/>
        </td>
</tr>
</table>