﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiCodeLookUp.ascx.cs" Inherits="Riskmaster.UI.Shared.Controls.MultiCodeLookUp" %>
<%--mgaba2:MITS 23158: Multi code fields overlaps other fields if item size is big enough to accommodate--%>
<asp:listbox  ToolTip="" id="multicode" runat="server" SelectionMode ="Multiple" Rows ="3" RMXType=""  Style="width:75%" ></asp:listbox><asp:Button ID="multicodebtn"  class="CodeLookupControl"  runat="server"/><asp:Button ID="multicodebtndel" class="BtnRemove" Text="-"  runat="server"/><%--abansal23 MITS 15876 on 06/23/2009--%>
<asp:TextBox id="multicode_lst" style="display:none" runat ="server" RMXType=""/>
<asp:TextBox id="multicode_maxsize" style="display:none" runat ="server"/>
