﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EntitySSNCheck.aspx.cs"
    Inherits="Riskmaster.UI.UI.SupportScreens.EntitySSNCheck" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Duplicate SSN Check</title>    
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function SetCloseWindow() 
        {           
            document.forms[0].FormMode.value = "close";
            return true;
        }

        function checkWindow() 
        {
            if (document.forms[0].HdnDupExists.value == "0" || document.forms[0].FormMode.value == "close") 
            {                
                 window.opener.document.forms[0].DupSSN.value = "AfterIgnore";
                 setParentWindowValues();
                 document.forms[0].FormMode.value = "";
                 window.close();
            }         
        }

        function setParentWindowValues() 
        {            
            window.opener.m_DataChanged = true;
            window.opener.document.forms[0].SysCmd.value = "9";
            window.opener.document.forms[0].submit();
        }
        
        function fnCancelDupSSN() {
            if (window.opener.document.forms[0].DupSSN != null)
            {
            window.opener.document.forms[0].DupSSN.value = "";
            }
            window.opener.m_DataChanged = true;
            window.close();
        }
    </script>

</head>
<body class="10pt" onload="checkWindow();">
    <form id="frmData" runat="server">
     <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <div class="formtitle" id="formtitle">
                    <%--Yukti-ML Changes Start MITS 34218--%>
                        <%--Warning...</div>--%>
                    <asp:Label runat="server" ID="lblWarning" Text="<%$ Resources:lblWarning %>" /> </div>
                    <%--Yukti-ML Changes End MITS 34218--%>
                </td>
            </tr>
            <tr>
                <td class="formsubtitle">
                <%--Yukti-ML Changes Start MITS 34218--%>
                    <%--<asp:Label runat="server" class="label" ID="lbl_TaxId"  /> is not unique TaxId/SSN. It is in use by another person/entity.</td>--%>
                    <asp:Label runat="server" class="label" ID="lbl_TaxId"  /><asp:Label runat="server" ID="lblUniqueTaxId" Text="<%$ Resources:lblUniqueTaxId %>"/></td>
                <%--Yukti-ML Changes End MITS 34218--%>
            </tr>
            <tr>                
                <td>
                <%--Yukti-ML Changes Start MITS 34218--%>
                   <%-- Conflicting People/Entities:--%>
                   <asp:Label runat="server" ID="lblConflictPeople" Text="<%$ Resources:lblConflictPeople %>" />
                   <%--Yukti-ML Changes End MITS 34218--%>
                    <br></br>
                </td>
            </tr>
            <tr>
            <td>
            <asp:ListBox runat="server" ID="cboEntities" rmxref="/Instance/Document/Document/temp" ItemSetRef="/Instance/Document/Entities" Width="95%" Rows="10" AutoPostBack ="false" EnableViewState ="false"/>
            </td>
            </tr>
            <tr>
      <td colspan="2" align="center">
      <%--Yukti-ML Changes Start MITS 34218--%>
        <%--<asp:Button ID="btnIgnore" runat="server" type="submit" Text="Ignore" 
              class="button" OnClientClick ="return SetCloseWindow();" />              
        <asp:Button ID="btnCancel" runat="server" type="submit" Text="Cancel" class="button" OnClientClick="return fnCancelDupSSN();" />--%>
        <asp:Button ID="btnIgnore" runat="server" type="submit" Text="<%$ Resources:btnIgnore %>" 
              class="button" OnClientClick ="return SetCloseWindow();" />              
        <asp:Button ID="btnCancel" runat="server" type="submit" Text="<%$ Resources:btnCancel %>" class="button" OnClientClick="return fnCancelDupSSN();" />
        <%--Yukti-ML Changes End MITS 34218--%>
      </td>
     </tr>      
     <tr>
     <td>
     <%--Yukti-ML Changes Start MITS 34218--%>
     <%--<asp:CheckBox runat="server" appearance="full" id="chkIgnoreForFuture" RMXRef="/Instance/Document/IgnoreForFuture" Text="Ignore Future Tax Id conflict warnings" />--%>
     <asp:CheckBox runat="server" appearance="full" id="chkIgnoreForFuture" RMXRef="/Instance/Document/IgnoreForFuture" Text="<%$ Resources:chkIgnoreTaxId %>" />
     <%--Yukti-ML Changes End MITS 34218--%>
     </td>
     </tr>
        </table>
    </div>
     <asp:TextBox style="display:none" runat="server" id="HdnTaxId" RMXRef="" RMXType="hidden" />       
     <asp:TextBox style="display:none" runat="server" id="HdnEntityId" RMXRef="" RMXType="hidden"/>       
     <asp:TextBox style="display:none" runat="server" id="FormMode" RMXRef="" RMXType="hidden"/>    
     <asp:TextBox style= "display:none" runat="server" id="HdnDupExists" RMXRef="//Entities/@dupexists" RMXType="hidden"/>
    </form>
</body>
</html>
