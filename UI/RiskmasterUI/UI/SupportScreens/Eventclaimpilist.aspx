﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Eventclaimpilist.aspx.cs" Inherits="Riskmaster.UI.UI.SupportScreens.Eventclaimpilist" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Choose Employee Involved for Claim</title>
     <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript">
        function selRecord(lPiEid , lPiRowId) {
            //Raman 12/07/2009
            //Usa FDM claimwc page to bind existing person involved data instead of passing on the buck to Search
            
//            window.opener.lookupCallback = "entitySelected";
//            window.opener.m_LookupType = 4;
//            window.opener.m_AllowRestore = false;
//            window.opener.m_sFieldName = "emp";

            var sUrl = window.opener.location.href + "&SysExPiRowId=" + lPiRowId;
            window.opener.pleaseWait.Show();
            window.opener.location.href = sUrl;
            window.close();
            
            
//            
//            if (window.opener.lookupCallback != null) {
//                window.opener.entitySelected(lPiEid);
//                if (window.opener.document.getElementById('SysFormPiRowId') != null) {
//                    window.opener.document.getElementById('SysFormPiRowId').value = lPiRowId;
//                }
//                if (window.opener.document.getElementById('pirowid') != null) {
//                    window.opener.document.getElementById('pirowid').value = lPiRowId;
//                }
//                window.close();
//                return false;
//            }
            
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div runat="server" id="dv" style="width:100%;overflow:auto" >
        
    <asp:HiddenField runat="server" ID="hdcaller" />
    <asp:HiddenField runat="server" ID="eventid" />
    <asp:HiddenField runat="server" ID="picount" />
    <%if (picount.Value != "0") { %>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
						
						<tr>
							<td class="ctrlgroup">Choose Existing Employee Involved for Claim</td>
						</tr>
						
						<% int i = 0; foreach (XElement item in result)
         {
             if (item.Element("PiTypeCode").Attribute("codeid").Value == "237")
             {%>
							
							
								<tr>
									<td><%
                                            lnkPiEid.ID = "lnkPiEid" + i;
                                            lnkPiEid.Text = item.Element("PiEid").Value;
                                            lnkPiEid.OnClientClick = "return selRecord(" + item.Element("PiEid").Attribute("codeid").Value + " , " + item.Element("PiRowId").Value + ")";%>
                              
                             <asp:LinkButton runat="server" id="lnkPiEid"></asp:LinkButton>  
									
										
										
									</td>
								</tr>
							
						<%}
         } %>
						<tr><td/></tr>
						<tr><td/></tr>
						<tr>
							<td class="ctrlgroup"></td>
						</tr>

						<tr><td/></tr>
						<tr><td><i>or</i></td></tr>
						<tr><td/></tr>
						
						<tr>
							<td>
								<asp:LinkButton runat="server" id="lnkOtherEmployee" Text="(Enter or Select Other Employee on Claim Screen)" OnClientClick="window.close();return false;"></asp:LinkButton>  
							</td>
						</tr>
						
					</table>
					<%} %>
					 <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
					 </div>
    </form>
</body>
</html>
