﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SupportScreens
{
    public partial class DupClaim : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
			//RMA-12047
                //Preparing XML to send to service
                XElement oMessageElement = GetDupClaimListTemplate();
                //Modify XML 
                if (Request.QueryString["Type"] != null)
                {
                    if ("PolicyDownload" == Request.QueryString["Type"])
                    {
                        hdr2.Visible = true;
                        hdr1.Visible = false;
                        colhdrForClaim1.Visible = false;
                        colhdrForClaim2.Visible = false;
                        idTrEmp.Visible = false;
                        colhdrForPolDownload1.Visible = true;
                        colhdrForPolDownload2.Visible = true;
                        XElement oPolicyName = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/PolicyName");
                        if (oPolicyName != null)
                        {
                            if (Request.QueryString["policyname"] != null)
                            {
                                oPolicyName.Value = Request.QueryString["policyname"];
                            }
                        }
                    }
                    else
                    {
                        hdr2.Visible = false;
                        hdr1.Visible = true;
                        colhdrForClaim1.Visible = true;
                        colhdrForClaim2.Visible = true;
                        idTrEmp.Visible = true;
                        colhdrForPolDownload1.Visible = false;
                        colhdrForPolDownload2.Visible = false;
                    }
                }
                else
                {
                    hdr2.Visible = false;
                    hdr1.Visible = true;
                    colhdrForClaim1.Visible = true;
                    colhdrForClaim2.Visible = true;
                    idTrEmp.Visible = true;
                    colhdrForPolDownload1.Visible = false;
                    colhdrForPolDownload2.Visible = false;
                }
				//RMA-12047
                XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";
               
                XElement oClaimIDs = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/ClaimIds");
                if (oClaimIDs != null)
                {
                    if (Request.QueryString["claimids"] != null)
                    {
                        oClaimIDs.Value = Request.QueryString["claimids"];
                        //hdnEmployeeId.Value = Request.QueryString["employeeid"];
                    }
                }
                XElement oFormName = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/FormName");
                if (oFormName != null)
                {
                    if (Request.QueryString["formname"] != null)
                    {
                        oFormName.Value = Request.QueryString["formname"];
                        //hdnClaimId.Value = Request.QueryString["claimid"];
                    }

                }
                XElement oEventDate = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/EventDate");
                if (oEventDate != null)
                {
                    if (Request.QueryString["eventdt"] != null)
                    {
                        oEventDate.Value = Request.QueryString["eventdt"];
                        //hdnClaimId.Value = Request.QueryString["claimid"];
                    }

                }
                XElement oDept = oMessageElement.XPathSelectElement("./Document/AdjusterDatedText/Dupes/Dept");
                if (oDept != null)
                {
                    if (Request.QueryString["dept"] != null)
                    {
                        oDept.Value = Request.QueryString["dept"];
                        //hdnClaimId.Value = Request.QueryString["claimid"];
                    }

                }
                //Calling Service to get all PreBinded Data 
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                //if (oFDMPageDom.GetElementsByTagName("MMIHistory").Count > 0)
                //{
                //    XmlElement xmlMMIHistoryNode = (XmlElement)oFDMPageDom.GetElementsByTagName("MMIHistory")[0];
                //    title.InnerText = "MMI History [ " + xmlMMIHistoryNode.Attributes["claimnumber"].Value + " ]";
                //    ChangedBy.InnerText = xmlMMIHistoryNode.Attributes["username"].Value; ;
                //    hdnMaxDate.Value = xmlMMIHistoryNode.Attributes["maxdate"].Value; ;
                //}
                if (oFDMPageDom.GetElementsByTagName("Dupes").Count > 0)
                {
                    DataSet aDataSet = new DataSet();
                    aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("Dupes")[0].OuterXml));

                    //// Bind the DataSet to the grid view
                    GridView1.DataSource = aDataSet.Tables["claim"];
                    GridView1.DataBind();
                }
            }
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        private void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        /// <summary>
        /// CWS request message template for GetDupClaimListTemplate
        /// </summary>
        /// <returns></returns>
        private XElement GetDupClaimListTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                    <Call>
                        <Function>DupClaimListAdaptor.GetDupClaimList</Function>
                     </Call>
                      <Document>
                          <AdjusterDatedText>
                            <Dupes>
                                <ClaimIds></ClaimIds>
                                <FormName></FormName>
                                <EventDate></EventDate>
                                <Dept></Dept>
                                <PolicyName></PolicyName>
                            </Dupes>
                        </AdjusterDatedText>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
		//RMA-12047
            if (e.Row.RowIndex ==0)
            {
                if (Request.QueryString["formname"] == "claimwc" || Request.QueryString["formname"] == "claimdi" || Request.QueryString["Type"] == "PolicyDownload")
                {
                    GridView1.Columns[8].Visible = true;
                }
                else
                {
                    GridView1.Columns[8].Visible = false;
                }
                if (Request.QueryString["Type"] == "PolicyDownload")
                {
                    GridView1.Columns[3].Visible = false;
                    GridView1.Columns[4].Visible = true;
                    GridView1.Columns[6].Visible = false;
                    GridView1.Columns[7].Visible = true;
                }
                else
                {
                    GridView1.Columns[3].Visible = true;
                    GridView1.Columns[4].Visible = false;
                    GridView1.Columns[6].Visible = true;
                    GridView1.Columns[7].Visible = false;
                }
            }
			//RMA-12047
        }
    }
}
