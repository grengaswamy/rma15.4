﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="personinvolvedlist.aspx.cs" Inherits="Riskmaster.UI.SupportScreens.personinvolvedlist" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Claimant</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script language="JavaScript" type="text/javascript">
					function selRecord(lRecordID)
					{
						if(window.opener.lookupCallback!=null)
						{
							eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+")");
							return false;
						}
					}
	</script>
</head>
<body>
    <form id="frmData" runat="server">
    <div>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr>
							<td class="ctrlgroup" width="50px" />
							<td class="ctrlgroup">LastName, FirstName</td>
							<td class="ctrlgroup" nowrap="1">Person Involved Type</td>
						</tr>
						   <%int i = 0; foreach (XElement item in result)
                            {
                                string rowclass = "";
                                if ((i % 2) == 1) rowclass = "datatd1";
                                else rowclass = "datatd";
                                i++;
                                %>
                                
                          <tr>                          
                                 <td class="<%=rowclass%>"/>                              
                                 <td class="<%=rowclass%>" nowrap="1"><%     
									lnkClaimant.ID = "lnkClaimant" + i;
                                lnkClaimant.Text = item.Element("PiEid").Value;
                                lnkClaimant.OnClientClick = "return selRecord(" + item.Element("PiEid").Attribute("codeid").Value + ")";%>
                              
                             <asp:LinkButton runat="server" id="lnkClaimant"></asp:LinkButton>  
									
								</td>
								<td class="<%=rowclass%>" nowrap="1">
									
									 <%=item.Element("PiTypeCode").Value%>
								</td>                                                                                         
                            
                                                                        
                          </tr>                          
                            <% }%> 
					</table>
    </div>
    </form>
</body>
</html>
