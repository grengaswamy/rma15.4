﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.SupportScreens
{
    public partial class personinvolvedlist : System.Web.UI.Page
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        public IEnumerable result = null;
        public XElement rootElement = null;  
        string sReturn = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            oFDMPageDom = new XmlDocument();
            if (!Page.IsPostBack)
            {
                Page.Title = AppHelper.GetQueryStringValue("callingentity");
                
                //Preparing XML to send to service
                XElement oMessageElement = GetMessageTemplate();

                XElement oElement = oMessageElement.XPathSelectElement("./Document/GetPIList/ClaimId");
                if (oElement != null)
                {
                    oElement.Value = AppHelper.GetQueryStringValue("claimid");
                }

                //Calling Service to get all PreBinded Data 
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                oFDMPageDom.LoadXml(sReturn);
                XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Instance/PersonInvolvedList/PersonInvolved")
                         select c;


            
            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>32187afd-8341-426a-9fa8-1e88fd92f8a8</Authorization> 
             <Call>
              <Function>ListPersonInvolvedAdaptor.GetPIList</Function> 
              </Call>
            <Document>
             <GetPIList>
              <ClaimId></ClaimId> 
              </GetPIList>
              </Document>
              </Message>


            ");

            return oTemplate;
        }
    }
}
