﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdjDatedTextList.aspx.cs" Inherits="Riskmaster.UI.SupportScreens.AdjDatedTextList" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Adjuster Dated Text</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="javascript" >
        function PrintPage() {
            self.print();
            return false;
        }
    </script>
</head>
<body  class="10pt" onload="self.print();">
    <form id="frmData" runat="server">
    <asp:TextBox id="adjRowID" runat="server" style="display:none" />
    <asp:TextBox id="FilterMethod" runat="server" style="display:none" />
    <asp:TextBox id="BeginDate" runat="server" style="display:none" />
    <asp:TextBox id="EndDate" runat="server" style="display:none" />
    <asp:TextBox id="FilterType" runat="server" style="display:none" />
    <asp:TextBox id="FilterTypeList" runat="server" style="display:none" />
    
    <asp:TextBox id="ReportDate" runat="server" style="display:none" />
    <asp:TextBox ID="hdnClaimNumber" runat="server"  style="display:none" />
    <%--MITS:13515 abansal23--%>
    <asp:TextBox ID="hdnPrimaryClaimantName" runat="server"  style="display:none" />
     <%--MITS:13515 abansal23--%>
    <asp:TextBox ID="ProprietaryInfo" runat="server"  style="display:none" />
    <asp:TextBox ID="RMName" runat="server"  style="display:none" />
    <asp:TextBox ID="Copyright" runat="server"  style="display:none" />
    <asp:TextBox ID="RightsReserved" runat="server"  style="display:none" />
		<table align="center" border="0" width="97%"  cellspacing="0">
			<tr>
                <td colspan="2">
                  <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
			<tr>
				<td class="ctrlgroup">ADJUSTER TEXT</td>
				<td class="ctrlgroup" align="right"><%=ReportDate.Text%></td>
			</tr>
			<tr><td><br/></td></tr>
			<tr>
			     <%--MITS:13515 Start By abansal23--%>
				<td class="formtitle" colspan="2" align="center">
				 <%if (hdnPrimaryClaimantName.Text != "")%>
                          <%  {
                                %>
                                 <%=hdnClaimNumber.Text%>&#160;-&#160;<%=hdnPrimaryClaimantName.Text%>   
                          <%  }%>  
                          <%else %>
                        <%=hdnClaimNumber.Text%> 
                               <%  %>
                 <%--MITS:13515 Stop By abansal23--%>
				</td>
			</tr>
			<tr><td><br/></td></tr>
			
			 <%foreach (XElement item in result)
      {%>
                    <tr>
                        <td width="30%">Adjuster Name:&#160;<%=item.Element("AdjusterName").Value %></td>
                        <td>Text Type: <%=item.Element("TextType").Value %></td>
                    </tr>
					<tr>    
                        <td colspan="2">Added by User:<%=item.Element("AddedByUser").Value %> </td>
                    </tr>
                    <tr>
						<td valign="top"><%=item.Element("DateTimeEntered").Value %></td>
						<%--gagnihotri MITS 15101 04/21/2009--%>
						<%XElement xelemrow = item.XPathSelectElement("DatedText"); %>
                        <td valign="top"><%=Server.HtmlDecode(xelemrow.Element("FirstInnerRow").Value)%></td> 
                    					
                    </tr> 
					<tr>
					    <td valign="top"  colspan="2">
					    <%if(item.Element("InnerRow") != null)%>
                          <%  {
                                  //gagnihotri MITS 15101 04/21/2009
                                  foreach (XElement itemtext in item.XPathSelectElements("DatedText"))
                                {
                                    %>
								    <%=itemtext.Element("InnerRow").Value%>
									 <br/>
								<% }
                            }%>   
					    </td>
				    </tr>
					<tr><td><br/></td></tr>
                 <%} %>
			<tr>
							<td colspan="2">
								<br/>
								<b><u>Note:</u></b>
								<br/>
								<EM>
								<%=ProprietaryInfo.Text%><br/>
								<%=RMName.Text%><br/>
								<%=Copyright.Text%><br/>
								<%=RightsReserved.Text%>
								</EM>
							</td>
						</tr>
						<tr><td><br/></td></tr>
						<tr><td><br/></td></tr>
						<tr>
							<td class="ctrlgroup" align="right" colspan="2">Confidential Data - <%=RMName.Text%></td>
						</tr>
					</table>
    </form>
</body>
</html>
