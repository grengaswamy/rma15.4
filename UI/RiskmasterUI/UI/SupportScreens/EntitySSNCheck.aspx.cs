﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.Linq;
namespace Riskmaster.UI.UI.SupportScreens
{
    public partial class EntitySSNCheck : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
              try
            {
                XElement XmlTemplate = null;
                bool bReturnStatus = false;
                
                if (!IsPostBack)
                {
                    //ErrorHelper.IsRMXSessionExists();
                    HdnTaxId.Text = AppHelper.GetQueryStringValue("TaxId");                    
                    HdnEntityId.Text = AppHelper.GetQueryStringValue("EntityId");
                    XmlTemplate = GetMessageTemplate("EntitySSNCheckAdaptor.CheckSSN");
                    bReturnStatus = CallCWSFunction("EntitySSNCheckAdaptor.CheckSSN", XmlTemplate);
                }
                lbl_TaxId.Text = HdnTaxId.Text + " ";
                if (FormMode.Text == "close")
                {
                    string sCWSresponse = "";
                     XmlTemplate = GetMessageTemplate("EntitySSNCheckAdaptor.IgnoreSSNChecking");
                    bReturnStatus = CallCWS("EntitySSNCheckAdaptor.IgnoreSSNChecking", XmlTemplate, out sCWSresponse, false, false);                    
                }                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }        
         }
        private XElement GetMessageTemplate(string sFunctionName)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append(sFunctionName);            
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><DupSSN><TaxId>");
            sXml = sXml.Append(HdnTaxId.Text);
            sXml = sXml.Append("</TaxId><EntityId>");
            sXml = sXml.Append(HdnEntityId.Text);
            sXml = sXml.Append("</EntityId><IgnoreForFuture>");
            sXml = sXml.Append(chkIgnoreForFuture.Checked);
            sXml = sXml.Append("</IgnoreForFuture><FormMode>");
            sXml = sXml.Append(FormMode.Text);
            sXml = sXml.Append("</FormMode><temp>");
            sXml = sXml.Append(cboEntities.SelectedValue);
            sXml = sXml.Append("</temp></DupSSN></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }        
    }
}
