﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml;
using System.Text;
//Added by Swati Agarwal
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.FNOLClaimReserve
{
    public partial class FNOLAddReserve : NonFDMBasePageCWS
    {
        string SetReserveTemplate = "<Message> " +
           "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization>" +
            " <Call>" +
               "<Function>FNOLReserveAdaptor.Save</Function>" +               
             "</Call>" +
             "<Document>" +
               "<ClaimID></ClaimID>" +
             "</Document>" +
           "</Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            string sClaimID = string.Empty;
            XmlDocument XmlDoc = new XmlDocument();
            XmlElement objElm = null;
            try
            {
                //Swati Agarawl ML Changes
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("FNOLAddReserve.aspx"), "FNOLAddReserveValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "FNOLAddReserveValidations", sValidationResources, true);
                //Swati Agarwal ML Changes
                //if (!IsPostBack)
                //{
                if (Request.QueryString["ClaimId"] != null)
                {
                    sClaimID = Request.QueryString["ClaimId"];
                }
                    txtClaimId.Text = sClaimID;
                    if (string.Compare(FNOLClmResGrid_RowDeletedFlag.Text.ToLower(), "true", true) == 0)
                    {
                        string selectedRowId = FNOLClmResSelectedId.Text;
                        bool bDelete = true;
                        XmlTemplate = GetMessageTemplate();
                        CallCWSFunctionBind("FNOLReserveAdaptor.Get", out sCWSresponse, XmlTemplate);
                        XmlDoc.LoadXml(sCWSresponse);
                        foreach (XmlElement xmlElem in XmlDoc.SelectNodes("//ResultMessage/Document/FNOLResList/listrow"))
                        {
                            if (xmlElem.SelectSingleNode("RowId") != null)
                            {
                                objElm = (XmlElement)xmlElem.SelectSingleNode("RowId");
                                if (string.Compare(objElm.InnerText, selectedRowId, true) == 0)
                                {
                                    if (xmlElem.SelectSingleNode("ResExst") != null)
                                    {
                                        if (string.Compare(xmlElem.SelectSingleNode("ResExst").InnerText, "Yes", true) == 0 || string.Compare(xmlElem.SelectSingleNode("ResExst").InnerText, "No", true) == 0)
                                        {
                                            //ClientScript.RegisterStartupScript(this.GetType(), "deletescript", "<script>FNOLResDelete();</script>");
                                            ClientScript.RegisterClientScriptBlock(this.GetType(), "deletescript", "<script>alert('The selected row can not be deleted. Reserve has not been processed for this row.');</script>");
                                            bDelete = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (bDelete)
                        {
                            XmlTemplate = GetMessageTemplate(selectedRowId);
                            CallCWS("FNOLReserveAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);                           
                        }
                        FNOLClmResGrid_RowDeletedFlag.Text = "false";
                    }                   
                //}
                if (string.Compare(txtAction.Text, "save", true) != 0)
                {                    
                    XmlTemplate = GetMessageTemplate();
                    CallCWSFunctionBind("FNOLReserveAdaptor.Get", out sCWSresponse, XmlTemplate);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate()
        {
            //added by Swati
            string item = string.Empty;
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<FNOLResList>");
            sXml = sXml.Append("<ClaimID>" + txtClaimId.Text + "</ClaimID>");
            sXml = sXml.Append("<listhead>");
            //Changed by Swati Agarwal
            item = RMXResourceProvider.GetSpecificObject("lblRowIDNo", RMXResourceProvider.PageId("FNOLAddReserve.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            sXml = sXml.Append("<RowId>");
            sXml = sXml.Append(item);
            sXml = sXml.Append("</RowId>");

            item = RMXResourceProvider.GetSpecificObject("lblClaimantDesc", RMXResourceProvider.PageId("FNOLAddReserve.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            sXml = sXml.Append("<Claimant>");
            sXml = sXml.Append(item);
            sXml = sXml.Append("</Claimant>");


            item = RMXResourceProvider.GetSpecificObject("lblPolicyDesc", RMXResourceProvider.PageId("FNOLAddReserve.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            sXml = sXml.Append("<Policy>");
            sXml = sXml.Append(item);
            sXml = sXml.Append("</Policy>");

            item = RMXResourceProvider.GetSpecificObject("lblUnitDesc", RMXResourceProvider.PageId("FNOLAddReserve.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            sXml = sXml.Append("<Unit>");
            sXml = sXml.Append(item);
            sXml = sXml.Append("</Unit>");

            item = RMXResourceProvider.GetSpecificObject("lblCoverageDesc", RMXResourceProvider.PageId("FNOLAddReserve.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            sXml = sXml.Append("<Coverage>");
            sXml = sXml.Append(item);
            sXml = sXml.Append("</Coverage>");

            item = RMXResourceProvider.GetSpecificObject("lblLossTypeDesc", RMXResourceProvider.PageId("FNOLAddReserve.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            sXml = sXml.Append("<LossType>");
            sXml = sXml.Append(item);
            sXml = sXml.Append("</LossType>");

            item = RMXResourceProvider.GetSpecificObject("lblResCreated", RMXResourceProvider.PageId("FNOLAddReserve.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            sXml = sXml.Append("<ResExst>");
            sXml = sXml.Append(item);
            sXml = sXml.Append("</ResExst>");
            sXml = sXml.Append("</listhead></FNOLResList>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetMessageTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><FNOLClmRes>");
            sXml = sXml.Append("<control name='FNOLClmResGrid'>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</FNOLClmRes></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                SetReserveTemplate = AppHelper.ChangeMessageValue(SetReserveTemplate, "//ClaimID", txtClaimId.Text);
                string sReturn = AppHelper.CallCWSService(SetReserveTemplate.ToString());
                txtAction.Text = string.Empty;
                ClientScript.RegisterStartupScript(this.GetType(), "reopenScript", "<script type='text/javascript'>window.document.forms[0].submit();</script>");
                //XmlDocument objReturnXml = new XmlDocument();
                //objReturnXml.LoadXml(sReturn);
                //string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
    }
}