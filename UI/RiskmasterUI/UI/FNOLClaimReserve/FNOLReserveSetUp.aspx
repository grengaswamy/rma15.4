﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FNOLReserveSetUp.aspx.cs" Inherits="Riskmaster.UI.FNOLClaimReserve.FNOLReserveSetUp" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FNOL Reserve Setup</title>
      <%--<link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />--%>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

    <script type="text/javascript">
        window.onbeforeunload = function () {
            if (((window.event.clientX || event.clientX) < 0) || ((window.event.clientY || event.clientY) < 0)) {
                var objAction = document.getElementById("txtWinClose");
                if (objAction != null) {
                    objAction.value = "yes";
                    window.document.forms[0].submit();
                }                
            }
        }
   </script>
</head>
<body>
    <form id="frmData" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />              
            </td>
        </tr>
    </table>   
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../Images/tb_save_active.png" Width="28"
                Height="28" border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'" OnClick="btnSave_Click"
               OnClientClick="return FNOLClmRes_Save();" /></div>
    </div>
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="FNOL Reserve Setup" />
    </div>
    <br />
     
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/FNOLResList/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="txtWinClose" />
    <asp:TextBox Style="display: none" runat="server" ID="txtSaveOnCancel" />
    <div>
    <table width="100%">
     <tr>
   <td><u><b><asp:Label ID="lblClaimantdr" runat="server" Text="<%$ Resources:lblClaimantdr %>"></asp:Label>
   </b></u></td>
   <td>
       <asp:DropDownList runat="server" ID="cmbClaimant" onchange="setDataChanged(true);"></asp:DropDownList>
   </td>
   </tr>
    <tr>
   <td><u><b><asp:Label ID="lblPolicyNamedr" runat="server" Text="<%$ Resources:lblPolicyNamedr %>"></asp:Label>
   </b></u></td>
   <td>
       <asp:DropDownList runat="server" ID="cmbPolicy" AutoPostBack="true" OnSelectedIndexChanged="cmbPolicy_SelectedIndexChanged" onchange="setDataChanged(true);" ></asp:DropDownList>
   </td>
   </tr>
   <tr>
   <td><u><b><asp:Label ID="lblUnitdr" runat="server" Text="<%$ Resources:lblUnitdr %>"></asp:Label>
   </b></u></td>
   <td>
       <asp:DropDownList runat="server" ID="cmbUnit" AutoPostBack="true" OnSelectedIndexChanged="cmbUnit_SelectedIndexChanged" Enabled="false" onchange="setDataChanged(true);"></asp:DropDownList>
   </td>
   </tr>  
   <tr>
   <td><u><b><asp:Label ID="lblCoverageTypedr" runat="server" Text="<%$ Resources:lblCoverageTypedr %>"></asp:Label>
   </b></u></td>
   <td>
       <asp:DropDownList runat="server" ID="cmbCoverageType" AutoPostBack="true" OnSelectedIndexChanged="cmbCoverageType_SelectedIndexChanged" Enabled="false" onchange="setDataChanged(true);"></asp:DropDownList>
   </td>
   </tr>
    <tr>
   <td><u><b><asp:Label ID="lblTypeLossdr" runat="server" Text="<%$ Resources:lblTypeLossdr %>"></asp:Label>
   </b></u></td>
   <td>
       <asp:DropDownList runat="server" ID="cmbLossType" Enabled="false" onchange="setDataChanged(true);"></asp:DropDownList>
   </td>
   </tr>
    <%--<tr>
    <td> <b><u>Coverage</u></b></td>
    <td>
    <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="CoverageType" CodeTable="COVERAGE_TYPE" ControlName="CoverageTypeCode" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='CoverageTypeCode']" RMXType="code" Required="true" ValidationGroup="vgSave" />
    </td>
    </tr>
    <tr>
    <td> <b><u>Type of Loss</u></b></td>
    <td>
    <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="LossType" CodeTable="COVERAGE_TYPE" ControlName="CoverageTypeCode" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name='CoverageTypeCode']" RMXType="code" Required="true" ValidationGroup="vgSave" />
    </td>
    </tr>--%>
    </table>
     <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="txtClaimId" />
     <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnNext">
            <asp:Button class="button" runat="server" ID="btnNext" Text="Add Next" Width="100px" OnClientClick="return FNOLClmRes_Next();"
                OnClick="btnNext_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return FNOLClmRes_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </div>
    </form>
</body>
</html>
