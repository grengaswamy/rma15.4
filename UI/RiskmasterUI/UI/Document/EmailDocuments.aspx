﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailDocuments.aspx.cs" Inherits="Riskmaster.UI.Document.EmailDocuments"  EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Document Management</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script src="../../Scripts/cul.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <p>
    <div id="maindiv" style="height:100%;width:99%;overflow:auto">
        <table class="singleborder" align="center">
            <tr>
                <td colspan="3">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup"> 
                    <asp:Label ID="lblEmailDocuments" runat="server" Text="<%$ Resources:lblEmailDocumentsResrc %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td nowrap="" class="datatd">
                    <asp:Label ID="lblAddTheUsers" runat="server" Font-Bold="true" Text="<%$ Resources:lblAddTheUsersResrc %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="datatd">      
                    <asp:ListBox ID="UserEmailId" runat="server" Rows="15" Width="182px" SelectionMode="Multiple"></asp:ListBox>
                    <input type="button" class="CodeLookupControl" value="" id="cmdAddCustomizedListUser" onclick="AddCustomizedListUser('email-doc','UserEmailId','UserIdStr','UserStr')"/>
                    <input type="button" class="BtnRemove" value="" id="cmdDelCustomizedListUser" style="width:21px" onclick="DelCustomizedListUser('email-doc','UserEmailId','UserIdStr','UserStr')"/>
                    <input type="hidden"  runat="server" name="" value="" id="UserIdStr"/>
                    <input type="hidden"  runat="server" value="" id="UserStr"/>
                    <asp:HiddenField ID="NonMCMFormName" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="datatd">
                    <asp:Label ID="lblAdditionalRecipient" runat="server" Text="<%$ Resources:lblAdditionalRecipientResrc %>" /><br /><input type="text" name="txtEmailAdd" value="" size="45" id="txtEmailAdd" runat="server" /><br /><asp:Label ID="lblAddressSeperation" runat="server" CssClass="smallnote" Text="<%$ Resources:lblAddressSeperationResrc %>" />
                </td>
            </tr>
   
            <tr>    
                <td class="datatd">
                    <asp:Label ID="lblSubject" runat="server" Text="<%$ Resources:lblSubjectResrc %>" /><br /><input type="text" name="EmailSubject" value="" size="45" id="EmailSubject" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="datatd">
                    <asp:Label ID="lblRecipientsMsg" runat="server" Text="<%$ Resources:lblRecipientsMsgResrc %>" /><br /><textarea cols="36" wrap="soft" name="Message" rows="5"></textarea>
                </td>
            </tr>
   
            <tr>
                <td>
                    <asp:HiddenField ID="Regarding" runat="server" Value="Files"/>
                    <asp:HiddenField ID="FolderId" runat="server" />
                    <asp:HiddenField ID="hdocId" runat="server" />
                    <asp:HiddenField ID="Psid" runat="server" />
                    <asp:HiddenField ID="flag" runat="server" />
                    <asp:HiddenField ID="hdnPageNumber" runat="server" />
                    <asp:HiddenField ID="hdnSortExpression" runat="server" />
                    <asp:HiddenField ID="TableName" runat="server" />
                    <asp:HiddenField ID="AttachTableName" runat="server" />
                    <asp:HiddenField ID="AttachRecordId" runat="server" />
                    <asp:HiddenField ID="FormName" runat="server"/>
                </td>
            </tr>
            <tr>
                <td align="center" nowrap="">
                    <asp:Button ID="btnEmail" runat="server"  Text="<%$ Resources:btnEmailResrc %>" CssClass="button" OnClick ="btnEmail_Click"/>
            
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass="button"  OnClick="btnCancel_Click"/>
                </td>
        
            </tr>
   
        </table>
  
    </div>
        </p>
    </form>
    
</body>
</html>
