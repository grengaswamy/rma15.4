﻿using System;
using System.Web;
using System.Web.UI;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;
namespace Riskmaster.UI.Document
{
    public partial class DisplayDocument : System.Web.UI.Page
    {
        public DocumentType ViewData = null;

        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            // pen testing changes:atavaragiri MITS 27871
            if(!String.IsNullOrEmpty(AppHelper.GetValue("Psid")))
          //  Psid.Value = AppHelper.GetValue("Psid");
            Psid.Value =AppHelper.HTMLCustomEncode( AppHelper.GetValue("Psid"));
            //END:pen testing 
            flag.Value = AppHelper.GetValue("flag");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder queryString = null;
            HiddenField FileSize = null;
            ErrorControl1.Text = "";
            int prevUploadStatus = 0;
            SetInitialValues();
            ViewData = new DocumentType();
            FileSize = (HiddenField)Page.FindControl("hdnFileSize");
            //rsushilaggar Pen Testing
            if (!string.IsNullOrEmpty(Request.QueryString["redirectdoc"]))
            {
                Server.Transfer(Request.QueryString["redirectdoc"]);
            }
            //rsushilaggar Pen Testing
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DisplayDocument.aspx"), "DisplayDocumentValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "DisplayDocumentValidationScripts", sValidationResources, true);

            if (!Page.IsPostBack)
            {                
                DocumentBusinessHelper dc = new DocumentBusinessHelper();
                btnActDownload.Attributes.Add("style", "DISPLAY: none");
                try
                {
                    if (AppHelper.GetQueryStringValue("id") != "")
                    {
                        //Added by Nitin to findout error occured while upload this docid
                        if (IsErrorOccuredInUpload(AppHelper.GetQueryStringValue("id")))
                            prevUploadStatus = 3;

                        //smishra25:Get the file size
                        
                            FileSize.Value = AppHelper.GetQueryStringValue("FileSize");
                        //smishra25:Get the file size

                        ViewData = dc.Edit(Conversion.ConvertStrToInteger(AppHelper.GetQueryStringValue("id")), AppHelper.GetFormValue("flag").ToString(), AppHelper.GetFormValue("Psid").ToString(), FormName.Value,prevUploadStatus);
                        ViewData.FileName = ViewData.FileName.Replace(".fdf)", ".fdf").Replace("FROI(", "").Replace("SROI(", "").Replace("ACORD(", ""); //To remove the unwanted content from filename
                        //smishra25:Put the size limit Outlook setting in the hidden field
                        hdnSizeLimitOutlook.Value = ViewData.SizeLimitOutlook.ToString();

                        //MITS Start MITS 20965      <!-- skhare7:MITS 23094-->
                        if (Request.Form["hdocIdListids"] != null && Request.Form["hdocIdListids"]!="")
                        {
                            string shdocId = Request.Form["hdocIdListids"].ToString();
                            hdocIdList.Value = shdocId;
                           
                        }
                        else if (Request.Form["hdocIdDeleteLIst"] != null && Request.Form["hdocIdDeleteLIst"] != "")
                        {
                            string shdocId = Request.Form["hdocIdDeleteLIst"].ToString();
                            hdocIdList.Value = shdocId;
                        }
                        else
                        {
                            string shdocId = Request.Form["hdocId"].ToString();
                            hdocIdList.Value = shdocId;
                        }
                        //  End MITS 20965      <!-- skhare7:MITS 23094-->
                            FolderId.Value = Request.Form["FolderId"].ToString();
                        hdocId.Value = AppHelper.GetQueryStringValue("id");
                        Psid.Value = Request.Form["Psid"].ToString();
                        flag.Value = Request.Form["flag"].ToString();
                        hdnPageNumber.Value = Request.Form["hdnPageNumber"].ToString();
                        hdnSortExpression.Value = Request.Form["hdnSortExpression"].ToString();
                        FolderName.Value = Request.Form["FolderName"].ToString();
                        lblFolderName.Text = AppHelper.GetFormValue("FolderName");
                        hdnDocumentSize.Value = ViewData.FileSize.ToString();
                        FileSize.Value = ViewData.FileSize.ToString();//set file size if value null from query string asingh263 mits 34137
                        NonMCMFormName.Value = AppHelper.GetFormValue("NonMCMFormName");
                        //mudabbir added 36022
                        hdnSilver.Value = ViewData.UseSilverlight.ToString();
                        DataBind();

                        if (ViewData.Upload_Status == 1)
                        {
                            //that means document upload is in process ,
                            btnDownload.Enabled = false;
                            btnEdit.Disabled = true;
                            btnEmailDoc.Disabled = true;
                            btnMove.Disabled = true;
                            btnTransfer.Disabled = true;
                            btnDelete.Disabled = true;
                            btnCopy.Disabled = true;

                            lblDownloading.Visible = true;
                            //lblDownloading.Text = "Uploading is in Process,Try Again!";
                            lblDownloading.Text = GetResourceValue("lblDownLoading1Resrc","0");
                            imgProcessing.Visible = true;
                        }
                        else if (ViewData.Upload_Status == 3)
                        {
                            lblDownloading.Visible = false;
                            imgProcessing.Visible = false;
                            
                            //btnDownload.Text = "Upload Again";
                            btnDownload.Text = GetResourceValue("btnDownload1Resrc", "0");
                            btnDownload.Visible = true;
                            btnDownload.Enabled = true;
                            btnDelete.Visible = true;
                            btnDelete.Disabled = false;
                            btnBack.Visible = true;

                            btnEdit.Visible = false;
                            btnEmailDoc.Visible = false;
                            btnMove.Visible = false;
                            btnTransfer.Visible = false;
                            btnCopy.Visible = false;

                            queryString = new StringBuilder();

                            queryString.Append("folderid=" + ViewData.FolderId);
                            queryString.Append("&psid=" + ViewData.PsId.ToString());
                            queryString.Append("&typecodeid=" + ViewData.Type.Id.ToString());
                            queryString.Append("&classcodeid=" + ViewData.Class.Id.ToString());
                            queryString.Append("&categorycodeid=" + ViewData.Category.Id.ToString());
                            queryString.Append("&documentid=" + ViewData.DocumentId.ToString());
                            
                            ViewState["querystring"] = queryString.ToString();

                            //throw new ApplicationException("Error occured while uploading the document,Please check error log for details");
                            throw new ApplicationException(GetResourceValue("CatchUploadingError", "0"));
                        }
                        else
                        {
                            btnDownload.Enabled = true;
                            //for large documents, Edit button should be hidden
                            //if (ViewData.Upload_Status == -1)
                            //{
                                btnEdit.Visible = true;
                            //}
                            btnEdit.Disabled = false;
                            btnEmailDoc.Disabled = false;
                            btnMove.Disabled = false;
                            btnDelete.Disabled = false;
                            btnTransfer.Disabled = false;
                            btnCopy.Disabled = false;

                            lblDownloading.Visible = false;
                            imgProcessing.Visible = false;
                        }
                        ViewState["Upload_Status"] = ViewData.Upload_Status.ToString();
                        ViewState["FileName"] = ViewData.FileName;
                    }
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
            else
            {
                if (Page.FindControl("hdnEmailButtonClick") != null)
                {
                    HiddenField hdnEmailClick = (HiddenField)Page.FindControl("hdnEmailButtonClick");
                    if (string.Compare(hdnEmailClick.Value, "1") == 0)
                    {


                        DocumentBusinessHelper dc = new DocumentBusinessHelper();
                        try
                        {
                            if (AppHelper.GetFormValue("hdocId") != "")
                            {

                                ViewData = dc.Download(Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdocId")));


                                if (Page.FindControl("hdnformfilecontent") != null)
                                {
                                    HiddenField hdnFile = (HiddenField)Page.FindControl("hdnformfilecontent");
                                    hdnFile.Value = Convert.ToBase64String(ViewData.FileContents);
                                }

                            }
                        }
                        catch (FaultException<RMException> ee)
                        {
                            ErrorHelper.logErrors(ee);
                            ErrorControl1.errorDom = ee.Detail.Errors;
                        }
                        catch (Exception ee)
                        {
                            ErrorHelper.logErrors(ee);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ee, BusinessAdaptorErrorType.SystemError);
                            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                        }
                    }
                }
            }

            //smishra25:Get the file size

            HtmlInputText txtFileSize = (HtmlInputText)Page.FindControl("FileSize");
            //asingh263 MITS 34137 starts
            if (string.IsNullOrEmpty(FileSize.Value))
                FileSize.Value = "0";
            txtFileSize.Value = string.Concat(Convert.ToDecimal(Math.Round((Convert.ToDecimal(Convert.ToDecimal(FileSize.Value) / 1024) / 1024), 4)), " MB");
            //asingh263 MITS 34137 ends

            //smishra25:Get the file size

            //mudabbir added 36022
            hdnSilver.Value = AppHelper.GetFormValue("UseSilverlight");
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            //Added:Yukti,MITS 34587,Dt:12/10/2013
            //Server.Transfer("DocumentList.aspx", true);
            string strue = "True";
            Server.Transfer(String.Format("DocumentList.aspx?DocList=" + strue), true);
            //Ended:MITS 34587
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                //if (btnDownload.Text == "Upload Again")
                if (string.Equals(btnDownload.Text, GetResourceValue("btnDownload1Resrc", "0"),StringComparison.OrdinalIgnoreCase))
                {
                    Server.Transfer("AddDocument.aspx?" + ViewState["querystring"].ToString(), true);
                    return;
                }

                if (AppHelper.GetFormValue("hdocId") != "")
                {
                   DownLoadLargeDocument(Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdocId")));
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        private void DownloadDocument(int p_iDocumentId)
        {
            DocumentBusinessHelper dc = new DocumentBusinessHelper();

            ViewData = dc.Download(p_iDocumentId);
            using (MemoryStream ms = new MemoryStream(ViewData.FileContents))
            {
                long dataLengthToRead = ms.Length;
                int blockSize = dataLengthToRead >= 5000 ? 5000 : (int)dataLengthToRead;
                byte[] buffer = new byte[dataLengthToRead];

                Response.Clear();

                // Clear the content of the response
                Response.ClearContent();
                Response.ClearHeaders();

                // Buffer response so that page is sent
                // after processing is complete.
                Response.BufferOutput = true;

                // Add the file name and attachment,
                // which will force the open/cance/save dialog to show, to the header
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(ViewData.FileName));

                // bypass the Open/Save/Cancel dialog
                //Response.AddHeader("Content-Disposition", "inline; fileid=" + doc.FileName);

                // Add the file size into the response header
                Response.AddHeader("Content-Length", ms.Length.ToString());

                // Set the ContentType
                Response.ContentType = "application/octet-stream";

                // Write the document into the response
                while (dataLengthToRead > 0 && Response.IsClientConnected)
                {
                    Int32 lengthRead = ms.Read(buffer, 0, blockSize);
                    Response.OutputStream.Write(buffer, 0, lengthRead);
                    //Response.Flush(); // do not flush since BufferOutput = true
                    dataLengthToRead = dataLengthToRead - lengthRead;
                }

                Response.Flush();
                Response.Close();
            }

            // End the response

            Response.End();

        }

        private void DownLoadLargeDocument(int p_iDocumentId)
        {
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            string tempFileName = string.Empty;

            tempFileName = Server.MapPath("~/UploaderTemp/" + System.Guid.NewGuid().ToString());
            ViewState["tempfilename"] = tempFileName + "_completed";
            imgProcessing.Visible = true;
            lblDownloading.Visible = true;
            btnBack.Visible = false;
            dc.DownloadLargeFile(p_iDocumentId, tempFileName);
            Timer1.Enabled = true;
        }

        private bool IsErrorOccuredInUpload(string p_sDocumentId)
        {
            string errorFile = string.Empty;
            errorFile = Server.MapPath("~/UploaderTemp/") + p_sDocumentId + "_error";

            if (File.Exists(errorFile))
            {
                File.Delete(errorFile);
                return true;
            }
            else
                return false;
        }


        protected void Timer1_Tick(object sender, EventArgs e)
        {
            string tempFileName = string.Empty;
            FileStream targetStream = null;
            try
            {
                tempFileName = ViewState["tempfilename"].ToString();
                if (File.Exists(tempFileName))
                {
                    Timer1.Enabled = false;

                    string script = "OpenPage('completed')";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "handleDownload", script, true);
                }
                else if(File.Exists(tempFileName.Replace("_completed","_error")))
                {
                    Timer1.Enabled = false;
                    string script = "OpenPage('error')";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "handleDownload", script, true);
                }

            }
            catch (FaultException<RMException> ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void Timer2_Tick(object sender, EventArgs e)
        {
            string tempFileName = string.Empty;
            FileStream targetStream = null;
            try
            {
                Timer2.Enabled = false;
                    
                    string script = "OpenPage('downloadStart')";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "handleDownload", script, true);
                
            }
            catch (FaultException<RMException> ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void btnActDownload_Click(object sender, EventArgs e)
        {
            FileStream targetStream = null;
            string tempFileName = string.Empty;
            long dataLengthToRead = 0;
            int blockSize = 0;
            long count = 0;
            byte[] buffer = null;

            try
            {
                tempFileName = ViewState["tempfilename"].ToString();

                if (Request.Params.Get("__EVENTARGUMENT") == "error")
                {
                    imgProcessing.Visible = false;
                    lblDownloading.Visible = false;
                    btnBack.Visible = true;
                    File.Delete(tempFileName.Replace("_completed", "_error"));
                    //throw new ApplicationException("Error has been Occured while Downloading document,please see log for details");
                    //MITS 27051 hlv 5/10/2012
                    //throw new ApplicationException("An error has occurred while downloading the document, please see the error log for details.");
                    throw new ApplicationException(GetResourceValue("CatchDownloadingError","0"));
                }
                else if (Request.Params.Get("__EVENTARGUMENT") == "completed")
                {
                    imgProcessing.Visible = false;
                    lblDownloading.Visible = false;
                    btnBack.Visible = true;

                    Timer2.Enabled = true; 
                }
                else
                {
                    using (targetStream = new System.IO.FileStream(tempFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                    {
                        dataLengthToRead = targetStream.Length;
                        blockSize = dataLengthToRead >= 500 ? 500 : (int)dataLengthToRead;
                        buffer = new byte[blockSize];
                        targetStream.Seek(0, SeekOrigin.Begin);
                        Response.Clear();

                        // Clear the content of the response
                        Response.ClearContent();
                        Response.ClearHeaders();

                        // Buffer response so that page is sent
                        // after processing is complete.
                        Response.BufferOutput = false;

                        // Add the file name and attachment,
                        // which will force the open/cance/save dialog to show, to the header
                        //kkaur8 added for MITS 33703 starts
                        //Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(ViewState["FileName"].ToString()));
                        //rsharma220 MITS 35803
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlPathEncode(ViewState["FileName"].ToString().Replace(";","%3B").Replace("#","%23")));
                        //kkaur8 added for MITS 33703 Ends

                        // Add the file size into the response header
                        Response.AddHeader("Content-Length", targetStream.Length.ToString());

                        // Set the ContentType
                        Response.ContentType = "application/octet-stream";

                        while ((count = targetStream.Read(buffer, 0, blockSize)) > 0)
                        {
                            Response.OutputStream.Write(buffer, 0, (int)count);
                            Response.Flush();
                        }
                        Response.Flush();
                        Response.Close();
                    }
                    
                    targetStream.Close();

                    imgProcessing.Visible = false;
                    lblDownloading.Visible = false;
                    btnBack.Visible = true;
                    File.Delete(tempFileName);

                    // End the response
                    Response.End();
                }
            }
            catch (FaultException<RMException> ee)
            {
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }

            //mkaran2 - MITS #32088 - start
            catch (HttpException ee)
            {
                // delete temp file
                if (File.Exists(tempFileName))
                    File.Delete(tempFileName);

                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } //mkaran2 - MITS #32088 - end

            catch (Exception ee)
            {
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("DisplayDocument.aspx"),strResourceType).ToString();
        }
    }
}
