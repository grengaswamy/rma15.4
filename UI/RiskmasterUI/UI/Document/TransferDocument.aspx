﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransferDocument.aspx.cs" Inherits="Riskmaster.UI.Document.TransferDocument"  EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Document Management</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <table class="singleborder" align="center">
        <tr>
            <td colspan="3">
                <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="ctrlgroup">
                <asp:Label ID="lblTransferDocuments" runat="server" Text="<%$ Resources:lblTransferDocumentsResrc %>" />
            </td>
        </tr>
        <tr>
            <td nowrap="" class="datatd">
                <asp:Label ID="lblUsersForDocument" runat="server" Font-Bold="true" Text="<%$ Resources:lblUsersForDocumentResrc %>" />
            </td>
        </tr>
        <tr>
            <td class="datatd">
                <p align="center">
                    <asp:ListBox ID="Userslist" runat="server" Rows="15" SelectionMode="Multiple"></asp:ListBox>
                    <br />
                    <asp:Label ID="lblMultipleSelection" CssClass="smallnote" runat="server" Text="<%$ Resources:lblMultipleSelectionResrc %>" />
                </p>
            </td>
        </tr>
        <tr>
            <td align="center" nowrap="">
                <asp:Button ID="btnTransfer" runat="server"  Text="<%$ Resources:btnTransferResrc %>" CssClass="button" OnClick ="btnTransfer_Click"/>
            
                <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass="button"  OnClick="btnCancel_Click"/>
            </td>        
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="FolderId" runat="server" />
                <asp:HiddenField ID="hdocId" runat="server" />
                <asp:HiddenField ID="Psid" runat="server" />
                <asp:HiddenField ID="flag" runat="server" />
                <asp:HiddenField ID="hdnPageNumber" runat="server" />
                <asp:HiddenField ID="hdnSortExpression" runat="server" />
                <asp:HiddenField ID="AttachTableName" runat="server" />
                <asp:HiddenField ID="AttachRecordId" runat="server" />
                <asp:HiddenField ID="FormName" runat="server"/>
                <asp:HiddenField ID="Regarding" runat="server" Value="Files"/>
            </td>
        </tr>
   </table>
    </form>
</body>
</html>
