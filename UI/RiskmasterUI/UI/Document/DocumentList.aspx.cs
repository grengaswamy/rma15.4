﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Riskmaster.UI.DocumentService;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.Data;
using System.Text;
using Riskmaster.RMXResourceManager;
using System.IO;  //mkaran2 : MITS 
using Riskmaster.Models;
using System.Xml;

namespace Riskmaster.UI.Document
{
    public class DocumentDatas
    {
        public string Name
        {
            get;
            set;

        }

        public string Type
        {
            get;
            set;

        }
        public string CreateDate
        {
            get;
            set;

        }
        public string Message
        {
            get;
            set;

        }
        public string Category
        {
            get;
            set;

        }
        public string Title
        {
            get;
            set;

        }
        public string Class
        {
            get;
            set;

        }
        public string DocumentType
        {
            get;
            set;

        }
        public string Subject
        {
            get;
            set;

        }
        public int Id
        {
            get;
            set;

        }
        //smishra25:Adding filesize
        public string FileSize
        {
            get;
            set;

        }
        //mkaran2 : Adding UploadStatus : MITS 33747
        public string UploadStatus
        {
            get;
            set;

        }
        //tanwar2 - ImageRight - start
        public string IRUpload { get; set; }
        //tanwar2 - ImageRight - end 
    }
    public partial class DocumentList : System.Web.UI.Page
    {
        private string m_SortDirection = "";
        private bool bIsEmptyMessage = false;
        private string m_strSortExp = "";
        private int m_iPagesize = 10;
        public bool m_AddDoc = false;          //Added:YUkti,MITS 30990
        //Added:YUkti, MITS 34595
        // akaushik5 Commented for MITS 36901 Starts
        //public bool bSortAlpha = false;
        //public bool bSortDt = false;
        // akaushik5 Commented for MITS 36901 Ends
        //Ended:YUkti, MITS 34595
        public Riskmaster.Models.DocumentList Model = new Riskmaster.Models.DocumentList();
        public Riskmaster.Models.DocumentType ViewData = null;

        bool bCheckStatus = false;
        
        // akaushik5 Added for MITS 36901 Starts
        /// <summary>
        /// Gets or sets the grid view sort direction.
        /// </summary>
        /// <value>
        /// The grid view sort direction.
        /// </value>
        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                {
                    ViewState["sortDirection"] = SortDirection.Ascending;
                }

                return (SortDirection)ViewState["sortDirection"];
            }

            set
            {
                ViewState["sortDirection"] = value;
            }
        }
        // akaushik5 Added for MITS 36901 Ends

        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            // pen testing changes:atavaragiri MITS 27871
            //Psid.Value = AppHelper.GetValue("Psid");
            if (!String.IsNullOrEmpty(AppHelper.GetValue("Psid")))
                Psid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetValue("Psid"));
            //END:pen testing changes 
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
            NonMCMFormName.Value = AppHelper.GetValue("NonMCMFormName");
            //MITS 20965 skhare7 MITS 23094
            if (Request.Form["hdocIdList"] != null)
            {
                if (Request.Form["hdocIdDeleteLIst"] != null)
                    hdocIdListids.Value = Request.Form["hdocIdDeleteLIst"].ToString();
                else
                    hdocIdListids.Value = Request.Form["hdocIdList"].ToString();


            }
            if (Request.Form["hdocIdDeleteLIst"] != null)
                hdocIdListids.Value = Request.Form["hdocIdDeleteLIst"].ToString();
            //MITS 20965 skhare7 End 
        }
        private List<DocumentDatas> BindData(string exp, string pagenum)
        {
            List<DocumentDatas> doc = new List<DocumentDatas>();
            DocumentBusinessHelper docHelper = new DocumentBusinessHelper();
            try
            {
                // akaushik5 Changed for MITS 36901 Starts
                //if (exp == hdnSortExpression.Value)
                //{
                //    exp = (6 + Conversion.ConvertObjToInt(exp)).ToString();
                //}

                //if (exp != "")
                //    hdnSortExpression.Value = exp;
                //else
                //    exp = hdnSortExpression.Value;

                if (string.IsNullOrEmpty(exp) && !object.ReferenceEquals(hdnSortExpression, null))
                {
                    exp = hdnSortExpression.Value;
                }

                // akaushik5 Changed for MITS 37157 Starts
                //if (this.GridViewSortDirection.Equals(SortDirection.Descending))
                if (this.GridViewSortDirection.Equals(SortDirection.Descending) && Conversion.ConvertObjToInt(exp) <= 6)
                // akaushik5 Changed for MITS 37157 Ends
                {
                    exp = (6 + Conversion.ConvertObjToInt(exp)).ToString();
                }

                // akaushik5 Added for MITS 37157 Starts
                if (!object.ReferenceEquals(hdnSortExpression, null))
                {
                    this.hdnSortExpression.Value = exp;
                }
                // akaushik5 Added for MITS 37157 Ends
                // akaushik5 Changed for MITS 36901 Ends

                if (Conversion.ConvertStrToInteger(exp) >= 1 && Conversion.ConvertStrToInteger(exp) <= 6)
                {
                    m_SortDirection = "Descending";
                }
                else
                {
                    m_SortDirection = "Ascending";
                }

                //nnithiyanand 33936 - Commented this code snippet -Starts
                //if (Page.PreviousPage != null)
                //{
                //    exp = AppHelper.GetFormValue("hdnSortExpression");
                //    hdnSortExpression.Value = exp;
                //}
                //nnithiyanand 33936 - Commented this code snippet -Ends

                m_strSortExp = exp;
                string screenflag = AppHelper.GetValue("flag");
                // pen  testing changes:atavaragiri MITS 27871
                string psid = String.Empty;
                //string psid = AppHelper.GetValue("Psid");
                if (!String.IsNullOrEmpty(AppHelper.GetValue("Psid")))
                    psid = AppHelper.HTMLCustomEncode(AppHelper.GetValue("Psid"));
                //Added:Yukti,MITS 30990,DT:11/14/2013
                if (bCheckStatus)
                {
                    m_AddDoc = true;
                }
                //Ended:Yukti,MITS 30990
                //END: pen  testing changes:atavaragiri
                //Added:Yukti,DT:11/14/2013, MITS 30990
                //Model = docHelper.List(exp, screenflag, psid, pagenum);
                Model = docHelper.List(exp, screenflag, psid, pagenum, m_AddDoc);
                //Ended:Yukti,MITS 30990
                //smishra25:Put the size limit Outlook setting in the hidden field
                hdnSizeLimitOutlook.Value = Model.SizeLimitOutlook.ToString();
                if (Model.JumpToAcrosoft != "1")
                {  // pen testing changes:atavaragiri MITS 27871
                    //hdTotalPages.Value = Model.Pagecount;
                    hdTotalPages.Value = AppHelper.HTMLCustomEncode(Model.Pagecount);
                    //End :pen testing
                    hdnPageNumber.Value = Model.Pagenumber;
                    Model.ScreenFlag = screenflag;
                    flag.Value = screenflag;
                    if (screenflag == "Files")
                        ParentFolder.Value = Model.TopFolder.FolderID;

                    DocumentDatas dcUpfolder = null;
                    if (screenflag == "Files")
                    {
                        if (Model.Pid != "0")
                        {
                            dcUpfolder = new DocumentDatas();

                            dcUpfolder.Name = "";
                            dcUpfolder.Id = -922;
                            dcUpfolder.Type = "Upfolder";

                        }
                    }

                    var query2 = from s1 in Model.Folders
                                 select new DocumentDatas
                                 {
                                     Name = s1.FolderName,
                                     Id = (int)Convert.ToInt64(s1.FolderID),
                                     Type = "F",
                                     Message = ""


                                 };
                    var query1 = from s in Model.Documents
                                 select new DocumentDatas
                                 {
                                     Name = s.FileName,
                                     Id = (int)Convert.ToInt64(s.Pid
                                     ),
                                     Type = "D",
                                     Subject = s.Subject,
                                     Class = s.DocumentClass,
                                     DocumentType = s.DocumentsType,
                                     Category = s.DocumentCategory,
                                     CreateDate = s.CreateDate,
                                     Title = s.Title,
                                     Message = string.Empty,
                                     FileSize = s.FileSizeInMB.ToString() + " MB",
                                     UploadStatus = s.Upload_Status.ToString() //mkaran2  : Adding UploadStatus : MITS 33747
                                     //tanwar2 - ImageRight - start
                                     ,IRUpload = s.bIRUploadStatus?"Yes":"-"
                                     //tanwar2 - ImageRight - end
                                 };


                    doc = new List<DocumentDatas>();
                    if (dcUpfolder != null)
                    {
                        doc.Add(dcUpfolder);
                    }
                    doc.AddRange(query2.ToList());
                    doc.AddRange(query1.ToList());
                    Psid.Value = Model.Psid;
                    FolderId.Value = Model.Pid;

                    if (Model.Name.Equals("Attachments"))
                    {
                        FolderName.Value = GetResourceValue("lblFolderName1", "0"); //tmalhotra3:MITS 31222
                    }
                    else if (Model.Name.Equals("My Documents"))
                    {
                        FolderName.Value = GetResourceValue("lblFolderName2", "0");
                    }
                    else
                    {
                        FolderName.Value = Model.Name;
                    }

                    if (AppHelper.GetValue("formsubtitle") != string.Empty)
                    {   // pen testing changes:atavaragiri MITS 27871
                        //lblFolder.Text = AppHelper.GetValue("formsubtitle");
                        //hdn_lblFolder.Value = AppHelper.GetValue("formsubtitle");
                        if (!String.IsNullOrEmpty(AppHelper.GetValue("formsubtitle")))
                            lblFolder.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle").Replace("^AND^", "&"));//srajindersin MITS 32260
                        if (!String.IsNullOrEmpty(AppHelper.GetValue("formsubtitle")))
                            //apeykov RMA-9719 Start
                            hdn_lblFolder.Value = HTMLCustomLblFolderEncode(HTMLCustomLblFolderDecode(AppHelper.GetValue("formsubtitle")));
                            //  hdn_lblFolder.Value = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle").Replace("^AND^", "&"));//srajindersin MITS 32260
                            //apeykov RMA-9719 End
                        // END:pen testing changes :atavaragiri
                    }
                    else if (hdn_lblFolder != null && hdn_lblFolder.Value != string.Empty)
                    {
                        // pen testing changes:atavaragiri MITS 27871
                        //lblFolder.Text = (hdn_lblFolder.Value;
                        lblFolder.Text = AppHelper.HTMLCustomEncode(hdn_lblFolder.Value);
                        //END: pen testing
                    }
                    else
                    {
                        lblFolder.Text = Model.Name;
                    }

                    //tanwar2 - ImageRight - start
                    hdnUseIR.Value = Model.UseImageRight.ToString();
                    hdnUseIRWS.Value = Model.UseImageRightWS.ToString();
                    //tanwar2 - ImageRight  -end
                    
                    if (Model.Documents.Count == 0 && Model.Folders.Count == 0)
                    {
                        //lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                        //lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;
                        lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                        lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                        DataTable dt = new DataTable();
                        DataColumn dc = new DataColumn();
                        dc.ColumnName = "Id";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Type";
                        dt.Columns.Add(dc);


                        dc = new DataColumn();
                        dc.ColumnName = "Message";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Title";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Name";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Subject";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "DocumentType";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Class";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "Category";
                        dt.Columns.Add(dc);

                        dc = new DataColumn();
                        dc.ColumnName = "CreateDate";
                        dt.Columns.Add(dc);

                        //smishra25:Adding filesize
                        dc = new DataColumn();
                        dc.ColumnName = "FileSize";
                        dt.Columns.Add(dc);

                        //mkaran2  : Adding UploadStatus : MITS 33747
                        dc = new DataColumn();
                        dc.ColumnName = "UploadStatus";
                        dt.Columns.Add(dc);
                        //tanwar2 - ImageRight - start
                        dc = new DataColumn();
                        dc.ColumnName = "IRUpload";
                        dt.Columns.Add(dc);
                        //tanwar2 - ImageRight - end

                        DataRow dr = dt.NewRow();
                        if (dcUpfolder != null)
                        {
                            dr["Type"] = "Upfolder";
                            dr["Id"] = "-922";

                            if (screenflag != "Files")
                            {
                                //dr["Message"] = "<i>No documents attached. Click Add Document to add new documents.</i>";
                                dr["Message"] = "<i>" + GetResourceValue("msgNoDocument", "0") + "</i>";
                            }
                            else if (screenflag == "Files" && Model.Folders.Count == 0)
                            {
                                //dr["Message"] = "<i>This folder is empty. Click Add New to add new documents.</i>";
                                dr["Message"] = "<i>" + GetResourceValue("msgEmptyFolder", "0") + "</i>";
                            }
                            dt.Rows.Add(dr);
                            GridView1.DataSource = dt;
                            GridView1.DataBind();

                        }
                        if (screenflag != "Files")
                        {
                            //dr["Message"] = "<i>No documents attached. Click Add Document to add new documents.</i>";
                            dr["Message"] = "<i>" + GetResourceValue("msgNoDocument", "0") + "</i>";
                            dt.Rows.Add(dr);
                            bIsEmptyMessage = true;
                            GridView1.DataSource = dt;
                            GridView1.DataBind();
                        }

                    }
                    else
                    {
                        GridView1.DataSource = doc;
                        GridView1.DataBind();

                    }

                }
            }

            catch (FaultException<Riskmaster.Models.RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            m_AddDoc = false;               //Added:Yukti,MITs 30990,DT:11/13/2013
            Page.DataBind();
            return doc;
        }
        void AddSortImage(GridViewRow headerRow)
        {
            if ((Conversion.ConvertStrToInteger(m_strSortExp) >= 7))
                m_strSortExp = ((Conversion.ConvertStrToInteger(m_strSortExp) - 6)).ToString();
            Int32 iCol = GetSortColumnIndex(m_strSortExp);
            if (-1 == iCol)
            {
                return;
            }
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if ("Ascending" == m_SortDirection)
            {
                sortImage.ImageUrl = "~/Images/arrow_down_white.gif";
                //sortImage.AlternateText = "Ascending Order";
                sortImage.AlternateText = GetResourceValue("sortImageAscResrc", "0");
            }
            else
            {
                sortImage.ImageUrl = "~/Images/arrow_up_white.gif";
                //sortImage.AlternateText = "Descending Order";
                sortImage.AlternateText = GetResourceValue("sortImageDesResrc", "0");
            }

            // Add the image to the appropriate header cell.
            headerRow.Cells[iCol].Controls.Add(sortImage);

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DocumentList.aspx"), "DocumentListValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DocumentListValidationScripts", sValidationResources, true);
                //Added:Yukti,Dt:11/14/2013,MITS 30990
                if (Request.QueryString["DocList"] != null)
                {
                    if (Request.QueryString["DocList"].ToLower() == "true" && (!Page.IsPostBack)) //tmalhotra3- MITS 36452
                    {
                        bCheckStatus = true;
                    }
                }
                //Ended:Yukti,MITS 30990

                if (!string.IsNullOrEmpty(Request.QueryString["RedirectUrl"]))
                {
                    string strURL = Request.QueryString["RedirectUrl"];
                    strURL = strURL.Replace('^', '&');
                    //Response.Redirect(strURL,false);
                    Server.Transfer(strURL);
                }
                if (AppHelper.GetValue("flag") == "")
                {
                    //Commented by Nitin for Mits 21612 for adding Paging for Attachments
                    //lnkFirstTop.Visible=false;
                    //lnkLastTop.Visible=false;
                    //lnkPrevTop.Visible=false;
                    //lnkNextTop.Visible=false;
                    //lnkFirstDown.Visible=false;
                    //lnkLastDown.Visible=false;
                    //lnkPrevDown.Visible=false;
                    //lnkNextDown.Visible=false;
                    //lblPageRangeTop.Visible = false;
                    //lblPageRangeDown.Visible = false;
                    //lblSpace.Visible = false;
                    //lnkRefresh.Visible = false;
                }
                if (!Page.IsPostBack)
                {
                    //Added:Yukti,Dt:11/14/2013,MITS 30990
                    //if (Request.QueryString["DocList"] != null)
                    //{
                    //    if (Request.QueryString["DocList"] == "true")
                    //    {
                    //        bCheckStatus = true;
                    //    }
                    //}
                    //Ended:Yukti,MITS 30990
                    // akaushik5 Changed for MITS 36901 Starts
                    //hdnSortVar.Value = "True";           //added:Yukti,MITS 34595,DT:12/11/2013
                    // akaushik5 Changed for MITS 36901 Ends
                    SetInitialValues();

                    List<DocumentDatas> doc = new List<DocumentDatas>();
                    // akaushik5 Changed for MITS 37157 Starts
                    //doc = BindData("", AppHelper.GetFormValue("hdnPageNumber"));
                    doc = BindData(AppHelper.GetFormValue("hdnSortExpression"), AppHelper.GetFormValue("hdnPageNumber"));
                    // akaushik5 Changed for MITS 37157 Ends
                    if (Model.JumpToAcrosoft != "1")
                    {
                        if (!Page.IsPostBack)
                        {
                            lnkFirstTop.Enabled = true;
                            lnkLastTop.Enabled = true;
                            lnkPrevTop.Enabled = true;
                            lnkNextTop.Enabled = true;
                            lnkFirstDown.Enabled = true;
                            lnkLastDown.Enabled = true;
                            lnkPrevDown.Enabled = true;
                            lnkNextDown.Enabled = true;
                            if (Page.PreviousPage != null)
                            {
                                //lblPageRangeTop.Text = "Page " + hdnPageNumber.Value + " of " + hdTotalPages.Value;
                                //lblPageRangeDown.Text = "Page " + hdnPageNumber.Value + " of " + hdTotalPages.Value;
                                lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + hdnPageNumber.Value + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                                lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + hdnPageNumber.Value + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                            }
                            else
                            {
                                //lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                                //lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;
                                lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                                lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;

                                if (hdTotalPages.Value == "1") // There is only 1 page.
                                {
                                    lnkFirstTop.Enabled = false;
                                    lnkLastTop.Enabled = false;
                                    lnkPrevTop.Enabled = false;
                                    lnkNextTop.Enabled = false;
                                    lnkFirstDown.Enabled = false;
                                    lnkLastDown.Enabled = false;
                                    lnkPrevDown.Enabled = false;
                                    lnkNextDown.Enabled = false;
                                }
                                else
                                {
                                    lnkFirstTop.Enabled = false;
                                    lnkPrevTop.Enabled = false;
                                    lnkFirstDown.Enabled = false;
                                    lnkPrevDown.Enabled = false;
                                }
                            }
                            if (Page.PreviousPage != null)
                            {
                                if (Conversion.ConvertStrToInteger(hdnPageNumber.Value) == 1)
                                {
                                    lnkFirstTop.Enabled = false;
                                    lnkPrevTop.Enabled = false;
                                    lnkFirstDown.Enabled = false;
                                    lnkPrevDown.Enabled = false;

                                }
                                if (Conversion.ConvertStrToInteger(hdnPageNumber.Value) == Conversion.ConvertStrToInteger(hdTotalPages.Value))
                                {
                                    lnkLastTop.Enabled = false;
                                    lnkNextTop.Enabled = false;
                                    lnkLastDown.Enabled = false;
                                    lnkNextDown.Enabled = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (hdnLoadImageRight.Value == "1")
                    {
                        GetImageRightDocuments();
                    }
                    else
                    {
                        ProcessPostBack();
                        if (Page.FindControl("hdnEmailButtonClick") != null)
                        {
                            HiddenField hdnEmailClick = (HiddenField)Page.FindControl("hdnEmailButtonClick");
                            if (string.Compare(hdnEmailClick.Value, "1") == 0)
                            {

                            StringBuilder sbDocIds = new StringBuilder();
                            StringBuilder sbFileNames = new StringBuilder();
                            DocumentBusinessHelper dc = new DocumentBusinessHelper();
                            try
                            {
                                if (AppHelper.GetFormValue("hdocId") != "")
                                {

                                    HiddenField hdnFileNames = (HiddenField)Page.FindControl("hdnFileNames");
                                    string[] sFileNames = hdnFileNames.Value.Split(',');
                                    //rsharma220 MITS 35261 : File attributes index changed
                                    for (int i = 0; i < sFileNames.Length / 5; i++)
                                    {
                                        if (!string.IsNullOrEmpty(sFileNames[i * 5]) && !string.IsNullOrEmpty(sFileNames[i * 5 + 3]))
                                        {


                                            ViewData = dc.Download(Conversion.ConvertStrToInteger(sFileNames[i * 5]));
                                            sbDocIds.Append(sFileNames[i * 5]);
                                            sbDocIds.Append("~");
                                            //Start rsushilaggar MITS 22040 Date 02/03/2011 Merging the fdf file with pdf to email pdf file.
                                            if (sFileNames[i * 5 + 3].Contains(".fdf"))
                                            {
                                                sFileNames[i * 5 + 3] = sFileNames[i * 5 + 3].Substring(0, sFileNames[i * 5 + 3].LastIndexOf('.')) + ".pdf";
                                            }
                                            //End rsushilaggar
                                            //rsharma220 MITS 36128
                                            if (sFileNames[i * 5 + 2].CompareTo("@@^^rmA^^@@") == 0)
                                            {
                                                sFileNames[i * 5 + 2] = "rmA - " + sFileNames[i * 5 + 3];
                                            }
                                            sbDocIds.Append(sFileNames[i * 5 + 3]);
                                            sbDocIds.Append(",");
                                            System.Web.UI.HtmlControls.HtmlInputHidden hdnFileContent = new System.Web.UI.HtmlControls.HtmlInputHidden();
                                            hdnFileContent.ID = "hdnFileContent" + i.ToString();
                                            hdnFileContent.Value = Convert.ToBase64String(ViewData.FileContents);
                                            Page.Controls.Add(hdnFileContent);

                                        }


                                    }
                                    if (Page.FindControl("hdnformfilecontent") != null)
                                    {
                                        HiddenField hdnFile = (HiddenField)Page.FindControl("hdnformfilecontent");
                                        hdnFile.Value = sbDocIds.ToString();
                                    }

                                }
                                // akaushik5 Added for RMA-8465 Starts 
                                Page.DataBind();
                                // akaushik5 Added for RMA-8465 Ends
                            }
                            catch (FaultException<Riskmaster.Models.RMException> ee)
                            {
                                ErrorHelper.logErrors(ee);
                                ErrorControl1.errorDom = ee.Detail.Errors;
                            }
                            catch (Exception ee)
                            {
                                ErrorHelper.logErrors(ee);
                                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

                            }
                        }

                        }
                    }
                    
                }
               
            }
            catch (FaultException<Riskmaster.Models.RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            bool bFileStillUploading = false;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HiddenField id = (HiddenField)e.Row.Cells[0].FindControl("hdnId");
                HiddenField Type = (HiddenField)e.Row.Cells[0].FindControl("hdnType");
                ImageButton img = (ImageButton)e.Row.Cells[1].FindControl("img");
                Image imgProgress = (Image)e.Row.Cells[1].FindControl("imgProgress");
                imgProgress.Visible = false;
                if (Conversion.ConvertStrToInteger(id.Value) == -1 && Type.Value == "F")
                {

                    img.ImageUrl = "~/Images/Document/recycle.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");

                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = Name.Value;

                }
                else if (Conversion.ConvertStrToInteger(id.Value) > 0 && Type.Value == "F")
                {

                    img.ImageUrl = "~/Images/Document/folder.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = Name.Value;
                }
                if (Type.Value == "D")
                {
                    HiddenField FileSize = (HiddenField)e.Row.Cells[0].FindControl("hdnFileSize");
                    Label lblFileSize = (Label)e.Row.FindControl("lblFileSize");
                    lblFileSize.Visible = true;
                    //if (FileSize.Value.IndexOf("-1") > -1)
                    //{

                    //    lblFileSize.Visible = false;
                    //    imgProgress.Visible = true; ;
                    //    imgProgress.ImageUrl = "~/Images/loading.gif";
                    //    HiddenField hdnCounterShow = (HiddenField)Page.FindControl("hdnCounterShow");
                    //    hdnCounterShow.Value = "1";
                    //    e.Row.Enabled = false;

                    //}
                    //img.ImageUrl = "~/Images/Document/doc.gif";
                    //img.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "','" + FileSize.Value.Replace("MB", "").Trim() + "')");
                    //HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    //HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    //HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    //link.Text = title.Value + "(" + Name.Value + ")";
                    ////sgupta243 MITS 22213 Date 09/14/2012
                    //if (title.Value == "SROI")
                    //{
                    //    link.Text = Name.Value;
                    //}

                    //if (FileSize.Value.IndexOf("-1") > -1)
                    //{
                    //}
                    //else
                    //{
                    //    link.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "','" + FileSize.Value.Replace("MB", "").Trim() + "')");
                    //}   

                    // mkaran2 : Adding UploadStatus : MITS 33747 : Start
                    HiddenField UploadStatus = (HiddenField)e.Row.Cells[0].FindControl("hdnUploadStatus");
                    if (UploadStatus.Value == "3" || IsErrorOccuredInUpload(id.Value))
                    {
                        img.ImageUrl = "~/Images/Document/doc.gif";
                        img.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "','" + FileSize.Value.Replace("MB", "").Trim() + "')");

                        HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                        HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");

                        HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                        link.Text = title.Value + "(" + Name.Value + ")";
                        link.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "','" + FileSize.Value.Replace("MB", "").Trim() + "')");
                        link.ForeColor = System.Drawing.Color.Red;

                        if (title.Value == "SROI")
                        {
                            link.Text = Name.Value;
                        }
                    }
                    else
                    {

                        if (FileSize.Value.IndexOf("-1") > -1)
                        {

                            lblFileSize.Visible = false;
                            imgProgress.Visible = true; ;
                            imgProgress.ImageUrl = "~/Images/loading.gif";
                            HiddenField hdnCounterShow = (HiddenField)Page.FindControl("hdnCounterShow");
                            hdnCounterShow.Value = "1";
                            e.Row.Enabled = false;

                        }
                        img.ImageUrl = "~/Images/Document/doc.gif";
                        img.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "','" + FileSize.Value.Replace("MB", "").Trim() + "')");
                        HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                        HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                        HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                        link.Text = title.Value + "(" + Name.Value + ")";
                        link.ForeColor = System.Drawing.Color.Blue;
                        //sgupta243 MITS 22213 Date 09/14/2012
                        if (title.Value == "SROI")
                        {
                            link.Text = Name.Value;
                        }

                        if (FileSize.Value.IndexOf("-1") > -1)
                        {
                        }
                        else
                        {
                            link.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "','" + FileSize.Value.Replace("MB", "").Trim() + "')");
                        }
                    }
                    // mkaran2 : Adding UploadStatus : MITS 33747 : End                          
                }

                if (Type.Value == "Upfolder")
                {

                    img.ImageUrl = "~/Images/Document/upfolder.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + ParentFolder.Value + "')");


                }
                if (Type.Value == "F")
                {
                    HyperLink link1 = (HyperLink)e.Row.FindControl("linkName");
                    link1.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                }
                if (bIsEmptyMessage == true)
                {
                    img.Visible = false;
                }
            }
            //tanwar2 - ImageRight - start
            if (string.Compare(hdnUseIR.Value, "True", true) != 0)
            {
                e.Row.Cells[9].Visible = false;
            }
            //tanwar2 - ImageRight - end
        }



        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            m_strSortExp = e.SortExpression;

            //Added:Yukti,Dt:12/11/2013,MITS 34595
            // bCheckStatus = false;   //yukti, MITS 30990, DT:12/05/2013
            // akaushik5 Changed for MITS 36901 Starts
            //if (e.SortExpression == "1")
            //{
            //    bCheckStatus = false;
            //    bSortAlpha = true;   
            //}
            //else if (e.SortExpression == "6")
            //{
            //    bCheckStatus = false;
            //    bSortDt = true;
            //}

            if (!object.ReferenceEquals(hdnSortExpression, null))
            {
                if (hdnSortExpression.Value.Equals(e.SortExpression))
                {
                    this.GridViewSortDirection = this.GridViewSortDirection.Equals(SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
                }
                else
                {
                    this.hdnSortExpression.Value = e.SortExpression;
                    this.GridViewSortDirection = SortDirection.Ascending;
                }
            }
            // akaushik5 Changed for MITS 36901 Ends
            //Ended:Yukti,Dt:12/11/2013,MITS 34595
            BindData(e.SortExpression, hdnPageNumber.Value);

        }

        private int GetSortColumnIndex(String strCol)
        {
            foreach (DataControlField field in GridView1.Columns)
            {
                if (field.SortExpression == strCol)
                {
                    return GridView1.Columns.IndexOf(field);
                }
            }

            return -1;
        }
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (String.Empty != m_strSortExp)
                {
                    AddSortImage(e.Row);
                }
            }
        }
        private void ProcessPostBack()
        {
            if (hdnIsMoveToDiffFolder.Value == "move")
            {
                List<DocumentDatas> doc = new List<DocumentDatas>();
                GridView1.PageIndex = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdnPageNumber"));
                BindData("", "1");
                //lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                //lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;
                lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                lnkFirstTop.Enabled = true;
                lnkLastTop.Enabled = true;
                lnkPrevTop.Enabled = true;
                lnkNextTop.Enabled = true;
                lnkFirstDown.Enabled = true;
                lnkLastDown.Enabled = true;
                lnkPrevDown.Enabled = true;
                lnkNextDown.Enabled = true;
                if (hdTotalPages.Value == "1") // There is only 1 page.
                {
                    lnkFirstTop.Enabled = false;
                    lnkLastTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkNextTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkLastDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                    lnkNextDown.Enabled = false;
                }
                else
                {
                    lnkFirstTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                }
                hdnIsMoveToDiffFolder.Value = "";
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Server.Transfer("ConfirmDelete.aspx", true);
        }
        protected void btnAddFolder_Click(object sender, EventArgs e)
        {
            try
            {
                if (FolderId.Value == "")
                    FolderId.Value = "0";
                DocumentBusinessHelper dc = new DocumentBusinessHelper();
                dc.CreateFolder(newfoldername.Value, FolderId.Value);
                List<DocumentDatas> doc = new List<DocumentDatas>();
                doc = BindData("", hdnPageNumber.Value);
                //lblPageRangeTop.Text = "Page " + hdnPageNumber.Value + " of " + hdTotalPages.Value;
                //lblPageRangeDown.Text = "Page " + hdnPageNumber.Value + " of " + hdTotalPages.Value;
                lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + hdnPageNumber.Value + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + hdnPageNumber.Value + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                lnkFirstTop.Enabled = true;
                lnkLastTop.Enabled = true;
                lnkPrevTop.Enabled = true;
                lnkNextTop.Enabled = true;
                lnkFirstDown.Enabled = true;
                lnkLastDown.Enabled = true;
                lnkPrevDown.Enabled = true;
                lnkNextDown.Enabled = true;
                if (hdTotalPages.Value == "1") // There is only 1 page.
                {
                    lnkFirstTop.Enabled = false;
                    lnkLastTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkNextTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkLastDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                    lnkNextDown.Enabled = false;
                }
                else
                {
                    lnkFirstTop.Enabled = false;
                    lnkPrevTop.Enabled = false;
                    lnkFirstDown.Enabled = false;
                    lnkPrevDown.Enabled = false;
                }
                if (Conversion.ConvertStrToInteger(hdnPageNumber.Value) < Conversion.ConvertStrToInteger(hdTotalPages.Value))
                {
                    lnkFirstTop.Enabled = true;
                    lnkLastTop.Enabled = true;
                    lnkPrevTop.Enabled = true;
                    lnkNextTop.Enabled = true;
                    lnkFirstDown.Enabled = true;
                    lnkLastDown.Enabled = true;
                    lnkPrevDown.Enabled = true;
                    lnkNextDown.Enabled = true;
                }

            }

            catch (FaultException<Riskmaster.Models.RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void LinkCommand_Refresh(object sender, CommandEventArgs e)
        {
            BindData("", hdnPageNumber.Value);
        }
        protected void LinkCommand_Click(object sender, CommandEventArgs e)
        {


            int iCurrentPage = 0;
            int iPreviousPage = 0;
            string pagenum = "";
            //added:Yukti,MITS 34595,Dt:12/11/2013
            string srtexpression = "";
            m_AddDoc = false;
            //Ended:Yukti,DT:12/11/2013


            lnkFirstTop.Enabled = true;
            lnkLastTop.Enabled = true;
            lnkPrevTop.Enabled = true;
            lnkNextTop.Enabled = true;
            lnkFirstDown.Enabled = true;
            lnkLastDown.Enabled = true;
            lnkPrevDown.Enabled = true;
            lnkNextDown.Enabled = true;

            //added:Yukti,MITS 34595,Dt:12/11/2013
            // akaushik5 Changed for MITS 36901 Starts
            //if (hdnSortVar.Value == "True")
            //{
            //    srtexpression = "1";
            //}

            //if (hdnSortExpression.Value != null && bSortAlpha == true)
            //{
            //    srtexpression = "1";
            //}
            //else if (hdnSortExpression.Value != null && bSortDt == true)
            //{
            //    srtexpression = "6";
            //}
            //else
            //{
            //    hdnSortExpression.Value = "00";
            //}
            srtexpression = hdnSortExpression.Value;
            // akaushik5 Changed for MITS 36901 Ends
            //Ended MITS 34595

            switch (e.CommandName.ToUpper())
            {
                case "FIRST":
                    {

                        iCurrentPage = 0;

                        //lblPageRangeTop.Text = "Page 1" + " of " + hdTotalPages.Value;
                        //lblPageRangeDown.Text = "Page 1" + " of " + hdTotalPages.Value;
                        lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                        lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " 1 " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;

                        lnkFirstTop.Enabled = false;
                        lnkPrevTop.Enabled = false;
                        lnkFirstDown.Enabled = false;
                        lnkPrevDown.Enabled = false;

                        pagenum = "1";
                        break;
                    }
                case "LAST":
                    {

                        iCurrentPage = Conversion.ConvertStrToInteger(hdTotalPages.Value) - 1;

                        //lblPageRangeTop.Text = "Page " + hdTotalPages.Value + " of " + hdTotalPages.Value;
                        //lblPageRangeDown.Text = "Page " + hdTotalPages.Value + " of " + hdTotalPages.Value;
                        lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + hdTotalPages.Value + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                        lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + hdTotalPages.Value + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;


                        lnkLastTop.Enabled = false;
                        lnkNextTop.Enabled = false;
                        lnkLastDown.Enabled = false;
                        lnkNextDown.Enabled = false;

                        pagenum = hdTotalPages.Value;
                        break;
                    }
                case "PREV":
                    {

                        iCurrentPage = Conversion.ConvertStrToInteger(hdnPageNumber.Value);


                        iPreviousPage = iCurrentPage - 1;

                        if (iPreviousPage == 1)
                        {
                            lnkFirstTop.Enabled = false;
                            lnkPrevTop.Enabled = false;
                            lnkFirstDown.Enabled = false;
                            lnkPrevDown.Enabled = false;
                        }


                        //lblPageRangeTop.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Value;
                        //lblPageRangeDown.Text = "Page " + iPreviousPage + " of " + hdTotalPages.Value;
                        lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + iPreviousPage + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                        lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + iPreviousPage + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;

                        //changed by Nitin for Mits 21612 as Next , Prev buttons were not running fine
                        pagenum = (iCurrentPage - 1).ToString();
                        break;
                    }
                case "NEXT":
                    {

                        iCurrentPage = Conversion.ConvertStrToInteger(hdnPageNumber.Value);

                        if ((iCurrentPage + 1) == Conversion.ConvertObjToInt(hdTotalPages.Value))
                        {
                            lnkLastTop.Enabled = false;
                            lnkNextTop.Enabled = false;
                            lnkLastDown.Enabled = false;
                            lnkNextDown.Enabled = false;
                        }

                        //lblPageRangeTop.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Value;
                        //lblPageRangeDown.Text = "Page " + (iCurrentPage + 1) + " of " + hdTotalPages.Value;
                        lblPageRangeTop.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + (iCurrentPage + 1) + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;
                        lblPageRangeDown.Text = GetResourceValue("lblPageRange1Resrc", "0") + " " + (iCurrentPage + 1) + " " + GetResourceValue("lblPageRange2Resrc", "0") + " " + hdTotalPages.Value;

                        //changed by Nitin for Mits 21612 as Next , Prev buttons were not running fine
                        pagenum = (iCurrentPage + 1).ToString();
                        break;
                    }
            }

            //added:Yukti,DT:12/11/2013, MITS 34595
            // BindData("", pagenum);
            BindData(srtexpression, pagenum);
            //Ended:Yukti,DT:12/11/2013, MITS 34595
        }
        /// <summary>
        /// Function to Refresh the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGridRefresh_Click(object sender, EventArgs e)
        {
            HiddenField hdnCounterShow = (HiddenField)Page.FindControl("hdnCounterShow");
            hdnCounterShow.Value = "0";
            List<DocumentDatas> doc = new List<DocumentDatas>();
            doc = BindData("", AppHelper.GetFormValue("hdnPageNumber"));
        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("DocumentList.aspx"), strResourceType).ToString();
        }
        //mkaran2 : MITS 33747 : Start
        private bool IsErrorOccuredInUpload(string p_sDocumentId)
        {
            string errorFile = string.Empty;
            errorFile = Server.MapPath("~/UploaderTemp/") + p_sDocumentId + "_error";

            if (File.Exists(errorFile))
                return true;
            else
                return false;
        }
        //mkaran2 : MITS 33747 : End
        //tanwar2 - ImageRight interface - start
        protected void OnApproveClicked(object sender, EventArgs e)
        {
            string sOutXml = AppHelper.CallCWSService(GetApproveMessageTemplate());
            hdocId.Value = string.Empty;
            BindData(string.Empty, hdnPageNumber.Value);

            bool bError = false;
            XmlDocument xDom = new XmlDocument();
            xDom.LoadXml(sOutXml);
            XmlNode xNode = xDom.SelectSingleNode("//MsgStatusCd");
            if (xNode!=null && string.Compare(xNode.InnerText,"Error",true) == 0)
            {
                bError = true;
                xNode = xDom.SelectSingleNode("//ExtendedStatusDesc");
                if (xNode!=null)
                {
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(new Exception(xNode.InnerText), BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
            else
            {
                hdnSelectImgRightTab.Value = "1";
            }
        }

        public void GetImageRightDocuments()
        {
            grdDoc.Rebind();
            hdnSortExpression.Value = "6";
            BindData("6", hdnPageNumber.Value);
        }

        protected void grdDoc_OnNeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //No rebind on initial load.
            //We need to bind only when the new tab is selected.
            if (e.RebindReason != Telerik.Web.UI.GridRebindReason.InitialLoad)
            {
                if (string.Compare(hdnUseIRWS.Value, "True", true) == 0 && string.Compare(AttachTableName.Value, "claim", true) == 0)
                {
                    string sReturnMsg = string.Empty;
                    bool bReturnValue = false;
                    XmlDocument xDoc = new XmlDocument();
                    sReturnMsg = AppHelper.CallCWSService(GetIRDocumentTemplate());// CallCWS("DocumentMapAdaptor.Get", GetDefaultTemplate(), out sReturnMsg, false, false);
                    //if (bReturnValue)
                    {
                        DataSet oDocDataSet = new DataSet();
                        DataTable oDocDataTable = oDocDataSet.Tables.Add("Documents");
                        DataRow oDocDataRow = null;

                        oDocDataTable.Columns.Add("DocId", typeof(string));
                        oDocDataTable.Columns.Add("Description", typeof(string));
                        oDocDataTable.Columns.Add("CreateDate", typeof(DateTime));
                        oDocDataTable.Columns.Add("DocType", typeof(string));
                        oDocDataTable.Columns.Add("Pages", typeof(string));

                        xDoc.LoadXml(sReturnMsg);

                        foreach (XmlNode xNode in xDoc.SelectNodes("//Doc"))
                        {
                            oDocDataRow = oDocDataTable.NewRow();
                            if (xNode.SelectSingleNode("ID") != null)
                            {
                                oDocDataRow["DocId"] = xNode.SelectSingleNode("ID").InnerText;
                            }
                            if (xNode.SelectSingleNode("Description") != null)
                            {
                                if (!string.IsNullOrEmpty(xNode.SelectSingleNode("Description").InnerText))
                                {
                                    oDocDataRow["Description"] = xNode.SelectSingleNode("Description").InnerText;
                                }
                                else
                                {
                                    oDocDataRow["Description"] = "{No Description}";
                                }
                            }
                            if (xNode.SelectSingleNode("CreateDate") != null)
                            {
                                if (!string.IsNullOrEmpty(xNode.SelectSingleNode("CreateDate").InnerText))
                                {
                                    oDocDataRow["CreateDate"] = Convert.ToDateTime(xNode.SelectSingleNode("CreateDate").InnerText);
                                }
                            }
                            if (xNode.SelectSingleNode("DocType") != null)
                            {
                                oDocDataRow["DocType"] = xNode.SelectSingleNode("DocType").InnerText;
                            }
                            if (xNode.SelectSingleNode("Pages") != null)
                            {
                                oDocDataRow["Pages"] = xNode.SelectSingleNode("Pages").InnerText;
                            }

                            oDocDataTable.Rows.Add(oDocDataRow);
                        }

                        oDocDataTable.DefaultView.Sort = "CreateDate DESC";

                        grdDoc.DataSource = oDocDataTable.DefaultView.ToTable();

                        oDocDataTable.Dispose();
                        oDocDataTable.Dispose();
                    }
                }
            }
        }

        private string GetApproveMessageTemplate()
        {
            StringBuilder sbTemplate = new StringBuilder("<Message>");
            sbTemplate.Append("<Authorization />");
            sbTemplate.Append("<Call><Function>ImageRightAdaptor.ApproveDoc</Function></Call><Document><Data>");
            sbTemplate.Append("<DocumentIds>");
            sbTemplate.Append(hdocId.Value.Trim(','));
            sbTemplate.Append("</DocumentIds>");
            sbTemplate.Append("<ClaimId>");
            sbTemplate.Append(AttachRecordId.Value);
            sbTemplate.Append("</ClaimId></Data></Document></Message>");

            return sbTemplate.ToString();
        }

        private string GetIRDocumentTemplate()
        {
            StringBuilder sbTemplate = new StringBuilder("<Message>");
            sbTemplate.Append("<Authorization />");
            sbTemplate.Append("<Call><Function>ImageRightAdaptor.GetDoc</Function></Call><Document><Data>");
            sbTemplate.Append("<ClaimId>");
            sbTemplate.Append(AttachRecordId.Value);
            sbTemplate.Append("</ClaimId></Data></Document></Message>");

            return sbTemplate.ToString();
        }
        //tanwar 2- ImageRight interface - end

        //apeykov RMA-9719 Start
        private static string HTMLCustomLblFolderEncode(string inputString)
        {
            string encodedString = string.Empty;

            encodedString = System.Web.HttpUtility.HtmlEncode(inputString);

            while (encodedString.Contains("&amp;"))
                encodedString = encodedString.Replace("&amp;", "&");

            if (encodedString.Contains("&"))
                encodedString = encodedString.Replace("&", "^@");
            if (encodedString.Contains("#"))
                encodedString = encodedString.Replace("#", "*@");
            if (encodedString.Contains("'"))
                encodedString = encodedString.Replace("'", "@*^");
            return encodedString;
        }

        private static string HTMLCustomLblFolderDecode(string inputString)
        {
            string decodedString = string.Empty;
            decodedString = System.Web.HttpUtility.HtmlDecode(inputString);

            if (decodedString.Contains("^AND^"))
                decodedString = decodedString.Replace("^AND^", "&");
            if (decodedString.Contains("^@"))
                decodedString = decodedString.Replace("^@", "&");
            if (decodedString.Contains("*@"))
                decodedString = decodedString.Replace("*@", "#");
            if (decodedString.Contains("@*^"))
                decodedString = decodedString.Replace("@*^", "'");
            return decodedString;
        }
        //apeykov RMA-9719 End
    }
}
