﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmDelete.aspx.cs" Inherits="Riskmaster.UI.Document.ConfirmDelete" EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" Tagname="ErrorControl" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="~/Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link href="~/Content/system.css" rel="stylesheet" type="text/css" />
</head>
<body>
  
    <form id="form1" runat="server">
    <div>
    <table class="singleborder" align="center">
      <tr>
      <td colspan="2">
          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
             </td>
     </tr>
    <tr>
     <td class="ctrlgroup">
        <asp:Label ID="lblConfirmDelete" runat="server" Text="<%$ Resources:lblConfirmDeleteResrc %>" />
     </td>
    </tr>
    <tr>
     <td nowrap="" class="datatd">
        <asp:Label ID="lblDeleteSelection1Resrc" Font-Bold="true" runat="server" Text="<%$ Resources:lblDeleteSelection1Resrc %>" />
        <br /><br />
        <asp:Label ID="lblDeleteSelection2Resrc" Font-Bold="true" runat="server" Text="<%$ Resources:lblDeleteSelection2Resrc %>" />
        <br />
    </td>
    </tr>
    <tr>
     <td align="center" nowrap="">
        
         <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" Text="<%$ Resources:btnDeleteResrc %>" CssClass="button" />
         <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" Text="<%$ Resources:btnCancelResrc %>" CssClass="button" />
        
         <asp:HiddenField ID="FolderId" runat="server" />
         <asp:HiddenField ID="hfolderId" runat="server" />
           <!--  MITS 23094 skhare7-->
        <asp:HiddenField ID="hdocIdDeleteLIst" runat="server" />
          <asp:HiddenField ID="hdocId" runat="server" />
         <asp:HiddenField ID="Psid" runat="server" />
         <asp:HiddenField ID="flag" runat="server" />
         <asp:HiddenField ID="hdnPageNumber" runat="server" />
         <asp:HiddenField ID="hdnSortExpression" runat="server" />
           <asp:HiddenField ID="AttachTableName" runat="server" />
     <asp:HiddenField ID="AttachRecordId" runat="server" />
      <asp:HiddenField ID="FormName" runat="server"/>
      <asp:HiddenField ID="Regarding" runat="server"/>
        <asp:HiddenField ID="NonMCMFormName" runat="server" />
        </td>
        </tr>
   </table>
   
  
    </div>
    </form>
</body>
</html>
