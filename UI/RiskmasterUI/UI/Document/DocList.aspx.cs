﻿using System;
using System.Collections.Generic;
//using Riskmaster.UI.DocumentService;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Data;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;
using System.Linq;
namespace Riskmaster.UI.Document
{
    public class DocumentData
    {
        public string Name
        {
            get;
            set;

        }
        public string Type
        {
            get;
            set;

        }
        public string CreateDate
        {
            get;
            set;

        }
        public string Category
        {
            get;
            set;

        }
        public string Title
        {
            get;
            set;

        }
        public string Class
        {
            get;
            set;

        }
        public string DocumentType
        {
            get;
            set;

        }
        public string Subject
        {
            get;
            set;

        }
        public int Id
        {
            get;
            set;

        }
        
    
    }
    public partial class DocList : System.Web.UI.Page
    {
        private string m_SortDirection = "";
        private bool  bIsEmptyMessage = false;
        private string m_strSortExp = "";
        public Riskmaster.Models.DocumentList Model = new Riskmaster.Models.DocumentList();
        private void SetInitialValues()
        {
            Regarding.Value = AppHelper.GetValue("Regarding");
            AttachTableName.Value = AppHelper.GetValue("AttachTableName");
            AttachRecordId.Value = AppHelper.GetValue("AttachRecordId");
            FormName.Value = AppHelper.GetValue("FormName");
            Psid.Value = AppHelper.GetValue("Psid");
            flag.Value = AppHelper.GetValue("flag");
            Regarding.Value = AppHelper.GetValue("Regarding");
        }
        private List<DocumentData> BindData(string exp)
        {
            List<DocumentData> doc = new List<DocumentData>();
              DocumentBusinessHelper docHelper = new DocumentBusinessHelper();
              try
              {  
                      if (exp == hdnSortExpression.Value)
                      {
                          exp = (6 + Conversion.ConvertObjToInt(exp)).ToString();
                      }
                    
                      if (exp != "")
                          hdnSortExpression.Value = exp;
                      else
                          exp = hdnSortExpression.Value;
                   
                        
                      if (Conversion.ConvertStrToInteger(exp) >= 1 && Conversion.ConvertStrToInteger(exp) <= 6)
                      {
                          m_SortDirection = "Ascending";
                      }
                      else
                      {
                          m_SortDirection = "Descending";
                      }
                   
                      if (Page.PreviousPage != null)
                      {
                          exp = AppHelper.GetFormValue("hdnSortExpression");
                          hdnSortExpression.Value = exp;
                      }
                      m_strSortExp = exp;
                     string flag=AppHelper.GetValue("flag");
                     string psid = AppHelper.GetValue("Psid");
                  //Added:Yukti,MITS 30990
                     //Model = docHelper.List(exp, flag, psid,"");
                     Model = docHelper.List(exp, flag, psid, "", false);         
                  //Ended:Yukti,:11/13/2013
                     Model.ScreenFlag = flag;
                  if (flag=="Files")
                  ParentFolder.Value = Model.TopFolder.FolderID;
                 
                  DocumentData dcUpfolder = null;
                  if (flag == "Files")
                  {
                      if (Model.Pid != "0")
                      {
                           dcUpfolder = new DocumentData();

                           dcUpfolder.Name = "";
                                   dcUpfolder.Id = -922;
                                   dcUpfolder.Type = "Upfolder";
                      }
                  }
                 
                  var query2 = from s1 in Model.Folders
                               select new DocumentData
                               {
                                   Name = s1.FolderName,
                                   Id = (int)Convert.ToInt64(s1.FolderID),
                                   Type = "F"

                               };
                  var query1 = from s in Model.Documents
                               select new DocumentData
                                {
                                    Name = s.FileName,
                                    Id = (int)Convert.ToInt64(s.Pid
                                    ),
                                    Type = "D",
                                    Subject = s.Subject,
                                    Class = s.DocumentClass,
                                    DocumentType = s.DocumentsType,
                                    Category = s.DocumentCategory,
                                    CreateDate = s.CreateDate,
                                    Title = s.Title

                                };


                  doc = new List<DocumentData>();
                  if (dcUpfolder != null)
                  {
                      doc.Add(dcUpfolder);
                  }
                  doc.AddRange(query2.ToList());
                  doc.AddRange(query1.ToList());
                  Psid.Value = Model.Psid;
                  FolderId.Value = Model.Pid;
                  FolderName.Value = Model.Name;
                  lblFolder.Text = Model.Name;
                  if (Model.Documents.Count == 0 && Model.Folders.Count == 0)
                  {
                      DataTable dt = new DataTable();
                      DataColumn dc = new DataColumn();
                      //dc.ColumnName = "Id";
                      dc.ColumnName = GetResourceValue("gvHdrId", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "Type";
                      dc.ColumnName = GetResourceValue("gvHdrType", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "Title";
                      dc.ColumnName = GetResourceValue("gvHdrTitle", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "Name";
                      dc.ColumnName = GetResourceValue("gvHdrName", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "Subject";
                      dc.ColumnName = GetResourceValue("gvHdrSubject", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "DocumentType";
                      dc.ColumnName = GetResourceValue("gvHdrDocumentType", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "Class";
                      dc.ColumnName = GetResourceValue("gvHdrClass", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "Category";
                      dc.ColumnName = GetResourceValue("gvHdrCategory", "0");
                      dt.Columns.Add(dc);

                      dc = new DataColumn();
                      //dc.ColumnName = "CreateDate";
                      dc.ColumnName = GetResourceValue("gvHdrCreateDate", "0");
                      dt.Columns.Add(dc);

                      DataRow dr=dt.NewRow();
                      if (dcUpfolder != null)
                      {
                          dr["Type"] = "Upfolder";
                          dr["Id"] = "-922";
                          if (flag != "Files")
                          {
                              //dr["Subject"] = "No documents attached. Click Add Document to add new documents.";
                              dr["Subject"] = GetResourceValue("gvNoDocumentAttached", "0");
                          }
                          else if (flag == "Files" && Model.Folders.Count == 0)
                          {
                              //dr["Subject"] = "This folder is empty. Click Add New to add new documents.";
                              dr["Subject"] = GetResourceValue("gvNoDocumentAttached", "0");
                          }
                          dt.Rows.Add(dr);
                          GridView1.DataSource = dt;
                          GridView1.DataBind();
                       
                      }
                      if (flag != "Files")
                      {
                          //dr["Subject"] = "No documents attached. Click Add Document to add new documents.";
                          dr["Subject"] = GetResourceValue("gvNoDocumentAttached", "0");
                          dt.Rows.Add(dr);
                          bIsEmptyMessage = true;
                          GridView1.DataSource = dt;
                          GridView1.DataBind();                        
                      }

                  }
                  else
                  {
                      if (!Page.IsPostBack)
                      {
                          GridView1.PageIndex = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdnPageNumber"));
                          hdnPageNumber.Value = GridView1.PageIndex.ToString();
                      }
                      GridView1.DataSource = doc;
                      GridView1.DataBind();
                  }

              }

              catch (FaultException<RMException> ee)
              {
                  ErrorHelper.logErrors(ee);
                  ErrorControl1.errorDom = ee.Detail.Errors;

              }
              catch (Exception ee)
              {
                  ErrorHelper.logErrors(ee);
                  BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                  err.Add(ee, BusinessAdaptorErrorType.SystemError);
                  ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

              }
                return doc;
        }
        void AddSortImage(GridViewRow headerRow)
        {
            if ((Conversion.ConvertStrToInteger(m_strSortExp) >= 7))
                m_strSortExp = ((Conversion.ConvertStrToInteger(m_strSortExp) - 6)).ToString();
            Int32 iCol = GetSortColumnIndex(m_strSortExp);
            if (-1 == iCol)
            {
                return;
            }
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if ("Ascending" == m_SortDirection)
            {
                sortImage.ImageUrl = "~/Images/arrow_down_white.gif";
                //sortImage.AlternateText = "Ascending Order";
                sortImage.AlternateText = GetResourceValue("ImgAscendingOrder", "0");
            }
            else
            {
                sortImage.ImageUrl = "~/Images/arrow_up_white.gif";
                //sortImage.AlternateText = "Descending Order";
                sortImage.AlternateText = GetResourceValue("ImgDescendingOrder", "0");
            }

            // Add the image to the appropriate header cell.
            headerRow.Cells[iCol].Controls.Add(sortImage);
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("DocList.aspx"), "DocListValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "DocListValidations", sValidationResources, true);

                if (!Page.IsPostBack)
                {
                    SetInitialValues();
                    
                    List<DocumentData> doc = new List<DocumentData>();
                    doc = BindData("");



                }
                else
                {
                    ProcessPostBack();
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox check = null;
                HiddenField id = (HiddenField)e.Row.Cells[0].FindControl("hdnId");
                HiddenField Type = (HiddenField)e.Row.Cells[0].FindControl("hdnType");
                ImageButton img = (ImageButton)e.Row.Cells[1].FindControl("img");
                if (Conversion.ConvertStrToInteger(id.Value) == -1 && Type.Value == "F")
                {
               
                    img.ImageUrl = "~/Images/Document/recycle.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                    check = (CheckBox)e.Row.Cells[0].FindControl("chkDoc");
                    check.Visible = false;
                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = Name.Value;

                }
                else if (Conversion.ConvertStrToInteger(id.Value) > 0 && Type.Value == "F")
                {
                   
                    img.ImageUrl = "~/Images/Document/folder.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = Name.Value;
                }
                if (Type.Value == "D")
                {
               
                    img.ImageUrl = "~/Images/Document/doc.gif";
                    img.Attributes.Add("onclick", "return GetDocument("+"'" + id.Value + "')");
                    HiddenField title = (HiddenField)e.Row.FindControl("hdnTitle");
                    HiddenField Name = (HiddenField)e.Row.FindControl("hdnName");
                    HyperLink link = (HyperLink)e.Row.FindControl("linkName");
                    link.Text = title.Value + "(" + Name.Value + ")";
                    link.Attributes.Add("onclick", "return GetDocument(" + "'" + id.Value + "')");
                }

                if (Type.Value == "Upfolder")
                {
               
                    img.ImageUrl = "~/Images/Document/upfolder.gif";
                    img.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + ParentFolder.Value + "')");
                    check = (CheckBox)e.Row.Cells[0].FindControl("chkDoc");
                    check.Visible = false;
                   
                }
                if (Type.Value == "F")
                {
                    HyperLink link1 = (HyperLink)e.Row.FindControl("linkName");
                    link1.Attributes.Add("onclick", "return GetDocumentsInformation('1','0','','" + id.Value + "')");
                }
                check = (CheckBox)e.Row.Cells[0].FindControl("chkDoc");
                if (Type.Value == "F")
                {
                    
                    check.Attributes.Add("onclick", "return checkFolder(this.id,"+id.Value+")");
                }
                else
                {
                    check.Attributes.Add("onclick", "return checkDoc(this.id," + id.Value + ")");
                }
                if (bIsEmptyMessage == true)
                {
                    check.Visible = false;
                    img.Visible = false;
                }
            }


        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            GridView1.PageIndex = e.NewPageIndex;
            hdnPageNumber.Value = e.NewPageIndex.ToString();
            BindData("");
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            m_strSortExp = e.SortExpression;
            BindData(e.SortExpression);
           
        }

        private int GetSortColumnIndex(String strCol)
        {
            foreach (DataControlField field in GridView1.Columns)
            {
                if (field.SortExpression == strCol)
                {
                    return GridView1.Columns.IndexOf(field);
                }
            }

            return -1;
        }
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (String.Empty != m_strSortExp)
                {
                    AddSortImage(e.Row);
                }
            }
        }
        private void ProcessPostBack()
        {
            if (Request.QueryString["type"] != null)
            {
                if (Request.QueryString["type"].ToString() == "move")
                {
                    List<DocumentData> doc = new List<DocumentData>();
                    GridView1.PageIndex = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("hdnPageNumber"));
                    BindData("");
                   
                   
                }
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Server.Transfer("ConfirmDelete.aspx", true);
        }
        protected void btnAddFolder_Click(object sender, EventArgs e)
        {
            try
            {
                if (FolderId.Value == "")
                    FolderId.Value = "0";
            DocumentBusinessHelper dc = new DocumentBusinessHelper();
            dc.CreateFolder(newfoldername.Value, FolderId.Value);
            List<DocumentData> doc = new List<DocumentData>();
            doc = BindData("");

            }

            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("DocList.aspx"), strResourceType).ToString();
        }


    }
}
