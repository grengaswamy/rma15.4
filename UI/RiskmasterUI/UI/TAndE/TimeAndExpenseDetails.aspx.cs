﻿using System;
using System.Data;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System.Threading;
using System.Globalization;
namespace Riskmaster.UI.TAndE
{
    public partial class TimeAndExpenseDetails : NonFDMBasePageCWS
    {
        private string sMode = String.Empty;

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeCulture();
            chkoverride.Attributes.Add("onclick",          "setDataChangedInv('chkoverride');");
            txtfdate.Attributes.Add("onblur",              "dateLostFocus(this.id);");
            txttdate.Attributes.Add("onblur",              "dateLostFocus(this.id);");
            //commented by rupal for r8 multicurrency,
            //currencylostfocus is not needed for currencytextbox and setDataChangedInv function is added as attribute for OnBlur property of currency textbox
            //iteminv.Attributes.Add("onblur",               ";currencyLostFocus(this);setDataChangedInv('iteminv');"); //function added by yatharth : MITS 17500 :
            nonbillreasoncode.Attributes.Add("onchange",   "lookupTextChanged(this);");
            nonbillreasoncode.Attributes.Add("onblur",     "codeLostFocus(this.id);");
            txtQty.Attributes.Add("onchange",              "setDataChangedCheck('txtQty');");
            //txtrate.Attributes.Add("onchange",             "setDataChangedCheck('txtrate');");//rupal:r8 multicurrency, added this attributr for "OnBlur"
            //txtrate.Attributes.Add("onblur",               ";currencyLostFocus(this);");//Added by yatharth 17500 //commented by rupal as its not required for currencytextbox
            txtTransType.Attributes.Add("onchange",        "UpdateDataChanged(this);");
            txtTransType.Attributes.Add("onblur",          "TransCodeLostFocus(this);");
            rbIsBillable.Attributes.Add("onclick",         "setDataChangedInv('optbillable');");
            rbIsNotBillable.Attributes.Add("onclick",      "setDataChangedInv('optbillable');");
            rbPostable.Attributes.Add("onclick",           "setDataChangedInv('optpostable');");
            rbNonPostable.Attributes.Add("onclick",        "setDataChangedInv('optpostable');");
            // MITS 15561 - MAC
            //txtcommentbtnMemo.Attributes.Add("onclick",    "EditMemo('txtcomment', '');");

            nonbillreasoncodebtn.Attributes.Add("onclick", "return selectCode('NON_BILL_REASON', 'nonbillreasoncode')");

            //MITS 15561 - MAC
            //txtcommentbtnMemo.Attributes.Add("onclick", "EditMemo('txtcomment', '');");

            invtotal.Enabled       = false;
            txttotal.Enabled       = false;
            totavailable.Enabled   = false;
            //MGaba2:MITS 19003:vertical scrollbar were coming disabled in Previos comments
           // txtprecomment.Enabled  = false;
            txtprecomment.ReadOnly = true;
            txtClaimnumber.Enabled = false;
            txtInvnum.Enabled      = false;//MITS 15562 by MJP
            chkoverride.Enabled    = false;//MITS 17610

            //MITS 17501 - start
            //if (chkoverride.Checked)
            //{
                h_checkoverride.Value = "True";
                txtunit.ReadOnly      = false;
                //txtrate.ReadOnly      = false;
                txtrate.Enabled = true;//rupal
            //}//if
            //else
            //{
                //h_checkoverride.Value = "False";
                //txtunit.ReadOnly      = true;
                //txtrate.ReadOnly      = true;
            //}//else
                //MITS 17501 - end
            
            if (h_ratemode.Value == "saved")
            {
                // MITS 16184 MAC
                //h_ratemode.Value = String.Empty;
                txtTransType.Text = h_transtype.Value;
                txtReservetype.Text = h_reservetype.Value;
                txtunit.Text = h_unit.Value;
                //txtrate.Text = h_rate.Value;
                //rupal:start, multicurrency
                if (h_rate.Value == string.Empty)
                {
                    h_rate.Value = "0";
                }
                txtrate.AmountInString = h_rate.Value;//rupal
                //txtrate.Attributes["Amount"] = h_rate.Value;//rupal
                txtrate.Attributes.Add("Amount", h_rate.Value);
                txtQty.Text = h_qty.Value;

                if (h_total.Value == string.Empty)
                {
                    h_total.Value = "0";
                }
                //txttotal.Text = h_total.Value;
                txttotal.AmountInString = h_total.Value;//rupal
                txttotal.Attributes.Add("Amount", h_total.Value);
                //rupal:end
            }//if

            //MITS 17501 - start
            int iRate = 0;
            //int.TryParse(txtrate.Text, out iRate);
            int.TryParse(txtrate.AmountInString, out iRate);
            if (iRate == 0)
            {
                chkoverride.Checked = true;
                h_checkoverride.Value = "True";
                txtunit.ReadOnly = false;
                //txtrate.ReadOnly = false;
                txtrate.Enabled = true; //rupal
            }
            //MITS 17501 - end

            nonbillreasoncodebtn.Enabled = (rbIsBillable.Checked) ? false : true;
            //Start - Yukti-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //Ended:Yukti ML changes

            if (IsPostBack)
            {
                //MITS 16272 - start - mpalinski
                if (!String.IsNullOrEmpty(h_nonbillreasoncode.Value))
                {
                    nonbillreasoncode.Text = h_nonbillreasoncode.Value;
                }//if
                //MITS 16272 - end - mpalinski
            }//if
            else
            {
                sMode               = AppHelper.GetQueryStringValue("ctlnumber").ToLower();
                hMode.Value         = sMode;
                txtClaimnumber.Text = AppHelper.GetQueryStringValue("claimnum");
                h_claimnum.Value    = txtClaimnumber.Text;
                hRowId.Value        = AppHelper.GetQueryStringValue("maxtrackid");
                isAllocated.Value   = AppHelper.GetQueryStringValue("IsAllocated");
                h_claimanteid.Value = AppHelper.GetQueryStringValue("ClaimantEid");//MITS 16608
                h_unitid.Value      = AppHelper.GetQueryStringValue("UnitId");//MITS 16608
                lblCurrencyType.Text = AppHelper.GetQueryStringValue("currencytype");//rupal: r8 multicurrency
                currencytype.Text = AppHelper.GetQueryStringValue("culture");//rupal: r8 multicurrency
                InitializeCulture();
                lblCurrencyType.Text = currencytype.Text.Split('|')[0].Substring(0, 3) + " " + Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                //rupal:end

                if (sMode == "edit")
                {
                    bodyDetails.Attributes["onload"] = "PageLoadDetails();";

                   // iteminv.Text = iteminv.Text.TrimStart('$'); commented by yatharth : MITS 17500 :

                    string sIsBillable      = h_billable.Value.ToLower();

                    rbIsBillable.Checked    = (sIsBillable == "yes") ? true : false;
                    rbIsNotBillable.Checked = (sIsBillable == "yes") ? false : true;

                    // MITS 15567 - MAC
                    nonbillreasoncode.ReadOnly = (sIsBillable == "yes") ? true : false;
                    //invtotal.Text = ConvertToUSD(h_invtotal.Value, false, false); //modified by yatharth : MITS 17500 : for $ sign
                    //rupal:start, r8 multicurrency
                    if (h_invtotal.Value == string.Empty)
                    {
                        h_invtotal.Value = "0";
                    }
                    invtotal.AmountInString = h_invtotal.Value; //modified by yatharth : MITS 17500 : for $ sign 
                    //rupal:end
                }//if
                else if (sMode == "add")
                {
                    int iMaxId = 0;

                    bodyDetails.Attributes["onload"] = "PageLoadDetailsLite();";

                    int.TryParse(hRowId.Value, out iMaxId);
                    iMaxId++;
                    hRowId.Value = iMaxId.ToString();

                    //invtotal.Text  = ConvertToUSD(h_invtotal.Value,false,false); //modified by yatharth : MITS 17500 : for $ sign
                    //rupal:start,multicurrency
                    if (h_invtotal.Value == string.Empty)
                    {
                        h_invtotal.Value = "0";
                    }
                    invtotal.AmountInString = h_invtotal.Value; //modified by yatharth : MITS 17500 : for $ sign //rupal
                    txtInvnum.Text = h_invoicenum.Value;
                    //rupal:end
                    // MITS 15567 - MAC
                    nonbillreasoncode.ReadOnly = true;

                }//else if
            }//else
        }//method: Page_Load
        //added by yatharth : MITS 17500 :
        private string ConvertToUSD(string sMoney, bool isBillAmt, bool isBillable)
        {
            decimal dMoney = 0.00m;
            string sConvertedMoney = String.Empty;

            if (String.IsNullOrEmpty(sMoney))
            {
                sMoney = "0.00";
            }//if

            sMoney = Regex.Replace(sMoney, @"[,]+", String.Empty);
            Decimal.TryParse(sMoney, out dMoney);

            if (isBillAmt && !isBillable)
            {
                sMoney = String.Format("{0:C}", dMoney).TrimStart('$');
                sConvertedMoney = String.Format("$ [{0}]", sMoney);
            }//if
            else
            {
                sConvertedMoney = String.Format("{0:C}", dMoney);
            }//else

            return sConvertedMoney;
        } // method: ConvertToUSD

        //rupal:start, r8 multicurrency
        protected override void InitializeCulture()
        {
            if (Request.Form["currencytype"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Form["currencytype"]))
                {
                    Culture = Request.Form["currencytype"].Split('|')[1];
                    UICulture = Request.Form["currencytype"].Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["currencytype"].Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["currencytype"].Split('|')[1]);
                }
            }
            else if ((currencytype != null) && (!string.IsNullOrEmpty(currencytype.Text)))
            {
                Culture = currencytype.Text.Split('|')[1];
                UICulture = currencytype.Text.Split('|')[1];
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currencytype.Text.Split('|')[1]);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(currencytype.Text.Split('|')[1]);
            }
            base.InitializeCulture();
        }
        //rupal:end

        //RMA-16243 - start
        protected void txtTansType_TextChanged(object sender, EventArgs e)
        {
            htranstypeChange.Value = "1";
        }
        //RMA-16243 - end
    }
}