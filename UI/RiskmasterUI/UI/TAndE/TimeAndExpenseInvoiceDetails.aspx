﻿<%@ Page Language="C#" Theme="RMX_Default" AutoEventWireup="true" CodeBehind="TimeAndExpenseInvoiceDetails.aspx.cs" Inherits="Riskmaster.UI.TAndE.TimeAndExpenseInvoiceDetails" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Invoice Details</title>
    <script type="text/javascript" src="../../Scripts/TandE.js"></script>
    <script type="text/javascript" src="../../Scripts/form.js"></script>
</head>
<body class="Margin0">
    <form id="frmData" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
            <tr>
                <td class="msgheader" colspan="13">Time and Expenses Details (<asp:Label ID="lblClaimNum" runat="server" Text=""></asp:Label>)</td>
            </tr>
        </table>
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr class="data2">
                <td align="left">
                    <asp:GridView ID="gvInvDetails" runat="server" Width="100%" CellPadding="4" CellSpacing="0" 
                                  autogeneratecolumns="False" AlternatingRowStyle-CssClass="data1">
                        <Columns>
                            <asp:boundfield DataField="vendor" 
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="ctrlgroup2" 
                                            HeaderStyle-HorizontalAlign="Left"
                                            HeaderText="Vendor/Work Done By">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup2">
                                <headertemplate>Billable?</headertemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" Enabled="false" id="billable" Checked='<%# Eval("isBillable") %>' />
                                </ItemTemplate>
                                <HeaderStyle CssClass="ctrlgroup2" horizontalalign="Center"></HeaderStyle>
                                <ItemStyle horizontalalign="Center"></ItemStyle>
                            </asp:templatefield>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup2">
                                <headertemplate>Postable?</headertemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" Enabled="false" id="postable" Checked='<%# Eval("isPostable") %>' />
                                </ItemTemplate>
                                <HeaderStyle CssClass="ctrlgroup2" horizontalalign="Center"></HeaderStyle>
                                <ItemStyle horizontalalign="Center"></ItemStyle>
                            </asp:templatefield>
                            <asp:boundfield DataField="rate" 
                                            ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="Rate">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:boundfield DataField="qty" 
                                            ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="Qty">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:boundfield DataField="unit" 
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Unit">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:boundfield DataField="total" 
                                            ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="Total">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:boundfield DataField="itemamt" 
                                            ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="Item Amount">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup2">
                                <headertemplate>Override</headertemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" Enabled="false" id="override" Checked='<%# Eval("isOverride") %>' />
                                </ItemTemplate>
                                <HeaderStyle CssClass="ctrlgroup2" horizontalalign="Center"></HeaderStyle>
                                <ItemStyle horizontalalign="Center"></ItemStyle>
                            </asp:templatefield>
                            <asp:boundfield DataField="fromtodate" 
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="From/To Date">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:boundfield DataField="transtype" 
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Left"
                                            HeaderText="Transaction Type">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:boundfield DataField="invdate" 
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Invoice Date">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                            <asp:boundfield DataField="nobillreason" 
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderStyle-HorizontalAlign="Left"
                                            HeaderText="Non Bill Reason">
                                <HeaderStyle CssClass="ctrlgroup2"></HeaderStyle>
                            </asp:boundfield>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr><td class="ctrlgroup2" colspan="13">&nbsp;</td></tr>
        </table>
        
        <br />
        <table width="100%" align="center">
            <tr>
                <td align="center">
                    <asp:button id="action" CssClass="button" runat="server" text="Close" 
                                OnClientClick="javascript:window.close();" />
                </td>
            </tr>
        </table>
        
        <asp:HiddenField ID="SysFormName" runat="server" />
        
        <asp:textbox style="display: none" runat="server" id="FrmName" rmxref="Instance/Document/TimeExpense/FrmName" Text="tandedetails" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId1" rmxref="Instance/Document/TimeExpense/TableId1" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId2" rmxref="Instance/Document/TimeExpense/TableId2" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId3" rmxref="Instance/Document/TimeExpense/TableId3" rmxtype="hidden" />
        <asp:TextBox runat="server" ID ="currencytype" Text="" style="display:none" rmxignoreset="true" />
        
    </form>
</body>
</html>