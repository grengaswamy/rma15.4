﻿<%@ Page Language="C#" Theme="RMX_Default" AutoEventWireup="true" CodeBehind="TimeAndExpenseRateAndUnits.aspx.cs" Inherits="Riskmaster.UI.TAndE.TimeAndExpenseRateAndUnits" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Code Selection</title>
    <script language="javascript" type="text/javascript" src="../../Scripts/TandE.js"></script>
</head>
<body runat="server" id="bodyRates">
    <form id="frmData" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
        </table>
        
        <asp:Panel ID="pnlCodeSelection" runat="server">
            <table border="0">
                <tr>
                    <td>
                        <asp:DataList ID="dlRateTables" GridLines="None" runat="server" ShowHeader="true" Height="142px">
                            <HeaderStyle Height="24px" Wrap="false" />
                            <HeaderTemplate>
                                <td class="Bold2">Code</td>
                                <td class="Bold2">Description</td>
                                <td class="Bold2">&nbsp;</td>
                            </HeaderTemplate>
                            <ItemStyle Height="24px" Wrap="False" />
                            <ItemTemplate>
                                <td class="Bold2"><%# DataBinder.Eval(Container.DataItem, "name") %></td>
                                <td class="Bold2"><%# DataBinder.Eval(Container.DataItem, "ratename") %></td>
                                <td class="Bold2">&nbsp;</td>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:Label ID="lblDesc" Visible="false" runat="server" Text="" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <asp:Panel ID="pnlTypeCode" runat="server">
            <table>
                <tr>
                    <td>
                        <%--<div id="div1" class="divScroll" style="position:absolute; height:180; width:550; overflow:auto;">--%>
                        <div id="div2" class="divScroll" style="position:absolute; height:90%; width:95%;">
                            <table>
                                <tr>
                                    <td>
                                        <asp:DataList ID="dlTypeCodes" runat="server" GridLines="None" 
                                                      CellPadding="1" CellSpacing="4" ShowHeader="true">
                                            <HeaderStyle Wrap="false" />
                                            <HeaderTemplate>
                                                <td class="colheader4">Code</td>
                                                <td class="colheader4">Description</td>
                                                <td class="colheader4">Rate</td>                                                
                                                <td class="colheader4">Unit</td>
                                            </HeaderTemplate>
                                            <ItemStyle Wrap="False" />
                                            <ItemTemplate>
                                                <td><%# DataBinder.Eval(Container.DataItem, "code1") %></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "desclink")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "rate_Formatted")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "unit") %></td>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <asp:Panel ID="pnlZeroRecords" runat="server">
            <table align="center" width="100%" border="0">
                <tr valign="middle" align="center">
    	            <td><font face="Verdana, Arial" size="4"><br /><b><asp:Label ID="lblNoData" Visible="false" runat="server" Text="" /></b></font></td>
                </tr>
                <tr valign="middle" align="center">
                    <td><asp:Button ID="cmdClose" Visible="false" CssClass="button" runat="server" Text=" Cancel " /></td>
                </tr>
            </table>
        </asp:Panel>
        
        <asp:HiddenField ID="hclmnum" runat="server"/>
        <asp:HiddenField ID="htranstype" runat="server" />
        <asp:HiddenField ID="SysFormName" runat="server" />
        
        <asp:textbox style="display: none" runat="server" id="FrmName" rmxref="Instance/Document/TimeExpense/FrmName" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId1" rmxref="Instance/Document/TimeExpense/TableId1" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId2" rmxref="Instance/Document/TimeExpense/TableId2" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId3" rmxref="Instance/Document/TimeExpense/TableId3" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="hcustid" rmxref="Instance/Document/TimeExpense/CustomerId" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="LookUpText" rmxref="Instance/Document/TimeExpense/LookUpText" Text="" rmxtype="hidden" />
<%--akaushik5 Added for RMA-19193 Starts--%>
        <asp:textbox style="display: none" runat="server" id="ClaimNumber" rmxref="Instance/Document/TimeExpense/ClaimNumber" Text="" rmxtype="hidden" />
<%--akaushik5 Added for RMA-19193 Ends--%>
        
    </form>
</body>
</html>