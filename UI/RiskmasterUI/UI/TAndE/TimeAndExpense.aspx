﻿<%@ Page Language="C#" Theme="RMX_Default" AutoEventWireup="true" CodeBehind="TimeAndExpense.aspx.cs" Inherits="Riskmaster.UI.TAndE.TimeAndExpense" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Time and Expense</title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/TandE.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js"></script>
</head>

<body id="bodyTandE" class="Margin0" runat="server">
    <form id="frmData" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
            <tr class="msgheader">
                <td class="msgheader">Time and Expenses (<asp:label ID="lblClaimNum" runat="server" Text="" />)</td>
            </tr>
        </table>
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr class="data2">
                <td align="left">
                    <asp:gridview ID="gvExpenses" runat="server" Width="100%" 
                                  onrowdatabound="gvExpenses_RowDataBound" 
                                  autogeneratecolumns="False" 
                                  ShowFooter="true"
                                  AlternatingRowStyle-CssClass="data1" 
                                  ondatabound="gvExpenses_DataBound">
                        <Columns>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup2" 
                                               HeaderStyle-HorizontalAlign="Center"
                                               FooterStyle-CssClass="ctrlgroup2"
                                               ItemStyle-HorizontalAlign="Center">
                                <headertemplate>Bill It?</headertemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="optBillit" runat="server" Checked='<%# Eval("billit") %>' />
                                </ItemTemplate>
                            </asp:templatefield>
                            <asp:boundfield DataField="invoicenumber" 
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderText="Invoice Number" 
                                            HeaderStyle-HorizontalAlign="Right" 
                                            ItemStyle-HorizontalAlign="Right"
                                            FooterStyle-CssClass="ctrlgroup2">
                            </asp:boundfield>
                            <asp:boundfield DataField="invoicedate" 
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderText="Invoice Date" 
                                            HeaderStyle-HorizontalAlign="Center" 
                                            ItemStyle-HorizontalAlign="Center"
                                            FooterStyle-HorizontalAlign="Right"
                                            FooterStyle-CssClass="ctrlgroup2" 
                                            FooterText="Total&nbsp;:&nbsp;">
                            </asp:boundfield>
                            <asp:boundfield DataField="gvstatus" 
                                            HeaderStyle-CssClass="ctrlgroup2"
                                            HeaderText="Status"
                                            HeaderStyle-HorizontalAlign="Center" 
                                            ItemStyle-HorizontalAlign="Center"
                                            FooterStyle-CssClass="ctrlgroup2">
                            </asp:boundfield>
                            <asp:templatefield headerstyle-cssclass="ctrlgroup2" 
                                               ItemStyle-HorizontalAlign="Center"
                                               FooterStyle-CssClass="ctrlgroup2">
                                <ItemTemplate>
                                    <asp:imagebutton id="img" imageurl="~/Images/book02.gif" ImageAlign="Middle" runat="server" />
                                </ItemTemplate>
                            </asp:templatefield>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="rowid" runat="server" Value='<%# Eval("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="transid" runat="server" Value='<%# Eval("transid") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="payeeid" runat="server" Value='<%# Eval("payeeid") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hinvoiceamt" runat="server" Value='<%# Eval("hinvoiceamt") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="msg" runat="server" Value='<%# Eval("msg") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hstatus" runat="server" Value='<%# Eval("hstatus") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:gridview>
                </td>
            </tr>
        </table>
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr class="data">
                <td class="data" valign="middle" align="left">Claim Rate Table : 
                    <asp:textbox id="ratetable" runat="server" />
                    <%--bkumar33:cosmetic change--%>
                    <asp:Button ID="btnClaimRateTable" runat="server" class="EllipsisControl" text="..." style="width:100" />
                </td>
            </tr>
        </table>
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr class="data">
                <td class="data" align="left">
                    <asp:button id="btnBilledExpenses" class="button" runat="server" text="Billed Expenses" />
                    <asp:button id="btnSelectAll" class="button" runat="server" text="Select All" 
                                OnClientClick="SelectAllCheckBoxes(); return false;" />
                    <asp:button id="btnBillIt" class="button" runat="server" text="Bill It"
                                OnClientClick="return billit();;" />
                    <asp:button id="btnVoidIt" class="button" runat="server" text="Void It" 
                                OnClientClick="return voidit();;" />
                    <asp:button id="btnGoToFunds" class="button" runat="server" text="Goto Funds" 
                                OnClientClick="return gotofunds();;"/>
                    <asp:button id="btnNew" class="button" runat="server" text="New" 
                                OnClientClick="return checkRateTable();;" />
                    <asp:button id="btnBackToFinancials" class="button" runat="server" text="Back To Financials" 
                                OnClientClick="return BackToFinancials();;" /><!-- //MITS 15850 - MJP -->
                </td>
            </tr>
        </table>
        
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr class="data">
                <td class="data" align="left">
                    <asp:checkbox id="chkprintTE" runat="server" appearance="full" />
                    Automatic T&amp;E Report on Bill It
                </td>
            </tr>
        </table>
        
        <uc1:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading ..." />
        
        <asp:placeholder id="PlaceHolder" runat="server" />
        
        <asp:HiddenField ID="accountid" runat="server" />
        <asp:HiddenField ID="actionid" runat="server" />
        <asp:HiddenField ID="allcheck" runat="server" />
        <asp:HiddenField ID="alldate" runat="server" />
        <asp:HiddenField ID="allowclsclm" runat="server" />
        <asp:HiddenField ID="autochecks" runat="server" />
        <asp:HiddenField ID="autopayment" runat="server" />
        <asp:HiddenField ID="autorpt" runat="server" />
        <asp:HiddenField ID="billed_void" runat="server" />
        <asp:HiddenField ID="bypass" runat="server" />
        <asp:HiddenField ID="checkstock" runat="server" />
        <asp:HiddenField ID="claimid" runat="server" />
        <asp:HiddenField ID="ClaimantEid" runat="server" />
        <asp:HiddenField ID="entrytype" runat="server" />
        <asp:HiddenField ID="finished" runat="server" />
        <asp:HiddenField ID="fromdate" runat="server" />
        <asp:HiddenField ID="h_invoiceid" runat="server" />
        <asp:HiddenField ID="h_isParent" runat="server" />
        <asp:HiddenField ID="h_ratetable" runat="server" />
        <asp:HiddenField ID="h_ratetableid" runat="server" />
        <asp:HiddenField ID="h_status" runat="server" />
        <asp:HiddenField ID="hclmnum" runat="server" />
        <asp:HiddenField ID="hclmnumtag" runat="server" />
        <asp:HiddenField ID="hcustid" runat="server" />
        <asp:HiddenField ID="hdnInvId" runat="server" />
        <asp:HiddenField id="Hidden1" runat="server" />
        <asp:HiddenField ID="invoiceamt" runat="server" />
        <asp:HiddenField ID="lastchecknum" runat="server" />
        <asp:HiddenField ID="markall" runat="server" />
        <asp:HiddenField ID="nextbatchnum" runat="server" />
        <asp:HiddenField ID="nextchecknum" runat="server" />
        <asp:HiddenField ID="nullpayeeid" runat="server" />
        <asp:HiddenField ID="nulltransid" runat="server" />
        <asp:HiddenField ID="postcheckfilename" runat="server" />
        <asp:HiddenField ID="postchecktype" runat="server" />
        <asp:HiddenField ID="precheckfilename" runat="server" />
        <asp:HiddenField ID="preorderfield" runat="server" />
        <asp:HiddenField ID="printcheck" runat="server" />
        <asp:HiddenField ID="ratetableid" runat="server" />
        <asp:HiddenField ID="repeat" runat="server" />
        <asp:HiddenField ID="selectedchecks" runat="server" />
        <asp:HiddenField ID="stage" runat="server" />
        <asp:HiddenField ID="SysFormName" runat="server" />
        <asp:HiddenField ID="todate" runat="server" />
        <asp:HiddenField ID="UnitId" runat="server" />
        
        <asp:textbox style="display: none" runat="server" id="FrmName" rmxref="Instance/Document/TimeExpense/FrmName" Text="tandesummery" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId1" rmxref="Instance/Document/TimeExpense/TableId1" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId2" rmxref="Instance/Document/TimeExpense/TableId2" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId3" rmxref="Instance/Document/TimeExpense/TableId3" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="InvoiceIds" rmxref="Instance/Document/TimeExpense/InvoiceIds" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="IsContinue" rmxref="Instance/Document/TimeExpense/IsContinue" text="true" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="AllowClosedClaim" rmxref="Instance/Document/TimeExpense/AllowClosedClaim" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="Status" rmxref="Instance/Document/TimeExpense/InvoiceStatus" rmxtype="hidden" />
    </form>
</body>
</html>