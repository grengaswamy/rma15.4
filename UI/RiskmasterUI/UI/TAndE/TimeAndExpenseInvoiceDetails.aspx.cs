﻿using System;
using System.Data;
using System.Xml;
using System.Xml.Linq;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.TAndE
{
    public partial class TimeAndExpenseInvoiceDetails : NonFDMBasePageCWS
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus   = false;
            string sreturnValue  = String.Empty;
            XElement oMsgElement = null;

            if (String.IsNullOrEmpty(FrmName.Text))
            {
                FrmName.Text = "tandedetails";
            } // if

            // Read the values from the previous form
            string sClaimNumber = AppHelper.GetQueryStringValue("ClaimNum");
            string sRowId       = AppHelper.GetQueryStringValue("RowId");
            string sClaimId     = AppHelper.GetQueryStringValue("ClaimId");
            string sRateCodeId  = AppHelper.GetQueryStringValue("InvoiceId");

            TableId1.Text       = sRowId;      //# of invoice details to return
            TableId2.Text       = sClaimId;    //claim #
            TableId3.Text       = sRateCodeId; //rate table #

            if (!String.IsNullOrEmpty(sClaimNumber))
            {
                lblClaimNum.Text = sClaimNumber;
            } // if

            if (!IsPostBack)
            {
                try
                {
                    oMsgElement = GetMessageTemplate();
                    CallCWSFunction("TimeExpenseAdaptor.GetXmlData", out sreturnValue, oMsgElement);
                    bReturnStatus = ErrorHelper.IsCWSCallSuccess(sreturnValue);

                    if (bReturnStatus)
                    {
                        BindGridView(sreturnValue);
                    }//if
                }//try
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch
            } // if
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private void BindGridView(string data)
        {
            DataSet invoiceRecordsSet = null;
            DataTable tandeDT         = null;
            DataTable tandedataDT     = null;
            DataRow tandeDR           = null;
            XmlDocument invoiceXDoc   = new XmlDocument();

            //XElement xmlTandEDetails = XElement.Parse(data);

            try
            {
                //fill xmldocument from the string xml
                invoiceXDoc.LoadXml(data);

                //fill dataset with the xmldocument object
                invoiceRecordsSet = new DataSet();
                invoiceRecordsSet.ReadXml(new XmlNodeReader(invoiceXDoc.SelectSingleNode("//Document")));

                tandeDT = invoiceRecordsSet.Tables["TandE"];

                if (tandeDT != null)
                {
                    tandeDR = tandeDT.Rows[0];

                    if (invoiceRecordsSet.Tables["TandEData"] != null)
                    {
                        tandedataDT = invoiceRecordsSet.Tables["TandEData"];

                        //add columns to the datatable manually so we can retrieve values from the gridview later with javascript
                        tandedataDT.Columns.Add("isBillable", typeof(bool));
                        tandedataDT.Columns.Add("isPostable", typeof(bool));
                        tandedataDT.Columns.Add("isOverride", typeof(bool));

                        //loop through each invoice/node
                        foreach (DataRow d in tandedataDT.Rows)
                        {
                            int iBillable = 0;
                            int iPostable = 0;
                            int iOverride = 0;

                            //Start MITS 16061 by MJP
                            //if (!String.IsNullOrEmpty(d["fromtodate"].ToString()))
                            //{
                            //    DateTime date   = Conversion.ToDate(d["fromtodate"].ToString());
                            //    d["fromtodate"] = String.Format("{0:MM/dd/yyyy}", date);
                            //} // if
                            //End MITS 16061 by MJP

                            if (!String.IsNullOrEmpty(d["invdate"].ToString()))
                            {
                                DateTime date2 = Conversion.ToDate(d["invdate"].ToString());
                                d["invdate"]   = String.Format("{0:MM/dd/yyyy}", date2);
                            } // if
                            
                            int.TryParse(d["billable"].ToString(), out iBillable);
                            int.TryParse(d["postable"].ToString(), out iPostable);
                            int.TryParse(d["override"].ToString(), out iOverride);

                            d["isBillable"] = (iBillable != 0) ? true : false;
                            d["isPostable"] = (iPostable != 0) ? true : false;
                            d["isOverride"] = (iOverride != 0) ? true : false;
                        }//foreach

                        //binds the datatable to the gridview
                        gvInvDetails.Visible             = true;
                        gvInvDetails.DataSource          = tandedataDT.DefaultView;
                        gvInvDetails.AutoGenerateColumns = false;
                        gvInvDetails.DataBind();
                    }//if
                }//if
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        } // method: BindGridView

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <TimeExpense>
                        <FrmName></FrmName>
                        <InputXml>
		                    <TandE/>
	                    </InputXml>
                        <TableId1/>
                        <TableId2/>
                        <TableId3/>
                    </TimeExpense>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}