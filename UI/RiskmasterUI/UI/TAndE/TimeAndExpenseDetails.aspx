﻿<%@ Page Language="C#" Theme="RMX_Default" AutoEventWireup="True" CodeBehind="TimeAndExpenseDetails.aspx.cs" Inherits="Riskmaster.UI.TAndE.TimeAndExpenseDetails" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Detail Items</title>
    <%--<script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TandE.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript" language="javascript" ></script>
</head>
<body runat="server" id="bodyDetails">
    <form id="frmData" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        
    <table border="0" width="800px">
        <tr>
            <td>
                <div class="msgheader" id="div_formtitle" runat="server" style="text-align:center">Time and Expense Details</div>
            </td>
        </tr>
    </table>
    
    <table border="0" width="800px">
        <tr>
            <td><rmcontrol:ErrorControl ID="ucErrorControl" runat="server" /></td>
        </tr>
    </table>
    
    <table border="0" width="800px">
        <tr>
            <%--First Column--%>
            <td class="fieldRightAligned"><b><u>Trans Type</u></b> :</td>
            <td>
                <asp:TextBox ID="txtTransType" MaxLength="50" runat="server" ontextchanged="txtTansType_TextChanged" /><%--RMA-16243--%>
                <asp:Button ID="txtTransTypebtn" runat="server" class="EllipsisControl" text="..." style="width:100" 
                    OnClientClick="return formHandler1('TimeAndExpenseRateAndUnits.aspx','','','childwindow1');" />
            </td>
            <%--Second Column--%>
            <td class="fieldRightAligned">Claim Number :</td>
            <td>
                <asp:TextBox ID="txtClaimnumber" runat="server" />
            </td>
        </tr>
        
        <tr>
            <%--First Column--%>
            <td class="fieldRightAligned">Reserve Type :</td>
            <td>
                <asp:TextBox ID="txtReservetype" runat="server" ReadOnly="true" />
            </td>
            <%--Second Column--%>
            <td class="fieldRightAligned">
                <asp:Label ID="lblInvnum" runat="server" Text="Invoice Number :" />
            </td>
            <td>
                <asp:TextBox ID="txtInvnum" runat="server" />
            </td>
        </tr>
        
        <tr>
            <%--First Column--%>
            <td class="fieldRightAligned">From Date :</td>
            <td>
                <asp:TextBox ID="txtfdate" runat="server" />
                <%--<asp:Button ID="btGetFromDate" runat="server" CssClass="DateLookupControl" style="width:100" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					        {
					            inputField: "txtfdate",
					            ifFormat: "%m/%d/%Y",
					            button: "btGetFromDate"
					        }
					    );
                </script>--%>
                 <script type="text/javascript">
                     $(function () {
                         $("#txtfdate").datepicker({
                             showOn: "button",
                             buttonImage: "../../Images/calendar.gif",
                            // buttonImageOnly: true,
                             showOtherMonths: true,
                             selectOtherMonths: true,
                             changeYear: true
                         }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                     });
                    </script>
            </td>
            <%--Second Column--%>
            <td class="fieldRightAligned">To Date :</td>
            <td>
                <asp:TextBox ID="txttdate" runat="server" />
               <%-- <asp:Button ID="btGetToDate" runat="server" CssClass="DateLookupControl" style="width:100" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					        {
					            inputField: "txttdate",
					            ifFormat: "%m/%d/%Y",
					            button: "btGetToDate"
					        }
					    );
                </script>--%>
                 <script type="text/javascript">
                     $(function () {
                         $("#txttdate").datepicker({
                             showOn: "button",
                             buttonImage: "../../Images/calendar.gif",
                             //buttonImageOnly: true,
                             showOtherMonths: true,
                             selectOtherMonths: true,
                             changeYear: true
                         }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                     });
                    </script>
            </td>
        </tr>
        
        <tr>
            <td class="fieldRightAligned">
                <asp:Label ID="lblinvtotal" runat="server" Text="Invoice Total :" />
            </td>
            <td>
                <mc:CurrencyTextbox runat="server" id="invtotal" />
            </td>
            <td class="fieldRightAligned">
                <asp:Label ID="lbliteminv" runat="server" Text="<b><u>Item Amount</u></b> :" />
            </td>
            <td>                
                <mc:CurrencyTextbox runat="server" id="iteminv" OnBlur="setDataChangedInv('iteminv');" />
            </td>
        </tr>
    </table>
    
    <table border="0" width="800px">
        <tr>
            <td colspan="2">
                <asp:radiobutton groupname="optbillable" runat="server" 
                                 id="rbIsBillable" 
                                 rmxtype="radio" 
                                 text="Billable : " 
                                 TextAlign="Left" 
                                 value="0" 
                                 rmxref="Instance/Document/TimeExpense/IsBillable" 
                                 checked="true" />
                <asp:radiobutton groupname="optbillable" runat="server" 
                                 id="rbIsNotBillable" 
                                 rmxtype="radio" 
                                 text="Non Billable : " 
                                 TextAlign="Left" 
                                 value="1" />
            </td>
            <td colspan="2">
                Non Bill Reason :
                <asp:TextBox runat="server" ID="nonbillreasoncode" />
                <asp:Button runat="server" ID="nonbillreasoncodebtn" class="EllipsisControl" Text="..." />
            </td>
        </tr>
    </table>
    
    <table border="0" width="800px">
        <tr>
            <td colspan="4">
                <asp:radiobutton groupname="optpostable" runat="server" 
                                 id="rbPostable" 
                                 rmxtype="radio" 
                                 text="Postable : " 
                                 TextAlign="Left" 
                                 value="0" 
                                 rmxref="Instance/Document/TimeExpense/IsPostable" 
                                 checked="true" />
                <asp:radiobutton groupname="optpostable" runat="server" 
                                 id="rbNonPostable" 
                                 rmxtype="radio" 
                                 text="Non Postable : " 
                                 TextAlign="Left" 
                                 value="1" />
            </td>
        </tr>
    </table>
    
    <table border="0" width="800px">
        <tr>
            <td>Reason/Desc :</td>
            <td colspan="3">
                <asp:TextBox ID="txtreason" runat="server" width="500" />
            </td>
        </tr>
    </table>
    
    <table border="0" width="800px">
        <tr><td colspan="4"><hr style="color:#f5f5f5;" /></td></tr>
    </table>
    
    <table border="0" width="800px">
        <tr>
            <td colspan="2" width="40%" valign="top">
                <span class="ctrlgroup">&nbsp;<b>Item Calculator</b>&nbsp;</span><br /><br />
                <table border="0">
                    <tr>
                        <td>Override :</td>
                        <td>
                            <asp:CheckBox runat="server" ID="chkoverride" />
                        </td>
                    </tr>
                    <tr>
                        <td>Quantity :</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtQty" />
                        </td>
                    </tr>
                    <tr>
                        <td>Unit :</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtunit" />
                        </td>
                    </tr>
                    <tr>
                        <td>Rate :</td>
                        <td>                            
                            <mc:CurrencyTextbox runat="server" id="txtrate" OnBlur="setDataChangedCheck('txtrate');" />
                        </td>
                    </tr>
                    <tr class="data1">
                        <td><b><u>Total</u></b> :</td>
                        <td>                            
                            <mc:CurrencyTextbox runat="server" id="txttotal" />
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="2" width="60%">
                Comments :<br />
                <%--MITS 15716 - MAC--%>
                <textarea id="txtcomment" cols="30" rows="3" runat="server"></textarea>
                <%--<asp:TextBox runat="server" TextMode="MultiLine" Columns="30" Rows="3" ID="txtcomment" />--%>
                <%-- MITS 15561 and 15716 - MAC --%>
                <%--<asp:Button runat="server" CssClass="button" id="txtcommentbtnMemo" Text="..." />--%>
                <input type="button" class="MemoButton" name="txtcommentbtnMemo" id="txtcommentbtnMemo" onclick="EditHTMLMemo('txtcomment','')" />
                <br />
                Previous Comment :
                <asp:TextBox runat="server" ID="txtprecomment" TextMode="MultiLine" Rows="5" Columns="60" />
            </td>
        </tr>
    </table>
    
    <table border="0" width="800px">
        <tr><td colspan="4"><hr style="color:#f5f5f5;" /></td></tr>
        <tr>
            <td colspan="3"  class="data2">
                Amount of Invoice Total Not Applied :                 
                <mc:CurrencyTextbox runat="server" id="totavailable" />
                <asp:Label id="Label1" runat="server" Visible="false" Text="Currency Type" ></asp:Label>
                <asp:Label id="lblCurrencyType" runat="server" Visible="false" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">
                <asp:button id="btnSave" CssClass="button" runat="server" text="  Save &amp; Close  " 
                            OnClientClick="javascript:return saveInvDetail();" />
                <asp:button id="btncancel" CssClass="button" runat="server" text="Cancel" 
                            OnClientClick="javascript:window.close();" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
    <asp:HiddenField runat="server" ID="abc" />
    <asp:HiddenField runat="server" ID="h_billable" />
    <asp:HiddenField runat="server" ID="h_Billed" />
    <asp:HiddenField runat="server" ID="h_checkoverride" />
    <asp:HiddenField runat="server" ID="h_claimanteid" /><%-- MITS 16608 --%>
    <asp:HiddenField runat="server" ID="h_claimnum" />
    <asp:HiddenField runat="server" ID="h_fromdate" />
    <asp:HiddenField runat="server" ID="h_id" />
    <asp:HiddenField runat="server" ID="h_invnum" />
    <asp:HiddenField runat="server" ID="h_invoicenum" />
    <asp:HiddenField runat="server" ID="h_invtotal" />
    <asp:HiddenField runat="server" ID="h_nonbillreasoncode" /><%-- MITS 16272 --%>
    <asp:HiddenField runat="server" ID="h_postable" />
    <asp:HiddenField runat="server" ID="h_qty" />
    <asp:HiddenField runat="server" ID="h_rate" />
    <asp:HiddenField runat="server" ID="h_ratemode" />
    <asp:HiddenField runat="server" ID="h_ratetableid_child" />
    <asp:HiddenField runat="server" ID="h_reservetype" />
    <asp:HiddenField runat="server" ID="h_reservetypecode" />
    <asp:HiddenField runat="server" ID="h_status" />
    <asp:HiddenField runat="server" ID="h_todate" />
    <asp:HiddenField runat="server" ID="h_total" />
    <asp:HiddenField runat="server" ID="h_trackmode" />
    <asp:HiddenField runat="server" ID="h_transtype" />
    <asp:HiddenField runat="server" ID="h_transtypecode" />
    <asp:HiddenField runat="server" ID="h_unit" />
    <asp:HiddenField runat="server" ID="h_unitid" /><%-- MITS 16608 --%>
    <asp:HiddenField runat="server" ID="h_vendor" />
    <asp:HiddenField runat="server" ID="hMode" />
    <asp:HiddenField runat="server" ID="hRowId" />
    <asp:HiddenField runat="server" ID="id" />
    <asp:HiddenField runat="server" ID="isAllocated" />
    <asp:HiddenField runat="server" ID="maxtrackid" />
    <asp:HiddenField runat="server" ID="nonbillreasoncode_cid" />
    <asp:HiddenField runat="server" ID="h_pid" />
    <asp:HiddenField runat="server" ID="PostBillAmount" />
    <asp:HiddenField runat="server" ID="PreBillAmount" />
    <asp:HiddenField runat="server" ID="PreItemAmount" />
    <asp:HiddenField runat="server" ID="reservetypecode" />
    <asp:HiddenField runat="server" ID="SysFormName" />
    <asp:HiddenField runat="server" ID="TotAvailableInvoice" />
    <asp:HiddenField runat="server" ID="trackid" />
    <asp:HiddenField runat="server" ID="transtypecode" />
    <asp:HiddenField runat="server" ID="vendor_cid" />
        <asp:HiddenField runat="server" ID="htranstypeChange" /> <%--RMA-16243--%>
    
    <asp:TextBox style="display:none" runat="server" id="txtData" RMXType="id" />
    <asp:textbox runat="server" id="IsPostable" style="display: none" rmxref="Instance/Document/TimeExpense/IsPostable" rmxtype="hidden" />
    <asp:textbox runat="server" id="IsBillable" style="display: none" rmxref="Instance/Document/TimeExpense/IsBillable" rmxtype="hidden" />
    <asp:textbox style="display: none" runat="server" id="currencytype" rmxtype="hidden" />
    </form>
</body>
</html>