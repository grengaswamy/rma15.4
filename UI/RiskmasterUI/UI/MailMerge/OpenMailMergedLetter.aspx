﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenMailMergedLetter.aspx.cs" Inherits="Riskmaster.UI.MailMerge.OpenMailMergedLetter"  ValidateRequest="false" EnableViewStateMac="false"%>

<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
         <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
        <script type="text/javascript" language="javascript" src="../../Scripts/Silverlight.js"></script>
        <script language='javascript'>
        function submitthisPage()
        {
            if (document.forms[0].firstTime.value=="")
            {
            document.forms[0].btnLaunchMerge.disabled=true;
               document.forms[0].firstTime.value=1;
               // alert('1');
                //MITS ID: 33447 - nkaranam2 : Used Parent.document instead of window.opener.document
                document.forms[0].attach.value=parent.document.forms[0].attach.value;
                try
                {
                 
                   if (parent.document.forms[0].recordselect.checked)
                   {
                      document.forms[0].recordselect.value=parent.document.forms[0].recordselect.checked;
                   }
                }
                catch(e)
                {
                    document.forms[0].recordselect.value=parent.document.forms[0].recordselect.value;
                }
               document.forms[0].lettername.value=parent.document.forms[0].lettername.value;
               document.forms[0].DocTitle.value=parent.document.forms[0].DocTitle.value;
               document.forms[0].Subject.value=parent.document.forms[0].Subject.value;
               document.forms[0].Keywords.value=parent.document.forms[0].Keywords.value;
               document.forms[0].Notes.value=parent.document.forms[0].Notes.value;
               document.forms[0].Class.value=parent.document.forms[0].Class.value;
               document.forms[0].Category.value=parent.document.forms[0].Category.value;
               document.forms[0].Type.value=parent.document.forms[0].Type.value;
               document.forms[0].Comments.value=parent.document.forms[0].Comments.value;
               document.forms[0].DirectDisplay.value=parent.document.forms[0].DirectDisplay.value;
               document.forms[0].hdnrowids.value=parent.document.forms[0].hdnrowids.value;
               //alert(document.forms[0].hdnrowids.value);
               document.forms[0].RecordId.value=parent.document.forms[0].RecordId.value;
               document.forms[0].lettername.value=parent.document.forms[0].lettername.value;
               document.forms[0].EmailCheck.value = parent.document.forms[0].EmailCheck.value;
               document.forms[0].lstMailRecipient.value = parent.document.forms[0].lstMailRecipient.value;
               document.forms[0].TableName.value = parent.document.forms[0].TableName.value;
               document.forms[0].txtSelectedRecipient.value = parent.document.forms[0].txtSelectedRecipient.value;
               document.forms[0].submit();
               pleaseWait.Show();		
            }
            else
            document.forms[0].btnLaunchMerge.disabled=false;
        }   
            //Changes for MITS 29607 sgupta243
            //RMA - 10937 : This function blocks onload event in chrome.Hence commented.
            /*function window::onload()
             {
               window.focus();
             }*/
             //End for MITS 29607      
        </script>
</head>
    <%--Giving Focus to this page MITS 32096 add window.focus()  --%>
<body  onload="InitPageSettingsOnBodyLoad();submitthisPage();window.focus();">
    <form id="frmData" runat="server">
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage="<%$ Resources:PleaseWaitDialog1Resrc %>"/>
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
       <table width="100%" border="0"><B><asp:Label ID="lblMergeDoc" runat="server" Text="<%$ Resources:lblMergeDocResrc %>"></asp:Label><asp:Label ID="FileName" runat=server></asp:Label></B><br><br><tr>
     <td colspan="2" class="ctrlgroup"><asp:Label ID="lblCmpltMerge" runat="server" Text="<%$ Resources:lblCmpltMergeResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td valign="top" colspan="2"> <asp:Label ID="lblNowCmptMerge" runat="server" Text="<%$ Resources:lblNowCmptMergeResrc %>"></asp:Label>
      										<br><br><ol class="gen">
       <li><asp:Label ID="lblMrgEditLstOpt1" runat="server" Text="<%$ Resources:lblMrgEditLstOpt1Resrc %>"></asp:Label></li>
       <li><asp:Label ID="lblMrgEditLstOpt2" runat="server" Text="<%$ Resources:lblMrgEditLstOpt2Resrc %>"></asp:Label></li>
       <li><asp:Label ID="lblMrgEditLstOpt3" runat="server" Text="<%$ Resources:lblMrgEditLstOpt3Resrc %>"></asp:Label></li>
       <li><asp:Label ID="lblMrgEditLstOpt4" runat="server" Text="<%$ Resources:lblMrgEditLstOpt4Resrc %>"></asp:Label>
       </li>
       <%if (IsClaimEventMerge)
         {%>
       <li><asp:Label ID="lblMrgEditLstOpt5" runat="server" Text="<%$ Resources:lblAfterSaving %>" />&nbsp;<asp:Label ID="lblMrgEditLstOpt6" runat="server" Font-Bold="true" Text="<%$ Resources:lblScreenClickFinish %>" /> <asp:Label ID="lblMrgEditLstOpt7" runat="server" Text= "<%$ Resources:lblSendAutomatedEmail %>" /></li>
       <%} %>
      </ol>
      <center>

      <input type="button" name="Launch" value="<%$ Resources: btnLaunchMergeResrc%>" runat="server" class="button" disabled="true" id="btnLaunchMerge" onclick="Javascript:return StartEditor('',false);; "/>

    
      </center><br><br></td>
    </tr>
    <tr>
     <td colspan="2" class="ctrlgroup"></td>
    </tr>
    <tr>
    <td colspan="2">
     <%if (IsClaimEventMerge)
         {%>
      <asp:Button ID="btnFinish" runat="server" Text="<%$ Resources: btnFinishResrc%>" CssClass=button 
             EnableTheming="False" onclick="btnFinish_Click" UseSubmitBehavior="true" OnClientClick="return FinishOpenMerge(true);" /><%} %>
    <input type="button" name="Cancel" value="<%$ Resources: btnCancelResrc%>" runat="server" class="button" onclick="Javascript:return CleanAndCancel();; "/>


   
     </td>
    </tr>
    <tr>
    <td>
        <asp:HiddenField ID="hdnRTFContent" runat="server" />
        <asp:HiddenField ID="hdnDataSource" runat="server" />
        <asp:HiddenField ID="hdnHeaderContent" runat="server" />
        <asp:HiddenField ID="hdnTempFileName" runat="server" />
        <asp:HiddenField ID="hdnNumRecords" runat="server" />
        <asp:HiddenField ID="hdnNewFileName" runat="server" />
        <asp:HiddenField ID="hdnError" runat="server" />
        <asp:HiddenField ID="firstTime" runat="server" />
        
              <asp:HiddenField ID="attach" runat="server" />
              <asp:HiddenField ID="hdnrowids" runat="server" />
           <asp:HiddenField ID="recordselect" runat="server" />
           <asp:HiddenField ID="lettername" runat="server" />
            <asp:HiddenField ID="DocTitle" runat="server" />
        <asp:HiddenField ID="Subject" runat="server" />   
        <asp:HiddenField ID="Keywords" runat="server" />   
        <asp:HiddenField ID="Notes" runat="server" />   
         <asp:HiddenField ID="RecordId" runat="server" />
          <asp:HiddenField ID="Class" runat="server" />   
        <asp:HiddenField ID="Category" runat="server" />   
        <asp:HiddenField ID="Type" runat="server" />   
        <asp:HiddenField ID="Comments" runat="server" />   
         <asp:HiddenField ID="DirectDisplay" runat="server" />
         <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
        <asp:HiddenField ID="txtSelectedRecipient" runat="server" />
          <asp:HiddenField ID="TableName" runat="server" />
          <asp:HiddenField ID="RetMessage" runat="server" />
       <asp:HiddenField ID="hdnFlagMessage" runat="server" />
         <asp:HiddenField ID="hdnTemplateFormat" runat="server" />
		 <asp:HiddenField ID="hdnSilver" runat="server" />
    </td>
    
    </tr>
    
   </table>
  <div id="silverlightControlHost">
        <object data="data:application/x-silverlight-2," id="Silverlight" type="application/x-silverlight-2" width="100%" height="100%">
		  <param name="source" value="ClientBin/RiskmasterSL.xap"/>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="5.0.61118.0" />
		  <param name="autoUpgrade" value="true" />
           <param name="onload" value="silverlight_onload" />
		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
	    </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe></div>
    </form>
</body>
</html>
