﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FetchData.aspx.cs" Inherits="Riskmaster.UI.MailMerge.FetchData" ValidateRequest="false" EnableViewStateMac="false"%>

<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>





<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
       <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
       <script language=javascript>
        function checkRecords(id,value)
					{
					
						var objElem = document.getElementById(id);
						var sID = value + ',';
						if(objElem.checked)
						{
							document.all.hdnrowids.value += sID;
						}
						else
						{
							var sIDs = document.all.hdnrowids.value;
							document.all.hdnrowids.value = sIDs.replace(sID, '');
						}
					}
       </script>
</head>
<body>
    <form id="frmData" runat="server">
    		<rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
    		<table border="0" width="100%">
						<tr>
							<td colspan="3" class="formtitle">

								<asp:Label ID="lblUseMergeLetter" runat="server" Text="<%$ Resources:lblUseMergeLetterResrc %>"></asp:Label>			
                                [
									<asp:Label ID="title" runat=server></asp:Label>
								]
							</td>
						</tr>
						<tr>
							<td>
								&#160;
							</td>
						</tr>
						<tr>
							<td colspan="3" class="ctrlgroup">
								<asp:Label ID="lblMergeFrmRcdSel" runat="server" Text="<%$ Resources:lblMergeFrmRcdSelResrc %>"></asp:Label>
							</td>
						</tr>
						<tr>
							<td>
								&#160;
							</td>
						</tr>
						<tr>
							<td width="100%">
                            <asp:Label ID="LblMergeFinishMsg" runat="server" Text="<%$ Resources:lblMergeFinishMsgResrc %>"></asp:Label>								
								</td>
						</tr>
					</table>
					<br></br>
					<table border="0" width="100%">
						<%foreach(XmlElement ele in Model.SelectNodes("//mergedata")){ %>
							<tr>
								<td>
									<em><asp:Label ID="lblUseRecord" runat="server" Text="<%$ Resources:lblUseRecordResrc %>"></asp:Label></em>
								</td>
								<%foreach (XmlElement eleTempCol in ele.SelectNodes("//header/column"))
          { %>
									<td>
										<em>
											<%=eleTempCol.InnerText%>
										</em>
									</td>
									<td>&#160;</td>
								<%} %>
							</tr>
							<%foreach (XmlElement eleTempRecord in ele.SelectNodes("record"))
         { %>
							<tr>
								<td>
								<input type=checkbox onclick="checkRecords('rowid_<%=eleTempRecord.Attributes["rowid"].Value %>','<%=eleTempRecord.Attributes["rowid"].Value %>')" id='rowid_<%=eleTempRecord.Attributes["rowid"].Value %>' value='<%=eleTempRecord.Attributes["rowid"].Value %>' /> 
								
								</td>
								<%foreach(XmlElement eleTempField in eleTempRecord.SelectNodes("field")){ %>
								
									<td>
										<em>
											<%=eleTempField.InnerText%>
										</em>
									</td>
									<td>&#160;</td>
									<%} %>
								
							</tr>
							<%} %>
						<%} %>
					</table>
					<br />
					<br />
	
					<table width="100%" border="0">
						<tr>
							<td>
								<em>
										<asp:Label ID="lblProduceDocument" runat="server" Text="<%$ Resources:lblProduceDocumentResrc %>"></asp:Label>
									</em>
							</td>
						</tr>
						<tr>
							<td>&#160;</td>
						</tr>
						<tr>
							<td align="left">
								  <asp:Button ID="Button1" onclick="btnBack_Click" runat="server" Text="<%$ Resources:btnBackResrc %>"  CssClass=button UseSubmitBehavior="true" />
         <asp:Button ID="Button2" runat="server" Text="<%$ Resources:btnNextResrc %>"  onclick="btnNext_Click" UseSubmitBehavior="true"  CssClass=button OnClientClick="return ValidateRecords();"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             EnableTheming="False" onclick="btnCancel_Click" UseSubmitBehavior="true" OnClientClick="return Cancel();" />
							</td>
						</tr>
					</table>
    <asp:HiddenField ID="TableName" runat="server" />
        <asp:HiddenField ID="FormName" runat="server" />
        <asp:HiddenField ID="RecordId" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
    <asp:HiddenField ID="allstatesselected" runat="server" />
    <asp:HiddenField ID="psid" runat="server" />
      <asp:HiddenField ID="attach" runat="server" />
        <asp:HiddenField ID="docresultflag" runat="server" />
           <asp:HiddenField ID="hdnrowids" runat="server" />
           <asp:HiddenField ID="recordselect" runat="server" />
           <asp:HiddenField ID="lettername" runat="server" />
            <asp:HiddenField ID="DocTitle" runat="server" />
        <asp:HiddenField ID="Subject" runat="server" />   
        <asp:HiddenField ID="Keywords" runat="server" />   
        <asp:HiddenField ID="Notes" runat="server" />   
          
          <asp:HiddenField ID="Class" runat="server" />   
        <asp:HiddenField ID="Category" runat="server" />   
        <asp:HiddenField ID="Type" runat="server" />   
        <asp:HiddenField ID="Comments" runat="server" />   
         <asp:HiddenField ID="DirectDisplay" runat="server" />
         <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
         <asp:HiddenField ID="txtSelectedRecipient" runat="server" />
           <asp:HiddenField ID="hdnSilver" runat="server" />
    </form>
</body>
</html>