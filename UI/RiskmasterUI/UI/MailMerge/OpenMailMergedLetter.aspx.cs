﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class OpenMailMergedLetter : System.Web.UI.Page
    {
        public Boolean IsClaimEventMerge = false;
        string MergeMessageTemplate = "<Message><Authorization>ebaf1b5e-f41e-4f5f-847d-6faa1aeb394a</Authorization><Call>" +
            "<Function>MailMergeAdaptor.GenerateRTF</Function></Call><Document><Template><TemplateId>5102</TemplateId>" +
            "<Name>558***5102***1 558</Name><Attach /><DirectDisplay /><Title /><Subject /><Keywords />" +
            "<Notes /><Class /><Category /><Type /><Comments /><RecordId>1</RecordId><RowIds></RowIds>" +
            "</Template></Document></Message>";
        string MergeMessageMailTemplate = "<Message><Authorization>ebaf1b5e-f41e-4f5f-847d-6faa1aeb394a</Authorization><Call>" +
                "<Function>MailMergeAdaptor.MailRTF</Function></Call><Document><Template><TemplateId></TemplateId>" +
                "<Name></Name><Attach /><SendEmail /><MailRecipient />" +
                "<Type /><NewFileName/><FileContent/><TableName /><FormName/><RecordId></RecordId><RowIds></RowIds>" +
                "<CatId/><psid/></Template></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("OpenMailMergedLetter.aspx"), "MergeClaimLetterValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "OpenMailMergedLetterValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end
                if (string.Compare(TableName.Value, "claim", true) == 0 || string.Compare(TableName.Value, "event", true) == 0)
                {
                    IsClaimEventMerge = true;
                }

                Button btn = sender as Button;
                if (btn != null)
                {
                    if (Page.IsPostBack && !btn.Text.StartsWith("La"))
                        GetData();
                }

                else
                {
                    if (Page.IsPostBack)
                        GetData();
                    if (!string.IsNullOrEmpty(RetMessage.Value) && string.Compare(hdnFlagMessage.Value, "DisplayMessage", true) == 0)
                    {
                        Server.Transfer("OpenMainMergeSaved.aspx");
                        //Response.Redirect("OpenMainMergeSaved.aspx");
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();
        }
        private void GetData()
        {
            if (firstTime.Value == "1")
            {
                string TemplateId = lettername.Value;
                string[] sep = { "***" };
                string[] arr = TemplateId.Split(sep, StringSplitOptions.None);
                TemplateId = arr[1];
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//TemplateId", TemplateId);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Name", lettername.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//RecordId", RecordId.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Attach", attach.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Notes", Notes.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Type", Type.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Comments", Comments.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Class", Class.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Category", Category.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Keywords", Keywords.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Subject", Subject.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Title", DocTitle.Value);
                MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//DirectDisplay", DirectDisplay.Value);
                XmlDocument Model = new XmlDocument();
                Model.LoadXml(MergeMessageTemplate);
                string Rowids = hdnrowids.Value;
                arr = Rowids.Split(",".ToCharArray()[0]);
                XmlElement ele = (XmlElement)Model.SelectSingleNode("//RowIds");
                foreach (string id in arr)
                {
                    if (id != "")
                    {
                        XmlElement eleRow = Model.CreateElement("RowId");
                        eleRow.InnerText = id;
                        ele.AppendChild((XmlNode)eleRow);
                    }
                }
                string sReturn = AppHelper.CallCWSService(Model.OuterXml);
                Model = new XmlDocument();
                Model.LoadXml(sReturn);
                SetData(Model);
            }
        }
        private void SetData(XmlDocument Model)
        {
            FileName.Text = Model.SelectSingleNode("//File").Attributes["Filename"].Value;
            hdnTempFileName.Value = Model.SelectSingleNode("//File").Attributes["Filename"].Value;
            hdnRTFContent.Value = Model.SelectSingleNode("//File").InnerText;
            hdnDataSource.Value = Model.SelectSingleNode("//DataSource").InnerText;
            hdnHeaderContent.Value = Model.SelectSingleNode("//HeaderContent").InnerText;
            hdnNumRecords.Value = Model.SelectSingleNode("//NumRecords").InnerText;
            hdnTemplateFormat.Value = Model.SelectSingleNode("//TemplateFormat").InnerText;//33465 - nkaranam2 
            hdnSilver.Value = Model.SelectSingleNode("//UseSilverlight").InnerText;//mudabbir added for 36022
			if(string.Compare(hdnSilver.Value,"true",true)!=0){
				System.Web.HttpBrowserCapabilities browser = Request.Browser;
	            string sBrowser = browser.Browser;
	            if (sBrowser != "IE" && sBrowser != "InternetExplorer")//bkuzhanthaim : RMA-8407
	                hdnSilver.Value = "true";
			}
        }
        protected void btnFinish_Click(object sender, EventArgs e)
        {
            bool bError = false; //pmittal5 Mits 17752 12/29/09
            string sMessage = string.Empty;
            try
            {
                if (TableName.Value == "claim" || TableName.Value == "event")
                    sMessage = SendMailToDegRecipient();
            }
            catch (Exception ee)
            {
                bError = true; //pmittal5 Mits 17752
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            if (!bError && !string.IsNullOrEmpty(sMessage))
            {
                RetMessage.Value = sMessage;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "k", "<script>SetRetMessage();</script>", false);
            }
        }
        private string SendMailToDegRecipient()
        {
            XmlElement objDisMsg = null;
            string sMessage = string.Empty;
            string TemplateId = lettername.Value;
            string[] sep = { "***" };
            string[] arr = TemplateId.Split(sep, StringSplitOptions.None);
            TemplateId = arr[1];
            string CatId = arr[2];
            string Isattached = "0";
            //added by swati MITS # 36930
            string[] arrRecipient = txtSelectedRecipient.Value.Split('|');
            string sRecipient = string.Empty;
            //end here
            try
            {
                if (attach.Value == "Checked")
                    Isattached = "1";
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//TemplateId", TemplateId);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Name", lettername.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//RecordId", RecordId.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Attach", Isattached);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Notes", Notes.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Type", Type.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//TableName", TableName.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//FileContent", hdnRTFContent.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//NewFileName", hdnNewFileName.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//CatId", CatId);
                //MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//FormName", "");
                //MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//psid", psid.Value);
                //asharma326 JIRA 5984 
                if (string.IsNullOrEmpty(EmailCheck.Value))
                    EmailCheck.Value = AppHelper.GetQueryStringValue("EmailCheck");
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//SendEmail", EmailCheck.Value);

                if (string.IsNullOrEmpty(lstMailRecipient.Value))
                    lstMailRecipient.Value = txtSelectedRecipient.Value;

                //added by swati for MITS # 36930
                foreach (string s in arrRecipient)
                {
                    if (!string.IsNullOrEmpty(s) && s.Split(',').Length > 1)
                    {
                        if (!string.IsNullOrEmpty(sRecipient))
                        {
                            sRecipient = sRecipient + "," + s.Split(',')[1];
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(s))
                            sRecipient = s.Split(',')[1].ToString();
                        }
                    }
                }
                //modified by swati  MITS # 36930 11/17/2014
                //MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//MailRecipient", lstMailRecipient.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//MailRecipient", sRecipient);
                //if (string.Compare(EmailCheck.Value, "true", true) == 0 && !string.IsNullOrEmpty(lstMailRecipient.Value))
                if (string.Compare(EmailCheck.Value, "true", true) == 0 && !string.IsNullOrEmpty(sRecipient))
                    //modification end by swati
                {
                    XmlDocument Model = new XmlDocument();
                    Model.LoadXml(MergeMessageMailTemplate);
                    string sReturn = AppHelper.CallCWSService(Model.OuterXml);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    if (Model.SelectSingleNode("//DispalyMessage") != null)
                    {
                        objDisMsg = (XmlElement)Model.SelectSingleNode("//DispalyMessage");
                        sMessage = objDisMsg.SelectSingleNode("Message").InnerText;
                    }
                    if (Model.SelectSingleNode("//MsgStatusCd").InnerText == "Error")
                    {
                        throw new Exception(Model.SelectSingleNode("//ExtendedStatusDesc").InnerText);
                    }
                }
                else
                {
                    //sMessage = "The selected template does not indicate that an email copy should be sent; therefore, no email has been generated.";
                    sMessage = RMXResourceProvider.GetSpecificObject("lblNoEmailRecpOpen", RMXResourceProvider.PageId("OpenMailMergedLetter.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                }
                return sMessage;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return sMessage;
            }

        }
    }
}
