﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using Riskmaster.RMXResourceManager;
using Riskmaster.Common;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeEditResult : System.Web.UI.Page
    {
        string MergeMessageTemplate = "<Message><Authorization>ebaf1b5e-f41e-4f5f-847d-6faa1aeb394a</Authorization><Call>" +
                  "<Function>MailMergeAdaptor.GenerateRTF</Function></Call><Document><Template><TemplateId>5102</TemplateId>" +
                  "<Name>558***5102***1 558</Name><Attach /><DirectDisplay /><Title /><Subject /><Keywords />" +
                  "<Notes /><Class /><Category /><Type /><Comments /><RecordId>1</RecordId><RowIds></RowIds>" +
                  "</Template></Document></Message>";
        string MergeMessageSaveTemplate = "<Message><Authorization>ebaf1b5e-f41e-4f5f-847d-6faa1aeb394a</Authorization><Call>" +
                "<Function>MailMergeAdaptor.AttachRTF</Function></Call><Document><Template><TemplateId></TemplateId>" +
                "<Name></Name><Attach /><DirectDisplay /><Title /><Subject /><Keywords />" +
                "<Notes /><Class /><Category /><Type /><NewFileName/><FileContent/><Comments /><FormName/><RecordId></RecordId><RowIds></RowIds>" +
                "<CatId/><psid/></Template></Document></Message>";
        string MergeMessageMailTemplate = "<Message><Authorization>ebaf1b5e-f41e-4f5f-847d-6faa1aeb394a</Authorization><Call>" +
                "<Function>MailMergeAdaptor.MailRTF</Function></Call><Document><Template><TemplateId></TemplateId>" +
                "<Name></Name><Attach /><SendEmail /><MailRecipient />" +
                "<Type /><NewFileName/><FileContent/><TableName /><FormName/><RecordId></RecordId><RowIds></RowIds>" +
                "<CatId/><psid/></Template></Document></Message>";
        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                lettername.Value = AppHelper.GetValue("lettername");
                attach.Value = AppHelper.GetValue("attach");
                RecordId.Value = AppHelper.GetValue("RecordId");
                Notes.Value = AppHelper.GetFormValue("Notes");
                Type.Value = AppHelper.GetFormValue("Type");
                Comments.Value = AppHelper.GetFormValue("Comments");
                Category.Value = AppHelper.GetFormValue("Category");
                Class.Value = AppHelper.GetFormValue("Class");
                Keywords.Value = AppHelper.GetFormValue("Keywords");
                Subject.Value = AppHelper.GetFormValue("Subject");
                DocTitle.Value = AppHelper.GetFormValue("DocTitle");
                DirectDisplay.Value = AppHelper.GetFormValue("DirectDisplay");
                FormName.Value = AppHelper.GetFormValue("FormName");
                psid.Value = AppHelper.GetValue("psid");

                //spahariya MITS 28867
                if (string.IsNullOrEmpty(EmailCheck.Value))
                    EmailCheck.Value = AppHelper.GetQueryStringValue("EmailCheck");
                else
                    EmailCheck.Value = AppHelper.GetValue("EmailCheck");
                if (string.IsNullOrEmpty(lstMailRecipient.Value))
                    lstMailRecipient.Value = AppHelper.GetValue("txtSelectedRecipient"); //JIRA -6313
                else
                    lstMailRecipient.Value = AppHelper.GetValue("lstMailRecipient");
                TableName.Value = AppHelper.GetValue("TableName");
                txtSelectedRecipient.Value = AppHelper.GetValue("txtSelectedRecipient");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SaveValuesFromPreviousScreen();
                //Amandeep MultiLingual Changesc--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeEditResult.aspx"), "MergeEditResultValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeEditResultValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changesc--end

                if (!Page.IsPostBack)
                {
                    string sattach = "";
                    string TemplateId = lettername.Value;
                    string[] sep = { "***" };
                    string[] arr = TemplateId.Split(sep, StringSplitOptions.None);
                    TemplateId = arr[1];
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//TemplateId", TemplateId);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Name", lettername.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//RecordId", RecordId.Value);
                    if (attach.Value == "Checked")
                        sattach = "1";
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Attach", sattach);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Notes", Notes.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Type", Type.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Comments", Comments.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Class", Class.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Category", Category.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Keywords", Keywords.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Subject", Subject.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Title", DocTitle.Value);
                    MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//DirectDisplay", DirectDisplay.Value);
                    XmlDocument Model = new XmlDocument();
                    Model.LoadXml(MergeMessageTemplate);
                    string Rowids = hdnrowids.Value;
                    // rrachev JIRA 5115 I didn't remove previous row to not broke something.
                    if (String.IsNullOrEmpty(Rowids)) Rowids = AppHelper.GetValue("hdnrowids");
                    arr = Rowids.Split(",".ToCharArray()[0]);
                    XmlElement ele = (XmlElement)Model.SelectSingleNode("//RowIds");
                    foreach (string id in arr)
                    {
                        if (id != "")
                        {
                            XmlElement eleRow = Model.CreateElement("RowId");
                            eleRow.InnerText = id;
                            ele.AppendChild((XmlNode)eleRow);
                        }
                    }
                    string sReturn = AppHelper.CallCWSService(Model.OuterXml);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    SetData(Model);
                }
                else
                {
                    if (!string.IsNullOrEmpty(RetMessage.Value) && string.Compare(hdnFlagMessage.Value, "DisplayMessage", true) == 0)
                    {
                        Server.Transfer("MergeEditResultSaved.aspx");
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();
        }
        private void SetData(XmlDocument Model)
        {
            FileName.Text = Model.SelectSingleNode("//File").Attributes["Filename"].Value;
            hdnTempFileName.Value = Model.SelectSingleNode("//File").Attributes["Filename"].Value;
            hdnRTFContent.Value = Model.SelectSingleNode("//File").InnerText;
            if (Model.SelectSingleNode("//DataSource") != null)
            {
                hdnDataSource.Value = Model.SelectSingleNode("//DataSource").InnerText;
            }
            if (Model.SelectSingleNode("//HeaderContent") != null)
            {
                hdnHeaderContent.Value = Model.SelectSingleNode("//HeaderContent").InnerText;
            }
            if (Model.SelectSingleNode("//NumRecords") != null)
            {
                hdnNumRecords.Value = Model.SelectSingleNode("//NumRecords").InnerText;
            }
			if (Model.SelectSingleNode("//UseSilverlight") != null)
            {
                hdnSilver.Value = Model.SelectSingleNode("//UseSilverlight").InnerText;//mudabbir 36022
            }
			if(string.Compare(hdnSilver.Value,"true",true) != 0){
				// RMA-8407 : MITS-36022 : bkuzhanthim
                System.Web.HttpBrowserCapabilities browser = Request.Browser;
                string sBrowser = browser.Browser;
                if (sBrowser != "IE" && sBrowser != "InternetExplorer")
                    hdnSilver.Value = "true";
            }
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            bool bError = false; //pmittal5 Mits 17752 12/29/09
            string sMessage = string.Empty;
            try
            {
                string TemplateId = lettername.Value;
                string[] sep = { "***" };
                string[] arr = TemplateId.Split(sep, StringSplitOptions.None);
                TemplateId = arr[1];
                string CatId = arr[2];
                string Isattached = "0";
                if (attach.Value == "Checked")
                    Isattached = "1";
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//TemplateId", TemplateId);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Name", lettername.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//RecordId", RecordId.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Attach", Isattached);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Notes", Notes.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Type", Type.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Comments", Comments.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Class", Class.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Category", Category.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Keywords", Keywords.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Subject", Subject.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title", DocTitle.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//DirectDisplay", DirectDisplay.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//FileContent", hdnRTFContent.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//NewFileName", hdnNewFileName.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//CatId", CatId);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//FormName", FormName.Value);
                MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//psid", psid.Value);
                XmlDocument Model = new XmlDocument();
                Model.LoadXml(MergeMessageSaveTemplate);
                string sReturn = AppHelper.CallCWSService(Model.OuterXml);
                Model = new XmlDocument();
                Model.LoadXml(sReturn);
                //pmittal5 Mits 17752 12/29/09 - Error thrown on Business Layer
                if (Model.SelectSingleNode("//MsgStatusCd").InnerText == "Error")
                {
                    throw new Exception(Model.SelectSingleNode("//ExtendedStatusDesc").InnerText);
                }//End - pmittal5
                //spahariya MITS 28867
                if (TableName.Value == "claim" || TableName.Value == "event")
                {
                    sMessage = SendMailToDegRecipient();
                }
            }
            catch (Exception ee)
            {
                bError = true; //pmittal5 Mits 17752
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            if (!bError && !string.IsNullOrEmpty(sMessage))
            {
                RetMessage.Value = sMessage;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "k", "<script>SetRetMessage();</script>", false);
            }
            else if (!bError) //pmittal5 Mits 17752
            {
                Response.Redirect("MergeEditResultSaved.aspx");
            }
        }
        private string SendMailToDegRecipient()
        {
            string TemplateId = lettername.Value;
            string[] sep = { "***" };
            string[] arr = TemplateId.Split(sep, StringSplitOptions.None);
            TemplateId = arr[1];
            string CatId = arr[2];
            string Isattached = "0";
            XmlDocument Model = new XmlDocument();
            XmlElement objDisMsg = null;                   
            string sMessage = string.Empty;
            //added by swati MITS # 36930
            string[] arrRecipient = txtSelectedRecipient.Value.Split('|');
            string sRecipient = string.Empty;
            //end here
            try
            {
                if (attach.Value == "Checked")
                    Isattached = "1";
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//TemplateId", TemplateId);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Name", lettername.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//RecordId", RecordId.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Attach", Isattached);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Notes", Notes.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//Type", Type.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//TableName", TableName.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//FileContent", hdnRTFContent.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//NewFileName", hdnNewFileName.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//CatId", CatId);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//FormName", FormName.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//psid", psid.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//SendEmail", EmailCheck.Value);
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//MailRecipient", lstMailRecipient.Value);
                //added by swati for MITS # 36930
                foreach (string s in arrRecipient)
                {
                    if(!string.IsNullOrEmpty(s)  && s.Split(',').Length > 1) 
                    {
                        if (!string.IsNullOrEmpty(sRecipient))
                        {
                            sRecipient = sRecipient + "," + s.Split(',')[1];
                        }
                        else
                        {
                            sRecipient = s.Split(',')[1].ToString();
                        }
                    }
                }
                //modified by swati  MITS # 36930 11/17/2014                
                MergeMessageMailTemplate = AppHelper.ChangeMessageValue(MergeMessageMailTemplate, "//MailRecipient", sRecipient);
                //if (string.Compare(EmailCheck.Value, "true", true) == 0 && !string.IsNullOrEmpty(lstMailRecipient.Value))
                if (string.Compare(EmailCheck.Value, "true", true) == 0 && !string.IsNullOrEmpty(sRecipient))
                    //modification end by swati                
                {
                    Model.LoadXml(MergeMessageMailTemplate);
                    string sReturn = AppHelper.CallCWSService(Model.OuterXml);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    if (Model.SelectSingleNode("//DispalyMessage") != null)
                    {
                        objDisMsg = (XmlElement)Model.SelectSingleNode("//DispalyMessage");
                        sMessage = objDisMsg.SelectSingleNode("Message").InnerText;
                    }
                    if (Model.SelectSingleNode("//MsgStatusCd").InnerText == "Error")
                    {
                        throw new Exception(Model.SelectSingleNode("//ExtendedStatusDesc").InnerText);
                    }
                }
                else
                {
                    //sMessage = "The selected template does not indicate that an email copy should be sent; therefore, no email has been generated.";
                    sMessage = RMXResourceProvider.GetSpecificObject("lblNoEmailRecp", RMXResourceProvider.PageId("MergeEditResult.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                }
                return sMessage;
            }
            catch (Exception ee)
            {                
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return sMessage;
            }
            
        }
    }
}
