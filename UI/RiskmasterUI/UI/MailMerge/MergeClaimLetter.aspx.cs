﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using Riskmaster.RMXResourceManager; //mbahl3 Mits 31493

namespace Riskmaster.UI.UI.MailMerge
{
    public partial class MergeClaimLetter : System.Web.UI.Page
    {
        //Incoming XML
        string MergeMessageTemplate = "<Message><Authorization>ebaf1b5e-f41e-4f5f-847d-6faa1aeb394a</Authorization><Call>" +
                  "<Function>ClaimLetterAdaptor.GetClmLetter</Function></Call><Document><ClaimLetter><TemplateId>5102</TemplateId>" +
                  "<Name>558***5102***1 558</Name><FormName /><RecordId /><LetterType /><RowIds></RowIds>" +
                  "</ClaimLetter></Document></Message>";

        //Outgoing XML 
        string MergeMessageSaveTemplate = "<Message><Authorization>ebaf1b5e-f41e-4f5f-847d-6faa1aeb394a</Authorization><Call>" +
                "<Function>ClaimLetterAdaptor.AttachAndMailLetter</Function></Call><Document><Template>" +
                "<L1File></L1File><L1SecFile></L1SecFile><L2File></L2File><L2SecFile></L2SecFile><L3File></L3File><L3SecFile></L3SecFile>" +
                "<L4File></L4File><L4SecFile></L4SecFile><L5File></L5File><L5SecFile></L5SecFile><L6File></L6File><L6SecFile></L6SecFile>" +
                "<L7File></L7File><L7SecFile></L7SecFile><L8File></L8File><L8SecFile></L8SecFile><LetterType/>" +
                "<ClaimNumber></ClaimNumber>" +
                "<Title1/><Title2/><Title3/><Title4/><Title5/><Title6/><Title7/><Title8/>" +
                "<Notes/><Class/><Type/><Comments/><RecordId/><Subject/><FormName/><psid/><Password/><Category/></Template></Document></Message>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //mbahl3 Mits 31493

                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeClaimLetter.aspx"), "MergeClaimLetterValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeClaimLetterValidations", sValidationResources, true);
                //mbahl3 Mits 31493

                if (!Page.IsPostBack)
                {
                    if (hdnLetterGenerated.Value != "-1" )
                    {
                         //Praveen ML Changes
                        //FileName.Text = "Generating Claim Letter."; 
                        FileName.Text = hdnFilenameMessage1.Value;
                        //Praveen ML Changes


                        //Generate Claim Letter
                        MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//TemplateId", AppHelper.GetQueryStringValue("TemplateId"));
                        MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//Name", AppHelper.GetQueryStringValue("NO"));
                        MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//FormName", AppHelper.GetQueryStringValue("NO"));
                        MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//RecordId", AppHelper.GetQueryStringValue("RecordId"));
                        MergeMessageTemplate = AppHelper.ChangeMessageValue(MergeMessageTemplate, "//LetterType", AppHelper.GetQueryStringValue("Type"));                        
                        XmlDocument Model = new XmlDocument();
                        Model.LoadXml(MergeMessageTemplate);
                        string sReturn = AppHelper.CallCWSService(Model.OuterXml);
                        Model = new XmlDocument();
                        Model.LoadXml(sReturn);
                        
                        //Praveen ML Changes
                        //FileName.Text = "Letter Template has been loaded.";
                        FileName.Text = hdnFilenameMessage2.Value;
                        //Praveen ML Changes

                        SetData(Model);
                    }
                    else
                    {
                        //Praveen ML Changes
                        //FileName.Text = "Claim Letter could not be generated.";
                        FileName.Text = hdnFilenameMessage3.Value;
                        //Praveen ML Changes
                    }
                }                    
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);                
            }
            DataBind();
        }

        private void SetData(XmlDocument Model)
        {

            if (Model.SelectSingleNode("//Password") != null)
                hdnPassword.Value = Model.SelectSingleNode("//Password").InnerText;
            
            if (hdnPassword.Value != "")
            {
                hdnTempFileName.Value = Model.SelectSingleNode("//File").Attributes["Filename"].Value;
                hdnRTFContent.Value = Model.SelectSingleNode("//File").InnerText;
                hdnHeaderContent.Value = Model.SelectSingleNode("//HeaderContent").InnerText;
                hdnDataSource.Value = Model.SelectSingleNode("//DataSource").InnerText;
                hdnSecFileName.Value = Model.SelectSingleNode("//SecFileName").InnerText;
                hdnOrgEntryExist.Value = Model.SelectSingleNode("//OrgEntryExist").InnerText;

                //Title
                hdnTitle1.Value = Model.SelectSingleNode("//Title0").InnerText;
                hdnTitle2.Value = Model.SelectSingleNode("//Title1").InnerText;
                hdnTitle3.Value = Model.SelectSingleNode("//Title2").InnerText;
                hdnTitle4.Value = Model.SelectSingleNode("//Title3").InnerText;
                hdnTitle5.Value = Model.SelectSingleNode("//Title4").InnerText;
                hdnTitle6.Value = Model.SelectSingleNode("//Title5").InnerText;
                hdnTitle7.Value = Model.SelectSingleNode("//Title6").InnerText;
                hdnTitle8.Value = Model.SelectSingleNode("//Title7").InnerText;

                hdnNumRecords.Value = Model.SelectSingleNode("//NumRecords").InnerText;

                //Imp Fix: Since Number of Records in ACK and Closed Letter will be just one, So Num record value should always be
                //one to avoid making of Duplicate Copy
                hdnNumRecords.Value = Convert.ToString(1) ;
                

                if (Model.SelectSingleNode("//SplitData0") != null)
                    hdnData4Lv1.Value = Model.SelectSingleNode("//SplitData0").InnerText;
                if (Model.SelectSingleNode("//SplitData1") != null)
                    hdnData4Lv2.Value = Model.SelectSingleNode("//SplitData1").InnerText;
                if (Model.SelectSingleNode("//SplitData2") != null)
                    hdnData4Lv3.Value = Model.SelectSingleNode("//SplitData2").InnerText;
                if (Model.SelectSingleNode("//SplitData3") != null)
                    hdnData4Lv4.Value = Model.SelectSingleNode("//SplitData3").InnerText;
                if (Model.SelectSingleNode("//SplitData4") != null)
                    hdnData4Lv5.Value = Model.SelectSingleNode("//SplitData4").InnerText;
                if (Model.SelectSingleNode("//SplitData5") != null)
                    hdnData4Lv6.Value = Model.SelectSingleNode("//SplitData5").InnerText;
                if (Model.SelectSingleNode("//SplitData6") != null)
                    hdnData4Lv7.Value = Model.SelectSingleNode("//SplitData6").InnerText;
                if (Model.SelectSingleNode("//SplitData7") != null)
                    hdnData4Lv8.Value = Model.SelectSingleNode("//SplitData7").InnerText;

                hdnLetterGenerated.Value = "-1";
                
                //Praveen ML Changes
                //FileName.Text = " Please Wait.....";
                FileName.Text = hdnFilenameMessage4.Value;
                //Praveen ML Changes
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //Store Claim Letter
            try
            {
                if (hdnLetterGenerated.Value == "-1")
                {
                    if (hdnOrgEntryExist.Value == "False")
                    {
                        hdnRTFContent8.Value = hdnRTFContent7.Value = hdnRTFContent6.Value = hdnRTFContent5.Value = hdnRTFContent4.Value = hdnRTFContent3.Value = hdnRTFContent2.Value = hdnRTFContent1.Value;                        
                        hdnSecureRTFContent8.Value = hdnSecureRTFContent7.Value = hdnSecureRTFContent6.Value = hdnSecureRTFContent5.Value = hdnSecureRTFContent4.Value = hdnSecureRTFContent3.Value = hdnSecureRTFContent2.Value = hdnSecureRTFContent1.Value;
                    }          
                    
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//RecordId", AppHelper.GetQueryStringValue("RecordId"));
                    //Normal Letters
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L1File", hdnRTFContent1.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L2File", hdnRTFContent2.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L3File", hdnRTFContent3.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L4File", hdnRTFContent4.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L5File", hdnRTFContent5.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L6File", hdnRTFContent6.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L7File", hdnRTFContent7.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L8File", hdnRTFContent8.Value);
                    
                    ////File Names
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L1FileName", hdnFileName1.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L2FileName", hdnFileName2.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L3FileName", hdnFileName3.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L4FileName", hdnFileName4.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L5FileName", hdnFileName5.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L6FileName", hdnFileName6.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L7FileName", hdnFileName7.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L8FileName", hdnFileName8.Value);
                    
                    //Titles
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title1", hdnTitle1.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title2", hdnTitle2.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title3", hdnTitle3.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title4", hdnTitle4.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title5", hdnTitle5.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title6", hdnTitle6.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title7", hdnTitle7.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Title8", hdnTitle8.Value);
                    //Secure Letters
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L1SecFile", hdnSecureRTFContent1.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L2SecFile", hdnSecureRTFContent2.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L3SecFile", hdnSecureRTFContent3.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L4SecFile", hdnSecureRTFContent4.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L5SecFile", hdnSecureRTFContent5.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L6SecFile", hdnSecureRTFContent6.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L7SecFile", hdnSecureRTFContent7.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L8SecFile", hdnSecureRTFContent8.Value);
                    
                    ////Secure File Names
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L1SecFileName", hdnSecFileName1.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L2SecFileName", hdnSecFileName2.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L3SecFileName", hdnSecFileName3.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L4SecFileName", hdnSecFileName4.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L5SecFileName", hdnSecFileName5.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L6SecFileName", hdnSecFileName6.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L7SecFileName", hdnSecFileName7.Value);
                    //MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//L8SecFileName", hdnSecFileName8.Value);
                    
                    //Other Data required to save Attachments
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//LetterType", AppHelper.GetQueryStringValue("Type"));
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Notes", "");
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Class", "0");
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Category", "0"); 
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Type", "0");
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Comments", "");                    
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//RecordId", AppHelper.GetQueryStringValue("RecordId"));
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Subject", AppHelper.GetQueryStringValue("Subject"));
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//ClaimNumber", AppHelper.GetQueryStringValue("NO"));
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//FormName", "CLAIM");
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//Password", hdnPassword.Value);
                    MergeMessageSaveTemplate = AppHelper.ChangeMessageValue(MergeMessageSaveTemplate, "//psid", psid.Value);
                    XmlDocument Model = new XmlDocument();
                    Model.LoadXml(MergeMessageSaveTemplate);
                    string sReturn = AppHelper.CallCWSService(Model.OuterXml);
                    Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "abc", "window.close();", true);                            
        }
    }
}
