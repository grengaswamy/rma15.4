﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
namespace Riskmaster.UI.MailMerge
{
    public partial class MailMergeIssue : System.Web.UI.Page
    {
        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                FormName.Value = AppHelper.GetValue("FormName");
                TableName.Value = AppHelper.GetValue("TableName");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                psid.Value = AppHelper.GetValue("ipsid");
                RecordId.Value = AppHelper.GetValue("RecordId");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MailMergeIssue.aspx"), "MailMergeIssueValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MailMergeIssueValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                SaveValuesFromPreviousScreen();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();

        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplateFilter.aspx");
        }
        
    }
}
