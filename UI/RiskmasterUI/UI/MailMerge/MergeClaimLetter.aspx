﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeClaimLetter.aspx.cs"
    Inherits="Riskmaster.UI.UI.MailMerge.MergeClaimLetter" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Riskmaster</title>
    <script type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/merge.js")%>'></script>
    <script type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/OpenRTF.js")%>'></script>
    <script type="text/javascript" language="javascript"  src="../../Scripts/Silverlight.js"></script>
</head>
<body>
    <form id="frmLetter" runat="server">

    <script language="javascript" type="text/javascript">
        var IsPageLoaded = false;        
            function MergeClaimLetter(){
                if (IsPageLoaded == true){
                    return;
                }                
                IsPageLoaded = true;
                //StartLetterEditor('', true);                                
                if( document.getElementById('hdnPassword').defaultValue != "")
                    StartClmEditor('', true);
                
                var objBtn = document.getElementById('<%= btnSubmit.ClientID%>');
                if (objBtn != null)
                {
                    objBtn.click();
                }
            }
    </script>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <table width="100%" border="0">
        <tr>
            <td>
                <b><asp:Label ID="lblGenClaimLetter" runat="server" Text="<%$ Resources:lblGenClaimLetterResrc %>"></asp:Label><asp:Label ID="FileName" runat="server"></asp:Label></b><br>
                <br></br>
            </td>
        </tr>
        <tr>
            <td style="margin-left: 80px">
                <asp:UpdatePanel ID="updPnl" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>
                        <%--Claim Letter Content--%>
                        <asp:HiddenField ID="hdnRTFContent" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent1" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent2" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent3" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent4" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent5" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent6" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent7" runat="server" />
                        <asp:HiddenField ID="hdnRTFContent8" runat="server" />
                        <%--Claim Letter Titles--%>
                        <asp:HiddenField ID="hdnTitle1" runat="server" />
                        <asp:HiddenField ID="hdnTitle2" runat="server" />
                        <asp:HiddenField ID="hdnTitle3" runat="server" />
                        <asp:HiddenField ID="hdnTitle4" runat="server" />
                        <asp:HiddenField ID="hdnTitle5" runat="server" />
                        <asp:HiddenField ID="hdnTitle6" runat="server" />
                        <asp:HiddenField ID="hdnTitle7" runat="server" />
                        <asp:HiddenField ID="hdnTitle8" runat="server" />                        
                        <%--Secured Claim Letter Content--%>
                        <asp:HiddenField ID="hdnSecureRTFContent1" runat="server" />
                        <asp:HiddenField ID="hdnSecureRTFContent2" runat="server" />
                        <asp:HiddenField ID="hdnSecureRTFContent3" runat="server" />
                        <asp:HiddenField ID="hdnSecureRTFContent4" runat="server" />
                        <asp:HiddenField ID="hdnSecureRTFContent5" runat="server" />
                        <asp:HiddenField ID="hdnSecureRTFContent6" runat="server" />
                        <asp:HiddenField ID="hdnSecureRTFContent7" runat="server" />
                        <asp:HiddenField ID="hdnSecureRTFContent8" runat="server" />
                        <%--Data for all Org Levels--%>
                        <asp:HiddenField ID="hdnData4Lv1" runat="server"  />
                        <asp:HiddenField ID="hdnData4Lv2" runat="server" />
                        <asp:HiddenField ID="hdnData4Lv3" runat="server" />
                        <asp:HiddenField ID="hdnData4Lv4" runat="server" />
                        <asp:HiddenField ID="hdnData4Lv5" runat="server" />
                        <asp:HiddenField ID="hdnData4Lv6" runat="server" />
                        <asp:HiddenField ID="hdnData4Lv7" runat="server" />
                        <asp:HiddenField ID="hdnData4Lv8" runat="server" />
                        <asp:HiddenField ID="hdnPassword" runat="server" />
                        <asp:HiddenField ID="hdnTempFileName" runat="server" />
                        <asp:HiddenField ID="hdnNumRecords" runat="server" />                        
                        <asp:HiddenField ID="hdnSecFileName" runat="server" />                        
                       <asp:HiddenField ID="hdnLetterGenerated" runat="server" />
                        <asp:HiddenField ID="psid" runat="server" />
                        <asp:HiddenField ID="hdnOrgEntryExist" runat="server" />
                        <asp:HiddenField ID="hdnPopUpOpen" runat="server" Value = "No" />
                        
                        
                        <%--Data for all Org Levels--%>
                        <asp:HiddenField ID="hdnHeaderContent" runat="server" />
                        <asp:HiddenField ID="hdnDataSource" runat="server" />                        
                        <%--Data for all Org Levels--%>

                       <%-- //praveen ML changes--%>
                       <asp:HiddenField ID="hdnFilenameMessage1" runat="server" Value="<%$ Resources:hdnFilenameMessage1 %>" />
                       <asp:HiddenField ID="hdnFilenameMessage2" runat="server" Value="<%$ Resources:hdnFilenameMessage2 %>" />
                       <asp:HiddenField ID="hdnFilenameMessage3" runat="server" Value="<%$ Resources:hdnFilenameMessage3 %>" />
                       <asp:HiddenField ID="hdnFilenameMessage4" runat="server" Value="<%$ Resources:hdnFilenameMessage4 %>" />
                       <%-- //praveen ML changes--%>
                       
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <asp:Button ID="btnSubmit" runat="server" Style="display: none;" OnClick="btnSubmit_Click" />
        </tr>
    </table>
        <asp:HiddenField ID="hdnSilver" runat="server" />
         <div id="silverlightControlHost">
        <object data="data:application/x-silverlight-2," id="Silverlight" type="application/x-silverlight-2" width="100%" height="100%">
		  <param name="source" value="ClientBin/RiskmasterSL.xap"/>
		  <param name="onError" value="onSilverlightError" />
		  <param name="background" value="white" />
		  <param name="minRuntimeVersion" value="5.0.61118.0" />
		  <param name="autoUpgrade" value="true" />
           <param name="onload" value="silverlight_onload" />
		  <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		  </a>
	    </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe></div>
    <%--<uc2:PleaseWaitDialog ID="PleaseWaitDialog" runat="server" CustomMessage="" />--%>
    <script language="javascript" type="text/javascript">
            //Do Merge after the page has loaded
            function pageLoad()   
            {  
                MergeClaimLetter();
            }                
    </script>    
    </form>
</body>
</html>
