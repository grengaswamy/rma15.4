﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoveFinancials.aspx.cs" Inherits="Riskmaster.UI.UI.Reserves.MoveFinancials" ValidateRequest="false" EnableViewState="true" EnableViewStateMac="false" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }
    </script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/org.js"></script>
    <script type="text/javascript">
        window.onload = function () {
            if (window.frameElement)
                window.frameElement.scrolling = "auto";

        }
        function NoCoverages() {
            alert(MoveFinancialsValidations.NoCoverage);
            
            return false;
        }
        function NoMapping()
        {
            alert(MoveFinancialsValidations.NoMapping);
            return false;
        }
        function AddEvent(sEvent) {

            if (sEvent == 'showtree') {
                document.getElementById("isMapEnabled").value = "true";
            }
            document.getElementById('txtFunctionToCall').value = sEvent;

        }
        function SimiliarRecord() {
    alert(MoveFinancialsValidations.SimiliarRecord);
            return false;

        }
        function AddMapping() {
            alert(MoveFinancialsValidations.AddMapping);
            return false;

        }

        function SelectTarget() {
            alert(MoveFinancialsValidations.SelectTarget);
            return false;

        }

        function SelectSource() {
            alert(MoveFinancialsValidations.SelectSource);
            return false;

        }


        function MarkPolicyInvalid() {


            var sRetVal = confirm(MoveFinancialsValidations.MarkPolicyInvalid);
            if (sRetVal) {
                document.getElementById("txtFunctionToCall").value = "MarkPolicyInvalid";
                document.forms[0].submit();

            }
            else {
                MoveToReserveListing();
            }
            return false;
        }
        function BulkMoveSuccessful() {
            alert(MoveFinancialsValidations.BulkMoveSuccessful);
            MoveToReserveListing();
            return false;
        }
        function EditReserveSuccessful() {
            alert(MoveFinancialsValidations.EditReserveSuccessful);
            MoveToReserveListing();
            return false;
        }
        function MoveToReserveListing() {
            //RMA-8490 START
            //var sRedirectString = "/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=" + document.getElementById("txtClaimId").value;
            var sRedirectString = "/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=" + document.getElementById("txtClaimId").value + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value;
            //RMA-8490 END
            window.location.href = sRedirectString;
            return false;
        }
        function MappingFailed() {
            alert(MoveFinancialsValidations.MappingFailed);

            return false;
        }
        function InValidMapping() {
            alert(MoveFinancialsValidations.InValidMapping);

            return false;
        }


    </script>
    <style>
        .disable {
  color: grey;
  font-size: 14px;
}
        .enable {
  color: black;
  font-size: 14px;
}
    </style>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
     
      
        <uc1:errorcontrol id="ErrorControl1" runat="server" />
        <asp:textbox style="display: none" runat="server" name="hTabName" id="hTabName" />
        <asp:textbox style="display: none" id="txtFunctionToCall" runat="server" />
        <asp:textbox style="display: none" id="txtClaimId" runat="server" />
        <asp:textbox style="display: none" id="txtReserveID" runat="server" />
        <asp:textbox style="display: none" id="txtCovgID" runat="server" />
        <asp:textbox style="display: none" id="txtPolicyUnitRowId" runat="server" />
        <asp:textbox style="display: none" id="txtPolicyId" runat="server" />
        <%--<asp:textbox style="display: none" id="txtSourceIds" runat="server" />--%>
        <asp:textbox style="display: none" id="txtLossCode" runat="server" />
        <asp:textbox style="display: none" id="txtClaimantEid" runat="server" />
        <%--<asp:textbox style="display: none" id="txtTargetIds" runat="server" />--%>
        <asp:textbox style="display: none" id="txtMappedIds" runat="server" />

        <asp:textbox style="display: none" id="txtLOB" runat="server" />
        <asp:textbox style="display: none" id="txtSourceText" runat="server" />
        <asp:textbox style="display: none" id="txtTargetText" runat="server" />
        <asp:textbox style="display: none" id="txtIsPolicyMove" runat="server" />
        <asp:hiddenfield id="isMapEnabled" runat="server" />
           <asp:TextBox Style="display: none" ID="ClaimantListClaimantEID" runat="server"></asp:TextBox><%--RMA-8490--%>

        <div id="Div1" class="tabGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" name="TABSEditPolicy" id="TABSEditPolicy">
                <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="EditPolicy"
                    id="LINKTABSEditPolicy"><span style="text-decoration: none">Move Financials</span></a>
            </div>
            <div id="Div2" class="tabSpace" runat="server">
                &nbsp;&nbsp;
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSEditReserve" id="TABSEditReserve">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="EditReserve"
                    id="LINKTABSEditReserve"><span style="text-decoration: none">Move Reserve  </span></a>
            </div>
            <div class="tabSpace">
                &nbsp;&nbsp;
            </div>
        </div>

        <div class="singletopborder" runat="server" name="FORMTABEditPolicy" id="FORMTABEditPolicy">
            <div style="width: 100%">
                <table>
                    <tr>
                        <td>
                            <asp:imagebutton id="btnBulkMove" imageurl="~/Images/tb_save_active.png" class="bold"
                                runat="server" onclick="btnBulkMove_Click"  title="<%$ Resources:btnBulkMove %>"/>


                        </td>

                    </tr>
                </table>
                <br />

                <table style="width: 100%" >

                    <tr>
                        <td>
                             <asp:label text="<%$ Resources:lblSourcePolicy %>" id="Label1" runat="server" font-bold="true"></asp:label>

                            <asp:dropdownlist id="cmbSourcePolicy" runat="server" onselectedindexchanged="cmbSourcePolicy_onchange" autopostback="True"></asp:dropdownlist>
                        </td>
                        <td>
                             <asp:label text="<%$ Resources:lblTargetPolicy %>" id="Label2" runat="server" font-bold="true"></asp:label>
                            <asp:dropdownlist id="cmbTargetPolicy" runat="server" onselectedindexchanged="cmbTargetPolicy_onchange" autopostback="True"></asp:dropdownlist>
                        </td>
                    </tr>
                   
                    <tr>
                        <td  style="text-anchor: start;height: 50%;width:50%">

                            <div style="width: 100%;height:35vh; overflow: auto;">
                                <asp:treeview
                                    id="SourceTree"
                                    populatenodesfromclient="true"
                                    showlines="true"
                                    nodewrap="true"
                                    showexpandcollapse="true"
                                    runat="server"
                                    selectednodestyle-font-bold="true"
                                    selectednodestyle-backcolor="#6cabe4"
                                    forecolor="Black"
                                    target="_self"
                                    enableviewstate="true"
                                    expanddepth="1"
                                    onselectednodechanged="SourceTree_Change"
                                    >
                           </asp:treeview>
                            </div>
                        </td>

                        <td  style="vertical-align: top;width:50%">
                            <div style="width: 100%;height:35vh; overflow: auto;">
                                <asp:treeview
                                    id="TargetTree"
                                    populatenodesfromclient="true"
                                    nodewrap="true"
                                    showlines="true"
                                    showexpandcollapse="true"
                                    runat="server"
                                    selectednodestyle-font-bold="true"
                                    selectednodestyle-backcolor="#6cabe4"
                                    forecolor="Black"
                                    target="_self"
                                    enableviewstate="true"
                                    expanddepth="1">
                          
                           </asp:treeview>
                            </div>
                        </td>
                    </tr>
               
                    <tr style="vertical-align:bottom;">
                        <td >
                            <asp:button id="btnAddMapping" runat="server" text="<%$ Resources:btnAddMapping %>" onclick="btnAddMapping_Click" />
                            <asp:button id="btnRemoveMapping" runat="server" text="<%$ Resources:btnRemoveMapping %>" onclick="btnRemoveMapping_Click"/>
                        </td>
                    </tr>
                </table>

            </div>
           
            <div style="width: 100%;height:30vh; overflow: auto;">
                <asp:gridview id="MappingGrid" autogeneratecolumns="false" runat="server" font-bold="True" datakeynames="MAPPING_ID"
                                cellpadding="0" gridlines="None" cellspacing="0" width="100%" >
		                <HeaderStyle CssClass="ctrlgroup"/>
                        <rowstyle CssClass ="datatd" HorizontalAlign="Left" Font-Bold="false"  />
                        <alternatingrowstyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />

                    <Columns>
                    <asp:TemplateField  HeaderStyle-HorizontalAlign="Left" >
                        <ItemTemplate>
                            <asp:CheckBox ID="chkMapping" runat="server" value='<%# Eval("MAPPING_ID")%>' />
                        </ItemTemplate>
                   </asp:TemplateField>
             <asp:BoundField  HeaderText="Source" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" DataField="SOURCE">
                       
                    </asp:BoundField >
                 
                    <asp:BoundField HeaderText="Target" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" DataField="TARGET" >
                        
                    </asp:BoundField>
                        <asp:TemplateField  >
                        <ItemTemplate>
                             <asp:TextBox runat="server" id="SOURCE_NODE_PATH" Text='<%# Eval("SOURCE_NODE_PATH")%>' style="display:none"></asp:TextBox>
                            </ItemTemplate>
                   </asp:TemplateField>
                        </Columns>
            </asp:gridview>


                <asp:textbox id="txtMappingXML" runat="server" style="display: none;"></asp:textbox>
            </div>
                    
        </div>
      

        <div class="singletopborder" style="display: none;" runat="server" name="FORMTABEditReserve"
            id="FORMTABEditReserve">
            <table>
                <tr>
                    <td>
                        <asp:imagebutton id="btnEditReserve" imageurl="~/Images/tb_save_active.png" class="bold"
                            runat="server" onclick="btnEditReserve_Click" title="<%$ Resources:btnEditReserve %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label text="<%$ Resources:lblSource %>" id="lblSource" runat="server" font-bold="true"></asp:label>
                    </td>
                </tr>

            </table>

            <table width="100%">

                <tr>

                    <td>
                        <asp:label text="<%$ Resources:lblSourceClaimant %>" id="lblSourceClaimant" runat="server" font-bold="true"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>
                    <td>
                        <asp:label text="<%$ Resources:lblSourceClaimantText %>" id="lblSourceClaimantText" runat="server"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>

                    <td>
                        <asp:label text="<%$ Resources:lblsourceLossType %>" id="lblsourceLossType" runat="server" font-bold="true"></asp:label>
                        <asp:label text="<%$ Resources:lblsourceDisabilityType %>" id="lblsourceDisabilityType" runat="server" font-bold="true"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>
                    <td>
                        <asp:label text="<%$ Resources:lblSourceLossTypeText %>" id="lblSourceLossTypeText" runat="server"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>

                </tr>
                <tr>
                    <td>

                        <asp:label text="<%$ Resources:lblReserveTypeID %>" id="lblReserveTypeID" runat="server" font-bold="true"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>
                    <td>
                        <asp:label text="<%$ Resources:lblSourceRsvTypeText %>" id="lblSourceRsvTypeText" runat="server"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>



                    <td>

                        <asp:label text="<%$ Resources:lblReserveBalance %>" id="lblReserveBalance" runat="server" font-bold="true"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>
                    <td>
                        <asp:label text="<%$ Resources:lblSourceBalText %>" id="lblSourceBalText" runat="server"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>


                </tr>
                <tr>

                    <td>
                        <asp:label text="<%$ Resources:lblReserveSubTypeID %>" id="lblReserveSubTypeID" runat="server" font-bold="true"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>
                    <td>
                        <asp:label text="<%$ Resources:lblSubTypeText %>" id="lblSubTypeText" runat="server"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>


                    <td>


                        <asp:label text="<%$ Resources:lblIncurred %>" id="lblIncurred" runat="server" font-bold="true"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>
                    <td>
                        <asp:label text="<%$ Resources:lblSourceIncurredText %>" id="lblSourceIncurredText" runat="server"></asp:label>
                    </td>
                    <td>
                        <br />
                    </td>



                </tr>
            </table>
            <br />
            <br />
            <br />
            <table>

                <tr>
                    <td>
                        <asp:label text="<%$ Resources:lblClaimant %>" id="lblClaimant" runat="server"></asp:label>
                    </td>
                    <td>
                        <asp:dropdownlist runat="server" id="cmbClaimant">
                                </asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label text="<%$ Resources:lblLossType%>" id="lblLossType" runat="server"></asp:label>
                        <asp:label text="<%$ Resources:lblDisabilityType %>" id="lblDisabilityType" runat="server"></asp:label>
                    </td>
                    <td>
                        <asp:dropdownlist runat="server" id="cmbCoverageLossType">
                                </asp:dropdownlist>
                        <asp:dropdownlist runat="server" id="cmbCovDisabilityType">
                                </asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label id="lblTargetReserveTypeID" runat="server" text="<%$ Resources:lblTargetReserveTypeID %>" visible="false"></asp:label>
                    </td>
                    <td>
                        <asp:dropdownlist runat="server" id="cmbReserveType" visible="false">
                                </asp:dropdownlist>
                    </td>
                </tr>
            </table>



        </div>
    </form>
    <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="" />
</body>
</html>
