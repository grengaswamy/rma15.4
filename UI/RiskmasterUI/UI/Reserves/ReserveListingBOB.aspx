﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReserveListingBOB.aspx.cs" ValidateRequest="false" Inherits="Riskmaster.UI.UI.Reserves.ReserveListingBOB" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .hiddencol {
            display: none;
        }

        .viscol {
            display: block;
        }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }

        .modalPopup {
            background-color: #ffffdd;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 350px;
        }
       
    </style>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }
    </script>
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/org.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js"> { var i; } </script>
    <!-- Mits 37856 -->
    <!-- bsharma33 RMA-3887 Reserve Supplemental starts -->
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery/jquery-ui-1.8.17.custom.min.js"></script>
    <link href="../../Scripts/jquery/themes/cupertino/jquery-ui-1.8.17.custom.css" rel="Stylesheet" type="text/css" />
    <!-- bsharma33 RMA-3887 Reserve Supplemental ends -->
    <script src="../../Scripts/angularjs/angular.min.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/Directives/rma-ng-grid.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
    <script src="../../Scripts/angularjs/Controllers/ReserveListingController.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ShowDialog(mode) {
            
            var page = "";
            var popuptitle;
            var iClaimId = $('#claimid').attr('value');
            var iClaimantEID = document.getElementById('claimanteid').value;
            var bMultiCurrency = $('#hdUseMultiCurrency').attr('value');            
            var claimcurrencytype =  document.getElementById('currencytype').value;
            claimcurrencytype = claimcurrencytype.replace(/\&/gi, '^@');
            var claimcurrencyid = document.getElementById('txtReserveCurrID').value;
            var sclaimNumber = document.getElementById('claimnumber').value;
            if (bMultiCurrency == '') {
                bMultiCurrency = "0";
            }
            //RMA-8490 START
            var ClaimantListClaimantEID = document.getElementById('ClaimantListClaimantEID').value;
            if (ClaimantListClaimantEID != '' && ClaimantListClaimantEID != 0)
                iClaimantEID = ClaimantListClaimantEID;
            //RMA-8490 END
            //document.getElementById('lblError1').innerHTML = ""; //rkotak:ng grid, this control seems not used  in the page
            var sAdjusterDetails;
            var sClmCurrBalAmount = 0;
            var sClmCurrResAmount = 0;
            var sStatus = "0", iRsvStatusParent = "", sDisabilityCat = "", sCvgLossType, iPolicyID = "0", sUnit = "";
            var reserveid = document.getElementById("hdnreserveid").value;

            if (reserveid != '') {
                //sStatus = document.getElementById("Stauts").value //rkotak :ng grid,  seems this is not used now
                sDisabilityCat = document.getElementById("DisabilityCat").value;
                sDisabilityCat = sDisabilityCat.replace(/\&/gi, '^@');
                sCvgLossType = document.getElementById("Losstype").value;
                sCvgLossType = sCvgLossType.replace(/\&/gi, '^@');
                iPolicyID = document.getElementById("policyid").value;
                sUnit = document.getElementById("Unit").value;
                sUnit = sUnit.replace(/\&/gi, '^@');
                iRsvStatusParent = document.getElementById("ResStatusParent").value;
            }
            var lob = $('#lob').attr('value');
            if (!ValidateClaimCurrency()) {
                return false;
            }
            //var iReserveID = CheckReserveSelection();
            var iReserveID = reserveid
            if (iReserveID == 0 || mode == "0") {
                iReserveID = 0;
                if (mode == "1") {
                    alert(ReserveListingBOBValidations.SelectRowEditID);
                    return false;
                }
                popuptitle = "Add New Reserve";
            }
            else {

                popuptitle = "Edit Existing Reserve";
            }

            var SysExternalParam = "";
            SysExternalParam = "<SysExternalParam><ClaimId>" + iClaimId + "</ClaimId><LOBQueryString>" + lob + "</LOBQueryString><ClaimantEID>" + iClaimantEID + "</ClaimantEID><PolicyID>" + iPolicyID + "</PolicyID><UnitName>" + sUnit + "</UnitName><ClaimCurrencyCode>" + claimcurrencyid + "</ClaimCurrencyCode></SysExternalParam>";
            var querystring = escape(SysExternalParam + "&claimId=" + iClaimId.toString() + "&recordID=" + iReserveID.toString() + "&multicurrencyonoff=" + bMultiCurrency.toString() + "&lob=" + lob.toString() + "&claimanteid=" + iClaimantEID.toString() + "&claimcurrencytype=" + claimcurrencytype + "&ClaimCurrencyCode=" + claimcurrencyid + "&status=" + iRsvStatusParent + "&discat=" + sDisabilityCat + "&cvglosstyp=" + sCvgLossType + "&policyid=" + iPolicyID + "&UnitName=" + sUnit + "&claimnumber=" + sclaimNumber + "&ClaimantListClaimantEID=" + ClaimantListClaimantEID);
            page = "/RiskmasterUI/UI/FDM/reservecurrent.aspx?SysExternalParam=" + querystring;
           

            var $dialog = $('<div id="res"></div>')
                   .html('<iframe style="border: 0px;width:100%;height:100%;background:white;" src="' + page + '"></iframe>')
                   .dialog({
                       create: function () { $(".ui-dialog-titlebar-close").attr("title", "close") },
                       autoOpen: false,
                       modal: true,
                       height: 510, //aravi5 RMA-15519
                       //vsharma205 Starts Jira: RMA-15631   
                       //width: 710, //aravi5 RMA-15519 Over chrome, edit reserve window is showing double horizontal scroll
                       width: 750,
                       //vsharma205 End Jira: RMA-15631
                       title: popuptitle,
                       resizable: false,
                       draggable: false
                   });
            $dialog.dialog('open');
            return false;
        }

        function TransfertoPayment() {

            var reserveid = document.getElementById("hdnreserveid").value;
            //Rupal:start, for first & final payment
            if (reserveid == '') {

                //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
                if (document.getElementById('hdnEnableFirstAndFinalPayment').value == "true") {
                    if (confirm(ReserveListingBOBValidations.IsFirstnFinal) == false) {

                        document.getElementById('IsFirstFinal').value = "False"; //false                    
                        return false;
                    }
                    else {
                        document.getElementById('IsFirstFinal').value = "True"; //true     
                        var ClaimantListClaimantEID = document.getElementById('ClaimantListClaimantEID').value;
                        if (ClaimantListClaimantEID != '' && ClaimantListClaimantEID != 0)
                            document.getElementById('claimanteid').value = ClaimantListClaimantEID;
                    }
                }
                    //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
                else {
                    alert(ReserveListingBOBValidations.DisableFirstAndFinalPaymentCheck);
                    document.getElementById('IsFirstFinal').value = "False";
                    return false;
                }
                //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
            }
            else {
                document.getElementById('IsFirstFinal').value = "False"; //false              
            }
            //First & final Payment checkbox in split screen will always be readonly when we navigate from this page
            document.getElementById("IsFirstFinalReadOnly").value = "True"; //true
            //Rupal:end, for first & final payment

            //bpaskova 13 Jan 2015: added second condition for fixing JIRA RMA-6270
            //if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlag(iIndex, p_sGridName, iPos))
            if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlagNew())
                return RMX.Reserves.NavigateToFundsBOB("false");
            else
                return false;

            //rupal:end first & final payment
        }

        function TransfertoCollection() {
            //var reserveid = document.getElementById("hdnreserveid").value;
            //if (reserveid == '') {
            //    alert(ReserveListingBOBValidations.SelectRowID);
            //    return false;
            //}
            var reserveid = document.getElementById("hdnreserveid").value;
            //Rupal:start, for first & final payment
            if (reserveid == '') {
                if (document.getElementById('hdnEnableFirstAndFinalPayment').value == "true") {
                    //  if (confirm(ReserveListingBOBValidations.IsFirstnFinal) == false) {
                    if (confirm(ReserveListingBOBValidations.IsFirstnFinal) == false) {

                        document.getElementById('IsFirstFinal').value = "False"; //false                    
                        return false;
                    }
                    else {
                        //bpaskova 13 Jan 2015: added second condition for fixing JIRA RMA-6270
                        if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlagNew()) {
                            document.getElementById('IsFirstFinal').value = "True"; //true   
                            document.getElementById("IsFirstFinalReadOnly").value = "True"; //true
                            var ClaimantListClaimantEID = document.getElementById('ClaimantListClaimantEID').value;
                            if (ClaimantListClaimantEID != '' && ClaimantListClaimantEID != 0)
                                document.getElementById('claimanteid').value = ClaimantListClaimantEID;
                            return RMX.Reserves.NavigateToFundsBOB("true");
                        }
                        else {
                            document.getElementById('IsFirstFinal').value = "False";
                            return false;
                        }


                    }
                }
                else {
                    alert(ReserveListingBOBValidations.DisableFirstAndFinalPaymentCheck);
                    document.getElementById('IsFirstFinal').value = "False";
                    return false;
                }
            }

            else {
                document.getElementById('IsFirstFinal').value = "False"; //false     
                //bpaskova 23 Feb 2015: added second condition for fixing JIRA RMA-6270
                if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlagNew() && RMX.Reserves.CheckForDeductibleNew())
                    //JIRA-857 Starts
                {
                    if ((document.getElementById('PreventCollOnRes').value == -1) || ((document.getElementById('collonlob').value == -1) && (document.getElementById('MasResType').value != "R"))) {
                        alert(ReserveListingBOBValidations.CollectionNotAllowed + " " + document.getElementById('ResType').value);
                        return false;
                    }
                    else
                        //JIRA-857 Ends
                        return RMX.Reserves.NavigateToFundsBOB("true");
                }
                else
                    return false;
            }

        }

        function SetActiveTab(action) {

            var sIsDataLoadedID = document.getElementById("IsDataLoaded");
            sIsDataLoadedID.value = "0";//set is data loaded to 0 so that the reserve listing grid is refreshed
            document.getElementById("txtaction").value = action;
            document.getElementById('currencytype').value = document.getElementById('txtClaimCurrency').value;//MITS:34082 
            document.forms[0].submit();
        }

        function GetReserveTransactiondetails() {
            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid == '') {
                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }
            var popuptitle = ReserveListingBOBValidations.lblReserveTransactionDetail;// "Reserve Transaction Details"
            var PolicyId = document.getElementById('policyid').value;
            var PolCvgRowId = document.getElementById('polcvgid').value;
            var ResTypeCode = document.getElementById('Reserve').value;
            var CovgLossId = document.getElementById('cvglossid').value;
            var ReserveId = document.getElementById('hdnreserveid').value;
            var ClaimId = document.getElementById('claimid').value;
            var ClaimantEId = document.getElementById('claimanteid').value;

            var page = "/RiskmasterUI/UI/Reserves/ReserveTransactionDetail.aspx?PolicyID=" + PolicyId + "&PolicyCvgRowID=" + PolCvgRowId + "&ReserveTypeCode=" + ResTypeCode + "&CvgLossId=" + CovgLossId + "&ReserveId=" + ReserveId + "&ClaimantEId=" + ClaimantEId + "&claimId=" + ClaimId;

            var $dialog = $('<div id="resTrans"></div>')
                   .html('<iframe style="border: 0px;width:100%;height:100%;background:white;" src="' + page + '"></iframe>')
                   .dialog({
                       create: function () { $(".ui-dialog-titlebar-close").attr("title", "close") },
                       autoOpen: false,
                       modal: true,
                       height: 300,
                       width: 1000,
                       title: popuptitle,
                       resizable: true,
                       draggable: true
                   });
            $dialog.dialog('open');
            return false;
        }

        function TransferToFinancialDetailHistory(level) {

            var claimID = document.getElementById('claimid').value;
            var claimnumber = document.getElementById('claimnumber').value;
            var claimantID = document.getElementById('claimanteid').value;
            var policyID = document.getElementById('policyid').value;
            var unitrowid = document.getElementById('txtUnitID').value;
            var coverageID = document.getElementById('polcvgid').value;
            var multicurrencyindicator = document.getElementById('txtUseMulticurrency').value;
            var claimcurrencytype = document.getElementById('txtClaimCurrenyType').value;

            var SBrowser = navigator.appName;

            if (navigator.appName != "Netscape") {
                alert(ReserveListingBOBValidations.UpgradeVersion);
                return false;
            }

            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid != '') {
                var claimID = document.getElementById('claimid').value;
                var claimnumber = document.getElementById('claimnumber').value;
                var claimantID = document.getElementById('claimanteid').value;
                var sclaimantname = document.getElementById("txtClaimants").value;
                var reservetypecode = document.getElementById("Reserve").value;
                var reservestatus = document.getElementById("reservestatus").value;
                var reservetypedesc = document.getElementById("MasResType").value;
                var policyID = policyID;
                var sPolicy = document.getElementById("PolicyName").value;
                var unitrowid = document.getElementById("txtUnitID").value;
                var sUnit = document.getElementById("txtUnitText").value;
                var sPolCvg = document.getElementById("CoverageType").value;
                var coverageID = coverageID;
                var polcvglossID = document.getElementById("cvglossid").value;
                var rcrowID = document.getElementById("hdnreserveid").value;
                document.getElementById('txtReserveSummaryLevel').value = level;
            }
            else {
                var claimID = document.getElementById('claimid').value;
                var claimnumber = document.getElementById('claimnumber').value;
                var claimantID = document.getElementById('claimanteid').value;
                var sclaimantname = document.getElementById("txtClaimants").value;
                var reservetypecode = "0";
                var reservestatus = "0";
                var reservetypedesc = "0";
                var policyID = "0";
                var sPolicy = "0";
                var unitrowid = "0";
                var sUnit = "0";
                var sPolCvg = "0";
                var coverageID = "0";
                var polcvglossID = "0";
                var rcrowID = "0";
                document.getElementById('txtReserveSummaryLevel').value = level;
            }

            document.forms[0].action = "FinancialDetailHistory.aspx?ClaimID=" + claimID + "&ClaimantID=" + claimantID + "&PolicyID=" + policyID + "&CoverageID=" + coverageID + "&UnitID=" + unitrowid + "&UseMulticurrency=" + multicurrencyindicator + "&ClaimCurrency=" + claimcurrencytype + "&CvgLossId=" + polcvglossID + "&ReserveType=" + reservetypecode + "&RcRowId=" + rcrowID + "&ReserveStatus=" + reservestatus + "&ReserveTypeDesc=" + reservetypedesc + "&ClaimNo=" + claimnumber + "&ClaimantName=" + sclaimantname;
            document.forms[0].action += "&Policy=" + sPolicy + "&Unit=" + sUnit + "&Coverage=" + sPolCvg + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value;
            document.forms[0].method = "post";
            document.forms[0].submit();
            return false;
        }

        function TransferToUnitCoverageSummary(level) {

            var policyID = document.getElementById('policyid').value;
            var coverageID = document.getElementById('polcvgid').value;
            var unitrowid = document.getElementById('txtUnitID').value;
            var multicurrencyindicator = document.getElementById('txtUseMulticurrency').value;
            var claimcurrencytype = document.getElementById('txtClaimCurrenyType').value;
            //RMA-8490 START
            //document.forms[0].action = "/RiskmasterUI/UI/Reserves/ReserveSummary.aspx?PolicyID=" + policyID + "&CoverageID=" + coverageID + "&UnitID=" + unitrowid + "&UseMulticurrency=" + multicurrencyindicator + "&ClaimCurrency=" + claimcurrencytype;
            document.forms[0].action = "/RiskmasterUI/UI/Reserves/ReserveSummary.aspx?PolicyID=" + policyID + "&CoverageID=" + coverageID + "&UnitID=" + unitrowid + "&UseMulticurrency=" + multicurrencyindicator + "&ClaimCurrency=" + claimcurrencytype + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value;
            //RMA-8490 END
            document.forms[0].method = "post";


            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid == '') {

                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }
            else if (document.getElementById('txtReserveSummaryLevel') != null) {
                //Ashish Ahuja - MITS 34275 - Display header start
                var sPolicy = document.getElementById("PolicyName").value
                var sUnit = document.getElementById("Unit").value
                var sPolCvg = document.getElementById("CoverageType").value
                document.forms[0].action += "&Policy=" + sPolicy + "&Unit=" + sUnit + "&Coverage=" + sPolCvg;
                //Ashish Ahuja - MITS 34275 - Display header end
                document.getElementById('txtReserveSummaryLevel').value = level;
            }
            document.forms[0].submit();
            return false;
        }

        function TransferToReserveSummary(level) {
            // akaushik5 Changed for MITS 33577 Ends
            //document.forms[0].action = "ReserveSummary.aspx";
            document.forms[0].method = "post";
            // akaushik5 Added for MITS 33577 Starts
            if (document.getElementById('txtReserveSummaryLevel') != null) {
                document.getElementById('txtReserveSummaryLevel').value = level;
            }
            //RMA-8490 START
            if (document.getElementById('ClaimantListClaimantEID').value != '') {
                document.getElementById('txtReserveSummaryLevel').value = '';
            }
            //RMA-8490 END
            // akaushik5 Added for MITS 33577 Ends
            document.forms[0].action = "ReserveListingBOB.aspx?page=ReserveSummary";
            document.forms[0].submit();
            return false;
        }

        function TransfertoReserveHistoryDetail() {
            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid == '') {
                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }
        }

        function TransfertoScheduleChecks() {
            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid == '') {
                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }
            else {
                //bpaskova 23 Feb 2015: added second condition for fixing JIRA RMA-6270
                //if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlag(iIndex, p_sGridName, iPos))
                if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlagNew())
                    return RMX.Reserves.ScheduleChecksBOB();
                else
                    return false;
            }
        }

        function ExportToVSS() {
            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid == '') {
                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }
            else
                return true;
        }

        function MoveFinancials() {

            var iUnitRowId;
            var lob = document.getElementById("lob").value;
            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid == '') {
                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }

            iReserveID = reserveid;
            iPolCvgID = document.getElementById('polcvgid').value;
            iUnitRowId = document.getElementById('txtUnitID').value;
            iPolicyID = document.getElementById('policyid').value;
            iPolCvgLossCode = document.getElementById('losstypecode').value;
            iReserveBalanceAmount = document.getElementById('balanceAmount').value.replace(/[^A-Za-z 0-9 \.,\?""!@#\%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
            sCvgLoss = document.getElementById("Losstype").value;
            iReserveSubType = document.getElementById("reservecatdesc").value;
            iIncurred = document.getElementById('incurredAmount').value.replace(/[^A-Za-z 0-9 \.,\?""!@#\%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
            sReserve = document.getElementById("ResType").value;
            //rkaur27 - JIRA RMA-18673
			//window.location.href = "MoveFinancials.aspx?ClaimID=" + document.getElementById('claimid').value + "&ReserveID=" + iReserveID + "&CovgID=" + iPolCvgID + "&PolicyUnitRowId=" + iUnitRowId + "&PolicyId=" + iPolicyID + "&LOB=" + lob + "&LossCode=" + iPolCvgLossCode + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&ReserveBalanceAmount=" + iReserveBalanceAmount + "&ReserveSubType=" + iReserveSubType + "&Incurred=" + iIncurred + "&ReserveType=" + sReserve + "&Loss=" + sCvgLoss;
            document.forms[0].action = "MoveFinancials.aspx?ClaimID=" + document.getElementById('claimid').value + "&ReserveID=" + iReserveID + "&CovgID=" + iPolCvgID + "&PolicyUnitRowId=" + iUnitRowId + "&PolicyId=" + iPolicyID + "&LOB=" + lob + "&LossCode=" + iPolCvgLossCode + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&ReserveBalanceAmount=" + iReserveBalanceAmount + "&ReserveSubType=" + iReserveSubType + "&Incurred=" + iIncurred + "&ReserveType=" + sReserve + "&Loss=" + sCvgLoss + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value;
            document.forms[0].method = "post";
            document.forms[0].submit();
            //rkaur27 - JIRA RMA-18673
            return false;
        }

        function TransfertoManualDeductible() {
            var reserveid = document.getElementById("hdnreserveid").value;
            if (reserveid == '') {
                alert(ReserveListingBOBValidations.SelectRowID);
                return false;
            }
            else {
                //bpaskova 23 Feb 2015: added second condition for fixing JIRA RMA-6270
                if (RMX.Reserves.CheckForFrozenFlag() && RMX.Reserves.CheckForOnHoldFlagNew() && RMX.Reserves.CheckForDeductibleNew())
                    //JIRA-857 Starts
                {
                    if ((document.getElementById('PreventCollOnRes').value == -1) || ((document.getElementById('collonlob').value == -1) && (document.getElementById('MasResType').value != "R"))) {
                        alert(ReserveListingBOBValidations.CollectionNotAllowed + " " + document.getElementById('ResType').value);
                        return false;
                    }
                    else {
                        iPolCvgID = document.getElementById('polcvgid').value;
                        var StatusObj = document.getElementById('DedTypeCode').value;
                        var ctrltxtNotDetVal = document.getElementById("txtFPDedVal");
                        if (StatusObj != null && StatusObj.innerHTML == ctrltxtNotDetVal.value) {
                            //alert("Manual deductible can not be created for first party.");
                            alert(ReserveListingBOBValidations.ManualDedForFirstParty);
                            return false;
                        }
                        var ctrltxtNoneDedVal = document.getElementById("txtNoneDedVal");
                        if (StatusObj != null && StatusObj.innerHTML == ctrltxtNoneDedVal.value) {
                            //alert("The deductible type associated with this coverage is set to None. Deductible processing does not apply for it.");
                            alert(ReserveListingBOBValidations.ManualDedForNone);
                            return false;
                        }
                        var ctrlDedRcRowId = document.getElementById('DedRcRowId').value;
                        if (ctrlDedRcRowId != null && ctrlDedRcRowId.innerHTML == "0") {
                            //alert("There is no Deductible reserve associated with this payment reserve. Manual Deductible cannot be created.");
                            alert(ReserveListingBOBValidations.ManualDedForNoPymtRC);
                            return false;
                        }
                        var ctrlMasterReserveType = document.getElementById("MasResType");
                        if (ctrlMasterReserveType != null && ctrlMasterReserveType.innerHTML == "R") {
                            alert(ReserveListingBOBValidations.ManualDedForRecRsv);
                            //alert("Manual Deductibles cannot be created for Recovery reserve.");
                            return false;
                        }

                        document.getElementById('txtIsDeductible').value = "1";
                        //JIRA-857 Ends
                        return RMX.Reserves.NavigateToFundsBOB("true");
                    }
                }
                else
                    return false;
            }
        }

    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <asp:TextBox Style="display: none" runat="server" ID="EditLoad" RMXType="hidden" Text="" />
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/ClaimID" ID="claimid" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/Reserves/@claimnumber" ID="claimnumber" runat="server" rmxignoreset="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="claimantrowid" runat="server" ></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/ClaimantEID" ID="claimanteid" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/SubTitle" ID="subtitle" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="policyid" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="polcvgid" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="GridUnitId" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtUseMulticurrency" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="cvglossid" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="Reserve" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="rc_row_id" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/ReserveFunds/LOB" ID="lob" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="caption" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@caption"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/Reserves/@paymentfrozenflag" ID="frozenflag" runat="server" rmxignoreset="true"></asp:TextBox>
        <asp:TextBox Style="display: none" rmxref="/Instance/Document/Reserves/@preventcollonlob" ID="collonlob" runat="server" rmxignoreset="true"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="PreventCollOnRes" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="ResType" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="MasResType" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="PrevResModifyzero" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="claimstatus" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@claimstatus"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="ScheduleCheckViewPermissionOnly" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@viewonly"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="hIsClaimCurrencySet" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@IsClaimCurrencySet"></asp:TextBox>
        <%--Mgaba2: MITS 28450 --%>
        <asp:TextBox Style="display: none" runat="server" ID="SysFormName" RMXType="hidden" Text="ReserveListingBOB" />
        <%--Mgaba2: MITS 28450 --%>
        <asp:TextBox runat="server" ID="currencytype" Text="" Style="display: none" rmxref="/Instance/Claim/CurrencyType" rmxignoreset="true" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID="txtClaimCurrency" Text="" Style="display: none" rmxref="/Instance/Claim/CurrencyType" rmxignoreset="true" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID="TabContainer" Text="" Style="display: none" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID="txtAddEditPopup" Text="" Style="display: none" /><%--MITS:34082--%>
        <asp:TextBox runat="server" ID="txtReserveCurrID" Text="" Style="display: none" rmxref="/Instance/Document/Reserves/CurrencyTypeID" rmxignoreset="true" /><%--MITS:34082--%>
        <asp:TextBox Style="display: none" ID="txtClaimCurrenyType" runat="server"></asp:TextBox><%--MITS:34082--%>
        <asp:TextBox Style="display: none" ID="txtClaimants" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtClaimantEIDs" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtClaimantRowIds" runat="server"></asp:TextBox><%--JIRA 10347--%>
        <asp:TextBox Style="display: none" ID="txtDisabilityCode" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="Selected" runat="server" ClientIDMode="Static"></asp:TextBox>
        <input id="hdUseMultiCurrency" name="" runat="server" type="hidden" />
        <asp:TextBox Style="display: none" ID="txtReserveSummaryLevel" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtReserveSubTypeCode" runat="server"></asp:TextBox>
        <asp:HiddenField ID="hdnEnableFirstAndFinalPayment" runat="server" Value="false" />
        <%--WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298 - End--%>

        <%-- tanwar2 - mits 30910 - 01302013 - start --%>
        <asp:TextBox Style="display: none" ID="txtApplyDedToPayments" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@ApplyDedToPayments"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtDedRecoveryReserveCode" runat="server" rmxignoreset="true" rmxref="/Instance/Document/Reserves/@DedRecoveryReserveCode"></asp:TextBox>
        <%-- tanwar2 - mits 30910 - 01302013 - end --%>
        <asp:TextBox Style="display: none" runat="server" ID="txtNotDetDedVal" rmxref="/Instance/Document/Reserves/@NotDetDedVal"></asp:TextBox>
        <asp:TextBox runat="server" ID="txtIsDeductible" Style="display: none"></asp:TextBox>
        <asp:TextBox Style="display: none" runat="server" ID="txtFPDedVal" rmxref="/Instance/Document/Reserves/@FPDedVal"></asp:TextBox>
        <asp:TextBox Style="display: none" runat="server" ID="txtNoneDedVal" rmxref="/Instance/Document/Reserves/@NoneDedVal"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtUnitID" Text="" runat="server"></asp:TextBox>
        <!--rupal:end-->
        <!--Rupal:Start For First & Final Payment-->
        <asp:TextBox Style="display: none" ID="IsFirstFinal" runat="server" />
        <asp:TextBox Style="display: none" ID="IsFirstFinalReadOnly" runat="server" />
        <asp:TextBox Style="display: none" ID="CovgSeqNum" runat="server" />
        <asp:TextBox Style="display: none" ID="ResStatusParent" runat="server" />
        <asp:TextBox Style="display: none" ID="TransSeqNum" runat="server" />
        <asp:TextBox Style="display: none" ID="CoverageKey" runat="server" />
        <asp:TextBox Style="display: none" ID="losstypecode" runat="server" />
        <asp:TextBox Style="display: none" ID="balanceAmount" runat="server" />
        <asp:TextBox Style="display: none" ID="incurredAmount" runat="server" />
        <!--Ankit Start : Worked on MITS - 34297-->
        <asp:TextBox Style="display: none" ID="txtaction" Text="" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtCancelledShortCode" runat="server" />
        <!--aaggarwal29 : RMA-9856-->
        <asp:TextBox Style="display: none" ID="txtClosedShortCode" runat="server" />
        <!--aaggarwal29 : RMA-9856-->
        <asp:TextBox runat="server" ID="txtUnitText" ReadOnly="true" ForeColor="Black" BackColor="White" Width="250 px" BorderWidth="0" Style="display: none"></asp:TextBox>
        <input id="hdLSSReserve" name="" runat="server" type="hidden" />
        <asp:TextBox Style="display: none" ID="hdnreserveid" runat="server" ClientIDMode="Static"></asp:TextBox>
        <asp:TextBox Style="display: none" ClientIDMode="Static" ID="Stauts" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ClientIDMode="Static" ID="Losstype" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ClientIDMode="Static" ID="DisabilityCat" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ClientIDMode="Static" ID="Unit" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ClientIDMode="Static" ID="CoverageType" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ClientIDMode="Static" ID="PolicyName" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="txtReserveTypeCode" Text="" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ClientIDMode="Static" ID="reservestatus" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="IsDataLoaded" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="DedTypeCode" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="DedRcRowId" runat="server"></asp:TextBox>
        <asp:Label runat="server" ID="lblUnit" ForeColor="Black" BackColor="White" Style="display: none"></asp:Label>
        <asp:TextBox Style="display: none" ID="reservecatdesc" runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" ID="ClaimantListClaimantEID" runat="server"></asp:TextBox><%--RMA-8490--%>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldata">
            <tr>
                <td>
                    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="100%" Text="<%$ Resources:lblcaptionID %> "></asp:Label></td>
            </tr>
        </table>

        <div ng-app="rmaApp" id="ng-app">
            <div ng-controller="ReserveListingController">
                <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="right">
                                <div class="toolBarButton" runat="server" id="div_addreserve" xmlns="" xmlns:cul="remove">
                                    <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
                                    <asp:ImageButton runat="server" src="../../Images/tb_addreserve_active.png" OnClientClick="return ShowDialog('0');"
                                        Width="28" Height="28" border="0" ID="AddReserve" AlternateText="<%$ Resources:ttAddReserve %>" onmouseover="this.src='../../Images/tb_addreserve_mo.png';;this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_addreserve_active.png';;this.style.zoom='100%'" title="<%$ Resources:ttAddReserve %>" />
                                    <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
                                </div>
                                <div class="toolBarButton" runat="server" id="div_editreserve" xmlns="" xmlns:cul="remove">
                                    <%-- bsharma33 RMA-3887 Reserve supplementals starts --%>
                                    <asp:ImageButton runat="server" OnClientClick="return ShowDialog('1');"
                                        src="../../Images/tb_editreserve_active.png" Width="28" Height="28" border="0" ID="EditReserve" AlternateText="<%$ Resources:ttEditReserve %>"
                                        onmouseover="this.src='../../Images/tb_editreserve_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_editreserve_active.png';this.style.zoom='100%'" title="<%$ Resources:ttEditReserve %>" />
                                    <%-- bsharma33 RMA-3887 Reserve supplementals ends --%>
                                </div>
                                <div class="toolBarButton" runat="server" id="div_makepayment" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick='if(ValidateClaimCurrency()) return TransfertoPayment(); else return false; ' src="../../Images/tb_payment_active.png"
                                        Width="28" Height="28" border="0" ID="MakePayment" AlternateText="<%$ Resources:ttPayment %>" onmouseover="this.src='../../Images/tb_payment_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_payment_active.png';this.style.zoom='100%'" title="<%$ Resources:ttPayment %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div_addcollection" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick='if(ValidateClaimCurrency()) return TransfertoCollection(); else return false;' src="../../Images/tb_collection_active.png"
                                        Width="28" Height="28" border="0" ID="AddCollection" AlternateText="<%$ Resources:ttAddColl %>"
                                        onmouseover="this.src='../../Images/tb_collection_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_collection_active.png';this.style.zoom='100%'" title="<%$ Resources:ttAddColl %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div_ManualDeductible" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick='if(ValidateClaimCurrency()) return TransfertoManualDeductible(); else return false;' src="../../Images/tb_ManualDeductible.png"
                                        Width="28" Height="28" border="0" ID="AddManualDeductible" AlternateText="<%$ Resources:ttAddManDed %>"
                                        onmouseover="this.src='../../Images/tb_ManualDeductible_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_ManualDeductible.png';this.style.zoom='100%'" title="<%$ Resources:ttAddManDed %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div_paymenthistory" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return RMX.Reserves.AssignValue();" src="../../Images/tb_paymenthistory_active.png"
                                        Width="28" Height="28" border="0" ID="PaymentHistory" AlternateText="<%$ Resources:ttPayHist %>" onmouseover="this.src='../../Images/tb_paymenthistory_mo.png';;this.style.zoom='110%'"
                                        OnClick="PaymentHistory_Click" onmouseout="this.src='../../Images/tb_paymenthistory_active.png';this.style.zoom='100%'" title="<%$ Resources:ttPayHist %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div_schedulecheck" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick='if(ValidateClaimCurrency()) return TransfertoScheduleChecks(); else return false;' src="../../Images/tb_schedulecheck_active.png"
                                        Width="28" Height="28" border="0" ID="ScheduleCheck" AlternateText="<%$ Resources:ttSchCheck %>" onmouseover="this.src='../../Images/tb_schedulecheck_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_schedulecheck_active.png';this.style.zoom='100%'" title="<%$ Resources:ttSchCheck %>" />
                                </div>
                              <%--  <div class="toolBarButton" runat="server" id="div_summary" xmlns="" xmlns:cul="remove">--%>
                                    <%--akaushik5 Changed for MITS 33577 Starts--%>
                                    <%--<asp:imagebutton runat="server" onclientclick="return TransferToReserveSummary();"  src="../../Images/tb_finsummary_active.png"
                        width="28" height="28" border="0" id="Summary" alternatetext="<%$ Resources:ttSummary %>" onmouseover="this.src='../../Images/tb_finsummary_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_finsummary_active.png';this.style.zoom='100%'" />--%>
                    <%--    <asp:ImageButton runat="server" OnClientClick="return TransferToReserveSummary('claimant');" src="../../Images/tb_finsummary_active.png"
                                        Width="28" Height="28" border="0" ID="Summary" AlternateText="<%$ Resources:ttSummary %>" onmouseover="this.src='../../Images/tb_finsummary_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_finsummary_active.png';this.style.zoom='100%'" title="<%$ Resources:ttSummary %>" />
                                </div>--%>
                                <%--akaushik5 Changed for MITS 33577 Ends--%>
                                <div class="toolBarButton" runat="server" id="div1" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return TransfertoReserveHistoryDetail();" src="../../Images/tb_ofac_active.png"
                                        Width="28" Height="28" border="0" ID="ReserveHistory" AlternateText="<%$ Resources:ttResHist %>" onmouseover="this.src='../../Images/tb_ofac_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_ofac_active.png';this.style.zoom='100%'"
                                        OnClick="ReserveHistory_Click" title="<%$ Resources:ttResHist %>" />
                                </div>
                                <%-- tanwar2 - mits 30910 - 01302013 - start --%>
                                <div class="toolBarButton" runat="server" id="div_deductible" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" src="../../Images/tb_deductiblesummary_active.png"
                                        Width="28" Height="28" border="0" ID="Deductible" AlternateText="<%$ Resources:ttDeductibleSummary %>" onmouseover="this.src='../../Images/tb_deductiblesummary_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_deductiblesummary_active.png';this.style.zoom='100%'" OnClick="Deductible_Click" title="<%$ Resources:ttDeductibleSummary %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div_deductibleEvent" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" src="../../Images/tb_deductibleeventsummary_active.png"
                                        Width="28" Height="28" border="0" ID="DeductibleEvent"
                                        AlternateText="<%$ Resources:ttDeductibleEventSummary %>" onmouseover="this.src='../../Images/tb_deductibleeventsummary_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_deductibleeventsummary_active.png';this.style.zoom='100%'"
                                        OnClick="DeductibleEvent_Click" title="<%$ Resources:ttDeductibleEventSummary %>" />
                                </div>
                                <%-- tanwar2 - mits 30910 - 01302013 - end --%>
                                <%--MITS 28450--%>
                                <div class="toolBarButton" runat="server" id="div_enhancednotes" xmlns="" xmlns:mc="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return EnhancedNotes();" src="../../Images/tb_enhancednotes_active.png" Width="28" Height="28" border="0" ID="enhancednotes" AlternateText="<%$ Resources:ttEnhNotes %>" onMouseOver="this.src='../../Images/tb_enhancednotes_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_enhancednotes_active.png';this.style.zoom='100%'" title="<%$ Resources:ttEnhNotes %>" />
                                </div>
                                <%--start - averma62--%>
                                <div class="toolBarButton" runat="server" id="div_vssexport" xmlns="" xmlns:mc="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return ExportToVSS();" OnClick="VssExport_Click" src="../../Images/tb_exporttoVSS_active.png" Width="28" Height="28" border="0" ID="VssExport" AlternateText="<%$ Resources:ttvssexport %>" onMouseOver="this.src='../../Images/tb_exporttoVSS_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_exporttoVSS_active.png';this.style.zoom='100%'" title="<%$ Resources:ttvssexport %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div_lssexport" xmlns="" xmlns:mc="remove">
                                    <asp:ImageButton src="../../Images/tb_LSS_active.png" runat="server" Width="28" Height="28" border="0" ID="LSSExport" Visible="false" AlternateText="<%$ Resources:ttlssexport %>" title="<%$ Resources:ttlssexport %>"
                                        onMouseOver="this.src='../../Images/tb_LSS_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_LSS_active.png';this.style.zoom='100%'" OnClick="LssExport_Click" />
                                </div>
                                <%--End Mits 35472--%>
                                <%--akaushik5 Added for MITS 33577 Starts--%>
                                <div class="toolBarButton" runat="server" id="div_claimsummary" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return TransferToReserveSummary('claim');" src="../../Images/tb_claimreservesummary_active.png" Width="28" Height="28" border="0" ID="ClaimSummary" AlternateText="<%$ Resources:ttClmReserveSummary %>" onmouseover="this.src='../../Images/tb_claimreservesummary_mo.png';this.style.zoom='110%'" onmouseout="this.src='../../Images/tb_claimreservesummary_active.png';this.style.zoom='100%'" title="<%$ Resources:ttClmReserveSummary %>" />
                                </div>
                                <%--akaushik5 Added for MITS 33577 Ends--%>
                                <%-- MITS 34275- RMA Swiss Re Financial Summary START --%>
                                <div class="toolBarButton" runat="server" id="div_unitcoveragesummary" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return TransferToUnitCoverageSummary('Policy');"
                                        src="../../Images/tb_unitcoveragesummary_active.png" Width="28" Height="28" border="0"
                                        ID="UnitCoveragefinancialSummary" AlternateText="<%$ Resources:ttUnitCoveragefinancialSummary %>" onmouseover="this.src='../../Images/tb_unitcoveragesummary_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_unitcoveragesummary_active.png';this.style.zoom='100%'" title="<%$ Resources:ttUnitCoveragefinancialSummary %>" />
                                </div>
                                <%-- MITS 34275- RMA Swiss Re Financial Summary END --%>
                                <div class="toolBarButton" runat="server" id="div2" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return TransferToFinancialDetailHistory('Claim');"
                                        src="../../Images/tb_financialdetailhistory_active.png" Width="28" Height="28" border="0"
                                        ID="FinancialDetailHistory" AlternateText="<%$ Resources:ttFDHforCarrier %>" onmouseover="this.src='../../Images/tb_financialdetailhistory_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_financialdetailhistory_active.png';this.style.zoom='100%'" title="<%$ Resources:ttFDHforCarrier %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div_MoveFinancials" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return MoveFinancials();"
                                        src="../../Images/tb_MovetoFinancial_active.png" Width="28" Height="28" border="0"
                                        ID="MoveFinancials" AlternateText="<%$ Resources:ttMoveFinancials %>" onmouseover="this.src='../../Images/tb_MovetoFinancial_mo.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/tb_MovetoFinancial_active.png';this.style.zoom='100%'" title="<%$ Resources:ttMoveFinancials %>" />
                                </div>
                                <div class="toolBarButton" runat="server" id="div3" xmlns="" xmlns:cul="remove">
                                    <asp:ImageButton runat="server" OnClientClick="return GetReserveTransactiondetails();"
                                        src="../../Images/reserve_trans_detail.png" Width="28" Height="28" border="0"
                                        ID="ReserveTransactionDetail" AlternateText="<%$ Resources:ttReserveTransactionDetail %>" onmouseover="this.src='../../Images/reserve_trans_detail_MO.png';this.style.zoom='110%'"
                                        onmouseout="this.src='../../Images/reserve_trans_detail.png';this.style.zoom='100%'" title="<%$ Resources:ttReserveTransactionDetail %>" />
                                </div>
                            </td>
                            <td align="right">
                                <div align="right">
                                    <asp:Label runat="server" ID="lblCurrencytype" Text="<%$ Resources:lblCurrencytype %>" Font-Bold="True"></asp:Label>
                                    <asp:DropDownList runat="server" ID="drdCurrencytype" OnSelectedIndexChanged="drdCurrencytype_onchange" rmxref="/Instance/Document/ReserveFunds/CurrencyType" itemsetref="/Instance/Document/Reserves/CurrencyTypeList" rmxtype="combobox" type="combobox"></asp:DropDownList>

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblNoPermissionMessage" Width="100%"></asp:Label>
                </div>
                <div style="clear: both"></div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldata">
                    <tr></tr>
                    <tr>
                        <td style="width: 4%">
                            <span id="Span1" runat="server" width="100%"></span>
                        </td>
                        <td></td>
                    </tr>
                    <tr width="100%" class="msgheader">
                        <td colspan="2">
                            <span id="Span2" width="100%"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 4%">
                            <span id="lblLevel" runat="server" width="100%"></span>
                        </td>
                    </tr>
                </table>

                <input type="hidden" id="hdnJsonData" runat="server" />
                <input type="hidden" id="hdnJsonUserPref" runat="server" />
                <input type="hidden" id="hdnJsonAdditionalData" runat="server" />
                <div id="gridDiv" runat="server">
                    <rma-grid id="gridReserveListing" name="ReserveListig" multiselect="false" showselectioncheckbox="false" enablerowselection="true"></rma-grid>
                </div>
                <div style="text-align: center;">
                    <label style="display: none;" id="lblEmptyData" />
                </div>

                <div style="clear: both"></div>
            </div>
        </div>

    </form>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
</body>
</html>
