﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/06/2013 | 34082   | pgupta93   | Changes req for MultiCurrency
 * 12/03/2014 | 34275  | abhadouria |  RMA Swiss Re Financial Summary 
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Collections;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using AjaxControlToolkit;
using System.Text;
using System.Web.Services;
using System.Collections.Specialized;
using System.Threading;
using System.Globalization;
using Riskmaster.UI.Shared.Controls; //rupal:mits 27373
using Riskmaster.RMXResourceManager;
using Newtonsoft.Json;
using System.Web.Services;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class ReserveListingBOB : NonFDMBasePageCWS
    {
        public XmlElement CurrencytypeList = null;
        string enableLSS = string.Empty;
        static string sSuccess = "Success";
        string exportToLss = string.Empty;
        public string sCWSOutput = String.Empty; //rupal;mits 27373
        private bool bApplyDeductibles = false;
        private string TotalReservesCount = "0";
        private bool bBindReserveData = true;
        protected void Page_Init(object sender, EventArgs e)
        {
            string[] arrClaimantNames = null;
            string[] arrClaimantEIDs = null;
            int iLength = 0;
            //rupal:multicurrency
            string sUseMulticurrency = string.Empty;
            string sLobCode = string.Empty;
            string sPrevtResModZero = string.Empty;

            try
            {
                string sSubmitedFrmRsv = AppHelper.GetQueryStringValue("page");
                if (!string.IsNullOrEmpty(sSubmitedFrmRsv))
                {
                    if (sSubmitedFrmRsv == "ReserveSummary")
                    {
                        Server.Transfer("ReserveSummary.aspx?ClaimantListClaimantEID=" + ClaimantListClaimantEID.Text);
                    }
                }
                if (!IsPostBack)
                {

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                arrClaimantNames = null;
                arrClaimantEIDs = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region javascript registration
            XmlDocument objXmlDoc = null;  //akaur9 MITS 27907 --Start
            //RMA-11468 pgupta93 START
            string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);
            string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

            string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);
            //RMA-11468 pgupta93 END
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ReserveListingBOB.aspx"), "ReserveListingBOBValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ReserveListingBOBValidations", sValidationResources, true);
            #endregion
            try
            {
                objXmlDoc = new XmlDocument();
                if (!Page.IsPostBack)
                {
                    claimid.Text = AppHelper.GetQueryStringValue("ClaimId");
                    ClaimantListClaimantEID.Text = AppHelper.GetQueryStringValue("ClaimantListClaimantEID");//RMA-8490
                    IsDataLoaded.Text = "0";//set is data loaded to 0 when the pade is loaded for the first time                    
                }

                //preeti:start,jira 341
                if (ViewState["Error"] != null)
                {

                    string error = ViewState["Error"].ToString();
                    objXmlDoc.LoadXml(error);
                    if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        HttpContext.Current.Items["Error"] = error;
                        ErrorControl1.errorDom = HttpContext.Current.Items["Error"].ToString();
                    }
                    ViewState["Error"] = null;
                }
                //preeti:end,jira 341
                {
                    if (ErrorControl1.errorhandle == string.Empty)
                        ErrorControl1.errorDom = string.Empty;
                }
                txtAddEditPopup.Text = "";
                if (txtaction.Text == "Add" || txtaction.Text == "AddMore" || txtaction.Text == "Approve" || txtaction.Text == "Reject")//rupal:jira 341 //bsharma33 RMA-3887 Reserve supp starts
                {
                    currencytype.Text = txtClaimCurrency.Text;//MITS:34082
                    //bsharma33 RMA-3887 Reserve supp starts                   
                    if (txtaction.Text == "AddMore")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "ShowDialog('0');", true);
                    }
                    //bsharma33 RMA-3887 Reserve supp ends

                    txtaction.Text = "";
                }


                if (ErrorControl1 != null)
                {
                    if (ErrorControl1.errorhandle == string.Empty)
                        ErrorControl1.errorDom = string.Empty;
                }

                if (IsDataLoaded.Text == "0")
                {
                    GetReserveListing();
                    //BindCurrencyTypeDropDown();
                }
                #region Make controls visible /invisible based on settings, this code has to be after GetReserveListing() functin is called
                if (AppHelper.ReadCookieValue("EnableVSS").Equals("-1"))
                    div_vssexport.Visible = true; //JIRA RMA-442
                else
                    div_vssexport.Visible = false;

                if (ViewState["IsMultiCurrencyEnabled"] != null && ViewState["IsMultiCurrencyEnabled"].ToString() == "0")
                {
                    drdCurrencytype.Visible = false;
                    lblCurrencytype.Visible = false;
                }
                //Added by sharishkumar for Mits 35472
                if (ViewState["enableLSS"] != null && ViewState["enableLSS"].ToString() == "True")
                    LSSExport.Visible = true;
                else
                    LSSExport.Visible = false;
                //End Mits 35472
                //rupal:end
                #endregion
            }
            catch (Exception ex)
            {

            }
        }

        public void GetReserveListing()
        {
            XmlDocument resultDoc = new XmlDocument();
            XElement messageElement;
            string sCWSresponse = "";
            XmlElement xmlIn;
            string AdditionData = string.Empty;

            try
            {

                messageElement = GetReserveListingMessageTemplate();
                CallCWS("ReserveFundsAdaptor.GetClaimReservesBOB", messageElement, out sCWSresponse, true, true);//rupal:ng change

                resultDoc.LoadXml(sCWSresponse);
                MakeToolBarButtonsvisibleInvisible(resultDoc);
                if (bBindReserveData)
                {
                    xmlIn = (XmlElement)resultDoc.SelectSingleNode("//Reserve//Data");
                    if (xmlIn != null)
                        hdnJsonData.Value = xmlIn.InnerText;
                }
                else
                {
                    hdnJsonData.Value = "[]";
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//Reserve//UserPref");
                if (xmlIn != null)
                    hdnJsonUserPref.Value = xmlIn.InnerText;
                hdUseMultiCurrency.Value = "0";//rkotak: ng grid
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//Reserve//AdditionalData");
                if (xmlIn != null)
                {
                    hdnJsonAdditionalData.Value = xmlIn.InnerText;

                    AdditionData = xmlIn.InnerText;
                    AdditionData = AdditionData.Replace("[", "");
                    AdditionData = AdditionData.Replace("]", "");
                    Dictionary<string, string> Symptoms = new Dictionary<string, string>();
                    JsonConvert.PopulateObject(AdditionData.Trim(), Symptoms);
                    hdUseMultiCurrency.Value = Symptoms["Multicurrency"];
                    //RMA-8490 START
                    if ((!string.IsNullOrEmpty(ClaimantListClaimantEID.Text)) && ClaimantListClaimantEID.Text != "0")
                    {
                        claimantrowid.Text = Symptoms["ClaimantRowID"];
                    }
                    //RMA-8490 END
                    TotalReservesCount = Symptoms["TotalCount"];
                    //rma 16494 strats
                    if (Symptoms["BaseCurrCode"] == Symptoms["ClaimCurrCode"])
                        drdCurrencytype.AutoPostBack = false;
                    else
                        drdCurrencytype.AutoPostBack = true;
                    //rma 16494 ends
                }
                ViewState["IsMultiCurrencyEnabled"] = hdUseMultiCurrency.Value;//rkotak: ng grid
                hdnEnableFirstAndFinalPayment.Value = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["EnableFirstAndFinalPayment"].Value;
                lob.Text = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["lobcode"].Value;
                claimnumber.Text = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["claimnumber"].Value;
                frozenflag.Text = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["paymentfrozenflag"].Value;
                claimnumber.Text = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["claimnumber"].Value;
                caption.Text = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["caption"].Value;
                lblcaption.Text = String.Format("{0} ({1})", RMXResourceProvider.GetSpecificObject("lblcaptionID", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), caption.Text);
                collonlob.Text = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["preventcollonlob"].Value;
                txtReserveCurrID.Text = resultDoc.SelectSingleNode("//CurrencyTypeID").InnerText;
                txtClaimCurrency.Text = resultDoc.SelectSingleNode("//CurrencyTypeForClaim").InnerText; ;//rupal:ng grid
                currencytype.Text = resultDoc.SelectSingleNode("//CurrencyTypeForClaim").InnerText;
                //Reservecurrencytypetext.CodeText = resultDoc.SelectSingleNode("//CurrencyTypeForClaim").InnerText;//rupal:ng grid
                enableLSS = resultDoc.SelectSingleNode("//Document//Reserves").Attributes["rmxlssenable"].Value;
                ViewState["enableLSS"] = enableLSS;//rkotak: ng grid
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//CurrencyTypeList");
                if (xmlIn != null)
                    CurrencytypeList = xmlIn;
                IsDataLoaded.Text = "1";

            }
            catch (Exception ex)
            {

            }
            finally
            {
                resultDoc = null;
                messageElement = null; sCWSresponse = "";
                xmlIn = null;
            }

        }

        private void MakeToolBarButtonsvisibleInvisible(XmlDocument objXmlDoc)
        {
            bool bIsViewPermission = true;//Add by kuladeep for SMS Permission
            //Mgaba2: MITS 28450
            XmlNode xmlNode = objXmlDoc.SelectSingleNode("//HideEnhancedNotes");
            if (xmlNode != null && xmlNode.InnerText == "True")
            {
                div_enhancednotes.Visible = false;
            }
            //aaggarwal29 : RMA-9856 start
            xmlNode = objXmlDoc.SelectSingleNode("//CancelledShortCode");
            if (xmlNode != null)
            {
                txtCancelledShortCode.Text = xmlNode.InnerText;
            }

            xmlNode = objXmlDoc.SelectSingleNode("//ClosedShortCode");
            if (xmlNode != null)
            {
                txtClosedShortCode.Text = xmlNode.InnerText;
            }
            //aaggarwal29 : RMA-9856 end

            //rupal:start, following code needs to be moved to application layer, come again
            ////tanwar2 - mits 30910 - 01302013 - start
            if (string.Compare(txtApplyDedToPayments.Text, "-1") != 0)
            {
                div_deductible.Visible = false;
                div_deductibleEvent.Visible = false;
            }
            //rupal:following code i smoved to application layer
            //if (!string.IsNullOrEmpty(txtDedRecoveryReserveCode.Text))
            //{
            //    string sDedRecReserveCode = txtDedRecoveryReserveCode.Text;
            //}
            //tanwar2 - mits 30910 - 01302013 - end
            //rupal:end

            //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
            if (objXmlDoc.SelectSingleNode("//Reserves/@EnableFirstAndFinalPayment") != null)
            {
                hdnEnableFirstAndFinalPayment.Value = objXmlDoc.SelectSingleNode("//Reserves/@EnableFirstAndFinalPayment").InnerText;
            }
            //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
            if (objXmlDoc.SelectSingleNode("//Reserves/@AllowManualdeductible") != null && objXmlDoc.SelectSingleNode("//Reserves/@AllowManualdeductible").InnerText.ToLower() == "false")
            {
                AddManualDeductible.Visible = false;
                div_ManualDeductible.Visible = false;
            }
            #region Apply SMS Permission by kuladeep MITS:34720  Start
            if (objXmlDoc.SelectSingleNode("//Reserves/@IsReservePermission") != null)
            {
                if (objXmlDoc.SelectSingleNode("//Reserves/@IsReservePermission").InnerText == "0")
                {
                    AddReserve.Enabled = false;
                    EditReserve.Enabled = false;
                    PaymentHistory.Enabled = false;
                    bIsViewPermission = false;
                }
                else
                {
                    if (objXmlDoc.SelectSingleNode("//Reserves/@IsAddPermission") != null)
                    {
                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsAddPermission").InnerText == "1")
                        {
                            AddReserve.Enabled = false;
                        }
                        else
                            AddReserve.Enabled = true;
                    }

                    if (objXmlDoc.SelectSingleNode("//Reserves/@IsUpdatePermission") != null)
                    {
                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsUpdatePermission").InnerText == "1")
                        {
                            EditReserve.Enabled = false;
                        }
                        else
                            EditReserve.Enabled = true;
                    }

                    if (objXmlDoc.SelectSingleNode("//Reserves/@IsViewPermission") != null)
                    {
                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsViewPermission").InnerText == "1")
                        {
                            bIsViewPermission = false;
                        }
                        else
                            bIsViewPermission = true;
                    }

                    if (objXmlDoc.SelectSingleNode("//Reserves/@IsPaymentHistPermission") != null)
                    {
                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsPaymentHistPermission").InnerText == "1")
                        {
                            PaymentHistory.Enabled = false;
                        }
                        else
                            PaymentHistory.Enabled = true;
                    }
                    if (objXmlDoc.SelectSingleNode("//Reserves/@IsMoveToPermission") != null)
                    {
                        if (objXmlDoc.SelectSingleNode("//Reserves/@IsMoveToPermission").InnerText == "1")
                        {
                            MoveFinancials.Visible = false;
                        }
                        else
                            MoveFinancials.Visible = true;
                    }
                }
            }

            #endregion Apply SMS Permission by kuladeep MITS:34720 End
            //rupal:start check how this code can be implemented now, come again
            if (!bIsViewPermission)//Add IF condition for View Permission as per SMS Setting by kuladeep for MITS:34720
            {
                lblNoPermissionMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    " + RMXResourceProvider.GetSpecificObject("lblNoResPermission", RMXResourceProvider.PageId("ReserveListingBOB.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + "</h3></p>";
                bBindReserveData = false;
                lblCurrencytype.Visible = false;
                drdCurrencytype.Visible = false;
                gridDiv.Attributes.Add("style", "display:none");
            }
            else
            {
                lblNoPermissionMessage.Text = "";
                bBindReserveData = true;
            }
            //rupal:end
        }

        public XElement GetReserveListingMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.GetClaimReservesBOB</Function></Call><Document>");
            sXml = sXml.Append("<Reserve>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(claimid.Text);
            sXml = sXml.Append("</ClaimID>");
            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(ClaimantListClaimantEID.Text);//RMA-8490
            sXml = sXml.Append("</ClaimantEID>");
            sXml = sXml.Append("<CurrencyType>");
            sXml = sXml.Append(Selected.Text);
            sXml = sXml.Append("</CurrencyType>");
            sXml = sXml.Append("<PageName>");
            sXml = sXml.Append("ReserveListingBOB.aspx");
            sXml = sXml.Append("</PageName>");
            sXml = sXml.Append("<GridId>");
            sXml = sXml.Append("gridReserveListing");
            sXml = sXml.Append("</GridId>");
            sXml = sXml.Append("</Reserve></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }

        protected void PaymentHistory_Click(object sender, ImageClickEventArgs e)
        {
            //RMA-8490 START
           // string sRedirectString = String.Format("../Funds/PaymentHistory.aspx?ClaimId={0}&ClaimantEid={1}&ClaimantRowId={2}&UnitId={3}&UnitRowId={4}&SubTitle={5}&FrozenFlag={6}&ClaimNumber={7}&Caption={8}&CheckMode=1", claimid.Text, claimanteid.Text, claimantrowid.Text, "", "", "", "", claimnumber.Text, "");
            //rkotak:starts, rma 19440
            //when payment history is opened from claim level financial\reserves, we dont want to pass claimant eid as payment history will display records at claim level, claimant level payment history can be viewed when user comes from claims->financial\reserves
            string sRedirectString = String.Format("../Funds/PaymentHistory.aspx?ClaimId={0}&ClaimantEid={1}&ClaimantRowId={2}&UnitId={3}&UnitRowId={4}&SubTitle={5}&FrozenFlag={6}&ClaimNumber={7}&Caption={8}&ClaimantListClaimantEID={9}&CheckMode=1", claimid.Text, "", "", "", "", "", "", claimnumber.Text, "", ClaimantListClaimantEID.Text);
            //string sRedirectString = String.Format("../Funds/PaymentHistory.aspx?ClaimId={0}&ClaimantEid={1}&ClaimantRowId={2}&UnitId={3}&UnitRowId={4}&SubTitle={5}&FrozenFlag={6}&ClaimNumber={7}&Caption={8}&ClaimantListClaimantEID={9}&CheckMode=1", claimid.Text, claimanteid.Text, claimantrowid.Text, "", "", "", "", claimnumber.Text, "", ClaimantListClaimantEID.Text);
            //RMA-8490 END
            //Server.Transfer(sRedirectString);
            //Neha changed to Response. Redirect as telrik gives an issue on filter.
            Response.Redirect(sRedirectString);

        }


        protected void ReserveTransactionDetail_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ReserveHistory_Click(object sender, ImageClickEventArgs e)
        {
            //RMA-8490 START
           // string sRedirectString = String.Format("../Reserves/ViewReserveDetail.aspx");
            string sRedirectString = String.Format("../Reserves/ViewReserveDetail.aspx?ClaimantListClaimantEID=" + ClaimantListClaimantEID.Text);
            //RMA-8490 END
            Server.Transfer(sRedirectString);
        }

        //tanwar2 - NIS
        protected void Deductible_Click(object sender, ImageClickEventArgs e)
        {
            string sRedirectString = String.Format("../Deductible/DeductibleGrid.aspx?ClaimantListClaimantEID=" + ClaimantListClaimantEID.Text);
            Server.Transfer(sRedirectString);
        }

        protected void DeductibleEvent_Click(object sender, ImageClickEventArgs e)
        {
            string sRedirectString = String.Format("../Deductible/DeductibleEventSummary.aspx?ClaimantListClaimantEID=" + ClaimantListClaimantEID.Text);
            Server.Transfer(sRedirectString);
        }

        private string GetLSSExportTemplate()
        {

            exportToLss = hdLSSReserve.Value.ToString();

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.ReserveExport</Function></Call><Document>");

            sXml = sXml.Append("<BOB>");
            //claimid and claimant eid are not used at adaptor function, only ReserveID is used
            //sXml = sXml.Append("<ClaimID>");
            //sXml = sXml.Append(claimid.Text);
            //sXml = sXml.Append("</ClaimID>");

            //sXml = sXml.Append("<ClaimantEID>");
            //sXml = sXml.Append(claimanteid.Text);
            //sXml = sXml.Append("</ClaimantEID>");

            sXml = sXml.Append("<ReserveID>");
            sXml = sXml.Append(exportToLss);
            sXml = sXml.Append("</ReserveID>");

            sXml = sXml.Append("</BOB></Document></Message>");

            return sXml.ToString();
        }
        protected void LssExport_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSRequest = String.Empty;
            exportToLss = hdLSSReserve.Value.ToString();
            sCWSRequest = GetLSSExportTemplate();
            if (!string.IsNullOrEmpty(exportToLss))
            {
                sCWSOutput = AppHelper.CallCWSService(sCWSRequest);
            }
            GetReserveListing();
        }

        private string GetVSSExportTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>VSSInterface.VSSExport</Function></Call><Document>");

            sXml = sXml.Append("<BOB>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(claimid.Text);
            sXml = sXml.Append("</ClaimID>");

            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(claimanteid.Text);
            sXml = sXml.Append("</ClaimantEID>");

            sXml = sXml.Append("<ReserveID>");
            sXml = sXml.Append(rc_row_id.Text);
            sXml = sXml.Append("</ReserveID>");

            sXml = sXml.Append("</BOB></Document></Message>");

            return sXml.ToString();
        }
        protected void VssExport_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSRequest = String.Empty;
            sCWSRequest = GetVSSExportTemplate();
            sCWSOutput = AppHelper.CallCWSService(sCWSRequest);
            GetReserveListing();
        }


        /// <summary>
        /// this function is sused to save preferences for the grid in the database
        /// </summary>
        /// <param name="gridPreference"></param>
        /// <param name="gridId"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string SavePreferences(string gridPreference, string gridId)
        {

            StringBuilder sbMessageTemplate = new StringBuilder();
            string sReturnResponse = "";
            Dictionary<string, string> dicResponse = new Dictionary<string, string>();
            try
            {
                gridPreference = AppHelper.HtmlEncodeString(gridPreference);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>GridPreferenceAdaptor.SaveUserHeaderAndPreference</Function>");
                sbMessageTemplate.Append("</Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<Reserve>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(gridId);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>ReserveListingBOB.aspx</PageName>");
                sbMessageTemplate.Append("<UserPref>" + gridPreference + "</UserPref>");
                sbMessageTemplate.Append("</Reserve>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString()); //return bool.   
                XmlDocument objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(sReturn);

                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    dicResponse.Add("error", "true");
                    dicResponse.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    JsonSerializerSettings js = new JsonSerializerSettings();
                    Newtonsoft.Json.JsonSerializer o = new JsonSerializer();

                    sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
                }
                //else                    
                //    dicResponse.Add("response", sSuccess);                
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            catch (Exception ex)
            {
                dicResponse.Add("error", "true");
                dicResponse.Add("errorMessage", ex.Message);
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            finally
            {
                sbMessageTemplate = null;
                dicResponse = null;
            }
            return sReturnResponse;

        }

        /// <summary>
        /// this function is used to restore default values for the grid.
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string RestoreDefault(string inputData)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            XmlDocument objXmlDoc = new XmlDocument();
            Dictionary<string, string> dicInputData;
            string sResponseData = "";
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>();
            try
            {
                dicInputData = new Dictionary<string, string>();
                dicResponseData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(inputData, dicInputData);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>ReserveFundsAdaptor.RestoreDefaults</Function>");
                sbMessageTemplate.Append(" </Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<RestoreDefault>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(dicInputData["GridId"]);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>ReserveListingBOB.aspx</PageName>");
                sbMessageTemplate.Append("</RestoreDefault>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString());
                objXmlDoc.LoadXml(sReturn);
                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    xmlIn = (XmlElement)objXmlDoc.SelectSingleNode("//UserPref");
                    if (xmlIn != null)
                    {
                        sResponseData = xmlIn.InnerText;
                    }
                }
                else
                {//error handling
                    dicResponseData.Add("error", "true");
                    dicResponseData.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
                }
            }
            catch (Exception ex)
            {
                dicResponseData.Add("error", "true");
                dicResponseData.Add("errorMessage", ex.Message);
                sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
            }

            finally
            {
                sbMessageTemplate = null;
                objXmlDoc = null;
                dicInputData = null;
                xmlIn = null;
                dicResponseData = null;
            }
            return sResponseData;
        }

        protected void drdCurrencytype_onchange(object sender, EventArgs e)
        {
            GetReserveListing();
        }

        //private void BindCurrencyTypeDropDown()
        //{
        //    drdCurrencytype.Items.Clear();
        //    if (CurrencytypeList != null)
        //    {
        //        foreach (XmlNode xm in CurrencytypeList)
        //        {
        //            drdCurrencytype.Items.Add(new ListItem(xm.InnerText, xm.Attributes["value"].Value));
        //        }
        //    }

        //}

    }
}