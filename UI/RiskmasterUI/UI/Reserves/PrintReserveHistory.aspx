﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintReserveHistory.aspx.cs" Inherits="Riskmaster.UI.UI.Reserves.PrintReserveHistory" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Reserve History</title>
    <link href="../../Content/rmnet.css" rel="Stylesheet" type="text/css"/>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
    
</head>
<body onload="RMX.Reserves.PageLoaded();return RMX.Reserves.Print();">
    <form id="frmData" runat="server">
    <div id="maindiv" class="divScroll">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />	
     <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/PolicyID" id="policyid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/CoverageID" ID="polcvgid" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/CvgLossID" ID="cvglossid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimId" id="claimid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantEID" ID="claimanteid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimNumber" ID="claimnumber" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/UnitID" ID="unitid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ParentSecurityId" ID="ParentSecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/SecurityId" ID="SecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" ID="lob" runat="server" rmxignoreget="true"></asp:TextBox>    
    <asp:TextBox style="display:none" ID="Reserve" runat="server" rmxref="/Instance/Document/ReserveFunds/Reserve" rmxignoreget="true"></asp:TextBox>    
    <asp:TextBox style="display:none" id="Claim" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@Claim"></asp:TextBox>
    <asp:TextBox style="display:none" id="LoginUser" runat="server" rmxref="/Instance/Document/ReserveFunds/Reserves/@LoginUser"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/Reserves/@HideForm" id="HideForm" runat="server" rmxignoreset="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/Reserves/@PrevResModifyzero" id="PrevResModifyzero" runat="server" rmxignoreset="true"></asp:TextBox>
    <asp:HiddenField runat="server" ID="Caption" />
				
  <table width="97%" border="0" cellspacing="0" cellpadding="4">
    
    
        
						<tr>
							<td class="ctrlgroup2" colspan="6">
									<asp:Label ID="lblReserveDesc" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@ReserveDesc"></asp:Label>
									<!-- Mits 18559 - Merged changes from R5 to R6-->
                                    <!--  MITS 27276 - Manish Jain-->
									Reserve for 
									<asp:Label ID="lblClaim" runat="server"></asp:Label>
							</td>
						</tr>
						
						<tr>
						    <td colspan="6">
						<asp:GridView ID="grdReserve" runat="server" OnRowCreated="grdReserve_RowCreated" OnRowDataBound="grdReserve_RowDataBound" Width="100%" 
                             CellPadding="4" RowStyle-HorizontalAlign="Center">
                        </asp:GridView>     
                        </td>
                        </tr>
						
					</table>
					
    
					
   </div>
    
    </form>
</body>
</html>
