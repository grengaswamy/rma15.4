<%@ Page Language="C#" AutoEventWireup="true" ResponseEncoding="ISO-8859-15" CodeBehind="FinancialDetailHistory.aspx.cs" Inherits="Riskmaster.UI.UI.Reserves.FinancialDetailHistory" EnableViewStateMac="false" ValidateRequest="false" %>
<%@ Register TagPrefix="uc" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html>


<head id="Head2" runat="server">
    <title>Financial Detail History</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <script src="../../Scripts/angularjs/angular.min.js"></script>    
     <script type="text/javascript" src="../../Scripts/angularjs/Directives/rma-ng-grid.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>    
    <script type="text/javascript" src="../../Scripts/angularjs/Controllers/FinancialDetailHistory.js"></script>   
    <script type="text/javascript" src="../../Scripts/angularjs/Directives/NumericInputDirective.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
     <script type="text/javascript" language="javascript">

         //RMA-8490 START
         function NavigateToFunds(TransID) {
             var ClaimantListClaimantEID = document.getElementById('ClaimantListClaimantEID').value;
             if (ClaimantListClaimantEID != '' && ClaimantListClaimantEID != 0)
                 document.getElementById('ClaimantId').value = ClaimantListClaimantEID
 
             sRedirectString = "/RiskmasterUI/UI/FDM/funds.aspx?TransId=" + TransID + "&SysCmd=0" + "&ClaimantListClaimantEID=" + ClaimantListClaimantEID + "&ClaimantEid=" + document.getElementById('ClaimantId').value + "&FromFinancial=Yes";
              window.location.href = sRedirectString;
              return false;
         }
         //RMA-8490 END
    </script>
</head>

<body onload="parent.MDIScreenLoaded();" style="width:99%;height:99%">
    <form id="frmData" runat="server">
    
    <div id="maindiv" style="height:100%;width:100%;overflow:auto">
        <table>
            
            <asp:TextBox Style="display: none" ID="ClaimId" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ClaimantId" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="PolicyId" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="UnitId" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="CoverageId" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="Multicurrencyindicator" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="claimcurrency" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="PolicyName" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="Unit" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="Coverage" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="CoverageLoss" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="RcRowId" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ReserveType" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ReserveStatus" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="Selected" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox style="display: none" ID="caption" runat="server" clientidmode="Static" ></asp:TextBox>
            <asp:TextBox Style="display: none" ID="selectedcurrencytype" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="previouscurrencytype" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ReserveTypeDesc" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ClaimNumber" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ClaimantName" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="SysFormName" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ClaimantRowID" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="Subtitle" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="FrozenFlag" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="FromFunds" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="CarrierClaim" runat="server" clientidmode="Static"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="ClaimantListClaimantEID" runat="server"></asp:TextBox><%--RMA-8490--%>
            <asp:TextBox Style="display: none" ID="IsLandingFromClaimant" runat="server"  value="0"></asp:TextBox><%--RMA-8490--%>
            
        </table>
    </div>
    
    <div ng-app="rmaApp" id="ng-app">
        <div ng-controller="FinancialHistoryController">
           
           <div style="clear: both"></div>
           <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldata">
                <tr>
                <td style="width:4%">
                    <span id="Span1" runat="server" width="100%"></span>
                </td>
                <td>
                    <uc:errorcontrol id="ErrorControl1" runat="server" /> 
                </td>
                    <td></td>
            </tr>
                <tr  Width="100%" class="msgheader">
              <td colspan="2" >
                   <span id="lblcaption" Width="100%"></span>
              </td>
            </tr>
              <tr><td><td>&nbsp;</td></tr>
            <tr>
                <td style="width:4%">
                    <span id="lblLevel" runat="server" width="100%"></span>
                </td>
                <td>
                    <asp:DropDownList ID="drdFS" AutoPostBack="true" type="combobox" runat="server" onselectedindexchanged="drdFS_SelectedIndexChanged" />                     
                </td> 
                            
            </tr>
        </table>
            <div style="clear: both"></div>

            <!-- GRID Template-->
            <input type="hidden" id="hdnJsonData" runat="server" />
            <input type="hidden" id="hdnJsonUserPref" runat="server" />
            <input type="hidden" id="hdnJsonAdditionalData" runat="server" />
            <rma-grid id="gridFDH" name="FinancialDetailHistory"  multiselect="false"  showselectioncheckbox="false" ></rma-grid>
            <!-- GRID Template-->
             <div style="text-align:center;" >
             <label style="display: none;" id="lblEmptyData" />
             </div>
            <!-- End CUSTOMIZATION-->
            <div style="clear: both"></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="4" id="tblback">
			    <tr>
				  <td>
                    <input type="button" id="btnBack" class="button" value="<%$ Resources:btnback %>" runat="server" />    
                </td>  
				</tr>
			</table>
            <div class="footer">
                <iframe id="csvDownloadFrame" style="display:none" />
            </div>
      </div>
    </div>
    </form>         
</body>

