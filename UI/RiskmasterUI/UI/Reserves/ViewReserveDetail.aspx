﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewReserveDetail.aspx.cs"  ValidateRequest="false" Inherits="Riskmaster.UI.UI.Reserves.ViewReserveDetail" %>



<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>ViewReserveDetail</title>
    <link href="../../Content/rmnet.css" rel="Stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" language="javascript">
       
        function PopulateDate() {
            if (typeof RMX == 'undefined')
                RMX = {};
            if (typeof (RMX.Reserves) == 'undefined')
                RMX.Reserves = {};

            if (document.getElementById('HideForm').value == "False")
                RMX.Reserves.ApplyDefaultDateFlag = true;
            else
                RMX.Reserves.ApplyDefaultDateFlag = false;
        }
        function CancelReserveHistory() {
            //RMA-8490 START
            //var sRedirectString = "/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=" + document.getElementById('claimid').value;
            var sRedirectString = "/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=" + document.getElementById('claimid').value + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value;
            //RMA-8490 END
            window.location.href = sRedirectString;
           
          return false;
        
        
        }
					
    </script>
    
</head>
<body onload="return RMX.Reserves.PageLoaded();">
    <form id="frmData" name="frmData" runat="server">
    
					
  <div style="width:97%;height:100%;" id="maindiv">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/PolicyID" id="policyid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/CoverageID" id="polcvgid" runat="server" rmxignoreget="true"></asp:TextBox>
        <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/CvgLossID" id="cvglossid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimId" id="claimid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantEID" ID="claimanteid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimNumber" ID="claimnumber" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/UnitID" ID="unitid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" ID="unitrowid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ParentSecurityId" ID="ParentSecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/SecurityId" ID="SecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" ID="lob" runat="server" rmxignoreget="true"></asp:TextBox>    
    <asp:TextBox style="display:none" ID="Reserve" runat="server" rmxref="/Instance/Document/ReserveFunds/Reserve" rmxignoreget="true"></asp:TextBox>    
    
    
    
    <asp:TextBox style="display:none" id="Claim" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@Claim"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/Reserves/@HideForm" id="HideForm" runat="server" rmxignoreset="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/Reserves/@PrevResModifyzero" id="PrevResModifyzero" runat="server" rmxignoreset="true"></asp:TextBox>
    <asp:HiddenField runat="server" ID="ClaimantRowId" />
    <asp:HiddenField runat="server" ID="Caption" />
         <asp:TextBox Style="display: none" ID="ClaimantListClaimantEID" runat="server"></asp:TextBox><%--RMA-8490--%>
        <asp:Label runat="server" class="msgheader" id="errorhook" Visible="false"></asp:Label>
        
        <table width="97%" border="0" cellspacing="0" cellpadding="4">
						<tr>
							<td class="ctrlgroup2">
									<asp:Label ID="lblReserveDesc" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@ReserveDesc"></asp:Label>
									Reserve for <%=Caption.Value %>
									<!--<asp:Label ID="lblClaim" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@Claim"></asp:Label>-->
							</td>
						</tr>
						<%if (HideForm.Text == "False")
        { %>
						
						<%} %>
						</table>
					
						<br />
						<div style="overflow:auto;height:50%">
						<asp:GridView ID="grdReserve" runat="server" OnRowCreated="grdReserve_RowCreated" OnRowDataBound="grdReserve_RowDataBound" Width="97%" 
                             CellPadding="4" RowStyle-HorizontalAlign="Center">
                        </asp:GridView>     
                        </div>
                    	
					
					<br />
					<table border="0" width="100%">
						<tr>
							<td align="center">
								<%if(HideForm.Text == "False") { %>
								    
								<%} %>
								
								<asp:button ID="btnCancel" runat="server" CssClass="button" onclientclick="return CancelReserveHistory();" Text="Cancel" OnClick="btnCancel_onclick"/>
								<asp:button ID="btnPrint" runat="server" CssClass="button" Text="Print" onclientclick="return RMX.Reserves.PrintWindowOpen();"/>
              </td>
            </tr>
					</table>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
