﻿<!--**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 03/14/2014 | 34082   | pgupta93   | Multicurrency changes
**********************************************************************************************-->
<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ModifyReserve.aspx.cs" Inherits="Riskmaster.UI.UI.Reserves.ModifyReserve" ValidateRequest="false" EnableViewStateMac="false" %>

<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"
    TagPrefix="mc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Modify Reserve</title>
    <link href="../../Content/rmnet.css" rel="Stylesheet" type="text/css"/>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-reserves.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/rmx-custom.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
    <!--Praveen ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <!--Praveen ML-->

    <script type="text/javascript" language="javascript">
        //BSB 03.05.2007 
        // This approach keeps "global" variable usage more clear and explicit in Javascript and 
        // prevents naming collisions.
        function PopulateDate() {
            if (typeof RMX == 'undefined')
                RMX = {};
            if (typeof (RMX.Reserves) == 'undefined')
                RMX.Reserves = {};

            if (document.getElementById('HideForm').value == "False")
                RMX.Reserves.ApplyDefaultDateFlag = true;
            else
                RMX.Reserves.ApplyDefaultDateFlag = false;
        }
		
					
    </script>
    
</head>
<body onload="PopulateDate();return RMX.Reserves.PageLoaded();">
    <form id="frmData" name="frmData" runat="server">
    
					
  <div style="width:97%;height:100%;" id="maindiv">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
   <asp:HiddenField runat="server" ID="hdFDHButton" /> <%--pgupta93: RMA-4608--%>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/PolicyID" id="policyid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/CoverageID" id="polcvgid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimId" id="claimid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimantEID" ID="claimanteid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ClaimNumber" ID="claimnumber" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/UnitID" ID="unitid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" ID="unitrowid" runat="server" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/ParentSecurityId" ID="ParentSecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/SecurityId" ID="SecurityId" runat="server" Text="0" rmxignoreget="true"></asp:TextBox>
    <asp:TextBox style="display:none" ID="lob" runat="server" rmxignoreget="true"></asp:TextBox>    
    <asp:TextBox style="display:none" ID="Reserve" runat="server" rmxref="/Instance/Document/ReserveFunds/Reserve" rmxignoreget="true"></asp:TextBox>    
    
    
    
    <asp:TextBox style="display:none" id="Claim" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@Claim"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/Reserves/@HideForm" id="HideForm" runat="server" rmxignoreset="true"></asp:TextBox>
    <asp:TextBox style="display:none" rmxref="/Instance/Document/ReserveFunds/Reserves/@PrevResModifyzero" id="PrevResModifyzero" runat="server" rmxignoreset="true"></asp:TextBox>
    <asp:HiddenField runat="server" ID="ClaimantRowId" />
    <asp:HiddenField runat="server" ID="Caption" />
             <%--//MITS:34082 START--%>
            <asp:textbox style="display: none" runat="server" id="currencytype" rmxref="/Instance/Document/ReserveFunds/CurrencyType" />
            <asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="modifyreserve" />
             <%--//MITS:34082 END--%>
        <asp:Label runat="server" class="msgheader" id="errorhook" Visible="false"></asp:Label>
        
        <table width="97%" border="0" cellspacing="0" cellpadding="4">
						<tr>
							<td class="ctrlgroup2" colspan="6">
									<asp:Label ID="lblReserveDesc" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@ReserveDesc"></asp:Label>
                                <asp:Label ID="lblReserveforID" runat="server" Text="<%$ Resources:lblReserveforID %>"></asp:Label> <%=Caption.Value %>
									<!--<asp:Label ID="lblClaim" runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/Reserves/@Claim"></asp:Label>-->
							</td>
						</tr>
						<%if (HideForm.Text == "False")
        { %>
						
							<tr>
								<td class="datatd"><u>
                                    <asp:Label ID="lblDateID" runat="server" Text="<%$ Resources:lblDateID %>"></asp:Label></u>:</td>
								<td class="datatd"><u>
                                    <asp:Label ID="lblReserveAmountID" runat="server" Text="<%$ Resources:lblReserveAmountID %>"></asp:Label></u>:</td>
								<td class="datatd">
                                    <asp:Label ID="lblStatusID" runat="server" Text="<%$ Resources:lblStatusID %>"></asp:Label></td>
								<td class="datatd">
                                    <asp:Label ID="lblUserID" runat="server" Text="<%$ Resources:lblUserID %>"></asp:Label></td>
								<td class="datatd">
                                    <asp:Label ID="lblReasonID" runat="server" Text="<%$ Resources:lblReasonID %>"></asp:Label></td>
							</tr>
							<tr>
								<td>
									 <asp:textbox runat="server" id="ProcDate" size="15" onblur="dateLostFocus(this.id);" onchange="setDataChanged(true);" rmxref="/Instance/Document/ReserveFunds/DateEntered" rmxignoreget="true" FormatAs="date" ></asp:textbox>
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#ProcDate").datepicker({
                                                showOn: "button",
                                                buttonImage: "../../Images/calendar.gif",
                                                buttonImageOnly: true,
                                                showOtherMonths: true,
                                                selectOtherMonths: true,
                                                changeYear: true
                                            });
                                        });
                                    </script>
                                    
                                    <br />
                                    <br />
                                    <%--Deb--%>
                                    <asp:Label Text="Currency Type:" ID="lblCurrencyType" runat="server"></asp:Label>
                        <%--MITS:34082 MultiCurrency START--%>
                        <%--  <asp:Label Text="" ID="lcurrencytype" rmxref="/Instance/Document/ReserveFunds/CurrencyType" runat="server"></asp:Label>
                     <asp:TextBox runat="server" ID ="currencytype" Text="" style="display:none" rmxref="/Instance/Document/ReserveFunds/CurrencyType" />--%>
                        <span>
                            <asp:textbox runat="server" style="width: 30%" onchange="lookupTextChanged(this);" id="ReservecurrencytypetextModify"
                                rmxref="/Instance/Document/ReserveFunds/CurrencyType" rmxtype="code" cancelledvalue=""
                                tabindex="11" onblur="codeLostFocus(this.id);" />
                            <asp:button class="CodeLookupControl" runat="Server" id="ReservecurrencytypetextModifybtn"
                                onclientclick="return selectCode('CURRENCY_TYPE','ReservecurrencytypetextModify');"
                                tabindex="14" />
                            <asp:textbox style="display: none" runat="server" id="ReservecurrencytypetextModify_cid"
                                rmxref="/Instance/Document/ReserveFunds/CurrencyTypeID" cancelledvalue="" />
                        </span>
                        <%--MITS:34082 MultiCurrency END--%>
                   				</td>
                                <td>
                        <%--MITS:34082 MultiCurrency START--%>
                        <%--<asp:TextBox runat="server" id="txtAmount" max="9999999999.99" onblur="currencyLostFocus(this);" onchange="setDataChanged(true);" rmxref="/Instance/Document/ReserveFunds/Amount" rmxignoreget="false"/>--%>
                        <%--   <mc:CurrencyTextbox runat="server" ID="txtAmount" onchange="setDataChanged(true);" rmxref="/Instance/Document/ReserveFunds/Amount" rmxignoreget="false" />--%>
                        <mc:currencytextbox runat="server" id="txtAmount" onchange="setDataChanged(true);" rmxref="/Instance/Document/ReserveFunds/Amount" rmxignoreget="false" />
                        <%--MITS:34082 MultiCurrency END--%>
                  				</td>
								<td>
                                 <%-- asatishchand MITS - 31534  --%>
									<asp:DropDownList runat="server" onchange="setDataChanged(false);" rmxref="/Instance/Document/ReserveFunds/StatusCodeID" itemsetref="/Instance/Document/ReserveFunds/Reserves/Dropdowns/Status" id="lstStatus"></asp:DropDownList>
								</td>
								<td>
									<asp:TextBox id="txtUserId" onchange="setDataChanged(true);" rmxref="/Instance/Document/ReserveFunds/Reserves/@LoginUser" runat="server" ></asp:TextBox>
								</td>
								<td>
									<asp:textbox runat="server" onchange="setDataChanged(true);" rmxref="/Instance/Document/ReserveFunds/Reason" id="Reason" rmxignoreget="false" MaxLength="30"></asp:textbox> <br/><%--rsriharsha : Incresed maxlength for MITS 38187 | RMA 10379--%>
				                    <%--JIRA-18700 pkumari3 start --%>
				                    <asp:DropDownList runat="server" rmxignoreset="true" rmxref="/Instance/Document/ReserveFunds/ReasonCode" itemsetref="/Instance/Document/ReserveFunds/Reserves/Dropdowns/Reason" id="lstReason" onchange="setDataChanged(true);SetReasonToTextBoxAdd(this);"></asp:DropDownList>					
                                    <%--JIRA-18700 pkumari3 end --%>
								</td>
							</tr>
                            <%-- Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement--%>
                            <tr>
                                <td>
                                    <asp:Label ID="lbl_claimadjusterlookup" runat="server" Text="<%$ Resources:lblAssignedAdjusterID %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="claimadjusterlookup" />
                                    <asp:button runat="server" class="EllipsisControl" id="claimadjusterlookupbtn" onclientclick="return lookupData('claimadjusterlookup','ADJUSTERS',7,'claimadjusterlookup',2);" />
                                    <asp:TextBox style="display:none" runat="server" id="claimadjusterlookup_cid" rmxref="/Instance/Document/ReserveFunds/AssignAdjusterEid" />
                                </td>
                            </tr>
                            <%--Ankit End --%>
						<%} %>
						</table>
					
						<br />
                        <!-- mkaran2 : MITS 32837 : Start-->
                        <div style="margin-bottom:10px">
						 	<table border="0"> 
                                <tr>
                                    <td><asp:Label runat="server" ID="lblPaid" Text="<%$ Resources:lblPaid %>" /></td>
                                    <td><asp:Label runat="server" ID="lblPaidAmount" /></td>
                                    <td>&nbsp</td>
                                    <td><asp:Label runat="server" ID="lblCollections" Text="<%$ Resources:lblCollections %>" /></td>
                                    <td><asp:Label runat="server" ID="lblCollectionsAmount" /></td>
                                </tr>
                                
                            </table>
                        </div>
                        <!-- mkaran2 : MITS 32837 : End-->
						<div style="overflow:auto;height:50%">
						<asp:GridView ID="grdReserve" runat="server" OnRowCreated="grdReserve_RowCreated" OnRowDataBound="grdReserve_RowDataBound" Width="97%" 
                             CellPadding="4" RowStyle-HorizontalAlign="Center">
                        </asp:GridView>     
                        </div>
                    	
					
					<br />
					<table border="0" width="100%">
                  <%--  skhare7 R8 SuperVisory approval--%>
                    <tr>
			  <td  align="center" >
			  <asp:Label runat="server" ID="lblReason" Text="<%$ Resources:lblReasonID %>" />
              <asp:TextBox runat="server" ID="txtAppRejReqCom" rmxref="/Instance/Document/ReserveFunds/ApproveReasonCom" Columns="50" Rows="4" 
                      TextMode="MultiLine" Width="371px" />
				  <!--<xforms:textarea xhtml:cols="50" xhtml:rows="4" ref="/Instance/Document/AppRejReqCom" xhtml:id="txtAppRejReqCom"></xforms:textarea>-->
			  </td>
		  </tr>
        <%--  skhare7 R8 SuperVisory approval End--%>
                    <tr>
			  <td  align="center" >
			      &nbsp;</td>
		  </tr>
						<tr>
							<td align="center">
								<%if(HideForm.Text == "False") { %>
								    
									<asp:button id="btnModify" runat="server" CssClass="button" OnClientClick="return RMX.Reserves.ValidateForm();" Text="<%$ Resources:btnModify %>" OnClick="btnModify_onclick"/>
								<%} %>
<!-- MGaba2:R8:SuperVisory Approval -->
<asp:TextBox runat="server" ID ="hdnRsvRowId" Text="-1" style="display:none" rmxref="/Instance/Document/ReserveFunds/RsvRowId" />
                         <!-- mbahl3:SuperVisory Approval -->       
                        <asp:TextBox runat="server" ID ="hdnRsvHistRowId" Text="-1" style="display:none" rmxref="/Instance/Document/ReserveFunds/RsvHistRowId" />
                        <asp:TextBox runat="server" ID="hdnAction" Text="" Style="display: none" rmxref="/Instance/Document/ReserveFunds/ApproveorRejectRsv" />
									<asp:button ID="btnApprove" runat="server" CssClass="button" Text="<%$ Resources:btnApprove %>" OnClick="btnApprove_onclick" Visible ="false" />
                                    <asp:button ID="btnReject" runat="server" CssClass="button" Text="<%$ Resources:btnReject %>" OnClick="btnReject_onclick"  Visible ="false" />
								<asp:button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:btnCancel %>" OnClick="btnCancel_onclick"/>
								<asp:button ID="btnPrint" runat="server" CssClass="button" Text="<%$ Resources:btnPrint %>" onclientclick="return RMX.Reserves.PrintWindowOpen();"/>
              </td>
            </tr>
            
					</table>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
