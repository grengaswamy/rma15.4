﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReserveTransactionDetail.aspx.cs" Inherits="Riskmaster.UI.UI.Reserves.ReserveTransactionDetail" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reserve Transaction Details</title>
    <link href="../../Content/rmnet.css" rel="Stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldata">
            <tr>
                <td>
                    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>     
             <tr>
                <td>
                    <asp:Label id="lblNoRecords" runat="server" Text="<%$ Resources:lblNoRecords %> " ></asp:Label>
                </td>
            </tr>                
        </table>
        <div>            
            <asp:GridView ID="grdPaymentInfo"  runat="server" AutoGenerateColumns="true" AllowPaging="false" OnRowDataBound="grdPaymentInfo_RowDataBound"  HeaderStyle-CssClass="colheader3" AlternatingRowStyle-CssClass="rowlight2" RowStyle-CssClass="rowlight1" Width="100%" >
            </asp:GridView>
        </div>
    </form>
</body>
</html>
