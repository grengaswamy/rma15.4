﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.Models;
using Riskmaster.BusinessHelpers;
using Newtonsoft.Json;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager;
using System.Xml.XPath;
using Riskmaster.Common.Extensions;
using System.Dynamic;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Web.Services;
using System.Threading;
using Riskmaster.UI.Shared.Controls; //rkotak
using System.IO;
using System.Runtime.Serialization.Json;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class FinancialDetailHistory : NonFDMBasePageCWS
    {
        
        string Const_Width = "150";
        XElement resultDoc = null;
        XmlDocument Model = null;
        public int selvalue = 0;
        //static ErrorControl objErrorCtl;
        static string sSuccess = "Success";
        static string sPageName = "FinancialDetailHistory.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            string FDHToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId(sPageName.ToUpper()), "FDHToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "FDHToolTips", FDHToolTips, true);

            string FDHLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId(sPageName.ToUpper()), "FDHLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "FDHLabels", FDHLabels, true);

            string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);

            string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

            string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);
            if (!Page.IsPostBack)
            {

                #region Get intitial values, query string values
                ClaimantListClaimantEID.Text = AppHelper.GetFormValue("ClaimantListClaimantEID"); //RMA-8490
                //string FDHToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId(sPageName.ToUpper()), "FDHToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "FDHToolTips", FDHToolTips, true);

                //string FDHLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId(sPageName.ToUpper()), "FDHLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "FDHLabels", FDHLabels, true);

                //string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);

                //string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

                //string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);
                
                
                //RMA-10394 starts
                //string NgGridAdditionalToolBarButtons = "'<img ng-src=\"../../Images/tb_backtosummary_active.png\" style=\"cursor: pointer;cursor: hand;\" ng-click=\"BackClick()\" title=\"Back\"/>'";
                //string NgGridAdditionalToolBarButtons = "var NgGridAdditionalToolBarButtons = { htmlMarkup: \"<img ng-src=\"../../Images/tb_backtosummary_active.png\" style=\"cursor: pointer;cursor: hand;\" ng-click=\"BackClick()\" title=\"Back\"/>};";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAdditionalToolBarButtons", NgGridAdditionalToolBarButtons, true);
                //RMA-10394 ends
                // achouhan3    added to support ML Starts
                lblLevel.InnerHtml = RMXResourceProvider.GetSpecificObject("lbllevel", RMXResourceManager.RMXResourceProvider.PageId(sPageName.ToUpper()), "0") + ":";
                BindDropDown();
                // achouhan3    added to support ML Ends
                ClaimId.Text = AppHelper.GetQueryStringValue("ClaimID");
                ClaimantId.Text = AppHelper.GetQueryStringValue("ClaimantID");
                PolicyId.Text = AppHelper.GetQueryStringValue("PolicyID");
                UnitId.Text = AppHelper.GetQueryStringValue("UnitID");
                CoverageId.Text = AppHelper.GetQueryStringValue("CoverageID");
                Multicurrencyindicator.Text = AppHelper.GetQueryStringValue("UseMulticurrency");
                claimcurrency.Text = AppHelper.GetQueryStringValue("ClaimCurrency");
                PolicyName.Text = AppHelper.GetQueryStringValue("Policy");
                Unit.Text = AppHelper.GetQueryStringValue("Unit");
                Coverage.Text = AppHelper.GetQueryStringValue("Coverage");
                if (AppHelper.GetQueryStringValue("SelectedDropdown") == "")
                {
                    //RMA-8490 START
                    //Selected.Text = "1";
                    if ((!string.IsNullOrEmpty(ClaimantListClaimantEID.Text)) && ClaimantListClaimantEID.Text != "0" )
                    {
                        ClaimantId.Text = ClaimantListClaimantEID.Text;
                        IsLandingFromClaimant.Text = "1";
                        Selected.Text = "2";
                    }
                    else
                      Selected.Text = "1";
                    //RMA-8490 END
                }
                else
                {
                    Selected.Text = AppHelper.GetQueryStringValue("SelectedDropdown");
                }
                CoverageLoss.Text = AppHelper.GetQueryStringValue("CvgLossId");
                RcRowId.Text = AppHelper.GetQueryStringValue("RcRowId");
                ReserveType.Text = AppHelper.GetQueryStringValue("ReserveType");
                ReserveStatus.Text = AppHelper.GetQueryStringValue("ReserveStatus");
                ReserveTypeDesc.Text = AppHelper.GetQueryStringValue("ReserveTypeDesc");
                ClaimNumber.Text = AppHelper.GetQueryStringValue("ClaimNo");
                ClaimantName.Text = AppHelper.GetQueryStringValue("ClaimantName");
                SysFormName.Text = AppHelper.GetQueryStringValue("SysFormName");

                ClaimantRowID.Text = AppHelper.GetQueryStringValue("ClaimantRowID");
                Subtitle.Text = AppHelper.GetQueryStringValue("Subtitle");
                FrozenFlag.Text = AppHelper.GetQueryStringValue("FrozenFlag");
                FromFunds.Text = AppHelper.GetQueryStringValue("FromFunds");
                CarrierClaim.Text = AppHelper.GetQueryStringValue("CarrierClaim");
                #endregion
                GetFinancialDetailHistory();
            }           
           
        }
                

       /// <summary>
       /// This is a method which will be called to bind grid from page load.
       /// </summary>
        public void GetFinancialDetailHistory()
        {   
            XmlDocument resultDoc = new XmlDocument();
            XElement messageElement;
            string sCWSresponse = "";
            XmlElement xmlIn;
            string AdditionData = string.Empty;//RMA-8490
            try
            {
                //messageElement = GetFinancialDetailHistoryMessageTemplate(ClaimId.Text, ClaimantId.Text, PolicyId.Text, UnitId.Text, CoverageId.Text, CoverageLoss.Text, ReserveType.Text, ReserveStatus.Text, RcRowId.Text, Multicurrencyindicator.Text, claimcurrency.Text, Selected.Text, previouscurrencytype.Text, selectedcurrencytype.Text, "gridFDH");
                messageElement = GetFinancialDetailHistoryMessageTemplate();
                CallCWS("ReserveFundsAdaptor.GetFinancialDetailHistory", messageElement, out sCWSresponse, false, false);
                resultDoc.LoadXml(sCWSresponse);
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//FinancialHistory//Data");
                if (xmlIn != null)
                    hdnJsonData.Value = xmlIn.InnerText;
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//FinancialHistory//UserPref");
                if (xmlIn != null)
                    hdnJsonUserPref.Value = xmlIn.InnerText;
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//FinancialHistory//AdditionalData");
                if (xmlIn != null)
                {
                    hdnJsonAdditionalData.Value = xmlIn.InnerText;
                    //RMA-8490 START
                    AdditionData = xmlIn.InnerText;
                    AdditionData = AdditionData.Replace("[", "");
                    AdditionData = AdditionData.Replace("]", "");
                    Dictionary<string, string> Symptoms = new Dictionary<string, string>();
                    JsonConvert.PopulateObject(AdditionData.Trim(), Symptoms);
                    if(IsLandingFromClaimant.Text == "1")
                     ClaimantName.Text = Symptoms["ClaimantName"];
                    //RMA-8490 END
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                resultDoc = null;
                messageElement = null; sCWSresponse = "";
                xmlIn = null;
            }
           
        }              

        //public static XElement GetFinancialDetailHistoryMessageTemplate(string ClaimId, string ClaimantId, string PolicyId, string UnitId, string CoverageId, string CoverageLoss, string Reservetype, string ReserveStatus, string RcRowId, string Multicurrencyindicator, string claimcurrency, string SummaryLevel, string previouscurrencytype, string selectedcurrencytype,string GridId)
        //{   
        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
        //    sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.GetFinancialDetailHistory</Function></Call><Document>");
        //    sXml = sXml.Append("<FinancialDetailHistory>");
        //    sXml = sXml.Append("<ClaimID>");
        //    sXml = sXml.Append(ClaimId);
        //    sXml = sXml.Append("</ClaimID>");

        //    sXml = sXml.Append("<ClaimantEID>");
        //    sXml = sXml.Append(ClaimantId);
        //    sXml = sXml.Append("</ClaimantEID>");

        //    sXml = sXml.Append("<PolicyID>");
        //    sXml = sXml.Append(PolicyId);
        //    sXml = sXml.Append("</PolicyID>");

        //    sXml = sXml.Append("<UnitID>");
        //    sXml = sXml.Append(UnitId);
        //    sXml = sXml.Append("</UnitID>");

        //    sXml = sXml.Append("<CoverageID>");
        //    sXml = sXml.Append(CoverageId);
        //    sXml = sXml.Append("</CoverageID>");

        //    sXml = sXml.Append("<CoverageLossID>");
        //    sXml = sXml.Append(CoverageLoss);
        //    sXml = sXml.Append("</CoverageLossID>");

        //    sXml = sXml.Append("<ReserveTypeCode>");
        //    sXml = sXml.Append(Reservetype);
        //    sXml = sXml.Append("</ReserveTypeCode>");

        //    sXml = sXml.Append("<ReserveStatus>");
        //    sXml = sXml.Append(ReserveStatus);
        //    sXml = sXml.Append("</ReserveStatus>");

        //    sXml = sXml.Append("<RCrowId>");
        //    sXml = sXml.Append(RcRowId);
        //    sXml = sXml.Append("</RCrowId>");

        //    sXml = sXml.Append("<Multicurrency>");
        //    sXml = sXml.Append(Multicurrencyindicator);
        //    sXml = sXml.Append("</Multicurrency>");

        //    sXml = sXml.Append("<claimcurrency>");
        //    sXml = sXml.Append(claimcurrency);
        //    sXml = sXml.Append("</claimcurrency>");

        //    sXml = sXml.Append("<SelectedLevel>");
        //    sXml = sXml.Append(SummaryLevel);
        //    sXml = sXml.Append("</SelectedLevel>");

        //    sXml = sXml.Append("<previouscurrencytype>");
        //    sXml = sXml.Append(previouscurrencytype);
        //    sXml = sXml.Append("</previouscurrencytype>");

        //    sXml = sXml.Append("<selectedcurrencytype>");
        //    sXml = sXml.Append(selectedcurrencytype);
        //    sXml = sXml.Append("</selectedcurrencytype>");

        //    //rkotak:9681
        //    sXml = sXml.Append("<PageName>");
        //    sXml = sXml.Append("FinancialDetailHistory.aspx");
        //    sXml = sXml.Append("</PageName>");
        //    sXml = sXml.Append("<GridId>");
        //    sXml = sXml.Append(GridId);
        //    sXml = sXml.Append("</GridId>");
        //    //sXml = sXml.Append("<IsJsonResponse>");
        //    //sXml = sXml.Append("1");
        //    //sXml = sXml.Append("</IsJsonResponse>");
            
        //    //rkotak:9681

        //    sXml = sXml.Append("</FinancialDetailHistory></Document></Message>");

        //    XElement oTemplate = XElement.Parse(sXml.ToString());

        //    return oTemplate;
        //}

        public  XElement GetFinancialDetailHistoryMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.GetFinancialDetailHistory</Function></Call><Document>");
            sXml = sXml.Append("<FinancialDetailHistory>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(ClaimId.Text);
            sXml = sXml.Append("</ClaimID>");

            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(ClaimantId.Text);
            sXml = sXml.Append("</ClaimantEID>");

            sXml = sXml.Append("<PolicyID>");
            sXml = sXml.Append(PolicyId.Text);
            sXml = sXml.Append("</PolicyID>");

            sXml = sXml.Append("<UnitID>");
            sXml = sXml.Append(UnitId.Text);
            sXml = sXml.Append("</UnitID>");

            sXml = sXml.Append("<CoverageID>");
            sXml = sXml.Append(CoverageId.Text);
            sXml = sXml.Append("</CoverageID>");

            sXml = sXml.Append("<CoverageLossID>");
            sXml = sXml.Append(CoverageLoss.Text);
            sXml = sXml.Append("</CoverageLossID>");

            sXml = sXml.Append("<ReserveTypeCode>");
            sXml = sXml.Append(ReserveType.Text);
            sXml = sXml.Append("</ReserveTypeCode>");

            sXml = sXml.Append("<ReserveStatus>");
            sXml = sXml.Append(ReserveStatus.Text);
            sXml = sXml.Append("</ReserveStatus>");

            sXml = sXml.Append("<RCrowId>");
            sXml = sXml.Append(RcRowId.Text);
            sXml = sXml.Append("</RCrowId>");

            sXml = sXml.Append("<Multicurrency>");
            sXml = sXml.Append(Multicurrencyindicator.Text);
            sXml = sXml.Append("</Multicurrency>");

            sXml = sXml.Append("<claimcurrency>");
            sXml = sXml.Append(claimcurrency.Text);
            sXml = sXml.Append("</claimcurrency>");

            sXml = sXml.Append("<SelectedLevel>");
            sXml = sXml.Append(Selected.Text);
            sXml = sXml.Append("</SelectedLevel>");

           
            //rkotak:9681
            sXml = sXml.Append("<PageName>");
            sXml = sXml.Append("FinancialDetailHistory.aspx");
            sXml = sXml.Append("</PageName>");
            sXml = sXml.Append("<GridId>");
            sXml = sXml.Append("gridFDH");
            sXml = sXml.Append("</GridId>");
            sXml = sXml.Append("<IsLandingFromClaimant>");
            sXml = sXml.Append(IsLandingFromClaimant.Text);
            sXml = sXml.Append("</IsLandingFromClaimant>");
            //sXml = sXml.Append("<IsJsonResponse>");
            //sXml = sXml.Append("1");
            //sXml = sXml.Append("</IsJsonResponse>");

            //rkotak:9681

            sXml = sXml.Append("</FinancialDetailHistory></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }

        private void BindDropDown()
        {
            drdFS.Items.Add(new ListItem(RMXResourceProvider.GetSpecificObject("lblClaimlevel", RMXResourceManager.RMXResourceProvider.PageId(sPageName), "0"), "1"));
            drdFS.Items.Add(new ListItem(RMXResourceProvider.GetSpecificObject("lblClaimantlevel", RMXResourceManager.RMXResourceProvider.PageId(sPageName), "0"), "2"));
            drdFS.Items.Add(new ListItem(RMXResourceProvider.GetSpecificObject("lblReservelevel", RMXResourceManager.RMXResourceProvider.PageId(sPageName), "0"), "3"));
            drdFS.Items.Add(new ListItem(RMXResourceProvider.GetSpecificObject("lblPolicylevel", RMXResourceManager.RMXResourceProvider.PageId(sPageName), "0"), "4"));
            drdFS.Items.Add(new ListItem(RMXResourceProvider.GetSpecificObject("lblUnitlevel", RMXResourceManager.RMXResourceProvider.PageId(sPageName), "0"), "5"));
            drdFS.Items.Add(new ListItem(RMXResourceProvider.GetSpecificObject("lblCoveragelevel", RMXResourceManager.RMXResourceProvider.PageId(sPageName), "0"), "6"));
        }
        
        /// <summary>
        /// this function i sused to save preferences for the grid in the database
        /// </summary>
        /// <param name="gridPreference"></param>
        /// <param name="gridId"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string SavePreferences(string gridPreference,string gridId)
        {

            StringBuilder sbMessageTemplate = new StringBuilder();
            string sReturnResponse = "";
            Dictionary<string, string> dicResponse = new Dictionary<string, string>();
            try
            {
                gridPreference = AppHelper.HtmlEncodeString(gridPreference);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");                
                sbMessageTemplate.Append("<Function>GridPreferenceAdaptor.SaveUserHeaderAndPreference</Function>");                
                sbMessageTemplate.Append("</Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<FinancialDetailHistory>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(gridId);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>FinancialDetailHistory.aspx</PageName>");
                sbMessageTemplate.Append("<UserPref>" + gridPreference + "</UserPref>");
                sbMessageTemplate.Append("</FinancialDetailHistory>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString()); //return bool.   
                XmlDocument objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(sReturn);

                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    dicResponse.Add("error", "true");
                    dicResponse.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    JsonSerializerSettings js = new JsonSerializerSettings();
                    Newtonsoft.Json.JsonSerializer o = new JsonSerializer();
                    
                    sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
                }
                //else                    
                //    dicResponse.Add("response", sSuccess);                
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            catch (Exception ex)
            {
                dicResponse.Add("error", "true");
                dicResponse.Add("errorMessage", ex.Message);    
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            finally
            {
                sbMessageTemplate = null;
                dicResponse = null;
            }
            return sReturnResponse;

        }
        
        /// <summary>
        /// this function is used to restore default values for the grid.
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string RestoreDefault(string inputData)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            XmlDocument objXmlDoc = new XmlDocument();
            Dictionary<string, string> dicInputData;
            string sResponseData = "";
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>();
            try
            {
                dicInputData = new Dictionary<string, string>();
                dicResponseData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(inputData, dicInputData);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>ReserveFundsAdaptor.RestoreDefaults</Function>");
                sbMessageTemplate.Append(" </Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<RestoreDefault>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(dicInputData["GridId"]);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>FinancialDetailHistory.aspx</PageName>");
                sbMessageTemplate.Append("</RestoreDefault>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString());
                objXmlDoc.LoadXml(sReturn);
                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {                    
                    xmlIn = (XmlElement)objXmlDoc.SelectSingleNode("//UserPref");
                    if (xmlIn != null)
                    {
                        sResponseData = xmlIn.InnerText;
                    }                    
                }
                else
                {//error handling
                    dicResponseData.Add("error", "true");
                    dicResponseData.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
                }
            }
            catch (Exception ex)
            {
                dicResponseData.Add("error", "true");
                dicResponseData.Add("errorMessage", ex.Message);
                sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
            }
                
            finally
            {
                sbMessageTemplate = null;
                objXmlDoc = null;                
                dicInputData = null;
                xmlIn = null;
                dicResponseData = null;
            }
            return sResponseData;
        }
        
        protected void drdFS_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetFinancialDetailHistory();
        }

    }
}