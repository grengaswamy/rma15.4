﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Reserves
{
    public partial class ReserveTransactionDetail : NonFDMBasePageCWS
    {
        string sPolicyID = "0";
        string sPolicyCvgRowID = "0";
        string sReserveTypeCode = "0";        
        string sCvgLossId = "0";        
        string sReserveId = "0";
        bool bApplyDeductibles = false;
        string sClaimId = "";
        string sClaimantEId = "";
       
        

        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ReserveTransactionDetail.aspx"), "ReserveTransDetailsValidations", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ReserveTransDetailsValidations", sValidationResources, true);
            GetQuerystringValues();
            PaymentsDataBind();
        }

        private void GetQuerystringValues()
        {
            try
            {

                if (Request.QueryString["PolicyID"] != null)
                {
                    sPolicyID = Request.QueryString["PolicyID"].ToString();
                }
                if (Request.QueryString["PolicyCvgRowID"] != null)
                {
                    sPolicyCvgRowID = Request.QueryString["PolicyCvgRowID"].ToString();
                }
                if (Request.QueryString["ReserveTypeCode"] != null)
                {
                    sReserveTypeCode = Request.QueryString["ReserveTypeCode"].ToString();
                }
                if (Request.QueryString["CvgLossId"] != null)
                {
                    sCvgLossId = Request.QueryString["CvgLossId"].ToString();
                }
                if (Request.QueryString["ReserveId"] != null)
                {
                    sReserveId = Request.QueryString["ReserveId"].ToString();
                }
                if (Request.QueryString["ClaimId"] != null)
                {
                    sClaimId = Request.QueryString["ClaimId"].ToString();
                }
                if (Request.QueryString["ClaimantEId"] != null)
                {
                    sClaimantEId = Request.QueryString["ClaimantEId"].ToString();
                }
            }
            catch (Exception ex)
            {
            }
          
        }
        public int PaymentsDataBind()
        {
            DataRow objRow;
            DataTable objTable = null;
            int iPolicyID = 0;
            int iPolicyCvgRowID = 0;
            int iReserveTypeCode = 0;
            int iRC_RowID = 0;
            int iCvgLossId = 0;
            int iCollAlertonRes = 0; //JIRA-857

            string[] arrIDs = null;
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            XmlNodeList objNodeList = null;
            XmlNode objXmlNode = null;
            XmlDocument objDocument = null;
            string sCWSresponse = "";
            int iCount = 0;

            try
            {
             
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("ReserveFundsAdaptor.GetPaymentsBOB", XmlTemplate, out sCWSresponse, false, false);

                objDocument = new XmlDocument();
                objDocument.LoadXml(sCWSresponse);

                objNodeList = objDocument.SelectNodes("//Payments/Payment");

                objTable = new DataTable("Payments");
                grdPaymentInfo.ShowHeader = true;
                objTable.Columns.Add("PAYEE_NAME");
                objTable.Columns.Add("CHECK_NUMBER");
                objTable.Columns.Add("DATE_OF_CHECK");
                //pgupta93: RMA-7113 START
                objTable.Columns.Add("Type");
                //pgupta93: RMA-7113 END
                objTable.Columns.Add("TRANSACTION_TYPE");
                objTable.Columns.Add("AMOUNT");
                objTable.Columns.Add("STATUS_CODE");
                //Ankit Start : Worked on MITS - 32120
                objTable.Columns.Add("TOTAL_PAYEES");
                //Ankit End
                //RUPAL Start : MITS - 33152
                objTable.Columns.Add("VOID_FLAG");
                //RUPAL End
                //Added by Swati agarwal for MITS # 33431
                objTable.Columns.Add("STOP_PAY_FLAG");
                //change end here by swati
                objTable.Columns.Add("CLEARED_FLAG");//MITS 33239 asharma326
                //START -  added by Nikhil on 08/04/14. Check Total field added.
                // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
                //code needs to be reverted.
                bApplyDeductibles = objDocument.SelectSingleNode("//Payments").Attributes["ApplyDeductibles"].Value.Equals("True", StringComparison.InvariantCultureIgnoreCase);
                if (bApplyDeductibles)
                {
                    objTable.Columns.Add("CHECK_TOTAL");
                }
                //end amitosh
                //End -  added by Nikhil on 08/04/14. Check Total field added.
                objTable.Columns.Add("CONTROL_NUMBER");

                foreach (XmlNode objNode in objNodeList)
                {
                    objRow = objTable.NewRow();
                    objRow["PAYEE_NAME"] = objNode.Attributes["payeename"].Value;
                    objRow["CHECK_NUMBER"] = objNode.Attributes["checknumber"].Value;
                    objRow["DATE_OF_CHECK"] = objNode.Attributes["dateofcheck"].Value;
                    //pgupta93: RMA-7113 START
                    objRow["Type"] = objNode.Attributes["type"].Value;
                    //pgupta93: RMA-7113 END
                    objRow["TRANSACTION_TYPE"] = objNode.Attributes["transactiontype"].Value;
                    objRow["AMOUNT"] = objNode.Attributes["amount"].Value;
                    objRow["STATUS_CODE"] = objNode.Attributes["statuscode"].Value;
                    //Ankit Start : Worked on MITS - 32120
                    objRow["TOTAL_PAYEES"] = objNode.Attributes["totalpayees"].Value;
                    //Ankit End
                    //rupal Start : Worked on MITS - 33152
                    objRow["VOID_FLAG"] = objNode.Attributes["voidflag"].Value;
                    //rupal End
                    //Added by Swati Aggarwal for MITS # 33431
                    objRow["STOP_PAY_FLAG"] = objNode.Attributes["stoppayflag"].Value;
                    //end by swati
                    objRow["CLEARED_FLAG"] = objNode.Attributes["clearedflag"].Value;//MITS 33239 asharma326
                    //START -  added by Nikhil on 08/04/14. Check Total field added.
                    // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
                    //code needs to be reverted.
                    if (bApplyDeductibles)
                    {
                        objRow["CHECK_TOTAL"] = objNode.Attributes["checktotal"].Value;
                    }
                    //End -  added by Nikhil on 08/04/14. Check Total field added.
                    objRow["CONTROL_NUMBER"] = objNode.Attributes["ctrlnumber"].Value;
                    objTable.Rows.Add(objRow);
                }


                grdPaymentInfo.DataSource = objTable;
                grdPaymentInfo.DataBind();


                iCount = objTable.Rows.Count;
                if (iCount > 0)
                {
                    lblNoRecords.Visible = false;
                    
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                objRow = null;
                objTable = null;
                arrIDs = null;
                XmlTemplate = null;
                objNodeList = null;
                objXmlNode = null;
                objDocument = null;
            }

            return iCount;
        }

        protected void grdPaymentInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Payee Name";
                e.Row.Cells[1].Text = "Check Number";
                e.Row.Cells[2].Text = "Date of Check";
                e.Row.Cells[3].Text = "Type";//pgupta93: RMA-7113
                e.Row.Cells[4].Text = "Transaction Type";
                e.Row.Cells[5].Text = "Amount";
                e.Row.Cells[6].Text = "Status";
                e.Row.Cells[8].Text = "Void";//rupal:mits 33152
                e.Row.Cells[9].Text = "Stop Pay";//swati:mits 33431
                e.Row.Cells[10].Text = "Cleared";//asharma326 MITS 33239
                //START -  added by Nikhil on 08/04/14. Check Total field added.
                // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
                //code needs to be reverted.
                if (bApplyDeductibles)
                {
                    e.Row.Cells[11].Text = "Check Total";//asharma326 MITS 33239
                    e.Row.Cells[12].Text = "Control Number";
                }
                else
                {
                    e.Row.Cells[11].Text = "Control Number";
                }
                e.Row.Cells[7].Visible = false;
                //End -  added by Nikhil on 08/04/14. Check Total field added.

                // e.Row.CssClass = "";
            }
            //Ankit Start : Worked on MITS - 32120
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TableCell tableCell = e.Row.Cells[0];
                System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
                lbl.Text = tableCell.Text;
                tableCell.Controls.Add(lbl);

                System.Web.UI.WebControls.Image img = new System.Web.UI.WebControls.Image();
                string imageUrl = "~/Images/secUsersNode.gif";
                img.ImageUrl = imageUrl;
                img.AlternateText = Server.HtmlDecode(e.Row.Cells[7].Text);
                img.ToolTip = Server.HtmlDecode(e.Row.Cells[7].Text);
                img.Style.Add(HtmlTextWriterStyle.MarginLeft, "10px");
                tableCell.Wrap = false;
                tableCell.Controls.Add(img);
                e.Row.Cells[7].Visible = false;
            }
            
            //Ankit End
        }


        private XElement GetMessageTemplate( )
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<Payments>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(sClaimId);
            sXml = sXml.Append("</ClaimID>");
            sXml = sXml.Append("<ClaimantEID>");
            sXml = sXml.Append(sClaimantEId);
            sXml = sXml.Append("</ClaimantEID>");
            sXml = sXml.Append("<PolicyID>");
            sXml = sXml.Append(sPolicyID);
            sXml = sXml.Append("</PolicyID>");
            sXml = sXml.Append("<PolCvgID>");
            sXml = sXml.Append(sPolicyCvgRowID);
            sXml = sXml.Append("</PolCvgID>");
            sXml = sXml.Append("<ReserveTypeCode>");
            sXml = sXml.Append(sReserveTypeCode);
            sXml = sXml.Append("</ReserveTypeCode>");
            sXml = sXml.Append("<RCRowID>");
            sXml = sXml.Append(sReserveId);
            sXml = sXml.Append("</RCRowID>");
            sXml = sXml.Append("<CvgLossId>");
            sXml = sXml.Append(sCvgLossId);
            sXml = sXml.Append("</CvgLossId>");            
            sXml = sXml.Append("<CurrencyType>");           
            sXml = sXml.Append("</CurrencyType>");          
            sXml = sXml.Append("</Payments></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}