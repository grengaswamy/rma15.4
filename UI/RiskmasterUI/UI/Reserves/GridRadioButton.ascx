﻿<%--DataKeyNames="ID"--%> 
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GridRadioButton.ascx.cs" Inherits="Riskmaster.UI.UI.Reserves.GridRadioButton" %>
<asp:GridView ID="GridView1" Width="100%" runat="server" 
    AutoGenerateColumns="False" OnRowCreated="grdFixtures_RowCreated" 
    onrowdatabound="GridView1_RowDataBound">       
    
    <HeaderStyle CssClass="colheader3" />
    <AlternatingRowStyle CssClass="rowlight2" />
	<RowStyle CssClass="rowlight1" />
       <Columns>
       <asp:TemplateField>
           <ItemTemplate>
               <asp:Literal ID="RadioButtonMarkup" runat="server"></asp:Literal>
           </ItemTemplate>
       </asp:TemplateField>                        
       <asp:TemplateField HeaderText="PolicyID" >
           <ItemTemplate>
               <asp:Label runat="server" ID="PolicyID"  
               Text='<%#Eval("PolicyID") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="UnitID" >
           <ItemTemplate>
               <asp:Label runat="server" ID="PolicyUnitRowID"  
               Text='<%#Eval("PolicyUnitRowID") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="PolCvgID" >
           <ItemTemplate>
               <asp:Label runat="server" ID="PolCvgID"  
               Text='<%#Eval("CoverageID") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="PolCvgLossID" >
           <ItemTemplate>
               <asp:Label runat="server" ID="PolCvgLossID"  
               Text='<%#Eval("CoverageLossID") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="ReserveID" >
           <ItemTemplate>
               <asp:Label runat="server" ID="ReserveID"  
               Text='<%#Eval("ReserveID") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="ReserveTypeCode" >
           <ItemTemplate>
               <asp:Label runat="server" ID="ReserveTypeCode"  
               Text='<%#Eval("ReserveTypeCode") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="Policy Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="Policy"  
               Text='<%#Eval("Policy") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="Unit" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" ItemStyle-Wrap="true" >
           <ItemTemplate >
               <asp:Label runat="server" ID="Unit"  
               Text='<%#Eval("Unit") %>' ></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="Coverage Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="CoverageType"  
               Text='<%#Eval("CoverageType") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
         <asp:TemplateField HeaderText="Loss Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="LossType"  
               Text='<%#Eval("LossType") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
              <asp:TemplateField HeaderText="Reserve Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="ReserveType"  
               Text='<%#Eval("ReserveType") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       

       <asp:TemplateField HeaderText="Reserve Amount" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="ReserveAmount"  
               Text='<%#Eval("ReserveAmount") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>

       <asp:TemplateField HeaderText="Balance" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="Balance"  
               Text='<%#Eval("Balance") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>

       <asp:TemplateField HeaderText="Paid" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="Paid"  
               Text='<%#Eval("Paid") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>

       <asp:TemplateField HeaderText="Collection" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="Collection"  
               Text='<%#Eval("Collection") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="Incurred" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="Incurred"  
               Text='<%#Eval("Incurred") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>

       <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White">
           <ItemTemplate>
               <asp:Label runat="server" ID="Status"  
               Text='<%#Eval("Status") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
        <asp:TemplateField HeaderText="CovgSeqNum" >
           <ItemTemplate>
               <asp:Label runat="server" ID="CovgSeqNum"  
               Text='<%#Eval("CovgSeqNum") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
        <asp:TemplateField HeaderText="LossTypeCode" >
           <ItemTemplate>
               <asp:Label runat="server" ID="LossTypeCode"  
               Text='<%#Eval("LossTypeCode") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>       
        <asp:TemplateField HeaderText="ReserveSubType" >
           <ItemTemplate>
               <asp:Label runat="server" ID="ReserveSubType"  
               Text='<%#Eval("ReserveSubType") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
        <asp:TemplateField HeaderText="disabilitycatdesc" >
           <ItemTemplate>
               <asp:Label runat="server" ID="disabilitycatdesc"  
               Text='<%#Eval("disabilitycatdesc") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="StatusDetail" >
           <ItemTemplate>
               <asp:Label runat="server" ID="StatusDetail"  
               Text='<%#Eval("StatusDetail") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="Reason" >
           <ItemTemplate>
               <asp:Label runat="server" ID="Reason"  
               Text='<%#Eval("Reason") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="Adjuster" >
           <ItemTemplate>
               <asp:Label runat="server" ID="AdjusterDetails"  
               Text='<%#Eval("AdjusterDetails") %>'></asp:Label>
               <asp:Label runat="server" ID="AdjusterName"></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="RsvStatusParent">
           <ItemTemplate>
                <asp:Label runat="server" ID="RsvStatusParent" Text= '<%#Eval("RsvStatusParent")  %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="TransSeqNum" >
           <ItemTemplate>
               <asp:Label runat="server" ID="TransSeqNum"  
               Text='<%#Eval("TransSeqNum") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="CoverageKey" >
           <ItemTemplate>
               <asp:Label runat="server" ID="CoverageKey"  
               Text='<%#Eval("CoverageKey") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
        <asp:TemplateField HeaderText="ClmCurrBalAmount" >
            <ItemTemplate>
                <asp:Label runat="server" ID="ClmCurrBalAmount"  
                Text='<%#Eval("ClmCurrBalAmount") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ClmCurrResAmount" >
            <ItemTemplate>
                <asp:Label runat="server" ID="ClmCurrResAmount"  
                Text='<%#Eval("ClmCurrResAmount") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
         <%--sharishkumar: Mits 35472--%>
           <asp:TemplateField HeaderText="LSS Export">
           <ItemTemplate>
               <asp:Literal ID="CheckBoxMarkup" runat="server"></asp:Literal>
           </ItemTemplate>
       </asp:TemplateField>
           <asp:TemplateField HeaderText="LSSResExpFlag" >
           <ItemTemplate>
               <asp:Label runat="server" ID="LSSResExpFlag"  
               Text='<%#Eval("LSSResExpFlag") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
           <asp:TemplateField HeaderText="MasterReserveType" >
           <ItemTemplate>
               <asp:Label runat="server" ID="MasterReserveType"  
               Text='<%#Eval("MasterReserveType") %>'></asp:Label>
           </ItemTemplate>
       </asp:TemplateField>
           <%--End Mits 35472--%>    
       <%--JIRA-857 Starts--%>
           <asp:TemplateField HeaderText="PreventCollOnRes">
           <ItemTemplate>
               <asp:Label runat="server" style="display:none" ID="PreventCollOnRes"  
               Text='<%#Eval("PreventCollOnRes") %>'></asp:Label>
           </ItemTemplate>
       
       <%--JIRA-857 Ends--%>
               </asp:TemplateField>
           <asp:TemplateField HeaderText="DedTypeCode" >
               <ItemTemplate>
                   <asp:Label runat="server" style="display:none" ID="DedTypeCode" Text='<%#Eval("DedTypeCode") %>' ></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="DedRcRowId" >
               <ItemTemplate>
                   <asp:Label runat="server" style="display:none" ID="DedRcRowId" Text='<%#Eval("DedRcRowId") %>' ></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
       </Columns>
</asp:GridView>