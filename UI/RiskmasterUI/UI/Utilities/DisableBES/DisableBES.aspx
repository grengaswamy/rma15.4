﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisableBES.aspx.cs" Inherits="Riskmaster.UI.Utilities.DisableBES.DisableBES" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Entity Security</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../../Scripts/form.js"></script>

    <script type="text/javascript">
        function Validate() {
            if (document.forms[0].optPartial.checked == false && document.forms[0].optFull.checked == false) {
                alert("Please select atleast one option");
                return false;
            }

            return true;
        }
    </script>

</head>
<body>
    <form id="frmData" method="post" runat="server">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""><input type="hidden"
        id="IsFirstTime" ref="/Instance/Document/Flag" value=""><table border="0" cellspacing="0"
            celpadding="0" width="70%" align="center">
            <tr>
                <td class="msgheader" colspan="2">
                    Disable Business Entity Security
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup" colspan="2">
                    Select the disable method you would like to use:
                </td>
            </tr>
            <tr>
                <td colspan="2" class="Bold1">
                    <input type="radio" name="$node^21" value="0" id="optPartial" runat="server">Partial
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td width="*">
                    Partially disabling Business Entity Security will only turn off this setting. 
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" class="Bold1">
                    <input type="radio" name="$node^21" value="1" id="optFull" runat="server">Full
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    Fully disabling Business Entity Security will additionally remove Views, Triggers, User Loging and all configuration
                    information related to this feature (i.e. Security Groups, Entity to Group Mappings,
                    etc.).
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="group" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button  id="btnOK" Text="&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;"
                        class="button" OnClientClick="return Validate(); " runat ="server" OnClick ="btnOk_Click"></asp:Button>
                    <asp:Button  id="btnCancel" OnClick ="btnCancel_Click"
                            Text="&nbsp;&nbsp;Cancel&nbsp;&nbsp;" class="button" runat ="server"></asp:Button>
                </td>
            </tr>
        </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-4" id="SysWindowId"><input
        type="hidden" name="$instance" value="H4sIAAAAAAAAAIWTy1LDMAxF1+UrMtkn4rHrpF0wsOgAGx4zbF1HoRr8Gttpwt+jNq1LgEBWse7x&#xA;tSxZ1cqEKIzErNfKhHlPi3wTo5sDdF1Xdlel9W9weX5+Ba8rI1VbY35AnceG+gn8EvrGeh2OMG4n&#xA;wAtgoMAtmpjgvh3BHKzXe75vXS1iysD2zQi0fo3WlNJqYAmctxJDsD4Zuz9xcqjIJPfhAv/db3k2&#xA;m82yw1dpjGIfSaEkBekRTUH1EjyFdy1CRA+PDy+RFEXCADcUxFrh9e1TBSf6d7c2oN+pMsgKjouJ&#xA;g7kGZHdeMEo2AR2Z2nY7gwpO/z+wZwzxDj+GI4+LpEK6+ilHmshIyMgJwYR6T+adradk7mcspFWt&#xA;nnTYI9bXXN+/CH5KoogfDiepDZcCvecXtGyECsh9+RL6fdMgjkvNLaJx4Gloyso09hva05yGQcs2&#xA;PGGLvCzh9eEejoNaPKN2iqegeFntRiP/tv/GylbzNJ1aI62J3qrMCI2L3Lp4eGh5thWq5RBbDFl+&#xA;2cueVTpz+QlDzGceJwQAAA==">
    </form>
</body>
</html>
