﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BES.aspx.cs" Inherits="Riskmaster.UI.Utilities.BES.BES" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Entity Security</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../../Scripts/form.js"></script><script type="text/javascript">
     var m_FormSubmitted=false;
      function Validate()
      { 
	    if(!m_FormSubmitted)
		{
			m_FormSubmitted=true;
			if(document.forms[0].IsFirstTime.value == "True")
			{
			 //return confirm("This is the first time that business entity security has been used for this database.  An initial setup process which will prepare the database for use with this feature has to be run.  This process can take several hours depending on the size of your database.  Do you wish to continue?");
			    m_FormSubmitted = confirm("This is the first time that business entity security has been used for this database. An asynchronous process will complete the initial setup to prepare the database. Please ensure that BES Scheduler is configured through Task Manager.");
			 return m_FormSubmitted;
			 }
		}
		else
		{
			alert("You already submitted this form. Please wait for server to respond.");
			return false;
		}

      }
      function OnSubmitForm()
	{
		if(m_FormSubmitted)
			{
				alert("You already submitted this form. Please wait for server to respond.");
				return false;
			}
	
		m_FormSubmitted=true;
		return true;
	} 
     </script>
</head>
<body>
    <form id="frmData" runat="server">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">
    <input type="hidden"  id="IsFirstTime" value="False" runat="server"><table border="0" cellspacing="0"
            celpadding="0" width="60%" align="center">
            <tr>
                <td class="msgheader">
                    Business Entity Security
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    Business Entity Security (BES) enables you to create and maintain separate data
                    views (groups) within a single RISKMASTER database, based on selections from the
                    organizational hierarchy.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <!-- pmittal5 02/24/10 - Confidential Records-->
            <tr>
                <td>
                    Select the Confidential Record checkbox to activate Confidential Record functionality. Confidential Record checkbox will be disabled once BES gets enabled. If Business Entity Security(BES) is enabled, it would need to be disabled and enabled again to activate/ deactivate Confidential Record functionality.
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="chkConfRec" runat="server" appearance="full" Checked="false" rmxref="/Instance/Document/ConfRec"/>Enable Confidential Record
                </td>
            </tr> 
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <!-- End - pmittal5-->
            <tr>
                <td>
                    To Activate Business Entity Security click on the Set-Up button
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button Text="&nbsp;&nbsp;BES Setup&nbsp;&nbsp;" CssClass="button" ID ="btnBesSetup"
                         OnClientClick="return Validate();" OnClick ="btnBesSetup_click" PostBackUrl="~/UI/Utilities/LoadOrgSet/LoadOrgSet.aspx" runat="server"></asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    To Disable Business Entity Security click the button below
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:button  name="Disable" id ="btnDisable" Text="&nbsp;&nbsp;Disable BES&nbsp;&nbsp;"
                        class="button" OnClientClick="return OnSubmitForm(); " runat ="server" OnClick ="btnDisable_Click"></asp:Button>
                </td>
            </tr>
        </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId"><input
        type="hidden" name="$instance" value="H4sIAAAAAAAAAIWTwW7CMAyGz/AUFffWDG6o9DBtk9DgMkDiWlIXItKkSlJa3n6mXUMpY8up/v3Z&#xA;sR03XEhjY8nQqzIhzazi89HR2nwGUJZlUE4DpQ8wGY+nsFtIJooERz9orjHl1RN8AlWqdGZaGM9P&#xA;wBcgwMczSuvgqriDSUz2NV8VeRJbV4Gq0jtQ6T0qGTCVAbkg14qhMUq7xPmfOM9RcOmyNw381180&#xA;HAwGHp0wQxvXVm3WkmEaUfo8iUBzc8piY1HD12prueCWo4HX93UIN+w+vDCoryozLITW6N1ADXJ1&#xA;jQVXSe0ouUxUeQ0I4fbt3Bs09hMvTerWqD3g+mhq4L0bY2bpQuipSy5PlKIv0/Ctz5QosoeI2qV0&#xA;QgP5zUPvHPv2kuOD90itoNb0rFEaC4M0v450DzfibTQ0Rn4z1s3wFjJVHaTiM95sunekFZ+PggB2&#xA;qyW0f4q/wSwXtIb+dnHdzVEn9k2xIqNVjoZe74QfIj50C3Go55EYuvTRN5+05fyTAwAA">
    </form>
</body>
</html>
