﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

//namespace Riskmaster.UI.Shared.Controls
namespace Riskmaster.UI.Utilities.SSO
{
    public partial class SSOC_WindowsADConfig : System.Web.UI.UserControl
    {

        //public bool IsAuthEnabled
        //{
        //    get
        //    {
        //        System.Web.UI.WebControls.CheckBox Enabled = (System.Web.UI.WebControls.CheckBox)frmADConfig.FindControl("chkEnabled");
                
        //        return Enabled.Checked;
        //    }
        //} // property IsEnabled

        public System.Web.UI.WebControls.CheckBox Enabled
        {
            get
            {
                return (System.Web.UI.WebControls.CheckBox)frmADConfig.FindControl("chkEnabled");
            }
        }

        public string Hostname
        {
            get
            {
                System.Web.UI.WebControls.TextBox txtHostname = (System.Web.UI.WebControls.TextBox)frmADConfig.FindControl("txtHostName");
                return txtHostname.Text.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }//event: Page_Load()

        
        protected void chkEnabled_CheckedChanged(object sender, EventArgs e)
        {
            SSOConfig myParent = (SSOConfig)Parent.Page;
            SSOC_LDAPConfig ucLDAP = myParent.LDAPConfig;


            if (this.Enabled.Checked && ucLDAP.Enabled!=null && ucLDAP.Enabled.Checked)
            {
                StringBuilder sWarning = new StringBuilder();

                sWarning.Append("<script>window.alert('By choosing to enable \\'");
                sWarning.Append(Hostname);
                sWarning.Append("\\' you will be disabling \\'");
                sWarning.Append(ucLDAP.Hostname);
                sWarning.Append("\\'');");
                sWarning.Append("</script>");

                myParent.RegisterStartupScript("SourceWarning", sWarning.ToString());

                ucLDAP.Enabled.Checked = false;

            } // if

        }
    }
}