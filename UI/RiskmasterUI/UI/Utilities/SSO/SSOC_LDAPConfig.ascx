﻿<%@ control language="C#" autoeventwireup="true" codebehind="SSOC_LDAPConfig.ascx.cs"
    inherits="Riskmaster.UI.Utilities.SSO.SSOC_LDAPConfig" %>
<div>
    <asp:formview id="frmLDAPConfig" runat="server" datasourceid="objLDAPDS">
        <itemtemplate>
            <table border="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:hiddenfield id="hdnProviderID" runat="server" value='<%# Eval("ProviderID") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center-text">
                        <asp:checkbox id="chkEnabled" runat="server" text="Enabled" Enabled="false" checked='<%# Eval("IsEnabled") %>' />
                    </td>
                </tr>
                <tr>
                    <td>
                    Please enter the LDAP Host Name:
                    </td>
                    <td>
                        <%--<asp:label id="lstHostName" runat="server" text='<%# Eval("HostName") %>' />--%>
                        <asp:TextBox ID="txtHostName" runat="server" Text='<%# Eval("HostName") %>' ReadOnly="true" />
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    Please choose the type of Secure Sockets Layer (SSL) authentication used by the
                    LDAP host:
                </td>
            </tr>--%>
                <tr>
                    <td>
                    Type of SSL Authentication:
                    </td>
                    <td>
                        <asp:dropdownlist id="ddlSSLAuthentication" runat="server" datasourceid="objDSConnectionTypes"
                        datatextfield="value" datavaluefield="key" selectedvalue='<%# Eval("ConnectionType") %>'
                        enabled="false" />
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    Please enter the base LDAP distinguished name you would like to use:
                </td>
            </tr>--%>
                <tr>
                    <td>
                    Base LDAP distinguished name:
                    </td>
                    <td>
                        <asp:textbox id="txtBaseLDAPDN" runat="server" width="100%" ReadOnly="true" Text='<%# Bind("BaseDN") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="required" align="center">
                    LDAP Server Attribute Mappings:
                    </td>
                </tr>
                <tr>
                    <td>
                    User Name:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPUsernameAttribute" width="100%" ReadOnly="true" runat="server" Text='<%# Bind("UserAttribute") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="required">
                    Credentials required by the LDAP host:
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    LDAP Server Admin credentials:
                </td>
            </tr>--%>
                <tr>
                    <td class="fieldRightAligned">
                    Distinguished Name:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPCredentialsDN" runat="server" ReadOnly="true" width="100%" text='<%# Eval("AdminUserName") %>' />
                    </td>
                </tr>
                <tr>
                    <td class="fieldRightAligned">
                    Password:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPCredentialsPW" runat="server" ReadOnly="true" width="100%" textmode="Password"
                        text='<%# Eval("AdminPwd") %>' autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:button id="cmdEdit" text="Edit" commandname="Edit" runat="server" />
                    </td>
                </tr>
            </table>
        </itemtemplate>
        <InsertItemTemplate>
            <table border="0" width="100%">
                <%--            <tr>
                <td>
                    <asp:hiddenfield id="i_hdnProviderID" runat="server" value='<%# Bind("ProviderID") %>' />
                </td>
            </tr>--%>
                <tr>
                    <td colspan="2" class="center-text">
                        <asp:checkbox id="chkEnabled" AutoPostBack="true" OnCheckedChanged="chkEnabled_CheckedChanged" runat="server" text="Enabled" checked='<%# Bind("IsEnabled") %>'
                        enabled="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                    Please enter the LDAP Host Name:
                    </td>
                    <td>
                        <asp:textbox id="txtHostName" runat="server" text='<%# Bind("HostName") %>' />
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    Please choose the type of Secure Sockets Layer (SSL) authentication used by the
                    LDAP host:
                </td>
            </tr>--%>
                <tr>
                    <td>
                    Type of SSL Authentication:
                    </td>
                    <td>
                        <asp:dropdownlist id="ddlSSLAuthentication" runat="server" datasourceid="objDSConnectionTypes"
                        datatextfield="value" datavaluefield="key" selectedvalue='<%# Bind("ConnectionType") %>' />
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    Please enter the base LDAP distinguished name you would like to use:
                </td>
            </tr>--%>
                <tr>
                    <td>
                    Base LDAP distinguished name:
                    </td>
                    <td>
                        <asp:textbox id="txtBaseLDAPDN" runat="server" width="100%" Text='<%# Bind("BaseDN") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="required" align="center">
                    LDAP Server Attribute Mappings
                    </td>
                </tr>
                <tr>
                    <td>
                    User Name:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPUsernameAttribute" width="100%" runat="server" Text='<%# Bind("UserAttribute") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="required">
                    Credentials required by the LDAP host
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    LDAP Server Admin credentials:
                </td>
            </tr>--%>
                <tr>
                    <td class="fieldRightAligned">
                    Distinguished Name:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPCredentialsDN" runat="server" width="100%" text='<%# Bind("AdminUserName") %>' />
                    </td>
                </tr>
                <tr>
                    <td class="fieldRightAligned">
                    Password:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPCredentialsPW" runat="server" width="100%" textmode="Password"
                        text='<%# Bind("AdminPwd") %>' autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:button id="cmdInsert" text="Insert" commandname="Insert" runat="server" />
                        <asp:button id="cmdCancel" text="Cancel" commandname="Cancel" runat="server" />
                    </td>
                </tr>
            </table>
        </InsertItemTemplate>
        <edititemtemplate>
            <table border="0" width="100%">
                <tr>
                    <td>
                        <asp:hiddenfield id="hdnProviderID" runat="server" value='<%# Bind("ProviderID") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="center-text">
                        <asp:checkbox id="chkEnabled" AutoPostBack="true" OnCheckedChanged="chkEnabled_CheckedChanged" runat="server" text="Enabled" checked='<%# Bind("IsEnabled") %>'
                        enabled="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                    Please enter the LDAP Host Name:
                    </td>
                    <td>
                        <asp:textbox id="txtHostName" runat="server" text='<%# Bind("HostName") %>' />
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    Please choose the type of Secure Sockets Layer (SSL) authentication used by the
                    LDAP host:
                </td>
            </tr>--%>
                <tr>
                    <td>
                    Type of SSL Authentication:
                    </td>
                    <td>
                        <asp:dropdownlist id="ddlSSLAuthentication" runat="server" datasourceid="objDSConnectionTypes"
                        datatextfield="value" datavaluefield="key" selectedvalue='<%# Bind("ConnectionType") %>' />
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    Please enter the base LDAP distinguished name you would like to use:
                </td>
            </tr>--%>
                <tr>
                    <td>
                    Base LDAP distinguished name:
                    </td>
                    <td>
                        <asp:textbox id="txtBaseLDAPDN" runat="server" width="100%" Text='<%# Bind("BaseDN") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="required" align="center">
                    LDAP Server Attribute Mappings
                    </td>
                </tr>
                <tr>
                    <td>
                    User Name:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPUsernameAttribute" width="100%" runat="server" Text='<%# Bind("UserAttribute") %>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="required">
                    Credentials required by the LDAP host
                    </td>
                </tr>
                <%--            <tr>
                <td>
                    LDAP Server Admin credentials:
                </td>
            </tr>--%>
                <tr>
                    <td class="fieldRightAligned">
                    Distinguished Name:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPCredentialsDN" runat="server" width="100%" text='<%# Bind("AdminUserName") %>' />
                    </td>
                </tr>
                <tr>
                    <td class="fieldRightAligned">
                    Password:
                    </td>
                    <td>
                        <asp:textbox id="txtLDAPCredentialsPW" runat="server" width="100%" textmode="Password"
                        text='<%# Bind("AdminPwd") %>' autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:button id="cmdUpdate" text="Update" commandname="Update" runat="server" />
                        <asp:button id="cmdCancel" text="Cancel" commandname="Cancel" runat="server" />
                    </td>
                </tr>
            </table>
        </edititemtemplate>
        <EmptyDataTemplate>
            <asp:button id="cmdNew" CommandName="New" runat="server" Text="New" />
        </EmptyDataTemplate>
    </asp:formview>
</div>
<asp:objectdatasource id="objLDAPDS" runat="server" oldvaluesparameterformatstring="original_{0}"
    selectmethod="SelectLDAPRecord" updatemethod="UpdateLDAPRecord" typename="LDAPDataSource"
    dataobjecttypename="LDAPDataSource" deletemethod="DeleteLDAPRecord" insertmethod="InsertLDAPRecord">
    <InsertParameters>
        
    </InsertParameters>
</asp:objectdatasource>
<asp:objectdatasource id="objDSConnectionTypes" runat="server" oldvaluesparameterformatstring="original_{0}"
    selectmethod="SelectConnectionTypes" typename="ConnectionTypes"></asp:objectdatasource>


