﻿<%@ page language="C#" autoeventwireup="true" codebehind="SSOConfig.aspx.cs" inherits="Riskmaster.UI.Utilities.SSO.SSOConfig"
    masterpagefile="~/App_Master/RMXMain.Master" theme="RMX_Default" %>

<%@ register src="~/UI/Utilities/SSO/SSOC_LDAPConfig.ascx" tagname="LDAPConfig" tagprefix="lcf" %>
<%@ register src="~/UI/Utilities/SSO/SSOC_WindowsADConfig.ascx" tagname="WindowsADConfig"
    tagprefix="wcf" %>
<asp:content id="cHeader" contentplaceholderid="cphHeader" runat="server">
</asp:content>

<asp:content id="cMainBody" contentplaceholderid="cphMainBody" runat="server">
    <div>   
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ajaxtoolkit:tabcontainer id="SSOConfigTabs" CssClass="ajax__tab_xp2" runat="server" activetabindex="0">
            <ajaxtoolkit:tabpanel runat="server" headertext="LDAP" id="tabLDAPConfig">
                <headertemplate>
                    LDAP
                </headertemplate>
                <contenttemplate>
                    <lcf:ldapconfig id="ucLDAPConfig" runat="server" />
                </contenttemplate>
            </ajaxtoolkit:tabpanel>
            <ajaxtoolkit:tabpanel runat="server" headertext="Windows AD" id="tabWindowsADConfig">
                <headertemplate>
                    Active Directory
                </headertemplate>
                <contenttemplate>
                    <wcf:windowsadconfig id="ucWindowsADConfig" runat="server" />
                </contenttemplate>
            </ajaxtoolkit:tabpanel>
        </ajaxtoolkit:tabcontainer>
    </div>
    <br /><br />
    <div align="center"><input type="button" id="btClose" title="Close" 
            onclick="javascript:window.close()" value="Close" /></div>
</asp:content>
