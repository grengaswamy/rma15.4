﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Riskmaster.UI.Utilities.SSO
{
    public partial class SSOConfig : System.Web.UI.Page
    {
        public SSOC_LDAPConfig LDAPConfig
        {
            get{return this.ucLDAPConfig;}
        } // property LDAPConfig

        public SSOC_WindowsADConfig WindowsADConfig
        {
            get{return this.ucWindowsADConfig;}
            
        } // property WindowsADConfig



        protected void Page_Load(object sender, EventArgs e)
        {
            //achouhan3 JIRA 88 issue fix - Active Directory tab not visible when SSO enable    Aug/08/2014     Starts
            bool m_bUseSSO = false;
            Boolean.TryParse(ConfigurationManager.AppSettings["UseSSO"] ?? "false", out m_bUseSSO);
            if (m_bUseSSO)
            {
                tabWindowsADConfig.Visible = false;
            }
            //achouhan3 JIRA 88 issue fix - Active Directory tab not visible when SSO enable    Aug/08/2014     Ends

        }
    }
}
