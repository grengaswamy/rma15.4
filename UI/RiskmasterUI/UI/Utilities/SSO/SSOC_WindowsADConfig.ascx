﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SSOC_WindowsADConfig.ascx.cs" Inherits="Riskmaster.UI.Utilities.SSO.SSOC_WindowsADConfig" %>

<asp:formview id="frmADConfig" runat="server" datasourceid="ObjectDataSource1">
    <itemtemplate>
        <table border="0" width="100%">
            <tr>
                <td>
                    <asp:hiddenfield id="hdnProviderID" runat="server" value='<%# Eval("ProviderID") %>' />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center-text">
                    <asp:checkbox id="chkEnabled" runat="server" OnCheckedChanged="chkEnabled_CheckedChanged" text="Enabled" checked='<%# Eval("IsEnabled") %>' enabled="false" />
                </td>
            </tr>
            <tr>
                <td>
                    Active Directory Domain Name:
                </td>
                <td>
                    <%--<asp:label id="lstHostName" runat="server" text='<%# Eval("HostName") %>' />--%>
                    <asp:TextBox ID="txtHostName" runat="server" Text='<%# Eval("HostName") %>' ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td class="required" align="center" colspan="2">
                    Windows Active Directory Options
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    Type of SSL Authentication :
                </td>
                <td>
                    <asp:dropdownlist id="ddlSSLAuthentication" runat="server" datasourceid="objDSConnectionTypes" datatextfield="value" datavaluefield="key" selectedvalue='<%# Eval("ConnectionType") %>' enabled="false" />
                </td>
            </tr>
            <tr>
                <td class="required" align="center" colspan="2">
                    Windows AD Credentials
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    User ID :
                </td>
                <td>
                    <%--<asp:label id="lblUserID" runat="server" text='<%# Eval("AdminUserName") %>' />--%>
                    <asp:TextBox id="txtUserID" runat="server" ReadOnly="true" Text='<%#Eval("AdminUserName") %>' />
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    Password :
                </td>
                <td>
                    <%--<asp:label id="lblPassword" runat="server" text='<%# Eval("AdminUserName") %>' />--%>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" ReadOnly="true" Text='<%# Eval("AdminPwd") %>' autocomplete="off"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:button id="cmdEdit" text="Edit" commandname="Edit" runat="server" />
                </td>
                
            </tr>
        </table>
    </itemtemplate>
    <edititemtemplate>
        <table border="0" width="100%">
            <tr>
                <td>
                    <asp:hiddenfield id="hdnProviderID" runat="server" value='<%# Bind("ProviderID") %>' />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="center-text">
                    <asp:checkbox id="chkEnabled" AutoPostBack="true" OnCheckedChanged="chkEnabled_CheckedChanged" runat="server" text="Enabled" checked='<%# Bind("IsEnabled") %>' />
                </td>
            </tr>
            <tr>
                <td>
                    Active Directory Domain Name:
                </td>
                <td>
                    <asp:textbox id="txtHostName" runat="server" text='<%# Bind("HostName") %>' />
                </td>
            </tr>
            <tr>
                <td class="required" align="center" colspan="2">
                    Windows Active Directory Options
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    Type of SSL Authentication :
                </td>
                <td>
                    <%--TODO : Add more AD options here :--%>
                    <asp:dropdownlist id="ddlSSLAuthentication" runat="server" datasourceid="objDSConnectionTypes" datatextfield="value" datavaluefield="key" selectedvalue='<%# Bind("ConnectionType") %>' />
                </td>
            </tr>
            <tr>
                <td class="required" align="center" colspan="2">
                    Windows AD Credentials
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    User ID :
                </td>
                <td>
                    <asp:textbox id="txtUserID" runat="server" width="100%" text='<%# Bind("AdminUserName") %>' />
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    Password :
                </td>
                <td>
                    <asp:textbox name="txtPassword" id="txtPassword" runat="server" width="100%" textmode="Password" text='<%# Bind("AdminPwd") %>' autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:button id="cmdUpdate" text="Update" commandname="Update" runat="server" />
                   <asp:button id="cmdCancel" text="Cancel" commandname="Cancel" runat="server" />
                </td>
            </tr>
        </table>
    </edititemtemplate>
    <InsertItemTemplate>
            <table border="0" width="100%">
<%--            <tr>
                <td>
                    <asp:hiddenfield id="hdnProviderID" runat="server" value='<%# Bind("ProviderID") %>' />
                </td>
            </tr>--%>
            <tr>
                <td colspan="2" class="center-text">
                    <asp:checkbox id="chkEnabled" AutoPostBack="true" OnCheckedChanged="chkEnabled_CheckedChanged" runat="server" text="Enabled" checked='<%# Bind("IsEnabled") %>' />
                </td>
            </tr>
            <tr>
                <td>
                    Active Directory Domain Name:
                </td>
                <td>
                    <asp:textbox id="txtHostName" runat="server" text='<%# Bind("HostName") %>' />
                </td>
            </tr>
            <tr>
                <td class="required" align="center" colspan="2">
                    Windows Active Directory Options
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    Type of SSL Authentication :
                </td>
                <td>
                    <%--TODO : Add more AD options here :--%>
                    <asp:dropdownlist id="ddlSSLAuthentication" runat="server" datasourceid="objDSConnectionTypes" datatextfield="value" datavaluefield="key" selectedvalue='<%# Bind("ConnectionType") %>' />
                </td>
            </tr>
            <tr>
                <td class="required" align="center" colspan="2">
                    Windows AD Credentials
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    User ID :
                </td>
                <td>
                    <asp:textbox id="txtUserID" runat="server" width="100%" text='<%# Bind("AdminUserName") %>' />
                </td>
            </tr>
            <tr>
                <td class="fieldRightAligned">
                    Password :
                </td>
                <td>
                    <asp:textbox name="txtPassword" id="txtPassword" runat="server" width="100%" textmode="Password" text='<%# Bind("AdminPwd") %>' autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:button id="cmdInsert" text="Insert" commandname="Insert" runat="server" />
                   <asp:button id="cmdCancel" text="Cancel" commandname="Cancel" runat="server" />
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <EmptyDataTemplate>
        <asp:Button ID="cmdNew" Text="New" CommandName="New" runat="server" />
    </EmptyDataTemplate>
</asp:formview>
<asp:objectdatasource id="ObjectDataSource1" runat="server" oldvaluesparameterformatstring="original_{0}"
    selectmethod="SelectADRecord" updatemethod="UpdateADRecord" InsertMethod="InsertADRecord"
    typename="ADDataSource" dataobjecttypename="ADDataSource"></asp:objectdatasource>
<asp:objectdatasource id="objDSConnectionTypes" runat="server" 
    oldvaluesparameterformatstring="original_{0}" 
    selectmethod="SelectConnectionTypes" typename="ConnectionTypes">
</asp:objectdatasource>

