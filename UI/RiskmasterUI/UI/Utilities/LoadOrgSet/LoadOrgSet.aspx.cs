﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.LoadOrgSet
{
    public partial class LoadOrgSet : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            string sCWSresponse = "";
            if (hdnRowId.Value == "")// For Handling Delete since the UserControlGrid is used and Delete is handled thru it
            {
                if (!Page.IsPostBack)
                {
                    try
                    {
                        //Preparing XML to send to service
                        XElement oMessageElement = LoadOrgSetTemplate();
                        //Modify XML 
                        //If the page is coming from System Login
                        if (AppHelper.GetQueryStringValue("AdminUserID") != "")
                        {
                            XElement oAdminUserID = oMessageElement.XPathSelectElement("./Document/PassToWebService/AdminUserID");
                            oAdminUserID.Value = AppHelper.GetQueryStringValue("AdminUserID");
                            AdminUserID.Value = oAdminUserID.Value;

                        }//If the page is coming from System Login
                        if (AppHelper.GetQueryStringValue("AdminPwd") != "")
                        {
                            XElement oAdminPwd = oMessageElement.XPathSelectElement("./Document/PassToWebService/AdminPwd");
                            oAdminPwd.Value = AppHelper.GetQueryStringValue("AdminPwd");
                            AdminPwd.Value = oAdminPwd.Value;

                        }//If the page is coming from System Login
                        if (AppHelper.GetQueryStringValue("OracleRole") != "")
                        {
                            XElement oOracleRole = oMessageElement.XPathSelectElement("./Document/PassToWebService/OracleRole");
                            oOracleRole.Value = AppHelper.GetQueryStringValue("OracleRole");
                            OracleRole.Value = oOracleRole.Value;

                        }
                        //pmittal5 03/08/10 - Passing Confidential Record Flag value
                        if (AppHelper.GetQueryStringValue("ConfRec") != "")
                        {
                            XElement oConfRec = oMessageElement.XPathSelectElement("./Document/PassToWebService/ConfRec");
                            oConfRec.Value = AppHelper.GetQueryStringValue("ConfRec");
                            ConfRec.Value = oConfRec.Value;

                        }
                        CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                        //CallCWSFunctionBind("ORGSECAdaptor.LoadOrgSet", out sCWSresponse, oMessageElement);

                        #region For normal grid
                        //if (oFDMPageDom.SelectSingleNode("//OrgEntitySecurityList") != null)
                        //{
                        //    DataSet aDataSet = new DataSet();
                        //    aDataSet.ReadXml(new StringReader(oFDMPageDom.SelectSingleNode("//OrgEntitySecurityList").OuterXml));
                        //    GridView1.DataSource = aDataSet.Tables["Option"];
                        //    //GridView1.DataTextField = "title";
                        //    //GridView1.DataValueField = "value";
                        //    GridView1.DataBind();
                        //}
                        #endregion

                        //oFDMPageDom.LoadXml(sCWSresponse);
                        if (oFDMPageDom.SelectSingleNode("//Document/IsAdmin") != null)//To open System Login page before opening LoadOrgSec
                        {
                            //Changed by nadim for BES(4/16/2009)
                            if (oFDMPageDom.SelectSingleNode("//Document/IsAdmin").InnerText.ToLower() == "false")
                            {
                                string IsAdmin = oFDMPageDom.SelectSingleNode("//Document/IsAdmin").InnerText;
                                string ShowOnlyRole = "";
                                if (oFDMPageDom.SelectSingleNode("//Document/ShowOnlyRole") != null)
                                    ShowOnlyRole = oFDMPageDom.SelectSingleNode("//Document/ShowOnlyRole").InnerText;
                                Context.Items.Add("IsAdmin", IsAdmin);
                                Context.Items.Add("ShowOnlyRole", ShowOnlyRole);
                                //Context.Items.Add("ViewEnhanceBES", oFDMPageDom.SelectSingleNode("//ViewEnhanceBES").InnerText);
                                Server.Transfer("~/UI/Utilities/SystemAdminLogin/SystemAdminLogin.aspx");
                            }
                        }
                        else
                        {
                            OrgSetGrid.BindData(oFDMPageDom);
                        }

                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else//To handle the delete button in case of UserControl Grid
            {
                
                //Preparing XML to send to service
                XElement oMessageElement = DeleteOrgSetTemplate();
                //Modify XML 
                XElement oRowId = oMessageElement.XPathSelectElement("./Document/document/RowId");
                if (oRowId != null)
                {
                    oRowId.Value = hdnRowId.Value;
                }
                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                oMessageElement = LoadOrgSetTemplate();
                //pmittal5 -Confidential Record
                XElement oConfRec = oMessageElement.XPathSelectElement("./Document/PassToWebService/ConfRec");
                oConfRec.Value = ConfRec.Value;
                //End - pmittal5
                CallCWSFunctionBind("ORGSECAdaptor.LoadOrgSet", out sCWSresponse, oMessageElement);
                hdnRowId.Value = "";
            }
            CustomizePage();
        }
        
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            ErrorControl1.errorDom = p_sReturn;
            Exception ex = new Exception("Service Error");
            if (ErrorControl1.errorFlag == true)
                throw ex;
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }

        /// <summary>
        /// CWS request message template for Org Set
        /// </summary>
        /// <returns></returns>
        private XElement LoadOrgSetTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>ORGSECAdaptor.LoadOrgSet</Function> 
                      </Call>
                      <Document>
                          <PassToWebService>
                              <ConfRec></ConfRec>
                              <OrgEntitySecurityList>
                                  <listhead>
                                    
                                    <GroupName>a</GroupName>
                                    <GroupStatus>Group Status</GroupStatus>
                                  </listhead>
                              </OrgEntitySecurityList>
                              <AdminUserID /> 
                              <AdminPwd /> 
                              <OracleRole /> 
                          </PassToWebService>
                          <RowId></RowId> 
                      </Document>
                </Message>


            ");
            return oTemplate;
        }

        /// <summary>
        /// CWS request message template for Org Set
        /// </summary>
        /// <returns></returns>
        private XElement DeleteOrgSetTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                     <Message>
                          <Authorization>5a773df7-80bc-4e87-9a56-96501d3f92d3</Authorization> 
                             <Call>
                              <Function>ORGSECAdaptor.DeleteGroupDef</Function> 
                              </Call>
                              <Document>
                                  <document>
                                      <PassToWebService>
                                          <ConfRec></ConfRec>
                                          <OrgEntitySecurityList>
                                          <listhead>
                                              <RowId /> 
                                              <GroupName /> 
                                            <GroupStatus></GroupStatus>
                                          </listhead>
                                          </OrgEntitySecurityList>
                                          <AdminUserID /> 
                                          <AdminPwd /> 
                                          <OracleRole /> 
                                      </PassToWebService>
                                      <RowId></RowId> 
                                  </document>
                               </Document>
                      </Message>");
            return oTemplate;
        }

        /// <summary>
        /// CWS request message template for Org Set
        /// </summary>
        /// <returns></returns>
        private XElement LoadDeleteOrgSetTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                     <Message>
                          <Authorization>a7abceb9-2cec-4b1e-9a2b-5938d7c09be1</Authorization> 
                          <Call>
                                <Function>ORGSECAdaptor.PrintOrgSec</Function> 
                          </Call>
                          <Document>
                                <Utility /> 
                          </Document>
                      </Message>
                    ");
            return oTemplate;
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string sReturn = "";
            //Preparing XML to send to service
            XElement oMessageElement = LoadDeleteOrgSetTemplate();
            //Modify XML 
            CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
            string pdf = oFDMPageDom.SelectSingleNode("//Document/OrgSecReport").InnerText;
           
            Response.ClearContent();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=OrgSecReport.pdf");
            Response.BinaryWrite(Convert.FromBase64String(pdf));
            Response.Flush();
            Response.Close();
        }
        /// <summary>
        /// CustomizePage to change color depending upon status
        /// </summary>
        /// <returns></returns>
        private void CustomizePage()
        {
            XmlDocument oFDMPageDom = null;
            const string GROUP_STATUS = "Group Status";
            string sGroupStatus=string.Empty;
            GridView objOrgSetGrid = (GridView)OrgSetGrid.GridView;         
            int iGroupStatusCellIndex = 0;

            for (int iCount = 0; iCount < objOrgSetGrid.Rows.Count; iCount++)
            {
               
               
                iGroupStatusCellIndex = GetGridCellIndex(objOrgSetGrid, GROUP_STATUS);
                sGroupStatus = objOrgSetGrid.Rows[iCount].Cells[iGroupStatusCellIndex].Text.ToLower();
                switch (sGroupStatus)
                {
                    case "in process":
                        objOrgSetGrid.Rows[iCount].Cells[iGroupStatusCellIndex].ForeColor = System.Drawing.Color.Blue;
                        
                        break;
                    case "failed":

                        objOrgSetGrid.Rows[iCount].Cells[iGroupStatusCellIndex].ForeColor = System.Drawing.Color.Red;
                        break;
                    case "completed":
                        objOrgSetGrid.Rows[iCount].Cells[iGroupStatusCellIndex].ForeColor = System.Drawing.Color.Green;
                        break;
                    default:
                        break;
                }
              
            }         
           
        }
        /// <summary>
        /// Get Grid Cell Index 
        /// </summary>
        /// <returns></returns>
        private int GetGridCellIndex(GridView p_grdGridView, string p_sHeaderText)
        {
            int iIndex = 0;

            for (int iHeaderCount = 0; iHeaderCount < p_grdGridView.HeaderRow.Cells.Count; iHeaderCount++)
            {
                if (p_grdGridView.HeaderRow.Cells[iHeaderCount].Text.ToLower() == p_sHeaderText.ToLower())
                {
                    iIndex = iHeaderCount;
                    break;
                }
            }

            return iIndex;
        }
    }
}
