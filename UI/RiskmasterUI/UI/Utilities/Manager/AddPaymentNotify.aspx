﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPaymentNotify.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.AddPaymentNotify" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payment Notification Setup</title>
      <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="javascript" type="text/javascript" src="../../../Scripts/checkoptions.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
    <script language="javascript" type="text/javascript" src="../../../Scripts/grid.js"></script>
    <script language="javascript" type="text/javascript" src="../../../Scripts/AddPaymentNotify.js"></script>
</head>
<body class="10pt" onload="CopyGridRowDataToPopup();">
  <form id="frmData" runat="server">
  <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">
  <div class="toolbardrift" id="toolbardrift" name="toolbardrift">
    <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
          <asp:TextBox Style="display: none" runat="server" ID="RecordId" />
          <asp:TextBox Style="display: none" runat="server" ID="txtData" />
          <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
          <asp:TextBox Style="display: none" runat="server" ID="mode" />
          <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
          <asp:TextBox Style="display: none" runat="server" ID="gridname" />
          <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
          <asp:TextBox Style="display: none" runat="server" ID="FormMode" />
           <asp:TextBox Style="display: none" runat="server" ID="txtComboType" Text="combotext" />

        <asp:ImageButton id="btnOk" OnClientClick=" return Check();" 
               onmouseover="javascript:document.all.btnOk.src='/RiskmasterUI/Images/save2.gif'" 
               onmouseout="javascript:document.all.btnOk.src='/RiskmasterUI/Images/save.gif'" 
               ImageUrl="~/Images/save.gif" class="bold" ToolTip="Save" runat="server" onclick="btnOk_Click" 
               />
      </td>
      </tr>
     </tbody>
    </table>
   </div>
   <br><br><%--<div class="msgheader" id="formtitle">Payment Notification Setup</div>--%><%--zalam 04/21/2009--%>
   <div class="errtextheader"></div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colSpan="2">Payment Notification Setup</td>
         </tr>
         <tr>
          <td>Line of Business:&nbsp;&nbsp;</td>
          <td>
              <asp:DropDownList ID="LBusiness" type="combobox" rmxref="/Instance/Document/AddPaymentNotify/control[@name='LOB']/@value" ItemSetRef="/Instance/Document/AddPaymentNotify/control[@name='LOB']"  onchange="setDataChanged(true);" runat="server">
              </asp:DropDownList>
          </td>
         </tr>
         <tr>
          <td>User To Notify:&nbsp;&nbsp;</td>
          <td>
              <asp:DropDownList ID="UNotify" type="combobox" onchange="setDataChanged(true);"  rmxref="/Instance/Document/AddPaymentNotify/control[@name='UserNotify']/@value" ItemSetRef="/Instance/Document/AddPaymentNotify/control[@name='UserNotify']"  runat="server">
              </asp:DropDownList>
          </td>
         </tr>
         <tr>
          <td>Notify Period:&nbsp;&nbsp;</td>
          <td>
         
              <asp:DropDownList ID="NPeriod" rmxref="/Instance/Document/AddPaymentNotify/control[@name='NotifyPeriod']" onchange="setDataChanged(true);" runat="server">
                <asp:ListItem Value="0">Every Log In</asp:ListItem>
                <asp:ListItem Value="1">Monday</asp:ListItem>
                <asp:ListItem Value="2">Tuesday</asp:ListItem>
                <asp:ListItem Value="3">Wednesday</asp:ListItem>
                <asp:ListItem Value="4">Thursday</asp:ListItem>
                <asp:ListItem Value="5">Friday</asp:ListItem>
               </asp:DropDownList>
         </td>
         </tr>
         <tr>
          <td>Date Criteria:&nbsp;&nbsp;</td>
          <td>
              <asp:DropDownList ID="DCriteria" rmxref="/Instance/Document/AddPaymentNotify/control[@name='DateCriteria']" onchange="setDataChanged(true);" runat="server">
                <asp:ListItem Value="10">less/equal to current date</asp:ListItem>
                <asp:ListItem Value="20">by end of week</asp:ListItem>
                <asp:ListItem Value="30">by end of month</asp:ListItem>
              </asp:DropDownList>
         </td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td></td>
         </tr>
        </tbody>
       </table>
       <input type="text" name="" value="" id="SysViewType" style="display:none"/>
       <input type="text" name="" value="" id="SysCmd" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
       <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
       <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
       <input type="text" name="" value="" id="SysFormIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""/>
       <input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="PaymentNotify"/>
       <input type="text" name="" value="" id="SysRequired" style="display:none"/>
       </td>
      <td valign="top"></td>
     </tr>
    </tbody>
   </table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
   <input type="hidden" value="rmx-widget-handle-3" id="SysWindowId" />
   </form>
 </body>
</html>
