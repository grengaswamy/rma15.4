﻿<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA  | Programmer | Description                                       *
 **********************************************************************************************
 * 05/20/2014 | 33573       | dagrawal   | Text changes for some settings
 * 03/14/2014 | 35039       | pgupta93   | Changes req for search code description on lookup
 * 7/24/2014  | RMA-718     | ajohari2   | Changes for TPA Access ON/OFF
 **********************************************************************************************/--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralSystemParameterSetup.aspx.cs"
    ValidateRequest="false" Inherits="Riskmaster.UI.Utilities.Manager.GeneralSystemParameterSetup" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="usc" %>
<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="ucmc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>System Setup Parameters</title>
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/cul.js"></script>
    <script src="../../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
	
    <script type="text/javascript">

        //mbahl3 strataware Usernhancement

        function Enablesingleusersetting(objCtrl) {
        
                if (objCtrl.id == "StrataWare") {
                if (objCtrl.checked) {
                    
                    document.getElementById("EnableSingleuser").disabled = false;
                   
                  

                }
                else {
                    document.getElementById("EnableSingleuser").disabled = true;
                    document.getElementById("cmdAddCustomizedListUser").disabled = true;
                    document.getElementById("txtUser").disabled = true;
                    document.getElementById("txtUser").readOnly = true;
                    // mbahl3 mits 34901
                    if (document.getElementById("EnableSingleuser").checked)    
                    document.getElementById("EnableSingleuser").checked = false;
                    //mbahl3 mits 34901
                }
                }
                if (objCtrl.id == "EnableSingleuser") {
                    if (objCtrl.checked) {
                    
                        document.getElementById("cmdAddCustomizedListUser").disabled = false;
                        document.getElementById("txtUser").disabled = false;
                        document.getElementById("txtUser").readOnly = false;

                 

                    }
                    else {
                        document.getElementById("cmdAddCustomizedListUser").disabled = true;
                        document.getElementById("txtUser").disabled = true;
                        document.getElementById("txtUser").readOnly = true;
                        // mbahl3 mits 34901
                        document.getElementById("txtUser").value = "";
                        // mbahl3 mits 34901
                    }
                }
            
        }
        //mbahl3 strataware enhancement
        function OutlookFileSizeCheck(ctrl) {
            if (ctrl.value == '' || ctrl.value < 1) {
                alert("Minimum value required is 1.");
                ctrl.value = 1;
            }
            if (ctrl.value > 35) {
                alert("Maximum value allowed is 35.");
                ctrl.value = 35;
            }
        }
        function ConfirmVoidOfCleared() {
            var bAllowVoidCleared = document.getElementById('AllowVoidClearedPmts').checked;
            if (bAllowVoidCleared == false) {
                var confirmVoidOfCleared = confirm("Computer Sciences Corporation does not recommend the use of this option.  Do you still wish to use the option to void cleared Funds Transactions?");
                if (confirmVoidOfCleared == false) {
                    document.getElementById('AllowVoidClearedPmts').checked = false;
                }
                else {
                    document.getElementById('AllowVoidClearedPmts').checked = true;
                    setDataChanged(true); //Deb 06/21/2011: MITS 25265
                }
            }
        }
        function TellMinMaxAndSet1(ctrl)//Parijat 19713
        {
 		//rsharma220 MITS 28264, 28065
            if (ctrl.value == '' || ctrl.value < 10) {
                alert("Minimum value required is 10.");
                ctrl.value = '10';
            }
            if (ctrl.value > 100) {
                alert("Maximum value allowed is 100.");
                ctrl.value = '100';
            }

        }
		


        function DisableControl() {
            var rmx = null;
				//mbahl3 strataware enhancement
            document.getElementById("cmdAddCustomizedListUser").disabled = false;
            document.getElementById("txtUser").disabled = false;
            document.getElementById("txtUser").readOnly = false;
                if (document.getElementById("StrataWare").checked) {
                  //  document.getElementById("StrataWare")
                    document.getElementById("EnableSingleuser").disabled = false;

                   // document.getElementById("txtUser").disabled = false;

                }
                else {
                    document.getElementById("EnableSingleuser").disabled = true;
                    document.getElementById("cmdAddCustomizedListUser").disabled = true;
                    document.getElementById("txtUser").disabled = true;
                    document.getElementById("txtUser").readOnly = true;
                }
            
            
                if (document.getElementById("EnableSingleuser").checked) {

                    document.getElementById("cmdAddCustomizedListUser").disabled = false;
                    document.getElementById("txtUser").disabled = false;
                    document.getElementById("txtUser").readOnly = false;



                }
                else {
                    document.getElementById("cmdAddCustomizedListUser").disabled = true;
                    document.getElementById("txtUser").disabled = true;
                    document.getElementById("txtUser").readOnly = true;
                }
            
			//mbahl3 strtaware enhancement
            rmx = document.getElementById("RMXLSSEnable");
            if (rmx != null && rmx.checked != true)
                document.getElementById("ShowLSSInvoice").disabled = true;;
            rmx = document.getElementById("AutoPopulateDpt");
            if (rmx != null) {
                if (rmx.checked != true) {
                    document.getElementById("AutoFillDpteid").disabled = true;
                    document.getElementById("AutoFillDpteidbtn").disabled = true;
                    document.getElementById('AutoFillDpteid').style.backgroundColor = "#F2F2F2"; //MITS 25124
                }
                else {
                    document.getElementById("AutoFillDpteid").disabled = false;
                    document.getElementById("AutoFillDpteidbtn").disabled = false;
                    document.getElementById('AutoFillDpteid').style.backgroundColor = ""; //MITS 25124
                }
            }
            //Added By nitika
            rmx = document.getElementById("UseTPA");
            if (rmx != null) {
                if (rmx.checked != true) {
                    document.getElementById("btnAvailableTPA").disabled = true;
                }
                else {
                    document.getElementById("btnAvailableTPA").disabled = false;
                }
            }            
            rmx = document.getElementById("UseMultiCurrency");
            if (rmx != null) {
                if (rmx.checked != true) {
                    document.getElementById("basecurrencytype_codelookupbtn").disabled = true;
                    document.getElementById("basecurrencytype_codelookup").disabled = true;
                    document.getElementById('basecurrencytype_codelookup').style.backgroundColor = "#F2F2F2"; //MITS 25124
                }
                else {
                    document.getElementById("basecurrencytype_codelookupbtn").disabled = false;
                    document.getElementById("basecurrencytype_codelookup").disabled = false;
                    document.getElementById('basecurrencytype_codelookup').style.backgroundColor = ""; //MITS 25124
                }
            }
            //igupta 3
            if (document.getElementById('chkCurrentDateForDiaries').checked) {
                document.getElementById('chkNotifyAssignerOfCompletion').disabled = false;
            }
            else {
                document.getElementById('chkNotifyAssignerOfCompletion').disabled = true;
            }

            //tanwar2 - ImageRight - start
            var UseImgRight = document.getElementById('UseImgRight');
            var UseImgRightWS = document.getElementById('UseImgRighWebService');
            if (UseImgRight && UseImgRightWS) {
                if (!UseImgRight.checked) {
                    UseImgRightWS.checked = false;
                    UseImgRightWS.disabled = true;
                }
            }
            //tanwar2 - ImageRight - end
        }
        //Added by Amitosh for R8 enhancement of Policy Interface
        function toggleUsePolicyInerface(objCtrl) {
            if (objCtrl.id == "UsePolicyInterface") {
                if (objCtrl.checked) {
                    //                    document.getElementById("UsePolicyInterface1").disabled = false;
                    //                    document.getElementById("UsePolicyInterface2").disabled = false;
                    alert(parent.CommonValidations.PolicyInterfaceSettingAlert);
                    document.getElementById("PolicyType1").disabled = false;
                    document.getElementById("PolicyType2").disabled = false;
                    document.getElementById("AllowPolicySearch").disabled = false;    //averma62 MITS 25163- Policy Interface Implementation
                    document.getElementById("NoReqFieldsForPolicySearch").disabled = false; //neha goel-MITS#33414-11212013
                    document.getElementById("FetchRecOnPolicySearch").readOnly = "false"; //aaggarwal29 changes for Point Policy Interface
                    //document.getElementById("FetchRecOnPolicySearch").disabled = false;
                    //  document.getElementById("UploadSuppToPolicy").disabled = false;
                    document.getElementById("UseCodeMapping").disabled = false;
                    document.getElementById("EnableTPAAccess").disabled = false; //JIRA RMA-718 ajohari2
                    document.getElementById("AllowAutoLogin").disabled = false;     //JIRA 5504
					//Added By Nitika for FNOL reserve 
                    document.getElementById("FNOLReserve").disabled = false;
                   // Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
                    document.getElementById("UploaddCheckTotal").disabled = false;

                }
                else {
                    //  document.getElementById("UsePolicyInterface1").disabled = true;
                    // document.getElementById("UsePolicyInterface2").disabled = true;
                    document.getElementById("PolicyType1").disabled = true;
                    document.getElementById("PolicyType2").disabled = true;
                    document.getElementById("AllowPolicySearch").disabled = true;   //averma62 MITS 25163- Policy Interface Implementation
                    document.getElementById("NoReqFieldsForPolicySearch").disabled = true; //neha goel-MITS#33414-11212013
                    document.getElementById("FetchRecOnPolicySearch").readOnly = "true";  //aaggarwal29 changes for Point Policy Interface
                    //  document.getElementById("UploadSuppToPolicy").disabled = true;
                    document.getElementById("UseCodeMapping").disabled = true;
                    document.getElementById("EnableTPAAccess").disabled = true; //JIRA RMA-718 ajohari2
                    document.getElementById("AllowAutoLogin").disabled = true;     //JIRA 5504
					//Added By Nitika for FNOL reserve 
                    document.getElementById("FNOLReserve").disabled = true;
                    // Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
                    document.getElementById("UploaddCheckTotal").disabled = true;
                }
            }
        }
		//Added By nitika for Samsung TPA
        function toggleUseTPAImport(objCtrl) {
            if (objCtrl.id == "UseTPA") {
                if (objCtrl.checked) {
                    document.getElementById("btnAvailableTPA").disabled = false;
                }
                else {
                    document.getElementById("btnAvailableTPA").disabled = true;
                }
            }
        }
        function openWindow(objImport) {

            //if-else if condition added by swati
            if (objImport == 'TPA') {
                window.open('/RiskmasterUI/UI/Utilities/Manager/AvailableTPA.aspx', 'open_window',
                'width=500,height=250' + ',top=' + (screen.availHeight - 250) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
                //window.open('AvailableTPA.aspx', 'open_window', ' width=400, height=250, left=0, top=0');
            }
            else if (objImport == 'Process') {
                window.open('/RiskmasterUI/UI/Utilities/Manager/AvailableProcessTypes.aspx', 'open_window',
                'width=500,height=250' + ',top=' + (screen.availHeight - 250) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            }
        }

        //Added By nitika for Samsung TPA     
        //skhare7 RMSC merge
        function clearOrgEntity(objCtrl) {
            if (document.getElementById("OrgEntlastfirstname") != null)
                document.getElementById("OrgEntlastfirstname").value = '';
            if (document.getElementById("OrgEntentityid") != null)
                document.getElementById("OrgEntentityid").value = '';

        }
        //nsachdeva2 - 7/9/2012 - PolicySave
        function ConfirmAllowPolicySearch() {
            var bAllowPolicySearch = document.getElementById('AllowPolicySearch').checked;
            if (bAllowPolicySearch == false) {
                var confirmPolicySearch = confirm("Computer Sciences Corporation does not recommend to use two policy systems at same time, Are you sure want to enable that?");
                if (confirmPolicySearch == true) {
                    document.getElementById('AllowPolicySearch').checked = true;
                    //neha goel--MITS#33414--11202013
                    document.getElementById('NoReqFieldsForPolicySearch').disabled = false;
                    setDataChanged(true);
                }
                else {
                    document.getElementById('AllowPolicySearch').checked = false;
                    document.getElementById('NoReqFieldsForPolicySearch').disabled = true; //neha goel--MITS#33414--11202013
                }
                //MGaba2:MITS 29111         
                rmx = document.getElementById("BillReviewFee");
                if (rmx != null) {
                    if (rmx.checked != true) {
                        document.getElementById("OrgBillRevFee").disabled = true;
                        document.getElementById("OrgEntlastfirstnamebtn").disabled = true;
                        document.getElementById("OrgEntlastfirstname").value = '';
                        document.getElementById("OrgEntlastfirstname").readOnly = true;
                        document.getElementById("OrgEntentityid").value = '';
                    }
                    else {
                        document.forms[0].OrgBillRevFee.disabled = false;
                        document.forms[0].OrgEntlastfirstnamebtn.disabled = false;
                        document.forms[0].OrgEntlastfirstname.readOnly = false;
                    }
                }
            }
        }
        //aaggarwal29 - 23/07/2012 - Fetch policy records check for Point Policy Interface
        function CheckPolicyRecordCount(objCtrl) {
            if (isNaN(parseInt(objCtrl.value)) || parseInt(objCtrl.value) < 1) {
                alert("Please enter a non zero postive value. Min=1, Max=999 ");
                objCtrl.value = '30';
            }
            else
                objCtrl.value = parseInt(objCtrl.value);
        }
        //igupta3
        function EnableNotifyAssigner()
        {
            if (document.getElementById('chkCurrentDateForDiaries').checked) {
                document.getElementById('chkNotifyAssignerOfCompletion').disabled = false;
            }
            else {
                document.getElementById('chkNotifyAssignerOfCompletion').disabled = true;
                document.getElementById('chkNotifyAssignerOfCompletion').checked = false;
            }
        }
        //Ankit Start : Worked on MITS - 32386
        function SetFASControlProp(objIsLoaded) {
            if (document.getElementById('chkFASEnable') != null && document.getElementById('chkFASEnable').checked) {
                var objUserSelection = document.getElementById('txtFileLocationSelection');
                var objRDBNone = document.getElementById('rdNone');
                var objRDBFTP = document.getElementById('rdFTP');
                var objRDBShared = document.getElementById('rdShared');

                if (objIsLoaded && objUserSelection != null) {
                    switch (objUserSelection.value) {
                        case 'F':
                            if (objRDBFTP != null)
                                objRDBFTP.checked = true;
                            break;
                        case 'S':
                            if (objRDBShared != null)
                                objRDBShared.checked = true;
                            break;
                        default:
                            if (objRDBNone != null)
                                objRDBNone.checked = true;
                    }
                }

                if (objRDBNone != null && objRDBNone.checked) {
                    SetFASControlsProp('N');
                }
                else if (objRDBFTP != null && objRDBFTP.checked) {
                    SetFASControlsProp('F');
                }
                else if (objRDBShared != null && objRDBShared.checked) {
                    SetFASControlsProp('S');
                }
            }
            else
                SetFASControlsProp('C')
        }
        function SetFASControlsProp(type) {
            if (document.getElementById("trFileLocation") != null)
                document.getElementById("trFileLocation").style.display = (type == 'C') ? 'none' : '';
            if (type == 'C')
                type = 'N';         //Do not want to show any thing if FAS Enable checkbox is not checked.

            if (document.getElementById('txtFileLocationSelection') != null)
                document.getElementById('txtFileLocationSelection').value = type;
            if (document.getElementById("trShared") != null)
                document.getElementById("trShared").style.display = (type == 'N' || type == 'F') ? 'none' : '';
            if (document.getElementById("trFTP1") != null)
                document.getElementById("trFTP1").style.display = (type == 'N' || type == 'S') ? 'none' : '';
            if (document.getElementById("trFTP2") != null)
                document.getElementById("trFTP2").style.display = (type == 'N' || type == 'S') ? 'none' : '';
            if (document.getElementById("trFTP3") != null)
                document.getElementById("trFTP3").style.display = (type == 'N' || type == 'S') ? 'none' : '';
            if (document.getElementById("trFTP4") != null)
                document.getElementById("trFTP4").style.display = (type == 'N' || type == 'S') ? 'none' : '';
        }
        //Ankit End
		//Mits 36708
        function DefaultEntryCreate(ctrl) {
            if (ctrl.value == '' || ctrl.value < null) {
                ctrl.value = '0';
            }
        }
    </script>
</head>

<%-- Changed by Saurabh Arora for MITS	20258:Start --%>
<%-- <body class="" onload="SystemParameter_Load();loadTabList();parent.MDIScreenLoaded();StateEnable();" style="height:90%"> --%>
<body class="" onload="SystemParameter_Load();loadTabList();parent.MDIScreenLoaded();SetRB();EnableMultipleReInsurer();DisableControl();CalculateEnhNotesTime();SetFASControlProp(true);"
    style="height: 90%">
    <%-- Changed by Saurabh Arora for MITS	20258:End --%>
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <input type="hidden" name="hTabName" />
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
            <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="middle" height="32"> 
                         <asp:ImageButton ID="Save" ImageUrl="~/Images/tb_save_active.png" class="bold" ToolTip="Save"
                            OnClientClick="return UtilitiesForm_Save();" runat="server" OnClick="Save_Click" />                      
                    </td>
                </tr>
            </table>
        </div>
        <div class="msgheader" id="formtitle">
            General System Parameter Setup
        </div>
        <%--<div class="errtextheader"></div>--%> <!-- aravi5 Jira Id: 7207 Starts -->
        <div class="tabGroup" id="TabsDivGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" name="TABSSystemSettings" id="TABSSystemSettings">
                <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="SystemSettings" id="LINKTABSSystemSettings">System Settings</a>
            </div>
            <div class="tabSpace" runat="server" id="Div1">
                &#160;
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSFundSettings" id="TABSFundSettings">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="FundSettings" id="LINKTABSFundSettings">Funds Settings</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSFundSettings">
                &#160;
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSEmployee" id="TABSEmployee">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="Employee" id="LINKTABSEmployee">Employee Settings</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSEmployee">
                &#160;
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSDiaries" id="TABSDiaries">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="Diaries" id="LINKTABSDiaries">Diaries / Text Fields</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSDiaries">
                &#160;
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSAdvanceClaim" id="TABSAdvanceClaim">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="AdvanceClaim" id="LINKTABSAdvanceClaim">Carrier Claim Settings</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSAdvanceClaim">
                &#160;
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSGlobalization" id="TABSGlobalization">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="Globalization" id="LINKTABSGlobalization">Globalization Settings</a>
            </div>
            <div class="tabSpace" runat="server" id="TBSGlobalization">
                &#160;
            </div>
        </div><!-- aravi5 Jira Id: 7207 Ends -->
        <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 100%; height: inherit; overflow: auto;">
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABSystemSettings" id="FORMTABSystemSettings">
                <tr id="">
                    <asp:TextBox ID="RowId" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']"
                        value="1" Style="display: none" type="id" runat="server"></asp:TextBox>
                    <asp:TextBox ID="hdnOracleCaseInsensitive" Style="display: none" runat="server"></asp:TextBox>
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">General Parameters
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Event Number Prefix:
                    </td>
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="EvPrefix" size="3" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EvPrefix']"
                                type="text" onchange="setDataChanged(true);" onblur="TrimSpace();" MaxLength="3"
                                runat="server"></asp:TextBox>
                        </div>
                    </td>
                    <td></td>
                    <td>Org. Level for Event Caption:
                    </td>
                    <td>
                        <asp:DropDownList ID="EvCaptionLevel" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EvCaptionLevel']/@value"
                            onchange="setDataChanged(true);" runat="server">
                            <asp:ListItem Value="0">None</asp:ListItem>
                            <asp:ListItem Value="1005">Client</asp:ListItem>
                            <asp:ListItem Value="1006">Company</asp:ListItem>
                            <asp:ListItem Value="1007">Operation</asp:ListItem>
                            <asp:ListItem Value="1008">Region</asp:ListItem>
                            <asp:ListItem Value="1009">Division</asp:ListItem>
                            <asp:ListItem Value="1010">Location</asp:ListItem>
                            <asp:ListItem Value="1011">Facility</asp:ListItem>
                            <asp:ListItem Value="1012">Department</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="">
                    <td>Include Year In Event Number:
                    </td>
                    <td>
                        <asp:CheckBox ID="EvIncludeYearFlag" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EvIncludeYearFlag']"
                            runat="server" />
                    </td>
                    <td></td>
                    <td>Autoselect Policy on Claim:
                    </td>
                    <td>
                        <asp:CheckBox ID="AutoSelectPolicy" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AutoSelectPolicy']"
                            runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Autonumber Vehicle ID(VIN):
                    </td>
                    <td>
                        <asp:CheckBox ID="AutoNumberVeh" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AutoNumberVeh']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td>Limit SORTMASTER Directories:
                    </td>
                    <td>
                        <asp:CheckBox ID="ListSmDir" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ListSmDir']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Default Employment Dept. To Claim Dept. for NEW WC Claims:&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="DefaultDept" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DefaultDept']"
                            runat="server" />
                    </td>
                    <td></td>
                    <td>Don't Default Time Fields:
                    </td>
                    <td>
                        <asp:CheckBox ID="DefaultTime" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DefaultTime']"
                            onclick="DisableOnClick(this);setDataChanged(true);" value="True" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <%--Sumit - 09/22/2009 MITS# Moved to Line of Business Parameters Setup screen--%>
                    <%--            <td>Use Policy Management System:</td>
            <td>
                <asp:CheckBox ID="UseEnhPol" onchange="setDataChanged(true);"  onclick="setDataChanged(true);SystemParameter_UseEnhPol();DisableOnClick(this);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseEnhPol']" value="True" type="checkbox" runat="server" />
               </td>
            <td></td>--%>
                    <td>Use Billing System:
                    </td>
                    <td>
                        <asp:CheckBox ID="BillingFlag" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='BillingFlag']"
                            value="True" runat="server" />
                    </td>
                     <%--< Start - averma62>--%>
                    <td></td>
                     <td>Enable VSS:
                                </td>
                                <td>
                                    <asp:CheckBox ID="EnableVSS" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='EnableVSS']"
                                        onchange="" onclick="setDataChanged(true);"
                                        value="True" type="checkbox" runat="server" />
                                </td>
                    <%--  < End averma62>--%>
                </tr>
                <tr id="">
                    <td>Use Case Management:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseCaseMgmt" onclick="setDataChanged(true);;DisableOnClick(this);return SystemParameter_UseCaseMgt();"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseCaseMgmt']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Multiple Insurers for Policy:
                    </td>
                    <td>
                        <%-- Changed by Saurabh Arora for MITS	20258:Start --%>
                        <%-- <asp:CheckBox ID="MultipleInsurer" type="checkbox"  onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='MultipleInsurer']" value="True" runat="server" />  --%>
                        <asp:CheckBox ID="MultipleInsurer" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);EnableMultipleReInsurer();"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name='MultipleInsurer']"
                            value="True" runat="server" />
                        <%-- Changed by Saurabh Arora for MITS	20258:End --%>
                    </td>
                </tr>
                <tr id="">
                    <td>Use MCM Interface:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseAcrosoftInterface" onclick="SystemParameter_UseAcrosoftInterface();setDataChanged(true);DisableOnClick(this);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseAcrosoftInterface']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Multiple Re-Insurers for Policy:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkMultipleReInsurer" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='MultipleReInsurer']"
                            runat="server" />
                    </td>
                </tr>
                <!-- Mona:PaperVisionMerge : Animesh Inserted -->
                <tr id="Tr1">
                    <td>Use PaperVision Document System:
                    </td>
                    <td>
                        <asp:CheckBox ID="UsePaperVisionInterface" onclick="setDataChanged(true);;DisableOnClick(this);return SystemParameter_UsePaperVision();"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UsePaperVisionInterface']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Use Media View Interface:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseMediaViewInterface" onclick="setDataChanged(true);;DisableOnClick(this);return SystemParameter_UseMediaViewInterface();"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseMediaViewInterface']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>

                <%--tanwar2 - ImageRight Interface - start--%>
                <tr id="Tr8" style="display:none">
                    <td>Use Image Right Interface:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseImgRight" onclick="setDataChanged(true);DisableOnClick(this);SystemParameter_UseImageRightInterface();"
                            rmxref="/Instance/Document/form//SystemSettings/group/displaycolumn/systemcontrol[@name ='UseImgRight']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>List ImageRight files in rmA (requires webservice):
                    </td>
                    <td>
                        <asp:CheckBox ID="UseImgRighWebService" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form//SystemSettings/group/displaycolumn/systemcontrol[@name='UseImgRightWebService']"
                            runat="server" />
                    </td>
                </tr>
                <%--tanwar2 - ImageRight Interface - end--%>

                <!-- Animesh Insertion Ends -->
                <tr id="">
                    <td>Use Script Editor:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseScriptEditor" onclick="SystemParameter_UseScriptEditor();setDataChanged(true);DisableOnClick(this);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseScriptEditor']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Broker Firm in BES:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkBrokerFrmBes" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='UseBrokerBES']"
                            runat="server" />
                    </td>
                </tr>                

                <tr id="">
                     <%--YUkti, DT:02/22/2014, MITS 35456--%>
                    <%--<td>Program Type In policy Tracking:--%>
                    <td style="visibility: hidden">Program Type In policy Tracking:
                    </td>
                    <td>
                        <%--<asp:CheckBox ID="ChkProgramTypePolicyTrkng" onclick="SystemParameter_UseAcrosoftInterface();setDataChanged(true);DisableOnClick(this);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UsePrgrmType']"
                            value="True" type="checkbox" runat="server" />--%>
                        <asp:CheckBox ID="ChkProgramTypePolicyTrkng" onclick="SystemParameter_UseAcrosoftInterface();setDataChanged(true);DisableOnClick(this);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UsePrgrmType']"
                            value="False" type="checkbox" runat="server" visible ="False"/>
                    <%--Yukti, DT:02/22/2014, MITS 35456--%>
                    </td>
                    <td></td>
                    <td>Include Collections in Coverages:
                    </td>
                    <td>
                        <asp:CheckBox ID="PolicyDropDown" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PolicyDropDown']"
                            runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Use 1st/3rd Party Claimant Processing:
                    </td>
                    <td>
                        <asp:CheckBox ID="ClaimantProcFlag" onclick="ShowConfigButton();setDataChanged(true);DisableOnClick(this);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ClaimantProcFlag']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td id="dOracleCaseInsensitive">Oracle Case Insensitive:
                    </td>
                    <td id="dOracleCaseInsensitiveCheckBox">
                        <asp:CheckBox ID="OracleCaseIns" value="True" onclick="DisableOnClick(this);setDataChanged(true);"
                            type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='OracleCaseIns']"
                            runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td></td>
                    <td>
                        <input type="button" id="Button1" class="button" value="Configure..." onclick="return SystemParameter_CP();"
                            runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>BRS 2nd Fee Table:
                    </td>
                    <td>
                        <asp:CheckBox ID="BRS2FeeTbl" value="True" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='BRS2FeeTbl']"
                            type="checkbox" runat="server" />
                    </td>
                    <!--Navdeep (Chubb ACK) : START-->
                    <td></td>
                    <td>Acknowledgement/Closed Claim Letter:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkClmLetter" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ClaimLetterFlag']"
                            runat="server" />
                    </td>
                    <!--Navdeep (Chubb ACK) : End-->
                    <!--Mridul 10/26/09 MITS#18230-->
                    <!--Comment by Kuladeep 04/21/2010 :Start-->
                    <%--<td></td>
            <td>Autonumber Property ID (PIN):</td>
            <td>
                <asp:CheckBox ID="chkAutoPIN" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AutoNumberProp']" type="checkbox" value="True" runat="server" />
            </td>--%>
                    <!--Comment by Kuladeep 04/21/2010 :End-->
                </tr>
                <tr id="">
                    <td>Use FL WC Max Rate:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseFlWCMaxRate" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseFlWCMaxRate']"
                            value="True" runat="server" />
                    </td>
                    <!--Navdeep For Chubb AUTO FROI ACORD : START-->
                    <td></td>
                    <td>Auto FROI ACORD:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAutoFroiAcord" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='AutoFROIACORDFlag']"
                            runat="server" />
                    </td>
                    <!--Navdeep For Chubb AUTO FROI ACORD : End-->
                </tr>
                <!--Gagan Safeway Retrofit Policy Jursidiction : START-->
                <tr>
                    <td>Auto Assign Adjuster:
                    </td>
                    <td id="GroupParentRB4">
                        <asp:CheckBox ID="AutoAssignAdj" onclick="setDataChanged(true);DisableOnClick(this);EnableDisableRB(this.id);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseAutoAdj']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <!--Start Debabrata Biswas Entity payment Approval R6 retrofit MITS # 19715 18/02/2010-->
                    <td></td>
                    <td>Use Entity Payment Approval:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseEntityApproval" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseEntityApproval']"
                            runat="server" />
                    </td>
                    <!--End Debabrata Biswas Entity payment Approval R6 retrofit MITS # 19715 18/02/2010-->
                </tr>
                <tr id="AutoAssignAdjDateTimeMethod">
                    <td colspan="2">
                        <asp:RadioButton ID="AutoAssignAdj1" type="radio" value="1" rmxref="/Instance/Document/form/group/RBAutoAssignAdj"
                            onclick="setDataChanged(true);" runat="server" GroupName="RB4" />Date/Time Assigned
                    </td>
                    <td></td>
                    
                </tr>
                <tr id="AutoAssignAdjWorkItemsMethod">
                    <td colspan="2">
                        <asp:RadioButton ID="AutoAssignAdj2" Enabled="true" value="2" rmxref="/Instance/Document/form/group/RBAutoAssignAdj"
                            type="radio" onclick="setDataChanged(true);" GroupName="RB4" runat="server" />Work
                    Items
                    </td>
                </tr>
                <!-- Animesh inserted For CPE MITS 18735 Auto Assign Adjuster-->
                <tr>
                    <td>Show Jurisdiction in Policy Tracking:
                    </td>
                    <td id="GroupParentRB0">
                        <asp:CheckBox ID="ShowJurisdiction" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);EnableDisableRB(this.id);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ShowJurisdiction']"
                            value="True" runat="server" />
                    </td>
                    <!-- Animesh Insertion ends -->
                    <!--Add for Change location by Kuladeep 04/21/2010: START-->
                    <td></td>
                    <td>Autonumber Property ID (PIN):
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAutoPIN" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AutoNumPin']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <!--Add for Change location by Kuladeep 04/21/2010: END-->
                </tr>
                <tr id="StateId">
                    <td colspan="2">
                        <asp:RadioButton ID="ShowJurisdiction1" type="radio" value="0" rmxref="/Instance/Document/form/group/State"
                            onclick="setDataChanged(true);" runat="server" GroupName="RB0" />Event State
                    </td>
                </tr>
                <tr id="JurisId">
                    <td colspan="2">
                        <asp:RadioButton ID="ShowJurisdiction2" Enabled="true" rmxref="/Instance/Document/form/group/State"
                            type="radio" value="1" onclick="setDataChanged(true);" GroupName="RB0" runat="server" />Jurisdiction
                    </td>
                </tr>
                <tr>
                    <td>Include Physician Med. and Employee in Physician Search:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkPhysicianSearch" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='PhysicianSearchFlag']"
                            runat="server" />
                    </td>
                    <td></td>
                    <!--R7 Time Zone Setting START-->
                    <td>Org. Level for Time Zone validation:
                    </td>
                    <td>
                        <asp:DropDownList ID="OrgTimeZoneLevel" type="combobox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='OrgTimeZoneLevel']/@value"
                            onchange="setDataChanged(true);" runat="server">
                            <asp:ListItem Value="0">None</asp:ListItem>
                            <asp:ListItem Value="1005">Client</asp:ListItem>
                            <asp:ListItem Value="1006">Company</asp:ListItem>
                            <asp:ListItem Value="1007">Operation</asp:ListItem>
                            <asp:ListItem Value="1008">Region</asp:ListItem>
                            <asp:ListItem Value="1009">Division</asp:ListItem>
                            <asp:ListItem Value="1010">Location</asp:ListItem>
                            <asp:ListItem Value="1011">Facility</asp:ListItem>
                            <asp:ListItem Value="1012">Department</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <!--R7 Time Zone Setting END-->
                </tr>
                <tr id="TRMulAddreses">
                    <!--Added For R7:Add Emp Data Elements-->
                    <td>Enable Multiple Addresses for Entities:
                    </td>
                    <td>
                        <asp:CheckBox ID="MulAddresses" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);EnableDisableAddressSetting(this.id)"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseMultipleAddresses']"
                            value="True" runat="server" />
                    </td>
                    <td></td>
                    <!--Added For R7:Add Emp Data Elements-->
                    <!--Shivendu for Use Outlook seting : START-->
                    <td>Use Outlook To Send Emails:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseOutlook" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseOutlook']"
                            runat="server" />
                    </td>
                    <!--Shivendu for Use Outlook seting : End-->
                </tr>
                <tr id="TRAddressAddUpdate">
                    <%--RMA-8753 nshah28,aahuja21 start--%>
                    <td>Enable Add/Modify Addresses</td>
                    <td>
                        <asp:CheckBox id="AddModifyAddress" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddModifyAddress']"  value="True" type="checkbox" runat="server"></asp:CheckBox>
                    </td>
                    <%--RMA-8753 nshah28 end--%>
                </tr>
                <tr>
                    <!--START (06/29/2010): Sumit -Filter Policy based on LOB  -->
                    <td>Filter Policy based on LOB:
                    </td>
                    <td>
                        <asp:CheckBox ID="UsePolicyLOBFilter" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UsePolicyLOBFilter']"
                            runat="server" />
                    </td>
                    <td></td>
                    <!--End: Sumit-->
                    <!--START (07/02/2010): Sumit - BRS Activation Flag  -->
                    <td>Use Bill Review System (BRS):
                    </td>
                    <td>
                        <asp:CheckBox ID="UseBRS" onclick="SystemParameter_UseBRS();DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseBRS']"
                            runat="server" />
                    </td>
                </tr>
                <!--End: Sumit-->
                <!--START (07/02/2010): Sumit - General Claim Activation Flag  -->
                <tr>
                    <td>Use General Claim:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseGCLOB" onclick="SystemParameter_UseGCLOB();DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseGCLOB']"
                            runat="server" />
                    </td>
                    <!--End: Sumit-->
                    <td></td>
                    <!--START (07/02/2010): Sumit - Non-occupational Claim Activation Flag  -->
                    <td>Use Non-occupational Claim:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseDILOB" onclick="SystemParameter_UseDILOB();DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseDILOB']"
                            runat="server" />
                    </td>
                </tr>
                <!--End: Sumit-->
                <!--START (07/02/2010): Sumit - Property Claim Activation Flag  -->
                <tr>
                    <td>Use Property Claim:
                    </td>
                    <td>
                        <asp:CheckBox ID="UsePCLOB" onclick="SystemParameter_UsePCLOB();DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UsePCLOB']"
                            runat="server" />
                    </td>
                    <!--End: Sumit-->
                    <td></td>
                    <!--START (07/02/2010): Sumit - Vehicle Accident Activation Flag  -->
                    <td>Use Vehicle Accident Claim:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseVALOB" onclick="SystemParameter_UseVALOB();DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseVALOB']"
                            runat="server" />
                    </td>
                </tr>
                <!--End: Sumit-->
                <!--START (07/02/2010): Sumit - Workers Comp. Activation Flag  -->
                <tr>
                    <td>Use Worker's Compensation Claim:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseWCLOB" onclick="SystemParameter_UseWCLOB();DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseWCLOB']"
                            runat="server" />
                    </td>
                    <td></td>
               <td>Add Adjuster as Person Involved:</td>
            <td>    
                <asp:CheckBox ID="chkAddAdjAsPI" 
                              onclick="setDataChanged(true);" 
                              value="false" type="checkbox" 
                              rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddAdjAsPI']" 
                              runat="server" />
            
            </td>
                </tr>
                <%--npadhy Start MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts--%>
                <tr>
                    <td>Maximum No of Discount Tiers for a State and LOB:
                    </td>
                    <td>
                        <asp:TextBox ID="MaxDiscTier" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='MaxDiscTier']"
                            onchange="setDataChanged(true);" onblur="numLostFocus(this)" type="text" runat="server"></asp:TextBox>
                    </td>
                    <td></td>
                    <td>Maximum No of Discounts Associated For a State and LOB:
                    </td>
                    <td>
                        <asp:TextBox ID="MaxDiscounts" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='MaxDiscounts']"
                            onchange="setDataChanged(true);" onblur="numLostFocus(this)" type="text" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <%--npadhy End MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts--%>

<%--spahariya Start MITS  08/05/2011 Added Settings for using PSO reporting system--%>
              <tr>
            <td>Use PSO Reporting System:</td>
                <td>
                    <asp:CheckBox ID="PSOReportingSys"  onclick="SystemParameter_UsePSOReporting();DisableOnClick(this);setDataChanged(true);"  rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UsePSOReportingSys']" value="True" type="checkbox" runat="server" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>Event Types Excluded in PSO Reporting System:</td>
			    <td>
				    <ucmc:MultiCode runat="server" ID="PSOEventTypes" width="200px" height="88px"  CodeTable="EVENT_TYPE" ControlName="PSOEventTypes"  RMXRef="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='PSOEventTypes']" RMXType="codelist"/>
			    </td>
                <td></td>
			</tr>
             <%--spahariya end MITS  08/05/2011 Added Settings for using PSO reporting system--%>
			 
                <tr>
                    <%--Added by Amitosh for mits 23476(05/11/2011)--%>
                    <td>Display Claimant Name in MDI:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseClaimantName" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseClaimantName']"
                            runat="server" />
                    </td>
                    <td></td>
                    <!--nsachdeva2: Mits: 26418 - Claim activity log -->
                    <td>Enable Activity Log:
                    </td>
                    <td>
                        <asp:CheckBox ID="EnableClaimActivityLog" onclick="setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='EnableClaimActivityLog']"
                            runat="server" />
                    </td>
                </tr>
                <!--End: Sumit-->
                <!-- start Add Claimant as Person Involved hlv MITS 29356 11/12/12 begin  -->
                <tr>
                    <td>Add Claimant as Person Involved:
                    </td>
                    <td >
                  
                        <asp:CheckBox ID="AddClaimantPI" onclick="setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddClaimantPI']"
                            runat="server" />
                    </td>
                    <td></td>
                   <%-- MBAHL3 MITS  30224--%>
                    
                      <td>Filter Entity by Effective Date:
                    </td>
                    <td >
                      
                        <asp:CheckBox ID="StatusActive" onclick="setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='StatusActive']"
                            runat="server" />
                    </td>
                   <%--      MBAHL3 MITS 30224--%>
                </tr>
           <%--     mbahl3 strataware Enhancement --%>
                 <tr>
                    <td>Use StrataWare
                    </td>
                    <td >
                  
                        <asp:CheckBox ID="StrataWare" onclick="Enablesingleusersetting(this);setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='StrataWare']"
                            runat="server" />
                    </td>
                    <td></td>


                      <td>Use single user for StrataWare sign on
                    </td>
                    <td >
                      
                        <asp:CheckBox ID="EnableSingleuser" onclick="Enablesingleusersetting(this);setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='EnableSingleuser']"
                            runat="server" />
                    </td>

                     </tr>
                  <!--mmudabbir for MITS 36022-->
                <tr id="">                   
                    <td>Use Silverlight for Mail Merge (IE only):
                    </td>
                    <td>
                        <asp:CheckBox ID="UseSilverlight" onclick="setDataChanged(this);DisableOnClick(this);"
                            type="checkbox" value="True" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseSilverlight']"
                            runat="server" />
                    </td>
                </tr>

                <%--    look up for strataware --%>
                <tr>
                    <td>
                                User:&nbsp;&nbsp; <asp:TextBox  runat="server"  ID="txtUser" Text="" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='StratawareUserName']" onblur="if(document.getElementById('txtUser').value != '') AddCustomizedListUserLookup('GeneralSystemParameterSetup', 'txtUser', 'StratawareUserID', 'UserName')"/>
                                <input type="button" class="CodeLookupControl" value="" id="cmdAddCustomizedListUser" runat="server"
                                    onclick="AddCustomizedListUser('GeneralSystemParameterSetup', 'txtUser', 'StratawareUserID', 'UserName'); setDataChanged(true);" />
                             <%--   <input type="button" class="BtnRemove" value="" id="cmdDelCustomizedListUser" style="width: 21px"
                                    onclick="DelCustomizedListUser('GeneralSystemParameterSetup', 'txtUser', 'UserId', 'UserName')" />--%>
                               <asp:TextBox Style="display: none" runat="server"  ID="StratawareUserID"
                                    Text="" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='StratawareUserID']" rmxtype="hidden" />
                                <asp:TextBox Style="display: none" runat="server"  ID="UserName" Text="" rmxtype="hidden" />

                            </td>
                    <%--JIRA 340 skhare7 Entity Role --%>    
                    <td></td> 
                    <td></td>      
                    <td style="display:none">Use Entity Role</td>
                    <td style="display:none">                      
                        <asp:CheckBox ID="UseEntityRole" onclick="setDataChanged(true);" value="True"
                        type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseEntityRole']"
                        runat="server" />
                    </td>
                <%--JIRA 340 skhare7 Entity Role End--%>
                </tr>
        
                <%--    look up--%>

                 <tr id="Tr9">
                    <td>Consumer Service URL:
                    </td>
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="UseConsumerURL" type="text" onchange="setDataChanged(true);" value="https://b-cs.stratacare.net/StrataCareIdentityProvider/SAMLLogin.aspx"
                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StratawareConsumerUrl']"
                                runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>

                <tr id="Tr10">
                    <td>Service Provider URL:
                    </td>
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="UseServiceURL"  type="text" onchange="setDataChanged(true);" value="https://b-cs.stratacare.net/d-cs/default.aspx"
                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StratwareServiceUrl']"
                                runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <%-- MITS:35039 Serach Code Description START--%>
                <tr>
                    <td>Search on Short Code & Code Description:
                    </td>
                    <td>
                        <asp:checkbox id="chkSearchhCodeDes" onclick="setDataChanged(true);" value="True" type="checkbox"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='SearchCodeDescription']"
                            runat="server" />
                    </td>

                      <td></td>
                    <td>
                          Enable Insured Department (Claim Level) for Policy Verification:

                    </td>
                    <td>
                    <%--Added by Nitin goel for mits 33588(07/29/2014)--%>
                    <asp:CheckBox ID="UseInsuredClaimDept" 
                            onclick="DisableOnClick(this);setDataChanged(true);" 
                            value="True" type="checkbox" 
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseInsuredClaimDept']" 
                                     runat="server" />
                     <%--Added by Nitin goel for mits 33588(07/29/2014)--%>
                    </td>
                </tr>
                 <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                <tr>
                    <td>Auto Populate Claim Date Reported:
                    </td>
                    <td id="GroupParentRB6">
                        <asp:CheckBox ID="AutoAssignClaimReported" onclick="setDataChanged(true);DisableOnClick(this);EnableDisableRB(this.id);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseAutoClaimReported']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <!--Start Debabrata Biswas Entity payment Approval R6 retrofit MITS # 19715 18/02/2010-->
                    <td></td>
                </tr>
                <tr id="AutoAssignClaimReportedClaimDate">
                    <td colspan="2">
                        <asp:RadioButton ID="AutoAssignClaimReported1" type="radio" value="1" rmxref="/Instance/Document/form/group/RBAutoAssignClaimReported"
                            onclick="setDataChanged(true);" runat="server" GroupName="RB6" />Populate Claim Date Reported with Date of Claim
                    </td>
                    <td></td>
                    
                </tr>
                <tr id="AutoAssignClaimReportedSystemDate">
                    <td colspan="2">
                        <asp:RadioButton ID="AutoAssignClaimReported2" Enabled="true" value="2" rmxref="/Instance/Document/form/group/RBAutoAssignClaimReported"
                            type="radio" onclick="setDataChanged(true);" GroupName="RB6" runat="server" />Populate Claim Date Reported with Date of System
                    </td>
                </tr>
                <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                 <%-- MITS:35039 Serach Code Description END--%>
                     <%-- mbahl3 strataware Enhancement --%>
                <!-- start Add Claimant as Person Involved hlv MITS 29356 11/12/12 end  -->
                <!--Gagan Safeway Retrofit Policy Jursidiction : END-->
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Financial History Evaluation Date
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td colspan="2">
                        <asp:RadioButton ID="FinHis" type="radio" value="0" rmxref="/Instance/Document/form/group/FinHis"
                            onclick="setDataChanged(true);" runat="server" GroupName="RB" />Transaction
                    Date
                    </td>
                    <td colspan="2">
                        <asp:RadioButton ID="FinHis1" rmxref="/Instance/Document/form/group/FinHis" type="radio"
                            value="1" onclick="setDataChanged(true);" GroupName="RB" runat="server" />Date
                    of Check
                    </td>
                </tr>
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">System Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>SMTP Mail Server:
                    </td>
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="SmtpSvr" size="20" value="" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SmtpSvr']"
                                type="text" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <!-- Start by Yatharth for OFAC check functionality -->
                <tr id="">
                    <td>OFAC Check for Payees and Entities
                    </td>
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <asp:CheckBox ID="DoOfacCheck" type="checkbox" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='DoOfacCheck']"
                                runat="server" />
                        </div>
                    </td>
                </tr>
                <!-- End by Yatharth for OFAC check functionality -->
                <tr id="">
                    <td>Patriot Protector Web Service URL:
                    </td>
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="P2Url" size="20" type="text" onchange="setDataChanged(true);" value="https://www.patriotprotector.com/PPWSPublic/PPWSPublic.asmx?WSDL"
                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='P2Url']"
                                runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr id="">
                    <td>Patriot Protector Web Service Login:
                    </td>
                    <td>
                        <input id="btnPpwcl" runat="server" type="button" class="button" onclick="return SystemParameter_P2();"
                            value="Modify..." />
                    </td>
                </tr>
                <tr id="">
                    <td></td>
                    <td>
                        <asp:TextBox ID="P2Login" Text="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='P2Login']"
                            type="text" runat="server" Style="display: none"></asp:TextBox>
                    </td>
                    <td></td>
                    <td>
                        <asp:TextBox ID="P2Pass" type="text" value="" Text="ha3RuTE$" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='P2Pass']"
                            Style="display: none" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr id="">
                    <td>Use Quality Management for Events:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseQualityMgt" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseQualityMgt']"
                            runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <!-- Size limit for Outlook added by Shivendu -->
                    <td>Size Limit(in MB) for Files that can be emailed using Outlook :
                    </td>
                    <td>
                        <asp:TextBox ID="FileSizeLimitForOutlook" ToolTip="Min : 1, Max : 35" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FileSizeLimitForOutlook']"
                            onchange="setDataChanged(true);" onblur="numLostFocus(this);OutlookFileSizeCheck(this);"
                            size="20" type="numeric" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <%-- Start averma62 MITS 32386--%>
                <tr>
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">&nbsp;Fraud Analytics Suite Settings</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>Use Fraud Analytics Suite (FAS):</td>
                    <td>
                         <asp:CheckBox ID="chkFASEnable" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='EnableFAS']"
                        onclick="setDataChanged(true);SetFASControlProp();"
                        value="True" type="checkbox" runat="server" />
                    </td>

                </tr>
                <tr id="trFileLocation">
                    <td>FAS XML File Location:</td>
                    <td>
                        <asp:RadioButton ID="rdNone"
                            type="radio" runat="server" Checked="true" onclick="setDataChanged(true);SetFASControlProp();" GroupName="FAS" />None
                        <asp:RadioButton ID="rdFTP"
                            type="radio" runat="server" onclick="setDataChanged(true);SetFASControlProp();" GroupName="FAS" />FTP
                        <asp:RadioButton ID="rdShared"
                            type="radio" runat="server" onclick="setDataChanged(true);SetFASControlProp();" GroupName="FAS" />Shared
                        
                        <asp:TextBox ID="txtFileLocationSelection" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FileLocationSelection']"
                            type="text" runat="server" Style="display: none" />
                    </td>
                </tr>
                <tr id="trShared">
                    <td>FAS Shared Location:</td>
                    <td>
                        <asp:TextBox ID="txtSharedLocation" size="20" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='SharedLocation']"
                        type="text" runat="server" />
                    </td>
                </tr>
                <tr id="trFTP1">
                    <td>FAS Server:</td>
                    <td>
                        <asp:TextBox ID="txtFASServer" size="20" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FASServer']"
                        type="text" runat="server" />
                    </td>
                </tr>
                   <tr id="trFTP2">
                    <td>FAS User Id:</td>
                    <td>
                        <asp:TextBox ID="txtFASUserId" size="20" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FASUserId']"
                        type="text" runat="server" />
                    </td>
                </tr>
                   <tr id="trFTP3">
                    <td>FAS Password:</td>
                    <td>
                        <asp:TextBox ID="txtFASPassword" size="20" TextMode="Password" type="text" onchange="setDataChanged(true);" 
                        rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FASPassword']"
                        runat="server" autocomplete="off"/>
                    </td>
                </tr>
                <tr id="trFTP4">
                    <td>FAS Folder:</td>
                    <td>
                        <asp:TextBox ID="txtFASFolder" size="20" type="text" onchange="setDataChanged(true);"
                        rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FASFolder']"
                        runat="server" />
                    </td>
                </tr>
                <%--Ankit End--%>
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Time and Expense Module
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Use Time and Expense:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseTnE" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseTnE']"
                            onclick="SystemParameter_CheckTnEClick();setDataChanged(true);DisableOnClick(this);"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Rate Org Level:
                    </td>
                    <td>
                        <asp:DropDownList ID="RateOrgLvl" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RateOrgLvl']/@value"
                            onchange="setDataChanged(true);" runat="server">
                            <asp:ListItem Value="0">None</asp:ListItem>
                            <asp:ListItem Value="1005">Client</asp:ListItem>
                            <asp:ListItem Value="1006">Company</asp:ListItem>
                            <asp:ListItem Value="1007">Operation</asp:ListItem>
                            <asp:ListItem Value="1008">Region</asp:ListItem>
                            <asp:ListItem Value="1009">Division</asp:ListItem>
                            <asp:ListItem Value="1010">Location</asp:ListItem>
                            <asp:ListItem Value="1011">Facility</asp:ListItem>
                            <asp:ListItem Value="1012">Department</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="">
                    <td>Bank Account:
                    </td>
                    <td>
                        <asp:DropDownList ID="BankAccount" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='BankAccount']/@value"
                            itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='BankAccount']"
                            onchange="setDataChanged(true);" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="">
                    <td>Show All Transaction Types for Invoice Detail:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseAllTransTypes" onclick="DisableOnClick(this);setDataChanged(true);"
                            type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseAllTransTypes']"
                            value="True" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Privacy Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Jurisdictions Implementing SSN Privacy (Comma separated State Codes):
                    </td>
                    <td>
                        <div title="Enter Comma Separated State Codes eg. MI,MN,CA..." style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="PrivacySettingsText" type="text" onchange="setDataChanged(true);"
                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PrivacySettingsText']"
                                ToolTip="Enter Comma Separated State Codes eg. MI,MN,CA..." value="" size="20"
                                runat="server"></asp:TextBox>
                        </div>
                    </td>
                    <td></td>
                    <td>Mask TaxId/SSN:
                    </td>
                    <!--Deb MITS 25775 -->
                    <td>
                        <asp:CheckBox ID="maskSSN" onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='maskSSN']"
                            value="false" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Number Of Records Per Page
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Code List:
                    </td>
                    <td>
                        <div title="Min : 10, Max : 10000, Recommended : 500" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="CodeList" ToolTip="Min : 10, Max : 10000, Recommended : 500" onchange="setDataChanged(true);"
                                onblur="numLostFocus(this);TellMinMaxAndSet(this)" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CodeList']"
                                type="text" value="10" Text="10" size="20" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr id="">
                    <!-- Enhanced notes added by Shivendu -->
                    <td>Search Results and Look Ups:
                    </td>
                    <td>
                        <div title="Min : 10, Max : 10000, Recommended : 500" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="ResultLkpUp" ToolTip="Min : 10, Max : 10000, Recommended : 500"
                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ResultLkpUp']"
                                onchange="setDataChanged(true);" onblur="numLostFocus(this);TellMinMaxAndSet(this);"
                                value="20" size="20" Text="20" type="text" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr id="Tr1">
                    <!-- Enhanced notes added by Parijat 19713 -->
                    <td>Enhanced Notes:
                    </td>
                    <td>
                        <div title="Min : 10, Max : 100, Recommended : 40" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="ResultEnhNot" ToolTip="Min : 10, Max : 100, Recommended : 40" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ResultEnhNot']"
                                onchange="setDataChanged(true);" onblur="numLostFocus(this);TellMinMaxAndSet1(this);"
                                value="20" size="20" Text="20" type="text" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr id="">
                    <td>Quick Look Ups:
                    </td>
                    <td>
                        <div title="Min : 10, Max : 100, Recommended : 40" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="ResultQuickLkpUp" ToolTip="Min : 10, Max : 10000, Recommended : 500"
                                rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='ResultQuickLkpUp']"
                                onchange="setDataChanged(true);" onblur="numLostFocus(this);TellMinMaxAndSet(this);"
                                value="20" size="20" Text="20" type="text" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <!--Setting for Records per Page on Payee Check Review Screen -->
                <tr id="">
                    <td>Payee Check Review:
                    </td>
                    <td>
                        <div title="Min : 10, Max : 150, Recommended : 100" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="ResultPayeeCheckReview" ToolTip="Min : 10, Max : 150, Recommended : 100"
                                rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='ResultPayeeCheckReview']"
                                onchange="setDataChanged(true);" onblur="numLostFocus(this);TellMinMaxAndSet(this);"
                                value="20" size="20" Text="20" type="text" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <!--Deb : MITS 25598, Setting for Records per Page on Document Attachment List -->
                <tr id="Tr4">
                    <td>Document Attachment List:
                    </td>
                    <td>
                        <div title="Min : 10, Max : 1000, Recommended : 20" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="txtDocList" ToolTip="Min : 10, Max : 1000, Recommended : 20" onchange="setDataChanged(true);"
                                onblur="numLostFocus(this);TellMinMaxAndSet(this)" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DocList']"
                                type="text" value="10" Text="10" size="20" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <!--Deb : MITS 25598, Setting for Records per Page on Document Attachment List -->
                <!--Ankit : MITS 29834, Setting for Records per Page on Payment History screen -->
                <tr id="Tr6">
                    <td>Payment history limit:</td>
                    <td>
                        <div title="Min : 10, Max : 1000, Recommended : 20" style="padding: 0px; margin: 0px">
                            <asp:TextBox ID="txtPayHistoryList" 
                                ToolTip="Min : 10, Max : 10000, Recommended : 20" 
                                onchange="setDataChanged(true);" 
                                onblur="numLostFocus(this);TellMinMaxAndSet(this)" 
                                rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PaymentHistory']" 
                                type="text" value="10" Text="10" size="20" runat="server"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <!--Ankit : MITS 29834, Setting for Records per Page on Payment History Screen -->
           <%--Ankit - MITS 27074 -Start changes for adding new field for setting records per page on Diary List Page--%>
           <tr id="">
            <td>Diary List:</td>
            <td>
             <div title="Min : 10, Max : 10000, Recommended : 500" style="padding: 0px; margin: 0px">
                 <asp:TextBox ID="txtDiaryList" ToolTip="Min : 10, Max : 10000, Recommended : 10" onchange="setDataChanged(true);" onblur="numLostFocus(this);TellMinMaxAndSet(this)" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DiaryList']" type="text" value="10" Text="10" size="20" runat="server"></asp:TextBox>
             </div>
            </td>
           </tr>
           <%--Ankit - End changes for adding new field for setting records per page on Diary List Page--%>

                <%-- <tr id="Tr5">
            <td colspan=5>
             <table width="100%" align="left">
              <tr>
               <td class="ctrlgroup" width="100%">Multi-Currency Settings</td>
              </tr>
             </table>
            </td>
           </tr>--%>
                <%-- <tr id="Tr6">
            <td>Base Currency:</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
                  <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="basecurrencytype" CodeTable="CURRENCY_TYPE" ControlName="basecurrencytype" RMXRef="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='BaseCurrencyType']" CodeFilter="" RMXType="code" />
             </div>
            </td>
           </tr>--%>
                <%--<tr id="Tr7">
            <td>Source Currency:</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
                  <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="sourcecurrencytype" CodeTable="CURRENCY_TYPE" ControlName="sourcecurrencytype" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='SourceCurrencyType']" CodeFilter="" RMXType="code" />
             </div>
            </td>
           </tr>
           <tr id="Tr8">
            <td>Exchange Rate:</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px">
                 <asp:TextBox ID="txtExchangeRate"  onchange="setDataChanged(true);"  rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ExchangeRate']" type="text" size="20" runat="server"></asp:TextBox>
             </div>
            </td>
           </tr>--%>
                <tr id="">
                    <td></td>
                    <td>
                        <asp:TextBox ID="CPUrl" type="text" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPUrl']"
                            Style="display: none" runat="server"></asp:TextBox>
                    </td>
                    <td></td>
                    <td>
                        <asp:TextBox ID="CPUserID" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPUserID']"
                            Style="display: none" type="text" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr id="">
                    <td></td>
                    <td>
                        <asp:TextBox ID="CPPass" type="text" Text="CSC" value="csc" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPPass']"
                            Style="display: none" runat="server"></asp:TextBox>
                    </td>
                </tr>
              <tr>
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">3rd Party Interface Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>Use TPA Import:
                    </td>
                    <td >
                        <asp:CheckBox ID="UseTPA" onclick="toggleUseTPAImport(this);setDataChanged(true);DisableOnClick(this);"  onchange=""
                            value="false" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseTPA']"
                            runat="server" />
                    </td>
                    <td></td>
                    <td>Use NMVTIS Reported Fields:
                    </td>
                    <td>
                        <asp:checkbox id="UseNMVTISReqFields" onclick="setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseNMVTISReqFields']"
                            runat="server" />
                    </td>
                </tr>

                 <tr>
                    <td>Available TPA(s):
                    </td>
                    <td >
                        <!--<input type="button" id="btnAvailableTPA" class="button" value="..." onclick="return openWindow();"-->
                         <input type="button" id="btnAvailableTPA" class="button" value="..." onclick="return openWindow('TPA');"
                            runat="server" />
                    </td>
                    <td></td>
                     <td  style="display:none">Use DCI Reporting:
                    </td>
                    <td  style="display:none">
                        <asp:checkbox id="Checkbox2" onclick="setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseDCIReportingFields']"
                            runat="server" />
                    </td>
                  </tr>
                <!-- End Use TPA Import Nitika -- Samsung TPA 25/03/2013 begin  -->
            </table>
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABFundSettings" id="FORMTABFundSettings"
                style="display: none;">
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Funds Parameters
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Use Account Owner for Funds:
                    </td>
                    <td>
                        <asp:CheckBox ID="AccOwnrFnds" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AccOwnrFnds']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td>Prevent Back-Dating of Reserves:
                    </td>
                    <td>
                        <asp:CheckBox ID="PrvntBckDtng" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PrvntBckDtng']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Use Sub Bank Accounts:
                    </td>
                    <td>
                        <asp:CheckBox ID="SubBankAcc" onclick="ToggleUseMasterBankAcc(this.id);DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SubBankAcc']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Prevent Modifying Reserves to Zero:
                    </td>
                    <td>
                        <asp:CheckBox ID="PrvntModfyngRes" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PrvntModfyngRes']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Assign Check Stock to Sub Accounts:
                    </td>
                    <td>
                        <asp:CheckBox ID="AssignSubAcc" onclick="DisableOnClick(this);setDataChanged(true);"
                            type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AssignSubAcc']"
                            value="" runat="server" />
                    </td>
                    <td></td>
                    <td>Prevent Modifying Reserves less than Paid:
                    </td>
                    <td>
                        <asp:CheckBox ID="PrvntBelowPaidRes" type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PrvntBelowPaidRes']"
                            value="" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Do Not Check Sub Account Deposit Balances:
                    </td>
                    <td>
                        <asp:CheckBox ID="AccDepositBal" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AccDepositBal']"
                            runat="server" />
                    </td>
                    <td></td>
                    <td>Don't Recalc Reserves on Load:
                    </td>
                    <td>
                        <asp:CheckBox ID="ReCalcResLoad" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ReCalcResLoad']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Filter 'Other People' from Funds Entity Search:
                    </td>
                    <td>
                        <asp:CheckBox type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FltrOthrPpl']"
                            ID="FltrOthrPpl" runat="server" />
                    </td>
                    <td></td>
                    <td>Use Master Bank Accounts:
                    </td>
                    <td>
                        <asp:CheckBox type="checkbox" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseMasterBankAcc']"
                            ID="UseMasterBankAcc" onclick="DisableOnClick(this);setDataChanged(true);" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Show Orphaned Reserve Records (i.e. reserve types not defined in Line of Business
                    Setup):
                    </td>
                    <td>
                        <asp:CheckBox ID="ShowResOrphns" value="True" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ShowResOrphns']"
                            type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <!--Raman 03/08/2009 : R6 Work Loss / Restriction -->
                    <td>Create Work Loss and Restriction Records From Funds From/To Dates:
                    </td>
                    <td>
                        <asp:CheckBox ID="AutoCrtWrkLossRest" value="True" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AutoCrtWrkLossRest']"
                            type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>Use Reserve Type to Filter Transaction Type:
                    </td>
                    <td>
                        <asp:CheckBox type="checkbox" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseResFilter']"
                            ID="UseResFilter" onclick="setDataChanged(true);" runat="server" />
                    </td>
                    <!--Start Rahul Aggarwal Entity payment Approval MITS # 20606 05/05/2010-->
                    <td></td>
                    <td>Show LSS Invoice Link:
                    </td>
                    <td>
                        <asp:CheckBox ID="ShowLSSInvoice" value="True" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='ShowLSSInvoice']"
                            onclick="setDataChanged(true);" runat="server" />
                    </td>
                    <!--End Rahul Aggarwal Entity payment Approval -->
                </tr>
                <!--Animesh Inserted For RMSC Bill Review Fees //skhare7 RMSC Merge 28397 -->
                <tr id="">
                    <td>Enable Bill Review Fee:
                    </td>
                    <td>
                        <asp:CheckBox type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='BillReviewFee']"
                            ID="BillReviewFee" runat="server" />
                    </td>
                    <td></td>
                    <td>Org. Level for Bill Review Fee:
                    </td>
                    <td>
                        <asp:DropDownList ID="OrgBillRevFee" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='OrgBillRevFee']/@value"
                            onchange="setDataChanged(true);clearOrgEntity();" runat="server">
                            <asp:ListItem Value="1005">Client</asp:ListItem>
                            <asp:ListItem Value="1006">Company</asp:ListItem>
                            <asp:ListItem Value="1007">Operation</asp:ListItem>
                            <asp:ListItem Value="1008">Region</asp:ListItem>
                            <asp:ListItem Value="1009">Division</asp:ListItem>
                            <asp:ListItem Value="1010">Location</asp:ListItem>
                            <asp:ListItem Value="1011">Facility</asp:ListItem>
                            <asp:ListItem Value="1012">Department</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <!-- Animesh insertion Ends -->
                <tr>
                    <!--Start mcapps2 Allow void of cleared payments 08/03/2010-->
                    <!-- changed "Allow Void of Cleared Payments" to "Allow Void of Cleared Payments/Collections"; MITS 26144 -->
                    <td>Allow Void of Cleared Payments/Collections:
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowVoidClearedPmts" onMouseDown="ConfirmVoidOfCleared();" type="checkbox"
                            onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AllowVoidClearedPmts']"
                            value="True" runat="server" />
                    </td>
                    <!--End mcapps2 Allow void of cleared payments 08/03/2010-->
                    <td></td>
                    <!--Start skhare7 RMSC merge changes-->
                    <td>Org Entity Name
                    </td>
                    <td>
                        <asp:TextBox runat="server" ReadOnly="false" size="8" onchange="lookupTextChanged(this);setDataChanged(true);"
                            TabIndex="1" RMXType="orgh" ID="OrgEntlastfirstname" onblur="lookupCustomProvField(this);"
                            RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='OrgBillLevelEntityId']" />
                        <asp:Button runat="server" class="EllipsisControl" ID="OrgEntlastfirstnamebtn" TabIndex="2"
                            OnClientClick="return lookupData('OrgEntlastfirstname',' ',4,'OrgEnt',1);" />
                        <asp:TextBox Style="display: none" runat="server" onchange="setDataChanged(true);"
                            ID="OrgEntentityid" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='OrgBillLevelEntityId']/@codeid"
                            RMXType="id" />
                    </td>
                    <td></td>
                </tr>
                <!--start:rupal-->
                <tr id="Tr5">
                    <td>Add Person Involved To Payee Type:
                    </td>
                    <td>
                        <asp:CheckBox ID="AddPersonInvolvedAsPayeeType" onclick="setDataChanged(true);" type="checkbox"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddPersonInvolvedAsPayeeType']"
                            value="True" runat="server" />
                    </td>
                    <td></td>
                    <!-- Start rsushilaggar MITS 21970 Date 01/01/2010-->
                    <td>LSS checks on Hold:
                    </td>
                    <td>
                        <asp:CheckBox ID="LssChecksonHold" type="checkbox" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='LSSChecksOnHold']"
                            value="True" runat="server" />
                    </td>
                    <!--End Rahul Aggarwal -->
                    <td></td>
                </tr>
                <!--ebd:rupal-->
                 <!--Start : Bkuzhanthaim : MITS-36026/RMA-338 -->
                <tr id="Tr11">
                    <td>Add Org. Hierarchy To Payee Type:
                    </td>
                    <td>
                        <asp:CheckBox ID="AddOrgHierarchyAsPayeeType" onclick="setDataChanged(true);" type="checkbox"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddOrgHierarchyAsPayeeType']"
                            value="True" runat="server" />
                    </td>
                    <td></td>
                    <td>Add Payee Address To Select Check screen:
                    </td>
                    <td>
                        <asp:CheckBox ID="PayeeAddressSelect" type="checkbox" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='IsPayeeAddressSelect']"
                            value="True" runat="server" />
                    </td>
                    <td></td>
                </tr>
                <!--End : Bkuzhanthaim : MITS-36026/RMA-338 -->
                <tr>
                    <%--JIRA RMA-16584 nshah28 start--%>
                   <td>Allow Summary For Booked Reserve Only:
                    </td>
                    <td>
                       <asp:CheckBox id="AllowSummaryForBookedReserve" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AllowSummaryForBookedReserve']"  value="True" type="checkbox" runat="server"></asp:CheckBox>
                    </td>
                    <td></td>
                  <%--JIRA RMA-16584 nshah28 end--%>
                </tr>
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Order Bank Accounts
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr id="">
                    <td colspan="2">
                        <asp:RadioButton ID="OrderBankAcctDisp" rmxref="/Instance/Document/form/group/OrderBankAcct"
                            type="radio" runat="server" value="0" onclick="setDataChanged(true);" GroupName="RB3" />
                        None (No Order)
                    </td>
                </tr>
                <tr id="">
                    <td colspan="2">
                        <asp:RadioButton ID="rbPriority" value="1" rmxref="/Instance/Document/form/group/OrderBankAcct"
                            onclick="setDataChanged(true);" type="radio" runat="server" GroupName="RB3" />
                        Priority
                    </td>
                </tr>
                <tr id="">
                    <td colspan="2">
                        <asp:RadioButton ID="rbAccountName" GroupName="RB3" value="2" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/OrderBankAcct" type="radio" runat="server" />
                        Account Name (Default)
                    </td>
                </tr>
                <tr id="">
                    <td colspan="2">
                        <asp:RadioButton ID="rbAccntNumber" GroupName="RB3" type="radio" value="3" rmxref="/Instance/Document/form/group/OrderBankAcct"
                            onclick="setDataChanged(true);" runat="server" />
                        Account Number
                    </td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABEmployee" id="FORMTABEmployee"
                style="display: none;">
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="20%">WC Employee Number Prefix:
                                </td>
                                <td width="10%">
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="MasterBankAcc" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='MasterBankAcc']"
                                            type="text" value="" size="20" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="20%">Autonumber Blank WC Employee Numbers (new employee entry only):
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="AutoNumBankEmp" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AutoNumBankEmp']"
                                        onclick="DisableOnClick(this);setDataChanged(true);" value="" type="checkbox"
                                        Width="10%" runat="server"></asp:CheckBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="20%">Exclude Holidays from Work Loss/Restricted Days Calculation:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="ExcludeHolidays" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ExcludeHolidays']"
                                        onclick="DisableOnClick(this);setDataChanged(true);" value="True" Width="10%"
                                        type="checkbox" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="20%">Allow Edit of WC Employee Numbers:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="AllowEditWCEmp" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AllowEditWCEmp']"
                                        value="True" onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                   <!--Ashish Ahuja-->
                <tr id="Tr8">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="20%">Ignore SSN Checking:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="IgnoreSSNChecking" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='IgnoreSSNChecking']"
                                        value="True" onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="10%">
                                    <table width="100%" align="left">
                                        <tr>
                                            <td class="ctrlgroup" width="100%">Work Shifts
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>Shift 1:
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="Shift1Name" value="" onchange="setDataChanged(true);" size="20"
                                            Text="Morning Shift" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Shift1Name']"
                                            type="text" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                                <td></td>
                                <td>Start:
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="Shift1Start" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift1Start']"
                                        type="time" value="" size="10" Text="8:00 AM" onblur="timeLostFocus(this.id);"
                                        onchange="setDataChanged(true);" runat="server"></asp:TextBox>
                                </td>
                                <td></td>
                                <td>End:
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="Shift1End" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift1End']"
                                        type="time" Text="4:00 PM" value="" size="10" onblur="timeLostFocus(this.id);"
                                        onchange="setDataChanged(true);" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>Shift 2:
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="Shift2Name" size="20" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift2Name']"
                                            onchange="setDataChanged(true);" type="text" value="" Text="Evening Shift" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                                <td></td>
                                <td>Start:
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="Shift2Start" type="time" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift2Start']"
                                        Text="4:00 PM" size="10" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);"
                                        runat="server"></asp:TextBox>
                                </td>
                                <td></td>
                                <td>End:
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="Shift2End" Text="12:00 AM" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift2End']"
                                        size="10" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);" type="time"
                                        runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>Shift 3:
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="Shift3Name" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift3Name']"
                                            onchange="setDataChanged(true);" type="text" value="" Text="Night Shift" size="20"
                                            runat="server"></asp:TextBox>
                                    </div>
                                </td>
                                <td></td>
                                <td>Start:
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="Shift3Start" Text="12:00 AM" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift3Start']"
                                        size="10" value="" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);"
                                        type="time" runat="server"></asp:TextBox>
                                </td>
                                <td></td>
                                <td>End:
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="Shift3End" value="" Text="8:00 AM" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Shift3End']"
                                        size="10" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);" type="time"
                                        runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Work Days
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="2%">Sunday:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="WorkSun" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WorkSun']"
                                        onclick="DisableOnClick(this);setDataChanged(true);" value="" type="checkbox"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="2%">Monday:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="WorkMon" onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WorkMon']"
                                        value="True" type="checkbox" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="2%">Tuesday:&nbsp;&nbsp;
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="WorkTue" value="True" onclick="DisableOnClick(this);setDataChanged(true);"
                                        rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WorkTue']"
                                        type="checkbox" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="2%">Wednesday:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="WorkWed" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WorkWed']"
                                        onclick="DisableOnClick(this);setDataChanged(true);" value="True" ref="//form/group/displaycolumn/control[@name ='WorkWed']"
                                        type="checkbox" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="2%">Thursday:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="WorkThu" onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox"
                                        value="True" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WorkThu']"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="2%">Friday:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="WorkFri" onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WorkFri']"
                                        value="True" type="checkbox" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="2%">Saturday:
                                </td>
                                <td width="10%">
                                    <asp:CheckBox ID="WorkSat" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='WorkSat']"
                                        onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABDiaries" id="FORMTABDiaries"
                style="display: none;">
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Diary Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Attached Diaries Globally Visible:
                    </td>
                    <td>
                        <asp:CheckBox ID="AttchDiaryVis" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AttchDiaryVis']"
                            onchange="" onclick="DisableOnClick(this);setDataChanged(true);" value="True"
                            type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Delete All diaries Related to Claim when Claim is closed:
                    </td>
                    <td>
                        <asp:CheckBox ID="DeleteAllClaimDiaries" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DeleteAllClaimDiaries']"
                            type="checkbox" onchange="" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Allow Global Peek for Diaries:
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowDiaryPeek" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AllowDiaryPeek']"
                            value="True" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Level of Org. Hierarchy Displayed in Diary List:
                    </td>
                    <td>
                        <asp:DropDownList ID="DiaryOrgLvl" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DiaryOrgLvl']/@value"
                            onchange="setDataChanged(true);" type="combobox" runat="server">
                            <asp:ListItem Value="1005">Client</asp:ListItem>
                            <asp:ListItem Value="1006">Company</asp:ListItem>
                            <asp:ListItem Value="1007">Operation</asp:ListItem>
                            <asp:ListItem Value="1008">Region</asp:ListItem>
                            <asp:ListItem Value="1009">Division</asp:ListItem>
                            <asp:ListItem Value="1010">Location</asp:ListItem>
                            <asp:ListItem Value="1011">Facility</asp:ListItem>
                            <asp:ListItem Value="1012">Department</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="">
                    <td>Auto Launch Diary (Non-Powerview):
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAutoLaunchDiary" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AutoLaunchDiary']"
                            value="True" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Allow Task Description Filter:
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowTaskDescriptionFilter" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AllowTaskDescriptionFilter']"
                            value="True" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Default "Assigned to" in Create Diary to Current User:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAssignedTo" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DefAssignedTo']"
                            value="False" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Task Manager Auto Diary:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkTaskMgrDiary" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='TaskManagerDiary']"
                            value="True" onclick="setDataChanged(true);return UpdateExistingTasks(this,'diary');"
                            type="checkbox" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="hdUpdateTasks" value="" type="hidden" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdUpdateTasks']"
                            Style="display: none" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr id="">
                    <!-- nsachdeva2 MITS: 23654 09/19/2011 -->
                    <td>Freeze Diary Completion Date:
                    </td>
                    <td>
                        <asp:CheckBox ID="FreezeDiaryCompletionDate" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FreezeDiaryCompletionDate']"
                            value="False" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                     <td>
                        Task Manager Email Notification:
                    </td>
                    <td>
                       <asp:CheckBox ID="chkTaskMgrEmail" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='TaskManagerEmail']"
                            value="True" onclick="setDataChanged(true);return UpdateExistingTasks(this,'email');"
                            type="checkbox" runat="server" />
                    </td>
                </tr>                
                <!--Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement-->
                <tr id="Tr8">
                    <td>Adjuster Assignment Auto Diary:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAdjAssignmentAutoDiary" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AdjAssignmentAutoDiary']"
                            value="False" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                    <%-- Add by kuladeep for mits:25083 Start--%>
                    <td></td>
                    <td>Disable diaries If check status "Reviewed But Denied":
                    </td>
                    <td>
                        <asp:CheckBox ID="chkDenDiaries" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DenDiaries']"
                            value="False" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                    <%-- Add by kuladeep for mits:25083 End--%>
                </tr>
                <!--Ankit End-->
                <tr id="">
                    <td>Use Current Date for Completed Diaries: 
                    </td>
                    <td>
                        <asp:CheckBox ID="chkCurrentDateForDiaries" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='CurrentDateForDiaries']"
                            value="False" onclick="EnableNotifyAssigner();setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Notify Task Assigner of Completion: 
                    </td>
                    <td>
                        <asp:CheckBox ID="chkNotifyAssignerOfCompletion" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='NotifyAssignerOfCompletion']"
                            value="False" onclick="setDataChanged(true);" type="checkbox" runat="server" />
                    </td>
                </tr>
                <!-- Mits 36708 -->
                <tr id="Tr9">
                    <td>
                        Allowed length for Diary Columns:
                    </td>
                    <td>
                        <asp:TextBox ID="txtClaimantLength" runat="server" type="text" size="5" MaxLength="5" text ="0" value ="0" 
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='ClaimantLength']" onchange="setDataChanged(true);DefaultEntryCreate(this);"></asp:TextBox>
                    </td>
                </tr>
                <!-- End of Mits 36708 -->
                <!-- Igupta3 changes ends-->
                <tr id="">
                    <!-- End MITS:23654 -->
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Free Text Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Date Stamp Free Text:
                    </td>
                    <td>
                        <asp:CheckBox ID="DateStampText" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DateStampText']"
                            onclick="setDataChanged(true);DisableOnClick(this);" value="True" type="checkbox"
                            runat="server" />
                    </td>
                    <td></td>
                    <td>Freeze Free Text Entries:
                    </td>
                    <td>
                        <asp:CheckBox ID="FreezeFreeText" onclick="ChkClickd();setDataChanged(this);DisableOnClick(this);"
                            type="checkbox" value="True" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FreezeFreeText']"
                            runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Don't Allow Edit of Date Stamp in Free Text Areas:
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowEditDateStamp" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AllowEditDateStamp']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Freeze Event Description:
                    </td>
                    <td>
                        <asp:CheckBox ID="FreezEventDesc" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FreezEventDesc']"
                            onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox" value="True"
                            runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Date Stamp Event / Location Description:
                    </td>
                    <td>
                        <asp:CheckBox ID="DateEventDesc" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DateEventDesc']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Freeze Location Description:
                    </td>
                    <td>
                        <asp:CheckBox type="checkbox" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" ID="FreezLocDesc" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FreezLocDesc']"
                            runat="server" />
                    </td>
                </tr>
                <!--MITS 19014 for Use Legacy Comments Functionality: START-->
                <tr id="">
                    <td>Use Legacy Comments:
                    </td>
                    <td>
                        <asp:CheckBox ID="UseLegacyComments" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UseLegacyComments']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>Freeze Supplemental Free Text Entries:
                    </td>
                    <td>
                        <asp:CheckBox ID="ChkFreezeSuppFreeText" onclick="ChkClickd();setDataChanged(this);DisableOnClick(this);"
                            type="checkbox" value="True" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FreezeSuppFreeText']"
                            runat="server" />
                    </td>
                </tr>
                <!--jramkumar for MITS 32095-->
                <tr id="">                   
                    <td>Freeze Pay to the Order of:
                    </td>
                    <td>
                        <asp:CheckBox ID="FreezePayOrderFreeText" onclick="setDataChanged(this);DisableOnClick(this);"
                            type="checkbox" value="True" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FreezePayOrderFreeText']"
                            runat="server" />
                    </td>
                    <td></td>
                    <td>Show Date Stamp on HTML Text:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkDateStampHTMLText" onclick="setDataChanged(this);DisableOnClick(this);"
                            type="checkbox" value="True" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='DateStampHTMLText']"
                            runat="server" />
                    </td>
                </tr>                
                <!--MITS19014 for Use Legacy Comments Functionality: END-->
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Adjuster Text Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Don't Allow Edit of Adjuster Text Date:
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowEditDateAdjuster" onclick="DisableOnClick(this);setDataChanged(true);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AllowEditDateAdjuster']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                    <td></td>
                    <td>
                        <asp:TextBox ID="hdAction" value="" type="hidden" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdAction']"
                            Style="display: none" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Enhanced Notes Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Freeze Existing Notes:
                    </td>
                    <td>
                        <asp:CheckBox ID="FreezeExistingNotes" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FreezeExistingNotes']"
                            type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Sort First:
                    </td>
                    <td>
                        <asp:DropDownList ID="EnhanceNotesPrintOrder1" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EnhanceNotesPrintOrder1']/@value"
                            onchange="ChkEnhSettingsPrintOrder(this);" runat="server">
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1">Activity Date</asp:ListItem>
                            <asp:ListItem Value="2">Date Created</asp:ListItem>
                            <asp:ListItem Value="3">Note Type</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="">
                    <td>Sort Second:
                    </td>
                    <td>
                        <asp:DropDownList ID="EnhanceNotesPrintOrder2" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EnhanceNotesPrintOrder2']/@value"
                            onchange="ChkEnhSettingsPrintOrder(this);" runat="server">
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1">Activity Date</asp:ListItem>
                            <asp:ListItem Value="2">Date Created</asp:ListItem>
                            <asp:ListItem Value="3">Note Type</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="">
                    <td>Sort Third:
                    </td>
                    <td>
                        <asp:DropDownList ID="EnhanceNotesPrintOrder3" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EnhanceNotesPrintOrder3']/@value"
                            type="combobox" onchange="ChkEnhSettingsPrintOrder(this);" runat="server">
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1">Activity Date</asp:ListItem>
                            <asp:ListItem Value="2">Date Created</asp:ListItem>
                            <asp:ListItem Value="3">Note Type</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <%--Parijat:Post Editable Enhanced Note--%>
                <tr id="">
                    <td>Hours for which Enhanced Notes are editable (Zero will mean notes will be editable
                    for infinite number of hours):&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:TextBox ID="EnhNoteTime" onchange="setDataChanged(true);" onblur="EnhTimeValidate(this);"
                            rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EnhNoteTime']"
                            ToolTip="Zero value means that the Enhanced notes are editable for infinite amount of time."
                            type="text" value="0.00" Text="0.00" size="7" MaxLength="7" runat="server"></asp:TextBox>
                        </td>
                    <td colspan="2"><asp:TextBox ID="txtDaysnHours" runat="server" Enabled="false"></asp:TextBox></td>
                </tr>
                <tr id="">
                    <td>Give editing rights only to the user who has created the note:
                    </td>
                    <td>
                        <asp:CheckBox ID="EnhNoteEditRight" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EnhNoteEditRight']"
                            type="checkbox" runat="server" />
                    </td>
                </tr>
                <%-- Parijat:Post Editable Enhanced Note- End--%>
                <%--Williams-neha goel:MITS:21704:view attached policy enhanced notes in claim enhnaced notes--%>
                <tr id="">
                    <td>Show Policy Enhanced Notes in Claim:
                    </td>
                    <td>
                        <asp:CheckBox ID="EnhNotePolicyInClaim" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="True" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='EnhNotePolicyInClaim']"
                            type="checkbox" runat="server" />
                    </td>
                </tr>
                <%-- Williams-neha goel:MITS:21704:view attached policy enhanced notes- End--%>
                <%-- gbindra MITS#34104 WWIG Gap15 01312014--%>
                <tr id="">
                    <td>Allow Notes at the Claimant level:
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowNotesAtClaimant"  onclick="setDataChanged(true);DisableOnClick(this);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AllowNotesAtClaimant']" value="True"
                            type="checkbox" runat="server" />
                    </td>
                </tr>
                <%-- gbindra MITS#34104 WWIG Gap15 01312014--%>
            </table>
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABAdvanceClaim" id="FORMTABAdvanceClaim"
                style="display: none;">
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Carrier Claim Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="">
                    <td>Use Carrier Claims:
                    </td>
                    <td>
                        <asp:CheckBox ID="MultiCovgPerClm" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='MultiCovgPerClm']"
                            onchange="" onclick="SystemParameter_UseCarrierClaims();DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
			    <%-- Nitika : Start--%>
               <tr>
                    <td>Use FNOL Reserve
                    </td>
                    <td>
                        <asp:CheckBox ID="FNOLReserve" onclick="DisableOnClick(this);setDataChanged(true);"
                            value="true" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='FNOLReserve']"
                            runat="server" />
                    </td>
                    <td></td>
              </tr>
                 <%-- Nitika : End--%>
                <tr id="Tr3">
                    <td>Use Unit Stat:
                    </td>
                    <td>
                        <asp:CheckBox ID="UnitStat" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UnitStat']"
                            onchange="" onclick="DisableOnClick(this);setDataChanged(true);" value="True"
                            type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                    <td>Auto Populate Department:
                    </td>
                    <td>
                        <asp:CheckBox ID="AutoPopulateDpt" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AutoPopulateDpt']"
                            onchange="" onclick="DisableAutoFillDpt(this.id);DisableOnClick(this);setDataChanged(true);"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="Tr2">
                    <td>Department For Auto Population:
                    </td>
                    <td>
                        <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                            ID="AutoFillDpteid" RMXRef="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AutoFillDpteid']"
                            type="orgh" name="AutoFillDpteid" cancelledvalue="" />
                        <asp:Button runat="server" class="CodeLookupControl" Text="" ID="AutoFillDpteidbtn"
                            OnClientClick="return selectCode('orgh','AutoFillDpteid','Department');" />
                        <asp:TextBox Style="display: none" runat="server" ID="AutoFillDpteid_cid" RMXRef="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AutoFillDpteid']/@codeid"
                            cancelledvalue="" />
                    </td>
                </tr>
                <%--added by swati--%>
                <tr id="Tr14" style="display:none">
                    <td>Use CLUE Reporting:
                    </td>
                    <td>
                        <asp:checkbox id="UseCLUEReportingFields" onclick="setDataChanged(true);" value="True"
                            type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseCLUEReportingFields']"
                            runat="server" />
                    </td>
                </tr>
                <%--change end here by swati--%>
                <tr>
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr>
                                <td class="ctrlgroup" width="100%">Policy Interface Settings
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--Added by Amitosh for R8 enhancement of Policy system interface--%>
                <tr>
                    <td>Use Policy System Interface:
                    </td>
                    <td id="GroupParentRB5">
                        <asp:CheckBox ID="UsePolicyInterface" onclick="toggleUsePolicyInerface(this);DisableOnClick(this);setDataChanged(true);"
                            value="false" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UsePolicyInterface']"
                            runat="server" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                <!--
                    <td>Use Staging Policy System:
                    </td>
                    -->
                    <td>
                        <asp:CheckBox ID="UseStagingPolicySystem" visible="false" value="false" type="checkbox" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseStagingPolicySystem']"
                            runat="server" />
                    </td>
                </tr>
                <%--Start - averma62 MITS 25163- Policy Interface Implementation--%>
                <tr style="display :none;">
                    <td>Allow Policy Search:
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowPolicySearch"  value="false" type="checkbox" onMouseDown="ConfirmAllowPolicySearch();" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AllowPolicySearch']"
                            runat="server" />
                    </td>
                    <td></td>
                </tr>
                <%//neha goel--MITS#33414--11202013 %>
                <tr>
                    <td>No Required fields for Policy Download Search:
                    </td>
                    <td>
                        <asp:CheckBox ID="NoReqFieldsForPolicySearch" value="false" type="checkbox" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='NoReqFieldsForPolicySearch']"
                            runat="server" />
                    </td>
                    <td></td>
                </tr>
                <!--neha goel--MITS#33414--11202013 end-->
                <!-- aaggarwal29 changes for Point Policy Interface start-->
                <tr>
                    <td>Fetch Records on policy search
                    </td>
                    <td>
                        <asp:TextBox ID="FetchRecOnPolicySearch" type="text" runat="server" onchange="setDataChanged(true);" onblur="CheckPolicyRecordCount(this)" value="30" MaxLength="3" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='PolicySearchCount']"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <!-- aaggarwal29 changes for Point Policy Interface end -->
                <%--End - averma62 MITS 25163- Policy Interface Implementation--%>
                <%--  <tr>
                <td>
                    Policy System Financial Updates will use Real Time updates:
                </td>
                <td>
                    <asp:RadioButton ID="UsePolicyInterface1" type="radio" value="0" rmxref="/Instance/Document/form/group/PolicySysUpdate"
                        onclick="setDataChanged(true);" runat="server" GroupName="rbgPolicySysUpdate" />
                </td>
                <td>
                </td>
            </tr>

            <tr>
                <td>
                    Policy System Financial Updates will use Batch updates:
                </td>
                <td>
                    <asp:RadioButton ID="UsePolicyInterface2" type="radio" value="1" rmxref="/Instance/Document/form/group/PolicySysUpdate"
                        onclick="setDataChanged(true);" runat="server" GroupName="rbgPolicySysUpdate" />
                </td>
                <td>
                </td>
            </tr>--%>
                <tr>
                    <!--MITS:33573 Starts-->
                    <!--td>Download External Policy as Claim Based:</!--td-->
                    <td>Search External Policies Based on Claim Date:</td>
                    <!--MITS:33573 Ends-->
                    <td>
                        <asp:RadioButton ID="PolicyType1" type="radio" value="0" rmxref="/Instance/Document/form/group/PolicyCvgType"
                            onclick="setDataChanged(true);" runat="server" GroupName="rbgPolicyCvgType" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <!--MITS:33573 Starts-->
                    <!--td>Download External Policy as Event Based:</!--td-->
                    <td>Search External Policies Based on Event Date:</td>
                    <!--MITS:33573 End-->
                    <td>
                        <asp:RadioButton ID="PolicyType2" type="radio" value="1" rmxref="/Instance/Document/form/group/PolicyCvgType"
                            onclick="setDataChanged(true);" runat="server" GroupName="rbgPolicyCvgType" />
                    </td>
                    <td></td>
                </tr>

                <%--    <tr>
                <td>Upload Supplemental data to Policy Interface:</td>
                    <td>
                    <asp:CheckBox ID="UploadSuppToPolicy" value="false" type="checkbox"  rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UploadSuppToPolicy']" runat="server" />
                    </td>
            <td></td>
                         </tr>--%>
                <tr>
                    <td>Use Code Mapping:</td>
                    <td>
                        <asp:CheckBox ID="UseCodeMapping" value="false" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseCodeMapping']" runat="server" />
                    </td>
                    <td></td>
                </tr>
                 <!-- Start - Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount -->
              <tr>
                    <td>Upload Check Total instead of Funds Amount :
                    </td>
                    <td>
                        <asp:CheckBox ID="UploaddCheckTotal" onclick="setDataChanged(true);"
                            value="true" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UploadCheckTotal']"
                            runat="server" />
                    </td>
                    <td></td>

             </tr>
              <!-- End - Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount -->
				 <tr>
                    <td>Show Media View Button :
                    </td>
                    <td>
                        <asp:CheckBox ID="ShowMediaViewButton" onclick="setDataChanged(true);"
                            value="true" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='ShowMediaViewButton']"
                            runat="server" />
                    </td>
                    <td></td>
             </tr>
             <tr>
                    <td>Open External Policy in Point :
                    </td>
                    <td>
                        <asp:CheckBox ID="OpenPointPolicy" onclick="setDataChanged(true);"
                            value="true" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='OpenPointPolicy']"
                            runat="server" />
                    </td>
                    <td></td>
             </tr>
                <tr>
                    <td>Allow AutoLogin from External Policy System :
                    </td>
                    <td>
                        <asp:CheckBox ID="AllowAutoLogin" onclick="setDataChanged(true);"
                            value="flase" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AllowAutoLogin']"
                            runat="server" />
                    </td>
                    <td></td>
             </tr>
            <!--//JIRA RMA-718 ajohari2 Start-->
             <tr>
                    <td>Enable TPA Access (only applies with Business Entity Security):</td>
                    <td>
                        <asp:CheckBox ID="EnableTPAAccess" value="false" type="checkbox" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='EnableTPAAccess']" runat="server" />
                    </td>
                    <td></td>
             </tr>
             <!--//JIRA RMA-718 ajohari2 End-->
            </table>
            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABGlobalization" id="FORMTABGlobalization"
                style="display: none;">
                <tr id="">
                    <td colspan="5">
                        <table width="100%" align="left">
                            <tr id="">
                                <td colspan="5">
                                    <table width="100%" align="left">
                                        <tr>
                                            <td class="ctrlgroup" width="100%">Globalization Settings
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="">
                                <td>Use Multiple Currency:
                                </td>
                                <td>
                                    <asp:CheckBox ID="UseMultiCurrency" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='UseMultiCurrency']"
                                        onchange="" onclick="DisableAutoFillDpt(this.id);DisableOnClick(this);setDataChanged(true);"
                                        value="True" type="checkbox" runat="server" />
                                </td>
                            </tr>
                            <tr id="">
                                <td>Select Base Currency:
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  "
                                            ID="basecurrencytype" CodeTable="CURRENCY_TYPE" ControlName="basecurrencytype"
                                            RMXRef="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='BaseCurrencyType']"
                                            CodeFilter="" RMXType="code" />
                                    </div>
                                </td>
                            </tr>
                        
                            <tr id="">
                                <td colspan="5">
                                    <table width="100%" align="left">
                                        <tr>
                                            <td class="ctrlgroup" width="100%">Regional Configuration
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dg:UserControlDataGrid runat="server" ID="RegionalLanguageSetupGrid" GridName="RegionalLanguageSetupGrid"
                                                    Target="/Document/form/RegionalLanguageSetup"
                                                    Ref="/Instance/Document/form[@name='RegionalLanguageSetupGrid']" Unique_Id="RowId"
                                                    ShowRadioButton="true" Width="" Height="800" HideNodes="|RowId|" ShowHeader="True"
                                                    LinkColumn="" PopupWidth="500" PopupHeight="200" Type="GridAndButtons" RowDataParam="listrow"
                                                    OnClick="KeepRowForEdit('RegionalLanguageSetupGrid');" HideButtons="New|Delete" />
                                                <asp:TextBox Style="display: none" runat="server" ID="RegionalLanguageSetupSelectedId"
                                                    RMXType="id" />
                                                <asp:TextBox Style="display: none" runat="server" ID="RegionalLanguageSetupGrid_RowDeletedFlag"
                                                    RMXType="id" Text="false" />
                                                <asp:TextBox Style="display: none" runat="server" ID="RegionalLanguageSetupGrid_Action"
                                                    RMXType="id" />
                                                <asp:TextBox Style="display: none" runat="server" ID="RegionalLanguageSetupGrid_RowAddedFlag"
                                                    RMXType="id" Text="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                          </table>
                    </td>
                </tr>
                <tr id="Tr7"> 
                    <td class="" width="100%"><%--Multi-Language Setup--%>
                    </td>
                </tr>
                <tr id="">
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <table>
                                <tr>
                                    <td>
                                        <%--<dg:UserControlDataGrid runat="server" ID="MultiLanguageSetupGrid" GridName="MultiLanguageSetupGrid"
                                            Target="/Document/form/MultiLanguageSetup"
                                            Ref="/Instance/Document/form[@name='MultiLanguageSetupGrid']" Unique_Id="RowId"
                                            ShowRadioButton="true" Width="" Height="800" HideNodes="|RowId|" ShowHeader="True"
                                            LinkColumn="" PopupWidth="500" PopupHeight="200" Type="GridAndButtons" RowDataParam="listrow"
                                            OnClick="KeepRowForEdit('MultiLanguageSetupGrid');" HideButtons="New|Delete" />
                                        <asp:TextBox Style="display: none" runat="server" ID="MultiLanguageSetupSelectedId"
                                            RMXType="id" />
                                        <asp:TextBox Style="display: none" runat="server" ID="MultiLanguageSetupGrid_RowDeletedFlag"
                                            RMXType="id" Text="false" />
                                        <asp:TextBox Style="display: none" runat="server" ID="MultiLanguageSetupGrid_Action"
                                            RMXType="id" />
                                        <asp:TextBox Style="display: none" runat="server" ID="MultiLanguageSetupGrid_RowAddedFlag"
                                            RMXType="id" Text="false" />--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <input type="text" name="" value="" id="SysViewType" style="display: none">
        <input type="text" name="" value="" id="SysCmd" style="display: none">
        <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none">
        <input type="text" name="" value="" id="SysCmdQueue" style="display: none">
        <input type="text" name="" value="" id="SysCmdText" style="display: none">
        <input type="text" name="" value="" id="SysClassName" style="display: none">
        <input type="text" name="" value="" id="SysSerializationConfig" style="display: none">
        <input type="text" name="" value="" id="SysFormIdName" style="display: none">
        <input type="text" name="" value="" id="SysFormPIdName" style="display: none">
        <input type="text" name="" value="" id="SysFormPForm" style="display: none">
        <input type="text" name="" value="" id="SysInvisible" style="display: none">
        <input type="text" name="" value="generalsystemparameter" id="SysFormName" style="display: none">
        <input type="text" name="" value="" id="SysRequired" style="display: none">
        <input type="text" name="" value="" id="SysFocusFields" style="display: none">
        <!-- rsushilaggar MITS 20606 Date 05/05/2010-->
        <asp:CheckBox ID="RMXLSSEnable" type="checkbox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='RMXLSSEnable']"
            Style="display: none" runat="server" />
        <script language="javascript" type="text/javascript">
            try {
                var elem = document.getElementById("dOracleCaseInsensitive");
                var elem1 = document.getElementById("dOracleCaseInsensitiveCheckBox");
                var ctrl = document.getElementById("hdnOracleCaseInsensitive");
                if (ctrl.value == 'true') {
                    elem.style.display = 'none';
                    elem1.style.display = 'none';
                }
                else {
                    elem.style.display = '';
                    elem1.style.display = '';
                }
                //Added Rakhi for MITS:16804-Assign Check Stock to Sub Accounts checkbox should be enabled only when Use Sub Bank Accounts is selected
                var objSubAcc = eval(document.getElementById("SubBankAcc"));
                if (objSubAcc != null) {
                    if (objSubAcc.checked) {
                        document.forms[0].AssignSubAcc.disabled = false;
                        document.forms[0].AccDepositBal.disabled = false;
                    }
                    else {
                        document.forms[0].AssignSubAcc.disabled = true;
                        document.forms[0].AccDepositBal.disabled = true;
                    }
                }
                //Added Rakhi for MITS:16804-Assign Check Stock to Sub Accounts checkbox should be enabled only when Use Sub Bank Accounts is selected

            } catch (ex) { }
        </script>
        <asp:TextBox ID="SysWindowId" type="hidden" value="rmx-widget-handle-3" Style="display: none"
            runat="server"></asp:TextBox>
        <asp:TextBox Style="display: none" runat="server" ID="txtGenerateXml" Text="true"></asp:TextBox>
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
