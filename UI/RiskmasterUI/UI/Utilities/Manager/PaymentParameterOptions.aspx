﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentParameterOptions.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.PaymentParameterOptions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>

    <script language="javascript" type="text/javascript">
			function Save()
			{
				 window.opener.document.forms[0].hdnAction.value="setCriteria";
				 window.opener.document.forms[0].hidden_DataChanged.value="true";
				 window.opener.document.forms[0].hdnValue.value = document.forms[0].txtHowMany.value; //JIRA RMA - 579
				 window.opener.document.forms[0].hdnCriteria.value="Y";
				 window.opener.document.forms[0].submit();
				 self.close();
			}
			function numLostFocus(objCtrl)
			{
				val=0;
				if (window.opener.document.forms[0].hdnValue.value.length>0)
				{
					val=window.opener.document.forms[0].hdnValue.value;
				}
				
				if(objCtrl.value.length==0)
				{
					objCtrl.value=val;
					return false;
				}
				if(isNaN(parseFloat(objCtrl.value)))
					objCtrl.value="";
				else
					objCtrl.value=parseFloat(objCtrl.value);
				if(objCtrl.value.length==0)
				{
					objCtrl.value=val;
				}
				return true;
			}
			function Close()
			{
				self.close();
			}
    </script>
      <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
</head>
<body class="Center32">
    <form id="frmOpen" runat="server">
    <table width="95%" border="0" align="center" id="Table3">
        <tr>
            <td class="msgheader" colspan="2">
                Number Of Days
            </td>
        </tr>
        <tr>
            <td>
                How many days within the Transaction Date?
            </td>
            <td>
                <input type="text" id="txtHowMany" value="" name="txtHowMany" onblur="numLostFocus(this)"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <br />
                <input type="button" class="button" value="  OK  " onclick="Save();" id="Button1"
                    name="Button1" />
                <input type="button" class="button" value="Cancel" onclick="Close();" id="Button2"
                    name="Button2" />
            </td>
        </tr>
    </table>
    </form>

    <script language="javascript" type="text/javascript">
    
        document.forms[0].txtHowMany.value = window.opener.document.forms[0].hdnValue.value; //JIRA RMA - 579
    </script>

</body>
</html>
