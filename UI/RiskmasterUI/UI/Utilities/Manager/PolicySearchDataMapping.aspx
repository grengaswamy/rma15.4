﻿<%--**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 **********************************************************************************************
 * 06/04/2014 | 33371   | ajohari2   | Added new page for Policy search data mapping 
 **********************************************************************************************--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicySearchDataMapping.aspx.cs"
    ValidateRequest="false" Inherits="Riskmaster.UI.Utilities.Manager.PolicySearchDataMapping" %>

<%@ Register TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Policy Search Data Mapping</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">{var i;}
    </script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
        <div>
            <!--MITS 33371 ajohari2: Start-->
            <uc1:errorcontrol id="ErrorControl1" runat="server" />
            <div>
                <span>
                    <dg:usercontroldatagrid runat="server" id="PolicySearchDataMappingGrid" gridname="PolicySearchDataMappingGrid" gridtitle="Policy Search Data Mapping" target="/Document/PolicySearchDataMappingList" ref="/Instance/Document/form//control[@name='PolicySearchDataMappingGrid']" unique_id="RowId" showradiobutton="true" width="" height="100%" hidenodes="|RowId|" showheader="True" linkcolumn="" popupwidth="700" popupheight="500" type="GridAndButtons" rowdataparam="listrow" />
                </span>
            </div>
            <asp:textbox style="display: none" runat="server" id="PolicySearchDataMappingSelectedId" rmxtype="id" />
            <asp:textbox style="display: none" runat="server" id="PolicySearchDataMappingGrid_RowDeletedFlag" rmxtype="id" text="false" />
            <asp:textbox style="display: none" runat="server" id="PolicySearchDataMappingGrid_Action" rmxtype="id" />
            <asp:textbox style="display: none" runat="server" id="PolicySearchDataMappingGrid_RowAddedFlag" rmxtype="id" text="false" />
            <!--MITS 33371 ajohari2: End-->
        </div>

    </form>
</body>
</html>
