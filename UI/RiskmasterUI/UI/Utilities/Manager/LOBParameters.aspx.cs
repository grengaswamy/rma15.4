﻿/**********************************************************************************************
 *   Date     |  Jira   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.UI.Shared.Controls;
using System.Drawing.Printing;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class LOBParameters : NonFDMBasePageCWS
    {
    private XElement XmlTemplate = null;
    private bool bReturnStatus = false;
    private XmlDocument XmlDoc = new XmlDocument();
    private string sreturnValue = string.Empty;
    private string sNode = null;
    private string sNotes = null;
    private string strSelected = string.Empty;
    private string strDupPolSelected = string.Empty;//RMA-12047
    private XmlNode XNodeTemp = null; //MGaba2: R6 Reserve WorkSheet
    

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                PrintDocument oPrintDocument = new PrintDocument();
                if (!IsPostBack)
                {
                    XmlTemplate = GetMessageTemplate();
                    //Changed By Nitika
                    ViewState["PrinterName"] = ddlPrinters.SelectedIndex;
                    ViewState["PaperBin"] = ddlPbfl.SelectedIndex;
                    //Changed By Nitika
                    bReturnStatus = CallCWS("LOBParmsAdaptor.Get", XmlTemplate, out sreturnValue, false, true);
                    if (bReturnStatus)
                    {
                        ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                        XmlDoc.LoadXml(sreturnValue);
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types[@selected='1']/@value").Value;
                        hdnLOBSelected.Text = sNode;
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DupNoOfDays").InnerText;
                        hdnValue.Text = sNode;
                        ViewState["DupNoOfDays"] = hdnValue.Text;
						//RMA-12047
                        if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDupXDays") != null)
                        {
                            hdnPolClaimDupDays.Text = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDupXDays").InnerText;
                            ViewState["PolClaimDupXDays"] = hdnPolClaimDupDays.Text;
                        }
                        //Changed By Nitika
                        //For Printer DropDownSelection
                        //New Functionality Start
                        ddlPrinters.Items.Clear();
                        ddlPrinters.Items.Add("");
                        //tkatsarski: 02/17/15 - RMA-6802: When the server's printing service is stopped ("print spooler"), PrinterSettings.InstalledPrinters is throwing exception
                        try
                        {
                            foreach (string printer in PrinterSettings.InstalledPrinters)
                            {

                                ListItem item = new ListItem(printer);
                                ddlPrinters.Items.Add(printer);
                            }
                        }
                        catch (System.ComponentModel.Win32Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                        }
                        sNode = null;


                        if (ViewState["PrinterName"] != null)
                        {
                            ddlPrinters.Enabled = true;
                            ddlPrinters.SelectedIndex = (int)ViewState["PrinterName"];

                        }

                        if (ViewState["PaperBin"] != null)
                        {
                            ddlPbfl.Enabled = true;
                            ddlPbfl.Items.Clear();
                            ddlPbfl.Items.Add("");
                            foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                            {
                                ListItem lstitem = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                                ddlPbfl.Items.Add(lstitem);
                            }
                            ddlPbfl.SelectedIndex = (int)ViewState["PaperBin"];

                        }
                        //Changed By Nitika
                    }
                }
                if (state.Value != "btnSaveClicked")
                {
                    if (IsPostBack)
                    {
                        if (hdnAction.Text=="setCriteria")
                        {
                            XmlTemplate = getCritereaTemplate();
                            sreturnValue = string.Empty;
                            ViewState["lobSelected"] = hdnLOBSelected.Text;
                            //Changed By Nitika
                            ViewState["PrinterName"] = ddlPrinters.SelectedIndex;
                            ViewState["PaperBin"] = ddlPbfl.SelectedIndex;
                            //Changed By Nitika
                            bReturnStatus = false;
                            bReturnStatus = CallCWS("LOBParmsAdaptor.SaveDupNoOfDays", XmlTemplate, out sreturnValue, false,false);
                            if (bReturnStatus)
                            {
                                ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                                XmlDoc.LoadXml(sreturnValue);
                                lstDupClaimCriteria.Items.Clear();
                                XmlNodeList DupClaimCriteria = XmlDoc.SelectNodes("//SetCriteria/DupClaimCriteria/Types");
                                foreach (XmlNode xNode in DupClaimCriteria)
                                {
                                    lstDupClaimCriteria.Items.Add(new ListItem(xNode.InnerText, xNode.SelectSingleNode("@value").Value));
                                }
                                strSelected =(string) (XmlDoc.SelectSingleNode("//SetCriteria/DupClaimCriteria/Types[@selected='1']").Attributes["value"]).Value;
                                XmlTemplate = GetMessageTemplate();
                                sreturnValue = "";
                                hdnValue.Text = (string)(XmlDoc.SelectSingleNode("//SetCriteria/DupNoOfDays").InnerText);
                                ViewState["DupNoOfDays"] = hdnValue.Text;
                                lstDupClaimCriteria.SelectedValue = strSelected;
                                chkDuplicate.Checked = true;
                                hdnValue.Text = ViewState["DupNoOfDays"].ToString();
                                lstLOB.SelectedValue = ViewState["lobSelected"].ToString();
                            }
                           
                        }
						//RMA-12047
                        else if (hdnAction.Text == "setCriteriaForPolicyDup")
                        {
                            XmlTemplate = getCritereaTemplateForPolicyDup();
                            sreturnValue = string.Empty;
                            bReturnStatus = false;
                            bReturnStatus = CallCWS("LOBParmsAdaptor.SaveDupPolNoOfDays", XmlTemplate, out sreturnValue, false,false);
                            if (bReturnStatus)
                                ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                            XmlTemplate = GetMessageTemplateForResClaimType();
                            sreturnValue = "";
                            bReturnStatus = false;
                            bReturnStatus = CallCWS("LOBParmsAdaptor.Get", XmlTemplate, out sreturnValue, false, true);
                            if (bReturnStatus)
                                XmlDoc.LoadXml(sreturnValue);
                            hdnAction.Text = string.Empty;
                            hdnLOBSelected.Text = lstLOB.SelectedValue;
                            strDupPolSelected = (string)(XmlDoc.SelectSingleNode(".//PolClaimDuplicateCriteria/Types[@selected='1']").Attributes["value"]).Value;
                            drpDupCriteriaPolDown.SelectedValue = strDupPolSelected;
                            hdnPolClaimDupDays.Text = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDupXDays").InnerText;
                            ViewState["PolClaimDupXDays"] = hdnPolClaimDupDays.Text;
                        }
						//RMA-12047
                            //For ResClaim Type
                        else if (hdnAction.Text == "UpdateClaimResType")
                        {
                            bReturnStatus = false;
                            sreturnValue = string.Empty;
                            chkRBZero.Attributes["value"] = hdnoption1.Value;
                            chkRAZero.Attributes["value"] = hdnoption2.Value;
                            chkNRBZero.Attributes["value"] = hdnoption3.Value;
                            chkDoNothing.Attributes["value"] = hdnoption4.Value;
                            ViewState["ClaimType"] = lstClaimType.SelectedIndex;
                            //Changed By Nitika
                            ViewState["PrinterName"] = ddlPrinters.SelectedIndex;
                            ViewState["PaperBin"] = ddlPbfl.SelectedIndex;
                            //Changed By Nitika

                            bReturnStatus = CallCWS("LOBParmsAdaptor.UpdateSysClaimTypeReserve", XmlTemplate, out sreturnValue, true, false);
                            if (bReturnStatus)
                                ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                                XmlTemplate = GetMessageTemplateForResClaimType();
                                sreturnValue = "";
                                bReturnStatus = false;
                                bReturnStatus = CallCWS("LOBParmsAdaptor.Get", XmlTemplate, out sreturnValue, false, true);
                            if(bReturnStatus)
                                XmlDoc.LoadXml(sreturnValue);
                            hdnAction.Text = string.Empty;
                            hdnLOBSelected.Text = lstLOB.SelectedValue;
                        }
                        else if (hdnAction.Text == "ReserveChange")
                        {
                            bReturnStatus = false;
                            sreturnValue = string.Empty;
                            chkRBZero.Attributes["value"] = hdnoption1.Value;
                            chkRAZero.Attributes["value"] = hdnoption2.Value;
                            chkNRBZero.Attributes["value"] = hdnoption3.Value;
                            chkDoNothing.Attributes["value"] = hdnoption4.Value;
                            ViewState["ClaimType"] = lstClaimType.SelectedIndex;
                            ViewState["ClaimTypeSelectedValue"] = lstClaimType.SelectedValue;
                            //Changed By Nitika
                            ViewState["PrinterName"] = ddlPrinters.SelectedIndex;
                            ViewState["PaperBin"] = ddlPbfl.SelectedIndex;
                            //Changed By Nitika
                            XmlTemplate = GetMessageTemplateForResClaimType();
                            //bkumar33- MITS 14780(03/25/09)- Changed from false to true to Bind value to controls
                            bReturnStatus = CallCWS("LOBParmsAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                            if (bReturnStatus)
                                XmlDoc.LoadXml(sreturnValue);
                            hdnAction.Text = string.Empty;
                        }
                        else if (hdnAction.Text == "SelectedIndexChange")
                        {
                            //Changed By Nitika
                            ViewState["PrinterName"] = ddlPrinters.SelectedIndex;
                            ViewState["PaperBin"] = ddlPbfl.SelectedIndex;
                            //Changed By Nitika
                            bReturnStatus = CallCWS("LOBParmsAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                           if (bReturnStatus)
                               XmlDoc.LoadXml(sreturnValue);
                          
                        }
                        
                        else
                        {
                            XmlTemplate = GetMessageTemplate();
                            //Changed By Nitika
                            ViewState["PrinterName"] = ddlPrinters.SelectedIndex;
                            ViewState["PaperBin"] = ddlPbfl.SelectedIndex;
                            //Changed By Nitika
                            bReturnStatus = CallCWS("LOBParmsAdaptor.Get", XmlTemplate, out sreturnValue, false, true);
                            if (bReturnStatus)
                            {
                                ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                                XmlDoc.LoadXml(sreturnValue);
                            }
                            sNode = null;
                            sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types[@selected='1']/@value").Value;
                            hdnLOBSelected.Text = sNode;

                        }
                       
                    }
                    sNode = null;
                    //MGaba2: MITS 23003:Use Claim comments checkbox once disabled, was not becoming editable again 
                    //even after use enhanced notes chkbox is checked or adj text is unchecked and record is saveds.LOB needs to be changed
                    //if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/UseAdjusterText") != null)
                    //{
                    //    sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/UseAdjusterText").InnerText;
                    //}
                    //if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/UseEnhancedNotes") != null)
                    //{
                    //    sNotes = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/UseEnhancedNotes").InnerText;
                    //}

                    ////if (sNode == "-1" || sNotes != "-1")//-1 for true 
                    //if (sNode != "0" || sNotes == "0")//-1 for true 
                    //{
                    //    lblClaimComments.Enabled = false;
                    //    chkClmComments.Enabled = false;
                    //}
                    //else
                    //{
                    //    lblClaimComments.Enabled = true;
                    //    chkClmComments.Enabled = true;
                    //}
                    //sNode = null;
                    //MGaba2: MITS 23003:End
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/UseClaimComments") != null)
                    {
                        sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/UseClaimComments").InnerText;
                        if (sNode == "-1")
                        {
                            hdnClaimCommentsDsplyonly.Text = "True";
                        }
                    }
                    sNode = null;
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/LevelTypes/Types[@selected='1']") != null)
                    {
                         hdnLevelTypesDisabled.Text = "N";
                    }
                    else
                    {
                        hdnLevelTypesDisabled.Text = "Y";
                    }
                    sNode = null;
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria/Types[@selected='1']") != null)
                    {
                        sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria/Types[@selected='1']/@value").Value;

                        hdnDupClaimCriteriaDisabled.Text = "N";
                    }
                    else
                    {
                        hdnDupClaimCriteriaDisabled.Text = "Y";
                    }
                    sNode = null;
                  
                    //RMA-12047
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria/Types[@selected='1']") != null)
                    {
                        sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria/Types[@selected='1']/@value").Value;

                        hdndupCriteriaAtPolDownDisable.Text = "N";
                    }
                    else
                    {
                        hdndupCriteriaAtPolDownDisable.Text = "Y";
                    }
                    sNode = null;
                    //RMA-12047
                    //spahariya MITS 29221 07/27/2012 add printer settings for Auto Mail merge-- Start
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DirectToPrinter") != null)
                    {
                        sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DirectToPrinter").InnerText;
                        if (sNode == "-1")
                        {
                            DirectToPrinter.Checked = true;
                        }
                    }
                    //For Printer DropDownSelection
                    //New Functionality Start
                    ddlPrinters.Items.Clear();
                    ddlPrinters.Items.Add("");
                    //tkatsarski: 02/17/15 - RMA-6802: When the server's printing service is stopped ("print spooler"), PrinterSettings.InstalledPrinters is throwing exception
                    try
                    {
                        foreach (string printer in PrinterSettings.InstalledPrinters)
                        {

                            ListItem item = new ListItem(printer);
                            ddlPrinters.Items.Add(printer);
                        }
                    }
                    catch (System.ComponentModel.Win32Exception ex)
                    {
                        ErrorHelper.logErrors(ex);
                    }
                    sNode = null;
                    
                           
                    if (ViewState["PrinterName"] != null)
                    {
                        ddlPrinters.Enabled = true;
                        ddlPrinters.SelectedIndex = (int)ViewState["PrinterName"];
                                
                    }

                    if (ViewState["PaperBin"] != null)
                    {
                        ddlPbfl.Enabled = true;
                        ddlPbfl.Items.Clear();
                        ddlPbfl.Items.Add("");
                        foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                        {
                            ListItem lstitem = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                            ddlPbfl.Items.Add(lstitem);
                        }
                        ddlPbfl.SelectedIndex = (int)ViewState["PaperBin"];

                    }
                    //New Functionality End
                    sNode = null;
                  //spahariya MITS 29221 07/27/2012 add printer settings for Auto Mail merge-- End
                  if (hdnAction.Text != "setCriteria")
                  {
                    lstLOB.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types[@selected='1']").Attributes["value"]).Value;
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']") != null)
                    {
                        lstPeriodTypes.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']").Attributes["value"]).Value;
                    }
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/CaptionTypes/Types[@selected='1']") != null)
                    {
                        lstCaptionTypes.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/CaptionTypes/Types[@selected='1']").Attributes["value"]).Value;
                    }
                  
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/LevelTypes/Types[@selected='1']") != null)
                    {
                        lstEntities.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/LevelTypes/Types[@selected='1']").Attributes["value"]).Value;
                    }
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria/Types[@selected='1']") != null)
                    {
                        lstDupClaimCriteria.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria/Types[@selected='1']").Attributes["value"]).Value;
                    }
                    //RMA-12047
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria/Types[@selected='1']") != null)
                    {
                        drpDupCriteriaPolDown.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria/Types[@selected='1']").Attributes["value"]).Value;
                        strDupPolSelected = drpDupCriteriaPolDown.SelectedValue;
                    }
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']") != null)
                    {
                        lstPeriodTypes.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']").Attributes["value"]).Value;
                    }
                       
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/optClosedReserves1").InnerText == "-1")
                        chkRBZero.Checked = true;
                    else
                        chkRBZero.Checked = false;
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/optClosedReserves2").InnerText == "-1")
                        chkRAZero.Checked = true;
                    else
                        chkRAZero.Checked = false;

                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/optClosedReserves3").InnerText == "-1")
                        chkNRBZero.Checked = true;
                    else
                        chkNRBZero.Checked = false;
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/optClosedReserves4").InnerText == "-1")
                        chkDoNothing.Checked = true;
                    else
                        chkDoNothing.Checked = false;

                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/optTrigger").InnerText == "0")
                        rdClmYear.Checked = true;
                    else
                        rdClmEvtYear.Checked = true;
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/AutoCloseOptions/IncludeScheduledChecks").InnerText == "0")
                        chkIncludeScheduledChecks.Checked = false;
                    else
                        chkIncludeScheduledChecks.Checked = true;
                }
                    hdnAction.Text = string.Empty;
                    if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/ClaimTypes/Types[@selected='1']") != null)
                    {
                        lstClaimType.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/ClaimTypes/Types[@selected='1']").Attributes["value"]).Value;
                    }
                    else
                    {
                        ViewState["ClaimType"] = null;
                    }

                    //Sumit - 09/22/2009 MITS#18227 Fetch value of IsActCodeReq attribute
                    if (XmlDoc.SelectSingleNode("//UseEnhPol") != null)
                    {
                        hdnIsActCodeReq.Text = (string)XmlDoc.SelectSingleNode("//UseEnhPol").Attributes["IsActCodeReq"].Value;
                    }
                    //Deb:BOB
                    if (XmlDoc.SelectSingleNode("//IsPolicyFilter") != null)
                    {
                        if (XmlDoc.SelectSingleNode("//IsPolicyFilter").Attributes["IsmultiCovgClm"].Value.ToString() == "-1")
                        {
                            UseEnhPol.Enabled = false;
                            ChkUseRSW.Enabled = false;
                            rdDetailLvl.Enabled = false;//Deb : MITS 25430
                            rdClaimDetailLvl.Enabled = false;
                            //Start:added by Nitin Goel, for NI ,MITS 30910,01/25/2013
                            TABSDeductibleOptions.Visible = true;
                            //Start: Commented by Nikhil ON 07/10/14
                             //if (XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types") != null)
                             //    if ((string)XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types[@selected='1']/@value").Value == "241")
                             //    {
                             //        divWCDisabLossType.Visible = false;
                             //        divGCLossType.Visible = true;
                             //    }
                             //    else if((string)XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types[@selected='1']/@value").Value == "243")
                             //    {
                             //        divWCDisabLossType.Visible = true;
                             //        divGCLossType.Visible = false;
                             //    }
                            //End: Commented by Nikhil ON 07/10/14
                            //end: nitin goel   
							//RMA-12047  
                            trPolDownDupFlag.Visible = true;
                            trPolDownDupCriteria.Visible = true;
							//RMA-12047

                          
                        }
                            //Start:added by Nitin Goel, for NI ,MITS 30910,01/25/2013
                        else
                        {
                            //tanwar2 - mits 30910 - start
                            TABSDeductibleOptions.Visible = false;
							//RMA-12047
                            trPolDownDupFlag.Visible = false;
                            trPolDownDupCriteria.Visible = false;
                           
							//RMA-12047
                        }

                        if (XmlDoc.SelectSingleNode("//IsPolicyFilter").Attributes["IsPolicySystemInterface"] != null
                            && string.Compare(XmlDoc.SelectSingleNode("//IsPolicyFilter").Attributes["IsPolicySystemInterface"].Value, "-1") != 0)
                        {
                            TABSDeductibleOptions.Visible = false;
                            trUseLimitTrk.Visible = false;
                        }
                        else
                        {
                            trUseLimitTrk.Visible = true;
                        }
                           //End:Nitin Goel
                    }
                }
                ViewState["lobSelected"] =lstLOB.SelectedValue;
                if(strSelected.Trim() != "")
                lstDupClaimCriteria.SelectedIndex = Int32.Parse(strSelected);
                if (!string.IsNullOrEmpty(strDupPolSelected.Trim()))
                {
                    drpDupCriteriaPolDown.SelectedIndex = Int32.Parse(strDupPolSelected);
                }
                else
                {
                    drpDupCriteriaPolDown.SelectedIndex = -1;
                    if (state.Value != "btnSaveClicked") chkDupClmatPolDown.Checked = false;
                }
                //Start : Changed By Nitika
                ViewState["PrinterName"] = null;
                ViewState["PaperBin"] = null;
                //End : Changed By Nitika
            }
            catch (Exception Ex)
            {
                ErrorHelper.logErrors(Ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(Ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally 
            
            {
                sNode = null;
                sNotes = null;
                bReturnStatus = false;
                sreturnValue = null;
            }
        }

        private XElement GetMessageTemplateForResClaimType()
        {
            string strClaimType = string.Empty;
            if (chkClaimType.Checked)
                strClaimType = "True";
            else
                strClaimType = string.Empty;

            if (ViewState["ClaimTypeSelectedValue"] != null)
            {
                hdnClaimReserveTypes.Text = ViewState["ClaimTypeSelectedValue"].ToString();
            }
            else
            {
                hdnClaimReserveTypes.Text = string.Empty;
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><Get>");
            sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness ><ResClaimType>" + strClaimType + "</ResClaimType><ClaimTypes>" +hdnClaimReserveTypes.Text + "</ClaimTypes></Get>");
            sXml.Append("</Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><Get>");
            sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness ><ResClaimType /><ClaimTypes /></Get>");
            sXml.Append("</Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            bool bSuccess;
            bSuccess = false;
            try
            {
                chkRBZero.Attributes["value"] = hdnoption1.Value;
                chkRAZero.Attributes["value"] = hdnoption2.Value;
                chkNRBZero.Attributes["value"] = hdnoption3.Value;
                chkDoNothing.Attributes["value"] = hdnoption4.Value;
                if (chkEvent.Checked == true)
                    lstEntities.Attributes["selected"] = null;
                if (chkDuplicate.Checked == true)
                    lstDupClaimCriteria.Attributes["selected"] = null;
                bool bReturnStatus = false;
                hdnValue.Text = ViewState["DupNoOfDays"].ToString();
                hdnPolClaimDupDays.Text = Convert.ToString(ViewState["PolClaimDupXDays"]);//RMA-12047
                bReturnStatus = CallCWS("LOBParmsAdaptor.Save", XmlTemplate, out sreturnValue, true, false);
                state.Value = "";
                //Start:Added by Nitin goel, MITS 30910,01/29/2013                
                XmlDoc.LoadXml(sreturnValue);
                if (XmlDoc.SelectSingleNode("//MsgStatusCd")!=null)
                {
                    //Start - Changed by Nikhil on 09/22/14.Code Review changes
                    //if (!XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText.ToLower().Equals("error"))
                    if (string.Compare(XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText.ToString(),"error",true) !=0)
                        //END - Changed by Nikhil on 09/22/14.Code Review changes
                    {
                        bSuccess = true;
                    }
                }
                if (bReturnStatus && bSuccess)
                //end:Added by Nitin goel
                {
                    ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                    XmlTemplate = GetMessageTemplateForResClaimType();

                    bReturnStatus = false;
                    bReturnStatus = CallCWS("LOBParmsAdaptor.Get", XmlTemplate, out sreturnValue, false, true);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);
                        lstLOB.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types[@selected='1']").Attributes["value"]).Value;
                        if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']") != null)
                        {
                            lstPeriodTypes.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']").Attributes["value"]).Value;
                        }
                        if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/CaptionTypes/Types[@selected='1']") != null)
                        {
                            lstCaptionTypes.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/CaptionTypes/Types[@selected='1']").Attributes["value"]).Value;
                        }

                        if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/LevelTypes/Types[@selected='1']") != null)
                        {
                            lstEntities.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/LevelTypes/Types[@selected='1']").Attributes["value"]).Value;
                        }
                        if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria/Types[@selected='1']") != null)
                        {
                            lstDupClaimCriteria.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria/Types[@selected='1']").Attributes["value"]).Value;
                        }
                        //RMA-12047
                        if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria/Types[@selected='1']") != null)
                        {
                            drpDupCriteriaPolDown.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria/Types[@selected='1']").Attributes["value"]).Value;
                        }
                        //RMA-12047
                        if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']") != null)
                        {
                            lstPeriodTypes.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ClaimOptions/PeriodTypes/Types[@selected='1']").Attributes["value"]).Value;
                        }
                        hdnLOBSelected.Text = ViewState["lobSelected"].ToString();
                        if (ViewState["ClaimType"] == null)
                        {
                            if (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/ClaimTypes/Types[@selected='1']") != null)
                            {
                                lstClaimType.SelectedValue = (XmlDoc.SelectSingleNode("//LOB/LOBParms/Options/ReserveOptions/ClaimTypes/Types[@selected='1']").Attributes["value"]).Value;
                            }
                        }
                        else
                        {
                            lstClaimType.SelectedIndex = (int)(ViewState["ClaimType"]);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorHelper.logErrors(Ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(Ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } 
        }

        private XElement getCritereaTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><SetCriteria>");
            sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness >");
            sXml.Append("<DupClaimCriteria>"+hdndupClaimCriterea.Text+"</DupClaimCriteria>");
            sXml.Append("<DupNoOfDays>" + hdnValue.Text+ "</DupNoOfDays>");
            sXml.Append("</SetCriteria></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
		//RMA-12047
        private XElement getCritereaTemplateForPolicyDup()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><SetCriteria>");
            //sXml.Append("<PolClaimDuplicateFlag>" + chkDupClmatPolDown.Checked + "</PolClaimDuplicateFlag>");
            sXml.Append("<LineOfBusiness>" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness>");
            sXml.Append("<PolClaimDuplicateCriteria>" + hdndupCriteriaAtPolDown.Text + "</PolClaimDuplicateCriteria>");
            sXml.Append("<PolClaimDupXDays>" + hdnPolClaimDupDays.Text + "</PolClaimDupXDays>");
            sXml.Append("</SetCriteria></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
		//RMA-12047
        public override void ModifyXml(ref XElement xelement)
        {
             string sfunctionName =(string) xelement.XPathSelectElement("//Call/Function").Value;
            if (sfunctionName == "LOBParmsAdaptor.UpdateSysClaimTypeReserve")
            {
                XElement Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves1");
                Xelement.Add(new XAttribute("type", "radio"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves2");
                Xelement.Add(new XAttribute("type", "radio"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves3");
                Xelement.Add(new XAttribute("type", "radio"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves4");
                Xelement.Add(new XAttribute("type", "radio"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AdjustReservesCheck");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AutoAdjustPerReserve");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/CollResBal");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/CollIncBal");
                Xelement.Add(new XAttribute("type", "checkbox"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/chkInsuff");
                Xelement.Add(new XAttribute("type", "checkbox"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/chkInsuffAutoPay");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/ResClaimType");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/NoResAdjBelowZero");
                Xelement.Add(new XAttribute("type", "checkbox"));

                //JIRA 857 Starts
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PreventCollectionForAllRsvTypes");
                Xelement.Add(new XAttribute("type", "checkbox"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PreventCollectionForPerRsvTypes");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //JIRA 857 Ends

                //asharma326 JIRA 871 Starts
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AddRecoveryReservetoTotalBalanceAmount");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //asharma326 JIRA 871 Ends
                //asharma326 JIRA 872 Starts
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AddRecoveryReservetoTotalIncurredAmount");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //asharma326 JIRA 872 Ends
                //asharma326 JIRA 870 Starts
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PerRsvCollInRsvBal");
                Xelement.Add(new XAttribute("type", "checkbox"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PerRsvCollInIncBal");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //asharma326 JIRA 870 Ends
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncClmTypeFlag");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncYearFlag");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncLobCodeFlag");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncOrgFlag");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/optTrigger");
                Xelement.Add(new XAttribute("type", "radio"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/Duplicate");
                Xelement.Add(new XAttribute("type", "checkbox"));

                //RMA-12047
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateFlag");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //RMA-12047


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseLimitTracking");
                Xelement.Add(new XAttribute("type", "checkbox"));


                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseAdjusterText");
                Xelement.Add(new XAttribute("type", "checkbox"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseEnhancedNotes");
                Xelement.Add(new XAttribute("type", "checkbox"));

                //Sumit - 09/21/2009 MITS#18227 Add "Use Enhanced Policy Flag" 
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseEnhPol");
                Xelement.Add(new XAttribute("type", "checkbox"));

                //spahariya MITS 29221 - Gap 12 - start
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/DirectToPrinter");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //spahariya MITS 29221 - Gap 12 - End
                //smahajan6 - 02/03/2010 MITS#18230 : Start - Add "Filter Property For Selected Policy"
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IsPolicyFilter");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //smahajan6 - 02/03/2010 MITS#18230 : End
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseClaimComments");
                Xelement.Add(new XAttribute("type", "checkbox"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/AutoCloseOptions/IncludeScheduledChecks");
                Xelement.Add(new XAttribute("type", "checkbox"));

                //rsushilaggar: ISO Claim Search
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseISOClaimSubmission");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //End rsushilaggar
                //start:Added by Nitin goel,MITS 30910,01/23
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/AllowToApplyDedPayments");
                Xelement.Add(new XAttribute("type", "checkbox"));

                //start - jira -6205.12/18/2014
              //  Xelement = null;
                //Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/DedRecTransactionType");
                //Xelement.Add(new XAttribute("type", "code"));
                //Xelement = null;
                //Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/DedRecReserveType");
                //Xelement.Add(new XAttribute("type", "code"));
                //end - jira -6205
             
                //end:Nitin goel
                //Start: Added by Sumit Agarwal to add Deductible settings parameter at Utility Level :10/10/2014
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/EnableDiminishingDedFirstParty");
                Xelement.Add(new XAttribute("type", "checkbox"));
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/EnableSharedAggregateDedThirdParty");
                Xelement.Add(new XAttribute("type", "checkbox"));
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/EnableDistributionDedAmtToSubsequentPmtThirdParty");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //End: Added by Sumit Agarwal to add Deductible settings parameter at Utility Level :10/10/2014

                //tanwar2 - mits 30910 - start
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/PreventPrintZeroCheck");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //tanwar2 - mits 30910 - end

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseClaimCommentsDisplayOnly");
                Xelement.Add(new XAttribute("type", "checkbox"));
                string strTemp = string.Empty;
                string strTmp = string.Empty;

                //MGaba2:R6 Reserve Worksheet :Start
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document//UseReserveWorksheet");
                Xelement.Add(new XAttribute("type", "checkbox"));

                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document//AttachReserveWorksheet");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //Mgaba2: R6 :End

                //nnorouzi; MITS 18989
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document//ReasonCommentsRequiredRSW");
                Xelement.Add(new XAttribute("type", "checkbox"));
                //nnorouzi; MITS 18989

                //1
                MultiCodeLookUp ctrllstAutoClose = (MultiCodeLookUp)this.Form.FindControl("lstAutoClose");
                strTemp = ctrllstAutoClose.CodeId;
                string strTmpClose = string.Empty;
                if (strTemp != string.Empty)
                {
                    strTmp = strTemp.Trim();
                    strTmpClose = strTmp.Replace(" ", ",");
                    strTmpClose = strTmpClose + ",";

                }
                //2
                Xelement = null;
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                XElement xGetAutoCloseOptions = new XElement("GetAutoClose");
                xGetAutoCloseOptions.Value = strTmpClose;
                Xelement.Add(xGetAutoCloseOptions);
                MultiCodeLookUp ctrllstAutoadj = (MultiCodeLookUp)this.Form.FindControl("lstAutoAdj");
                strTemp = string.Empty;
                strTemp = ctrllstAutoadj.CodeId;
                string strTmpAuto = string.Empty;
                if (strTemp != string.Empty)
                {
                    strTmp = string.Empty;
                    strTmp = strTemp.Trim();
                    strTmpAuto = strTmp.Replace(" ", ",");
                    strTmpAuto = strTmpAuto + ",";

                }
                //3
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                XElement xGetAutoAdjustOptions = new XElement("GetAutoAdjust");
                xGetAutoAdjustOptions.Value = strTmpAuto;
                Xelement.Add(xGetAutoAdjustOptions);
                //JIRA 857 Start
                MultiCodeLookUp ctrllstRsvColl = (MultiCodeLookUp)this.Form.FindControl("lstRsvColl");
                strTemp = string.Empty;
                strTemp = ctrllstRsvColl.CodeId;
                strTmpAuto = string.Empty;
                if (strTemp != string.Empty)
                {
                    strTmp = string.Empty;
                    strTmp = strTemp.Trim();
                    strTmpAuto = strTmp.Replace(" ", ",");
                    strTmpAuto = strTmpAuto + ",";
                }
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                XElement xGetCollOptions = new XElement("GetCollOptions");
                xGetCollOptions.Value = strTmpAuto;
                Xelement.Add(xGetCollOptions);
                //JIRA 857 Ends
                
                strTemp = string.Empty;
                MultiCodeLookUp ctrllstReserve = (MultiCodeLookUp)this.Form.FindControl("lstReserveTypes");
                strTemp = ctrllstReserve.CodeId;
                string strTmpRes = string.Empty;
                if (strTemp != string.Empty)
                {
                    strTmp = string.Empty;
                    strTmp = strTemp.Trim();
                    strTmpRes = strTmp.Replace(" ", ",");
                    strTmpRes = strTmpRes + ",";
                }
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                XElement xGetReserveType = new XElement("GetClaimReserveTypes");
                xGetReserveType.Value = strTmpRes;
                Xelement.Add(xGetReserveType);//Asif

                //MGaba2:R6 Reserve Worksheet :Start
                MultiCodeLookUp objRSWClaimTypes = (MultiCodeLookUp)this.Form.FindControl("lstRSWClaimType");
                strTemp = objRSWClaimTypes.CodeId;
                strTmpRes = string.Empty;
                if (strTemp != string.Empty)
                {
                    strTmp = string.Empty;
                    strTmp = strTemp.Trim();
                    strTmpRes = strTmp.Replace(" ", ",");
                    strTmpRes = strTmpRes + ",";
                }
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                xGetReserveType = new XElement("GetAllRSWClaimTypes");
                xGetReserveType.Value = strTmpRes;
                Xelement.Add(xGetReserveType);

                MultiCodeLookUp objRSWClaimStatus = (MultiCodeLookUp)this.Form.FindControl("lstRSWClaimStatus");
                strTemp = objRSWClaimStatus.CodeId;
                strTmpRes = string.Empty;
                if (strTemp != string.Empty)
                {
                    strTmp = string.Empty;
                    strTmp = strTemp.Trim();
                    strTmpRes = strTmp.Replace(" ", ",");
                    strTmpRes = strTmpRes + ",";
                }
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                xGetReserveType = new XElement("GetAllRSWClaimStatus");
                xGetReserveType.Value = strTmpRes;
                Xelement.Add(xGetReserveType);

                //MGaba2:R6:End

                //asharma326 jira 870 Starts
                //for Balance MultiCode lookup
                MultiCodeLookUp objlstPerRsvTypeRecovery = (MultiCodeLookUp)this.Form.FindControl("lstPerRsvTypeRecovery");
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                XElement xGetAllPerReserveTypesBalance = new XElement("GetAllPerReserveTypesBalance");
                xGetAllPerReserveTypesBalance.Value = string.Join(",", objlstPerRsvTypeRecovery.CodeId.Split(' '));
                Xelement.Add(xGetAllPerReserveTypesBalance);

                //for incurred MultiCode lookup
                MultiCodeLookUp objlstPerRsvTypeIncurred = (MultiCodeLookUp)this.Form.FindControl("lstPerRsvTypeIncurred");
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
                XElement xGetAllPerReserveTypesIncurred = new XElement("GetAllPerReserveTypesIncurred");
                xGetAllPerReserveTypesIncurred.Value = string.Join(",", objlstPerRsvTypeIncurred.CodeId.Split(' '));
                Xelement.Add(xGetAllPerReserveTypesIncurred);
                //asharma326 jira 870 Ends


                XElement UpdateSysClaimTypeReserve = new XElement("UpdateSysClaimTypeReserve");
                UpdateSysClaimTypeReserve.Add(xelement);
            }
            if (sfunctionName == "LOBParmsAdaptor.Get" && IsPostBack)
            {
            MultiCodeLookUp MuserControl = (MultiCodeLookUp)this.Form.FindControl("lstReserveTypes");
            ListBox lstUserControl = (ListBox)MuserControl.FindControl("multicode");
            lstUserControl.Items.Clear();
            MultiCodeLookUp MautoAdj = (MultiCodeLookUp)this.Form.FindControl("lstAutoAdj");
            ListBox lstAutoAdj = (ListBox)MautoAdj.FindControl("multicode");
            lstAutoAdj.Items.Clear();
            //JIRA-857 Starts
            MultiCodeLookUp MCollOpt = (MultiCodeLookUp)this.Form.FindControl("lstRsvColl");
            ListBox lstRsvColl = (ListBox)MCollOpt.FindControl("multicode");
            lstRsvColl.Items.Clear();
            //JIRA-857 Ends
            MultiCodeLookUp MautoClose = (MultiCodeLookUp)this.Form.FindControl("lstAutoClose");
            ListBox lstAutoClose = (ListBox)MautoClose.FindControl("multicode");
            lstAutoClose.Items.Clear();
                //MGaba2:R6 Reserve Worksheet :Start
            MultiCodeLookUp ctrllstRSWClaimType = (MultiCodeLookUp)this.Form.FindControl("lstRSWClaimType");
            ListBox lstRSWClaimType = (ListBox)ctrllstRSWClaimType.FindControl("multicode");
            lstRSWClaimType.Items.Clear();

            MultiCodeLookUp ctrllstRSWClaimStatus = (MultiCodeLookUp)this.Form.FindControl("lstRSWClaimStatus");
            ListBox lstRSWClaimStatus = (ListBox)ctrllstRSWClaimStatus.FindControl("multicode");
            lstRSWClaimStatus.Items.Clear();
                //MGaba2:R6:End
            //asharma326 JIRA 870 Starts
            MultiCodeLookUp MlstPerRsvTypeRecovery = (MultiCodeLookUp)this.Form.FindControl("lstPerRsvTypeRecovery");
            ListBox lstPerRsvTypeRecovery = (ListBox)MlstPerRsvTypeRecovery.FindControl("multicode");
            lstPerRsvTypeRecovery.Items.Clear();

            MultiCodeLookUp MlstPerRsvTypeIncurred = (MultiCodeLookUp)this.Form.FindControl("lstPerRsvTypeIncurred");
            ListBox lstPerRsvTypeIncurred = (ListBox)MlstPerRsvTypeIncurred.FindControl("multicode");
            lstPerRsvTypeIncurred.Items.Clear();
                //asharma326 JIRA 870 Starts
            }

            if (sfunctionName== "LOBParmsAdaptor.Save")
            {
            XElement Xelement = null;
            if (chkEvent.Checked == false)
            {
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/LevelTypes");
                Xelement.Value =string.Empty;
            }
            if (chkDuplicate.Checked == false)
            {
                Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria");
                Xelement.Value = string.Empty;
            }
             Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves1");
            Xelement.Add(new XAttribute("type", "radio"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves2");
            Xelement.Add(new XAttribute("type","radio"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves3");
            Xelement.Add(new XAttribute("type", "radio"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves4");
            Xelement.Add(new XAttribute("type", "radio"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AdjustReservesCheck");
            Xelement.Add(new XAttribute("type", "checkbox"));

            
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AutoAdjustPerReserve");
            Xelement.Add(new XAttribute("type", "checkbox"));
          

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/CollResBal");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/CollIncBal");
            Xelement.Add(new XAttribute("type", "checkbox"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/chkInsuff");
            Xelement.Add(new XAttribute("type", "checkbox"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/chkInsuffAutoPay");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/ResClaimType");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/NoResAdjBelowZero");
            Xelement.Add(new XAttribute("type", "checkbox"));
			//JIRA 857 Starts
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PreventCollectionForAllRsvTypes");
            Xelement.Add(new XAttribute("type", "checkbox"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PreventCollectionForPerRsvTypes");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //JIRA 857 Ends

            //asharma326 JIRA 871 Starts
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AddRecoveryReservetoTotalBalanceAmount");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //asharma326 JIRA 871 Ends

            //asharma326 JIRA 872 Starts
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/AddRecoveryReservetoTotalIncurredAmount");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //asharma326 JIRA 872 Ends
            //asharma326 JIRA 870 Starts
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PerRsvCollInRsvBal");
            Xelement.Add(new XAttribute("type", "checkbox"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ReserveOptions/PerRsvCollInIncBal");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //asharma326 JIRA 870 Ends

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncClmTypeFlag");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncYearFlag");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncLobCodeFlag");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IncOrgFlag");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/optTrigger");
            Xelement.Add(new XAttribute("type", "radio"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/Duplicate");
            Xelement.Add(new XAttribute("type", "checkbox"));

            //RMA-12047
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateFlag");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //RMA-12047
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseLimitTracking");
            Xelement.Add(new XAttribute("type", "checkbox"));


            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseAdjusterText");
            Xelement.Add(new XAttribute("type", "checkbox"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseEnhancedNotes");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //Sumit - 09/21/2009 MITS#18227 Add "Use Enhanced Policy Flag" 
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseEnhPol");
            Xelement.Add(new XAttribute("type", "checkbox"));

            //spahariya MITS 29221 - Gap 12 - start
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/DirectToPrinter");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //spahariya MITS 29221 - Gap 12 - End
            //smahajan6 - 02/03/2010 MITS#18230 : Start - Add "Filter Property For Selected Policy" 
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/IsPolicyFilter");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //smahajan6 - 02/03/2010 MITS#18230 : End

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseClaimComments");
            Xelement.Add(new XAttribute("type", "checkbox"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/AutoCloseOptions/IncludeScheduledChecks");
            Xelement.Add(new XAttribute("type", "checkbox"));

            //rsushilaggar: ISO Claim Search
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseISOClaimSubmission");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //end: rsushilaggar
            //start:Added by Nitin goel,MITS 30910,01/23
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/AllowToApplyDedPayments");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //Xelement = null;
            //Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/AllowEditDeductibleReserve");
            //Xelement.Add(new XAttribute("type", "checkbox"));
            //end:Nitin goel

            //Start: Added by Sumit Agarwal to add Deductible settings parameter at Utility Level :10/10/2014
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/EnableDiminishingDedFirstParty");
            Xelement.Add(new XAttribute("type", "checkbox"));
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/EnableSharedAggregateDedThirdParty");
            Xelement.Add(new XAttribute("type", "checkbox"));
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/EnableDistributionDedAmtToSubsequentPmtThirdParty");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //End: Added by Sumit Agarwal to add Deductible settings parameter at Utility Level :10/10/2014

            //tanwar2 - mits 30910 - start
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/DeductibleOptions/PreventPrintZeroCheck");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //tanwar2 - mits 30910 - end

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms/Options/ClaimOptions/UseClaimCommentsDisplayOnly");
            Xelement.Add(new XAttribute("type", "checkbox"));
           
            //MGaba2:R6 Reserve Worksheet :Start
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document//UseReserveWorksheet");
            Xelement.Add(new XAttribute("type", "checkbox"));

            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document//AttachReserveWorksheet");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //Mgaba2: R6 :End

            //nnorouzi; MITS 18989
            Xelement = xelement.XPathSelectElement(".//Document//ReasonCommentsRequiredRSW");
            Xelement.Add(new XAttribute("type", "checkbox"));
            //nnorouzi; MITS 18989

                string strTemp = string.Empty;
                string strTmp = string.Empty;
                MultiCodeLookUp ctrllstAutoClose = (MultiCodeLookUp)this.Form.FindControl("lstAutoClose");
                strTemp = ctrllstAutoClose.CodeId;
                string strTmpClose = string.Empty;
                if(strTemp!=string.Empty)
                {
                     strTmp = strTemp.Trim();
                   strTmpClose= strTmp.Replace(" ",",");
                  strTmpClose =strTmpClose + ",";
             
                }
            Xelement = null;
            Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
            XElement xGetAutoCloseOptions = new XElement("GetAutoClose");
            xGetAutoCloseOptions.Value =strTmpClose;
            Xelement.Add(xGetAutoCloseOptions); 
            MultiCodeLookUp ctrllstAutoadj = (MultiCodeLookUp)this.Form.FindControl("lstAutoAdj");
            strTemp = string.Empty;
            strTemp = ctrllstAutoadj.CodeId;
            string strTmpAuto = string.Empty;
              if (strTemp != string.Empty)
              {
                  strTmp = string.Empty;
                  strTmp = strTemp.Trim();
                  strTmpAuto= strTmp.Replace(" ",",");
                 strTmpAuto = strTmpAuto+ ","; 
              }
               Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
              XElement xGetAutoAdjustOptions = new XElement("GetAutoAdjust");
              xGetAutoAdjustOptions.Value = strTmpAuto;
               Xelement.Add(xGetAutoAdjustOptions);            
            //JIRA 857 Start
               MultiCodeLookUp ctrllstRsvColl = (MultiCodeLookUp)this.Form.FindControl("lstRsvColl");
               strTemp = string.Empty;
               strTemp = ctrllstRsvColl.CodeId;
               strTmpAuto = string.Empty;
               if (strTemp != string.Empty)
               {
                   strTmp = string.Empty;
                   strTmp = strTemp.Trim();
                   strTmpAuto = strTmp.Replace(" ", ",");
                   strTmpAuto = strTmpAuto + ",";
               }
               Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
               XElement xGetCollOptions = new XElement("GetCollOptions");
               xGetCollOptions.Value = strTmpAuto;
               Xelement.Add(xGetCollOptions);
            //JIRA 857 Ends
              
              strTemp = string.Empty;
              MultiCodeLookUp ctrllstReserve = (MultiCodeLookUp)this.Form.FindControl("lstReserveTypes");
              strTemp = ctrllstReserve.CodeId;
              string strTmpRes = string.Empty;
              if (strTemp != string.Empty)
              {
                  strTmp = string.Empty;
                  strTmp = strTemp.Trim();
                  strTmpRes = strTmp.Replace(" ", ",");
                  strTmpRes = strTmpRes + ",";
              }
              Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
              XElement xGetReserveType = new XElement("GetClaimReserveTypes");
              xGetReserveType.Value = strTmpRes;
              Xelement.Add(xGetReserveType);

                //MGaba2:R6 Reserve Worksheet :Start
              MultiCodeLookUp objRSWClaimTypes = (MultiCodeLookUp)this.Form.FindControl("lstRSWClaimType");
              strTemp = objRSWClaimTypes.CodeId;
              strTmpRes = string.Empty;
              if (strTemp != string.Empty)
              {
                  strTmp = string.Empty;
                  strTmp = strTemp.Trim();
                  strTmpRes = strTmp.Replace(" ", ",");
                  strTmpRes = strTmpRes + ",";
              }
              Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
              xGetReserveType = new XElement("GetAllRSWClaimTypes");
              xGetReserveType.Value = strTmpRes;
              Xelement.Add(xGetReserveType);

              MultiCodeLookUp objRSWClaimStatus = (MultiCodeLookUp)this.Form.FindControl("lstRSWClaimStatus");
              strTemp = objRSWClaimStatus.CodeId;
              strTmpRes = string.Empty;
              if (strTemp != string.Empty)
              {
                  strTmp = string.Empty;
                  strTmp = strTemp.Trim();
                  strTmpRes = strTmp.Replace(" ", ",");
                  strTmpRes = strTmpRes + ",";
              }
              Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
              xGetReserveType = new XElement("GetAllRSWClaimStatus");
              xGetReserveType.Value = strTmpRes;
              Xelement.Add(xGetReserveType);

                //MGaba2:R6:End
              //asharma326 jira 870 Starts
              //for Balance MultiCode lookup
              MultiCodeLookUp objlstPerRsvTypeRecovery = (MultiCodeLookUp)this.Form.FindControl("lstPerRsvTypeRecovery");
              Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
              XElement xGetAllPerReserveTypesBalance = new XElement("GetAllPerReserveTypesBalance");
              xGetAllPerReserveTypesBalance.Value = string.Join(",", objlstPerRsvTypeRecovery.CodeId.Trim().Split(' '));
              Xelement.Add(xGetAllPerReserveTypesBalance);

              //for incurred MultiCode lookup
              MultiCodeLookUp objlstPerRsvTypeIncurred = (MultiCodeLookUp)this.Form.FindControl("lstPerRsvTypeIncurred");
              Xelement = xelement.XPathSelectElement(".//Document/LOB/LOBParms");
              XElement xGetAllPerReserveTypesIncurred = new XElement("GetAllPerReserveTypesIncurred");
              xGetAllPerReserveTypesIncurred.Value = string.Join(",", objlstPerRsvTypeIncurred.CodeId.Trim().Split(' '));
              Xelement.Add(xGetAllPerReserveTypesIncurred);
                //asharma326 jira 870 Ends
            }    
        }
        //spahariya MITS 29221 07/27/2012 add printer settings for Auto Mail merge-- Start
        protected void ddlPrinters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DirectToPrinter.Checked = true;
                PrintDocument oPrintDocument = new PrintDocument();

                oPrintDocument.PrinterSettings.PrinterName = ddlPrinters.SelectedValue;
                ddlPbfl.Items.Clear();                
                foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                {
                    ListItem item = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                    ddlPbfl.Items.Add(item);                   
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //spahariya MITS 29221 - End
    }
}
