<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegionalFormats.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.RegionalFormats" ValidateRequest="false" %>


<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
   <title>Regional Formats</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css"  type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">      { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">      { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">      { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
     <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Regional Formats" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>   
            <tr>
                <td>
                    <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageSave" AlternateText="Save" ValidationGroup="vgSave" OnClick="btnOk_Click" />
                </td>
            </tr>  
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblCountry" Text="Country:" />
                </td>
                <td>
                   &nbsp;&nbsp;&nbsp;
                   <asp:TextBox runat="server" Enabled="false" ID="txtCountry" 
                   RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='Country']" TabIndex="1" MaxLength = "9" type="text" />
                </td>
            </tr>
            <tr>
            &nbsp;
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblZipCode" Text="ZipCode Format:" />
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="drpZipCodeFormats"  type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ZipCodeFormats']/@value"
                    itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='ZipCodeFormats']"    onchange="setDataChanged(true);" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
            &nbsp;
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblPhoneFormat" Text="Phone Format:" />
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="drpPhoneFormats"  type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PhoneFormats']/@value"
                     itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='PhoneFormats']"   onchange="setDataChanged(true);" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblSSNFormat" Text="SSN Format:" />
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                    <asp:ListBox ID="lbSSNFormats" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SSNFormats']/@value"
                     itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='SSNFormats']" onchange="setDataChanged(true);" runat="server" SelectionMode="Multiple">
                        </asp:ListBox>
                </td>
                 </tr>
            <tr>
                  <td>
                    <asp:Label runat="server" class="required" ID="lblIsNumeric" Text="Is SSN Numeric">

                    </asp:Label>
                </td>
                 <td>
                       &nbsp;&nbsp;&nbsp;
                     <asp:CheckBox Id="cbSSNFormats" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SSNFormats']/@isnumeric"
                         itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='SSNFormats']" runat="server" >

                     </asp:CheckBox>
                 </td>
               
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/option/RowId" rmxignoreset='true' />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="RegionalFormatsIDLoadvalue" />
    <br />
    </form>
</body>
</html>
