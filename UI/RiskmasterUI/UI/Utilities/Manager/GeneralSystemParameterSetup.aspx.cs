﻿/***************************************************************************************************
 *   Date     |  MITS/JIRA  | Programmer | Description                                            
 ***************************************************************************************************
 * 7/24/2014  | RMA-718     | ajohari2   | Changes for TPA Access ON/OFF
 ***************************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Common;
using System.Text;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class GeneralSystemParameterSetup :NonFDMBasePageCWS
    {
       private XElement XmlTemplate = null;
       private bool bReturnStatus = false;
       private string sreturnValue = string.Empty;
       private XmlDocument XmlDoc = new XmlDocument();
       private string sNode = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (EnhanceNotesPrintOrder1.SelectedIndex == 0)
                {
                    EnhanceNotesPrintOrder1.SelectedItem.Text = string.Empty;
                }
                if (EnhanceNotesPrintOrder2.SelectedIndex == 0)
                {
                    EnhanceNotesPrintOrder2.SelectedItem.Text = string.Empty;
                }
                if (EnhanceNotesPrintOrder3.SelectedIndex == 0)
                {
                    EnhanceNotesPrintOrder3.SelectedItem.Text = string.Empty;
                }

                // psharma206  smtp cloud start
                if (ConfigurationManager.AppSettings["CloudDeployed"].ToLower() == "true")
                {
                    SmtpSvr.Enabled = false;
                }
                // psharma206  smtp cloud end

                if (!IsPostBack)
                {
                    bReturnStatus = CallCWS("SystemParameterAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                    if (bReturnStatus)
                    {
                        ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                        XmlDoc.LoadXml(sreturnValue);
                        if (XmlDoc.SelectSingleNode("//form/group/displaycolumn/control[@name='OracleCaseIns']") != null)
                        {
                            sNode = (string)XmlDoc.SelectSingleNode("//form/group/displaycolumn/control[@name='OracleCaseIns']/@hideExceptOracle").Value;
                            hdnOracleCaseInsensitive.Text = sNode;
                        } 

                        //Sumit - 09/21/2009 MITS# Disable Billing Flag
                        if (XmlDoc.SelectSingleNode("//form/DisableControl") != null)
                        {
                            sNode = (string)XmlDoc.SelectSingleNode("//form/DisableControl").InnerText;
                            if (sNode.Equals("BillingFlag"))
                            {
                                BillingFlag.Enabled = false;
                            }
                        }

                        //Start:Add by kuladeep 04/20/2010 Retrofit for MITS 18278
                        if (XmlDoc.SelectSingleNode("//form/EnhPolicyStatus") != null)
                        {
                            sNode = (string)XmlDoc.SelectSingleNode("//form/EnhPolicyStatus").InnerText;
                            if(sNode.Equals("4"))
                            {
                            ShowJurisdiction.Checked = false;
                            ShowJurisdiction.Enabled = false;
                            }
                        }
                        //End:Add by kuladeep 04/20/2010 Retrofit for MITS 18278
                        //Deb:BOB
                        if (XmlDoc.SelectSingleNode("//systemcontrol[@name='MultiCovgPerClm']") != null)
                        {
                            if (XmlDoc.SelectSingleNode("//systemcontrol[@name='MultiCovgPerClm']").InnerText == "True")
                            {
                                //Mona: R8.2 : BRS in Carrier claim :start
                             //   UseBRS.Enabled = false;
                                AutoSelectPolicy.Enabled = false;
                                UseVALOB.Enabled = false;
                                UseDILOB.Enabled = false;
                                UsePCLOB.Enabled = false;
                                UsePolicyInterface.Enabled = true;
                                UnitStat.Enabled = true;
                                AccOwnrFnds.Enabled = false;
                                //zmohammad MITS 33047 : Disabling Time and Expense module settings start
                                UseTnE.Enabled = false;
                                RateOrgLvl.Enabled = false;
                                BankAccount.Enabled = false;
                                UseAllTransTypes.Enabled = false;
                                //zmohammad MITS 33047 : Disabling Time and Expense module settings end
                                //Added by Amitosh for R8 enhancement of Policy interface
                                UseCLUEReportingFields.Enabled = true;  //added by swati
                            }
                            else
                            {
                                UsePolicyInterface.Enabled = false;
                                //dbisht6 RMA-15600
                                UnitStat.Enabled = false;
                                UnitStat.Checked = false;
                                //dbisht6 end
                                //zmohammad MITS 33047 : Disabling Time and Expense module settings start
                                UseTnE.Enabled = true;
                                RateOrgLvl.Enabled = true;
                                BankAccount.Enabled = true;
                                UseAllTransTypes.Enabled = true;
                                //zmohammad MITS 33047 : Disabling Time and Expense module settings end
                                //added by swati
                                UseCLUEReportingFields.Enabled = false;
                                UseCLUEReportingFields.Checked = false;
                                //END BY SWATI
                                //JIRA RMA-16584 nshah28(Do not visible for corporate) start
                                AllowSummaryForBookedReserve.Enabled = false; 
                                AllowSummaryForBookedReserve.Checked = false;
                                //END BY nshah28
                            }
                        }
                        else
                        {
                            UsePolicyInterface.Enabled = false;
                        }
                        //AA Address Master 8753
                        if (XmlDoc.SelectSingleNode("//systemcontrol[@name='UseMultipleAddresses']") != null)
                        {
                            if (XmlDoc.SelectSingleNode("//systemcontrol[@name='UseMultipleAddresses']").InnerText == "True")
                            {
                                AddModifyAddress.Enabled = true;
                            }
                            else
                            {
                                AddModifyAddress.Enabled = false;
                                AddModifyAddress.Checked = false;
                            }
                        }
                        else
                        {
                            AddModifyAddress.Enabled = false;
                            AddModifyAddress.Checked = false;

                        }
                        //AA end
                        if (UsePolicyInterface.Checked)
                        {
                           // UsePolicyInterface1.Enabled = true;
                            PolicyType2.Enabled = true;
                            PolicyType1.Enabled = true;
                           // UsePolicyInterface2.Enabled = true;
                            AllowPolicySearch.Enabled = true; //averma62 MITS 25163- Policy Interface Implementation
                            FetchRecOnPolicySearch.Enabled = true;  //aaggarwal29 changes for Point Policy Interface
                         //   UploadSuppToPolicy.Enabled = true;
                            UseCodeMapping.Enabled = true;
                            EnableTPAAccess.Enabled = true; //JIRA RMA-718 ajohari2
							FNOLReserve.Enabled = true;
							//tanwar2 - WWIG - Requirement 1.4.1 - start
                            NoReqFieldsForPolicySearch.Enabled = true;
                            //tanwar2 - WWIG - Requirement 1.4.2 - end
                           //Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
                            UploaddCheckTotal.Enabled = true;
                        }
                        else
                        {
                            PolicyType1.Enabled = false;
                            PolicyType2.Enabled = false;
                          //  UsePolicyInterface1.Enabled = false;
                          //  UsePolicyInterface2.Enabled = false;
                            AllowPolicySearch.Enabled = false; //averma62 MITS 25163- Policy Interface Implementation
                            FetchRecOnPolicySearch.Enabled = false;   //aaggarwal29 changes for Point Policy Interface
                          //  UploadSuppToPolicy.Enabled = false;
                            UseCodeMapping.Enabled = false;
                            EnableTPAAccess.Enabled = false; //JIRA RMA-718 ajohari2
							FNOLReserve.Enabled = false;
							//tanwar2 - WWIG - Requirement 1.4.1 - start
                            NoReqFieldsForPolicySearch.Enabled = false;
                            //tanwar2 - WWIG - Requirement 1.4.2 - end
                            //Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
                            UploaddCheckTotal.Enabled = false;
                        }
                        //rsharma220 MITS 33838 Start
                        if (MultiCovgPerClm.Checked)
                        {
                            UseTnE.Enabled = false;
                            UseTnE.Checked = false;
                            RateOrgLvl.Enabled = false;
                            BankAccount.Enabled = false;
                            UseAllTransTypes.Enabled = false;
                            UseAllTransTypes.Checked = false;
                            UseCLUEReportingFields.Enabled = true;  //added by swati
                        }
                        else
                        {
                            UseTnE.Enabled = true;
                            RateOrgLvl.Enabled = true;
                            BankAccount.Enabled = true;
                            UseAllTransTypes.Enabled = true;
                            //added by swati
                            UseCLUEReportingFields.Enabled = false;
                            UseCLUEReportingFields.Checked = false;
                            //END BY SWATI
                        }
                        //rsharma220 MITS 33838 End
                        //MGaba2:MITS 29111         
                        //skhare7 MITS 28805
                        //if (BillReviewFee.Checked)
                        //{

                        //    OrgBillRevFee.Enabled = true;

                            
                        //    OrgEntlastfirstname.ReadOnly = false;
                        //    OrgEntlastfirstnamebtn.Enabled = true;


                        //}
                        //else
                        //{
                        //    OrgBillRevFee.Enabled = false;

                        //    OrgEntlastfirstname.Text = string.Empty;
                        //    OrgEntentityid.Text = string.Empty;
                        //    OrgEntlastfirstname.ReadOnly = true;
                        //    OrgEntlastfirstnamebtn.Enabled = false;
                            
                        
                        //}
                        //skhare7 MITS 28805 End
                    }					

                }
                if (IsPostBack)
                {
                    if (hdAction.Text == "SubBankAcct")
                    {
                        CallCWS("SystemParameterAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                        string script = "<script>setDataChanged(true);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
                        hdAction.Text = string.Empty;
                    }
                    //if (hdAction.Text == "CountryChanged")
                    //{
                    //    CallCWS("SystemParameterAdaptor.Get", XmlTemplate, out sreturnValue, true, true);
                    //    Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", "<script type='text/javascript'>setDataChanged(true); tabChange('Globalization');</script>");
                    //    hdAction.Text = string.Empty;
                    //}
                }
                //Ankit Start : Worked on MITS - 32386
                if (chkFASEnable.Checked)
                {
                    //Riskmaster.Security.Encryption.RMCryptography.EncryptString
                    if (XmlDoc.SelectSingleNode("//form/SystemSettings/group/displaycolumn/systemcontrol[@name='FASPassword']") != null)
                    {
                        txtFASPassword.Attributes.Add("value", XmlDoc.SelectSingleNode("//form/SystemSettings/group/displaycolumn/systemcontrol[@name='FASPassword']").InnerText);
                    }
                }
                //Ankit End
                //igupta3 code done from javascript.
                //if (chkCurrentDateForDiaries.Checked)
                //    chkNotifyAssignerOfCompletion.Enabled = true;
                //else
                //    chkNotifyAssignerOfCompletion.Enabled = false;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        //Ashish Ahuja
        private XElement GetMessageTemplate(string sFunctionName)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append(sFunctionName);
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><DupSSN><IgnoreForFuture>");
            sXml = sXml.Append(IgnoreSSNChecking.Checked);
            sXml = sXml.Append("</IgnoreForFuture></DupSSN></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        //Ashish Ahuja : end
        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
           
               // bReturnStatus = CallCWSFunction("SystemParameterAdaptor.Save");
                bReturnStatus = CallCWS("SystemParameterAdaptor.Save", XmlTemplate, out sreturnValue, true, true);
                if (bReturnStatus)
                {
                    ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                    //Removing this unnecessary call and also it override any error comes during save
                    //bReturnStatus = CallCWSFunction("SystemParameterAdaptor.Get");
                    HttpContext.Current.Session["IsBOB"] = MultiCovgPerClm.Checked; //Added by Amitosh for adding Carrier Claim settings in Session.
                    //Added by amitosh for mits 25163
                    XmlDoc.LoadXml(sreturnValue);
                    if (XmlDoc.SelectSingleNode("//systemcontrol[@name='MultiCovgPerClm']") != null)
                    {
                        if (XmlDoc.SelectSingleNode("//systemcontrol[@name='MultiCovgPerClm']").InnerText == "True")
                        {
                            //Mona: R8.2 : BRS in Carrier claim :start
                            //UseBRS.Enabled = false;
                            AutoSelectPolicy.Enabled = false;
                            UseVALOB.Enabled = false;
                            UseDILOB.Enabled = false;
                            UsePCLOB.Enabled = false;
                            UsePolicyInterface.Enabled = true;
                            UnitStat.Enabled = true;
                            //zmohammad MITS 33047 : Disabling Time and Expense module settings start
                            UseTnE.Enabled = false;
                            RateOrgLvl.Enabled = false;
                            BankAccount.Enabled = false;
                            UseAllTransTypes.Enabled = false;
                            //zmohammad MITS 33047 : Disabling Time and Expense module settings end
                                
                        }
                        //Added by Amitosh for R8 enhancement of Policy interface
                        else
                        {
                            UsePolicyInterface.Enabled = false;
                            //dbisht6 RMA-15600
                            UnitStat.Enabled = false;
                            UnitStat.Checked = false;
                            //dbisht6 end
                            //zmohammad MITS 33047 : Disabling Time and Expense module settings start
                            UseTnE.Enabled = true;
                            RateOrgLvl.Enabled = true;
                            BankAccount.Enabled = true;
                            UseAllTransTypes.Enabled = true;
                            //zmohammad MITS 33047 : Disabling Time and Expense module settings end
                        }
                    }
                    else
                    {
                        UsePolicyInterface.Enabled = false;
                    }
                    //AA Address Master 8753
                    if (XmlDoc.SelectSingleNode("//systemcontrol[@name='UseMultipleAddresses']") != null)
                    {
                        if (XmlDoc.SelectSingleNode("//systemcontrol[@name='UseMultipleAddresses']").InnerText == "True")
                        {
                            AddModifyAddress.Enabled = true;
                        }
                        else
                        {
                            AddModifyAddress.Enabled = false;
                            AddModifyAddress.Checked = false;
                        }
                    }
                    else
                    {
                            AddModifyAddress.Enabled = false;
                            AddModifyAddress.Checked = false;
                        
                    }
                    //AA end
                    if (UsePolicyInterface.Checked)
                    {
                     //   UsePolicyInterface1.Enabled = true;
                       // UsePolicyInterface2.Enabled = true;
                        PolicyType2.Enabled = true;
                        PolicyType1.Enabled = true;
                        AllowPolicySearch.Enabled = true; //averma62 MITS 25163- Policy Interface Implementation
                        FetchRecOnPolicySearch.Enabled = true; //aaggarwal29 changes for Point Policy Interface
                        //UploadSuppToPolicy.Enabled = true;
                        UseCodeMapping.Enabled = true;
                        EnableTPAAccess.Enabled = true; //JIRA RMA-718 ajohari2
                        //tanwar2 - WWIG - Requirement 1.4.1 - start
                        NoReqFieldsForPolicySearch.Enabled = true;
                        //tanwar2 - WWIG - Requirement 1.4.2 - end
                        //Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
                        UploaddCheckTotal.Enabled = true;
                      
                    }
                    else
                    {
                    //    UsePolicyInterface1.Enabled = false;
                     //   UsePolicyInterface2.Enabled = false;
                        PolicyType2.Enabled = false;
                        PolicyType1.Enabled = false;
                        AllowPolicySearch.Enabled = false; //averma62 MITS 25163- Policy Interface Implementation
                        FetchRecOnPolicySearch.Enabled = false;  //aaggarwal29 changes for Point Policy Interface
                    //    UploadSuppToPolicy.Enabled = false;
                        UseCodeMapping.Enabled = false;
                        EnableTPAAccess.Enabled = false; //JIRA RMA-718 ajohari2
                        //tanwar2 - WWIG - Requirement 1.4.1 - start
                        NoReqFieldsForPolicySearch.Enabled = false;
                        //tanwar2 - WWIG - Requirement 1.4.2 - end
                        //Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
                        UploaddCheckTotal.Enabled = false;

                      
                    }
                    //rsharma220 MITS 33838 Start
                    if (MultiCovgPerClm.Checked)
                    {
                        UseTnE.Enabled = false;
                        UseTnE.Checked = false;
                        RateOrgLvl.Enabled = false;
                        BankAccount.Enabled = false;
                        UseAllTransTypes.Enabled = false;
                        UseAllTransTypes.Checked = false;
                        UseCLUEReportingFields.Enabled = true;  //added by swati
                    }
                    else
                    {
                        UseTnE.Enabled = true;
                        RateOrgLvl.Enabled = true;
                        BankAccount.Enabled = true;
                        UseAllTransTypes.Enabled = true;
                        //added by swati
                        UseCLUEReportingFields.Enabled = false;
                        UseCLUEReportingFields.Checked = false;
                        //END BY SWATI
                    }
                    //Asharma326 MITS 32386 Starts
                    if (chkFASEnable.Checked)
                    {
                        //Riskmaster.Security.Encryption.RMCryptography.EncryptString
                        if (XmlDoc.SelectSingleNode("//form/SystemSettings/group/displaycolumn/systemcontrol[@name='FASPassword']") != null)
                        {
                            txtFASPassword.Attributes.Add("value", XmlDoc.SelectSingleNode("//form/SystemSettings/group/displaycolumn/systemcontrol[@name='FASPassword']").InnerText);
                        }
                    }
                    //Asharma326 MITS 32386 Ends

                    //rsharma220 MITS 33838 End
                    //Ashish Ahuja : To Insert the status of IgnoreSSNChecking in DB
                    string sCWSresponse = "";
                    XmlTemplate = GetMessageTemplate("EntitySSNCheckAdaptor.IgnoreSSNChecking");
                    bReturnStatus = CallCWS("EntitySSNCheckAdaptor.IgnoreSSNChecking", XmlTemplate, out sCWSresponse, false, false);                    

                    //MGaba2:MITS 29111         
                    //skhare7 MITS 28805
                    //if (BillReviewFee.Checked)
                    //{

                    //    OrgBillRevFee.Enabled = true;


                    //    OrgEntlastfirstname.ReadOnly = false;
                    //    OrgEntlastfirstnamebtn.Enabled = true;


                    //}
                    //else
                    //{
                    //    OrgBillRevFee.Enabled = false;

                    //    OrgEntlastfirstname.Text = string.Empty;
                    //    OrgEntentityid.Text = string.Empty;
                    //    OrgEntlastfirstname.ReadOnly = true;
                    //    //OrgEntlastfirstnamebtn.Enabled = false;


                    //}
                    //skhare7 MITS 28805 End
                }				  

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
            
    }
}
