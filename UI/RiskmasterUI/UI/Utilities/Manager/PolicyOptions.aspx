﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyOptions.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.Manager.PolicyOptions" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%-- <%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>--%>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Policy Management Setup</title>

    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/Utilities.js"></script>

    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload="PolicyLoad();parent.MDIScreenLoaded()">
    <form id="frmData" runat="server" method="post">
    <table>
        <tr>
            <td colspan="2">
                <uc3:ErrorControl ID="ErrorControl1" runat="server" />                
            </td>
        </tr>
    </table>
    <%--<asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />--%><!-- abansal23 : MITS 15215 -->
    <asp:TextBox Style="display: none" runat="server" ID="hTabName"></asp:TextBox>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../../Images/tb_save_active.png" Width="28"
                Height="28" border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../../Images/tb_save_mo.png';this.style.zoom='110%'"
                onmouseout="this.src='../../../Images/tb_save_active.png';this.style.zoom='100%'"
                OnClick="save_Click" OnClientClick="PreventPageLoadBeforeSave();" /></div>
    </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:Label ID="formtitle" runat="server" Text="Policy Management Setup" />
    </div>
    <br />
    <table border="0" style="width: 98%">
        <tr>
            <td>
                <div class="tabGroup" id="TabsDivGroup" runat="server">
                    <div class="Selected" nowrap="true" runat="server" name="TABSGeneral" id="TABSGeneral">
                        <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server"
                            rmxref="" name="General" id="LINKTABSGeneral">General</a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSGeneral">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSDiscountMaintenance"
                        id="TABSDiscountMaintenance">
                        <%--VACo Change Start--%>
                        <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                            rmxref="" name="DiscountMaintenance" id="LINKTABSDiscountMaintenance">Discount/Surcharge Maintenance</a>
                    </div>
                    
                    <div class="tabSpace" runat="server" id="TBSDiscountSetup">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSRateSetup"  id="TABSRateSetup">
                        <a class="NotSelected1" href="#" onclick="tabChange(this.name,'DIVRateSetup');return false;" runat="server"
                            rmxref="" name="RateSetup" id="LINKTABSRateSetup">Rate Maintenance</a>
                    </div>
                    <div class="tabSpace" runat="server" id="TBSDiscountMaintenance">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSDiscountSetup" id="TABSDiscountSetup">
                        <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                            rmxref="" name="DiscountSetup" id="LINKTABSDiscountSetup">Rate Algorithm Setup</a>
                    </div>
                    <div class="tabSpace" runat="server" id="Div4">
                        <nbsp />
                        <nbsp />
                    </div>
                </div>
            </td>
        </tr>
         <tr>
            <td>
                <div class="tabGroup" id="DIVRateSetup" runat="server" style="display:none  "  >
                    <div class="Selected" nowrap="true" runat="server" name="TABSExposure" id="TABSExposure">
                        <a class="Selected" href="#" onclick="tabChange(this.name,'DIVRateSetup');return false;" runat="server"
                            rmxref="" name="Exposure" id="LINKTABSExposure">Exposure</a>
                    </div>
                    <div class="tabSpace" runat="server" id="Div6">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSCoverages"
                        id="TABSCoverages">
                        <a class="NotSelected1" href="#" onclick="tabChange(this.name,'DIVRateSetup');return false;" runat="server"
                            rmxref="" name="Coverages" id="LINKTABSCoverages">Coverage</a>
                    </div>
                    
                    <div class="tabSpace" runat="server" id="Div8">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSUAR" id="TABSUAR">
                        <a class="NotSelected1" href="#" onclick="tabChange(this.name,'DIVRateSetup');return false;" runat="server"
                            rmxref="" name="UAR" id="LINKTABSUAR">Unit At Risk</a>
                    </div>
                    <div class="tabSpace" runat="server" id="Div10">
                    <%--VACo Change Ends--%>
                        <nbsp />
                        <nbsp />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABGeneral" id="FORMTABGeneral" width="100%">
            <tr>
                <td class="ctrlgroup" width="100%">
                    Audit Cancelled Policy
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox ID="AuditPolicy" Text="Allow Audit of cancelled policies" runat="server" rmxref="Instance/Document/Document/AuditPolicy"
                         RMXType="checkbox" />
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup" width="100%">
                    Year Info
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox ID="UseYear" Text="Include Year In Policy Number" runat="server" rmxref="Instance/Document/Document/UseYear"
                        onclick="ValidatePolicyOptionUseYear()" RMXType="checkbox" />
                    <asp:TextBox ID="UseYear_mirror" runat="server" Style="display: none"></asp:TextBox>
                    <asp:Label runat="server" ID="lbl_Placement" Text="Policy Number Year Placement"
                        class="required" />
                    <uc4:CodeLookUp runat="server" ID="Placement" CodeTable="YEAR_PLACEMENT" ControlName="Placement"
                        RMXRef="Instance/Document/Document/Placement" type="code" TabIndex="15" Required="true" />
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup" width="100%">
                    Term Info
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="TermInfoGrid" GridName="TermInfoGrid"
                            GridTitle="" Target="/Document/Document/RenewalInfoList" Ref="/Instance/Document/form//control[@name='TermInfoGrid']"
                            Unique_Id="renewalinforowid" ShowRadioButton="True" Width="99%" Height="100%"
                            HideNodes="|tag|renewalinforowid|" ShowHeader="True" LinkColumn="" PopupWidth="500"
                            PopupHeight="340" HideButtons="" Type="GridAndButtons" AllowPaging="true" PageSize="10"/>
                        <asp:TextBox Style="display: none" runat="server" ID="TermInfoGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="TermInfoGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="TermInfoGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="TermInfoGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup" width="100%">
                    Premium Calculation Options
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="RoundAmounts" Text="Round Premium Amounts" runat="server" rmxref="Instance/Document/Document/RoundAmounts"
                        RMXType="checkbox" class="required" />
                </td>
            </tr>
            <tr>
                <td width="80%">
                    <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="ExpConstGrid" GridName="ExpConstGrid"
                            GridTitle="Expense Constant" Target="/Document/Document/ExpConstList" Ref="/Instance/Document/form//control[@name='ExpConstGrid']"
                            Unique_Id="expconrowid" ShowRadioButton="True" Width="99%" Height="100%" HideNodes="|tag|expconrowid|"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10" />
                        <asp:TextBox Style="display: none" runat="server" ID="ExpConstGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="ExpConstGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="ExpConstGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="ExpConstGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>
                </td>
            </tr>
            <tr>
                <td width="80%">
                    <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="ThresholdsGrid" GridName="ThresholdsGrid"
                            GridTitle="Minimum Billed Threshold" Target="/Document/Document/ThresholdsList"
                            Ref="/Instance/Document/form//control[@name='ThresholdsGrid']" Unique_Id="thresholdrowid"
                            ShowRadioButton="True" Width="99%" Height="100%" HideNodes="|tag|thresholdrowid|"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10" />
                        <asp:TextBox Style="display: none" runat="server" ID="ThresholdsGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="ThresholdsGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="ThresholdsGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="ThresholdsGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Start --%> 
            <tr>
                <td width="80%">
                    <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="PolicyTaxGrid" GridName="PolicyTaxGrid"
                            GridTitle="Tax" Target="/Document/Document/PolicyTaxList"
                            Ref="/Instance/Document/form//control[@name='PolicyTaxGrid']" Unique_Id="PolicyTaxrowid"
                            ShowRadioButton="True" Width="99%" Height="100%" HideNodes="|tag|PolicyTaxrowid|"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10" />
                        <asp:TextBox Style="display: none" runat="server" ID="PolicyTaxGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="PolicyTaxGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="PolicyTaxGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="PolicyTaxGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>
                </td>
            </tr>
            <%--npadhy RMSC retrofit Ends --%>
            
        </table>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABDiscountMaintenance"
            id="FORMTABDiscountMaintenance">
            <tr>
                <td width="80%" style="height: 100%">
                    <div class="completerow" style="height: 100%">
                        <dg:UserControlDataGrid runat="server" ID="DiscountsGrid" GridName="DiscountsGrid"
                            GridTitle="" Target="/Document/Document/DiscountsList" Ref="/Instance/Document/form//control[@name='DiscountsGrid']"
                            Unique_Id="discountrowid" ShowRadioButton="True" Width="99%" Height="80%"
                            HideNodes="|tag|discountrowid|" ShowHeader="True" LinkColumn="" PopupWidth="500"
                            PopupHeight="400" HideButtons="" Type="GridAndButtons" AllowPaging="true" PageSize="10" />
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsGridSelectedId" RMXType="id"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsGrid_RowDeletedFlag"
                            RMXType="id" Text="false"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsGrid_Action" RMXType="id"></asp:TextBox>
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsGrid_RowAddedFlag"
                            RMXType="id" Text="false"></asp:TextBox>
                    </div>
                </td>
            </tr>
        </table>
        <%--VACo Changes Starts--%>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABRateSetup"            id="FORMTABRateSetup">
            <tr>
                <td width="80%" style="height: 100%">

                </td>
            </tr>
        </table>
        <%--VACo Changes Ends--%>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABDiscountSetup" id="FORMTABDiscountSetup">
            <tr>
                <td width="80%">
                    <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="TierGrid" GridName="TierGrid" GridTitle="Tier Setup"
                            Target="/Document/Document/TierList" Ref="/Instance/Document/form//control[@name='TierGrid']"
                            Unique_Id="disctierrowid" ShowRadioButton="True" Width="99%" Height="100%"
                            HideNodes="|tag|disctierrowid|" ShowHeader="True" LinkColumn="" PopupWidth="500"
                            PopupHeight="450" HideButtons="" Type="GridAndButtons" AllowPaging="true" PageSize="10"/>
                        <asp:TextBox Style="display: none" runat="server" ID="TierGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="TierGrid_RowDeletedFlag" RMXType="id"
                            Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="TierGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="TierGrid_RowAddedFlag" RMXType="id"
                            Text="false" />
                    </div>
                </td>
            </tr>
            <tr>
                <td width="80%">
                    <div class="completerow">
                    <%--VACo Changes Starts--%>
                        <dg:UserControlDataGrid runat="server" ID="DiscountsSetUpGrid" GridName="DiscountsSetUpGrid"
                            GridTitle="Discount/Surcharge Setup" Target="/Document/Document/DiscountsSetUpList" Ref="/Instance/Document/form//control[@name='DiscountsSetUpGrid']"
                            Unique_Id="seldiscrowid" ShowRadioButton="True" Width="99%" Height="45%" HideNodes="|tag|seldiscrowid|"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="450" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10" />
                    <%--VACo Changes Ends--%>        
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsSetUpGridSelectedId"
                            RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsSetUpGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsSetUpGrid_Action"
                            RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="DiscountsSetUpGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>
                </td>
            </tr>
             <tr>
             <%--VACo Changes Starts--%>
                <td width="80%">
                    <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="RateSetUpGrid" GridName="RateSetUpGrid"
                            GridTitle="Rate Setup" Target="/Document/Document/RateSetUpList" Ref="/Instance/Document/form//control[@name='RateSetUpGrid']"
                            Unique_Id="selraterowid" ShowRadioButton="True" Width="99%" Height="45%" HideNodes="|tag|seldiscrowid|"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="450" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10"/>
                        <asp:TextBox Style="display: none" runat="server" ID="RateSetUpGridSelectedId"
                            RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="RateSetUpGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="RateSetUpGrid_Action"
                            RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="RateSetUpGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>
                </td>
              <%--VACo Changes Ends--%>  
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABExposure" id="FORMTABExposure">
            <tr>
                <td width="80%">
                    <div class="completerow">
                 <%--VACo Changes Starts--%>   
                        <dg:UserControlDataGrid runat="server" ID="ExpRatesGrid" GridName="ExpRatesGrid"
                            GridTitle="Exposure Rates" TextColumn="tag" Target="/Document/Document/ExpRatesList" Ref="/Instance/Document/form//control[@name='ExpRatesGrid']"
                            Unique_Id="expraterowid" ShowRadioButton="True" Width="99%" Height="100%" HideNodes="|tag|expraterowid"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="450" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10" sortOrder="desc" AllowSorting="true"/>
                        <%--Zakir : MITS 34188 Allowing Sorting for UserControl Data Grid, and setting sortOrder to desc by default so that when property gets called it switches to ascending and first time its sorted in ascending order.--%>
                        <asp:TextBox Style="display: none" runat="server" ID="ExpRatesGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="ExpRatesGrid_RowDeletedFlag"     RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="ExpRatesGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="ExpRatesGrid_RowAddedFlag" RMXType="id" Text="false" />
                    </div>
                    </td>
                    </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" name="FORMTABCoverages" id="FORMTABCoverages">
            <tr>
                <td width="80%">
                    <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="CovRatesGrid" GridName="CovRatesGrid"
                            GridTitle="Coverage" TextColumn="tag" Target="/Document/Document/CovRatesList" Ref="/Instance/Document/form//control[@name='CovRatesGrid']"
                            Unique_Id="expraterowid" ShowRadioButton="True" Width="99%" Height="100%" HideNodes="|tag|covraterowid|"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="450" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10" />
                        <asp:TextBox Style="display: none" runat="server" ID="CovRatesGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="CovRatesGrid_RowDeletedFlag"   RMXType="id"  Text="false"/>
                        <asp:TextBox Style="display: none" runat="server" ID="CovRatesGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="CovRatesGrid_RowAddedFlag" RMXType="id" Text="false" />
                    </div>
                    </td>
                    </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" name="FORMTABUAR" id="FORMTABUAR">
            <tr>
                <td width="80%">
                    <div class="completerow" id="DIVUAR">
                        <dg:UserControlDataGrid runat="server" ID="UARRatesGrid" GridName="UARRatesGrid"
                            GridTitle="Unit At Risk" TextColumn="tag" Target="/Document/Document/UARRatesList" Ref="/Instance/Document/form//control[@name='UARRatesGrid']"
                            Unique_Id="expraterowid" ShowRadioButton="True" Width="99%" Height="100%" HideNodes="|tag|expraterowid|"
                            ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="450" HideButtons=""
                            Type="GridAndButtons" AllowPaging="true" PageSize="10" />
                        <asp:TextBox Style="display: none" runat="server" ID="UARRatesGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="UARRatesGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="UARRatesGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="UARRatesGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>
                </td>
            </tr>
        </table>
        <%-- <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />--%>
        <asp:TextBox Style="display: none" runat="server" ID="HdnPolOptRowId" RMXType="id"
            rmxref="Instance/Document/Document/PolOptRowId" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnActionSave" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnSelectedIdForDeletion" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnListNameForDeletion" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnISRateSetUp" Text="" /> 
        <%--VACo Changes Ends--%>
        <!--Tushar:MITS#18231-->       
        <asp:TextBox Style="display: none" runat="server" ID="HdnTabPress" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnTab" Text="" />
        <!--End-->
        <!--Start: Sumit-08/13/2010 - MITS# 20815-->       
        <asp:TextBox Style="display: none" runat="server" ID="HdnAssocDiscounts" RMXType="id"
            rmxref="Instance/Document/Document/AssocDiscounts" />
        <!--Sumit:End-->
        <!--Start: Sumit-11/09/2010 - MITS# 22877-->       
        <asp:TextBox Style="display: none" runat="server" ID="HdnAssocCoverages" RMXType="id"
            rmxref="Instance/Document/Document/AssocCoverages" />
        <!--Sumit:End-->
        
    </div>
    </form>
</body>
</html>
