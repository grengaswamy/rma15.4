<%--**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 **********************************************************************************************
 * 06/04/2014 | 33371   | ajohari2   | Added new page for Policy search data setup 
 **********************************************************************************************--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicySearchDataSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.PolicySearchDataSetup" ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Policy Search Data Setup</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">        { var i; }</script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
        <!--MITS 33371 ajohari2: Start-->
        <div class="msgheader" id="div_formtitle">
            <asp:label id="formtitle" runat="server" text="Policy Search Data Setup" />
        </div>
        <uc1:errorcontrol id="ErrorControl1" runat="server" />

        <br />
        <asp:textbox style="display: none" runat="server" id="RowId" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='RowId']" />
        <asp:textbox style="display: none" runat="server" id="selectedid" />
        <asp:textbox style="display: none" runat="server" id="mode" />
        <asp:textbox style="display: none" runat="server" id="selectedrowposition" />
        <asp:textbox style="display: none" runat="server" id="gridname" />
        <asp:textbox style="display: none" runat="server" id="UniqueId" text="RowId" />

        <div id="div_PolSys" class="full">
            <asp:label runat="server" class="required" id="lbl_PolSys" text="Policy System:" />
            <span>
                <asp:dropdownlist id="PolSys" tabindex="1" runat="server" onchange="setDataChanged(true);" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='PolSys']">
                    </asp:dropdownlist>
            </span>
        </div>
        <div id="div_AgentCode" class="full">
            <asp:label runat="server" class="required" id="lbl_AgentCode" text="Agent Code:" />
            <span>
                <asp:textbox runat="server" onchange="setDataChanged(true);" id="AgentCode" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='AgentCode']"
                    tabindex="2" maxlength="7" />
            </span>
        </div>
        <div id="div_MCO" class="full">
            <asp:label runat="server" class="required" id="lbl_MCO" text="MCO:" />
            <span>
                <asp:textbox runat="server" onchange="setDataChanged(true);" id="MCO" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='MCO']"
                    tabindex="3" maxlength="2" />
            </span>
        </div>
        <div id="div_PCO" class="full">
            <asp:label runat="server" class="required" id="lbl_PCO" text="PCO:" />
            <span>
                <asp:textbox runat="server" onchange="setDataChanged(true);" id="PCO" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='PCO']"
                    tabindex="4" maxlength="2" />
            </span>
        </div>
        <div id="div_BusUnit" class="full">
            <asp:label runat="server" class="required" id="lbl_BusUnit" text="Business Unit:" />
            <span>
                <asp:textbox runat="server" onchange="setDataChanged(true);" id="BusUnit" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='BusUnit']"
                    tabindex="5" maxlength="10" />
            </span>
        </div>
        <div id="div_BusSeg" class="full">
            <asp:label runat="server" class="required" id="lbl_BusSeg" text="Business Segment:" />
            <span>
                <asp:textbox runat="server" onchange="setDataChanged(true);" id="BusSeg" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='BusSeg']"
                    tabindex="6" maxlength="8" />
            </span>
        </div>
        <div id="div_SecGroup" class="full">
            <asp:label runat="server" class="required" id="lbl_SecGroup" text="Org. Security Group:" />
            <span>
                <asp:dropdownlist id="SecGroup" tabindex="7" runat="server" onchange="setDataChanged(true);" rmxref="/Instance/Document/PolicySearchDataMapping/control[@name ='SecGroup']">
                    </asp:dropdownlist>
            </span>
        </div>
        <br />
        <br />
        <br />
        <asp:textbox style="display: none" runat="server" id="txtData" />
        <asp:textbox style="display: none" runat="server" id="txtPostBack" />
        <div id="div_buttons" style="left: 1%">
            <div class="formButton" id="div_btnOk">
                <asp:button class="button" runat="server" id="btnOk" text="OK" width="75px" onclientclick="return PolicySearchDataMapping_onOk();"
                    onclick="btnOk_Click" />
            </div>
            <div class="formButton" id="div_btnCancel">
                <asp:button class="button" runat="server" id="btnCancel" text="Cancel" width="75px"
                    onclientclick="return PolicySearchDataMapping_onCancel();" onclick="btnCancel_Click" />
            </div>
        </div>
        <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="" />
        <!--MITS 33371 ajohari2: End-->
    </form>
</body>
</html>
