﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ThresHolds.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ThresHolds" %>

<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Add/Modify Threshold</title>
    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%-- <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>

    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/drift.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/dhtml-div.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/dhtml-help-setup.js"></script>

</head>
<body class="10pt" onload="CheckForWindowClosing();">
    <form id="frmData" runat="server" method="post">
    <table>
        <tr>
            <td colspan="2">
                <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""><div class="toolbardrift"
        id="toolbardrift" name="toolbardrift">
        <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle" height="32">
                        <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageButton1" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClick="save_Click"
                            OnClientClick="return ValidateFieldsforMinBillThreshold();" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <div class="msgheader" id="formtitle">
        Add/Modify Threshold</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Minimum Billed Threshold
                                </td>
                            </tr>
                            <tr>
                                <asp:TextBox Style="display: none" runat="server" ID="RowId" rmxref="/Instance/Document//control[@name='RowId']"
                                    type="id"></asp:TextBox></tr>
                            <tr>
                                <asp:TextBox Style="display: none" runat="server" ID="AddedByUser" rmxref="/Instance/Document//control[@name='AddedByUser']"
                                    type="id" />
                            </tr>
                            <tr>
                                <asp:TextBox Style="display: none" runat="server" ID="DttmRcdAdded" rmxref="/Instance/Document//control[@name='DttmRcdAdded']"
                                    type="id" />
                            </tr>
                            <tr>
                                <asp:TextBox Style="display: none" runat="server" ID="DttmRcdLastUpd" rmxref="/Instance/Document//control[@name='DttmRcdLastUpd']"
                                    type="id" />
                            </tr>
                            <tr>
                                <asp:TextBox Style="display: none" runat="server" ID="UpdatedByUser" rmxref="/Instance/Document//control[@name='UpdatedByUser']"
                                    type="id" />
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label2" Text="Use Threshold:" class="required" />
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" rmxref="/Instance/Document//control[@name='UseThreshold']"
                                        ID="UseThreshold" onchange="ApplyBool(this);" />
                                    <input type="text" name="$node^57" value="" style="display: none" id="UseThreshold_mirror">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label1" Text="Line of Business:" class="required" />
                                </td>
                                <td>
                                    <uc4:CodeLookUp runat="server" ID="LOBcode" CodeTable="POLICY_LOB" ControlName="LOBcode"
                                        type="code" class="required" RMXRef="/Instance/Document//control[@name='LOBcode']" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label3" Text="State:" class="required" />
                                </td>
                                <td>
                                    <uc4:CodeLookUp runat="server" ID="Statecode" CodeTable="states" ControlName="Statecode"
                                        type="code" class="required" RMXRef="/Instance/Document//control[@name='Statecode']" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_Amount" Text="Amount:" class="required" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="Amount" rmxref="/Instance/Document//control[@name='Amount']" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label4" Text="Effective Date:" class="required" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" rmxref="/Instance/Document//control[@name='EffectiveDate']"
                                        RMXType="date" TabIndex="2" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                        <%--vkumar258 - RMA-6037 - Starts --%>
                                        <%--<asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" TabIndex="3" />--%>
                                        <%--                    <input type="text" name="$node^89" value="" size="30" onblur="dateLostFocus(this.id);&#xA;"
                                        rmxforms:as="date" id="EffectiveDate" onchange="&#xA;setDataChanged(true);&#xA;">
                                        <input type="button" class="DateLookupControl" id="EffectiveDatebtn">
                                        --%>

                                        <%-- <script type="text/javascript">
											Zapatec.Calendar.setup(
											{
												inputField : "EffectiveDate",
												ifFormat : "%m/%d/%Y",
												button : "EffectiveDatebtn"
											}
											);
                                    </script>--%>
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#EffectiveDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "3");
                                            });
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:label runat="server" id="Label5" text="Expiration Date:" />
                                    </td>
                                    <td>
                                        <asp:textbox runat="server" formatas="date" id="ExpirationDate" rmxref="/Instance/Document//control[@name='ExpirationDate']"
                                            rmxtype="date" tabindex="4" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                                        <%--vkumar258 - RMA-6037 - Starts --%>
                                        <%--   <asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" TabIndex="3" />--%>
                                        <%--                                    <input type="text" name="$node^95" value="" size="30" onblur="dateLostFocus(this.id);&#xA;"
                                        rmxforms:as="date" id="ExpirationDate" onchange="&#xA;                        setDataChanged(true);&#xA;                        "><input
                                            type="button" class="DateLookupControl" id="ExpirationDatebtn">--%>

                                        <%--<script type="text/javascript">
											Zapatec.Calendar.setup(
											{
												inputField : "ExpirationDate",
												ifFormat : "%m/%d/%Y",
												button : "ExpirationDatebtn"
											}
											);
                                    </script>--%>
                                        <script type="text/javascript">
                                            $(function () {
                                                $("#ExpirationDate").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "../../../Images/calendar.gif",
                                                    //buttonImageOnly: true,
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true,
                                                    changeYear: true
                                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
                                            });
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:textbox style="display: none" runat="server" id="FormMode" rmxref="/Instance/Document//control[@name='FormMode']" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <uc2:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="Loading" />
                        <input type="text" name="" value="" id="SysViewType" style="display: none" />
                        <input type="text" name="" value="" id="SysCmd" style="display: none" />
                        <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                        <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                        <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                        <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                        <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                        <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                        <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                        <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="MinBillThreshold" />
                        <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                        <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="MinBillThreshold" />
                        <input type="text" name="" value="LOBcode_cid|Statecode_cid|Amount|EffectiveDate|"
                            id="SysRequired" style="display: none" />
                        <asp:textbox style="display: none" runat="server" id="TextBox1" rmxref="/Instance/Document/Document/ExpCon/expconrowid" />
                        <asp:textbox style="display: none" runat="server" id="selectedrowposition" />
                        <asp:textbox style="display: none" runat="server" id="gridname" />
                        <asp:textbox style="display: none" runat="server" id="txtPostBack" />
                        <%--Start: Sumit-08/18/2010  MITS# 21804 Changed Controls name--%>
                        <asp:textbox style="display: none" runat="server" id="validate" />
                        <asp:textbox style="display: none" runat="server" id="mode" />
                        <asp:textbox style="display: none" runat="server" id="Action" />
                        <asp:textbox style="display: none" runat="server" id="FunctionToCall"></asp:textbox>
                        <asp:textbox style="display: none" runat="server" id="txtData" />
                        <%--End:Sumit--%>
                    </td>
                    <td valign="top"></td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
