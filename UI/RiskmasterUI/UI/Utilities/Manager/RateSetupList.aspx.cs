﻿using System;
///<Summary>
/// Developed By: Tushar Agarwal
/// Completed On: 13 Nov, 2009
/// MITS 18231
///</Summary>
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class RateSetupList : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tb_LOB_Code.Text = AppHelper.GetQueryStringValue("lobcode");
            tb_State_Code.Text = AppHelper.GetQueryStringValue("statecode");
            NonFDMCWSPageLoad("RateSetupListAdaptor.Get");
        }
    }
}
