﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LOBParameters.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.LOBParameters" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Line Of Business Parameter Setup</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="javascript" type="text/javascript" src="../../../Scripts/Utilities.js"></script>
     <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
 
  <script language="javascript" type="text/javascript">
  function trim(value)
				{
				  value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
				  return value;
				}
				function HideDiv()
				{
				try
				{
					var elem = document.getElementById("TABSAutoAdjustOptions");
					var ctrl = document.getElementById("chkAutoAdjust");
					var elem1 = document.getElementById("TabSpaceAutoOptions");
					if (ctrl.checked==true)
					{
					elem.style.display = 'none';
					        //MITS 16750 : abansal23 on 05/25/2009 Starts
					        document.forms[0].chkAutoAdjustNegative.checked = false;
					        elem1.style.display = 'none';
					        //MITS 16750 : abansal23 on 05/25/2009 Ends
					}
					else
					{
					 elem.style.display = '';
					 elem1.style.display = '';
					}
				}catch(ex){}
				}
                //JIRA-857 Starts
				function HideDivColl(s) {
				    try {
				        var elem = document.getElementById("TABSCollOptions");
				        var ctrl = document.getElementById("chkPrevCollPerRes");
				        var ctrl_coll = document.getElementById("chkPrevColl");
				        if ((ctrl.checked == true) && (ctrl_coll.checked == false)) {
				            elem.style.display = '';
				            document.forms[0].chkPrevColl.checked = false;
				            elem1.style.display = '';
				        }
				        else if ((ctrl.checked == true) && (ctrl_coll.checked == true)) {
				            if (s == 'all') {
				                document.forms[0].chkPrevCollPerRes.checked = false;
				                elem.style.display = 'none';
				                elem1.style.display = 'none';
				            }
				            else {
				                document.forms[0].chkPrevColl.checked = false;
				                elem.style.display = '';
				                elem1.style.display = '';
				            }
				        }
				        else {
				            elem.style.display = 'none';
				            elem1.style.display = 'none';
				        }
				    } catch (ex) { }
				}
                //JIRA-857 Ends
				function submitLOB()
				{
          // ABhateja, 08.31.2007 mits 10237
          // Clear the claim type value as the old value is getting passed when the form is submitted.
          if(document.forms[0].lstClaimType!=null)
            document.forms[0].lstClaimType.value = "";
				  document.forms[0].submit();
				}
				function OpenCriteria()
				{
				    //MITS 34260 start
				    //if (document.forms[0].lstDupClaimCriteria.value=="1" ||document.forms[0].lstDupClaimCriteria.value=="2"
				    //|| document.forms[0].lstDupClaimCriteria.value == "3") 
					if (document.forms[0].lstDupClaimCriteria.value=="1" ||document.forms[0].lstDupClaimCriteria.value=="2"
						|| document.forms[0].lstDupClaimCriteria.value == "3" || document.forms[0].lstDupClaimCriteria.value == "4")
                    //MITS 34260 end
					{
						m_Wnd=window.open('/RiskmasterUI/UI/Utilities/Manager/LobOptions.aspx','edit','width=350,height=150'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');
					   document.getElementById("hdndupClaimCriterea").value=document.forms[0].lstDupClaimCriteria.value;
					}
				}
				function OpenDupCriteriaForPolDownload() {
				    var objDpdrpolClm = document.getElementById('drpDupCriteriaPolDown');
				    if (objDpdrpolClm.selectedIndex != -1) {
				        m_Wnd = window.open('/RiskmasterUI/UI/Utilities/Manager/LobOptions.aspx?DupCriteriaType=Policy', 'edit', 'width=400,height=200' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 200) / 2 + ',resizable=yes,scrollbars=no');
				        document.getElementById("hdndupCriteriaAtPolDown").value = document.forms[0].drpDupCriteriaPolDown.value;
				    }
		        }
				function EntityEnable()
				{
				try
				{
                  //Start : Jira 9859 MITS 38095 bsharma33
				  //LevelType();
				  if (document.forms[0].chkEvent.checked)
				  {
					document.forms[0].lstEntities.disabled=false;
				      document.forms[0].hdnLevelTypesDisabled.value = "N";
				  }
				  else
				  {
				    document.forms[0].lstEntities.selectedIndex=-1;
				    document.forms[0].lstEntities.disabled=true;
				    document.forms[0].hdnLevelTypesDisabled.value = "Y";
				  }
				    //End : Jira 9859 MITS 38095 bsharma33
				 }catch(ex){}
				}
				function UnCheckAutoAdjust()
				{
				try
				{
				  if (document.forms[0].chkAutoAdjustNegative.checked)
				  {
				    document.forms[0].chkAutoAdjust.checked=false;
				  }
				  HideDiv();
				}catch(ex){}
				}
				function DisableNR()
				{
				  try
				  {
				  if (document.forms[0].chkInsuff.checked)
				  {
				    document.forms[0].chkNegtiveRA.disabled=false;
          			document.forms[0].chkInsuffAutoPay.disabled=false;
				  }
				  else
				  {
					document.forms[0].chkNegtiveRA.disabled=true;
					document.forms[0].chkNegtiveRA.checked=false;
          			document.forms[0].chkInsuffAutoPay.disabled=true;
          			document.forms[0].chkInsuffAutoPay.checked=false;
				  }
				  }catch(ex){}
				}
				function EnableClaimType()
				{
				  try
				  {
				  var elem1=document.getElementById('SpecifyClmType1');
				  var elem2=document.getElementById('SpecifyClmType2');
				  if (document.forms[0].chkClaimType.checked==true)
				  {
				      if (confirm_on()){
				         
							document.forms[0].hdnCriteria.value="Y";
							document.forms[0].hdnAction.value="UpdateClaimResType";
							document.forms[0].hdnUpdateClaimResType.value="Y";
							document.forms[0].submit();
							elem1.style.display = '';
							elem2.style.display = '';
							
							
							
						}
						else
						{
							elem1.style.display = 'none';
							elem2.style.display = 'none';
							document.forms[0].chkClaimType.checked = false;							
						}
				   }
				  else if(document.forms[0].chkClaimType.checked==false)
				  {
						if (confirm_off())
						{
						
						
							elem1.style.display = 'none';
							elem2.style.display = 'none';
							document.forms[0].hdnCriteria.value="Y";
							document.forms[0].hdnAction.value="UpdateClaimResType";
							document.forms[0].hdnUpdateClaimResType.value="N";
							document.forms[0].submit();
						}
						else
						{
							elem1.style.display = '';
							elem2.style.display = '';
							document.forms[0].chkClaimType.checked = true;
						}
				  }
				  }catch(ex){}
				}
				function EnableClaimTypeStartUp()
				{
				  try
				  {
				  var elem1=document.getElementById('SpecifyClmType1');
				  var elem2=document.getElementById('SpecifyClmType2');
				  if (document.forms[0].chkClaimType.checked==false)
				  {
				    elem1.style.display = 'none';
				    elem2.style.display = 'none';
				  }
				  else
				  {
				     
					 elem1.style.display = '';
					 elem2.style.display = '';
				  }
				  }catch(ex){}
				}
				function checkDoNothing()
				{
				try
				{
				 var elem1=document.getElementById('chkRBZero');
				 var elem2=document.getElementById('chkRAZero');
				 var elem3=document.getElementById('chkNRBZero');
				 var elem4=document.getElementById('chkDoNothing');
				 if (elem1.checked==false && elem2.checked==false && elem3.checked==false)
          elem4.checked=true;
          }catch(ex){}
          }
          function UnCheckOthers(e)
          {
          try
          {
          var elem1=document.getElementById('chkRBZero');
          var elem2=document.getElementById('chkRAZero');
          var elem3=document.getElementById('chkNRBZero');
          var elem4=document.getElementById('chkDoNothing');
          switch(e)
          {
          case '1':
          elem2.checked=false;
          elem3.checked=false;
          elem4.checked=false;
          break;
          case '2':
          elem1.checked=false;
          elem3.checked=false;
          elem4.checked=false;
          break;
          case '3':
          elem2.checked=false;
          elem1.checked=false;
          elem4.checked=false;
          break;
          case '4':
          elem2.checked=false;
          elem3.checked=false;
          elem1.checked=false;
          break;
          }
          }catch(ex){}

          }

          function UnCheckOther(e)
          {
              try {
                  var elem1 = document.getElementById('chkCollResBal');
                  var elem2 = document.getElementById('chkCollResBalPerRsv');
                  var elem3 = document.getElementById('chkCollIncBal');
                  var elem4 = document.getElementById('chkCollIncBalPerRsv');
                  switch (e) {
                      case '1':
                          if (elem1 != null) {
                              if (elem2 != null && elem3 != null && elem4 != null) {
                                  if (elem1.checked) {
                                      elem2.checked = false;
                                      elem3.checked = false;
                                      elem4.checked = false;
                                      funcEnableDisablePerRsvLookUp(elem4, 'lstPerRsvTypeIncurred');
                                      funcEnableDisablePerRsvLookUp(elem2, 'lstPerRsvTypeRecovery');
                                  }
                              }
                          }
                          break;
                      case '2':
                          if (elem1 != null) {
                              if (elem2 != null && elem3 != null) {
                                  if (elem2.checked) {
                                      elem1.checked = false;
                                      elem3.checked = false;
                                  }
                                  funcEnableDisablePerRsvLookUp(elem2, 'lstPerRsvTypeRecovery');
                              }
                          }
                          break;
                      case '3':
                          if (elem3 != null && elem4 != null && elem3.checked && elem1 != null) {
                              elem4.checked = false;
                              elem1.checked = false;
                              elem2.checked = false;
                              funcEnableDisablePerRsvLookUp(elem2, 'lstPerRsvTypeRecovery');
                              funcEnableDisablePerRsvLookUp(elem4, 'lstPerRsvTypeIncurred');
                          }
                          break;
                      case '4':
                          if (elem3 != null && elem4 != null && elem4.checked && elem1 != null) {
                              elem3.checked = false;
                              elem1.checked = false;
                          }
                          funcEnableDisablePerRsvLookUp(elem4, 'lstPerRsvTypeIncurred');
                          break;
                      case 'PAGELOAD':
                          if (elem2.checked == false || elem4.checked == false) {
                              funcEnableDisablePerRsvLookUp(elem2, 'lstPerRsvTypeRecovery');
                              funcEnableDisablePerRsvLookUp(elem4, 'lstPerRsvTypeIncurred');
                          }
                          break;
                  }
              } catch (ex) { }

          }
          
          function confirm_off()
          {
          try
          {
          if (confirm("By turning the 'Specify Reserves by Claim Type' option off, all claim types in the selected \n line of buiness will utilize the same reserves.\n\n Do you still want to turn this option off?")==true)
          {if(document.getElementById("chkRBZero").checked==true)
              {
               document.getElementById("hdnoption1").value="-1";
              }
              else
              {
               document.getElementById("hdnoption1").value="";
              }
               if(document.getElementById("chkRAZero").checked==true)
              {
                document.getElementById("hdnoption2").value="-1";
              }
              else
              {
               document.getElementById("hdnoption2").value="";
              }
               if(document.getElementById("chkNRBZero").checked==true)
              {
                document.getElementById("hdnoption3").value="-1";
              }
              else
              {
               document.getElementById("hdnoption3").value="";
              }
               if(document.getElementById("chkDoNothing").checked==true)
              {
               document.getElementById("hdnoption4").value="-1";
              }
              else
              {
               document.getElementById("hdnoption4").value="0";
              }
          return true;
          }
          else
          return false;
          }catch(ex){}
          }
          function confirm_on()
          {
          try
          {
          if (confirm("Turning on the 'Specify Reserves by Claim Type' option can have a negative impact on historical data.\n It is strongly recommended that you contact Riskmaster support regarding this option before turning it on.\n\nDo you still want to turn this option on?")==true)
           {
         
		     if(document.getElementById("chkRBZero").checked==true)
              {
               document.getElementById("hdnoption1").value="-1";
              }
              else
              {
               document.getElementById("hdnoption1").value="";
              }
               if(document.getElementById("chkRAZero").checked==true)
              {
                document.getElementById("hdnoption2").value="-1";
              }
              else
              {
               document.getElementById("hdnoption2").value="";
              }
               if(document.getElementById("chkNRBZero").checked==true)
              {
                document.getElementById("hdnoption3").value="-1";
              }
              else
              {
               document.getElementById("hdnoption3").value="";
              }
               if(document.getElementById("chkDoNothing").checked==true)
              {
               document.getElementById("hdnoption4").value="-1";
              }
              else
              {
               document.getElementById("hdnoption4").value="0";
              }
                return true;           
                           
            }
          
          else
          return false;
          }catch(ex){}
          }

          function submitValue()
          {
          document.forms[0].hdnCriteria.value="Y";
          document.forms[0].hdnAction.value="ReserveChange";
          // document.forms[0].hdnAction.value="";//Asif
          document.forms[0].hdnReserveType.value="1";
                    if(document.getElementById("chkRBZero").checked==true)
          {
           document.getElementById("hdnoption1").value="-1";
          }
          else
          {
           document.getElementById("hdnoption1").value="";
          }
           if(document.getElementById("chkRAZero").checked==true)
          {
            document.getElementById("hdnoption2").value="-1";
          }
          else
          {
           document.getElementById("hdnoption2").value="";
          }
           if(document.getElementById("chkNRBZero").checked==true)
          {
            document.getElementById("hdnoption3").value="-1";
          }
          else
          {
           document.getElementById("hdnoption3").value="";
          }
           if(document.getElementById("chkDoNothing").checked==true)
          {
           document.getElementById("hdnoption4").value="-1";
          }
          else
          {
           document.getElementById("hdnoption4").value="0";
          }
          document.forms[0].submit();
          }
          function SelectAll()
          {
          try
          {
              var varlstPerRsvTypeRecovery = document.getElementById('lstPerRsvTypeRecovery_multicode');
              var varlstPerRsvTypeIncurred = document.getElementById('lstPerRsvTypeIncurred_multicode');
              var varchkCollResBalPerRsv = document.getElementById('chkCollResBalPerRsv');
              var varchkCollIncBalPerRsv = document.getElementById('chkCollIncBalPerRsv');
              if (varlstPerRsvTypeRecovery != null && varlstPerRsvTypeIncurred != null && varchkCollResBalPerRsv != null && varchkCollIncBalPerRsv != null) {
                  if ((varlstPerRsvTypeRecovery.length == 0 && varchkCollResBalPerRsv.checked) || (varlstPerRsvTypeIncurred.length == 0 && varchkCollIncBalPerRsv.checked)) {
                      alert(parent.CommonValidations.AlertWhenReserveTypeNotSpecified);
                      tabChange('ReserveIncurredBalanceOptions');
                      if (varlstPerRsvTypeRecovery.length == 0 && varchkCollResBalPerRsv.checked)
                          document.getElementById('lstPerRsvTypeRecovery_multicodebtn').focus();
                      if (varlstPerRsvTypeIncurred.length == 0 && varchkCollIncBalPerRsv.checked)
                          document.getElementById('lstPerRsvTypeIncurred_multicodebtn').focus();
                      return false;
                  }
              }
         //asif start
         
          document.getElementById("state").value='btnSaveClicked';
          if(document.getElementById("chkRBZero").checked==true)
          {
           document.getElementById("hdnoption1").value="-1";
          }
          else
          {
           document.getElementById("hdnoption1").value="";
          }
           if(document.getElementById("chkRAZero").checked==true)
          {
            document.getElementById("hdnoption2").value="-1";
          }
          else
          {
           document.getElementById("hdnoption2").value="";
          }
           if(document.getElementById("chkNRBZero").checked==true)
          {
            document.getElementById("hdnoption3").value="-1";
          }
          else
          {
           document.getElementById("hdnoption3").value="";
          }
           if(document.getElementById("chkDoNothing").checked==true)
          {
           document.getElementById("hdnoption4").value="-1";
          }
          else
          {
           document.getElementById("hdnoption4").value="0";
          }
             if(document.getElementById("rdClmEvtYear").checked==true)
          {
            document.getElementById("hdnClmEvtYear").value="-1";
            document.getElementById("rdClmYear").checked=false;
             document.getElementById("rdClmEvtYear").checked=true;
            
            
          }
          else
          {
           document.getElementById("hdnClmEvtYear").value="0";
           document.getElementById("rdClmYear").checked=true;
           document.getElementById("rdClmEvtYear").checked=false;
          }
          var ClmLvl=document.getElementById("rdClaimLvl");
           var DetailLvl=document.getElementById("rdDetailLvl");
            var ClmDetailLvl=document.getElementById("rdClaimDetailLvl");
          var lobsel=document.getElementById("hdnLOBSelected");
          if(ClmLvl.checked==true)
          {
          document.getElementById("hdnClaimDetailLvl").value='0';
          }
          if(DetailLvl.checked==true)
          {
          document.getElementById("hdnClaimDetailLvl").value='1';
          }
          if(ClmDetailLvl.checked==true)
          {
          document.getElementById("hdnClaimDetailLvl").value='2';
          }
          //asif end
          var allvalue="";
          ctrl=document.getElementById('chkNegtiveRA');
          if (!ctrl.checked)
          {
			  document.forms[0].chkNegtiveRA.checked=false;
          }

          var allvalue="";
          ctrl=document.getElementById('lstAutoAdj');
          
          //for(var f=0;f&lt;ctrl.options.length;f++)
           for(var f=0;f<ctrl.options.length;f++)
					{
						allvalue=allvalue+ctrl.options[f].value+",";
					}
					ctrl=document.getElementById('hdnAutoAdjust');
					ctrl.value=allvalue;
					allvalue="";
					ctrl=document.getElementById('lstAutoClose');
					//for(var f=0;f&lt;ctrl.options.length;f++)
					for(var f=0;f<ctrl.options.length;f++)
					{
						allvalue=allvalue+ctrl.options[f].value+",";
					}
					ctrl=document.getElementById('hdnAutoClose');
					ctrl.value=allvalue;
				
					allvalue="";
					ctrl=document.getElementById('lstReserveTypes');
					
					//for(var f=0;f&lt;ctrl.options.length;f++)
					for(var f=0;f<ctrl.options.length;f++)
          {
          allvalue=allvalue+ctrl.options[f].value+",";
          }
          ctrl=document.getElementById('hdnClaimReserveTypes');
          ctrl.value=allvalue;
          //if((document.forms[0].WeeksPerMonth.value == "") || (document.forms[0].WeeksPerMonth.value == "0") || document.forms[0].WeeksPerMonth.value &lt; 0)
          if((document.forms[0].WeeksPerMonth.value == "") || (document.forms[0].WeeksPerMonth.value == "0") || document.forms[0].WeeksPerMonth.value < 0)
          {
          document.forms[0].WeeksPerMonth.value = 4.34812141;
          }
          
          return false;
         

          }catch(ex){}
          }
          function ClaimEvent()
          {
          try
          {
          var elem1=document.getElementById('rdClmYear');
          var elem2=document.getElementById('rdClmEvtYear');
          if (elem1.checked==false)
          {
          elem2.checked=true;
          }
          }catch(ex){}
          }
          function DuplicateCriteria()
          {
          try
          {
          if (document.forms[0].hdnDupClaimCriteriaDisabled.value=="Y")
          {
          document.forms[0].lstDupClaimCriteria.selectedIndex=-1;
          document.forms[0].lstDupClaimCriteria.value=-1;
          }
          }catch(ex){}
          }

          function DuplicateEnable(type)
          {
          try
          {
          var elem1=document.getElementById('chkDuplicate');
          var elem2=document.getElementById('lstDupClaimCriteria');
          if (elem1.checked==false)
          {
          elem2.selectedIndex=-1;
          elem2.disabled=true;
          document.forms[0].hdnDupClaimCriteriaDisabled.value = 'Y';
          }
          else
          {
          if (type=="2")
          {
          elem2.selectedIndex=0;
          }
          elem2.disabled=false;
          document.forms[0].hdnDupClaimCriteriaDisabled.value = 'N';
          }
          DuplicateCriteria();
          }catch(ex){}
          }
	      function EnableDupCriteriaAtPolDown(type) {
	            try {
	                var elem1 = document.getElementById('chkDupClmatPolDown');
	                var elem2 = document.getElementById('drpDupCriteriaPolDown');
	                if (elem1.checked == false) {
	                    elem2.selectedIndex = -1;
	                    elem2.disabled = true;
	                    document.forms[0].hdndupCriteriaAtPolDownDisable.value = 'Y';
	                }
	                else {
	                    elem2.disabled = false;
	                    document.forms[0].hdndupCriteriaAtPolDownDisable.value = 'N';
	                    if (type == "2") {
	                        elem2.selectedIndex = 0;
	                        OpenDupCriteriaForPolDownload();
	                    }
	                }
	                DuplicateCriteriaAtPolDown();
	            } catch (ex) { }
	       }
	      function DuplicateCriteriaAtPolDown() {
	            try {
	                if (document.forms[0].hdndupCriteriaAtPolDownDisable.value == "Y") {
	                    document.forms[0].drpDupCriteriaPolDown.selectedIndex = -1;
	                    document.forms[0].drpDupCriteriaPolDown.value = -1;
	                }
	            } catch (ex) { }
	      }
          function LevelType()
          {
          try
          {
          if (document.forms[0].hdnLevelTypesDisabled.value=="Y")
          {
          document.forms[0].lstEntities.selectedIndex=-1;
          }
          }catch(ex){}
          }
          function TabSelected(ctrl)
          {
          try
          {
          if (ctrl.className="Selected")
          {
          document.forms[0].SysTabSelected.value=ctrl.name+"**"+ctrl.className;
          }
          }catch(ex){}
          }
          function selectSelectedTab()
          {
          try
          {
          if (trim(document.forms[0].SysTabSelected.value)=="")
          {
          return;
          }
          for(i=0;i<document.all.length;i++)
					{
						if(document.all[i].id != null)
							{
							if(document.all[i].id.substring(0,7) == 'FORMTAB')
								document.all[i].style.display = "none";
							if(document.all[i].id.substring(0,4) == 'TABS')
								document.all[i].className = "NotSelected";
							if(document.all[i].id.substring(0,8) == 'LINKTABS')
								document.all[i].className = "NotSelected1";
							}
					}
					arr=document.forms[0].SysTabSelected.value.split("**");
					ctrl=document.getElementById(arr[0]);
					ctrl.className=arr[1];
					eval('document.all.FORMTAB'+arr[0]+'.style.display=""');
					eval('document.all.TABS'+arr[0]+'.className="Selected"');
					eval('document.all.LINKTABS'+arr[0]+'.className="Selected"');
					if (document.forms[0].chkAutoAdjustNegative.checked==false)
					{
						if (arr[0]=="AutoAdjustOptions")
						{
							eval('document.all.FORMTAB'+'ReserveOptions'+'.style.display=""');
							eval('document.all.TABS'+'ReserveOptions'+'.className="Selected"');
							eval('document.all.LINKTABS'+'ReserveOptions'+'.className="Selected"');
							document.forms[0].SysTabSelected.value="ReserveOptions**Selected";
							
							eval('document.all.FORMTAB'+'ReserveOptions'+'.style.display="none"');
							eval('document.all.TABS'+'ReserveOptions'+'.className="NotSelected"');
							eval('document.all.LINKTABS'+'ReserveOptions'+'.className="NotSelected1"');
							
						}
					}
					}catch(ex){}
				}
				function ConfirmForBoth()
				{
                    if (document.getElementById('rdDetailLvl').disabled || document.getElementById('rdDetailLvl').disabled)
                        return false; //Deb : MITS 25430
                    var bClaim = document.getElementById('rdClaimLvl').checked;
					var bDetail = document.getElementById('rdDetailLvl').checked;
					var confirmBoth = confirm("Computer Sciences Corporation does not recommend the use of this option.  It can cause reserve figures to appear incorrect.  It is provided for backward Compatibility only.  Do you still wish to use the option to record reserves at the claim and detail levels?");
					if (confirmBoth == false)
					{
						if (bClaim)
							document.getElementById('rdClaimLvl').checked = true;
						else if (bDetail)
							document.getElementById('rdDetailLvl').checked = true;
					}
					else
						document.getElementById('rdClaimDetailLvl').checked = true;
					return false;
				}
				//MGaba2:R6 Reserve WorkSheet:Start
				//In case Use Reserve WorkSheet checkbox is check/uncheck,corresponding
				//Multicode lookups are made enabled/disabled
				//This function is called on body load and clicking of check box
				function fnEnableRSWLookUps() {
				    if (document.forms[0].ChkUseRSW.checked == false) {
				        document.forms[0].ChkAttRSW.disabled = true;
					document.forms[0].ChkReasonCommentsRequiredRSW.disabled = true;
				        document.forms[0].lstRSWClaimType_multicode.disabled = true;
				        document.forms[0].lstRSWClaimType_multicodebtndel.disabled = true;
				        document.forms[0].lstRSWClaimType$multicodebtn.disabled = true;
				        document.forms[0].lstRSWClaimStatus_multicode.disabled = true;
				        document.forms[0].lstRSWClaimStatus_multicodebtndel.disabled = true;
				        document.forms[0].lstRSWClaimStatus$multicodebtn.disabled = true;
				    }
				    else {
				        document.forms[0].ChkAttRSW.disabled = false;
					document.forms[0].ChkReasonCommentsRequiredRSW.disabled = false;
				        document.forms[0].lstRSWClaimType_multicode.disabled = false;
				        document.forms[0].lstRSWClaimType_multicodebtndel.disabled = false;
				        document.forms[0].lstRSWClaimType$multicodebtn.disabled = false;
				        document.forms[0].lstRSWClaimStatus_multicode.disabled = false;
				        document.forms[0].lstRSWClaimStatus_multicodebtndel.disabled = false;
				        document.forms[0].lstRSWClaimStatus$multicodebtn.disabled = false;
				    }

				}
				//MGaba2:R6 Reserve WorkSheet:End
				// MGaba2: MITS 23003: Use claim Comments chkbox should be checked and disabled if
               //Use Adjuster Text is checked or Enhanced Notes is unchecked
				function fnDisableUseCommentChkBox()
                {
				    var chkUseEnhancedNotes = document.getElementById("chkEnhNotes");
				    var chkUseClaimComments = document.getElementById("chkClmComments");
				    var chkIncludeScheduledChecks = document.getElementById("chkIncludeScheduledChecks");
				    var chkUseAdjText = document.getElementById("chkAdjCom");
				    if (chkUseEnhancedNotes != null && chkUseClaimComments != null && chkUseAdjText != null) {

				        if (chkUseEnhancedNotes.checked == false || chkUseAdjText.checked == true) {
				            chkUseClaimComments.checked = true;
				            chkUseClaimComments.disabled = true;
				        }
				        else {
				            chkUseClaimComments.disabled = false;
                        }
                    }

				}
				//spahariya MITS 29221 - start
				function DisablePrinterSources() {
				    if (document.forms[0].DirectToPrinter.checked) {
				        document.forms[0].ddlPrinters.disabled = false;
				        document.forms[0].ddlPbfl.disabled = false;
				    }
				    else {
				        document.forms[0].ddlPrinters.disabled = true;
				        document.forms[0].ddlPrinters.value = "";
				        document.forms[0].ddlPbfl.disabled = true;
				        document.forms[0].ddlPbfl.value = "";
				    }
				    HideDivColl(); //rma-857
				}
				function SetValue() {

				    document.frmData.hdnAction.value = 'SelectedIndexChange';
				}
      ////spahariya MITS 29221 - End
				function funcEnableDisablePerRsvLookUp(ctrlName, lstName) {
				    if (ctrlName.checked == false) {
				        eval('document.forms[0].' + lstName + '_multicode').disabled = true;
				        eval('document.forms[0].' + lstName + '_multicodebtndel').disabled = true;
				        eval('document.forms[0].' + lstName + '$multicodebtn').disabled = true;
				        //document.forms[0].lstPerRsvTypeRecovery$multicodebtn.disabled = true;
				    }
				    else {
				        eval('document.forms[0].' + lstName + '_multicode').disabled = false;
				        eval('document.forms[0].' + lstName + '_multicodebtndel').disabled = false;
				        eval('document.forms[0].' + lstName + '$multicodebtn').disabled = false;
				    }

				}
  </script>
</head>
<!--MGaba2: MITS 23003 -->
<!--Start:Added by Nitin Goel: Call a method DisablePolicyFilterFlag() to disable property/vehicle filter06/09/2010-->
    <!--JIRA-857<body onload="loadTabList();parent.MDIScreenLoaded();fnEnableRSWLookUps(); DisablePolicyFilterFlag() ;fnDisableUseCommentChkBox();DisablePrinterSources();">--> 
<body onload="loadTabList();parent.MDIScreenLoaded();fnEnableRSWLookUps(); DisablePolicyFilterFlag() ;fnDisableUseCommentChkBox();DisablePrinterSources();HideDivColl();UnCheckOther('PAGELOAD');"><!--J#857-->
<!--End:Nitin Goel,Call a method DisablePolicyFilterFlag() to disable property/vehicle filter06/09/2010-->
    <form id="frmData" runat="server">
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
    <table border="0"  cellpadding="0" cellspacing="0" align="left" >
    <tr>
								<td>
								
									<%--<input type="hidden" name="chkAutoAdjust_mirror" id="chkAutoAdjust_mirror" runat="server"/>
									<input type="hidden" name="chkAutoAdjustNegative_mirror" id="chkAutoAdjustNegative_mirror" runat="server"/>
									<input type="hidden" name="chkClaimType_mirror" id="chkClaimType_mirror" runat="server"/>
									<input type="hidden" name="chkCollResBal_mirror" id="chkCollResBal_mirror" runat="server"/>
									 <!-- Shruti for MITS 8551-->
									<input type="hidden" name="chkCollIncBal_mirror" id="chkCollIncBal_mirror" runat="server"/>
									<input type="hidden" name="chkInsuff_mirror" id="chkInsuff_mirror" runat="server"/>
                  <!-- Shruti for FL Max Rate-->
                  <input type="hidden" name="chkInsuffAutoPay_mirror" id="chkInsuffAutoPay_mirror" runat="server"/>
									<input type="hidden" name="chkNegtiveRA_mirror" id="chkNegtiveRA_mirror" runat="server"/>
									<input type="hidden" name="chkCType_mirror" id="chkCType_mirror" runat="server"/>
									<input type="hidden" name="chkYear_mirror" id="chkYear_mirror" runat="server"/>
									<input type="hidden" name="chkLOB_mirror" id="chkLOB_mirror" runat="server"/>
									<input type="hidden" name="chkEvent_mirror" id="chkEvent_mirror" runat="server"/>
									<input type="hidden" name="chkDuplicate_mirror" id="chkDuplicate_mirror" runat="server"/>
									<input type="hidden" name="chkAdjCom_mirror" id="chkAdjCom_mirror" runat="server"/>--%>
									<asp:HiddenField ID="state" runat="server" />
									<asp:HiddenField ID="hdnoption1" runat="server" />
									<asp:HiddenField ID="hdnoption2" runat="server" />
									<asp:HiddenField ID="hdnoption3" runat="server" />
									<asp:HiddenField ID="hdnoption4" runat="server" />
								<asp:HiddenField ID="hdnClmEvtYear" runat="server" />
									<asp:HiddenField ID="hdnClaimDetailLvl" runat="server" />
								</td>
							</tr>
							<tr>
							<td align="left" valign="middle" HEIGHT="32">
							<asp:ImageButton id="Save" OnClientClick="return SelectAll();" onMouseOver="this.src='../../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../../Images/save.gif';this.style.zoom='100%'" ImageUrl="~/Images/save.gif" class="bold" ToolTip="Save"  runat="server" onclick="Save_Click" />
									</td>
									<td>
                                        <asp:TextBox ID="SysFocusFields" value="" runat="server" style="display:none"></asp:TextBox>
                                        <asp:HiddenField ID="hTabName" runat="server" />
                                        <asp:TextBox ID="SysTabSelected" rmxref="/Instance/Document/LOB/TabSelected" style="display:none" runat="server"></asp:TextBox>
								</td>
							</tr>
                       </table>
                     </div>
                     <table width="100%" border="0" cellspacing="0" cellpadding="4">
                     <tr>
            <td colspan="2">
            <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr>
                     <tr>
							<td class="msgheader" colspan="4">Line of Business Parameter Setup</td>
						</tr>
						<tr>
							<td>
		                	   <asp:TextBox ID="hdnValue" Style="display: none" rmxref="/Instance/Document/LOB/LOBParms/DupNoOfDays" runat="server" />
                 			   <asp:TextBox ID="hdnPolClaimDupDays" Style="display: none" rmxref="/Instance/Document/LOB/LOBParms/PolClaimDupXDays" runat="server" />
                                <asp:TextBox ID="hdnLOBSelected" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/LOBSelected" runat="server" />
                               <asp:TextBox ID="hdnAction" style="display:none" runat="server" />
                                <asp:TextBox ID="hdnCriteria" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/Criteria" runat="server" />
                                <asp:TextBox ID="hdnReserveType" style="display:none"  runat="server" />
                                <asp:TextBox ID="hdnAutoAdjust" style="display:none"  runat="server" />
                                <asp:TextBox ID="hdnAutoClose" style="display:none"  runat="server" />
                                <asp:TextBox ID="hdnClaimReserveTypes" style="display:none" runat="server" /> 
                                <asp:TextBox ID="hdnLevelTypesDisabled" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/LevelTypesDisabled" runat="server" />
                                <asp:TextBox ID="hdnDupClaimCriteriaDisabled" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/DupClaimCriteriaDisabled" runat="server" />
                                <asp:TextBox ID="hdnUpdateClaimResType" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/UpdateClaimResType" runat="server" />
                                <%--<asp:TextBox ID="hdnEnhancedNotes" style="display:none" rmxref="//LOB/LOBParms/Options/ClaimOptions/UseEnhancedNotes" runat="server" />
                                <asp:TextBox ID="hdnAdjusterText" style="display:none" rmxref="//LOB/LOBParms/Options/ClaimOptions/UseAdjusterText" runat="server" />--%>
                                <asp:TextBox ID="hdnClaimCommentsDsplyonly" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/UseClaimCommentsDisplayOnly" runat="server" />
                                 <asp:TextBox ID="hdndupClaimCriterea" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/DupCriteriaValues" runat="server" />
                   				 <asp:TextBox ID="hdndupCriteriaAtPolDown" Style="display: none" rmxref="/Instance/Document/LOB/LOBParms/DupCriteriaValuesAtPolDown" runat="server" />
                   				 <asp:TextBox ID="hdndupCriteriaAtPolDownDisable" Style="display: none" rmxref="/Instance/Document/LOB/LOBParms/DupCriteriaAtPolDownDis" runat="server" />
                                 <asp:TextBox ID="hdnReserve" style="display:none"  runat="server" />
                                 <%--Sumit - 09/22/2009 MITS#18227 Hidden node to store the value if Activation code window needs to be displayed or not.--%>
                                 <asp:TextBox ID="hdnIsActCodeReq" style="display:none"  runat="server" />
                                <asp:TextBox ID="SysFormName" style="display:none" Text="lobparameters" value="lobparameters" runat="server" />
                               
                                
							</td>
						</tr>
					</table>
						<table border="0" width="100%">
						<tr>
							<td>
								<table border="0" cellspacing="0" cellpadding="0" width="80%">
									<tr>
										<td align="center">Line of Business:
                                             <%-- bpaskova - 06/18/2014 JIRA RMA-220 document.forms[0].submit(); should be the last statement in the javaScript.--%>
                                            <asp:DropDownList ID="lstLOB" type="combobox" rmxref="/Instance/Document/LOB/LOBParms/LineOfBusiness" itemsetref="/Instance/Document/LOB/LOBParms/LineOfBusiness"  runat="server" onchange="setDataChanged(true);submitLOB();" > 
                                            </asp:DropDownList>
					
					</td>
									</tr>
								</table>
									</td>
						</tr>
						<tr>
							<td>
								<table border="0" cellspacing="0" cellpadding="0" style="height:5%;width:99.5%">  <%-- igupta3 Mits: 33301--%>
									<tr>
									    <td><%--aravi5 Jira Id: RMA-7106 starts--%>
									    <div class="tabGroup" id="TabsDivGroup" runat="server">
                                            <div class="Selected" nowrap="true" runat="server" name="TABSReserveOptions" id="TABSReserveOptions">
                                                <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="ReserveOptions" id="LINKTABSReserveOptions">Reserve Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="TBSReserveOptions">
                                                 &#160;
                                            </div>
										
										    <div class="NotSelected" nowrap="true" runat="server" name="TABSClaimOptions" id="TABSClaimOptions">
                                                <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="ClaimOptions" id="LINKTABSClaimOptions">Claim Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="TBSClaimOptions">
                                                &#160;
                                            </div>
										
										    <div class="NotSelected" nowrap="true" runat="server" name="TABSAutoCloseOptions" id="TABSAutoCloseOptions">
                                                <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="AutoCloseOptions" id="LINKTABSAutoCloseOptions">Auto-Close Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="TBSAutoCloseOptions">
                                                &#160;
                                            </div>
											<div class="NotSelected" nowrap="true" runat="server" name="TABSAutoAdjustOptions" id="TABSAutoAdjustOptions">
                                                <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="AutoAdjustOptions" id="LINKTABSAutoAdjustOptions">Auto-Adjust Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="TBSAutoAdjustOptions">
                                               &#160;
                                            </div>
                                            <%--Sumit--%>
                                            <div class="NotSelected" nowrap="true" runat="server" name="TABSPolicyOptions" id="TABSPolicyOptions">
                                                <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="PolicyOptions" id="LINKTABSPolicyOptions">Policy Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="TBSPolicyOptions">
                                               &#160;
                                            </div> 
                                            
                                                                                        <!--JIRA 857 Start -->
                                            <div class="NotSelected" nowrap="true" runat="server" name="TABSCollOptions" id="TABSCollOptions">
                                                <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="CollOptions" id="LINKTABSCollOptions">Reserve Collection Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="TBSCollOptions">
                                                &#160;
                                            </div>
                                            <!--JIRA 857 Ends--> 
                                          <div class="NotSelected" nowrap="true" runat="server" name="TABSReserveIncurredBalanceOptions" id="TABSReserveIncurredBalanceOptions">
                                                <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="ReserveIncurredBalanceOptions" id="LINKTABSReserveIncurredBalanceOptions">Reserve/Incurred Balance Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="Div2">
                                                &#160;
                                            </div> 
                                            <%--tanwar2 - mits 30910 - start--%>
                                            <div class="NotSelected" nowrap="true" runat="server" name="TABSPolicyOptions" id="TABSDeductibleOptions">
                                                <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="DeductibleOptions" id="LINKTABSDeductibleOptions">Deductible Options</a>
                                            </div>
                                            <div class="tabSpace" runat="server" id="TBSDeductibleOptions">
                                                &#160;
                                            </div>
                                            <%--tanwar2 - mits 30910 - end--%>
										</div><%--aravi5 Jira Id: RMA-7106 Ends--%>
										</td>
									</tr>
								</table>
								<div style="position:relative;left:0;top:0;width:inherit; height:inherit ";  class="singletopborder">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td>
												<table border="0" cellspacing="0" cell cellpadding="0" width="100%" name="FORMTABReserveOptions"
													id="FORMTABReserveOptions">
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td class="ctrlgroup" width="48%" colspan=4>On Claim Closed</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														Close Reserve and Set Reserve Balance to Zero:
														</td>
														<td width="25%">
                                                            <asp:RadioButton ID="chkRBZero" type="radio" onclick="UnCheckOthers('1');setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves1" runat="server" value="-1"  group="">
                                                            
                                                            </asp:RadioButton>	
                                                            
																	</td>
																	<td width="25%">
																		Specify Reserves by Claim Type:
																	</td>
																	<td width="25%">
                                                                        <asp:CheckBox ID="chkClaimType" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ResClaimType" onchange="setDataChanged(true);" onclick="EnableClaimType();setDataChanged(true);" runat="server" />
																	</td>
																</tr>
															</table>
																</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														Close Reserve and Set Reserve Amount to Zero:
														</td>
																	<td width="25%">
                                                                        <asp:RadioButton ID="chkRAZero" type="radio"  value="-1" onclick="UnCheckOthers('2');setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves2" group=""  runat="server"  >
                                                                        </asp:RadioButton>
																	</td>
																	<td width="25%">
																	<table id="SpecifyClmType1">
																			<tr>
																				<td>
																		 Claim Type:	
																	</td>
																			</tr>
																		</table>
																	</td>
																	<td width="25%">
																		<table id="SpecifyClmType2">
																			<tr>
																				<td>
                                                                                    <%-- bpaskova - 06/18/2014 JIRA RMA-220 document.forms[0].submit(); should be the last statement in the javaScript.--%>
                                                                                    <asp:DropDownList ID="lstClaimType" onChange="setDataChanged(true);submitValue();" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ClaimTypes"  itemsetref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ClaimTypes" runat="server">
                                                                                    </asp:DropDownList>
																				</td>
																			</tr>
																		</table>
																		</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%" valign="top">
														Close Reserve and Set Negative Reserve Balance to Zero:
														</td>
														<td width="25%" valign="top">
                                                            <asp:RadioButton ID="chkNRBZero" type="radio" value="-1" onclick="UnCheckOthers('3');setDataChanged(true);"  rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves3"  group="" runat="server">
                                                            </asp:RadioButton>
																	</td>
																	<td width="25%" valign="top">
														Reserve Type																												
														</td>
																	<td width="25%" valign="top">
																		</td>
																</tr>
															</table>
															</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="50%" colspan="2" valign="top">
																		<table>
																			<tr>
																				<td width="51%">
																		Do Nothing:
																	</td>
																	<td valign="top" width="47%">
                                                                        <asp:RadioButton ID="chkDoNothing" type="radio" onclick="UnCheckOthers('4');setDataChanged(true);" value="-1" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/optClosedReserves4" group=""  runat="server" />
																					
																				</td>
																				</tr>
																			<tr>
																				<td colspan="2">
																					
																							
																								<hr />
																							
																				</td>
																			</tr>
																			<tr>
																				<td>
														Auto Adjust Negative Reserves To Zero(Open Claims):
														</td>
																				<td>
                                                                                    <asp:CheckBox ID="chkAutoAdjust" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/AdjustReservesCheck" onchange="setDataChanged(true);" onclick="HideDiv();setDataChanged(true);" runat="server" />
																			
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														</td>
																			</tr>
																			<tr>
																				<td width="51%">
														Auto Adjust Negative Reserves To Zero(Open Claims) Per Reserve:
														</td>
														<td width="47%">
                                                            <asp:CheckBox ID="chkAutoAdjustNegative" value="True" type="checkbox"  rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/AutoAdjustPerReserve" onchange="setDataChanged(true);" onclick="UnCheckAutoAdjust();setDataChanged(true);" runat="server" />
																					
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														</td>
																			</tr>
																			<tr>
																				<td width="51%">
														Check for Insufficient Reserve Funds:
														</td>
																				<td width="47%">
                                                                                    <asp:CheckBox ID="chkInsuff" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/chkInsuff"  onchange="setDataChanged(true);" onclick="DisableNR();setDataChanged(true);" runat="server" />
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														 </td>
																			</tr>
                                      <!-- Shruti for FL Max Rate starts-->
                                      <tr>
                                        <td width="51%">
                                          Check for Insufficient Reserve Funds in Auto Checks:
                                        </td>
                                        <td width="47%">
                                            <asp:CheckBox ID="chkInsuffAutoPay" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/chkInsuffAutoPay"  onchange="setDataChanged(true);" runat="server" />
                                        </td>
                                        <td width="25%">
                                          &#160;
                                        </td>
                                        <td width="25%">
                                          &#160;
                                        </td>
                                      </tr>
                                      
																			<tr>
																				<td width="51%">
														Do Not Allow Negative Reserve Adjustments:
														</td>
																				<td width="47%">
                                                                                    <asp:CheckBox ID="chkNegtiveRA" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/NoResAdjBelowZero" onchange="setDataChanged(true);"  runat="server" />
																					
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														</td>
																			</tr>
                                                        <!-- JIRA 857 Start -->
                                                                            <tr>
																				<td width="51%">
														Prevent Collections For Non Recovery Reserves (All Claims):
														</td>
																				<td width="47%">
                                                                                    <asp:CheckBox ID="chkPrevColl" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/PreventCollectionForAllRsvTypes" onchange="setDataChanged(true);" onclick="HideDivColl('all');setDataChanged(true);" runat="server" />
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														</td>
																			</tr>
                                                                            <tr>
																				<td width="51%">
														Prevent Collections For Non Recovery Reserves (All Claims) Per Reserve:
														</td>
																				<td width="47%">
                                                                                    <asp:CheckBox ID="chkPrevCollPerRes" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/PreventCollectionForPerRsvTypes" onchange="setDataChanged(true);" onclick="HideDivColl('per');setDataChanged(true);" runat="server" />
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														</td>
																			</tr>
                                                        <!-- JIRA 857 End -->
                                                                             <tr>
																				<td width="51%">
														Add Recovery Reserve To Total Claim Reserve Balance:
														</td>
																				<td width="47%">
                                                                                    <asp:CheckBox ID="chkAddRecoveryReservetoTotalBalanceAmount" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/AddRecoveryReservetoTotalBalanceAmount" onchange="setDataChanged(true);"  runat="server" />
																					
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														</td>
																			</tr>
                                                                               <tr>
																				<td width="51%">
														Add Recovery Reserve To Total Claim Incurred Balance:
														</td>
																				<td width="47%">
                                                                                    <asp:CheckBox ID="chkAddRecoveryReservetoTotalIncurredAmount" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/AddRecoveryReservetoTotalIncurredAmount" onchange="setDataChanged(true);"  runat="server" />
																					
																				</td>
																				<td width="25%">
														 &#160;
														</td>
																				<td width="25%">
														 &#160;
														</td>
																			</tr>
																		</table>
																	</td>
																	<td colspan="2" valign="top">
																	<table>
																			<tr>	
																				<td >
																		            	<uc:MultiCode runat="server" ID="lstReserveTypes" width="200px" height="88px"  CodeTable="RESERVE_TYPE" ControlName="lstReserveTypes"  RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ReserveTypes" RMXType="codelist"/>
																	            </td>
																	           </tr>
																		    </table>
																		</td>
																</tr>
															</table>
														</td>
													</tr>
													<%--MGaba2:R6 Reserve WorkSheet Section:Start --%>
                                    <tr>
                                        <td>
                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td colspan="4">
                                                        <div id="divRSW" name="ReserveWorkSheet">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td class="ctrlgroup" colspan="4">
                                                                        Reserve WorkSheet
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="49%">
                                                                        Use Reserve WorkSheet:
                                                                        <asp:CheckBox ID="ChkUseRSW" type="checkbox" onchange="setDataChanged(true);" runat="server"
                                                                            RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/UseReserveWorksheet" onclick="fnEnableRSWLookUps();"/>
                                                                    </td>
                                                                    <td >
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="49%">
                                                                        Attach PDF:
                                                                        <asp:CheckBox ID="ChkAttRSW" type="checkbox" onchange="setDataChanged(true);" runat="server"
                                                                            RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/AttachReserveWorksheet" />
                                                                    </td>
                                                                    <td >
                                                                        Reason and Comments to be required:
                                                                        <asp:CheckBox ID="ChkReasonCommentsRequiredRSW" type="checkbox" onchange="setDataChanged(true);" runat="server"
                                                                            RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ReasonCommentsRequiredRSW" onclick="fnEnableRSWLookUps();"/>
                                                                    </td>
                                                                </tr>
								<tr><td colspan="4"><hr /></td></tr>
                                                                <tr>
                                                                    <td width="49%">
                                                                        Disable ToolBar for selected Claim Types(Open Claims):
                                                                    </td>
                                                                    <td width="49%">
                                                                        Disable ToolBar for selected Claim Status(Open Claims):
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="49%">
                                                                        <uc:MultiCode runat="server" ID="lstRSWClaimType" width="200px" Height="88px" CodeTable="CLAIM_TYPE"
                                                                            ControlName="lstRSWClaimType" RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/RSWCLaimTypes"
                                                                            RMXType="codelist" />
                                                                    </td>
                                                                    <td width="49%">
                                                                        <uc:MultiCode runat="server" ID="lstRSWClaimStatus" width="200px" Height="88px" CodeTable="CLAIM_STATUS"
                                                                            ControlName="lstRSWClaimStatus" RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/RSWCLaimStatus"
                                                                            RMXType="codelist" Filter=" CODES.RELATED_CODE_ID = 9"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--MGaba2:R6 Reserve WorkSheet Section:End --%>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td colspan="4">
																		<div id="ReserveTracking" name="ReserveTracking">
																			<table width="100%">
																				<tr>
																					<td class="ctrlgroup" colspan="4">Reserve Tracking</td>
																				</tr>
																				<tr>
																					<td width="25%">Claim Level: 
                                                                                        <asp:RadioButton ID="rdClaimLvl" type="radio" value="0" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/optReserveTracking" GroupName="RB2" Checked="false" runat="server" onclick="setDataChanged(true);" />	
																		
														</td>
																					<td width="25%">Detail Level:
														
                                                                                        <asp:RadioButton ID="rdDetailLvl" type="radio" value="1"  rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/optReserveTracking" runat="server" Checked="false" GroupName="RB2" />
																			
														</td>
																					<td width="25%">Claim &amp; Detail Level:
																				
                                                                                        <asp:RadioButton ID="rdClaimDetailLvl" type="radio" value="2" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/optReserveTracking" Checked="false" onMouseDown="ConfirmForBoth();"  runat="server" GroupName="RB2" />
																																										
														</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABPaymentClaimOptions"
													id="FORMTABClaimOptions" style="display:none;">
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td class="ctrlgroup" colspan="4">Include in Claim Id</td>
																</tr>
																<tr>
																	<td width="25%">
														Claim Type:
														</td>
																	<td width="25%">
                                                                        <asp:CheckBox ID="chkCType" value="True" type="checkbox"  rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/IncClmTypeFlag" onchange="setDataChanged(true);" runat="server" />
																		
																	</td>
																	<td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																		</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														Year:
														</td>
																	<td width="25%">
                                                                        <asp:CheckBox ID="chkYear" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/IncYearFlag" onchange="setDataChanged(true);"  runat="server" />
																		
																	</td>
																	<td colspan="2">
																		&#160;
																	</td>
																</tr>
																<tr>
                                                                    
																	<td width="25%">
														  Claim Year:
                                                                        <asp:RadioButton ID="rdClmYear" type="radio" value="0" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/optTrigger"  GroupName="RB1" runat="server" />
                                                                        
                                                                        
														
														</td>
																	<td width="25%">
														  Event Year:
														 
                                                                        <asp:RadioButton ID="rdClmEvtYear" type="radio" value="-1" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/optTrigger" runat="server" GroupName="RB1" />
											
																	</td>
																	</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														LOB Indicator:
														</td>
																	<td width="25%">
                                                                        <asp:CheckBox ID="chkLOB" type="checkbox" value="True" onchange="setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/IncLobCodeFlag"  runat="server" />
																	</td>
																	<td width="25%">
														 &#160;
														</td>
															<td width="25%">
														 &#160;
														</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="50%">
																		<table>
																			<tr>
																				<td width="50%">
																	Entity ID:
                                                                                    <asp:CheckBox ID="chkEvent" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/IncOrgFlag" onchange="setDataChanged(true);" onclick="EntityEnable();setDataChanged(true);"  runat="server" />
																	
																	</td>
																				<td align="center" width="50%">
																	At Level:
                                                                                    <asp:DropDownList ID="lstEntities"  rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/LevelTypes" itemsetref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/LevelTypes" runat="server">
                                                                                    </asp:DropDownList>        
																		
																	</td>
																			</tr>
																		</table>
																	</td>
																	<td width="50%">
																&#160;
																</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<font color="#000000">
																<hr></hr>
															</font>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" celpadding="0" width="100%">
																<tr>
																	<td width="25%">
														Allow Check For Possible Duplicate Claims:
														</td>
																	<td width="25%">
                                                                        <asp:CheckBox ID="chkDuplicate" type="checkbox" value="True" onclick="DuplicateEnable('2');setDataChanged(true);" onchange="setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/Duplicate" runat="server" />
																	</td>
																	<td width="25%">
														&#160;
														</td>
																	<td width="25%">
														 &#160;
														</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														Duplicate Criteria:
														</td>
																	<td colspan="2" align="left">
                                                                        <asp:DropDownList ID="lstDupClaimCriteria"   onchange="OpenCriteria();setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria" itemsetref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/DupClaimCriteria"  runat="server">
                                                                        </asp:DropDownList>
																	</td>
																	<td width="25%">
														&#160;
														</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
																		Use Adjuster Text For Claim Comments:
																			</td>
																	<td width="25%">
                                                                    <!--MGaba2: MITS 23003 -->
                                                                        <asp:CheckBox ID="chkAdjCom" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/UseAdjusterText" onclick="HideDiv();setDataChanged(true);fnDisableUseCommentChkBox();" runat="server" />
																	</td>
																	<td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
																Use Enhanced Notes:
															</td>
															<td width="25%">
														<!--MGaba2: MITS 23003 -->
                                                                <asp:CheckBox ID="chkEnhNotes" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/UseEnhancedNotes" onclick="HideDiv();setDataChanged(true);fnDisableUseCommentChkBox();" runat="server" />
																	</td>
																	<td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
														<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td id="claimcommentstext" width="25%">
																		<asp:Label ID="lblClaimComments" Text="Use Claim Comments:" runat="server"></asp:Label>
														
															</td>
																	<td width="25%">
                                                                        <asp:CheckBox ID="chkClmComments" type="checkbox" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/UseClaimComments" onclick="HideDiv();setDataChanged(true);" runat="server" />

																	</td>
																	<td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>
													<!--
													</xsl:if>
													-->
													
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														Show Claim Duration In:
														</td>
																	<td colspan="2" align="left">
                                                                        <asp:DropDownList ID="lstPeriodTypes" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/PeriodTypes" itemsetref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/PeriodTypes"  runat="server">
                                                                        </asp:DropDownList>
																	</td>
																	<td >
																	<div id="divWeeks" runat="server">	
														Number of Weeks Per Month:open
														<asp:TextBox ID="WeeksPerMonth" onchange="return numLostFocus(this);" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/WeeksPerMonth" runat="server"></asp:TextBox>
												
																		<asp:Button ID="btnWeeksPerMonthMED" OnClientClick="return openCalculator('WeeksPerMonth');" Text="..." class="button" runat="server"></asp:Button>
																		</div>	
																	</td>
														
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														Org. Level For Claim Caption:
														</td>
														<td colspan="2" align="left">
                                                            <asp:DropDownList ID="lstCaptionTypes" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/CaptionTypes" itemsetref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/CaptionTypes"  runat="server">
                                                            </asp:DropDownList>
																	</td>
																	<td width="25%">
														&#160;
														</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
														<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td id="Td1" width="25%">
																		<asp:Label ID="lblISOClaimSubmission" Text="Use ISO Claim Submission:" runat="server"></asp:Label>
														
															        </td>
																	<td width="25%">
                                                                        <asp:CheckBox ID="chkISOClmSubmission" type="checkbox" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/UseISOClaimSubmission" onclick="HideDiv();setDataChanged(true);" runat="server" />

																	</td>
																	<td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>
													<!--smahajan6 - 02/03/2010 MITS#18230 - Start - Add "Filter For Selected Policy"-->
													<tr>
														<td>
														    <table border="0" cellspacing="0" cellpadding="0" width="100%">
															    <tr id="IsPolicyFilter">
																    <td id="Td2" width="25%">
																	    <asp:Label ID="lblIsPolicyFilter" Text="Filter For Selected Policy:" runat="server"></asp:Label>
														            </td>
																    <td width="25%">
																        <asp:CheckBox ID="chkIsPolicyFilter" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/IsPolicyFilter" onclick="HideDiv();setDataChanged(true);" runat="server" />
																    </td>
																    <td width="25%">&#160;</td>
																    <td width="25%">&#160;</td>
															    </tr>
														    </table>
														</td>
													</tr>
                                                    <!--spahariya - 07/26/2010 MITS#29221 G.12 - Start - Add Printer for Auto Mail Merge-->
													<tr>
														<td>
														    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
																	<td class="ctrlgroup" colspan="4">Auto Mail Merge Printing Options</td>
                                                                    <td></td>
																</tr>
															     <tr>
                                                                        <td> Print Direct To Printer:&nbsp;&nbsp;</td>
                                                                           <td>
                                                                               <asp:CheckBox ID="DirectToPrinter" onclick="setDataChanged(true);DisableOnClick(this);DisablePrinterSources()" tabindex="2" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/DirectToPrinter" value="True" type="checkbox" runat="server" />
                                                                           </td>
                                                                           <td></td>
                                                                   </tr>
                                                                   <tr>
                                                                            <td>Following Printers are attached with the Server:&nbsp;&nbsp;</td>
                                                                            <td>
                
                                                                                <asp:DropDownList ID="ddlPrinters" onchange="SetValue();" runat="server" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/PrinterName" AutoPostBack='true' onselectedindexchanged="ddlPrinters_SelectedIndexChanged" >
                                                                              </asp:DropDownList>
                                                                           </td>
                                                                           <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                        
                                                                        <td>Please Choose Paper Bin For Letters:&nbsp;&nbsp;</td>
                                                                        <td>
                                                                           <asp:DropDownList ID="ddlPbfl" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/PaperBinForLetters" runat="server" >
                                                                            </asp:DropDownList>
                                                                       </td>
                                                                       <td></td>
                                                                  </tr>
														    </table>
														</td>
													</tr>
													<!--spahariya - 07/26/2010 MITS#29221 G.12 - End-->																										
													<!--smahajan6 - 02/03/2010 MITS#18230 - End-->																									
												</table>
												<table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABAutoCloseOptions"
													id="FORMTABAutoCloseOptions" style="display:none;">
													<tr>
													<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td class="ctrlgroup" colspan="4">Claim is automatically closed when payment is made of type listed below</td>
																</tr>
																<tr>
																	<td>
																		<table>
																			<tr>
																				<td align="left">
																		Auto-Close Transaction Types			
																	</td>
																			</tr>
																			<tr>
																				<td align="left">
																				
																				<uc:MultiCode runat="server" ID="lstAutoClose" Height="150px" width="500px" CodeTable="TRANS_TYPES" ControlName="lstAutoClose" RMXRef="/Instance/Document/LOB/LOBParms/Options/AutoCloseOptions/AutoCloseTransTypeCode" RMXType="codelist"/>
																				</td>
																			</tr>
																		</table>
                                                                        <table>
                                                                            <td id="Td4" width="25%">
																		        <asp:Label ID="Label3" Text="Include Scheduled Checks:" runat="server"></asp:Label>
														
															                </td>
																	        <td width="25%">
                                                                                <asp:CheckBox ID="chkIncludeScheduledChecks" type="checkbox" rmxref="/Instance/Document/LOB/LOBParms/Options/AutoCloseOptions/IncludeScheduledChecks" onclick="HideDiv();setDataChanged(true);" runat="server" />
																	        </td>
                                                                        </table>

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABAutoAdjustOptions"
													id="FORMTABAutoAdjustOptions" style="display:none;">
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td class="ctrlgroup" colspan="4">Auto Adjust Negative Reserve to Zero (Open Claims) option is applied to Reserve Types	listed below</td>
																</tr>
																<tr>
																	<td width="25%">
																		<table>
																			<tr>
																				<td align="left">
																		Auto Adjusted Reserve Types	
																		</td>
																			</tr>
																			<tr>
																				<td align="left">
																			
                                                                                    <!--MITS 14815 Raman Bhatia: Table Name should be Reserve Type and NOT Trans Type--> 
                                                                                    <%--<uc:MultiCode runat="server" ID="lstAutoAdj" width="500px" Height="150px"  CodeTable="TRANS_TYPES" ControlName="lstAutoAdj"  RMXRef="/Instance/Document/LOB/LOBParms/Options/AutoAdjustOptions/AutoAdjustTransTypeCode" RMXType="codelist"/>--%>
                                                                                    <uc:MultiCode runat="server" ID="lstAutoAdj" width="500px" Height="150px"  CodeTable="RESERVE_TYPE" ControlName="lstAutoAdj"  RMXRef="/Instance/Document/LOB/LOBParms/Options/AutoAdjustOptions/AutoAdjustTransTypeCode" RMXType="codelist"/>
																				</td>
																				<td valign="top">
                                                                                    
																				</td>
																			</tr>
																		</table>
																		</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
                                                <%--857 Starts--%>												
												<table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABCollOptions"
													id="FORMTABCollOptions" style="display:none;">
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td class="ctrlgroup" colspan="4">Collection Reserve Types listed below</td>
																</tr>
                                                                <tr>
																	<td width="25%">
																		<table>
																			<tr>
																				<td align="left">
																		Reserve Collection Options
																		        </td>
																			</tr>
																			<tr>
																				<td align="left">
																			        <uc:MultiCode runat="server" ID="lstRsvColl" width="500px" Height="150px" CodeTable="RESERVE_TYPE" ControlName="lstRsvColl" RMXRef="/Instance/Document/LOB/LOBParms/Options/CollOptions/RsvCollRsvTypeCode" RMXType="codelist"/>
																				</td>
																				<td valign="top">
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
                                                <%--857 Ends--%>
												<table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABPolicyOptions"
													id="FORMTABPolicyOptions" style="display:none;">
					                                <tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td class="ctrlgroup" colspan="4">Policy Management</td>
																</tr>
															</table>
														</td>
													</tr>
                                                    <%--Sumit - 09/21/2009 MITS#18227 Add "Use Enhanced Policy Flag" --%>
													<tr>
														<td>
														<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr id="UseEnhPolRow">
																	<td id="Td1" width="25%">
																		<asp:Label ID="Label1" Text="Use Policy Management System:" runat="server"></asp:Label>
														
															</td>
																	<td width="25%">
																		<!--Start:Added by Nitin Goel: Call a method DisablePolicyFilterFlag() to disable property/vehicle filter06/09/2010-->
                                                                        <asp:CheckBox ID="UseEnhPol" onchange="setDataChanged(true);"  onclick="setDataChanged(true);SystemParameter_UseEnhPol();DisableOnClick(this);DisablePolicyFilterFlag();" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/UseEnhPol" value="True" type="checkbox" runat="server"/>
																		<!--End:Nitin Goel,Call a method DisablePolicyFilterFlag() to disable property/vehicle filter06/09/2010-->
																	</td>
																	<td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
                                                    </tr>
											            </table> 
                                                     </td>
                                                   </tr>
                                                   <tr id="trPolDownDupFlag" runat="server">
                                                        <td>
                                                            <table border="0" cellspacing="0" celpadding="0" width="100%">
                                                                <tr>
                                                                    <td width="25%">Check For Duplicate Claims at Policy Download:
                                                                    </td>
                                                                    <td width="25%">
                                                                        <asp:CheckBox ID="chkDupClmatPolDown" type="checkbox" value="True" onclick="EnableDupCriteriaAtPolDown('2');setDataChanged(true);" onchange="setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateFlag" runat="server" />
                                                                    </td>
                                                                    <td width="25%">&#160;
                                                                    </td>
                                                                    <td width="25%">&#160;
                                                                    </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                  </tr>
                                                   <tr id="trPolDownDupCriteria" runat="server">
                                                        <td>
                                                          <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                          <tr>
                                                            <td width="25%">Duplicate Criteria:
                                                            </td>
                                                            <td colspan="2" align="left" style="padding-left:3px">
                                                                <asp:DropDownList ID="drpDupCriteriaPolDown" onchange="OpenDupCriteriaForPolDownload();setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria" itemsetref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/PolClaimDuplicateCriteria" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td width="25%">&#160;
                                                            </td>
															</tr>
															</table>
														</td>
													</tr>
                                                    <tr id="trUseLimitTrk" runat="server">
                                                        <td>
                                                            <table border="0" cellspacing="0" celpadding="0" width="100%">
                                                                <tr>
                                                                    <td width="25%">Use Policy Limit Tracking:
                                                                    </td>
                                                                    <td width="25%">
                                                                        <asp:CheckBox ID="chkUseLimitTrk" type="checkbox" value="True" onclick="setDataChanged(true);"  rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/UseLimitTracking" runat="server" />
                                                                    </td>
                                                                    <td width="25%">&#160;
                                                                    </td>
                                                                    <td width="25%">&#160;
                                                                    </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                  </tr>
                                                   
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABReserveIncurredBalanceOptions"
                                                    id="FORMTABReserveIncurredBalanceOptions" style="display: none;">
                                                    <tr>
                                                        <td>Calculate Collections in Reserve Balance:</td>
                                                        <td>
                                                            <asp:CheckBox ID="chkCollResBal" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/CollResBal" onchange="setDataChanged(true);" onclick="UnCheckOther('1');setDataChanged(true);" runat="server" /></td>
                                                        <td width="5%"></td>
                                                        <td>Calculate Collections in Incurred Balance:</td>
                                                        <td>
                                                            <asp:CheckBox ID="chkCollIncBal" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/CollIncBal" onchange="setDataChanged(true);" onclick="UnCheckOther('3');setDataChanged(true);" runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Calculate Collections in Reserve Balance Per Reserve:</td>
                                                        <td>
                                                            <asp:CheckBox ID="chkCollResBalPerRsv" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/PerRsvCollInRsvBal" onchange="setDataChanged(true);" onclick="UnCheckOther('2');setDataChanged(true);" runat="server" /></td>
                                                        <td width="5%"></td>
                                                        <td>Calculate Collections in Incurred Balance Per Reserve:</td>
                                                        <td>
                                                            <asp:CheckBox ID="chkCollIncBalPerRsv" type="checkbox" value="True" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/PerRsvCollInIncBal" onchange="setDataChanged(true);" onclick="UnCheckOther('4');setDataChanged(true);" runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="msgheader" colspan="2">Reserve Types for Reserve Balance Calculations :</td>

                                                        <td width="5%"></td>
                                                        <td class="msgheader" colspan="2">Reserve Types for Incurred Balance Calculations :</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <uc:MultiCode runat="server" ID="lstPerRsvTypeRecovery" width="200px" Height="88px" CodeTable="RESERVE_TYPE" ControlName="lstPerRsvTypeRecovery" RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/PerReserveTypesBalance" RMXType="codelist" />
                                                        </td>
                                                        <td width="5%"></td>
                                                        <td colspan="2">
                                                            <uc:MultiCode runat="server" ID="lstPerRsvTypeIncurred" width="200px" height="88px" CodeTable="RESERVE_TYPE" ControlName="lstPerRsvTypeIncurred" RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/PerReserveTypesIncurred" RMXType="codelist" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%--tanwar2 - mits 30910 - start--%>
                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABDeductibleOptions"
													id="FORMTABDeductibleOptions" style="display:none;">
                                                    <%-- start:Added by Nitin goel,MITS 30910,01/23/2013--%>       
                                                    <div title="" style="padding: 0px; margin: 0px" id="deductiblesection" name="DeductibleSection" runat ="server">
                                                    <tr>
														<%--<td>
															<font color="#000000">
																<hr></hr>
															</font>
														</td>--%>
                                                        <td class="ctrlgroup" colspan="4">
                                                                       Deductible Settings
                                                                    </td>
													</tr>                                            
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
														 Calculate Deductible on Payments:
														</td>
														<td width="25%">
                                                             <asp:CheckBox ID="allowtoapplydedpayments" onclick="setDataChanged(true);" type="checkbox" 
                    rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/AllowToApplyDedPayments"   
                        value="True" runat="server" />
                                                            
																	</td>
                                                                    <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>

                                                    <%--start:added by nitin goel for NI--%>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td width="30%">
                                                                        Prevent Printing Zero or Negative Amount Checks and Set to Printed:&nbsp;&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="PreventPrintZeroCheck" onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/PreventPrintZeroCheck" type="checkbox" runat="server" />
                                                                    </td>                                                                                                                            
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        
                                                    </tr>                                                    
                                                     <%--<tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td width="30%">
                                                                        Prevent Edit of Deductible Aggregate/Group Info:&nbsp;&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="PreventEditDedAggInfo" onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/PreventEditDedAggInfo" type="checkbox" runat="server" />
                                                                    </td>                                                                                                                            
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        
                                                    </tr>--%>
                                                    <%--end:added by nitin goel for NI Enhancement--%>
                                                     <tr>
                                                    <td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
																		Deductible Reserve Type:
																	</td>
																	<td>
                                                                      <div title="" style="padding: 0px; margin: 0px">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="dedrecreservetype" CodeTable="RESERVE_TYPE" ControlName="dedrecreservetype" RMXRef="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/systemcontrol[@name ='DedRecReserveType']" Filter="CODES.RELATED_CODE_ID IN(dedreservetype)" RMXType="code" />
             </div>
																	</td>
                                                                     <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
																</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
																		Deductible Transaction Type:
																	</td>
																	<td>
                                                                      <div title="" style="padding: 0px; margin: 0px">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="dedrectransactiontype" CodeTable="TRANS_TYPES" ControlName="dedrectransactiontype" RMXRef="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/systemcontrol[@name ='DedRecTransactionType']" Filter="CODES.RELATED_CODE_ID IN(dedtranstype)" RMXType="code" />
             </div>
																	</td>
                                                                     <td width="25%"><asp:TextBox ID="ToStoreMasterRecoveryCode" style="display:none" RMXRef="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/ToStoreMasterRecoveryCode" runat="server" /></td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
																</td>
                                                    </tr>
                                                    <%--Start: Commented by Nikhil ON 07/10/14--%>
                                        <%--             <div title="" style="padding: 0px; margin: 0px" id="divGCLossType" name="GeneralClaimLossType" runat ="server">
                                                     <tr>
                                                    <td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
																		Deductible Reserve Loss Type:
																	</td>
																	<td >
                                                                      <div title="" style="padding: 0px; margin: 0px">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="dedrecreservelosstype" CodeTable="LOSS_CODES" ControlName="dedrecreservelosstype" RMXRef="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/systemcontrol[@name ='DedRecReserveLossTypeGC']" Filter="" RMXType="code" />
             </div>
																	</td>
                                                                     <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
																</td>
                                                    </tr>
                                                    </div>
                                                    <div title="" style="padding: 0px; margin: 0px" id="divWCDisabLossType" name="WorkersClaimLossType" runat ="server">
                                                    <tr>
                                                    <td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
																		Deductible Reserve Disability Type:
																	</td>
																	<td>
                                                                      <div title="" style="padding: 0px; margin: 0px">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="dedrecreservedisabilitytype" CodeTable="DISABILITY_TYPE_CODE" ControlName="dedrecreservedisabilitytype" RMXRef="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/systemcontrol[@name ='DedRecReserveLossTypeWC']" Filter="" RMXType="code" />
             </div>
																	</td>
                                                                     <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
																</td>
                                                    </tr>
                                                    </div>--%>
                                                  <%--  end: Commented by Nikhil ON 07/10/14--%>
									<%--					<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="25%">
														 Do Not Allow Add/Edit Deductible Reserve:
														</td>
														<td width="25%">
                                                             <asp:CheckBox ID="AllowEditDeductibleReserve" onclick="setDataChanged(true);" type="checkbox" 
                    rmxref="/Instance/Document/LOB/LOBParms/Options/ClaimOptions/AllowEditDeductibleReserve"   
                        value="True" runat="server" />
                                                            
																	</td>
                                                                    <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>--%>

                                                    <%--tanwar2 - mits 30910 - start--%>
                                                    <tr>
                                                        <td>
                                                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td width="30%">
                                                                    Diminishing % Per Year:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="DimPercent" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/DiminishingPercent" onchange="setDataChanged(true);" onblur="numLostFocus(this)" type="text" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td width="25%">&#160;</td>
                                                                <td width="25%">&#160;</td>
                                                            </tr>                                                        
                                                        </table>
                                                        </td>
                                                    </tr>
                                                    <%--tanwar2 - mits 30910 - end--%>

                                                    <%-- Start: Added by Sumit Agarwal to add Deductible settings parameter at Utility Level :10/10/2014 : --%>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td width="30%">
                                                                       Automatic Deductible Recovery Identifier Character:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDedRecoveryIdentifierChar" onkeyup="this.value=this.value.toUpperCase()" MaxLength="1" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/DedRecoveryIdentifierChar" onchange="setDataChanged(true);" type="text" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td width="25%">&#160;</td>
                                                                    <td width="25%">&#160;</td>
                                                                </tr>                                                        
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td width="30%">
                                                                       Manual Deductible Recovery Identifier Character:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtManualDedRecChar" onkeyup="this.value=this.value.toUpperCase()" MaxLength="2" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/ManualDedRecoveryIdentifierChar" onchange="setDataChanged(true);" type="text" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td width="25%">&#160;</td>
                                                                    <td width="25%">&#160;</td>
                                                                </tr>                                                        
                                                            </table>
                                                        </td>
                                                    </tr>
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
														                Enable Diminishing Deductibles For First Party:
														            </td>
            														<td width="25%">
                                                                        <asp:CheckBox ID="chkEnableDiminishingDedFirstParty" onclick="setDataChanged(true);" type="checkbox" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/EnableDiminishingDedFirstParty" value="True" runat="server" />
																	</td>
                                                                    <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>

													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
														                Enable Shared And Aggregate Deductibles For Third Party:
														            </td>
            														<td width="25%">
                                                                        <asp:CheckBox ID="chkEnableSharedAggregateDedThirdParty" onclick="setDataChanged(true);" type="checkbox" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/EnableSharedAggregateDedThirdParty" value="True" runat="server" />
																	</td>
                                                                    <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>

													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td width="30%">
														                Enable Distribution Of Deductible Amount To Subsequent Payment(s) When Payment Is Voided For Third Party:
														            </td>
            														<td width="25%">
                                                                        <asp:CheckBox ID="chkEnableDistributionDedAmtToSubsequentPmtThirdParty" onclick="setDataChanged(true);" type="checkbox" rmxref="/Instance/Document/LOB/LOBParms/Options/DeductibleOptions/EnableDistributionDedAmtToSubsequentPmtThirdParty" value="True" runat="server" />
																	</td>
                                                                    <td width="25%">&#160;</td>
																	<td width="25%">&#160;</td>
																</tr>
															</table>
														</td>
													</tr>
                                                    <%-- End: Added by Sumit Agarwal to add Deductible settings parameter at Utility Level :10/10/2014 : --%>

                                                    </div>
                                                    <%-- end:Added by Nitin goel,MITS 30910,01/23/2013--%>
                                                </table>
                                                <%--tanwar2 - mits 30910 - end--%>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								
								<script language="javascript" type="text/javascript">
									try
									{
									  
									var elem = document.getElementById("ReserveTracking");
									var ctrl= document.getElementById("hdnLOBSelected");
									document.forms[0].hdnUpdateClaimResType.value="N";
									if (ctrl.value=='243' || ctrl.value=='844')
										elem.style.display = 'none';
									else
										elem.style.display = '';
									HideDiv();
									DisableNR();
									UnCheckAutoAdjust();
									EntityEnable();
									EnableClaimTypeStartUp();
									checkDoNothing();
									DuplicateEnable('1');
									EnableDupCriteriaAtPolDown('1');
									ClaimEvent();
									selectSelectedTab();
									var elem1 = document.getElementById("divWeeks");
									if (ctrl.value!='844')
										elem1.style.display = 'none';
									else
										elem1.style.display = '';
									
									//Sumit - 09/21/2009 MITS#18227 Hide "Policy Options" Tab.
									var elemPolOptTab = document.getElementById("TABSPolicyOptions");
									if (ctrl.value=='844')
									{
										elemPolOptTab.style.display = 'none';
									}
									else
									{
										elemPolOptTab.style.display = '';
									}
									//smahajan6 - 02/03/2010 MITS#18230 - Start 
									//Show "Filter Property For Selected Policy" checkbox cnly for Property Claim and Vehcile Accident Claim
									var elemIsPolicyFilter = document.getElementById("IsPolicyFilter");
									var elemlblIsPolicyFilter = document.getElementById("lblIsPolicyFilter");
									if (ctrl.value == '242')
									{
										elemIsPolicyFilter.style.display = '';
										elemlblIsPolicyFilter.innerText = 'Filter Vehicle For Selected Policy:';
									}
									else if (ctrl.value == '845')
									{
										elemIsPolicyFilter.style.display = '';
										elemlblIsPolicyFilter.innerText = 'Filter Property For Selected Policy:';
									}
									else
									{
										elemIsPolicyFilter.style.display = 'none';
									}
									//smahajan6 - 02/03/2010 MITS#18230 - End
									}catch(ex){}
									</script>	
							</td>
							
						</tr>
					</table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
