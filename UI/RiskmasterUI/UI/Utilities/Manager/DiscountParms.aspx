﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiscountParms.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.Manager.DiscountParms" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%--VACo Changes MITS 18231 STARTS--%>
    <title>Add/Modify Discount/Surcharge</title>
    <%--VACo Changes MITS 18231 ENDS--%>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
    <%--vkumar258 - RMA-6037 - Starts --%>

    <%--<script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>

    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>

    <uc3:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload="CopyGridRowDataToPopup();CheckForWindowClosing();">
    <form id="frmData" method="post" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforDiscountParms();"
                OnClick="save_Click" /></div>
    </div>
    <div class="msgheader" id="formtitle">
    <%--VACo Changes MITS 18231 STARTS--%>
        Add/Modify Discount/Surcharge</div>
    <%--VACo Changes MITS 18231 ENDS--%>
    <table border="0">
        <tr>
            <td class="ctrlgroup" colspan="2">
            <%--VACo Changes MITS 18231 STARTS--%>
                Discount/Surcharge Parameters
            <%--VACo Changes MITS 18231 ENDS--%>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_UseVolDisc" Text="Use Volume Discount:" />
            </td>
            <td>
                <asp:CheckBox runat="server" Checked="false" RMXref="/Instance/Document/Document/discount/UseVolDisc"
                    ID="UseVolDisc" onclick="return UseVolumeDiscount();" />
            </td>
        </tr>
        <tr>
            <td>
            <%--VACo Changes MITS 18231 STARTS--%>
                <asp:Label runat="server" ID="lbl_DiscountName" Text="Discount/Surcharge Name:" class="required" />
            <%--VACo Changes MITS 18231 ENDS--%>
            </td>
            <td>
                <asp:TextBox runat="server" ID="DiscountName" RMXref="/Instance/Document/Document/discount/discountname"/>
                <asp:TextBox runat="server" Style="display: none" RMXref="/Instance/Document/Document/discount/discountnametemp"
                    ID="DiscountNameTemp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_LOBcode" Text="Line of Business:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" name="LOBcode" ControlName="LOBcode" ID="LOBcode"
                    type="code" CodeTable="POLICY_LOB" ref="/Instance/Document/Document/discount/lob" />
                <asp:TextBox runat="server" Style="display: none" ID="LOBcodetemp"></asp:TextBox>
                <asp:TextBox runat="server" Style="display: none" ID="LOBcodeidtemp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Statecode" Text="State:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" name="Statecode" ControlName="Statecode" ID="Statecode"
                    type="code" CodeTable="states" ref="/Instance/Document/Document/discount/state" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Amount" Text="Amount/Rate:" class="required" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="Amount" ref="/Instance/Document/Document/discount/amount" />
                <asp:TextBox runat="server" Style="display: none" ID="AmountTemp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_DiscountType" Text="Flat Or Percent:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" name="DiscountType" ID="DiscountType" ControlName="DiscountType"
                    type="code" CodeTable="DISCOUNT_TYPE" class="required" ref="/Instance/Document/Document/discount/discounttype" />
                <asp:TextBox runat="server" Style="display: none" ID="DiscountTypeCodeIdTemp"></asp:TextBox>
                <asp:TextBox runat="server" Style="display: none" ID="DiscountTypeTemp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Range Criteria:&nbsp;&nbsp;
            </td>
            <td>
                <asp:Button runat="server" class="button" OnClientClick="return openPremiumRangeCriteria();"
                    Text="..." />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_EffectiveDate" Text="Effective Date:" class="required" />&nbsp;&nbsp;
            </td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document/Document/discount/effectivedate"
                    RMXType="date" TabIndex="2" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <%--vkumar258 - RMA-6037 - Starts --%>

                    <%--<asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "EffectiveDate",
				                ifFormat: "%m/%d/%Y",
				                button: "EffectiveDatebtn"
				            }
				            );
                </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#EffectiveDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "3");
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" class="label" ID="lbl_ExpirationDate" Text="Expiration Date:" />&nbsp;&nbsp;
            </td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" ID="ExpirationDate" RMXRef="/Instance/Document/Document/discount/expirationdate"
                    RMXType="date" TabIndex="4" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    <%--vkumar258 - RMA-6037 - Starts --%>
                    <%--<asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "ExpirationDate",
				                ifFormat: "%m/%d/%Y",
				                button: "ExpirationDatebtn"
				            }
				            );
                </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#ExpirationDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
                        });
                    </script>
                    <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
        </table>
        <asp:textbox style="display: none" runat="server" id="RowId" rmxref="/Instance/Document/Document/discount/discountrowid" />
        <asp:textbox style="display: none" runat="server" id="selectedrowposition"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="gridname"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="txtPostBack"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="validate"></asp:textbox>
        <!-- abansal23: MITS 15212 on 04/09/2009 -->
        <asp:textbox style="display: none" runat="server" id="mode"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="txtData"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="Action"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="FormMode"></asp:textbox>
        <asp:textbox style="display: none" runat="server" id="FunctionToCall"></asp:textbox>
        <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="Loading" />
    </form>
</body>
</html>
