﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using System.Text;

namespace Riskmaster.UI.Utilities
{
    public partial class FiscalYearSetup : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
           
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           divOrg.Visible = false;
           tblFiscallist.Visible = true;
           if (hdnAction.Value == "")
           {
               GetWCTransData("FiscalYearsListAdaptor.Get");
           }
           
        }

        private XmlDocument GetWCTransData(string sFunctionName)
        {
            string sreturnValue = "";
            XmlDocument xmlDoc = GetMessageTemplate(sFunctionName);
            XmlDocument xmlOutDoc = new XmlDocument();
            DataTable dtGridData = new DataTable();

            try
            {

                sreturnValue = AppHelper.CallCWSService(xmlDoc.InnerXml);
                ErrorControl1.errorDom = sreturnValue;

                xmlOutDoc.LoadXml(sreturnValue);


                XmlNode xmlNodeFiscalList = xmlOutDoc.SelectSingleNode("ResultMessage/Document/FiscalYearsList/control[@name ='FiscalYearList']");
                if (xmlNodeFiscalList != null)
                {
                    GridHeaderAndData(xmlNodeFiscalList.SelectSingleNode("listhead"), xmlNodeFiscalList.SelectNodes("listrow"), dtGridData);
                        
                    if (tblFiscallist.Visible)
                    {
                        gvFiscalYearSetup.DataSource = dtGridData;
                        gvFiscalYearSetup.DataBind();
                        gvFiscalYearSetup.Rows[dtGridData.Rows.Count-1].Visible = false;
                    }
                    else
                    {
                        grdOrg.DataSource = dtGridData;
                        grdOrg.DataBind();
                        grdOrg.Rows[dtGridData.Rows.Count-1].Visible = false;
                    }
                   
                  
                }
                if (xmlOutDoc.SelectSingleNode("ResultMessage/Document/FiscalYearsList/control[@name ='CurrentYear']") != null)
                {
                    hdnCurrentYear.Value = xmlOutDoc.SelectSingleNode("ResultMessage/Document/FiscalYearsList/control[@name ='CurrentYear']").InnerXml;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

            return xmlOutDoc;

        }


        private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;
           

            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.GetAttribute("colname").Trim();

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {


                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();
  
                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        if (iColumnIndex == 0)
                        {
                            dr[iColumnIndex++] = objElem.GetAttribute("value");
                        }
                        else
                        {
                            dr[iColumnIndex++] = objElem.GetAttribute("title");
                        }

                    }
                    dtGridData.Rows.Add(dr);

                }
            }
           
            dr = dtGridData.NewRow();
            dtGridData.Rows.Add(dr);

        }





        private XmlDocument GetMessageTemplate(string sFunctionName)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
            <Message>
            <Authorization></Authorization> 
            <Call>
            <Function>"+ sFunctionName + @"</Function> 
            </Call>
            <Document>
            <FiscalYearsList>
            <control name='LineOfBusiness'>" + lobcode_cid.Value + @"</control> 
            <control name='Organization'>"+ orgcode_cid.Value + @"</control> 
            <control name='CurrentYear' type='code' >" + hdnCurrentYear.Value + @"</control>   
            <control name='RowId' type='id' >" +  hdnSelected.Value + @"</control>  
            <control name='FiscalYearList' type='radiolist' /> 
            </FiscalYearsList>
            </Document>
            </Message>
            "
            );

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }

        protected void btAllLOB_Click(object sender, EventArgs e)
        {
            lobcode.Value = "All Lines";
            lobcode_cid.Value = "0";
            GetWCTransData("FiscalYearsListAdaptor.Get");
        }

        protected void btAllOrganization_Click(object sender, EventArgs e)
        {
            orgcode.Text = "Entire Organization";
            orgcode_cid.Value = "0";
            GetWCTransData("FiscalYearsListAdaptor.Get");
        }

      

        protected void orgcode_TextChanged(object sender, EventArgs e)
        {
            GetWCTransData("FiscalYearsListAdaptor.Get");
        }

        protected void btLobOrg_Click(object sender, EventArgs e)
        {
            divOrg.Visible = true;
            tblFiscallist.Visible = false;
            GetWCTransData("FiscalYearsListAdaptor.GetLobOrg");
        }

        protected void btDelete_Click(object sender, EventArgs e)
        {
            GetWCTransData("FiscalYearsListAdaptor.DeleteFiscalYear");
            hdnSelected.Value = "";
            GetWCTransData("FiscalYearsListAdaptor.Get");

            hdnAction.Value = "";
        }

        protected void gvFiscalYearSetup_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectFiscalYear('" + DataBinder.Eval(e.Row.DataItem, "FiscalYear").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }

        protected void btPrint_Click(object sender, EventArgs e)
        {
            XmlNode xmlOLE = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            XmlDocument xmlOutDoc = new XmlDocument();
            try
            {
                xmlOutDoc = GetWCTransData("FiscalYearsListAdaptor.PrintFiscalYearList");

                xmlOLE = xmlOutDoc.SelectSingleNode("ResultMessage/Document/FiscalYearList/Report");

                if (xmlOLE != null)
                {
                    string sFileContent = xmlOLE.InnerXml;
                    byte[] byteOrg = Convert.FromBase64String(sFileContent);

                    Response.Clear();
                    Response.Charset = "";
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=FiscalSetup.pdf");

                    Response.BinaryWrite(byteOrg);
                    //Response.End();
                }


            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                hdnAction.Value = "";
            }

        }



    }
}
