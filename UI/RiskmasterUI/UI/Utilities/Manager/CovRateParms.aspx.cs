﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;

///<Summary>
// Developed By: Tushar Agarwal
// Completed On: 26 Oct, 2009
//MITS 18231
///<Summary>
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class CovRateParms : NonFDMBasePageCWS
    {
       private XmlDocument oFDMPageDom = null;
        XElement XmlTemplate = null;
        string sCWSresponse = string.Empty  ;
        XmlDocument XmlDoc = new XmlDocument();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (!IsPostBack)
                {
                    Session["RowCount"] = -1;
                    Session["raterangelist"] = Getraterangelist();
                    Session["PostBackraterangelist"] = "";
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    EditAddMode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    if (EditAddMode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("ExpRateParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, false);
                        BindControls();
                    }
                }
                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";
                    Control CtlPostBack = DatabindingHelper.GetPostBackControl(this.Page);
                    if (CtlPostBack == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("ExpRateParmsAdaptor.Get", XmlTemplate, out sCWSresponse, false, false);
                        BindControls();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

       private XElement GetMessageTemplate()
        {
            if (EditAddMode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("ExpRateParmsAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><ExpRates>");
            // Start:Added by Nitin Goel,02/19/2010,MITS#18231
            //sXml = sXml.Append("<amount>" + Amount.Text + "</amount>");
            sXml = sXml.Append("<amount>" + CovAmount.Text + "</amount>");
            //End:Nitin Goel,02/19/2010,MITS#18231
            sXml = sXml.Append("<LINEOFBUSINESS codeid='" + covlob.CodeIdValue + "'>" + covlob.CodeText + "</LINEOFBUSINESS>");
            sXml = sXml.Append("<RateMaintenanceType>C</RateMaintenanceType>");
            sXml = sXml.Append("<exposureid codeid='" + Coverage.CodeIdValue + "'>" + Coverage.CodeTextValue.Replace("&","&amp;")  + "</exposureid>");
            sXml = sXml.Append("<effectivedate>" + EffectiveDate.Text + "</effectivedate>");
            sXml = sXml.Append("<expirationdate>" + ExpirationDate.Text + "</expirationdate>");
            sXml = sXml.Append("<expratetype codeid='" + ExpRateType.CodeIdValue + "'>" + ExpRateType.CodeTextValue + "</expratetype>");
            sXml = sXml.Append("<state  codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</state>");
            sXml = sXml.Append("<fixedorprorate codeid='" + FixedOrProRata.CodeIdValue + "'>" + FixedOrProRata.CodeTextValue + "</fixedorprorate >");
            sXml = sXml.Append("<OrgHierarchyCode codeid='" + OrgHierarchyCode_cid.Text + "'>" + OrgHierarchyCode_cid.Text + "</OrgHierarchyCode >");
            
            sXml = sXml.Append("<expraterowid>" + RowId.Text + "</expraterowid>");
            if (EditAddMode.Text.ToLower() == "edit")
            {
                sXml = sXml.Append("<EditAddMode>" + EditAddMode.Text + "</EditAddMode>");
            }
            else
            {
                sXml = sXml.Append("<EditAddMode />");
            }

            if (Session["raterangelist"] != null)
            {
                string strTemp = Session["raterangelist"].ToString();
                if (strTemp.LastIndexOf("<raterangerowid>-") > 0)
                {
                    Session["PostBackraterangelist"] = Session["raterangelist"].ToString();
                    string strTempChar = strTemp[strTemp.LastIndexOf("<raterangerowid>-") + 17].ToString();
                    for (int i = 1; i <= Int32.Parse(strTempChar); i++)
                    {
                        Session["raterangelist"] = Session["raterangelist"].ToString().Replace("-" + i, "");
                    }
                }
                sXml = sXml.Append(Session["raterangelist"].ToString());
            }

            sXml = sXml.Append("</ExpRates></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("ExpRateParmsAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Binds data to the controls
        /// </summary>
        internal void BindControls()
        {
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

           XmlElement objExpRatesXMLEle = null;
           objExpRatesXMLEle = (XmlElement)oFDMPageDom.SelectSingleNode("//ExpRates");

           // Start:Added by Nitin Goel,02/19/2010,MITS#18231
           //Amount.Text = objExpRatesXMLEle.GetElementsByTagName("amount").Item(0).InnerText;
           CovAmount.Text = objExpRatesXMLEle.GetElementsByTagName("amount").Item(0).InnerText;
           //End:Nitin Goel,02/19/2010,MITS#18231
           ((TextBox)covlob.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("LINEOFBUSINESS").Item(0).InnerText;
           ((TextBox)covlob.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("LINEOFBUSINESS").Item(0).Attributes["codeid"].Value;
          ((TextBox)Coverage.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).InnerText;
           ((TextBox)Coverage.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).Attributes["codeid"].Value;
           EffectiveDate.Text = objExpRatesXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
           ExpirationDate.Text = objExpRatesXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
           ((TextBox)ExpRateType.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0).InnerText;
           ((TextBox)ExpRateType.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0).Attributes["codeid"].Value;
           ((TextBox)Statecode.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).InnerText;
           ((TextBox)Statecode.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value;
           ((TextBox)FixedOrProRata.FindControl("codelookup")).Text = objExpRatesXMLEle.GetElementsByTagName("fixedorprorate").Item(0).InnerText;
           ((TextBox)FixedOrProRata.FindControl("codelookup_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("fixedorprorate").Item(0).Attributes["codeid"].Value;
           ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode")).Text = objExpRatesXMLEle.GetElementsByTagName("OrgHierarchyCode").Item(0).InnerText;
           ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode_cid")).Text = objExpRatesXMLEle.GetElementsByTagName("OrgHierarchyCode").Item(0).Attributes["codeid"].Value;
           Session["raterangelist"] = objExpRatesXMLEle.SelectSingleNode("//raterangelist").OuterXml;
           AddExtraCol();
           
        }


        /// <summary>
        /// Added By Tushar for Rate Range
        /// Gets The Rate list for RangeCriteria
        /// </summary>
        /// <returns></returns>
        private string Getraterangelist()
        {
            StringBuilder sList = new StringBuilder("<raterangelist>");
            sList = sList.Append("<listhead>");
            sList = sList.Append("<addedbyuser>Added By User</addedbyuser>");
            sList = sList.Append("<dttmrcdadded>Dttm Rcd Added</dttmrcdadded>");
            sList = sList.Append("<updatedbyuser>Updated By User</updatedbyuser>");
            sList = sList.Append("<dttmrcdlastupd>Dttm Rcd Last Upd</dttmrcdlastupd>");
            //Anu Tennyson MITS 18231 2/1/2010 STARTS
            sList = sList.Append("<rangetype>RangeType</rangetype>");
            //Anu Tennyson MITS 18231 ENDS
            //Start: Neha Suresh Jain,05/10/2010 MITS 20659, Rename Modifier to Premium Modifier
            sList = sList.Append("<modifier>Premium Modifier</modifier>");
            //End: Neha Suresh Jain,05/10/2010 MITS 20659
            sList = sList.Append("<Deductible>Deductible</Deductible>");
            sList = sList.Append("<beginningrange>Beginning Range</beginningrange>");
            sList = sList.Append("<raterangerowid>Rate Range Row Id</raterangerowid>");
            sList = sList.Append("<endrange>End Range</endrange></listhead></raterangelist>");

            return sList.ToString();
        }
        
        internal void AddExtraCol()
        {
            string strData = Session["raterangelist"].ToString();
            XmlDocument xdata = new XmlDocument();
            xdata.LoadXml(strData);
            XmlNodeList nodes = xdata.SelectNodes("//raterange");
            XmlText extraValue=null;
            string sType = string.Empty;

            foreach (XmlElement node in nodes)
            {
                XmlElement extra = xdata.CreateElement("Extra");
                sType = node.ChildNodes[4].InnerText.Substring(0, 1); 
                if(sType =="D")
                    extraValue = xdata.CreateTextNode(node.ChildNodes[5].InnerText + "|" + node.ChildNodes[6].InnerText + "|" + node.ChildNodes[9].InnerText + "|D|" + node.ChildNodes[8].InnerText);
                else
                    extraValue = xdata.CreateTextNode(node.ChildNodes[5].InnerText + "|" + node.ChildNodes[7].InnerText + "|" + node.ChildNodes[9].InnerText + "|R|" + node.ChildNodes[8].InnerText);
                extra.AppendChild(extraValue);
                node.AppendChild(extra);
            }
            Session["raterangelist"] = xdata.OuterXml;
        }
    }
}
