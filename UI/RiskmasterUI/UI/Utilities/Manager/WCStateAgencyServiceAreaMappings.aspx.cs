﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class WCStateAgencyServiceAreaMappings1 : NonFDMBasePageCWS
    {
        private XElement oMessageElement = null;
        private XmlDocument CurrentXDoc = new XmlDocument();
        private string sCWSresponse = string.Empty;
        private string message = string.Empty;

        
        protected void Page_Load(object sender, EventArgs e)
        {
            Label lblError = (Label)ErrorControl.FindControl("lblError");
            lblError.Text = "";

            if (!IsPostBack)
            {

                hdJurisSelected.Text = "";
                hdOfficeSelected.Text = "";
                hdAgentsSelected.Text = "";
                hdnSelected.Text = "";
                hdnFind.Text = "";
                try
                {
                    oMessageElement = GetMessageTemplate("Get");
                    CallCWS("WorkCompAgentServiceAreaAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                    lstJurisdictions.Items[0].Value = "";
                    lstOfficeFunction.Items[0].Value = "";
                    lstAgents.Items[0].Value = "";
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }


            }
            

            if (IsPostBack)
            {
                lstJurisdictions.Items[0].Value = "";
                lstOfficeFunction.Items[0].Value = "";
                lstAgents.Items[0].Value = "";
                hdJurisSelected.Text = lstJurisdictions.SelectedValue;
                hdOfficeSelected.Text = lstOfficeFunction.SelectedValue;
                hdAgentsSelected.Text = lstAgents.SelectedValue;
                message = "";
                if (hdnFind.Text != "Find")
                    txtFindZip.Text = "";


            }

        }

        protected void FindZipCode(object sender, EventArgs e)
        {
 
            oMessageElement = GetMessageTemplate("FindZip");
            CallCWS("WorkCompAgentServiceAreaAdaptor.FindZip", oMessageElement, out sCWSresponse, false, true);
            lstJurisdictions.Items[0].Value = "";
            lstOfficeFunction.Items[0].Value = "";
            lstAgents.Items[0].Value = "";
            lstJurisdictions.SelectedValue = hdJurisSelected.Text;
            lstOfficeFunction.SelectedValue = hdOfficeSelected.Text;
            lstAgents.SelectedValue = hdAgentsSelected.Text;
            hdnFind.Text = "";
            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sCWSresponse);
            if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
            {
                message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                Exception ex = new Exception(message);
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
           

        }

        protected void SaveAgents(object sender, EventArgs e)
        {
            string sJurisSel = string.Empty, sOfficeSel = string.Empty, sAgentsSel = string.Empty;
            bool bReturnStatus = false;
            try
            {
                //PJS -MITS 14775 (03-23-2009): changes to false as no bindata2control is required
                bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.Save", null, out sCWSresponse, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        oMessageElement = GetSaveMessageTemplate("Get");
                        txtlstJurisdictionalTableName.Text = "";
                        txtlstServiceAreaTableName.Text = "";
                        bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                        LoadResponse(sCWSresponse, out sJurisSel, out sOfficeSel, out sAgentsSel);
                        hdJurisSelected.Text = sJurisSel;
                        hdOfficeSelected.Text = sOfficeSel;
                        hdAgentsSelected.Text = sAgentsSel;
                    }
                    else
                        if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                        {
                            lstJurisdictions.SelectedValue = hdJurisSelected.Text;
                            lstOfficeFunction.SelectedValue = hdOfficeSelected.Text;
                            lstAgents.SelectedValue = hdAgentsSelected.Text;
                            message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                            Exception ex = new Exception(message);
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }

                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
           
        }

        protected void DeleteAgents(object sender, EventArgs e)
        {
            XmlNode sOffice, sJuris;
            bool bReturnStatus = false;
            try
            {
                //PJS -MITS 14775 (03-23-2009): changes to false as no bindata2control is required
                bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.DeleteAgency", null, out sCWSresponse, true, false);
                if (bReturnStatus)
                {
                    hdAgentsSelected.Text = "";
                    lstJurisdictions.SelectedValue = hdJurisSelected.Text;
                    lstOfficeFunction.SelectedValue = hdOfficeSelected.Text;
                    lstAgents.SelectedValue = hdAgentsSelected.Text;
                    oMessageElement = GetMessageTemplate("Get");
                    CallCWS("WorkCompAgentServiceAreaAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                    CurrentXDoc.LoadXml(sCWSresponse);
                    lstAgents.Items[0].Value = "";
                    lstJurisdictions.Items[0].Value = "";
                    lstOfficeFunction.Items[0].Value = "";
                    sOffice = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='OfficeFunction']/option[@selected='1']/@value");
                    sJuris = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='Jurisdictions']/option[@selected='1']/@value");
                    if (sJuris != null)
                        lstJurisdictions.SelectedValue = sJuris.Value;
                    if (sOffice != null)
                        lstOfficeFunction.SelectedValue = sOffice.Value;
                    lstAgents.SelectedValue = lstAgents.Items[0].Value;
                }
                hdJurisSelected.Text = lstJurisdictions.SelectedValue;
                hdOfficeSelected.Text = lstOfficeFunction.SelectedValue;
                hdAgentsSelected.Text = lstAgents.SelectedValue;
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void DrpGet(object sender, EventArgs e)
        {

            if (hdnSelected.Text == "Add New")
            {
                hdnSelected.Text = "";
            }
            else
            {
                string sJurisSel = string.Empty, sOfficeSel = string.Empty, sAgentsSel = string.Empty;
                try
                {
                    oMessageElement = GetMessageTemplate("Get");
                    CallCWS("WorkCompAgentServiceAreaAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                    LoadResponse(sCWSresponse, out sJurisSel, out sOfficeSel, out sAgentsSel);
                    hdJurisSelected.Text = sJurisSel;
                    hdOfficeSelected.Text = sOfficeSel;
                    hdAgentsSelected.Text = sAgentsSel;
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                
            }

        }

        protected void AddJurisPostalCodes(object sender, EventArgs e)
        {
            string sJurisSel = string.Empty, sOfficeSel = string.Empty, sAgentsSel = string.Empty;
            bool bReturnStatus = false;
            try
            {
                oMessageElement = GetMessageTemplate("AddJurisPostalCodes");
                //PJS -MITS 14775 (03-23-2009): changes to false as no bindata2control is required
                bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.AddJurisPostalCodes", oMessageElement, out sCWSresponse, false, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                    txtJurisCodeAdd.Text = "";
                    oMessageElement = GetMessageTemplate("Get");
                    bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                    LoadResponseNoAgents(sCWSresponse, out sJurisSel, out sOfficeSel, out sAgentsSel);
                    hdJurisSelected.Text = sJurisSel;
                    hdOfficeSelected.Text = sOfficeSel;
                    hdAgentsSelected.Text = sAgentsSel;
                    }
                    else
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        lstJurisdictions.SelectedValue = hdJurisSelected.Text;
                        lstOfficeFunction.SelectedValue = hdOfficeSelected.Text;
                        lstAgents.SelectedValue = hdAgentsSelected.Text;
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                       
                    }
                }
                
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void DeletePostalCode(object sender, EventArgs e)
        {
            string sJurisSel = string.Empty, sOfficeSel = string.Empty, sAgentsSel = string.Empty;
            bool bReturnStatus = false;
            try
            {
                oMessageElement = GetMessageTemplate("DeleteJurisPostalCodes");
                //PJS -MITS 14775 (03-23-2009): changes to false as no bindata2control is required
                bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.DeleteJurisPostalCodes", oMessageElement, out sCWSresponse, false, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {

                        txtJurisCodDelete.Text = "";
                        oMessageElement = GetMessageTemplate("Get");
                        bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                        LoadResponseNoAgents(sCWSresponse, out sJurisSel, out sOfficeSel, out sAgentsSel);
                        hdJurisSelected.Text = sJurisSel;
                        hdOfficeSelected.Text = sOfficeSel;
                        hdAgentsSelected.Text = sAgentsSel;
                    }
                    else
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        lstJurisdictions.SelectedValue = hdJurisSelected.Text;
                        lstOfficeFunction.SelectedValue = hdOfficeSelected.Text;
                        lstAgents.SelectedValue = hdAgentsSelected.Text;
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                    }
                }
                

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void SavePostalCode(object sender, EventArgs e)
        {
            string sJurisSel = string.Empty, sOfficeSel = string.Empty, sAgentsSel = string.Empty;
            bool bReturnStatus = false;
            try
            {
                UpdateListItems();
               // bReturnStatus = CallCWSFunction("WorkCompAgentServiceAreaAdaptor.SaveZips");

                bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.SaveZips", null, out sCWSresponse, true, true);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {

                        oMessageElement = GetMessageTemplate("Get");
                        txtlstJurisdictionalTableName.Text = "";
                        txtlstServiceAreaTableName.Text = "";
                        bReturnStatus = CallCWS("WorkCompAgentServiceAreaAdaptor.Get", oMessageElement, out sCWSresponse, false, true);
                        LoadResponse(sCWSresponse, out sJurisSel, out sOfficeSel, out sAgentsSel);

                        hdJurisSelected.Text = sJurisSel;
                        hdOfficeSelected.Text = sOfficeSel;
                        hdAgentsSelected.Text = sAgentsSel;
                    }
                    else
                    if(oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        lstJurisdictions.SelectedValue = hdJurisSelected.Text;
                        lstOfficeFunction.SelectedValue = hdOfficeSelected.Text ;
                        lstAgents.SelectedValue = hdAgentsSelected.Text;
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err); 
                    }

                    
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


        }

        private XElement GetMessageTemplate(string sMethodName)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>WorkCompAgentServiceAreaAdaptor." + sMethodName + @"</Function></Call>");
            sXml = sXml.Append("<Document><WorkCompAgentServiceArea><control name=\"Jurisdictions\">");
            sXml = sXml.Append(hdJurisSelected.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"NoOfficeSelected\" NoOffice=\"\" /> ");
            sXml = sXml.Append("<control name=\"OfficeFunction\" >");
            sXml = sXml.Append(hdOfficeSelected.Text);
            sXml = sXml.Append("</control><control name=\"Agents\" >");
            sXml = sXml.Append(hdAgentsSelected.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FindZip\">");
            sXml = sXml.Append(txtFindZip.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"AgentsByZip\" />");
            sXml = sXml.Append("<control name=\"Address1\" /><control name=\"BusinessName\" />");
            sXml = sXml.Append("<control name=\"Address2\" /><control name=\"Contact\" />");
            sXml = sXml.Append("<control name=\"Address3\" /><control name=\"Address4\" />");
            sXml = sXml.Append("<control name=\"City\" /><control name=\"ContactPhone\" />");
            sXml = sXml.Append("<control name=\"State\" /><control name=\"PostCode\" />");
            sXml = sXml.Append("<control name=\"ServiceAreaTableName\" /><control name=\"JurisdictionalTableName\" />");
            sXml = sXml.Append("<control name=\"JurisCodDelete\" >");
            sXml = sXml.Append(txtJurisCodDelete.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"JurisCodeAdd\">");
            sXml = sXml.Append(txtJurisCodeAdd.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</WorkCompAgentServiceArea></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetSaveMessageTemplate(string sMethodName)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>WorkCompAgentServiceAreaAdaptor." + sMethodName + @"</Function></Call>");
            sXml = sXml.Append("<Document><WorkCompAgentServiceArea><control name=\"Jurisdictions\">");
            sXml = sXml.Append(hdJurisSelected.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"NoOfficeSelected\" NoOffice=\"\" /> ");
            sXml = sXml.Append("<control name=\"OfficeFunction\" >");
            sXml = sXml.Append(hdOfficeSelected.Text);
            sXml = sXml.Append("</control><control name=\"Agents\" >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FindZip\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"AgentsByZip\" />");
            sXml = sXml.Append("<control name=\"Address1\" /><control name=\"BusinessName\" />");
            sXml = sXml.Append("<control name=\"Address2\" /><control name=\"Contact\" />");
            sXml = sXml.Append("<control name=\"Address3\" /><control name=\"Address4\" />");
            sXml = sXml.Append("<control name=\"City\" /><control name=\"ContactPhone\" />");
            sXml = sXml.Append("<control name=\"State\" /><control name=\"PostCode\" />");
            sXml = sXml.Append("<control name=\"ServiceAreaTableName\" /><control name=\"JurisdictionalTableName\" />");
            sXml = sXml.Append("<control name=\"JurisCodDelete\" >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"JurisCodeAdd\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</WorkCompAgentServiceArea></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void LoadResponse(string sCWSResponse, out string sJurisSel, out string sOfficeSel, out string sAgentsSel)
        {
            XmlNode sJuris, sOffice, sAgents;
            CurrentXDoc.LoadXml(sCWSresponse);
            lstJurisdictions.Items[0].Value = "";
            lstOfficeFunction.Items[0].Value = "";
            lstAgents.Items[0].Value = "";
            sOffice = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='OfficeFunction']/option[@selected='1']/@value");
            sAgents = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='Agents']/option[@selected='1']/@value");
            sJuris = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='Jurisdictions']/option[@selected='1']/@value");
            if (sJuris != null)
                lstJurisdictions.SelectedValue = sJuris.Value;
            else
                lstJurisdictions.SelectedValue = lstJurisdictions.Items[0].Value;
            if (sOffice != null)
                lstOfficeFunction.SelectedValue = sOffice.Value;
            else
                lstOfficeFunction.SelectedValue = lstOfficeFunction.Items[0].Value;
            if (sAgents != null)
                lstAgents.SelectedValue = sAgents.Value;
            else
                lstAgents.SelectedValue = lstAgents.Items[0].Value;
            sJurisSel = lstJurisdictions.SelectedValue;
            sOfficeSel = lstOfficeFunction.SelectedValue;
            sAgentsSel = lstAgents.SelectedValue;

        }

        private void LoadResponseNoAgents(string sCWSResponse, out string sJurisSel, out string sOfficeSel, out string sAgentsSel)
        {
            XmlNode sJuris, sOffice, sAgents;
            CurrentXDoc.LoadXml(sCWSresponse);
            lstJurisdictions.Items[0].Value = "";
            lstOfficeFunction.Items[0].Value = "";
            lstAgents.Items[0].Value = "";
            sOffice = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='OfficeFunction']/option[@selected='1']/@value");
            sAgents = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='Agents']/option[@selected='1']/@value");
            sJuris = CurrentXDoc.SelectSingleNode("//WorkCompAgentServiceArea/control[@name='Jurisdictions']/option[@selected='1']/@value");
            if (sJuris != null)
                lstJurisdictions.SelectedValue = sJuris.Value;
            if (sOffice != null)
                lstOfficeFunction.SelectedValue = sOffice.Value;
            if (sAgents != null)
                lstAgents.SelectedValue = sAgents.Value;
            else
                lstAgents.SelectedValue = lstAgents.Items[0].Value;
            sJurisSel = lstJurisdictions.SelectedValue;
            sOfficeSel = lstOfficeFunction.SelectedValue;
            sAgentsSel = lstAgents.SelectedValue;

        }

        protected void UpdateListItems()
        {
            lstServiceAreaTableName.Items.Clear();
            lstJurisdictionalTableName.Items.Clear();
            txtlstServiceAreaTableName.Text = "";

            char[] splitter = { ';' };
            string svalues = string.Empty;
            string[] lsthndValues = hndlstServiceAreaTableName.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (!lstServiceAreaTableName.Items.Contains(listitem))
                            {
                                lstServiceAreaTableName.Items.Add(listitem);
                                svalues += svalue[0] + " ";

                            }

                            if (lstJurisdictionalTableName.Items.Contains(listitem))
                            {
                                lstJurisdictionalTableName.Items.Remove(listitem);

                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                    }
                }
                if (svalues.Length > 0)
                {
                    txtlstServiceAreaTableName.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndlstServiceAreaTableName.Value = "";
            }


            lsthndValues = hndlstJurisdictionalTableName.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (lstServiceAreaTableName.Items.Contains(listitem))
                            {
                                lstServiceAreaTableName.Items.Remove(listitem);

                            }
                            if (!lstJurisdictionalTableName.Items.Contains(listitem))
                            {
                                lstJurisdictionalTableName.Items.Add(listitem);
                                svalues += svalue[0] + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                    }
                }
                if (svalues.Length > 0)
                {
                    txtlstJurisdictionalTableName.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndlstJurisdictionalTableName.Value = "";


            }

        }
    }
}
