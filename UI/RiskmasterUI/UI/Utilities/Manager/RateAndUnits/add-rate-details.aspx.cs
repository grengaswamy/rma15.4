﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.Shared.Controls;
namespace Riskmaster.UI.UI.Utilities.Manager.RateAndUnits
{
    public partial class add_rate_details : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ((CodeLookUp)transactiontypecode).CodeId = AppHelper.GetQueryStringValue("transtypecode");//MITS 18547
                ((CodeLookUp)transactiontypecode).CodeText = AppHelper.GetQueryStringValue("transtypetext");//MITS 18547
                ((CodeLookUp)unit).CodeId = AppHelper.GetQueryStringValue("unit");
                ((CodeLookUp)unit).CodeText = AppHelper.GetQueryStringValue("unit");//MITS 18547

                ((TextBox)rate).Text = AppHelper.GetQueryStringValue("rate");
                ((TextBox)trackid).Text = AppHelper.GetQueryStringValue("trackid");
                ((TextBox)mode).Text = AppHelper.GetQueryStringValue("mode");
                ((TextBox)rowid).Text = AppHelper.GetQueryStringValue("rowid");
                ((TextBox)user).Text = AppHelper.GetQueryStringValue("user");
                ((TextBox)datetime).Text = AppHelper.GetQueryStringValue("datetime");
            }
        }
    }
}
