﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-rate-table.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.RateAndUnits.add_rate_table" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rate Table</title>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%--<script type="text/javascript" language="javascript" src="../../../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>

    <script type="text/javascript" language="javascript" src="../../../../Scripts/TandE.js"></script>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="javascript">

         function select_option(ctname)
         { 
           if (document.getElementById('CustomerFieldData') != null)
           {
                 var ilevel = parseInt(document.getElementById('CustomerFieldData').value);
                 var leveldesc = getOrgLevelEx(ilevel);
                 return selectCode('orgh', 'customer', leveldesc);
           }
         }
          
         function fnOpenDetailPage(RowId,TrackId,TransTypeCode,TransType,Rate,Unit,Mode,User,DateTime)
         {             
             var sUrl = '/RiskmasterUI/UI/Utilities/Manager/RateAndUnits/add-rate-details.aspx?&mode=' + Mode + '&trackid=' + TrackId + '&rate=' + Rate + '&unit=' + Unit + '&transtypecode=' + TransTypeCode + '&transtypetext=' + TransType + '&rowid=' + RowId + '&user=' + User + '&datetime=' + DateTime;
             var wnd = window.open(sUrl, 'DetailItems', 'width=400,height=300,top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=no,scrollbars=yes');
             return false;
         }
          
         function BackToRateTables() {
             window.location.href = "/RiskmasterUI/UI/Utilities/Manager/RateAndUnits/rate-tables.aspx";
             return false;
         }
     </script>
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
</head>

<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <div id="maindiv" style="height: 100%; overflow: auto">
        <table align="center">
            <%--<tr>
                <td align="center" colspan="2">--%>
                    <tr>
                        <td colspan="2">
                            <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b><u>Rate Table Name</u></b>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="RateTableName" runat="server" TabIndex="1" RMXRef="/Instance/Document/form/group/control[@name='rtname']/@value" onchange="setDataChanged(true);"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b><u>Customer</u></b>
                        </td>
                        <td align="left">
                            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                                ID="customer" RMXRef="/Instance/Document/form/group/control[@name='customer']/@value"
                                RMXType="orgh" name="customer" cancelledvalue="" TabIndex="2" />
                            <asp:Button runat="server" class="CodeLookupControl" Text="" ID="customerbtn" TabIndex="3" OnClientClick="return select_option('customer');"/>
                            <asp:TextBox Style="display: none" runat="server" ID="customer_cid" RMXRef="/Instance/Document/form/group/control[@name='customer']/@id" cancelledvalue="" />
                            <asp:TextBox Style="display: none" runat="server" ID="CustomerFieldData" RMXRef="/Instance/Document/form/group/control[@name='customer']/@mid" cancelledvalue="" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b><u>Effective Date</u></b>
                        </td>
                        <td align="left">
                            <span class="formw">
                                <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document/form/group/control[@name='efdate']/@value"
                                    RMXType="date" TabIndex="4" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                            <%--vkumar258 - RMA-6037 - Starts --%>
                            <%-- <asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" TabIndex="5" />

                                <script type="text/javascript" language="javascript">
                                    Zapatec.Calendar.setup(
					                {
					                    inputField: "EffectiveDate",
					                    ifFormat: "%m/%d/%Y",
					                    button: "EffectiveDatebtn"
					                }
					                );
                                </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#EffectiveDate").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                        //buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
                                });
                            </script>
                            <%--vkumar258 - RMA_6037- End--%>

                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <b><u>Expiration Date</u></b>
                        </td>
                        <td align="left">
                            <span class="formw">
                                <asp:TextBox runat="server" FormatAs="date" ID="ExpirationDate" RMXRef="/Instance/Document/form/group/control[@name='exdate']/@value"
                                    RMXType="date" TabIndex="6" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                            <%--vkumar258 - RMA-6037 - Starts --%>
                            <%--<asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" TabIndex="7" />

                                <script type="text/javascript" language="javascript">
                                    Zapatec.Calendar.setup(
					                {
					                    inputField: "ExpirationDate",
					                    ifFormat: "%m/%d/%Y",
					                    button: "ExpirationDatebtn"
					                }
					                );
                                </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#ExpirationDate").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                        //buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "7");
                                });
                            </script>
                            <%--vkumar258 - RMA_6037- End--%>

                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><u>Rate Details</u></b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        <div id="divForms" style="height:215px;overflow: auto"  runat="server" >
                            <asp:GridView ID="GridDisplayTable" runat="server" AutoGenerateColumns="false" CssClass=""
                                Width="100%" OnRowDataBound="GridDisplayTable_RowDataBound" Visible="true"
                                ShowHeader="true" GridLines="Vertical" >
                                <HeaderStyle CssClass="colheader3" HorizontalAlign="Left" />
                                <RowStyle CssClass="datatd" HorizontalAlign="Left" Font-Bold="false" />
                                <AlternatingRowStyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />
                                <Columns>
                                  <asp:TemplateField HeaderStyle-CssClass="hiderowcol" ItemStyle-CssClass="hiderowcol">
                                    <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="RowId" Value='<%# Eval("rowid")%>'/>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                 <asp:TemplateField HeaderText="" HeaderStyle-CssClass="hiderowcol" ItemStyle-CssClass="hiderowcol">
                                    <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="TrackId" Value='<%# Eval("trackid")%>'/>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transaction Type">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="TransactionType" PostBackUrl="" Text='<%# Eval("transtype")%>' value='<%# Eval("transtypecode")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Unit">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Unit" Text='<%# Eval("unit")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rate" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="Rate" Text='<%# Eval("rate")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="" HeaderStyle-CssClass="hiderowcol" ItemStyle-CssClass="hiderowcol">
                                    <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="Status" Value='<%# Eval("status")%>'/>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="hiderowcol" ItemStyle-CssClass="hiderowcol" >
                                    <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="User" Value='<%# Eval("user")%>'/>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="hiderowcol" ItemStyle-CssClass="hiderowcol">
                                    <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="DateTime" Value='<%# Eval("datetime")%>'/>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Delete" runat="server" Text="Delete" ImageUrl="../../../../Images/tb_delete_active.png" ImageAlign="Middle" CommandArgument='<%# Eval("rowid")%>' CommandName="delete" OnCommand="Delete_ItemCommand"  />
                                        </ItemTemplate>                                       
                                   </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </div> 
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <a style="cursor:hand;" onclick="return fnOpenDetailPage('','0','','','','','new','','')"><u>Add Detail</u></a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button class="button" ID="btnSave" Text=" Save " runat="server"  OnClientClick="return saveRatetable();"/>
                        </td>
                        <td align="center">
                            <asp:Button class="button" ID="btnBack" Text=" Back " runat="server" OnClientClick="return BackToRateTables();" />
                        </td>
                    </tr>
                <%--</td>
            </tr>--%>
        </table>
    </div>
    
    <asp:TextBox Style="display: none" runat="server" ID="ratetableid" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="h_transtype" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="h_transtypecode" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="h_unit" rmxtype="hidden" />    
    <asp:TextBox Style="display: none" runat="server" ID="h_rate" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="h_trackid" rmxtype="hidden" />
     <asp:TextBox Style="display: none" runat="server" ID="h_rowid" rmxtype="hidden" />
     <asp:TextBox Style="display: none" runat="server" ID="h_user" rmxtype="hidden" />
     <asp:TextBox Style="display: none" runat="server" ID="h_datetime" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" rmxtype="hidden" />
     <asp:TextBox Style="display: none" runat="server" ID="SelectedRowId" rmxtype="hidden" />
      <asp:TextBox Style="display: none" runat="server" ID="req_fields" rmxtype="hidden" Text="RateTableName|customer|EffectiveDate|ExpirationDate"/>
      <asp:TextBox Style="display: none" runat="server" ID="hidden_DataChanged"  rmxtype="hidden" />      
    </form>
</body>
</html>
