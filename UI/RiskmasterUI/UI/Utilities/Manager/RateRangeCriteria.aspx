﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RateRangeCriteria.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.RateRangeCriteria" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
    <%@ Register TagPrefix="uc4" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
    <%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
    <%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <style type="text/css">
        @import url(csc-Theme/riskmaster/common/style/dhtml-div.css);
    </style>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
 
 <uc1:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" runat="server" method="post">
     <table>
        <tr>
            <td colspan="2">
                <uc4:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" /><div class="msgheader"
        id="formtitle">
    </div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Range Criteria
                                </td>
                            </tr>
                            <tr>
                                <td width="400px">
                                    <b></b>
                                    <table width="100%">
                                        <tr>
                                            <td width="80%">
                                                <div class="completerow">
                                                    <dg:UserControlDataGrid runat="server" ID="RateRangeCriteriaGrid" GridName="RateRangeCriteriaGrid"
                                                        GridTitle="" Target="/raterangelist" Ref="/Instance/Document/form//control[@name='RateRangeCriteriaGrid']"
                                                        Unique_Id="raterangerowid" TextColumn="Extra" RowDataParam="raterange" ShowRadioButton="True"
                                                        Width="99%" Height="30%" HideNodes="|addedbyuser|dttmrcdadded|updatedbyuser|dttmrcdlastupd|raterangerowid|Extra"
                                                        ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="340" HideButtons=""
                                                        Type="GridAndButtons" />
                                                    <asp:TextBox Style="display: none" runat="server" ID="RateRangeCriteriaGridSelectedId"
                                                        RMXType="id"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="RateRangeCriteriaGrid_RowDeletedFlag"
                                                        RMXType="id" Text="false"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="RateRangeCriteriaGrid_Action"
                                                        RMXType="id"></asp:TextBox>
                                                    <asp:TextBox Style="display: none" runat="server" ID="RateRangeCriteriaGrid_RowAddedFlag"
                                                        RMXType="id" Text="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
                    <asp:TextBox Style="display: none" runat="server" ID="HdnActionSave" Text="" />
                    <asp:TextBox Style="display: none" runat="server" ID="HdnSelectedIdForDeletion" Text="" />
                    <asp:TextBox Style="display: none" runat="server" ID="HdnListNameForDeletion" Text="" />
                    <input type="text" name="" value="" id="SysViewType" style="display: none" />
                    <input type="text" name="" value="" id="SysCmd" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                    <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                    <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                    <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                    <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="DiscountParms" />
                    <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                    <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="PremiumRangeCriteria" />
                    <input type="text" name="" value="" id="SysRequired" style="display: none" />
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
