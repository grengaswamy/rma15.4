﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;


///////////////////////////////
// Developed By: abansal23
// Completed On: 6th Feb, 2009
///////////////////////////////
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class SelectedDisc : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = string.Empty  ;
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

                    // Added By Amit Bansal
                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("SelectedDiscAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control CtlPostBack = DatabindingHelper.GetPostBackControl(this.Page);

                    if (CtlPostBack == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("SelectedDiscAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Creates Message Template for input xml
        /// </summary>
        /// <returns>Message Element</returns>
        private XElement GetMessageTemplate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SelectedDiscAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><SelectedDisc>");
            sXml = sXml.Append("<control name='RowId' type='id'>" + RowId.Text + "</control>");
            sXml = sXml.Append("<control name='UseDiscTier' type='checkbox'>" + UseDiscTier.Checked + "</control>");
            sXml = sXml.Append("<control name='Override' type='checkbox'>" + Override.Checked + "</control>");
            sXml = sXml.Append("<control name='UseVolumeDiscount' type='hidden'>" + UseVolumeDiscount.Text + "</control>");
            sXml = sXml.Append("<control name='LOBcode' type='code' codetable='POLICY_LOB' codeid='" + LOBcode.CodeIdValue + "'>" + LOBcode.CodeTextValue + "</control>");
            sXml = sXml.Append("<control name='Statecode' type='code' codetable='states' codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</control>");

            // npadhy Start MITS 21557 The Values from the Readonly control gets removed after postback
            // Since the Control is readonly, we will retrieve the Value from the Request.Form
            sXml = sXml.Append("<control name='DiscountName' codeid='" + DiscountName_cid.Text + "'>" + Request.Form["DiscountName"] + "</control>");
            sXml = sXml.Append("<control name='ParentLevel' codeid='" + ParentLevel_cid.Text + "'>" + Request.Form["ParentLevel"] + "</control>");
            // npadhy End MITS 21557 The Values from the Readonly control gets removed after postback

            //VACo Changes Starts
            sXml = sXml.Append("<control name='ISRateSetup' type='id'>0</control>");
            //VACo Changes Ends
            sXml = sXml.Append("<control name='FormTitle' type='text' />"); 
            sXml = sXml.Append("<control name='AddedByUser'/><control name='DttmRcdAdded'/><control name='DttmRcdLastUpd'/><control name='UpdatedByUser'/>");

            sXml = sXml.Append("</SelectedDisc></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                // npadhy Start MITS 21557 The Values from the Readonly control gets removed after postback
                CallCWS("SelectedDiscAdaptor.Save", XmlTemplate, out sCWSresponse, false, true);
                // npadhy End MITS 21557 The Values from the Readonly control gets removed after postback
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
