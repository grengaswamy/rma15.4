﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TPACodeMapping.aspx.cs"
    ValidateRequest="false" Inherits="Riskmaster.UI.Utilities.Manager.TPACodeMapping" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TPA Code Mapping</title>
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js"></script>
    <script type="text/JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/cul.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            var currentTableId = '';
            $('tbody input[type=checkbox]').change(function () {
                currentTableId = '#' + $(this).closest('table').attr('id');
                if ($(this).closest('tr').hasClass('colheader3')) {
                    if ($(this).is(':checked')) {
                        SelectAll(currentTableId);
                    }
                    else {
                        UnSelectAll(currentTableId);
                    }
                }
                else {
                    if ($(this).is(':checked')) {
                        SelectHeader(currentTableId);
                    }
                    else {
                        UnSelectHeader(currentTableId);
                    }

                }
                return true;
            });
        });

        function SelectHeader(currentTableId) {
            if ($(currentTableId + ' tbody input:checked').length === ($(currentTableId + ' tbody input[type=checkbox]').length - 1)) {
                $(currentTableId + ' th input[type=checkbox]').prop('checked', true);
            }
        }

        function UnSelectHeader(currentTableId) {
            $(currentTableId + ' th input[type=checkbox]').prop('checked', false);
        }

        function SelectAll(currentTableId)
        {
            $(currentTableId + ' tbody input[type=checkbox]').prop('checked', true);
        }

        function UnSelectAll(currentTableId) {
            $(currentTableId + ' tbody input[type=checkbox]').prop('checked', false);
        }

        function ValidateMappingAdd() 
        {

            var obj = document.getElementById("lstCodeType");
            if (obj.options.length == 0) 
            {
                alert(TPACodeMappingValidations.ValidCodetype);
                //alert("Code Type can not be empty.");
                return false;
            }


            obj = document.getElementById("lstRMCodes");
            if (obj.options.length == 0) 
            {
                alert(TPACodeMappingValidations.ValidRiskmasterCodes);
                //alert("Riskmaster Codes are not defined for selected code type.");
                return false;
            }

            obj = document.getElementById("lstTPASysCodes");
            if (obj.options.length == 0) 
            {
                alert(TPACodeMappingValidations.ValidTPAInterfaceCodes);
                //alert("TPA Interface Codes are not defined for selected code type.");
                return false;
            }

        }

        function DeleteCode() {

            var selectedVal = "";
            if (SelectionValid()) {

                $('input:checked').each(function () {
                    if (!$(this).closest('tr').hasClass('ctrlgroup')) {
                        if (selectedVal == "") {
                            selectedVal = $(this).val();
                        }
                        else {
                            selectedVal = selectedVal + "," + $(this).val();
                        }
                    }
                });

                //for (var i = 0; i < document.getElementById('frmData').elements.length; i++) {
                //    if (document.getElementById('frmData').elements[i].type == 'checkbox') {
                //        if (document.getElementById('frmData').elements[i].checked == true) {
                //            if (selectedVal == "") {
                //                selectedVal = document.getElementById('frmData').elements[i].value;
                //            }
                //            else {
                //                selectedVal = selectedVal + "," + document.getElementById('frmData').elements[i].value;
                //            }
                //        }
                //    }
                //}
                document.getElementById('hdnSelected').value = selectedVal;
                return true;
            }
            else {
                alert(TPACodeMappingValidations.RemoveTPACodes);
                //alert("Please select checkbox to remove TPA code");
                return false;
            }
        }
        function SelectionValid() {
            if ($('input:checked').length > 0) {
                return true;
            }
            //for (var i = 0; i < document.getElementById('frmData').elements.length; i++)
            //    if (document.getElementById('frmData').elements[i].type == 'checkbox')
            //        if (document.getElementById('frmData').elements[i].checked == true)
            //            return true;

            return false;
        }

        function onPageLoaded() {
            if (ie) {
                if ((eval("document.all.divForms") != null) && (ieversion >= 6)) {
                    eval("document.all.divForms").style.height = 300;
                }
            }
            else {
                var o_divforms;
                o_divforms = document.getElementById("divForms");
                if (o_divforms != null) {
                    o_divforms.style.height = window.frames.innerHeight * 0.70;
                    o_divforms.style.width = window.frames.innerWidth * 0.995;
                }
            }

            loadTabList();
            if (document.getElementById('hTabName') != null && document.getElementById('hTabName').value != '') {
                tabChange(document.getElementById('hTabName').value);
            }
        }

        
        

        
    </script>
    <script type="text/javascript">
        

        function ValidateAdd() {
            //if (document.getElementById('txtProcessSystemName').value == "") {

            //    //debugger;
            //    alert(TPACodeMappingValidations.DCISystemNameExist);
            //    return false;
            //}
            if (document.getElementById('txtProcessName').value == "") {
                alert(TPACodeMappingValidations.InterfaceNameExist);
                return false;
            }

        }

        function DeleteInterfaceCode() {
            var selectedVal = "";
            if (SelectionValid()) {
                ret = confirm(TPACodeMappingValidations.RemoveRecord);
                if (ret == true) {
                    $('input:checked').each(function () {
                        if (!$(this).closest('tr').hasClass('ctrlgroup')) {
                            if (selectedVal == "") {
                                selectedVal = $(this).val();
                            }
                            else {
                                selectedVal = selectedVal + "," + $(this).val();
                            }
                        }
                    });

                    //for (var i = 0; i < document.getElementById('frmData').elements.length; i++)
                    //{
                    //    if (document.getElementById(i).type == 'checkbox') {
                    //        if (document.getElementById('frmData').elements(i).checked == true) {
                    //            if (selectedVal == "") {
                    //                selectedVal = document.getElementById('frmData').elements(i).value;
                    //            }
                    //            else {
                    //                selectedVal = selectedVal + "," + document.getElementById('frmData').elements(i).value;
                    //            }
                    //        }
                    //    }
                    //}
                    document.getElementById('hdnSelected').value = selectedVal;
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert(TPACodeMappingValidations.InterfaceNameRemove);
                return false;
            }
        }

        function SelectionValid() {
            if ($('input:checked').length > 0) {
                return true;
            }

            //for (var i = 0; i < document.getElementById('frmData').elements.length; i++)
            //    if (document.getElementById('frmData').elements(i).type == 'checkbox')
            //        if (document.getElementById('frmData').elements(i).checked == true)
            //            return true;

            return false;
        }

        function SelectedAvailableCode() {
            var selectedVal = "";
            for (var i = 0; i < document.getElementById('frmData').elements.length; i++) {
                if (document.getElementById('frmData').elements(i).type == 'checkbox') {
                    if (document.getElementById('frmData').elements(i).checked == true) {
                        selectedVal = "true";
                    }
                    else {
                        if (document.getElementById('frmData').elements(i).checked == false) {
                            selectedVal = "false";
                        }
                        break;
                    }
                }
            }

            if (selectedVal == "false") {
                if (document.getElementById('GridViewAvailableProcess_ctl01_cbHdrSelectAll').checked == true) {
                    document.getElementById('GridViewAvailableProcess_ctl01_cbHdrSelectAll').checked = false;
                }
            }
        }

        function openWindow(ctrlId) {
            var processID = $('#' + ctrlId).closest('tr').find('input[type=checkbox]').val();
            //alert($('#' + ctrlId).closest('tr').find('input[type=checkbox]').val());
            window.open('/RiskmasterUI/UI/Utilities/Manager/ProcessCodeMapping.aspx?&processID=' + processID, 'open_newWindow',
                'width=500,height=250' + ',top=' + (screen.availHeight - 250) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

        }

      </script>
</head>
<body onload="javascript:onPageLoaded();parent.MDIScreenLoaded()">
    <form id="frmData" name="frmData" method="post" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:HiddenField ID="hTabName" runat="server"/>
        <%--<input type="hidden" id="hTabName" name="hTabName" />--%>
        <div class="tabGroup" id="TabsDivGroup" runat="server">
            <div class="Selected" nowrap="true" runat="server" name="TABSAddThirdPartyInterface" id="TABSAddThirdPartyInterface">
                <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="AddThirdPartyInterface" id="LINKTABSAddThirdPartyInterface">
                    <asp:Label ID="lblAddThirdPartyInterface" runat="server" Text="<%$ Resources:lblAddThirdPartyInterface %>"></asp:Label>     
                </a>
            </div>
            <div class="tabSpace" runat="server" id="TBSAddThirdPartyInterface">
                <nbsp />
                <nbsp />
            </div>
            <div class="NotSelected" nowrap="true" runat="server" name="TABSThirdPartyCodeMapping" id="TABSThirdPartyCodeMapping">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                    rmxref="" name="ThirdPartyCodeMapping" id="LINKTABSThirdPartyCodeMapping">
                    <asp:Label ID="lblrmAPartyCode" runat="server" Text="<%$ Resources:lblrmAPartyCode %>"></asp:Label>                    
                </a>
            </div>
            <div class="tabSpace" runat="server" id="TBSThirdPartyCodeMapping">
                <nbsp />
                <nbsp />
            </div>   
        </div>

    <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 100%; height: 83%; overflow: auto;">
       <table border="0" cellspacing="0" cellpadding="0" name="FORMTABAddThirdPartyInterface" id="FORMTABAddThirdPartyInterface" width="100%">
           <tr>
               <td>
                    <table width="100%" cellspacing="0" cellpadding="0">
		
		<tr class="msgheader">
		<!-- Label control added by Shivendu for MITS 16148 -->
		    <td class="ctrlgroup" colspan="2" width="100%"><asp:Label ID="lblTPApage" runat="server" Text="Label"></asp:Label></td>
		</tr>
	</table>
                  
	<br/>
    <table width="100%" cellspacing="0" cellpadding="0">
		<%--<tr>
            <td>
             <asp:Label ID="lblProcessSystemName" runat="server" Text="<%$ Resources:lblProcessSystemNamedr %>"></asp:Label>   
            </td>
            <td>
                <asp:TextBox ID="txtProcessSystemName" type="text"  runat="server" />
            </td>
        </tr>--%>

        <tr>
            <td colspan="2">
            <table width="100%">
                <tr>
            <td width="30%">
            <asp:Label ID="lblProcessName" runat="server" Text="<%$ Resources:lblThrdPartyInterfaceNamedr %>"></asp:Label>  
            </td>
            <td align="left">              
                <asp:TextBox ID="txtProcessName" type="text"  runat="server" />  
            </td>
            <td></td>
                    </tr>
               </table>
                </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
         
        <tr>
            <td align="right">
                <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                <asp:Button runat="server" Text="Add Third Party Interface " class="button" 
                    ID="btnadd" OnClick="addTPABtn_Click" OnClientClick="return ValidateAdd();" />
            </td>
            <td align="left">
                &nbsp;
               <%-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                <asp:Button runat="server" Text="Remove Third Party Interface" class="button" ID="btnremove" 
                OnClick="removeTPBtn_Click" OnClientClick="return DeleteInterfaceCode();" />
            </td>             
        </tr>
        <tr><td>&nbsp;</td></tr>
        
        <tr>
            <td colspan="2">
                <table width="100%" style="margin-right: 0px; height: 191px;">
                    <tr>
                        <td width="80%">
                            <div style="width: 98%; height: 300px; overflow: auto">
                                <asp:GridView ID="GridViewAvailableProcess" runat="server" AutoGenerateColumns="False"
                                    Font-Names="Tahoma" Font-Size="Smaller" CssClass="singleborder" Width="98%" Height=""
                                    CellPadding="4" ForeColor="#333333" GridLines="Both" HorizontalAlign="Left">
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <Columns>
                                         <asp:TemplateField>
                                            <HeaderTemplate>
                                                <%--taleeb - changed to jquery--%>
                                                <%--<input id="cbHdrSelectAll" name="cbHdrSelectAll" runat="server" type="checkbox" onclick="javascript:SelectAll(this)" value='-1' />--%>
                                                <input id="Checkbox1" name="cbHdrSelectAll" runat="server" type="checkbox"  value='-1'  />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                  <%--taleeb - changed to jquery--%>
                                                 <%--<input id="cbTPACodeMappingGrid" name="TPACodeMappingGrid" runat="server" type="checkbox"
                                                    value='<%# Eval("CBId")%>' onclick= "return SelectedCode();" />--%>
                                                <input id="cbAvailableTPAGrid" name="AvailableProcessMappingGrid" runat="server" type="checkbox"
                                                    value='<%# Eval("CBId")%>'  />
                                               
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="colheader5" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CBId" HeaderText="RowId" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Third Party Interface Name" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="msgheader" >
                                            <ItemTemplate>
                                                <%# Eval("ProcessName")%>
                                            </ItemTemplate> 
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                                            <ItemTemplate>
                                                <input type="button" id="btnAddProcessMapping" class="button" value="Table Mapping" onclick="return openWindow(this.id);"
                                                runat="server" />
                                            </ItemTemplate> 
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="colheader3" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBox1" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>    
    </table> 

               </td>
           </tr>
      </table> 

      <table width="100%" border="0" cellspacing="3" cellpadding="3" style="display: none;" name="FORMTABThirdPartyCodeMapping" id="FORMTABThirdPartyCodeMapping">
        <!--added by swati for Process Type-->
        <tr>
            <td class="ctrlgroup" colspan="4" width="100%">
                <asp:Label ID="lblthirdPartyCodeMapping" runat="server" Text="<%$ Resources:lblthirdPartyCodeMapping %>"></asp:Label>                   
            </td>
        </tr>
         <tr>
            <td class="style1">
                <asp:TextBox runat="server" Style="display: none" rmxignorevalue="true"
                    ID="hdnSelectedProcess"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblThirdPartyInterfaceName" runat="server" Text="<%$ Resources:lblThirdPartyInterfaceName %>"></asp:Label> 
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstProcessNames" type="combobox"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProcessNames_SelectedIndexChanged"></asp:DropDownList>
                <asp:TextBox runat="server" ID="txtProcessImportId" Style="display: none" rmxref="/Instance/Document/TPACodeMapping/SelectedProcessSystem"></asp:TextBox>
            </td>
        </tr>
        <!--addition end by swati-->
       
      <tr>
            <td class="ctrlgroup" colspan="4" width="100%">
                <asp:Label ID="lblthirdPartyCodeMapping1" runat="server" Text="<%$ Resources:lblthirdPartyCodeMapping %>"></asp:Label>   
            </td>
        </tr>
         <tr>
            <td class="style1">
                <asp:TextBox runat="server" Style="display: none" rmxignorevalue="true"
                    ID="hdnSelected"></asp:TextBox>
            </td>
        </tr>
        <!--if cond added by swati-->
        <%if (string.Compare(sProcessType, "TPA", true) == 0) { %>
        <tr>
            <td>
                <asp:Label ID="lblTPAName" runat="server" Text="<%$ Resources:lblTPAName %>"></asp:Label>  
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstTPAImportNames" type="combobox"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstTPAImportNames_SelectedIndexChanged"></asp:DropDownList>
                <asp:TextBox runat="server" ID="txtTPAImportId" Style="display: none" rmxref="/Instance/Document/TPACodeMapping/SelectedTPASystem"></asp:TextBox>
            </td>
        </tr>
        <% } %>
        <tr>
            <td>
                <asp:Label ID="lblSelectCodeTable" runat="server" Text="<%$ Resources:lblSelectCodeTable %>"></asp:Label> 
            </td>
            <td width="30%">
                <asp:DropDownList ID="lstCodeType" type="combobox" rmxref="/Instance/Document/TPACodeMapping/CodeTypes"
                    ItemSetRef="/Instance/Document/TPACodeMapping/CodeTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstCodeType_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtCodeTypeId" Style="display: none" rmxref="/Instance/Document/TPACodeMapping/SelectedCodeType"></asp:TextBox>
            </td>
        </tr>
        
         <tr>
            <td>
                <asp:Label ID="lblTPASysCode" runat="server" Text="<%$ Resources:lblThirdPartySysCodedr %>"></asp:Label>
               <%-- <asp:Label runat="server" ID="txtTPASysCode" Text="TPA Interface Codes"></asp:Label>--%>
            </td>
            <td>
                 <asp:DropDownList ID="lstTPASysCodes" type="combobox" rmxref="/Instance/Document/TPACodeMapping/TPASysCodes"
                    ItemSetRef="/Instance/Document/TPACodeMapping/TPASysCodes" runat="server"
                    AutoPostBack="false">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtTPASysCodeId" Style="display: none" rmxref="/Instance/Document/TPACodeMapping/TPAMapValues/TPACode[@value='TPACode']/@TPASysSelectedCodeId"></asp:TextBox>
            </td>
        </tr>

         <tr>
            <td>
                <asp:Label ID="lblRMCode" runat="server" Text="<%$ Resources:lblRiskmasterCodedr %>"></asp:Label>
                <%--<asp:Label ID="txtRMCode" Text="Riskmaster Codes" runat="server"></asp:Label>--%>
            </td>
            <td>
                <asp:DropDownList ID="lstRMCodes" type="combobox" rmxref="/Instance/Document/TPACodeMapping/RMCodes"
                    ItemSetRef="/Instance/Document/TPACodeMapping/RMCodes" runat="server" AutoPostBack="false">
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtRMCodeId" Style="display: none" rmxref="/Instance/Document/TPACodeMapping/TPAMapValues/TPACode[@value='TPACode']/@RMSelectedCodeId"></asp:TextBox>
            </td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
         <tr><td colspan="4"><asp:Label ID="lblThirdPartyDisclaimer" runat="server" Text="<%$ Resources:lblThirdPartyDisclaimer %>"></asp:Label></td></tr>
        <tr></tr>
        <tr>
            <%--<td colspan="4" align="center">--%>
            <%--<table><tr>--%>
            <td align="right" width="30%">
                <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                <asp:Button runat="server" Text="Add Code" class="button" 
                    ID="addTPAbtn" OnClick="addTPA_btn_Click" OnClientClick="return ValidateMappingAdd();" />
            </td>
            <%-- <td colspan="2"></td> --%> 
            <td  align="center">
                <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                <asp:Button runat="server" Text="Remove Code" class="button" ID="removeTPAbtn" 
                OnClick="removeTPA_btn_Click" OnClientClick="return DeleteCode();" />
            </td>
            <td></td>
            <%--</tr></table>
                </td>--%>
        </tr>

        <tr>
            <td colspan="4">
                <table width="100%" style="margin-right: 0px; height: 191px;">
                    <tr>
                        <td width="80%">
                            <div style="width: 98%; height: 300px; overflow: auto">
                                <asp:GridView ID="gvTPACodeMappingGrid" runat="server" AutoGenerateColumns="False"
                                    Font-Names="Tahoma" Font-Size="Smaller" CssClass="singleborder" Width="98%" Height=""
                                    CellPadding="4" ForeColor="#333333" GridLines="Both" HorizontalAlign="Left">
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <Columns>
                                         <asp:TemplateField>
                                            <HeaderTemplate>
                                                <%--taleeb - changed to jquery--%>
                                                <%--<input id="cbHdrSelectAll" name="cbHdrSelectAll" runat="server" type="checkbox" onclick="javascript:SelectAll(this)" value='-1' />--%>
                                                <input id="cbHdrSelectAll" name="cbHdrSelectAll" runat="server" type="checkbox" value='-1' />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                  <%--taleeb - changed to jquery--%>
                                                 <%--<input id="cbTPACodeMappingGrid" name="TPACodeMappingGrid" runat="server" type="checkbox"
                                                    value='<%# Eval("CBId")%>' onclick= "return SelectedCode();" />--%>
                                                <input id="cbTPACodeMappingGrid" name="TPACodeMappingGrid" runat="server" type="checkbox"
                                                    value='<%# Eval("CBId")%>' />
                                               
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="colheader5" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CBId" HeaderText="RowId" Visible="false" ItemStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="tpasyscode" HeaderText="Third Party Codes" HeaderStyle-CssClass="colheader5" />
                                        <asp:BoundField DataField="rmcode" HeaderText="Riskmaster Codes" HeaderStyle-CssClass="colheader5" />
                                    </Columns>
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="colheader3" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="hdnCbId" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>

      
    </div>
    <asp:TextBox Style="display: none" runat="server" ID="SysWindowId"></asp:TextBox>
    <input id="Hidden1" type="hidden" runat="server" value="" />
 <input id="hdValidateKey" type="hidden" runat="server" value="" />
    </form>
</body>
</html>
