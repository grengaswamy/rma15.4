﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Data.SqlClient;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using System.IO;
using System.ServiceModel;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class TPACodeMapping : NonFDMBasePageCWS
    {
        public string sProcessType = string.Empty;
        public string sTPAType = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("TPACodeMapping.aspx"), "TPACodeMappingValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "TPACodeMappingValidations", sValidationResources, true);
                string sSelCodeType = string.Empty;
                if (!IsPostBack)
                {
                    //added by swati
                    GetAvailableProcessSystem();
                    if (lstProcessNames.SelectedItem != null)
                    {
                        sProcessType = lstProcessNames.SelectedItem.Text;
                    }
                    //if condn added by swati
                    if (string.Compare(sProcessType, "TPA", true) == 0)
                    {
                        GetTPASystem();
                    }
                    GetCodeTypes();
                    GetCodeDetails();
                    sSelCodeType = lstCodeType.SelectedValue;

                    lblTPApage.Text = "Available Third Party Interface(s)";
                    BindGridData();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void GetTPASystem()
        {
            try
            {
                XElement XmlTemplate = null;
                string Data = "";
                DataSet oDS =null;
                oDS = new DataSet();
                XmlTemplate = GetMessageTemplateForTPATypes();
                bool bReturnStatus = false;
                string sSelTPAType = string.Empty;
                sSelTPAType = lstTPAImportNames.SelectedValue;
                txtTPAImportId.Text = sSelTPAType;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetTPASystem", XmlTemplate, out Data, false, true);
                if (Data != null && !string.IsNullOrEmpty(Data))
                {
                    oDS.ReadXml(new StringReader(Data));
                    ListItem item = null;
                    if (oDS != null)
                    {
                        if (oDS.Tables["option"] != null)			//Ankit Start : Worked on MITS - 34657
                        {			//Ankit Start : Worked on MITS - 34657
                            for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                            {
                                item = new ListItem();
                                item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                                item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();
                                lstTPAImportNames.Items.Add(item);		
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void GetCodeTypes()
        {
            try
            {
                XElement XmlTemplate = null;
                string Data = "";
                XmlTemplate = GetMessageTemplateForCodeTypes();
                bool bReturnStatus = false;
                string sSelCodeType = string.Empty;
                sSelCodeType = lstCodeType.SelectedValue;
                txtCodeTypeId.Text = sSelCodeType;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.Get", XmlTemplate, out Data, false, true);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err); 
            }
        }

        private void GetCodeDetails()
        {
            try
            {
                lstRMCodes.Items.Clear();
                lstTPASysCodes.Items.Clear();
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForCodeDetails();
                string Data = "";
                bool bReturnStatus = false;
                string sSelCodeType = string.Empty;
                lstRMCodes.Visible = true;
                //txtCodeTypeId.Text = lstCodeType.SelectedValue;
                sSelCodeType = lstCodeType.SelectedValue;
                txtCodeTypeId.Text = sSelCodeType;

                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetCodeDetails", XmlTemplate, out Data, false, true);

                if (bReturnStatus)
                {
                    BindMappingGridData(Data);
                }


            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void addTPA_btn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateForSave();
                string Data = "";
                bool bReturnStatus = false;
                txtRMCodeId.Text = lstRMCodes.SelectedValue.ToString();
                txtTPASysCodeId.Text = lstTPASysCodes.SelectedValue.ToString();
                txtCodeTypeId.Text = lstCodeType.SelectedValue.ToString();
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.SaveTPACodeInfo", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }

                    else
                    {
                        XmlTemplate = GetMessageTemplateForCodeDetails();
                        while (lstRMCodes.Items.Count > 0)
                            lstRMCodes.Items.RemoveAt(0);
                        while (lstTPASysCodes.Items.Count > 0)
                            lstTPASysCodes.Items.RemoveAt(0);
                        bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetCodeDetails", XmlTemplate, out Data, false, true);

                        BindMappingGridData(Data);
                    }
                }
                //if condn added by swati
                sProcessType = lstProcessNames.SelectedItem.Text;
                sTPAType = lstTPAImportNames.SelectedValue;
                lstTPAImportNames.Items.Clear();
                if (string.Compare(sProcessType, "TPA", true) == 0)
                {
                    GetTPASystem();
                    lstTPAImportNames.SelectedValue = sTPAType;
                }
                //else
                //{
                //    lstTPAImportNames.Items.Clear();
                //}
                //change end here by swati
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void removeTPA_btn_Click(object sender, EventArgs e)
        {
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForMappingDeletion();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.DeleteTPACodeMapping", XmlTemplate, out Data, true, false);
                XmlTemplate = GetMessageTemplateForCodeDetails();
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetCodeDetails", XmlTemplate, out Data, false, true);

                if (bReturnStatus)
                {
                    BindMappingGridData(Data);

                }
                //if condn added by swati
                sProcessType = lstProcessNames.SelectedItem.Text;
                sTPAType = lstTPAImportNames.SelectedValue;
                lstTPAImportNames.Items.Clear();
                if (string.Compare(sProcessType, "TPA", true) == 0)
                {
                    GetTPASystem();
                    lstTPAImportNames.SelectedValue = sTPAType;
                }
                //else
                //{
                //    lstTPAImportNames.Items.Clear();
                //}
                //change end here by swati
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

      

        protected void lstCodeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if condn added by swati
            sProcessType = lstProcessNames.SelectedItem.Text;
            sTPAType = lstTPAImportNames.SelectedValue;
            lstTPAImportNames.Items.Clear();
            if (string.Compare(sProcessType, "TPA", true) == 0)
            {
                GetTPASystem();
                lstTPAImportNames.SelectedValue = sTPAType;
            }
            //else
            //{
            //    lstTPAImportNames.Items.Clear();
            //}
            //change end here by swati

            GetCodeDetails();
            string sSelCodeType = string.Empty;            
            sSelCodeType = lstCodeType.SelectedValue;
        }

        protected void lstTPAImportNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if condn added by swati
            sProcessType = lstProcessNames.SelectedItem.Text;
            sTPAType = lstTPAImportNames.SelectedValue;
            lstTPAImportNames.Items.Clear();
            if (string.Compare(sProcessType, "TPA", true) == 0)
            {
                GetTPASystem();
                lstTPAImportNames.SelectedValue = sTPAType;
            }
            //else
            //{
            //    lstTPAImportNames.Items.Clear();
            //}
            //change end here by swati
            GetCodeTypes();
            GetCodeDetails();
           
        }

        //added by swati
        private void GetAvailableProcessSystem()
        {
            try
            {
                XElement XmlTemplate = null;
                string Data = "";
                DataSet oDS = null;
                oDS = new DataSet();
                XmlTemplate = GetMessageTemplateForProcessTypes();
                bool bReturnStatus = false;
                string sSelProcessType = string.Empty;
                sSelProcessType = lstProcessNames.SelectedValue;
                txtProcessImportId.Text = sSelProcessType;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetProcessTypes", XmlTemplate, out Data, false, true);
                if (Data != null && !string.IsNullOrEmpty(Data))
                {
                    oDS.ReadXml(new StringReader(Data));
                    ListItem item = null;
                    if (oDS != null)
                    {
                        if (oDS.Tables["option"] != null)			//Ankit Start : Worked on MITS - 34657
                        {			//Ankit Start : Worked on MITS - 34657
                            for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                            {
                                item = new ListItem();
                                item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                                item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();
                                lstProcessNames.Items.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        protected void lstProcessNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            sProcessType = lstProcessNames.SelectedItem.Text;
            //if condn added by swati
            if (string.Compare(sProcessType, "TPA", true) == 0)
            {
                GetTPASystem();
            }
            else
            {
                lstTPAImportNames.Items.Clear();                
            }
            GetCodeTypes();
            GetCodeDetails();
        }

        private XElement GetMessageTemplateForProcessTypes()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;

            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>TPACodeMappingAdaptor.GetProcessTypes</Function></Call>");
            sXML.Append("<Document><ProcessType></ProcessType></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }
        //change end here by swati

        private XElement GetMessageTemplateForTPATypes()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;

            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>TPACodeMappingAdaptor.GetTPASystem</Function></Call>");
            sXML.Append("<Document><TPASystem></TPASystem></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForCodeTypes()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            string sTPAType = string.Empty;
            sTPAType = lstTPAImportNames.SelectedValue;
            //added by swati
            string sProcessType = string.Empty;
            sProcessType = lstProcessNames.SelectedItem.Text;
            //change end here by swati

            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>TPACodeMappingAdaptor.Get</Function></Call>");
            sXML.Append("<Document><TPATypeCode>");
            sXML.Append("<SelectedProcessType>" + sProcessType + "</SelectedProcessType>"); //added by swati
            sXML.Append("<SelectedTPAType>" + sTPAType + "</SelectedTPAType>");
            sXML.Append("</TPATypeCode></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForCodeDetails()
        {

            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
        
            string sCodeType = "0";
            string sSelCodeType = string.Empty;
            string sSelTPAType = string.Empty;
            string sSelProcessType = string.Empty;  //added by swati
            if (lstCodeType.Items.Count > 0)
            {
                sSelCodeType = lstCodeType.SelectedValue;
                sCodeType = sSelCodeType;  // lstCodeType.SelectedValue;
            }
            if (lstTPAImportNames.Items.Count > 0)
            {
                sSelTPAType = lstTPAImportNames.SelectedValue;
            }

            //added by swati
            if (lstProcessNames.Items.Count > 0)
            {
                sSelProcessType = lstProcessNames.SelectedValue;
            }
            //change end here by swati

           
            //rupal:end
            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>TPACodeMappingAdaptor.GetCodeDetails</Function></Call>");
            sXML.Append("<Document><TPACodeMapping>");
            sXML.Append("<SelectedCodeType>" + sCodeType + "</SelectedCodeType>");
            sXML.Append("<SelectedTPAType>" + sSelTPAType + "</SelectedTPAType>");
            sXML.Append("<SelectedProcessType>" + sSelProcessType + "</SelectedProcessType>");
            sXML.Append("</TPACodeMapping></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForSave()
        {
            string sSelCodeType = string.Empty;
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.SaveTPACodeInfo");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMapping><AddCode>");
            sXml = sXml.Append("<ProcessType>" + lstProcessNames.SelectedValue + "</ProcessType>");   //added by swati
            sXml = sXml.Append("<TPASystemName>" + lstTPAImportNames.SelectedValue + "</TPASystemName>");
            sXml = sXml.Append("<RMCode>" + lstRMCodes.SelectedValue + "</RMCode>");
            sXml = sXml.Append("<TPASysCode>" + lstTPASysCodes.SelectedValue + "</TPASysCode>");
            sXml = sXml.Append("<CodeType>" + lstCodeType.SelectedValue + "</CodeType>");
            sXml = sXml.Append("</AddCode></TPACodeMapping></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForMappingDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.DeleteTPACodeMapping");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMapping><DeleteCode>");
            sXml = sXml.Append("<DeletedCode>" + hdnSelected.Text + "</DeletedCode>");
            sXml = sXml.Append("<ProcessType>" + lstProcessNames.SelectedValue + "</ProcessType>");   //added by swati
            sXml = sXml.Append("</DeleteCode></TPACodeMapping></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        

        private void BindMappingGridData(string data)
        {
            try
            {
                hdValidateKey.Value = string.Empty;
                XmlDocument xmldoc = new XmlDocument();
                string sSelCodeType = string.Empty;
                xmldoc.LoadXml(data);
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                dtGridData.Columns.Add("CBId");
                dtGridData.Columns.Add("tpasyscode");
                dtGridData.Columns.Add("rmcode");
                
                
                XmlNodeList xmlNodeList = xmldoc.SelectNodes("//MappedCodes");
                if (xmlNodeList.Count > 0)
                {
                    foreach (XmlNode objNodes in xmlNodeList)
                    {
                        objRow = dtGridData.NewRow();
                        string sRowId = objNodes.Attributes["RowId"].Value.ToString();
                        string sTPASysCode = objNodes.Attributes["TPASysCode"].Value.ToString();
                        string sRMCode = objNodes.Attributes["RMCode"].Value.ToString();
                        string sValidateKey = objNodes.Attributes["Key"].Value.ToString();
                        objRow["CBId"] = sRowId;
                        objRow["tpasyscode"] = sTPASysCode;
                        objRow["rmcode"] = sRMCode;
                        
                       
                        hdValidateKey.Value = hdValidateKey.Value + "," + sValidateKey;
                        dtGridData.Rows.Add(objRow);
                    }
                }

                objRow = dtGridData.NewRow();
                objRow["CBId"] = "";
                objRow["tpasyscode"] = "";
                objRow["rmcode"] = "";
               
                
                dtGridData.Rows.Add(objRow);

                sSelCodeType = lstCodeType.SelectedValue;

                gvTPACodeMappingGrid.DataSource = dtGridData;
                gvTPACodeMappingGrid.DataBind();
                gvTPACodeMappingGrid.Rows[gvTPACodeMappingGrid.Rows.Count - 1].Visible = false;

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void addTPABtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateForAdd();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.AddNewProcess", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        //txtProcessSystemName.Text = "";
                        txtProcessName.Text = "";
                    }

                    else
                    {
                        BindGridData();
                        //txtProcessSystemName.Text = "";
                        txtProcessName.Text = "";

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void removeTPBtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForDeletion();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.DeleteProcessMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        //txtProcessSystemName.Text = "";
                        txtProcessName.Text = "";
                    }

                    else
                    {
                        BindGridData();
                        //txtProcessSystemName.Text = "";
                        txtProcessName.Text = "";
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplateForAdd()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.AddNewProcess");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor><AddCode>");
            //sXml = sXml.Append("<ProcessSystemName>" + txtProcessSystemName.Text + "</ProcessSystemName>");
            sXml = sXml.Append("<ProcessName>" + txtProcessName.Text + "</ProcessName>");
            sXml = sXml.Append("</AddCode></TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void BindGridData()
        {
            try
            {
                string data = string.Empty;
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                XElement XmlTemplate = null;
                bool bReturnStatus = false;

                XmlTemplate = GetMessageTemplateForAllMappings();
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetAvailableProcess", XmlTemplate, out data, false, true);
                dtGridData.Columns.Add("CBId");
                //dtGridData.Columns.Add("ProcessSystemName");
                dtGridData.Columns.Add("ProcessName");
                if (bReturnStatus)
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(data);
                    string sRowId = string.Empty;
                    string sProcessSystemName = string.Empty;
                    string sProcessName = string.Empty;
                    XmlNodeList xmlNodeList = xmldoc.SelectNodes("//AvailableProcessList/ProcessType");
                    if (xmlNodeList.Count > 0)
                    {
                        foreach (XmlNode objNodes in xmlNodeList)
                        {
                            objRow = dtGridData.NewRow();
                            sRowId = objNodes.Attributes["ProcessRowID"].Value.ToString();
                            //sProcessSystemName = objNodes.Attributes["ProcessSystemName"].Value.ToString(); //spahariya MITS 30911
                            sProcessName = objNodes.Attributes["ProcessName"].Value.ToString();//spahariya MITS 30911
                            objRow["CBId"] = sRowId;
                            //objRow["ProcessSystemName"] = sProcessSystemName;
                            objRow["ProcessName"] = sProcessName;
                            dtGridData.Rows.Add(objRow);
                        }
                        GridViewAvailableProcess.DataSource = dtGridData;
                        GridViewAvailableProcess.DataBind();
                    }
                }
                if (dtGridData.Rows.Count <= 0)
                {
                    objRow = dtGridData.NewRow();
                    objRow["CBId"] = "";
                    //objRow["ProcessSystemName"] = "";
                    objRow["ProcessName"] = "";

                    dtGridData.Rows.Add(objRow);
                    GridViewAvailableProcess.DataSource = dtGridData;
                    GridViewAvailableProcess.DataBind();
                    GridViewAvailableProcess.Rows[GridViewAvailableProcess.Rows.Count - 1].Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplateForAllMappings()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.GetAvailableProcess");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor>");
            sXml = sXml.Append("<AddCode></AddCode>");
            sXml = sXml.Append("</TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.DeleteProcessMapping");
            sXml = sXml.Append("</Function></Call><Document><ProcessMapping><DeleteCode>");
            sXml = sXml.Append("<DeletedCode>" + hdnSelected.Text + "</DeletedCode>");
            sXml = sXml.Append("</DeleteCode></ProcessMapping></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }        

    }
}