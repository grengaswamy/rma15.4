﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

namespace Riskmaster.UI.Utilities.Manager.SupplementalFieldData
{
    public partial class AccomSupplemental : System.Web.UI.Page
    {
        private string strSysTableName = string.Empty;
        private string strUserTableName = string.Empty;
        private string strTableType = string.Empty;
        private bool isFieldListEmpty = false;
        private bool isIndexListEmpty = false;
        public bool isTableFASReportable
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["queryString"] != null)
                {
                    string[] strSplit = Request.QueryString["queryString"].Split(new Char[] { '|' });
                    if (strSplit.Length > 1)
                    {
                        strUserTableName = strSplit[0];
                        Title.Text = "Table :" + strUserTableName;
                        formtitle.InnerText = Title.Text;
                        strSysTableName = strSplit[1];
                        SysTableName.Value = strSysTableName;
                    }
                }

                if (Request.QueryString["tabletype"] != null)
                {
                    strTableType = Request.QueryString["tabletype"];
                    TableType.Value = strTableType;
                    //if juridiction data
                    if (strTableType == "9")
                    {
                        New_FieldListGrid.Visible = false;
                    }
                }
                if (AppHelper.ReadCookieValue("EnableFAS").Equals("-1") && (FASSupplementalTableList.IsTableFASReportable(strSysTableName)))
                {
                    isTableFASReportable = true;
                }
                if (!isTableFASReportable)
                {
                    FASReportable.Style.Add("display", "none");
                }
                if (Action.Value != "Move" && Action.Value != "Save_PV")
                {
                    ViewState.Remove("arrRowIds");
                    ViewState.Remove("arrRowIdIndex");
                    BindToGridView();

                    if (IsPostBack)
                    {
                        if ((IsPVSaved.Value == "") && Page.Request.Params["__EVENTTARGET"] != "chkAllFASReportable")
                        {
                            IsPVSaved.Value = "False";
                        }
                    }
                }

                //Called from page unload
                if (Action.Value == "Force_PVRun")
                {
                    RunPowerViewUpgrade();
                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindToGridView()
        {
            Action.Value = "";
            //DataSet dsAccomList = null;
            DataTable dtFieldList = null;
            DataTable dtIndexlist = null;
            GetGetFieldListData(ref dtFieldList, ref dtIndexlist);

            GridView1.DataSource = dtFieldList;

            GridView1.DataBind();
            if (isFieldListEmpty == true)
            {
                GridView1.Rows[0].Visible = false;
            }

            GridView2.DataSource = dtIndexlist;
            GridView2.DataBind();
            if (isIndexListEmpty == true)
            {
                GridView2.Rows[0].Visible = false;

            }
            //asharma326 MITS 32386
            if (isTableFASReportable)
            {
                if ((!IsPostBack) || (string.IsNullOrEmpty(Page.Request.Params["__EVENTTARGET"])))
                {
                    if (!isFieldListEmpty)
                    {
                        int iNoCount = dtFieldList.Select("[FAS rep] = 'No'").Count();
                        if (iNoCount > 0)
                            chkAllFASReportable.Checked = false;
                        else
                            chkAllFASReportable.Checked = true;
                    }
                    else
                        chkAllFASReportable.Checked = false;
                }
            }
            else { GridView1.Columns[8].Visible = false; }

        }

        private void GetGetFieldListData(ref DataTable dtFieldList, ref DataTable dtIndexlist)
        {
            string supplementalServiceOutput = string.Empty;
            XmlDocument supplementalDoc = null;
            SupplementalHelper suppHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;
            string orbeonSessionId = string.Empty;
            try
            {
                suppHelper = new SupplementalHelper();

                inputDocNode = InputDocForAccommodationSupplemental();

                serviceMethodToCall = "TableManagerAdaptor.GetFieldList";
                supplementalServiceOutput = suppHelper.CallCWSFunctionForSupplemental(orbeonSessionId, inputDocNode, serviceMethodToCall);

                supplementalDoc = new XmlDocument();
                supplementalDoc.LoadXml(supplementalServiceOutput);
                ErrorControl1.errorDom = supplementalServiceOutput;
                if (supplementalDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/FieldList/listrow") == null)
                {
                    isFieldListEmpty = true;
                }

                if (supplementalDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/IndexList/listrow") == null)
                {
                    isIndexListEmpty = true;
                }

                dtFieldList = new DataTable();
                GridHeaderAndData(supplementalDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/FieldList/listhead"), supplementalDoc.SelectNodes("ResultMessage/Document/PassToWebService/FieldList/listrow"), dtFieldList);


                dtIndexlist = new DataTable();
                GridHeaderAndData(supplementalDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/IndexList/listhead"), supplementalDoc.SelectNodes("ResultMessage/Document/PassToWebService/IndexList/listrow"), dtIndexlist);



            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {

            }
        }

        private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;
            char[] arrSpclChar = { '.', '?' };
            // Create the Columns corresponding to the Headers specified in ListHead section of Xml
            foreach (XmlElement objHeaderDetails in objGridHeaders)
            {
                //Add Column to be associated with Datasource of grid
                dcGridColumn = new DataColumn();
                if (objHeaderDetails.Name != "SeqNo")
                {
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.InnerText.Replace(".?", "").TrimEnd(arrSpclChar);
                }
                else
                {
                    dcGridColumn.ColumnName = "SeqNo";
                }

                dtGridData.Columns.Add(dcGridColumn);
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();
                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        dr[iColumnIndex++] = HttpUtility.HtmlEncode(objElem.InnerText);   //Ritesh-MITS 27533-Handling special characters

                    }
                    dtGridData.Rows.Add(dr);

                }
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
            }

        }


        private XmlNode InputDocForAccommodationSupplemental()
        {
            XmlNode xmlNode = null;
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
              <PassToWebService>
                  <SysTableName></SysTableName> 
                  <UserTableName></UserTableName> 
                  <TableType></TableType> 
                  <FieldList>
                   <listhead>
                      <DisplaySeq>#</DisplaySeq> 
                      <SysFieldName>System Field Name</SysFieldName> 
                      <UserPrompt>User Prompt</UserPrompt> 
                      <FieldType>Field Type</FieldType> 
                      <CodeFileId /> 
                      <Size>Size</Size> 
                      <Required type='bool'>Req.?</Required> 
                      <LookUp type='bool'>Lookup.?</LookUp> 
                      <Delete type='bool'>Del.?</Delete> 
                      <FASReportable type='bool'>FAS rep.?</FASReportable> 
                      <Pattern>Pattern</Pattern> 
                      <GrpAssoFlag /> 
                      <AssoField /> 
                      <GroupAssocs>Group Association</GroupAssocs> 
                      <IsPattern /> 
                      <SeqNo />
                      <RowId>Row Id</RowId> 
                   </listhead>
                   </FieldList>
                  <IndexList>
                   <listhead>
                      <IndexName>Index Names</IndexName> 
                      <IndexField>Index Fields</IndexField> 
                      <Unique>Unique?</Unique> 
                      <RowId>Row Id</RowId> 
                   </listhead>
                  </IndexList>
              </PassToWebService>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            xmlNode = xmlNodeDoc.SelectSingleNode("PassToWebService/SysTableName");
            xmlNode.InnerText = strSysTableName;

            xmlNode = xmlNodeDoc.SelectSingleNode("PassToWebService/UserTableName");
            xmlNode.InnerText = strUserTableName;

            xmlNode = xmlNodeDoc.SelectSingleNode("PassToWebService/TableType");
            xmlNode.InnerText = strTableType;




            return xmlNodeDoc;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;
            
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ArrayList arrRowId = null;
                ArrayList arrRowIdIndex = null;
                if (ViewState["arrRowIds"] != null)
                {
                    arrRowId = (ArrayList)ViewState["arrRowIds"];
                    arrRowIdIndex = (ArrayList)ViewState["arrRowIdIndex"];
                }
                else
                {
                    arrRowId = new ArrayList();
                    arrRowIdIndex = new ArrayList();
                }

                javascriptSelectRow = "SelectSupplemental('" + DataBinder.Eval(e.Row.DataItem, "Row Id").ToString() + "')";
                //Adding rowids to the array list will be used to swap rows
                arrRowId.Add(DataBinder.Eval(e.Row.DataItem, "Row Id").ToString());
                arrRowIdIndex.Add(DataBinder.Eval(e.Row.DataItem, "SeqNo").ToString());
                ViewState.Add("arrRowIds", arrRowId);
                ViewState.Add("arrRowIdIndex", arrRowIdIndex);
                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);

                if (isTableFASReportable)
                {
                    if (e.Row.Cells[8].Text == "No")
                        chkAllFASReportable.Checked = false;
                }
            }
            
            
            
        }
        protected void chkAllFASReportable_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                string supplementalServiceOutput = "";
                string s_flag = "";
                s_flag = chkAllFASReportable.Checked ? "true" : "false";
                XElement oTemplate = XElement.Parse(@"
                        <Message>
                        <Authorization></Authorization> 
                        <Call>
                        <Function>TableManagerAdaptor.UpdateAllFASFields</Function> 
                        </Call>
                        <Document>
                            <PassToWebService>
                                  <control name='TableType' >" + TableType.Value + @"</control>
                                  <control name='chkAllFASReportable' >" + s_flag + @"</control>
                                  <control name='SysTableName' >" + SysTableName.Value + @"</control>
                            </PassToWebService>
                        </Document>
                        </Message>
                    ");
                XmlDocument xmlInputDoc = new XmlDocument();
                using (XmlReader reader = oTemplate.CreateReader())
                {
                    xmlInputDoc.Load(reader);
                }

                supplementalServiceOutput = AppHelper.CallCWSService(xmlInputDoc.InnerXml);
                ErrorControl1.errorDom = supplementalServiceOutput;
                BindToGridView();
            }
            catch (Exception exe)
            {
                ErrorHelper.logErrors(exe);
                ErrorControl1.errorDom = exe.Message.ToString();
            }
        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectSupplemental('" + DataBinder.Eval(e.Row.DataItem, "Row Id").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }

        protected void FieldListGrid_moveup_Click(object sender, ImageClickEventArgs e)
        {
            ArrayList arrList = (ArrayList)ViewState["arrRowIds"];
            ArrayList arrRowIdIndex = (ArrayList)ViewState["arrRowIdIndex"];
            Action.Value = "";
            XmlDocument inputDocNode = new XmlDocument();
            int intCurrSeq = arrList.IndexOf(currentSelRowId.Value);
            string strForm = "<form name='FieldList'><group name='FieldList'>";
            string Field1;
            string Field2;
            string SeqNo1;
            string SeqNo2;
            int index;
            if (arrList.Count > 1 && (intCurrSeq + 1) > 1)
            {
                index = arrList.IndexOf(intCurrSeq);
                Field1 = "<control name='Field1'>" + arrList[intCurrSeq].ToString() + "</control>";
                Field2 = "<control name='Field2'>" + arrList[intCurrSeq-1].ToString() + "</control>";

                SeqNo1 = "<control name='SeqNo1'>" + arrRowIdIndex[intCurrSeq].ToString() + "</control>";
                SeqNo2 = "<control name='SeqNo2'>" + arrRowIdIndex[intCurrSeq - 1].ToString() + "</control>";

                strForm = strForm + Field1 + Field2 + SeqNo1 + SeqNo2 + "</group></form>";


                XElement oTemplate = XElement.Parse(strForm);

                using (XmlReader reader = oTemplate.CreateReader())
                {
                    inputDocNode.Load(reader);
                }

                string supplementalServiceOutput = string.Empty;
                SupplementalHelper suppHelper = null;
                XmlDocument supplementalDoc = null;
                string serviceMethodToCall = string.Empty;
                string orbeonSessionId = string.Empty;
                try
                {
                    suppHelper = new SupplementalHelper();
                    serviceMethodToCall = "TableManagerAdaptor.SwapField";
                    supplementalServiceOutput = suppHelper.CallCWSFunctionForSupplemental(orbeonSessionId, inputDocNode, serviceMethodToCall);

                    supplementalDoc = new XmlDocument();
                    supplementalDoc.LoadXml(supplementalServiceOutput);

                    //ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>document.forms[0].submit()</script>");

                    ViewState.Remove("arrRowIds");
                    ViewState.Remove("arrRowIdIndex");
                    BindToGridView();
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>alert('No row to move up');</script>");

            }




        }

        protected void FieldListGrid_movedown_Click(object sender, ImageClickEventArgs e)
        {
            ArrayList arrList = (ArrayList)ViewState["arrRowIds"];
            ArrayList arrRowIdIndex = (ArrayList)ViewState["arrRowIdIndex"];
            XmlDocument inputDocNode = new XmlDocument();
            Action.Value = "";
            int intCurrSeq = arrList.IndexOf(currentSelRowId.Value);
            string strForm = "<form name='FieldList'><group name='FieldList'>";
            string Field1;
            string Field2;
            string SeqNo1;
            string SeqNo2;
            if (arrList.Count > (intCurrSeq + 1))
            {
                Field1 = "<control name='Field1'>" + arrList[intCurrSeq].ToString() + "</control>";
                Field2 = "<control name='Field2'>" + arrList[intCurrSeq + 1].ToString() + "</control>";

                SeqNo1 = "<control name='SeqNo1'>" + arrRowIdIndex[intCurrSeq].ToString() + "</control>";
                SeqNo2 = "<control name='SeqNo2'>" + arrRowIdIndex[intCurrSeq + 1].ToString() + "</control>";

                strForm = strForm + Field1 + Field2 + SeqNo1 + SeqNo2 + "</group></form>";


                XElement oTemplate = XElement.Parse(strForm);

                using (XmlReader reader = oTemplate.CreateReader())
                {
                    inputDocNode.Load(reader);
                }

                string supplementalServiceOutput = string.Empty;
                SupplementalHelper suppHelper = null;
                XmlDocument supplementalDoc = null;
                string serviceMethodToCall = string.Empty;
                string orbeonSessionId = string.Empty;
                try
                {
                    suppHelper = new SupplementalHelper();
                    serviceMethodToCall = "TableManagerAdaptor.SwapField";
                    supplementalServiceOutput = suppHelper.CallCWSFunctionForSupplemental(orbeonSessionId, inputDocNode, serviceMethodToCall);

                    supplementalDoc = new XmlDocument();
                    supplementalDoc.LoadXml(supplementalServiceOutput);

                    //ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>document.forms[0].submit()</script>");
                    ViewState.Remove("arrRowIds");
                    ViewState.Remove("arrRowIdIndex");
                    BindToGridView();

                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>alert('No row to move down');</script>");

            }
        }

        private void RunPowerViewUpgrade()
        {
            string sXmlFormName = "";
            //RMA-4691 - start - Supplementals not getting update in Riskmaster views case of Non-Powerviewable FDM pages
            bool bPowerviewable = true;
            bool bSuccess = false;
            //RMA-4691 - end
            string supplementalServiceOutput = string.Empty;
            try
            {
                if (SysTableName.Value != "")
                {

                    switch (TableType.Value)
                    {
                        case "6":
                            XmlDocument xmlNodeDoc = new XmlDocument();
                            xmlNodeDoc.Load(Server.MapPath("~/App_Data/SupplementalUpgradeList.xml"));
                            XmlElement objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("SystemTableNameMapping/SystemTable[@name ='" + SysTableName.Value + "']");
                            sXmlFormName = objXmlSourceElement.InnerXml;
                            //RMA-4691 - start - Supplementals not getting update in Riskmaster views case of Non-Powerviewable FDM pages
                            if (objXmlSourceElement.Attributes["NonPowerviewable"] != null && Conversion.CastToType<bool>(objXmlSourceElement.Attributes["NonPowerviewable"].Value, out bSuccess) == true)
                                bPowerviewable = false;
                            //RMA-4691 - end
                            break;
                        case "8":
                            sXmlFormName = "admintracking.xml";
                            break;
                        case "9":
                            sXmlFormName = "";
                            break;
                    }

                    IsPVSaved.Value = "True";
                    XElement oTemplate = XElement.Parse(@"
                        <Message>
                        <Authorization></Authorization> 
                        <Call>
                        <Function>TableManagerAdaptor.SaveField_PVUpgrade</Function> 
                        </Call>
                        <Document>
                            <PassToWebService>
                                  <control name='XmlFormToUpgrade' >" + sXmlFormName + @"</control>
                                  <control name='TableType' >" + TableType.Value + @"</control>
                                  <control name='SysTableName' >" + SysTableName.Value + @"</control>
                                  <control name='Powerviewable' >" + bPowerviewable + @"</control>
                            </PassToWebService>
                        </Document>
                        </Message>
                    ");
                    XmlDocument xmlInputDoc = new XmlDocument();
                    using (XmlReader reader = oTemplate.CreateReader())
                    {
                        xmlInputDoc.Load(reader);
                    }

                    supplementalServiceOutput = AppHelper.CallCWSService(xmlInputDoc.InnerXml);
                    ErrorControl1.errorDom = supplementalServiceOutput;

                    if (Action.Value == "Force_PVRun")
                    {

                    }
                    Action.Value = "";
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void Save_PV_Click(object sender, ImageClickEventArgs e)
        {
            RunPowerViewUpgrade();

//                string sXmlFormName = "";
//                string supplementalServiceOutput = string.Empty;
//                try
//                {
//                    if (SysTableName.Value != "")
//                    {

//                        switch (TableType.Value)
//                        {
//                            case "6":
//                                XmlDocument xmlNodeDoc = new XmlDocument();
//                                xmlNodeDoc.Load(Server.MapPath("~/App_Data/SupplementalUpgradeList.xml"));
//                                XmlElement objXmlSourceElement = (XmlElement)xmlNodeDoc.SelectSingleNode("SystemTableNameMapping/SystemTable[@name ='" + SysTableName.Value + "']");
//                                sXmlFormName = objXmlSourceElement.InnerXml;
//                                break;
//                            case "8":
//                                sXmlFormName = "admintracking.xml";
//                                break;
//                            case "9":
//                                sXmlFormName = "";
//                                break;
//                        }


//                        XElement oTemplate = XElement.Parse(@"
//                        <Message>
//                        <Authorization></Authorization> 
//                        <Call>
//                        <Function>TableManagerAdaptor.SaveField_PVUpgrade</Function> 
//                        </Call>
//                        <Document>
//                            <PassToWebService>
//                                  <control name='XmlFormToUpgrade' >" + sXmlFormName + @"</control>
//                                  <control name='TableType' >" + TableType.Value + @"</control>
//                                  <control name='SysTableName' >" + SysTableName.Value + @"</control>
//                            </PassToWebService>
//                        </Document>
//                        </Message>
//                    ");
//                        XmlDocument xmlInputDoc = new XmlDocument();
//                        using (XmlReader reader = oTemplate.CreateReader())
//                        {
//                            xmlInputDoc.Load(reader);
//                        }

//                        supplementalServiceOutput = AppHelper.CallCWSService(xmlInputDoc.InnerXml);
//                        ErrorControl1.errorDom = supplementalServiceOutput;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    ErrorHelper.logErrors(ex);
//                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
//                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
//                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
//                }
        }
    }

    public class FASSupplementalTableList
    {
        private static ArrayList _FASSupplementalTableList = new ArrayList();

        public static bool IsTableFASReportable(string p_tableName)
        {
            if(_FASSupplementalTableList.Count==0)
                FillArray();
            if (_FASSupplementalTableList.Contains(p_tableName))
                return true;
            else
                return false;
        }

        private static void FillArray()
        {
            _FASSupplementalTableList.Add("EVENT_SUPP");
            _FASSupplementalTableList.Add("CLAIM_SUPP");
            _FASSupplementalTableList.Add("CLAIMANT_SUPP");
            _FASSupplementalTableList.Add("POLICY_SUPP");
            _FASSupplementalTableList.Add("UNIT_X_CLAIM_SUPP");
            _FASSupplementalTableList.Add("CLAIM_X_PROPERTYLOSS_SUPP");
            _FASSupplementalTableList.Add("CLAIM_X_LIABILITYLOSS_SUPP");
            _FASSupplementalTableList.Add("CLAIM_ADJ_SUPP");
            _FASSupplementalTableList.Add("LITIGATION_SUPP");
            _FASSupplementalTableList.Add("DEFENDANT_SUPP");
            _FASSupplementalTableList.Add("EXPERT_SUPP");
            _FASSupplementalTableList.Add("EMP_SUPP");
        }
    }
}
