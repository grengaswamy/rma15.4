﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager.SupplementalFieldData
{
    public partial class GridProperty : System.Web.UI.Page
    {

        private string strTableType = string.Empty;
        private string strSysTableName = string.Empty;
        private string strUserPrompt = string.Empty;
        private bool IsCallFromCurrentWindow = false;
        private bool IsRemoveButtonPressed = false;//Added by Shivendu for MITS 16995
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["tabletype"] != null)
            {
                strTableType = Request.QueryString["tabletype"];
            }

            if (Request.QueryString["rowId"] != null)
            {
                rowId.Value = Request.QueryString["rowId"];
            }

            if (Request.QueryString["SysFieldName"] != null)
            {
                sysFieldName.Value = Request.QueryString["SysFieldName"];
            }

            if (Request.QueryString["SysTableName"] != null)
            {
                strSysTableName = Request.QueryString["SysTableName"];
            }

            if (Request.QueryString["UserPrompt"] != null)
            {
                strUserPrompt = Request.QueryString["UserPrompt"];
                GridHeader.InnerText = strUserPrompt;
            }

            if (hdnaction.Value == "true")
            {
                GetGridPropertyData();
                hdnaction.Value = "false";
            }

            

           
        }

        private void GetGridPropertyData()
        {
            string supplementalServiceOutput = string.Empty;
            XmlDocument supplementalDoc = null;
            SupplementalHelper suppHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                suppHelper = new SupplementalHelper();

                inputDocNode = InputXmlDoc();

                serviceMethodToCall = "TableManagerAdaptor.GetGridInformation";

                supplementalServiceOutput = suppHelper.CallCWSFunctionForSupplemental("", inputDocNode, serviceMethodToCall);
                ErrorControl1.errorDom = supplementalServiceOutput;

                supplementalDoc = new XmlDocument();
                supplementalDoc.LoadXml(supplementalServiceOutput);

                BindDataToControl(supplementalDoc);


            }
            catch(Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
           
        }

        private void BindDataToControl(XmlDocument xmlServiceDoc)
        {
            XmlElement objXmlSourceElement;
            
            DataTable dtGridProperty = new DataTable();

           
            SetGridHeader(xmlServiceDoc.SelectSingleNode("ResultMessage/Document/XMLCol/GridXML/grid/table/fields"), xmlServiceDoc.SelectSingleNode("ResultMessage/Document/XMLCol/ColName"), dtGridProperty);
           
            objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/XMLCol/MinCols");
            minCols.Value = objXmlSourceElement.InnerXml;

            objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/XMLCol/MaxCols");
            maxCols.Value = objXmlSourceElement.InnerXml;


            objXmlSourceElement = (XmlElement)xmlServiceDoc.SelectSingleNode("ResultMessage/Document/XMLCol/ExistingColumns");
            if (hdnIsExistingColSet.Value == "" && objXmlSourceElement.InnerXml != "")
            {
                SetExistingColumn(objXmlSourceElement.InnerXml);
                hdnIsExistingColSet.Value = "true";

            }



        }

        private void SetExistingColumn(string GridColumnHdr)
        {
            string[] strSplit = GridColumnHdr.Split(new Char[] { ',' });
            bool IsColumnAdded = false;
            DataColumn dcGridColumn;
            DataTable dtGridData = new DataTable();
            
            for(int colCnt=0; colCnt < strSplit.Length ;  colCnt++)        
            {

                   //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();

                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = strSplit[colCnt].ToString() ;

                    dtGridData.Columns.Add(dcGridColumn);
                    colCount.Value = (colCnt + 1).ToString();
                    IsColumnAdded = true;
                
            }

            if (IsColumnAdded)
            {
                dtGridData.Rows.Add(dtGridData.NewRow());
                ViewState.Remove("dtGrid");
                ViewState["dtGrid"] = dtGridData;

                grdView.DataSource = dtGridData;
                grdView.DataBind();
            }


        }

        private void SetGridHeader(XmlNode objGridHeaders, XmlNode objGridColName, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;
            bool IsColumnAdded = false;
            if (objGridHeaders != null)
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {

                    if (objHeaderDetails.SelectSingleNode("title") != null)
                    {
                         //Add Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();

                        // Assign the Column Name from XmlNode
                        dcGridColumn.ColumnName = objHeaderDetails.SelectSingleNode("title").InnerXml;

                        dtGridData.Columns.Add(dcGridColumn);
                        IsColumnAdded = true;
                    }
                }
            }
            if (objGridColName != null && objGridColName.InnerText != "")
            {
                //Add Column to be associated with Datasource of grid
                dcGridColumn = new DataColumn();

                // Assign the Column Name from XmlNode
                dcGridColumn.ColumnName = objGridColName.InnerText;

                dtGridData.Columns.Add(dcGridColumn);
                IsColumnAdded = true;
            }

            if (IsColumnAdded)
            {
                dtGridData.Rows.Add(dtGridData.NewRow());
                ViewState.Remove("dtGrid");
                ViewState["dtGrid"] = dtGridData;

                grdView.DataSource = dtGridData;
                grdView.DataBind();
            }
            else
            {
                if (colCount.Value == "0")
                {
                    ViewState.Remove("dtGrid");
                    ViewState["dtGrid"] = dtGridData;
                    grdView.DataSource = dtGridData;
                    grdView.DataBind();
                }
            }

            //set hidden fields
            GetGridInnerXML();
        }



        private XmlDocument InputXmlDoc()
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XmlElement objElement = null;
            XmlNode objectXmlNode = null;
            XElement oTemplate = XElement.Parse(@" 
                                                    <XMLCol>
                                                      <GridXML /> 
                                                      <ColName /> 
                                                      <ColCount /> 
                                                      <ColToBeRemoved /> 
                                                      <Columns /> 
                                                      <FieldType /> 
                                                      <UserPrompt></UserPrompt> 
                                                      <SysFieldName></SysFieldName> 
                                                      <TableType></TableType> 
                                                      <SysTableName></SysTableName> 
                                                      <RowId /> 
                                                      <MinCols /> 
                                                      <MaxCols /> 
                                                      <DuplicateField /> 
                                                      <ExistingColumns /> 
                                                      <GridProperty /> 
                                                     </XMLCol>
                                                   ");
            

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/ColName");
            objElement.InnerXml = txtColName.Value;

            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/ColCount");
            objElement.InnerXml = colCount.Value;


            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/GridXML");
            if (IsCallFromCurrentWindow)
            {
                objElement.InnerXml = GetGridInnerXML();
            }
            else
            {
                objElement.InnerXml = hdnGridXml.Value;
                //objElement.InnerXml = AppHelper.HTMLCustomDecode(hdnGridXml.Value);//Bharani - MITS : 35826
                hdnGridXml.Value = "";
            }

            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/TableType");
            objElement.InnerXml = strTableType;

            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/RowId");
            objElement.InnerXml = rowId.Value;

            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/SysFieldName");
            objElement.InnerXml = sysFieldName.Value;

            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/SysTableName");
            objElement.InnerXml = strSysTableName;

            objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/UserPrompt");
            objElement.InnerXml = strUserPrompt;

            //if (colToBeRemoved.Value != "")
            //{

            //    objElement = (XmlElement)xmlNodeDoc.SelectSingleNode("XMLCol/ColToBeRemoved");
            //    objElement.InnerXml = colToBeRemoved.Value;
            //}



            return xmlNodeDoc;


        }

        private string GetGridInnerXML()
        {
            string strGridStart = "<grid> <table style='border: solid black 1px'> <fields>";
            string strGridBodystart = "<field nosort='true'><title>";
            string strGridBodyend = "</title><datatype>istring</datatype></field>";
            string strGridEnd = "</fields> <rows> <row></row> </rows> </table> </grid>";
            string strGridInnerXML = string.Empty;
            
            hdnColumns.Value = "";
            if (ViewState["dtGrid"] != null)
            {
                DataTable dt = (DataTable)ViewState["dtGrid"];
                if (colCount.Value != "0")
                {
                    for (int intColCnt = 0; intColCnt < dt.Columns.Count; intColCnt++)
                    {
                        //not adding the column to remove
                        //smishra25:Added remove button pressed check for MITS 16995
                        if (IsRemoveButtonPressed && hdnRemove.Value != "" && hdnRemove.Value == dt.Columns[intColCnt].ColumnName)
                            continue;
                        strGridInnerXML = strGridInnerXML + strGridBodystart + dt.Columns[intColCnt].ColumnName + strGridBodyend;
                        if (hdnColumns.Value == "")
                        {
                            hdnColumns.Value = dt.Columns[intColCnt].ColumnName;
                        }
                        else
                        {
                            hdnColumns.Value = hdnColumns.Value + "," + dt.Columns[intColCnt].ColumnName; 
                        }
                    }
                }

                strGridInnerXML = strGridStart + strGridInnerXML + strGridEnd;

            }

            hdnGridXml.Value = strGridInnerXML;
            return strGridInnerXML;

        }

        protected void grdView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectColumn = string.Empty;

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int columnIndex = 0; columnIndex < e.Row.Cells.Count; columnIndex++)
                {
                    e.Row.Cells[columnIndex].Attributes["id"] = ("grdhd" + columnIndex.ToString());
                    // Add a cursor style to the cells
                    e.Row.Cells[columnIndex].Attributes["style"] += "cursor:pointer;cursor:hand;";

                    e.Row.Cells[columnIndex].Attributes["onmouseover"] = "this.className='gridHdColover'";

                    e.Row.Cells[columnIndex].Attributes["onmouseout"] = "this.className='grdColHdActive'";
                
                
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int columnIndex = 0; columnIndex < e.Row.Cells.Count; columnIndex++)
                {
                    javascriptSelectColumn = "SelectColumn('" + columnIndex.ToString() + "')";

                    e.Row.Cells[columnIndex].Attributes["id"] = ("grd" + columnIndex.ToString());
                   // Add this javascript to the onclick Attribute of the cell
                    e.Row.Cells[columnIndex].Attributes["onclick"] = javascriptSelectColumn;
                    // Add a cursor style to the cells
                    e.Row.Cells[columnIndex].Attributes["style"] += "cursor:pointer;cursor:hand;";

                    e.Row.Cells[columnIndex].Attributes["onmouseover"] = "this.className='gridCelover'";

                    e.Row.Cells[columnIndex].Attributes["onmouseout"] = "this.className='grdCelActive'";
                }
            }

        }

        private DataTable GetGridPropertyTable()
        {
            DataTable dtGridProperty = null; 
            if (ViewState["dtGrid"] == null)
            {
                dtGridProperty = new DataTable();
            }
            else
            {
                dtGridProperty = (DataTable)ViewState["dtGrid"];

                dtGridProperty.Rows[0].Delete();
            }

            dtGridProperty.Columns.Add(txtColName.Value);
            dtGridProperty.Rows.Add(dtGridProperty.NewRow());
            ViewState["dtGrid"] = dtGridProperty;
            return dtGridProperty;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            IsCallFromCurrentWindow = true;
            if (colCount.Value != "" || colCount.Value == "0")
            {
                colCount.Value = (int.Parse(colCount.Value) + 1).ToString();
            }
            else
            {
                colCount.Value = "1";
            }
            GetGridPropertyData();
            
            
            buttonRemove.Enabled = true;  
            txtColName.Value = "";
 
        }

        protected void buttonRemove_Click(object sender, EventArgs e)
        {
            IsCallFromCurrentWindow = true;
            try
            {
                colCount.Value = (int.Parse(colCount.Value) - 1).ToString();
                IsRemoveButtonPressed = true;//Added by Shivendu for MITS 16995
                GetGridPropertyData();
                IsRemoveButtonPressed = false;//Added by Shivendu for MITS 16995
                colToBeRemoved.Value = "";
                hdnRemove.Value = "";
            }
            catch(Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        protected void btnRemvAll_Click(object sender, EventArgs e)
        {
            try
            {
                IsCallFromCurrentWindow = true;
                colCount.Value = "0";
                GetGridPropertyData();
                colToBeRemoved.Value = "";
                hdnRemove.Value = "";
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

       

    }
}
