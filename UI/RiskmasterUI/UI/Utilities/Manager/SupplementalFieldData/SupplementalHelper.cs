﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.UI.Utilities.Manager.SupplementalFieldData;
using System.Collections;


namespace Riskmaster.UI.Utilities.Manager.SupplementalFieldData
{
    public class SupplementalHelper
    {

        public DataSet ConvertXmlDocToDataSet(XmlDocument xmlDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(xmlDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        public string CallCWSFunctionForSupplemental(string sessionId, XmlNode documentNodeChild, string serviceMethodToCall)
        {
            XmlNode oSessionCmdElement = null;
            string oMessageElement = string.Empty;
            string responseCWS = string.Empty;
          
            XmlDocument messageDoc = null;

            try
            {
                System.Collections.ArrayList arList = new System.Collections.ArrayList();
                oMessageElement = GetMessageTemplate();

                messageDoc = new XmlDocument();
                messageDoc.LoadXml(oMessageElement);

                if (!string.IsNullOrEmpty(sessionId))
                {
                    oSessionCmdElement = messageDoc.SelectSingleNode("Message/Authorization");
                    oSessionCmdElement.InnerText = sessionId;
                }

                if (documentNodeChild != null)
                {
                    oSessionCmdElement = messageDoc.SelectSingleNode("Message/Document");
                    oSessionCmdElement.InnerXml = documentNodeChild.OuterXml;
                }

                if (!string.IsNullOrEmpty(serviceMethodToCall))
                {
                    oSessionCmdElement = messageDoc.SelectSingleNode("Message/Call/Function");
                    oSessionCmdElement.InnerText = serviceMethodToCall;
                }

                responseCWS = AppHelper.CallCWSService(messageDoc.InnerXml);

                return responseCWS;

            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }

        }



                

        #region Private Methods

        private string GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function></Function>
              </Call>
              <Document>
              </Document>
            </Message>
            ");

            return oTemplate.ToString();
        }
        #endregion 
    }
}
