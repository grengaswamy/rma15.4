﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Utilities.Manager.SupplementalFieldData
{
    public partial class AddIndex : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.Attributes.Add("onclick", "javascript:return CheckList();");//abansal23 : MITS 14693
            if (Request.QueryString["SysTableName"] != null)
             {
                 SysTableName.Text = Request.QueryString["SysTableName"].ToString();
                         
             }

            if (!IsPostBack)
            {
                NonFDMCWSPageLoad("TableManagerAdaptor.GetAddIndex");
            }
        }

        protected void ADD_Click(object sender, EventArgs e)
        {
            ListItem objSelListItem = new ListItem(AvlFieldsList.SelectedItem.Text, AvlFieldsList.SelectedValue);
            SelFieldsList.Items.Add(objSelListItem);
            AvlFieldsList.Items.Remove(objSelListItem);
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            ListItem objSelListItem = new ListItem(SelFieldsList.SelectedItem.Text, SelFieldsList.SelectedValue);
            AvlFieldsList.Items.Add(objSelListItem);
            SelFieldsList.Items.Remove(objSelListItem);
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            FormMode.Text = "close";
            char[] charToTrim = {','};

            for(int itemsCount = 0; itemsCount < SelFieldsList.Items.Count ; itemsCount++)
            {
                SelectedFieldList.Text = SelectedFieldList.Text + SelFieldsList.Items[itemsCount].Text + ",";
            }
            SelectedFieldList.Text = SelectedFieldList.Text.TrimEnd(charToTrim);

            bReturnStatus = CallCWSFunction("TableManagerAdaptor.CreateIndex");

            if (bReturnStatus)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            } 
        }

        






        
    }
}
