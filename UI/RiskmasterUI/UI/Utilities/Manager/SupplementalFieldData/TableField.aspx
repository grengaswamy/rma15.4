﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TableField.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.SupplementalFieldData.TableField" ValidateRequest="false" %>

<%@ Register src="../../../Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCodeLookup" tagprefix="uc1" %>

<%@ Register src="../../../Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc2" %>

<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title runat="server" id="FormTitle">Add Field</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript">
        var frmTableField = 'Supplemental';
        function getOrglookup(sCodeTable,sFieldName,fieldtitle)
        {
	        if(m_codeWindow!=null)
		        m_codeWindow.close();
        	
	        m_sFieldName=sFieldName;
        	
	        var sFormName;
        	
	        if (eval('document.forms[0].SysFormName')!=null)
		        sFormName=document.forms[0].SysFormName.value;
	        else
		        sFormName='';
            fieldtitle = eval('document.forms[0].hdnorgLevel'); // abansal23 on 05/13/2009 MITS 14847
        		
	        var orglevel;
        	
	        var objFormElem=eval('document.forms[0].'+sFieldName+'_cid');
	        var sFind=objFormElem.value;
            orglevel = getOrgLevel(fieldtitle.value); // abansal23 on 05/13/2009 MITS 14847
        	
	        if(m_codeWindow!=null)
		        m_codeWindow.close();
	        if(sFind=='0')
	        sFind='';
	        var eventdate = "";
	        var claimdate = "";
	        if (sFormName!='')
	        {
        		
	        }
	        m_codeWindow=window.open('../../../OrganisationHierarchy/OrgHierarchyLookup.aspx?amp;tablename='+sFormName+'&rowid='+getCurrentID()+'&lob='+orglevel+'&searchOrgId='+sFind+'&eventdate='+eventdate+'&claimdate='+claimdate,'Table','width=840,height=520'+',top='+(screen.availHeight-520)/2+',left='+(screen.availWidth-840)/2+',resizable=yes,scrollbars=yes');			

        		
        	
        	
	        m_LookupTextChanged=false;
	        return false;
                 

        }
        
        function OpenGridProperty()
        {
          if(document.forms[0].UserPrompt.value==null || document.forms[0].UserPrompt.value == "" || document.forms[0].SysFieldName.value==null || document.forms[0].SysFieldName.value == "")
            {
            alert("Please enter UserPrompt and SysFieldName before clicking");
            return false;
            }
            else
            {
              m_codeWindow = window.open('GridProperty.aspx?SysTableName=' + document.getElementById('SysTableName').value + '&tabletype=' + document.getElementById('TableType').value + '&rowId=' + document.getElementById('RowId').value + '&SysFieldName=' + document.getElementById('SysFieldName').value + '&UserPrompt=' + document.getElementById('UserPrompt').value, 'Table', 'width=540,height=420' + ',top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
             return false;
            }
        }


        function TblField_Save() {        
            //MITS 25710 hlv 6/28/12 begin
            var userPrompt = document.getElementById("UserPrompt");
            userPrompt.value = userPrompt.value.replace(/\,/g, "");
            //MITS 25710 hlv 6/28/12 end

            if (TableField_Save() != false)
            {
               pleaseWait.Show();
               
               return true;
            }
            else
            {
               return false;
            }
       }

       //MITS 25710 hlv 6/28/12 begin
       function checkComma() {
           if (event.keyCode == 44) {
               alert("The comma ',' is not allowed in this field.");
               event.keyCode = 0;
           }
           
           /* undo hlv 8/17/12
           if (event.keyCode == 95) {
               alert("The underline '_' is not allowed in this field.");
               event.keyCode = 0;
           }
           */
           //MITS 25710 hlv 6/28/12 end

           //MITS 26255 hlv
           if (event.keyCode == 95) {
               alert("The underline '_' is not allowed in this field.");
               event.keyCode = 0;
           }
           //MITS 25710 hlv 6/28/12 end
       }
       

    </script>
    <style type="text/css">
        #Save
        {
            height: 28px;
        }
    </style>
</head>
 <body class="10pt">
    <form id="frmData" runat="server" name="frmData">
    <div>
     <uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
     <uc2:ErrorControl ID="ErrorControl1" runat="server" />
      <table class="toolbar"  cellspacing="0"  cellpadding="0" border="0">
        <tbody>
         <tr>
          <td> 
             
          </td>
         </tr>
         <tr>
          <td align="center" valign="middle" height="32">
           <asp:ImageButton type="image" name="Save" src="../../../../Images/save.gif" alt="" 
                  id="Save" runat="server" 
                  onmouseover="&#xA;javascript:document.all[&#34;Save&#34;].src='../../../../Images/save2.gif';&#xA;" 
                  onmouseout="&#xA;javascript:document.all[&#34;Save&#34;].src='../../../../Images/save.gif';&#xA;" 
                  title="Save" onclick="Save_Click" OnClientClick="return TblField_Save();"  />
              
             </td>
         </tr>
       </tbody>
     </table>
     
     <br /><%--<div class="msgheader" id="formtitle">Add Field</div>--%><%--zalam 04/21/2009--%>
     
     <table border="0">
       <tbody>
         <tr>
              <td><input type="text" name="300" value="" runat="server" style="display:none" id="TableType" /></td>
              <td><input type="text" name="304" value="" runat="server" style="display:none" id="SysTableName" /></td>
         </tr>
         <tr>
           <td colspan="2">
              <table border="0">
               <tbody>
                  <tr>
                  <%--MGaba2:MITS 17194:Wrong label shown on Modify Grid screen--%>
                    <td class="ctrlgroup"  colspan="2" runat ="server" id="SubHeading">Add Field</td>
                  </tr>
                  <tr> 
                    <td colspan="2"><input type="text" name="38" value="" style="display:none" runat="server" id="RowId" />
                    </td>
                  </tr>
                  <tr>
                   <td>
                    <u>Field Type</u>:&nbsp;&nbsp;
                   </td>
                   <td>
                    <asp:DropDownList ID="FieldType" runat="server" AutoPostBack="True"  
                        onselectedindexchanged="FieldType_SelectedIndexChanged" >
                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="31">Checkbox</asp:ListItem> <%-- MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22  --%>
                        <asp:ListItem Value="10">Claim Number Lookup</asp:ListItem>
                        <asp:ListItem Value="6">Code</asp:ListItem>
                        <asp:ListItem Value="2">Currency</asp:ListItem>
                        <asp:ListItem Value="3">Date</asp:ListItem>
                        <asp:ListItem Value="8">Entity</asp:ListItem>
                        <asp:ListItem Value="12">Event Number Lookup</asp:ListItem>
                      
                        <asp:ListItem Value="11">Free Text</asp:ListItem>
                        <asp:ListItem Value="17">Grid</asp:ListItem>						
						<asp:ListItem Value="25">HTML Text</asp:ListItem>
                        <asp:ListItem Value="23">Hyperlink</asp:ListItem><%--WWIG GAP20A - agupta298 - MITS 36804 JIRA-4691--%>
                        <asp:ListItem Value="5">Multi Text/Codes</asp:ListItem>
                        <asp:ListItem Value="14">Multi-Code</asp:ListItem>
                        <asp:ListItem Value="16">Multi-Entity</asp:ListItem>
                        <asp:ListItem Value="15">Multi-State</asp:ListItem>
                        <asp:ListItem Value="1">Number</asp:ListItem>
                        <%-- Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373--%>
                        <%--Rename the name of Policy Management Number Lookup to Policy Management Lookup,05/05/2010,MITS 20618--%>
                        <%--<asp:ListItem Value="19">Policy Management Number Lookup</asp:ListItem>--%>
                        <asp:ListItem Value="19">Policy Management Lookup</asp:ListItem>
                       
                        <%--Rename the name of Policy Number Lookup to Policy Tracking Lookup,05/05/2010,MITS 20618 --%>
                        <%--<asp:ListItem Value="18">Policy Number Lookup</asp:ListItem>--%>
                        <asp:ListItem Value="18">Policy Tracking Lookup</asp:ListItem>
                       
                        <%--End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373--%>
                        <asp:ListItem Value="9">State</asp:ListItem>
                        <asp:ListItem Value="0">String</asp:ListItem>
                        <asp:ListItem Value="4">Time</asp:ListItem>
                        <asp:ListItem Value="22">User Lookup</asp:ListItem><%--sharishkumar Jira 6415--%>
                        <asp:ListItem Value="13">Vehicle Lookup</asp:ListItem>
                        
                        
                    </asp:DropDownList>
                   </td>
                  </tr>
                  <tr>
                   <td>Deleted:&nbsp;&nbsp;</td>
                   
                   <td>
                    <input type="checkbox" name="88" value="True" appearance="full" runat="server" id="DeleteFlag" />
                    <input type="text" name="88" value="" style="display:none" cancelledvalue="" id="DeleteFlag_mirror" /></td>
                   
                  </tr> 
                  <tr>
                   <td><u>User Prompt</u>:&nbsp;&nbsp;
                   </td>
                   <td>
                     <div title="" style="padding: 0px; margin: 0px"><input type="text" name="94" value="" size="30" id="UserPrompt" runat="server" onkeypress="checkComma()" onpast onchange="&#xA;;setDataChanged(true);&#xA;" /></div>
                   </td>
                  </tr> 
                  <tr>
                   <td><u>System Field Name</u>:&nbsp;&nbsp;
                   </td>
                   <td>
                    <div title="" style="padding: 0px; margin: 0px"><input type="text" name="100" value="" size="30" id="SysFieldName" runat="server" onchange="&#xA;;setDataChanged(true);&#xA;" /></div>
                   </td>
                 </tr>
                 <tr id="CodeFileTr" runat="server">
                  <td id="filetype" runat="server">Code File:  </td>
                  <td><select name="112" id="CodeFileId" runat="server" onchange="setDataChanged(true);" ></select></td>
                 </tr>
                 
                  <%-- sharishkumar Jira 6415 starts--%>

                  <tr id="MultiSelectTr" runat="server">
                  <td>MultiSelect:&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chkBoxMultiSelect" value=""  id="MultiSelect" runat="server"  />
                  </td>
                  </tr>

                  <tr id="UserlookupTr" runat="server">
                  <td id="UsersGroups" runat="server">User/Groups:&nbsp;&nbsp;</td>
                  <td><asp:DropDownList  id="ddUsersGroups" runat="server">
                        <asp:ListItem Value="0">Users</asp:ListItem>
                        <asp:ListItem Value="1">Groups</asp:ListItem>                        
                </asp:DropDownList>
                </td>
                 </tr>
                   <%--sharishkumar Jira 6415 ends--%>
                   <tr id="trHtmltextconfig" runat="server" visible="false">
                       <td>Configure HTML Text</td>
                       <td>
                           <input type="checkbox" name="chkhtmltextconfig" value="" id="chkhtmltextconfig" runat="server" /></td>
                   </tr>
                 <tr>
                  <td colspan="2">
                   <asp:Button name="formbutton"  Text="Modify Grid" id="ModifyGrid" class="button" runat="server" OnClientClick="return OpenGridProperty();"  /></td>
                 </tr>
                 
                 <tr>
                  <td>Required Field:&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chkBoxReqField" value="True"  id="RequiredFlag" runat="server"  />
                  </td>
                </tr>
                <tr id="FASReportable" runat="server">
                    <td>FAS Reportable:&nbsp;&nbsp;</td>
                    <td>
                        <input type="checkbox" name="chkFASReportable" value="True" id="FASReportableflag" runat="server" />
                    </td>
                </tr>
                <tr id="lookUpFldTr" runat="server" >
                  <td>Lookup Field:&nbsp;&nbsp;</td>
                  <td> 
                  <input type="checkbox" name="124" value="True" id="LookupFlag" runat="server" onchange="ApplyBool(this);" />
                  <input type="text" name="124" value="" style="display:none" id="LookupFlag_mirror" />
                  </td>
                </tr>
                <tr>
                  <td>Input Format:&nbsp;&nbsp;</td>
                  <td></td>
                </tr>
                <tr>
                 <td colspan="2">
                  <input type="radio" name="rdoPattern" value="0" onclick="TableField_IsPaternClick();" runat="server" id="freeform" />Free-Form</td>
                </tr>
                <tr>
                  <td><input type="text" name="" value="" runat="server" style="display:none" id="IsPattern" /></td>
                </tr>
                <tr>
                <td colspan="2"><input type="radio" name="rdoPattern" value="-1" onclick="TableField_IsPaternClick();" runat="server" id="patterned" />Patterned
                <asp:DropDownList  id="Pattern" runat="server" onchange="TableField_SetFieldSize();">
                <asp:ListItem Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="(###) ###-####   Ext. #####">(###) ###-####   Ext. #####</asp:ListItem>
                        <asp:ListItem Value="(###) ###-####">(###) ###-####</asp:ListItem>
                        <asp:ListItem Value="###-##-####">###-##-####</asp:ListItem>
                        <asp:ListItem Value="#####-####">#####-####</asp:ListItem>
                </asp:DropDownList>
                </td>
                </tr>
                <!-- abansal23 : MITS 14287 -->
                <tr runat="server" id="fldsize">       
                  <td><asp:Label ID="lblFieldSize" runat="server" Text="Field Size:"></asp:Label>&nbsp;&nbsp;</td>
                  <td><input type="text" name="175" value="" size="30" onblur="numLostFocus(this);" runat="server" id="FieldSize" onchange="setDataChanged(true);" /></td>
                </tr>
                <tr id="GrpAssocTR" runat="server">
                <td colspan="2">
                 <table width="100%" align="left">
                 <tr>
                  <td class="ctrlgroup" width="100%" id="GrpAssocia">Group Association</td>
                 </tr>
                 </table>
                </td>
               </tr>
             <tr id="GrpAssocTR2" runat="server">
                 <td>Source Field:&nbsp;&nbsp;</td>
                 <td>
                 <asp:DropDownList name="193" id="SourceField" runat="server" 
                         onchange="TableField_SourceFieldChange();" 
                         onselectedindexchanged="SourceField_SelectedIndexChanged" 
                         AutoPostBack="True">
                 
                 </asp:DropDownList></td>
               </tr> 
            <tr id="GrpAssocTR3" runat="server" >
                <td>Codes/Entities:&nbsp;&nbsp;</td>
                <td>
                <div id="GAListbtn" runat="server" >
                    <uc1:MultiCodeLookup ID="MultiCodeLookup1"  runat="server" />
                </div> 
                <div id="OrgControls" runat="server" visible="false">
                <select name="GAList"  multiple id="GAList" size="3" runat="server" ></select>
                <input type="button" class="button" id="GAListbtn2"  runat="server" onclick="getOrglookup('orgh','GAList','Codes/Entities')" value="..." />
                <input type="button" class="button" id="GAListbtndel" onclick="deleteSelCode('GAList')" value="-" />
                </div>
                <div id="entLookup" runat="server" visible="false">
                  <input type="text" runat="server" value="" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="GAEntLookup" cancelledvalue="">
                  <input runat="server" type="button" class="button" value="..." id="GAEntLookupbtn" onclick="lookupData('GAEntLookup','',4,'GAEntLookup',2)">
                </div>
                <input type="text" name="198" value="" style="display:none" id="GAList_lst" runat="server" />                
                <input type="text" value="" style="display:none" id="GAEntLookup_cid" runat="server" />
                </td>
              </tr>
              <tr>
          <td></td>
          <td><input type="text" name="257" value="" style="display:none" id="GAList_cid" runat="server" />
          <input type="text" name="205" value="" style="display:none" id="GAListHidden" runat="server" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="211" value="" style="display:none" id="ListType" runat="server" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="216" value="" style="display:none" id="JuridictionHidden" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="222" value="" style="display:none" runat="server" id="bPatterned" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="229" value="" style="display:none" id="AssoField" runat="server" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="234" value="" style="display:none" runat="server" id="GrpAssoFlag" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="239" value="" style="display:none" id="SeqNo" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="244" value="" style="display:none" runat="server" id="SuppTblName" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="249" value="" style="display:none" runat="server" id="HelpContext" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="254" value="" style="display:none" runat="server" id="hdnCodeFile" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="259" value="" style="display:none" id="GroupAssoList" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="264" value="" style="display:none" runat="server" id="ControlChanged" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="269" value="" style="display:none" runat="server" id="Columns" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="274" value="" style="display:none" runat="server" id="ColCount" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="279" value="" style="display:none" id="GridXML" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="284" value="" style="display:none" id="GridProperty" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="290" value="" style="display:none" id="FormMode" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="295" value="" style="display:none" id="hdnFieldType" runat="server" /></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="hdnGridXml" value="" style="display:none" id="hdnGridXml" runat="server" /></td>
         </tr>
          <tr>
          <td></td>
          <td><input type="text" name="hdnColCnt" value="" style="display:none" id="hdnColCnt" runat="server" /></td>
         </tr>
         <%--rkaur27 : RMA-18764 - Start--%>
          <tr>
          <td></td>
          <td><input type="text" name="hdnDttmLastUpdtd" value="" style="display:none" id="hdnDttmLastUpdtd" runat="server" /></td>
          </tr>
          <%--rkaur27 : RMA-18764 - End--%>
         <tr>
          <td></td>
          <td><input type="text" name="hdnColName" value="" style="display:none" id="hdnColName" runat="server" /></td>
                                    <td>
                                        
                                        <input type="text" name="hdnOrgLevel" value="" style="display: none" id="hdnorgLevel"
                                            runat="server" />
                                    </td>
         </tr>
         <tr><td> <%--Mgaba2: R7: Added Element to check whether History Tracking is On--%> <input type="text"  value="" style="display:none" id="hdnHistTrack" runat="server" /></td>
         </tr>
              </tbody>
             </table> 
           
           </td>
         </tr>
      </tbody>
     </table>  
       
          <%--rsushilaggar MITS 31339 Date 02/15/2013--%> 
     <input type="text" name="" value="" id="SysViewType" style="display:none"/>
     <input type="text" name="" value="" id="SysCmd" style="display:none"/>
     <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
     <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/>
     <input type="text" name="" id="SysCmdText" style="display:none" value="Navigate"/>
     <input type="text" name="" id="SysClassName" style="display:none" value=""/>
     <input type="text" name="" id="SysSerializationConfig" style="display:none"/>
     <input type="text" name="" id="SysFormIdName" style="display:none" value="RowId"/>
     <input type="text" name="" id="SysFormPIdName" style="display:none" value="RowId"/>
     <input type="text" name="" id="SysFormPForm" style="display:none" value="TableField"/>
     <input type="text" name="" id="SysInvisible" style="display:none" value="" />
     <input type="text" name="" id="SysFormName" style="display:none" value="TableField" />
     <input type="text" name="" value="FieldType|UserPrompt|SysFieldName|" id="SysRequired" style="display:none" />
     <input type="text" name="$node^8" style="display:none" id="hdnAction" value=""/>
     <input type="text" id="hdnXmlFormName" runat="server" value="" style="display:none" />
    
    
    </div>
    </form>
</body>
</html>
