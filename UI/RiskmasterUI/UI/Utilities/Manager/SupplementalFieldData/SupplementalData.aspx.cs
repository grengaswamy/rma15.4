﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Utilities.Manager.SupplementalFieldData;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class SupplementalData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindToGridView();

        }

        private void BindToGridView()
        {
            string orbeonSessionId = string.Empty;

            DataSet suppRecordsSet = null;

            suppRecordsSet = GetSupplementalFieldData(orbeonSessionId);

            try
            {
                GridView1.DataSource = suppRecordsSet.Tables[1];
                GridView1.DataBind();

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private DataSet GetSupplementalFieldData(string orbeonSessionId)
        {
            string supplementalServiceOutput = string.Empty;
            XmlDocument supplementalDoc = null;
            XmlNode retSupplementalNode = null;
            DataSet dSet = null;
            SupplementalHelper  suppHelper = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                suppHelper = new SupplementalHelper();

                inputDocNode = InputDocForSupplementalField();

                serviceMethodToCall = "SupplementalDataAdaptor.Get";
                supplementalServiceOutput = suppHelper.CallCWSFunctionForSupplemental(orbeonSessionId, inputDocNode, serviceMethodToCall);

                supplementalDoc = new XmlDocument();
                supplementalDoc.LoadXml(supplementalServiceOutput);
                ErrorControl1.errorDom = supplementalServiceOutput;
                retSupplementalNode = supplementalDoc.SelectSingleNode("ResultMessage/Document/SupplementalList/listrow");
                if (retSupplementalNode != null)
                {
                    dSet = new DataSet();
                    dSet.ReadXml(new XmlNodeReader(supplementalDoc.SelectSingleNode("ResultMessage/Document/SupplementalList")));
                }
                

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {

            }

            return dSet;
        }

        private XmlNode InputDocForSupplementalField()
        {
         
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
             <SupplementalList>
                <listhead>
                    <UserTableName>Table Name</UserTableName> 
                    <SysTableName>System Table Name</SysTableName>
                    <RowId>RowId</RowId> 
                </listhead>
            </SupplementalList>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }
            
           
            return xmlNodeDoc;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectSupplemental('" + DataBinder.Eval(e.Row.DataItem, "RowId").ToString() + "')";
               
                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }

            
        }

    }
}
