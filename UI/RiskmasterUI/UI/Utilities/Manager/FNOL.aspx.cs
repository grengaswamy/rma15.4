﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class FNOL : NonFDMBasePageCWS
    {
        #region "Constants"

        internal const string ReserveType = "RESERVE_TYPE";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtGridData = new DataTable();

                DataRow objRow;
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("FNOL.aspx"), "FNOLValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "FNOLValidations", sValidationResources, true);
                if (!IsPostBack)
                {
                    hdnRowIDValue.Value = AppHelper.GetQueryStringValue("RowID");
                    GetReserveType();
                    lblReservepage.Text = "Reserve";



                }
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void GetReserveType()
        {
            try
            {
                XElement XmlTemplate = null;
                DataSet oDS = new DataSet();
                string sPolCode = "";
                string Data = "";
                bool bReturnStatus = false;
                XmlTemplate = GetMessageTemplateForCodeType();

                //txtPolicyClaimLOB.Text = lstPolicyClaimLOB.SelectedValue;
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.GetCodeType", XmlTemplate, out Data, false, true);
                if (Data != null && !string.IsNullOrEmpty(Data))
                {
                    oDS.ReadXml(new StringReader(Data));
                    ListItem item = null;
                    //spahariya MITS 30911 - start
                    item = new ListItem();
                    item = new ListItem();
                    item.Text = "";
                    item.Value = "";
                    lstReserveType.Items.Add(item);
                    //spahariya -End
                    for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                    {
                        item = new ListItem();
                        item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                        item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();

                        switch (oDS.Tables["option"].Rows[i]["table_name"].ToString().ToUpper())
                        {
                            case ReserveType:
                                lstReserveType.Items.Add(item);
                                break;
                        }
                    }
                }

                //Grid Binding
                BindGridData();
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplateForCodeType()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;
            string sPolSystemID = string.Empty;
            string sPolSysTypeID = string.Empty;
            //GetPolicySystemSelectedValue(ref sPolSystemID, ref sPolSysTypeID);
            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>LossCodeMappingAdaptor.GetCodeType</Function></Call>");
            sXML.Append("<Document><SystemTableName>");
            sXML.Append("'" + ReserveType + "'");
            sXML.Append("</SystemTableName></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        protected void addReserveBtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateForAdd();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.AddReserveNewMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }

                    else
                    {
                        BindGridData();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplateForAdd()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("LossCodeMappingAdaptor.AddNewMapping");
            sXml = sXml.Append("</Function></Call><Document><LossCodeMappingAdaptor><AddCode>");
            sXml = sXml.Append("<CoverageLossLOBID>" + hdnRowIDValue.Value + "</CoverageLossLOBID>");
            sXml = sXml.Append("<ReserveType>" + lstReserveType.SelectedValue + "</ReserveType>");
            sXml = sXml.Append("<ReserveAmount>" + txtReserveAmnt.Amount + "</ReserveAmount>");

            sXml = sXml.Append("</AddCode></LossCodeMappingAdaptor></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void BindGridData()
        {
            try
            {
                string data = string.Empty;
                DataTable dtGridData = new DataTable();
                DataRow objRow = null;
                XElement XmlTemplate = null;
                bool bReturnStatus = false;

                XmlTemplate = GetMessageTemplateForAllMappings();
                bReturnStatus = CallCWS("LossCodeMappingAdaptor.GetReserveMapping", XmlTemplate, out data, false, true);
                dtGridData.Columns.Add("ReserveType");
                dtGridData.Columns.Add("ReserveAmount");
                if (bReturnStatus)
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(data);
                    string sReserveType = string.Empty;
                    string sReserveAmnt = string.Empty;
                    XmlNodeList xmlNodeList = xmldoc.SelectNodes("//ReserveList/Reserve");
                    if (xmlNodeList.Count > 0)
                    {
                        foreach (XmlNode objNodes in xmlNodeList)
                        {
                            objRow = dtGridData.NewRow();
                            sReserveType = objNodes.Attributes["ReserveType"].Value.ToString(); //spahariya MITS 30911
                            sReserveAmnt = objNodes.Attributes["ReserveAmount"].Value.ToString();//spahariya MITS 30911
                            objRow["ReserveType"] = sReserveType;
                            objRow["ReserveAmount"] = sReserveAmnt;
                            dtGridData.Rows.Add(objRow);
                        }
                        GridViewReserveStatus.DataSource = dtGridData;
                        GridViewReserveStatus.DataBind();
                    }
                }
                if (dtGridData.Rows.Count <= 0)
                {
                    objRow = dtGridData.NewRow();

                    ////spahariya MITS 30911
                    objRow["ReserveType"] = "";
                    objRow["ReserveAmount"] = "";

                    dtGridData.Rows.Add(objRow);
                    GridViewReserveStatus.DataSource = dtGridData;
                    GridViewReserveStatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplateForAllMappings()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("LossCodeMappingAdaptor.GetReserveMapping");
            sXml = sXml.Append("</Function></Call><Document><LossCodeMappingAdaptor>");
            sXml = sXml.Append("<AddCode><CoverageLossID>" + hdnRowIDValue.Value + "</CoverageLossID></AddCode>");
            //sXml = sXml.Append("<CvgTypeCode>" + lstCoverageType.SelectedValue + "</CvgTypeCode>");
            //sXml = sXml.Append("<LossCode>" + lstLossOfCode.SelectedValue + "</LossCode>");
            sXml = sXml.Append("</LossCodeMappingAdaptor></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }




    }
}
