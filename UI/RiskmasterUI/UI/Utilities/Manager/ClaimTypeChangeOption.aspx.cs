﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Text;
using Riskmaster.UI.FDM;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class ClaimTypeChangeOption : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("ClaimTypeChangeOptionAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);

                    }
                }
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("ClaimTypeChangeOptionAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate()
        {
            TextBox txtDataRunTimeCodeId=null;
            TextBox txtDataRunTimeCode=null;
            string strCodeId=string.Empty;
            string strCode=string.Empty;
            string strRowId = string.Empty;
            if (mode.Text.ToLower() == "edit")
            {
                strRowId = AppHelper.GetQueryStringValue("selectedid");
            }
            else
            {
                strRowId = RowId.Text;
            }

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><ClaimTypeChangeOption>");
            sXml = sXml.Append("<control name='RowId'>");
            sXml = sXml.Append(strRowId);
            sXml = sXml.Append("</control>");

            txtDataRunTimeCodeId = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "Lob_codelookup_cid");
            if(txtDataRunTimeCodeId!=null)
                strCodeId = txtDataRunTimeCodeId.Text;
            txtDataRunTimeCode = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "Lob_codelookup");
            if(txtDataRunTimeCode!=null)
                strCode = txtDataRunTimeCode.Text.Replace("&", "&amp;");   //averma62 MITS - 28145

            sXml = sXml.Append("<control name='Lob' type='code' codeid='" + strCodeId + "' codetable='LINE_OF_BUSINESS'>");
            sXml = sXml.Append(strCode);
            sXml = sXml.Append("</control>");

            txtDataRunTimeCodeId = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "CurClaimType_codelookup_cid");
            if(txtDataRunTimeCodeId!=null)
                strCodeId = txtDataRunTimeCodeId.Text;
            txtDataRunTimeCode = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "CurClaimType_codelookup");
            if(txtDataRunTimeCode!=null)
                strCode = txtDataRunTimeCode.Text.Replace("&", "&amp;"); //averma62 MITS - 28145

            sXml = sXml.Append("<control name='CurClaimType' type='code' codeid='" + strCodeId + "' codetable='CLAIM_TYPE'>");
            sXml = sXml.Append(strCode);
            sXml = sXml.Append("</control>");

            txtDataRunTimeCodeId = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "NewClaimType_codelookup_cid");
            if(txtDataRunTimeCodeId!=null)
                strCodeId = txtDataRunTimeCodeId.Text;
            txtDataRunTimeCode = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "NewClaimType_codelookup");
            if(txtDataRunTimeCode!=null)
                strCode = txtDataRunTimeCode.Text.Replace("&", "&amp;"); //averma62 MITS - 28145

            sXml = sXml.Append("<control name='NewClaimType' type='code' codeid='" + strCodeId + "' codetable='CLAIM_TYPE'>");
            sXml = sXml.Append(strCode);
            sXml = sXml.Append("</control>");

            txtDataRunTimeCodeId = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "TrigType_codelookup_cid");
            if(txtDataRunTimeCodeId!=null)
                strCodeId = txtDataRunTimeCodeId.Text;
            txtDataRunTimeCode = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "TrigType_codelookup");
            if(txtDataRunTimeCode!=null)
                strCode = txtDataRunTimeCode.Text.Replace("&", "&amp;"); //averma62 MITS - 28145

            sXml = sXml.Append("<control name='TrigType' type='code' codeid='" + strCodeId + "' codetable='TRANS_TYPES'>");
            sXml = sXml.Append(strCode);
            sXml = sXml.Append("</control>");
            //

            txtDataRunTimeCodeId = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "IncSchChecks_codelookup_cid");
            if (txtDataRunTimeCodeId != null)
            {
                strCodeId = txtDataRunTimeCodeId.Text;
            }
            txtDataRunTimeCode = (TextBox)GridPopupBase.FindControlRecursive(this.Form, "IncSchChecks_codelookup");
            if (txtDataRunTimeCode != null)
            {
                strCode = txtDataRunTimeCode.Text.Replace("&", "&amp;");
            }
            sXml = sXml.Append("<control name='IncSchChecks' type='code' codeid='" + strCodeId + "' codetable='PSO_YES_NO'>");
            sXml = sXml.Append(strCode);
            sXml = sXml.Append("</control>");

            sXml = sXml.Append("</ClaimTypeChangeOption></Document>");
            sXml = sXml.Append("</Message>");

            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

    }
}
