﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentParameterSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.PaymentParameterSetup" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix ="uc1" TagName="UserControlGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%--npadhy JIRA 6418 Removed the drop down for Printing options replaced it with Mapping option. And changed the corresponding Tabindex for all the controls
    Also added the Codelookup for Default Distribnution Type--%>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="usc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payment Parameter Setup</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="javascript" type="text/javascript" src="../../../Scripts/checkoptions.js"></script>
     <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
     <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
</head>
<body class="10pt" onload="loadTabList();parent.MDIScreenLoaded();DisableEnableSources();DisableResSupChain();DisablePaySupChain();">
  <form id="frmData" name="frmData" runat="server">
   <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <input type="hidden" name="hTabName"/>
  <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
    <table border="0"  cellpadding="0" cellspacing="0">
     <tr>
  
         <asp:TextBox ID="hdnValue" style="display:none" rmxref="/Instance/Document/form/DupNoOfDays" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnDuplicatePaymentCheck" style="display:none" rmxref="/Instance/Document/form/DuplicatePaymentCheck" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnPaymentNotifyValue" style="display:none" rmxref="/Instance/Document/form/PaymentNotifyValues" runat="server"></asp:TextBox>
         <asp:TextBox ID="txtScheduleNotify" style="display:none" rmxref="/Instance/Document/form/ScheduleNotifyValues" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnCriteria" style="display:none" rmxref="/Instance/Document/form/SetCriteria" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnAction" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnTransValues" style="display:none" rmxref="/Instance/Document/form/TransValues" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNewLOB" style="display:none" rmxref="/Instance/Document/form/control[@name='NewLOB']" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNewUserToNotify" rmxref="/Instance/Document/form/control[@name='NewUserToNotify']" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNewNotifyPeriod" rmxref="/Instance/Document/form/control[@name='NewNotifyPeriod']" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNewDateCriteria" rmxref="/Instance/Document/form/control[@name='NewDateCriteria']" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="UserNotify" rmxref="/Instance/Document/form/UserNotify" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnLOB" rmxref="/Instance/Document/form/LOB" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNotifyUsers" rmxref="/Instance/Document/form/NotifyUsers" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNotifyPeriod" rmxref="/Instance/Document/form/NotifyPeriod" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnDateCriteria" rmxref="/Instance/Document/form/DateCriteria" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hidden_DataChanged" rmxref="/Instance/Document/form/DataChanged" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnRecordId"  style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnAddNotifyUsers" rmxref="/Instance/Document/form/AddNotifyUsers" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnPayment" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnDeletedValues" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNewScheduleDate" rmxref="/Instance/Document/form/control[@name='Date']" style="display:none" runat="server"></asp:TextBox>
         <asp:TextBox ID="hdnNewScheduleDescrip" rmxref="/Instance/Document/form/control[@name='Description']" style="display:none" runat="server"></asp:TextBox>
          <asp:TextBox ID="hdnScheduleId"  style="display:none" runat="server"></asp:TextBox>
         
   
      <td align="center" valign="middle" HEIGHT="32">
      <asp:ImageButton id="Save" OnClientClick="SelectAll();" 
              onMouseOver="this.src='../../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../../Images/save.gif';this.style.zoom='100%'" ImageUrl="~/Images/save.gif" class="bold" ToolTip="Save"  runat="server" 
              onclick="Save_Click" TabIndex="31"  />
      </td>
     </tr>
    </table>
   </div>
   <br/>
   <div class="msgheader" id="formtitle">Payment Parameter Setup</div>
   <div class="errtextheader"></div>
   <table border="0" >
    <tr>
     <td  valign="top">
      <table border="0" cellspacing="" cellpadding="" style="height:5%;width:99.5%">  <!-- igupta3 Mits: 33301 -->
       <tr>
        <td >
        <div class="tabGroup" id="TabsDivGroup" runat="server">
                    <div class="Selected" nowrap="true" runat="server" name="TABSCheckOptions" id="TABSCheckOptions">
                        <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="CheckOptions" id="LINKTABSCheckOptions">Check Options Setup</a>
                        
                    </div>
                    <div class="tabSpace" runat="server" id="TBSCheckOptions">
                        <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSSupervisorApproval" id="TABSSupervisorApproval">
                        <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="SupervisorApproval" id="LINKTABSSupervisorApproval">Supervisor Approval Configuration</a>
                        
                    </div>
                    <div class="tabSpace" runat="server" id="TBSSupervisorApproval">
                         <nbsp />
                        <nbsp />
                    </div>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSPaymentNotifySetup" id="TABSPaymentNotifySetup">
                        <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="PaymentNotifySetup" id="LINKTABSPaymentNotifySetup">Payment Notification Setup</a>
                       
                    </div>
                    <div class="tabSpace" runat="server" id="TBSPaymentNotifySetup">
                         <nbsp />
                        <nbsp />
                    </div>
                    <!--Start-----Added BY Ashutosh Enhancement for R8-->
                     <!--Start-----A NewTab is required for Scheduling Date and Holidays-->
                      <div class="NotSelected" nowrap="true" runat="server" name="TABSScheduleDateSetup" id="TABSScheduleDateSetup">
                        <a class="NotSelected1" HREF="#"  onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="ScheduleDateSetup" id="LINKTABSScheduleDateSetup">Schedule Date</a>
                        
                    </div>
                    <div class="tabSpace" runat="server" id="TBSScheduleDateSetup">
                         <nbsp />
                        <nbsp />
                    </div>
                   <!--ENd-->
        </div>
       </td>
       </tr>
      </table>
      <div class="singletopborder" style="position:relative;left:0;top:0;height:100%;width:98%;overflow:auto">  <!-- igupta3 Mits: 33301 -->
       <table border="0" cellspacing="0" cellpadding="0" >
        <tr>
         <td>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABCheckOptions" id="FORMTABCheckOptions">
           <tr id="">
            <td>Roll Up Checks to the same payee:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox type="checkbox" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="1" rmxref="/Instance/Document/form/control[@name='RollUpChecks']" ID="RollUpChecks" value="True" runat="server" />
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Print EOBs With Checks:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PrintEOBChecks" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="16" rmxref="/Instance/Document/form/control[@name='PrintEOBChecks']" value="True" type="checkbox" runat="server" />
           </td>
           </tr>
        
           <tr id="">
            <td>Allow Post Date of Checks:&nbsp;&nbsp;</td>
            <td>
             
                 <asp:CheckBox ID="AllowPostChecks" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="2" rmxref="/Instance/Document/form/control[@name='AllowPostChecks']" value="True" type="checkbox" runat="server" />
            </td>
            </td>
            <%-- <td>&nbsp;&nbsp;</td> --%>
            <td>&nbsp;&nbsp;</td>
            <td>Print EOB Detail On Check Stub:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PrintEOBStub" onclick="setDataChanged(true);DisableRollup('0')" tabindex="17" rmxref="/Instance/Document/form/control[@name='PrintEOBStub']" value="True" type="checkbox" runat="server" />
            </td>
           </tr>
           <tr>
           <td> Print Direct To Printer:&nbsp;&nbsp;</td>
           <td>
               <asp:CheckBox ID="DirectToPrinter" onclick="setDataChanged(true);document.getElementById('chkLocalPrinter').checked=false;DisableOnClick(this);DisableEnableSources();" tabindex="3" rmxref="/Instance/Document/form/control[@name='DirectToPrinter']" value="True" type="checkbox" runat="server" />
           </td>
             <td>&nbsp;&nbsp;</td>
            <td>Following Printers are attached with the Server:&nbsp;&nbsp;</td>
            <td>
                
                <asp:DropDownList ID="ddlPrinters" onchange="SetValue();" runat="server" rmxref="/Instance/Document/form/PrinterName" AutoPostBack='true' onselectedindexchanged="ddlPrinters_SelectedIndexChanged" TabIndex="18" >
              </asp:DropDownList>
           </td>
           </tr>
            <tr>
           <td> Print To Local Printer:&nbsp;&nbsp;</td>
           <td>
               <asp:CheckBox ID="chkLocalPrinter" onclick="setDataChanged(true);document.getElementById('DirectToPrinter').checked=false;DisableOnClick(this);DisableEnableSources();" tabindex="4" rmxref="/Instance/Document/form/control[@name='LocalPrinter']" value="True" type="checkbox" runat="server" />
           </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td>
           </td>
           </tr>
           <tr id="">
           <%--MGaba2:MITS 18929:Minimum value should be 1--%>
            <td>Max # of Auto Checks per Batch:&nbsp;&nbsp;</td>
           
            <td>
             <asp:TextBox ID="MaxAutoChecksBatch" rmxref="/Instance/Document/form/control[@name='MaxAutoChecksBatch']" MaxLength="5" tabindex="5" onchange="setDataChanged(true);" onblur="numLostFocus(this);fnAcceptMinimumValue(this,'1');" type="text" value="50" size="5" runat="server" ></asp:TextBox>  
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Please Choose Paper Bin For Checks:&nbsp;&nbsp;</td>
            <td>
               <asp:DropDownList ID="ddlPbfc" rmxref="/Instance/Document/form/PaperBinForChecks" runat="server" TabIndex="19" >
        </asp:DropDownList>
           </td>
           </tr>
           <tr id="">
            <td>Print Check Stub Detail when printing individual EOBs:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PrntCheckStubEOB" onclick="DisableOnClick(this);setDataChanged(true);" tabindex="6" rmxref="/Instance/Document/form/control[@name='PrntCheckStubEOB']" value="True" type="checkbox" runat="server" /> 
            </td>
            <td>&nbsp;&nbsp;</td>
             <td>Please Choose Paper Bin For EOBs:&nbsp;&nbsp;</td>
            <td>
              
               <asp:DropDownList ID="ddlpbfeob" rmxref="/Instance/Document/form/PaperBinForEobs" runat="server" TabIndex="20" >
        </asp:DropDownList>
             </td>
           </tr>
           
           <tr id="">
           
            <%--<td>Print Check To File Options:&nbsp;&nbsp;</td>
            <td>
                
                <asp:DropDownList ID="PrntChecktoFile" type="combobox" rmxref="/Instance/Document/form/control[@name='PrntChecktoFile']" tabindex="5" onchange="setDataChanged(true);" runat="server">
                <asp:ListItem Value="0">To Printer Only</asp:ListItem>
                <asp:ListItem Value="1">To File Only</asp:ListItem>
                <asp:ListItem Value="2">To Printer and File</asp:ListItem>
                <asp:ListItem Value="3">To Custom File Only</asp:ListItem>  -MITS 30623 mcapps2 12/07/2012
                <asp:ListItem Value="4">To Printer and Custom File Only</asp:ListItem> MITS 30623 mcapps2 12/07/2012
                </asp:DropDownList>
           </td>--%>
            <td>Print Additional Details of a Check Batch in a Separate Report:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PrntAddlCheckDetailsSeparate" onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/form/control[@name='PrntAddlCheckDetailsSeparate']" tabindex="7" value="True" type="checkbox" runat="server" />
           </td>
           </tr>
            <tr>
           <td> Print Claimant EOB:&nbsp;&nbsp;</td>
           <td>
               <asp:CheckBox ID="PrintClmtEOB" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="8" rmxref="/Instance/Document/form/control[@name='PrintClmtEOB']" value="True" type="checkbox" runat="server" />
           </td>
             <td>&nbsp;&nbsp;</td>
            <td>Print Payee EOB:&nbsp;&nbsp;</td>
            <td>
                
            <asp:CheckBox ID="PrintPayeeEOB" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="21" rmxref="/Instance/Document/form/control[@name='PrintPayeeEOB']" value="True" type="checkbox" runat="server" />
              
           </td>
           </tr>
           <tr id="">
            <td>Allow Duplicate Payment Checking:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="DupPayCheck" onclick="DisableDuplicatePayment();setDataChanged(true);DisableOnClick(this);" rmxref="/Instance/Document/form/control[@name='DupPayCheck']" tabindex="9" value="True" type="checkbox" runat="server" />
           </td>
            <td>&nbsp;&nbsp;</td>
            <td>Prohibit Saving of Duplicate Checks:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="ProhibitDupSaveCheck" onclick="DisableOnClick(this);setDataChanged(true);" rmxref="/Instance/Document/form/control[@name='ProhibitDupSaveCheck']" tabindex="22" value="True" type="checkbox" runat="server" />
            </td>
           </tr>
           <tr id="">
            <td>Duplicate Criteria:&nbsp;&nbsp;</td>
            <td colspan="5">
                <asp:DropDownList ID="DupCriteria" rmxref="/Instance/Document/form/control[@name='DupCriteria']/@value" ItemSetRef="/Instance/Document/form/control[@name='DupCriteria']" tabindex="10" onchange="OpenCriteria();setDataChanged(true);" runat="server">
                </asp:DropDownList>
           </td>
           </tr>
           <%--Debabrata Biswas MITS# 20050:Start- Date 04/22/2010--%>
           <tr id="">
           <td>Default Level to display on Precheck Register:</td>
            <td>
                <asp:DropDownList ID="PreCheckOrgLvl" type="combobox" rmxref="/Instance/Document/form/control[@name='PreCheckOrgLvl']" onchange="setDataChanged(true);" runat="server" TabIndex="11">
                <asp:ListItem Value="1012">None</asp:ListItem>
                <asp:ListItem Value="1005">Client</asp:ListItem>
                 <asp:ListItem Value="1006">Company</asp:ListItem>
                 <asp:ListItem Value="1007">Operation</asp:ListItem>
                 <asp:ListItem Value="1008">Region</asp:ListItem>
                 <asp:ListItem Value="1009">Division</asp:ListItem>
                 <asp:ListItem Value="1010">Location</asp:ListItem>
                 <asp:ListItem Value="1011">Facility</asp:ListItem>
                </asp:DropDownList>
            </td>
            <%--Debabrata Biswas MITS# 20050- End--%>
            <%--Debabrata Biswas MITS# 20050:Start- Date 04/26/2010--%>
            <td>&nbsp;&nbsp;</td>
            <td>LSS Payments on HOLD:&nbsp;&nbsp;</td>
            <td>
             
                 <asp:CheckBox ID="LSSPaymentsOnHOLD" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="23" rmxref="/Instance/Document/form/control[@name='LSSPaymentsOnHOLD']" value="True" type="checkbox" runat="server" />
            </td>
            <%--Debabrata Biswas MITS# 20050- End--%>
            <!--Start: Rahul Aggarwal: LSS Payee/multi data Exchange MITS 20073/20036 05/21/2010-->
            </tr>
            <tr id="">
            <td>Check Void Reason:&nbsp;&nbsp;</td>
             <td>
                <asp:CheckBox ID="EnableVoidReasonComment" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="12" rmxref="/Instance/Document/form/control[@name='EnableVoidReasonComment']" value="True" type="checkbox" runat="server" />
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Create/Update Entities Pushed from LSS:&nbsp;&nbsp;</td>
            <td>
                 <asp:CheckBox ID="CreateUpdateEntity" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="24" rmxref="/Instance/Document/form/control[@name='CreateUpdateEntity']" value="True" type="checkbox" runat="server" />
            </td>
            </tr>
            <!--End: Rahul Aggarwal: LSS Payee/multi data Exchange -->
            
            <tr id="">
         <%--   skhare7 R8 Enhancement--%>
            <td>Max Payees:&nbsp;&nbsp;</td>
             <td>
             <asp:TextBox ID="MaxPayees" 
                     rmxref="/Instance/Document/form/control[@name='MaxPayees']" 
                     MaxLength="5" tabindex="13" onchange="setDataChanged(true);" 
                     onblur="numLostFocus(this);fnAcceptMinimumValue(this,'1');" type="text" 
                     value="5" size="5" runat="server" ></asp:TextBox>  
            </td>
             <%--   skhare7 R8 Enhancement--%>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                 &nbsp;</td>
            </tr>
            <!--Ankit Start : Financial Enhancement - Prefix and Suffix Changes-->            
            <tr id="">
                <td>Include Prefix on Payments:&nbsp;&nbsp;</td>
                <td>
                    <asp:CheckBox ID="IncludePrefix" onclick="setDataChanged(true)" rmxref="/Instance/Document/form/control[@name='IncludePrefix']" value="True" type="checkbox" runat="server" TabIndex="14" />
                </td>
                <td>&nbsp;&nbsp;</td>
                <td>Include Suffix on Payments:&nbsp;&nbsp;</td>
                <td>
                     <asp:CheckBox ID="IncludeSuffix" onclick="setDataChanged(true)" rmxref="/Instance/Document/form/control[@name='IncludeSuffix']" value="True" type="checkbox" runat="server" TabIndex="25" />
                </td>
                   </tr>
             <tr id="">
             
                <td>Print EOB Desc in next line:&nbsp;&nbsp;</td>
                <td>
                <asp:CheckBox ID="PrintEOBDescNext" onclick="setDataChanged(true)" rmxref="/Instance/Document/form/control[@name='EOBDescNextLine']" value="True" type="checkbox" runat="server" TabIndex="15" />
                </td>
                 <td>&nbsp;&nbsp;</td>
                 <td>Default Distribution Type:&nbsp;&nbsp;</td>
                <td>
                     <usc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="DefaultDistributionType" CodeTable="DISTRIBUTION_TYPE" ControlName="DefaultDistributionType" RMXRef="/Instance/Document/form/control[@name ='DefaultDistributionType']" RMXType="code" TabIndex="26"/>
                </td>
              </tr>
         
            <!--Ankit End-->
           <%--sharishkumar jira 6418 starts--%>
            <tr id="Tr6">
            <td colspan="6">
             <table width="100%" align="left">
              <tr>
               <td class="ctrlgroup" width="100%">Print Output Settings</td>
              </tr>
             </table>
            </td>
           </tr>
           <tr id="">             
                <td >Distribution Type:&nbsp;&nbsp;</td>                
                <td>
                 <asp:ListBox   type="combobox" runat="server" 
                          ID="lstDistributionType" size="5" Height="100px" style="Width:200px"  Rows="6"
                          SelectionMode="Single"  rmxref="/Instance/Document/form/control[@name='lstDistributionType']/@value" ItemSetRef="/Instance/Document/form/control[@name='lstDistributionType']" TabIndex="26"></asp:ListBox>
                </td>
               <td>&nbsp;&nbsp;</td>
               <td>
                <asp:Button runat="server" ID="btnAddMapping" Text="Add Mapping" class="button" 
                          style="width:100px" Height="25px" OnClientClick="return AddPrintOption();" UseSubmitBehavior="False" TabIndex="28"/>
                <asp:Button runat="server" ID="btnDeleteMapping" Text="Delete Mapping" class="button" 
                          style="width:100px" Height="25px" OnClientClick="return RemovePrintOption();" UseSubmitBehavior="False" TabIndex="29"/>
                    <asp:TextBox runat ="server" rmxref="/Instance/Document/form/control[@name='txtMapping']"  ID="txtMapping" style="display:none"></asp:TextBox>  
                </td>                                   
              <td>
                 <asp:ListBox   type="combobox" runat="server" 
                          ID="lstPrintDistribution" size="5" Height="100px" style="Width:200px"  Rows="30"
                          SelectionMode="Single"  rmxref="/Instance/Document/form/control[@name='lstPrintDistribution']/@value" ItemSetRef="/Instance/Document/form/control[@name='lstPrintDistribution']"></asp:ListBox>
                </td>              
               </tr>
          <tr id="">           
            <td>Print Check To File Options:&nbsp;&nbsp;</td>
            <td>                
                <asp:DropDownList ID="ddlPrintOptions" type="combobox" rmxref="/Instance/Document/form/control[@name='ddlPrintOptions']" tabindex="27" onchange="setDataChanged(true);" runat="server">
                <asp:ListItem Value="0">To Printer Only</asp:ListItem>
                <asp:ListItem Value="1">To File Only</asp:ListItem>
                <asp:ListItem Value="2">To Printer and File</asp:ListItem>
                <asp:ListItem Value="3">To Custom File Only</asp:ListItem>  <%--MITS 30623 mcapps2 12/07/2012--%>
                <asp:ListItem Value="4">To Printer and Custom File Only</asp:ListItem> <%--MITS 30623 mcapps2 12/07/2012--%>
                </asp:DropDownList>
           </td>
           </tr>
           <%--sharishkumar jira 6418 ends--%>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABSupervisorApproval" id="FORMTABSupervisorApproval" style="display:none;">
           <tr id="">
            <td colspan="2">
            <asp:Label ID="lblSup" Text="Select the criteria to determine if a hold should be placed on a funds transaction." runat="server"></asp:Label>
            
            </td>
           </tr>
           <tr id="">
            <td>Payment Limits Are Exceeded:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PayLimitExceed" onclick="DisableOnClick(this);setDataChanged(true);" tabindex="12" rmxref="/Instance/Document/form/control[@name='PayLimitExceed']" value="True" type="checkbox" runat="server" />
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Specific Transaction Type Codes:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="SpecTranTypeCodes" onclick="DisableTrans();setDataChanged(true);DisableOnClick(this);" tabindex="19" value="True" rmxref="/Instance/Document/form/control[@name='SpecTranTypeCodes']" type="checkbox" runat="server" />
           </td>
           </tr>
           <tr id="">
            <td>Payment Detail Limits Are Exceeded:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PayDetailLimitExceed" onclick="DisableOnClick(this);setDataChanged(true);" tabindex="13" rmxref="/Instance/Document/form/control[@name='PayDetailLimitExceed']" value="True" type="checkbox" runat="server" />
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td >
               <uc:MultiCode runat="server" ID="TransCodes" tabindex="20"  CodeTable="TRANS_TYPES" ControlName="TransCodes"  RMXRef="/Instance/Document/form/control[@name='TransCodes']" RMXType="codelist"/>
            </td>
           </tr>
           <tr id="">
            <td>Payee does Not Exist in System:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PayNotExist" onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox" rmxref="/Instance/Document/form/control[@name='PayNotExist']" tabindex="14" value="True" runat="server" />
            </td>
           </tr>
           <tr id="Tr2">
            <td>Per Claim Pay Limits Exceeded:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PerClaimPayLimitsExceeded" onclick="DisableOnClick(this);setDataChanged(true);" type="checkbox" rmxref="/Instance/Document/form/control[@name='PerClaimPayLimitsExceeded']" tabindex="14" value="True" runat="server" />
            </td>
           </tr>
           <%--<tr id="">
            <td>Diary To Manager If Placed On Hold:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="DiaryToManager" onclick="DisableDiaryManager();setDataChanged(true);DisableOnClick(this);" value="True" rmxref="/Instance/Document/form/control[@name='DiaryToManager']" tabindex="15" type="checkbox" runat="server" />
           </td>
           </tr>--%>
           <tr id="">
            <td>Queue Payments Instead Of Placing On Hold:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="QueuePayment" onclick="DisableQueuePayment('0');setDataChanged(true);DisableOnClick(this);" rmxref="/Instance/Document/form/control[@name='QueuePayment']" tabindex="16" value="True" type="checkbox" runat="server" />
            </td>
           </tr>
           <%--<tr id="">
            <td>Use Supervisory Approval For Payments:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="UseSupAppPayments" rmxref="/Instance/Document/form/control[@name='UseSupAppPayments']" onclick="DisableApprovalOptions();setDataChanged(true);DisableOnClick(this);" tabindex="17" value="True" type="checkbox" runat="server" />
            </td>
           </tr>--%>
           <%--//Start: rsushilaggar 23-NOV-2011 MITS-26332--%>
           <tr id="">
            <td>Supervisory Approval:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="SupervisoryApproval" tabindex="20" rmxref="/Instance/Document/form/control[@name='SupervisoryApproval']" onchange="setDataChanged(true);" value="" onclick=";DisablePaySupChain();" type="checkbox" runat="server"></asp:CheckBox>
            </td>
           </tr>
           <tr id="">
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Allow the group of supervisor to approve:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="AccessGrpApprove" tabindex="20" rmxref="/Instance/Document/form/control[@name='AccessGrpApprove']" onchange="setDataChanged(true);" value="" type="checkbox" runat="server"></asp:CheckBox>
            </td>
           </tr>
           <%--//END: rsushilaggar 23-NOV-2011 MITS-26332--%>
           <tr id="">
            <td>Use Current Adjuster Supervisory Chain:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="UseSupChain" rmxref="/Instance/Document/form/control[@name='UseSupChain']" onclick="DisableUseSupChain('0');setDataChanged(true);DisableOnClick(this);" tabindex="18" value="True" type="checkbox" runat="server" />           
            </td>
           </tr>
           <tr id="">
            <td>Time Frame:&nbsp;&nbsp;</td>
            <td>
                <asp:DropDownList ID="TimeInterval" tabindex="19" rmxref="/Instance/Document/form/control[@name='TimeInterval']" onchange="setDataChanged(true);" type="combobox" runat="server">
                <asp:ListItem Value="0">Days</asp:ListItem>
                <asp:ListItem Value="1">Hours</asp:ListItem>
                </asp:DropDownList>
            </td>
           </tr>
           <tr id="">
            <td>Days/Hours For Approval Of Checks On Hold:&nbsp;&nbsp;</td>
            <td>
                <asp:TextBox ID="DaysApproval" tabindex="20" rmxref="/Instance/Document/form/control[@name='DaysApproval']" onchange="setDataChanged(true);" Text="0" size="5" maxlength="5" onblur="CheckNumeric(DaysApproval);" type="text" runat="server"></asp:TextBox>
            </td>
           </tr>
           <%--//rrachev JIRA RMA-13311--%>
           <tr>
            <td>Consider 0 as Valid value for Escalation</td>
            <td>
            <asp:CheckBox ID="ZeroEscalationFunds" tabindex="21" rmxref="/Instance/Document/form/control[@name='ZeroEscalationFunds']" onchange="setDataChanged(true);" value="" type="checkbox" runat="server"></asp:CheckBox>
            </td>
           </tr>
           <tr id="Tr1">
            <td>Notify The Immediate Supervisor:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="PmtNotfyImmSup" tabindex="21" rmxref="/Instance/Document/form/control[@name='PmtNotfyImmSup']" onchange="setDataChanged(true);" value="" type="checkbox" runat="server"></asp:CheckBox>
            </td>
           </tr>
           <tr id="Tr8">
            <td>Disable Diary Notification To Supervisor:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="DiaryToManager" onclick="DisableDiaryManager();setDataChanged(true);DisableOnClick(this);" value="True" rmxref="/Instance/Document/form/control[@name='DiaryToManager']" tabindex="15" type="checkbox" runat="server" />
           </td>
           </tr>
           <tr>
                <td>Disable Email Notification To Supervisor:&nbsp;&nbsp;</td>
                <td><asp:CheckBox type="checkbox" name="$node^88" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='DisableEmailNotifyForPayment']" tabindex="22" id="DisableEmailNotifyForPayment" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
              <!--zmohammad MITs 21109-->
              <tr id="Tr5">
            <td>Enable Payment Approval Diary:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="EnableCRDiary" onclick="setDataChanged(true)" value="True" rmxref="/Instance/Document/form/control[@name='EnableCRDiary']" tabindex="15" type="checkbox" runat="server" />
           </td>
           </tr>
           <tr>
                <td>Enable Payment Approval Email:&nbsp;&nbsp;</td>
                <td><asp:CheckBox type="checkbox" onclick="setDataChanged(true)" name="$node^88" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='EnableCREmail']" tabindex="22" id="EnableCREmail" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
           <!-- Start: Rahul Aggarwal Vendor's Supervisor Apprpval mits 20606 05/05/2010-->
           <tr id="">
            <td>Use Supervisor Approval Search:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="UseSupAppSearch" tabindex="23" rmxref="/Instance/Document/form/control[@name='UseSupAppSearch']" onchange="setDataChanged(true);" value="" type="checkbox" runat="server"></asp:CheckBox>
            </td>
           </tr>
           <!-- end rahul aggarwal-->
           <!--Start: Rahul Aggarwal Collections from LSS MITs 20938 05/21/2010 -->
            
            <tr id="Tr3">
            <td>Put Collections from LSS on Hold:&nbsp;&nbsp;</td>
             <td>
                <asp:CheckBox ID="LssCollectionOnHold" onclick="setDataChanged(true);DisableOnClick(this)" tabindex="24" rmxref="/Instance/Document/form/control[@name='LssCollectionOnHold']" value="True" type="checkbox" runat="server" />
            </td>
            <td>&nbsp;&nbsp;</td>
            </tr>
              <%--sachin 7810--%>
              <tr id="Tr7">
            <td>Disable Diary Notification for Unapproved Entity:&nbsp;&nbsp;</td>
           <td><asp:CheckBox type="checkbox" name="$node^88" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='DisableDiaryNotifyForUnapprovedEntity']" tabindex="22" id="DisableDiaryNotifyForUnapprovedEntity" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
              <%--sachin 7810--%>
            <!--End: Rahul Aggarwal Collections from LSS  -->
           <tr id="">
            <td>
             <table width="100%" align="left">
              <tr>
               <td class="ctrlgroup" width="100%">Supervisory Approval for Reserves</td>
              </tr>
             </table>
            </td>
           </tr>
            <%--  mbahl3 Jira [RMA-826]--%>
              <tr>
             <td>
              <asp:Label ID="LblRsvcriteria" Text="Select the criteria to determine if a hold should be placed on a Reserve." runat="server"></asp:Label>
            </td>
           </tr>
             <%--//Start :Add by kuladeep for MITS-33322--%>
            <tr id="Tr9">       
           <%-- <td>Select a criteria to determine whether reserves should be placed on hold when the limits are exceeded:&nbsp;&nbsp;</td>--%>
              <td>Reserve Limits Are Exceeded:&nbsp;&nbsp;</td>
                 <%--  mbahl3 Jira [RMA-826]--%>
            <td><asp:CheckBox type="checkbox" name="$node^80" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='UseHoldReserve']" tabindex="21" id="UseHoldReserve" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" runat="server"></asp:CheckBox>
            </td>
           </tr>
            <%--//End :Add by kuladeep for MITS-33322--%>
           <tr id="">       
              <%--ref of these tab controls is corrected by Nitin for Mits 18403 on Nov 05 09--%>
            <td>Use Supervisory Approval For Reserves:&nbsp;&nbsp;</td>
            <td><asp:CheckBox type="checkbox" name="$node^80" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='UseSupAppReserves']" tabindex="21" id="UseSupAppReserves" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" onclick=";DisableResSupChain();" runat="server"></asp:CheckBox></td>
           </tr>
           <%--//Start: rsushilaggar 23-NOV-2011 MITS-26332--%>
           <tr id="">
            <td>&nbsp;&nbsp;&nbsp;&nbsp;Allow the group of supervisor to approve:&nbsp;&nbsp;</td>
            <td>
                <asp:CheckBox ID="AccessGrpApproveReserve" tabindex="20" rmxref="/Instance/Document/form/control[@name='AccessGrpApproveReserve']" onchange="setDataChanged(true);" value="" type="checkbox" runat="server"></asp:CheckBox>
            </td>
           </tr>
           <%--//END: rsushilaggar 23-NOV-2011 MITS-26332--%>
		<%--MGaba2:Added combo box --%>
            <tr >
            <td>Time Frame:&nbsp;&nbsp;</td>
            <td>
                <asp:DropDownList ID="DDLTimeFrame" rmxref="/Instance/Document/form/control[@name='TimeIntervalAppRes']" onchange="setDataChanged(true);" type="combobox" runat="server">
                <asp:ListItem Value="0">Days</asp:ListItem>
                <asp:ListItem Value="1">Hours</asp:ListItem>
                </asp:DropDownList>
            </td>
           </tr>

           <tr id="">
           <td>Days/Hours For Approval Of Reserves:</td>
            <td><asp:TextBox type="text" name="$node^83" value="1" size="5" id="DaysApprovalReserves" rmxref="/Instance/Document/form/control[@name='DaysApprovalReserves']" maxlength="5" tabindex="26" onchange="setDataChanged(true);" onblur="CheckNumeric(DaysApprovalReserves);" runat="server"></asp:TextBox></td>
           </tr>
           <%--//rrachev JIRA RMA-13311--%>
           <tr>
            <td>Consider 0 as Valid value for Escalation</td>
            <td>
            <asp:CheckBox ID="ZeroEscalationReserves" tabindex="21" rmxref="/Instance/Document/form/control[@name='ZeroEscalationReserves']" onchange="setDataChanged(true);" value="" type="checkbox" runat="server"></asp:CheckBox>
            </td>
           </tr>
           <tr id="">
            <td>Notify The Immediate Supervisor:&nbsp;&nbsp;</td>
            <td><asp:CheckBox type="checkbox" name="$node^85" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='NotifySupReserves']" tabindex="27" id="NotifySupReserves" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
           <tr id="">
            <td>Use Current Adjuster Supervisory Chain:&nbsp;&nbsp;</td>
            <td><asp:CheckBox type="checkbox" name="$node^88" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='UseCurrAdjReserves']" tabindex="28" id="UseCurrAdjReserves" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
           <tr id="">
           <%--Start rsushilaggar MITS-19624 --%>
            <td>Disable Email Notification To Supervisor:&nbsp;&nbsp;</td>
            <td><asp:CheckBox type="checkbox" name="$node^88" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='DisableEmailNotifyForSuperVsr']" tabindex="29" id="DisableEmailNotifyForSuperVsr" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
           <tr id="">
            <td>Disable Diary Notification To Supervisor:&nbsp;&nbsp;</td>
            <td><asp:CheckBox type="checkbox" name="$node^88" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='DisableDiaryNotifyForSuperVsr']" tabindex="30" id="DisableDiaryNotifyForSuperVsr" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
           <%--End rsushilaggar --%>
              <%--// Jira 6385- Incurred Limit--%>
               <tr id="">
            <td>Incurred Limits are exceeded:&nbsp;&nbsp;</td>
            <td><asp:CheckBox type="checkbox" name="$node^88" value="True" appearance="full" rmxref="/Instance/Document/form/control[@name='UseHoldIncurredLimits']" tabindex="31" id="UseHoldIncurredLimits" readonly="false" relevant="true" required="false" valid="true" onchange="ApplyBool(this);" checked runat="server"></asp:CheckBox></td>
           </tr>
              <%--// Jira 6385- Incurred Limit--%>
           <tr id="">
            <td colspan="2"><font color="blue">In case there is no Current Adjuster for a claim then the Supervisory Approval will default to follow the Current User's Supervisory Chain.</font></td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABPaymentNotifySetup" id="FORMTABPaymentNotifySetup" style="display:none;">
           <tr id="">
            <td width="780px" valign="top"><b></b><table width="100%">
              <tr>
               <td width="90%">
                <div style="">
                 <table width="100%" class="" cellspacing="0" cellpadding="0">  <!-- igupta3 Mits: 33301 -->
                  <tr class="msgheader">
                   <%--<td width="5%"></td>
                   <td width="31.666666666666666667%">User to Notify</td>
                   <td width="31.666666666666666667%">Line of Business</td>
                   <td width="31.666666666666666667%">Notify Period</td>
                   <td width="31.666666666666666667%">Date Criteria</td>--%>
                    <uc1:UserControlGrid runat="server" ID="PaymentNotificationGrid" OverrideBlankRowInsert="true" GridName="PaymentNotificationGrid" GridTitle="" Target="Document/form/NotifyUsers/PaymentNotification"  Unique_Id="RecordId" ShowRadioButton="true"  Width="680px" hidenodes="|RecordId|" ShowHeader="True" LinkColumn="" PopupWidth="290" PopupHeight="350" Type="GridAndButtons" RowDataParam="User" HideButtons="Edit"/>
                    <asp:TextBox style="display:none" runat="server" id="PaymentNotificationSelectedId"  RMXType="id" />
                    <asp:TextBox style="display:none" runat="server" id="PaymentNotificationGrid_RowDeletedFlag"  RMXType="id" Text="false" />
                    <asp:TextBox style="display:none" runat="server" id="PaymentNotificationGrid_Action"  RMXType="id" />
                    <asp:TextBox style="display:none" runat="server" id="PaymentNotificationGrid_RowAddedFlag"  RMXType="id" Text="false"  />
                     <asp:TextBox style="display:none" runat="server" id="txtGenerateXml" Text="true"  RMXType="id" />
                  </tr>
                 </table>
                </div>
               </td>
               <td width="20%" valign="top">
                <table width="100%" valign="top" cellspacing="0" cellpadding="0">
                 <tr>
                  <td>
    
                  </td>
                 </tr>
                </table>
               </td>
              </tr>
             </table>
            </td>
           </tr>
           <tr id="">
           <script language="JavaScript" type="text/javascript">
           DisableDuplicatePayment();
           DisableTrans();DisableDiaryManager();
           DisableQueuePayment('1');
           DisableUseSupChain('1');
           DisableRollup('0');
           </script>
           </tr>
           <tr id=""></tr>
           </table>
           <table border="0"  cellspacing="0" cellpadding="0" id="FORMTABScheduleDateSetup" style="display:none;">
           
              <tr>
               
               <td width="780px" valign="top">
                <table width="80%"  cellspacing="0" cellpadding="0" border="0">
                 <tr>
                  <td width="40%">
                       Use For Automatic Checks:
                  </td>
                  <td  >
                      <asp:CheckBox type="checkbox" onclick="setDataChanged(true);DisableOnClick(this)"  rmxref="/Instance/Document/form/control[@name='AutomaticChecks']" ID="AutomaticChecks" value="True" runat="server" /></td>
                 </tr>
                 <tr>
                  <td>
                       Use For Non-Occupationals Claim Payments:
                  </td>
                  <td>
                      <asp:CheckBox type="checkbox" onclick="setDataChanged(true);DisableOnClick(this)"  rmxref="/Instance/Document/form/control[@name='NOCPayments']" ID="NOCPayments" value="True" runat="server" /></td>
                 </tr>
                 <tr>
                  <td>
                       Use For Combined Payments:
                  </td>
                  <td>
                      <asp:CheckBox type="checkbox" onclick="setDataChanged(true);DisableOnClick(this)"  rmxref="/Instance/Document/form/control[@name='CombPayments']" ID="CombPayments" value="True" runat="server" /></td>
                 </tr>
                 <tr>
                         <td colspan="2">
                             <asp:RadioButton groupname="checkSchedule" runat="server" id="precedFri"  rmxref="/Instance/Document/form/group/checkSchedule"  type="radio"  value="Friday"   Text="Friday: Schedule Saturday and Sunday Checks for the preceding Friday" />
                        </td>
                   
                 </tr> 
                 <tr>
                      <td colspan="2">
                        <asp:RadioButton groupname="checkSchedule" runat="server" id="followMon" rmxref="/Instance/Document/form/group/checkSchedule" RMXType="radio"  value="Monday"  onclick="" Text="Monday: Schedule Saturday and Sunday Checks for the following Monday" />
                        </td>
                 </tr>
                 <tr>
                      <td colspan="2">
                        <asp:RadioButton groupname="checkSchedule" runat="server" id="precedSat"  rmxref="/Instance/Document/form/group/checkSchedule"   RMXType="radio"  value="saturday"  onclick="" Text="Saturday: Schedule Sunday Checks for the preceding Saturday" />
                        </td>
                 </tr>
                 <tr>
                      <td colspan="2">
                        <asp:RadioButton groupname="checkSchedule" runat="server" id="preceThu"  rmxref="/Instance/Document/form/group/checkSchedule"  RMXType="radio" value="Thursday"  onclick="" Text="Thursday: Schedule Saturday and Sunday Checks for the preceding Thursday" />
                        </td>
                 </tr>

                   
                
                </table>
               </td>
              </tr>
               <tr>
                        
                   
                 </tr> 
              <!-- Start for the pop up to take the date and description for holidays-->
                                 <tr>
               <td width="780px">
                <div style="">
                 <table width="100%" class="" cellspacing="0" cellpadding="0">  <!-- igupta3 Mits: 33301 -->
                  <tr class="msgheader">
                      
                      <uc1:UserControlGrid runat="server" ID="ScheduleDateGrid" OverrideBlankRowInsert="true" GridName="ScheduleDateGrid" GridTitle="" Target="Document/form/NotifySchedule/ScheduleDate"       Unique_Id="ScheduleId"         ShowRadioButton="true"   hidenodes="|ScheduleId|"  ShowHeader="True" LinkColumn=""   PopupWidth="290" PopupHeight="350"             Type="GridAndButtons"          RowDataParam="Schedule1" />
                      <asp:TextBox style="display:none" runat="server" id="ScheduleDateGridSelectedId"  RMXType="id" />
                      <asp:TextBox style="display:none" runat="server" id="ScheduleDateGrid_RowDeletedFlag"  RMXType="id" Text="false" />
                      <asp:TextBox style="display:none" runat="server" id="ScheduleDateGrid_Action"  RMXType="id" />
                      <asp:TextBox style="display:none" runat="server" id="ScheduleDateGrid_RowAddedFlag"  RMXType="id" Text="false"  />
                      
            
                   </tr>
                 </table>
                </div>
               </td>
              
              </tr>

              <!--End-->


             </table>
         </td>
       
        </tr>
       </table>
      </div>
      <table>
       <tr>
        <td></td>
       </tr>
      </table>
      <input type="text" name="" value="" id="SysViewType" style="display:none" />
      <input type="text" name="" value="" id="SysCmd" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
      <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
      <input type="text" name="" value="" id="SysFormIdName" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysFormPForm" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="PaymentParamSetup" />
      <input type="text" name="" value="" id="SysRequired" style="display:none" />
      <input type="text" name="" value="RollUpChecks|" id="SysFocusFields" style="display:none"/>
     
      </td>
    </tr>
        <tr id="Tr4">
           <script language="JavaScript" type="text/javascript">
               DisableDuplicatePayment();
               DisableTrans(); DisableDiaryManager();
               DisableQueuePayment('1');
               DisableUseSupChain('1');
           </script>
           </tr>
   </table>
       <input type="hidden"  value="rmx-widget-handle-9" id="SysWindowId"/>
       <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
  </form>
 </body>
</html>
