﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Utilities.Manager
{
    /// <summary>
    /// Class Name : NonAvailabilitySetUp
    /// Author: Animesh Sahai
    /// Mits 18738
    /// </summary>
    public partial class NonAvailabilitySetUp : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();

        /// <summary>
        /// Event handler for the page load event 
        /// Author: Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    txtAdjusterID.Text = AppHelper.GetQueryStringValue("Adjid"); 
                    if (mode.Text.ToLower() == "edit")
                        selectedid.Text = AppHelper.GetQueryStringValue("selectedid");
                    else
                        selectedid.Text ="";  
                }
                TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                txtPostBackParent.Text = "Done";
                Control c = DatabindingHelper.GetPostBackControl(this.Page);
                if (c == null)
                {
                    if (mode.Text.ToLower() == "edit")
                    {
                        txtRowID.Text = selectedid.Text;
                        XmlTemplate = GetMessageTemplate("edit");
                        CallCWSFunctionBind("NonAvailScreenAdaptor.Get", out sCWSresponse, XmlTemplate);
                    }
                    else
                    {
                        XmlTemplate = GetMessageTemplate("add");
                        BindData2Control(XmlTemplate, Page.Form.Controls);     
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Method to Generate the Screen XML template
        /// Author: Animesh Sahai
        /// </summary>
        /// <param name="strMode">Mode of the screen</param>
        /// <returns></returns>
        private XElement GetMessageTemplate(string strMode)
        {
            StringBuilder sXml = new StringBuilder();
            try
            {
                if (strMode == "edit")
                {
                    sXml = sXml.Append("<Message>");
                    sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                    sXml = sXml.Append("<Call><Function></Function></Call><Document><NAPlan>");
                    sXml = sXml.Append("</NAPlan></Document>");
                    sXml = sXml.Append("</Message>");
                }
                else
                {
                    sXml = sXml.Append("<Document><NAPlan>");
                    sXml = sXml.Append("<control name='RowID' type='id'></control>");
                    sXml = sXml.Append("<control name='AdjusterID' type='id'>");
                    sXml = sXml.Append(txtAdjusterID.Text);
                    sXml = sXml.Append("</control>");
                    sXml = sXml.Append("<control name='sDate' type='date'></control>");
                    sXml = sXml.Append("<control name='sTime' type='time'>12:00 AM</control>");
                    sXml = sXml.Append("<control name='eDate' type='date'></control>");
                    sXml = sXml.Append("<control name='eTime' type='time'>11:59 PM</control>");
                    sXml = sXml.Append("</NAPlan></Document>");
                }
                XElement oElement = XElement.Parse(sXml.ToString());
                return oElement;
            }
            finally
            {
                sXml = null; 
            }
        }
        /// <summary>
        /// Event handler for button btnCancel
        /// Author: Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

        /// <summary>
        /// Event handler for button OK
        /// Author: Animesh Sahai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate("edit");
                CallCWSFunction("NonAvailScreenAdaptor.Save",out sCWSresponse,XmlTemplate);
                ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.getElementById('txtReloadNA').value='true';window.opener.document.forms[0].submit();window.close();</script>");
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
