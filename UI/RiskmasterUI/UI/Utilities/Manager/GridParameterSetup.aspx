﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GridParameterSetup.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.GridParameterSetup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Grid Properties</title>
    <script language="javascript" type="text/javascript" src="../../../Scripts/form.js"></script>
    <script language="JavaScript" type="text/javascript">
          var m_DataChanged=false;
          function Increment(m_FormID)
          {
          m_DataChanged = true;
          var objElem = eval('document.forms[0].'+ m_FormID);
          if(objElem != null)
          if(objElem.value<99)
          objElem.value++;

          }
          function Decrement(m_FormID)
          {
          m_DataChanged = true;
          var objElem = eval('document.forms[0].'+ m_FormID);
          if(objElem != null)
          if(objElem.value>1)
          objElem.value--;
          }
          function ClickNotAllowed(m_FormID)
          {
            //alert("You cannot type in the textbox. To edit the value click on moveup/down arrows");
            alert(GridParameterSetupValidations.ValClickNotAllowed);
          var objElem = eval('document.forms[0].'+ m_FormID);
          if(objElem != null)
          objElem.blur();
          return;
          }

          function Save()
          {

          if(!m_DataChanged)
          return false;
          if( Number(document.forms[0].minRows.value) > Number(document.forms[0].maxRows.value))
          {
                //alert("Minimum rows allowed cannot exceed Maximum rows allowed!");
                alert(GridParameterSetupValidations.ValSave1);
          return false;
          }
          if( Number(document.forms[0].minCols.value) > Number(document.forms[0].maxCols.value))
          {
                //alert("Minimum columns allowed cannot exceed Maximum columns allowed!");
                alert(GridParameterSetupValidations.ValSave2);
          return false;
          }
          document.forms[0].hdnaction.value = 'Save';
              //document.forms[0].submit();
          document.forms[form].submit();                   // RMA-19554
          return true;
          }
          function PageLoad()
          {
          }
        </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <table border="0">
    <tr>
     <td height="32"><a id="btnSave" class="bold" href="javaScript:return Save();">
     <asp:ImageButton runat="server" ID="Save"  ImageUrl="~/Images/tb_save_active.png" border="0" alt="<%$ Resources:imgbtnSave %>" 
             title="<%$ Resources:imgbtnSave %>" onclick="Save_Click" OnClientClick= "return Save();" />
     </a></td>
    </tr>
    <tr>
     <td class="msgheader" colspan="5">
         <asp:Label ID="lblGridParameters" runat="server" Text="<%$ Resources:lblGridParameters %>"></asp:Label></td>
    </tr>
    <tr>
     <td>
       <asp:Label ID="lblMinRowAllowed" runat="server" Text="<%$ Resources:lblMinRowAllowed %>"></asp:Label>
      
     </td>
     <td><asp:TextBox runat="server" rmxref=""  style="background-color: silver" id="minRows" onfocus="javascript:ClickNotAllowed('minRows')"></asp:TextBox></td>
     <td><a class="bold" href="javascript:Increment('minRows')">
     <img id="Img1" src="../../../Images/up.gif" runat="server" width="20" height="20" border="0" alt="<%$ Resources:imgbtnAltMoveUp %>" title="<%$ Resources:imgbtnMoveUp %>"></a>
     <a class="bold" href="javascript:Decrement('minRows')"><img id="Img2" src="../../../Images/down.gif" width="20" height="20" border="0" runat="server" alt="<%$ Resources:imgbtnAltMoveDown %>" title="<%$ Resources:imgbtnMoveDown %>"></a></td>
     <td>
      <asp:Label ID="lblMaxRowAllowed" runat="server" Text="<%$ Resources:lblMaxRowAllowed %>"></asp:Label>
      
     </td>
     <td><asp:TextBox runat="server" id="maxRows" style="background-color: silver" onfocus="javascript:ClickNotAllowed('maxRows')"></asp:TextBox></td>
     <td><a class="bold" href="javascript:Increment('maxRows')"><img id="Img3" src="../../../Images/up.gif" runat="server" width="20" height="20" border="0" alt="<%$ Resources:imgbtnAltMoveUp %>" title="<%$ Resources:imgbtnMoveUp %>"></a>
     <a class="bold" href="javascript:Decrement('maxRows')"><img id="Img4" src="../../../Images/down.gif" width="20" height="20" border="0" runat="server" alt="<%$ Resources:imgbtnAltMoveDown %>" title="<%$ Resources:imgbtnMoveDown %>"></a></td>
    </tr>
    <tr>
     <td>
      <asp:Label ID="lblMinColAllowed" runat="server" Text="<%$ Resources:lblMinColAllowed %>"></asp:Label>
      
     </td>
     <td><asp:TextBox runat="server" id="minCols" style="background-color: silver" onfocus="javascript:ClickNotAllowed('minCols')"></asp:TextBox></td>
     <td><a class="bold" href="javascript:Increment('minCols')"><img id="Img5" src="../../../Images/up.gif" width="20" height="20" border="0" runat="server" alt="<%$ Resources:imgbtnAltMoveUp %>" title="<%$ Resources:imgbtnMoveUp %>"></a>
     <a class="bold" href="javascript:Decrement('minCols')"><img id="Img6" src="../../../Images/down.gif" width="20" height="20" border="0" runat="server" alt="<%$ Resources:imgbtnAltMoveDown %>" title="<%$ Resources:imgbtnMoveDown %>"></a></td>
     <td>
     <asp:Label ID="lblMaxColAllowed" runat="server" Text="<%$ Resources:lblMaxColAllowed %>"></asp:Label> 
      
     </td>
     <td><asp:TextBox runat="server" id="maxCols" style="background-color: silver" onfocus="javascript:ClickNotAllowed('maxCols')"></asp:TextBox></td>
     <td><a class="bold" href="javascript:Increment('maxCols')"><img id="Img7" src="../../../Images/up.gif" width="20" height="20" border="0" runat="server" alt="<%$ Resources:imgbtnAltMoveUp %>" title="<%$ Resources:imgbtnMoveUp %>"></a>
     <a class="bold" href="javascript:Decrement('maxCols')"><img id="Img8" src="../../../Images/down.gif" width="20" height="20" border="0" runat="server" alt="<%$ Resources:imgbtnAltMoveDown %>" title="<%$ Resources:imgbtnMoveDown %>"></a></td>
    </tr>
    <tr>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td><input type="hidden" name="$node^7" value="" id="hdnaction"></td>
     <td></td>
    </tr>
   </table>
    </form>
</body>
</html>
