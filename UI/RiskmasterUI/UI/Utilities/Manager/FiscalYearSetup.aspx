﻿<%@ Page Title="" theme="RMX_Default" Language="C#" AutoEventWireup="true" CodeBehind="FiscalYearSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.FiscalYearSetup" %>
<%@ Register src="../../Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc1" %>
<%@ Register src="../../Shared/Controls/OrgSearchCriteria.ascx" tagname="OrgSearchCriteria" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>
        Fiscal Year Setup
    </title>
       <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
       <script language="javascript" type="text/javascript" >
         var rdoSelected = 0;
         function SelectFiscalYear(year)
         {
           document.forms[0].hdnSelected.value = year;
           rdoSelected = 1;
         }
       
         function openOrgPage()
				{
					m_sFieldName='orgcode';
					m_codeWindow=window.open('../../OrganisationHierarchy/OrgHierarchyLookup.aspx?amp;tablename=nothing&rowid=all&lob=all','Table',
				'width=700,height=600'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
				
				   
				}

		 function OpenFiscalWizard(type)
		 {
		     vfrmLob = document.forms[0].lobcode_cid.value;
		     if (vfrmLob == "") {
		         vfrmLob = "0";
		     }
		     vfrmOrg = document.forms[0].orgcode_cid.value;
		     if (vfrmOrg == "") {
		         vfrmOrg = "0";
		     }
		
		    if (type == 0) {

		        window.open('FiscalYearWizard.aspx?tablename=nothing&type=1&frmLob=' + vfrmLob + '&frmOrg=' + vfrmOrg + '&fYear=' + document.forms[0].hdnCurrentYear.value, 'Table',
				'width=550,height=500' + ',top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
		    }
		    else {

		        window.open('FiscalYearWizard.aspx?tablename=nothing&type=0&frmLob=' + vfrmLob + '&frmOrg=' + vfrmOrg + '&fYear=' + document.forms[0].hdnSelected.value, 'Table',
				'width=550,height=500' + ',top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
		  
		    }	
		  return false;
        } 
		
		
				
				
			function refreshList()
				{
					document.forms[0].submit();
				}
   
   
        function Copy()
		{
		  if(rdoSelected == 0)
		  {
		    alert ("Please select Fiscal Year to Copy.");
		    return false;
		  }
		  vfrmLob=document.forms[0].lobcode_cid.value;
		  if (vfrmLob=="")
		  { 
		    vfrmLob="0";
		  }
		  vfrmOrg=document.forms[0].orgcode_cid.value;
		  if (vfrmOrg=="")
		  { 
		    vfrmOrg="0";
		  }
		  vtoLob=document.forms[0].lobcode_cid.value;
		  if (vtoLob=="")
		  { 
		    vtoLob="0";
		  }
		  vtoOrg=document.forms[0].orgcode_cid.value;
		  if (vtoOrg=="")
		  { 
		    vtoOrg="0";
		  }
		 
		  vfYear=document.forms[0].hdnSelected.value;
		  var objWnd=window.open("FiscalCopy.aspx?frmLob="+vfrmLob+"&frmOrg="+vfrmOrg+"&toLob="+vtoLob+"&toOrg="+vtoOrg+"&fYear="+vfYear,"memoWnd",
		'width=430,height=370'+',top='+(screen.availHeight-370)/2+',left='+(screen.availWidth-390)/2+',resizable=yes,scrollbars=yes');
		  return false;
		}
		
		
        function SetAction(sAction) {
		    document.forms[0].hdnAction.value = sAction;
            //bijender MIts 14547 //MITS 35313 srajindersin 2/13/2014
		    //if (document.getElementById('gvFiscalYearSetup_ctl02_lblFiscalYear') == null) 
		    if ( document.getElementById('hdnSelected').value == "")
		    {
		        return false;
		    }
		    //end
        }

        //bijender MIts 14547
        function onPageLoaded() {
            //MITS 35313 srajindersin 2/13/2014
            if (document.getElementById('gvFiscalYearSetup').rows.length > 1) {
                document.getElementById('selectrdo').checked = true;
                rdoSelected = 1;
                document.forms[0].hdnSelected.value = document.getElementById('gvFiscalYearSetup').rows[1].cells[1].innerText;
            }
        }   
        //end
			
	
        
      </script>
     <style type="text/css">
         .style1
         {
             width: 251px;
         }
         .style2
         {
             width: 250px;
         }
     </style>
</head>
<body onload="javascript:onPageLoaded();parent.MDIScreenLoaded()">
 <form id="frmData" runat="server" name="frmData" autocomplete="off" method="post">
      <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <table border="0" width="100%">
        <tr>
            <td colspan="3" class="msgheader" colspan="3">
                Fiscal Years List
            </td>
        </tr>
        
        <tr>
            <td class="style2" width="20%"  >
                Line of Business :
            </td>
            <td class="style1" width="30%">
           
            <input type="text" value="All Lines" size="30" id="lobcode" cancelledvalue=""  onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" runat="server"  />
            <input type="button" id="lobcodebtn" name="lobcodebtn" value="..." class="button"  runat="server" onclick="return selectCode('LINE_OF_BUSINESS','lobcode')" />
            <input type="hidden" value="" id="lobcode_cid" runat="server" />
            
            </td>
            <td width="50%">
                <asp:button id="btAllLOB" name="btAllLOB" text="All LOB" runat="server" 
                    class="button" width="67px" onclick="btAllLOB_Click" />
            </td>
        </tr>
        
        <tr>
            <td class="style2" width="20%">
                Organization :
            </td>
            <td width="30%">
                   <asp:TextBox  runat="server"  Text="Entire Organization" size="30" 
                       onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);"  
                       id="orgcode" cancelledvalue="" ontextchanged="orgcode_TextChanged" ></asp:TextBox>
                  <%--<input type="button" onclick="openOrgPage();" value="..." class="button" name="orgcodebtn" />--%>
                  <input type="button" onclick="return selectCode('orgh','orgcode','ALL')" value="..." class="button" name="orgcodebtn" />
                  <input type="hidden" name="$node^20" value="" id="orgcode_cid" runat="server" />
                    
            </td>
            <td width="50%">
                <asp:button id="btAllOrganization" name="btAllOrganization" text="All Org" 
                    runat="server" width="67px" class="button" 
                    onclick="btAllOrganization_Click" />
            </td>
        </tr>
        </table>
        <table id="tblFiscallist" runat="server" width="100%" >
        <tr>
            <td colspan="3">
            <div id="divForms" class="divScroll" style="height:320px"  runat="server" >
                <asp:gridview id="gvFiscalYearSetup" AutoGenerateColumns="false" 
                    name="gvFiscalYearSetup" runat="server" width="98%" 
                    onrowdatabound="gvFiscalYearSetup_RowDataBound" >
                <HeaderStyle CssClass="colheader5" />
		        <AlternatingRowStyle CssClass="data2" />
		           
		          <Columns>
	                    <asp:TemplateField  ItemStyle-CssClass="data" ControlStyle-Width="5%">
                            <ItemTemplate>
                               <input type="radio" id="selectrdo" name="selectrdo" />
                            </ItemTemplate> 
                        </asp:TemplateField>
                               
                      <asp:TemplateField HeaderText="Fiscal Year" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblFiscalYear" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FiscalYear")%>' ></asp:Label>
                                </ItemTemplate> 
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblStartDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate")%>' ></asp:Label>
                                </ItemTemplate> 
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="End Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblEndDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDate")%>' ></asp:Label>
                                </ItemTemplate> 
                  </asp:TemplateField>  
                   <asp:TemplateField HeaderText="Periods" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblPeriods" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Periods")%>' ></asp:Label>
                                </ItemTemplate> 
                  </asp:TemplateField>  
                   <asp:TemplateField HeaderText="Line of Business" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblLOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LOB")%>' ></asp:Label>
                                </ItemTemplate> 
                  </asp:TemplateField>  
                  <asp:TemplateField HeaderText="Organization" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblOrganization" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Organization")%>' ></asp:Label>
                                </ItemTemplate> 
                  </asp:TemplateField>  
                 
                 
                 </Columns>  
		           
                </asp:gridview>
            </div>
            </td>
        </tr>
  
        <tr>
            <td colspan="3">
            
                <asp:button id="btLobOrg" name="btLobOrg" text="Lob/Org" runat="server" 
                    class="button" width="75px" onclick="btLobOrg_Click" />
                <asp:button id="btRefreshList" name="btRefreshList" text="Refresh List" runat="server" class="button" width="80px" />
                <asp:button id="btPrint" name="btPrint" text="Print" runat="server" 
                    class="button" width="75px" onclick="btPrint_Click" OnClientClick="SetAction('Print');"  />
                <asp:button id="btDelete" name="btDelete" text="Delete" runat="server" OnClientClick="return SetAction('Delete');" 
                    class="button" width="75px" onclick="btDelete_Click" />
                <asp:button id="btCopy" name="btCopy" text="Copy" runat="server" OnClientClick="return Copy();" class="button" width="75px" />
                <asp:button id="btNew" name="btNew" text="New" runat="server" class="button" width="75px" OnClientClick="return OpenFiscalWizard(0);" />
                <asp:button id="btEdit" name="btEdit" text="Edit" runat="server" class="button" width="75px" OnClientClick="return OpenFiscalWizard(1);" />
                
            </td>
        </tr>
  
    </table>
    
    <div id="divOrg" runat="server" >
         <table width="100%">
         <tr>
         <td>
                <asp:GridView ID="grdOrg" runat="server"  AutoGenerateColumns="false" width="100%" >
                <HeaderStyle CssClass="colheader5" />
		        <AlternatingRowStyle CssClass="data2" />
		         
		          <Columns>
		               <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblRow" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Row")%>' ></asp:Label>
                                </ItemTemplate> 
                      </asp:TemplateField>
		          
	                  <asp:TemplateField HeaderText="1st Year" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblFyear" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.1Year")%>' ></asp:Label>
                                </ItemTemplate> 
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="2nd Year" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblSyear" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.2Year")%>' ></asp:Label>
                                </ItemTemplate> 
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Years" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblYears" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Years")%>' ></asp:Label>
                                </ItemTemplate> 
                  </asp:TemplateField>  
                   <asp:TemplateField HeaderText="Line of Business" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblLo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LOB")%>' ></asp:Label>
                                </ItemTemplate> 
                  </asp:TemplateField>  
                   <asp:TemplateField HeaderText="Organization" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                <ItemTemplate>
                                  <asp:Label ID="lblOrganization" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Organization")%>' ></asp:Label>
                                </ItemTemplate> 
                  </asp:TemplateField>  
                  
                 
                 
                 </Columns>  
		         
                </asp:GridView>
                </td>
                </tr>
                <tr>
                 <td>
                    <asp:button id="Button1" name="btLobOrg" text="Fiscal years" runat="server" 
                    class="button" width="75px" />
                 </td>
                </tr>
                </table>
                
            </div>
    
    <input type="hidden" id="hdnLineOfBusiness" runat="server" />
    <input type="hidden" id="hdnCurrentYear" runat="server" />
    <input type="hidden" id="formname" value="FiscalyearSetup"  />
    <input type="hidden" id="hdnSelected" runat="server" />
    <input type="hidden" id="hdnAction" runat="server" />
  </form>
</body>
</html>
