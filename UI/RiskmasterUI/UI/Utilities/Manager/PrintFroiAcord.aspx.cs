﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class PrintFroiAcord : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            string filename = null;

            try
            {
                filename = AppHelper.GetQueryStringValue("FileName");
                Header.Title = filename;
                XmlTemplate = GetMessageTemplate(filename);
                bReturnStatus = CallCWS("PrintBatchFroiAdaptor.GetFroiAcordPDF", XmlTemplate, out sCWSresponse, true, false);

                XmlTemplate = XElement.Parse(sCWSresponse.ToString());
                XElement oEle = XmlTemplate.XPathSelectElement("//File");
                if (oEle != null)
                {
                    string sFileContent = oEle.Value;
                    Response.ClearContent();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "inline;filename=" + filename);
                    Response.BinaryWrite(Convert.FromBase64String(sFileContent));
                    Response.Flush();
                    Response.Close();
                }
                ErrorControl1.errorDom = sCWSresponse;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate(string sFile)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>c9243784-54ff-4f04-b7cd-cba4c910ce2a</Authorization>");
            sXml = sXml.Append("<Call><Function>FROIBatchPrintingAdaptor.GetFroiAcordPDF</Function></Call><Document>");
            sXml = sXml.Append("<PrintInvoicesNotices>");
            sXml = sXml.Append("<FileName>");
            sXml = sXml.Append(sFile);
            sXml = sXml.Append("</FileName>");
            sXml = sXml.Append("</PrintInvoicesNotices>");
            sXml = sXml.Append("</Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}