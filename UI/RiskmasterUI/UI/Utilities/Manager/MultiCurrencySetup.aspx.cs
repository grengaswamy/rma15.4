﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
  public partial class MultiCurrencySetup : NonFDMBasePageCWS
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      XElement XmlTemplate = null;
      string sCWSresponse = "";
      XmlDocument XmlDoc = new XmlDocument();

      try
      {
        if (MultiCurrencySetupGrid_RowDeletedFlag.Text.ToLower() == "true")
        {
          string selectedRowId = MultiCurrencySetupSelectedId.Text;
          XmlTemplate = GetMessageTemplate(selectedRowId);
          CallCWS("MultiCurrencyAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
          MultiCurrencySetupGrid_RowDeletedFlag.Text = "false";
        }
        XmlTemplate = GetMessageTemplate();
        CallCWSFunctionBind("MultiCurrencyAdaptor.Get", out sCWSresponse, XmlTemplate);
      }
      catch (Exception ee)
      {
        ErrorHelper.logErrors(ee);
        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
      }

    }
    private XElement GetMessageTemplate()
    {     
      StringBuilder sXml = new StringBuilder("<Message>");
      sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
      sXml = sXml.Append("<Call><Function></Function></Call><Document>");
      sXml = sXml.Append("<ExchangeRates><ExchangeRateList>");
      sXml = sXml.Append("<listhead>");
      sXml = sXml.Append("<CurrencyRowId>Row Id</CurrencyRowId>");
      sXml = sXml.Append("<SourceCurrency>Source Currency</SourceCurrency>");      
      sXml = sXml.Append("<DestinationCurrency>Destination Currency</DestinationCurrency>");
      sXml = sXml.Append("<ExchangeRate>Exchange Rate</ExchangeRate>");
      sXml = sXml.Append("</listhead></ExchangeRateList></ExchangeRates>");
      sXml = sXml.Append("</Document></Message>");
      XElement oElement = XElement.Parse(sXml.ToString());
      return oElement;

    }
    private XElement GetMessageTemplate(string selectedRowId)
    {     
      StringBuilder sXml = new StringBuilder("<Message>");
      sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
      sXml = sXml.Append("<Call><Function></Function></Call><Document><MultiCurrency>");
      sXml = sXml.Append("<control name='MultiCurrencySetupGrid'>");
      sXml = sXml.Append(selectedRowId);
      sXml = sXml.Append("</control>");
      sXml = sXml.Append("</MultiCurrency></Document>");
      sXml = sXml.Append("</Message>");
      XElement oElement = XElement.Parse(sXml.ToString());
      return oElement;

    }
  }
}
