﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
namespace Riskmaster.UI.UI.Utilities.Manager
{
    /// <summary>
    ///Author  :   Neha Sharma
    ///Dated   :   30th May,2011
    ///Purpose :   Code Relationships 
    /// </summary>
    public partial class CodeRelationships : NonFDMBasePageCWS
    {
        private XElement XmlTemplate = null;
        XElement oMessageElement = null;
        private bool bReturnStatus = false;
        private string sreturnValue = string.Empty;
        private XmlDocument XmlDoc = new XmlDocument();
        private string sNode = null;
        string strValue = null;
        string sSubRelationType = string.Empty;
       
        /// <summary>
        /// Loads the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    oMessageElement = GetMessageTemplate();
                    CallCWS("CodeRelationshipsAdaptor.Get", oMessageElement, out sreturnValue, false, true);
                    RenameControls();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
       
        /// <summary>
        /// Called on the selection of list box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstClaimType_SelectedIndexChanged(object sender, EventArgs e)
        {
             StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>CodeRelationshipsAdaptor.GetRelatedLossComponents</Function></Call>");//Deb
            sXml = sXml.Append("<Document><CodeRelationships><control name=\"cboRelationshipType\">" + cboRelationshipType.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"cboRelationshipSubType\">" + cboRelationshipSubType.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"txtRelatedComponents\" >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"lstClaimType\">" + lstClaimType.SelectedValue );
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"hndPagetype\">" + hndPagetype.Value);
            sXml = sXml.Append("</control>");
            //tanwar2 - mits 30910 - start
            sXml = sXml.Append("<control name=\"txtRemoveComponents\" >" + txtRemoveComponents.Text + "</control>");
            sXml = sXml.Append("<control name=\"txtAddComponents\" />");
            //tanwar2 - mits 30910 - end
            sXml = sXml.Append("</CodeRelationships></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            CallCWS("CodeRelationshipsAdaptor.GetRelatedLossComponents", oTemplate, out sreturnValue, false, true);
            UpdateRelatedLossComponent();
        }
        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                UpdateRelatedLossComponent();
                if (string.Compare(lstClaimType.SelectedValue, "") != 0)
                {
                    //Deb
                    bool bReturnStatus = false;
                    StringBuilder sXml = new StringBuilder("<Message>");
                    sXml = sXml.Append("<Authorization></Authorization>");
                    sXml = sXml.Append("<Call><Function>CodeRelationshipsAdaptor.Save</Function></Call>");
                    sXml = sXml.Append("<Document><CodeRelationships><control name=\"cboRelationshipType\">" + cboRelationshipType.SelectedValue);
                    sXml = sXml.Append("</control>");
                    sXml = sXml.Append("<control name=\"cboRelationshipSubType\" >" + cboRelationshipSubType.SelectedValue);
                    sXml = sXml.Append("</control>");
                    sXml = sXml.Append("<control name=\"txtRelatedComponents\" >" + txtRelatedComponents.Text);
                    sXml = sXml.Append("</control>");
                    //if (cboRelationshipType.SelectedValue.EndsWith("-O"))   //Aman MITS 27637
                    //{
                    //    sXml = sXml.Append("<control name=\"lstAvailableLossComponents\">" + lstClaimType.SelectedValue);
                    //}
                    //else
                    //{
                        sXml = sXml.Append("<control name=\"lstClaimType\">" + lstClaimType.SelectedValue);
                    //}
                    sXml = sXml.Append("</control>");
                    sXml = sXml.Append("</CodeRelationships></Document></Message>");
                    XElement oTemplate = XElement.Parse(sXml.ToString());
                    bReturnStatus = CallCWS("CodeRelationshipsAdaptor.Save", oTemplate, out sreturnValue, false,false);
                    //Deb
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        /// <summary>
        /// method called when relationship type is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cboRelationshipType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeView();
            oMessageElement = GetMessageTemplate();

            CallCWS("CodeRelationshipsAdaptor.Get", oMessageElement, out sreturnValue, false, true);
            cboRelationshipType.SelectedValue = strValue;
            //cboRelationshipSubType.SelectedValue = string.Empty;
            lstRelatedLossComponents.Items.Clear();
            RenameControls();
        }

        protected void cboRelationshipSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeView();
            oMessageElement = GetMessageTemplate();

            CallCWS("CodeRelationshipsAdaptor.GetSubData", oMessageElement, out sreturnValue, false, true);
            cboRelationshipType.SelectedValue = strValue;
            cboRelationshipSubType.SelectedValue = sSubRelationType;
            lstRelatedLossComponents.Items.Clear();
            RenameControls();
        }

        #region Private Methods
        private void ChangeView()
        {
            string sRefClaimType;

            string sRefLCType;
            string strCode1;

            //sRefClaimType = lstClaimType.Attributes["RMXRef"];
            //sRefLCType = lstAvailableLossComponents.Attributes["RMXRef"];
            //lstClaimType.Attributes["RMXRef"] = sRefLCType;
            //lstAvailableLossComponents.Attributes["RMXRef"] = sRefClaimType;
            strValue = cboRelationshipType.SelectedValue;
              sSubRelationType =cboRelationshipSubType.SelectedValue;
            //strCode1 = btnCode2.OnClientClick;
            //btnCode2.OnClientClick = btnCode1.OnClientClick;
            //btnCode1.OnClientClick = strCode1;
            if (strValue.EndsWith("-O"))
            {
                hndPagetype.Value = "O";
            }
            else
            {
                hndPagetype.Value = "N";
            }
        }
        private void UpdateRelatedLossComponent()
        {
            string svalues = string.Empty;
            string[] lsthndValues = txtRelatedComponents.Text.Split(',');
            ListItem  lstItem = null;
            
            //tanwar2 - mits 30910 - start
            string[] lstValuesToRemove = null;
            lstValuesToRemove = txtRemoveComponents.Text.Split(',');
            //tanwar2 - mits 30910 - end
            try
            {
                foreach (ListItem lstItemvalue in lstRelatedLossComponents.Items)
                {
                    lstAvailableLossComponents.Items.Add(lstItemvalue);
                   
                }
                lstRelatedLossComponents.Items.Clear();
                //tanwar2 - mits 30910 - start
                //Add the items which were removed in last iteration
                foreach (ListItem lstItemvalue in lstAddComponents.Items)
                {
                    lstAvailableLossComponents.Items.Add(lstItemvalue);
                }
                lstAddComponents.Items.Clear();
                //tanwar2 - mits 30910 - end
                for (int Icount = 0; Icount < lsthndValues.Length; Icount++)
                {

                    lstItem = lstAvailableLossComponents.Items.FindByValue(lsthndValues[Icount]);
                    if (lstAvailableLossComponents.Items.Contains(lstItem))
                    {
                        lstRelatedLossComponents.Items.Add(lstItem);
                        lstAvailableLossComponents.Items.Remove(lstItem);
                    }

                }
                //tanwar2 - mits 30910 - start
                //Remove items to force one to many mapping
                for (int Icount = 0; Icount < lstValuesToRemove.Length; Icount++)
                {
                    lstItem = lstAvailableLossComponents.Items.FindByValue(lstValuesToRemove[Icount]);
                    if (lstAvailableLossComponents.Items.Contains(lstItem))
                    {
                        lstAvailableLossComponents.Items.Remove(lstItem);
                        lstAddComponents.Items.Add(lstItem);
                    }

                }
                //tanwar2 - mits 30910 - end
               
            }
             catch (Exception ex)
             {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>AutoFroiAcordAdaptor.Get</Function></Call>");
            sXml = sXml.Append("<Document><CodeRelationships><control name=\"cboRelationshipType\" value=\""+ cboRelationshipType.SelectedValue+"\">");
            sXml = sXml.Append("</control>");
             sXml = sXml.Append("<control name=\"cboRelationshipSubType\" value=\""+ cboRelationshipSubType.SelectedValue+"\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"lstClaimType\" >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"lstAvailableLossComponents\" >");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</CodeRelationships></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

       
        private void RenameControls()
        {
            string strLabel;
            
            strLabel = cboRelationshipSubType.SelectedItem.Text;
            lblCode1.Text = strLabel.Substring(0, strLabel.LastIndexOf("To")-1);
            lblCode2.Text = strLabel.Substring(strLabel.LastIndexOf("To")+3);


        }
        #endregion
    }
}