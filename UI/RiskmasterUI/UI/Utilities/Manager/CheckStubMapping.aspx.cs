﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml.Linq;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class CheckStubMapping : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //nsachdeva2 - MITS:28267 - 6/1/2012
            txt_CheckText.Attributes.Add("onchange", "return limitText();");
            txt_CheckText.Attributes.Add("onKeyPress", "return limitText();");
            //End MITS: 28267
            if (!IsPostBack)
            {
                string selectedRowId = "1";
                XmlTemplate = GetXmlTemplate(selectedRowId);
                CallCWS("CheckStubTextsAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
            }
        }
        /// <summary>
        /// Get the sample template to get the data from the database
        /// </summary>
        /// <param name="selectedRowId"></param>
        /// <returns></returns>
        private XElement GetXmlTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><CheckStubTextMappingList><listhead>");
            sXml = sXml.Append("<StateId>State Id</StateId><TransTypeCode type='code'>Transaction Type</TransTypeCode><CheckStubText>Check Stub Text</CheckStubText><RowId>RowId");
            sXml = sXml.Append("</RowId>");
            sXml = sXml.Append("</listhead></CheckStubTextMappingList></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }

        /// <summary>
        /// server event to cater for delete scenario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Remove_Record_Click(object sender, EventArgs e)
        {
            NonFDMCWSPageLoad("CheckStubTextAdaptor.Delete");
            RowId.Text = "";
        }

        /// <summary>
        /// server event to cater to add record scenario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Add_Record_Click(object sender, EventArgs e)
        {
            RowId.Text = "";
            bool bError = false;
            bool bError2 = false;
            string State = ((TextBox)States.FindControl("multicode_lst")).Text;
            string[] StateList = State.Split(' ');
            string TransType = ((TextBox)TransTypeCode.FindControl("multicode_lst")).Text;
            string[] TransTypeList = TransType.Split(' ');
            GridViewRowCollection Rows = ((GridView)(CheckStubMappingsGrid.FindControl("gvData"))).Rows;
            if (!string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(TransType))
            {
                foreach (string statecode in StateList)
                {
                    foreach (string transTypecode in TransTypeList)
                    {
                        foreach (GridViewRow row in Rows)
                        {
                            if ((statecode.Trim() == row.Cells[4].Text.Trim())
                                && (transTypecode.Trim() == row.Cells[6].Text.Trim()))
                            {
                                bError = true;
                                break;
                            }
                            if (bError)
                                break;
                        }
                        if (bError)
                        {
                            break;
                        }
                    }
                    if (bError)
                        break;
                }
            }
            else if (!string.IsNullOrEmpty(State))
            {
                foreach (string statecode in StateList)
                {
                    foreach (GridViewRow row in Rows)
                    {
                        if ((statecode.Trim() == row.Cells[4].Text.Trim())
                            && (row.Cells[6].Text.Trim() == "0"))
                        {
                            bError = true;
                            break;
                        }
                        if (bError)
                            break;
                    }
                    if (bError)
                    {
                        break;
                    }

                }
            }
            else if (!string.IsNullOrEmpty(TransType))
            {
                foreach (string transTypecode in TransTypeList)
                {
                    foreach (GridViewRow row in Rows)
                    {
                        if ((transTypecode.Trim() == row.Cells[6].Text.Trim())
                            && (row.Cells[4].Text.Trim() == "0"))
                        {
                            bError = true;
                            break;
                        }
                        if (bError)
                            break;
                    }
                    if (bError)
                    {
                        break;
                    }
                }
            }
            else
            {
                foreach (GridViewRow row in Rows)
                {
                    if ((row.Cells[4].Text.Trim() == "0")
                        && (row.Cells[6].Text.Trim() == "0"))
                    {
                        bError = true;
                        break;
                    }
                    if (bError)
                        break;
                }

            }
            //else
            //{
            //    bError = true;
            //    bError2 = true;
            //}
            if (!bError)
            {
                NonFDMCWSPageLoad("CheckStubTextAdaptor.Save");
                
            }
            else
            {
                ErrorControl1.Text = ErrorHelper.FormatErrorsForUI("Check stub text for the given Jurisdiction and Transaction Type already exists.", true);
            }

            ListBox stateListbox = ((ListBox)States.FindControl("multicode"));
            stateListbox.Items.Clear();
            TextBox txtStates = ((TextBox)States.FindControl("multicode_lst"));
            txtStates.Text = "";
            ListBox TransTypeListbox = ((ListBox)TransTypeCode.FindControl("multicode"));
            TransTypeListbox.Items.Clear();
            TextBox txtTransTypeCode = ((TextBox)TransTypeCode.FindControl("multicode_lst"));
            txtTransTypeCode.Text = "";
            txt_CheckText.Text = "";
        }

        /// <summary>
        /// server event to cater for edit scenario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit_Record_Click(object sender, EventArgs e)
        {

            if (EditFlag.Text == "EDIT")
            {
                GridViewRowCollection Rows = ((GridView)(CheckStubMappingsGrid.FindControl("gvData"))).Rows;
                foreach (GridViewRow oRow in Rows)
                {
                    if (oRow.Cells[9].Text == RowId.Text)
                    {
                        txt_CheckText.Text = oRow.Cells[7].Text;
                        ListBox stateListbox = ((ListBox)States.FindControl("multicode"));
                        stateListbox.Items.Clear();
                        stateListbox.Items.Add(new ListItem(AppHelper.StripHTMLTags(oRow.Cells[3].Text), AppHelper.StripHTMLTags(oRow.Cells[3].Text)));
                        TextBox txtStates = ((TextBox)States.FindControl("multicode_lst"));
                        txtStates.Text = oRow.Cells[4].Text;    
                        ListBox TransTypeListbox = ((ListBox)TransTypeCode.FindControl("multicode"));
                        TransTypeListbox.Items.Clear();
                        TransTypeListbox.Items.Add(new ListItem(AppHelper.StripHTMLTags(oRow.Cells[5].Text), AppHelper.StripHTMLTags(oRow.Cells[5].Text)));
                        TextBox txtTransTypeCode = ((TextBox)TransTypeCode.FindControl("multicode_lst"));
                        txtTransTypeCode.Text = oRow.Cells[6].Text;
                        //RadioButtonList radioButtonList = ((RadioButtonList)(CheckStubMappingsGrid.FindControl("MyRadioButton")));
                        //for (int i = 0; i < radioButtonList.Items.Count; i++)
                        //{
                        //    if (radioButtonList.Items[i].Text == RowId.Text)
                        //        radioButtonList.Items[i].Selected = true;
                        //}
                    }
                }
                EditFlag.Text = "UPDATE";
                Edit_Record.Text = "Update";
            }
            else
            {
                NonFDMCWSPageLoad("CheckStubTextAdaptor.Save");
                Edit_Record.Text = "Edit";
                EditFlag.Text = "EDIT";
                //RowId.Text = "";
                ListBox stateListbox = ((ListBox)States.FindControl("multicode"));
                stateListbox.Items.Clear();
                TextBox txtStates = ((TextBox)States.FindControl("multicode_lst"));
                txtStates.Text = ""; 
                ListBox TransTypeListbox = ((ListBox)TransTypeCode.FindControl("multicode"));
                TransTypeListbox.Items.Clear();
                TextBox txtTransTypeCode = ((TextBox)TransTypeCode.FindControl("multicode_lst"));
                txtTransTypeCode.Text = "";
                txt_CheckText.Text = "";
            }

        }
    }
}
