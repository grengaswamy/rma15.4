﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDataMapForm.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP.UserDataMap" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="usc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    .left{
      width: 49%;
      float: left;
      margin-top:5px;
    }
    .right{
      margin-top:5px;
    }
    </style>
    <script src="../../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type='text/javascript'>
        $('document').ready(function () {
            if ($.trim($('#hdnCloseWindow').val()) === 'True') {
                if (window.opener.$('#frmData')) {
                    window.opener.$('#frmData').submit();
                }
                window.close();
            }

            $('#btnSave').click(function () {
                if (!Validate()) {
                    alert('Required Field Missing');
                    return false;
                }
            });

            $('#btnCancel').click(function () {
                window.close();
            });
        });

        function Validate() {
            var bValid = true;
            $('.required').next('input[type=text]').each(function () {
                if ($.trim($(this).val()) === '') {
                    bValid = false;
                }
            });

            return bValid;
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
    <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip=" <%$ Resources:btnSaveResrc %> "
                        runat="server" OnClick="OnSaveClick" />
    <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:btnCancelResrc %>"
                        runat="server" />

    <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="99%" Text="<%$ Resources:lblUserDataMappingResrc %>"></asp:Label>
        <div>
            <asp:Label ID="lblUserData" runat="server" CssClass="left required" Text="<%$ Resources:lblUserDataResrc %> "></asp:Label>
            <asp:DropDownList runat="server" CssClass="right" ID="cmbUserData" OnChange="setDataChanged(true);"/>
        </div>
        <div>
            <asp:Label ID="lblIRName" runat="server" CssClass="left required" Text="<%$ Resources:lblImgRightNameResrc %> "></asp:Label>
            <asp:TextBox runat="server" ID="txtIRName" CssClass="right" OnChange="setDataChanged(true);"/>
        </div>
        <div>
            <asp:Label ID="lblTableName" runat="server" CssClass="left" Text="<%$ Resources:lblTableNameResrc %> "></asp:Label>
            <asp:DropDownList runat="server" AutoPostBack="True" CssClass="right" ID="cmbTableName" OnChange="setDataChanged(true);" OnSelectedIndexChanged="cmbTableName_OnSelectedIndexChange"/>
        </div>
        <div>
            <asp:Label ID="lblColumnName" runat="server" CssClass="left" Text="<%$ Resources:lblFieldNameResrc %> "></asp:Label>
            <asp:DropDownList runat="server" CssClass="right" ID="cmbColumnName" OnChange="setDataChanged(true);"/>
        </div>
    </div>
    <asp:HiddenField ID='hdnCloseWindow' runat="server" />
    <asp:HiddenField ID="hdnType" runat="server" />
    <asp:TextBox style="display:none" ID='UserdataMapId' runat="server"></asp:TextBox>
    </form>
</body>
</html>
