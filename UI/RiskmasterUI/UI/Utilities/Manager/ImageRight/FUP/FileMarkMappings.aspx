﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileMarkMappings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP.FileMarkMappings" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type='text/javascript'>
        $('document').ready(function () {
            if (parent.MDIScreenLoaded != null) {
                parent.MDIScreenLoaded();
            }

            $('#btnNew').click(function () {
                window.open("./FileMarkMapForm.aspx", "fileMapFormWin", 'width=650,height=400' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes,modal=1');
                return false;
            });

            $('#btnEdit').click(function () {
                var selectedRow = GetSelectedRows();
                if (selectedRow != null && selectedRow != undefined) {
                    if (selectedDocId != '') {
                        window.open("./FileMarkMapForm.aspx?mode=edit&Id=" + selectedDocId, "fileMapFormWin", 'width=500,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes,modal=1');
                    }
                }
                return false;
            });

            $('input[type=radio]').change(function () {
                $('input:checked').closest('tr').trigger('click');
            });

            $('tr').click(function () {
                $('input[type=radio]').prop('checked', false);
                $(this).find('input[type=radio]').prop('checked', true);
            });

            $('#btnDelete').mouseover(function () {
                this.src = "../../../../../Images/tb_delete_mo.png";
            });
            $('#btnDelete').mouseout(function () {
                this.src = "../../../../../Images/tb_delete_active.png";
            });

            $('#btnDelete').click(function () {
                //get the checked/selected row
                //1. to find out if radio button of a row is checked or not
                var selectedRow = GetSelectedRows();
                if (selectedRow == null && selectedRow == undefined) {
                    //2.if not checked then ask user to select row
                    alert("Please select the record first");
                    return false;
                }
                else {
                    //3. else delete
                    if (!confirm('Are you sure you want to delete this mapping?')) {
                        return false;
                    }
                    else {
                        if (selectedDocId != '') {
                            $('#hdnRowID').val(selectedDocId);
                            $('#hdnRebind').val(false);
                            return true;
                        }
                    }
                }
            });
        });

        ///Code from:
        ///http ://www.telerik.com/community/code-library/aspnet-ajax/grid/single-radiobutton-check-at-a-time-with-row-selection.aspx
        function selectSingleRadio(objRadioButton, grdName) {
            var i, obj, pageElements;

            if (navigator.userAgent.indexOf("MSIE") != -1) {
                //IE browser  
                pageElements = document.all;
            }
            else if (navigator.userAgent.indexOf("Mozilla") != -1 || navigator.userAgent.indexOf("Opera") != -1) {
                //FireFox/Opera browser  
                pageElements = document.documentElement.getElementsByTagName("input");
            }
            for (i = 0; i < pageElements.length; i++) {
                obj = pageElements[i];

                if (obj.type == "radio") {
                    if (objRadioButton.id.substr(0, grdName.length) == grdName) {
                        if (objRadioButton.id == obj.id) {
                            obj.checked = true;
                        }
                        else {
                            obj.checked = false;
                        }
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="toolBarButton" runat="server" id="div_create" xmlns="">
       <asp:ImageButton ID="btnNew" ImageUrl="~/Images/tb_new_active.png" class="bold" ToolTip="<%$ Resources:btnNewResrc %>" runat="server" />
       <asp:ImageButton ID="btnEdit" ImageUrl="~/Images/tb_edit_active.png" class="bold" ToolTip="<%$ Resources:btnEditResrc %>" runat="server" />
 	   <asp:ImageButton ID="btnDelete" ImageUrl="~/Images/tb_delete_active.png" class="bold" OnClick="btnDelete_Click" ToolTip="<%$ Resources:btnDeleteResrc %>" runat="server" />
    </div>
    <div>
        <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="99.7%" Text="<%$ Resources:lblFileMarkMappingResrc %>"></asp:Label>  
    </div>
    <div>
        <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
        <script type="text/javascript">
            function GetSelectedRows() {
                masterTable = $find("<%=grdFileMarkaMap.ClientID %>").get_masterTableView();
                var selectedRows = masterTable.get_selectedItems();
                if (selectedRows != null && selectedRows != undefined) {
                    return selectedRows[0];
                }
            }
            function RowSelected(sender, args) {
                selectedDocId = args.getDataKeyValue("FileMarkId");
            }
          </script>
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        
    </telerik:RadAjaxManager>

    <telerik:RadGrid runat="server" ID="grdFileMarkaMap"
        AllowPaging="true" PageSize="15" AllowFilteringByColumn="false" AllowSorting="false"
        AutoGenerateColumns="false" ItemStyle-CssClass="rowlight1" AlternatingItemStyle-CssClass="rowlight2"
         OnNeedDataSource="grdFileMarkaMap_OnNeedDataSource" OnItemDataBound="grdFileMarkaMap_OnItemDataBound">
         <SelectedItemStyle CssClass="SelectedItem" />
        <ClientSettings>
                <ClientEvents OnRowSelected="RowSelected" />                    
                <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="500px"/>
                <Selecting AllowRowSelect="true" />
        </ClientSettings>

        <MasterTableView Width="100%" AllowCustomSorting="false" ClientDataKeyNames="FileMarkId">
             <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                 <NoRecordsTemplate>
                  <div>
                    There are no records to display.
                  </div>
                </NoRecordsTemplate>
            <Columns>
                <telerik:GridTemplateColumn UniqueName="grdRadio">
                        <ItemTemplate>
                            <asp:RadioButton ID="gdRadio" runat="server" AutoPostBack="false"/>
                        </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="FileMarkId" HeaderText="<%$ Resources:gvHdrFileMarkIdResrc %> " UniqueName="FileMarkId" visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IRMarkId" HeaderText="<%$ Resources:gvHdrFileMarkResrc %> " UniqueName="IRMarkId">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IrFileMarkName" HeaderText="<%$ Resources:gvHdrFileMarkResrc %> " UniqueName="IrFileMarkName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Count" HeaderText="<%$ Resources:gvHdrCountResrc %> " UniqueName="Count">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TableName" HeaderText="<%$ Resources:gvHdrTableNameResrc %>" UniqueName="TableName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FieldName" HeaderText="<%$ Resources:gvHdrFieldNameResrc %>" UniqueName="FieldName">
                </telerik:GridBoundColumn>         
                <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:gvHdrOperatorResrc %>" UniqueName="Operator">
                </telerik:GridBoundColumn>  
                <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:gvHdrValueResrc %>" UniqueName="Value">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
            
    </telerik:RadGrid>
    </div>
    <asp:HiddenField ID="hdnRebind" runat="server" />
    <asp:HiddenField ID="hdnRowID" runat="server" />
    </form>
</body>
</html>
