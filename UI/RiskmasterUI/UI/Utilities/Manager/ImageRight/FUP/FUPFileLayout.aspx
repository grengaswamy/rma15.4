﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FUPFileLayout.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ImageRight.FUP.FUPFileLayout" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            UpdateAllColumns();

            if (parent.MDIScreenLoaded != null) {
                parent.MDIScreenLoaded();
            }

            $('input[type=text].riTextBox').change(function () {
                var iValue = $(this).val();
                iValue = Math.round(iValue);
                if (iValue < 0) {
                    iValue = 0;
                }
                $(this).val(iValue);
                var fldname = $(this).closest('tr').children('td:nth-child(1)').text();
                
                var reg = new RegExp(fldname + "(.*?)~", "g");
                $('#hdnChangedValues').val($('#hdnChangedValues').val().replace(reg, ''));
                $('#hdnChangedValues').val($('#hdnChangedValues').val() + fldname + ':' + iValue + '~');

                UpdateAllColumns();
            });

            $('input[title=Cancel]').click(function () {
                var fldname = $(this).closest('tr').children('td:nth-child(1)').text();
                var reg = new RegExp(fldname + "(.*?)~", "g");
                $('#hdnChangedValues').val($('#hdnChangedValues').val().replace(reg, ''));
            });
        });

        function UpdateAllColumns() {
            var firstCellValue = 1;
            var secondCellValue = 0;
            $('tbody tr').each(function () {
                firstCellValue = parseInt(firstCellValue) + parseInt(secondCellValue);
                $(this).children('td:nth-child(2)').each(function () {
                    $(this).text(firstCellValue);
                    firstCellValue = $(this).text();
                });
                $(this).children('td:nth-child(4)').each(function () {
                    if ($(this).find('input[type=text]').length > 0) {
                        secondCellValue = $(this).find('input[type=text]').val();
                    }
                    else {
                        secondCellValue = $(this).text();
                    }
                });
            });
        }
    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="toolBarButton" runat="server" id="div_create" xmlns="">
        <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_savemapping_active.png" class="bold" ToolTip=" <%$ Resources:btnSaveResrc %> " runat="server" OnClick="OnSaveClick"/>
    </div>
    <div>
        <asp:Label runat="server" class="msgheader" ID="lblcaption" Width="99.7%" Text=" <%$ Resources:lblDocumentMappingResrc %> "></asp:Label>  
    </div>
    <div>
        <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
        
    </telerik:RadCodeBlock>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        
    </telerik:RadAjaxManager>

    <telerik:RadGrid runat="server" ID="grdFUPFileLayout" AllowMultiRowEdit="true"
        AllowPaging="false" PageSize="50" AllowFilteringByColumn="false" AllowSorting="false"
        AutoGenerateColumns="false" Skin="Office2007"
         OnNeedDataSource="grdFUPFileLayout_OnNeedDataSource" OnItemDataBound="grdFUPFileLayout_ItemDataBound">
        <ClientSettings>
                <ClientEvents />                    
                <Scrolling SaveScrollPosition="true" AllowScroll="true" ScrollHeight="555px" UseStaticHeaders="true"/>
        </ClientSettings>

        <MasterTableView Width="85%" AllowCustomSorting="false" ClientDataKeyNames="FupFileLayoutId" EditMode="InPlace">
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                 <NoRecordsTemplate>
                  <div>
                     <asp:Label runat="server" Text="<%$ Resources:divNoRecordToDisplayResrc %>"></asp:Label>
                  </div>
                </NoRecordsTemplate>
            <Columns>
                <telerik:GridBoundColumn DataField="FupFileLayoutId" HeaderText="<%$ Resources:gvHdrLayoutIdResrc %>" UniqueName="FupFileLayoutId" Visible="false" ReadOnly="true">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FieldName" HeaderText="<%$ Resources:gvHdrFieldNameResrc %> " UniqueName="FieldName" ReadOnly="true" ItemStyle-Width="33%">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="StartPosition" HeaderText="<%$ Resources:gvHdrStartPositionResrc %> " UniqueName="StartPosition" ReadOnly="true" ItemStyle-Width="33%">
                </telerik:GridBoundColumn>
                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" ItemStyle-Width="1%" />
                <telerik:GridNumericColumn DataField="Length" HeaderText="<%$ Resources:gvHdrLengthResrc %>" UniqueName="Length" ItemStyle-Width="33%">
                </telerik:GridNumericColumn>
            </Columns>
        </MasterTableView>
            
    </telerik:RadGrid>
    </div>
    <asp:HiddenField ID="hdnChangedValues" runat="server"/>
    </form>
</body>
</html>
