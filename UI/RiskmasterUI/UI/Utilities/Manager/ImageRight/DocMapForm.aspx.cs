﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager.ImageRight
{
    public partial class DocMapForm : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnCloseWindow.Value = "False";
            if (!Page.IsPostBack)
            {
                string sMode = AppHelper.GetQueryStringValue("mode");
                if (string.Compare(sMode,"edit", true) == 0)
                {
                    IRDocMapId.Text = AppHelper.GetQueryStringValue("Id");
                    string sCWSResponse = string.Empty;
                    bool bReturnValue = CallCWSFunctionBind("DocumentMapAdaptor.Get", out sCWSResponse);
                }
            }
        }

        protected void OnBtnSaveClick(object sender, EventArgs e)
        {
            string sCWSResponse = string.Empty;
            XmlDocument xReturnDoc = new XmlDocument();
            bool bReturnValue = CallCWSFunctionBind("DocumentMapAdaptor.Save", out sCWSResponse);
            if (bReturnValue)
            {
                xReturnDoc.LoadXml(sCWSResponse);
                if (string.Compare(xReturnDoc.SelectSingleNode("//MsgStatusCd").InnerText,"Success",true)==0)
                {
                    hdnCloseWindow.Value = "True";
                }
            }
        }
    }
}