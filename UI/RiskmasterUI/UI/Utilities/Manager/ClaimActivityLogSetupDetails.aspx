﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimActivityLogSetupDetails.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ClaimActivityLogSetupDetails" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../../Scripts/zapatec/zpcal/themes/system.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/jscript" language="javascript" src="../../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="javascript" src="../../../Scripts/rmx-common-ref.js"></script>
    <script src="../../../Scripts/zapatec/zpwin/src/window.js" type="text/javascript"></script>
    <script src="../../../Scripts/zapatec/zpwin/src/dialog.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
    <script type="text/javascript" src="../../../Scripts/calendar-alias.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="javascript">
        function ClaimActLogSetupDetail_Save() {
            //tkatsarski: 11/27/14 - RMA-4878: If unexisting value is added for "Operation", the code stored in OpType_cid.value is "0"
            if ((document.forms[0].OpType_cid.value == "") || (document.forms[0].OpType_cid.value == "0")) {
                alert("Please select operation type for claim activity log setup.");
                return false;
            }
            else 
            {
                return true;
            }
        }
    
    </script>
</head>
<body>
    <form id="frmData" runat="server">
   <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    
    <div>
       <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
       <asp:ImageButton src="../../../Images/save.gif" alt="" id="Save" 
               onmouseover="javascript:document.all['Save'].src='../../../Images/save2.gif'" 
               onmouseout="javascript:document.all['Save'].src='../../../Images/save.gif'" 
               title="Save" runat="server" OnClientClick="return ClaimActLogSetupDetail_Save();"
               onclick="Save_Click"  /></td>
      </tr>
     </tbody>
    </table>
   </div><br /><br /><div class="msgheader" id="formtitle">Claim Activity Log Setup Details</div>
   
  
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colspan="2">New</td>
         </tr>
         <tr>
         
         <asp:TextBox rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='LogId']"  value="" style="display:none" id="LogId" runat="server" ></asp:TextBox>
         </tr>
         <tr>
          <td><u>Table Name</u>:&nbsp;&nbsp;
          </td>
          <td>
           <asp:DropDownList runat="server" AutoPostBack="true" id="DropDownList1" 
                  itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='Table']" 
                  rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Table']/@value" 
                  type="combobox"></asp:DropDownList>
          </td>
         </tr>
       
         <tr>
          <td><u>Operation</u>:&nbsp;&nbsp;
          </td>
          <td>
          <asp:TextBox runat="server" type="code" codetable="OPERATION_TYPE" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='OpType']"  size="30" onchange="lookupTextChanged(this);" id="OpType" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;"></asp:TextBox>
            <input type="button" class="CodeLookupControl" id="OpTypebtn" onclick="selectCode('OPERATION_TYPE','OpType')" />
            <asp:TextBox runat="server" style="display:none" id="OpType_cid" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='OpType']/@codeid" Text="" />
              </td>
         </tr>
         <tr>
          <td>Activity Type:&nbsp;&nbsp;
          </td>
          <td><asp:TextBox runat="server" type="code" codetable="CLAIM_ACTIVITY_TYPE" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ActType']"  size="30" onchange="lookupTextChanged(this);" id="ActType" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;"></asp:TextBox>
            <input type="button" class="CodeLookupControl" id="ActTypebtn" onclick="selectCode('CLAIM_ACTIVITY_TYPE','ActType')" />
            <asp:TextBox runat="server" style="display:none" id="ActType_cid" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ActType']/@codeid" Text="" />
            </td>
         </tr>
         <tr>
          <td>Log Text:&nbsp;&nbsp;
          </td>
          <td>
          <span class="formw">
          <asp:TextBox runat="Server" id="ev_LogText" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='LogText']" RMXType="memo" readonly="true" 
          style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="7" TextMode="MultiLine" Columns="30" rows="5" />
           <input type="button" class="button" value="..." name="ev_LogTextbtnMemo" tabindex="8" id="ev_LogTextbtnMemo"
                    onclick="EditHTMLMemo('ev_LogText','yes')" />
           <asp:textbox style="display: none" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='LogText_HTMLComments']"
                        id="ev_LogText_HTML" />
          </span>
          </td>
         </tr>
        </tbody>
       </table>
             
       <input type="hidden" runat="server" id="hdntitle" />
    </form>
</body>
</html>
