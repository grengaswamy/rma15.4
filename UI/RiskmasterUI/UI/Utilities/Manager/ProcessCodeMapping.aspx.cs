﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class ProcessCodeMapping : NonFDMBasePageCWS
    {
        #region "Constants"
        public string sProcessId = string.Empty;
        public string Data = "";
        XElement XmlTemplate = null;
        bool bReturnStatus = false;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["processID"] != null)
            {
                sProcessId = Request.QueryString["processID"];
            }

            DataTable dtGridData = new DataTable();

            DataRow objRow;
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ProcessCodeMapping.aspx"), "ProcessCodeMappingValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ProcessCodeMappingValidations", sValidationResources, true);
            if (!IsPostBack)
            {
                XmlTemplate = GetMessageTemplateForProcessName(sProcessId);
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetProcessName", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(Data);
                    XmlNodeList xmlNodeList = xmldoc.SelectNodes("//AvailableProcessList/ProcessType");
                    if (xmlNodeList.Count > 0)
                    {
                        foreach (XmlNode objNodes in xmlNodeList)
                        {
                            lblProcessTableName.Text = objNodes.Attributes["ProcessName"].Value.ToString() + " Table Name";                            
                        }
                        
                    }
                    GetTableData();
                    
                }
                lblTPApage.Text = "Third Party Interface Table Mapping";
                BindGridData();
            }
        }

        private XElement GetMessageTemplateForTableTypes()
        {
            StringBuilder sXML = new StringBuilder();
            XElement oTemplate = null;

            sXML.Append("<Message><Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization>");
            sXML.Append("<Call><Function>TPACodeMappingAdaptor.GetTableDetails</Function></Call>");
            sXML.Append("<Document><RMTables></RMTables></Document></Message>");
            oTemplate = XElement.Parse(sXML.ToString());
            return oTemplate;
        }

        public void GetTableData()
        {
            try
            {
                XElement XmlTemplate = null;
                string Data = "";
                DataSet oDS = null;
                oDS = new DataSet();
                XmlTemplate = GetMessageTemplateForTableTypes();
                bool bReturnStatus = false;
                

                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetTableDetails", XmlTemplate, out Data, false, true);
                if (Data != null && !string.IsNullOrEmpty(Data))
                {
                    oDS.ReadXml(new StringReader(Data));
                    ListItem item = null;
                    if (oDS != null)
                    {
                        if (oDS.Tables["option"] != null)			//Ankit Start : Worked on MITS - 34657
                        {			//Ankit Start : Worked on MITS - 34657
                            for (int i = 0; i < oDS.Tables["option"].Rows.Count; i++)
                            {
                                item = new ListItem();
                                item.Text = oDS.Tables["option"].Rows[i]["option_text"].ToString();
                                item.Value = oDS.Tables["option"].Rows[i]["value"].ToString();
                                lstProcessTableName.Items.Add(item);
                                lstRMTableName.Items.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void addTPABtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateForAdd();
                string Data = "";
                bool bReturnStatus = false;
                txtProcessTableId.Text = lstProcessTableName.SelectedValue;
                txtRMTableId.Text = lstRMTableName.SelectedValue;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.AddNewTableMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        txtProcessTableId.Text = "";
                        txtRMTableId.Text = "";
                    }

                    else
                    {
                        BindGridData();
                        txtProcessTableId.Text = "";
                        txtRMTableId.Text = "";

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void removeTPBtn_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            try
            {
                XElement XmlTemplate = null;
                string sPolCode = "";
                XmlTemplate = GetMessageTemplateForDeletion();
                string Data = "";
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.DeleteTableMapping", XmlTemplate, out Data, true, false);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(Data);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        txtProcessTableId.Text = "";
                        txtRMTableId.Text = "";
                    }

                    else
                    {
                        BindGridData();
                        txtProcessTableId.Text = "";
                        txtRMTableId.Text = "";
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        

        private XElement GetMessageTemplateForAdd()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.AddNewTableMapping");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor><AddCode>");
            sXml = sXml.Append("<ProcessTable>" + lstProcessTableName.SelectedValue + "</ProcessTable>");
            sXml = sXml.Append("<RMTable>" + lstRMTableName.SelectedValue + "</RMTable>");
            sXml = sXml.Append("<ProcessID>" + sProcessId + "</ProcessID>");
            sXml = sXml.Append("</AddCode></TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForProcessName(string sProcessId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.GetProcessName");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor>");
            sXml = sXml.Append("<ProcessId>" + sProcessId + "</ProcessId>");
            sXml = sXml.Append("</TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void BindGridData()
        {
            string data = string.Empty;
            DataTable dtGridData = new DataTable();
            DataRow objRow = null;
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            try
            {
                XmlTemplate = GetMessageTemplateForTableMappings();
                bReturnStatus = CallCWS("TPACodeMappingAdaptor.GetTableMapping", XmlTemplate, out data, false, true);
                dtGridData.Columns.Add("CBId");
                dtGridData.Columns.Add("ProcessTable");
                dtGridData.Columns.Add("RMATable");
                if (bReturnStatus)
                {

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(data);
                    string sRowId = string.Empty;
                    string sRMATable = string.Empty;
                    string sProcessTable = string.Empty;
                    XmlNodeList xmlNodeList = xmldoc.SelectNodes("//AvailableTableList/TableMapping");
                    if (xmlNodeList.Count > 0)
                    {
                        foreach (XmlNode objNodes in xmlNodeList)
                        {
                            objRow = dtGridData.NewRow();
                            sRowId = objNodes.Attributes["MapRowId"].Value.ToString();
                            sRMATable = objNodes.Attributes["RMXTable"].Value.ToString(); //spahariya MITS 30911
                            sProcessTable = objNodes.Attributes["ThirdPartyTable"].Value.ToString();//spahariya MITS 30911
                            objRow["CBId"] = sRowId;
                            objRow["ProcessTable"] = sProcessTable;
                            objRow["RMATable"] = sRMATable;
                            dtGridData.Rows.Add(objRow);
                        }
                        GridViewAvailableTPA.DataSource = dtGridData;
                        GridViewAvailableTPA.DataBind();
                    }
                }
                if (dtGridData.Rows.Count <= 0)
                {
                    objRow = dtGridData.NewRow();
                    objRow["CBId"] = "";
                    objRow["ProcessTable"] = "";
                    objRow["RMATable"] = "";

                    dtGridData.Rows.Add(objRow);
                    GridViewAvailableTPA.DataSource = dtGridData;
                    GridViewAvailableTPA.DataBind();
                    GridViewAvailableTPA.Rows[GridViewAvailableTPA.Rows.Count - 1].Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (dtGridData != null)
                    dtGridData = null;
            }

        }

        private XElement GetMessageTemplateForTableMappings()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.GetTableMapping");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor>");
            sXml = sXml.Append("<AddCode></AddCode>");
            sXml = sXml.Append("<ProcessId>" + sProcessId + "</ProcessId>");
            sXml = sXml.Append("</TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.DeleteTableMapping");
            sXml = sXml.Append("</Function></Call><Document><TPAMapping><DeleteCode>");
            sXml = sXml.Append("<DeletedCode>" + hdnSelected.Text + "</DeletedCode>");
            sXml = sXml.Append("<ProcessId>" + sProcessId + "</ProcessId>");
            sXml = sXml.Append("</DeleteCode></TPAMapping></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        private XElement GetMessageTemplateForRefresh()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("TPACodeMappingAdaptor.RefershNewMapping");
            sXml = sXml.Append("</Function></Call><Document><TPACodeMappingAdaptor><RefreshCode>");
            sXml = sXml.Append("</RefreshCode></TPACodeMappingAdaptor></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void lstRMTableName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sSelProcessTable = string.Empty;
            string sSelRMTable = string.Empty;
            sSelProcessTable = lstProcessTableName.SelectedValue;
            sSelRMTable = lstRMTableName.SelectedValue;
            txtProcessTableId.Text = sSelProcessTable;
            txtRMTableId.Text = sSelRMTable;
        }       
        

    }
}