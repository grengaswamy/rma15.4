﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExpConParms.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.ExpConParms" ValidateRequest="false"%>

<%@ Register TagName="PleaseWaitDialog" TagPrefix="uc2" Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add/Modify Expense Constant</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">        { var i; }</script>
        <%--vkumar258 - RMA-6037 - Starts --%>

<%--    <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
<link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/drift.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/dhtml-div.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/dhtml-help-setup.js"></script>

    <uc3:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" method="post" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" OnClientClick="return ValidateFieldsforExpConParms();"
                OnClick="save_Click" /></div>
    </div>
    <div class="msgheader" id="formtitle">
        Add/Modify Expense Constant</div>
    <table border="0">
        <tr>
            <td class="ctrlgroup" colspan="2">
                Expense Constant Parameters
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_useExpCon" Text="Use Expense Constant:" />
            </td>
            <td>
                <asp:CheckBox runat="server" name="useExpCon" rmxref="/Instance/Document/Document/ExpCon/use" ID="useExpCon" onchange="ApplyBool(this);"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_LOBcode" Text="Line of Business:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ID="LOBcode" CodeTable="POLICY_LOB" ControlName="LOBcode" 
                    type="code" class="required" rmxref="/Instance/Document/Document/ExpCon/lob"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Statecode" Text="State:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ID="Statecode" CodeTable="states" ControlName="Statecode"
                    type="code" class="required" rmxref="/Instance/Document/Document/ExpCon/state" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Amount" Text="Amount:" class="required" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="Amount" rmxref="/Instance/Document/Document/ExpCon/amount" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_ExpConType" Text="Flat Or Percent:" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ControlName="ExpConType" ID="ExpConType" type="code"
                    CodeTable="DISCOUNT_TYPE" class="required" rmxref="/Instance/Document/Document/ExpCon/expcontype" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_EffectiveDate" Text="Effective Date:" class="required" />&nbsp;&nbsp;
            </td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document/Document/ExpCon/effectivedate"
                    RMXType="date" TabIndex="2" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                              <%--vkumar258 - RMA-6037 - Starts --%>

                 <%-- <asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "EffectiveDate",
				                ifFormat: "%m/%d/%Y",
				                button: "EffectiveDatebtn"
				            }
				            );
                </script>--%>

<script type="text/javascript">
    $(function () {
        $("#EffectiveDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
            //buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "3");
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" class="label" ID="lbl_ExpirationDate" Text="Expiration Date:" />&nbsp;&nbsp;
            </td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" ID="ExpirationDate" RMXRef="/Instance/Document/Document/ExpCon/expirationdate"
                    RMXType="date" TabIndex="4" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                            <%--vkumar258 - RMA-6037 - Starts --%>
   <%-- <asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" TabIndex="3" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
				            {
				                inputField: "ExpirationDate",
				                ifFormat: "%m/%d/%Y",
				                button: "ExpirationDatebtn"
				            }
				            );
                </script>--%>
<script type="text/javascript">
    $(function () {
        $("#ExpirationDate").datepicker({
            showOn: "button",
            buttonImage: "../../../Images/calendar.gif",
           // buttonImageOnly: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeYear: true
        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
    });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
            </td>
        </tr>
    </table>
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/Document/ExpCon/expconrowid" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="FormMode" />
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    </form>
</body>
</html>
