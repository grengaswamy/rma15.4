﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LobOptions.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.LobOptions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Riskmaster</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
    <script language="javascript" type="text/javascript">
            function getQval(name) {
                var m, Q = window.location.search.substring(1);
                if ('' != Q) {
                    var re = new RegExp(escape(name) + '=([^&$]+)');
                    if (m = Q.match(re))
                        return m[1];
                    else
                        return '';
                }
                return '';
            }
            function Save() {
                if (getQval('DupCriteriaType') == "Policy") {
                    window.opener.document.forms[0].hdnAction.value = "setCriteriaForPolicyDup";
                    window.opener.document.forms[0].hdnPolClaimDupDays.value = document.forms[0].txtHowMany.value;
                }
                else {
                    window.opener.document.forms[0].hdnAction.value = "setCriteria";
                    window.opener.document.forms[0].hdnValue.value = document.forms[0].txtHowMany.value;
                }
                window.opener.document.forms[0].hdnCriteria.value = "Y";
                window.opener.document.forms[0].submit();
                self.close();
            }
			function numLostFocus(objCtrl)
			{
				if(objCtrl.value.length==0)
				{
					objCtrl.value="0";
					return false;
				}
				if(isNaN(parseFloat(objCtrl.value)))
					objCtrl.value="";
				else
					objCtrl.value=parseFloat(objCtrl.value);
				if(objCtrl.value.length==0)
				{
					objCtrl.value="0";
				}
				return true;
			}
			function Close()
			{
				self.close();
			}
			
			</script>
</head>
<body>
    <form id="frmOpen" runat="server">
    <div>
     <table width="95%" border="0" align="center" id="Table3">
            <tr>
              <td class="msgheader" colspan="2">Number Of Days</td>
            </tr>
            <tr>
              <td>How many days from the Event Date/Claim Date?</td>
              <td>   <%--srajindersin MITS 28176:set maxlength to 4--%>
       				<input type="text" id="txtHowMany" value="" name="txtHowMany" maxlength="4" onblur="numLostFocus(this)">
              </td>
            </tr>
                  
            <tr>
              <td colspan="2"  align="center">
                <BR/>
                <input type="button" class="button" value="  OK  " onClick="Save();" id="Button1" name="Button1"/>
                <input type="button" class="button" value="Cancel" onClick="Close();" id="Button2" name="Button2"/>
              </td>
            </tr>
			  </table>
			 
    </div>
    </form>
     <script language="javascript" type="text/javascript">
         //document.frmOpen.txtHowMany.value = "0";
         document.forms[0].txtHowMany.value = "0";//Bharani - MITS : 36186
	</script>
</body>
</html>
