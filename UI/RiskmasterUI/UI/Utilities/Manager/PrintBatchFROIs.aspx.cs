﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions
{
    public partial class PrintBatchFROIs : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        XmlDocument oFDMPageDom = new XmlDocument();
        string sReturn = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes


            if (!IsPostBack)
            {
                XmlTemplate = GetMessageTemplate();
                CallService(XmlTemplate, ref oFDMPageDom, ref sReturn);
                PrintBatchFroiGrid.BindData(oFDMPageDom);
                
            }

            //else
            //{
            //    XmlTemplate = GetMessageTemplate("refresh");
            //    CallService(XmlTemplate, ref oFDMPageDom, ref sReturn);
            //    PrintBatchFroiGrid.BindData(oFDMPageDom);
            //}




        }


        protected void btnRefersh_Click(object sender, EventArgs e)
        {
            XmlTemplate = GetMessageTemplate("refresh");
            CallService(XmlTemplate, ref oFDMPageDom, ref sReturn);
            PrintBatchFroiGrid.BindData(oFDMPageDom);
        }

        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            ErrorControl1.errorDom = p_sReturn;
            Exception ex = new Exception("Service Error");
            if (ErrorControl1.errorFlag == true)
                throw ex;
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.Save");
            if (bReturnStatus)
                bReturnStatus = CallCWSFunction("PrintBatchFroiAdaptor.Get");

        }
        protected void Drp_Save(object sender, EventArgs e)
        {
            NonFDMCWSPageLoad("PrintBatchFroiAdaptor.Get");
        }

        private XElement GetMessageTemplate()
        {
            
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PrintBatchFroiAdaptor.Get</Function></Call><Document>");
            sXml = sXml.Append("<PassToWebService><PrintBatchFroi><listhead>");
            sXml = sXml.Append("<ClaimNumber>Claim Number</ClaimNumber><ClaimDate>Claim Date</ClaimDate><ClaimantName>Claimant Name</ClaimantName><Jurisdiction>Jurisdiction</Jurisdiction>");
            sXml = sXml.Append("</listhead></PrintBatchFroi>");
            sXml = sXml.Append("<RowId></RowId><FromDate></FromDate><ToDate></ToDate><PrintedClaimsToInclude></PrintedClaimsToInclude><State1></State1><State2></State2></PassToWebService>");
            sXml = sXml.Append("</Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplate(string selectedRowId)
        {


            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PrintBatchFroiAdaptor.Get</Function></Call><Document>");
            sXml = sXml.Append("<PassToWebService><PrintBatchFroi><listhead>");
            sXml = sXml.Append("<ClaimNumber>Claim Number</ClaimNumber><ClaimDate>Claim Date</ClaimDate><ClaimantName>Claimant Name</ClaimantName><Jurisdiction>Jurisdiction</Jurisdiction>");
            sXml = sXml.Append("</listhead></PrintBatchFroi><RowId></RowId>");
            sXml = sXml.Append("<FromDate>");
            sXml = sXml.Append(FromDate.Text);
            sXml = sXml.Append("</FromDate>");
            sXml = sXml.Append("<ToDate>");
            sXml = sXml.Append(ToDate.Text);
            sXml = sXml.Append("</ToDate>");
            sXml = sXml.Append("<PrintedClaimsToInclude>");
            sXml = sXml.Append(PrintedClaimsToInclude.Text);
            sXml = sXml.Append("</PrintedClaimsToInclude>");
            sXml = sXml.Append("<State1>");
            sXml = sXml.Append(State1.Checked);
            sXml = sXml.Append("</State1>");
            sXml = sXml.Append("<State2>");
            sXml = sXml.Append(State2.Checked);
            sXml = sXml.Append("</State2>");
            sXml = sXml.Append("</PassToWebService></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        private XElement GetMessageTemplateForSave()
        {


            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PrintBatchFroiAdaptor.Save</Function></Call><Document>");
            sXml = sXml.Append("<PassToWebService><PrintBatchFroi><listhead>");
            sXml = sXml.Append("<ClaimNumber>Claim Number</ClaimNumber><ClaimDate>Claim Date</ClaimDate><ClaimantName>Claimant Name</ClaimantName><Jurisdiction>Jurisdiction</Jurisdiction>");
            sXml = sXml.Append("</listhead></PrintBatchFroi><RowId></RowId>");
            sXml = sXml.Append("<FromDate>");
            sXml = sXml.Append(FromDate.Text);
            sXml = sXml.Append("</FromDate>");
            sXml = sXml.Append("<ToDate>");
            sXml = sXml.Append(ToDate.Text);
            sXml = sXml.Append("</ToDate>");
            sXml = sXml.Append("<PrintedClaimsToInclude>");
            sXml = sXml.Append(PrintedClaimsToInclude.Text);
            sXml = sXml.Append("</PrintedClaimsToInclude>");
            sXml = sXml.Append("<State1>");
            sXml = sXml.Append(State1.Checked);
            sXml = sXml.Append("</State1>");
            sXml = sXml.Append("<State2>");
            sXml = sXml.Append(State2.Checked);
            sXml = sXml.Append("</State2>");
            sXml = sXml.Append("<SelectedRowId>");
            sXml = sXml.Append(PrintBatchFroiSelectedId.Text);
            sXml = sXml.Append("</SelectedRowId>");
            sXml = sXml.Append("</PassToWebService></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }


        protected void btnOK_Click(object sender, EventArgs e)
        {

            XmlTemplate = GetMessageTemplateForSave();
            CallService(XmlTemplate, ref oFDMPageDom, ref sReturn);
            PrintBatchFroiGrid.BindData(oFDMPageDom);

        }
       


    }
}
