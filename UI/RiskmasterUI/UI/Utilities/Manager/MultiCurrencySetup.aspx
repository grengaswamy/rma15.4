﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiCurrencySetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.MultiCurrencySetup" EnableViewStateMac="false" ValidateRequest="false" %>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
  <title>Multi Currency Setup</title>
  <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">    { var i; }
</script>
  </head>
  <body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div>
          <span>
            <dg:UserControlDataGrid runat="server" ID="MultiCurrencySetupGrid" GridName="MultiCurrencySetupGrid" GridTitle="Exchange Rate Setup" 
            Target="/Document/ExchangeRates/ExchangeRateList" Ref="/Instance/Document/form//control[@name='MultiCurrencySetupGrid']" 
            Unique_Id="CurrencyRowId" ShowRadioButton="true" Width="" Height="800" hidenodes="|CurrencyRowId|" ShowHeader="True" LinkColumn="" 
            PopupWidth="500" PopupHeight="250" Type="GridAndButtons" RowDataParam="listrow" OnClick="KeepRowForEdit('MultiCurrencySetupGrid');"/>
          </span>
        </div>
          <asp:TextBox style="display:none" runat="server" id="MultiCurrencySetupSelectedId"  RMXType="id" />
         <asp:TextBox style="display:none" runat="server" id="MultiCurrencySetupGrid_RowDeletedFlag"  RMXType="id" Text="false" />
         <asp:TextBox style="display:none" runat="server" id="MultiCurrencySetupGrid_Action"  RMXType="id" />
         <asp:TextBox style="display:none" runat="server" id="MultiCurrencySetupGrid_RowAddedFlag"  RMXType="id" Text="false"  />
      </div>
      
    </form>
  </body>
</html>
