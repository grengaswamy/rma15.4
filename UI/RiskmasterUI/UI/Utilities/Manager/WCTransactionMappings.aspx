﻿<%@ Page Title="" Language="C#" theme="RMX_Default" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="WCTransactionMappings.aspx.cs" Inherits="Riskmaster.UI.Utilities.WCTransactionMappings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>
        WC Transaction Mapping
    </title>
       <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
       <script language="javascript" type="text/javascript" >
       
        function Selected()
				{
				    var benefitsList=document.forms[0].WCBenefits.value;
				    var transactionType=document.forms[0].WCTransactionType.value;
				    if (trim(benefitsList)==""  || trim(transactionType)=="")
					{
						 alert("Benefit and Transaction Type must be selected before adding to mappings.");	
						 return false;
					}
					return true;
				}
				function SaveMapping()
				{
					arr=document.forms[0].hdnWCBenefitsList.value.split("**");
					str=document.forms[0].WCBenefits.value+"|"+document.forms[0].WCTransactionType.value;
					if (Selected())
				    {
						for(i=0;i<arr.length;i++)
						{
							if (arr[i]==str)
							{ 
								alert("The combination of Benefit and Transaction Type are currently \n part of the mapping and will not be added.");
								return false;
							}
						}
					   return true;
					}
					else
					{
					 return false;
					}	
					return true;
	            }
	            //Start: rsushilaggar MITS 18734 06/16/2010
	            function DeleteMapping() {
	                var IsRowSelected = false;
	                var bCheckBoxes = self.document.all.tags("input");
	                var GVList1Id; var GVList1Row = 0;
	                for (var i = 0; i < bCheckBoxes.length - 1; i++) {
	                    GVList1Id = 'gvWorkersCompensation_selectchk_';
	                    //if (GVList1Row < 10) {
	                    //    GVList1Id = GVList1Id + '0';
	                    //}
	                    GVList1Id = GVList1Id + GVList1Row;
	                    if (bCheckBoxes[i].id == GVList1Id) {
	                        if (bCheckBoxes[i].checked == true) {
	                            IsRowSelected = true;
	                            break;
	                        }
	                        GVList1Row = GVList1Row + 1;
	                    }
	                }
	                if (!IsRowSelected) {
	                    alert("Benefits and Transactions must be selected before Deleting a Mapping.");
	                    return false;
	                }
	                return true;
	            }
	            //End rsushilaggar
        
        
        
     </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
 <form id="frmData" runat="server" name="frmData">
   
    <table border="0" width="100%">
        <tr>
           <td>
               <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
           </td>
        </tr>
        <tr>
            <td class="msgheader" colspan="4">
                Worker's Compensation Transaction Type Setup
            </td>
        </tr>
        
        <tr>
            <td class="ctrlgroup" colspan="4">
                A benefit can have an unlimited number of Transaction Types
                <br />
                A Transaction Type should be used only one time
            </td>
        </tr>
        
        <tr>
            <td colspan="4">
                Select jurisdiction :
                <asp:dropdownlist id="WCJurisdictions" name="WCJurisdictions" 
                   rmxref="/Instance/Document/form/control[@name ='WCJurisdictions']" 
                    itemSetref="/Instance/Document/form/control[@name ='WCJurisdictions']"  
                    runat="server" AutoPostBack="True" 
                    onselectedindexchanged="WCJurisdictions_SelectedIndexChanged" />
            </td>
        </tr>
        
        <tr>
            <td>
                Benefit
            </td>
            <td colspan="4">
                Transaction Type
            </td>
        </tr>
        
        <tr>
            <td>
                <asp:dropdownlist id="WCBenefits" name="WCBenefits" itemSetref="/Instance/Document/form/control[@name ='WCBenefits']" rmxref="/Instance/Document/form/control[@name ='WCBenefits']"  runat="server" />
            </td>
            <td>
                <asp:dropdownlist id="WCTransactionType" name="WCTransactionType" rmxref="/Instance/Document/form/control[@name ='WCTransactionType']"  itemSetref="/Instance/Document/form/control[@name ='WCTransactionType']" runat="server" />
            </td>
            <td colspan="2">
                <asp:button id="btSaveMapping" name="btSaveMapping" text="Save Mapping" class="button"
                    runat="server" onclick="btSaveMapping_Click" OnClientClick="return SaveMapping()" />
                <asp:button id="btDeleteMapping" name="btDeleteMapping" text="Delete Mapping" class="button"
                    runat="server" onclick="btDeleteMapping_Click" OnClientClick ="return DeleteMapping()" />
                    <%--rsushilaggar MITS 18734 06/16/2010--%>
            </td>
        </tr>
        
        <tr>
            <td colspan="4" class="ctrlgroup">
                Worker's Compensation
            </td>
        </tr>
        
        <tr>
            <td colspan="4">
                 <asp:TextBox ID="WCBenefitsList"   style="display:none" runat="server" rmxref="/Instance/Document/form/control[@name ='WCBenefitsList']" ></asp:TextBox>
                 <asp:TextBox ID="hdnWCBenefitsList"   style="display:none" runat="server"></asp:TextBox>
                 
                  <asp:gridview id="gvWorkersCompensation" AutoGenerateColumns="false" CssClass="singleborder" 
                     name="gvWorkersCompensation"  width="100%" runat="server" 
                     onrowdatabound="gvWorkersCompensation_RowDataBound" ondatabound="gvWorkersCompensation_DataBound">
                   <HeaderStyle CssClass="colheader5" />
		           <AlternatingRowStyle CssClass="data2" />
		           
		            <Columns>
		        <asp:TemplateField  ItemStyle-CssClass="data" ControlStyle-Width="5%">
                    <ItemTemplate>
                       <asp:CheckBox id="selectchk" name="selectchk" runat="server" />
                    </ItemTemplate> 
                </asp:TemplateField>
                       
                          <asp:TemplateField HeaderText="Benefit Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Size="Small" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblWCBenefitType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WCBenefitType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Transaction Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Size="Small" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblWCTransactionType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WCTransactionType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Use in Calculator" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White"  HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                      <asp:Label ID="lblUseCalc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UseCalc")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
            </Columns>
                  
                  </asp:gridview>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
