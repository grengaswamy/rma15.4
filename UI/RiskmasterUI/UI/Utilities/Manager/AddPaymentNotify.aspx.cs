﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Riskmaster.UI.FDM;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class AddPaymentNotify :GridPopupBase 
    {
       // string strPaymentNotifyValues = string.Empty;
       // XElement XmlTemplate = null;
        string sreturnValue = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
          //  strPaymentNotifyValues =Session["PaymentNotifyValues"].ToString();
            if (!IsPostBack)
            {
                NonFDMCWSPageLoad("CheckOptionsAdaptor.GetAddNotifyValues");
                gridname.Text = AppHelper.GetQueryStringValue("gridname");
                mode.Text = AppHelper.GetQueryStringValue("mode");
                selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

            }
        }

       
        //private XElement GetMessageTemplate()
        //{

        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml.Append("<Authorization>13cdbe38-ddc2-4161-b08c-0ce9d342a652</Authorization>");
        //    sXml.Append("<Call><Function></Function></Call><Document><PaymentNotification><control name=\"");
        //    sXml.Append("LOB\">");
        //    sXml.Append(LOB.SelectedItem.Value);
        //    sXml.Append("</control>");
        //    sXml.Append("<control name=\"UserNotify\">");
        //    sXml.Append(UserNotify.SelectedItem.Value);
        //    sXml.Append("</control>");
        //    sXml.Append("<control name=\"NPeriod\">");
        //    sXml.Append(NotifyPeriod.SelectedItem.Value);
        //    sXml.Append("</control>");
        //    sXml.Append("<control name=\"NotifyPeriod\" title=\"Notify Period\" type=\"combobox\" value=\"\">");
        //    sXml.Append("<option value=\"0\">Every Log In</option>");
        //    sXml.Append("<option value=\"1\">Monday</option>");
        //    sXml.Append("<option value=\"2\">Tuesday</option>");
        //    sXml.Append("<option value=\"3\">Wednesday</option>");
        //    sXml.Append("<option value=\"4\">Thursday</option>");
        //    sXml.Append("<option value=\"5\">Friday</option>");
        //    sXml.Append("</control>");
        //    sXml.Append("<control name=\"DateCriteria\">");
        //    sXml.Append(DateCriteria.SelectedItem.Value);
        //    sXml.Append("</control>");
        //    sXml.Append("<control name=\"NotifyList\">");
        //    sXml.Append("</control>");
        //    sXml.Append("<RecordId>" + hdnRecordId.Text + "</RecordId>");
        //    sXml.Append("<PaymentNotifyValues>" + strPaymentNotifyValues + "</PaymentNotifyValues>");
        //    sXml.Append("</PaymentNotification></Document></Message>");
        //    XElement oTemplate = XElement.Parse(sXml.ToString());
        //    return oTemplate;
        //}

        //protected void btnOk_Click(object sender, ImageClickEventArgs e)
        //{
        //    if (hdnRecordId.Text == "")
        //    {
        //        hdnRecordId.Text = "0";
        //    }
        //    else
        //    {
        //        hdnRecordId.Text = (int.Parse(hdnRecordId.Text) - 1).ToString();
        //    }
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "Script", "<script>GetDataFromOpener();</script>");
        //}
    }
}
