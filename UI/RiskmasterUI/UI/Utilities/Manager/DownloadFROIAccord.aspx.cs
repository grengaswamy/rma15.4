﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.DocumentService;
using Riskmaster.UI.Shared;
using System.Web.UI;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using Riskmaster.UI.MDAService;
using Riskmaster.Models;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class DownloadFROIAccord : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string tempFileName = string.Empty;
            int iDocId = 0;
            DocumentBusinessHelper dc = null;
            if (!IsPostBack)
            {
                dc = new DocumentBusinessHelper();
                btnActDownload.Attributes.Add("style", "DISPLAY: none");
                tempFileName = Server.MapPath("~/UploaderTemp/" + System.Guid.NewGuid().ToString());
                ViewState["tempfilename"] = tempFileName + "_completed";

                if (AppHelper.GetQueryStringValue("DocumentId") != "")
                {

                    iDocId = Convert.ToInt32(AppHelper.GetQueryStringValue("DocumentId"));
                }
                
                imgProcessing.Visible = true;
                lblDownloading.Visible = true;
                btnBack.Visible = false;
                dc.DownloadLargeFile(iDocId, tempFileName);
                Timer1.Enabled = true;
            }

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            string tempFileName = string.Empty;
            FileStream targetStream = null;
            try
            {
                tempFileName = ViewState["tempfilename"].ToString();
                if (File.Exists(tempFileName))
                {
                    Timer1.Enabled = false;

                    string script = "OpenPage('completed')";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "handleDownload", script, true);
                }
                else if (File.Exists(tempFileName.Replace("_completed", "_error")))
                {
                    Timer1.Enabled = false;
                    string script = "OpenPage('error')";
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "handleDownload", script, true);
                }

            }
            catch (FaultException<Riskmaster.Models.RMException> ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void Timer2_Tick(object sender, EventArgs e)
        {
            string tempFileName = string.Empty;
            FileStream targetStream = null;
            try
            {
                Timer2.Enabled = false;

                string script = "OpenPage('downloadStart')";
                ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "handleDownload", script, true);

            }
            catch (FaultException<Riskmaster.Models.RMException> ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                Timer1.Enabled = false;
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("LinkBatchFroiAcord.aspx", true);
        }

        protected void btnActDownload_Click(object sender, EventArgs e)
        {
            FileStream targetStream = null;
            string tempFileName = string.Empty;
            long dataLengthToRead = 0;
            int blockSize = 0;
            long count = 0;
            byte[] buffer = null;

            try
            {
                tempFileName = ViewState["tempfilename"].ToString();

                if (Request.Params.Get("__EVENTARGUMENT") == "error")
                {
                    imgProcessing.Visible = false;
                    lblDownloading.Visible = false;
                    btnBack.Visible = true;
                    File.Delete(tempFileName.Replace("_completed", "_error"));
                    throw new ApplicationException("Error has been Occured while Downlaoding document,please see log for details");
                }
                else if (Request.Params.Get("__EVENTARGUMENT") == "completed")
                {
                    imgProcessing.Visible = false;
                    lblDownloading.Visible = false;
                    btnBack.Visible = true;

                    Timer2.Enabled = true;
                }
                else
                {
                    using (targetStream = new System.IO.FileStream(tempFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                    {
                        dataLengthToRead = targetStream.Length;
                        blockSize = dataLengthToRead >= 500 ? 500 : (int)dataLengthToRead;
                        buffer = new byte[blockSize];
                        targetStream.Seek(0, SeekOrigin.Begin);
                        Response.Clear();

                        // Clear the content of the response
                        Response.ClearContent();
                        Response.ClearHeaders();

                        // Buffer response so that page is sent
                        // after processing is complete.
                        Response.BufferOutput = false;

                        // Add the file name and attachment,
                        // which will force the open/cance/save dialog to show, to the header
                        //Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(ViewState["FileName"].ToString()));

                        //Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode("froiacord.pdf"));
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode( AppHelper.GetQueryStringValue("filename").ToString()));

                        // Add the file size into the response header
                        Response.AddHeader("Content-Length", targetStream.Length.ToString());

                        // Set the ContentType
                        Response.ContentType = "application/octet-stream";

                        while ((count = targetStream.Read(buffer, 0, blockSize)) > 0)
                        {
                            Response.OutputStream.Write(buffer, 0, (int)count);
                            Response.Flush();
                        }
                        Response.Flush();
                        Response.Close();
                    }

                    targetStream.Close();

                    imgProcessing.Visible = false;
                    lblDownloading.Visible = false;
                    btnBack.Visible = true;
                    File.Delete(tempFileName);

                    // End the response
                    Response.End();
                }
            }
            catch (FaultException<Riskmaster.Models.RMException> ee)
            {
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            //mkaran2 - MITS #32088 - start
            catch (HttpException ee)
            {
                // delete temp file
                if (File.Exists(tempFileName))
                    File.Delete(tempFileName);

                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } //mkaran2 - MITS #32088 - end
            catch (Exception ee)
            {
                btnBack.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}