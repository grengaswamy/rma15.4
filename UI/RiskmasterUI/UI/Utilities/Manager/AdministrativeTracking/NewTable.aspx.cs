﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager.AdministrativeTracking
{
    public partial class NewTable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           TableType.Value = AppHelper.GetQueryStringValue("TableType");
        }

        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {

            string strAdmintrackingSrvcOutput = string.Empty;
            XmlDocument xmlAdmintrackingDoc = new XmlDocument();
            XmlNode inputDocNode = null;
            
            string serviceMethodToCall = string.Empty;
    

            try
            {
                inputDocNode = InputForCWS();

                strAdmintrackingSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

                ErrorControl1.errorDom = strAdmintrackingSrvcOutput;

                xmlAdmintrackingDoc.LoadXml(strAdmintrackingSrvcOutput);

                if ((xmlAdmintrackingDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
                {
                    ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
                
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }



        private XmlNode InputForCWS()
        {

            XmlNode xmlNode = null;
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
              <Message>
              <Authorization></Authorization>
              <Call>
                <Function>TableManagerAdaptor.CreateTable</Function>
              </Call>
              <Document>
                 <form>
                   <group name='NewTable' selected='1' title='New Table'>
                        <displaycolumn>
                          <control name='RowId' type='id' /> 
                          <control name='UserTableName' required='yes' title='User Table Name' type='text'>" + UserTableName.Value + @"</control> 
                          </displaycolumn>
                        <displaycolumn>
                          <control name='SysTableName' required='yes' title='System Table Name' type='text'>" + SysTableName.Value + @"</control> 
                          </displaycolumn>
                        <displaycolumn>
                          <control name='TableType' title='' type='hidden'>" + TableType.Value + @"</control> 
                          <control name='Org_orglist' type='hidden' /> 
                          </displaycolumn>
                  </group>
                </form> 
             </Document>
             </Message>  
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;

        }
    }
}
