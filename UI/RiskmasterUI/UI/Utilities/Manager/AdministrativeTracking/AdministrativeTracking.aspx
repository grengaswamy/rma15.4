﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdministrativeTracking.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.AdministrativeTracking.AdminTracking" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Administrative Tracking1</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script language="javascript" type="text/javascript" >
        var qString = "";
        //Setting query string on row selection
        function SelectAdminRow(strRowId)
        {
            qString =  strRowId;
            document.getElementById('currentSelRowId').value = strRowId;
        }
    </script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server" name="frmData">
    <%--<div id="maindiv" style="height:100%;width:99%;overflow:auto">--%>
    <div>
        <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    </div>
    <table border="0" width="100%" cellspacing="0" cellpadding="0" >
      <tr>
       <td valign="top">
       <div class="ctrlgroup" id="formtitle" style="width:950px" runat="server"><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></div>
       
       </td>
      </tr>
      <tr>
        <td>
      <div>
      <table border="0" width="90%" >
       <tr>
         <td></td>
       </tr>
       <tr>
       <td width="95%" valign="top" class="singleborder" >
         <asp:GridView ID="GridView1" runat="server"   AutoGenerateColumns="false" 
               AllowPaging="false"  width="100%"   GridLines="None" 
               onrowdatabound="GridView1_RowDataBound">
		    <HeaderStyle CssClass="msgheader" />
		     <AlternatingRowStyle CssClass="data2" />
		    <Columns>
		        <asp:TemplateField  ItemStyle-CssClass="data" ControlStyle-Width="5%">
                    <ItemTemplate>
                       <input type="radio" id="selectrdo" name="AdminTrackingGrid"  />
                    </ItemTemplate> 
                </asp:TemplateField>
                         <asp:TemplateField HeaderText="Row Id" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RowId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrTableName %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserTableName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrSysTableName %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SysTableName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
            </Columns>
		     
		     
		 </asp:GridView>
       </td>
       <td width="5%" valign="top">
          <%--MITS 34135 begin hlv 10/22/2013--%>
         <%--<input type="image" onclick="openGridAddEditWindow('AdminTrackingGrid','add',800,450);return false;"  src="../../../../Images/tb_new_mo.png" alt="" id="New_FieldListGrid" onmouseover="&#xA;javascript:document.all[&#34;New_FieldListGrid&#34;].src='../../../../Images/tb_new_active.png';&#xA;" onmouseout="&#xA;javascript:document.all[&#34;New_FieldListGrid&#34;].src='../../../../Images/tb_new_mo.png';&#xA;" title="New" onclick="return New_FieldListGrid_onclick()" /><br />
         <input type="image" onclick="openGridAddEditWindow('AdminTrackingGrid','edit',800,450);return false;" id="Edit_FieldListGrid" src="../../../../Images/edittoolbar.gif" onmouseover="&#xA;javascript:document.all[&#34;Edit_FieldListGrid&#34;].src='../../../../Images/edittoolbar2.gif';&#xA;" onmouseout="&#xA;javascript:document.all[&#34;Edit_FieldListGrid&#34;].src='../../../../Images/edittoolbar.gif';&#xA;" title="Edit" /><br />--%>
         <%--<input type="image" visible="false" runat="server" onserverclick="DeleteAdminTrackingRow" onclick="return validateGridForDeletion('AdminTrackingGrid');" id="Delete_AdminTrackingGrid_2" src="../../../../Images/delete.gif" onmouseover="&#xA;javascript:document.all[&#34;Delete_AdminTrackingGrid&#34;].src='../../../../Images/delete2.gif';&#xA;" onmouseout="&#xA;javascript:document.all[&#34;Delete_AdminTrackingGrid&#34;].src='../../../../Images/delete.gif';&#xA;" title="Delete" /><br />--%>
             <%-- MITS 34135 begin --%>
        <asp:ImageButton runat="server" src="../../../../Images/tb_new_mo.png" alt="" 
             id="New_FieldListGrid" 
             onmouseover="&#xA;javascript:document.all[&#34;New_FieldListGrid&#34;].src='../../../../Images/tb_new_active.png';&#xA;" 
             onmouseout="&#xA;javascript:document.all[&#34;New_FieldListGrid&#34;].src='../../../../Images/tb_new_mo.png';&#xA;" 
             title="<%$ Resources:ttNew %>" OnClientClick="openGridAddEditWindow('AdminTrackingGrid','add',800,450);return false;" />
        <asp:ImageButton runat="server" src="../../../../Images/edittoolbar.gif" alt="" 
             id="Edit_FieldListGrid" 
             onmouseover="&#xA;javascript:document.all[&#34;Edit_FieldListGrid&#34;].src='../../../../Images/edittoolbar2.gif';&#xA;" 
             onmouseout="&#xA;javascript:document.all[&#34;Edit_FieldListGrid&#34;].src='../../../../Images/edittoolbar.gif';&#xA;" 
             title="<%$ Resources:ttEdit %>" OnClientClick="openGridAddEditWindow('AdminTrackingGrid','edit',800,450);return false;" />
        <asp:ImageButton runat="server" src="../../../../Images/delete.gif" alt="" 
             id="Delete_AdminTrackingGrid" 
             onmouseover="&#xA;javascript:document.all[&#34;Delete_AdminTrackingGrid&#34;].src='../../../../Images/delete2.gif';&#xA;" 
             onmouseout="&#xA;javascript:document.all[&#34;Delete_AdminTrackingGrid&#34;].src='../../../../Images/delete.gif';&#xA;" 
             title="<%$ Resources:ttDel %>" OnClientClick="return validateGridForDeletion('AdminTrackingGrid');" OnClick="DeleteAdminTrackingRow" />
       </td>
       </tr>
       <tr>
         <td width="5%" valign="top">
          <input type="text" style="display:none" runat="server" id="currentSelRowId" value="" />
          
         </td>
       </tr>
       
      </table>
      </div>
      </td>
      </tr>
    </table>
   <%-- </div>--%>
    </form>
</body>
</html>
