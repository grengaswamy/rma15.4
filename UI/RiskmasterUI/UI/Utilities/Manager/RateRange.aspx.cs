﻿using System;
using System.Web.UI;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;

///<Summary>
// Developed By: Tushar Agarwal
// MITS # 18231
// Completed On: 22,Oct, 2009
///</Summary>
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class RateRange : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gridname.Text = AppHelper.GetQueryStringValue("gridname");
                mode.Text = AppHelper.GetQueryStringValue("mode");
                selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedid");
                if (mode.Text.ToLower() != "add")
                {
                    string strData = Session["raterangelist"].ToString();
                    XmlDocument xdata = new XmlDocument();
                    xdata.LoadXml(strData);
                    XmlNodeList nodes = xdata.SelectNodes("//raterange");
//Start1: Added by Neha Suresh Jain,03/12/2010, to use in javascript
                    Count.Text = nodes.Count.ToString();
//end1
                    foreach (XmlElement node in nodes)
                    {
                        if (node.ChildNodes[8].InnerText == selectedrowposition.Text)
                        {
                            RowId.Text = selectedrowposition.Text;  
                            UpperBound.Text = node.ChildNodes[9].InnerText;
                            //Start: Neha Suresh Jain, 05/10/2010, MITS 20659                            
                            //Modifier.Text = node.ChildNodes[5].InnerText;
                            String str = "";
                            str = node.ChildNodes[5].InnerText.Replace("%", "");
                            str = str.Replace("$", "");
                            Modifier.Text = str;
                            //End: MITS 20659
                            //Start2: Added by Neha Suresh Jain,03/12/2010, to use in javascript
                            rangeType.Text = node.ChildNodes[4].InnerText;
                            //end2
                            if (node.ChildNodes[4].InnerText == "Deductible")
                            {
                                //Start: Neha Suresh Jain, 05/10/2010, MITS 20659
                               // LowerBound.Text = node.ChildNodes[6].InnerText;
                                str = "";
                                str = node.ChildNodes[6].InnerText.Replace("%", "");
                                str = str.Replace("$", "");
                                LowerBound.Text = str;
                                //End: MITS 20659
                                Deductible_Radio.Checked = true;
                            }
                            else
                            {
                                LowerBound.Text = node.ChildNodes[7].InnerText;
                                Range_Radio.Checked = true;
                            }
                        }
                    }
                }
                    //start :code added by Neha Suresh Jain, 03/11/2010
                else
                {
                    string strData = Session["raterangelist"].ToString();
                    XmlDocument xdata = new XmlDocument();
                    xdata.LoadXml(strData);
                    XmlNodeList nodes = xdata.SelectNodes("//raterange");
                    if (nodes.Count > 0)
                    {
                        if (nodes[0].ChildNodes[4].InnerText == "Deductible")
                        {
                            Deductible_Radio.Checked = true;
                        }
                        else
                        {
                            Range_Radio.Checked = true;
                        }
                    }
                    else
                        Deductible_Radio.Checked = true;
                }
               //end : Neha Suresh Jain
            }  
        }
        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                mode.Text = AppHelper.GetQueryStringValue("mode");
                selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedid");
                string strData = Session["raterangelist"].ToString();
                XmlDocument xdata = new XmlDocument();
                xdata.LoadXml(strData);
                XmlNodeList nodes = xdata.SelectNodes("//raterange");
                XmlText extraValue = null;

                if (mode.Text.ToLower() != "add")
                {
                    
                   
                    foreach (XmlElement node in nodes)
                    {
                        if (node.ChildNodes[8].InnerText == selectedrowposition.Text)
                        {
                            
                            node.ChildNodes[9].InnerText = UpperBound.Text;
                            node.ChildNodes[5].InnerText = Modifier.Text;

                            if (Deductible_Radio.Checked == true)
                            {
                                node.ChildNodes[4].InnerText = Deductible_Radio.Text;
                                node.ChildNodes[6].InnerText = LowerBound.Text;
                                //Start1:Neha Suresh Jain,03/11/2010,to clear beginning node in the nodes list
                                node.ChildNodes[7].InnerText = UpperBound.Text;
                                //end1
                                node.ChildNodes[10].InnerText = node.ChildNodes[5].InnerText + "|" + node.ChildNodes[6].InnerText + "|" + node.ChildNodes[9].InnerText + "|D|" + node.ChildNodes[8].InnerText;
                            }
                            else
                            {
                                node.ChildNodes[4].InnerText = Range_Radio.Text;
                                //Start2:Neha Suresh Jain,03/11/2010,to clear Deductible node in the nodes list
                                node.ChildNodes[6].InnerText = "";
                                //end2
                                node.ChildNodes[7].InnerText = LowerBound.Text;
                                node.ChildNodes[10].InnerText = node.ChildNodes[5].InnerText + "|" + node.ChildNodes[7].InnerText + "|" + node.ChildNodes[9].InnerText + "|R|" + node.ChildNodes[8].InnerText;
                            }
 
                            
                        }
                    }
                    Session["raterangelist"] = xdata.OuterXml;
                }
                else
                {
                    XmlNode node = xdata.SelectSingleNode("//listhead");
                    string strTempNode = node.OuterXml;
                    XmlDocument xTempData = new XmlDocument();
                    xTempData.LoadXml(strTempNode);
                    xTempData.FirstChild.ChildNodes[0].InnerText = "" ;
                    xTempData.FirstChild.ChildNodes[1].InnerText = "";
                    xTempData.FirstChild.ChildNodes[2].InnerText = "";
                    xTempData.FirstChild.ChildNodes[3].InnerText = "";
                    xTempData.FirstChild.ChildNodes[8].InnerText = Session["RowCount"].ToString();
                    xTempData.FirstChild.ChildNodes[9].InnerText = UpperBound.Text;
                    xTempData.FirstChild.ChildNodes[5].InnerText = Modifier.Text;

                    XmlElement extra = xTempData.CreateElement("Extra");
                    
                    
                    if (Deductible_Radio.Checked == true)
                    {
                        xTempData.FirstChild.ChildNodes[4].InnerText = Deductible_Radio.Text;
                        xTempData.FirstChild.ChildNodes[6].InnerText = LowerBound.Text;
                        xTempData.FirstChild.ChildNodes[7].InnerText = "";
                        extraValue = xTempData.CreateTextNode(xTempData.FirstChild.ChildNodes[5].InnerText + "|" + xTempData.FirstChild.ChildNodes[6].InnerText + "|" + xTempData.FirstChild.ChildNodes[9].InnerText + "|D|" + xTempData.FirstChild.ChildNodes[8].InnerText);
                    }
                    else
                    {
                        xTempData.FirstChild.ChildNodes[4].InnerText = Range_Radio.Text;
                        xTempData.FirstChild.ChildNodes[7].InnerText = LowerBound.Text;
                        xTempData.FirstChild.ChildNodes[6].InnerText = "";
                        extraValue = xTempData.CreateTextNode(xTempData.FirstChild.ChildNodes[5].InnerText + "|" + xTempData.FirstChild.ChildNodes[7].InnerText + "|" + xTempData.FirstChild.ChildNodes[9].InnerText + "|R|" + xTempData.FirstChild.ChildNodes[8].InnerText);
                    }

                    

                    extra.AppendChild(extraValue);
                    xTempData.FirstChild.AppendChild(extra);                   
                    strTempNode = xTempData.OuterXml;
                    strTempNode = strTempNode.Replace("listhead", "raterange");
                    Session["raterangelist"] = strData.Replace("</raterangelist>", strTempNode + "</raterangelist>");
                    Session["RowCount"] = Int32.Parse(Session["RowCount"].ToString()) - 1;

                }
                FormValid.Text = "valid";
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
