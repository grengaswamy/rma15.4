﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientInformationSetup.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.ClientInformationSetup" ValidateRequest = "false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Client Information Setup</title>
    <%--<link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script src="../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/utilities.js"></script>
    <script language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;}</script>

    <link rel="stylesheet" href="../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript">
        function ValForm() 
        {
            if(replace(document.forms[0].DoingBusinessAs.value," ","")!="" && replace(document.forms[0].LegalName.value," ","")!="")
            {
                alert("Either 'Doing Business As' or 'Legal Name' should be filled in");
                document.forms[0].DoingBusinessAs.focus();
                return false;
            }
            return true;
        }
        function CheckBrowser() {
            if (navigator.userAgent.indexOf('Chrome') != -1) {
                var varGetcalendars = document.getElementById('InstallationDate');
                if (varGetcalendars != null) {
                    varGetcalendars.type = "text";
                }
            }
        }
    </script>
</head>
<body class="10pt" onload="CheckBrowser();pageLoaded();parent.MDIScreenLoaded()">
    <form id="frmData" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
  
  <input type="hidden" name="hTabName"/><div id="toolbardrift">
    <table border="0" cellpadding="0" cellspacing="0">
     <tr>
     <td height="32"><asp:imagebutton id="btnSave" runat="server" OnClientClick="if(ValForm()){return UtilitiesForm_Save();}else{return false;}" onclick="Save" ImageUrl = "../../../Images/save.gif" ToolTip="Save" onMouseOver="this.src='../../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../../Images/save.gif';this.style.zoom='100%'"/></td>
    </tr>
    </table>
   </div><div class="msgheader" id="formtitle">Client Information Setup</div>
   <div class="errtextheader"></div><table border="0">
    <tr>
     <td>
        <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSClientInfo" id="TABSClientInfo">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="ClientInfo" id="LINKTABSClientInfo">Client Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPeventinfo">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSContactInformation" id="TABSContactInformation">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="ContactInformation" id="LINKTABSContactInformation">Contact Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPeventdetail">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSMMSEAMedicare" id="TABSMMSEAMedicare">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="MMSEAMedicare" id="LINKTABSMMSEAMedicare">MMSEA Medicare</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPeventMMSEA">
          <nbsp />
          <nbsp />
        </div>
      </div>  
      
     
      <div class="singletopborder" style="position:relative;left:0;top:0;width:800px;height:315px;overflow:auto">
       <table border="0" cellspacing="0" cellpadding="0">
        <tr>
         <td>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABClientInfo" id="FORMTABClientInfo">
           <tr id=""><asp:TextBox style="display:none" id="RowId" type="id" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']"/><td>Client Name:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="1" id="ClientName" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ClientName']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Tax ID:&nbsp;&nbsp;</td>
            <td><asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="taxidLostFocus(this);" id="taxid" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='taxid']" RMXType="taxid" name="taxid" value="" tabindex="10"/></td>            
           </tr>
           <tr id="">
            <td>ESSP #:&nbsp;&nbsp;</td>
            <td><asp:TextBox size="30" tabindex="2" onblur="numLostFocus(this);" type="numeric" id="Essp" onchange="setDataChanged(true);" maxlength="9" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Essp']"/></td>
            <td>&nbsp;&nbsp;</td>
            <td>Hardware:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="11" id="Hardware" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Hardware']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Address:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="3" type="text" id="Address1" onchange="setDataChanged(true);" maxlength="100" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Address1']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Installation Date:&nbsp;&nbsp;</td>
            <td><asp:TextBox size="30" tabindex="12" id="InstallationDate" type="date" onblur="dateLostFocus(this.id);" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='InstallationDate']"/>
                <%--<input type="button" class="DateLookupControl" id="InstallationDatebtn" tabindex="13"/><script type="text/javascript">
											Zapatec.Calendar.setup(
											{
												inputField : "InstallationDate",
												ifFormat : "%m/%d/%Y",
												button : "InstallationDatebtn"
											}
											);
										</script>--%>

                 <script type="text/javascript">
                     $(function () {
                         $("#InstallationDate").datepicker({
                             showOn: "button",
                             buttonImage: "../../../Images/calendar.gif",
                             //buttonImageOnly: true,
                             showOtherMonths: true,
                             selectOtherMonths: true,
                             changeYear: true
                         }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "13");
                     });
                    </script>
            </td>
           </tr>
           <tr id="">
            <td></td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="4" id="Address2" type="text" onchange="setDataChanged(true);" maxlength="100" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Address2']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Phone:&nbsp;&nbsp;</td>
            <td><asp:TextBox size="30" tabindex="14" type="phone" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="Phone" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Phone']"/></td>
           </tr>
           <tr id="">
            <td>City:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="5" id="City" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='City']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Fax:&nbsp;&nbsp;</td>
            <td><asp:TextBox size="30" tabindex="15" type="phone" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="Fax" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Fax']"/></td>
           </tr>
           <tr id="">
            <td>State:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="6" id="State" type="text" onchange="setDataChanged(true);" maxlength="4" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='State']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Distribution Media:&nbsp;&nbsp;</td>
            <td><uc:CodeLookUp runat="server" ID="DistributionMedia" CodeTable="DISTRIBUTION_MEDIA" ControlName="DistributionMedia" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DistributionMedia']" type="code" tabindex="16"/></td>
           </tr>
           <tr id="">
            <td>Zip:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="7" onblur="zipLostFocus(this);" id="Zip" type="zip" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Zip']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Client Status:&nbsp;&nbsp;</td>
            <td>
             <%--Bijender has added for Mits 15763--%>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="17" id="ClientStatus" type="text" onblur="CheckClientSatus(this);" onchange="setDataChanged(true);" maxlength="9" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ClientStatus']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Product:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="8" id="Product" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Product']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Version:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="18" id="Version" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Version']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Printer:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="9" id="Printer" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Printer']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>CSC Support #:&nbsp;&nbsp;</td>
            <td><asp:TextBox size="30" tabindex="19" type="phone" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="Support" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Support']"/></td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABContactInformation" id="FORMTABContactInformation" style="display:none;">
           <tr id="">
            <td colspan="8">
             <table border="0" CELLPADDING="0" CELLSPACING="0">
              <tr>
               <td colspan="70"></td>
               <td>&nbsp;&nbsp;</td>
               <td colspan="200">Name:</td>
               <td>&nbsp;&nbsp;</td>
               <td>Title:</td>
              </tr>
             </table>
            </td>
           </tr>
           <tr id="">
            <td>Primary:&nbsp;&nbsp;</td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="1" id="PrimaryContact" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PrimaryContact']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="7" id="PrimaryTitle" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PrimaryTitle']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Secondary:&nbsp;&nbsp;</td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="2" id="SecondaryContact" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SecondaryContact']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
            <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="8" id="SecondaryTitle" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SecondaryTitle']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Data Entry:&nbsp;&nbsp;</td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="3" id="DataEntryContact" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DataEntryContact']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="9" id="DataEntryTitle" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DataEntryTitle']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Reporting:&nbsp;&nbsp;</td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="4" id="ReportingContact" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ReportingContact']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="10" id="ReportingTitle" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ReportingTitle']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Dept. Mgr:&nbsp;&nbsp;</td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="5" id="DeptMgrContact" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DeptMgrContact']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="11" id="DeptMgrTitle" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DeptMgrTitle']"/></div>
            </td>
           </tr>
           <tr id="">
            <td>Accounting:&nbsp;&nbsp;</td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="6" id="AccountingContact" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AccountingContact']"/></div>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td></td>
             <td>
             <div title="" style="padding: 0px; margin: 0px"><asp:TextBox size="30" tabindex="12" id="AccountingTitle" type="text" onchange="setDataChanged(true);" maxlength="50" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AccountingTitle']"/></div>
            </td>
           </tr>
          </table>   
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABMMSEAMedicare" id="FORMTABMMSEAMedicare" style="display:none;">
            <tr id=""><td colspan=100>Enter data for the MMSEA Medicare Reporting when it applies to the whole database</td>
            </tr>    
            <tr id=""><td>&nbsp;</td></tr>                                                          
           <tr id="">           
            <td align=right>Doing Business As:&nbsp;&nbsp;</td>
            <td><asp:TextBox size="50" maxlength="50" tabindex="1" type="text" id="DoingBusinessAs" onchange="setDataChanged(true);" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DoingBusinessAs']"/></td>
           </tr> 
           <tr id="">
            <td align=right>Legal Name, if no Doing Business As:&nbsp;&nbsp;</td>
            <td><asp:TextBox size="50" maxlength="50" tabindex="2" type="text" id="LegalName" onchange="setDataChanged(true);" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='LegalName']"/></td>
           </tr>   
           <tr id=""><td>&nbsp;</td></tr>                                                                                                                            
           <tr id=""><td colspan=100>
                <dg:UserControlDataGrid runat="server" ID="CMMSEAXDataGrid" GridName="CMMSEAXDataGrid" GridTitle="MMSEA DATA" Target="/Document/form/CMMSEAXData" DynamicHideNodes="" Ref="/Instance/UI/FormVariables/SysExData/CMMSEAXDataSelectedId" Unique_Id="EntMMSEARowID" ShowCloneButton="False" ShowRadioButton="True" ShowCheckBox="False" RefImplementation="False" OnClick="KeepRowForEdit('CMMSEAXDataGrid');;" Width="680px" Height="150px" hidenodes="|EntMMSEARowID|EntityEID|DttmRcdAdded|DttmRcdLastUpd|UpdatedByUser|AddedByUser|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" Type="GridAndButtons"  />
            </td></tr>
            <tr><td><asp:TextBox style="display:none" runat="server" id="CMMSEAXDataSelectedId"  RMXType="id" /></td></tr>
            <tr><td><asp:TextBox style="display:none" runat="server" id="CMMSEAXDataGrid_RowDeletedFlag"  RMXType="id" Text="false" /></td></tr>
            <tr><td><asp:TextBox style="display:none" runat="server" id="CMMSEAXDataGrid_Action"  RMXType="id" /></td></tr>
            <tr><td><asp:TextBox style="display:none" runat="server" id="CMMSEAXDataGrid_RowAddedFlag"  RMXType="id" Text="false"  /></td></tr>         
            <tr><td><asp:TextBox style="display:none" runat="server" id="setGridDataChanged"  RMXType="id" Text="false"  /></td></tr>         
          </table>       
         </td>
         <td valign="top"></td>
        </tr>
       </table>
      </div>
      <table>
       <tr>
        <td></td>
       </tr>
      </table><input type="text" name="" value="" id="SysViewType" style="display:none"/>
      <input type="text" name="" value="" id="SysCmd" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/
      ><input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
      <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
      <input type="text" name="" value="claiminformationsetup" id="SysFormIdName" style="display:none" rmxforms:value="RowId"/>
      <input type="text" name="" value="" id="claiminformationsetup" style="display:none"/>
      <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value="RowId"/>
      <input type="text" name="" value="" id="SysFormPForm" style="display:none" rmxforms:value="ClientInformation"/>
      <input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="ClientInformation"/>
      <input type="text" name="" value="" id="SysRequired" style="display:none"/>
      <input type="text" name="" value="ClientName|" id="SysFocusFields" style="display:none"/></td>
    </tr>
   </table>
 
    </div>
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
