﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager; //MITS 34712 - hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSFeeTable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //MITS 34712 - begin
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("BRSFeeTable.aspx"), "BRSFeeTableButtons",
                ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSFeeTableButtons", sValidationResources, true);

            string sValidationResourcesMsg = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("BRSFeeTable.aspx"), "BRSFeeTableVal",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSFeeTableVal", sValidationResourcesMsg, true);
            //MITS 34712 - end
            
            if(hdnMode.Value != "Delete")
            LoadBRSFeeTable();
        }


        private void LoadBRSFeeTable()
        {
            XmlNode inputDocNode = null;
            string strCWSSrvcOutput = string.Empty;
            XmlDocument xmlOutDoc = new XmlDocument();
            DataTable dtGridData = new DataTable();

            inputDocNode = InputXMLDoc("BRSFeeTableAdaptor.Get");
            strCWSSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

            xmlOutDoc.LoadXml(strCWSSrvcOutput);

            GridHeaderAndData(xmlOutDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/BRSFeeTableList/listhead"), xmlOutDoc.SelectNodes("ResultMessage/Document/PassToWebService/BRSFeeTableList/listrow"), dtGridData);

            GridView1.DataSource = dtGridData;
            GridView1.DataBind();
        }

        private XmlNode InputXMLDoc(string sFunctionToCall)
        {
            
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>" + sFunctionToCall + @"</Function> 
                </Call>
                <Document>
                    <PassToWebService>
                        <BRSFeeTableList>
                        <listhead>
                        <RowId>Row Id</RowId>
                        <UserTableName>Table</UserTableName>
                        <CalcType>Type</CalcType>
                        <StartDt type='date'>Effective Date</StartDt> 
                        <BrsModCount /> 
                        <CalcTypeCode /> 
                        <TableType /> 
                        <OdbcName /> 
                        <HiddenUserTableName /> 
                        </listhead>
                        </BRSFeeTableList>
                    </PassToWebService>
                </Document>
                </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }


        private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;


            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.Name;

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();

                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        //MITS 34712 hlv begin
                        //dr[iColumnIndex++] = objElem.InnerXml;
                        if (objElem.Name.Equals("StartDt"))
                            dr[iColumnIndex++] = Convert.ToDateTime(objElem.InnerXml).ToString(AppHelper.GetDateFormat());
                        else
                            dr[iColumnIndex++] = objElem.InnerXml;
                    }
                    dtGridData.Rows.Add(dr);

                }
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
            }



        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectGridRow('" + DataBinder.Eval(e.Row.DataItem, "RowId").ToString() + "', '" + DataBinder.Eval(e.Row.DataItem, "UserTableName").ToString() + "','" + DataBinder.Eval(e.Row.DataItem, "CalcTypeCode").ToString() + "','" + DataBinder.Eval(e.Row.DataItem, "TableType").ToString() + "', '"  + DataBinder.Eval(e.Row.DataItem, "BrsModCount").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }

        protected void Delete_BRSFeeTableGrid_Click(object sender, ImageClickEventArgs e)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                        <Message>
                        <Authorization></Authorization> 
                        <Call>
                        <Function>BRSFeeTableAdaptor.Delete</Function> 
                        </Call>
                        <Document>
                        <form name='BRSFeeTableDetail' supp='' title='Fee Table Details'>
                        <group name='BRSFeeTableDetail' title='Fee Table Details'>
                        <displaycolumn>
                        <control name='RowId' type='id'>" + hdnTableId.Value + @"</control> 
                        <control name='UserTableName' type='text'>" + hdnUserTableName.Value + @"</control> 
                        </displaycolumn>
                        </group>
                        </form>
                        </Document>
                        </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }
            hdnMode.Value = "";
            AppHelper.CallCWSService(xmlNodeDoc.InnerXml);
            LoadBRSFeeTable();
        }

    }
}
