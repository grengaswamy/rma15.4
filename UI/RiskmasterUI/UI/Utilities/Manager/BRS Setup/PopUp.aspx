﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopUp.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.PopUp" EnableViewStateMac="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript" src="../../../../Scripts/Utilities.js"></script>
    <script language="javascript" type="text/javascript" src="../../../../Scripts/form.js"></script>
    <script language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
</head>
<body onload="BRSImportSchedule_Upload()">
    <form id="frmData" runat="server">
    <div>
    
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
     <td class="ctrlgroup"><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></td>
    </tr>
    <tr>
     <td>
         <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
        </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <CuteWebUI:UploadAttachments  ID="UploadDocumentAttachments" runat="server" InsertText="<%$ Resources:btnUpload %>" OnAttachmentRemoveClicked="OnAttachmentRemoveClicked_Click">
                <InsertButtonStyle />
                <%--<VALIDATEOPTION MaxSizeKB="35000"  />--%>
            </CuteWebUI:UploadAttachments>
        </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2" align="center">
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:btnSave %>" CssClass="button" onclick="SaveData_Click" UseSubmitBehavior="false" OnClientClick="javascript: FileTransfer();" /><%--Jira id: 7572 --%>&nbsp;
        &nbsp;
        <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancel %>" CssClass="button" OnClientClick="window.close(); return false;" />
     </td>
    </tr>
   </table><input type="hidden" value="" id="file">
   <input type="hidden" value="" id="filename" runat="server">
   <input type="hidden" value="" id="hdAction">
   <input type="hidden" value="" id="SysWindowId">
   <uc2:PleaseWaitDialog ID="pleaseWait" runat="server"  CustomMessage="<%$ Resources:lblUploading %>"/>
    
    </div>
    </form>
</body>
</html>
