﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSCPTBasicCodeDetails.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSCPTBasicCodeDetails" %>
<%@ Register src="../../../Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CPT Code Details</title>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/Utilities.js"></script>
</head>
<body onload="BRSCPTBasicCodeDetails_Load();" >
    <form id="frmData" runat="server">
    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
     <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
    
     <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
       <asp:ImageButton runat="server" name="Save" src="../../../../Images/save.gif" 
               alt="" id="Save" 
               onmouseover="javascript:document.all['Save'].src='../../../../Images/save2.gif'" 
               onmouseout="javascript:document.all['Save'].src='../../../../Images/save.gif'" 
               title="<%$ Resources:ttSave %>" OnClientClick="return BRSCPTBasicCodeDetails_Save(); " 
               onclick="Save_Click" />
       </td>
      </tr>
     </tbody>
    </table>
    <br /><br />
    <div class="msgheader" id="formtitle">
        <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label>
    </div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colSpan="2">
              <asp:Label ID="lblAdd" runat="server" Text="<%$ Resources:lblAdd %>"></asp:Label>
          </td>
         </tr>
         <tr><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']"   style="display:none" id="RowId"></asp:TextBox></tr>
         <tr>
          <td><asp:Label ID="lblCPTCode" runat="server" Font-Underline="true" Text="<%$ Resources:lblCPTCode %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPTCode']" id="CPTCode" onchange="setDataChanged(true);" maxlength="50"></asp:TextBox>
           </div>
          </td>
         </tr>
         <tr>
          <td><u></u></td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" value="" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPTCodeSupp']" id="CPTCodeSupp" onchange="setDataChanged(true);" ></asp:TextBox>
           </div>
          </td>
         </tr>
         <tr>
          <td><asp:Label ID="lblCodeDesc" runat="server" Font-Underline="true" Text="<%$ Resources:lblCodeDesc %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CodeDesc']" value="" size="30" id="CodeDesc" onchange="setDataChanged(true);" ></asp:TextBox>
           </div>
          </td>
         </tr>
         <tr>
          <td><asp:Label ID="lblSpecType" runat="server" Text="<%$ Resources:lblSpecType %>"></asp:Label>&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SpecialityCode']" size="30" onchange="lookupTextChanged(this);" id="SpecialityCode" cancelledvalue="" onblur="codeLostFocus(this.id);"></asp:TextBox>
          <input type="button" class="CodeLookupControl" id="SpecialityCodebtn" onclick="selectCode('SPECIALTY','SpecialityCode')" />
          <input type="text" value="" style="display:none" id="SpecialityCode_cid" runat="server" cancelledvalue="" />
          </td>
         </tr>
         <tr>
          <td><asp:Label ID="lblAmount" runat="server" Font-Underline="true"  Text="<%$ Resources:lblAmount %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Amount']" value="" size="30" onblur="numLostFocus(this);" id="Amount" onchange="setDataChanged(true);"></asp:TextBox>
          </td>
         </tr>
         <tr>
          <td>
          <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TableId']" style="display:none" id="hdnTableId"></asp:TextBox>
 
          
          </td>
         </tr>
         <tr>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPTCodeComp']" style="display:none" id="CPTCodeComp"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FormMode']" value="" style="display:none" id="FormMode"></asp:TextBox></td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td></td>
         </tr>
        </tbody>
       </table>
       </td>
      <td></td>
     </tr>
    </tbody>
   </table>
    
    </div>
    </form>
</body>
</html>
