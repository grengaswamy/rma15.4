﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.RMXResourceManager; //MITS 34779 - hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSModifierValue : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            //MITS 34779 - hlv begin 
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("BRSModifierValue.aspx"), "BRSModifierValue",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSModifierValue", sValidationResources, true);
            //MITS 34779 - hlv end
            hdnUserTableName.Text = AppHelper.GetQueryStringValue("usertablename");
            hdnTableId.Text = AppHelper.GetQueryStringValue("tableid");
            RowId.Text = AppHelper.GetQueryStringValue("selectedid");
            
            if (IsPostBack != true && RowId.Text != "")
            {
                string sCWSresponse = string.Empty;
                bool bResult = false;
                bResult = CallCWS("BRSModifierValueAdaptor.Get", null, out sCWSresponse, true, true);

                if (bResult)
                {
                    SetModifierCode(sCWSresponse);
                }
            }
        }

        private void SetModifierCode(string sCWSresponse)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sCWSresponse);
            XmlElement objXmlElement = (XmlElement)xmlDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name='ModifierCode']");
            ModifierCode_cid.Text = objXmlElement.GetAttribute("codeid");
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            using (XmlReader reader = Xelement.CreateReader()) 
            {
                xmlNodeDoc.Load(reader); 
            }

            XmlElement objXmlElement = (XmlElement)xmlNodeDoc.SelectSingleNode("Message/Document/form/group/displaycolumn/control[@name='ModifierCode']");
            objXmlElement.SetAttribute("codeid", ModifierCode_cid.Text);
            objXmlElement.SetAttribute("codetable", "MODIFIER_CODES");

            Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            bool bReturnStatus = false;
            bReturnStatus = CallCWSFunction("BRSModifierValueAdaptor.Save");

            if (bReturnStatus)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
        }
    }
}
