﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSFactors : System.Web.UI.Page
    {

        private bool bNoRecords = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bNoRecords = false;
                hdnTableId.Value = AppHelper.GetQueryStringValue("tableid");
                hdnUserTableName.Value = AppHelper.GetQueryStringValue("usertablename");
                tdHead.InnerText = hdnUserTableName.Value;
                if (hdnMode.Value != "Delete")
                    LoadBRSFactors();


            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }


        private void LoadBRSFactors()
        {
            XmlNode inputDocNode = null;
            string strCWSSrvcOutput = string.Empty;
            XmlDocument xmlOutDoc = new XmlDocument();
            DataTable dtGridData = new DataTable();

            inputDocNode = InputXMLDoc("BRSFactorsAdaptor.Get");
            strCWSSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

            xmlOutDoc.LoadXml(strCWSSrvcOutput);

            GridHeaderAndData(xmlOutDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/BRSFactorsList/listhead"), xmlOutDoc.SelectNodes("ResultMessage/Document/PassToWebService/BRSFactorsList/listrow"), dtGridData);

            GridView1.DataSource = dtGridData;
            GridView1.DataBind();

            if (bNoRecords)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>document.forms[0].selectrdo.style.display = 'none';</script>");
            }
        }

        private XmlNode InputXMLDoc(string sFunctionToCall)
        {

            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>" + sFunctionToCall + @"</Function> 
                </Call>
                <Document>
                    <PassToWebService>
                    <TableId>" + hdnTableId.Value + @"</TableId> 
                    <UserTableName>" + hdnUserTableName.Value + @"</UserTableName> 
                    <BRSFactorsList>
                    <listhead>
                    <ClinicalCatCode>Code</ClinicalCatCode> 
                    <ClinicalCategory>Clinical Category</ClinicalCategory> 
                    <ConversionFact>Conversion Factor</ConversionFact>
                    <RowId>Row Id</RowId> 
                    </listhead>
                    </BRSFactorsList>

                    </PassToWebService>
                </Document>
                </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }


        private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;


            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.Name;

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();

                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        dr[iColumnIndex++] = objElem.InnerXml;
                    }
                    dtGridData.Rows.Add(dr);

                }
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
                bNoRecords = true;
            }



        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectGridRow('" + DataBinder.Eval(e.Row.DataItem, "RowId").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }

        protected void Delete_BRSFeeTableGrid_Click(object sender, ImageClickEventArgs e)
        {
            string[] strRowId = hdnRowId.Value.Split(new Char[] { '|' });
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                        <Message>
                        <Authorization></Authorization> 
                        <Call>
                        <Function>BRSFactorAdaptor.Delete</Function> 
                        </Call>
                        <Document>
                        <form name='Factor' supp='' title='Factor' topbuttons='1'>
                            <group name='Factor' selected='1' title='Factor'>
                            <TableId>" + hdnTableId.Value + @"</TableId> 
                            <displaycolumn>
                            <control name='RowId' type='id'>" + strRowId[1] + @"</control> 
                            <control codetable='CLINICAL_CATEGORY' name='ClinicalCat' required='yes' title='-Clinical Category:' type='code'>" + strRowId[1] + @"</control> 
                            </displaycolumn>
                            </group>
                        </form>
                        </Document>
                        </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }
            hdnMode.Value = "";
            AppHelper.CallCWSService(xmlNodeDoc.InnerXml);
            LoadBRSFactors();
        }




    }
}
