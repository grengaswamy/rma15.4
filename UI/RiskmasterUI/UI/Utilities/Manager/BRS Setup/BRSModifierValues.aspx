﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSModifierValues.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSModifierValues" %>

<%@ Register src="../../../Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modifier Values</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="javascript">
        var bSelected = false;
        function openAddModifierValue() {
            //Yukti,MITS:34790
            //window.open("BRSModifierValue.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;usertablename=" + document.forms[0].hdnUserTableName.value, "BRSModifierValue",
			//		'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');

            window.open("BRSModifierValue.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;usertablename=" + encodeURIComponent(document.forms[0].hdnUserTableName.value), "BRSModifierValue",
					'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
            return false;
        }

        function openEditModifierValue() {
            if (bSelected) {
                //Yukti,MITS:34790
                //window.open("BRSModifierValue.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;usertablename=" + document.forms[0].hdnUserTableName.value + "&amp;selectedid=" + document.forms[0].hdnSelectedId.value, "BRSModifierValue",
				//	'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
                window.open("BRSModifierValue.aspx?tableid=" + document.forms[0].hdnTableId.value + "&amp;usertablename=" + encodeURIComponent(document.forms[0].hdnUserTableName.value) + "&amp;selectedid=" + document.forms[0].hdnSelectedId.value, "BRSModifierValue",
					'width=400,height=500,top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
            }
            else {
                //alert('Please select a record to edit.'); //MITS 34779 hlv
                alert(BRSModifierValues.valEditMod);
             }
            
            return false;
        }

        function SelectGridRow(RowId) {
            
            document.forms[0].hdnSelectedId.value = RowId;
            bSelected = true;
        }
    
    </script>
</head>
<body onunload="window.opener.document.forms[0].submit();">
    <form id="frmData" runat="server">
    <input type="hidden" runat="server" id="hdnUserTableName" />
    <input type="hidden" runat="server" id="hdnTableId" />
    <input type="hidden"  id="hdnSelectedId" runat="server" />
    <input type="hidden" id="hdnMode" runat="server" />
    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
    <div>
       <table  id="tblGrid" runat="server"  width="550px" cellspacing="0" cellpadding="0" border="0">
         <tr>
				
				<td class="ctrlgroup" colSpan="2">
				  <asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label>
				</td>
		</tr>
		<tr>
		        <td colspan="2"><b></b>&nbsp;</td>
		</tr>
		<tr>
          <%--<td ><div class="msgheader"  id="formtitle"><asp:Label ID="lblUserTableName" runat="server" ></asp:Label> </div></td>--%>
            <td ><div class="msgheader"  id="formtitle"><asp:Label ID="lblUserTableName" runat="server" Text="<%$ Resources:lblUserTableName %>"></asp:Label> </div></td>
         </tr>
         <b></b>&nbsp;
		<tr>
		       <td width="95%" colspan="1" class="singleborder" >
		          <div id="divGrid" style="overflow:auto; height:400px"  >
		           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%"  AutoGenerateColumns="false"  
                            GridLines="None" onrowdatabound="GridView1_RowDataBound"  >
		              <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id="selectrdo" name="BRSFeeTableGrid"  />
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                          <asp:TemplateField HeaderText="<%$ Resources:gvHModCode %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ModifierCode")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHStartCPT %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartCPT")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHEndCPT %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndCPT")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="<%$ Resources:gvHModValue %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ModValue")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                          <asp:TemplateField HeaderText="<%$ Resources:gvHMaxRLV %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaxRLV")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
		              </Columns> 
		           </asp:GridView>
		           </div>
		       </td>
		       <td width="5%" valign="top" colspan="1">
		        <%--Sharishkumar:RMA -Jira:1313--%>
		        <%--<input type="image" src="../../../../Images/new.gif" alt="" id="New_BRSFeeTableGrid" onmouseover="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new2.gif'" onmouseout="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new.gif'" title="New" onclick="return openAddModifierValue();" /><br />
		        <input type="image" src="../../../../Images/edittoolbar.gif" alt="" id="Edit_BRSFeeTableGrid" onmouseover="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar2.gif'" onmouseout="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar.gif'" title="Edit" onclick="return openEditModifierValue();" /><br />--%>
              <%--  Sharishkumar:RMA -Jira:1313 Ends--%>
		        <asp:ImageButton runat="server" src="../../../../Images/new.gif" alt="" 
                       id="New_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new2.gif'" 
                       onmouseout="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new.gif'" 
                       title="<%$ Resources:ttNew %>" 
                       OnClientClick="return openAddModifierValue(); return false;"  />
		        <asp:ImageButton runat="server" src="../../../../Images/edittoolbar.gif" alt="" 
                       id="Edit_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar2.gif'" 
                       onmouseout="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar.gif'" 
                       title="<%$ Resources:ttEdit %>" 
                       OnClientClick="return openEditModifierValue(); return false;"  />
		        <asp:ImageButton runat="server" src="../../../../Images/delete.gif" alt="" 
                       id="Delete_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete2.gif'" 
                       onmouseout="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete.gif'" 
                       title="<%$ Resources:ttDelete %>" 
                       OnClientClick="return validateGridForDeletion('BRSFeeTableGrid');" 
                       onclick="Delete_BRSFeeTableGrid_Click" />
		        
		        
		       </td>
		</tr>
       	 
       </table>
    </div>
    
    </form>
</body>
</html>
