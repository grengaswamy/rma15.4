﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSSettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSSettings" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>BRS Settings</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../../Scripts/WaitDialog.js">{var i;}</script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/brs.js"></script>
    <script type="text/javascript">
    function moveItemBetweenListBoxes(sFromListBox, sToListBox, mode)
    {
        var optionObject;
        var oToListBox = document.getElementById(sToListBox);
        var oFromListBox = document.getElementById(sFromListBox);

        for (var i = oFromListBox.options.length - 1; i >= 0; i--)
        {
            if ((oFromListBox.options[i].selected == true && mode == "selected") || (mode != "selected"))
            {
                //add to selected list
                optionObject = new Option(oFromListBox.options[i].text, oFromListBox.options[i].value);
                oToListBox.options[oToListBox.options.length] = optionObject;

                //remove from available list
                removeListBoxItem(sFromListBox, i);
                setDataChanged(true);
            }
        }

        sortListBox(sToListBox);
        
        return false;
    }

    function removeListBoxItem(listBoxName, id)
    {
        select = document.getElementById(listBoxName);

        for (var i = id; i < select.options.length - 1; i++)
        {
            select.options[i].text = select.options[i + 1].text;
            select.options[i].value = select.options[i + 1].value;
        }

        select.options.length = select.options.length - 1;
    }

    function sortListBox(id)
    {
        var lb = document.getElementById(id);
        arrTexts = new Array();

        for (i = 0; i < lb.length; i++)
        {
            arrTexts[i] = lb.options[i].text + ':' + lb.options[i].value;
        }
        arrTexts.sort();
        for (i = 0; i < lb.length; i++)
        {
            el = arrTexts[i].split(':');
            lb.options[i].text = el[0];
            lb.options[i].value = el[1];
        }
    }

    function SelectAll()
    {
        hdnSelectedFields = document.getElementById('txtKeepOnNextFields');
        var values = "";

        for (var f = 0; f < document.forms[0].lstKeepOnNextFields.options.length; f++)
        {
            values += document.forms[0].lstKeepOnNextFields.options[f].value + "|";
        }
        hdnSelectedFields.value = values;

        pleaseWait.Show();
        return true;
    }										
	</script>  
</head>
<body class="" onload="parent.MDIScreenLoaded();EnableCriteria();" style="height:90%">
    <form id="frmData" runat="server" >
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <input type="hidden" name="hTabName" />
        <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" valign="middle" height="32">
                    <asp:ImageButton tabindex="1" id="Save" ImageUrl="~/Images/tb_save_active.png" class="bold" ToolTip="<%$ Resources:ttSave %>" runat="server" onclick="Save_Click" OnClientClick = "return SelectAll();"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="ctrlgroup" width="100%"><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></td>
            </tr>
        </table>
        </div>
        
        <br /><br />

        <table border="0" cellspacing="2" cellpadding="0" width="100%">
            <tr>
                <td width="30%" align="left"><asp:Label ID="lblBPField" runat="server" Text="<%$ Resources:lblBPField %>"></asp:Label></td>                                
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td width="30%" align="left"><asp:Label ID="lblNextField" runat="server" Text="<%$ Resources:lblBPField %>"></asp:Label></td>                            
            </tr>                           
            <tr> 
                <td width="40%" valign="top">
                    <asp:ListBox ToolTip="<%$ Resources:ttLeft %>" tabindex="10" RMXRef="/Instance/Document/Settings/AllFields" ItemSetRef="/Instance/Document/Settings/CandidateFields" rmxignoreset="false" SelectionMode="Multiple" ID="lstFields" runat="server" Rows="10" width="208px"></asp:ListBox>                                    
                </td>
                <td>&nbsp;</td>
                <td width="20%" valign="middle" align="left">
                    <asp:Button tabindex="20" runat="server" Text="  >   " class="button" Style="width: 100" ID="btnAddSelected" OnClientClick="return moveItemBetweenListBoxes('lstFields', 'lstKeepOnNextFields', 'selected');" />
                    <br />
                    <br />
                    <asp:Button tabindex="30" runat="server" Text=" >>  " class="button" Style="width: 100" ID="btnAddAll" OnClientClick="return moveItemBetweenListBoxes('lstFields', 'lstKeepOnNextFields', 'all');" />
                    <br />
                    <br />
                    <asp:Button tabindex="40" runat="server" Text="  <   " class="button" Style="width: 100" ID="btnRemoveSelected" OnClientClick="return moveItemBetweenListBoxes('lstKeepOnNextFields', 'lstFields', 'selected');" />
                    <br />
                    <br />
                    <asp:Button tabindex="50" runat="server" Text=" <<  " class="button" Style="width: 100" ID="btnRemoveAll" OnClientClick="return moveItemBetweenListBoxes('lstKeepOnNextFields', 'lstFields', 'all');" />
                    <br />
                </td>
                <td>&nbsp;</td>
                <td width="40%" valign="top" align="left">                                            
                    <asp:ListBox ToolTip="<%$ Resources:ttRight %>" tabindex="60" RMXRef="/Instance/Document/Settings/SelectedField" ItemSetRef="/Instance/Document/Settings/KeepOnNextFields" rmxignoreset="false" SelectionMode="Multiple" ID="lstKeepOnNextFields" runat="server" Rows="10" width="208px"></asp:ListBox>
                    <asp:TextBox runat ="server" rmxignoreget="true" ID="txtKeepOnNextFields" style="display:none" RMXRef="/Instance/Document/Settings/SelectedField"></asp:TextBox>                                    
                </td>
            </tr>                       
        </table>
        <br /><br />
        <table width="100%">
            <tr>
                <td class="ctrlgroup" width="100%" align="left" colspan="2"><asp:Label ID="lblBasicSetting" runat="server" Text="<%$ Resources:lblBasicSetting %>"></asp:Label></td>                   
            </tr>
            <tr>
                <td>
                    <asp:CheckBox runat="server" ID="chkGenEOBCodeFlag" Text="<%$ Resources:lblGenEOBCodeFlag %>" RMXRef="/Instance/Document/Settings/GenEOBCodeFlag" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkDiscOnSchdFlag" Text="<%$ Resources:lblDiscOnSchdFlag %>" RMXRef="/Instance/Document/Settings/DiscOnSchdFlag" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox runat="server" ID="chkDiscOnBillFlag" Text="<%$ Resources:lblDiscOnSchdFlag %>" RMXRef="/Instance/Document/Settings/DiscOnBillFlag" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkDisc2ndSchdFlag" Text="<%$ Resources:lblDisc2ndSchdFlag %>" RMXRef="/Instance/Document/Settings/Disc2ndSchdFlag" />
                </td>
            </tr>
           <%-- Mona: preventing duplication of BRS splits --%>
            <tr>
            <td>
             <asp:CheckBox runat="server" ID="chkDupBRSSplits" Text="<%$ Resources:lblDupBRSSplits %>" RMXRef="/Instance/Document/Settings/DupSplitsFlag" onclick="EnableCriteria();" />
             </td>
            </tr>
            <tr>
            <td> &nbsp;&nbsp;&nbsp;&nbsp; <asp:label runat="server" id="lbldupcriteria" Text="<%$ Resources:lbldupcriteria %>" />  &nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:DropDownList ID="DupCriteria" rmxref="/Instance/Document/Settings/DupSplitsCriteria/@value" ItemSetRef="/Instance/Document/Settings/DupSplitsCriteria" onchange="setDataChanged(true);" runat="server">
                </asp:DropDownList>
            </td>
            </tr>
            <%-- Mona: preventing duplication of BRS splits --%>
        </table>
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
