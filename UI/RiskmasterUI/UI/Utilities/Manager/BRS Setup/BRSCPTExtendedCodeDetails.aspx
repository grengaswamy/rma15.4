﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSCPTExtendedCodeDetails.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSCPTExtendedCodeDetails" %>
<%@ Register src="../../../Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc1" %>
<%@ Register TagPrefix="uc" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>CPT Extended Code Details</title>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/Utilities.js"></script>
</head>
<body onload="BRSCPTExtendedCodeDetails_Load();" >
    <form id="frmData" runat="server">
    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
     <uc:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
    
     <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
       <asp:ImageButton runat="server" name="Save" src="../../../../Images/save.gif" 
               alt="" id="Save" 
               onmouseover="javascript:document.all['Save'].src='../../../../Images/save2.gif'" 
               onmouseout="javascript:document.all['Save'].src='../../../../Images/save.gif'" 
               title="Save" OnClientClick="return BRSCPTExtendedCodeDetails_Save(); " 
               onclick="Save_Click" />
       </td>
      </tr>
     </tbody>
    </table>
    <br /><br /><div class="msgheader" id="formtitle">CPT Extended Code Details</div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colSpan="2">Add</td>
         </tr>
         <tr><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']"   style="display:none" id="RowId"></asp:TextBox></tr>
         <tr>
          <td><u>CPT Code</u>:&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPTCode']" id="CPTCode" MaxLength="6" onchange="setDataChanged(true);" ></asp:TextBox>
           </div>
          </td>
         </tr>
        <%-- <tr>
          <td><u></u></td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" value="" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPTCodeSupp']" id="CPTCodeSupp" onchange="setDataChanged(true);" ></asp:TextBox>
           </div>
          </td>
         </tr>--%>
         <tr>
          <td>Code Description:&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
           <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CodeDesc']" value="" size="30" id="CodeDesc" onchange="setDataChanged(true);" ></asp:TextBox>
           </div>
          </td>
         </tr>
         <tr>
          <td><u>Amount</u>:&nbsp;&nbsp;
          </td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Amount']" value="" size="30" onblur="numLostFocus(this);" id="Amount" onchange="setDataChanged(true);"></asp:TextBox>
          </td>
         </tr>
         <tr>
          <td>Professional Professional Component:&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ProfComp']" value="" size="30" onblur="numLostFocus(this);" id="ProfComp" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
         <tr>
          <td>Follow-Up Days:&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FollowUp']" value="" size="30" onblur="numLostFocus(this);" id="FollowUp" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
         <tr>
          <td>Starred (*):&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Strrd']" value="" size="30" MaxLength="1" id="Strrd" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
         <tr>
          <td>Anesthesia Value Unit:&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AnsthValUnit']" value="" size="30" onblur="numLostFocus(this);" id="AnsthValUnit" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
         <tr>
          <td>ByReport (BR):&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='ByRep']" value="" size="30"  MaxLength="2" id="ByRep" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
          <tr>
          <td>Anesthesia ByReport (BR):&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AnsthByRep']" value="" size="30"  MaxLength="2" id="AnsthByRep" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
          <tr>
          <td>Assistant At Surgery (Y,N,R):&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AsstAtSurgery']" value="" size="30"  MaxLength="1" id="AsstAtSurgery" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
          <tr>
          <td>Anesthesia Not Applicable (NA):&nbsp;&nbsp;</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='AnsthNotAppl']" value="" size="30" MaxLength="2" id="AnsthNotAppl" onchange="setDataChanged(true);"></asp:TextBox>
         </tr>
         <tr>
          <td>
          <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TableId']" style="display:none" id="hdnTableId"></asp:TextBox>
 
          
          </td>
         </tr>
         <%--<tr>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPTCodeComp']" style="display:none" id="CPTCodeComp"></asp:TextBox></td>
         </tr>--%>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FormMode']" value="" style="display:none" id="FormMode"></asp:TextBox></td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td></td>
         </tr>
        </tbody>
       </table>
       </td>
      <td></td>
     </tr>
    </tbody>
   </table>
    
    </div>
    </form>
</body>
</html>

