﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSFactor : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strSelectedid=AppHelper.GetQueryStringValue("selectedid");
            if (!IsPostBack)
            {
                if (strSelectedid != "")
                {
                    string[] strIds = strSelectedid.Split(new Char[] { '|' });
                    RowId.Text = strIds[1];
                    ClinicalCat.Text = strIds[1];
                    TableId.Text = strIds[0];
                    ClinicalCat_cid.Text = strIds[1];
                    string sCWSresponse = string.Empty;
                    bool bResult = false;
                    bResult = CallCWS("BRSFactorAdaptor.Get", null, out sCWSresponse, true, true);

                }
                else
                {
                    string strTableId = AppHelper.GetQueryStringValue("tableid");
                    TableId.Text = strTableId;
                }
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            using (XmlReader reader = Xelement.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            XmlElement objXmlElement = (XmlElement)xmlNodeDoc.SelectSingleNode("Message/Document/form/group/displaycolumn/control[@name ='ClinicalCat']");
            objXmlElement.SetAttribute("codeid", ClinicalCat_cid.Text);
            objXmlElement.SetAttribute("codetable", "CLINICAL_CATEGORY");
            objXmlElement.InnerText = ClinicalCat.Text;
            Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            
            string sCWSresponse = string.Empty;
            bool bResult = false;
            bResult = CallCWS("BRSFactorAdaptor.Save", null, out sCWSresponse, true, true);
            XmlDocument xmlServiceDoc = new XmlDocument();
            xmlServiceDoc.LoadXml(sCWSresponse);
  
            if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
        }
    }
}
