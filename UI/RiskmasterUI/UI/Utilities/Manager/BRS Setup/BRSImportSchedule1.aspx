﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="BRSImportSchedule1.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSImportSchedule1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Fee Schedule Import Wizard</title>
    <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/Utilities.js"></script>
    <script type="text/javascript" language="javascript">
        function BRSImportLoad() {

            if (document.forms[0].StepNo.value == "1") {
                document.getElementById("Step1").style.display = '';
                document.getElementById("Step2").style.display = 'none';
                document.getElementById("Step3").style.display = 'none';
            }

            if (document.forms[0].StepNo.value == "2") {
                document.getElementById("Step1").style.display = 'none';
                document.getElementById("Step2").style.display = '';
                document.getElementById("Step3").style.display = 'none';
            }
            if (document.forms[0].StepNo.value == "3") {
                document.getElementById("Step1").style.display = 'none';
                document.getElementById("Step2").style.display = 'none';
                document.getElementById("Step3").style.display = '';
            }
            if (document.forms[0].StepNo.value == "3") {
                //BRSImportFinish_Load()
            }
            else {
                BRSImportSchedule_Load();
            }
        }

    </script>
</head>
<body onload="BRSImportLoad();parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <input type="hidden" id="StepCount" />
   <div id="Step1" runat="server">
       
     <div class="msgheader" id="formtitle"><asp:Label ID="lblTitle" runat="server" Text="<%$ Resources:lblTitle %>"></asp:Label></div>
   <div class="errtextheader"></div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <td></td>
       <td><input type="text" name="$node^8" value="" style="display:none" id="hdAction"></td>
       <td></td>
       <td><input type="text" name="$node^185" rmxref="/Instance/Document/form/control[@name ='ErrMsg']" value="" style="display:none" id="ErrMsg"></td>
       <table border="0">
        <tbody>
<%--         <tr>
          <td class="ctrlgroup" colSpan="2">Fee Schedule Import Wizard - Step 1 of 2 - Source Information</td>
         </tr> Akashyap3 MITS:14908 --%>
         <tr>
          <td><asp:Label ID="lblDesc" runat="server" Text="<%$ Resources:lblDesc %>"></asp:Label>&nbsp;&nbsp;
          </td>
          <td></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="$node^34" value="" style="display:none" id="Label2"></td>
         </tr>
         <tr>
          <td><asp:Label ID="lblProvider" runat="server" Text="<%$ Resources:lblProvider %>"></asp:Label>&nbsp;&nbsp;</td>
          <td><asp:DropDownList runat="server" id="Provider" 
                  itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='Provider']" 
                  rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Provider']/@value" 
                  onchange="BRSImportSchedule_ProviderChange();" 
                  onselectedindexchanged="Provider_SelectedIndexChanged">
             <asp:ListItem Text="<%$ Resources:lblProItem1 %>" Value="1"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:lblProItem2 %>" Value="2"></asp:ListItem>
            </asp:DropDownList></td>
         </tr>
         <tr>
          <td><asp:Label ID="lblFSType" runat="server" Text="<%$ Resources:lblFSType %>"></asp:Label>&nbsp;&nbsp;</td>
          <td> <asp:DropDownList id="FeeScheduleType"  itemsetref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeScheduleType']" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeScheduleType']/@value" runat="server" >
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem1 %>" Value="2"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem2 %>" Value="13"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem3 %>" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem4 %>" Value="1"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem5 %>" Value="7"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem6 %>" Value="8"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem7 %>" Value="3"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:lblFeeTypeItem8 %>" Value="4"></asp:ListItem>
                </asp:DropDownList></td>
         </tr>
         <tr>
          <td><asp:Label ID="lblFileImp" runat="server" Text="<%$ Resources:lblFileImp %>"></asp:Label>&nbsp;&nbsp;</td>
          <td>
              <!--<input type="submit" name="$action^" value="Upload File for Import" class="button" id="SelectFile" style="display:'';" onclick="return WindowOpen();; ">-->
              <asp:Button ID="SelectFile" Text="<%$ Resources:btnUploadFile %>" style="display:'';" class="button" OnClientClick="return WindowOpen();return false" runat="server" />
          </td>
         </tr>
         <tr>
          <td></td>
          <td>
          </td>
         </tr>         
         <tr>
          <td></td>
          <td><asp:TextBox runat="server"  rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Files']" value="" style="display:none" id="Files"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeScheduleTypeIngenix']/@value"  style="display:none" id="FeeScheduleTypeIngenix"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeScheduleTypeOWCP']/@value"  style="display:none" id="FeeScheduleTypeOWCP"></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StepNo']"  value="1" style="display:none" id="StepNo"></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='UploadStatus']"  style="display:none" id="UploadStatus"></asp:TextBox></td>
         </tr>
         <tr>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='RefreshInterval']"  value="10000" style="display:none" id="RefreshInterval"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='StatusMessage']"  style="display:none" id="StatusMessage"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='filename']"  style="display:none" id="filename"></asp:TextBox></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/files/file"   style="display:none" id="file"></asp:TextBox></td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td>
          
          </td>
         </tr>
        </tbody>
       </table>
      </td>
      <td valign="top"></td>
     </tr>
    </tbody>
   </table> 
   </div>
   <asp:TextBox ID="SourceDir" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='SourceDir']" style="display:none"></asp:TextBox> 
   <asp:TextBox ID="TargetDir" runat="server" style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='TargetDir']"></asp:TextBox>
   
   <div id="Step2" style="display:none" runat="server">
     <table border="0">
        
         <tr>
          <td class="ctrlgroup" colSpan="2"><asp:Label ID="lblTitle2" runat="server" Text="<%$ Resources:lblTitle2 %>"></asp:Label></td>
         </tr>
         <tr>
          <td></td>
          <td><input type="text" name="$node^28" value="" style="display:none" id="Label1"></td>
         </tr>
         <tr>
          <td><asp:Label ID="lblDesc2" runat="server" Text="<%$ Resources:lblDesc2 %>"></asp:Label>&nbsp;&nbsp;</td>
          <td></td>
         </tr>
         <tr>
          <td colspan="2"><br /></td>
         </tr>
         <tr>
          <td>
            <asp:Label ID="lblFSName" runat="server" Text="<%$ Resources:lblFSName %>"></asp:Label>
          </td>
          <td>
           <asp:TextBox ID="FeeScheduleName" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='FeeScheduleName']"  runat="server" ></asp:TextBox>
          </td>
         </tr>
           <tr>
          <td></td>
          <td>
          </td>
         </tr>
         <tr>
          <td id="colCptRuv" runat="server"></td>          
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CptRvu']" value="" id="CptRvu" ></asp:TextBox></td>
         </tr>
         <tr>
          <td id="colGpci" runat="server"></td>  
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Gpci']" value="" id="Gpci" ></asp:TextBox></td>
         </tr>
         <tr>
          <td id="colMod" runat="server"></td>  
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Mods']" id="Mods" ></asp:TextBox></td>
         </tr>     
       </table>
   
   </div> 
   <div id="Step3" runat="server" style="display:none">
    <table>
      <tr>
          <td class="ctrlgroup" colSpan="2"><asp:Label ID="lblTitle3" runat="server" Text="<%$ Resources:lblTitle3 %>"></asp:Label></td>
         </tr>
         <tr>
          <td runat="server" id="Msg"><asp:Label ID="lblDesc3" runat="server" Text="<%$ Resources:lblDesc3 %>"></asp:Label>&nbsp;&nbsp;</td>
          <td></td>
         </tr>
         <tr>
          <td><img border="0" src="../../../../Images/anglobe.gif" id="ImageGlobe" runat="server" align="left"></td>
         </tr>
         <tr>
          <td>
           <!--<input type="button" class="button" name="FeeSched" style="display:none" value="Fee Schedule" onClick="BRSImportSchedule_FeeSched();" id="FeeSched">-->
            <asp:Button ID="FeeSchedSec" Text="<%$ Resources:btnFeeSche %>" style="display:none" class="button" OnClientClick="BRSImportSchedule_FeeSched();return false;" runat="server" />
          </td>
         </tr>
    </table>
   </div>   
   <tr>
          <td><script language="JavaScript" SRC="">{var i;}</script>
          <!--<input type="button" runat="server" class="button" name="Back" value="Back" onClick="BRSImportSchedule_Back();" id="btnBack2">-->
          <asp:Button ID="btnBack" Text="<%$ Resources:btnBack %>" class="button" OnClientClick="BRSImportSchedule_Back();return false;" runat="server" />
          <asp:Button class="button" runat="server" name="Next" Text="<%$ Resources:btnNext %>" OnClientClick="return BRSImportSchedule_Next()" id="Next" onclick="Next_Click" />
          
          <script language="JavaScript" SRC="">{var i;}</script>
          <asp:Button class="button" Text="<%$ Resources:btnFinish %>" runat="server" OnClientClick="return BRSImportSchedule_Next();" id="btnFinish" Enabled="false" onclick="btnFinish_Click" />
        <asp:Button ID="FeeSched" class="button" Text="<%$ Resources:btnFeeSche %>" runat="server"  Visible="false" onclick="FeeSched_Click" />
        <asp:TextBox ID="CallImport" runat="server" Text="No" style="display:none" ></asp:TextBox> 
        <asp:TextBox ID="hdnBreak" runat="server" Text="0"  style="display:none" ></asp:TextBox> 
    </form>
</body>
</html>
