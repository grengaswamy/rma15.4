﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager; //MITS 34779 - hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSModifierValues : System.Web.UI.Page
    {
        bool bNoRecords = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("BRSModifierValues.aspx"), "BRSModifierValues",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSModifierValues", sValidationResources, true);
            bNoRecords = false;
            //hdnUserTableName.Value = AppHelper.GetQueryStringValue("usertablename");
            hdnUserTableName.Value = HttpUtility.ParseQueryString(Request.RawUrl).Get("usertablename");
            lblUserTableName.Text = hdnUserTableName.Value;
            hdnTableId.Value = AppHelper.GetQueryStringValue("tableid");

            if (hdnMode.Value != "Delete")
            {
                LoadModifierValues();
            }
        }

        private void LoadModifierValues()
        {
            XmlNode inputDocNode = null;
            string strCWSSrvcOutput = string.Empty;
            XmlDocument xmlOutDoc = new XmlDocument();
            DataTable dtGridData = new DataTable();

            inputDocNode = InputXMLDoc("BRSModifierValuesAdaptor.Get");
            strCWSSrvcOutput = AppHelper.CallCWSService(inputDocNode.InnerXml);

            xmlOutDoc.LoadXml(strCWSSrvcOutput);

            GridHeaderAndData(xmlOutDoc.SelectSingleNode("ResultMessage/Document/PassToWebService/BRSModifierValuesList/listhead"), xmlOutDoc.SelectNodes("ResultMessage/Document/PassToWebService/BRSModifierValuesList/listrow"), dtGridData);

            GridView1.DataSource = dtGridData;
            GridView1.DataBind();

            if (bNoRecords)
            {
                ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>document.forms[0].selectrdo.style.display = 'none';</script>");
            }
        }

        private XmlNode InputXMLDoc(string sFunctionToCall)
        {

            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>" + sFunctionToCall + @"</Function> 
                </Call>
                <Document>
                    <PassToWebService>
                        <UserTableName>" + hdnUserTableName.Value  + @"</UserTableName> 
                        <tableid>" + hdnTableId.Value + @"</tableid> 
                        <BRSModifierValuesList>
                        <listhead> 
                        <ModifierCode>Modifier Code</ModifierCode> 
                        <StartCPT>Start CPT</StartCPT> 
                        <EndCPT>End CPT</EndCPT> 
                        <ModValue>Modifier Value(%)</ModValue> 
                        <MaxRLV>Maximum RLV</MaxRLV> 
                        <RowId>Row Id</RowId>                     
                        </listhead>
                        </BRSModifierValuesList>
                    </PassToWebService>
                </Document>
                </Message>
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;
        }

         private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;


            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.Name;

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();

                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {  
                            dr[iColumnIndex++] = objElem.InnerXml;
                    }
                    dtGridData.Rows.Add(dr);

                }
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
                bNoRecords = true;
            }



        }

         protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
         {
             string javascriptSelectRow = string.Empty;

             if (e.Row.RowType == DataControlRowType.DataRow)
             {
                 javascriptSelectRow = "SelectGridRow('" + DataBinder.Eval(e.Row.DataItem, "RowId").ToString()  + "')";

                 //showing details of a particular row , after on click it at ,client side
                 e.Row.Attributes.Add("onclick", javascriptSelectRow);
             }
         }

         protected void Delete_BRSFeeTableGrid_Click(object sender, ImageClickEventArgs e)
         {
             
             XmlDocument xmlNodeDoc = new XmlDocument();
             XElement oTemplate = XElement.Parse(@"
                                <Message>
                                <Authorization></Authorization> 
                                <Call>
                                <Function>BRSModifierValueAdaptor.Delete</Function> 
                                </Call>
                                <Document>
                                <form name='ModifierValue' supp='' title='Modifier Value'>
                                <group name='ModifierValue' title='Modifier Value'>
                                <displaycolumn>
                                <control name='RowId' type='id'>" + hdnSelectedId.Value + @"</control> 
                                </displaycolumn>
                                </group>
                                </form>
                                </Document>
                                </Message>");

             using (XmlReader reader = oTemplate.CreateReader())
             {
                 xmlNodeDoc.Load(reader);
             }
             hdnMode.Value = "";
             string strCWSSrvcOutput = string.Empty;
             XmlDocument xmlOutDoc = new XmlDocument();


             strCWSSrvcOutput = AppHelper.CallCWSService(xmlNodeDoc.InnerXml);

             LoadModifierValues();
         }
    }
}
