﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BRSFactors.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.BRS_Setup.BRSFactors" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Factors</title>
    <script type="text/javascript" language="javascript">
        function SelectGridRow(rowId) {
            document.forms[0].hdnRowId.value = rowId;
        }

        function openBRSFactor(Mode, width, height) {

            if (Mode == "edit") {
                if (document.forms[0].hdnRowId.value != "") {
                    window.open("BRSFactor.aspx?" + "selectedid=" + document.forms[0].hdnRowId.value, "BRSFactor",
					'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=yes');
                }
                else {
                    alert("Please select a record to edit");
                }
            }
            else {
                window.open("BRSFactor.aspx?" + "tableid=" + document.forms[0].hdnTableId.value, "BRSFactor",
					'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=yes');

            }
            return false;
        }
    
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
     <input type="hidden" id="hdnUserTableName" runat="server" />
     <input type="hidden" id="hdnCalcTypeCode" runat="server" />
     <input type="hidden" id="hdnTableId" runat="server" />
     <input type="hidden" id="hdnTableType" />
     <input type="hidden" id="hdnRowId" runat="server" />
     <input type="hidden" runat="server" id="hdnMode" />
     <div>
       <table  id="tblGrid" runat="server"  width="550px" cellspacing="0" cellpadding="0" border="0">
         <tr>
				<td colspan="2" class="ctrlgroup" id="tdHead" runat="server">
				
				</td>
		</tr>
		<tr>
		        <td colspan="2"><b></b></td>
		</tr>
		<tr>
		       <td width="90%" colspan="1" class="singleborder" >
		          <div id="divGrid" style="overflow:auto; height:400px"  >
		           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%"  AutoGenerateColumns="false"  
                            GridLines="None" onrowdatabound="GridView1_RowDataBound"  >
		              <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="data2" />
		              <Columns>
		                  <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                      <input type="radio" id='selectrdo' name="BRSFeeTableGrid"  />
                                    </ItemTemplate> 
                          </asp:TemplateField> 
                          <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ClinicalCatCode")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Clinical Category" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ClinicalCategory")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Conversion Factor" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConversionFact")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
		              </Columns> 
		           </asp:GridView>
		           </div>
		       </td>
		       <td width="5%" valign="top" colspan="1">
		        
		        <input type="image" src="../../../../Images/new.gif" alt="" id="New_BRSFeeTableGrid" onmouseover="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new2.gif'" onmouseout="javascript:document.all['New_BRSFeeTableGrid'].src='../../../../Images/new.gif'" title="New" onclick="return openBRSFactor('add',500,580);" /><br />
		        <input type="image" src="../../../../Images/edittoolbar.gif" alt="" id="Edit_BRSFeeTableGrid" onmouseover="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar2.gif'" onmouseout="javascript:document.all['Edit_BRSFeeTableGrid'].src='../../../../Images/edittoolbar.gif'" title="Edit" onclick="return openBRSFactor('edit',500,580);" /><br />
		        <asp:ImageButton runat="server" src="../../../../Images/delete.gif" alt="" 
                       id="Delete_BRSFeeTableGrid" 
                       onmouseover="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete2.gif'" 
                       onmouseout="javascript:document.all['Delete_BRSFeeTableGrid'].src='../../../../Images/delete.gif'" 
                       title="Delete" 
                       onclick="Delete_BRSFeeTableGrid_Click" 
                        />
		        
		        
		       </td>
		</tr>
        <tr>
         <td>
         </td>
        </tr>		 
       </table>
    </div>
    </form>
</body>
</html>
