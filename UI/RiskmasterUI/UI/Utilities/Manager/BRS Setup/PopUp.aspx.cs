﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
//using Riskmaster.UI.DocumentService;
//using Riskmaster.UI.DataStreamingService;
using CuteWebUI;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models; //MITS 34822 - hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class PopUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //MITS 34822 hlv begin
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PopUp.aspx"), "PopUpLabel",
                ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "PopUpLabel", sValidationResources, true);
            //MITS 34822 hlv begin

            UploadDocumentAttachments.RemoveButtonText = GetResourceValue("lblRemove", "0");
            UploadDocumentAttachments.CancelText = GetResourceValue("btnCancel", "4");
            UploadDocumentAttachments.UploadingMsg = GetResourceValue("lblUploading", "0");
            UploadDocumentAttachments.TableHeaderTemplate = UploadDocumentAttachments.TableHeaderTemplate.Substring(0, 29) + GetResourceValue("lblFile", "0") + "</td>";
            UploadDocumentAttachments.CancelAllMsg = GetResourceValue("btnCancelAll", "4");

        }

        protected void OnAttachmentRemoveClicked_Click(object sender, CuteWebUI.AttachmentItemEventArgs e)
        {
            e.Item.Remove();
        }

        protected void SaveData_Click(object sender, EventArgs e)
        {
            try
            {
                if (UploadDocumentAttachments.Items.Count > 0)
                {
                   // StreamedBRSDocumentType oStreamedBRSDocumentType = new StreamedBRSDocumentType();
                    Riskmaster.UI.DataStreamingService.StreamedBRSDocumentType oStreamBRSDocType = new Riskmaster.UI.DataStreamingService.StreamedBRSDocumentType();

                    oStreamBRSDocType.Token = AppHelper.GetSessionId();

                    Riskmaster.UI.DataStreamingService.DataStreamingServiceClient rmservice = null;
                    //RMServiceType returnVal = null;

                    rmservice = new Riskmaster.UI.DataStreamingService.DataStreamingServiceClient();

                    try
                    {
                        foreach (AttachmentItem item in UploadDocumentAttachments.Items)
                        {
                            Stream data = item.OpenStream();
                            oStreamBRSDocType.fileStream = data;
                           // oStreamBRSDocType.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                            oStreamBRSDocType.FileName = item.FileName;
                            oStreamBRSDocType.ClientId = AppHelper.ClientId;//Code change by kuladeep for Jira-62
                            rmservice.UploadBRSFeeScheuleFile(oStreamBRSDocType.ClientId, oStreamBRSDocType.Errors, oStreamBRSDocType.FileName, oStreamBRSDocType.Token, oStreamBRSDocType.fileStream);//sdtDocument.Errors, sdtDocument.FileName, sdtDocument.Token, sdtDocument.fileStream, sdtDocument.ClientId);
                           // AppHelper.GetResponse("RMService/DataStream/addbrsfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oStreamedBRSDocumentType);
                            if (!string.IsNullOrEmpty(filename.Value))
                            {
                                filename.Value += ",";
                            }
                            filename.Value += item.FileName;
                        }
                    }
                    finally
                    {
                        if (rmservice != null)
                            rmservice.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("PopUp.aspx"), strResourceType).ToString();
        } 
    }
}
