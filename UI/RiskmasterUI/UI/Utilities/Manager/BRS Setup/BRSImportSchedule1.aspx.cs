﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager; //MITS 34756 - hlv

namespace Riskmaster.UI.UI.Utilities.Manager.BRS_Setup
{
    public partial class BRSImportSchedule1 : NonFDMBasePageCWS
    {

        private string pageID = RMXResourceProvider.PageId("BRSImportSchedule1.aspx"); //MITS 34756 - hlv

        XmlDocument xmlNodeDocument = new XmlDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            //MITS 34721 - hlv begin 
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, pageID, "BRSImportSchedule1",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "BRSImportSchedule1", sValidationResources, true);
            //MITS 34721 - hlv end

            string sCWSresponse;
            try
            {
            if (!IsPostBack)
            {
                DeleteFiles();               
            }

            if (CallImport.Text == "Yes" && UploadStatus.Text != "-1")
            {
                CallCWSFunction("BRSImportScheduleAdaptor.ImportSchedule", out sCWSresponse);
                xmlNodeDocument.LoadXml(sCWSresponse);
                if (xmlNodeDocument.InnerXml == "" || xmlNodeDocument.SelectSingleNode("//MsgStatusCd").InnerText != "Success")
                {
                    hdnBreak.Text = "1";
                    ErrorControl1.errorDom = sCWSresponse;
                    throw new Exception();
                }
                else
                {
                    UploadStatus.Text = xmlNodeDocument.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='UploadStatus']").InnerText;
                    if (UploadStatus.Text != "-1")
                    {
                        ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>BRSImportFinish_Load();</script>");
                    }
                    else
                    {
                        //Msg.InnerText = "Files uploaded successfully.";
                        Msg.InnerText = AppHelper.GetResourceValue(this.pageID, "lblFUpSucess", "0"); //MITS 34756 hlv
                        ImageGlobe.Visible = false;
                        CallImport.Text = "No";
                        btnBack.Visible = false;
                        btnFinish.Visible = true;
                        Next.Visible = false;
                        FeeSched.Visible = true;
                    }
                }
            }

            if (StepNo.Text != "2")
            {
                Next.Enabled = true;
                if (StepNo.Text == "1")
                {
                    btnFinish.Enabled = false;
                }
            }
            }
            catch(Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void DeleteFiles()
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>BRSImportScheduleAdaptor.DeleteFiles</Function> 
                </Call>
                <Document>
                <DeleteFiles /> 
                </Document>
                </Message>");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }
            AppHelper.CallCWSService(xmlNodeDoc.InnerXml);


        }


        protected void Next_Click(object sender, EventArgs e)
        {
            Next.Enabled = false;
            CallImport.Text = "No";
            btnFinish.Enabled = true;
           
            if (FeeScheduleType.SelectedItem.Value == "19")
            {
                //MITS 34756 hlv begin
                /*
                colCptRuv.InnerText = "CPT/RVU .xls:  ";
                colGpci.InnerText  = "GPCI by ZIP .xls  :";
                colMod.InnerText = "Mods.xls:  ";
                */
                colCptRuv.InnerText = AppHelper.GetResourceValue(this.pageID, "lblCptRuv", "0");
                colGpci.InnerText = AppHelper.GetResourceValue(this.pageID, "lblGpci", "0");
                colMod.InnerText = AppHelper.GetResourceValue(this.pageID, "lblMod", "0");
                //MITS 34756 hlv end

                CptRvu.Visible = true;
                Gpci.Visible = true;
                Mods.Visible = true;
            }
            else
            {
                colCptRuv.InnerText = "";
                colGpci.InnerText = "";
                colMod.InnerText = "";
                CptRvu.Visible = false;
                Gpci.Visible = false;
                Mods.Visible = false;
            }
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            string sCWSresponse;
            bool bReturnStatus = false;
            CallImport.Text = "Yes";
            //bReturnStatus = CallCWSFunction("BRSImportScheduleAdaptor.ImportSchedule");   csingh7 : MITS 14906
            CallCWSFunction("BRSImportScheduleAdaptor.ImportSchedule", out sCWSresponse);
            xmlNodeDocument.LoadXml(sCWSresponse);
            if (xmlNodeDocument.SelectSingleNode("//MsgStatusCd").InnerText == "Success")    
                UploadStatus.Text = xmlNodeDocument.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='UploadStatus']").InnerText;
            
            ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>BRSImportFinish_Load();</script>");
            
            btnFinish.Enabled = false;
            Next.Style.Add("Display", "none");
            btnBack.Style.Add("Display", "none");
        }

        protected void Provider_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeleteFiles();
            Files.Text = "";
            CallImport.Text = "No";
            FeeScheduleType.Items.Clear();

            if (Provider.SelectedValue == "1")
            {
                //MITS 34756 hlv begin
                /*
                FeeScheduleType.Items.Add(new ListItem("Workers' Compensation 1999 (old)", "2"));
                FeeScheduleType.Items.Add(new ListItem("Workers' Compensation 2000", "13"));
                FeeScheduleType.Items.Add(new ListItem("UCR 1994 (old)", "0"));
                FeeScheduleType.Items.Add(new ListItem("UCR 1995 to Present", "1"));
                FeeScheduleType.Items.Add(new ListItem("Anesthesia", "7"));
                FeeScheduleType.Items.Add(new ListItem("Dental UCR-1995 Style", "8"));
                FeeScheduleType.Items.Add(new ListItem("Outpatient Style", "3"));
                FeeScheduleType.Items.Add(new ListItem("HCPCS Style", "4"));              
                */
                string[] itemsId = new string[8] { "2", "13", "0", "1", "7", "8", "3", "4" };

                for (int i = 1; i < 9; i++)
                {
                    FeeScheduleType.Items.Add(new ListItem(AppHelper.GetResourceValue(this.pageID, "lblFeeTypeItem" + i.ToString(), "0"), itemsId[i-1]));
                }
                //MITS 34756 hlv end
            }
            if (Provider.SelectedValue == "2")
            {
                //MITS 34756 hlv begin
                /*
                FeeScheduleType.Items.Add(new ListItem("OWCP Fee Schedule", "19"));
                FeeScheduleType.SelectedItem.Text = "OWCP Fee Schedule";
                */
                FeeScheduleType.Items.Add(new ListItem(AppHelper.GetResourceValue(this.pageID, "lblOWCFee", "0"), "19"));
                FeeScheduleType.SelectedItem.Text = AppHelper.GetResourceValue(this.pageID, "lblOWCFee", "0");
                //MITS 34756 hlv end

                FeeScheduleType.SelectedItem.Value = "19";                            
            }           
        }

        protected void FeeSched_Click(object sender, EventArgs e)
        {
            Response.Redirect("BRSFeeTable.aspx");
        }
    }
}
