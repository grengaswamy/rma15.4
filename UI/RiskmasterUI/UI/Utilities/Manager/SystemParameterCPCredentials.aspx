﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemParameterCPCredentials.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.SystemParameterCPCredentials" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Claimant Processing Credentials</title>
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../../Scripts/WaitDialog.js">{var i;}</script>
    <script language="javascript" type="text/javascript" src="../../../Scripts/Utilities.js"></script>
</head>
<body onload="CPCredentials_Load();">
    <form id="frmData" runat="server">
    <div>
     <div class="msgheader" id="formtitle">Claimant Processing Credentials</div>
   <div class="errtextheader"></div>
   <table border="0">
    <tbody>
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
         <%-- <td class="ctrlgroup" colSpan="2">Claimant Processing Credentials</td>--%>
         </tr>
         <tr>
          <td><u>Claimant Processing Web Service URL:</u>:&nbsp;&nbsp;
          </td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
               <asp:TextBox ID="CPUrl" type="text" value="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CPUrl']"  onchange="setDataChanged(true);" size="30" runat="server"></asp:TextBox>
           </div>
          </td>
         </tr>
         <tr>
          <td>Claimant Processing Web Service Login::&nbsp;&nbsp;</td>
          <%--<td></td>--%>
         </tr>
         <tr>
          You must have credentials assigned by Claimant Processing Service before this Web Service can be used:
          <%--<td></td>--%>
         </tr>
         <tr>
          <td>User ID::&nbsp;&nbsp;</td>
          <td>
           <div title="" style="padding: 0px; margin: 0px">
               <asp:TextBox ID="User" onchange="setDataChanged(true);" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='User']" type="text" value="" size="30" runat="server"></asp:TextBox>
          </div>
          </td>
         </tr>
         <tr>
          <td>Password::&nbsp;&nbsp;</td>
          <td>
              <input id="Pwd"  value="" size="30" type="password" runat="server" autocomplete="off"/>
          </td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td>
          <script language="JavaScript" type="text/javascript" >
          {
          var i;
          }
          </script>
          <input type="button" class="button" name="Ok" value="OK" onclick="SystemParameterCP_Ok();" id="Ok" />
          <script language="JavaScript" type="text/javascript">
          {var i;}
          </script>
          <input type="button" class="button" name="Cancel" value="Cancel" onclick="SystemParameterCP_Cancel();" id="Cancel" />
          </td>
         </tr>
        </tbody>
       </table>
       <input type="text" name="" value="" id="SysViewType" style="display:none"/>
       <input type="text" name="" value="" id="SysCmd" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
       <input type="text" name="" value="" id="SysCmdQueue" style="display:none">
       <input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
       <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
       <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
       <input type="text" name="" value="" id="SysFormIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value="RowId"/>
       <input type="text" name="" value="" id="SysFormPForm" style="display:none" rmxforms:value="SystemParameterCPCredentials"><input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""><input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="SystemParameterCPCredentials"><input type="text" name="" value="CPUrl|" id="SysRequired" style="display:none"/>
       </td>
      <td valign="top"></td>
     </tr>
    </tbody>
   </table><input type="hidden" name="$node^5" value="" id="SysWindowId" />
   <%--<input type="hidden" name="$instance" value="H4sIAAAAAAAAAK1W32/aMBB+7v4KKw/T9gBu6VsLaBJ9QaMqaovWaZoqkxzg4diZ7ZDkv985vyBA&#xA;aDvRhyp39/nuvrsvMf2xNJZJH0gaCmluUj7wVtZGN5QmSdJNrrtKL2nv8vKavoylL+IAvBIaaVjw&#xA;tAXeo+lC6dBUYNi0AK8oAjqwAWlrcBo3wOgM5jk+jaOA2boDlS4aQKXnoGTXVyHFEI208sEYpevE&#xA;0Uk4j0BwWWcvCLzFb/jp4uKC4F8/BMtyKzdzl/E1gOzwYEg1N+uQGQuaPt7PLBfccjD0KUNXOGWa&#xA;4XHQo+lIQ4Cz4EyYPt2eb+aNDWjn9Y3fp5WxVxqZc+XO0rrFPJBwGajEHejT7XMdfgZjv0NWpK6M&#xA;PEJrgkUPfK8i8y0WpHveCZdrTLHvxq3Yjq9EHB6cyENKBzipYxEUAOvYLIKD6AqpgNa47+ECxwc4&#xA;vx1XE1w4t6PBMfKt8VQMbywXageS8htevAJkhdofeN0ufbmf0OoV6jxDGAnUZ2c2dqL1ds7eKT8O&#xA;ca97bTgNEYm7H3inlOARy61A0EgwHjJpybTQNpdL0sApKRQLELh7/HWCvi9fbz1S9r/gAoqquObO&#xA;lC1RiS0CHakwxK3Wju6fUvNbFjmTpVZxdE4qZZFGlbxSwA1OOSvEU6EOYDnUV9JqJcq2RtOZFlgf&#xA;tTPwLKT2ZC8/YE6eQG84fhtnj5Mbj+Q7r7dNq5VSt0Sa86eN3mhZ/tc3V58MPrNImdu8i+Lxt8v5&#xA;N+bIeuBlgJz71ZmP8JqwOYiriphwFsogeze7iVpyeTZ+RTcVQfphIr12Ij9VTMLYWLJiGyD+ViyE&#xA;IaulhIDMM3KMbkV1DkgGiF1x0xiBzyTGCH5Kg+5ZB9E7GMTBDOhRQZ9P9jO8H46q3gXI+O5sm3f5&#xA;Krr/p+RpElSdRrjSBG+Buttp6Thbu1isvdv3bamJyCsfi8xja5XEy5xHtmT6sK6JucdFLPPbs+X7&#xA;+fqwdp9v+s7kIzcWsX3/S/OtIgWutRCXCJOs2pXJzKsbOA+cXa1txQN8KT2yYSJG81El4+BD6aJz&#xA;53P/WrKdvKf2f17k6tr5vbB7o/drMQ7/Abd39lFRCwAA">--%>
    </div>
    </form>
</body>
</html>
