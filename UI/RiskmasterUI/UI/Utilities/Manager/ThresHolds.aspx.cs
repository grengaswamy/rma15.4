﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.FDM;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    /// <summary>
    /// coded by: Ashutosh Kashyap : akashyap3
    /// completed on 10-Feb-2009
    /// </summary>
    /// 
    public partial class ThresHolds : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes


                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");

                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("MinBillThresholdAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("MinBillThresholdAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                //string strRowId = AppHelper.GetQueryStringValue("selectedrowposition");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>3af7f280-a3cc-4365-a7c7-1312496d57f8</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("MinBillThresholdAdaptor.Get");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><MinBillThreshold>");
            sXml = sXml.Append("<control name='RowId' type='id'>" + RowId.Text + "</control>");
            sXml = sXml.Append("<control name='AddedByUser' type='id'>" + AddedByUser.Text + "</control>");
            sXml = sXml.Append("<control name='DttmRcdAdded' type='id'>" + DttmRcdAdded.Text + "</control>");
            sXml = sXml.Append("<control name='DttmRcdLastUpd' type='id'>" + DttmRcdLastUpd.Text + "</control>");
            sXml = sXml.Append("<control name='UpdatedByUser' type='id'>" + UpdatedByUser.Text + "</control>");
            sXml = sXml.Append("<control name='UseThreshold' type='checkbox'>" + UseThreshold.Checked + "</control>");
            sXml = sXml.Append("<control name='LOBcode' type='code' codetable='POLICY_LOB' codeid='" + LOBcode.CodeIdValue + "'>" + LOBcode.CodeTextValue + "</control>");

            sXml = sXml.Append("<control name='Statecode' type='code' codetable='states' codeid='" + Statecode.CodeIdValue + "'>" + Statecode.CodeTextValue + "</control>");

            sXml = sXml.Append("<control name='Amount' type='numeric'>" + Amount.Text + "</control>");
            sXml = sXml.Append("<control name='EffectiveDate' type='date'>"+ EffectiveDate.Text + "</control>");
            sXml = sXml.Append("<control name='ExpirationDate' type='date'>" + ExpirationDate.Text + "</control>");

            sXml = sXml.Append("<control name='FormMode' type='hidden'>" + FormMode.Text + "</control>");

            sXml = sXml.Append("</MinBillThreshold></Document></Document></Message>");
            
            
            
            
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }
        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("MinBillThresholdAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    //After the validation,there was a need to post back the parent page through javascript 
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                }
                //Start: Sumit-08/18/2010 - MITS# 21804
                else
                {
                    validate.Text = "ValidationFailed";
                }
                //End: Sumit
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
