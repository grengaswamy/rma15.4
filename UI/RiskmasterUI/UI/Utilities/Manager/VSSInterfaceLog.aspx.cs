﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class VSSInterfaceLog : NonFDMBasePageCWS
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        XmlDocument oFDMPageDom = null;
        public string lstPacketTypeVal = null;
        public string lstRequestTypeVal = null;
        public string lstStatusVal = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.FirstTime.Text = "";
            lstPacketTypeVal = "";
            lstRequestTypeVal = "";
            lstStatusVal = "";

            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes



            if (!IsPostBack)
            {
                this.FirstTime.Text = "1";
                NonFDMCWSPageLoad("VSSInterfaceLogAdaptor.Search");
            }
            if (this.lstPacketType.Items.Count > 0)
                this.lstPacketType.Items[0].Value = "";

            if (this.lstRequestType.Items.Count > 0)
                this.lstRequestType.Items[0].Value = "";

            if (this.lstStatus.Items.Count > 0)
                this.lstStatus.Items[0].Value = "";

            ViewState["RequestId"] = this.txtRequestId.Text;
            ViewState["FromDate"] = this.txtFromDate.Text;
            ViewState["ToDate"] = this.txtToDate.Text;
            lstPacketTypeVal = this.lstPacketType.SelectedValue;
            lstRequestTypeVal = this.lstRequestType.SelectedValue;
            lstStatusVal = this.lstStatus.SelectedValue;
        }

        protected void Search_But(object sender, EventArgs e)
        {
            NonFDMCWSPageLoad("VSSInterfaceLogAdaptor.Search");

            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            rootElement = XElement.Parse(oFDMPageDom.OuterXml);

            result = from c in rootElement.XPathSelectElements("SearchResults/row")
                     select c;

            this.lstPacketType.Items[0].Value = "";
            this.lstRequestType.Items[0].Value = "";
            this.lstStatus.Items[0].Value = "";

            this.txtRequestId.Text = ViewState["RequestId"].ToString();
            this.txtFromDate.Text = ViewState["FromDate"].ToString();
            this.txtToDate.Text = ViewState["ToDate"].ToString();


            this.lstPacketType.SelectedValue = lstPacketTypeVal;

            this.lstRequestType.SelectedValue = lstRequestTypeVal;

            this.lstStatus.SelectedValue = lstStatusVal;
        }

        public string ReplaceEscChars(string p_sOriginalString)
        {
            string sReplacedString = string.Empty;
            sReplacedString = p_sOriginalString;
            if (p_sOriginalString.Contains("\'"))
            {
                sReplacedString = sReplacedString.Replace("\'", "(##)=");
            }
            if (p_sOriginalString.Contains("\""))
            {
                sReplacedString = sReplacedString.Replace("\"", "(##)+");
            }
            if (p_sOriginalString.Contains("\\"))
            {
                sReplacedString = sReplacedString.Replace("\\", "(##)");
            }
            if (p_sOriginalString.Contains("\0"))
            {
                sReplacedString = sReplacedString.Replace("\0", "(##)0");
            }
            if (p_sOriginalString.Contains("\a"))
            {
                sReplacedString = sReplacedString.Replace("\a", "(##)a");
            }
            if (p_sOriginalString.Contains("\b"))
            {
                sReplacedString = sReplacedString.Replace("\b", "(##)b");
            }
            if (p_sOriginalString.Contains("\f"))
            {
                sReplacedString = sReplacedString.Replace("\f", "(##)f");
            }
            if (p_sOriginalString.Contains("\n"))
            {
                sReplacedString = sReplacedString.Replace("\n", "(##)n");
            }

            if (p_sOriginalString.Contains("\r"))
            {
                sReplacedString = sReplacedString.Replace("\r", "(##)r");
            }
            if (p_sOriginalString.Contains("\t"))
            {
                sReplacedString = sReplacedString.Replace("\t", "(##)t");
            }

            return sReplacedString;
        }
    }
}