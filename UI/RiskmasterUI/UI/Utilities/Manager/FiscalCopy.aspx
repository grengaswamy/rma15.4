﻿<%@ Page Title="" Language="C#" theme="RMX_Default" AutoEventWireup="true" CodeBehind="FiscalCopy.aspx.cs" Inherits="Riskmaster.UI.Utilities.FiscalCopy" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>
        Fiscal Year Setup
    </title>
     <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css"/>
     <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
     <script type="text/javascript" language="javascript">
       function makeInVisible()
				{
				  ctrl=document.getElementById("txtToFiscalYear");
				  ctrl.style.display="none";
				  ctrl=document.getElementById("lblTo");
				  ctrl.style.display="none";
				  
				}
				function makeVisible()
				{
				  ctrl=document.getElementById("txtToFiscalYear");
				   ctrl.style.display="";
				  ctrl=document.getElementById("lblTo");
				  ctrl.style.display="";
				  
				  
				}
				
				
				function trim(value)
				{
				  value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
				  return value;
				}
				function MandatoryClose()
				{
					ctrl=document.getElementById('rdYears2');
					if (ctrl.checked)
					{
						if (trim(document.forms[0].txtToFiscalYear.value)=='')
						{
							alert("Not all required fields contain the value.");
							document.forms[0].txtToFiscalYear.focus();
							return false;
						}
						if (IsNumeric(trim(document.forms[0].txtToFiscalYear.value)))
						{
							if (!((parseInt(document.forms[0].txtToFiscalYear.value) >= 1900) && (parseInt(document.forms[0].txtToFiscalYear.value) <= 2100)))
							{
								alert("Please enter a four digit fiscal year between 1900 and 2100");
								document.forms[0].txtToFiscalYear.focus();
								return false;
							}
						}
						else
						{
							alert("Please enter a four digit fiscal year between 1900 and 2100");
							document.forms[0].txtToFiscalYear.focus();
							return false;
						}

					}
					else
					{
						document.forms[0].txtToFiscalYear.value="";
					}
					
					
				}
				function Mandatory()
				{
					ctrl=document.getElementById('rdYears2');
					if (ctrl.checked)
					{
						if (trim(document.forms[0].txtToFiscalYear.value)=='')
						{
							alert("Not all required fields contain the value.");
							document.forms[0].txtToFiscalYear.focus();
							return false;
						}
						if (IsNumeric(trim(document.forms[0].txtToFiscalYear.value)))
						{
							if (!((parseInt(document.forms[0].txtToFiscalYear.value) >= 1900) && (parseInt(document.forms[0].txtToFiscalYear.value) <= 2100)))
							{
								alert("Please enter a four digit fiscal year between 1900 and 2100");
								document.forms[0].txtToFiscalYear.focus();
								return false;
							}
						}
						else
						{
							alert("Please enter a four digit fiscal year between 1900 and 2100");
							document.forms[0].txtToFiscalYear.focus();
							return false;
						}

					}
					else
					{
						document.forms[0].txtToFiscalYear.value="";
					}
					return true;
				}
			function IsNumeric(sText)
			{
			var ValidChars = "0123456789.";
			var IsNumber=true;
			var Char;
			for (i = 0; i < sText.length && IsNumber == true; i++) 
				{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
					{
					IsNumber = false;
					}
				}
			return IsNumber;
			   
			}


				function onPageLoaded()
				{
					document.getElementById('rdYears').checked=true;
				}
				
				 function openOrgPage()
				{
					m_sFieldName='orgcode';
					m_codeWindow=window.open('../../OrganisationHierarchy/OrgHierarchyLookup.aspx?amp;tablename=nothing&rowid=all&lob=all','Table',
				'width=700,height=600'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
				
				   
				}
     
     </script>
</head>
<body>
<form id="frmData" runat="server" name="frmData" autocomplete="off" method="post">     
     
     


    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
     
     


    <table border="0" width="100%">
        <tr>
            <td class="ctrlgroup" colspan="2">
                Fiscal Copy
            </td>
        </tr>
        
        <tr>
            <td>
                From Line of Business :
            </td>
            <td>
                <asp:textbox id="txtFromLineOfBusiness" name="txtFromLineOfBusiness" rmxref="/Instance/Document/FiscalCopy/FromLineOfBusiness/@text" readonly="true" width="200px" runat="server" />
                <asp:TextBox runat="server" style="display:none" rmxref="/Instance/Document/FiscalCopy/FromLineOfBusiness/@value" id="hdnFromLineOfBusiness"></asp:TextBox>	
            </td>
        </tr>
        
        <tr>
            <td>
                From Organization :
            </td>
            <td>
                <asp:textbox id="txtFromOrganization" name="txtFromOrganization" rmxref="/Instance/Document/FiscalCopy/FromOrganization/@text" readonly="true" width="200px" runat="server" />
                <asp:textbox style="display:none" runat="server" rmxref="/Instance/Document/FiscalCopy/FromOrganization/@value" id="hdnFromOrganization" ></asp:textbox>
            </td>
        </tr>
    
        <tr>
            <td align="right">
                <asp:image imageurl="~/Images/ArrowWPAUtil1.jpg" runat="server" />
            </td>
            <td align="left">
                All Years <input type="radio" name="rdYears" id="rdYears1" onClick="makeInVisible()" value="0">
                <br />
                Only <asp:Label ID="lblFsYear" runat="server"></asp:Label>  <input type="radio" name="rdYears" id="rdYears2" onClick="makeVisible()" value="1"><Label id="lblTo" style="display:none;">To Fiscal Year</Label> <asp:textbox id="txtToFiscalYear" style="display:none;" name="txtToFiscalYear" runat="server" width="50px" rmxref="/Instance/Document/FiscalCopy/ToFiscalYear" />
            </td>
        </tr>
        
        <tr>
            <td>
                To Line of Business :
            </td>
            <td>
                <asp:textbox id="lobcode" name="lobcode" rmxref="/Instance/Document/FiscalCopy/ToLineOfBusiness/@text" width="200px" runat="server" />
                <asp:textbox  runat="server" id="lobcode_cid" style="display:none" rmxref="/Instance/Document/FiscalCopy/ToLineOfBusiness/@value"></asp:textbox>
                <input type="submit" value="..." class="button" onClick="return selectCode('LINE_OF_BUSINESS','lobcode');" id="lobcodebtn">
            </td>
        </tr>
        
        <tr>
            <td>
                To Organization :
            </td>
            <td>
                <asp:textbox id="orgcode" name="orgcode" rmxref="/Instance/Document/FiscalCopy/ToOrganization/@text" width="200px" runat="server" />
                <asp:TextBox runat="server" rmxref="/Instance/Document/FiscalCopy/ToOrganization/@value" id="orgcode_cid" style="display:none" ></asp:TextBox>
                <input type="button" onClick="openOrgPage();" value="..." class="button" name="orgcodebtn">
            </td>
        </tr>
        
        <tr>
            <td colspan="2">
                <asp:button id="btCopy" name="btCopy" class="button" text="  Copy  " 
                    OnClientClick="return MandatoryClose();" runat="server" 
                    onclick="btCopy_Click" />
                <input type="button" class="button" onclick="self.close();"  value=" Close ">
            </td>
        </tr>
    
    </table>
    
    <asp:TextBox style="display:none" id="hdnFiscalYear" rmxref="/Instance/Document/FiscalCopy/FiscalYear" runat="server" ></asp:TextBox>
     <input type="hidden" id="formname" value=""  />
     
 </form>
</body>
</html>
