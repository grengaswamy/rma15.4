﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class UpdateExistingTasks : NonFDMBasePageCWS
    {
        private bool bReturnStatus = false;
        private string sReturnValue = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UpdatedFlag.Value = "False";                
                if (!IsPostBack)
                {
                    bReturnStatus = CallCWS("TaskManagementAdaptor.GetTasksForTaskMgrDiary", null, out sReturnValue , true, true);              
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private void GetSelectedListItem()
        {
            string sValues = string.Empty;
            string sTaskIndex = string.Empty;
            int iUpperBound = 0;
            
            txtTaskIndex.Text = "";
            txtTaskIndex.Text = "";

            for (int iCount = 0; iCount < lstTasks.Items.Count; iCount++)
            {
                if (lstTasks.Items[iCount].Selected)
                {
                    sValues += lstTasks.Items[iCount].Value + " ";
                    sTaskIndex += iCount + " ";
                }
            }
            if (sValues.Length > 0)
            {
                txtTasks.Text = sValues.Substring(0, sValues.Length - 1);
                txtTaskIndex.Text = sTaskIndex.Substring(0, sTaskIndex.Length - 1); 
            }

            txtDataSourceName.Text = lstDataSources.SelectedItem.Text;
            txtDataSourceId.Text = lstDataSources.SelectedItem.Value;            
           
            txtUsers.Text = lstUsers.SelectedItem.Text;            
            iUpperBound = txtUsers.Text.IndexOf("(");
            txtUsers.Text = txtUsers.Text.Substring(0, iUpperBound - 1);
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {         
            try
            {
                GetSelectedListItem();
                bReturnStatus = CallCWS("TaskManagementAdaptor.UpdateTasksForTaskMgrDiary", null, out sReturnValue, true, true);
                if (bReturnStatus)
                {
                    UpdatedFlag.Value  = "True" ;
                    bReturnStatus = CallCWS("TaskManagementAdaptor.GetTasksForTaskMgrDiary", null, out sReturnValue, true, true);
                    ErrorControl1.errorDom = sReturnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void lstDataSources_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {            
                string[] arrTaskIndex = null;
                int iTaskIndex = 0;
                
                GetSelectedListItem();                
                bReturnStatus = CallCWS("TaskManagementAdaptor.GetTasksForTaskMgrDiary", null, out sReturnValue, true, true);                

                lstDataSources.SelectedItem.Value = txtDataSourceId.Text;
                lstDataSources.SelectedItem.Text = txtDataSourceName.Text;

                if (txtTaskIndex.Text != "")
                {
                    arrTaskIndex = txtTaskIndex.Text.Split(' ');
                    for (int iCount = 0; iCount < arrTaskIndex.Length; iCount++)
                    {
                        iTaskIndex = Convert.ToInt32(arrTaskIndex[iCount]);
                        lstTasks.Items[iTaskIndex].Selected = true;
                    }
                }                
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
