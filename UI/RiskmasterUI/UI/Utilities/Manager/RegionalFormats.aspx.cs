﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.AppHelpers;
using System.Xml;
using System.Text;
using System.Xml.XPath;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class RegionalFormats : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        XElement doc = null;
        string sCWSresponse = "";
        private static string[] sDataSeparator = { "|^|" };

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    XmlTemplate = GetMessageTemplate();
                    CallCWS("MultiLanguageAdaptor.GetFormatLists", XmlTemplate, out sCWSresponse, true, true);
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("MultiLanguageAdaptor.Save", XmlTemplate, out sCWSresponse, true, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.location.reload(true); var bRepeat = true;while (bRepeat) { var sId=window.opener.document.getElementById('LINKTABSGlobalization');if(sId != null || sId != undefined) bRepeat=false; } window.opener.document.getElementById('FORMTABSystemSettings').style.display = \"None\";window.opener.document.getElementById('FORMTABGlobalization').style.display = \"\";window.opener.document.getElementById('TABSSystemSettings').className = \"NotSelected\";window.opener.document.getElementById('LINKTABSSystemSettings').className = \"NotSelected1\";window.opener.document.getElementById('TABSGlobalization').className = \"Selected\";window.opener.document.getElementById('LINKTABSGlobalization').className = \"Selected\";window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate()
        {
            string sCountryId = string.Empty;

            if (mode.Text.ToLower() == "edit")
            {
                sCountryId = AppHelper.GetQueryStringValue("selectedid");
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<form name='RegionalLanguageSetup' title='RegionalFormatSetup'>");
            sXml = sXml.Append("<group name='RegionalLanguageSetup' title='RegionalFormatSetup'>");
            sXml = sXml.Append("<displaycolumn>");
            sXml = sXml.Append("<control name='RowId' type='id'>");
            sXml = sXml.Append(sCountryId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Country' type='id'>");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='ZipCodeFormats' type='id'>");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='PhoneFormats' type='id'>");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='SSNFormats' type='id'>");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</displaycolumn>");
            sXml = sXml.Append("</group></form>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
    }
}