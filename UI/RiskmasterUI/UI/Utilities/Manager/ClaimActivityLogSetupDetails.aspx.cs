﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;

namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class ClaimActivityLogSetupDetails : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bool bReturnStatus = false;
                LogId.Text = AppHelper.GetQueryStringValue("logid");
                bReturnStatus = CallCWSFunction("ClaimActLogAdaptor.GetConfigDetails");
            }
        }
        /// <summary>
        /// override the base class function to change the xml values
        /// </summary>
        /// <param name="Xelement">xml</param>
        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlDoc = new XmlDocument();

            using (XmlReader reader = Xelement.CreateReader())
            {
                xmlDoc.Load(reader);
            }

            XmlNode objXmlNode = xmlDoc.SelectSingleNode("Message/Document/form/group");
            XmlElement xmlEle = (XmlElement)objXmlNode;

            xmlEle.SetAttribute("name", "ClaimActLogDetails");
            xmlEle.SetAttribute("title", hdntitle.Value);

            objXmlNode = xmlDoc.SelectSingleNode("Message/Document/form/group/displaycolumn/control[@name ='OpType']");
            xmlEle = (XmlElement)objXmlNode;
            xmlEle.SetAttribute("codetable", "OPERATION_TYPE");
            objXmlNode = xmlDoc.SelectSingleNode("Message/Document/form/group/displaycolumn/control[@name ='ActType']");
            xmlEle = (XmlElement)objXmlNode;
            xmlEle.SetAttribute("codetable", "CLAIM_ACTIVITY_TYPE");
            Xelement = XElement.Parse(xmlDoc.InnerXml);
        }
        /// <summary>
        /// button click function to save the data. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, ImageClickEventArgs e)
        {

            XmlDocument saveDoc = null;                         //ST:ygoyal3, MITS 35140
            
            bool bReturnStatus = false;
            string sCWSresponse;
            bReturnStatus = CallCWSFunction("ClaimActLogAdaptor.Save", out sCWSresponse);
            //ST:ygoyal3, MITS 35140, Dt:01/31/2014
            saveDoc = new XmlDocument();
            saveDoc.LoadXml(sCWSresponse);
            //if (bReturnStatus)
            if (saveDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
            //Ended:Yukti, Dt:01/31/2014, MITS 35140
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
        }
    }
}