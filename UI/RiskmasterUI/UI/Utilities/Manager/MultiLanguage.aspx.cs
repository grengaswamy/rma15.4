﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.AppHelpers;
using System.Xml;
using System.Text;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;
using System.Globalization;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class MultiLanguage : NonFDMBasePageCWS
  {
    XElement XmlTemplate = null;
    string sCWSresponse = "";

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
          if (!IsPostBack)
          {
                    txtLangCode.Text = AppHelper.GetLanguageCode();
                    txtPageId.Text = RMXResourceProvider.PageId("MultiLanguage.aspx");
              gridname.Text = AppHelper.GetQueryStringValue("gridname");
              mode.Text = AppHelper.GetQueryStringValue("mode");
              selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
              XmlTemplate = GetMessageTemplate();
              CallCWS("MultiLanguageAdaptor.GetSelectedMultiLanguageInfo", XmlTemplate, out sCWSresponse, true, true);
          }

      }
      catch (Exception ee)
      {
        ErrorHelper.logErrors(ee);
        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
      }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
      try
      {
            XmlTemplate = GetMessageTemplate();
            CallCWS("MultiLanguageAdaptor.Save", XmlTemplate, out sCWSresponse, true, false);
            XmlDocument objReturnXml = new XmlDocument();
            objReturnXml.LoadXml(sCWSresponse);
            string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
            if (sMsgStatus == "Success")
            {
                    //ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.location.reload(true); var bRepeat = true;while (bRepeat) { var sId=window.opener.document.getElementById('LINKTABSGlobalization');if(sId != null || sId != undefined) bRepeat=false; } window.opener.document.getElementById('FORMTABSystemSettings').style.display = \"None\";window.opener.document.getElementById('FORMTABGlobalization').style.display = \"\";window.opener.document.getElementById('TABSSystemSettings').className = \"NotSelected\";window.opener.document.getElementById('LINKTABSSystemSettings').className = \"NotSelected1\";window.opener.document.getElementById('TABSGlobalization').className = \"Selected\";window.opener.document.getElementById('LINKTABSGlobalization').className = \"Selected\";window.close();</script>");
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.location.reload(true); var bRepeat = true;window.close();</script>");
            }
      }
      catch (Exception ee)
      {
        ErrorHelper.logErrors(ee);
        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
      }

    }
    private XElement GetMessageTemplate()
    {
      string sLangId = string.Empty;

      if (mode.Text.ToLower() == "edit")
      {
          sLangId = AppHelper.GetQueryStringValue("selectedid");
      }
      StringBuilder sXml = new StringBuilder("<Message>");
      sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
      sXml = sXml.Append("<Call><Function></Function></Call><Document>");
      sXml = sXml.Append("<form name='MultiLanguageSetup' title='MultiLanguage'>");
      sXml = sXml.Append("<group name='MultiLanguageSetup' title='MultiLanguage'>");
      sXml = sXml.Append("<displaycolumn>");
      sXml = sXml.Append("<control name='RowId' type='id'>");
      sXml = sXml.Append(sLangId);
      sXml = sXml.Append("</control>");
      sXml = sXml.Append("<control name='LanguageName' type='id'>");
      sXml = sXml.Append("</control>");
      sXml = sXml.Append("<control name='SupportedFlag' type='id'>");
      sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='FormatDate' type='id'>");// Add By ttumula2 on 16 Dec 2014(RMA-6033)
            sXml = sXml.Append("</control>");// Add By ttumula2 on 16 Dec 2014(RMA-6033)
      sXml = sXml.Append("</displaycolumn>");
      sXml = sXml.Append("</group></form>");
      sXml = sXml.Append("</Document>");
      sXml = sXml.Append("</Message>");
      XElement oElement = XElement.Parse(sXml.ToString());
      return oElement;
    }
  }
}