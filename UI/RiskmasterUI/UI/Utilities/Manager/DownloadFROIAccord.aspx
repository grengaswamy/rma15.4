﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadFROIAccord.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.DownloadFROIAccord" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" tagprefix="uc3"  %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>

<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language ="javascript" type="text/javascript">
        function OpenPage(status) 
        {
            __doPostBack("btnActDownload", status);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
    <%-- <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage="Loading..."/>--%>
    <asp:scriptmanager id="ScriptManager1" runat="server" AsyncPostBackTimeout="120"/>
    <asp:updatepanel id="UpdatePanel1" runat="server">
        <contenttemplate>
            <asp:Timer ID="Timer1" runat="server" ontick="Timer1_Tick"  Enabled="false" Interval="60" > </asp:Timer>
            <asp:Timer ID="Timer2" runat="server" ontick="Timer2_Tick"  Enabled="false" Interval="60" > </asp:Timer>
        </contenttemplate> 
    </asp:updatepanel> 

    <asp:Image ID="imgProcessing" runat="server" Visible="false"  ImageUrl="../../../Images/loading1.gif"/>
    <asp:Label ID="lblDownloading" runat="server" Text="Downloading in Process" Font-Bold="true" Visible="false" ForeColor="Red"></asp:Label>
    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" onclick="btnBack_Click"/>
    <asp:Button ID="btnActDownload" runat="server"  Text="ActivateDownload" 
              onclick="btnActDownload_Click" />
    </form>
</body>
</html>
