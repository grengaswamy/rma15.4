﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JurisdictionalData.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.JurisdictionalData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Jurisdictional Data</title>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script language="javascript" type="text/javascript" >
        var qString = "";
        //Setting query string on row selection
        function SelectAdminRow(strRowId)
        {
            qString =  strRowId;
            document.getElementById('currentSelRowId').value = strRowId;
        }
    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server" name="frmData">
    <div id="maindiv" style="height:100%;width:99%;overflow:auto">
    <div>
        <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    </div>
    <table border="0" width="100%" cellspacing="0" cellpadding="0" >
      <tr>
       <td valign="top">
       <div class="ctrlgroup" id="formtitle" style="width:950px" runat="server">Jurisdictional Data</div>
       
       </td>
      </tr>
      <tr>
        <td>
      <div style="height:90%;">
      <table border="0" width="90%" >
       <tr>
         <td></td>
       </tr>
       <tr>
       <td width="85%" valign="top" class="singleborder" >
         <asp:GridView ID="GridView1" runat="server"   AutoGenerateColumns="false" 
               AllowPaging="false"  width="100%" Height="100%"   GridLines="None" 
               onrowdatabound="GridView1_RowDataBound">
		    <HeaderStyle CssClass="msgheader" />
		     <AlternatingRowStyle CssClass="data2" />
		    <Columns>
		        <asp:TemplateField  ItemStyle-CssClass="data" ControlStyle-Width="5%">
                    <ItemTemplate>
                       <input type="radio" id="selectrdo" name="JurisdictionalDataGrid"  />
                    </ItemTemplate> 
                </asp:TemplateField>
                        <asp:TemplateField HeaderText="Row Id" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RowId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Table Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserTableName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="System Table Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblSysTableName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SysTableName")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
            </Columns>
		     
		     
		 </asp:GridView>
       </td>
       <td width="15%" valign="top">
         <input type="image" onclick="openGridAddEditWindow('JurisdictionalDataGrid','edit',800,450);return false;" id="Edit_JurisdictionalGrid" src="../../../Images/edittoolbar.gif" onmouseover="&#xA;javascript:document.all[&#34;Edit_JurisdictionalGrid&#34;].src='../../../Images/edittoolbar2.gif';&#xA;" onmouseout="&#xA;javascript:document.all[&#34;Edit_JurisdictionalGrid&#34;].src='../../../Images/edittoolbar.gif';&#xA;" title="Edit" /><br />
       </td>
       </tr>
       <tr>
         <td width="5%" valign="top">
          <input type="text" style="display:none" runat="server" id="currentSelRowId" value="" />
          
         </td>
       </tr>
       
      </table>
      </div>
      </td>
      </tr>
    </table>
    </div>
    </form>
</body>
</html>
