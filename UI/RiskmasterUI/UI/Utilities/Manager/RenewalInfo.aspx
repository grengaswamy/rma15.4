﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RenewalInfo.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.RenewalInfo" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc3" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add/Modify Renewal Info</title>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/grid.js">{var i;}</script>

    <script type="text/javascript" language="JavaScript" src="/RiskmasterUI/Scripts/form.js">{var i;}</script>
 <uc3:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload="CopyGridRowDataToPopup();">
  <%--<script type="text/javascript" language="javascript">

      function Closewindow() {
          if (document.getElementById('hdsavecomplete').value == "true") 
          {
              document.getElementById('hdsavecomplete').value = "";
              window.opener.ReSubmitPage();
              window.close();
          }
      }
        
    </script>--%>
    <form id="frmData" runat="server" method="post">
    <asp:HiddenField ID="hdsavecomplete" runat="server" />
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../../Images/save.gif" Width="28" Height="28"
                border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../../Images/save2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../../Images/save.gif';this.style.zoom='100%'" 
                OnClientClick="return ValidateFieldsforRenewalInfo();" onclick="save_Click" /></div>
    </div>
    <div class="msgheader" id="formtitle">Add/Modify Renewal Info</div>
    <table><tr><td class="ctrlgroup" colSpan="2">Renewal Info</td></tr></table>
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_LOBcode" Text="Line of Business" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ID="LOBcode" CodeTable="POLICY_LOB" ControlName="LOBcode"
                    rmxref="/Instance/Document/Document/RenewalInfo/control[@name='LOBcode']" type="code" tabindex="1"
                    required="true" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Statecode" Text="State" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ID="Statecode" CodeTable="states" ControlName="Statecode"
                    rmxref="/Instance/Document/Document/RenewalInfo/control[@name='Statecode']" type="code" tabindex="2"
                    required="true" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_Amount" Text="Term Length" class="required" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="Amount" RMXRef="/Instance/Document/Document/RenewalInfo/control[@name='Amount']"
                    TabIndex="3" onblur="numLostFocus(this);" onChange="setDataChanged(true);" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lbl_TermCode" Text="Term Code" class="required" />
            </td>
            <td>
                <uc2:CodeLookUp runat="server" ID="TermCode" CodeTable="TERM_CODE" ControlName="TermCode"
                    rmxref="/Instance/Document/Document/RenewalInfo/control[@name='TermCode']" type="code" tabindex="15"
                    required="true" />
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/RenewalInfo/control[@name ='RowId']" />    
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="txtvalidate" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="FormMode" />
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
