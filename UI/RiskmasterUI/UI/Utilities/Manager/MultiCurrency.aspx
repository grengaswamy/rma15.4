<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiCurrency.aspx.cs" Inherits="Riskmaster.UI.Utilities.Manager.MultiCurrency" ValidateRequest="false" %>


<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
   <title>Add/Modify Currency</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css"  type="text/css" />
    <link rel="stylesheet" href="../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js">      { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/supportscreens.js">      { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/grid.js">      { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
     <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Multi Currency" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/option/CurrencyRowId" 
        rmxignoreset='true' />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="MultiCurrencyIDLoadvalue" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>          
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblSourceCurrency" Text="Source Currency:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc2:CodeLookUp runat="server" ID="SourceCurrency" CodeTable="CURRENCY_TYPE" ControlName="SourceCurrency" CodeFilter="" RMXType="code"  RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='SourceCurrency']"  />               
                </td>
            </tr>
             <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblDestinationCurrency" Text="Destination Currency:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <uc2:CodeLookUp runat="server" ID="DestinationCurrency" CodeTable="CURRENCY_TYPE" ControlName="DestinationCurrency" CodeFilter="" RMXType="code"  RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='DestinationCurrency']"  />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblExchangeRate" Text="Exchange Rate:" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" onchange="setDataChanged(true);"
                ID="ExchangeRate" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='ExchangeRate']" onblur="return numLostFocusNonNegative(this);"
                MaxLength = "9" type="numeric" />
                </td>
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="Save" Width="75px"
                OnClientClick="return MultiCurrency_onOk();" OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                 OnClientClick="return MultiCurrency_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    </form>
</body>
</html>
