﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
namespace Riskmaster.UI.Utilities.Manager
{
    public partial class PolicyOptions : NonFDMBasePageCWS
    {
        string sCWSresponse = string.Empty  ;
        XElement XmlTemplate = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (TermInfoGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = TermInfoGridSelectedId.Text;
                HdnListNameForDeletion.Text = "RenewalInfoList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                TermInfoGrid_RowDeletedFlag.Text = "false";
            }
            if (TierGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = TierGridSelectedId.Text;
                HdnListNameForDeletion.Text = "TierList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                TierGrid_RowDeletedFlag.Text = "false";
            }

            if (ExpConstGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = ExpConstGridSelectedId.Text;
                HdnListNameForDeletion.Text = "ExpConstList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                ExpConstGrid_RowDeletedFlag.Text = "false";
            }

            //akashyap3
            if (ThresholdsGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = ThresholdsGridSelectedId.Text;
                HdnListNameForDeletion.Text = "ThresholdsList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                ThresholdsGrid_RowDeletedFlag.Text = "false";
            }
            if (DiscountsSetUpGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = DiscountsSetUpGridSelectedId.Text;
                HdnListNameForDeletion.Text = "DiscountsSetUpList";
                //Added By Tushar MITS 18231 
                HdnISRateSetUp.Text = "0";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                DiscountsSetUpGrid_RowDeletedFlag.Text = "false";
            }
            if (ExpRatesGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = ExpRatesGridSelectedId.Text;
                HdnListNameForDeletion.Text = "ExpRatesList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                ExpRatesGrid_RowDeletedFlag.Text = "false";
            }
            //Addded By Tushar Agarwal for VACo
            if (CovRatesGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = CovRatesGridSelectedId.Text;
                HdnListNameForDeletion.Text = "CovRatesList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                CovRatesGrid_RowDeletedFlag.Text = "false";
            }
            if (UARRatesGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = UARRatesGridSelectedId.Text;
                HdnListNameForDeletion.Text = "CovRatesList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                UARRatesGrid_RowDeletedFlag.Text = "false";
            }
            if (RateSetUpGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = RateSetUpGridSelectedId.Text;
                HdnListNameForDeletion.Text = "RateSetUpList";
                //Added bY Tushar MITS 18231
                HdnISRateSetUp.Text = "1";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                RateSetUpGrid_RowDeletedFlag.Text = "false";
            }
            //End
            if (DiscountsGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = DiscountsGridSelectedId.Text;
                HdnListNameForDeletion.Text = "DiscountsList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                DiscountsGrid_RowDeletedFlag.Text = "false";
            }
			//npadhy RMSC retrofit Start 
            if (PolicyTaxGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = PolicyTaxGridSelectedId.Text;
                HdnListNameForDeletion.Text = "PolicyTaxList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("PolicyOptionsAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                PolicyTaxGrid_RowDeletedFlag.Text = "false";
            }
            //npadhy RMSC retrofit End  
            if (HdnActionSave.Text != "Save")
            {
                NonFDMCWSPageLoad("PolicyOptionsAdaptor.Get");
            }
            
        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            XmlTemplate = GetMessageTemplate();           
            CallCWS("PolicyOptionsAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
            NonFDMCWSPageLoad("PolicyOptionsAdaptor.Get");
            HdnActionSave.Text = "";
        }
         private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("PolicyOptionsAdaptor.Save");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><PolicyOptionsData>");
            sXml = sXml.Append("<PolOptRowId>" + HdnPolOptRowId.Text + "</PolOptRowId>");
            sXml = sXml.Append("<RoundAmounts>" + RoundAmounts.Checked + "</RoundAmounts>");
            sXml = sXml.Append("<UseYear>" + UseYear.Checked + "</UseYear>");
            sXml = sXml.Append("<Placement codeid='"+ Placement.CodeIdValue +"'>" + Placement.CodeTextValue + "</Placement>");
            sXml = sXml.Append("<AuditPolicy>" + AuditPolicy.Checked + "</AuditPolicy>");//rsushilaggar MITS 21495
            sXml = sXml.Append("<control name='AddedByUser'/><control name='DttmRcdAdded'/><control name='DttmRcdLastUpd'/><control name='UpdatedByUser'/>");
            sXml = sXml.Append("</PolicyOptionsData></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
         private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
             sXml = sXml.Append("PolicyOptionsAdaptor.Delete");
             sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><DeletionListName>");
             sXml = sXml.Append(HdnListNameForDeletion.Text);
             sXml = sXml.Append("</DeletionListName><DeletionId>");
             sXml = sXml.Append(HdnSelectedIdForDeletion.Text);
             //VACo Changes STARTS
             sXml = sXml.Append("</DeletionId><ISRateSetUp>");
             sXml = sXml.Append(HdnISRateSetUp.Text);   
             sXml = sXml.Append("</ISRateSetUp></Document></Document></Message>");
             //VACo Changes Ends
             XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
