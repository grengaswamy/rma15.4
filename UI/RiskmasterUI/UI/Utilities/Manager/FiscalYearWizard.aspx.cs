﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using System.Text;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class FiscalYearWizard : NonFDMBasePageCWS
    {
        // akaushik5 Added for MITS 37480 Starts
        private string PreviousFiscalYear { get; set; }
        // akaushik5 Added for MITS 37480 Ends
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Wizard1.ActiveStep.ID == "step1")
            {
                if (!IsPostBack)
                {
                    try
                    {
                        FillStep1();
                    }
                    catch (Exception ex)
                    {
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }

                }
            }

            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (Wizard1.ActiveStep.ID == "step2")
                {
                    FillStep2();
                }

                if (Wizard1.ActiveStep.ID == "step3")
                {
                    FillStep3();
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        

        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Wizard1.ActiveStep.ID == "step1")
            {
                hdnFiscalWeekDay.Value = ddlFiscalYear.SelectedValue;
                hdnLOB.Value = txtLOB.Value;
                hdnOrg.Value = txtOrg.Value;
            }
        }

        #region Step 1

        private void FillStep1()
        {

            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument XmlDoc = new XmlDocument();

            hdnFromLineOfBusiness.Text = AppHelper.GetQueryStringValue("frmLob");
            hdnFromOrganization.Text = AppHelper.GetQueryStringValue("frmOrg");
            hdnMode.Value = AppHelper.GetQueryStringValue("type");
            txtFiscalYear.Value = AppHelper.GetQueryStringValue("fYear");
            

            LoadStep1();
        }

        private void LoadStep1()
        {
            XmlDocument xmlInput = InputXmlDoc("FiscalYearWizardAdaptor.Get");
            string sXmlOut = "";
            XmlDocument xmlOut = new XmlDocument();
            XmlNode xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/LineOfBusiness");
            xmlNode.InnerXml = hdnFromLineOfBusiness.Text;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/Organization");
            xmlNode.InnerXml = hdnFromOrganization.Text;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/Call");
            xmlNode.InnerXml = hdnMode.Value;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/FiscalYear");
            xmlNode.InnerXml = txtFiscalYear.Value;
            hdnFiscalYear.Value = txtFiscalYear.Value;
            sXmlOut = AppHelper.CallCWSService(xmlInput.InnerXml);
            ErrorControl1.errorDom = sXmlOut;
            xmlOut.LoadXml(sXmlOut);

            //if mode is edit
            if (hdnMode.Value == "0")
            {
                ViewState["Step1Xml"] = sXmlOut;
            }

            txtLOB.Value = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/LineOfBusiness").InnerXml;
            
            txtOrg.Value = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/Organization").InnerXml;
            
            txtPeriods.Text = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/NoOfPeriods").InnerXml;
            hdnNoperiod.Value = txtPeriods.Text;

            FillDropdown(xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/option"), ddlFiscalYear);

            
        
        }

        private void FillDropdown(XmlNode objOptionLst, DropDownList ddlList)
        {
            ListItem objListItem = null;
            ddlList.Items.Clear();
            ListItem objSelectedListItem = null;
            if (objOptionLst.InnerXml != "0")
            {
                foreach (XmlElement objElement in objOptionLst)
                {
                    objListItem = new ListItem(objElement.InnerText, objElement.GetAttribute("value"));
                    ddlList.Items.Add(objListItem);

                    if (objElement.GetAttribute("selected") != null && objElement.GetAttribute("selected")== "1")
                    {
                        objSelectedListItem = objListItem;
                    }
                    
                }
            }
            if (objSelectedListItem != null)
            {

                ddlList.SelectedIndex = ddlList.Items.IndexOf(objSelectedListItem);
            }
        }

      

        private XmlDocument InputXmlDoc(string strFunction)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            
            XElement oTemplate = XElement.Parse(@" 
                                                    <Message>
                                                    <Authorization></Authorization>
                                                    <Call>
                                                    <Function>" + strFunction + @"</Function>
                                                    </Call>
                                                    <Document>
                                                    <FiscalYearWizard>
                                                    <LineOfBusiness></LineOfBusiness>
                                                    <Organization></Organization>
                                                    <NoOfPeriods />
                                                    <option />
                                                    <FiscalYear></FiscalYear>
                                                    <FiscalWeekDay />
                                                    <FiscalYearStart />
                                                    <FiscalYearEnd />
                                                    <FiscalQ1Start /> 
                                                    <FiscalQ1End />
                                                    <FiscalQ2Start />
                                                    <FiscalQ2End />
                                                    <FiscalQ3Start />
                                                    <FiscalQ3End />
                                                    <FiscalQ4Start /> 
                                                    <FiscalQ4End />
                                                    <OverlapFlag /> 
                                                    <FiscalPeriods />
                                                    <Call></Call>
                                                    <LineOfBusinessCode></LineOfBusinessCode> 
                                                    <OrganizationCode></OrganizationCode>
                                                    </FiscalYearWizard>
                                                    </Document>
                                                    </Message>
                                                 ");


            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;

        }




        protected void txtPeriods_TextChanged(object sender, EventArgs e)
        {
            //XmlDocument xmlInput = InputXmlDoc("FiscalYearWizardAdaptor.Get");
            //string sXmlOut = "";
            //XmlDocument xmlOut = new XmlDocument();
            
            //XmlNode xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/NoOfPeriods");
            //xmlNode.InnerXml = txtPeriods.Text;

            //xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/FiscalYearStart");
            //xmlNode.InnerXml = txtdatestart.Value;

            //xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/FiscalYearEnd");
            //xmlNode.InnerXml = txtdateend.Value;

            //sXmlOut = AppHelper.CallCWSService(xmlInput.InnerXml);
            //ErrorControl1.errorDom = sXmlOut;
            //xmlOut.LoadXml(sXmlOut);
            //if mode is edit
            //if (hdnMode.Value == "0")
            //{
            //    //BuildPeriods();
            //}         
           
        }


       #endregion


        #region Step 2

        private void FillStep2()
        {
            if (hdnBuildPeriod.Value == "Build")
            {
                BuildQuaters();
            }
            else
            {
                XmlDocument xmlOut = new XmlDocument();
                string sXmlOut = "";
                //if mode is edit
                if (hdnMode.Value == "0")
                {
                    sXmlOut = (string) ViewState["Step1Xml"];

                    xmlOut.LoadXml(sXmlOut);

                    hdnPeriods.Text = "<FiscalPeriods>" + xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalPeriods").InnerXml + "</FiscalPeriods>";
                    BindQuarters(xmlOut);
                }
            }
        }

        private void BuildQuaters()
        {
            XmlDocument xmlInput = InputXmlDoc("FiscalYearWizardAdaptor.BuildQuaters");
            string sXmlOut = "";
            XmlDocument xmlOut = new XmlDocument();
            XmlNode xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/FiscalYearStart");
            xmlNode.InnerXml = txtdatestart.Value;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/LineOfBusiness");
            xmlNode.InnerXml = hdnLOB.Value;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/Organization");
            xmlNode.InnerXml = hdnOrg.Value;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/LineOfBusinessCode");
            xmlNode.InnerXml = hdnFromLineOfBusiness.Text;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/OrganizationCode");
            xmlNode.InnerXml = hdnFromOrganization.Text;

            //Aman MITS 27341--Start
            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/Call");
            xmlNode.InnerXml = hdnMode.Value;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/FiscalYear");
            xmlNode.InnerXml = txtFiscalYear.Value;
            //Aman MITS 27341--End
            sXmlOut = AppHelper.CallCWSService(xmlInput.InnerXml);
            ErrorControl1.errorDom = sXmlOut;
            xmlOut.LoadXml(sXmlOut);

            hdnQuarters.Text = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard").InnerXml ;
            BindQuarters(xmlOut);

            

 


        }


        private void BindQuarters(XmlDocument xmlOut)
        {
            XmlNode xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalYearEnd");
            txtdateend.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalYearStart");
            txtdatestart.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ1Start");
            txtQ1start.Value  = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ1End");
            txtQ1end.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ2Start");
            txtQ2start.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ2End");
            txtQ2end.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ3Start");
            txtQ3start.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ3End");
            txtQ3end.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ4Start");
            txtQ4start.Value = xmlNode.InnerXml;

            xmlNode = xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/FiscalQ4End");
            txtQ4end.Value = xmlNode.InnerXml;

            if (xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/OverlapFlag") != null &&  xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard/OverlapFlag").InnerXml.ToUpper() == "TRUE")
            {

                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>CheckForUpdation();</script>");
               
            } 
            else
            {
                //if mode is edit
                if (hdnMode.Value == "0" || hdnNewMode.Value == "Build")
                {
                    BuildPeriods();
                }
            }
        }


        private void BuildPeriods()
        {
            XmlDocument xmlInput = InputXmlDoc("FiscalYearWizardAdaptor.BuildPeriods");
            string sXmlOut = "";
            XmlDocument xmlOut = new XmlDocument();
            XmlNode xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/FiscalYearStart");
            xmlNode.InnerXml = txtdatestart.Value;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/NoOfPeriods");
            xmlNode.InnerXml = hdnNoperiod.Value;

            xmlNode = xmlInput.SelectSingleNode("Message/Document/FiscalYearWizard/FiscalYearEnd");
            xmlNode.InnerXml = txtdateend.Value;

            sXmlOut = AppHelper.CallCWSService(xmlInput.InnerXml);
            ErrorControl1.errorDom = sXmlOut;
            xmlOut.LoadXml(sXmlOut);

            hdnPeriods.Text =xmlOut.SelectSingleNode("ResultMessage/Document/FiscalYearWizard").InnerXml;
        }

        #endregion


        #region Step 3

        private void FillStep3()
        {
            DataTable dtPeriods = new DataTable();
            FillPeriodDataTable(dtPeriods, hdnPeriods.Text);
            dlPeriod.DataSource = dtPeriods;
            dlPeriod.DataBind();
        }


        private void Save()
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            string sXmlOut = string.Empty;
            XmlDocument xmlServiceDoc = new XmlDocument();
            // akaushik Added <PreviousFiscalYear> for MITS 37480
            XElement oTemplate = XElement.Parse(@" 
                                                    <Message>
                                                    <Authorization></Authorization>
                                                    <Call>
                                                    <Function>FiscalYearWizardAdaptor.Save</Function>
                                                    </Call>
                                                    <Document>
                                                    <FiscalYearWizard>
                                                    <LineOfBusiness value='" + hdnFromLineOfBusiness.Text + @"' >" + hdnLOB.Value + @"</LineOfBusiness>
                                                    <Organization value='" + hdnFromOrganization.Text + @"' >" + hdnOrg.Value + @"</Organization>
                                                    <PreviousFiscalYear>" + this.PreviousFiscalYear + @"</PreviousFiscalYear>
                                                    <FiscalYear>" + hdnFiscalYear.Value + @"</FiscalYear> 
                                                    <FiscalWeekDay>" + hdnFiscalWeekDay.Value + @"</FiscalWeekDay>
                                                    <option>
                                                    <option value='0'>Sunday</option> 
                                                    <option value='1'>Monday</option> 
                                                    <option value='2'>Tuesday</option> 
                                                    <option value='3'>Wednesday</option> 
                                                    <option value='4'>Thursday</option> 
                                                    <option value='5'>Friday</option> 
                                                    <option value='6'>Saturday</option> 
                                                    </option>
                                                    <FiscalYearStart>" + txtdatestart.Value + @"</FiscalYearStart>
                                                    <FiscalYearEnd>" + txtdateend.Value + @"</FiscalYearEnd>
                                                    <FiscalQ1Start>" + txtQ1start.Value + @"</FiscalQ1Start> 
                                                    <FiscalQ1End>" + txtQ1end.Value + @"</FiscalQ1End>
                                                    <FiscalQ2Start>" + txtQ2start.Value + @"</FiscalQ2Start>
                                                    <FiscalQ2End>" + txtQ2end.Value + @"</FiscalQ2End>
                                                    <FiscalQ3Start>" + txtQ3start.Value + @"</FiscalQ3Start>
                                                    <FiscalQ3End>" + txtQ3end.Value + @"</FiscalQ3End>
                                                    <FiscalQ4Start>" + txtQ4start.Value + @"</FiscalQ4Start> 
                                                    <FiscalQ4End>" + txtQ4end.Value + @"</FiscalQ4End>
                                                    <NoOfPeriods>" + hdnNoperiod.Value + @"</NoOfPeriods>
                                                    " + hdnPeriods.Text + @"</FiscalYearWizard> 
                                                    </Document>
                                                    </Message>
                                                    ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            sXmlOut = AppHelper.CallCWSService(xmlNodeDoc.InnerXml);
            ErrorControl1.errorDom = sXmlOut;
            xmlServiceDoc.LoadXml(sXmlOut);
            if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
            {
                ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }

        }

        private void FillPeriodDataTable(DataTable dtPeriod , string sPeriods  )
        {
            XmlDocument xmldoc = new XmlDocument();
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex;
         
           
            xmldoc.LoadXml("<X>" + sPeriods + "</X>");
            XmlNodeList xmlNodes = xmldoc.SelectNodes("X/FiscalPeriods");

            dcGridColumn = new DataColumn();
            dcGridColumn.ColumnName = "PeriodNumber";
            dtPeriod.Columns.Add(dcGridColumn);

            dcGridColumn = new DataColumn();
            dcGridColumn.ColumnName = "StartDate";
            dtPeriod.Columns.Add(dcGridColumn);

            dcGridColumn = new DataColumn();
            dcGridColumn.ColumnName = "EndDate";
            dtPeriod.Columns.Add(dcGridColumn);

            dcGridColumn = new DataColumn();
            dcGridColumn.ColumnName = "Summary";
            dtPeriod.Columns.Add(dcGridColumn);
           
            foreach (XmlNode objNodes in xmlNodes)
            {
                
                foreach (XmlElement objElem in objNodes)
                {
                    iColumnIndex = 0;
                    dr = dtPeriod.NewRow();

                    dr[iColumnIndex++] = "Period #" + objElem.SelectSingleNode("PeriodNumber").InnerXml;
                    dr[iColumnIndex++] = objElem.SelectSingleNode("StartDate").InnerXml;
                    dr[iColumnIndex++] = objElem.SelectSingleNode("EndDate").InnerXml;
                    dr[iColumnIndex++] = objElem.SelectSingleNode("Summary").InnerXml;

                    dtPeriod.Rows.Add(dr);
                }
                
            }
        }

        #endregion

        protected void Wizard1_FinishButtonClick1(object sender, WizardNavigationEventArgs e)
        {
            
        }

        protected void StepFinishButton_Click(object sender, EventArgs e)
        {
            if (Wizard1.ActiveStep.ID == "step3")
            {
                // akaushik5 Added for MITS 37480 Starts
                if (AppHelper.GetQueryStringValue("fYear") != null)
                {
                    this.PreviousFiscalYear = hdnMode.Value.Equals("0") ? AppHelper.GetQueryStringValue("fYear") : string.Empty;
                }
                // akaushik5 Added for MITS 37480 Ends
                Save();
            }
        }

    }
}
