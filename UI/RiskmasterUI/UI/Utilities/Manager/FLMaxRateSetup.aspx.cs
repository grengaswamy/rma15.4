﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.Manager
{
    public partial class FLMaxRateSetup : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                if (FLMaxRateSetupGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = FLMaxRateSetupSelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("FLMaxRateAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    FLMaxRateSetupGrid_RowDeletedFlag.Text = "false";
                }
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("FLMaxRateSetupAdaptor.Get", out sCWSresponse, XmlTemplate);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<form name='FLMaxRate' title='FL Max Rate Setup'>");
            sXml = sXml.Append("<group name='General' title='FL Max Rate'>");
            sXml = sXml.Append("</group></form>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetMessageTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><FLMaxRate>");
            sXml = sXml.Append("<control name='MaxRateId'>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</FLMaxRate></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }
}
