﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;

namespace Riskmaster.UI.Utilities.Technician
{
    public partial class RebuildOrgHierarchy : NonFDMBasePageCWS
    {
        public string sCWSresponse = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oMessageElement = GetMessageTemplate();

            try 
            {  
                CallCWS("RMUTILAdaptor.RebuildOrgHierarchy", oMessageElement, out sCWSresponse, false, false);

                ErrorControl1.errorDom = sCWSresponse;

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
             <Call>
              <Function></Function> 
              </Call>
             <Document>
              <Utility /> 
             </Document>
            </Message>


            ");

            return oTemplate;
        }
    }
}
