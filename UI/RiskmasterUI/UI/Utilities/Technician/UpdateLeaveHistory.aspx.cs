﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Utilities.Technician
{
    public partial class UpdateLeaveHistory : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.SysWindowId.Text=AppHelper.GetQueryStringValue("planid");
            NonFDMCWSPageLoad("LeavePlanAdaptor.UpdateLeaveHistory");
        }
    }
}
