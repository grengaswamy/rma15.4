﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RestrictedClaimsUsersOverride.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.AdminUsers.RestrictedClaimsUsersOverride" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="pleasewait"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Restricted Claims User Override</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
   

    <script type="text/javascript" language="javascript">
        function RestrcitedClaimUser_Save() {
            //debugger;
            pleaseWait.Show();
            m_bSaveButtonClicked = true;    //jira-705
            return true;
        }
    </script>

    <script  type="text/javascript" language  ="JavaScript" src="../../../Scripts/cul.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../Scripts/Utilities.js">        { var i; }</script>

    <script type="text/javascript" src="../../../Scripts/WaitDialog.js"></script>

</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" runat="server" name="frmData" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox runat="server" Style="display: none" ID="hTabName"></asp:TextBox>
    <div id="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">                    
                        <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_save_active.png" class="bold"
                        ToolTip="Save" runat="server" OnClientClick="return RestrcitedClaimUser_Save();" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="msgheader" id="formtitle">
     Restricted Claims User Override</div>   
    <br />
    <table border="0">
        <tr>
            <td> 
                <div> 
                <%--class="singletopborder" style="position: relative; left: 0; top: 0; width: 800px; height: 315px; overflow: auto"--%>                      
                    <table border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>
                                    Override User:&nbsp;&nbsp;
                            </td>
                            <td>                                
                                <select multiple="true" id="lstUsers" size="3" runat="server" style="width: 190px;
                                    height: 71px">
                                </select>                                
                                <input type="button" class="CodeLookupControl" value="" id="cmdAddCustomizedListUser"
                                    onclick="AddCustomizedListUser('RestrictedClaimsUsersOverride','lstUsers','UserId','UserName')" runat="server"/> 
                                <input type="button" class="BtnRemove" value="" id="cmdDelCustomizedListUser" style="width: 21px"
                                    onclick="DelCustomizedListUser('RestrictedClaimsUsersOverride','lstUsers','UserId','UserName')" />
                                <asp:TextBox Style="display: none" runat="server"  ID="UserId"
                                    Text="" rmxref="Instance/Document/form/group/RestrictedClaimsUsersOverride/SelectedUsersID" rmxtype="hidden" />
                                <asp:TextBox Style="display: none" runat="server"  ID="UserName"
                                    Text="" rmxref="Instance/Document/form/group/RestrictedClaimsUsersOverride/SelectedUsers" rmxtype="hidden" />
                            </td>
                        </tr>
                    </table>                                          
                </div> 
               <asp:TextBox Style="display: none" runat="server" rmxretainvalue="true" ID="userList" Text="" rmxref="Instance/Document/form/group/displaycolumn/control/UserList" rmxtype="hidden" />                
                 <input type="text" name="" value="" id="SysViewType" style="display: none" />
                <asp:TextBox runat="server" ID="SysCmd" Style="display: none" Text=""></asp:TextBox>
                <input type="text" name="" value="" id="SysCmdConfirmSave" style="display: none" />
                <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
                <input type="text" name="" value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate" />
                <input type="text" name="" value="" id="SysClassName" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysSerializationConfig" style="display: none" />
                <input type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId" />
                <input type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId" />
                <input type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="RestrictedClaimsUsersOverride" />
                <input type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value="" />
                <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="RestrictedClaimsUsersOverride" />
                <input type="text" name="" value="" id="SysRequired" style="display: none" />
                <input type="text" name="" value="" id="SysFocusFields" style="display: none" />
            </td>
        </tr>
    </table>
    <uc:pleasewait CustomMessage="Loading" runat="server" ID="PleaseWaitDialog1" />
    </form>
</body>
</html>
