﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="BESGroupDef.aspx.cs" Inherits="Riskmaster.UI.Utilities.BESGroupDef.BESGroupDef" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>New Security Group (Data View Group Definition)</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../../Scripts/form.js" type="text/javascript">
    </script>

    <script language="JavaScript" src="../../../Scripts/BesSetup.js" type="text/javascript">
    </script>

    <script src="../../../Scripts/cul.js" type="text/javascript"></script>

    <script lang="javascript">
        var m_FormSubmitted = false;

        function SetAction() {
            var objGroupName = document.getElementById('Wizardlist_txtGroupName');
            if (objGroupName != null)
                document.forms[0].hiddenGroupName.value = objGroupName.value;
            var index;
            index = GetWizardIndex();
            if (index == '0') {
                if (!Validate(index))
                    return false;

                //            ScanListForIterms(document.forms[0].Wizardlist_Org,document.forms[0].SelEntitiesListItems);
                if (document.forms[0].DBType.value == "4") {
                    if ((document.forms[0].BrokerSupport.value == "1" || document.forms[0].InsurerSupport.value == "1") && document.forms[0].Wizardlist_optAll.checked == false) {
                        document.forms[0].hidNextPage.value = "GoToPage3";
                        document.forms[0].IsOptAll.value = "0";
                    }
                    //Nadim for Rem
                    else {
                        if (document.forms[0].Wizardlist_optAll.checked == true) {
                            document.forms[0].hidNextPage.value = "GoToPage2";
                            document.forms[0].IsOptAll.value = "1";
                        }
                        else {
                            document.forms[0].hidNextPage.value = "GoToPage4";
                            document.forms[0].IsOptAll.value = "0";
                        }
                    }
                    //			    else
                    //			    {	
                    //				    document.forms[0].hidNextPage.value = "GoToPage2";
                    //				    if(document.forms[0].Wizardlist_optAll.checked == true)
                    //					    document.forms[0].IsOptAll.value = "1";
                    //				    else
                    //					    document.forms[0].IsOptAll.value = "0";
                    //			    }
                    //Nadim for Rem
                }
                else {
                    //Boeing :02-18-2009 - UseBrokerBes Tag added for setting USE_BROKER_BES
                    if (document.forms[0].hdnUseBrokerBes.value == "1" && document.forms[0].Wizardlist_optAll.checked == false) {
                        document.forms[0].hidNextPage.value = "GoToPage5";
                        document.forms[0].IsOptAll.value = "0";
                    }
                    else {
                        if (document.forms[0].Wizardlist_optAll.checked == true) {
                            document.forms[0].hidNextPage.value = "GoToPage2";
                            document.forms[0].IsOptAll.value = "1";
                        }
                        else {
                            document.forms[0].hidNextPage.value = "GoToPage4";
                            document.forms[0].IsOptAll.value = "0";
                        }
                    }
                }
            }
            else if (index == '1') {
                if (!Validate(index))
                    return false;
                //          ScanListForIterms(document.forms[0].Wizardlist_lstAdjTextTypes_multicode,document.forms[0].AdjusterListItems);          
                //document.forms[0].hidNextPage.value = "GoToPage2";
                document.forms[0].hidNextPage.value = "GoToPage6";
            }
            else if (index == '2') {
                if (!Validate(index))
                    return false;
                //            ScanListForIterms(document.forms[0].Wizardlist_lstBrokers_multicode,document.forms[0].BrokersListItems);
                //            ScanListForIterms(document.forms[0].Wizardlist_lstInsurers_multicode,document.forms[0].InsurersListItems);
                if (document.forms[0].IsOptAll.value == "0")
                    document.forms[0].hidNextPage.value = "GoToPage4";
                else
                    document.forms[0].hidNextPage.value = "GoToPage2";
            }
            else if (index == '3') {
                //         ScanListForIterms(document.forms[0].Wizardlist_lstUsers,document.forms[0].UserListItems);
                if (document.forms[0].DBType.value == "6" && document.forms[0].IsOptAll.value == "0")
                    document.forms[0].hidNextPage.value = "GoToPage6";
            }
            else if (index == '4') //Boeing
            {
            }
            else if (index == '5') {
            if (!Validate(index))
                return false;
            document.forms[0].hidNextPage.value = "GoToPage2";
            }
            
            MaintainListBox();

            return true;
        }
        function MaintainListBox() {

            var index;
            var elementRef;
            var hiddenElementRef;
            var SecOrgLevels; //abansal23 10/05/2009 MITS 17648 : Insurers not getting filtered on the basis of org hierarchy selected
            index = GetWizardIndex();
            var flag = false;
            if (index == '0') {
                ScanListForIterms(document.forms[0].Wizardlist_Org, document.forms[0].SelEntitiesListItems);
                elementRef = document.getElementById('Wizardlist_Org'); //<%#Org.ClientID%>
                hiddenElementRef = document.getElementById('hdnOrg');
                hiddenElementRef.value = "";
                SecOrgLevels = document.getElementById('SelOrgLevels');
                SecOrgLevels.value = "";
                flag = true;

            }
            if (index == '1') {
                ScanListForIterms(document.forms[0].Wizardlist_lstAdjTextTypes_multicode, document.forms[0].AdjusterListItems);
                elementRef = document.getElementById('Wizardlist_lstAdjTextTypes_multicode');
                hiddenElementRef = document.getElementById('hdnlstAdjTextTypes');
                hiddenElementRef.value = "";
                flag = true;
            }
            if (index == '2') {
                //Nadim for Rem brokers and insurers
                ScanListForIterms(document.forms[0].Wizardlist_lstBrokerFirm2, document.forms[0].BrokersListItems2);
                ScanListForIterms(document.forms[0].Wizardlist_lstInsurerFirm3, document.forms[0].InsurersListItems3);
                elementRef = document.getElementById('Wizardlist_lstBrokerFirm2');
                hiddenElementRef = document.getElementById('hdnlstBrokerFirms2');
                hiddenElementRef.value = "";
                for (i = 0; i < elementRef.options.length; i++) {
                    if (hiddenElementRef.value != "")
                        hiddenElementRef.value += "|" + elementRef.options[i].text;
                    else
                        hiddenElementRef.value = elementRef.options[i].text;

                    hiddenElementRef.value += "^" + elementRef.options[i].value; //Parijat: 15583 & 15581

                }
                //Nadim for Rem brokers and insurers
                elementRef = document.getElementById('Wizardlist_lstInsurerFirm3');
                hiddenElementRef = document.getElementById('hdnlstInsurerFirms3');
                //Nadim for Rem brokers and insurers
                hiddenElementRef.value = "";
                flag = true;
            }
            if (index == '3') {
                ScanListForIterms(document.forms[0].Wizardlist_lstUsers, document.forms[0].UserListItems);

                elementRef = document.getElementById('Wizardlist_lstUsers');
                hiddenElementRef = document.getElementById('hdnlstUsers');
                hiddenElementRef.value = "";
                flag = true;
            }
            if (index == '4') //Boeing: Broker firm
            {//Parijat:Broker Changes have been incorporated for boeing --(BrokerFirm Listbox )
                ScanListForIterms(document.forms[0].Wizardlist_lstBrokerFirm1, document.forms[0].BrokerFirmList); //broker changes
                elementRef = document.getElementById('Wizardlist_lstBrokerFirm1');
                hiddenElementRef = document.getElementById('hdnlstBrokerFirms');
                hiddenElementRef.value = "";
                flag = true;
            }
            if (index == '5') {
                ScanListForIterms(document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode, document.forms[0].NotesListItems);
                elementRef = document.getElementById('Wizardlist_lstEnhanceNoteTypes_multicode');
                hiddenElementRef = document.getElementById('hdnlstEnhanceNoteTypes');
                hiddenElementRef.value = "";
                flag = true;
            }
            if (flag == true) {
                for (i = 0; i < elementRef.options.length; i++) {
                    //Amit: MITS 17648 10/05/2009 Starts
                    if (hiddenElementRef.value != "") {
                        hiddenElementRef.value += "|" + elementRef.options[i].text;
                        if (SecOrgLevels != null)
                            SecOrgLevels.value += "," + elementRef.options[i].value;
                    }
                    else {
                        hiddenElementRef.value = elementRef.options[i].text;
                        if (SecOrgLevels != null)
                            SecOrgLevels.value = elementRef.options[i].value;
                    }
                    //Amit: MITS 17648 10/05/2009 Ends
                    hiddenElementRef.value += "^" + elementRef.options[i].value; //Parijat: 15583 & 15581

                }
            }
        }
        function SetBackAction() {
            // debugger;
            var index;
            index = GetWizardIndex();
            if (index == '1') {
                if (!Validate(index))
                    return false;
                //        ScanListForIterms(document.forms[0].Wizardlist_lstAdjTextTypes_multicode,document.forms[0].AdjusterListItems);
            }
            else if (index == '2') {
                if (!Validate(index))
                    return false;
                //            ScanListForIterms(document.forms[0].Wizardlist_lstBrokers_multicode,document.forms[0].BrokersListItems);
                //            ScanListForIterms(document.forms[0].Wizardlist_lstInsurers_multicode,document.forms[0].InsurersListItems);
            }
            else if (index == '3') {
                //         ScanListForIterms(document.forms[0].Wizardlist_lstUsers,document.forms[0].UserListItems);
            }
            if (index == '5') {
                if (!Validate(index))
                    return false;
                //        ScanListForIterms(document.forms[0].Wizardlist_lstAdjTextTypes_multicode,document.forms[0].AdjusterListItems);
            }
            MaintainListBox();
        }

        function selectOrg() {
            //var m_codeWindow=window.open('home?pg=riskmaster/OrgHierarchy/searchorg','codeWnd','width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes'); 
            var m_codeWindow = window.open("home?pg=riskmaster/OrgHierarchy/Org&tablename=nothing&rowid=0&lob=0&searchOrgId=0", 'Table', 'width=700,height=600' + ',top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
        }

        function SetFinishAction() {
            var index;
            index = GetWizardIndex();
            if (index == '0') {
                if (!Validate(index))
                    return false;

                //              document.forms[0].hidClosePage.value = "1";
                if (m_FormSubmitted) {
                    alert("You already submitted this form. Please wait for server to respond.");
                    return false;
                }

                m_FormSubmitted = true;
            }
            else if (index == '1') {
                if (!Validate(index))
                    return false;

                //            document.forms[0].hidClosePage.value = "1";
                if (m_FormSubmitted) {
                    alert("You already submitted this form. Please wait for server to respond.");
                    return false;
                }

                m_FormSubmitted = true;
            }
            else if (index == '2') {
                if (!Validate())
                    return false;

                //            document.forms[0].hidClosePage.value = "1";
                if (m_FormSubmitted) {
                    alert("You already submitted this form. Please wait for server to respond.");
                    return false;
                }
            }
            else if (index == '3') {

                var objchkOveridePass = eval(document.forms[0].Wizardlist_chkOveridePass);
                if (objchkOveridePass != null) {
                    if (objchkOveridePass.checked) {
                        if (document.forms[0].Wizardlist_txtPass.value == "") {
                            alert("Please enter the overiding Password.");
                            return false;
                        }
                        document.forms[0].hidOveridePassFlag.value = "1";
                    }
                    else {
                        document.forms[0].hidOveridePassFlag.value = "0";
                    }
                }
                if (!ValidateEmptyList(document.forms[0].Wizardlist_lstUsers, "assign User to the group."))
                    return false;
                //            ScanListForIterms(document.forms[0].lstUsers,document.forms[0].UserListItems);
                //            document.forms[0].hidClosePage.value = "1";
                if (m_FormSubmitted) {
                    alert("You already submitted this form. Please wait for server to respond.");
                    return false;
                }

                m_FormSubmitted = true;
            }
            else if (index == '5') {
                if (!Validate(index))
                    return false;

                //            document.forms[0].hidClosePage.value = "1";
                if (m_FormSubmitted) {
                    alert("You already submitted this form. Please wait for server to respond.");
                    return false;
                }

                m_FormSubmitted = true;
            }
            MaintainListBox();
            var objGroupName = null;
            objGroupName = document.getElementById('Wizardlist_txtGroupName');
            if (objGroupName == null)
                objGroupName = document.getElementById('hiddenGroupName');
            for (var i = 0; i <= m_count - 1; i++) {
                if (m_groupNames[i].toUpperCase() == objGroupName.value.toUpperCase()) {
                    if (m_groupId[i] != document.forms[0].rowId.value) {
                        alert('The specified Group name already exists. \nPlease give a different Name');
                        return false;
                    }

                }
            }
            return true;
        }

        function Validate(index) {
            if (index == '0') {
                if (document.forms[0].Wizardlist_txtGroupName.value == "") {
                    alert("Please enter the Group Name");
                    document.forms[0].Wizardlist_txtGroupName.focus();
                    return false;
                }

                if (document.forms[0].Wizardlist_optSome.checked == true) {
                    if (!ValidateEmptyList(document.forms[0].Wizardlist_Org, "select atleast one Organization Entity."))
                        return false;
                }
            }
            else if (index == '1') {
                if (document.forms[0].Wizardlist_optExceptAdjTextTypes.checked == true || document.forms[0].Wizardlist_optSomeAdjTextTypes.checked == true) {
                    if (!ValidateEmptyList(document.forms[0].Wizardlist_lstAdjTextTypes_multicode, "select atleast one Adjuster Dated Text type."))
                        return false;
                }

            }
            else if (index == '2') {
                if (document.forms[0].Wizardlist_optSomeBrokers.checked == true) {
                    //Nadim for Rem brokers and insurers
                    if (!ValidateEmptyList(document.forms[0].Wizardlist_lstBrokerFirm2, "select atleast one Broker."))
                        return false;
                }

                if (document.forms[0].Wizardlist_optSomeInsurers.checked == true) {
                    //Nadim for Rem brokers and insurers
                    if (!ValidateEmptyList(document.forms[0].Wizardlist_lstInsurerFirm3, "select atleast one Insurer."))
                        return false;
                }

            }
            else if (index == '5') {
                if (document.forms[0].Wizardlist_optExceptEnhanceNoteTypes.checked == true || document.forms[0].Wizardlist_optSomeEnhanceNoteTypes.checked == true) {
                    if (!ValidateEmptyList(document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode, "select atleast one Note Type."))
                        return false;
                }

            }
            return true;
        }

        function checkwindowclose() {
            ////          if(document.forms[0].hidClosePage.value == "1")
            //          {
            //            window.opener.document.forms[0].submit();
            //            this.close();        
            //          }
            GetCollectionOfGroupName()
            index = GetWizardIndex();
            if (index == '0') {

                if (document.getElementById('Wizardlist_optAll').checked == true) {

                    document.getElementById('Wizardlist_Org').disabled = true;
                    document.getElementById('Wizardlist_Orgbtn').disabled = true;
                    document.getElementById('Wizardlist_Orgbtndel').disabled = true;
                }
                else {
                    document.getElementById('Wizardlist_Org').disabled = false;
                    document.getElementById('Wizardlist_Orgbtn').disabled = false;
                    document.getElementById('Wizardlist_Orgbtndel').disabled = false;
                }
                //            document.getElementById('Wizardlist_optSome').checked = 'true'
            }
            else if (index == '1') {
                if (document.getElementById('Wizardlist_optAllAdjTextTypes').checked == true) {
                    document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtn.disabled = true;
                    document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtndel.disabled = true;
                    document.forms[0].Wizardlist_lstAdjTextTypes_multicode.disabled = true;
                }
                else {
                    document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtn.disabled = false;
                    document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtndel.disabled = false;
                    document.forms[0].Wizardlist_lstAdjTextTypes_multicode.disabled = false;
                }
            }
            else if (index == '5') {
                if (document.getElementById('Wizardlist_optAllEnhanceNoteTypes').checked == true) {
                    document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtn.disabled = true;
                    document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtndel.disabled = true;
                    document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode.disabled = true;
                }
                else {
                    document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtn.disabled = false;
                    document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtndel.disabled = false;
                    document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode.disabled = false;
                }
            }
        }

        function SelectOnlyOneRadio(objRadioButton, wizardName) {

            var i, obj;
            //example of radio button id inside the grid (grdAddress): grdAddress__ctl2_radioSelect
            for (i = 0; i < document.all.length; i++) {
                //obj = document.all(i);
                obj = document.all[i]; //RMA-16671
                if (obj.type == "radio") {
                    if (objRadioButton.id.substr(0, wizardName.length) == wizardName)
                        if (objRadioButton.id == obj.id) {
                        //                        obj.checked = true;
                        if (objRadioButton.id == "Wizardlist_optAll") {

                            document.getElementById('Wizardlist_Org').disabled = true;
                            document.getElementById('Wizardlist_Orgbtn').disabled = true;
                            document.getElementById('Wizardlist_Orgbtndel').disabled = true;
                        }
                        else if (objRadioButton.id == "Wizardlist_optSome") {
                            document.getElementById('Wizardlist_Org').disabled = false;
                            document.getElementById('Wizardlist_Orgbtn').disabled = false;
                            document.getElementById('Wizardlist_Orgbtndel').disabled = false;
                        }
                    }
                    //                     else
                    //                    obj.checked = false ;
                }
            }
        }
    </script>

</head>
<body onload="checkwindowclose();">
    <form id="frmData" runat="server" autocomplete="off">
    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:HiddenField ID="OriginalType" runat="server" Value="" />
    <input type="hidden" name="$node^9" value="" id="Label2" runat="server" />
    <input type="hidden" name="$node^9" value="" id="hdnActiveIndex" runat="server" />
    <asp:TextBox rmxref="/Instance/Document/EventId" ID="eventid" Style="display: none"
        runat="server" />
    <input type="hidden" name="$node^61" id="hiddenGroupName" runat="server" />
    <input type="hidden" name="$node^61" id="SelEntitiesListItems" runat="server" />
    <input type="hidden" name="$node^61" id="AdjusterListItems" runat="server" />
    <input type="hidden" name="$node^61" id="NotesListItems" runat="server" />
    <input type="hidden" name="$node^61" id="BrokersListItems" runat="server" />
    <input type="hidden" name="$node^61" id="InsurersListItems" runat="server" />
    <input type="hidden" name="$node^61" id="UserListItems" runat="server" />
    <input type="hidden" name="$node^61" id="SelOrgLevels" runat="server" /><!-- abansal23 on 10/05/2009 MITS 17648: Insurers not getting filtered on the basis of org hierarchy selected Starts -->
    <input type="hidden" name="$node^61" id="BrokerFirmList" runat="server" /><%--// Boeing: Broker firms list being maintained--%>
    <!--Nadim for Rem brokers and insurers-->
    <input type="hidden" name="$node^61" id="BrokersListItems2" runat="server" />
    <input type="hidden" name="$node^61" id="InsurersListItems3" runat="server" />
    <input type="hidden" name="$node^61" id="BrokerFirmList2" runat="server" />
    <input type="hidden" name="$node^61" id="InsurerFirmList3" runat="server" />
    <!--Nadim for Rem brokers and insurers-->
    <input type="hidden" name="$node^46" value="1" id="DBType" runat="server" />
    <input type="hidden" name="$node^46" value="1" id="rowId" runat="server" />
    <input type="hidden" name="$node^46" value="1" id="ViewEnhanceBES" runat="server" />
    <input type="hidden" name="$node^40" value="0" id="BrokerSupport" runat="server" />
    <input type="hidden" name="$node^43" value="0" id="InsurerSupport" runat="server" />
    <input type="hidden" name="$node^37" value="" id="IsOptAll" runat="server" />
    <input type="hidden" name="$node^37" value="" id="PassWord" runat="server" />
    <input type="hidden" name="$node^37" value="" id="Defer" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hidNextPage" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hidOveridePassFlag" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnOrg" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnlstAdjTextTypes" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnlstEnhanceNoteTypes" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnlstInsurers" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnlstBrokers" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnlstUsers" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnlstBrokerFirms" runat="server" /><%--// Boeing: Broker firms--%>
    <input type="hidden" name="$node^46" value="0" id="ConfRec" runat="server" /> <!-- pmittal5 - Confidential Record-->
    <!--Nadim for Rem brokers and insurers-->
    <input type="hidden" name="$node^8" value="" id="hdnlstBrokerFirms2" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnlstInsurerFirms3" runat="server" />
    <!--Nadim for Rem brokers and insurers-->
    <input type="hidden" name="$node^8" value="" id="hdnVisited2stPage" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnVisited3stPage" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnVisited4stPage" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnVisited5stPage" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnVisited6stPage" runat="server" />
    <input type="hidden" name="$node^8" value="" id="hdnUseBrokerBes" runat="server" />
    <!-- PJS 02-18-2009: Boeing-->
    <%-- <asp:TextBox rmxref="/Instance/Document/RecordableFlag" ID="recordableflag" Style="display: none"
        runat="server" />
    <asp:TextBox rmxref="/Instance/Document/submitflag" ID="submitflag" Style="display: none"
        runat="server" />--%>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:Wizard ID="Wizardlist" runat="server" DisplaySideBar="false" ActiveStepIndex="0"
            OnNextButtonClick="SkiptoNextStep" OnPreviousButtonClick="SkiptoPreviousStep">
            <%-- OnFinishButtonClick ="Save"--%>
            <StartNavigationTemplate>
                <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server"
                    CausesValidation="True" CommandName="MovePrevious" Text="<Back" />
                <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext"
                    Text="Next>" CausesValidation="True" OnClientClick="Javascript:return SetAction();" />
                <asp:Button class="button" ID="StepFinishButton" runat="server" Text="Finish" CausesValidation="True"
                    OnClientClick="Javascript:return SetFinishAction();" OnClick="WizardFinish_Click" /><%--CommandName="Finish" OnClientClick="FinishClick();return false;"--%>
            </StartNavigationTemplate>
            <StepNavigationTemplate>
                <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel" OnClientClick="Javascript:return window.close();" />
                <asp:Button class="button" ID="StepPreviousButton" runat="server" CausesValidation="True"
                    CommandName="MovePrevious" Text="Back" OnClientClick="Javascript:return SetBackAction();" />
                <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext"
                    OnClientClick="Javascript:return SetAction();" Text="Next>" CausesValidation="True" />
                <asp:Button class="button" ID="StepFinishButton" runat="server" Text="Finish" CausesValidation="True"
                    OnClientClick="Javascript:return SetFinishAction();" OnClick="WizardFinish_Click" /><%--CommandName="Finish" OnClientClick="FinishClick();return false;"--%>
            </StepNavigationTemplate>
            <FinishNavigationTemplate>
                <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel" OnClientClick="CancelClick();return false;" />
                <asp:Button class="button" ID="StepPreviousButton" runat="server" CausesValidation="True"
                    CommandName="MovePrevious" Text="Back" OnClientClick="Javascript:return SetBackAction();" />
                <asp:Button class="button" ID="StepNextButton" disabled="true" runat="server" CommandName="MoveNext"
                    Text="Next" CausesValidation="True" />
                <asp:Button class="button" ID="StepFinishButton" runat="server" CommandName="MoveComplete"
                    Text="Finish" CausesValidation="True" OnClientClick="Javascript:return SetFinishAction();"
                    OnClick="WizardFinish_Click" /><%--OnClientClick="FinishClick();return false;" OnClientClick="return SaveQuery();"--%>
            </FinishNavigationTemplate>
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="BES Wizard - Step 1 of 4 (Basic Information)">
                    <table>
                        <tr>
                            <td colspan="2" class="msgheader">
                                Organizational Entity Security Setup
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <img src="../../../Images/frmorgsec.gif">
                            </td>
                            <td width="*" align="center">
                                The entity security group setup wizard will walk you through the process of creating/updating
                                your entity security group definitions. In this step you will select the organizational
                                entities that the users in this group will have access to.
                            </td>
                        </tr>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <input type="hidden" name="$node^9" value="0" id="index0" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="required">
                                What do you want to call this security group?&nbsp;:<input type="text" name="$node^64"
                                    value="" id="txtGroupName" runat="server" />
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="required">
                                Which parts of the organization should this group have access to?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="$node^94" id="optAll" runat="server" onclick="SelectOnlyOneRadio(this, 'Wizardlist');" />All
                                Organizational Entities<br>
                                <input type="radio" name="$node^94" id="optSome" runat="server" onclick="SelectOnlyOneRadio(this, 'Wizardlist');" />Selected
                                Organizational Entities
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--<select name="$node^96" multiple id="lstSelEntities" size="6">
                                </select><input type="button" class="button" id="btnSelEntities" value="..." onclick="selectCode('orgh','lstSelEntities','ALL')"><input
                                    type="button" class="button" id="btndelSelEntities" value="-" onclick="deleteSelCode('lstSelEntities')"><input
                                        type="text" name="$node^97" value="" style="display: none" id="lstSelEntities_lst"><input
                                            type="text" name="$node^97" value="" style="display: none" id="lstSelEntities_cid">--%>
                                <asp:ListBox SelectionMode="Multiple" Rows="3" orgid="" ID="Org" runat="server" RMXRef="/Instance/Document/Holidays/control[@name ='Org']" />
                                <asp:TextBox Style="display: none" ID="Org_lst" runat="server" />
                                <asp:Button ID="Orgbtn" OnClientClick="return selectCode('orgh','Wizardlist_Org','ALL')"
                                    Text="..." runat="server" class="EllipsisControl" />
                                <asp:Button ID="Orgbtndel" OnClientClick="return deleteSelCode('Wizardlist_Org')"
                                    Text="-" class="BtnRemove" runat="server" />
                                <asp:TextBox Style="display: none" ID="Org_cid" runat="server" />
                                <asp:TextBox Style="display: none" ID="Org_orglist" runat="server" RMXRef="/Instance/Document/Holidays/control[@name ='Org_orglist']" />
                                <asp:TextBox Style="display: none" ID="OrgId" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <%if (ViewEnhanceBES.Value == "yes")
                          {
                              if (rowId.Value == "")
                              {
                                  if (DBType.Value == "4")
                                  {%>
                        <tr>
                            <td>
                                <input type="checkbox" name="$node^224" value="1" appearance="full" readonly="false"
                                    relevant="true" xxforms:required="false" xxforms:valid="true" xxforms:name="$node^224"
                                    xxforms:value="" xxforms:node-ids="224" id="defer1" runat="server" />Defer Changes
                            </td>
                        </tr>
                        <%}
                              }
                          }%>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="BES Wizard - Step 2 of 4 (Query Field Selection)">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                        <tr>
                            <td colspan="2" class="msgheader">
                                Organizational Entity Security Setup
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <img src="../../../Images/frmorgsec.gif">
                            </td>
                            <td width="*" align="center">
                                In this step you will supply which types of adjuster dated text data may be accessed
                                by this group.
                            </td>
                        </tr>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                                <input type="hidden" name="$node^9" value="1" id="index1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="required">
                                Which types of Adjuster Dated Text should this group have access to?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="$node^121" value="1" id="optAllAdjTextTypes" runat="server"
                                    onclick="javascript:disableAdjField(this.id)" />
                                All Adjuster Text Types<br>
                                <input type="radio" name="$node^121" value="2" id="optSomeAdjTextTypes" runat="server"
                                    onclick="javascript:disableAdjField(this.id)" />
                                Only Selected Adjuster Text Types<br>
                                <input type="radio" name="$node^121" value="3" id="optExceptAdjTextTypes" runat="server"
                                    onclick="javascript:disableAdjField(this.id)" />All Types Except Selected Adjuster
                                Text Types &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--<select name="$node^123" multiple id="lstAdjTextTypes" size="6">
                                </select><input type="button" class="button" id="btnAdjTextTypes" value="..." onclick="selectCode('ADJ_TEXT_TYPE','lstAdjTextTypes')"><input
                                    type="button" class="button" id="btndelAdjTextTypes" value="-" onclick="deleteSelCode('lstAdjTextTypes')"><input
                                        type="text" name="$node^124" value="lstAdjTextTypes_lst" style="display: none"
                                        id="lstAdjTextTypes_lst">--%>
                                <%--<asp:ListBox SelectionMode="Multiple" Rows="3" orgid="" ID="lstAdjTextTypes" runat="server" RMXRef="/Instance/Document/Holidays/control[@name ='Org']" />
                                <asp:TextBox Style="display: none" ID="lstAdjTextTypes_lst" runat="server" />
                                <asp:Button ID="btnAdjTextTypes" OnClientClick="return selectCode('ADJ_TEXT_TYPE','Wizardlist_lstAdjTextTypes')"
                                    Text="..." runat="server" />
                                <asp:Button ID="btndelAdjTextTypes" OnClientClick="return deleteSelCode('Wizardlist_lstAdjTextTypes')"
                                    Text="-" runat="server" />
                                <asp:TextBox Style="display: none" ID="lstAdjTextTypes_cid" runat="server" />
                                <asp:TextBox Style="display: none" ID="TextBox3" runat="server" RMXRef="/Instance/Document/Holidays/control[@name ='Org_orglist']" />
                                <asp:TextBox Style="display: none" ID="TextBox4" runat="server" />--%>
                                <uc:MultiCode runat="server" ID="lstAdjTextTypes" CodeTable="ADJ_TEXT_TYPE" ControlName="Wizardlist_lstAdjTextTypes"
                                    RMXRef="/Instance/Claim/PrimaryPiEmployee/BodyParts" RMXType="codelist" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <%if (ViewEnhanceBES.Value == "yes")
                          {
                              if (rowId.Value == "")
                              {
                                  if (DBType.Value == "4")
                                  {%>
                        <tr>
                            <td>
                                <input type="checkbox" name="$node^224" value="1" appearance="full" readonly="false"
                                    relevant="true" xxforms:required="false" xxforms:valid="true" xxforms:name="$node^224"
                                    xxforms:value="" xxforms:node-ids="224" id="defer2" runat="server" />Defer Changes
                            </td>
                        </tr>
                        <%}
                              }
                          }%>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep3" runat="server" Title="BES Wizard - Step 3 of 4 (Query Field Selection)">
                    <table>
                        <tr>
                            <td colspan="2" class="msgheader">
                                Organizational Entity Security Setup
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <img src="../../../Images/frmorgsec.gif">
                            </td>
                            <td width="*" align="center">
                                In this step you will supply which policy brokers and/or insurers may be accessed
                                by this group.
                            </td>
                        </tr>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                                <input type="hidden" name="$node^9" value="2" id="index2" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="required">
                                Which broker's data should this group have access to?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="$node^165" value="1" id="optAllBrokers" runat="server" onclick="javascript:disableBrokerField(this.id)" />All
                                Brokers<br>
                                <input type="radio" name="$node^165" value="2" id="optSomeBrokers" runat="server" onclick="javascript:disableBrokerField(this.id)" />Selected
                                Brokers
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%-- <select name="$node^167" multiple id="lstBrokers" size="6">
                                </select><input type="button" class="button" id="Brokersbtn" onclick="selectCode('','Brokers')"
                                    value="..."><input type="button" class="button" id="Brokersbtndel" onclick="deleteSelCode('Brokers')"
                                        value="-"><input type="text" name="$node^168" value="" style="display: none" id="Brokers_lst">--%>
                                <!--Nadim for Rem brokers and insurers-->
                                <%--<uc:MultiCode runat="server" ID="lstBrokers" CodeTable="Brokers" ControlName="Wizardlist_lstBrokers"
                                    RMXRef="/Instance/Claim/PrimaryPiEmployee/BodyParts" RMXType="codelist" />--%>
                                <asp:ListBox runat="server" size="3" ID="lstBrokerFirm2" RMXRef="/Instance/Policy/PolicyXInsured"
                                    RMXType="entitylist" Style="" TabIndex="35" />
                                <input type="button" id="lstBrokerFirmbtn2" tabindex="36" onclick="return lookupData('Wizardlist_lstBrokerFirm2','BROKER',4,'Wizardlist_lstBrokerFirm2',3)"
                                    class=" button" runat="server" />
                                <input type="button" class="BtnRemove" id="lstBrokerFirmbtnde2" tabindex="37" onclick="deleteSelCode('Wizardlist_lstBrokerFirm2')"
                                    value="-" runat="server" />
                                <input name="lstBrokerFirm_lst2" type="text" id="lstBrokerFirm_lst2" rmxref="/Instance/Policy/PolicyXInsured/@codeid"
                                    style="display: none" />
                                <!--Nadim for Rem brokers and insurers-->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="required">
                                Which insurer's data should this group have access to?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="$node^178" value="1" id="optAllInsurers" runat="server"  onclick="javascript:disableInsurerField(this.id)" />All
                                Insurers<br>
                                <input type="radio" name="$node^178" value="2" id="optSomeInsurers" runat="server" onclick="javascript:disableInsurerField(this.id)" />Selected
                                Insurers
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--<select name="$node^180" multiple id="lstInsurers" size="6">
                                </select><input type="button" class="button" id="Insurersbtn" onclick="selectCode('','Insurers')"
                                    value="..."><input type="button" class="button" id="Insurersbtndel" onclick="deleteSelCode('Insurers')"
                                        value="-"><input type="text" name="$node^181" value="" style="display: none" id="Insurers_lst">--%>
                                <!--Nadim for Rem brokers and insurers-->
                                <%--<uc:MultiCode runat="server" ID="lstInsurers" CodeTable="Insurers" ControlName="Wizardlist_lstInsurers"
                                    RMXRef="/Instance/Claim/PrimaryPiEmployee/BodyParts" RMXType="codelist" />--%>
                                <asp:ListBox runat="server" size="3" ID="lstInsurerFirm3" RMXRef="/Instance/Policy/PolicyXInsured"
                                    RMXType="entitylist" Style="" TabIndex="35" />
                                <input type="button" id="lstInsurerFirmbtn3" tabindex="36" onclick="return lookupData('Wizardlist_lstInsurerFirm3','INSURERS',4,'Wizardlist_lstInsurerFirm3',3)"
                                    class=" button" runat="server" />
                                <input type="button" class="BtnRemove" id="lstInsurerFirmbtnde3" tabindex="37" onclick="deleteSelCode('Wizardlist_lstInsurerFirm3')"
                                    value="-" runat="server" />
                                <input name="lstInsurerFirm_lst3" type="text" id="lstInsurerFirm_lst3" rmxref="/Instance/Policy/PolicyXInsured/@codeid"
                                    style="display: none" />
                                <!--Nadim for Rem brokers and insurers-->
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <%if (ViewEnhanceBES.Value == "yes")
                          {
                              if (rowId.Value == "")
                              {
                                  if (DBType.Value == "4")
                                  {%>
                        <tr>
                            <td>
                                <input type="checkbox" name="$node^224" value="1" appearance="full" readonly="false"
                                    relevant="true" xxforms:required="false" xxforms:valid="true" xxforms:name="$node^224"
                                    xxforms:value="" xxforms:node-ids="224" id="defer3" runat="server" />Defer Changes
                            </td>
                        </tr>
                        <%}
                              }
                          }%>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep4" runat="server" Title="BES Wizard - Step 4 of 4 (Query Field Selection)">
                    <%--nadim for 14334--%>
                    <asp:Label runat="server" ID="lblerrorMessage" Width="100%" ForeColor="RED"></asp:Label>
                    <%--nadim for 14334--%>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                        <tr>
                            <td colspan="2" class="msgheader">
                                Organizational Entity Security Setup
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <img src="../../../Images/frmorgsec.gif">
                            </td>
                            <td width="*" align="center">
                                In this step you will assign users to the BES group.
                            </td>
                        </tr>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                                <input type="hidden" name="$node^9" value="3" id="index3" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--<select name="$node^170" multiple id="lstUsers" size="6">
                                </select><input type="button" class="button" id="btnUsers" value="..." onclick="AddCustomizedListUser('autodiarysetup','lstUsers','','')"><input
                                    type="button" class="button" id="btndelUsers" value="-" onclick="DelCustomizedListUser('autodiarysetup','lstUsers','','')">--%>
                                <%-- <select multiple="true" id="" size="3" runat="server" style="width: 190px;
                                    height: 71px"></select>--%>
                                <asp:ListBox SelectionMode="Multiple" Rows="3" ID="lstUsers" runat="server" Style="width: 190px;
                                    height: 71px" />
                                <input type="button" class="EllipsisControl" value="..." id="cmdAddCustomizedListUser"
                                    onclick="AddCustomizedListUser('autodiarysetup','Wizardlist_lstUsers','Wizardlist_UserIdStr','Wizardlist_UserStr')" />
                                <input type="button" class="BtnRemove" value="-" id="cmdDelCustomizedListUser" style="width: 21px"
                                    onclick="DelCustomizedListUser('creatediary','Wizardlist_lstUsers','Wizardlist_UserIdStr','Wizardlist_UserStr')" />
                                <input type="hidden" runat="server" name="" value="" id="UserIdStr" />
                                <input runat="server" id="UserStr" type="hidden" />
                            </td>
                        </tr>
                        <%if (IsOptAll.Value == "0")
                          {
                              //pmittal5 - Password change functionality in case of SQL also.
                              //if (DBType.Value == "4")
                              if (ConfRec.Value == "0")
                              {%>
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" id="chkOveridePass" onclick="EnableDisablePassword()" runat="server" /><label>Please
                                    check the checkbox if you want to enter new Password to overide the default Password.If
                                    the checkbox is not checked then the default password will be saved.&nbsp;</label>
                            </td>
                        </tr>
                        <%if (rowId.Value != "")
                          {%>
                        <tr>
                            <td colspan="2">
                                Group Name&nbsp;:<input type="text" name="$node^72" id="txtGroupId" disabled="true"
                                    runat="server"><%=txtGroupId.Value%></input>
                            </td>
                        </tr>
                        <%}%>
                        <tr>
                            <td colspan="2">
                                Please enter Password&nbsp;:<input type="password" name="$node^68" size="30" id="txtPass"
                                    runat="server" disabled="true" autocomplete="off" />
                                &nbsp;
                            </td>
                        </tr>
                        <%}
                          }%>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <%if (ViewEnhanceBES.Value == "yes")
                          {
                              if (rowId.Value == "")
                              {
                                  if (DBType.Value == "4")
                                  {%>
                        <tr>
                            <td>
                                <input type="checkbox" name="$node^224" value="1" ref="/Instance/Document/form/group//control[@name='Defer']"
                                    appearance="full" readonly="false" relevant="true" xxforms:required="false" xxforms:valid="true"
                                    xxforms:name="$node^224" xxforms:value="" xxforms:node-ids="224" id="defer4"
                                    runat="server" />Defer Changes
                            </td>
                        </tr>
                        <%}
                              }
                          }%>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep5" runat="server" Title="BES Wizard - Step 2 of 5 (Query Field Selection)">
                    <table>
                        <tr>
                            <td colspan="2" class="msgheader">
                                Organizational Entity Security Setup
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <img src="../../../Images/frmorgsec.gif">
                            </td>
                            <td width="*" align="center">
                                In this step you will supply which policy broker firms may be accessed by this group.
                            </td>
                        </tr>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                                <input type="hidden" name="$node^9" value="2" id="index4" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="required">
                                Which broker firms' data should this group have access to?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="$node^165" value="1" id="optAllBrokers1" runat="server" />All
                                Broker Firms<br>
                                <input type="radio" name="$node^165" value="2" id="optSomeBrokers1" runat="server" />Selected
                                Broker Firms
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%-- <select name="$node^167" multiple id="lstBrokers" size="6">
                                </select><input type="button" class="button" id="Brokersbtn" onclick="selectCode('','Brokers')"
                                    value="..."><input type="button" class="button" id="Brokersbtndel" onclick="deleteSelCode('Brokers')"
                                        value="-"><input type="text" name="$node^168" value="" style="display: none" id="Brokers_lst">--%>
                                <%-- <select  multiple="multiple" name="inslastname"  id="inslastname" tabindex="21" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" RMXRef="/Instance/Policy/InsurerEntity/LastName" RMXType="entitylookup" style="background-color:White;" />
                                    <input type="submit" name="inslastnamebtn" value="" onclick="return lookupData('inslastname','BROKER_FIRM',4,'ins',1);" id="inslastnamebtn" tabindex="22" class="EllipsisControl" />--%>
                                <asp:ListBox runat="server" size="3" ID="lstBrokerFirm1" RMXRef="/Instance/Policy/PolicyXInsured"
                                    RMXType="entitylist" Style="" TabIndex="35" />
                                <input type="button" id="lstBrokerFirmbtn" tabindex="36" onclick="return lookupData('Wizardlist_lstBrokerFirm1','BROKER_FIRM',4,'Wizardlist_lstBrokerFirm1',3)"
                                    class=" button" runat="server" />
                                <input type="button" class="BtnRemove" id="lstBrokerFirmbtndel" tabindex="37" onclick="deleteSelCode('Wizardlist_lstBrokerFirm1')"
                                    value="-" runat="server" />
                                <input name="lstBrokerFirm_lst" type="text" id="lstBrokerFirm_lst" rmxref="/Instance/Policy/PolicyXInsured/@codeid"
                                    style="display: none" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep6" runat="server" Title="Search Query Wizard - Step 2 of 4 (Query Field Selection)">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                        <tr>
                            <td colspan="2" class="msgheader">
                                Organizational Entity Security Setup
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <img src="../../../Images/frmorgsec.gif">
                            </td>
                            <td width="*" align="center">
                                In this step you will supply which Enhance Note type(s) data may be accessed
                                by this group.
                            </td>
                        </tr>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                                <input type="hidden" name="$node^9" value="1" id="index5" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="required">
                                Which types of enhance note type(s) should this group have access to?
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" value="1" id="optAllEnhanceNoteTypes" runat="server" onclick="javascript:noteTypeSelection(this.id)" />
                                All Enhance Note Type(s)<br>
                                <input type="radio" value="2" id="optSomeEnhanceNoteTypes" runat="server" onclick="javascript:noteTypeSelection(this.id)" />
                                Only Selected Enhance Note Type(s)<br>
                                <input type="radio" value="3" id="optExceptEnhanceNoteTypes" runat="server" onclick="javascript:noteTypeSelection(this.id)" />All
                                Types Except Selected Enhance Note Type(s) &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc:MultiCode runat="server" ID="lstEnhanceNoteTypes" CodeTable="NOTE_TYPE_CODE"
                                    ControlName="Wizardlist_lstEnhanceNoteTypes" RMXRef="/Instance/Claim/PrimaryPiEmployee/BodyParts"
                                    RMXType="codelist" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="group" colspan="2">
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep7" runat="server" Title="Search Query Wizard - Step 2 of 4 (Query Field Selection)">
                    <table>
                    </table>
                </asp:WizardStep>
            </WizardSteps>
        </asp:Wizard>
    </div>
    </form>
</body>
</html>
