﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class PVCreate : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            if (!IsPostBack)
            {
                Page OriginalPage = (Page)Context.Handler;
                TextBox textOrigRowId = (TextBox)OriginalPage.FindControl("rowid");
                TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
                if (txtRowId != null && textOrigRowId != null && textOrigRowId.Text != "")
                {
                    txtRowId.Text = textOrigRowId.Text;
                    CheckBox chkClone = (CheckBox)this.Form.FindControl("chkClone");
                    chkClone.Visible = false;
                }
                XElement oTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunctionBind("PVCreateAdaptor.Get", out sreturnValue, oTemplate);
                if (bReturnStatus)
                {
                    BindpageControls(sreturnValue);
                }
            }
            
            
        }
        protected void GoToPVList(object sender, EventArgs e)
        {
            Response.Redirect("PVList.aspx");
        }
        //protected void Save(object sender, EventArgs e)
        //{
        //    bool bReturnStatus = false;
        //    string sreturnValue = "";
        //     DropDownList lstPages = (DropDownList)this.Form.FindControl("cboHomePage");
        //     TextBox txtHomePage = (TextBox)this.Form.FindControl("txtHome");
        //    if (txtHomePage != null)
        //        txtHomePage.Text = lstPages.SelectedValue;
        //    bReturnStatus = CallCWSFunction("PVCreateAdaptor.Save", out sreturnValue);
        //    if (bReturnStatus)
        //    {

        //        Response.Redirect("PVList.aspx");
        //    }
        //}s

        //athapa2:modified Save() for mits:14386

        protected void Save(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool bReturnStatus = false;
            string sreturnValue = "";
            try
            {
             DropDownList lstPages = (DropDownList)this.Form.FindControl("cboHomePage");
             TextBox txtHome = (TextBox)this.Form.FindControl("txtHome");
             if (txtHome != null)
                 txtHome.Text = lstPages.SelectedValue;
                
            txtHomePage.Text = hdnHomeSysName.Value;
   
            bReturnStatus = CallCWSFunction("PVCreateAdaptor.Save", out sreturnValue);
            if (bReturnStatus)
            {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sreturnValue);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                    {
                        message = oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/ExtendedStatus/ExtendedStatusDesc").InnerText;
                        Exception ex = new Exception(message);
                        ErrorHelper.logErrors(ex);
                        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                        err.Add(ex, BusinessAdaptorErrorType.SystemError);
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    }

                    else
                Response.Redirect("PVList.aspx");
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
//athapa2:changes ends
        private void BindpageControls(string sreturnValue)
        {
            int iCount = 0;
            string sPageMenu = "";
            string sHomeSysName = "";
            DataSet pagesRecordsSet = null;
            //XmlElement objRowTxt = null;
            XmlDocument pagesXDoc = new XmlDocument();
            //XmlDocument pageOptions = new XmlDocument();
            //XElement oPageOptions = GetOptions();
            //pageOptions.LoadXml(oPageOptions.ToString());
            pagesXDoc.LoadXml(sreturnValue);
            //XmlElement objControl = (XmlElement)pagesXDoc.SelectSingleNode("//control[@name='HomePage']");
            ////XmlElement objform = (XmlElement)pagesXDoc.SelectSingleNode("//form");
            //XmlNodeList pageOptionNodes = pageOptions.GetElementsByTagName("option");
            //foreach (XmlElement objOption in pageOptionNodes)
            //{
            //    objRowTxt = pagesXDoc.CreateElement("option");
            //    objRowTxt.SetAttribute("pagemenu", objOption.Attributes["pagemenu"].Value);
            //    //Geeta 11/17/2006 : Modified for fixing mits no.8087
            //    objRowTxt.SetAttribute("value", objOption.Attributes["value"].Value);
            //    objRowTxt.InnerText = objOption.InnerText;
            //    objControl.AppendChild((XmlNode)objRowTxt);
            //    //objform.InsertAfter((XmlNode)objRowTxt,(XmlNode) objControl);
            //    //objControl = objRowTxt;
            //}

            //added by Nitin for Mits 16457 on 11-May-2009
            //in order to populate hddenfield on page load
            hdnHomeSysName.Value = txtHomePage.Text; 
            
            pagesRecordsSet = ConvertXmlDocToDataSet(pagesXDoc);
            
          
            //if (stateRecordsSet.Tables[5] != null)
            //{
            //    grdStateMaint.DataSource = stateRecordsSet.Tables[5];
            //    grdStateMaint.DataBind();
            //}
            DropDownList lstPages = (DropDownList)this.Form.FindControl("cboHomePage");
            //Deb : ML changes
            DataView objView = new DataView(pagesRecordsSet.Tables[8]);
            objView.RowFilter = pagesRecordsSet.Tables[8].Columns["control_id"].ColumnName + "=" + "0";
            objView.RowStateFilter = DataViewRowState.CurrentRows;
            //lstPages.DataSource = pagesRecordsSet.Tables[8].DefaultView;
            lstPages.DataSource = objView[0].DataView;
            //Deb : ML changes
            lstPages.DataTextField = "option_Text";
            lstPages.DataValueField = "value";
            lstPages.Attributes["pagemenu"] = "pagemenu";
          //added by Nitin for Default page implemetation in R5
            lstPages.Attributes["homeSysName"] = "homeSysName";

            lstPages.DataBind();
            pagesRecordsSet.Dispose();
            foreach(ListItem lPage in lstPages.Items)
            {
                sPageMenu = pagesXDoc.SelectNodes("//control[@name='HomePage']/option").Item(iCount).Attributes["pagemenu"].Value;
                lPage.Attributes.Add("pagemenu", sPageMenu);

                //added by Nitin for Default page implemetation in R5
                sHomeSysName = pagesXDoc.SelectNodes("//control[@name='HomePage']/option").Item(iCount).Attributes["homeSysName"].Value;
                lPage.Attributes.Add("homeSysName", sHomeSysName);

                iCount++;
            }
           SortCboHomePage(ref lstPages);
            txtHomePage.Enabled = false;
           
            
        }

        private void SortCboHomePage(ref DropDownList objDDL)
        {
            ArrayList arrTextList = new ArrayList();
            ArrayList arrValueList = new ArrayList();
            ArrayList arrMdiMenuList = new ArrayList();
            ArrayList arrPagemenuList = new ArrayList(); 
            foreach (ListItem li in objDDL.Items)
            {
                arrTextList.Add(li.Text);
            }

            arrTextList.Sort();


            foreach (object item in arrTextList)
            {
                string value = objDDL.Items.FindByText(item.ToString()).Value;
                arrValueList.Add(value);
                string strMdiMenu = objDDL.Items.FindByText(item.ToString()).Attributes["homeSysName"];
                arrMdiMenuList.Add(strMdiMenu);

                string strPagemenu = objDDL.Items.FindByText(item.ToString()).Attributes["pagemenu"];
                arrPagemenuList.Add(strPagemenu);
            }
            objDDL.Items.Clear();

            for (int i = 0; i < arrTextList.Count; i++)
            {
                ListItem objItem = new ListItem(arrTextList[i].ToString(), arrValueList[i].ToString());
                objItem.Attributes.Add("homeSysName", arrMdiMenuList[i].ToString());
                objItem.Attributes.Add("pagemenu", arrPagemenuList[i].ToString());
                objDDL.Items.Add(objItem);


                //Showing respective url for home page
                if (txtHomePage.Text == arrMdiMenuList[i].ToString())
                {
                    txtHomePage.Text = arrValueList[i].ToString();
                }
                
            }

        }

        private XElement GetOptions()
        {
            XElement oOptions = XElement.Parse(@"
<control name='HomePage' type='combobox' title='Home Page'>
            <option pagemenu='Documents' value='UI/FDM/event.aspx?fname=event&amp;mdiparentmenu=Document'>Events</option>
            <option pagemenu='Documents' value='UI/FDM/claimgc.aspx?fname=gc&amp;mdiparentmenu=Document'>General Claims</option>
            <option pagemenu='Documents' value='UI/FDM/claimwc.aspx?fname=wc&amp;mdiparentmenu=Document'>Workers' Compensation</option>
            <!--start:Sumit-11/06/2009 - MITS#18530 -Added Property claim to Dropdown -->
            <option pagemenu='Documents' value='UI/FDM/claimpc.aspx?fname=pc&amp;mdiparentmenu=Document'>Property Claim</option>
            <!--end:Sumit-11/06/2009 - MITS#18530 -Added Property claim to Dropdown -->
            <option pagemenu='Documents' value='UI/FDM/claimva.aspx?fname=va&amp;mdiparentmenu=Document'>Vehicle Accident</option>
            <option pagemenu='Documents' value='UI/FDM/claimdi.aspx?fname=nonocc&amp;mdiparentmenu=Document'>Non-Occupational</option>
            <option pagemenu='Documents' value='UI/FDM/policy.aspx?fname=polmgt&amp;mdiparentmenu=Search'>Policy Management</option>
            <option pagemenu='Documents' value='UI/FDM/patient.aspx?fname=PT&amp;mdiparentmenu=Maintenance'>Patient Tracking</option>
            <option pagemenu='Documents' value='UI/FDM/vehicle.aspx?fname=VM&amp;mdiparentmenu=Maintenance'>Vehicle Maintenance</option>
            <!--start:Sumit-11/12/2009 - MITS#18530 -Added Property Maintenance -->
            <option pagemenu='Documents' value='UI/FDM/property.aspx?fname=PRM&amp;mdiparentmenu=Maintenance'>Property Maintenance</option>
            <!--end:Sumit-11/12/2009 - MITS#18530 -Added Property Maintenance -->
            <option pagemenu='Documents' value='UI/FDM/people.aspx?fname=PM&amp;mdiparentmenu=Maintenance'>People Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/employee.aspx?fname=EmpM&amp;mdiparentmenu=Maintenance'>Employee Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/entitymaint.aspx?fname=EM&amp;mdiparentmenu=Maintenance'>Entity Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/physician.aspx?fname=PM&amp;mdiparentmenu=Maintenance'>Physician Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/leaveplan.aspx?fname=LPM&amp;mdiparentmenu=Maintenance'>Leave Plan Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/plan.aspx?fname=DP&amp;mdiparentmenu=Search'>Disability Plan</option>
            <option pagemenu='Documents' value='UI/FDM/staff.aspx?fname=SM&amp;mdiparentmenu=Maintenance'>Staff Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/bankaccount.aspx?fname=BM&amp;mdiparentmenu=Funds'>Bank Account Management</option>
            <option pagemenu='Documents' value='UI/FDM/deposit.aspx?fname=deposit&amp;mdiparentmenu=Funds'>Deposit</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=claim&amp;mdiparentmenu=Search'>Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=claim&amp;mdiparentmenu=Search'>Claims Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=event&amp;mdiparentmenu=Search'>Events Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=employee&amp;mdiparentmenu=Search'>Employees Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=entity&amp;mdiparentmenu=Search'>Entities Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=vehicle&amp;mdiparentmenu=Search'>Vehicles Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=policy&amp;mdiparentmenu=Search'>Policies Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=payment&amp;mdiparentmenu=Search'>Funds Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=patient&amp;mdiparentmenu=Search'>Patients Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=physician&amp;mdiparentmenu=Search'>Physicians Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=staff&amp;mdiparentmenu=Search'>Med. Staff Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=dp&amp;mdiparentmenu=Search'>Disability Plan Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=leaveplan&amp;mdiparentmenu=Search'>Leave Plan Search</option>
            <option pagemenu='Diaries' value='UI/Diaries/DiaryList.aspx?fname=Diaries&amp;mdiparentmenu=Diaries'>Diaries</option>
            <option pagemenu='Files' value='UI/Document/DocumentList.aspx?flag=Files&amp;mdiparentmenu=User Documents'>Files</option>
            <option pagemenu='Reports' value='home?pg=riskmaster/DCC/reports-listing'>Reports</option>
            <option pagemenu='Reports' value='home?pg=riskmaster/Reports/JobQueue'>Reports Job Queue</option>
            <option pagemenu='' value='custom'>Custom Home Page</option>
            <option pagemenu='Documents' value='UI/FDM/admintrackinglist.aspx?SysViewType=controlsonly&amp;mdiparentmenu=Utilities'>Administrative Tracking</option>
 </control>           
");

            return oOptions;
        }
        private XElement GetMessageTemplate()
        {
            //smishra25:17815:Added a blank option in the template for whats new page.
            XElement oTemplate = XElement.Parse(@"
           <Message>
  <Authorization>2d57b607-8783-475a-9c63-504dab153e23</Authorization>
  <Call>
    <Function>PVCreateAdaptor.Get</Function>
  </Call>
  <Document>
    <form>
      <group>
        <PVCreate>
          <ViewName></ViewName>
          <ViewDesc></ViewDesc>
          <HomePageURL></HomePageURL>
          <CloneBaseView>False</CloneBaseView>
          <PageMenu></PageMenu>
        </PVCreate>
        <PVList>
          <RowId></RowId>
        </PVList>
        <displaycolumn>
<control name='HomePage'>
            <option pagemenu='' homeSysName='' value=''></option>
            <option pagemenu='Documents' value='UI/FDM/event.aspx?fname=event&amp;mdiparentmenu=Document' homeSysName='event'>Event</option>
            <option pagemenu='Documents' value='UI/FDM/claimgc.aspx?fname=gc&amp;mdiparentmenu=Document' homeSysName='claimgc'>General Claim</option>
            <option pagemenu='Documents' value='UI/FDM/claimwc.aspx?fname=wc&amp;mdiparentmenu=Document' homeSysName='claimwc'>Workers' Compensation</option>
            <!--start:Sumit-11/06/2009 - MITS#18530 -Added Property claim to Dropdown -->
            <option pagemenu='Documents' value='UI/FDM/claimpc.aspx?fname=pc&amp;mdiparentmenu=Document' homeSysName='claimpc'>Property Claim</option>
            <!--end:Sumit-11/06/2009 - MITS#18530 -Added Property claim to Dropdown -->
            <option pagemenu='Documents' value='UI/FDM/claimva.aspx?fname=va&amp;mdiparentmenu=Document' homeSysName='claimva'>Vehicle Accident</option>
            <option pagemenu='Documents' value='UI/FDM/claimdi.aspx?fname=nonocc&amp;mdiparentmenu=Document' homeSysName='claimdi'>Non-Occupational</option>
  <!--Changed by Aman for Driver Enhancement -->
            <option pagemenu='Documents' value='UI/FDM/Driver.aspx?fname=DM&amp;mdiparentmenu=Maintenance' homeSysName='driver'>Driver Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/policy.aspx?fname=polmgt&amp;mdiparentmenu=Search' homeSysName='policy'>Policy Tracking</option>
            <option pagemenu='Documents' value='UI/FDM/patient.aspx?fname=PT&amp;mdiparentmenu=Maintenance' homeSysName='patient'>Patient Tracking</option>
            <option pagemenu='Documents' value='UI/FDM/vehicle.aspx?fname=VM&amp;mdiparentmenu=Maintenance' homeSysName='vehicle'>Vehicle Maintenance</option>
            <!--start:Sumit-11/12/2009 - MITS#18530 -Added Property Maintenance -->
            <!--Changed by Gagan for MITS 21673 -->
            <option pagemenu='Documents' value='UI/FDM/property.aspx?fname=PRM&amp;mdiparentmenu=Maintenance' homeSysName='propertyunit'>Property Maintenance</option>
            <!--Changed by Gagan for MITS 21673 -->
            <!--end:Sumit-11/12/2009 - MITS#18530 -Added Property Maintenance -->
            <option pagemenu='Documents' value='UI/FDM/people.aspx?fname=PM&amp;mdiparentmenu=Maintenance' homeSysName='people'>People Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/employee.aspx?fname=EmpM&amp;mdiparentmenu=Maintenance' homeSysName='employee'>Employee Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/entitymaint.aspx?fname=EM&amp;mdiparentmenu=Maintenance' homeSysName='entitymaint'>Entity Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/physician.aspx?fname=PM&amp;mdiparentmenu=Maintenance' homeSysName='physician'>Physician Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/leaveplan.aspx?fname=LPM&amp;mdiparentmenu=Maintenance' homeSysName='leaveplan'>Leave Plan Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/plan.aspx?fname=DP&amp;mdiparentmenu=Search' homeSysName='plan'>Disability Plan</option>
            <option pagemenu='Documents' value='UI/FDM/staff.aspx?fname=SM&amp;mdiparentmenu=Maintenance' homeSysName='staff'>Staff Maintenance</option>
            <option pagemenu='Documents' value='UI/FDM/bankaccount.aspx?fname=BM&amp;mdiparentmenu=Funds' homeSysName='bankaccount'>Bank Account Management</option>
            <option pagemenu='Documents' value='UI/FDM/deposit.aspx?fname=deposit&amp;mdiparentmenu=Funds' homeSysName='deposit'>Deposit</option>
            <!--<option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=claim&amp;mdiparentmenu=Search' homeSysName='sclaim'>Search</option>-->
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=claim&amp;mdiparentmenu=Search' homeSysName='sclaim'>Claim Search</option>
<!--Changed by Aman for Driver Enhancement -->            
<option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=driver&amp;mdiparentmenu=Search' homeSysName='sdriver'>Driver Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=event&amp;mdiparentmenu=Search' homeSysName='sevent'>Event Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=employee&amp;mdiparentmenu=Search' homeSysName='semployee'>Employee Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=entity&amp;mdiparentmenu=Search' homeSysName='sentity'>Entity Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=vehicle&amp;mdiparentmenu=Search' homeSysName='svehicle'>Vehicle Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=policy&amp;mdiparentmenu=Search' homeSysName='spolicy'>Policy Tracking Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=payment&amp;mdiparentmenu=Search' homeSysName='sfunds'>Funds Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=patient&amp;mdiparentmenu=Search' homeSysName='spatient'>Patient Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=physician&amp;mdiparentmenu=Search' homeSysName='sphysician'>Physician Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=staff&amp;mdiparentmenu=Search' homeSysName='smedstaff'>Med. Staff Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=dp&amp;mdiparentmenu=Search' homeSysName='splan'>Disability Plan Search</option>
            <option pagemenu='Search' value='UI/Search/SearchMain.aspx?formname=leaveplan&amp;mdiparentmenu=Search' homeSysName='sleaveplan'>Leave Plan Search</option>
            <option pagemenu='Diaries' value='UI/Diaries/DiaryList.aspx?fname=Diaries&amp;mdiparentmenu=Diaries' homeSysName='DiaryList'>Diaries</option>
            <option pagemenu='Files' value='UI/Document/DocumentList.aspx?flag=Files&amp;mdiparentmenu=User Documents' homeSysName='DocumentList'>Files</option>
            <option pagemenu='Reports' value='home?pg=riskmaster/DCC/reports-listing' homeSysName='reports-listing'>Reports</option>
            <option pagemenu='Reports' value='home?pg=riskmaster/Reports/JobQueue' homeSysName='smrepqueue'>Reports Job Queue</option>
            <!--<option pagemenu='' value='custom' homeSysName='custom'>Custom Home Page</option>-->
            <option pagemenu='Documents' value='UI/FDM/admintrackinglist.aspx' homeSysName='admintrackinglist'>Administrative Tracking</option>
            <!-- Added Pending Claims for RMA-8135 gsinghal7-->
            <option pagemenu='My Work' value='UI/RecentRecords/PendingClaims.aspx' homeSysName='PendingClaims'>Pending Claims</option>
        </control> 
        </displaycolumn>
      </group>
    </form>
  </Document>
</Message>
            ");

            return oTemplate;
        }

    }
}
