﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVFormEdit.aspx.cs" Inherits="Riskmaster.UI.Utilities.PVFormEdit" ValidateRequest="false" EnableEventValidation = "false"%>

<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Power Views</title>
    <script type="text/javascript" language="javascript" src="../../../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript">
          var m_Wnd = null;
          var ns, ie, ieversion;
          var browserName = navigator.appName;                   // detect browser 
          var browserVersion = navigator.appVersion;
          var arrHelpMsg=null;
          if (browserName == "Netscape")
          {
            ie=0;
            ns=1;
          }
          else		//Assume IE
          {
            ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
            ie=1;
            ns=0;
          }
          
          function MoveField(Direction)
          {
			//nadim MITS 11365
			//var arr=document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value.split("|");
            if(document.forms[0].lstFormList.selectedIndex<0)
            {
              alert("Please select field.");
              return;
            }
            
            if(document.forms[0].lstFormList.selectedIndex==0 && Direction==0 || document.forms[0].lstFormList.selectedIndex==document.forms[0].lstFormList.options.length-1 && Direction==1)
              return;
			var arr=document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value.split("|");
			//Nadim MITS 11365
            if(arr[0]=="")
            {
              alert("System item cannot be moved.");
              return;
            }

            var sText, sValue;
            var sText2, sValue2;
            var iFrom = document.forms[0].lstFormList.selectedIndex;
            var o1,o2, iTo, iStart, iLenTop, iLenBottom;
            sText=document.forms[0].lstFormList.options[iFrom].text;
            sValue=document.forms[0].lstFormList.options[iFrom].value;

            if(Direction==0) //up
            {
            	sText2 = document.forms[0].lstFormList.options[iFrom-1].text;
            	if( sText2.indexOf("+")==0 )
            	{
            	    if(sText.indexOf("+")==0)
            	    	iLenTop = 1;
            	    else
            	    	iLenTop = GetCGMemberNumber(iFrom, 0) + 1;
            	    iTo = iFrom - iLenTop;
            	}
            	else
            	{
            	    iLenTop = 1;
            	    iTo = iFrom - iLenTop;
            	}
            	iStart = iTo;
            	
            	if( sText == "[Hidden Control Group]" )
            	    iLenBottom = GetCGMemberNumber(iFrom, 1) + 1;
            	else
            	    iLenBottom = 1;
            }
            else //down
            {
                iStart = iFrom;
                if( sText == "[Hidden Control Group]" )
		{
		    iLenTop = GetCGMemberNumber(iFrom, 1) + 1;
		    iTo = iFrom + iLenTop;
		}
		else
		{
		    iLenTop = 1;
		    iTo = iFrom + iLenTop;
		}
		
		sText2 = document.forms[0].lstFormList.options[iTo].text;
		if( sText2 == "[Hidden Control Group]" )
		    iLenBottom = GetCGMemberNumber(iTo, 1) + 1;
		else
            	    iLenBottom = 1;
            }
            
            sText2=document.forms[0].lstFormList.options[iTo].text;
	    sValue2=document.forms[0].lstFormList.options[iTo].value;

            //add constrain that a control should be under at least a group header.
            if(((iTo==0)&&(sText.substr(0,5)!="[*** ")) || ((iFrom==0) &&(sText2.substr(0,5)!="[*** ")))
            {
              alert("System should have a top group header, it cannot be moved.");
              return;
            }
	    
	    //controlgroup members can only move up/down within the group
	    if((sText.indexOf("+")==0) || (sText2.indexOf("+")==0) )
	    {
	    	if((sText.indexOf("+")!=0) || (sText2.indexOf("+")!=0) )
	    	{
	    	    alert("Controls of a controlgroup can only move within the controlgroup");
	    	    return;
	    	}
	    }

	    if(sValue2=="")
                return;
                

	    //put the values into two arrays, one for text and another for value
	    var iTotalControls = iLenTop + iLenBottom;
	    var arrTexts = new Array(iTotalControls);
	    var arrValues = new Array(iTotalControls);
	    var arrbasereadonly = new Array(iTotalControls);
	    var arrpowerviewreadonly = new Array(iTotalControls);
	    var index = 0;
	    for(var i=0; i < iLenBottom; i++)
	    {
	        arrTexts[index] = document.forms[0].lstFormList.options[iStart+iLenTop+i].text;
	        arrValues[index] = document.forms[0].lstFormList.options[iStart + iLenTop + i].value;
	        arrbasereadonly[index] = document.forms[0].lstFormList.options[iStart + iLenTop + i].getAttribute("basereadonly");
	        arrpowerviewreadonly[index] = document.forms[0].lstFormList.options[iStart + iLenTop + i].getAttribute("powerviewreadonly");
	        index = index + 1;
	    }
	   

	    for(var i=0; i < iLenTop; i++)
	    {
	        arrTexts[index] = document.forms[0].lstFormList.options[iStart+i].text;
	        arrValues[index] = document.forms[0].lstFormList.options[iStart + i].value;
	        arrbasereadonly[index] = document.forms[0].lstFormList.options[iStart + i].getAttribute("basereadonly");
	        arrpowerviewreadonly[index] = document.forms[0].lstFormList.options[iStart + i].getAttribute("powerviewreadonly");
	        index = index + 1;
	    }
            
            //switch the controls
            for(var i=0; i < iTotalControls; i++)
            {
                o1 = new Option(arrTexts[i], arrValues[i], false, false);
                var a = document.createAttribute("basereadonly");
                a.value = arrbasereadonly[i];
                o1.setAttributeNode(a);
                var pr = document.createAttribute("powerviewreadonly");
                pr.value = arrpowerviewreadonly[i];
                o1.setAttributeNode(pr);
		document.forms[0].lstFormList.options[iStart+i]=o1;
            }
            if(Direction == 0) //up
            	document.forms[0].lstFormList.options[iStart].selected=true;
            else //down
            	document.forms[0].lstFormList.options[iStart+iLenBottom].selected=true;
		
         }
          
          //Get the number of members of a control group
          function GetCGMemberNumber(iCGStart, iDirection)
          {
		var iLen =0;
		if( iDirection == 1) //counting downward
		{
		    for (var i = iCGStart + 1; i <= document.forms[0].lstFormList.options.length - 1; i++)
			{
			    sText2 = document.forms[0].lstFormList.options[i].text;            	
			    if( sText2.indexOf("+")!=0 )
			    {
				break;
			    }
			    iLen ++;
			}
            	}
            	else //counting upward
            	{
			for(var i=iCGStart-1; i > 0; i--)
			{
			    sText2 = document.forms[0].lstFormList.options[i].text;            	
			    if( sText2.indexOf("+")!=0 )
			    {
				break;
			    }
			    iLen ++;
			}            	
            	}
            	return iLen;
          }
          
          function MoveButton(Direction, lstButtonList)
          {
            if(lstButtonList.selectedIndex<0)
            {
              alert("Please select the button.");
              return;
            }
            
            if(lstButtonList.selectedIndex==0 && Direction==0 || lstButtonList.selectedIndex==lstButtonList.options.length-1 && Direction==1)
              return;
            if(lstButtonList.options[lstButtonList.selectedIndex].value=="")
            {
              alert("System item cannot be moved.");
              return;
            }
            
            var sText, sValue;
            var sText2, sValue2;
            var i=lstButtonList.selectedIndex;
            var o1,o2;
            sText=lstButtonList.options[i].text;
            sValue=lstButtonList.options[i].value;
            if(Direction==0)
            {
              sText2=lstButtonList.options[i-1].text;
              sValue2=lstButtonList.options[i-1].value;
          
              
              if(sValue2=="")
                return;
              o1=new Option(sText, sValue, false, false);
              o2=new Option(sText2, sValue2, false, false);
              lstButtonList.options[i]=o2;
              lstButtonList.options[i-1]=o1;
            }
            else
            {
              sText2=lstButtonList.options[i+1].text;
              sValue2=lstButtonList.options[i+1].value;
          
              if(sValue2=="")
                return;
              o1=new Option(sText, sValue, false, false);
              o2=new Option(sText2, sValue2, false, false);
              lstButtonList.options[i]=o2;
              lstButtonList.options[i+1]=o1;
            }
            o1.selected=true;
          }
          
          function EditField()
          {
              var sText, sValue;
             var i=document.forms[0].lstFormList.selectedIndex;
			//var arr=document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value.split("|");
            //nadim MITS 11365
			if(document.forms[0].lstFormList.selectedIndex<0)
			{
			alert("Please select field.");
			return;
			}
           
             if(document.forms[0].lstFormList.selectedIndex<0)
              return;
             var arr = document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value.split("|");
			//MITS 11365
            if(arr[0]=="")
            {
              alert("This is system field and it cannot be edited.");
              return;
            }

    	    sText=document.forms[0].lstFormList.options[i].text;
            sValue=document.forms[0].lstFormList.options[i].value;

		//Pankaj Pawan added option for mits 11210 
            if ((sText == "[Hidden Control Group]") || (sText == "-") || (sText == "[HS* New Space *HS]") || (sText.indexOf("[LB*") == 0))//asharma326 JIRA 6411 check fror line break
	    {
	       alert("Control cannot be edited.");
	       return;
	    }
            
            if(m_Wnd!=null)
              m_Wnd.close();
            
            document.forms[0].FieldDisabled.value = false;
            if(document.forms[0].sysrequired.value.indexOf(arr[0]+",")>=0)            
              document.forms[0].FieldDisabled.value = true;
            
            //-- ABhateja, 08.24.2006, MITS 7629 -START-
            document.forms[0].CaptionDisabled.value = false;
            if(document.forms[0].hdPvtitle.value.indexOf(arr[0]+",")>=0)            
              document.forms[0].CaptionDisabled.value = true;
            //-- ABhateja, 08.24.2006, MITS 7629 -END-
            document.forms[0].NoCaption.value = false;//Mits 23821
            //Added rjhamb Mits22221
          if (arr.length > 2) {
              for (var i = 0; i < arr.length; i++) {
                  if (arr[i] == "phonetype") {
                      document.forms[0].NoCaption.value = true;
                      break;
                  }
              }
          }
            //Added rjhamb Mits22221
          document.forms[0].FieldType.value = "Caption";
          if (document.forms[0].lstFormList.options[i].getAttribute("powerviewreadonly") != null) {
              arr[2] = document.forms[0].lstFormList.options[i].getAttribute("powerviewreadonly").split("|")[0];
          }
          SetFormValues(document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text, arr[1], document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].getAttribute("basereadonly"), arr[2]);
            m_Wnd = window.open('PVEditField.aspx', 'edit', 'width=600,height=250' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 600) / 2 + ',resizable=yes,scrollbars=yes');

           // var Wnd=window.open("home?pg=riskmaster/RMUtilities/PVEditField&field="+escape(document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text)+"&disabled="+sDisabled,"edit",'width=350,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2);}
            
          }
          
          function SetFormValues(sValue, sHelp, sBaseReadOnly, sRequired)
          {
              
            if(sValue != "")
            {            
             //Geeta 11-09-06 : Code added for fixing MITS number 8377			 
			  if(sHelp != undefined)
			  {
				document.forms[0].HelpMsg.value = sHelp;
			  }
			  if (sRequired != undefined) {
			      document.forms[0].FieldReadOnly.value = sRequired;
			  }
			  else
			      document.forms[0].FieldReadOnly.value = false;
			  
              document.forms[0].FieldRequired.value = "0";
              document.forms[0].FieldVisible.value = "0";
              document.forms[0].ControlGroup.value = "0";
              if (sBaseReadOnly == "1")
                  document.forms[0].BaseReadonly.value = sBaseReadOnly;
              else {
                  document.forms[0].BaseReadonly.value = "0";
              }
              if(sValue.substr(0,5)=="[*** ")
              {
                document.forms[0].FieldCaption.value = "Group Caption";
                document.forms[0].FieldName.value = sValue.substr(5,sValue.length - 10);
              }
		
              else if(sValue.substr(0,5)=="[MS* ")
              {
                document.forms[0].FieldCaption.value = "Message Caption";
                document.forms[0].FieldName.value = sValue.substr(5,sValue.length - 10);
                document.forms[0].FieldType.value = "Message";
              }
              else if(document.forms[0].FieldType.value == "Button")
              {
                document.forms[0].FieldCaption.value = "Button Caption";
                document.forms[0].FieldName.value = sValue;
              }
              else if(document.forms[0].FieldType.value == "Toolbar")
              {
                document.forms[0].FieldCaption.value = "Button Caption";
                document.forms[0].FieldName.value = sValue;
              }
              else
              {
                  document.forms[0].FieldCaption.value = "Field";
                  document.forms[0].FieldVisible.value = "1";
                  document.forms[0].FieldType.value = "";
                if(sValue.substr(0,2)=="- ")
                {
                  document.forms[0].FieldName.value = sValue.substr(2);
                  document.forms[0].FieldRequired.value = "1";
                }
                else if(sValue.indexOf("+")==0)
                {
                  document.forms[0].FieldName.value = sValue.substr(1);
                  document.forms[0].ControlGroup.value = "1";
                  document.forms[0].FieldCaption.value = "Button Caption";
                  document.forms[0].FieldVisible.value = "0";
                  document.forms[0].FieldRequired.value = "0";
                }
                else
                  document.forms[0].FieldName.value = sValue;
                
              } 
            }
            else
             document.forms[0].FieldName.value = "Invalid Parameters.";
          }
          
          function EditButton()
          {
              if (document.forms[0].lstButtonList.selectedIndex < 0)
             {
                alert("Please select Button."); //abansal23:  MITS 14347
                return false;              
              }
            
            if(document.forms[0].lstButtonList.options[document.forms[0].lstButtonList.selectedIndex].value=="")
            {
              alert("This is system field and it cannot be edited.");
              return;
            }
            
            if(m_Wnd!=null)
              m_Wnd.close();
            
            document.forms[0].CaptionDisabled.value = false;
            document.forms[0].FieldDisabled.value = false;
            document.forms[0].FieldType.value = "Button";
            SetFormValues(document.forms[0].lstButtonList.options[document.forms[0].lstButtonList.selectedIndex].text,'');
            
            m_Wnd=window.open('PVEditField.aspx','edit','width=450,height=200'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');

            //m_Wnd=window.open("home?pg=riskmaster/RMUtilities/PVEditField&field="+escape(document.forms[0].lstButtonList.options[document.forms[0].lstButtonList.selectedIndex].text)+"&button=1","edit",'width=350,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2);}
          }
          
          function EditToolbar()
          {
              if (document.forms[0].lstToolBarList.selectedIndex < 0)
             {
                alert("Please select Button."); //abansal23:  MITS 14347
                return false;
            }
            
            if(document.forms[0].lstToolBarList.options[document.forms[0].lstToolBarList.selectedIndex].value=="")
            {
              alert("This is system field and it cannot be edited.");
              return;
            }
            
            if(m_Wnd!=null)
              m_Wnd.close();

            document.forms[0].CaptionDisabled.value = false;
            document.forms[0].FieldDisabled.value = false;
            document.forms[0].FieldType.value = "Toolbar";
            SetFormValues(document.forms[0].lstToolBarList.options[document.forms[0].lstToolBarList.selectedIndex].text,'');
            
            m_Wnd=window.open('PVEditField.aspx','edit','width=450,height=200'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');
            
           // m_Wnd=window.open("home?pg=riskmaster/RMUtilities/PVEditField&field="+escape(document.forms[0].lstToolBarList.options[document.forms[0].lstToolBarList.selectedIndex].text)+"&toolbar=1","edit",'width=350,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2);
          }
          
          function DeleteField()
          {
          
            var sText;
			//nadim MITS 11365
			if(document.forms[0].lstFormList.selectedIndex<0)
			{
			alert("Please select field.");
			return;
			}
            var arr=document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value.split("|");
            if(document.forms[0].lstFormList.selectedIndex<0)
              return;
			var arr=document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value.split("|");
			//MITS 11365
            if(arr[0]=="")
            {
              alert("This is system field and it cannot be removed.");
              return;
            }
            
            if(document.forms[0].sysrequired.value.indexOf(arr[0]+",")>=0)
            {
          //MITS  Nadim 12180
          //MITS  Nadim 12182
          if(document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text.indexOf("-")>=0)
          {
          alert("System required fields cannot be removed.");
          return;
          }
          //MITS 12180

          }
            sText= document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text
            //Pankaj
        //vsharma65 start MITS 23340
        //	    if((sText=="[Hidden Control Group]")||(sText.indexOf("+")==0))
        //	    {
        //	       alert("Control Group element. Cannot be removed.");
        //	       return;
        //	    }
        //vsharma65 End MITS 23340
	    if(sText=="-")
	    {
	       alert("Control cannot be deleted.");
	       return;
	    }
	   //Added rjhamb Mits 23065
	   if (arr.length > 2) {
	       for (var i = 0; i < arr.length; i++) {
	           if (arr[i] == "mandatory") {
	               alert("Control cannot be removed.");
	               return;
	           }
	       }
	   }
	   //Added rjhamb Mits 23065

            //'Code changed by Pawan Swarnkar dtd 06/11/2004 to add constrain that a control should be under atleast a group header.
            if(sText.substr(0,5)=="[*** " && document.forms[0].lstFormList.selectedIndex == 0)
            {
              alert("System should have a top group header, it cannot be deleted.");
              return;
            }
            //code change end
            //if(!self.confirm("Delete selected item?"))
            //	return;
            
            var i=document.forms[0].lstFormList.selectedIndex;
            document.forms[0].lstFormList.options[i]=null;
            if(i<document.forms[0].lstFormList.options.length)
              document.forms[0].lstFormList.selectedIndex=i;
          }
          
          function DeleteButton(lstButtonList)
          {
              if (lstButtonList.selectedIndex < 0)
              {
                  alert("Please select Button."); //abansal23:  MITS 14347
                      return false;
              }
            
            if(lstButtonList.options[lstButtonList.selectedIndex].value=="")
            {
              alert("This is system button and it cannot be removed.");
              return;
            }
            
            //if(!self.confirm("Delete selected item?"))
            //	return;
            
            var i=lstButtonList.selectedIndex;
            lstButtonList.options[i]=null;
            if(i<lstButtonList.options.length)
              lstButtonList.selectedIndex=i;
          }

          function Reset() {
              if (!self.confirm("This will reset the form to default layout with all fields. Click OK to proceed.")) {
                  return false;
              }
              else {
                  return true;
              }
              
          }
          
          //Code changed by Pawan Swarnkar dtd 06/02/2004 for showing tabbed view for xml in context .
          function TabView()
          {  
            var s="",s1="";
            for(var i=0;i<document.forms[0].lstFormList.options.length;i++)
            {
              s=s + replace(document.forms[0].lstFormList.options[i].value,"|","") + "|";
              s1=s1 + replace(document.forms[0].lstFormList.options[i].text,"|","") + "|";
            }
            if(m_Wnd!=null)
            m_Wnd.close();
              m_Wnd=window.open("tabview.asp?Listvalue="+escape(s1),"edit",
              'scrollbars=yes,width=450,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2);
            return true;
          }
  
          function AddField()
          {
            if(document.forms[0].cboFormFields.selectedIndex<0)
              return false;
            var sValue=document.forms[0].cboFormFields.options[document.forms[0].cboFormFields.selectedIndex].value;
            var sText = document.forms[0].cboFormFields.options[document.forms[0].cboFormFields.selectedIndex].text;
            var basereadonly = document.forms[0].cboFormFields.options[document.forms[0].cboFormFields.selectedIndex].getAttribute("basereadonly");
            var powerviewreadonly = document.forms[0].cboFormFields.options[document.forms[0].cboFormFields.selectedIndex].getAttribute("powerviewreadonly");
            var opt = null;
            var sFormListValue	//gagnihotri MITS 12617 06/06/2008
            
            if(sText.substr(0,5)!="[*** " && document.forms[0].lstFormList.options.selectedIndex==0)
            {
              alert("System should have a top field as  group header only, other fields cannot be added at the top.");
                  return false;
              }
              // Naresh Start MITS 17565 When the jurisdictional tab is removed from a powerview and then added 
              // back in the fields don't come with it. The Reason is the Name of the Groups was getting changed 
              // when a group is added to the Powerview. Now as a fix for this MITS the Name of the group is kept same.
	    //Pawan added option for mits 11210
              if ((sText.substr(0, 5) == "[*** " || sText.substr(0, 5) == "[MS* " || sText.substr(0, 5) == "[HS* ") && sValue == "newgroupcaption")
              // Naresh End MITS 17565 When the jurisdictional tab is removed from a powerview and then added 
              // back in the fields don't come with it. The Reason is the Name of the Groups was getting changed 
              // when a group is added to the Powerview. Now as a fix for this MITS the Name of the group is kept same.
            {
              var i=document.forms[0].lstFormList.options.length;
              
              var bFound = false;
              var sTmp = "";
              
              while( true )
              {
                i++;
                bFound = false;
                sTmp="group"+i;
                for(var f=0;f<document.forms[0].lstFormList.options.length;f++)
                {
                  if(document.forms[0].lstFormList.options[f].value==sTmp)
                  {
                    bFound = true;
                    break;
                  }
                }
                if( !bFound )
                  break;
              }
              opt=new Option(sText, sTmp, false, false);
            }
            else
            {
              for(var f=0;f<document.forms[0].lstFormList.options.length;f++)
              {
				//gagnihotri MITS 12617 10/06/2008
				//we were missing the case where fields were added with help text
				sFormListValue = document.forms[0].lstFormList.options[f].value;
				if(sFormListValue.indexOf("|") > 0)
				{
					var arr = sFormListValue.split("|");
					if (arr[1] != "")
						sFormListValue = (arr[0] + "|");
				}
				if(sFormListValue==sValue)
                {
                  alert("Field is already in use.");
                  return false;
                }
              }
              opt=new Option(sText, sValue, false, false);
              }
              var a = document.createAttribute("basereadonly");
              a.value = basereadonly;
              opt.setAttributeNode(a);
              var pr = document.createAttribute("powerviewreadonly");
              pr.value = powerviewreadonly;
              opt.setAttributeNode(pr);

            if(document.forms[0].lstFormList.options.selectedIndex>=0)//remove ie check for as it doesn't work in ie 11 "compatbile mode off".
            {
              document.forms[0].lstFormList.options.add(opt,document.forms[0].lstFormList.options.selectedIndex);
              document.forms[0].lstFormList.selectedIndex=document.forms[0].lstFormList.selectedIndex-1;
            }
            else
            {
              document.forms[0].lstFormList.options[document.forms[0].lstFormList.options.length]=opt;
              document.forms[0].lstFormList.selectedIndex=document.forms[0].lstFormList.options.length-1;
            }
            return true;
          }
          
          function AddButton()
          {
          
            if(document.forms[0].cboCommandButtons.selectedIndex<0)
              return false;
            var sValue=document.forms[0].cboCommandButtons.options[document.forms[0].cboCommandButtons.selectedIndex].value;
            var sText=document.forms[0].cboCommandButtons.options[document.forms[0].cboCommandButtons.selectedIndex].text;
            var opt=null;
            
            if(sText.substr(0,5)=="[*** ")
            {
              var i=document.forms[0].cboCommandButtons.options.length;
              
              newid:
              while(true)
              {
                i++;
                var sTmp="group"+i;
                for(var f=0;f<document.forms[0].cboCommandButtons.options.length;f++)
                {
                  if(document.forms[0].cboCommandButtons.options[f].value==sTmp)
                    continue newid;
                }
                break;
              }
              opt=new Option(sText, sTmp, false, false);
            }
            else
            {
              for(var f=0;f<document.forms[0].lstButtonList.options.length;f++)
              {
                if(document.forms[0].lstButtonList.options[f].value==sValue)
                {
                  alert("Field is already in use.");
                  return false;
                }
              }
              opt=new Option(sText, sValue, false, false);
            }
            
            if(document.forms[0].lstButtonList.options.selectedIndex>=0)//remove ie check for as it doesn't work in ie 11 "compatbile mode off".
            {
              document.forms[0].lstButtonList.options.add(opt,document.forms[0].lstButtonList.options.selectedIndex);
              document.forms[0].lstButtonList.selectedIndex=document.forms[0].lstButtonList.selectedIndex-1;
            }
            else
            {
              //document.forms[0].lstButtonList.options[document.forms[0].lstButtonList.options.length]=opt;
              document.forms[0].lstButtonList.options.add(opt,document.forms[0].lstButtonList.options.length);
              document.forms[0].lstButtonList.selectedIndex=document.forms[0].lstButtonList.options.length-1;
            }
            return true;
          }
          
          function AddToolbarButton()
          {
            if(document.forms[0].cboToolBarButtons.selectedIndex<0)
              return false;
            var sValue=document.forms[0].cboToolBarButtons.options[document.forms[0].cboToolBarButtons.selectedIndex].value;
            var sText=document.forms[0].cboToolBarButtons.options[document.forms[0].cboToolBarButtons.selectedIndex].text;
            var opt=null;
            
            if(sText.substr(0,5)=="[*** ")
            {
              var i=document.forms[0].cboToolBarButtons.options.length;
              
              newid:
              while(true)
              {
                i++;
                var sTmp="group"+i;
                for(var f=0;f<document.forms[0].cboToolBarButtons.options.length;f++)
                {
                  if(document.forms[0].cboToolBarButtons.options[f].value==sTmp)
                    continue newid;
                }
                break;
              }
              opt=new Option(sText, sTmp, false, false);
            }
            else
            {
              for(var f=0;f<document.forms[0].lstToolBarList.options.length;f++)
              {
                if(document.forms[0].lstToolBarList.options[f].value==sValue)
                {
                  alert("Toolbar Button is already in use.");
                  return false;
                }
              }
              opt=new Option(sText, sValue, false, false);
            }
            if(document.forms[0].lstToolBarList.options.selectedIndex>=0)//remove ie check for as it doesn't work in ie 11 "compatbile mode off".
            {
              document.forms[0].lstToolBarList.options.add(opt,document.forms[0].lstToolBarList.options.selectedIndex);
              document.forms[0].lstToolBarList.selectedIndex=document.forms[0].lstToolBarList.selectedIndex-1;
            }
            else
            {
              document.forms[0].lstToolBarList.options[document.forms[0].lstToolBarList.options.length]=opt;
              document.forms[0].lstToolBarList.selectedIndex=document.forms[0].lstToolBarList.options.length-1;
            }
            return true;
          }
          
          function pageLoaded()
          {
            self.onunload=pageUnload;
            document.onWndClose=onWndClose;
            document.OnEditFieldOK=OnEditFieldOK;
            self.onfocus=onWindowFocus;
            return true;
          }
          
          function pageUnload()
          {
            if(m_Wnd!=null)
              m_Wnd.close();
            m_Wnd=null;
            return true;
          }
          
          function onWindowFocus()
          {
            if(m_Wnd!=null)
              m_Wnd.close();
            m_Wnd=null;
            return true;
          }
          
          function onWndClose()
          {
            m_Wnd=null;
            return true;
          }

          function OnEditFieldOK() {
              
            var arr;
            if(document.forms[0].lstFormList.selectedIndex >=0)
            {
              arr = document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value.split("|");
            }
            var s=m_Wnd.document.forms[0].txtCaption.value;
            var hlp=m_Wnd.document.forms[0].txtHelp.value;
            if(replace(s," ","")=="")
            {
              m_Wnd.close();
              m_Wnd=null;
              return false;
            }
            if(m_Wnd.document.forms[0].groupcaption.value!="0" && m_Wnd.document.forms[0].groupcaption.value!="")
            {
              document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text="[*** " + s + " ***]";
            }
            //'Code changed by Pawan Swarnkar dtd 06/11/2004 to add message caption validation logic.
            else if(m_Wnd.document.forms[0].messagecaption.value!="0" && m_Wnd.document.forms[0].messagecaption.value!="")
            {
              document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text="[MS* " + s + " *MS]";
            }
            //code change end 
            else if(m_Wnd.document.forms[0].buttoncaption.value!="0" && m_Wnd.document.forms[0].buttoncaption.value!="")
            {
              document.forms[0].lstButtonList.options[document.forms[0].lstButtonList.selectedIndex].text=s;
            }
            else if(m_Wnd.document.forms[0].toolbarcaption.value!="0" && m_Wnd.document.forms[0].toolbarcaption.value!="")
            {
              document.forms[0].lstToolBarList.options[document.forms[0].lstToolBarList.selectedIndex].text=s;
            }
            else if(document.forms[0].ControlGroup.value=="1")
            {
				if((s.indexOf("+")<0))
				{
					document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text="+" + s;
				}
				else
				{
					document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text=s;
				}
            }
            else
            {
                //MITS 26838 hlv 5/10/2012
                var tmpsysr = "," + document.forms[0].sysrequired.value;
              //if(document.forms[0].sysrequired.value.indexOf(arr[0]+",")<0)
              if (tmpsysr.indexOf("," + arr[0] + ",") < 0)  //MITS 26838 hlv 5/10/2012
              {
                  if (m_Wnd.document.forms[0].required.checked) {
                      document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text = "- " + s;
                  }
                  else {
                      document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text = s;
                  }
              }
              else
              {
                document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].text="- " + s;
              }
              document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value = arr[0] + "|" + hlp;// + "|" + m_Wnd.document.forms[0].chkReadOnly.checked;
              var a = document.createAttribute("powerviewreadonly");
              a.value = m_Wnd.document.forms[0].chkReadOnly.checked + "|";//+ document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].getAttribute("powerviewreadonly");
              document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].setAttributeNode(a);
              //rsushilaggar mits 25388 Date 07/07/2011
              if (arr.length > 2) {
                  document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value += "|" + arr[2];
              }

            }
            m_Wnd.close();
            m_Wnd=null;
            return true;
          }
          function replace(sSource, sSearchFor, sReplaceWith)
          {
            var arr = new Array();
            if(sSource!=null)
				arr=sSource.split(sSearchFor);
            return arr.join(sReplaceWith);
          }
          
          function onSubmitForm()
          {
              
            document.forms[0].fields.value="";
            var s="",s1="",s2="",s3 = "";
            var arr, bPvTitle;
            var arrPvTitle = new Array();
            
            //-- ABhateja, 09.05.2006, MITS 7629
            if(document.forms[0].hdPvtitle.value != "")
				arrPvTitle = document.forms[0].hdPvtitle.value.split(",");
            
            for(var i=0;i<document.forms[0].lstFormList.options.length;i++)
            {
			  arr=document.forms[0].lstFormList.options[i].value.split("|")
              s=s + replace(arr[0],"|","") + "|";
              
              
				//-- ABhateja, 09.05.2006, MITS 7629 -START-
				if(arrPvTitle.length > 0)
				{
					for(var j=0;j<arrPvTitle.length;j++)
					{
						if(arrPvTitle[j] == replace(arr[0],"|",""))
						{
							bPvTitle = "true";
							break;
						}
					}
					if(bPvTitle == "true")            
					{
						s1=s1 + "" + "|";
						bPvTitle = "false";
					}
					else
						s1=s1 + replace(document.forms[0].lstFormList.options[i].text,"|","") + "|";
				}
				else
						s1=s1 + replace(document.forms[0].lstFormList.options[i].text,"|","") + "|";
				//-- ABhateja, 09.05.2006, MITS 7629 -END-
					
			    
              
              
				s2 = s2 + replace(arr[1], "|", "") + "|";
				if (document.forms[0].lstFormList.options[i].getAttribute('powerviewreadonly') != null) {
				    s3 = s3 + replace(document.forms[0].lstFormList.options[i].getAttribute('powerviewreadonly').split('|')[0], "|", "") + "|";
				}
				else {
				    s3 = s3 +"|";
				}

            }
				//s3 = s3 + replace(arr[2], "|", "") + "|";
            document.forms[0].fields.value=s;
            document.forms[0].captions.value=s1;
            document.forms[0].helps.value = s2;
            document.forms[0].ReadOnly.value = s3;
			
            s="";
            s1="";
            for(var i=0;i<document.forms[0].lstButtonList.options.length;i++)
            {
              s=s + replace(document.forms[0].lstButtonList.options[i].value,"|","") + "|";
              s1=s1 + replace(document.forms[0].lstButtonList.options[i].text,"|","") + "|";
            }
            document.forms[0].buttons.value=s;
            document.forms[0].buttoncaptions.value=s1;
            
            s="";
            s1="";
            for(var i=0;i<document.forms[0].lstToolBarList.options.length;i++)
            {
              s=s + replace(document.forms[0].lstToolBarList.options[i].value,"|","") + "|";
              s1=s1 + replace(document.forms[0].lstToolBarList.options[i].text,"|","") + "|";
            }
            document.forms[0].toolbar.value=s;
            document.forms[0].toolbarcaptions.value=s1;
            pleaseWait.Show();
            return true;
          }



		function PVFormEdit_Tab()
		{
			var s="",s1="";
			for(var i=0;i<document.forms[0].lstFormList.options.length;i++)
			{
				//s=s + replace(document.forms[0].lstFormList.options[i].value,"|","") + "|";
				s1=s1 + replace(document.forms[0].lstFormList.options[i].text,"|","") + "|";
			}
			document.forms[0].FieldCaptionList.value=s1;
			window.open('PVTabView.aspx','PVTabView',
					'width=420,height=150'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		}
		function setDataChanged(b) {return true;
		}

    </script>
</head>
<body >
    <form id="frmData" runat="server">
    <asp:textbox style="display: none" runat="server"  id="sysrequired" rmxref="Instance/Document/form/group/sysrequired"/>
        <asp:textbox style="display: none" runat="server"  id="IsBaseEditAllow" rmxref="Instance/Document/form/group/IsBaseEditAllow"/>
    <asp:textbox style="display: none" runat="server"  id="hdPvtitle" />
    <%-- akaushik5 Added for MITS 30290 Starts --%>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" rmxref="/Instance/Document/form/group/FormRequiredTabs" id="txtRequiredTabs"/>
    <%-- akaushik5 Added for MITS 30290 Ends --%>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" rmxref="/Instance/Document/form/group/CommandButtons" id="txtCommandButtons"/>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" rmxref="/Instance/Document/form/group/FormFields" id="Textbox1"/>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" rmxref="/Instance/Document/form/group/FormList" id="Textbox5"/>
    <asp:textbox style="display: none" runat="server"  rmxretainvalue="true" rmxref="/Instance/Document/form/group/ButtonList" id="Textbox4"/>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" rmxref="/Instance/Document/form/group/ToolBarButtons" id="Textbox3"/>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" rmxref="/Instance/Document/form/group/ToolBarList" id="Textbox2"/>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" rmxref="/Instance/Document/form/group/PVFormEdit/FormName" id="txtFormName"/>
    <asp:textbox style="display: none" runat="server" Text="0" rmxretainvalue="true" rmxref="/Instance/Document/form/group/PVFormEdit/Reset" id="txtReset"/>
    <asp:textbox style="display: none" runat="server"  rmxref="/Instance/Document/form/group/PVFormEdit/Fields" id="fields"/>
    <asp:textbox style="display: none" runat="server"  rmxref="/Instance/Document/form/group/PVFormEdit/Captions" id="captions"/>
    <asp:textbox style="display: none" runat="server"  rmxref="/Instance/Document/form/group/PVFormEdit/Helps" id="helps"/>
        <asp:textbox style="display: none" runat="server"  rmxref="/Instance/Document/form/group/PVFormEdit/ReadOnly" id="ReadOnly"/>
    <asp:textbox style="display: none" runat="server"  rmxref="/Instance/Document/form/group/PVFormEdit/Buttons" id="buttons"/>
    <asp:textbox style="display: none" runat="server"  rmxref="/Instance/Document/form/group/PVFormEdit/ButtonCaptions" id="buttoncaptions"/>
    <asp:textbox style="display: none" runat="server" rmxref="/Instance/Document/form/group/PVFormEdit/Toolbar" id="toolbar"/>
    <asp:textbox style="display: none" runat="server"  rmxref="/Instance/Document/form/group/PVFormEdit/ToolbarCaptions" id="toolbarcaptions"/>
    <asp:textbox style="display: none" runat="server"  id="FieldCaption"/>
    <asp:textbox style="display: none" runat="server"  id="FieldName"/>
    <asp:textbox style="display: none" runat="server"  id="HelpMsg"/>
        <asp:textbox style="display: none" runat="server"  id="BaseReadonly"/>
    <asp:textbox style="display: none" runat="server"  id="FieldReadOnly"/>
    <asp:textbox style="display: none" runat="server"  id="FieldRequired"/>
    <asp:textbox style="display: none" runat="server"  id="ControlGroup"/>
    <asp:textbox style="display: none" runat="server"  id="FieldType"/>
    <asp:textbox style="display: none" runat="server"  id="FieldDisabled"/>
    <asp:textbox style="display: none" runat="server"  id="CaptionDisabled"/>
    <asp:textbox style="display: none" runat="server"  id="FieldVisible"/>
    <asp:textbox style="display: none" runat="server"  id="FieldCaptionList"/>
    <asp:textbox style="display: none" runat="server"  id="NoCaption"/>
    <%-- akaushik5 Added for MITS 30290 Starts --%>
    <asp:textbox style="display: none" runat="server"  id="ShowRequiredTabs"/>
    <%-- akaushik5 Added for MITS 30290 Ends --%>
    <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="rowid" Text="" rmxref="Instance/Document/form/group/PVList/RowId"
                   rmxtype="hidden" />
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <uc1:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    <table width="100%" border="0">
    <%--<tr>
     <td class="msgheader" colspan="5">Power Views Setup</td>
    </tr>--%>
    <tr>
     <td class="ctrlgroup" colspan="5">Form: <asp:Label runat="server" ID="lblViewName" rmxretainvalue="true"></asp:Label></td>
    </tr>
    <tr>
     <td colspan="5">&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup2" width="44%" colspan="3">Form Fields</td>
     <td width="1%">&nbsp;</td>
     <td class="ctrlgroup2" width="55%">Command Buttons/Navigation Tree</td>
    </tr>
    <tr>
     <td valign="top" colspan="3">
      <table border="0" cellpadding="0" cellspacing="0">
       <tr>
        <td rowspan="2"><asp:DropDownList runat="server" id="cboFormFields"/>
        <br />
        <asp:listbox runat="server" id="lstFormList" Rows="20"/>
          </td>
        <td valign="top"><asp:Button UseSubmitBehavior="false" runat="server" Text="Add" class="button" onclientClick="AddField();return false;"/></td>
       </tr>
       <tr>
        <td valign="top">
       
        <asp:ImageButton ID="up1" runat="server" UseSubmitBehavior ="false" src="../../../../Images/up.gif" width="20" height="20" border="0" alt="Up" title="Move Up" OnClientClick = "MoveField(0);return false;"  />
        <asp:ImageButton ID="down1" runat="server" UseSubmitBehavior ="false" src="../../../../Images/down.gif" width="20" height="20" border="0" alt="Down" title="Move Down" OnClientClick = "MoveField(1);return false;"  />
        <asp:ImageButton ID="edit1" runat="server" UseSubmitBehavior ="false" src="../../../../Images/edit.gif" width="20" height="20" border="0" alt="Edit" title="Edit" OnClientClick = "EditField();return false;"  />
        <asp:ImageButton ID="delete1" runat="server" UseSubmitBehavior ="false" src="../../../../Images/delete3.gif" width="20" height="20" border="0" alt="Delete" title="Remove" OnClientClick = "DeleteField();return false;"  />
        </td>
       </tr>
      </table>
     </td>
     <td>&nbsp;</td>
     <td valign="top">
      <table border="0" cellpadding="0" cellspacing="0">
       <tr>
        <td rowspan="2"><asp:DropDownList runat="server" id="cboCommandButtons"/>
        <br />
        <asp:listbox runat="server" id="lstButtonList" Rows="6"/>
          <br/><asp:CheckBox name="chkOnlyTopDown" runat="server"  rmxref="/Instance/Document/form/group/OnlyTopDownLayout" appearance="full" Text="Show on Top for Top-Down Layout only"/>
         
        </td>
        <td valign="top">
        <asp:Button UseSubmitBehavior="false" runat="server" Text="Add" class="button" onclientClick="AddButton();return false;"/></td>
       </tr>
       <tr>
        <td valign="top">
        
        <asp:ImageButton ID="up2" runat="server" UseSubmitBehavior ="false" src="../../../../Images/up.gif" width="20" height="20" border="0" alt="Up" title="Move Up" OnClientClick = "MoveButton(0,document.forms[0].lstButtonList);return false;"  />
        <asp:ImageButton ID="down2" runat="server" UseSubmitBehavior ="false" src="../../../../Images/down.gif" width="20" height="20" border="0" alt="Down" title="Move Down" OnClientClick = "MoveButton(1,document.forms[0].lstButtonList);return false;"  />
        <asp:ImageButton ID="edit2" runat="server" UseSubmitBehavior ="false" src="../../../../Images/edit.gif" width="20" height="20" border="0" alt="Edit" title="Edit" OnClientClick = "EditButton();return false;"  />
        <asp:ImageButton ID="delete2" runat="server" UseSubmitBehavior ="false" src="../../../../Images/delete3.gif" width="20" height="20" border="0" alt="Delete" title="Remove" OnClientClick = "DeleteButton(document.forms[0].lstButtonList);return false;"  />
        </td>
       </tr>
      </table><br/><table border="0" cellpadding="0" cellspacing="4">
       <tr>
        <td colspan="2" class="ctrlgroup2">&nbsp;Toolbar Buttons</td>
       </tr>
       <tr>
        <td rowspan="2"><asp:DropDownList runat="server" id="cboToolBarButtons"/>
        <br />
          <asp:listbox runat="server" id="lstToolBarList" Rows="6"/>
        
          </td>
        <td valign="top"><asp:Button  UseSubmitBehavior="false" runat="server" Text="Add" class="button" onclientClick="AddToolbarButton();return false;"/></td>
       </tr>
       <tr>
        <td valign="top">
        
        <asp:ImageButton ID="up3" runat="server" UseSubmitBehavior ="false" src="../../../../Images/up.gif" width="20" height="20" border="0" alt="Up" title="Move Up" OnClientClick = "MoveButton(0,document.forms[0].lstToolBarList);return false;"  />
        <asp:ImageButton ID="down3" runat="server" UseSubmitBehavior ="false" src="../../../../Images/down.gif" width="20" height="20" border="0" alt="Down" title="Move Down" OnClientClick = "MoveButton(1,document.forms[0].lstToolBarList);return false;"  />
        <asp:ImageButton ID="edit3" runat="server" UseSubmitBehavior ="false" src="../../../../Images/edit.gif" width="20" height="20" border="0" alt="Edit" title="Edit" OnClientClick = "EditToolbar();return false;"  />
        <asp:ImageButton ID="delete3" runat="server" UseSubmitBehavior ="false" src="../../../../Images/delete3.gif" width="20" height="20" border="0" alt="Delete" title="Remove" OnClientClick = "DeleteButton(document.forms[0].lstToolBarList);return false;"  />
        </td>
       
       </tr>
       <%-- akaushik5 Added for MITS 30290 Starts--%>
       <tr>
         <% if (ShowRequiredTabs.Text == "true")
            {%>
         <td colspan="2" class="ctrlgroup2">&nbsp;Required Tabs</td>
         <%}
            else
            {%>
         <td/>
         <%} %>
         </tr>
         <tr>
         <% if (ShowRequiredTabs.Text == "true")
            {%>
        <td>
        <asp:CheckBoxList id="chkListRequiredTabs" TextAlign="Right" runat="server">
        </asp:CheckBoxList>
       </td>
       <%} else
            {%>
         <td/>
         <%} %>
       </tr>
       <%-- akaushik5 Added for MITS 30290 Ends--%>
      </table><br/></td>
    </tr>
    <tr>
     <td colspan="5"><asp:CheckBox name="chkReadOnly" runat="server" rmxref="/Instance/Document/form/group/ReadOnlyForm" appearance="full" Text="Form is Read-Only" />
      <asp:CheckBox ID="chkTopDown" name="chkTopDown" runat="server" rmxref="/Instance/Document/form/group/IsTopDownLayout" appearance="full" Text="Top-Down Layout" />
      
     </td>
    </tr>
    <tr>
     <td class="group" colspan="5"></td>
    </tr>
    <tr>
     <td colspan="5">
     <asp:Button  UseSubmitBehavior="false" runat="server" Text="Save" class="button" onclientClick="if(!onSubmitForm()) return false;" OnClick="Save"/>
     <asp:Button  runat="server" Text="Reset" class="button" id="btnReset"
             onclientClick="return Reset();" onclick="btnReset_Click"  />
     <asp:Button  UseSubmitBehavior="false" runat="server" Text="Cancel" class="button" OnClick="Cancel"/>
     <asp:Button  UseSubmitBehavior="false" runat="server" Text="Tab View" class="button" onclientClick="if(!PVFormEdit_Tab()) return false;"/>
     </td>
    </tr>
   </table>
    </form>
</body>
</html>
