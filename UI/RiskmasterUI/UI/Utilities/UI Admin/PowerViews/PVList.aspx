﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVList.aspx.cs" Inherits="Riskmaster.UI.Utilities.PVList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Power Views</title>
   <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js">
  </script>
  <script type="text/javascript">
					function DeleteView()
					{
          //if (!self.confirm("View will be permanently deleted. Are you sure?"))
          if (!self.confirm(PVListValidations.ConfirmViewDelete))
						return false;
						else
						return true;
					}
				</script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
   <table width="100%" cellspacing="0" cellpadding="2" border="0">
    <tr>
     <td class="msgheader" colspan="5">
         <asp:Label ID="lblPowerViewsSetUp" runat="server" Text="<%$ Resources:lblPowerViewsSetUp %>"></asp:Label></td>
    </tr>
    <tr>
     <td><%--<a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%269%26content%26PVDefination&amp;setvalue%26node-ids%2619%26content%2610&amp;setvalue%26node-ids%2620%26content%26FirstR5';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="Bold">FirstR5</a></td>
     <td align="right"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%269%26content%26Edit&amp;setvalue%26node-ids%2619%26content%2610&amp;setvalue%26node-ids%2620%26content%26FirstR5';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false">Edit</a></td>
     <td align="right"><a href="/oxf/home" onclick="; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%269%26content%26Delete&amp;setvalue%26node-ids%2619%26content%2610&amp;setvalue%26node-ids%2620%26content%26FirstR5';         event.returnValue=false;         if (DeleteView())          document.forms['wsrp_rewrite_form_1'].submit();         ">Delete</a></td>
     <td align="right"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%269%26content%26Clone&amp;setvalue%26node-ids%2619%26content%2610&amp;setvalue%26node-ids%2620%26content%26FirstR5';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false">Clone</a>--%>
     <asp:GridView ID="grdPowerViewList" runat="server" AllowPaging="false" AllowSorting="false" 
                              AutoGenerateColumns="False" 
                           Width="100%" GridLines="None"  EnableViewState="true"
                                >
                            
                            <PagerSettings FirstPageText="&quot;First&quot;" LastPageText="Last" Mode="NextPreviousFirstLast" 
                                Position="Top" NextPageText="Next" />
                             <%--<HeaderStyle CssClass="msgheader" />--%>
                          <%-- <AlternatingRowStyle CssClass="data2" /> --%>
                            <Columns>
                                <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    <table>
                                    <tr>
                                      <td>
                                      <a runat="server" id="pViewTitle" rowid='<%# DataBinder.Eval(Container, "DataItem.value")%>' pvname='<%# DataBinder.Eval(Container, "DataItem.title")%>' class ="data" onserverclick="GoToDefinition"><b><u><%# DataBinder.Eval(Container, "DataItem.title")%></u></b></a>
                                      </td>
                                      </tr>
                                      <tr>
                                      <td>
                                      <span id="spanDesc" class ="data"><%# DataBinder.Eval(Container, "DataItem.desc")%></span>
                                      
                                      </td>
                                      </tr>
                                      </table>
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    <table>
                                    <tr>
                                      <td>
                                      <a runat="server" id="Edit" rowid='<%# DataBinder.Eval(Container, "DataItem.value")%>' pvname='<%# DataBinder.Eval(Container, "DataItem.title")%>' class ="data" title="<%$ Resources:ttEdit %>" onserverclick = "Edit"><u><asp:Label ID="lblEdit" runat="server" Text="<%$ Resources:lblEdit %>"></asp:Label></u></a>
                                      </td>
                                      </tr>
                                      <tr>
                                      <td>
                                      
                                      </td>
                                      </tr>
                                      </table>
                                    </ItemTemplate> 
                                </asp:TemplateField> <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    <table>
                                    <tr>
                                      <td>
                                     <a runat="server" id="Delete"  rowid='<%# DataBinder.Eval(Container, "DataItem.value")%>'  pvname='<%# DataBinder.Eval(Container, "DataItem.title")%>' title = "<%$ Resources:ttDelete %>" class ="data" onclick="if(!DeleteView()) return false;" onserverclick = "Delete"><u><asp:Label ID="lblDelete" runat="server" Text="<%$ Resources:lblDelete %>"></asp:Label></u></a>
                                      </td>
                                      </tr>
                                      <tr>
                                      <td>
                                      
                                      </td>
                                      </tr>
                                      </table>
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    <table>
                                    <tr>
                                      <td>
                                     <a runat="server" rowid='<%# DataBinder.Eval(Container, "DataItem.value")%>'  pvname='<%# DataBinder.Eval(Container, "DataItem.title")%>' id="Clone" class ="data" title="<%$ Resources:ttClone %>" onserverclick = "Clone"><u><asp:Label ID="lblClone" runat="server" Text="<%$ Resources:lblClone %>"></asp:Label></u></a>
                                      </td>
                                      </tr>
                                      <tr>
                                      <td>
                                      
                                      </td>
                                      </tr>
                                      </table>
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                 <asp:TemplateField  ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                    <table>
                                    <tr>
                                      <td>
                                     <input id="Hidden1" type="hidden" runat="server"><%# DataBinder.Eval(Container, "DataItem.value")%></input>
                                      </td>
                                      </tr>
                                      <tr>
                                      <td>
                                      
                                      </td>
                                      </tr>
                                      </table>
                                    </ItemTemplate> 
                                </asp:TemplateField> 
                                
                                
                            </Columns>
                        </asp:GridView>
     
     </td>
    </tr>
    <%--<tr>
     <td colspan="4" class="small">R5 first Power View</td>
    </tr>--%>
    <tr>
     <td class="group" colspan="4"></td>
    </tr>
    <tr>
     <td colspan="4"><%--input type="submit" name="$action^setvalue%26node-ids%269%26content%26CreateNew" value="Create New" class="button">--%>
     <asp:button ID="btnCreateNew" runat="server" class="button" Text="<%$ Resources:btnCreateNew %>" OnClick="CreateNew" />
     
     </td>
    </tr>
   </table>
   <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="viewList" Text="ViewList" rmxref="Instance/Document/form/group/displaycolumn/control/@name"
                   rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="pvname" Text="" rmxref="Instance/Document/form/group/PVList/PVName"
                   rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="rowid" Text="" rmxref="Instance/Document/form/group/PVList/RowId"
                   rmxtype="hidden" />
    </form>
</body>
</html>
