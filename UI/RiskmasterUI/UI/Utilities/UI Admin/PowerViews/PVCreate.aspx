﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="PVCreate.aspx.cs" Inherits="Riskmaster.UI.Utilities.PVCreate" %>

<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Power Views</title>
   <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
   <script type="text/javascript" language="javascript" src="../../../../Scripts/WaitDialog.js"></script>
  <script type="text/javascript">
    
	  function replace(sSource, sSearchFor, sReplaceWith)
      {
        var arr = new Array();
        arr=sSource.split(sSearchFor);
        return arr.join(sReplaceWith);
      }
      function Validate()
      {
        if(replace(document.forms[0].txtViewName.value," ","")=="")
        {
          alert("Please enter view name.");
          document.forms[0].txtViewName.focus();
          return false;
        }
        else if (document.forms[0].language.options[document.forms[0].language.selectedIndex].value != "1033" && document.forms[0].chkClone.checked == true) {
            alert("This will take few minutes to complete!");
            pleaseWait.Show();
            return true;
        }
        else {
            pleaseWait.Show();

            return true;
        }
      }
      function OnHomePageClick()
      {
          if (document.forms[0].cboHomePage.options[document.forms[0].cboHomePage.selectedIndex].value == "custom") {
              document.forms[0].txtHomePage.value = "";
              document.forms[0].hdnHomeSysName.value = "";
              document.forms[0].txtHomePage.disabled = false;
              document.forms[0].txtHome.value = document.forms[0].cboHomePage.options[document.forms[0].cboHomePage.selectedIndex].text;
          }
          else
          {
              document.forms[0].txtHomePage.disabled = true;
              document.forms[0].txtHomePage.value = document.forms[0].cboHomePage.options[document.forms[0].cboHomePage.selectedIndex].value;
              document.forms[0].txtPageMenu.value = document.forms[0].cboHomePage.options[document.forms[0].cboHomePage.selectedIndex].pagemenu;
              document.forms[0].hdnHomeSysName.value = document.forms[0].cboHomePage.options[document.forms[0].cboHomePage.selectedIndex].getAttribute("homesysname");
              document.forms[0].txtHome.value = document.forms[0].cboHomePage.options[document.forms[0].cboHomePage.selectedIndex].text;
          }
         
      }
      function pageLoaded()
      {
     
        var iCustom=0;

        //changed by Nitin ,in order to select correct cbohomepage option 
        //on the basis of homesysname value ,which is unique
        //select option that = the home page, or custom
        var sHomePage = document.forms[0].hdnHomeSysName.value;
        //ugly hack to redirect to admquickentrymenu.asp
        if(sHomePage=="admmenu.asp")
          sHomePage="admquickentrymenu.asp"
    
        var iLength = document.forms[0].cboHomePage.length;
        for(var i=0;i<iLength;i++){
            if (sHomePage == document.forms[0].cboHomePage.options[i].getAttribute("homesysname")) {
              document.forms[0].cboHomePage.options[i].selected = true;
              if (document.forms[0].cboHomePage.options[i].value == "custom") {
                  document.forms[0].txtHomePage.disabled = false;
              }
              return false;
          }
          //identify ordinal position of the custom home page
          if(document.forms[0].cboHomePage.options[i].value=="custom")
            iCustom=i;
        }
        
        //if none selected is a custom home page
        document.forms[0].cboHomePage.options[iCustom].selected = true;
        document.forms[0].txtHomePage.disabled = false;
        OnHomePageClick();//Added by Shivendu for MITS 17815
      }
    </script>
</head>
<body onload="pageLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
   <table width="100%" cellspacing="0" cellpadding="2" border="0">
    <%--<tr>
     <td class="msgheader" colspan="2">Power Views Setup</td>
    </tr>--%>
    <tr>
     <td class="ctrlgroup" colspan="2">View Detail</td>
    </tr>
    <tr>
     <td nowrap="1" width="15%" class="required"><label>View Name:&nbsp;</label></td>
     <td width="*"><asp:TextBox runat="server" id="txtViewName" rmxref="/Instance/Document/form/group/PVCreate/ViewName"/></td>
    </tr>
    <tr>
     <td nowrap="1"><label>View Description:&nbsp;</label></td>
     <td><asp:TextBox runat="server" id="txtViewDesc" rmxref="/Instance/Document/form/group/PVCreate/ViewDesc"/></td>
    </tr>
    <tr>
     <td class="group" height="4" colspan="2"></td>
    </tr>
    <tr>
     <td nowrap="1"><label>Home Page:&nbsp;</label></td>
     <td><asp:dropdownlist runat="server" id="cboHomePage" pagemenu="" onchange="OnHomePageClick();"/>
      </td>
    </tr>
    <tr style="display:none;">
     <td nowrap="1"><label>Home page URL:&nbsp;</label></td>
     <td><asp:TextBox  runat="server" id="txtHomePage" size="60" rmxref="/Instance/Document/form/group/PVCreate/HomePageURL" /></td>
    </tr>
     <tr>
     <td nowrap="1"><label>Language:&nbsp;</label></td>
     <td>
     <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='language']/@value" id="language" itemsetref="/Instance/Document/form/group/displaycolumn/control[ @name = 'language' ]"/>
     </td>
    </tr>
    <tr>
     <td nowrap="1"><asp:CheckBox runat="server"  Text = "Clone Base View" appearance="full" rmxref="/Instance/Document/form/group/PVCreate/CloneBaseView" id="chkClone"/></td>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td class="group" colspan="2"></td>
    </tr>
    <tr>
     <td align="center" colspan="2"><asp:Button runat="server"  Text="Save" class="button" onclientClick="return Validate();" OnClick="Save"/>&nbsp;
      <asp:Button runat="server"  Text="Cancel" class="button" onClick="GoToPVList"/>
     </td>
    </tr>
   </table>
   <asp:HiddenField ID="hdnHomeSysName" runat="server" Value="" />
    <asp:textbox style="display: none" runat="server" id="rowid" Text="" rmxref="Instance/Document/form/group/PVList/RowId"
                   rmxtype="hidden" />
                   <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="viewList" Text="HomePage" rmxref="Instance/Document/form/group/displaycolumn/control/@name"
                   rmxtype="hidden" />
                   <asp:textbox style="display: none" runat="server" id="txtPageMenu" Text="" rmxref="Instance/Document/form/group/PVCreate/PageMenu"
                    />
                    <asp:textbox style="display: none" runat="server" id="txtHome" Text="" rmxref="Instance/Document/form/group/PVCreate/HomePage"
                    />
    <uc1:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
