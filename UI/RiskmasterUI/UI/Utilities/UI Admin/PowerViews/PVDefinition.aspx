﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVDefinition.aspx.cs" Inherits="Riskmaster.UI.Utilities.PVDefinition" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Power Views</title>
    <script type="text/javascript">
    var m_DataChanged=false;
    document.Cancel=Cancel;
    
    function pageLoaded()
    {    
      //tkr 1/2003 check if user action was interrupted by "do you want to save your changes" prompt
      //sysCmdQueue, if it exists, is the interrupted function call
      var obj=eval("document.forms[0].sysCmdQueue");
      if(obj!=null){
        var s = new String(document.forms[0].sysCmdQueue.value);
        if(s.length>0){
          s='document.'+s;
          document.forms[0].sysCmdQueue.value='';
          eval(s);
          return true;
        }
      }
    }
    
    function ConfirmSave()
    {
      var ret = false;
      if(m_DataChanged){
        ret = confirm('Data has changed. Do you want to save changes?');
      }
      return ret;
    }
    
    function DeleteView(lViewId)
    {
      if(!self.confirm("View will be permanently deleted. Are you sure?"))
        return;
      document.location="deleteview.asp?viewid=" + lViewId;
    }
    
    function Cancel()
    {
      if(ConfirmSave()){
        //document.forms[0].sysCmdQueue.value='Cancel();';
        document.forms[0].txtSave.value = "Save";
        OnSave();
        return true;
      }	
      //document.location='views.asp';
      return true;
    }
    
    function AddForm()
    {
      if(document.forms[0].cboAvailForms.selectedIndex < 0 ) 
      {
        return false;
      }
      var sValue=document.forms[0].cboAvailForms.options[document.forms[0].cboAvailForms.selectedIndex].value;
      var sText=document.forms[0].cboAvailForms.options[document.forms[0].cboAvailForms.selectedIndex].text;
        
	if(sValue=="1")
		return false;
      for(var f=0;f<document.forms[0].lstFormList.options.length;f++)
        if(document.forms[0].lstFormList.options[f].value==sValue)
          return false;
          
      var opt=new Option(sText, sValue, false, false);
      document.forms[0].lstFormList.options[document.forms[0].lstFormList.options.length]=opt;
      document.forms[0].formname.value=sValue;      
      document.forms[0].SelectedForms.value="";
      document.forms[0].SelectedFormsCaption.value="";
      document.forms[0].SelectedUsers.value="";
      var sForms="", sFormsCaptions="", sUsers="";
      for(var i=0;i<document.forms[0].lstFormList.options.length;i++)
      {
        sForms=sForms+document.forms[0].lstFormList.options[i].value+"|";
        sFormsCaptions=sFormsCaptions+document.forms[0].lstFormList.options[i].text+"|";
      }
      
      for(var i=0;i<document.forms[0].lstUserList.options.length;i++)
        sUsers=sUsers+document.forms[0].lstUserList.options[i].value+"|";
        
      //alert(sForms + "\n\n" + sUsers);
      
      document.forms[0].SelectedForms.value=sForms;
      document.forms[0].SelectedFormsCaption.value=sFormsCaptions;
      document.forms[0].SelectedUsers.value=sUsers;
	m_DataChanged=true;
      //document.forms[0].submit();
      return false;
    }
    
    function AddUserGroup()
    {
      if(document.forms[0].cboAvialUser.selectedIndex<0)
        return false;
      var sValue=document.forms[0].cboAvialUser.options[document.forms[0].cboAvialUser.selectedIndex].value;
      var sText=document.forms[0].cboAvialUser.options[document.forms[0].cboAvialUser.selectedIndex].text;
      
      for(var f=0;f<document.forms[0].lstUserList.options.length;f++)
        if(document.forms[0].lstUserList.options[f].value==sValue)
          return false;
          
      var opt=new Option(sText, sValue, false, false);
      document.forms[0].lstUserList.options[document.forms[0].lstUserList.options.length]=opt;
      m_DataChanged=true;
      return true;
    }
    
    function DeleteUserGroup()
    {
        if (document.forms[0].lstUserList.selectedIndex < 0)
        {
            alert("Please select the User/Group you would like to delete."); //abansal23:  MITS 14347
            return false;
        }
    
      document.forms[0].lstUserList.options[document.forms[0].lstUserList.selectedIndex]=null;
      m_DataChanged=true;
    }
    
    function DeleteForm()
    {
        if (document.forms[0].lstFormList.selectedIndex < 0)
         {
             alert("Please select the form you would like to delete."); //abansal23:  MITS 14347
            return false;
        }
        
      
      document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex]=null;
      m_DataChanged=true;
    }
    
    function EditForm()
    {
      if(m_DataChanged)
      {
        alert("Data has changed. Please save data and try again.");
        return false;
      }
      
      if(document.forms[0].lstFormList.selectedIndex<0)
      {
        alert("Please select the form you would like to edit.");
        return false;
      }
      
      var s=document.forms[0].lstFormList.options[document.forms[0].lstFormList.selectedIndex].value;
      if(s=="")
      {
        alert("Please select the form you would like to edit.");
        return false;
      }
      return true;
      
    }
    
    //^Rajeev 12/10/2002 MITS#3899 -- onSubmitForm() replaced by OnSave()
    
    //function onSubmitForm()
    function OnSave()
    {
      document.forms[0].SelectedForms.value="";
      document.forms[0].SelectedFormsCaption.value="";
      document.forms[0].SelectedUsers.value="";
      var sForms="", sFormsCaptions="", sUsers="";
      for(var i=0;i<document.forms[0].lstFormList.options.length;i++)
      {
        sForms=sForms+document.forms[0].lstFormList.options[i].value+"|";
        sFormsCaptions=sFormsCaptions+document.forms[0].lstFormList.options[i].text+"|";
      }
      
      for(var i=0;i<document.forms[0].lstUserList.options.length;i++)
        sUsers=sUsers+document.forms[0].lstUserList.options[i].value+"|";
        
      //alert(sForms + "\n\n" + sUsers);
      
      document.forms[0].SelectedForms.value=sForms;
      document.forms[0].SelectedFormsCaption.value=sFormsCaptions;
      document.forms[0].SelectedUsers.value=sUsers;
      //document.forms[0].submit();
      
      return true;
    }
    //^^Rajeev 12/10/2002 MITS#3899 -- onSubmitForm() replaced by OnSave()
  </script>
</head>
<body>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:textbox style="display: none" runat="server"  id="SelectedForms" Text="" rmxref="Instance/Document/form/group/PVDefination/SelectedForms"
                   rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="SelectedFormsCaption" Text="" rmxref="Instance/Document/form/group/PVDefination/SelectedFormsCaption"
                   rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="SelectedUsers" Text="" rmxref="Instance/Document/form/group/PVDefination/SelectedUsers"
                   rmxtype="hidden" />
                   <asp:textbox style="display: none" runat="server" id="formname" Text="" rmxref="Instance/Document/form/group/PVDefination/formname"
                   rmxtype="hidden" />
                   <asp:textbox style="display: none" runat="server" id="rowid" Text="" rmxref="Instance/Document/form/group/PVList/RowId"
                   rmxtype="hidden" />
                   <asp:textbox style="display: none" runat="server"  id="txtSave" Text="" 
                   rmxtype="hidden" />
   <table width="100%" cellspacing="0" cellpadding="2" border="0">
     
    <%--<tr>
     <td class="msgheader" colspan="5">Power Views Setup</td>
    </tr>--%>
    <tr>
     <td class="ctrlgroup" colspan="5">View definition for: <asp:Label runat="server" ID="lblViewName" rmxretainvalue="true"></asp:Label></td>
    </tr>
    <tr>
     <td width="20%">Available Forms:</td>
     <td width="20%" align="left"><font size="-2">* signifies top-level forms</font></td>
     <td width="20%">&nbsp;</td>
     <td width="20%">Available User/Groups:</td>
     <td width="20%"><font size="-2">* signifies user group</font></td>
    </tr>
    <tr>
     <td nowrap="1" colspan="2"><asp:DropDownList runat="server" id="cboAvailForms"/>
       <asp:Button UseSubmitBehavior="false" runat="server" Text="Add" class="button" onclientClick="AddForm();return false;"/></td>
     <td width="16">&nbsp;</td>
     <td nowrap="1" colspan="2"><asp:DropDownList runat="server" id="cboAvialUser"/>
       <asp:Button  UseSubmitBehavior="false" runat="server" Text="Add" class="button" onclientClick="AddUserGroup();return false;"/></td>
    </tr>
    <tr>
     <td><asp:listbox runat="server" id="lstFormList" Rows="10"/>
       </td>
     <td valign="center" nowrap="1">
     <asp:ImageButton ID="imgEdit1" runat="server" UseSubmitBehavior ="false" src="../../../../Images/edit.gif"  alt="" title="Edit Form Definition" OnClientClick = "if(!EditForm()) return false;" onclick="GoToFormEdit" />
     <asp:ImageButton ID="imgDelete1" runat="server" UseSubmitBehavior ="false" src="../../../../Images/delete3.gif" width="20" height="20" border="0" alt="Delete" title="Delete Form Definition" OnClientClick = "DeleteForm();return false;"  />
     </td>
     <td width="16">&nbsp;</td>
     <td align="left"><asp:listbox runat="server" id="lstUserList"  Rows="10"/></td>
     <td valign="center" nowrap="1">
      <asp:ImageButton ID="imageDelete2" runat="server" UseSubmitBehavior ="false" src="../../../../Images/delete3.gif" width="20" height="20" border="0" alt="Delete" title="Remove User/Group" OnClientClick = "DeleteUserGroup();return false;"  />
     </td>
    </tr>
    <tr>
     <td class="group" colspan="5"></td>
    </tr>
    <tr>
     <td colspan="5"><asp:Button runat="server" UseSubmitBehavior="false" Text="Save" class="button" onclientClick="if(! OnSave()) return false" OnClick = "Save"/>
     <asp:Button runat="server"  UseSubmitBehavior="false" Text="Back to Views" class="button" onclientClick="if(! Cancel()) return false;" OnClick = "GoToPVList"/>
     </td>
    </tr>
   </table>
     <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="viewName" Text="" rmxref="Instance/Document/form/group/displaycolumn/control/ViewName"
                   rmxtype="hidden" />
                     <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="availForms" Text="" rmxref="Instance/Document/form/group/displaycolumn/control/AvailForms"
                   rmxtype="hidden" />
                     <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="formList" Text="" rmxref="Instance/Document/form/group/displaycolumn/control/FormList"
                   rmxtype="hidden" />
                     <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="availUsers" Text="" rmxref="Instance/Document/form/group/displaycolumn/control/AvialUser"
                   rmxtype="hidden" />
                     <asp:textbox style="display: none" runat="server" rmxretainvalue="true" id="userList" Text="" rmxref="Instance/Document/form/group/displaycolumn/control/UserList"
                   rmxtype="hidden" />
    </form>
</body>
</html>
