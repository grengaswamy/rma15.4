<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizeSettings.aspx.cs" Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.CustomizeSettings" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head runat="server">
  <title>System Customization (Settings)</title>
  <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
  <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>
 </head>
 <body class="10pt" onload="parent.MDIScreenLoaded();">
  <script language="javascript" type="text/javascript">
				
					function openEditWindow(defaulttext,controlname)
					{
					    var defaultvalue=document.getElementById(defaulttext).value;
						var currentvalue=document.getElementById(controlname).value;
						
						m_codeWindow=window.open('../Customization/CustomizeEditWindow.aspx?defaulttext='+defaulttext+'&controlname='+controlname+'&defaultvalue='+defaultvalue+'&currentvalue='+currentvalue,'CustomizeEditWindow',
									'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=no,scrollbars=no');
					}
					
					function Validation()
					{
					 					    
					    if( IsNumeric(document.getElementById("TextMlWidth_hidden").value) == false)
					    {
					        alert("Field : TextMI Width can contain only numerals");
					        return false;
					    }
					    if( IsNumeric(document.getElementById("TextMlHeight_hidden").value) == false)
					    {
					        alert("Field : TextMI Height can contain only numerals");
					        return false;
					    }
					    if( IsNumeric(document.getElementById("FreeCodeWidth_hidden").value) == false)
					    {
					        alert("Field : FreeCode Width can contain only numerals");
					        return false;
					    }
					    if( IsNumeric(document.getElementById("FreeCodeHeight_hidden").value) == false)
					    {
					        alert("Field : FreeCode Height can contain only numerals");
					        return false;
					    }
					    if( IsNumeric(document.getElementById("ReadOnlyMemoWidth_hidden").value) == false)
					    {
					        alert("Field : ReadOnlyMemo Width can contain only numerals");
					        return false;
					    }
					    if( IsNumeric(document.getElementById("ReadOnlyMemoHeight_hidden").value) == false)
					    {
					        alert("Field : ReadOnlyMemo Height can contain only numerals");
					        return false;
					    }
					    if( IsNumeric(document.getElementById("MemoWidth_hidden").value) == false)
					    {
					        alert("Field : Memo Width can contain only numerals");
					        return false;
					    }
					    if( IsNumeric(document.getElementById("MemoHeight_hidden").value) == false)
					    {
					        alert("Field : Memo Height can contain only numerals");
					        return false;
					    }
					    if (IsNumeric(document.getElementById("HtmlTextWidth_hidden").value) == false) {
					        alert("Field : HTML Text Width can contain only numerals");
					        return false;
					    }
					    if (IsNumeric(document.getElementById("HtmlTextHeight_hidden").value) == false) {
					        alert("Field : HTML Text Height can contain only numerals");
					        return false;
					    }
					    
					    return true;
					}
					function IsNumeric(strString)
                        //  check for valid numeric strings	
                    {
                        var strValidChars = "0123456789";
                        var strChar;
                        var blnResult = true;

                        
                        //  test strString consists of valid characters listed above
                        for (i = 0; i < strString.length && blnResult == true; i++)
                        {
                            strChar = strString.charAt(i);
                            if (strValidChars.indexOf(strChar) == -1)
                            {
                                blnResult = false;
                            }
                        }
                            return blnResult;
                    }

  </script>
  <form id="frmData" name="frmData" method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <div class="msgheader">System Customization</div>
   <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr><td><br /></td></tr>
    <tr>
     <td width="100%">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
       <tr>
        <td colspan="3" class="ctrlgroup">&nbsp;Text Area Size</td>
       </tr>
       <tr>
           
        <td width="30%" class="datatd" runat="server" title="<%$ Resources:tttextml %> "  ><b>TextMl</b></td>
        <td width="30%" class="datatd">Width(Cols):
         <%  TextMlWidth.Text = TextMlWidth_hidden.Text;
             TextMlWidth.OnClientClick = "openEditWindow('" + TextMlWidth_default_hidden.ClientID + "','" + TextMlWidth_hidden.ClientID + "');"; %>   
         <asp:LinkButton runat="server" id="TextMlWidth" class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/TextML/Width" id="TextMlWidth_hidden"/>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/TextML/Width/@default" id="TextMlWidth_default_hidden"/>
        </td>
        <td width="30%" class="datatd">Height(Rows):
         <%  TextMlHeight.Text = TextMlHeight_hidden.Text;
             TextMlHeight.OnClientClick = "openEditWindow('" + TextMlHeight_default_hidden.ClientID + "','" + TextMlHeight_hidden.ClientID + "');";%>
         <asp:LinkButton id="TextMlHeight" runat="server" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/TextML/Height"  id="TextMlHeight_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/TextML/Height/@default"  id="TextMlHeight_default_hidden" />   
       </tr>
       <tr>
        <td width="30%" class="datatd1" runat="server" title="<%$ Resources:ttfreetext %> " ><b>FreeCode</b></td>
        <td width="30%" class="datatd1">Width(Cols):
         <% FreeCodeWidth.Text = FreeCodeWidth_hidden.Text;
            FreeCodeWidth.OnClientClick = "openEditWindow('" + FreeCodeWidth_default_hidden.ClientID + "','" + FreeCodeWidth_hidden.ClientID + "');";%>   
         <asp:LinkButton id="FreeCodeWidth"  runat="server" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/FreeCode/Width" id="FreeCodeWidth_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/FreeCode/Width/@default" id="FreeCodeWidth_default_hidden" />
        <td width="30%" class="datatd1">Height(Rows):
         <%  FreeCodeHeight.Text = FreeCodeHeight_hidden.Text;
             FreeCodeHeight.OnClientClick = "openEditWindow('" + FreeCodeHeight_default_hidden.ClientID + "','" + FreeCodeHeight_hidden.ClientID + "');";%>   
         <asp:LinkButton id="FreeCodeHeight" runat="server" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/FreeCode/Height" id="FreeCodeHeight_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/FreeCode/Height/@default" id="FreeCodeHeight_default_hidden" />
       </tr>
       <tr>
        <td width="30%" class="datatd" runat="server" title="<%$ Resources:ttreadonlymemo %> "><b>Readonly Memo</b></td>
        <td width="30%" class="datatd">Width(Cols):
         <% ReadOnlyMemoWidth.Text = ReadOnlyMemoWidth_hidden.Text;
            ReadOnlyMemoWidth.OnClientClick = "openEditWindow('" + ReadOnlyMemoWidth_default_hidden.ClientID + "','" + ReadOnlyMemoWidth_hidden.ClientID + "');";%>   
         <asp:LinkButton  runat="server" id="ReadOnlyMemoWidth" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/ReadOnlyMemo/Width" id="ReadOnlyMemoWidth_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/ReadOnlyMemo/Width/@default" id="ReadOnlyMemoWidth_default_hidden" />
        <td width="30%" class="datatd">Height(Rows):
         <%  ReadOnlyMemoHeight.Text = ReadOnlyMemoHeight_hidden.Text;
             ReadOnlyMemoHeight.OnClientClick = "openEditWindow('" + ReadOnlyMemoHeight_default_hidden.ClientID + "','" + ReadOnlyMemoHeight_hidden.ClientID + "');";%>      
         <asp:LinkButton runat="server" id="ReadOnlyMemoHeight" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/ReadOnlyMemo/Height" id="ReadOnlyMemoHeight_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/ReadOnlyMemo/Height/@default" id="ReadOnlyMemoHeight_default_hidden" />
       </tr>
       <tr>
        <td width="30%" class="datatd1" runat="server" title="<%$ Resources:ttmemo %> "><b>Memo</b></td>
        <td width="30%" class="datatd1">Width(Cols):
         <%  MemoWidth.Text = MemoWidth_hidden.Text;
             MemoWidth.OnClientClick = "openEditWindow('" + MemoWidth_default_hidden.ClientID + "','" + MemoWidth_hidden.ClientID + "');";%>      
         <asp:LinkButton runat="server" id="MemoWidth" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/Memo/Width" id="MemoWidth_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/Memo/Width/@default" id="MemoWidth_default_hidden" />
        <td width="30%" class="datatd1">Height(Rows):
         <% MemoHeight.Text = MemoHeight_hidden.Text;
            MemoHeight.OnClientClick = "openEditWindow('" + MemoHeight_default_hidden.ClientID + "','" + MemoHeight_hidden.ClientID + "');";%>      
         <asp:LinkButton runat="server" id="MemoHeight" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/Memo/Height" id="MemoHeight_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/Memo/Height/@default" id="MemoHeight_default_hidden" />
       </tr>
       <tr>
        <td width="30%" class="datatd" runat="server" title="<%$ Resources:tthtmltext %> "><b>HTML Text</b></td>
        <td width="30%" class="datatd">Width(Cols):
         <%  HtmlWidth.Text = HtmlTextWidth_hidden.Text;
             HtmlWidth.OnClientClick = "openEditWindow('" + HtmlText_default_hidden.ClientID + "','" + HtmlTextWidth_hidden.ClientID + "');";%>      
         <asp:LinkButton runat="server" id="HtmlWidth" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/HtmlText/Width" id="HtmlTextWidth_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/HtmlText/Width/@default" id="HtmlText_default_hidden" />
        <td width="30%" class="datatd">Height(Rows):
         <% HtmlHeight.Text = HtmlTextHeight_hidden.Text;
            HtmlHeight.OnClientClick = "openEditWindow('" + HtmlTextHeight_default_hidden.ClientID + "','" + HtmlTextHeight_hidden.ClientID + "');";%>      
         <asp:LinkButton runat="server" id="HtmlHeight" class="LinkBoldBlue" Font-Underline="true" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/HtmlText/Height" id="HtmlTextHeight_hidden" /></td>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_settings/RMAdminSettings/TextAreaSize/HtmlText/Height/@default" id="HtmlTextHeight_default_hidden" />
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td width="100%">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
       <tr>
        <td colspan="2" class="ctrlgroup">&nbsp;Buttons</td>
       </tr>
       <tr>
        <td width="60%"></td>
        <td width="40%"><i>Show</i></td>
       </tr>
       <tr>
        <td class="datatd">Claims &amp; Events</td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" ID="Claims" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Buttons/Document" appearance="full" /></td>
       </tr>
       <tr>
        <td class="datatd1">Search</td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" ID="Search" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Buttons/Search" appearance="full" /></td>
       </tr>
       <tr>
        <td class="datatd">Document Management</td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" ID="File" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Buttons/File" appearance="full" /></td>
       </tr>
       <tr>
        <td class="datatd1">Diaries</td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" ID="Diary" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Buttons/Diary" appearance="full" /></td>
       </tr>
       <tr>
        <td class="datatd">Reports</td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" ID="Report" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Buttons/Report" appearance="full" /></td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup" width="100%">&nbsp;Other Settings</td>
    </tr>
    <tr>
     <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" ID="Soundex" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Other/Soundex" appearance="full" />Choose Soundex on Searches</td>
    </tr>
    <tr>
     <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" ID="ShowName" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Other/ShowName" appearance="full" />Show User Name on Menu</td>
    </tr>
    <tr>
     <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" ID="ShowLogin" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Other/ShowLogin" appearance="full" />Show Login on Menu</td>
    </tr>
    <tr>
     <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" ID="SaveShowActiveDiary" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Other/SaveShowActiveDiary" appearance="full" />Save Show Active Diary Settings</td>
    </tr>
    <tr>
     <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
     <td width="100%">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
       <tr>
        <td class="ctrlgroup" width="25%">&nbsp;Funds Page Settings</td>
        <td width="75%">&nbsp;</td>
       </tr>
       <tr>
        <td width="25%">
         <table>
          <tr>
           <td>&nbsp;</td>
           <td><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Show</i></td>
          </tr>
         </table>
        </td>
        <td width="75%">&nbsp;</td>
       </tr>
       <tr>
        <td width="25%">
         <table>
          <tr>
           <td>OFAC Check:</td>
           <td><asp:CheckBox FormType="Customize" runat="server" ID="OFAC" type="checkbox" rmxref="Instance/Document/customize_settings/RMAdminSettings/Funds/OFAC" appearance="full" /></td>
          </tr>
         </table>
        </td>
        <td width="75%">&nbsp;</td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2">
     <asp:Button runat="server" ID="btnSave" type="submit" OnClientClick="return Validation();" OnClick="Save" Text="Save" class="button" style="width:100"/>  
     <asp:Button runat="server" ID="btnRefresh"  type="submit" OnClick="Refresh" Text="Refresh" class="button" style="width:100"/>
     </td>
    </tr>
   </table>
  </form>
 </body>
</html>
