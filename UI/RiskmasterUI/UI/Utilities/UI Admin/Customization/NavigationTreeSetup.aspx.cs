﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class NavigationTreeSetup : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim. START
            string sreturnValue = "";
            bool bReturnStatus = false;
            XmlDocument objXmlDocument = new XmlDocument();
            XmlElement xmlMultiCovgPerClm = null;
            XmlElement Policy = null;
            XmlElement Adjuster = null;
            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim. END
            if (!IsPostBack)
            {
                try
                {
                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim. START
                    //CallCWSFunction("NavigationTreeCustomizationAdaptor.GetCustomization");
                    bReturnStatus = CallCWSFunction("NavigationTreeCustomizationAdaptor.GetCustomization", out sreturnValue);
                    objXmlDocument.LoadXml(sreturnValue);
                    xmlMultiCovgPerClm = (XmlElement)objXmlDocument.SelectSingleNode("/ResultMessage/Document/ADJCustomization/CheckCarrierClaim");
                    Policy = (XmlElement)objXmlDocument.SelectSingleNode("/ResultMessage/Document/ADJCustomization/PolicyExpansion");
                    Adjuster = (XmlElement)objXmlDocument.SelectSingleNode("/ResultMessage/Document/ADJCustomization/AdjusterExpansion");
                    if (Policy != null)
                    {
                        if (Policy.InnerText.Trim() == "0")
                        {
                            chkPolicyExpansion.Checked = false;
                        }
                        else
                        {
                            chkPolicyExpansion.Checked = true;
                        }
                    }

                    if (Adjuster != null)
                    {
                        if (Adjuster.InnerText.Trim() == "0")
                        {
                            chkAdjusterExpansion.Checked = false;
                        }
                        else
                        {
                            chkAdjusterExpansion.Checked = true;
                        }
                    }
                    if (xmlMultiCovgPerClm != null)
                    {
                        if (xmlMultiCovgPerClm.InnerText.Trim() == "False")
                        {
                            divPolicyExpansion.Style.Add("display", "none");
                        }
                        else
                        {
                            divPolicyExpansion.Style.Add("display", "block");
                        }
                    }
                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim. END
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
        }

        protected void NavigateSave(object sender, ImageClickEventArgs e)
        {
            try
            {
                CallCWSFunction("NavigationTreeCustomizationAdaptor.SaveCustomization");
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}