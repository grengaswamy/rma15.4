﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyPortletInfo.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.ModifyPortletInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modify Portlet Information</title>
    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>

    <script language="javaScript" src="../../../../Scripts/Utilities.js" type="text/javascript"></script>

    <script language="javaScript" src="../../../../Scripts/grid.js" type="text/javascript"></script>

    <script language="javaScript" src="../../../../Scripts/supportscreens.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</head>
<body onload="checkWindow();CopyGridRowDataToPopup();">
    <form id="frmData" runat="server" formtype="GridPopup">
     <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <b><u>Name of Portlet</u></b>
            </td>
            <td>
                <asp:TextBox runat="server" value="" size="50" id="Name" rmxref="//Name" />
            </td>
        </tr>
        <tr id="tdUrlOfPortlet">
            <td>
                <b><u>URL Of Portlet</u></b>
            </td>
            <td>
                <asp:TextBox runat="server" value="" size="50" id="urlPortlet" rmxref="//urlPortlet" />
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td style="display: none;">
                <input type="checkbox" name="$node^50" value="true" ref="/Instance/Document/SelectAllUsersFlag"
                    appearance="full" id="chkAllUsers" readonly="false" relevant="true" required="false" />Apply
                to All Users
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2"><%-- aravi5 Modified for Jira Id: 7240 RMA Header Tab is not in proper design of rma standard in IE11 Full Mode --%>
                <asp:Button runat="server" ID="btnOk" Text="OK" class="button" UseSubmitBehavior="false" Style="width: 60px"
                    OnClick="btnOk_Click" />
                <%--<input type="submit" value="OK" class="button" onclick="Javascript:return Save();" style="width:60px"/>--%>
                &nbsp;
                <%--<input type="submit" value="Cancel" class="button" onclick="window.close();" style="width: 60px" />--%>
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" class="button" Style="width: 60px"
                    OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>
    <input type="hidden" value="" id="hdnaction" />
    <input type="hidden" value="ModifyPortalInfo" name="SysFormName" id="SysFormName" />
    <input type="hidden" value="" id="FormMode" runat="server"/>
    <input type="hidden" value="rmx-widget-handle-3" id="SysWindowId" />
    <asp:TextBox Style="display: none" runat="server" id="txtData" />
    <asp:TextBox Style="display: none" runat="server" id="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    </form>
    
    <script language="JavaScript" type="text/javascript">
        var sPortletURL = getQval("urlPortlet");
        if (sPortletURL == "**%20URL%20Editing%20Locked%20**") {
            var lblUrlOfPortlet = document.getElementById('tdUrlOfPortlet');
            lblUrlOfPortlet.style.display = "none";
        }
        var bIsModeEdit = getQval("edit");
        if (bIsModeEdit == "true") {
            var sPortletName = getQval("Name");

            var txtUrlOfPortlet = document.getElementById('UrlOfPortlet');
            var txtNameOfPortlet = document.getElementById('NameOfPortlet');

            txtUrlOfPortlet.value = sPortletURL;
            txtNameOfPortlet.value = sPortletName;
        }
    </script>
    
</body>
</html>
