﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Text;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class WebLinksSetup : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            try
            {
                TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
                if (txtRowId != null)
                {
                    txtRowId.Text = "0";
                }
               // **ksahu5-ML-MITS34310 Start**
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("WebLinksSetup.aspx"), "WebLinksSetupValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "WebLinksSetupValidations", sValidationResources, true);
               //**ksahu5-ML-MITS34310 End**

                if (!Page.IsPostBack)
                {

                    bReturnStatus = CallCWSFunctionBind("WebLinkAdaptor.Get", out sreturnValue);
                    if (bReturnStatus)
                    {
                        BindPageControls(sreturnValue);
                       
                    }

                }
                if (rdURLType.SelectedValue == "Public") //tmalhotra3-MITS 34120
                {
                    cmdAddCustomizedListUser.Visible = false;
                    cmdDelCustomizedListUser.Visible = false;//asingh263-MITS 34439
                }
                else
                {
                    cmdAddCustomizedListUser.Visible = true;
                    cmdDelCustomizedListUser.Visible = true;//asingh263-MITS 34439
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }


        }


        private void BindPageControls(string sreturnValue)
        {
            DataSet dsWebLinkRecordsSet = null;
            XmlDocument xmlWebLinkXDoc = new XmlDocument();
            xmlWebLinkXDoc.LoadXml(sreturnValue);
            dsWebLinkRecordsSet = ConvertXmlDocToDataSet(xmlWebLinkXDoc);

            if (hdnURLType.Text == "PUBLIC")
            {
                rdURLType.SelectedValue = "Public";
                cmdAddCustomizedListUser.Visible = false; //tmalhotra3-MITS 34120
                cmdDelCustomizedListUser.Visible = false;//asingh263-MITS 34439

            }
            else
            {
                rdURLType.SelectedValue = "Private";
                cmdAddCustomizedListUser.Visible = true;
                cmdDelCustomizedListUser.Visible = true;//asingh263-MITS 34439
            }
                if(rowid.Text == "0")
               {
              
                    if (dsWebLinkRecordsSet.Tables["AvailURLs"] != null)
                    {

                        lstSelectedURL.DataSource = dsWebLinkRecordsSet.Tables["AvailURLs"];
                        lstSelectedURL.DataTextField = "AvailURLs_Text";
                        lstSelectedURL.DataValueField = "value";
                        lstSelectedURL.DataBind();
                       
                    }
                    else
                    {
                        lstSelectedURL.Items.Clear();
                    }
         }

                if (dsWebLinkRecordsSet.Tables["UserListoption"] != null)
                {

                    lstUsers.DataSource = dsWebLinkRecordsSet.Tables["UserListoption"];
                    lstUsers.DataTextField = "UserListoption_Text";
                    lstUsers.DataValueField = "value";
                    lstUsers.DataBind();
                }
                else
                {
                    lstUsers.Items.Clear();
                }
            //if(xmlWebLinkXDoc.SelectSingleNode("ResultMessage//Document//form//group//WebLink//SelectedUsersID")!=null)
            //    UserId.Text = xmlWebLinkXDoc.SelectSingleNode("ResultMessage//Document//form//group//WebLink//SelectedUsersID").InnerText;
            //if (xmlWebLinkXDoc.SelectSingleNode("ResultMessage//Document//form//group//WebLink//SelectedUsers") != null)
            //    UserName.Text = xmlWebLinkXDoc.SelectSingleNode("ResultMessage//Document//form//group//WebLink//SelectedUsers").InnerText;
                dsWebLinkRecordsSet.Dispose();
          
        }
      

         protected void lstSelectedURL_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
        
            try
            {

                   rowid.Text = lstSelectedURL.SelectedValue;

                   bReturnStatus = CallCWSFunctionBind("WebLinkAdaptor.GetUrlData", out sreturnValue);
                if (bReturnStatus)
                {
                    BindPageControls(sreturnValue);
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
    
            try
            {

               
                    if (lstSelectedURL.SelectedValue != "")
                    {

                        rowid.Text = lstSelectedURL.SelectedValue;
                    }


                    bReturnStatus = CallCWSFunctionBind("WebLinkAdaptor.DeleteUrl", out sreturnValue);
                if (bReturnStatus)
                {
                    BindPageControls(sreturnValue);
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }


        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
          
          tbURLShortName.Text = "";
          tbURLString.Text= "";
            lstSelectedURL.SelectedValue = null;
           rowid.Text = "0";
           lstUsers.Items.Clear(); //tmalhotra3- MITS 34120
           chkUseAsSupplementalLink.Checked = false;//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691            
            
        }

        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
                XmlDocument objXml = null;
         
            try
            {
                    if (lstSelectedURL.SelectedValue != "")
                    {

                        rowid.Text = lstSelectedURL.SelectedValue;
                    }
                    else
                    {
                        rowid.Text = "0";
                    }
                bReturnStatus = CallCWSFunctionBind("WebLinkAdaptor.SaveUrl",  out sreturnValue);
                objXml = new XmlDocument();
                objXml.LoadXml(sreturnValue);
                if (bReturnStatus && !(objXml.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error"))
                {
                    BindPageControls(sreturnValue);
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        protected void rdUrlType_SelectionChange(object sender, EventArgs e)
        {
            while (lstUsers.Items.Count > 0)
                lstUsers.Items.RemoveAt(0);
        }
    }
}