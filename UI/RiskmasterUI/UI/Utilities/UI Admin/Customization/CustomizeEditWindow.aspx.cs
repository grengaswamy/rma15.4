﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Utilities.UI_Admin.Customization
{
    public partial class CustomizeEditWindow : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CustomizeEditWindow.aspx"), "CustomizeEditWindowValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CustomizeEditWindowValidations", sValidationResources, true);
            if (AppHelper.GetQueryStringValue("defaulttext") != "")
            {
                //bram4 - MITS:35852
                //this.defaulttext.Text = AppHelper.GetQueryStringValue("defaultvalue");
                this.defaulttext.Text = HttpUtility.ParseQueryString(Request.RawUrl).Get("defaultvalue");                
                //this.currenttext.Text = AppHelper.GetQueryStringValue("currentvalue");
               this.currenttext.Text= HttpUtility.ParseQueryString(Request.RawUrl).Get("currentvalue");
                //this.controlname.Text = AppHelper.GetQueryStringValue("controlname");
               this.controlname.Text = HttpUtility.ParseQueryString(Request.RawUrl).Get("controlname");
            }
            else
            {
                //bram4 - MITS:35852
                //this.currenttext.Text = AppHelper.GetQueryStringValue("currentvalue");
                this.currenttext.Text = HttpUtility.ParseQueryString(Request.RawUrl).Get("currentvalue");
                //this.controlname.Text = AppHelper.GetQueryStringValue("controlname");
                this.controlname.Text = HttpUtility.ParseQueryString(Request.RawUrl).Get("controlname");
                this.defaulttext.Visible = false;
                this.btndefault.Visible = false;
                this.defaultvalue.Visible = false;
            }

            if (this.currenttext.Text == "")
            {
                this.currenttext.Text = "***";
            }

            this.currenttext.Focus();
        }
    }
}
