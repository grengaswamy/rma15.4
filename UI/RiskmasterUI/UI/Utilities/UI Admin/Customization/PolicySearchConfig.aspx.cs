﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class PolicySearchConfig : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadPolicySearchConfigDetails();
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                SetPolicySearchConfigDetails();
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        #region Message Template
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>PolicySearchConfigAdaptor.GetSavedPolicySrchConfig</Function>
              </Call>
              <Document>
                 <PolicySearchConfig>
                  </PolicySearchConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement SetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>PolicySearchConfigAdaptor.SavePolicySrchConfig</Function>
              </Call>
              <Document>
                 <PolicySearchConfig>
                          <SysName></SysName>
                          <PolSym></PolSym> 
                          <Module></Module> 
                          <Lob></Lob>                           
                          <PolNum></PolNum>
                          <PolState></PolState>
                          <LocCmpny></LocCmpny> 
                          <LossDate></LossDate> 
                          <AgntNum></AgntNum> 
                          <InsrdZip></InsrdZip> 
                          <CustNum></CustNum> 
                          <MstrCmpny></MstrCmpny> 
                          <InsrdCity></InsrdCity> 
                          <InsrdSSN></InsrdSSN> 
                          <GrpNum></GrpNum> 
                          <InsrdLastName></InsrdLastName> 
                          <InsrdFirstName></InsrdFirstName> 
                          <InsrdName></InsrdName> 
                          <SysNameText></SysNameText>
                          <PolSymText></PolSymText> 
                          <ModuleText></ModuleText> 
                          <LobText></LobText>                           
                          <PolNumText></PolNumText>
                          <PolStateText></PolStateText>
                          <LocCmpnyText></LocCmpnyText> 
                          <LossDateText></LossDateText> 
                          <AgntNumText></AgntNumText> 
                          <InsrdZipText></InsrdZipText> 
                          <CustNumText></CustNumText> 
                          <MstrCmpnyText></MstrCmpnyText> 
                          <InsrdCityText></InsrdCityText> 
                          <InsrdSSNText></InsrdSSNText> 
                          <GrpNumText></GrpNumText> 
                          <InsrdLastNameText></InsrdLastNameText> 
                          <InsrdFirstNameText></InsrdFirstNameText> 
                          <InsrdNameText></InsrdNameText>
                    </PolicySearchConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }
        #endregion Message Template

        #region Private Methods
        /// <summary>
        /// get the Recent Claim configuration 
        /// </summary>
        /// <returns></returns>
        private XmlDocument GetPolicySearchConfigDetails()
        {
            XElement messageElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = GetConfigMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Sets the recent claim configuration
        /// </summary>
        /// <returns></returns>
        private XmlDocument SetPolicySearchConfigDetails()
        {
            XElement messageElement = null;
            XElement tempElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = SetConfigMessageTemplate();

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/SysName");
                if (chkSystemName.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/PolSym");
                if (chkPolicySymbol.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/Module");
                if (chkModule.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/Lob");
                if (chkLOB.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/PolNum");
                if (chkPolicyNumber.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/PolState");
                if (chkPolicyState.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/LocCmpny");
                if (chkLocationCompany.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/LossDate");
                if (chkDOL.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/AgntNum");
                if (chkAgentNumber.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdZip");
                if (chkInsuredZip.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/CustNum");
                if (chkCustomerNumber.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/MstrCmpny");
                if (chkMasterCompany.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdCity");
                if (chkInsuredCity.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdSSN");
                if (chkInsuredSSN.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/GrpNum");
                if (chkGroupNumber.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdLastName");
                if (chkInsuredLastName.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdFirstName");
                if (chkInsuredFirstName.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdName");
                if (chkInsuredName.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/SysNameText");
                tempElement.Value = txtSystemName.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/PolSymText");
                tempElement.Value = txtPolicySymbol.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/ModuleText");
                tempElement.Value = txtModule.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/LobText");
                tempElement.Value = txtLOB.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/PolNumText");
                tempElement.Value = txtPolicyNumber.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/PolStateText");
                tempElement.Value = txtPolicyState.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/LocCmpnyText");
                tempElement.Value = txtLocationCompany.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/LossDateText");
                tempElement.Value = txtDOL.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/AgntNumText");
                tempElement.Value = txtAgentNumber.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdZipText");
                tempElement.Value = txtInsuredZip.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/CustNumText");
                tempElement.Value = txtCustomerNumber.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/MstrCmpnyText");
                tempElement.Value = txtMasterCompany.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdCityText");
                tempElement.Value = txtInsuredCity.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdSSNText");
                tempElement.Value = txtInsuredSSN.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/GrpNumText");
                tempElement.Value = txtGroupNumber.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdLastNameText");
                tempElement.Value = txtInsuredLastName.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdFirstNameText");
                tempElement.Value = txtInsuredFirstName.Value;

                tempElement = messageElement.XPathSelectElement("./Document/PolicySearchConfig/InsrdNameText");
                tempElement.Value = txtInsuredName.Value;              

                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Load the Values from the databse in to the respective controls
        /// </summary>
        private void LoadPolicySearchConfigDetails()
        {
            XmlDocument resultDoc = null;
            XmlNode PolicySrchConfigNode = null;

            resultDoc = GetPolicySearchConfigDetails();

            if (resultDoc != null)
            {
                //set obtained values of Config
                PolicySrchConfigNode = resultDoc.SelectSingleNode("//Document/PolicySearchConfig");

                if (PolicySrchConfigNode != null)
                {
                    if (PolicySrchConfigNode.SelectSingleNode("SysName").InnerText == "True")
                    {
                        chkSystemName.Checked = true;
                    }
                    else
                    {
                        chkSystemName.Checked = false;
                    }

                    if (PolicySrchConfigNode.SelectSingleNode("PolSym").InnerText == "True")
                    {
                        chkPolicySymbol.Checked = true;
                    }
                    else
                    {
                        chkPolicySymbol.Checked = false;
                    }

                    if (PolicySrchConfigNode.SelectSingleNode("Module").InnerText == "True")
                    {
                        chkModule.Checked = true;
                    }
                    else
                    {
                        chkModule.Checked = false;
                    }

                    if (PolicySrchConfigNode.SelectSingleNode("Lob").InnerText == "True")
                    {
                        chkLOB.Checked = true;
                    }
                    else
                    {
                        chkLOB.Checked = false;
                    }

                    if (PolicySrchConfigNode.SelectSingleNode("PolNum").InnerText == "True")
                    {
                        chkPolicyNumber.Checked = true;
                    }
                    else
                    {
                        chkPolicyNumber.Checked = false;
                    }

                    if (PolicySrchConfigNode.SelectSingleNode("PolState").InnerText == "True")
                    {
                        chkPolicyState.Checked = true;
                    }
                    else
                    {
                        chkPolicyState.Checked = false;
                    }

                    if (PolicySrchConfigNode.SelectSingleNode("LocCmpny").InnerText == "True")
                    {
                        chkLocationCompany.Checked = true;
                    }
                    else
                    {
                        chkLocationCompany.Checked = false;
                    }

                    if (PolicySrchConfigNode.SelectSingleNode("LossDate").InnerText == "True")
                    {
                        chkDOL.Checked = true;
                    }
                    else
                    {
                        chkDOL.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("AgntNum").InnerText == "True")
                    {
                        chkAgentNumber.Checked = true;
                    }
                    else
                    {
                        chkAgentNumber.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("InsrdZip").InnerText == "True")
                    {
                        chkInsuredZip.Checked = true;
                    }
                    else
                    {
                        chkInsuredZip.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("CustNum").InnerText == "True")
                    {
                        chkCustomerNumber.Checked = true;
                    }
                    else
                    {
                        chkCustomerNumber.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("MstrCmpny").InnerText == "True")
                    {
                        chkMasterCompany.Checked = true;
                    }
                    else
                    {
                        chkMasterCompany.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("InsrdCity").InnerText == "True")
                    {
                        chkInsuredCity.Checked = true;
                    }
                    else
                    {
                        chkInsuredCity.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("InsrdSSN").InnerText == "True")
                    {
                        chkInsuredSSN.Checked = true;
                    }
                    else
                    {
                        chkInsuredSSN.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("GrpNum").InnerText == "True")
                    {
                        chkGroupNumber.Checked = true;
                    }
                    else
                    {
                        chkGroupNumber.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("InsrdLastName").InnerText == "True")
                    {
                        chkInsuredLastName.Checked = true;
                    }
                    else
                    {
                        chkInsuredLastName.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("InsrdFirstName").InnerText == "True")
                    {
                        chkInsuredFirstName.Checked = true;
                    }
                    else
                    {
                        chkInsuredFirstName.Checked = false;
                    }
                    if (PolicySrchConfigNode.SelectSingleNode("InsrdName").InnerText == "True")
                    {
                        chkInsuredName.Checked = true;
                    }
                    else
                    {
                        chkInsuredName.Checked = false;
                    }
                    txtSystemName.Value = PolicySrchConfigNode.SelectSingleNode("SysNameText").InnerText;
                    txtPolicySymbol.Value = PolicySrchConfigNode.SelectSingleNode("PolSymText").InnerText;
                    txtModule.Value = PolicySrchConfigNode.SelectSingleNode("ModuleText").InnerText;
                    txtLOB.Value = PolicySrchConfigNode.SelectSingleNode("LobText").InnerText;
                    txtPolicyNumber.Value = PolicySrchConfigNode.SelectSingleNode("PolNumText").InnerText;
                    txtPolicyState.Value = PolicySrchConfigNode.SelectSingleNode("PolStateText").InnerText;
                    txtLocationCompany.Value = PolicySrchConfigNode.SelectSingleNode("LocCmpnyText").InnerText;
                    txtDOL.Value = PolicySrchConfigNode.SelectSingleNode("LossDateText").InnerText;
                    txtAgentNumber.Value = PolicySrchConfigNode.SelectSingleNode("AgntNumText").InnerText;
                    txtInsuredZip.Value = PolicySrchConfigNode.SelectSingleNode("InsrdZipText").InnerText;
                    txtCustomerNumber.Value = PolicySrchConfigNode.SelectSingleNode("CustNumText").InnerText;
                    txtMasterCompany.Value = PolicySrchConfigNode.SelectSingleNode("MstrCmpnyText").InnerText;
                    txtInsuredCity.Value = PolicySrchConfigNode.SelectSingleNode("InsrdCityText").InnerText;
                    txtInsuredSSN.Value = PolicySrchConfigNode.SelectSingleNode("InsrdSSNText").InnerText;
                    txtGroupNumber.Value = PolicySrchConfigNode.SelectSingleNode("GrpNumText").InnerText;
                    txtInsuredLastName.Value = PolicySrchConfigNode.SelectSingleNode("InsrdLastNameText").InnerText;
                    txtInsuredFirstName.Value = PolicySrchConfigNode.SelectSingleNode("InsrdFirstNameText").InnerText;
                    txtInsuredName.Value = PolicySrchConfigNode.SelectSingleNode("InsrdNameText").InnerText;
                }
            }
        }
        #endregion
    }
}