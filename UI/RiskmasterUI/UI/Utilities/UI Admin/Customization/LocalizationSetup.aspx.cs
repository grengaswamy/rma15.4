﻿using System;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Linq;

using Riskmaster.Models;

namespace Riskmaster.UI.Utilities.UI_Admin.Customization
{
    public partial class LocalizationSetup : System.Web.UI.Page
    {
        private string pageID = RMXResourceProvider.PageId("SearchLocalization.aspx");//RMA 2176        
        private static string[] sDataSeparator = { "|^|" };
        private static string[] sSeparator = { "|^^|" };        
        private static string[] sSearchSeparator = { "||" };  
        protected void Page_Load(object sender, EventArgs e)
        {
            ListItem objItem =null;
            ListItem oItem = null;
            string sReturn = string.Empty;
            //RMXResourceServiceClient oRMXResClient = null;
            RMResource objRes = null;
            try
            {
                if (!IsPostBack)
                {   
                    //Getting Language list
                    //oRMXResClient = new RMXResourceServiceClient();
                    objRes = new RMResource();                   
                    objRes.LanguageCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
                    //sReturn = oRMXResClient.GetPageInfo(objRes, GetMessageTemplate().ToString());
                    objRes.Document = GetMessageTemplate().ToString();
                    objRes.ClientId = AppHelper.ClientId;
                    sReturn = AppHelper.GetResponse<string>("RMService/Resource/pageinfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);

                    XElement eReturn = XElement.Parse(sReturn);
                    foreach (XElement oElem in eReturn.Descendants("LanguageList").Nodes())
                    {
                        objItem = new ListItem(oElem.Value, oElem.FirstAttribute.Value.ToString());
                        lstLangList.Items.Add(objItem);
                    }
                    oItem = lstLangList.Items.FindByValue(objRes.LanguageCode);
                    if (oItem != null)
                        oItem.Selected = true;

                    ModifyControls();
                    ResetValueOfControls();
                   
                }
                RegisterScript(lstFieldList);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //if (oRMXResClient != null)
                //{
                //    oRMXResClient = null;
                //}
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }
        /// <summary>
        /// RegisterScript to improve the performance of listbox 
        /// </summary>
        /// <param name="lb"></param>
        private void RegisterScript(ListBox lb)
        {
            string script = string.Format(@"
                            var select=$get('{0}');
                            if (select) {{
                            var stub=document.createElement('input');
                            stub.type='hidden';
                            stub.id=select.id;
                            stub.name=select.name;
                            stub._behaviors=select._behaviors;
                            var val=new Array();
                            for (var i=0; i<select.options.length; i++)
                            if (select.options[i].selected) {{
                            val[val.length]=select.options[i].value;
                            }}
                            stub.value=val.join(',');
                            select.parentNode.replaceChild(stub,select);
                            }};
                            ", lb.ClientID);
            System.Web.UI.ScriptManager sm = System.Web.UI.ScriptManager.GetCurrent(this);
            if (sm != null) sm.RegisterDispose(lb, script);
            script = @"WebForm_InitCallback=function() {};";
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "removeWebForm_InitCallback", script, true);
        } 
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Document>
                 <LanguageList>
                 </LanguageList>    
                 <FieldList>
                 </FieldList>
                </Document>
            </Message>
            ");

            return oTemplate;
        }

        protected void lstFieldList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iCatId = 0;
            int iTableId = 0;
            string sDisplayCat = string.Empty;
            try
            {       
                //To do :how complete error will be displayed
                //RMA 2176 ksahu5 ML change start
                if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "5")
                {
                    lblSearchField.Visible = true;
                    lstSearchField.Visible = true;
                    iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                    if (iCatId == 20)
                    {
                        iTableId = Convert.ToInt32(lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[3].ToString());
                        SelectSearchFields(iCatId, "", iTableId);
                    }
                    {
                        sDisplayCat = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                        SelectSearchFields(iCatId, sDisplayCat,0);
                    }
                }
                else if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "6")
                {
                    tbxText.Text = lstFieldList.SelectedItem.Text;
                    tbxText.Enabled = true;
                    lstFieldList.Focus();
                }
                else
                {
                    tbxText.Text = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2];
                    tbxText.Enabled = true;             
                    lstFieldList.Focus();
                }
                //RMA 2176 ksahu5 ML change End
            }
            catch (Exception ee)
            {
               
            }
        }

        protected void drpPageList_SelectedIndexChanged(object sender, EventArgs e)
        {          
            //RMA 2176 ksahu5 ML change start
            int iCatId = 0;
            int iTableId = 0;
            string sSelectedValue = string.Empty;
            if (drpErrorsOrPages.SelectedValue == "3")
            {
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                if (iCatId == 20)
                {                    
                    selectTypeofControl(iCatId);
                    lblAdminTable.Visible = true;
                    lstAdminTable.Visible = true;
                }
                else
                {
                    sSelectedValue = drpType.SelectedValue;
                    if (drpType.Items.Count == 4)
                    {
                        drpType.Items.Remove(drpType.Items.FindByText("Administrative Tracking Table Name"));
                        if (sSelectedValue != "8")
                        {
                            drpType.SelectedValue = sSelectedValue;
                        }
                        else
                        {
                            drpType.SelectedValue = "7";
                        }                        
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(sSelectedValue))
                        {
                            drpType.SelectedValue = sSelectedValue;
                        } 
                    }                
                    lblAdminTable.Visible = false;
                    lstAdminTable.Visible = false;
                    
                }               
            }
            if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "6")
            {
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                if (iCatId == 20)
                {
                    SelectAdminTrackingTable(iCatId);
                    lblAdminTable.Visible = true;
                    lstAdminTable.Visible = true;
                    iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                    SelectSearchGroups(iCatId, iTableId);
                }
                else
                {
                    SelectSearchGroups(iCatId, 0);
                    lblAdminTable.Visible = false;
                    lstAdminTable.Visible = false;                    
                }
                lblFieldlst.Visible = true;
                lstFieldList.Visible = true;
                lblSearchField.Visible = false;
                lstSearchField.Visible = false;                                         
            }
            else if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "7")
            {
                lblAdminTable.Visible = false;
                lstAdminTable.Visible = false;                
                lblSearchField.Visible = false;
                lstSearchField.Visible = false;
                lstSearchField.Items.Clear();
                lblFieldlst.Visible = false;
                lstFieldList.Visible = false;
                lstFieldList.Items.Clear();
                tbxText.Text = drpPageList.SelectedItem.Text;
                tbxText.Enabled = true;
                drpPageList.Focus();

            }
            else if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "5")
            {
                if (iCatId == 20)
                {
                    SelectAdminTrackingTable(iCatId);
                    lblAdminTable.Visible = true;
                    lstAdminTable.Visible = true;
                    iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                    SelectSearchGroups(iCatId, iTableId);
                    SelectSearchFields(iCatId, "", iTableId);                    
                }
                else
                {
                    SelectSearchGroups(iCatId, 0);
                    string sDisplayCat = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                    SelectSearchFields(iCatId, sDisplayCat,0);
                    lblAdminTable.Visible = false;
                    lstAdminTable.Visible = false;
                }               
                lblFieldlst.Visible = true;
                lstFieldList.Visible = true;
                lblSearchField.Visible = true;
                lstSearchField.Visible = true;
            }
            //Jira 4327 start
            else if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "8")
            {
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                SelectAdminTrackingTable(iCatId);
                lblAdminTable.Visible = true;
                lstAdminTable.Visible = true;                
            }
            //jira 4327 end
            else
            {
            ResetValueOfControls();
            }
            //RMA 2176 ksahu5 ML change End
        }

        protected void lstLangList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RMA 2176 start
            if (drpErrorsOrPages.SelectedValue == "3")
            {
                hdnSearchKey.Value = drpPageList.SelectedValue + sSearchSeparator[0] + lstFieldList.SelectedValue + sSearchSeparator[0] + lstSearchField.SelectedValue + sSearchSeparator[0] + lstAdminTable.SelectedValue;   //jira 4327
                lstFieldList.Items.Clear();
                lstSearchField.Items.Clear();
                drpPageList.Items.Clear();
                lstAdminTable.Items.Clear();
            }
            //RMA 2176 End
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                PostBackSender.Value = "Search";
            }
            ResetValueOfControls();          
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //RMXResourceServiceClient oRMXResClient = null;
            string sMessage=string.Empty;
            ListItem oItem = null;
            RMResource objRes = null;

            string sDictSelectedValue = string.Empty;
            string sResourceID = string.Empty;
            string sKey = string.Empty;
            string strSearchType = string.Empty;
            Dictionary<string, string> oDict = new Dictionary<string, string>();
            string sOut = string.Empty;
            bool bRet = false;
            try
            {
                
                //oRMXResClient = new RMXResourceServiceClient();
                objRes = new RMResource();
                //if (drpPageList.SelectedValue == "MDIMenuXML")
                 if (drpErrorsOrPages.SelectedValue == "2" && drpType.SelectedValue =="9")//MDI
                {
                    objRes.ResourceKey = "MDIMenuXML";
                    XmlDocument objDoc = new XmlDocument();
                    objDoc.LoadXml(tbxSearchField.Text.Trim());
                    #region commented
                    //To change the Header node values through Localisation Setup
                    //foreach (XmlNode oElem in objDoc.SelectNodes("//item"))
                    //{
                    //    try
                    //    {
                    //        if (oElem.Attributes["sysName"] != null)
                    //        {
                    //            XmlAttribute xmAtrr=objDoc.CreateAttribute("title");
                    //            switch (oElem.Attributes["sysName"].Value)
                    //            {
                    //                case "DocumentRoot":
                    //                    xmAtrr.Value = "Document";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "PolicyRoot":
                    //                    xmAtrr.Value = "Policy";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "DiariesRoot":
                    //                    xmAtrr.Value = "Diaries";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "FundsRoot":
                    //                    xmAtrr.Value = "Funds";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "MaintenanceRoot":
                    //                    xmAtrr.Value = "Maintenance";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "MyWorkRoot":
                    //                    xmAtrr.Value = "My Work";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "ReportsRoot":
                    //                    xmAtrr.Value = "Reports";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "SearchRoot":
                    //                    xmAtrr.Value = "Search";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "SecurityRoot":
                    //                    xmAtrr.Value = "Security";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "UserDocumentsRoot":
                    //                    xmAtrr.Value = "User Documents";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "utilitiesRoot":
                    //                    xmAtrr.Value = "Utilities";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "HelpRoot":
                    //                    xmAtrr.Value = "Help";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //                case "OtherRoot":
                    //                    xmAtrr.Value = "Other";
                    //                    oElem.Attributes.Append(xmAtrr);
                    //                    break;
                    //            }
                                
                    //        }
                    //    }
                    //    catch
                    //    {
                    //    }
                    //}
                    #endregion
                    objRes.ResourceValue = objDoc.OuterXml;
                    objRes.ClientId = AppHelper.ClientId;
                    objRes.LanguageCode = lstLangList.SelectedValue;
                    //bool bRet = oRMXResClient.SaveResource(objRes, out sMessage);
                    sOut = AppHelper.GetResponse<string>("RMService/Resource/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, objRes);
                    bRet = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                    sMessage = sOut.Split(sSeparator, StringSplitOptions.None)[1];
                    if (bRet)
                    {
                        HttpRuntime.Cache.Remove("RMXMDIMenu" + "_" + objRes.LanguageCode);
                    }
                }
                //ksahu5-ML -RMA 2176 start
                 else if (drpErrorsOrPages.SelectedValue == "2" && drpType.SelectedValue == "10")//child screen
                 {
                     objRes.ResourceKey = "CHILDSCREENXML";
                     XmlDocument objDoc = new XmlDocument();
                     objDoc.LoadXml(tbxSearchField.Text.Trim());
                     objRes.ResourceValue = objDoc.OuterXml;
                     objRes.ClientId = AppHelper.ClientId;
                     objRes.LanguageCode = lstLangList.SelectedValue;
                     //bool bRet = oRMXResClient.SaveResource(objRes, out sMessage);
                     sOut = AppHelper.GetResponse<string>("RMService/Resource/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, objRes);
                     bRet = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                     sMessage = sOut.Split(sSeparator, StringSplitOptions.None)[1];
                 }
                else if (drpErrorsOrPages.SelectedValue == "3")
                {
                    bRet = false;
                    objRes.PageId = pageID;
                    strSearchType = drpType.SelectedValue.ToString();
                    switch (strSearchType)
                    {
                        case "5":
                            oDict = ResetControlsSearch("5");
                            if (!string.IsNullOrEmpty(lstSearchField.SelectedValue))
                            {
                                sKey = lstSearchField.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                            }
                            else
                            {
                                tbxText.Text = string.Empty;
                                return;
                            }                            
                            if (oDict.ContainsKey(sKey))
                            {
                                sDictSelectedValue = oDict[sKey].ToString();
                                sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            else
                            {
                                sResourceID = lstSearchField.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            objRes.ResourceId = sResourceID;
                            objRes.ClientId = AppHelper.ClientId;
                            objRes.ResourceType = strSearchType;
                            objRes.ResourceKey = sKey;
                            objRes.ResourceValue = tbxText.Text.Trim();
                            objRes.LanguageCode = lstLangList.SelectedValue;
                            objRes.User = HttpContext.Current.User.Identity.Name;
                           // bRet = oRMXResClient.SaveResource(objRes, out sMessage);
                            sOut = AppHelper.GetResponse<string>("RMService/Resource/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, objRes);
                            bRet = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                            sMessage = sOut.Split(sSeparator, StringSplitOptions.None)[1];

                            if (!bRet && !string.IsNullOrEmpty(sMessage))
                            {
                                ErrorControl1.DisplayError(sMessage);
                                ErrorControl1.Visible = true;
                                return;
                            }
                            else
                            {
                                oItem = lstSearchField.Items.FindByText(lstSearchField.SelectedItem.Text);
                                if (oItem != null)
                                {
                                    oItem.Value = lstSearchField.SelectedValue;
                                    oItem.Text = tbxText.Text.Trim();
                                }
                            }
                            break;
                        case "6":
                            oDict = ResetControlsSearch("6");
                            sKey = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                            if (oDict.ContainsKey(sKey))
                            {
                                sDictSelectedValue = oDict[sKey].ToString();
                                sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            else
                            {
                                sResourceID = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            objRes.ResourceId = sResourceID;
                            objRes.ClientId = AppHelper.ClientId;
                            objRes.ResourceType = strSearchType;
                            objRes.ResourceKey = sKey;
                            objRes.ResourceValue = tbxText.Text.Trim();
                            objRes.LanguageCode = lstLangList.SelectedValue;
                            objRes.User = HttpContext.Current.User.Identity.Name;
                            // bRet = oRMXResClient.SaveResource(objRes, out sMessage);
                            sOut = AppHelper.GetResponse<string>("RMService/Resource/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, objRes);
                            bRet = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                            sMessage = sOut.Split(sSeparator, StringSplitOptions.None)[1];

                            if (!bRet && !string.IsNullOrEmpty(sMessage))
                            {
                                ErrorControl1.DisplayError(sMessage);
                                ErrorControl1.Visible = true;
                                return;
                            }
                            else
                            {
                                oItem = lstFieldList.Items.FindByText(lstFieldList.SelectedItem.Text);
                                if (oItem != null)
                                {
                                    oItem.Value = lstFieldList.SelectedValue;
                                    oItem.Text = tbxText.Text.Trim();
                                }
                            }

                            break;
                        case "7":
                            oDict = ResetControlsSearch("7");
                            sKey = drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                            if (oDict.ContainsKey(sKey))
                            {
                                sDictSelectedValue = oDict[sKey].ToString();
                                sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            else
                            {
                                sResourceID = drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            objRes.ResourceId = sResourceID;
                            objRes.ClientId = AppHelper.ClientId;
                            objRes.ResourceType = strSearchType;
                            objRes.ResourceKey = sKey;
                            objRes.ResourceValue = tbxText.Text.Trim();
                            objRes.LanguageCode = lstLangList.SelectedValue;
                            objRes.User = HttpContext.Current.User.Identity.Name;
                            // bRet = oRMXResClient.SaveResource(objRes, out sMessage);
                            sOut = AppHelper.GetResponse<string>("RMService/Resource/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, objRes);
                            bRet = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                            sMessage = sOut.Split(sSeparator, StringSplitOptions.None)[1];
                            if (!bRet && !string.IsNullOrEmpty(sMessage))
                            {
                                ErrorControl1.DisplayError(sMessage);
                                ErrorControl1.Visible = true;
                                return;
                            }
                            else
                            {
                                oItem = drpPageList.Items.FindByText(drpPageList.SelectedItem.Text);
                                if (oItem != null)
                                {
                                    oItem.Value = drpPageList.SelectedValue;
                                    oItem.Text = tbxText.Text.Trim();
                                }
                            }
                            break;
                            //jira 4327 start
                        case "8":
                            oDict = ResetControlsSearch("8");
                            sKey = lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                            if (oDict.ContainsKey(sKey))
                            {
                                sDictSelectedValue = oDict[sKey].ToString();
                                sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            else
                            {
                                sResourceID = lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            }
                            objRes.ResourceId = sResourceID;
                            objRes.ClientId = AppHelper.ClientId;
                            objRes.ResourceType = strSearchType;
                            objRes.ResourceKey = sKey;
                            objRes.ResourceValue = tbxText.Text.Trim();
                            objRes.LanguageCode = lstLangList.SelectedValue;
                            objRes.User = HttpContext.Current.User.Identity.Name;
                            // bRet = oRMXResClient.SaveResource(objRes, out sMessage);
                            sOut = AppHelper.GetResponse<string>("RMService/Resource/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, objRes);
                            bRet = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                            sMessage = sOut.Split(sSeparator, StringSplitOptions.None)[1];

                            if (!bRet && !string.IsNullOrEmpty(sMessage))
                            {
                                ErrorControl1.DisplayError(sMessage);
                                ErrorControl1.Visible = true;
                                return;
                            }
                            else
                            {
                                oItem = lstAdminTable.Items.FindByText(lstAdminTable.SelectedItem.Text);
                                if (oItem != null)
                                {
                                    oItem.Value = lstAdminTable.SelectedValue;
                                    oItem.Text = tbxText.Text.Trim();
                                }
                            }
                            break;
                            //jira 4327 End
                    }  
                 
                }
                //ksahu5-ML -RMA 2176 End
                else
                {
                    if (drpErrorsOrPages.SelectedValue == "0")//Error Messages
                    {
                        objRes.PageId = "GlobalMessages";
                    }
                    else
                    {
                        objRes.PageId = drpPageList.SelectedValue;
                    }
                    objRes.ResourceId = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0];
                    objRes.ResourceType = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1];
                    objRes.ResourceKey = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[3];
                    objRes.ResourceValue = tbxText.Text.Trim();
                    objRes.LanguageCode = lstLangList.SelectedValue;
                    objRes.User = HttpContext.Current.User.Identity.Name;
                    objRes.ClientId = AppHelper.ClientId;
                    //bool bRet = oRMXResClient.SaveResource(objRes, out sMessage); 
                    sOut = AppHelper.GetResponse<string>("RMService/Resource/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, objRes);
                    bRet = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                    sMessage = sOut.Split(sSeparator, StringSplitOptions.None)[1];
                    if (!bRet && !string.IsNullOrEmpty(sMessage))
                    {
                        ErrorControl1.DisplayError(sMessage);
                        ErrorControl1.Visible = true;
                        return;
                    }
                    else
                    {
                        oItem = lstFieldList.Items.FindByText(lstFieldList.SelectedItem.Text);
                        if (oItem != null)
                        {
                            //Resource Id is returned from service
                           // oItem.Value = oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[0] + "|^|" + oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[1] + "|^|" + tbxText.Text.Trim() + "|^|" + oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[3];
                            oItem.Value = sMessage + "|^|" + oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[1] + "|^|" + tbxText.Text.Trim() + "|^|" + oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[3];
                            if (oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[1] == "3" || oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[1] == "1") //Global Messages
                            {
                                int iNoOfSpaces = 8 ;
                                oItem.Text = oItem.Value.Split(sDataSeparator, StringSplitOptions.None)[0].PadLeft(iNoOfSpaces,' ') + ": " + tbxText.Text.Trim();
                                if (oItem.Text.Trim().Length >114)
                                {
                                    oItem.Text = tbxText.Text.Trim().Substring(0, 110) + "...";
                                }
                            }
                            else
                                oItem.Text = tbxText.Text.Trim();

                            //oItem.Text = tbxText.Text.Trim();
                        }
                        if( objRes.PageId == "GlobalMessages")
                        {
                           // oRMXResClient.ClearGlobalErrorMessageCache(objRes.ResourceId);
                        }
                        else
                        {
                           RMXResourceManager.RMXResourceProvider.ClearResourceForPage(drpPageList.SelectedValue, lstLangList.SelectedValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }

        private string FormatXml(string sUnformattedXml)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(sUnformattedXml);
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            XmlTextWriter xtw = null;
            try
            {
                xtw = new XmlTextWriter(sw);
                xtw.Formatting = Formatting.Indented;
                xd.WriteTo(xtw);
            }
            finally
            {
                if (xtw != null)
                    xtw.Close();
            }
            return sb.ToString();
        }

        protected void drpErrorsOrPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem objItem = null;
            ListItem oItem = null;
            if (drpErrorsOrPages.SelectedValue == "1")
            {
                drpPageList.Items.Clear();
                XElement pageListElement = RMXResourceProvider.GetMLSupportedPageList();
                foreach (XElement oPageElem in pageListElement.Descendants("Page"))
                {
                    objItem = new ListItem(oPageElem.FirstAttribute.Value.ToString(), oPageElem.LastAttribute.Value.ToString());
                    drpPageList.Items.Add(objItem);
                    oItem = drpPageList.Items[0];
                    if (oItem != null)
                        oItem.Selected = true;
                }
            }
            //Sorting of list
            ArrayList arList = new ArrayList();
            ArrayList arObjList = new ArrayList();
            foreach (ListItem o in drpPageList.Items)
            {
                arList.Add(o.Text);
            }
            arList.Sort();
            foreach (object o in arList)
            {
               ListItem oB = drpPageList.Items.FindByText(o.ToString());
               arObjList.Add(oB);
            }
            drpPageList.Items.Clear();
            foreach (ListItem o in arObjList)
            {
                drpPageList.Items.Add(o);
            }

            ModifyControls();
            ResetValueOfControls();

        }

        private void ModifyControls()
        {
            ListItem objListItem = null;
            int iCatId = 0;//jira 4327

            txtSearch.Text = "";
            if (drpErrorsOrPages.SelectedValue == "0") //Errors n alert messages
            {
                lblResrcSet.Visible = false;
                drpPageList.Visible = false;

                lblType.Text = "Type :";
                lblType.Visible = true;

                drpType.Items.Clear();
                objListItem = new ListItem("Inline Messages", "0");
                drpType.Items.Add(objListItem);
                objListItem = new ListItem("Alert Messages", "1");
                drpType.Items.Add(objListItem);
                objListItem = new ListItem("Error Messages", "3");
                drpType.Items.Add(objListItem);
                drpType.Visible = true;
                
                lblFieldlst.Text = "List of messages:";
                lblFieldlst.Visible = true;
                lstFieldList.Visible = true;

                tbxSearchField.Visible = false;
                tbxText.Visible = true;
                lblText.Visible = true;

                lblSearch.Visible = true;
                txtSearch.Visible = true;
                btnSearch.Visible = true;
                btnReset.Visible = true;
                lblSearchField.Visible = false;//RMA-2176
                lstSearchField.Visible = false;//RMA-2176
                lblAdminTable.Visible = false;//RMA-4327
                lstAdminTable.Visible = false;//RMA-4327 
            }
            else if (drpErrorsOrPages.SelectedValue == "1") //Pages :labels/buttons/tooltips/alert messages
            {
                lblResrcSet.Text = "List of Pages :";
                lblResrcSet.Visible = true;
                drpPageList.Visible = true;

                lblType.Text = "Type of Control :";
                lblType.Visible = true;

                drpType.Items.Clear();
                objListItem=new ListItem("Alert Messages","1");
                drpType.Items.Add(objListItem);
                objListItem = new ListItem("Buttons", "4");
                drpType.Items.Add(objListItem);
                objListItem = new ListItem("Labels", "0");
                drpType.Items.Add(objListItem);
                objListItem = new ListItem("ToolTips", "2");
                drpType.Items.Add(objListItem);
                drpType.Visible = true;
                
                lblFieldlst.Text = "List of Controls:";
                lblFieldlst.Visible = true;
                lstFieldList.Visible = true;

                tbxSearchField.Visible = false;
                tbxText.Visible = true;
                lblText.Visible = true;

                lblSearch.Visible = false;
                txtSearch.Visible = false;
                btnSearch.Visible = false;
                btnReset.Visible = false;
                lblSearchField.Visible = false;//RMA-2176
                lstSearchField.Visible = false;//RMA-2176
                lblAdminTable.Visible = false;
                lstAdminTable.Visible = false; 

            }
            else if (drpErrorsOrPages.SelectedValue == "2") //MDI
            {
                lblType.Text = "Type :";
                lblType.Visible = true;

                drpType.Items.Clear();
                objListItem = new ListItem("MDI Menu", "9");
                drpType.Items.Add(objListItem);
                objListItem = new ListItem("Child Menu", "10");
                drpType.Items.Add(objListItem);
                drpType.Visible = true;
                lblResrcSet.Visible = false;
                drpPageList.Visible = false;

                //lblType.Visible = false;
                //drpType.Visible = false;                

                lblFieldlst.Visible = false;
                lstFieldList.Visible = false;

                tbxText.Visible = false;
                lblText.Visible = false;
                tbxSearchField.Visible = false;

                lblSearch.Visible = false;
                txtSearch.Visible = false;
                btnSearch.Visible = false;
                btnReset.Visible = false;
                lblSearchField.Visible = false;//RMA-2176
                lstSearchField.Visible = false;//RMA-2176
                lblAdminTable.Visible = false;
                lstAdminTable.Visible = false;                
            }
            //RMA-2176-start
            if (drpErrorsOrPages.SelectedValue == "3") //Search
            {
                lblResrcSet.Text = "Search Caterories :";
                lblResrcSet.Visible = true;
                drpPageList.Visible = true;

                SelectSearchCategories();
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());//jira 4327
                lblType.Text = "Type of Control :";
                lblType.Visible = true;

                drpType.Items.Clear();               
                selectTypeofControl(iCatId);//jira4327

                drpType.Visible = true;
                tbxSearchField.Visible = false;
                tbxText.Visible = true;
                lblText.Visible = true;
                lblSearch.Visible = false;
                txtSearch.Visible = false;
                btnSearch.Visible = false;
                btnReset.Visible = false;

                lblSearchField.Visible = false;
                lstSearchField.Visible = false;
                lblFieldlst.Visible = false;
                lstFieldList.Visible = false;
                lblAdminTable.Visible = false;
                lstAdminTable.Visible = false;                            
               
            }
            //RMA-2176-End

        }
        //jira 4327 Start        
        private void selectTypeofControl(int iCatId)
        {
            ListItem objListItem = null;
            string sSelectedValue = string.Empty;
            sSelectedValue = drpType.SelectedValue;
            int iCount = drpType.Items.Count;

            drpType.Items.Clear();            
            objListItem = new ListItem("Category Name", "7");
            drpType.Items.Add(objListItem);
            if (iCatId == 20)
            {
                objListItem = new ListItem("Administrative Tracking Table Name", "8");
                drpType.Items.Add(objListItem);
            }            
            objListItem = new ListItem("Group Name", "6");
            drpType.Items.Add(objListItem);
            objListItem = new ListItem("Field Name", "5");
            drpType.Items.Add(objListItem);
            drpType.DataBind();
            if (!string.IsNullOrEmpty(sSelectedValue))
            {
                drpType.SelectedValue = sSelectedValue;
            }
               
        }
        
        private void SelectAdminTrackingTable(int iCatId)
        {
            ListItem objItem = null;
            string sReturn = string.Empty;
            string sDispValue = string.Empty;
            string sTempValue = string.Empty;
            string sKey = string.Empty;
            string sDictSelectedValue = string.Empty;
            string sResourceID = string.Empty;
            string sKeyGr = string.Empty;
            string sAdminTableName = string.Empty;
            string sTableId = string.Empty;            

           // RMXResourceService.RMXResourceServiceClient oRMXResClient = null;
           // RMXResourceService.RMResource objRes = null;
            // RMXResourceService.RMXResourceServiceClient oRMXResClient = null;
            RMResource objRes = null;
            try
            {
                //oRMXResClient = new RMXResourceService.RMXResourceServiceClient();
                objRes = new RMResource();
                Dictionary<string, string> oDict = new Dictionary<string, string>();                
                oDict = ResetControlsSearch("8");
                //sReturn = oRMXResClient.GetSearchResult(objRes, GetMessageTemplate().ToString(), int.Parse(AppHelper.ReadCookieValue("DsnId")), "8", iCatId, "", 0,0);

                objRes.ClientId = AppHelper.ClientId;
                objRes.Document = GetMessageTemplate().ToString();
                objRes.DataSourceId = AppHelper.ReadCookieValue("DsnId");
                objRes.SearchType = "8";
                objRes.CatId = iCatId;
                objRes.DisplayCat = "";
                objRes.TableId = 0;
                //sReturn = oRMXResClient.GetSearchResult(objRes, , , "7", 0, "", 0);
                sReturn = AppHelper.GetResponse<string>("RMService/Resource/searchresult", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                XElement eReturn = XElement.Parse(sReturn);
                lstAdminTable.Items.Clear();
                foreach (XElement oElem in eReturn.Descendants("FieldList").Nodes())
                {
                    sResourceID = "0";
                    sDispValue = oElem.Value.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                    sAdminTableName = oElem.Value;
                    sKey = GenerateResourcKey(oElem.LastAttribute.Value);
                    sTableId = oElem.FirstAttribute.Value;
                    if (oDict.ContainsKey(sKey))
                    {
                        sDictSelectedValue = oDict[sKey].ToString();
                        sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                        sDispValue = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                    }
                    sTempValue = sResourceID + "|^|" + sKey + "|^|" + sTableId + "|^|" + sAdminTableName;
                    objItem = new ListItem(sDispValue, sTempValue);
                    lstAdminTable.Items.Add(objItem);
                }
                lstAdminTable.SelectedIndex = 0;
                tbxText.Text = lstAdminTable.Items[0].Text;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //if (oRMXResClient != null)
                //{
                //    oRMXResClient = null;
                //}
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }
        //jira 4327 End
        //ksahu5-RMA-2176-start
        private void SelectSearchCategories()
        {
            string sReturn = string.Empty;
            string sDispValue = string.Empty;
            string sTempValue = string.Empty;
            string sKey = string.Empty;
            string sDictSelectedValue = string.Empty;
            string sResourceID = string.Empty;

            // RMXResourceService.RMXResourceServiceClient oRMXResClient = null;
            RMResource objRes = null;
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();

            try
            {
                //oRMXResClient = new RMXResourceService.RMXResourceServiceClient();
                objRes = new RMResource();
                Dictionary<string, string> oDict = new Dictionary<string, string>();
                oDict = ResetControlsSearch("7");
                objRes.ClientId = AppHelper.ClientId;
                objRes.Document = GetMessageTemplate().ToString();
                objRes.DataSourceId = AppHelper.ReadCookieValue("DsnId");
                objRes.SearchType = "7";
                objRes.CatId = 0;
                objRes.DisplayCat = "";
                //sReturn = oRMXResClient.GetSearchResult(objRes, , , "7", 0, "", 0);
                sReturn = AppHelper.GetResponse<string>("RMService/Resource/searchresult", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                XElement eReturn = XElement.Parse(sReturn);
                drpPageList.Items.Clear();

                foreach (XElement oElem in eReturn.Descendants("FieldList").Nodes())
                {
                    sResourceID = "0";
                    sDispValue = oElem.Value;
                    sKey = GenerateResourcKey(oElem.LastAttribute.Value);
                    if (oDict.ContainsKey(sKey))
                    {
                        sDictSelectedValue = oDict[sKey].ToString();
                        sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                        sDispValue = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                    }
                    sTempValue = sResourceID + "|^|" + oElem.FirstAttribute.Value + "|^|" + sKey;
                    items.Add(new KeyValuePair<string, string>(sDispValue, sTempValue));
                }
                
                drpPageList.DataSource = items.OrderBy(item => item.Key);
                drpPageList.DataTextField = "Key";
                drpPageList.DataValueField = "Value";
                drpPageList.DataBind();

                drpPageList.SelectedIndex = 0;
                tbxText.Text = drpPageList.Items[0].Text;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //if (oRMXResClient != null)
                //{
                //    oRMXResClient = null;
                //}
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }
        private void SelectSearchGroups(int iCatId,int iTableId)
        {
            ListItem objItem = null;
            string sReturn = string.Empty;
            string sDispValue = string.Empty;
            string sTempValue = string.Empty;
            string sKey = string.Empty;
            string sDictSelectedValue = string.Empty;
            string sResourceID = string.Empty;
            string sKeyGr = string.Empty;
            string sDispCategory = string.Empty;
            string sFirstAttrValue = string.Empty;

            //RMXResourceService.RMXResourceServiceClient oRMXResClient = null;
            RMResource objRes = null;
            try
            {
                //oRMXResClient = new RMXResourceService.RMXResourceServiceClient();
                objRes = new RMResource();
                Dictionary<string, string> oDict = new Dictionary<string, string>();
                oDict = ResetControlsSearch("6");
                //sReturn = oRMXResClient.GetSearchResult(objRes, GetMessageTemplate().ToString(), int.Parse(AppHelper.ReadCookieValue("DsnId")), "6", iCatId, "", 0);
                objRes.ClientId = AppHelper.ClientId;
                objRes.Document = GetMessageTemplate().ToString();
                objRes.DataSourceId = AppHelper.ReadCookieValue("DsnId");
                objRes.SearchType = "6";
                objRes.CatId = iCatId;
                objRes.DisplayCat = "";
                objRes.TableId = iTableId;
                //sReturn = oRMXResClient.GetSearchResult(objRes, , , "7", 0, "", 0);
                sReturn = AppHelper.GetResponse<string>("RMService/Resource/searchresult", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                XElement eReturn = XElement.Parse(sReturn);
                lstFieldList.Items.Clear();
                foreach (XElement oElem in eReturn.Descendants("FieldList").Nodes())
                {
                    sResourceID = "0";
                    sDispValue = oElem.Value;
                    sDispCategory = oElem.Value;
                    sKey = GenerateResourcKey(oElem.LastAttribute.Value);
                    sFirstAttrValue = oElem.FirstAttribute.Value;
                    if (oDict.ContainsKey(sKey))
                    {
                        sDictSelectedValue = oDict[sKey].ToString();
                        sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                        sDispValue = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                    }
                    sTempValue = sResourceID + "|^|" + sKey + "|^|" + sDispCategory + "|^|" + sFirstAttrValue;
                    objItem = new ListItem(sDispValue, sTempValue);
                    lstFieldList.Items.Add(objItem);
                }
                lstFieldList.SelectedIndex = 0;
                tbxText.Text = lstFieldList.Items[0].Text;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //if (oRMXResClient != null)
                //{
                //    oRMXResClient = null;
                //}
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }
        private void SelectSearchFields(int iCatId, string sDisplayCat,int iTableId)
        {
            ListItem objItem = null;
            string sReturn = string.Empty;
            string sDispValue = string.Empty;
            string sTempValue = string.Empty;
            string sResourceKey = string.Empty;
            string sDictSelectedValue = string.Empty;
            string sResourceID = string.Empty;
            string sKey = string.Empty;
            string sFieldName = string.Empty;
            string sFirstAttrValue = string.Empty;

            //RMXResourceService.RMXResourceServiceClient oRMXResClient = null;
            RMResource objRes = null;
            try
            {
                //oRMXResClient = new RMXResourceService.RMXResourceServiceClient();
                objRes = new RMResource();
                Dictionary<string, string> oDict = new Dictionary<string, string>();
                oDict = ResetControlsSearch("5");
                //sReturn = oRMXResClient.GetSearchResult(objRes, GetMessageTemplate().ToString(), int.Parse(AppHelper.ReadCookieValue("DsnId")), "5", iCatId, sDisplayCat, 0);
                objRes.ClientId = AppHelper.ClientId;
                objRes.Document = GetMessageTemplate().ToString();
                objRes.DataSourceId = AppHelper.ReadCookieValue("DsnId");
                objRes.SearchType = "5";
                objRes.CatId = iCatId;
                objRes.DisplayCat = sDisplayCat;
                objRes.TableId = iTableId;
                //sReturn = oRMXResClient.GetSearchResult(objRes, , , "7", 0, "", 0);
                sReturn = AppHelper.GetResponse<string>("RMService/Resource/searchresult", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                XElement eReturn = XElement.Parse(sReturn);
                lstSearchField.Items.Clear();
                foreach (XElement oElem in eReturn.Descendants("FieldList").Nodes())
                {
                    sResourceID = "0";
                    sFieldName = oElem.Value.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                    if (!string.IsNullOrEmpty(sFieldName) && (sFieldName != "NULL"))
                    {
                        sDispValue = oElem.Value.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                        sKey = GenerateResourcKey(oElem.LastAttribute.Value.Split(sDataSeparator, StringSplitOptions.None)[0].ToString());
                        sFirstAttrValue = oElem.FirstAttribute.Value;
                        if (oDict.ContainsKey(sKey))
                        {
                            sDictSelectedValue = oDict[sKey].ToString();
                            sResourceID = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[0].ToString();
                            sDispValue = sDictSelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                        }
                    }
                    else
                    {
                        tbxText.Text = string.Empty;
                        return;
                    }
                    sTempValue = sResourceID + "|^|" + sKey + "|^|" + sFirstAttrValue;
                    objItem = new ListItem(sDispValue, sTempValue);
                    lstSearchField.Items.Add(objItem);
                }
                lstSearchField.SelectedIndex = 0;
                tbxText.Text = lstSearchField.Items[0].Text;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //if (oRMXResClient != null)
                //{
                //    oRMXResClient = null;
                //}
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }
        public string GenerateResourcKey(string strRawKey)
        {
            string strKey = string.Empty;
            string temp = string.Empty;
            temp = strRawKey.Replace(" ", "");
            strKey = Regex.Replace(temp, "[^a-zA-Z0-9]+", "");
            return strKey;

        }
        //ksahu5-RMA-2176-End      
        protected void drpType_SelectedIndexChanged(object sender, EventArgs e)
        {           
            int iCatId = 0;
            int iTableId = 0;
            string sDisplayCat = string.Empty;
            bool isGroupSelected = false;            
            //ksahu5-RMA-2176-Start
            if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "5")
            {
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                lblSearchField.Visible = true;
                lstSearchField.Visible = true;
                //When Admin tracking search  is used 
                if (lstAdminTable.SelectedIndex >= 0)
                {
                    lblAdminTable.Visible = true;
                    lstAdminTable.Visible = true;
                    if (isGroupSelected)
                    {
                        iTableId = Convert.ToInt32(lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[3].ToString());
                        SelectSearchFields(iCatId, "", iTableId);
                    }
                    else
                    {
                        iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                        SelectSearchGroups(iCatId, iTableId);
                        lblFieldlst.Visible = true;
                        lstFieldList.Visible = true;
                        lblFieldlst.Text = "List of Search Groups:";
                        SelectSearchFields(iCatId, "", iTableId);
                    }

                }
                else
                {

                    if (lstFieldList.SelectedIndex >= 0)
                    {
                        if (iCatId == 20)
                        {
                            SelectAdminTrackingTable(iCatId);
                            iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                            SelectSearchGroups(iCatId, iTableId);
                            lblAdminTable.Visible = true;
                            lstAdminTable.Visible = true;
                            lblFieldlst.Visible = true;
                            lstFieldList.Visible = true;
                            lblFieldlst.Text = "List of Search Groups:";
                            SelectSearchFields(iCatId, "", iTableId);
                        }
                        else
                        {
                            sDisplayCat = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                            SelectSearchFields(iCatId, sDisplayCat, 0);
                        }
                    }
                    else
                    {
                        if (iCatId == 20)
                        {
                            SelectAdminTrackingTable(iCatId);
                            iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                            SelectSearchGroups(iCatId, iTableId);
                            lblAdminTable.Visible = true;
                            lstAdminTable.Visible = true;
                            lblFieldlst.Visible = true;
                            lstFieldList.Visible = true;
                            lblFieldlst.Text = "List of Search Groups:";
                            SelectSearchFields(iCatId, "", iTableId);
                        }
                        else
                        {
                            SelectSearchGroups(iCatId, 0);
                            lblFieldlst.Text = "List of Search Groups:";
                            lblFieldlst.Visible = true;
                            lstFieldList.Visible = true;
                            sDisplayCat = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                            SelectSearchFields(iCatId, sDisplayCat, 0);
                        }
                    }
                }
            }
            else if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "7")
            {
                lblAdminTable.Visible = false;
                lstAdminTable.Visible = false;                
                lblSearchField.Visible = false;
                lstSearchField.Visible = false;
                lstSearchField.Items.Clear();
                lblFieldlst.Visible = false;
                lstFieldList.Visible = false;
                lstFieldList.Items.Clear();
                tbxText.Text = drpPageList.SelectedItem.Text;
            }

            else if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "6")
            {
                lblFieldlst.Text = "List of Search Groups:";
                lblFieldlst.Visible = true;
                lstFieldList.Visible = true;
                isGroupSelected = true;
                lblSearchField.Visible = false;
                lstSearchField.Visible = false;
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                if (iCatId == 20)
                {
                    if (lstAdminTable.SelectedIndex >= 0)
                    {
                        iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                        SelectSearchGroups(iCatId, iTableId);

                    }
                    else
                    {
                        SelectAdminTrackingTable(iCatId);
                        iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                        SelectSearchGroups(iCatId, iTableId);
                    }                    
                    lblAdminTable.Visible = true;
                    lstAdminTable.Visible = true;                     
                }
                else
                {
                    SelectSearchGroups(iCatId, 0);
                }
                
                tbxText.Text = lstFieldList.SelectedItem.Text;
            }
            else if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "8")
            {
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                SelectAdminTrackingTable(iCatId);
                lblAdminTable.Visible = true;
                lstAdminTable.Visible = true;                
                lblFieldlst.Visible = false;
                lstFieldList.Visible = false;
                lblSearchField.Visible = false;
                lstSearchField.Visible = false;
                tbxText.Text = lstAdminTable.SelectedItem.Text;
            }
            else
            {
                 ResetValueOfControls();
            }

        }
        protected void lstAdminTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iCatId = 0;
            int iTableId = 0;
            if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "8")
            {
                tbxText.Text = lstAdminTable.SelectedItem.Text;
                tbxText.Enabled = true;
                lstAdminTable.Focus();
            }
            if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "6")
            {
                lblFieldlst.Text = "List of Search Groups:";
                lblFieldlst.Visible = true;
                lstFieldList.Visible = true;
                lblSearchField.Visible = false;
                lstSearchField.Visible = false;
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                SelectSearchGroups(iCatId, iTableId);
                tbxText.Text = lstFieldList.SelectedItem.Text;
            }
            else if(drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "5")
            {
                lblFieldlst.Text = "List of Search Groups:";
                lblFieldlst.Visible = true;
                lstFieldList.Visible = true;
                lblSearchField.Visible = true;
                lstSearchField.Visible = true;
                iCatId = Convert.ToInt32(drpPageList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                iTableId = Convert.ToInt32(lstAdminTable.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                SelectSearchGroups(iCatId, iTableId);
                SelectSearchFields(iCatId, "", iTableId);
                tbxText.Text = lstSearchField.SelectedItem.Text;
            }
            
        }
        protected void lstSearchField_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpErrorsOrPages.SelectedValue == "3" && drpType.SelectedValue == "5")
            {
                tbxText.Text = lstSearchField.SelectedItem.Text;
                tbxText.Enabled = true;
                lstFieldList.Focus();
            }
        }
        //ksahu5-RMA-2176-End
        private void ResetValueOfControls()
        {
            //RMXResourceServiceClient oRMXResClient = null;
            RMResource objRes = null;
            string sReturn = string.Empty;
            ListItem objItem = null;          
            string sTempValue = string.Empty;
            string sMDIMenu = string.Empty;
            string sSelText = string.Empty;
            bool bSearchWithId = false;
            int iSearchId = 0;
            bool bSearchSuccess = false;
            //RMA 2176 Start
            string searchType = string.Empty;
            int iCatId = 0;
            string sKey = string.Empty;
            string sCategoryKey = string.Empty;
            string sGroupKey = string.Empty;
            string sDisplayCat = string.Empty;           
            int iTableId = 0;
            int index = 0;
            string sAdminkey = string.Empty;
            //RMA 2176 End
            try
            {
                //oRMXResClient = new RMXResourceServiceClient();
                objRes = new RMResource();
                objRes.ClientId = AppHelper.ClientId;
                objRes.PageId = drpPageList.SelectedValue;
                objRes.LanguageCode = lstLangList.SelectedValue;

                if (string.Compare(PostBackSender.Value, "Search") == 0)
                {
                    objRes.ResourceType = drpType.SelectedValue;
                    bSearchWithId = int.TryParse(txtSearch.Text.Trim(), out  iSearchId);
                    if (bSearchWithId)//Search on the basis of Resource Id
                    {
                        objRes.ResourceId = txtSearch.Text.Trim();
                    }
                    else//Search on the basis of text
                    {
                        objRes.ResourceId = "0";
                        objRes.ResourceValue = txtSearch.Text.Trim();
                    }
                    sReturn =  GetMessageTemplate().ToString();
                    //bSearchSuccess = oRMXResClient.GetSearchInfo(objRes,ref sReturn);
                    objRes.Document = GetMessageTemplate().ToString();
                    string sOut = AppHelper.GetResponse<string>("RMService/Resource/search", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                    bSearchSuccess = Convert.ToBoolean(sOut.Split(sSeparator, StringSplitOptions.None)[0]);
                    sReturn = sOut.Split(sSeparator, StringSplitOptions.None)[1];
                    PostBackSender.Value = "";
                }
                //RMA 2176 Start
                else if ((drpErrorsOrPages.SelectedValue == "3") && (!string.IsNullOrEmpty(hdnSearchKey.Value)))
                {
                    lblResrcSet.Text = "Search Caterories :";
                    lblResrcSet.Visible = true;
                    drpPageList.Visible = true;
                    SelectSearchCategories();//By default when "Select" is selected the search categories are populated.
                    sCategoryKey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[0].ToString();
                    iCatId = Convert.ToInt32(sCategoryKey.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());

                    searchType = drpType.SelectedValue;
                    switch (searchType)
                    {
                        case "5":
                            //sCategoryKey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[0].ToString();
                            //iCatId = Convert.ToInt32(sCategoryKey.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                            lblSearchField.Visible = true;
                            lstSearchField.Visible = true;
                            lblFieldlst.Visible = true;
                            lstFieldList.Visible = true;
                            if (iCatId == 20)
                            {
                                lblAdminTable.Visible = true;
                                lstAdminTable.Visible = true;
                                SelectionOnLanguageChange("7");
                                SelectAdminTrackingTable(iCatId);
                                SelectionOnLanguageChange("8");
                                sAdminkey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[3].ToString();
                                if (!string.IsNullOrEmpty(sAdminkey))
                                {
                                    iTableId = Convert.ToInt32(sAdminkey.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                                    SelectSearchGroups(iCatId, iTableId);
                                }
                                SelectionOnLanguageChange("6");
                                SelectSearchFields(iCatId, "", iTableId);
                                SelectionOnLanguageChange("5");

                            }
                            else
                            {
                                lblAdminTable.Visible = false;
                                lstAdminTable.Visible = false;                                
                                SelectSearchGroups(iCatId, 0);
                                sGroupKey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[1].ToString();
                                sDisplayCat = sGroupKey.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                                SelectSearchFields(iCatId, sDisplayCat, 0);
                                SelectionOnLanguageChange("5");
                                SelectionOnLanguageChange("6");
                                SelectionOnLanguageChange("7");
                            }
                            tbxText.Text = lstSearchField.SelectedItem.Text;
                            tbxText.Enabled = true;
                            lstFieldList.Focus();
                            break;
                        case "6":
                            index = 0;
                            lblFieldlst.Text = "List of Search Groups:";
                            lblFieldlst.Visible = true;
                            lstFieldList.Visible = true;
                            lblSearchField.Visible = false;
                            lstSearchField.Visible = false;
                            //sCategoryKey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[0].ToString();
                            //iCatId = Convert.ToInt32(sCategoryKey.Split(sDataSeparator, StringSplitOptions.None)[1].ToString());
                            SelectionOnLanguageChange("7");
                            if (iCatId == 20)
                            {
                                lblAdminTable.Visible = true;
                                lstAdminTable.Visible = true;
                                SelectAdminTrackingTable(iCatId);
                                SelectionOnLanguageChange("8");
                                sAdminkey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[3].ToString();
                                if (!string.IsNullOrEmpty(sAdminkey))
                                {
                                    iTableId = Convert.ToInt32(sAdminkey.Split(sDataSeparator, StringSplitOptions.None)[2].ToString());
                                    SelectSearchGroups(iCatId, iTableId);
                                }
                             }
                            else
                            {
                                SelectSearchGroups(iCatId, 0);
                            } 
                            SelectionOnLanguageChange("6");
                            tbxText.Text = lstFieldList.SelectedItem.Text;
                            tbxText.Enabled = true;
                            lstFieldList.Focus();
                            break;
                        case "7":
                            lblSearchField.Visible = false;
                            lstSearchField.Visible = false;
                            lblFieldlst.Visible = false;
                            lstFieldList.Visible = false;
                            SelectionOnLanguageChange(searchType);
                            tbxText.Text = drpPageList.SelectedItem.Text;
                            tbxText.Enabled = true;
                            break;
                        //jira 4327 start
                        case "8":
                            lblSearchField.Visible = false;
                            lstSearchField.Visible = false;
                            lblFieldlst.Visible = false;
                            lstFieldList.Visible = false;
                            lblAdminTable.Visible = true;
                            lstAdminTable.Visible = true;
                            lstAdminTable.Items.Clear();
                            SelectAdminTrackingTable(iCatId);
                            SelectionOnLanguageChange("8");
                            tbxText.Text = lstAdminTable.SelectedItem.Text;
                            break;
                        //jira 4327 end
                    }
                    hdnSearchKey.Value = string.Empty;
                    return;
                    
                }
                //RMA 2176 End
                else
                {
                    if (!string.IsNullOrEmpty(lstFieldList.SelectedValue))
                        sSelText = lstFieldList.SelectedValue.Split(sDataSeparator, StringSplitOptions.None)[2];
                    
                    //objRes.LanguageCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();

                    if (drpErrorsOrPages.SelectedValue == "0")//errors
                    {
                        objRes.Document = GetMessageTemplate().ToString();
                        sReturn = AppHelper.GetResponse<string>("RMService/Resource/globalinfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                        //sReturn = oRMXResClient.GetGlobalInfo(objRes, GetMessageTemplate().ToString());
                    }
                    else if (drpErrorsOrPages.SelectedValue == "1")//page
                    {
                        if (int.Parse(drpPageList.SelectedValue) >= 10000)
                        {
                            objRes.IsFDMPage = Boolean.TrueString;
                            objRes.User = HttpContext.Current.User.Identity.Name;
                            objRes.DataSourceId = AppHelper.ReadCookieValue("DsnId");
                        }
                        //sReturn = oRMXResClient.GetPageInfo(objRes, GetMessageTemplate().ToString());
                        objRes.Document = GetMessageTemplate().ToString();
                        sReturn = AppHelper.GetResponse<string>("RMService/Resource/pageinfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                    }
                    else if (drpType.SelectedValue == "9") //mdi
                    {
                        //to do :take it to design
                        // tbxSearchField.Width = Unit.Pixel(650);
                        //tbxSearchField.Height = Unit.Pixel(300);
                        tbxSearchField.Visible = true;
                        tbxSearchField.Font.Size = FontUnit.Point(8);
                        objRes.LanguageCode = lstLangList.SelectedValue;
                        objRes.Token = AppHelper.Token;
                        objRes.ResourceValue = "MDIMENU_XML";
                        sMDIMenu = AppHelper.GetResponse<string>("RMService/Resource/mdi", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes); 
                        //sMDIMenu = oRMXResClient.GetMDIMenuXML(oRMXResClient.GetConnectionstring("ViewDataSource"), int.Parse(lstLangList.SelectedValue));
                        if (string.IsNullOrEmpty(sMDIMenu))
                        {
                            objRes.LanguageCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
                            sMDIMenu = AppHelper.GetResponse<string>("RMService/Resource/mdi", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes); 
                            //sMDIMenu = oRMXResClient.GetMDIMenuXML(oRMXResClient.GetConnectionstring("ViewDataSource"), int.Parse(AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString()));
                        }
                        XmlDocument objDoc = new XmlDocument();
                        objDoc.LoadXml(sMDIMenu);
                        #region commented 
                        //To change the Header node values through Localisation Setup
                        //foreach (XmlNode oElem in objDoc.SelectNodes("//item"))
                        //{
                        //    try
                        //    {
                        //        if (oElem.Attributes["sysName"] != null)
                        //        {
                        //            if (oElem.Attributes["sysName"].Value == "DocumentRoot" ||
                        //                oElem.Attributes["sysName"].Value == "PolicyRoot" ||
                        //                oElem.Attributes["sysName"].Value == "DiariesRoot" ||
                        //                oElem.Attributes["sysName"].Value == "FundsRoot" ||
                        //                oElem.Attributes["sysName"].Value == "MaintenanceRoot" ||
                        //                oElem.Attributes["sysName"].Value == "MyWorkRoot" ||
                        //                oElem.Attributes["sysName"].Value == "ReportsRoot" ||
                        //                oElem.Attributes["sysName"].Value == "SearchRoot" ||
                        //                oElem.Attributes["sysName"].Value == "SecurityRoot" ||
                        //                oElem.Attributes["sysName"].Value == "UserDocumentsRoot" ||
                        //                oElem.Attributes["sysName"].Value == "utilitiesRoot" ||
                        //                oElem.Attributes["sysName"].Value == "HelpRoot" ||
                        //                oElem.Attributes["sysName"].Value == "OtherRoot")
                        //            {
                        //                oElem.Attributes.RemoveNamedItem("title");
                        //            }
                        //        }
                        //    }
                        //    catch
                        //    {
                        //    }
                        //}
                        #endregion
                        tbxSearchField.Text = FormatXml(objDoc.OuterXml);
                        return;
                    }
                    else if (drpType.SelectedValue == "10") // child mdi
                    {
                        tbxSearchField.Visible = true;

                        tbxSearchField.Font.Size = FontUnit.Point(8);
                        objRes.LanguageCode = lstLangList.SelectedValue;
                        objRes.Token = AppHelper.Token;
                        objRes.ResourceValue = "CHILDSCREEN_XML";
                        sMDIMenu = AppHelper.GetResponse<string>("RMService/Resource/childscreen", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                        //sMDIMenu = oRMXResClient.GetMDIMenuXML(oRMXResClient.GetConnectionstring("ViewDataSource"), int.Parse(lstLangList.SelectedValue));
                        if (string.IsNullOrEmpty(sMDIMenu))
                        {
                            objRes.LanguageCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
                            sMDIMenu = AppHelper.GetResponse<string>("RMService/Resource/childscreen", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);
                        }
                        XmlDocument objDoc = new XmlDocument();
                        objDoc.LoadXml(sMDIMenu);
                        tbxSearchField.Text = FormatXml(objDoc.OuterXml);
                        return;
                    }
                }
                XElement eReturn = XElement.Parse(sReturn);
                lstFieldList.Items.Clear();
                foreach (XElement oElem in eReturn.Descendants("FieldList").Nodes())
                {
                    if (oElem.FirstAttribute.Value.Split(sDataSeparator, StringSplitOptions.None)[1] == drpType.SelectedValue)
                    {
                        sTempValue = oElem.LastAttribute.Value;
                        //MITS id: 31770 - govind - Start
                        //if (drpType.SelectedValue == "3" || drpType.SelectedValue == "1" || drpType.SelectedValue == "0")
                        if (drpType.SelectedValue == "3" || drpType.SelectedValue == "1" )
                        {
                            //MITS id: 31770 - govind - End
                            int iNoOfSpaces = 8;
                            sTempValue = oElem.FirstAttribute.Value.Split(sDataSeparator, StringSplitOptions.None)[0].PadLeft(iNoOfSpaces,' ') + ": " + oElem.LastAttribute.Value;
                            if (sTempValue.Length > 114)
                                sTempValue = sTempValue.Substring(0, 110) + "...";    
                        }
                        else
                        {
                            sTempValue = oElem.LastAttribute.Value;
                        }
                        
                        objItem = new ListItem(sTempValue, oElem.FirstAttribute.Value.ToString() + "|^|" + oElem.LastAttribute.Value.ToString() + "|^|" + oElem.Value);
                      //rsharma220 MITS 31131 Start
					    if(string.IsNullOrEmpty(oElem.Value) == false)
                        {
                            lstFieldList.Items.Add(objItem);
                        }
						//rsharma220 MITS 31131 End
                   }
                }
                objItem = null;
                //Sorting of list
                //Deb Commented for now not required.
                //ArrayList arList = new ArrayList();
                //ArrayList arObjList = new ArrayList();
                //foreach (ListItem o in lstFieldList.Items)
                //{
                //    arList.Add(o.Text);
                //}
                //arList.Sort();
                //foreach (object o in arList)
                //{
                //    if (!string.IsNullOrEmpty(o.ToString()))
                //    {
                //        ListItem oB = lstFieldList.Items.FindByText(o.ToString());
                //        arObjList.Add(oB);
                //    }
                //}
                //lstFieldList.Items.Clear();
                //foreach (ListItem o in arObjList)
                //{
                //    lstFieldList.Items.Add(o);
                //}
                //Deb Commented for now not required.
                if (!string.IsNullOrEmpty(sSelText))
                {
                    objItem = lstFieldList.Items.FindByText(sSelText);
                }

                if (objItem == null && lstFieldList.Items.Count>0)
                {
                    objItem = lstFieldList.Items[0];
                }
                if (objItem != null)
                {
                    objItem.Selected = true;
                    tbxText.Text = objItem.Value.Split(sDataSeparator, StringSplitOptions.None)[2];
                }
                else
                    tbxText.Text = "";
                lstFieldList.Focus();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //if (oRMXResClient != null)
                //{
                //    oRMXResClient = null;
                //}
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PostBackSender.Value = "Search";
            ResetValueOfControls();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            ResetValueOfControls();
        }
        //ksahu5-RMA-2176-Start
        public Dictionary<string, string> ResetControlsSearch(string type)
        {
            Dictionary<string, string> oDict = new Dictionary<string, string>();
            //RMXResourceService.RMXResourceServiceClient oRMXResClient = null;
            RMResource objRes = null;
            string sReturn = string.Empty;
            //oRMXResClient = new RMXResourceService.RMXResourceServiceClient();
            objRes = new RMResource();
            objRes.PageId = pageID;
            objRes.ClientId = AppHelper.ClientId;
            objRes.LanguageCode = lstLangList.SelectedValue;
            objRes.Document = GetMessageTemplate().ToString();
            try
            {
                //sReturn = oRMXResClient.GetPageInfo(objRes, GetMessageTemplate().ToString());
                sReturn = AppHelper.GetResponse<string>("RMService/Resource/pageinfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes);

                XElement eReturn = XElement.Parse(sReturn);
                foreach (XElement oElem in eReturn.Descendants("FieldList").Nodes())
                {
                    if (oElem.FirstAttribute.Value.Split(sDataSeparator, StringSplitOptions.None)[1].ToString() == type)
                    {
                        oDict.Add(oElem.Value, oElem.FirstAttribute.Value.ToString() + "|^|" + oElem.LastAttribute.Value.ToString() + "|^|" + oElem.Value);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //if (oRMXResClient != null)
                //{
                //    oRMXResClient = null;
                //}
                if (objRes != null)
                {
                    objRes = null;
                }
            }
            return oDict;
        }
        //ksahu5-RMA-2176-Start
        public int GetIndexBasedOnKey(ListItemCollection p_Data, string p_Key)
        {
            int iIndex = 0;
            foreach (ListItem oItem in p_Data)
            {
                if (oItem.Value.Contains(p_Key))
                {
                    break;
                }
                iIndex++;
            }
            return iIndex;
        }        
        private void SelectionOnLanguageChange(string stype)
        {
            string sKey = string.Empty;
            int index = 0;
            string sCategoryKey = string.Empty;
            string sGroupKey = string.Empty;
            string sFieldKey = string.Empty;
            string sAdminkey = string.Empty;

            switch (stype)
            {
                case "5":

                    sFieldKey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[2].ToString();
                    if (!string.IsNullOrEmpty(sFieldKey))
                    {
                        sKey = sFieldKey.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                        lstSearchField.SelectedIndex = GetIndexBasedOnKey(lstSearchField.Items, sKey);
                    }                   
                    break;
                case "6":
                    sGroupKey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[1].ToString();
                    if (!string.IsNullOrEmpty(sGroupKey))
                    {
                        sKey = sGroupKey.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                        lstFieldList.SelectedIndex = GetIndexBasedOnKey(lstFieldList.Items, sKey);
                    }
                    break;
                case "7":
                    sCategoryKey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[0].ToString();
                    if (!string.IsNullOrEmpty(sCategoryKey))
                    {
                        sKey = sCategoryKey.Split(sDataSeparator, StringSplitOptions.None)[2].ToString();
                        drpPageList.SelectedIndex = GetIndexBasedOnKey(drpPageList.Items, sKey);
                    }
                    break;
                    //jira 4327 start
                case "8":
                    sAdminkey = hdnSearchKey.Value.Split(sSearchSeparator, StringSplitOptions.None)[3].ToString();
                    if (!string.IsNullOrEmpty(sAdminkey))
                    {
                        sKey = sAdminkey.Split(sDataSeparator, StringSplitOptions.None)[1].ToString();
                        lstAdminTable.SelectedIndex = GetIndexBasedOnKey(lstAdminTable.Items, sKey);
                    }
                    break;
                    //jira 4327 end
            }
        }
        //ksahu5-RMA-2176-End
    }
}
