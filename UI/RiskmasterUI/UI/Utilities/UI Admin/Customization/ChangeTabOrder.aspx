﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ChangeTabOrder.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.ChangeTabOrder" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Tab Order</title>
    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>

    <script language="javaScript" src="../../../../Scripts/Utilities.js" type="text/javascript"></script>

</head>
<body class="10pt" onload="unCheckRadioButtons_ChangeTabOrderPage();" onunload="passPortletXML_ChangeTabOrderPage();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="formtitle">
    </div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Change Tab Order
                                </td>
                            </tr>
                            <tr>
                                <td width="570px">
                                    <b></b>
                                    <table width="100%">
                                        <tr>
                                            <td width="80%">
                                                <%--<div style="&#xa; width: 570px; height: 100px; &#xa; &#xa;">--%>
                                                <div style="overflow: auto; width: 100%; position: relative; width: 570px; height: 200px">
                                                    <table width="100%" class="singleborder" cellspacing="0" cellpadding="0">
                                                        <asp:GridView ID="gvChangetabOrderGrid" runat="server" AutoGenerateColumns="false"
                                                            AllowPaging="false" AllowSorting="false" Width="100%" ShowHeader="true" GridLines="Both">
                                                            <RowStyle CssClass="" />
                                                            <AlternatingRowStyle CssClass="rowdark2" />
                                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headertext2">
                                                                    <ItemTemplate>
                                                                        <%--<input type="radio" id="selectPortlet" value='<%# DataBinder.Eval(Container.DataItem,"RowId")%>' />
                                                                                                       <asp:RadioButtonList runat="server">--%>
                                                                        <input type="radio" id='<%# DataBinder.Eval(Container.DataItem,"RowID")%>' name="portal" />
                                                                        <%--                                                     <asp:ListItem />
                                                                        </asp:RadioButtonList>--%>
                                                                        <asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"RowId") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Sequence Id" DataField="SequenceId" ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="Name of Portlet" DataField="Name" ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="URL of Portlet" DataField="urlPortlet" ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headertext2" FooterStyle-CssClass="headertext2">
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </table>
                                                </div>
                                            </td>
                                            <td width="2%"></td>
                                            <td width="5%" >
                                            <asp:ImageButton ImageUrl="~/Images/up.gif" runat="server" ID="imgMoveUp" 
                                                    onclick="imgMoveUp_Click"/>
                                            <asp:ImageButton ImageUrl="~/Images/down.gif" runat="server" ID="imgMoveDown" 
                                                    onclick="imgMoveDown_Click"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" name="$node^27" value="" style="display: none" id="selectedusername">
                                </td>
                                <td>
                                    <input type="text" name="$node^8" value="" style="display: none" id="hdnaction">
                                </td>
                                <td>
                                    <input type="text" name="$node^23" value="" style="display: none" id="sequenceid">
                                </td>
                                <td>
                                    <input type="text" name="$node^24" value="" style="display: none" id="direction">
                                </td>
                                <td>
                                    <input type="text" name="$node^25" value="" style="display: none" id="dltallflag">
                                </td>
                            </tr>
<%--                            <tr>
                                <td colspan="2">
                                    <asp:Button runat="server" Text="Move Up" class="button" ID="btnMoveUp" Style="width: 80"
                                        TabIndex="1" OnClick="btnMoveUp_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button runat="server" Text="Move Down" class="button" ID="btnMoveDown" Style="width: 80"
                                        TabIndex="2" OnClick="btnMoveDown_Click" />
                                </td>
                            </tr>--%>
                            <tr>
                                <td colspan="2">
                                    <input type="submit" name="$action^setvalue%26node-ids%26%26content%26formbutton"
                                        value="Close" class="button" onclick="window.document.getElementById('SortChanged').value=true;window.opener.document.getElementById('DataChanged').value=true; window.opener.document.getElementById('hdnaction').value='';window.close();return false; ; return XFormHandler('','','button')"
                                        id="btnClose" style="width: 80" tabindex="3">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="CommonPortletXML" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="SelectedID" value="0" />
    <asp:TextBox Style="display: none" runat="server" ID="SortChanged" Text="false"/>
    <asp:TextBox Style="display: none" runat="server" ID="GridRowCount" Text="false"/>
    </form>
</body>
</html>
