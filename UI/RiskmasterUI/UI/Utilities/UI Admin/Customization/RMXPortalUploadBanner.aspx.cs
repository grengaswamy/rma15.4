﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.AppHelpers;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Cache;
using Riskmaster.Models;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class RMXPortalUploadBanner : System.Web.UI.Page
    {
        XmlDocument objXmlDoc = new XmlDocument();
        XmlDocument objXmlDocTemp = new XmlDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            save.Attributes.Add("onclick", "javascript:return validateImageUpload();");
            if(!IsPostBack)
            {
                //mits 23786: RMX portal setting should be saved to database instead of web server       
                PortalData oPortalData = new PortalData();
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                if (Request.QueryString["src"] != null)
                {
                    //oPortalData.Sql = "SELECT FILE_CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                    //oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    //byte[] filecontent = AppHelper.GetResponse<byte[]>("RMService/Portal/filecontent", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData);
                    LoadRMALogo();
                    return;
                }
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 
                objXmlDoc.LoadXml(content);
                //imgPortal.ImageUrl = "~/Images/" + AppHelper.ClientId + "_rmx_logo.gif";
                
                //objXmlDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
                txtbkcolor.Text = objXmlDoc.SelectSingleNode("/PortalSettings/PortalTheme/BackgroundColor").InnerText;
                txtLoginLabelFontColor.Text = objXmlDoc.SelectSingleNode("/PortalSettings/PortalTheme/LoginLabelFontColor").InnerText;
                txtLoginValueFontColor.Text = objXmlDoc.SelectSingleNode("/PortalSettings/PortalTheme/LoginValueFontColor").InnerText;
                txtLogoutFontColor.Text = objXmlDoc.SelectSingleNode("/PortalSettings/PortalTheme/LogoutFontColor").InnerText;
				//zmohammad MITs 34734
                if (objXmlDoc.SelectSingleNode("/PortalSettings/PortalTheme/ForgotPasswordColor") != null)
                {
                    txtForgotPasswordColor.Text = objXmlDoc.SelectSingleNode("/PortalSettings/PortalTheme/ForgotPasswordColor").InnerText;
                }
                oPortalData = null;
            } // if

            lblbkcolor.Style.Add(HtmlTextWriterStyle.BackgroundColor, txtbkcolor.Text);
            lblLoginLabelFontColor.Style.Add(HtmlTextWriterStyle.BackgroundColor, txtLoginLabelFontColor.Text);
            lblLoginValueFontColor.Style.Add(HtmlTextWriterStyle.BackgroundColor, txtLoginValueFontColor.Text);
            lblLogoutFontColor.Style.Add(HtmlTextWriterStyle.BackgroundColor, txtLogoutFontColor.Text);
            lblForgotPasswordColor.Style.Add(HtmlTextWriterStyle.BackgroundColor, txtLogoutFontColor.Text); //zmohammad MITs 34734
        }

        private void LoadRMALogo()
        {
            try
            {
                byte[] filecontent = RMXPortalHelper.PortalHomeImage;
                if (filecontent != null)
                {
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "images/jpeg";
                    Response.AddHeader("content-disposition", "attachment;filename=image1");
                    Response.BinaryWrite(filecontent);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    imgPortal.ImageUrl = "~/Images/" + Request.QueryString["src"].ToString();
                }
            }
            catch
            {
            }
        }

        //protected void btnUpload_Click(object sender, EventArgs e)
        //{
        //    if (btnfileUpload.HasFile)
        //        try
        //        {
        //            //string sImageFileName = btnfileUpload.FileName;
        //            btnfileUpload.SaveAs(Server.MapPath("~/Images/") + "rmx_logo.gif");
        //            //btnfileUpload.SaveAs(Server.MapPath("~/Images/") + sImageFileName);
        //            //imgPortal.ImageUrl = Server.MapPath("~/Images/") + "rmx_logo.gif";
        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "closeWindow", "window.close();", true);
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorHelper.logErrors(ex);
        //            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //            err.Add(ex, BusinessAdaptorErrorType.SystemError);
        //            ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
        //        }

        //}

        //protected void btnSaveBkColor_Click(object sender, EventArgs e)
        //{
        //    objXmlDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
        //    if(txtbkcolor.Text !="")
        //        objXmlDoc.SelectSingleNode("/PortalSettings/BackgroundColor").InnerText = txtbkcolor.Text;
        //}

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //mits 23786: RMX portal setting should be saved to database instead of web server       
                PortalData oPortalData = new PortalData();
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 
               
                objXmlDoc.LoadXml(content);

                //objXmlDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
                objXmlDocTemp = objXmlDoc;
                objXmlDoc = null;
                if (txtbkcolor.Text != "")
                    objXmlDocTemp.SelectSingleNode("/PortalSettings/PortalTheme/BackgroundColor").InnerText = txtbkcolor.Text;
                if (txtLoginLabelFontColor.Text != "")
                {
                    objXmlDocTemp.SelectSingleNode("/PortalSettings/PortalTheme/LoginLabelFontColor").InnerText = txtLoginLabelFontColor.Text;
                } // if
                if (txtLoginValueFontColor.Text != "")
                {
                    objXmlDocTemp.SelectSingleNode("/PortalSettings/PortalTheme/LoginValueFontColor").InnerText = txtLoginValueFontColor.Text;
                } // if
                if (txtLogoutFontColor.Text != "")
                {
                    objXmlDocTemp.SelectSingleNode("/PortalSettings/PortalTheme/LogoutFontColor").InnerText = txtLogoutFontColor.Text;
                } // if
				
				//zmohammad MITs 34734
                if (txtForgotPasswordColor.Text != "")
                {
                    if (objXmlDocTemp.SelectSingleNode("/PortalSettings/PortalTheme/ForgotPasswordColor") != null)
                    {
                        objXmlDocTemp.SelectSingleNode("/PortalSettings/PortalTheme/ForgotPasswordColor").InnerText = txtForgotPasswordColor.Text;
                    }
                    else {
                        XmlNode childElement = objXmlDocTemp.CreateNode(XmlNodeType.Element, "ForgotPasswordColor",null);
                        childElement.InnerText = txtForgotPasswordColor.Text;
                        XmlNode parentNode = objXmlDocTemp.SelectSingleNode("/PortalSettings/PortalTheme");
                        parentNode.InsertAfter(childElement, parentNode.LastChild);
                    }
                }
                // akaushik5 Changed for MITS 32757 Starts
                //PortalClient.ContentUpdate(objXmlDocTemp.InnerXml.ToString(), "PORTALINFO");

                oPortalData.Content = objXmlDocTemp.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                oPortalData.Token = AppHelper.Token;
                if (btnfileUpload.HasFile)
                {
                    //string sImageFileName = btnfileUpload.FileName;
                    //btnfileUpload.SaveAs(Server.MapPath("~/Images/") + AppHelper.ClientId + "_rmx_logo.gif");
                    byte[] imageBytes = new byte[btnfileUpload.PostedFile.InputStream.Length + 1];
                    btnfileUpload.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length);
                    oPortalData.FileContent = imageBytes;
                    //btnfileUpload.SaveAs(Server.MapPath("~/Images/") + sImageFileName);
                    //imgPortal.ImageUrl = Server.MapPath("~/Images/") + "rmx_logo.gif";
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "closeWindow", "window.close();", true);
                } // if
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 
               

                //PortalClient.ContentUpdate(objXmlDocTemp.InnerXml.ToString(), "CUSTOMIZE_PORTALINFO");
                // akaushik5 Changed for MITS 32757 Ends
                //PortalClient.Close();

                //HttpContext.Current.Cache.Remove("RMXPortalSettings");                

                CacheCommonFunctions.RemoveValueFromCache("RMXPortalSettings", AppHelper.ClientId);
                CacheCommonFunctions.RemoveValueFromCache("RMXPortalImage", AppHelper.ClientId);

                //objXmlDocTemp.Save(@Server.MapPath("~/App_Data/Customization.xml"));
                if (btnfileUpload.HasFile)
                {
                    //string sImageFileName = btnfileUpload.FileName;
                    //btnfileUpload.SaveAs(Server.MapPath("~/Images/") + AppHelper.ClientId + "_rmx_logo.gif");
                    //btnfileUpload.SaveAs(Server.MapPath("~/Images/") + sImageFileName);
                    //imgPortal.ImageUrl = Server.MapPath("~/Images/") + "rmx_logo.gif";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "closeWindow", "window.close();", true);
                } // if
            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
