﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 05/23/2014 | 34270  | aahuja21   | Added check memo in payment history gridview
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class HeaderConfig : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                chkNotRollable.Checked = false;//jira : 4282
                chkNotRoutable.Checked = false;//jira :4282
                LoadConfigDetails();
            }
        }

        protected void btnSaveDiaryConfig_Click(object sender, EventArgs e)
        {
            SetDiaryConfigDetails();
            LoadConfigDetails();
            // aravi5 RMA-12084: Chrome Specific: When saving congif settings for transaction history, on saving we are moved to the other tab starts
            //ClientScript.RegisterStartupScript(this.GetType(), "form.js", "tabChangeObj(DiaryConfig);", true);
            string tabname = "DiaryConfig";
            ClientScript.RegisterStartupScript(this.GetType(), "form.js", string.Format("tabChange('{0}')", tabname), true);
            // aravi5 RMA-12084: Chrome Specific: When saving congif settings for transaction history, on saving we are moved to the other tab ends
        }

        protected void btnSaveProgNotesConfig_Click(object sender, EventArgs e)
        {
            SetProgNotesConfigDetails();
            LoadConfigDetails();
            // aravi5 RMA-12084: Chrome Specific: When saving congif settings for transaction history, on saving we are moved to the other tab starts
            //ClientScript.RegisterStartupScript(this.GetType(), "form.js", "tabChangeObj(ProgNotesConfig);", true);
            string tabname = "ProgNotesConfig";
            ClientScript.RegisterStartupScript(this.GetType(), "form.js", string.Format("tabChange('{0}')", tabname), true);
            // aravi5 RMA-12084: Chrome Specific: When saving congif settings for transaction history, on saving we are moved to the other tab ends
        }

        protected void btnSavePayHisConfig_Click(object sender, EventArgs e)
        {
            SetPayHisConfigDetails();
            LoadConfigDetails();
            // aravi5 RMA-12084: Chrome Specific: When saving congif settings for transaction history, on saving we are moved to the other tab starts
            //ClientScript.RegisterStartupScript(this.GetType(), "form.js", "tabChangeObj(PayHisConfig);", true);
            string tabname = "PayHisConfig";
            ClientScript.RegisterStartupScript(this.GetType(), "form.js", string.Format("tabChange('{0}')", tabname), true);
            // aravi5 RMA-12084: Chrome Specific: When saving congif settings for transaction history, on saving we are moved to the other tab ends
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement SetConfigMessageTemplate()
        {
            //WWIG GAP20A - agupta298 - MITS 36804 - Added Diary Supplemental - JIRA - 4691
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>HeaderConfigAdaptor.SetHeaderConfig</Function>
              </Call>
              <Document>
                <Config>
                 <DiaryConfig selected='0'>
                          <Priority></Priority> 
                          <Text></Text> 
                          <WorkActivity></WorkActivity>
                          <NotRoutable></NotRoutable >
                          <NotRollable></NotRollable>
                          <Department></Department> 
                          <Due></Due> 
                          <AttachedRecord></AttachedRecord> 
<ParentRecord></ParentRecord>
                          <Claimant></Claimant> 
                          <ClaimStatus></ClaimStatus> 
                          <AssignedUser></AssignedUser>
                          <AssigningUser></AssigningUser>
                          <PriorityHeader></PriorityHeader> 
                          <TextHeader></TextHeader> 
                          <WorkActivityHeader></WorkActivityHeader> 
                          <NotRoutableHeader></NotRoutableHeader> 
                          <NotRollableHeader></NotRollableHeader> 
                          <DepartmentHeader></DepartmentHeader> 
                          <DueHeader></DueHeader> 
                          <AttachedRecordHeader></AttachedRecordHeader> 
<ParentRecordHeader></ParentRecordHeader>
                          <ClaimantHeader></ClaimantHeader> 
                          <ClaimStatusHeader></ClaimStatusHeader> 
                          <AssignedUserHeader></AssignedUserHeader> 
                          <AssigningUserHeader></AssigningUserHeader> 
                          <DiarySupplemental></DiarySupplemental> 
                    </DiaryConfig>
                    <ProgNotesConfig selected='0'>
                          <Activity></Activity> 
                          <NoteType></NoteType> 
                          <NoteText></NoteText> 
                          <AttachedTo></AttachedTo> 
                          <EnteredBy></EnteredBy> 
                          <DateCreated></DateCreated> 
                          <TimeCreated></TimeCreated> 
                          <UserType></UserType> 
                          <Subject></Subject>
                          <ActivityHeader></ActivityHeader> 
                          <NoteTypeHeader></NoteTypeHeader> 
                          <NoteTextHeader></NoteTextHeader> 
                          <AttachedToHeader></AttachedToHeader> 
                          <EnteredByHeader></EnteredByHeader> 
                          <DateCreatedHeader></DateCreatedHeader> 
                          <TimeCreatedHeader></TimeCreatedHeader> 
                          <UserTypeHeader></UserTypeHeader> 
                          <SubjectHeader></SubjectHeader>
                    </ProgNotesConfig>
                    <PayHisConfig selected='0'>
                              <Control></Control> 
                              <Check></Check>
                              <TransDate></TransDate>
                              <Type></Type> 
                              <Cleared></Cleared> 
                              <Void></Void>
                              //Added by swati MITS # 33431
                              <StopPay></StopPay> 
                              <Status></Status> 
                              <Payee></Payee> 
                              <CheckAmount></CheckAmount>
                              <FromDate></FromDate>
                              <ToDate></ToDate> 
                              <Invoice></Invoice> 
                              <TransactionType></TransactionType> 
                              <SplitAmount></SplitAmount> 
                              <User></User> 
                              <CheckDate></CheckDate> 
                              <CombinedPay></CombinedPay> 
                              <IsFirstFinal></IsFirstFinal>
                              <PolicyName></PolicyName>
                              <CoverageType></CoverageType>
                              <Unit></Unit>
                              <LossType></LossType>
                              <ManualCheck></ManualCheck>
                              <Offset></Offset>
							  <CheckMemo></CheckMemo>
                              <HoldReason></HoldReason>
                              <InvoiceAmount></InvoiceAmount>
                              <CheckTotal></CheckTotal>
							  <ReserveType></ReserveType>
                              <PayToTheOrderOf></PayToTheOrderOf>
							  //added for RMA 16952
                              <ControlHeader></ControlHeader> 
                              <CheckHeader></CheckHeader>
                              <TransDateHeader></TransDateHeader>
                              <TypeHeader></TypeHeader> 
                              <ClearedHeader></ClearedHeader> 
                              <VoidHeader></VoidHeader> 
                              //Added by swati MITS # 33431
                              <StopPayHeader></StopPayHeader>
                              <StatusHeader></StatusHeader> 
                              <PayeeHeader></PayeeHeader> 
                              <CheckAmountHeader></CheckAmountHeader>
                              <FromDateHeader></FromDateHeader>
                              <ToDateHeader></ToDateHeader> 
                              <InvoiceHeader></InvoiceHeader> 
                              <TransactionTypeHeader></TransactionTypeHeader> 
                              <SplitAmountHeader></SplitAmountHeader> 
                              <UserHeader></UserHeader> 
                              <CheckDateHeader></CheckDateHeader> 
                              <CombinedPayHeader></CombinedPayHeader> 
                              <IsFirstFinalHeader></IsFirstFinalHeader>
                              <PolicyNameHeader></PolicyNameHeader>
                              <CoverageTypeHeader></CoverageTypeHeader>
                              <UnitHeader></UnitHeader>
                              <LossTypeHeader></LossTypeHeader>
                              <ManualCheckHeader></ManualCheckHeader>
                              <OffsetHeader></OffsetHeader>
                              <InvoiceAmountHeader></InvoiceAmountHeader>
							  <CheckMemoHeader></CheckMemoHeader>
                              <HoldReasonHeader></HoldReasonHeader>
                              <CheckTotalHeader></CheckTotalHeader>
							  <ReserveTypeHeader></ReserveTypeHeader>
                              <PayToTheOrderOfHeader></PayToTheOrderOfHeader>
							  //added for RMA 16952
                    </PayHisConfig>
                </Config>
              </Document>
            </Message>
            ");

            return oTemplate;
        }

        private XmlDocument SetDiaryConfigDetails()
        {
            XElement messageElement = null;
            XElement tempElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = SetConfigMessageTemplate();
                messageElement.XPathSelectElement("./Document/Config/DiaryConfig").Attribute("selected").Value = "1";
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/Priority");
                if (chkPriority.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/Text");
                if (chkText.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/WorkActivity");
                if (chkWorkActivity.Checked)
                {
                    tempElement.Value = "True";
                }
                //igupta3 jira 439
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/NotRoutable");
                if (chkNotRoutable.Checked)
                {
                    tempElement.Value = "True";
                }
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/NotRollable");
                if (chkNotRollable.Checked)
                {
                    tempElement.Value = "True";
                }
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/Department");
                if (chkDepartment.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/Due");
                if (chkDue.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/AttachedRecord");
                if (chkAttachedRecord.Checked)
                {
                    tempElement.Value = "True";
                }

                //Indu - Mits 33848 Added Parent Record to teh Header Config
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/ParentRecord");
                if (chkParentRecord.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/Claimant");
                if (chkClaimant.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/ClaimStatus");
                if (chkClaimStatus.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/PriorityHeader");
                tempElement.Value = txtPriority.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/TextHeader");
                tempElement.Value = txtText.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/WorkActivityHeader");
                tempElement.Value = txtWorkActivity.Value;

                //igupta3 jira 439
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/NotRoutableHeader");
                tempElement.Value = txtNotRoutable.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/NotRollableHeader");
                tempElement.Value = txtNotRollable.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/DepartmentHeader");
                tempElement.Value = txtDepartment.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/DueHeader");
                tempElement.Value = txtDue.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/AttachedRecordHeader");
                tempElement.Value = txtAttachedRecord.Value;

                //Indu - Mits 33843
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/ParentRecordHeader");
                tempElement.Value = txtParentRecord.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/ClaimantHeader");
                tempElement.Value = txtClaimant.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/ClaimStatusHeader");
                tempElement.Value = txtClaimStatus.Value;
                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/AssignedUser");
                if (chkAssignedUser.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/AssigningUser");
                if (chkAssigningUser.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/AssignedUserHeader");
                tempElement.Value = txtAssignedUser.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/DiaryConfig/AssigningUserHeader");
                tempElement.Value = txtAssigningUser.Value;

                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                messageElement = null;
                tempElement = null;
                resultDoc = null;
            }
        }

        private XmlDocument SetProgNotesConfigDetails()
        {
            XElement messageElement = null;
            XElement tempElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = SetConfigMessageTemplate();
                messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig").Attribute("selected").Value = "1";

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/Activity");
                if (chkActivity.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/NoteType");
                if (chkNoteType.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/NoteText");
                if (chkNoteText.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/AttachedTo");
                if (chkAttachedTo.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/EnteredBy");
                if (chkEnteredBy.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/DateCreated");
                if (chkDateCreated.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/TimeCreated");
                if (chkTimeCreated.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/UserType");
                if (chkUserType.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/Subject");
                if (chkSubject.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/ActivityHeader");
                tempElement.Value = txtActivity.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/NoteTypeHeader");
                tempElement.Value = txtNoteType.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/NoteTextHeader");
                tempElement.Value = txtNoteText.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/AttachedToHeader");
                tempElement.Value = txtAttachedTo.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/EnteredByHeader");
                tempElement.Value = txtEnteredBy.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/DateCreatedHeader");
                tempElement.Value = txtDateCreated.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/TimeCreatedHeader");
                tempElement.Value = txtTimeCreated.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/UserTypeHeader");
                tempElement.Value = txtUserType.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/ProgNotesConfig/SubjectHeader");
                tempElement.Value = txtSubject.Value;

                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                messageElement = null;
                tempElement = null;
                resultDoc = null;
            }
        }

        private XmlDocument SetPayHisConfigDetails()
        {
            XElement messageElement = null;
            XElement tempElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = SetConfigMessageTemplate();
                messageElement.XPathSelectElement("./Document/Config/PayHisConfig").Attribute("selected").Value = "1";
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Control");
                if (chkControl.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Check");
                if (chkCheck.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/TransDate");
                if (chkTransDate.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Type");
                if (chkType.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Cleared");
                if (chkCleared.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Void");
                if (chkVoid.Checked)
                {
                    tempElement.Value = "True";
                }

                //Added by swati for MITS # 33431 WWIg Gap 9
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/StopPay");
                if (chkStopPay.Checked)
                {
                    tempElement.Value = "True";
                }
                //change end here by swati

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Status");
                if (chkStatus.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Payee");
                if (chkPayee.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckAmount");
                if (chkCheckAmount.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/FromDate");
                if (chkFromDate.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ToDate");
                if (chkToDate.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Invoice");
                if (chkInvoice.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/TransactionType");
                if (chkTransType.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/SplitAmount");
                if (chkSplitAmount.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/User");
                if (chkUser.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckDate");
                if (chkCheckDate.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CombinedPay");
                if (chkCombinedPay.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/IsFirstFinal");
                if (chkIsFirstFinal.Checked)
                {
                    tempElement.Value = "True";
                }
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/PolicyName");
                if (chkPolicyName.Checked)
                {
                    tempElement.Value = "True";
                }
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CoverageType");
                if (chkCoverageType.Checked)
                {
                    tempElement.Value = "True";
                }
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Unit");
                if (chkUnit.Checked)
                {
                    tempElement.Value = "True";
                }
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/LossType");
                if (chkLossType.Checked)
                {
                    tempElement.Value = "True";
                }
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ManualCheck");
                if (chkManualCheck.Checked)
                {
                    tempElement.Value = "True";
                }
                //igupta3 Mits#34515 adding offset
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/Offset");
                if (chkOffset.Checked)
                {
                    tempElement.Value = "True";
                }
				 //Ashish Ahuja Mits 34270
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckMemo");
                if (chkCheckMemo.Checked)
                {
                    tempElement.Value = "True";
                }
                //sachin 7810 starts
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/HoldReason");
                if (chkHoldReason.Checked)
                {
                    tempElement.Value = "True";
                }
                //sachin 7810 ends
                //Mits 25726
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/InvoiceAmount");
                if (chkInvoiceAmount.Checked)
                {
                    tempElement.Value = "True";
                }
                //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckTotal");
                if (chkCheckTotal.Checked)
                {
                    tempElement.Value = "True";
                }
                //end - Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ReserveType");
                if (chkReserveType.Checked)
                {
                    tempElement.Value = "True";
                }
                //rsriharsha RMA-16952 starts
				tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/PayToTheOrderOf");
                if (chkPayToTheOrderOf.Checked)
                {
                    tempElement.Value = "True";
                }
				//rsriharsha RMA-16952 ends

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ControlHeader");
                tempElement.Value = txtControl.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckHeader");
                tempElement.Value = txtCheck.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/TransDateHeader");
                tempElement.Value = txtTransDate.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/TypeHeader");
                tempElement.Value = txtType.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ClearedHeader");
                tempElement.Value = txtCleared.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/VoidHeader");
                tempElement.Value = txtVoid.Value;

                //Added by swati for MITS # 33431 WWIg Gap 9
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/StopPayHeader");
                tempElement.Value = txtStopPay.Value;
                //change end here by swati

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/StatusHeader");
                tempElement.Value = txtStatus.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/PayeeHeader");
                tempElement.Value = txtPayee.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckAmountHeader");
                tempElement.Value = txtCheckAmount.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/FromDateHeader");
                tempElement.Value = txtFromDate.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ToDateHeader");
                tempElement.Value = txtToDate.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/InvoiceHeader");
                tempElement.Value = txtInvoice.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/TransactionTypeHeader");
                tempElement.Value = txtTransType.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/SplitAmountHeader");
                tempElement.Value = txtSplitAmount.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/UserHeader");
                tempElement.Value = txtUser.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckDateHeader");
                tempElement.Value = txtCheckDate.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CombinedPayHeader");
                tempElement.Value = txtCombinedPay.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/IsFirstFinalHeader");
                tempElement.Value = txtIsFirstFinal.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/PolicyNameHeader");
                tempElement.Value = txtPolicyName.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CoverageTypeHeader");
                tempElement.Value = txtCoverageType.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/UnitHeader");
                tempElement.Value = txtUnit.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/LossTypeHeader");
                tempElement.Value = txtLossType.Value;

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ManualCheckHeader");
                tempElement.Value = txtManualCheck.Value;
                
                //igupta3 Mits#34515 adding offset
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/OffsetHeader");
                tempElement.Value = txtOffset.Value;
				
				//Ashish Ahuja Mits 34270 start
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckMemoHeader");
                tempElement.Value = txtCheckMemo.Value;
                //Ashish Ahuja Mits 34270 end

                //sachin 7810 starts
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/HoldReasonHeader");
                tempElement.Value = txtHoldReason.Value;
                //sachin 7810
                //Mits 25726 start
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/InvoiceAmountHeader");
                tempElement.Value = txtInvoiceAmount.Value;
                //Mits 25726 end
                //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/CheckTotalHeader");
                tempElement.Value = txtCheckTotal.Value;
                //end - Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/ReserveTypeHeader");
                tempElement.Value = txtReserveType.Value;
				//rsriharsha RMA-16952 starts
				tempElement = messageElement.XPathSelectElement("./Document/Config/PayHisConfig/PayToTheOrderOfHeader");
                tempElement.Value = txtPayToTheOrderOf.Value;
				//rsriharsha RMA-16952 ends
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                messageElement = null;
                tempElement = null;
                resultDoc = null;
            }
        }

        private XmlDocument GetConfigDetails()
        {
            XElement messageElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = GetConfigMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                messageElement = null;
                resultDoc = null;
            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>HeaderConfigAdaptor.GetHeaderConfig</Function>
              </Call>
              <Document>
                <Config>
                 <DiaryConfig>
                    <IsAdminUser>False</IsAdminUser> 
                    <UserId /> 
                  </DiaryConfig>
                  <ProgNotesConfig>
                    <IsAdminUser>False</IsAdminUser> 
                    <UserId /> 
                  </ProgNotesConfig>
                  <PayHisConfig>
                    <IsAdminUser>False</IsAdminUser> 
                    <UserId /> 
                </PayHisConfig>
               </Config>
              </Document>
            </Message>
            ");

            return oTemplate;
        }

        private void LoadConfigDetails()
        {
            XmlDocument resultDoc = null;
            XmlNode ConfigNode = null;

            resultDoc = GetConfigDetails();

            if (resultDoc != null)
            {
                ConfigNode = resultDoc.SelectSingleNode("//Document/Config");
                //asharma326 MITS 34544 
                //Deb
                if (((ConfigNode != null)&&(ConfigNode.SelectSingleNode("CarrierClaim") != null)))
                {
                    if (ConfigNode.SelectSingleNode("CarrierClaim").InnerText == Boolean.FalseString)
                    {
                        chkIsFirstFinal.Visible = false;
                        lblIsFirstFinal.Visible = false;
                        txtIsFirstFinal.Visible = false;
                        btntxtIsFirstFinal.Visible = false;
                        CarrClmTR1.Visible = false;
                        CarrClmTR2.Visible = false;
                        trCheckTotal.Visible = false;


                    }
                    else
                    {
                        if (ConfigNode.SelectSingleNode("DeductibleApplicable") != null)
                        {
                            if (ConfigNode.SelectSingleNode("DeductibleApplicable").InnerText == Boolean.TrueString)
                            {
                                trCheckTotal.Visible = true;
                            }
                            else
                            {
                                trCheckTotal.Visible = false;
                            }
                        }
                        else
                        {
                           trCheckTotal.Visible = false;
                        }
                    
                    
                    }
                }
                //asharma326 MITS 34544 
                if (ConfigNode != null)
                {
                    //asharma326 MITS 34544 
                    if ((ConfigNode.SelectSingleNode("ErrorStatus") != null) && ConfigNode.SelectSingleNode("ErrorStatus").InnerText == Boolean.FalseString) return;
                    //For Diary Header Configuration
                    if (ConfigNode.SelectSingleNode("Priority").InnerText == "True")
                    {
                        chkPriority.Checked = true;
                    }
                    else
                    {
                        chkPriority.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Text").InnerText == "True")
                    {
                        chkText.Checked = true;
                    }
                    else
                    {
                        chkText.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("WorkActivity").InnerText == "True")
                    {
                        chkWorkActivity.Checked = true;
                    }
                    else
                    {
                        chkWorkActivity.Checked = false;
                    }
                    //igupta3 jiar 439
                    if (ConfigNode.SelectSingleNode("NotRoutable").InnerText == "True")
                    {
                        chkNotRoutable.Checked = true;
                    }
                    else
                    {
                        chkNotRoutable.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("NotRollable").InnerText == "True")
                    {
                        chkNotRollable.Checked = true;
                    }
                    else
                    {
                        chkNotRollable.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Department").InnerText == "True")
                    {
                        chkDepartment.Checked = true;
                    }
                    else
                    {
                        chkDepartment.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Due").InnerText == "True")
                    {
                        chkDue.Checked = true;
                    }
                    else
                    {
                        chkDue.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("AttachedRecord").InnerText == "True")
                    {
                        chkAttachedRecord.Checked = true;
                    }
                    else
                    {
                        chkAttachedRecord.Checked = false;
                    }
                    //Indu - Mits 33843
                    if (ConfigNode.SelectSingleNode("ParentRecord").InnerText == "True")
                    {
                        chkParentRecord.Checked = true;
                    }
                    else
                    {
                        chkParentRecord.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("Claimant").InnerText == "True")
                    {
                        chkClaimant.Checked = true;
                    }
                    else
                    {
                        chkClaimant.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("ClaimStatus").InnerText == "True")
                    {
                        chkClaimStatus.Checked = true;
                    }
                    else
                    {
                        chkClaimStatus.Checked = false;
                    }

                    txtPriority.Value = ConfigNode.SelectSingleNode("PriorityHeader").InnerText;
                    txtText.Value = ConfigNode.SelectSingleNode("TextHeader").InnerText;
                    txtWorkActivity.Value = ConfigNode.SelectSingleNode("WorkActivityHeader").InnerText;
                    //igupta3 jira 439
                    txtNotRoutable.Value = ConfigNode.SelectSingleNode("NotRoutableHeader").InnerText;
                    txtNotRollable.Value = ConfigNode.SelectSingleNode("NotRollableHeader").InnerText;
                    txtDepartment.Value = ConfigNode.SelectSingleNode("DepartmentHeader").InnerText;
                    txtDue.Value = ConfigNode.SelectSingleNode("DueHeader").InnerText;
                    txtAttachedRecord.Value = ConfigNode.SelectSingleNode("AttachedRecordHeader").InnerText;
                    //Indu - Mits 33843
                    txtParentRecord.Value = ConfigNode.SelectSingleNode("ParentRecordHeader").InnerText;
                    txtClaimant.Value = ConfigNode.SelectSingleNode("ClaimantHeader").InnerText;
                    txtClaimStatus.Value = ConfigNode.SelectSingleNode("ClaimStatusHeader").InnerText;

                    if (ConfigNode.SelectSingleNode("AssignedUser").InnerText == "True")
                    {
                        chkAssignedUser.Checked = true;
                    }
                    else
                    {
                        chkAssignedUser.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("AssigningUser").InnerText == "True")
                    {
                        chkAssigningUser.Checked = true;
                    }
                    else
                    {
                        chkAssigningUser.Checked = false;
                    }

                    txtAssignedUser.Value = ConfigNode.SelectSingleNode("AssignedUserHeader").InnerText;
                    txtAssigningUser.Value = ConfigNode.SelectSingleNode("AssigningUserHeader").InnerText;

                    //For Progress Notes Configuration
                    if (ConfigNode.SelectSingleNode("Activity").InnerText == "True")
                    {
                        chkActivity.Checked = true;
                    }
                    else
                    {
                        chkActivity.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("NoteType").InnerText == "True")
                    {
                        chkNoteType.Checked = true;

                    }
                    else
                    {
                        chkNoteType.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("NoteText").InnerText == "True")
                    {
                        chkNoteText.Checked = true;
                    }
                    else
                    {
                        chkNoteText.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("AttachedTo").InnerText == "True")
                    {
                        chkAttachedTo.Checked = true;
                    }
                    else
                    {
                        chkAttachedTo.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("EnteredBy").InnerText == "True")
                    {
                        chkEnteredBy.Checked = true;
                    }
                    else
                    {
                        chkEnteredBy.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("DateCreated").InnerText == "True")
                    {
                        chkDateCreated.Checked = true;
                    }
                    else
                    {
                        chkDateCreated.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("TimeCreated").InnerText == "True")
                    {
                        chkTimeCreated.Checked = true;
                    }
                    else
                    {
                        chkTimeCreated.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("UserType").InnerText == "True")
                    {
                        chkUserType.Checked = true;
                    }
                    else
                    {
                        chkUserType.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("Subject").InnerText == "True")
                    {
                        chkSubject.Checked = true;
                    }
                    else
                    {
                        chkSubject.Checked = false;
                    }
                    txtActivity.Value = ConfigNode.SelectSingleNode("ActivityHeader").InnerText;
                    txtNoteType.Value = ConfigNode.SelectSingleNode("NoteTypeHeader").InnerText;
                    txtNoteText.Value = ConfigNode.SelectSingleNode("NoteTextHeader").InnerText;
                    txtAttachedTo.Value = ConfigNode.SelectSingleNode("AttachedToHeader").InnerText;
                    txtEnteredBy.Value = ConfigNode.SelectSingleNode("EnteredByHeader").InnerText;
                    txtDateCreated.Value = ConfigNode.SelectSingleNode("DateCreatedHeader").InnerText;
                    txtTimeCreated.Value = ConfigNode.SelectSingleNode("TimeCreatedHeader").InnerText;
                    txtUserType.Value = ConfigNode.SelectSingleNode("UserTypeHeader").InnerText;
                    txtSubject.Value = ConfigNode.SelectSingleNode("SubjectHeader").InnerText;

                    //For Payment History Configuration
                    if (ConfigNode.SelectSingleNode("Control").InnerText == "True")
                    {
                        chkControl.Checked = true;
                    }
                    else
                    {
                        chkControl.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Check").InnerText == "True")
                    {
                        chkCheck.Checked = true;

                    }
                    else
                    {
                        chkCheck.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("TransDate").InnerText == "True")
                    {
                        chkTransDate.Checked = true;
                    }
                    else
                    {
                        chkTransDate.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Type").InnerText == "True")
                    {
                        chkType.Checked = true;
                    }
                    else
                    {
                        chkType.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Cleared").InnerText == "True")
                    {
                        chkCleared.Checked = true;
                    }
                    else
                    {
                        chkCleared.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Void").InnerText == "True")
                    {
                        chkVoid.Checked = true;
                    }
                    else
                    {
                        chkVoid.Checked = false;
                    }

                    //Added by swati agarwal for WWIG Gap 9 MITS # 33431
                    if (ConfigNode.SelectSingleNode("StopPay").InnerText == "True")
                    {
                        chkStopPay.Checked = true;
                    }
                    else
                    {
                        chkStopPay.Checked = false;
                    }
                    //change end here by swati

                    if (ConfigNode.SelectSingleNode("Status").InnerText == "True")
                    {
                        chkStatus.Checked = true;
                    }
                    else
                    {
                        chkStatus.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("Payee").InnerText == "True")
                    {
                        chkPayee.Checked = true;
                    }
                    else
                    {
                        chkPayee.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("CheckAmount").InnerText == "True")
                    {
                        chkCheckAmount.Checked = true;
                    }
                    else
                    {
                        chkCheckAmount.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("FromDate").InnerText == "True")
                    {
                        chkFromDate.Checked = true;
                    }
                    else
                    {
                        chkFromDate.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("ToDate").InnerText == "True")
                    {
                        chkToDate.Checked = true;
                    }
                    else
                    {
                        chkToDate.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("Invoice").InnerText == "True")
                    {
                        chkInvoice.Checked = true;
                    }
                    else
                    {
                        chkInvoice.Checked = false;
                    }

                    if (ConfigNode.SelectSingleNode("TransactionType").InnerText == "True")
                    {
                        chkTransType.Checked = true;
                    }
                    else
                    {
                        chkTransType.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("SplitAmount").InnerText == "True")
                    {
                        chkSplitAmount.Checked = true;
                    }
                    else
                    {
                        chkSplitAmount.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("User").InnerText == "True")
                    {
                        chkUser.Checked = true;
                    }
                    else
                    {
                        chkUser.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("CheckDate").InnerText == "True")
                    {
                        chkCheckDate.Checked = true;
                    }
                    else
                    {
                        chkCheckDate.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("CombinedPay").InnerText == "True")
                    {
                        chkCombinedPay.Checked = true;
                    }
                    else
                    {
                        chkCombinedPay.Checked = false;
                    }
                    //Deb
                    if (ConfigNode.SelectSingleNode("IsFirstFinal").InnerText == "True")
                    {
                        chkIsFirstFinal.Checked = true;
                    }
                    else
                    {
                        chkIsFirstFinal.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("PolicyName").InnerText == "True")
                    {
                        chkPolicyName.Checked = true;
                    }
                    else
                    {
                        chkPolicyName.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("CoverageType").InnerText == "True")
                    {
                        chkCoverageType.Checked = true;
                    }
                    else
                    {
                        chkCoverageType.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("Unit").InnerText == "True")
                    {
                        chkUnit.Checked = true;
                    }
                    else
                    {
                        chkUnit.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("LossType").InnerText == "True")
                    {
                        chkLossType.Checked = true;
                    }
                    else
                    {
                        chkLossType.Checked = false;
                    }
                    if (ConfigNode.SelectSingleNode("ManualCheck").InnerText == "True")
                    {
                        chkManualCheck.Checked = true;
                    }
                    else
                    {
                        chkManualCheck.Checked = false;
                    }

                    //igupta3 Mits#34515 adding offset
                    if (ConfigNode.SelectSingleNode("Offset").InnerText == "True")
                    {
                        chkOffset.Checked = true;
                    }
                    else
                    {
                        chkOffset.Checked = false;
                    }
					//Ashish Ahuja Mits 34270 start
                    if (ConfigNode.SelectSingleNode("CheckMemo").InnerText == "True")
                    {
                        chkCheckMemo.Checked = true;
                    }
                    else
                    {
                        chkCheckMemo.Checked = false;
                    }
                    //Ashish Ahuja Mits 34270 end

                    //sachin 7810 starts
                    if (ConfigNode.SelectSingleNode("HoldReason").InnerText == "True")
                    {
                        chkHoldReason.Checked = true;
                    }
                    else
                    {
                        chkHoldReason.Checked = false;
                    }
                    //sachin 7810 ends

                    //Mits 25726 start
                    if (ConfigNode.SelectSingleNode("InvoiceAmount").InnerText == "True")
                    {
                        chkInvoiceAmount.Checked = true;
                    }
                    else
                    {
                        chkInvoiceAmount.Checked = false;
                    }
                    //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    //Start - Changed by Nikhil on 09/22/14.Code Review changes
                    // if (ConfigNode.SelectSingleNode("CheckTotal").InnerText == "True")
                    if (string.Compare(ConfigNode.SelectSingleNode("CheckTotal").InnerText.ToString(),"True",true) == 0)
                    //End - Changed by Nikhil on 09/22/14.Code Review changes
                    {
                        chkCheckTotal.Checked = true;
                    }
                    else
                    {
                        chkCheckTotal.Checked = false;
                    }
                    //end - Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                    if (string.Compare(ConfigNode.SelectSingleNode("ReserveType").InnerText.ToString(), "True", true) == 0)
                    {
                        chkReserveType.Checked = true;
                    }
                    else
                    {
                        chkReserveType.Checked = false;
                    }
					//rsriharsha RMA-16952 starts
					if (ConfigNode.SelectSingleNode("PayToTheOrderOf").InnerText == "True")
                    {
                        chkPayToTheOrderOf.Checked = true;
                    }
                    else
                    {
                        chkPayToTheOrderOf.Checked = false;
                    }
					//rsriharsha RMA-16952 ends
 
                    txtControl.Value = ConfigNode.SelectSingleNode("ControlHeader").InnerText;
                    txtCheck.Value = ConfigNode.SelectSingleNode("CheckHeader").InnerText;
                    txtTransDate.Value = ConfigNode.SelectSingleNode("TransDateHeader").InnerText;
                    txtType.Value = ConfigNode.SelectSingleNode("TypeHeader").InnerText;
                    txtCleared.Value = ConfigNode.SelectSingleNode("ClearedHeader").InnerText;
                    txtVoid.Value = ConfigNode.SelectSingleNode("VoidHeader").InnerText;
                    //Added by swati agarwal for WWIG Gap 9 MITS # 33431
                    txtStopPay.Value = ConfigNode.SelectSingleNode("StopPayHeader").InnerText;
                    //change end here by swati

                    txtStatus.Value = ConfigNode.SelectSingleNode("StatusHeader").InnerText;
                    txtPayee.Value = ConfigNode.SelectSingleNode("PayeeHeader").InnerText;
                    txtCheckAmount.Value = ConfigNode.SelectSingleNode("CheckAmountHeader").InnerText;
                    txtFromDate.Value = ConfigNode.SelectSingleNode("FromDateHeader").InnerText;
                    txtToDate.Value = ConfigNode.SelectSingleNode("ToDateHeader").InnerText;
                    txtInvoice.Value = ConfigNode.SelectSingleNode("InvoiceHeader").InnerText;
                    txtTransType.Value = ConfigNode.SelectSingleNode("TransactionTypeHeader").InnerText;
                    txtSplitAmount.Value = ConfigNode.SelectSingleNode("SplitAmountHeader").InnerText;
                    txtUser.Value = ConfigNode.SelectSingleNode("UserHeader").InnerText;
                    txtCheckDate.Value = ConfigNode.SelectSingleNode("CheckDateHeader").InnerText;
                    txtCombinedPay.Value = ConfigNode.SelectSingleNode("CombinedPayHeader").InnerText;

                    //Deb
                    txtIsFirstFinal.Value = ConfigNode.SelectSingleNode("IsFirstFinalHeader").InnerText;
                    txtPolicyName.Value = ConfigNode.SelectSingleNode("PolicyNameHeader").InnerText;
                    txtCoverageType.Value = ConfigNode.SelectSingleNode("CoverageTypeHeader").InnerText;
                    txtUnit.Value = ConfigNode.SelectSingleNode("UnitHeader").InnerText;
                    txtLossType.Value = ConfigNode.SelectSingleNode("LossTypeHeader").InnerText;
                    txtManualCheck.Value = ConfigNode.SelectSingleNode("ManualCheckHeader").InnerText;
                    //igupta3 Mits#34515 adding offset
                    txtOffset.Value = ConfigNode.SelectSingleNode("OffsetHeader").InnerText;
                    //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    txtCheckTotal.Value = ConfigNode.SelectSingleNode("CheckTotalHeader").InnerText;
					//Ashish Ahuja Mits 34270
                    txtCheckMemo.Value = ConfigNode.SelectSingleNode("CheckMemoHeader").InnerText;
                    txtHoldReason.Value = ConfigNode.SelectSingleNode("HoldReasonHeader").InnerText;//sachin 7810
                    // Mits 25726
                    txtInvoiceAmount.Value = ConfigNode.SelectSingleNode("InvoiceAmountHeader").InnerText;
                    txtReserveType.Value = ConfigNode.SelectSingleNode("ReserveTypeHeader").InnerText;
                    //rsriharsha RMA-16952 starts
                    txtPayToTheOrderOf.Value = ConfigNode.SelectSingleNode("PayToTheOrderOfHeader").InnerText;
					//rsriharsha RMA-16952 ends
                    
                }
            }
        }
    }
}