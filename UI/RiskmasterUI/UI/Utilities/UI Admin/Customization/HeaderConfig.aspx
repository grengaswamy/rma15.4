﻿<!--**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 05/23/2014 | 34270   | aahuja21   | Added check memo in payment history grid
**********************************************************************************************-->
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HeaderConfig.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.HeaderConfig" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <title>Header Configuration</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
        <script src="../../../../Scripts/ExecutiveSummary.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="../../../../Scripts/WaitDialog.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script  type="text/javascript" language="Javascript">
        function setDefaultValue(name, value) {
            document.getElementById(name).value = value;
            return false;
        }

        //Ankit Starts : MITS 30992
        function checkByParent(aId, aChecked) {
            var bCheckBoxes = document.getElementById(aId).getElementsByTagName('INPUT');
            for (var i = 0; i < bCheckBoxes.length; i++) {
                if (bCheckBoxes[i].type.toUpperCase() == 'CHECKBOX') {
                    var objElem = bCheckBoxes[i];
                    if ((objElem.disabled == false)) {
                        objElem.checked = aChecked;
                    }
                }
            }
            m_DataChanged = true;
            return false;
        }
        //Ankit End
    
        //WWIG GAP20A - agupta298 - MITS 36804 - Start - JIRA - 4691
        function OpenDiarySuppHeaderConfig() {
            window.open('/RiskmasterUI/UI/Utilities/UI Admin/Customization/DiarySuppHeaderConfig.aspx', 'DiarySuppHeaderConfig', 'width=400,height=350' + ',top=' + (screen.availHeight - 350) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }
        //WWIG GAP20A - agupta298 - MITS 36804 - End - JIRA - 4691
    </script>
</head>
<body class="" onload="loadTabList();parent.MDIScreenLoaded();" style="height: 90%">
    <form id="frmData" runat="server">
     <uc1:ErrorControl ID="ErrorControl1" runat="server" />
     <input type="hidden" name="hTabName" />
     <div class="msgheader" id="formtitle">
      <asp:Label ID="lblDiaryHeader" runat="server" Text="<%$ Resources:lblHeader %>"></asp:Label>    
     </div>

     <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSDiaryConfig" id="TABSDiaryConfig">
            <a class="Selected" href="#" onclick="tabChange(this.name);return false;" runat="server"
                rmxref="" name="DiaryConfig" id="LINKTABSDiaryConfig">
                <asp:Label ID="lblDiaryConfig" runat="server" Text="<%$ Resources:lblDiaryConfig %>"></asp:Label>
            </a>
        </div>
        <div class="tabSpace" runat="server" id="Div1">
            <nbsp />
            <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSProgNotesConfig" id="TABSProgNotesConfig">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                rmxref="" name="ProgNotesConfig" id="LINKTABSProgNotesConfig">
                <asp:Label ID="lblProgNotesConfig" runat="server" Text="<%$ Resources:lblProgNotesConfig %>"></asp:Label>
             </a>
        </div>
        <div class="tabSpace" runat="server" id="TBSProgNotesConfig">
            <nbsp />
            <nbsp />
        </div>

        <div class="NotSelected" nowrap="true" runat="server" name="TABSPayHisConfig" id="TABSPayHisConfig">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" runat="server"
                rmxref="" name="PayHisConfig" id="LINKTABSPayHisConfig">
                <asp:Label ID="lblPayHisConfig" runat="server" Text="<%$ Resources:lblPayHisConfig %>"></asp:Label>
             </a>
        </div>
    </div>

   <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 100%;
        height: 83%; overflow: auto;">
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABDiaryConfig" id="FORMTABDiaryConfig">
            <tr valign="top">
            <td valign="top">
                <div style="position: relative; left: 0; top: 0; width: 950px; height: 280px; overflow: auto;valign:top" class="singletopborder" id="DivDiary">
                    <table border="0" cellspacing="0" celpadding="0" width="100%" valign="top" align="center">
                        <tr valign="top">
                            <td valign="top">
                                <table border="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkPriority" runat="server" checked ="checked" />
                                            <asp:Label ID="lblPriority" runat="server" Text="<%$ Resources:lblPriority %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Priority" runat="server" id="txtPriority" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPriority" runat="server" value="<%$ Resources:btnPriority %>" tabindex="3" onclick="return setDefaultValue('txtPriority','Priority');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkDue" checked ="checked"/>
                                            <asp:Label ID="lblDue" runat="server" Text="<%$ Resources:lblDue %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" id="txtDue" runat="server"  value="Due" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnDue" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtDue','Due');" class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkText" checked ="checked" />
                                            <asp:Label ID="lblText" runat="server" Text="<%$ Resources:lblText %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Task Name" id="txtText" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnText" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtText','Task Name');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkAttachedRecord" checked ="checked" />
                                            <asp:Label ID="lblAttachRecord" runat="server" Text="<%$ Resources:lblAttachRecord %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Attached Record" runat="server" id="txtAttachedRecord" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAttachedRecord" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtAttachedRecord','Attached Record');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkWorkActivity" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblWorkAct" runat="server" Text="<%$ Resources:lblWorkAct %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Work Activity" runat="server" id="txtWorkActivity" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnWorkActivity" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtWorkActivity','Work Activity');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkClaimant" runat="server" checked ="checked" />
                                            <asp:Label ID="lblClaimant" runat="server" Text="<%$ Resources:lblClaimant %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Claimant" id="txtClaimant" runat="server"/>
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnClaimant" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtClaimant','Claimant');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkDepartment" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblDept" runat="server" Text="<%$ Resources:lblDept %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Department" id="txtDepartment" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnDepartment" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtDepartment','Department');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkClaimStatus" runat="server" checked ="checked" />
                                            <asp:Label ID="lblClaimStatus" runat="server" Text="<%$ Resources:lblClaimStatus %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Claim Status" id="txtClaimStatus" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnClaimStatus" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtClaimStatus','Claim Status');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkAssignedUser" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblTo" runat="server" Text="<%$ Resources:lblTo %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="To" id="txtAssignedUser" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAssignedUser" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtAssignedUser','To');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkAssigningUser" runat="server" checked ="checked" />
                                            <asp:Label ID="lblFrom" runat="server" Text="<%$ Resources:lblFrom %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="From" id="txtAssigningUser" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAssigningUser" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtAssigningUser','From');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkParentRecord" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblParentRecord" runat="server" Text="<%$ Resources:lblParentRecord %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Parent Record" id="txtParentRecord" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnParentRecord" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtParentRecord', 'Parent Record');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <!--Igupta3 Jira 439-->
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkNotRoutable" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblNotRoute" runat="server" Text="<%$ Resources:lblNotRoute %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Routable" id="txtNotRoutable" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnNotRoutable" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtNotRoutable', 'Routable');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkNotRollable" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblNotRollable" runat="server" Text="<%$ Resources:lblNotRollable %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Rollable" id="txtNotRollable" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnNotRollable" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtNotRollable', 'Rollable');"
                                                class="button" style="width: 50" />
                                            </td>
                                    </tr>
                                    <%-- WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691--%>
                                    <tr>
                                        <td width="100%" colspan="6" valign="top">
                                        <input type="submit" runat="server" value="Diary Supplemental Header Configuration" id="btnDiarySuppHeaderConfig" class="button"
                                                onclick="return OpenDiarySuppHeaderConfig();" />
                                        </td>
                                    </tr>
                                    <%-- WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691--%>

                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
           <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                <asp:Label ID="lblMsg1" runat="server" Text="<%$ Resources:lblMsg1 %>"></asp:Label>
                                <br>
                                <asp:Label ID="lblMsg2" runat="server" Text="<%$ Resources:lblMsg2 %>"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" celpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveDiaryConfig" runat="server" Text="<%$ Resources:btnSaveConfiguration %>" 
                                CssClass="button" onclick="btnSaveDiaryConfig_Click" />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnSelectAll %>" id="btnDiarySelectAll" class="button"
                                onclick="return checkByParent('DivDiary',true);" />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnUnselectAll %>" id="btnDiaryUnselectAll" class="button"
                                onclick="return checkByParent('DivDiary',false);" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABProgNotesConfig" id="FORMTABProgNotesConfig"
            style="display: none;">
            <tr valign="top">
            <td valign="top">
                <div style="position: relative; left: 0; top: 0; width: 950px; height: 280px; overflow: auto; valign:top" class="singletopborder" id="DivProgNotes">
                    <table border="0" cellspacing="0" celpadding="0" width="100%" valign="top" align="center">
                        <tr valign="top">
                            <td valign="top">
                                <table border="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkActivity" runat="server" checked ="checked" />
                                            <asp:Label ID="lblActivity" runat="server" Text="<%$ Resources:lblActivity %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Activity" runat="server" id="txtActivity" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnActivity" runat="server" value="<%$ Resources:btnPriority %>" tabindex="3" onclick="return setDefaultValue('txtActivity','Activity');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkNoteType" checked ="checked"/>
                                            <asp:Label ID="lblNoteType" runat="server" Text="<%$ Resources:lblNoteType %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" id="txtNoteType" runat="server"  value="Note Type" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnProgNotesPriority" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtNoteType','Note Type');" class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkNoteText" checked ="checked" />
                                            <asp:Label ID="lblNoteText" runat="server" Text="<%$ Resources:lblNoteText %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Note Text" id="txtNoteText" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnNoteText" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtNoteText','Note Text');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkAttachedTo" checked ="checked" />
                                            <asp:Label ID="lblAttachedTo" runat="server" Text="<%$ Resources:lblAttachedTo %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Attached To" runat="server" id="txtAttachedTo" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAttachedTo" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtAttachedTo','Attached To');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkEnteredBy" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblEnteredBy" runat="server" Text="<%$ Resources:lblEnteredBy %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Entered By" runat="server" id="txtEnteredBy" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnEnteredBy" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtEnteredBy','Entered By');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkDateCreated" runat="server" checked ="checked" />
                                            <asp:Label ID="lblDateCreated" runat="server" Text="<%$ Resources:lblDateCreated %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Date Created" id="txtDateCreated" runat="server"/>
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnDateCreated" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtDateCreated','Date Created');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkTimeCreated" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblTimeCreated" runat="server" Text="<%$ Resources:lblTimeCreated %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Time Created" id="txtTimeCreated" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnTimeCreated" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtTimeCreated','Time Created');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkUserType" runat="server" checked ="checked" />
                                            <asp:Label ID="lblUserType" runat="server" Text="<%$ Resources:lblUserType %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="User Type" id="txtUserType" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnUserType" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtUserType','User Type');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkSubject" runat="server" checked ="checked" />
                                            <asp:Label ID="lblSubject" runat="server" Text="<%$ Resources:lblSubject %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Subject" id="txtSubject" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnSubject" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtSubject','Subject');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
           <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                <asp:Label ID="Label13" runat="server" Text="<%$ Resources:lblMsg3 %>"></asp:Label>
                                <br>
                                <asp:Label ID="Label14" runat="server" Text="<%$ Resources:lblMsg2 %>"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" celpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveProgNotesConfig" onclick="btnSaveProgNotesConfig_Click" runat="server" Text="<%$ Resources:btnSaveConfiguration %>" 
                                CssClass="button" />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnSelectAll %>" id="btnProgNotesSelectAll" class="button"
                                onclick="return checkByParent('DivProgNotes',true);" />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnUnselectAll %>" id="btnProgNotesUnselectAll" class="button"
                                onclick="return checkByParent('DivProgNotes',false);" />
                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                </table>

        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABPayHisConfig" id="FORMTABPayHisConfig"
            style="display: none;">
            <tr valign="top">
            <td valign="top">
                <div style="position: relative; left: 0; top: 0; width: 950px; height: auto; overflow: auto; valign:top" class="singletopborder" id="DivPayHis">
                    <table border="0" cellspacing="0" celpadding="0" width="100%" valign="top" align="center">
                        <tr valign="top">
                            <td valign="top">
                                <table border="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkControl" runat="server" checked ="checked" />
                                            <asp:Label ID="lblControl" runat="server" Text="<%$ Resources:lblControl %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Control #" runat="server" id="txtControl" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnControl" runat="server" value="<%$ Resources:btnPriority %>" tabindex="3" onclick="return setDefaultValue('txtControl','Control #');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkCheck" checked ="checked"/>
                                            <asp:Label ID="lblCheck" runat="server" Text="<%$ Resources:lblCheck %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" id="txtCheck" runat="server"  value="Check #" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCheck" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCheck','Check #');" class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkTransDate" checked ="checked" />
                                            <asp:Label ID="lblTransDate" runat="server" Text="<%$ Resources:lblTransDate %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Trans Date" id="txtTransDate" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnTransDate" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtTransDate','Trans Date');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkType" checked ="checked" />
                                            <asp:Label ID="lblType" runat="server" Text="<%$ Resources:lblType %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Type" runat="server" id="txtType" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnType" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtType','Type');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCleared" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblCleared" runat="server" Text="<%$ Resources:lblCleared %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Cleared?" runat="server" id="txtCleared" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCleared" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCleared','Cleared?');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkVoid" runat="server" checked ="checked" />
                                            <asp:Label ID="lblVoid" runat="server" Text="<%$ Resources:lblVoid %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Void?" id="txtVoid" runat="server"/>
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnVoid" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtVoid','Void?');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkStatus" runat="server" checked ="checked"/>
                                            <asp:Label ID="lblStatus" runat="server" Text="<%$ Resources:lblStatus %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Status" id="txtStatus" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnStatus" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtStatus','Status');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkPayee" runat="server" checked ="checked" />
                                            <asp:Label ID="lblPayee" runat="server" Text="<%$ Resources:lblPayee %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Payee" id="txtPayee" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPayee" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtPayee','Payee');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCheckAmount" runat="server" checked ="checked" />
                                            <asp:Label ID="lblCheckAmout" runat="server" Text="<%$ Resources:lblCheckAmout %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Check Amount" id="txtCheckAmount" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCheckAmount" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCheckAmount','Check Amount');"
                                                class="button" style="width: 50" />
                                        </td>
                                         <td width="20%" valign="top">
                                            <input type="checkbox" id="chkFromDate" runat="server" checked ="checked" />
                                            <asp:Label ID="lblFromDate" runat="server" Text="<%$ Resources:lblFromDate %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="From Date" id="txtFromDate" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnFromDate" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtFromDate','From Date');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkToDate" runat="server" checked ="checked" />
                                            <asp:Label ID="lblToDate" runat="server" Text="<%$ Resources:lblToDate %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="To Date" id="txtToDate" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnToDate" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtToDate','To Date');"
                                                class="button" style="width: 50" />
                                        </td>
                                         <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInvoice" runat="server" checked ="checked" />
                                            <asp:Label ID="lblInvoice" runat="server" Text="<%$ Resources:lblInvoice %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Invoice #" id="txtInvoice" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnInvoice" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtInvoice','Invoice #');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkTransType" runat="server" checked ="checked" />
                                            <asp:Label ID="lblTransType" runat="server" Text="<%$ Resources:lblTransType %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Transaction Type" id="txtTransType" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnTransType" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtTransType','Transaction Type');"
                                                class="button" style="width: 50" />
                                        </td>
                                         <td width="20%" valign="top">
                                            <input type="checkbox" id="chkSplitAmount" runat="server" checked ="checked" />
                                            <asp:Label ID="lblSplitAmount" runat="server" Text="<%$ Resources:lblSplitAmount %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Split Amount" id="txtSplitAmount" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnSplitAmount" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtSplitAmount','Split Amount');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>

                                      <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkUser" runat="server" checked ="checked" />
                                            <asp:Label ID="lblUser" runat="server" Text="<%$ Resources:lblUser %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="User" id="txtUser" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnUser" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtUser','User');"
                                                class="button" style="width: 50" />
                                        </td>
                                         <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCheckDate" runat="server" checked ="checked" />
                                            <asp:Label ID="lblCheckDate" runat="server" Text="<%$ Resources:lblCheckDate %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Check Date" id="txtCheckDate" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCheckDate" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCheckDate','Check Date');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>

                                       <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCombinedPay" runat="server" checked ="checked" />
                                            <asp:Label ID="lblCombinedPay" runat="server" Text="<%$ Resources:lblCombinedPay %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Combined Payment?" id="txtCombinedPay" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCombinedPay" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCombinedPay','Combined Payment?');"
                                                class="button" style="width: 50" />
                                        </td>
                                           <%--asharma326 change location of td MITS 34544 --%>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkOffset" runat="server" checked ="checked" />
                                            <asp:Label ID="lblOffset" runat="server" Text="<%$ Resources:lblOffset %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Offset" runat="server" id="txtOffset" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnOffset" runat="server" value="<%$ Resources:btnPriority %>" tabindex="3" onclick="return setDefaultValue('txtOffset', 'Offset');"
                                                class="button" style="width: 50" />
                                        </td>
                                           <%--asharma326 change location of td MITS 34544 --%>
</tr>
                                    <tr>
                                           <td width="20%" valign="top">
                                            <input type="checkbox" id="chkManualCheck" runat="server" checked ="checked" />
                                            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:lblManualCheck %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Manual Check" id="txtManualCheck" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnManualCheck" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtManualCheck', 'Manual Check');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <%--asharma326 change location of td MITS 34544 --%>
                                         <td width="20%" valign="top">
                                            <input type="checkbox" id="chkIsFirstFinal" runat="server" checked ="checked" />
                                            <asp:Label ID="lblIsFirstFinal" runat="server" Text="<%$ Resources:lblIsFirstFinal %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="IsFirstFinal?" id="txtIsFirstFinal" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btntxtIsFirstFinal" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtIsFirstFinal', 'IsFirstFinal');"
                                                class="button" style="width: 50" />
                                        </td>

                                        <%--asharma326 change location of td MITS 34544 --%>
                                    </tr>
                                    <tr id="CarrClmTR1" runat="server">
                                           <td width="20%" valign="top">
                                            <input type="checkbox" id="chkPolicyName" runat="server" checked ="checked" />
                                            <asp:Label ID="lblPolicyName" runat="server" Text="<%$ Resources:lblPolicyName %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Policy Name" id="txtPolicyName" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPolicyName" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtPolicyName', 'Policy Name');"
                                                class="button" style="width: 50" />
                                        </td>

                                           <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCoverageType" runat="server" checked ="checked" />
                                            <asp:Label ID="lblCoverageType" runat="server" Text="<%$ Resources:lblCoverageType %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Coverage Type" id="txtCoverageType" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCoverageType" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCoverageType', 'Coverage Type');"
                                                class="button" style="width: 50" />
                                        </td>
                                        </tr>
                                    <tr id="CarrClmTR2" runat="server">
                                           <td width="20%" valign="top">
                                            <input type="checkbox" id="chkUnit" runat="server" checked ="checked" />
                                            <asp:Label ID="lblUnit" runat="server" Text="<%$ Resources:lblUnit %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Unit" id="txtUnit" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnUnit" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtUnit', 'Unit');"
                                                class="button" style="width: 50" />
                                        </td>

                                           <td width="20%" valign="top">
                                            <input type="checkbox" id="chkLossType" runat="server" checked ="checked" />
                                            <asp:Label ID="lblLossType" runat="server" Text="<%$ Resources:lblLossType %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Loss Type" id="txtLossType" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnLossType" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtLossType', 'Loss Type');"
                                                class="button" style="width: 50" />
                                        </td>
                                        </tr>
                                    <tr>
                                                                                  <%-- Ashish Ahuja Mits 34270 --%>
                                          <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCheckMemo" runat="server" checked ="checked" />
                                            <asp:Label ID="lblCheckMemo" runat="server" Text="<%$ Resources:lblCheckMemo %>"></asp:Label>
                                        </td>
                                            <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:lblCheckMemo %>" id="txtCheckMemo" runat="server" />
                                                <input type="text" value="<%$ Resources:lblCheckMemo %>" style="display:none"  runat="server" id="txtCheckMemoDefault" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="Button11" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCheckMemo', 'Check Memo');"
                                                class="button" style="width: 50" />
                                        </td>
                                          <%-- Ashish Ahuja Mits 34270 --%>
                                       <%-- sachin 7810--%>
                                         <td width="20%" valign="top">
                                            <input type="checkbox" id="chkHoldReason" runat="server" checked ="checked" />
                                            <%--<asp:Label ID="lblHoldReason" runat="server" Text="<%$ Resources:lblHoldReason %>"></asp:Label>--%>
                                             <asp:Label ID="lblHoldReason" runat="server" Text="<%$ Resources:lblHoldReason %>"></asp:Label>
                                        </td>
                                            <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:lblHoldReason %>" id="txtHoldReason" runat="server" />
                                                <input type="text" value="<%$ Resources:lblHoldReason %>" style="display:none"  runat="server" id="txtHoldReasonDefault" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="Button3" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtHoldReason', 'Hold Reason');"
                                                class="button" style="width: 50" />
                                        </td>
                                     <%--   sachin 7810--%>
                                        </tr>
                                    <tr>
                                          <%-- Mits 25726 starts --%>
                                          <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInvoiceAmount" runat="server" checked ="checked" />
                                            <asp:Label ID="lblInvoiceAmount" runat="server" Text="<%$ Resources:lblInvoiceAmount %>"></asp:Label>
                                        </td>
                                            <td width="20%" valign="top">
                                            <input type="text" value="Invoice Amount" id="txtInvoiceAmount" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="Button1" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtInvoiceAmount', 'Invoice Amount');"
                                                class="button" style="width: 50" />
                                        </td>
                                          <%-- Mits 25726 ends --%>
                                        <%--Added by Swati Agarwal for MITS # 33431 WWIG--%>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkStopPay" runat="server" checked ="checked" />
                                            <asp:Label ID="lblStopPay" runat="server" Text="<%$ Resources:lblStopPay %>"></asp:Label>
                                        </td>
                                            <td width="20%" valign="top">
                                            <input type="text" value="Stop Pay" id="txtStopPay" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="Button2" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtStopPay', 'Stop Pay');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <%--end by Swati--%>
                                        </tr>
                                    <!-- Added by Nikhil on 07/29/14.Check Total  in payment history configuration-->
                                     <tr id ="trCheckTotal" runat ="server">
                                           <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCheckTotal" runat="server"  />
                                            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:lblCheckTotal %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Check Total" id="txtCheckTotal" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCheckTotal" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtCheckTotal', 'Check Total');"
                                                class="button" style="width: 50" />
                                        </td>

                                         <%--As we are adding new column, by default it should be checked. If the user does not want, then he/she can hide it--%>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkReserveType" runat="server"  checked ="checked"/>
                                            <asp:Label ID="Label3" runat="server" Text="<%$ Resources:lblReserveType %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Reserve Type" id="txtReserveType" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnReserveType" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtReserveType', 'Reserve Type');"
                                                class="button" style="width: 50" />
                                        </td>
									 </tr>
									 <tr>
									    
										<%--rsriharsha RMA-16952 starts--%>
										<td>
                                            <input type="checkbox" id="chkPayToTheOrderOf" runat="server" />
                                            <asp:Label ID="lblPayToTheOrderOf" runat="server" Text="<%$ Resources:lblPayToTheOrderOf %>"></asp:Label>
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Pay To The Order Of" id="txtPayToTheOrderOf" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPayToTheOrderOf" runat="server" value="<%$ Resources:btnPriority %>" onclick="return setDefaultValue('txtPayToTheOrderOf', 'Pay To The Order Of');"
                                                class="button" style="width: 50" />
                                        </td>
										<td width="20%" valign="top">
										<%--rsriharsha RMA-16952 starts--%>
                                    </tr>
                                    <!-- end - Added by Nikhil on 07/29/14.Check Total  in payment history configuration -->



                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
           <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                <asp:Label ID="Label10" runat="server" Text="<%$ Resources:lblMsg4 %>"></asp:Label>
                                <br>
                                <asp:Label ID="Label11" runat="server" Text="<%$ Resources:lblMsg2 %>"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" celpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button ID="Button10" onclick="btnSavePayHisConfig_Click" runat="server" Text="<%$ Resources:btnSaveConfiguration %>" 
                                CssClass="button" />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnSelectAll %>" id="btnPayHisSelectAll" class="button"
                                onclick="return checkByParent('DivPayHis',true);" />
                        </td>
                        <td>
                            <input type="submit" runat="server" value="<%$ Resources:btnUnselectAll %>" id="btnpayHisUnselectAll" class="button"
                                onclick="return checkByParent('DivPayHis',false);" />
                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                </table>
    </div>
    </form>
</body>
</html>
