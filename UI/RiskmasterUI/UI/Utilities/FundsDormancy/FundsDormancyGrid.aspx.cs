﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml;
using System.Text;

namespace Riskmaster.UI.UI.Utilities.FundsDormancy
{
    public partial class FundsDormancyGrid : NonFDMBasePageCWS
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                if (FundsDormancyGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = FundsDormancySelectedId.Text;
                    XmlTemplate = GetMessageTemplate(selectedRowId);
                    CallCWS("FundsDormancyAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    FundsDormancyGrid_RowDeletedFlag.Text = "false";
                }
                XmlTemplate = GetMessageTemplate();
                CallCWSFunctionBind("FundsDormancyAdaptor.Get", out sCWSresponse, XmlTemplate);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<PassToWebService><FundsDormancyList>");
            sXml = sXml.Append("<listhead>");
            sXml = sXml.Append("<DormancyRowId>Row Id</DormancyRowId>");
            sXml = sXml.Append("<JurisdictionId>Jurisdiction Id</JurisdictionId>");
            sXml = sXml.Append("<Jurisdiction>Jurisdiction</Jurisdiction>");
            sXml = sXml.Append("<UnclaimedDays>Unclaimed Dormancy (Days)</UnclaimedDays>");
            sXml = sXml.Append("<EscheatDays>Escheat Dormancy (Days)</EscheatDays>");
            sXml = sXml.Append("</listhead></FundsDormancyList></PassToWebService>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }

        private XElement GetMessageTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><FundsDormancy>");
            sXml = sXml.Append("<control name='FundsDormancyGrid'>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</FundsDormancy></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }

}