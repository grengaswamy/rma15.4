﻿<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-4042         | ajohari2   | Underwriters - EFT Payments Part 2
 **********************************************************************************************/--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckBatchPrintSettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.CheckBatchPrintSettings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>
       <script language="javaScript" src="../../../../Scripts/PrintChecks.js" type="text/javascript"></script>
    <script language="javaScript" src="../../../../Scripts/jquery/jquery-1.8.0.min.js" type="text/javascript"></script>
       <script type="text/javascript" language="javascript">
       function PrintSettings_Save() 
       {
           
           if (document.forms[0].ddlAcc.value == "") {
               alert("Please select bank account.");
               return false;
           }

          
           else if (document.forms[0].ddlcheckstock.value == ""  && $('#EFTAccount').val()!='true')
           {
               alert("Please select check stock.");
               return false;
           }
           //npadhy JIRA 6418 Starts Add validation, if the user is trying to save record with EFT as distribution Type, but non EFT bank as Account
           else if ($('#EFTAccount').val() == "false" && $('#ddlDistributionType').val() == $('#EFTDistributionType').val())
           {
               alert('EFT Bank Account must be selected while printing an EFT Payment. Print Settings cannot be saved.');
               return false;
           }
           //npadhy JIRA 6418 Ends Add validation, if the user is trying to save record with EFT as distribution Type, but non EFT bank as Account
           else 
           {
               return true;
           }

        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
    <div>
    <div>
        <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
    <div>
       <table class="toolbar" cellSpacing="0" cellPadding="0" border="0">
     <tbody>
      <tr>
       <td align="center" valign="middle" HEIGHT="32">
       <asp:ImageButton src="../../../../Images/save.gif" alt="" id="Save" 
               onmouseover="javascript:document.all['Save'].src='../../../../Images/save2.gif'" 
               onmouseout="javascript:document.all['Save'].src='../../../../Images/save.gif'" 
               title="Save" runat="server" OnClientClick="return PrintSettings_Save();"
                onclick="Save_Click"  TabIndex ="1"/></td>
      </tr>
     </tbody>
    </table>
   </div><br /><br /><div class="msgheader" id="formtitle">Check Batch Print Settings</div>
    <table border="0">
     <tbody>
            <tr>
                <td class="ctrlgroup" colspan="2">New</td>
            </tr>
            <tr id="">
                <td>
                    Bank Account:&nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="ddlAcc" runat="server"   AutoPostBack ="true" 
                        onselectedindexchanged="ddlAcc_SelectedIndexChanged" TabIndex="2" >
                    </asp:DropDownList>
                </td>
            </tr>
            <%--npadhy JIRA 6418 Starts Display the Distribution Type with which the Auto check Printing will occur--%>
            <tr id="">
                <td>
                    Distribution Type:&nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="ddlDistributionType" runat="server" onchange="setDataChanged(true);" TabIndex="3">
                    </asp:DropDownList>
                </td>
            </tr>
            <%--npadhy JIRA 6418 Ends Display the Distribution Type with which the Auto check Printing will occur--%>
            <tr id="Tr2">
                <td>
                    Check Stock:&nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="ddlcheckstock" runat="server" onchange="setDataChanged(true);" AutoPostBack="true" TabIndex="3">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="">
                <td>
                    Order Field:&nbsp;&nbsp;
                </td>
                <td>
                    <%-- Debabrata Biswas Batch Printing Retrofit r6 MITS# 19715/20050 Date: 03/17/2010 --%>
                    <asp:DropDownList runat="server" ID="orderfieldpre" onchange="setDataChanged(true);" TabIndex="4">
                        <asp:ListItem Value="(None)">(None)</asp:ListItem>
                        <asp:ListItem Value="Check Total">Check Total</asp:ListItem>
                        <asp:ListItem Value="Claim Number">Claim Number</asp:ListItem>
                        <asp:ListItem Value="Control Number">Control Number</asp:ListItem>
                        <asp:ListItem Value="Payee Name">Payee Name</asp:ListItem>
                        <asp:ListItem Value="Transaction Date">Transaction Date</asp:ListItem>
                        <asp:ListItem Value="Check Number">Check Number</asp:ListItem>
                        <asp:ListItem Value="Sub Bank Account">Sub Bank Account</asp:ListItem>
                        <asp:ListItem Value="Payer Level">Payer Level</asp:ListItem>
                        <asp:ListItem Value="Current Adjuster">Current Adjuster</asp:ListItem>
                        <asp:ListItem Value="Org Hierarchy">Org. Hierarchy</asp:ListItem> 
                    </asp:DropDownList>
                </td>
            </tr>
                                                                    
            <tr id="">
                <td>
                    Org. Hierarchy:&nbsp;&nbsp;
                </td>
                <td>
                        <%--<asp:ListBox SelectionMode="Multiple" Rows="3" orgid="" ID="orghierarchypre" runat="server" RMXRef="/Instance/Document//OrgHierarchy" ItemSetRef="/Instance/Document//OrgHierarchy"/>
                        <asp:TextBox Style="display: none" ID="orghierarchypre_lst" runat="server" />
                        <asp:TextBox Style="display: none" ID="orghierarchypre_cid" runat="server" />
                        <asp:Button ID="orghierarchyprebtn" OnClientClick="return selectCode('orgh','orghierarchypre','ALL')" Text="..." runat="server" />
                        <asp:Button ID="orghierarchyprebtndel" OnClientClick="return deleteSelCode('orghierarchypre')" Text="-" runat="server" />--%>
                        <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
		                    ID="OrgHierarchyCode" ControlName="OrgHierarchyCode" RMXRef="/Instance/Document/Document/discount/OrgHierarchyCode"
		                    RMXType="orgh" name="OrgHierarchyCode" cancelledvalue="" TabIndex="5" />
		                <asp:Button runat="server" class="CodeLookupControl" Text="" 
		                    ID="OrgHierarchyCodebtn" 
		                    OnClientClick="return selectCode('orgh','OrgHierarchyCode','ALL');" 
		                    TabIndex="6"/>
		                <asp:TextBox Style="display: none" runat="server" ID="OrgHierarchyCode_cid" RMXRef="/Instance/Document/Document/discount/OrgHierarchyCode" cancelledvalue="" />
                </td>
            </tr>
            <tr id="">
                <td>
                    Org Hierarchy Level(For Display Only):&nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="orghierarchylevelpre" TabIndex="7" onchange="setDataChanged(true);">
                        <asp:ListItem Value="1005">Client</asp:ListItem>
                        <asp:ListItem Value="1006">Company</asp:ListItem>
                        <asp:ListItem Value="1007">Operation</asp:ListItem>
                        <asp:ListItem Value="1008">Region</asp:ListItem>
                        <asp:ListItem Value="1009">Division</asp:ListItem>
                        <asp:ListItem Value="1010">Location</asp:ListItem>
                        <asp:ListItem Value="1011">Facility</asp:ListItem>
                        <asp:ListItem Value="1012">Department</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
                                    
            <tr id="">
                <td>
                    Include Auto Payments?:&nbsp;&nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" appearance="full" RMXRef="/Instance/Document//IncludeAutoPaymentsFlag"
                        TabIndex="8" ID="includeautopayments" onchange="ApplyBool(this);"  AutoPostBack="false" />
                </td>
            </tr>
                                  
            <tr id="">
                <td>
                    Include Combined Payments?:&nbsp;&nbsp;</td>
                <td>
                    <asp:CheckBox runat="server" appearance="full" RMXRef="/Instance/Document//IncludeCombinedPaymentsFlag"
                        TabIndex="9" ID="includecombinedpayments" onchange="ApplyBool(this);" 
                            AutoPostBack="false" />
                </td>
            </tr>
         <%--npadhy JIRA 6418 Starts - Remove the EFT check box as We now track an EFT Payment with Distribution Type as EFT--%>
            <%-- JIRA:4042 START: ajohari2 --%>
            <%--<tr id="Tr3">
                <td>
                    EFT Payment:&nbsp;&nbsp;</td>
                <td>
                    <asp:CheckBox runat="server" appearance="full" RMXRef="/Instance/Document//IsEFTPayment"
                        TabIndex="9" ID="IsEFTPayment" onchange="ApplyBool(this);" onclick="SetEFTPayment()" 
                            AutoPostBack="false" />
                    <input type="hidden" name="hdnIsEFTPayment" id="hdnIsEFTPayment" RMXRef="/Instance/Document//IsEFTPayment" runat="server"/>
                </td>
            </tr>--%>
            <%-- JIRA:4042 End: ajohari2 --%>              
         <%--npadhy JIRA 6418 Ends - Remove the EFT check box as We now track an EFT Payment with Distribution Type as EFT--%>
            <tr id="Tr1">
                <td>
                    Postcheck Register Report Type:&nbsp;&nbsp;    
                </td>
                <td>
                        <asp:RadioButton runat="server" ID="detail" RMXRef="/Instance/Document//RegisterType"
                        RMXType="radio" onclick="UncheckOthers(this.id);" Checked="true" TabIndex="10"/>Detail
                        <asp:RadioButton runat="server" ID="summary" RMXRef="/Instance/Document//RegisterType" 
                        RMXType="radio" onclick="UncheckOthers(this.id);" TabIndex="11"/>Summary
                        <asp:RadioButton runat="server" ID="subaccount" RMXRef="/Instance/Document//RegisterType" 
                        RMXType="radio" onclick="UncheckOthers(this.id);" TabIndex="12"/>Sub Account
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="hdnShowPleaseWait" id="hdnShowPleaseWait" />
                    <asp:TextBox runat="server" style="display: none" id="fileandconsolidate"  rmxref="/Instance/Document//FileAndConsolidate"/>
                    <input type="hidden" name="hTabName" id="hTabName"  runat="server"  value="precheckregister" />
                    <%--npadhy JIRA 6418 Starts Hidden control to keep track of EFTAccount and Distribution Type of EFT--%>
                    <asp:TextBox runat="server" style="display: none" id="EFTDistributionType"/>
                    <asp:TextBox runat="server" style="display: none" id="EFTAccount"/>
                    <%--npadhy JIRA 6418 Ends Hidden control to keep track of EFTAccount and Distribution Type of EFT--%>
                </td>
            </tr>
        </tbody>
    </table>
   </div>
    </form>
</body>
</html>
