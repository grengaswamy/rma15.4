﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using System.Text;
using System.Xml;
using Riskmaster.UI.DataIntegratorService;
using System.IO; 
using Riskmaster.ServiceHelpers;
//Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
using Riskmaster.UI.DataIntegratorService;   //ipuri 12/06/2014 SOA Service discontinued
using System.IO;
using Riskmaster.Models;    //ipuri 12/06/2014



namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class MMSEASettings : System.Web.UI.Page
    {
        private int iOptionSetIDParam;
        private const string sModuleName = "MMSEA";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                iOptionSetIDParam = CommonFunctions.OptionsetId;
                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                RetriveSettingsFromDB();
                SetExtraOptionFields();
                //mrishi2 - JIRA 16461 - Changes BEGINS
                RetrieveSettings(); 
                if (ddlExportRREId.SelectedIndex > 0)
                {
                    if (ddlExportRREId.Items.Count > 1)
                    {
                        ddlExportRREId_SelectedIndexChanged(null, null);


                    }
                }
            }
            
            ClientScript.RegisterClientScriptBlock(this.GetType(), "anything", "document.body.onload = function(){tabChange('" + hTabName.Value + "')};", true);
        }
        //npradeepshar 01/12/2010 MITS 23301
        /// <summary>
        /// Function will save setting into data integrator table. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private int m_iOptionSetID; 
        private const string m_sModuleName = "MMSEA"; //Modules Name
        private XElement XmlTemplate = null;
        private string sCWSresponse = "";

        protected void RetrieveSettings()
        {
            //DataIntegratorService.DataIntegratorServiceClient objDIService = new DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objDIModel = new Riskmaster.Models.DataIntegratorModel();
            //Vsoni5 : MITS 22565 : Exception handling done for Error display on UI and error logging in service error log.
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
            }

            bool isError = false;

            try
            {
                m_iOptionSetID = CommonFunctions.OptionsetId;

                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);

                objDIModel.OptionSetID = m_iOptionSetID;
                objDIModel.ModuleName = m_sModuleName;

                //objDIModel = objDIService.RetrieveSettings(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);

               
                hdClaimLOB.Value = objDIModel.LOB.ToString();
                ListBox1.DataSource = objDIModel.dsClaimTypeGC;
                ListBox1.DataTextField = objDIModel.dsClaimTypeGC.Tables[0].Columns[0].ToString();
                ListBox1.DataValueField = objDIModel.dsClaimTypeGC.Tables[0].Columns[1].ToString();
                ListBox1.DataBind();
                ListBox1.Items.Insert(0, "All Sub Claim Types");

                ListBox2.DataSource = objDIModel.dsClaimTypeVA;
                ListBox2.DataTextField = objDIModel.dsClaimTypeVA.Tables[0].Columns[0].ToString();
                ListBox2.DataValueField = objDIModel.dsClaimTypeVA.Tables[0].Columns[1].ToString();
                ListBox2.DataBind();
                ListBox2.Items.Insert(0, "All Sub Claim Types");

                ListBox3.DataSource = objDIModel.dsClaimTypeWC;
                ListBox3.DataTextField = objDIModel.dsClaimTypeWC.Tables[0].Columns[0].ToString();
                ListBox3.DataValueField = objDIModel.dsClaimTypeWC.Tables[0].Columns[1].ToString();
                ListBox3.DataBind();
                ListBox3.Items.Insert(0, "All Sub Claim Types");

                Dictionary<string, string> ISO_Parms = new Dictionary<string, string>();
                ISO_Parms = objDIModel.Parms;

                //mihtesham - start
                bIsCarrier.Value = ((objDIModel.bIsCarrier) ? -1 : 0).ToString();
                //mihtesham - end


                //ipuri end
            }
            catch (Exception ee)
            {
                //Vsoni5 : MITS 22565 : Exception thrown by Service Layer will be shown on UI.
                err.Add("DataIntegrator.Error", ee.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
				//mrishi2 - JIRA 16461 - Changes ENDS
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveSetting();
        }
        private void RetriveSettingsFromDB()
        {
            //ipuri REST Service Conversion 12/06/2014 Start
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objReturnModel = null;
            //DataIntegratorService.DataIntegratorServiceClient objService = null;
            Riskmaster.Models.DataIntegratorModel objReturnModel = null;
            //ipuri REST Service Conversion 12/06/2014 End

            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            bool bErr = false;
            try
            {
                //objReturnModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();		//ipuri 12/06/2014
                objReturnModel = new Riskmaster.Models.DataIntegratorModel();
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objReturnModel.Token = AppHelper.GetSessionId();
                }
                //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();	//ipuri 12/06/2014

                objReturnModel.OptionSetID = iOptionSetIDParam;


                objReturnModel.ModuleName = sModuleName;
                //Anand MITS 17486,17584.
                try
                {
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objReturnModel = objService.RetrieveSettings(objReturnModel);
                    objReturnModel.ClientId = AppHelper.ClientId;

                    objReturnModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objReturnModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                }
                catch (Exception exp)
                {
                    //Vsoni5 : MITS 22565 : Exception thrown by Service layer will be displayed on UI instead of hard coded error message
                    err.Add("DataIntegrator.Error", exp.Message, BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                Dictionary<string, string> MMSEA_Setting = new Dictionary<string, string>();
                ddlExportRREId.DataSource = objReturnModel.dsRREId;
                ddlExportRREId.DataTextField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlExportRREId.DataValueField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlExportRREId.DataBind();
                ddlExportRREId.Items.Insert(0, "--Select--");

                ddlImportRREId.DataSource = objReturnModel.dsRREId;
                ddlImportRREId.DataTextField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlImportRREId.DataValueField = objReturnModel.dsRREId.Tables[0].Columns[0].ToString();
                ddlImportRREId.DataBind();
                ddlImportRREId.Items.Insert(0, "--Select--");



                MMSEA_Setting = (Dictionary<string, string>)objReturnModel.Parms;

                if (MMSEA_Setting["TYPE_OF_FILE"] == "Export")
                {
                    hTabName.Value = "exportsetting";
                    if (objReturnModel.OptionSetName != null)
                    {
                        txtExportOptionset.Text = objReturnModel.OptionSetName;
                        txtExportOptionset.ReadOnly = true;
                    }
                    try
                    {
                        ddlExportRREId.Items.FindByText(MMSEA_Setting["RRE_ID"]).Selected = true;
                    }
                    catch
                    {
                        ddlExportRREId.SelectedIndex = 0;
                    }
                    try
                    {
                        ddlExportFileFormat.Items.FindByText(MMSEA_Setting["FILE_FORMAT"]).Selected = true;
                    }
                    catch
                    {
                        ddlExportFileFormat.SelectedIndex = 0;
                    }
                    if (MMSEA_Setting["TEST_EXPORT"] == "-1")
                        chkTestExport.Checked = true;
                    else
                        chkTestExport.Checked = false;


                    //mcapps3 08/03/2011 MITS 25536
                    try
                    {
                        if (MMSEA_Setting["PRIMARY_CLAIMANT"] == "-1")
                            chkPrimayClaimant.Checked = true;
                        else
                            chkPrimayClaimant.Checked = false;
                    }

                    catch
                    {
                        chkPrimayClaimant.Checked = false;
                    }
                    //mcapps3 08/03/2011 MITS 25634
                    try
                    {
                        if (MMSEA_Setting["TPOCS_THRESHOLD"] == "-1")

                            chkTPOCSThreshold.Checked = true;
                        else
                            chkTPOCSThreshold.Checked = false;
                    }
                    catch
                    {
                        chkTPOCSThreshold.Checked = false;
                    }


                }
                else if (MMSEA_Setting["TYPE_OF_FILE"] == "Import")
                {
                    hTabName.Value = "importsetting";
                    if (objReturnModel.OptionSetName != null)
                    {
                        txtImportOptionset.Text = objReturnModel.OptionSetName;
                        txtImportOptionset.ReadOnly = true;
                    }
                    try
                    {
                        ddlImportRREId.Items.FindByText(MMSEA_Setting["RRE_ID"]).Selected = true;
                    }
                    catch
                    {
                        ddlExportRREId.SelectedIndex = 0;
                    }
                    try
                    {
                        ddlImportFileFormat.Items.FindByText(MMSEA_Setting["FILE_FORMAT"]).Selected = true;
                    }
                    catch
                    {
                        ddlImportFileFormat.SelectedIndex = 0;
                    }

                }

            }
            catch (Exception ex)
            {
            }
            if (bErr == true)       //Anand MITS 17486,17584.
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }
        /// <summary>
        /// Function to save UI Settings
        /// </summary>
        private void SaveSetting()
        {//npradeepshar MITS 23301 17/01/2010 Logs the error into log file as well display it on UI
            string sTaskManagerXml = string.Empty;
            //ipuri REST Service Conversion 12/06/2014 Start
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            //DataIntegratorService.DataIntegratorServiceClient objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objModel = null;
            //ipuri REST Service Conversion 12/06/2014 End
            BusinessAdaptorErrors err = null;
            bool bErr = false;
            try
            {
                objModel = new Riskmaster.Models.DataIntegratorModel();
                err = new BusinessAdaptorErrors();
                sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetIDParam = Conversion.ConvertObjToInt(hdOptionsetId.Value);
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }
                Dictionary<string, string> MMSEA_Setting = new Dictionary<string, string>();
                //Anand MITS 17486,17584.
                if (txtExportOptionset.Text == "")
                {
                    err.Add("DataIntegrator.Error", "Optionset Name can not be blank. ", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlExportFileFormat.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a file format.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlExportRREId.SelectedItem == null || ddlExportRREId.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a RRE ID.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if (bErr == true)      //Anand MITS 17486,17584.
                {
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                MMSEA_Setting.Add("RRE_ID", ddlExportRREId.SelectedItem.Text);
                MMSEA_Setting.Add("TYPE_OF_FILE", "Export");
                MMSEA_Setting.Add("FILE_FORMAT", ddlExportFileFormat.SelectedItem.Text);
                if (chkTestExport.Checked)
                    MMSEA_Setting.Add("TEST_EXPORT", "-1");
                else
                    MMSEA_Setting.Add("TEST_EXPORT", "0");

                MMSEA_Setting.Add("IMPORTFILE_NAME", "");
                objModel.OptionSetName = txtExportOptionset.Text;

                //mcapps3 08/03/2011 MITS 25536
                if (chkPrimayClaimant.Checked)
                    MMSEA_Setting.Add("PRIMARY_CLAIMANT", "-1");
                else
                    MMSEA_Setting.Add("PRIMARY_CLAIMANT", "0");

                //mcapps3 08/04/2011 MITS 25634
                if (chkTPOCSThreshold.Checked && ddlExportFileFormat.Text == "Claim Input")
                    MMSEA_Setting.Add("TPOCS_THRESHOLD", "-1");
                else
                    MMSEA_Setting.Add("TPOCS_THRESHOLD", "0");

                //mcapps3 09/02/2011 MITS 25532
                MMSEA_Setting.Add("TIN_IMPORTFILE_NAME", "");

                //mrishi2 - JIRA 16461 - Changes BEGINS
                int iIndex = 0;
                string sClaimType = "";

                if (ListBox1Val.Value == "1")
                {

                    MMSEA_Setting.Add("LOBGC", "1");
                    for (iIndex = 0; iIndex < ListBox1.Items.Count; iIndex++)
                    {
                        if (ListBox1.Items[0].Selected == true)
                        {
                            if (iIndex > 0)
                            {
                                sClaimType = sClaimType + ListBox1.Items[iIndex].Value.ToString() + ",";
                            }
                        }
                        else if (ListBox1.Items[iIndex].Selected == true)
                        {
                            sClaimType = sClaimType + ListBox1.Items[iIndex].Value.ToString() + ",";
                        }
                    }
                    if (sClaimType != "")
                    {
                        sClaimType = sClaimType.Substring(0, sClaimType.Length - 1);
                    }
                    else
                    {
                        err.Add("DataIntagrator.Error", "Please select Claim Types for General Claims LOB.", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                    MMSEA_Setting.Add("ClaimSubTypeGC", sClaimType);
                }
                else
                {
                    MMSEA_Setting.Add("LOBGC", "0");
                    MMSEA_Setting.Add("ClaimSubTypeGC", sClaimType);
                }

                sClaimType = "";

                if (ListBox2Val.Value == "1")
                {
                    MMSEA_Setting.Add("LOBVA", "1");
                    for (iIndex = 0; iIndex < ListBox2.Items.Count; iIndex++)
                    {
                        if (ListBox2.Items[0].Selected == true)
                        {
                            if (iIndex > 0)
                            {
                                sClaimType = sClaimType + ListBox2.Items[iIndex].Value.ToString() + ",";
                            }
                        }
                        else if (ListBox2.Items[iIndex].Selected == true)
                        {
                            sClaimType = sClaimType + ListBox2.Items[iIndex].Value.ToString() + ",";
                        }
                    }
                    if (sClaimType != "")
                    {
                        sClaimType = sClaimType.Substring(0, sClaimType.Length - 1);
                    }
                    else
                    {
                        err.Add("DataIntagrator.Error", "Please select Claim Types for Vehicle Accident Claims LOB.", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                    MMSEA_Setting.Add("ClaimSubTypeVA", sClaimType);
                }
                else
                {
                    MMSEA_Setting.Add("LOBVA", "0");
                    MMSEA_Setting.Add("ClaimSubTypeVA", sClaimType);
                }

                sClaimType = "";

                if (ListBox3Val.Value == "1")
                {
                    MMSEA_Setting.Add("LOBWC", "1");
                    for (iIndex = 0; iIndex < ListBox3.Items.Count; iIndex++)
                    {
                        if (ListBox3.Items[0].Selected == true)
                        {
                            if (iIndex > 0)
                            {
                                sClaimType = sClaimType + ListBox3.Items[iIndex].Value.ToString() + ",";
                            }
                        }
                        else if (ListBox3.Items[iIndex].Selected == true)
                        {
                            sClaimType = sClaimType + ListBox3.Items[iIndex].Value.ToString() + ",";
                        }
                    }
                    if (sClaimType != "")
                    {
                        sClaimType = sClaimType.Substring(0, sClaimType.Length - 1);
                    }
                    else
                    {
                        err.Add("DataIntagrator.Error", "Please select Claim Types for Workers' Compensation Claims LOB.", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }

                    MMSEA_Setting.Add("ClaimSubTypeWC", sClaimType);
                }
                else
                {
                    MMSEA_Setting.Add("LOBWC", "0");
                    MMSEA_Setting.Add("ClaimSubTypeWC", sClaimType);
                }
                sClaimType = "";
             	//mrishi2 - JIRA 16461 - Changes ENDS
                objModel.Parms = MMSEA_Setting;
                objModel.OptionSetID = iOptionSetIDParam;
                objModel.ModuleName = sModuleName;

                objModel.TaskManagerXml = sTaskManagerXml;

                //ipuri REST Service Conversion 12/06/2014 Start
                //objModel = objService.SaveSettings(objModel);
                objModel.ClientId = AppHelper.ClientId;

                objModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                //ipuri REST Service Conversion 12/06/2014 End
                if (objModel.OptionSetID == -1)
                {
                    //Anand MITS 17486,17584.

                    err.Add("DataIntagrator.Error", "OptionSet Name already exists.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }

                if (objModel.OptionSetID > 0)
                {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                }
            }
            catch (Exception ex)
            {
                //npradeepshar 03/25/2011: Removed the code form event and added to the method.MITS 23301
                //npradeepshar 01/12/2010 MITS 23301 : Log the error into log file as well display it on UI
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                /// Vsoni5 : 01/29/2011 : MITS 23347
                try
                {
                    if (iOptionSetIDParam <= 0 && objModel.OptionSetID > 0)
                        //objService.SaveSettingsCleanup(objModel);
                        objModel.ClientId = AppHelper.ClientId;

                    objModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);

                }
                catch (Exception eCleanup)
                {
                }
                return;
            }

        }

        protected void btnSaveImport_Click(object sender, EventArgs e)
        {
            SaveImportSetting();
        }

        private void SaveImportSetting()
        {
            string sTaskManagerXml = string.Empty;
            //ipuri REST Service Conversion 12/06/2014 Start
            //kkaur25 -SOA start
            Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModelS = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            DataIntegratorService.DataIntegratorServiceClient objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
            Riskmaster.Models.DataIntegratorModel objModel = null;
            //kkaur25 -SOA end
            //ipuri REST Service Conversion 12/06/2014 End
            BusinessAdaptorErrors err = null;
            bool bErr = false;

            try
            {
                objModel = new Riskmaster.Models.DataIntegratorModel();
                err = new BusinessAdaptorErrors();

                sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetIDParam = Conversion.ConvertObjToInt(hdOptionsetId.Value);
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }
                Dictionary<string, string> MMSEA_Setting = new Dictionary<string, string>();
                //Anand MITS 17486,17584.
                if (txtImportOptionset.Text == "")
                {
                    err.Add("DataIntegrator.Error", "Optionset Name can not be blank. ", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlImportFileFormat.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a file format.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Anand MITS 17486,17584.
                if (ddlImportRREId.SelectedItem == null || ddlImportRREId.SelectedItem.Value == "--Select--")
                {
                    err.Add("DataIntegrator.Error", "Please choose a RRE ID.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                MMSEA_Setting.Add("RRE_ID", ddlImportRREId.SelectedItem.Text);
                MMSEA_Setting.Add("TYPE_OF_FILE", "Import");
                MMSEA_Setting.Add("FILE_FORMAT", ddlImportFileFormat.SelectedItem.Text);
                MMSEA_Setting.Add("TEST_EXPORT", "");

                string sFileName = FUImport.PostedFile.FileName.Substring(FUImport.PostedFile.FileName.LastIndexOf("\\") + 1);
                //mcapps3 09/02/2011 added sTinFileName for MITS 25532
                string sTinFileName = "";
                if (FUTinImport.Visible == true)
                {
                    sTinFileName = FUTinImport.PostedFile.FileName.Substring(FUTinImport.PostedFile.FileName.LastIndexOf("\\") + 1);
                }

                //Anand MITS 17486,17584.
                if (sFileName == null || sFileName == "")
                {
                    err.Add("DataIntegrator.Error", "Please choose a file to be imported.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                if (bErr == true)      //Anand MITS 17486,17584.
                {
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                MMSEA_Setting.Add("IMPORTFILE_NAME", sFileName);

                objModel.OptionSetName = txtImportOptionset.Text;
                //mcapps3 08/03/2011 MITS 25536
                MMSEA_Setting.Add("PRIMARY_CLAIMANT", "0");
                //mcapps3 08/04/2011 MITS 25634
                MMSEA_Setting.Add("TPOCS_THRESHOLD", "0");
                //mcapps3 09/02/2011 MITS 25532
                MMSEA_Setting.Add("TIN_IMPORTFILE_NAME", sTinFileName);

                objModel.Parms = MMSEA_Setting;
                objModel.OptionSetID = iOptionSetIDParam;
                objModel.ModuleName = sModuleName;
                objModel.ImportFlage = true;
                objModel.TaskManagerXml = sTaskManagerXml;
                //ipuri REST Service Conversion 12/06/2014 Start
                //objModel = objService.SaveSettings(objModel);

                objModel.ClientId = AppHelper.ClientId;

                objModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                //ipuri REST Service Conversion 12/06/2014 End
                if (objModel.OptionSetID == -1)
                {
                    //Anand MITS 17486,17584.
                    err.Add("DataIntagrator.Error", "OptionSet Name is already used.", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                //Vsoni5 : MITS 22699 : This block of code is no more required
                //else
                //{
                //    if (!String.IsNullOrEmpty(sFileName))
                //    {
                //        //Anand: Appended option set ID in the file name.
                //        sFileName = sFileName + "." + objModel.OptionSetID;
                //        //FUImport.PostedFile.SaveAs(objModel.FilePath + sFileName);
                //    }
                //    //Anand MITS 17487.
                //    //FUImport.SaveAs(objModel.FilePath + sFileName);
                //}

                //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer                
                if (objModel.OptionSetID > 0)
                {
                    //kkaur25-SOA start
                    Riskmaster.Models.DAImportFile objDAImportFile = new Riskmaster.Models.DAImportFile();
                    Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                    //kkaur25-SOA end     

                    objDAImportFile.Token = AppHelper.GetSessionId();
                    Stream data = FUImport.PostedFile.InputStream;
                    objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                    objDAImportFile.fileName = sFileName + "." + objModel.OptionSetID;
                    objDAImportFile.filePath = objModel.FilePath;
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                    //kkaur25 start- JIRA RMACLOUD 3070 MMSEA CLOUD EXE CHANGES
                    objDAImportFile.ModuleName = "MMSEA";
                    objDAImportFile.DocumentType = "Import";
                    objDAImportFile.OptionsetId = objModel.OptionSetID;
                    //kkaur25 end- JIRA RMACLOUD 3070 MMSEA CLOUD EXE CHANGES
                    objDAImportFile.ClientId = AppHelper.ClientId;

                    //kkaur25-SOA change start
                    objDAImportFileS.FileContents = objDAImportFile.FileContents;
                    objDAImportFileS.fileName = objDAImportFile.fileName;
                    objDAImportFileS.filePath = objModel.FilePath;
                    objDAImportFileS.Token = AppHelper.Token;
                    objDAImportFileS.ClientId = AppHelper.ClientId;
                    objDAImportFileS.ModuleName = "MMSEA";
                    objDAImportFileS.DocumentType = "Import";
                    objDAImportFileS.OptionsetId = objModel.OptionSetID;
                    //Call SOAP/XML service
                    objService.UploadDAImportFile(objDAImportFileS);
                    //kkaur25 SOA end

                    //AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile); 
                    //ipuri REST Service Conversion 12/06/2014 End

                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                    //mcapps3 01/24/2012  MITS 27183 added if statment to make sure there is a tin file to import 
                    if (sTinFileName.Length != 0)
                    {
                        Riskmaster.Models.DAImportFile objDATINImportFile = new Riskmaster.Models.DAImportFile();
                        Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileT = new Riskmaster.UI.DataIntegratorService.DAImportFile();
                        objDATINImportFile.Token = AppHelper.GetSessionId();
                        //mcapps3 12/8/2011 MITS 26778 add functionality to move the TIN file to the RMX server   
                        Stream dataTIN = FUTinImport.PostedFile.InputStream;
                        objDATINImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(dataTIN);
                        objDATINImportFile.fileName = sTinFileName + "." + objModel.OptionSetID;
                        objDATINImportFile.filePath = objModel.FilePath;
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objService.UploadDAImportFile(objDATINImportFile.Errors, objDATINImportFile.Token, objDATINImportFile.fileName, objDATINImportFile.filePath, objDATINImportFile.fileStream);
                        //kkaur25 start-JIRA RMACLOUD 3070  MMSEA CLOUD EXE CHANGES
                        objDATINImportFile.ModuleName = "MMSEA";
                        objDATINImportFile.DocumentType = "Import";
                        objDATINImportFile.OptionsetId = objModel.OptionSetID;
                        //kkaur25 end-JIRA RMACLOUD 3070  MMSEA CLOUD EXE CHANGES
                        objDATINImportFile.ClientId = AppHelper.ClientId;

                        //kkaur25-SOA change start
                        objDAImportFileT.FileContents = objDATINImportFile.FileContents;
                        objDAImportFileT.fileName = objDATINImportFile.fileName;
                        objDAImportFileT.filePath = objModel.FilePath;
                        objDAImportFileT.Token = AppHelper.Token;
                        objDAImportFileT.ClientId = AppHelper.ClientId;
                        objDAImportFileT.ModuleName = "MMSEA";
                        objDAImportFileT.DocumentType = "Import";
                        objDAImportFileT.OptionsetId = objModel.OptionSetID;
                        //Call SOAP/XML service
                        objService.UploadDAImportFile(objDAImportFileT);
                        //kkaur25 SOA end

                        //AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDATINImportFile); 
                        //ipuri REST Service Conversion 12/06/2014 End
                        Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                    }
                }
            }
            catch (Exception Ex)
            {
                //Anand MITS 17486,17584.
                err.Add(Ex, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                /// Vsoni5 : 01/29/2011 : MITS 23347
                try
                {
                    if (iOptionSetIDParam <= 0 && objModel.OptionSetID > 0)
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objService.SaveSettingsCleanup(objModel);
                        objModel.ClientId = AppHelper.ClientId;
                    AppHelper.GetResponse("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                }
                catch (Exception eCleanup)
                {
                }

            }
        }


        /// <summary>
        /// mcapps3  MITS 25634 09/17/2011
        /// The SetExtraOptionFields set the ddlExportFileFormat based on if it is a claim or query export.
        /// mcapps3 MITS 25532 added code to make fiel to browse do the the fine visible and un visible
        /// </summary>
        private void SetExtraOptionFields()
        {
            if (hTabName.Value == "exportsetting")
            {
                if (ddlExportFileFormat.Text == "Claim Input")
                {
                    Claim_TPOC.Visible = true;
                    Claim_TIN.Visible = false;
                }
                else if (ddlExportFileFormat.Text == "Query Input")
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = false;
                }
                else
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = false;
                }

            }
            else if (hTabName.Value == "importsetting")
            {

                if (ddlImportFileFormat.Text == "Claim")
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = true;
                }
                else
                {
                    Claim_TPOC.Visible = false;
                    Claim_TIN.Visible = false;
                }

            }
            else
            {
                Claim_TPOC.Visible = false;
                Claim_TIN.Visible = false;
            }
        }

        /// <summary>
        /// mcapps3  MITS 25634 08/17/2011
        /// when the ddlExportFileFormat changes this the ddlExportFileFormat_OnSelectedIndexChanged runs. 
        /// </summary>
        protected void ddlExportFileFormat_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetExtraOptionFields();
            PostFileFormatSelection();

        }
        /// mcapps3  MITS 25582 09/02/2011
        /// when the ddlImportFileFormat changes this the ddlImportFileFormat_OnSelectedIndexChanged runs. 
        /// </summary>

        protected void ddlImportFileFormat_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetExtraOptionFields();
        }
		//mrishi2 - JIRA 16461 - Changes BEGINS
        protected void chkTPOCSThreshold_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void txtExportOptionset_TextChanged(object sender, EventArgs e)
        {

        }
        protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            PostFileFormatSelection();
        }
        protected void ddlExportRREId_SelectedIndexChanged(object sender, EventArgs e)
        {
            PostFileFormatSelection();
            this.ListBox1.SelectedIndex = 0;
            this.ListBox2.SelectedIndex = 0;
            this.ListBox3.SelectedIndex = 0;
        }
        protected void PostFileFormatSelection()
        {
            string LOB1, LOB2;
            Riskmaster.Models.DataIntegratorModel objDIModel = null;

            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            bool bErr = false;
            try
            {
                objDIModel = new Riskmaster.Models.DataIntegratorModel();
                objDIModel.sRREIdSV = ddlExportRREId.SelectedValue; 


                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }
                objDIModel.OptionSetID = m_iOptionSetID;
                objDIModel.ModuleName = m_sModuleName;
                objDIModel.ClientId = AppHelper.ClientId;
                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);

                LOB1 = objDIModel.dsRREId.Tables[0].Rows[0][0].ToString();
                LOB2 = objDIModel.dsRREId.Tables[0].Rows[0][1].ToString();
                ListBox1Val.Value = "0";
                ListBox2Val.Value = "0";
                ListBox3Val.Value = "0";
            

                if (LOB1 == "241" && (LOB2 == "0" || LOB2 == "241"))
                {

                    ListBox1.Attributes.Remove("disabled");
                    ListBox1Val.Value = "1";
                }
                else if (LOB1 == "242" && (LOB2 == "0" || LOB2 == "242"))
                {
                    ListBox2.Attributes.Remove("disabled");
                    ListBox2Val.Value = "1";
                }
                else if (LOB1 == "243" && (LOB2 == "0" || LOB2 == "243"))
                {
                    ListBox3.Attributes.Remove("disabled");
                    ListBox3Val.Value = "1";
                }
                else if (LOB1 == "241" && LOB2 == "242")
                {
                    ListBox1.Attributes.Remove("disabled");
                    ListBox2.Attributes.Remove("disabled");
                    ListBox1Val.Value = "1";
                    ListBox2Val.Value = "1";
                }
                else if (LOB1 == "241" && LOB2 == "243")
                {
                    ListBox1.Attributes.Remove("disabled");
                    ListBox3.Attributes.Remove("disabled");
                    ListBox1Val.Value = "1";
                    ListBox3Val.Value = "1";

                }
                else if (LOB1 == "242" && LOB2 == "241")
                {
                    ListBox1.Attributes.Remove("disabled");
                    ListBox2.Attributes.Remove("disabled");
                    ListBox1Val.Value = "1";
                    ListBox2Val.Value = "1";
                }
                else if (LOB1 == "242" && LOB2 == "243")
                {
                    ListBox2.Attributes.Remove("disabled");
                    ListBox3.Attributes.Remove("disabled");
                    ListBox2Val.Value = "1";
                    ListBox3Val.Value = "1";
                }
                else if (LOB1 == "243" && LOB2 == "241")
                {
                    ListBox3.Attributes.Remove("disabled");
                    ListBox1.Attributes.Remove("disabled");
                    ListBox1Val.Value = "1";
                    ListBox3Val.Value = "1";
                }
                else if (LOB1 == "243" && LOB2 == "242")
                {
                    ListBox2.Attributes.Remove("disabled");
                    ListBox3.Attributes.Remove("disabled");
                    ListBox2Val.Value = "1";
                    ListBox3Val.Value = "1";
                }
                else
                {
                    ListBox1.Attributes.Remove("disabled");
                    ListBox2.Attributes.Remove("disabled");
                    ListBox3.Attributes.Remove("disabled");
                    ListBox1Val.Value = "1";
                    ListBox2Val.Value = "1";
                    ListBox3Val.Value = "1";
                }

            }
            catch (Exception exp)
            {
                err.Add("DataIntegrator.Error", exp.Message, BusinessAdaptorErrorType.Message);
                bErr = true;

            }
			//mrishi2 - JIRA 16461 - Changes ENDS
        }
    }
}
