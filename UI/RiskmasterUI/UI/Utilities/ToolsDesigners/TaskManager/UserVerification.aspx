﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserVerification.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.UserVerification" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>User Verification</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <style type="text/css">
        .auto-style1 {
            width: 716px;
        }
        .auto-style2 {
            width: 204px;
        }
        .auto-style3 {
            width: 220px;
        }
        .auto-style4 {
            width: 24%;
        }
    </style>
</head>
    <script type ="text/javascript">
        function calltest()
        {
            debugger;
            parent.MDIShowScreen("0",'TMView');
        }
    </script>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" name="frmData" method="post" runat="server" enctype="multipart/form-data" >
    <div>
     <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>    
        <%--<tr>
            <td>
            <asp:ScriptManager ID="ScriptManager1" runat ="server" />
            </td>
        </tr>--%>
    </table>
    <table border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td style="font-weight:bold" class="auto-style3">Import Area</td>
            <td style="font-weight:bold" class="auto-style3">Verification Table</td>
            <td style="font-weight:bold">
                    &nbsp;</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr><td class="auto-style3"><asp:DropDownList ID="lstImportArea" runat="server" 
                    onselectedindexchanged="lstImportArea_SelectedIndexChanged" 
                    AutoPostBack="True" /></td>
        <td class="auto-style3"><asp:DropDownList ID="lstVerificationTable" runat="server" 
                    onselectedindexchanged="lstVerificationTable_SelectedIndexChanged" 
                    AutoPostBack="True" /></td></tr>
        <tr>
            <td class="auto-style4"><br />
                <asp:RadioButton ID="RbtnViewData" Name="ViewData" 
                    GroupName="ViewData" runat="server" Text="View All Data" 
                    oncheckedchanged="RbtnViewData_CheckedChanged" AutoPostBack="True" 
                        Checked="True" /></td>
            <td class="auto-style4">
                <br />
                <asp:RadioButton ID="RbtnViewError" Name="ViewData" 
                GroupName="ViewData" runat="server" Text="Edit Failed Rows" 
                oncheckedchanged="RbtnViewData_CheckedChanged" AutoPostBack="True" />
                </td>
            <td style="font-weight:bold">
                &nbsp;</td>
        </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="4">
        <tr>
             <%--<td style="font-weight:bold">Verification Table</td>--%>
            <td style="width:5%"><br /></td>
           <%--<td style="font-weight:bold">
                    <asp:RadioButton ID="RbtnViewData" Name="ViewData" 
                    GroupName="ViewData" runat="server" Text="View All Data" 
                    oncheckedchanged="RbtnViewData_CheckedChanged" AutoPostBack="True" 
                        Checked="True" /></td>--%>
        </tr>
        <tr><td></td></tr>
        <tr>
            <%--<td><asp:DropDownList ID="lstVerificationTable" runat="server" 
                    onselectedindexchanged="lstVerificationTable_SelectedIndexChanged" 
                    AutoPostBack="True" /></td>
            <td style="width:5%"><br /></td>
            <%--<td style="font-weight:bold">
                <asp:RadioButton ID="RbtnViewError" Name="ViewData" 
                GroupName="ViewData" runat="server" Text="Edit Failed Rows" 
                oncheckedchanged="RbtnViewData_CheckedChanged" AutoPostBack="True" />
            </td>--%>
        </tr>
        <tr><td></td></tr>
        <tr><td><asp:Label id="lblMessage" runat="server" ></asp:Label></td></tr>
        <tr><td><br /></td></tr>
    </table>
    <%--<asp:UpdatePanel ID="test" runat ="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>--%>
        <table>
            <tr>
                <td style="overflow:hidden">
                
                    <asp:GridView ID="gvJobData" runat="server" AllowPaging="True" 
                        AllowSorting="True" 
                        onrowupdating="gvJobData_RowUpdating" 
                        onrowcancelingedit="gvJobData_RowCancelingEdit" 
                        onrowediting="gvJobData_RowEditing" RowStyle-HorizontalAlign="Center" 
                        onsorting="gvJobData_Sorting" 
                        onpageindexchanging="gvJobData_PageIndexChanging" 
                        onrowdatabound="gvJobData_RowDataBound" 
                        ondatabound="gvJobData_DataBound" style="overflow:hidden" PageSize="50" BorderColor="Black" ForeColor="Black" GridColor="Black" >
                        <Columns >
                        <asp:TemplateField >
                        <ItemTemplate>
                        <asp:LinkButton ID="EditRow" runat="server" CommandName="Edit" Text="Edit" ></asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate >
                        <asp:LinkButton ID="UpdateRow" runat="server" CommandName="Update"  Text="Update" ></asp:LinkButton>
                        <asp:LinkButton ID="CancelRow" runat="server" CommandName="Cancel" Text="Cancel" ></asp:LinkButton>
                        </EditItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                </td>
            </tr>
            <tr><td><br /></td></tr>
        </table>
        <table>
            <tr>
                <td colspan="2" style="font-weight:bold">Row Error Summary</td>
            </tr>
            <tr>
                <td><asp:TextBox ID="txtSummary" TextMode="MultiLine" runat="server" Width="700px" Height="100px"></asp:TextBox></td>
                <td class="auto-style2"></td>
                <td class="auto-style1">
                    <table>
                        <tr>
                            <td><asp:CheckBox ID="chkStopVerify" AutoPostBack="true" runat="server" Text="Stop Verification" 
                                    oncheckedchanged="chkStopVerify_CheckedChanged" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="chkAbandonTask" runat="server" Text="Abandon Task" style="display:none" /><asp:Button ID="btnResumetask" class ="button" runat="server" 
                                    Text="Resume Task" onclick="btnResumetask_Click" onClientCLick="calltest();" Enabled="true" /></td>
                        </tr>
                        <tr>
                            <td><asp:Button ID="btnConfirmAbondon" class ="button" runat="server" 
                                    Text="Confirm Abandon" Enabled="false" onclick="btnConfirmAbondon_Click" style="display:none"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:TextBox runat="server" ID="GridSortExpression" style="display:none" />
        <asp:TextBox runat="server" ID="GridSortDirection"  style="display:none" Text="ASC" />
   <%-- </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="lstVerificationTable" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="RbtnViewData" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="RbtnViewError" EventName="CheckedChanged" />--%>
        <%--</Triggers>
    </asp:UpdatePanel>--%>
    </div>
    
    </form>
</body>
</html>
