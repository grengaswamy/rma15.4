<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ISOMappingGrid.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.ISOMappingGrid" ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ISO Mapping Related Info</title>

    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js">        { var i; }</script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

    <script language="javascript" type="text/javascript">
        function fPassOptionsetIDtoParentPage() {
            //debugger;

            var hdClaimLOB = window.opener.document.getElementById('hdClaimLOB');
            var hdLOBpopup = document.getElementById('hdLOBpopup');

            hdClaimLOB.value = hdLOBpopup.value;
            window.opener.document.forms[0].submit();
            window.close();
        }

    </script>

    <style type="text/css">
        .auto-style1
        {
            height: 20px;
        }
    </style>

</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
        &nbsp;
    <div class="msgheader" id="div_formtitle">
        <asp:label id="formtitle" runat="server" text="ISO Mapping Related Info" />
    </div>

        <uc1:errorcontrol id="ErrorControl1" runat="server" />

        <asp:textbox style="display: none" runat="server" name="hTabName" id="TextBox9" />

        <asp:textbox style="display: none" runat="server" id="RowId" rmxref="/Instance/Document/ISOLossMappingList/control[@name ='RowId']" />
        <asp:textbox style="display: none" runat="server" id="selectedid" />
        <asp:textbox style="display: none" runat="server" id="mode" rmxref="/Instance/Document/ISOLossMappingList/control[@name ='mode']" />
        <asp:textbox style="display: none" runat="server" id="selectedrowposition" />
        <asp:textbox style="display: none" runat="server" id="gridname" />
        <asp:textbox style="display: none" runat="server" id="UniqueId" text="RowId" />

        <table>
                        <%-- abharti5 |MITS 36676| start  --%>
            <tr id="div_PSN" class="full" >
                <td>
                    <asp:label runat="server" class="required" id="lbl_PolicySystemNames"
                        text="Policy System Names" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td>
                     <asp:dropdownlist id="ddlPolicySystemNames" runat="server" autopostback="True"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='PSN']"
                            onselectedindexchanged="ddlPolicySystemNames_SelectedIndexChanged">
            <asp:ListItem>--Select--</asp:ListItem>
        </asp:dropdownlist>
                </td>
            </tr>
             <%--   abharti5 |MITS 36676| end--%>

            <tr id="div_LOB" class="full">
                <td>
                    <asp:label runat="server" class="required" id="lbl_PolicyLOB"
                        text="Line of Business" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td>
                    <span>
                        <asp:dropdownlist id="ddlPolicyLOB" runat="server" autopostback="True"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='LOB']"
                            onselectedindexchanged="ddlPolicyLOB_SelectedIndexChanged">
            <asp:ListItem>--Select--</asp:ListItem>
        </asp:dropdownlist>

                        &nbsp;
                    </span>
                </td>
            </tr>            
            <tr></tr>
            <tr></tr>
            <tr id="div_ClaimType">
                <td>
                    <asp:label runat="server" class="required" id="lbl_ClaimType"
                        text="Claim Type" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td>
                    <span>
                        <asp:dropdownlist id="ddlISOClaimType" runat="server" autopostback="True"
                            enabled="False"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='Claim Type']"
                            onselectedindexchanged="ddlISOClaimType_SelectedIndexChanged">
            <asp:ListItem>--Select--</asp:ListItem>
        </asp:dropdownlist>
                    </span>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr id="div_Coverage" class="full">

                <td>
                    <asp:label runat="server" class="required" id="lbl_Coverage"
                        text="Coverage Type" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td>
                    <span>
                        <asp:dropdownlist id="ddlCoverage" runat="server" autopostback="True"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='Coverage']"
                            onselectedindexchanged="ddlCoverage_SelectedIndexChanged">
            <asp:ListItem>--Select--</asp:ListItem>
        </asp:dropdownlist>

                        &nbsp;
                    </span>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr id="div_TypeLoss">
                <td class="auto-style1">
                    <asp:label runat="server" class="required" id="lbl_typeOfLoss"
                        text="Type Of Loss" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td class="auto-style1">

                    <span>
                        <asp:dropdownlist id="ddlTypeOfLoss" runat="server" autopostback="True"
                            enabled="False"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='Type Of Loss']"
                            onselectedindexchanged="ddlTypeOfLoss_SelectedIndexChanged">
            <asp:ListItem>--Select--</asp:ListItem>
        </asp:dropdownlist>

                    </span>
                    <td class="auto-style1"></td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr>

                <span>
                    <td>
                        <asp:label runat="server" class="required" id="lbl_DisabilityCode"
                            text="Disability Code" width="160px" font-names="Times New Roman"
                            font-size="Medium" />
                    </td>
                    <td>
                        <asp:dropdownlist id="ddlDisabilityCode" runat="server" autopostback="True"
                            enabled="False"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='Disability Code']"
                            onselectedindexchanged="ddlDisabilityCode_SelectedIndexChanged">
            <asp:ListItem>--Select--</asp:ListItem>
        </asp:dropdownlist>
                    </td>
                </span>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr id="div_PolicyType">
                <td>
                    <asp:label runat="server" class="required" id="lbl_PolicyType"
                        text="ISO Policy Type" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td>
                    <span>
                        <asp:dropdownlist id="ddlISOPolicyType" runat="server"
                            autopostback="True" enabled="False"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='ISO Policy Type']"
                            onselectedindexchanged="ddlISOPolicyType_SelectedIndexChanged">
                <asp:ListItem>--Select--</asp:ListItem>
            </asp:dropdownlist>
                    </span>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr id="div_CoverageType">
                <td>
                    <asp:label runat="server" class="required" id="lbl_CoverageType"
                        text="ISO Coverage Type" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td>
                    <span>
                        <asp:dropdownlist id="ddlISOCoverageType" runat="server"
                            autopostback="True" enabled="False"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='ISO Coverage Type']"
                            onselectedindexchanged="ddlISOCoverageType_SelectedIndexChanged">
                <asp:ListItem>--Select--</asp:ListItem>
            </asp:dropdownlist>
                    </span>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr id="div_LossType">
                <td>
                    <asp:label runat="server" class="required" id="lbl_LossType"
                        text="ISO Loss Type" width="160px" font-names="Times New Roman"
                        font-size="Medium" />
                </td>
                <td>
                    <span>
                        <asp:dropdownlist id="ddlISOLossType" runat="server" autopostback="True"
                            enabled="False"
                            rmxref="/Instance/Document/ISOLossMappingList/control[@name ='ISO Loss Type']"
                            onselectedindexchanged="ddlISOLossType_SelectedIndexChanged1">
                <asp:ListItem>--Select--</asp:ListItem>
             </asp:dropdownlist>
                    </span>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>


            <%--class="required"--%>
            <tr>
                <td>
                    <asp:textbox style="display: none" runat="server" id="txtData" />
                    <asp:textbox style="display: none" runat="server" id="txtPostBack" />
                </td>
                <td></td>
            </tr>

            <%--    <div id="div_buttons" class="formButtonGroup" align="center">--%>

            <tr class="formButton" id="div_btnOk" align="center">
                <td align="right">
                    <asp:button class="button" runat="server" id="btnSave" text="Save"
                        width="75px" onclick="btnSave_Click" />
                    </td>
                    <td align="left">
                    <asp:button class="button" runat="server" id="btnCancel" text="Cancel"
                        width="75px" onclick="btnCancel_Click" />
                </td>
                <%--<td class="formButton" id="div_btnCancel" align="center">
                    </td>--%>
                <%--class="required"--%>
                
            </tr>
            <tr >
                <td></td>
                <td>
                    
                </td>
            </tr>

            <%--   </div>--%>


            <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="" />


            <asp:hiddenfield id="hdLOBpopup" runat="server" />
            <%--hdOptionsetpopup--%>
            <asp:hiddenfield id="hdTaskManagerXml" runat="server" />
            <asp:hiddenfield id="hdOptionsetId" runat="server" />
            <asp:hiddenfield id="hdnaction" runat="server" />
        </table>
    </form>
</body>
</html>
