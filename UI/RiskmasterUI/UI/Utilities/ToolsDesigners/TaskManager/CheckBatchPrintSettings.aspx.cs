﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-4042         | ajohari2   | Underwriters - EFT Payments Part 2
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.Common;
using System.Data;
using System.Data.Sql;
using System.Xml.Linq;
using System.Xml;
using System.Text;
namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class CheckBatchPrintSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int iAccId = 0;
            string sId = string.Empty;
            string[] ArrId = null;
            bool bInSuccess = false;
            if (!Page.IsPostBack)
            {
                sId = AppHelper.GetQueryStringValue("ID");
                if (sId != "")
                {
                    ArrId = sId.Split('-');
                }
                if (ArrId != null)
                {
                    iAccId = Conversion.CastToType<int>(ArrId[0], out bInSuccess);
                    LoadDetails(iAccId);
                    ddlcheckstock.SelectedIndex = Conversion.CastToType<int>(ArrId[1], out bInSuccess);
                    //orghierarchypre.Text = ArrId[2];
                    //((TextBox)orghierarchypre.FindControl("orghierarchypre_cid")).Text = ArrId[2];
                    ////JIRA:4042 START: ajohari2
                    //if (ArrId.Length == 9)
                    //    ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode")).Text = ArrId[7] + "-" + ArrId[8];
                    //else
                    //    ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode")).Text = ArrId[7];
					// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    if (ArrId.Length == 10)
                    {
                        ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode")).Text = ArrId[7] + "-" + ArrId[8];
                        //IsEFTPayment.Checked = Conversion.CastToType<bool>(ArrId[9], out bInSuccess);
                        //hdnIsEFTPayment.Value = Conversion.CastToType<bool>(ArrId[9], out bInSuccess).ToString();
                        ddlDistributionType.SelectedValue = ArrId[9].ToString();
                    }
                    else
                    {
                        ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode")).Text = ArrId[7];
                        //IsEFTPayment.Checked = Conversion.CastToType<bool>(ArrId[8], out bInSuccess);
                        //hdnIsEFTPayment.Value = Conversion.CastToType<bool>(ArrId[8], out bInSuccess).ToString();
                        ddlDistributionType.SelectedValue = ArrId[8].ToString();
                    }
					// npadhy - JIRA Ends Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    //JIRA:4042 End: 

                    ((TextBox)OrgHierarchyCode.FindControl("OrgHierarchyCode_cid")).Text = ArrId[2];
                    orderfieldpre.SelectedValue = ArrId[3];
                    includecombinedpayments.Checked = Conversion.CastToType<bool>(ArrId[4], out bInSuccess);
                    includeautopayments.Checked = Conversion.CastToType<bool>(ArrId[5], out bInSuccess);
                    if (ArrId[6] == "summary")
                    {
                        detail.Checked = false;
                        summary.Checked = true;
                    }
                    else if (ArrId[6] == "subaccount")
                    {
                        detail.Checked = false;
                        subaccount.Checked = true;
                    }
                }
                else
                {
                    LoadDetails(0);
                }
            }
        }
        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            StringBuilder sbReturn = new StringBuilder(ddlAcc.SelectedItem.ToString());
            sbReturn.Append(";");
            sbReturn.Append(ddlAcc.SelectedValue.ToString());
            sbReturn.Append(";");

            //vsharma205=-Jira-RMA-RMA-19516 Starts
            //sbReturn.Append(ddlcheckstock.SelectedItem.ToString());
            //sbReturn.Append(";");
            //sbReturn.Append(ddlcheckstock.SelectedValue.ToString());
            //sbReturn.Append(";");
            if (ddlcheckstock.SelectedItem == null)
            {
                sbReturn.Append("");
                sbReturn.Append(";");
                sbReturn.Append("");
                sbReturn.Append(";");
            }
            else
            {
                sbReturn.Append(ddlcheckstock.SelectedItem.ToString());
                sbReturn.Append(";");
                sbReturn.Append(ddlcheckstock.SelectedValue.ToString());
                sbReturn.Append(";");
            }
            //vsharma205=-Jira-RMA-RMA-19516 Ends

            //sbReturn.Append(orghierarchypre_lst.Text);
            sbReturn.Append(OrgHierarchyCode_cid.Text);
            sbReturn.Append(";");
            sbReturn.Append(orderfieldpre.SelectedValue);
            sbReturn.Append(";");
            sbReturn.Append(includecombinedpayments.Checked);
            sbReturn.Append(";");
            sbReturn.Append(includeautopayments.Checked);
            sbReturn.Append(";");
            if(detail.Checked)
            sbReturn.Append("detail");
            else if(summary.Checked)
                sbReturn.Append("summary");
            else if(subaccount.Checked)
                sbReturn.Append("subaccount");
            sbReturn.Append(";");
            sbReturn.Append(OrgHierarchyCode.Text);
            sbReturn.Append(";");
            sbReturn.Append(AppHelper.GetQueryStringValue("ID"));
			// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //JIRA:4042 START: ajohari2
            //sbReturn.Append(";");
            //sbReturn.Append(IsEFTPayment.Checked);
            //JIRA:4042 End: 
            sbReturn.Append(";");
            sbReturn.Append(ddlDistributionType.SelectedValue);
            sbReturn.Append(";");
            sbReturn.Append(ddlDistributionType.SelectedItem.Text);
			// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            ClientScript.RegisterStartupScript(typeof(Page), "", "<script type='text/javascript'>if(window.opener.document.getElementById('hdnPrintSettings')!=null) window.opener.document.getElementById('hdnPrintSettings').value ='" + sbReturn.ToString() + "'; window.opener.document.forms[0].submit();window.close();</script>");
        }
        private void LoadDetails(int iAccId)
        {
            string oMessageElement = string.Empty;
            XmlDocument oTemplate = new XmlDocument();
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;
            DataSet resultDataSet = null;
			// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //JIRA:4042 START: ajohari2
            //IsEFTPayment.Enabled = false;
            //IsEFTPayment.Checked = false;
            //JIRA:4042 End: 
            string sPrevDistributionType = string.Empty;
            oMessageElement = GetMessageTemplate().ToString();
            oTemplate.LoadXml(oMessageElement);
            if (iAccId > 0) oTemplate.SelectSingleNode("//AccountId").InnerText = iAccId.ToString();
            sReturn = AppHelper.CallCWSService(oTemplate.InnerXml);
            if (sReturn != string.Empty)
            {
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);

                resultDataSet = ConvertXmlDocToDataSet(resultDoc);
                ddlAcc.DataSource = resultDataSet.Tables["AccountList"];
                ddlAcc.DataTextField = "ACCOUNT_NAME";
                ddlAcc.DataValueField = "ACCOUNT_ID";
                if (iAccId > 0)
                    ddlAcc.SelectedValue = iAccId.ToString();
                ddlAcc.DataBind();

                //JIRA:4042 START: ajohari2

                ddlcheckstock.Items.Clear();//Vsharma205-Jira-RMA-19516-On editing bank account Print Check Batch at task manager for EFT bank account getting a pop up message for check stock////////////////////
                ddlcheckstock.DataSource = resultDataSet.Tables["StockList"];
                ddlcheckstock.DataTextField = "STOCK_NAME";
                ddlcheckstock.DataValueField = "STOCK_ID";
                ddlcheckstock.DataBind();
                if (ddlcheckstock.Items.Count > 0)
                    ddlcheckstock.Items[0].Selected = true;

                if (ddlDistributionType.Items.Count > 0)
                    sPrevDistributionType = ddlDistributionType.SelectedValue;

                ddlDistributionType.DataSource = resultDataSet.Tables["DistributionType"];
                ddlDistributionType.DataTextField = "DistributionTypeDesc";
                ddlDistributionType.DataValueField = "DistributionTypeId";
                EFTDistributionType.Text = resultDoc.SelectSingleNode("//EFTDistributionType").InnerText;

                if (iAccId > 0)
                {
                    DataRow[] resultEFT = resultDataSet.Tables["AccountList"].Select("ACCOUNT_ID = '" + iAccId.ToString() + "'");
                    foreach (DataRow row in resultEFT)
                    {
                        if (row["IS_EFT_ACCOUNT"] != null && Convert.ToBoolean(row["IS_EFT_ACCOUNT"].ToString().Trim()))
                        {
                            if (ddlDistributionType.Items.Count > 0)
                                ddlDistributionType.SelectedValue = EFTDistributionType.Text;
                            EFTAccount.Text = "true";
                        }
                        else
                        {
                            if (sPrevDistributionType != string.Empty && sPrevDistributionType != EFTDistributionType.Text)
                                ddlDistributionType.SelectedValue = sPrevDistributionType;
                            EFTAccount.Text = "false";
                        }
                    }
                }
                else
                {
                    DataRow[] resultEFT = resultDataSet.Tables["AccountList"].Select("ACCOUNT_ID = '" + ddlAcc.SelectedValue + "'");
                    foreach (DataRow row in resultEFT)
                    {
                        if (row["IS_EFT_ACCOUNT"] != null && Convert.ToBoolean(row["IS_EFT_ACCOUNT"].ToString().Trim()))
                        {
                            if (ddlDistributionType.Items.Count > 0)
                                ddlDistributionType.SelectedValue = EFTDistributionType.Text;
                            EFTAccount.Text = "true";
                        }
                        else
                        {
                            if (sPrevDistributionType != string.Empty && sPrevDistributionType != EFTDistributionType.Text)
                                ddlDistributionType.SelectedValue = sPrevDistributionType;
                            EFTAccount.Text = "false";
                        }
                    }
                }
                ddlDistributionType.DataBind();

				// When we change the Account Number which is Not EFT, then the postback should persist the previous Distribution Type
                if (sPrevDistributionType == string.Empty || sPrevDistributionType == EFTDistributionType.Text)
                {
                    if (ddlDistributionType.Items.Count > 0)
                        ddlDistributionType.Items[0].Selected = true;
                }
				// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            }
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>TaskManagementAdaptor.GetBankAccList</Function> 
                    </Call>
                    <Document>
                        <BankAccountList>
                            <AccountId></AccountId>
                            <Account></Account>
                            <Stock></Stock>
                            <DistributionTypes></DistributionTypes>
                            <EFTDistributionType></EFTDistributionType>
                        </BankAccountList>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }

        private DataSet ConvertXmlDocToDataSet(XmlDocument resultDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(resultDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void ddlAcc_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bInSuccess = false;
            int iAccId = Conversion.CastToType<int>(((DropDownList)sender).SelectedValue, out bInSuccess);
            LoadDetails(iAccId);
        }
    }
}