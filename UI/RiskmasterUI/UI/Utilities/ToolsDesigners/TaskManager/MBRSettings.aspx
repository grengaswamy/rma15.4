<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MBRSettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.MBRSettings" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %> <%--vchaturvedi2 MITS 20971 applying error control--%>
<%--Neha RMACLOUD-9816 - MBR Compatible with Cloud--%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MBR Settings</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
   
       <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
<%--    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>--%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>

<%--Neha RMACLOUD-9816 - MBR Compatible with Cloud--%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
       <script src="../../../../Scripts/TMSettings.js" type="text/javascript"></script>
        <%--vkumar258 - RMA-6037 - Starts --%>
    <%-- <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>--%>

    <%--<script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>--%>
<link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/TMSettings.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            width: 152px;
        }
        .style2
        {
            width: 152px;
        }
        .style3
        {
            width: 224px;
        }
        .style4
        {
            width: 106px;
        }
        .style5
        {
            width: 128px;
        }
        .style6
        {
            width: 59px;
        }
        .style9
        {
            width: 358px;
        }
        .style11
        {   
            font-weight: bold;
            text-decoration: underline;
        }
         .style21
        {
            width: 265px;
        }
        .style35
        {
            width: 28px;
        }        
        .style36
        {
            width: 356px;
        }
        .style37
        {
            width: 280px;
        }
    </style>
   
    <script language="javascript" type="text/javascript">        
    function setFile()
    {
        if (document.getElementById('FUSourceFile').value != '' )
        {   
            
            document.getElementById('txtFileUpload').value =
            document.getElementById('FUSourceFile').value;
        }
        //Vsoni5 : MITS 25624 : Import Document Location Removed.
//        if (document.getElementById('FUAttachment').value !='')
//        {
//            document.getElementById('txtImportFile').value =
//            document.getElementById('FUAttachment').value;
//        }
        // vchaturvedi2- MITS -20851 export file path not required ..
//        if (document.getElementById('FUExportPath').value !='')
//        {
//            document.getElementById('txtExportPath').value= 
//            document.getElementById('FUExportPath').value;
//        }

    }

    //Vsoni5 : 06/17/2011 : 
    // Function added to Validate file type for MBR and Export Date on Claim Export criteria set.
    // Renaming this function from validateMBRImportFileType to Validate
    function Validate() {
        //Vsoni5 : Added condition to prevent Import criteria validation in case of export
        if (document.getElementById('hTabName').value == "importsetting") 
        {
            var sInputFileType = "";
            var sSelectedFileType = "";
            var sfileName = "";
            var oFileType = document.getElementsByName("rbFileType")
            // Get selected Input file type.
            for (var index = 0; index < oFileType.length; index++) {
                if (oFileType[index].type == 'radio' && oFileType[index].checked) {
                    sInputFileType = oFileType[index].value;
                    if (sInputFileType != null) {
                        sInputFileType = sInputFileType.toUpperCase();
                    }
                }
            }
            // Warn user if Import File Type is not selected 
            if (sInputFileType == null || sInputFileType == "")
            {
                    alert("Select file setting - Access/Pipe!");
                    return false;
            }

            //Check if no file selected
            sfileName = document.forms[0].txtFileUpload.value;

            if (sfileName == null || sfileName == "") {
                alert("Please select import file!");
                return false;
            }

            // Get the file extension    
            sSelectedFileType = sfileName.substring((sfileName.lastIndexOf('.') + 1), sfileName.length)
            sSelectedFileType = sSelectedFileType.toUpperCase();

            //Validate extension of input file  against selected input file type Radio Button (Access/Pipe)

            if (sInputFileType == "ACCESS") {
                if (sSelectedFileType == 'MDB' || sSelectedFileType == 'ACCDB')
                    return true;
                else {
                    alert("Only Microsoft Access Database files are allowed!");
                    return false;
                }
            }
            else if (sInputFileType == "PIPE") {
                if (sSelectedFileType == 'TXT')
                    return true;
                else {
                    alert("Only text files are allowed!")
                    return false;
                }
            }
            return false;
        }
        //Vsoni5 : MITS 24981 : Added this block to validate that the Export From date is not left blank.
        else if (document.getElementById('hTabName').value == "exportsetting") 
        {
            var oTxtFromDate = document.getElementById("tbExpOverrideDate")
            var sFromDate = "";
            var oExportType = document.getElementsByName("rdbExportType")
            var sExportType = "";
            // Get selected Input file type.
            for (var index = 0; index < oExportType.length; index++) {
                if (oExportType[index].type == 'radio' && oExportType[index].checked) {
                    sExportType = oExportType[index].value;
                    if (sExportType != null) {
                        sExportType = sExportType.toUpperCase();
                    }
                }
            }
            if (sExportType == "ENTIY") {
                if (oTxtFromDate != null) {
                    sFromDate = oTxtFromDate.value;
                }

                if (sFromDate == null || sFromDate == "") {
                    alert("Export From Date cannot be blank");
                    oTxtFromDate.focus();
                    return false;
                }
            }
        }
        return true;
    }

    //Vsoni5 - MITS 25236 - DA MBR - RMX UI : If "Use Suffix code" to identify Entities
    // checkbox is cheked, User should not be able to check "Use First Entity Listed in RM" checkbox
    function setPayeeEntitySearch() {
        var useSuffixCode;
        var chkbxSuffix = document.getElementById("chkbxSuffix");
        var chkbxFirstEntity = document.getElementById("chkbxFirstEntity");
        useSuffixCode = chkbxSuffix.checked;
        if(useSuffixCode && chkbxFirstEntity != null)
        {    
            chkbxFirstEntity.checked = false;
            chkbxFirstEntity.disabled = true;
        }
        else if(!useSuffixCode && chkbxFirstEntity != null)
        {
            chkbxFirstEntity.disabled = false;
        }
    }
//Vsoni5 : MITS 25250 : Added this function to show/hide criteria set according to Export type - Claim/entity
    function toggleExportType(sExportType) {
        var pnlClaimExport = null;
        var pnlEntityExport = null;

        pnlClaimExport = document.getElementById('pnlclaimExport');
        pnlEntityExport = document.getElementById('pnlEntityExport');
        if (sExportType == 'claim') {
            pnlClaimExport.style.display = 'block';
            pnlEntityExport.style.display = 'none';
        }
        else if (sExportType == 'entity') {
            pnlClaimExport.style.display = 'none';
            pnlEntityExport.style.display = 'block';
        }
    }

//Vsoni5 : MITS 25250 : Added this function to enable/disable Update Entity and Udate Codes checkboxes
 function setEntitySuffix(updateSuffix)
    {
        //var chkbxUpdateCodes = document.getElementById("chkbxUpdateCodes");
        var chkbxUpdateEntity = document.getElementById("chkbxUpdateEntity");
        if (updateSuffix == false || updateSuffix == 'False') {
            //chkbxUpdateCodes.disabled = true;
            //chkbxUpdateCodes.checked = false;
            chkbxUpdateEntity.disabled = true;
            chkbxUpdateEntity.checked = false;
        }
        else {
            //chkbxUpdateCodes.disabled = false;
            chkbxUpdateEntity.disabled = false;
        }
    }


    //Vsoni5 : MITS 25216 : Added this function to enable/disable Fee Payment Options
    function toggleFeePayment(enableFeePayment) {
        var feePaymentPanel = document.getElementById("trFeePaymentSetUp");
        if (enableFeePayment == true || enableFeePayment == 'True') {
            feePaymentPanel.disabled = false;
        }
        else {
            feePaymentPanel.disabled = true;
        }
    }

    </script>
</head>
<body class="10pt" >
    <form id="frmData" name="frmData" method="post" onMouseEnter="setFile()" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
               
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox1" />
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:Label ID="Label1" runat="server" Text="MBR Settings" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
                    
              <uc1:ErrorControl ID="ErrorControl1" runat="server" /> <%--vchaturvedi2 MITS 20971 applying error control--%>
           
        <div id="Div1" class="errtextheader" runat="server">
            <%-- <asp:Label ID="formdemotitle" runat="server" Text="" />-vchaturvedi2-MITS 20971--%>
        </div>
    </div>
    <br />
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="NotSelected" nowrap="true" name="TABSimportsetting" id="TABSimportsetting">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name); return false;" name="importsetting"
                                id="LINKTABSimportsetting"><span style="text-decoration: none">Import 
                            Setting</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSexportsetting" id="TABSexportsetting">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name); return false;" name="exportsetting"
                                id="LINKTABSexportsetting"><span style="text-decoration: none">Export 
                            Setting</span></a>
                        </td>
                                                
                        <td valign="top" nowrap="true">
                        </td>
                    </tr>
                </table>
                <div  class="singletopborder" 
                    style="position: relative; left: -6; top: -118; width: 100%;" >
                 <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABimportsetting"
                                    id="FORMTABimportsetting">
                                    <tr id="">
                                      <td class="style9">
                                          <span class="style11">OptionSet Name</span>
                                          <asp:TextBox ID="txtOptionSetName" runat="server"></asp:TextBox>
                                            </td>  
                                     <td>    
                                         <%--<asp:Label ID="lblOptionSet" runat="server" ForeColor="Red"></asp:Label>-vchaturvedi2 - MITS 20971--%> 
                                          
                                      </td>  
                                        
                                    </tr>
                                    <tr>
                                    <td class="style9">&nbsp;</td><td></td>
                                    </tr>
                                    <tr id="">
                                      <td class="style11">
                                          File Setting</td>  
                                        
                                      <td>
                                          &nbsp;</td>  
                                        
                                    </tr>
                                    
                                    <tr id="">
                                      <td class="style9">                                          
                                             <asp:RadioButtonList ID="rbFileType"  runat="server" 
                                              RepeatDirection="Horizontal"   
                                                 >  <%--vchaturvedi2 - MITS 20923 saving file from UI to server--%>
                                                      <asp:ListItem  Value="access">Access</asp:ListItem>
                                                      <asp:ListItem  Value="pipe">Pipe</asp:ListItem>
                                                        </asp:RadioButtonList>     
                                        </tr>
                                        <tr>
                                         <td>                                                                                           
                                                  <%--vchaturvedi2 - MITS 20923 saving file from UI to server--%>       
                                      <asp:TextBox ID="txtFileUpload" runat="server" onkeydown="return false;" 
                                                 onpaste="return false;" Width="374px"></asp:TextBox>
                                                    
                                                  
                                                  <%--vchaturvedi2 - MITS 20923 saving file from UI to server--%><%--vchaturvedi2 - MITS 20923 saving file from UI to server--%><%--vchaturvedi2 - MITS 20923 saving file from UI to server--%>

                                      
                                      
                                          <asp:FileUpload ID="FUSourceFile" runat="server" class="button" 
                                          Width="16px" style="vertical-align:bottom"/>
                                        </td>  
                                        
                                    </tr>
                                    
                                    <tr id="trSourcePath" runat="server">
                                      <td class="style9" >
                                          &nbsp;</td>  
                                        
                                      <td >
                                          </td>  
                                        
                                    </tr>
                                    <tr id="Tr1">
                                      <td class="style9">
                                          <b>Data Import Setting</b></td>  
                                        
                                      <td>
                                          
                                        </td>  
                                        
                                    </tr>
                                    
                                    <tr id="Tr4">
                                      <td class="style9">
                                          <asp:CheckBox ID="chkbxAllowClosedClaims" runat="server" 
                                              Text="Allow Payments on Closed Claims" />
                                      </td>  
                                        
                                      <td>
                                          <asp:CheckBox ID="chkbxInsuffReserve" runat="server" 
                                              Text="Notify\ Reject if Insufficient Reserves" />
                                      </td>  
                                        
                                    </tr>
                                    
                                    <tr id="Tr5">
                                      <td class="style9">
                                          <asp:CheckBox ID="chkBxEnclFlag" runat="server" 
                                              Text="Check Enclosure Flag for MBR Fee Payments" />
                                      </td>  
                                        
                                      <td>
                                          <asp:CheckBox ID="chkbxSuffix" runat="server" 
                                              Text="Use Suffix code to identify Entities" onClick="setPayeeEntitySearch();"/>
                                      </td>  
                                        
                                    </tr>
                                    
                                    <tr id="Tr6">
                                      <td class="style9">
                                          <asp:CheckBox ID="chkbxFrozenClaims" runat="server" 
                                              Text="No Payments on Frozen Claims" />
                                      </td>  
                                        
                                      <td>
                                          <asp:CheckBox ID="chkbxPrintedChecks" runat="server" 
                                              Text="Import Printed Checks" />
                                      </td>  
                                        
                                    </tr>
                                    
                                    <tr id="Tr7">
                                      <td class="style9">
                                          <asp:CheckBox ID="chkbxImportReason" runat="server" Text="Import Reason" />
                                          (EOB Code)</td>  
                                        
                                      <td>
                                          <asp:CheckBox ID="chkbxProvider" runat="server" 
                                              Text="Import Provider's Invoice Number" />
                                      </td>  
                                        
                                    </tr>
                                     <tr id="Tr8">
                                      <td class="style9">
                                          <asp:CheckBox ID="chkbxImportMedData" runat="server" Text="Import Medical Data" />
                                          </td>  
                                        
                                      <td>
                                          <asp:CheckBox ID="chkbxQueuedPayment" runat="server" Text="Queued Payment" /></td>  
                                        
                                    </tr>
                                    <tr id="Tr9">
                                      <td class="style9">
                                          &nbsp;</td>  
                                        
                                      <td>
                                          &nbsp;</td>  
                                        
                                    </tr>
                                    
                                    <tr id="">

                                        
                                      <td>
                                          <b>Payee Search Criteria
                                        </b></td>  
                                        <td>
                                          <b>Payment Duplicate Search</b></td>
                                    </tr>
                                    
                                    <tr id="">

                                      <td>
                                          <asp:CheckBox ID="chkbxFirstEntity" runat="server" 
                                              Text="Use First Entity Listed in RM" />
                                      </td>  
                                        <td>
                                          <asp:CheckBox ID="chkbxInvoice" runat="server" Text="Exclude Invoice Number" />
                                        </td>
                                    </tr>
                                    
                                    <tr id="">

                                        
                                      <td>
                                          <asp:CheckBox ID="chkbxSSN" runat="server" Text="Search by SSN only" />
                                      </td>  
                                        <td>
                                          <asp:CheckBox ID="chkbxVoid" runat="server" Text="Include Voids in Search" />
                                        </td>
                                    </tr>
                                    
                                    <tr id="">
                                      <!--<td class="style9">
                                          &nbsp;</td>  
                                        -->
                                      <td>
                                          <asp:CheckBox ID="chkbxZIP" runat="server" 
                                              Text="Use Zip Code For Payee Lookup" />
                                      </td>  
                                        <td>
                                          <asp:CheckBox ID="chkbxTransDate" runat="server" 
                                              Text="Use Service Dates not Trans Date" />
                                        </td>
                                    </tr>
                                    <tr id="Tr3">
                                      <td class="style9">
                                          <asp:CheckBox ID="chkbxNewPayee" runat="server" Text="Do not add new Payees " />
                                        </td>  
                                        
                                      <td>
                                          &nbsp;</td>  
                                        
                                    </tr>
                                                                        <tr>
                                    <td class="style9"><asp:CheckBox ID="chkbxImportPayee" runat="server" 
                                              Text="Use Import Payee Info For Payment" /></td><td>
                                                              <!--<asp:DropDownList ID="ddlJurisMedical" runat="server" AutoPostBack="True" 
                                                                  Width="110px">
                                                              </asp:DropDownList>-->
                                                                            </td>
                                    </tr>
                                                                        <tr>
                                    <td class="style9">&nbsp;</td><td>
                                                                            &nbsp;</td>
                                    </tr>
                                     <tr id="Tr10">
                                      <td class="style9">
                                          <asp:CheckBox ID="chkbxUseFeePayment" runat="server" 
                                              onclick="toggleFeePayment(this.checked);" Text="Use Fee Payment" />
                                         </td>  
                                        
                                      <td>
                                          &nbsp;</td>  
                                        
                                    </tr>
                                    
                                     
                                    
                                    <tr id="trFeePaymentSetUp" runat="server">
                                      <td class="style9" colspan="2">
                                          <asp:UpdatePanel ID="updPnlFeePayment" runat="server">
                                              <ContentTemplate>
                                                  <table id = "tblFeePaymentStates">
<%--                                                    <tr id="Tr2">
                                                          <td class="style9" colspan="2">
                                                              
                                                          </td>  
                                                          <td>&nbsp;</td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td style="padding-top:10px">
                                                            <b>
                                                           <%-- <br />--%>
                                                            Jurisdictions</b>
                                                            <asp:ListBox ID="lbStatesAll" runat="server" EnableTheming="False" Height="240px" 
                                                                SelectionMode="Multiple" Width="102px" style="margin-top:5px"></asp:ListBox>
                                                        </td>
                                                        <td class="style35">
                                                          <table style="margin-left:2px; display:inline;width:100%">
                                                          <tr><td class="style6">&nbsp;</td></tr>
                                                              <tr><td class="style6"></td></tr>
                                                          <tr><td class="style6">
                                                              <asp:Button ID="btnAddMedResState" runat="server" Text="&gt;" 
                                                                  Font-Bold="False" Width="35px" onclick="btnAddMedResState_Click" /></td></tr>
                                                          <tr><td class="style6">
                                                              <asp:Button ID="btnDelMedResState" runat="server" Text="&lt;" 
                                                                  Font-Bold="False" Width="35px" onclick="btnDelMedResState_Click" /></td></tr>
                                                          <tr><td class="style6">&nbsp;</td></tr>
                                                          <tr><td class="style6">&nbsp;</td></tr>
                                                          <tr><td class="style6">&nbsp;</td></tr>
                                                          <tr><td class="style6">&nbsp;</td></tr>
                                                          <tr><td class="style6">
                                                              <asp:Button ID="btnAddOtherResState" runat="server" Text="&gt;" Font-Bold="False" 
                                                                  Width="35px" onclick="btnAddOtherResState_Click" />
                                                              </td></tr>
                                                          <tr><td class="style6">
                                                              <asp:Button ID="btnDelOtherResState" runat="server" Text="&lt;" Font-Bold="False" 
                                                                  Width="35px" onclick="btnDelOtherResState_Click" />
                                                              </td></tr>
                                                          
                                                          </table>
                                                          </td> 
                                                        <td class="style21" style="width:600px;display:inline" >
                                                            <table>
                                                            <tr>
                                                            <td style="display:inline;" class="style36">
                                                                <b>Jurisdiction Medical Reserve</b></td>
                                                            </tr>
                                                            <tr>
                                                            <td class="style36">
                                                            <asp:ListBox ID="lbStateMedical" runat="server" 
                                                                Height="102px" SelectionMode="Multiple" style="margin-top:2px" Width="110px">
                                                            </asp:ListBox>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="padding-top:7px" class="style36">
                                                                <strong>Jurisdiction Other Reserve</strong><br />
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td class="style36">
                                                            <asp:ListBox ID="lbStateOther" runat="server" Height="102px" Width="110px" 
                                                                    SelectionMode="Multiple">
                                                            </asp:ListBox>
                                                            </td>
                                                            </tr>
                                                            </table>
                                                        </td>                                                         
                                                    </tr>
                                                  </table>
                                          <!--<asp:Panel ID = "pnlFeePaymentTTC" runat="server" Width="609px">-->
                                          <table width="609px">
                                              <tr>
                                                  <td class="style4">
                                                      <b>Fee Payments</b></td>
                                                        <td class="style11">
                                                           Transaction Type</td>
                                                        <td class="style11">
                                                            Payment Date</td>
                                                        <td class="style11">
                                                            <b>Identifier</b></td>
                                                        <td class="style1">
                                                            &nbsp;&nbsp;&nbsp; Printed Status</td>
                                                        <td>
                                                            <b></b></td>
                                                    </tr>
                                                    <tr id="trMedicalReserve" runat="server">
                                                        <td class="style4">
                                                            Medical Reserve</td>
                                                        <td class="style5">
                                                            <asp:DropDownList ID="ddlTTMedical" runat="server" Width="100%" 
                                                                style="margin-top: 0px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPaymentMedical" runat="server">
                                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="1">1st Day Next Month</asp:ListItem>
                                                                <asp:ListItem Value="2">15th Day Next Month</asp:ListItem>
                                                                <asp:ListItem Value="3">Last Day Current Mo</asp:ListItem>
                                                                <asp:ListItem Value="4">Use Date Bill Paid</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="style6" align="center">
                                                            <asp:TextBox ID="tbIDMedical" runat="server" Width="22px" MaxLength="1"></asp:TextBox>
                                                        </td>
                                                        <td class="style2">
                                                            &nbsp;&nbsp;
                                                            <asp:CheckBox ID="chkbxPrintedMedical" runat="server" />
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr id="trOtherReserve" runat="server">
                                                        <td class="style4">
                                                            Other Reserve</td>
                                                        <td class="style5">
                                                            <asp:DropDownList ID="ddlTTOther" runat="server" Width="100%">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPaymentOther" runat="server">
                                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="1">1st Day Next Month</asp:ListItem>
                                                                <asp:ListItem Value="2">15th Day Next Month</asp:ListItem>
                                                                <asp:ListItem Value="3">Last Day Current Mo</asp:ListItem>
                                                                <asp:ListItem Value="4">Use Date Bill Paid</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="style6" align="center">
                                                            <asp:TextBox ID="tbIDOther" runat="server" Width="22px" MaxLength="1"></asp:TextBox>
                                                        </td>
                                                        <td class="style2">
                                                            &nbsp;&nbsp;
                                                            <asp:CheckBox ID="chkbxPrintedOther" runat="server" />
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                       <!--   </asp:Panel>-->
                                         </ContentTemplate>
                                        </asp:UpdatePanel>
                                      </td>                                          
                                    </tr>                                                                       
                                                                                                         
                                    <tr id="">
                                      <td class="style9">&nbsp;</td>  
                                        
                                      <td>
                                            &nbsp;</td>  
                                        <td>
                                            &nbsp;</td>  
                                        
                                    </tr>
                                    
<%-- Neha RMACLOUD-9816 - MBR Compatible with Cloud starts--%>
                                                <%-- Provide the functionality to upload the attachments from UI. This would only be available only in case of Cloud--%>
                                                <div id="divAttachments" runat="server">
                                                   <tr>
                                                       <td>
                                                        <br />
                                                        <asp:Label ID="Label7" runat="server" Text="Upload Files as Attachments" 
                                                        Font-Size="12" Font-Bold="True"></asp:Label>
                                                        <br />
                                                        <br />                                             
                                                      </td>
                                                 </tr>
                                                    <tr>
                                                        <td>
                                                                <asp:Label ID="lbSuccess" runat="server" Visible="false" Text="The  Files have been successfully uploaded" Style="font-weight: bold;
                                                                            color: blue"></asp:Label>
       
                                                        </td>
                                                        </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="udpAttach" runat="server" UpdateMode="Conditional" >
                                                                <ContentTemplate>
                                                                    <CuteWebUI:UploadAttachments  ID="UploadDocumentAttachments" runat="server" InsertText="Click here to upload Files" 
                                                                            MultipleFilesUpload="True">
                                                                    </CuteWebUI:UploadAttachments>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <input type="hidden" value="" id="filename" runat="server">
                                                    <uc2:PleaseWaitDialog ID="pleaseWait" runat="server"  CustomMessage="Uploading"/>
                                                </div>

                                             <%-- Neha RMACLOUD-9816 - MBR Compatible with Cloud  ends--%>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABexportsetting" id="FORMTABexportsetting"
                                   style="">

                                    <tr id="">
                                        <td>
                                            <span class="style11">OptionSet Name</span>
                                          <asp:TextBox ID="txtOptionSetNameExport" runat="server"></asp:TextBox>
                                        </td>
                                        
                                        <td class="style3">
                                            <%-- <br />--%>
                                            </td>
                                        
                                    </tr>
                                    <tr >
                                        <td>
                                            &nbsp;</td><td class="style3">
                                            &nbsp;</td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <b><u>Export Type</u></b>
                                        </td>
                                        
                                        <td class="style3">
                                            &nbsp;</td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                        <asp:RadioButtonList ID="rdbExportType" runat="server" 
                                                RepeatDirection = "Horizontal" >
                                            <asp:ListItem Text = "Claim" Value="claim" onclick="toggleExportType('claim');"></asp:ListItem>
                                            <asp:ListItem Text = "Entity" Value="entity" onclick="toggleExportType('entity');"></asp:ListItem>
                                        </asp:RadioButtonList>
                                         </td>
                                    </tr>
                                    <tr id="Tr12">
                                        <td>
                                            &nbsp;</td><td class="style3">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                    <!-- Claim Export Setting -->
                                    <td>
                                    <asp:Panel ID="pnlClaimExport" runat="server">
                                    <table>

                                    
                                    <tr id="">
                                        <td>
                                        <%--- vchaturvedi2 MITS : 20850 Start Date Field --%>
                                            Claim Export Last Run Date :&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID = "lblExportDate" runat ="server" width="125px" Wrap="False"></asp:Label>
                                            </td>
                                            </tr>
                                        <%--<tr id=""> - vchaturvedi2 MITS : 20851 Export File Path Not Required 
                                        <td>
                                            <b>Export File Path</b></td><td class="style3">
                                            &nbsp;</td></tr><tr id="">
                                        <td colspan="2" class="style12">
                                            <asp:TextBox ID="txtExportPath" runat="server" Width="213px"></asp:TextBox><asp:FileUpload ID="FUExportPath" runat="server" CssClass="button" 
                                                Width="16px" />
                                            
                                        </td>
                                        
                                    </tr>--%>

                                    <tr id="">
                                        <td style="vertical-align:middle"> <%--vchaturvedi2 - MITS 20971 validating override date field--%>
                                            Export From Date:  
                                            <asp:TextBox ID="tbExpOverrideDate" runat="server" FormatAs="date" RMXRef="/Instance/Document/Details/Date" RMXType="date" 
                                                         onblur="dateLostFocus(this.id);" Width="125px" style="margin-top : 5px"></asp:TextBox>
                                               <%--vkumar258 - RMA_6037- Starts--%>
                                            <%-- <asp:Button ID="btnCalendar" runat="server"  class="DateLookupControl"  value=""/>
                                            <script type="text/javascript">
                                                Zapatec.Calendar.setup({ inputField: "tbExpOverrideDate", ifFormat: "%m/%d/%Y", button: "btnCalendar" }); 
                                            </script>--%>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $("#tbExpOverrideDate").datepicker({
                                                        showOn: "button",
                                                        buttonImage: "../../../../Images/calendar.gif",
                                                        showOtherMonths: true,
                                                        selectOtherMonths: true,
                                                        changeYear: true
                                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                                });
                                                        </script>
                                                           <%--vkumar258 - RMA_6037- End--%>
                                            <br />
                                        </td></tr>
                                        <tr id="Tr2">
                                        <td style="padding-top:7px">
                                            <asp:CheckBox ID="chkbxRemoveSSN" runat="server" Text="Remove SSN from File" />
                                        </td>
                                    </tr>
                                    <tr id="Tr13">
                                        <td>
                                           &nbsp;
                                        </td>
                                    </tr>
                                    <tr id="Tr14">
                                        <td>
                                          <b>Line Of Business</b>
                                        </td>
                                    </tr>
                                    <tr id="Tr11">
                                        <td>
                                            <asp:ListBox ID="lbLOBCode" runat="server" Height="100px" Rows="5" 
                                                Width="175px" SelectionMode="Multiple"></asp:ListBox>
                                        </td>
                                    </tr>

                                       </table>
                                       </asp:Panel>
                                       </td>
                                       <td>
                                       </td>
                                       </tr>    
                                       <tr>
                                        <td></td><td class="style3"></td>
                                       </tr>
                                        <tr>
                                        <td>
                                        <asp:Panel ID="pnlEntityExport" runat="server">
                                        <table>                                        
                                        <tr id="">
                                        <td class="style37">
                                            <b>Update Entity Suffix Data</b></td><td class="style3">
                                            &nbsp;
                                            </td>
                                            </tr>
                                            <tr>
                                        <td class="style37">
                                            <asp:CheckBox ID="chkbxIncludeSuffix" runat="server" 
                                                Text="Include Entity Suffix in File"/>
                                        </td>
                                        
                                        <td class="style3">
                                            &nbsp;</td></tr>
                                            <tr id="">
                                        <td class="style37">
                                            <asp:CheckBox ID="chkbxSourceForSuffix" runat="server" 
                                                Text="Use Source File for Suffix Updates" onclick="setEntitySuffix(this.checked);"/>
                                        </td>
                                        
                                        <td class="style3">
                                            &nbsp;</td></tr>

                                        <tr id="">
                                        <td class="style37">
                                            <asp:CheckBox ID="chkbxUpdateEntity" runat="server" 
                                                Text="Update Entity Suffix for All Entities" />
                                                </td>
                                                <td class="style3">
                                                    &nbsp;</td></tr>
                                        <tr>
                                        <td class="style37">&nbsp;</td><td class="style3">&nbsp;</td>
                                       </tr>
                                        <tr>
                                            <td class="style37"><b>Entity Type</b></td>
                                            <td class="style3">
                                                <b>People Type</b></td>
                                        </tr>         
                                         <tr>
                                            <td class="style37">
                                            <asp:ListBox ID="lbEntityCode" runat="server" SelectionMode="Multiple" 
                                                Height="120px" Width="250px">
                                            </asp:ListBox>
                                            </td>
                                            <td class="style3">
                                                <asp:ListBox ID="lbpeople" runat="server" SelectionMode="Multiple" 
                                                    Height="120px" style="margin-top: 0px" Width="250px">
                                                </asp:ListBox>
                                            </td>
                                        </tr>  
                                        <tr>
                                        <td class="style37">&nbsp;</td><td class ="style3">&nbsp;</td>
                                        </tr>
                                        </table>
                                            </asp:Panel>
                                           </td>
                                           </tr>                                                                                                                                                                                                                                              
                    </table>
                </div>
                
                
                    <input type="hidden" name="hTabName" id="hTabName"  runat="server"  value="importsetting" />
                
            </td>
        </tr>
        <tr>
			<td>
			
			    
			
                                            <asp:Button ID="btnSave" class="button" runat="server" Text="Save" 
                                                onclick="btnSave_Click" onclientclick= "return Validate();"/>
                                              
			                                <asp:Button ID="btnCancel" Text="Cancel" class="button" OnClientClick="OnCancel();return false;"
                                        runat="server"  />
			    
			
			                            </td>
			</tr>
			<tr>
			<td>
			
			    
			
                                           </td>
			</tr>
    </table>
    <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
    <asp:HiddenField ID="hdOptionsetId" runat="server" />
    
    </form>
    <script type="text/javascript">
        setPayeeEntitySearch();
        toggleExportType('<%= rdbExportType.SelectedValue %>');
        setEntitySuffix('<%= chkbxSourceForSuffix.Checked %>');
        toggleFeePayment('<%= chkbxUseFeePayment.Checked%>');
    </script>
</body>
</html>
