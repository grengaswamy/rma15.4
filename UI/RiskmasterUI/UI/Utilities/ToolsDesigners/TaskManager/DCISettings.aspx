<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DCISettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.DCISettings" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>DCI Option Setting Page</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style3
        {
            text-decoration: underline;
            width: 250px;
        }
        .style5
        {
            width: 337px;
        }
        .style9
        {
            width: 250px;
        }
        
        .style6
        {
            width: 59px;
        }

        .styleRemove
        {
            FONT-WEIGHT: bold;
            FONT-SIZE: 8pt;
            COLOR: #FFFFFF;
            FONT-FAMILY: Arial, Helvetica, sans-serif;
 	        background-repeat: no-repeat;
	        background-position: center;
        }
    </style>
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">{var i;}  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}  </script>
    <script src="../../../../Scripts/TMSettings.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid.js">{ var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{ var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{ var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{ var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <script src="../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/DCI-CLUE.js"></script>
</head>
<body>
    <form id="frmData" name="frmData" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2">
              <uc1:ErrorControl ID="ErrorControl1" runat="server" />
              
            </td>
            </tr>
    </table>
    <div>     
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox1" />
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:Label ID="Label1" runat="server" Text="DCI Optionset" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
    </div>
        <%--Ankit Gupta - MITS#36997 GAP 06 PMC - Start--%>

        <div class="singletopborder"
            style="position: relative; left: 0; top: 0; width:100%; height:auto;">
                        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABSettings"
                            id="FORMTABSettings" style="width:100%;">
                            <tr>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td width="13%">
                                                <asp:label id="lblOptionSetName" runat="server" forecolor="Black"
                                                    text="Optionset Name:" font-size="Small" font-bold="True"
                                                    font-underline="True"></asp:label>
                                            </td>
                                            <td width="87%">
                                                <asp:textbox id="txtOptionSetName" runat="server" width="33%" font-size="Small"
                                                    height="18px"></asp:textbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:label id="lblLastRunDate" runat="server" wrap="False" text="Last Run Date:"></asp:label>
                                            </td>
                                            <td>
                                                <asp:textbox id="txtLastRunDate" runat="server" font-size="Small" height="18px" readonly="True"
                                                    width="33%"></asp:textbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:label id="lblCarrierCode" runat="server" wrap="False" text="Carrier Code:"></asp:label>
                                            </td>
                                            <td>
                                                <asp:textbox id="txtCarrierCode" runat="server" font-size="Small" height="18px"
                                                    width="33%" maxlength="40"></asp:textbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:label id="lblClaimDateRange" runat="server" wrap="False" text="Claim Date Reported:"></asp:label>
                                            </td>
                                            <td>
                                                <asp:label id="lblFromDate" runat="server" text="From"></asp:label>
                                                &nbsp;
                                                <input type="text" runat="server" id="txtFromDate" size="10"
                                                    onblur="dateLostFocus(this.id);" />
                                                <input type="button" class="DateLookupControl" id="txtCompletedOnbtn1" value="" runat="server" />
                                                <script type="text/javascript">
                                                    Zapatec.Calendar.setup({ inputField: "txtFromDate", ifFormat: "%m/%d/%Y", button: "txtCompletedOnbtn1" });
                                                </script>
                                                <asp:label id="lblToDate" runat="server" text="To"></asp:label>
                                                &nbsp;
                                                <input type="text" runat="server" id="txtToDate" size="10"
                                                    onblur="dateLostFocus(this.id);" />
                                                <input type="button" class="DateLookupControl" id="txtCompletedOnbtn2" value="" runat="server" />
                                                <script type="text/javascript">
                                                    Zapatec.Calendar.setup({ inputField: "txtToDate", ifFormat: "%m/%d/%Y", button: "txtCompletedOnbtn2" });
                                                </script>
                                            </td>
                                        </tr>
                                        <tr>
                                           <td colspan="2">
                                    <asp:updatepanel id="Updatepanel1" runat="server">
                                              <ContentTemplate>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td width="13%">
                                                <asp:label id="lblJurisdictionState" runat="server" wrap="False" font-size="Small" font-underline="True" font-bold="True" text="Jurisdiction State:"></asp:label>
                                            </td>
                                            <td width="87%"> 
                                                <table width="100%" id="tblStates">
                                                    <tr>
                                                        <td width="10%">
                                                            <asp:ListBox ID="lstAllStates" runat="server" EnableTheming="False" Height="150px" 
                                                                SelectionMode="Multiple" Width="125px" style="margin-top:5px"></asp:ListBox>
                                                        </td>
                                                        <td width="5%">
                                                          <table style="margin-left:2px; display:inline;width:100%">
                                                            <tr><td class="style6">
                                                              <asp:Button ID="btnAddState" runat="server" Text="&gt;" 
                                                                  Font-Bold="False" Width="35px" onclick="AddSelectedOption" /></td></tr>
                                                            <tr><td class="style6"><asp:Button ID="btnAddAllState" runat="server" Text="&gt;&gt;" 
                                                                  Font-Bold="False" Width="35px" onclick="AddAllOption" /></td></tr>
                                                            <tr><td class="style6">
                                                              <asp:Button ID="btnRemoveState" runat="server" Text="&lt;" 
                                                                  Font-Bold="False" Width="35px" onclick="RemoveSelectedOption" /></td></tr>
                                                            <tr><td class="style6"><asp:Button ID="btnRemoveAllState" runat="server" Text="&lt;&lt;" 
                                                                  Font-Bold="False" Width="35px" onclick="RemoveAllOption" /></td></tr>
                                                          </table>
                                                          </td> 
                                                        <td width="85%" style="display:inline" >
                                                            <table>
                                                            <tr>
                                                            <td class="style36">
                                                            <asp:ListBox ID="lstSelectedStates" runat="server" 
                                                                Height="150px" SelectionMode="Multiple" style="margin-top:2px" Width="125px">
                                                            </asp:ListBox>
                                                            </td>
                                                            </tr>
                                                            </table>
                                                        </td>                                                         
                                                    </tr>
                                                  </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="13%">
                                                <asp:label id="lblClaimTypes" runat="server" wrap="False" text="Claim types:"></asp:label>
                                            </td>
                                            <td width="87%">
                                                <table>
                                                    <tr>

                                                         <td width="10%">
                                                <asp:listbox id="lstClaimType" runat="server" height="125px" rows="5"
                                                    width="200px" selectionmode="Multiple"></asp:listbox>
                                            </td>
                                            <td width="5%"><table style="margin-left:2px; display:inline;width:100%">
                                                          <tr><td class="style6">
                                                              <asp:Button ID="btnAddClaim" runat="server" Text="&gt;" 
                                                                  Font-Bold="False" Width="35px" onclick="AddSelectedOption" /></td></tr>
                                                            <tr><td class="style6"><asp:Button ID="btnAddAllClaim" runat="server" Text="&gt;&gt;" 
                                                                  Font-Bold="False" Width="35px" onclick="AddAllOption" /></td></tr>
                                                          <tr><td class="style6">
                                                              <asp:Button ID="btnRemoveClaim" runat="server" Text="&lt;" 
                                                                  Font-Bold="False" Width="35px" onclick="RemoveSelectedOption" /></td></tr>
                                                          <tr><td class="style6"><asp:Button ID="btnRemoveAllClaim" runat="server" Text="&lt;&lt;" 
                                                                  Font-Bold="False" Width="35px" onclick="RemoveAllOption" /></td></tr>
                                                          </table></td>
                                            <td width="85%">
                                                <asp:listbox id="lstSelectedClaimType" runat="server"  height="125px" rows="5"
                                                    width="200px" selectionmode="Multiple"></asp:listbox>
                                            </td>


                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                  </ContentTemplate>
                        </asp:updatepanel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:label id="lblClaimNumber" runat="server" wrap="False" text="Claim Number:"></asp:label>
                                            </td>
                                            <td>
                                                <asp:ListBox ID="lstClaimNumber" runat="server" Height="120px" Width="150px"></asp:ListBox>
                                                <asp:button runat="server" class="EllipsisControl" id="claimnumberbtn" onclientclick="return lookupData('lstClaimNumber','claim',1,'lstClaimNumber',6);" />
                                                <input type="button" title="Remove" class="BtnRemove styleRemove" id="lstClaimsbtndel" onclick="DeleteSelectedClaim('lstClaimNumber', 'lstClaimNumber_Ids')" value="-" runat="server" />
                                                <asp:TextBox runat="server" style="display: none" id="lstClaimNumber_Ids" name="lstClaimNumber_Ids" value="" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:label id="lblTPAType" font-underline="True" runat="server" font-size="Small" font-bold="True" wrap="False" text="Third Party Interface Type:"></asp:label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlTPInterfaceType" runat="server" Width="150px"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:label id="lblEmailNotification" runat="server" wrap="False" text="Email Notification:"></asp:label>
                                            </td>
                                            <td>
                                                <asp:textbox id="txtEmailNotification" runat="server" font-size="Small" height="18px" onblur="MultipleEmailLostFocus(this)"
                                                    width="33%"></asp:textbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:label id="lblTestRun" runat="server" wrap="False" text="Test Run:"></asp:label>
                                            </td>
                                            <td>
                                                <asp:checkbox id="chkTestRun" runat="server" text="" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 50%; height: 31px;">
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:button id="btnSave" class="button" runat="server" style="width: 200px"
                                                    text="Save Settings" OnClick="btnSave_Click" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:button id="Cancel" class="button" runat="server" onclientclick="OnCancel();return false;" text="Cancel"
                                                            width="81px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />

                                </td>
                            </tr>
                        </table>
            <input type="hidden" name="hTabName" id="hTabName" runat="server" value="Settings" />
            <asp:hiddenfield id="hdTaskManagerXml" runat="server" />
            <asp:hiddenfield id="hdOptionsetId" runat="server" />
        </div>

        <%--Ankit Gupta - MITS#36997 GAP 06 PMC - End--%>  
    </form>
</body>
</html>
