﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimExportCSStarsSettings.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.CSStarExport" ValidateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Claim Export CSStars</title>
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%-- <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }  </script>--%>
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/DataIntegrator.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/tmsettings.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/DataIntegrator.js"></script>
    <style type="text/css">        .style7
        {
            width: 300px;
        }
        .style1
        {
            width: 200px;
        }
        .style9
        {
            height: 7px;
            width: 320px;
        }
        .style10
        {
            height: 7px;
            width: 300px;
        }
        .style12
        {
            width: 300px;
            height: 45px;
        }
        .style13
        {
            height: 45px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        
        function LOBCheckedChanged(chkBoxChecked) {
            debugger;
            var chkboxlob_GL = document.getElementById("chkboxlob_GL");
            var lbclaimtypecode_GL = document.getElementById("lbclaimtypecode_GL");
            var lbreservetype_GL = document.getElementById("lbreservetype_GL");
            if (chkBoxChecked) {
                lbclaimtypecode_GL.disabled = false;
                lbclaimtypecode_GL.options[0].selected = true;
                for (var i = 1; i < lbclaimtypecode_GL.options.length; i++) {
                    lbclaimtypecode_GL.options[i].selected = false;
                }                
                lbreservetype_GL.disabled = false;
                lbreservetype_GL.options[0].selected = true;
                for (var i = 1; i < lbreservetype_GL.options.length; i++) {
                    lbreservetype_GL.options[i].selected = false;
                }
            }
            else {
                chkboxlob_GL.checked = false;
                lbclaimtypecode_GL.disabled = true;
                lbreservetype_GL.disabled = true;
            }
        }

//        function SuppCheckedChanged(chkBoxChecked) {
//            var chkboxsuppfield = document.getElementById("chkboxsuppfield");
//            var ddlsuppfield = document.getElementById("ddlsuppfield");
//            if (chkboxsuppfield != null && chkBoxChecked) {
//                chkboxsuppfield.disabled = false;
//                ddlsuppfield.disabled = false;
//            }
//            else if (chkboxsuppfield != null) {
//                chkboxsuppfield.checked = false;
//                ddlsuppfield.disabled = true;
//                ddlsuppfield.options[0].selected = true;
//            }
//        }
    </script>
</head>

<body>
    <form id="frmData" name="frmData" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />

    <!--<Table1:ErrorControl>-->    
    <table width="840px" border="0" cellspacing="0" cellpadding="4">
        <tr>
        <td colspan="2">
            <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        </td>
        </tr>
        <tr height="10">
        <td>
        </td>
        </tr>
    </table>

    <div>
        <asp:HiddenField runat="server" ID="hiddenfield" Value="" />
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox9" />

        <div class="msgheader" id="div_formtitle1" runat="server">
            <asp:Label ID="lblheader" runat="server" Text="Claim Export Setting" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>

        <div id="Div3" class="errtextheader" runat="server">
            <%--<asp:Label ID="lblError" runat="server" />--%>
        </div>
    </div>
    
    <br />

    <!--<Tab Settings>-->
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="NotSelected" nowrap="true" name="TABSCESettings" id="TABSCESettings">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="CESettings" id="LINKTABSCESettings"><span style="text-decoration: none">Claim Export Settings</span></a>
            </td>
            <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none; border-top: none;">
                &nbsp;&nbsp;
            </td>
            <td class="NotSelected" nowrap="true" name="TABSCECommonInfo" id="TABSCECommonInfo">
                <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="CECommonInfo" id="LINKTABSCECommonInfo"><span style="text-decoration: none">Claim Export Common Info</span></a>
            </td>
            <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none; border-top: none;">
                &nbsp;&nbsp;
            </td>                        
        </tr>
    </table>
        
    <div style="position: relative; left: 0; top: 0; width: 840px; height: 320px;">
    <table border="0" name="FORMTABCESettings" id="FORMTABCESettings" cellpadding="0" cellspacing="0" style="height: 300px">
    <tr>
        <td>
        <!--<OptionSet>-->
        <table border="0" cellspacing="0" cellpadding="0" style="width: 840px">
        <tr><td class="style9"></td></tr>
        <tr>
            <td>
                <asp:Label ID="lblOptionsetName" runat="server" ForeColor="Black" Text="Optionset Name:" Font-Bold="True" Font-Underline="True"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtOptionsetName" runat="server" Width="220px"></asp:TextBox>
                <asp:Label ID="lblErrorOnOptionsetName" runat="server" ForeColor="#FF3300" Style="font-weight: 700" Text="Optionset Name already exists..." Visible="False"></asp:Label>
            </td>
        </tr>
        </table>
        </td>
    </tr>
    <tr><td class="style9"></td></tr>
    <%--Org Hierarchy Grid ( Org + Date Of Claim + Active Org)--%>    
    <tr>
        <td>
        <div>
            <div>
                <span>
                    <dg:UserControlDataGrid runat="server" ID="CEOrgRelatedInfoGrid" GridName="CEOrgRelatedInfoGrid" GridTitle="Select Organizational Reporting Level :" Target="/Document/ClaimExportOrgList"  Unique_Id="RowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|RowId|" ShowHeader="True" LinkColumn="" PopupWidth="600" PopupHeight="250" Type="GridAndButtons" RowDataParam="listrow"/>
                </span>
            </div>
                <asp:TextBox style="display:none" runat="server" id="OrgRelatedInfoSelectedId"  RMXType="id" />
                <asp:TextBox style="display:none" runat="server" id="OrgRelatedInfoGrid_RowDeletedFlag"  RMXType="id" Text="false" />
                <asp:TextBox style="display:none" runat="server" id="OrgRelatedInfoGrid_Action"  RMXType="id" />
                <asp:TextBox style="display:none" runat="server" id="OrgRelatedInfoGrid_RowAddedFlag"  RMXType="id" Text="false"  />
        </div>
        </td>
    </tr>
    <tr><td class="style9"></td></tr>
    <%--</table>--%>
    <tr><td>
        <table style="width:840px;">
        <!--<Financial Date Range>-->
        <tr>
            <td><b><u>Select Financial Date Range :</u></b></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblfinancialdaterangefrom" runat="server" Text="From"></asp:Label>
                <input type="text" id="txtFinFromDate1" runat="server" formatas="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" size="10" />
                                    <%--vkumar258 - RMA-6037 - Starts --%>
                                    <%-- <input type="button" id="btnFinFromDate" runat="server" class="DateLookupControl" value="" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup({ inputField: "txtFinFromDate1", ifFormat: "%m/%d/%Y", button: "btnFinFromDate" });      
                </script>--%>
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#txtFinFromDate1").datepicker({
                                                showOn: "button",
                                                buttonImage: "../../../../Images/calendar.gif",
                                                showOtherMonths: true,
                                                selectOtherMonths: true,
                                                changeYear: true
                                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                        });
                                    </script>
                                    <%--vkumar258 - RMA_6037- End--%>
                                    <asp:label id="lblfinancialdaterangeto" runat="server" text="To"></asp:label>
                                    <input type="text" id="txtFinToDate1" runat="server" formatas="date" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" size="10" />
                                    <%--vkumar258 - RMA-6037 - Starts --%>
                                    <%--<input type="button" id="btnFinToDate" runat="server" class="DateLookupControl" value="" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup({ inputField: "txtFinToDate1", ifFormat: "%m/%d/%Y", button: "btnFinToDate" });      
                </script>--%>
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#txtFinToDate1").datepicker({
                                                showOn: "button",
                                                buttonImage: "../../../../Images/calendar.gif",
                                                showOtherMonths: true,
                                                selectOtherMonths: true,
                                                changeYear: true
                                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                        });
                                    </script>
                                    <%--vkumar258 - RMA_6037- End--%>
                                </td>
                            </tr>
                            <!--<LOB And Codes And Ajax>-->
                            <%--<asp:UpdatePanel ID="UpdatePanelLOB" runat="server" UpdateMode="Always">
        <ContentTemplate>--%>
        <%--<table border="0" cellspacing="0" cellpadding="0">--%>
        <tr><td class="style9"></td></tr>
        <tr>
            <td class="style7">Select Line of Business & Appropriate Codes :</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style7">
                <asp:CheckBox runat="server" ID="chkboxlob_GL" Text="General Liability Claims" Height="18px" AutoPostBack="true" oncheckedchanged="chkboxlob_GL_CheckedChanged" ></asp:CheckBox>
                <%--AutoPostBack="false" OnClick="LOBCheckedChanged(this.checked);"--%>
                <%-- AutoPostBack="true" oncheckedchanged="chkboxlob_GL_CheckedChanged"--%>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr><td class="style9"></td></tr>
        <tr>
            <td class="style7">Claim Type Codes :</td>
            <td>Reserve Type Codes :</td>
        </tr>
        <tr>
            <td class="style7">
                <asp:ListBox ID="lbclaimtypecode_GL" runat="server" Width="300px" SelectionMode="Multiple" ></asp:ListBox>
            </td>
            <td>
                <asp:ListBox ID="lbreservetype_GL" runat="server" Width="300px" SelectionMode="Multiple"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td class="style9"></td>
        </tr>
        <%--<caption>&nbsp;</caption>--%>                    
        <%--</table>--%>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
        </table>
        </td>
    </tr>
    </table>
    
    <!--<Claim Office Code,TPA Identification Code,Claim Extract Format,Multiple Claim#,Supp Field From Policy Supp,Email Addresses>-->
    <table name="FORMTABCECommonInfo" id="FORMTABCECommonInfo" style="display:none">
    <tr>
        <td class="style9"></td>
    </tr>
    <tr>
        <td class="style7"><b><u>Enter Claim Office : </u></b></td>
        <td>
            <asp:TextBox ID="txtclaimoffice" runat="server" Font-Size="Small" MaxLength="3" Height="18px" Width="260px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style7"><b><u>Enter TPA Identification Code : </u></b></td>
        <td>
            <asp:TextBox ID="txtidentificationcode" runat="server" Font-Size="Small" MaxLength="5" Height="18px" Width="260px"></asp:TextBox>
        </td>
    </tr>
    <%--<tr>
        <td class="style7"><b><u>Select Claim Extract Format : </u></b></td>
        <td>
            <asp:DropDownList ID="ddlExtractFormat" runat="server" Font-Size="Small" Width="260px">
            <asp:ListItem Text="CS Stars" Value="1" />
            </asp:DropDownList>
        </td>
    </tr>
    <tr><td class="style7"></td></tr>
    <tr>
        <td class="style7">
            <asp:CheckBox ID="chkboxmultipleclaimno" runat="server" Text="Select Multiple Claim Numbers : " />
        </td>
        <td>
            <asp:FileUpload ID="fuuploadclaimdata" runat="server" Width="260px" oncontextmenu="return false;" onkeypress="return false;" onpaste="return false" />
        </td>
    </tr>--%>

    <%--Policy Supp Field - Temporary Hidden--%>
    <%--<tr><td class="style7">&nbsp;</td></tr>
    <tr>
        <td class="style7">
            <asp:CheckBox ID="chkboxsuppfield" runat="server" onclick="SuppCheckedChanged(this.checked);" Text="Select Supplemental Field From POLICY SUPP : " />
        </td>
        <td>
            <asp:DropDownList ID="ddlsuppfield" runat="server" Font-Size="Small" Width="260px"></asp:DropDownList>
        </td>
    </tr>--%>

    <tr><td class="style7">&nbsp;</td></tr>
    <tr>
        <td class="style7">Provide Email Addresses For Error Log :</td>
        <td>Provide Email Addresses For Process Log :</td>
    </tr>
    <tr>
        <td class="style12">
            <asp:TextBox ID="txtemailforerror" runat="server" Height="45px" MaxLength="512" TextMode="MultiLine" Width="260px"></asp:TextBox>
        </td>
        <td class="style13">
            <asp:TextBox ID="txtemailforprocess" runat="server" Height="45px" MaxLength="512" TextMode="MultiLine" Width="260px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style7"><i>Note: Use comma(,) as separator in case of multiple email addresses&nbsp;</i></td>
    </tr>
    </table>
    <%-- bschroeder MITS 26773 corrected value of 'value' --%>
    <input type="hidden" name="hTabName" id="hTabName"  runat="server"  value="CESettings" />       
    
    <%--Save And Cancel--%>
    <table style="width: 840px; height: 31px;">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 320px" align="right">
            <asp:Button ID="OK" class="button" runat="server" Style="width: 100px" Text="Save" OnClick="OK_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Cancel" class="button" runat="server" Text="Cancel" Width="100px" OnClick="Cancel_Click" />
        </td>
    </tr>
    </table>

    <asp:HiddenField ID="hdTaskManagerXml" runat="server" />
    <asp:HiddenField ID="hdOptionsetId" runat="server" />
    <asp:HiddenField ID="hdnaction" runat="server" />
    <asp:HiddenField ID="hdnreq1" runat="server" />
    <asp:HiddenField ID="hdSelOrgHie" runat="server" />
    <asp:HiddenField ID="hdEntityDate" runat="server" />
    <asp:HiddenField ID="hdEntityName" runat="server" />
    <asp:HiddenField ID="hdGLClaimtype" runat="server" />
    <asp:HiddenField ID="hdGLReservetype" runat="server" />
    <asp:HiddenField ID="hdGLChkbox" runat="server" />
    <asp:HiddenField ID="hdOrgInactive" runat="server" />
    <asp:HiddenField ID="hdOptionset" runat="server" />
    </div>
    <!--<</ContentTemplate>
    </asp:UpdatePanel>>-->
    </form>
</body>
</html>
