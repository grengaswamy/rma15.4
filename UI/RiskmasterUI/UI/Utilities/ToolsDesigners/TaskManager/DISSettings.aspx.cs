//Created by pmittal5
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Riskmaster.ServiceHelpers;
using Riskmaster.BusinessAdaptor.Common;
//Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
using Riskmaster.UI.DataIntegratorService;      //kkaur25 JIRA 10996 REST-SOA change
using System.IO;
using Riskmaster.Models;    //ipuri 12/06/2014

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class DISSettings : System.Web.UI.Page
    {
        private const string sModuleName = "DIS";
        int iOptionSetId = 0;
        Array arrModulesSettings = new string[] { "EmployeeSettings", "OrgHierSettings", "FundsSettings", "FundsDepositsSettings", "VehiclesSettings",
                                                  "EntitiesSettings", "PoliciesSettings","PoliciesCarrierSettings", "ReservesSettings", "OrgExpoSettings", "PatientsSettings",
                                                  "PhysiciansSettings", "MedicalStaffSettings", "AdminTrackSettings" };
        static bool bCarrier; //sagarwal54 MITS 37049

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager1.RegisterAsyncPostBackControl(Employees_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(OrgHier_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(Funds_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(FundsDeposit_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(Vehicles_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(Entities_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(Policies_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(Reserves_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(OrgExpo_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(Patients_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(Physicians_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(MedicalStaff_Flag);
            ScriptManager1.RegisterAsyncPostBackControl(AdminTrack_Flag);
            
            //Hide Verification Flag
            //Verification_Flag.Visible = false;
            Verification_Flag.Visible = true;
            btnSave.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnSave, null) + ";");
            if (!IsPostBack)
            {
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                iOptionSetId = CommonFunctions.OptionsetId;
                hdOptionsetId.Value = Common.Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                RetrieveSettings(); //Retrieve Settings from data integrator tables.
            }

            if (CommonFunctions.OptionsetId > 0)
            {
                txtOptionName.Enabled = false;
            }
            else
            {
                txtOptionName.Enabled = true;
            }
            //Change Visibility of controls according to Selected settings
            ChangeControlsVisibility();
        }

        protected void RetrieveSettings()
        {
            //ipuri REST Service Conversion 12/06/2014 Start
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = null; 
            //Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objDIService = null; 
            Riskmaster.Models.DataIntegratorModel objDIModel = null;
            //ipuri REST Service Conversion 12/06/2014 End
            BusinessAdaptorErrors objAdaptorErrors = null; 
            bool bErr = false;

            try
            {
                objDIModel = new Riskmaster.Models.DataIntegratorModel();
                //objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                //objDIService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                //Vsoni5 : MITS 22565 : Exception thrown by Service Layer will be shown on UI.
                objAdaptorErrors = new BusinessAdaptorErrors();
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objDIModel.Token = AppHelper.GetSessionId();
                }
                objDIModel.OptionSetID = iOptionSetId;
                objDIModel.ModuleName = sModuleName;
                //ipuri REST Service Conversion 12/06/2014 Start
                //objDIModel = objDIService.RetrieveSettings(objDIModel);
                objDIModel.ClientId = AppHelper.ClientId;

                objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                //ipuri REST Service Conversion 12/06/2014 End
                bCarrier = objDIModel.bIsCarrier; //sagarwal54 MITS 37049
                txtOptionName.Text = objDIModel.OptionSetName;
                Dictionary<string, string> DIS_Setting = new Dictionary<string, string>();
                DIS_Setting = objDIModel.Parms;
                //  START :  mkaur24 : MITS 33475 , 33476 , 33478  : DIS UI changes in sync with Desktop DIS and Carrier Settings Functionality.

                if (objDIModel.bIsCarrier == false)
                {
                    PoliciesCarrierSettingsPanel.Visible = false;
                }
                else
                {
                    PoliciesCarrierSettingsPanel.Visible = true;
                }
                // END :  mkaur24 : MITS 33475 , 33476 , 33478  : DIS UI changes in sync with Desktop DIS and Carrier Settings Functionality.

                //---sgupta320: jira-10616 more than 2GB enhancement.
                if (objDIModel.OptionSetID == 0)
                {
                    chk2GBData.Enabled = true;
                }
                else
                {
                    chk2GBData.Enabled = false;
 
                }

                string chk2GbVal = string.Empty;

                foreach (KeyValuePair<string, string> kvp in DIS_Setting)
                {
                    if (kvp.Key.Contains("chk2GBData"))
                    {
                        chk2GbVal = kvp.Value.ToString();

                    }
                }

                if (chk2GbVal != "")
                {
                    lblBatchID.Visible = true;
                    txtBatchID.Visible = true;
                    txtBatchID.Text = Convert.ToString(objDIModel.BatchID);
                    txtBatchID.Enabled = false;
                    lbl_SelAccessDb.Visible = false;
                    DBPath.Visible = false;
                    chk2GBData.Enabled = false;
                }
                //-------end

                AdminTrack_Area.DataSource = objDIModel.dsAdminTrackingTables;
                AdminTrack_Area.DataTextField = objDIModel.dsAdminTrackingTables.Tables[0].Columns[1].ToString();
                AdminTrack_Area.DataValueField = objDIModel.dsAdminTrackingTables.Tables[0].Columns[0].ToString();
                AdminTrack_Area.DataBind();
                AdminTrack_Area.SelectedIndex = 0;

                AdminTrack_Match_Field.DataSource = objDIModel.dsAdminTrackTableFields;
                AdminTrack_Match_Field.DataTextField = objDIModel.dsAdminTrackTableFields.Tables[0].Columns[0].ToString();
                AdminTrack_Match_Field.DataValueField = objDIModel.dsAdminTrackTableFields.Tables[0].Columns[0].ToString();
                AdminTrack_Match_Field.DataBind();
                AdminTrack_Match_Field.SelectedIndex = 0;

                UpdateUIFlags(DIS_Setting);
                //sagarwal54 JIRA 2351 start 09/25/2014
                if (Policies_Insured_EntityID.Checked)
                {
                    Policies_Match_Insured_Abbrev.Enabled = false;
                    Policies_Match_Insured_Name.Enabled = false;
                    Policies_Match_Insured_TaxID.Enabled = false;
                }
                if (Policies_Insurer_EntityID.Checked)
                {
                    Policies_Match_Insurer_Abbrev.Enabled = false;
                    Policies_Match_Insurer_Name.Enabled = false;
                    Policies_Match_Insurer_TaxID.Enabled = false;
                }
                //sagarwal54 JIRA 2351 end 09/25/2014
            }
            catch(Exception exp)
            {
                bErr = true;
                //Vsoni5 : MITS 22565 : Exception thrown by Service Layer will be shown on UI.
                objAdaptorErrors.Add("DataIntegrator.Error",exp.Message,BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(objAdaptorErrors);
            }
        }

        private void UpdateUIFlags(Dictionary<string, string> DIS_Setting)
        {
            Control oControl = null;
            string sControlType = string.Empty;
            foreach (KeyValuePair<string, string> kvp in DIS_Setting)
            {
                oControl = FindControl(kvp.Key);
                if (oControl != null)
                {
                    sControlType = oControl.GetType().ToString();
                    sControlType = sControlType.Substring(sControlType.LastIndexOf(".") + 1);
                    switch (sControlType)
                    {
                        case "CheckBox":
                            if (kvp.Value == "1")
                                ((CheckBox)oControl).Checked = true;
                            else if (kvp.Value == "0")
                                ((CheckBox)oControl).Checked = false;
                            break;
                        case "RadioButton":
                            if (kvp.Value == "1")
                            {
                                ((RadioButton)oControl).Checked = true;
                                ((RadioButton)oControl).Attributes.Add("onclick","javascript:setTimeout('__doPostBack(\\'"+((RadioButton)oControl).ID+"\\',\\'\\')', 0)");

                            }
                            else
                                ((RadioButton)oControl).Checked = false;
                            break;
                        default:
                            if (kvp.Key == "AdminTrack_Area" || kvp.Key == "AdminTrack_Match_Field")
                                ((DropDownList)oControl).SelectedValue = kvp.Value;
                            break;
                    }
                }
                else
                {
                    if (kvp.Key == "AdminTrack_Add_Update")
                    {
                        if (kvp.Value == "0")
                            Add.Checked = true;
                        else if (kvp.Value == "1")
                            Add_Update.Checked = true;
                    }
                    else if (kvp.Key == "Patients_Match_Patient")
                    {
                        if (kvp.Value == "0")
                            AcctNo.Checked = true;
                        else if (kvp.Value == "1")
                            MedRcdNo.Checked = true;
                    }
                }

             }
        }

        private void ChangeControlsVisibility()
        {
            if (Employees_Flag.Checked)
                EmployeeSettings.Visible = true;
            else
                EmployeeSettings.Visible = false;

            if (OrgHier_Flag.Checked)
                OrgHierSettings.Visible = true;
            else
                OrgHierSettings.Visible = false;

            if (Funds_Flag.Checked)
                FundsSettings.Visible = true;
            else
                FundsSettings.Visible = false;

            if (FundsDeposit_Flag.Checked)
                FundsDepositsSettings.Visible = true;
            else
                FundsDepositsSettings.Visible = false;

            if (Vehicles_Flag.Checked)
                VehiclesSettings.Visible = true;
            else
                VehiclesSettings.Visible = false;

            if (Entities_Flag.Checked)
                EntitiesSettings.Visible = true;
            else
                EntitiesSettings.Visible = false;

            if (Policies_Flag.Checked)
            {
                PoliciesSettings.Visible = true;
                PoliciesCarrierSettings.Visible = true; //Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 
            }

            else
            {
                PoliciesSettings.Visible = false;
                PoliciesCarrierSettings.Visible = false; //Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 
            }           

            if (Reserves_Flag.Checked)
                ReservesSettings.Visible = true;
            else
                ReservesSettings.Visible = false;

            if (OrgExpo_Flag.Checked)
                OrgExpoSettings.Visible = true;
            else
                OrgExpoSettings.Visible = false;

            if (Patients_Flag.Checked)
                PatientsSettings.Visible = true;
            else
                PatientsSettings.Visible = false;

            if (Physicians_Flag.Checked)
                PhysiciansSettings.Visible = true;
            else
                PhysiciansSettings.Visible = false;

            if (MedicalStaff_Flag.Checked)
                MedicalStaffSettings.Visible = true;
            else
                MedicalStaffSettings.Visible = false;

            if (AdminTrack_Flag.Checked)
            {
                AdminTrackSettings.Visible = true;
                Verification_Flag.Enabled = false;		//Mits:37529
            }
            else
            {
                AdminTrackSettings.Visible = false;
                Verification_Flag.Enabled = true;		//Mits:37529
            }
            
            if (Add_Update.Checked)
                AdminTrack_FieldSettings.Visible = true;
            else
                AdminTrack_FieldSettings.Visible = false;
        }
        
        //npradeepshar 01/17/2010 MITS 23306
        /// <summary>
        /// Function will save setting into data integrator table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();

            try
            {
                SaveSettings();
            }
            catch (Exception ex)
            {
                //npradeepshar 01/17/2010 MITS 23306 : Log the error into log file as well display it on UI
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void SaveSettings()
        {
            string sCurrentUser = string.Empty;  //ipuri
            string sFileName = string.Empty;
            //ipuri REST Service Conversion 12/06/2014 Start
            //kkaur25 JIRA 10996 REST-SOA change
            Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModelS = null;
            Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objDIService = null;
	    //kkaur25 JIRA 10996 REST-SOA change
            Riskmaster.Models.DataIntegratorModel objDIModel = null;
            //ipuri REST Service Conversion 12/06/2014 End
            BusinessAdaptorErrors err = null;
            bool bErr = false;
            string sTemp = string.Empty;
            string sTaskManagerXML = string.Empty;
            int iOptionSetId = 0;
            int iNoOfImportedAreas = 0;
            //smahajan6 - 06/28/2010 - MITS# 21261 : Start
            string sFileExt = string.Empty;
            string sTempImportFile = string.Empty;
            //smahajan6 - 06/28/2010 - MITS# 21261 : End

            //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
            Riskmaster.Models.DAImportFile objDAImportFile = new Riskmaster.Models.DAImportFile();
	  //kkaur25 JIRA 10996 REST-SOA change
            Riskmaster.UI.DataIntegratorService.DAImportFile objDAImportFileS = new Riskmaster.UI.DataIntegratorService.DAImportFile();
	  //kkaur25 JIRA 10996 REST-SOA change
            Stream data = null;
            //Vsoni : End of MITS 22699.
            try
            {
            sTaskManagerXML = Server.HtmlDecode(hdTaskManagerXml.Value);
            iOptionSetId = Common.Conversion.ConvertObjToInt(hdOptionsetId.Value);
            objDIModel = new Riskmaster.Models.DataIntegratorModel();
	    //kkaur25 JIRA 10996 REST-SOA change
            objDIModelS = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            objDIService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
	    //kkaur25 JIRA 10996 REST-SOA change
            err = new BusinessAdaptorErrors();

            if (DBPath.HasFile)
            {
                sFileName = DBPath.PostedFile.FileName;
                if (!IsValidExtension(sFileName))
                {
                    err.Add("DataIntegrator.Error", "Unrecognized Database Format:" + sFileName + ". Only Files with Extensions .mdb ; .accdb accepted", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                    //throw new Exception("Unrecognized Database Format:" + sFileName + ". Only Files with Extension .mdb accepted");
                }
                sFileName = sFileName.Substring(sFileName.LastIndexOf("\\") + 1, sFileName.Length - sFileName.LastIndexOf("\\") - 1);
                //smahajan6 - 06/28/2010 - MITS# 21261 : Start
                sFileExt = sFileName.Substring(sFileName.LastIndexOf("."), sFileName.Length - sFileName.LastIndexOf("."));
                //smahajan6 - 06/28/2010 - MITS# 21261 : End
                sFileName = sFileName.Substring(0, sFileName.IndexOf("."));

            }

            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objDIModel.Token = AppHelper.GetSessionId();
                //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                objDAImportFile.Token = AppHelper.GetSessionId();
            }
            Dictionary<string, string> DIS_Setting = new Dictionary<string, string>();

            UpdateFlagsCollection(ref DIS_Setting, this.Form.Controls);
            //DIS_Setting.Add("Dairy_To_Users", "1");
            //ipuri UV Start
            sCurrentUser = AppHelper.GetUserLoginName();    
            //DIS_Setting.Add("Diary_To_Users", "1");  // csingh7 06/10/ 2010 : Defect # 5 : 'Dairy' changed to 'Diary'
            DIS_Setting.Add("Diary_To_Users", sCurrentUser);
            //ipuri UV end
            CalcNoOfImportedAreas(ref iNoOfImportedAreas, ImportedAreas.Controls);
            DIS_Setting.Add("Number_of_Areas_Importing", iNoOfImportedAreas.ToString());

            objDIModel.Parms = DIS_Setting;
            objDIModel.OptionSetID = iOptionSetId;
            objDIModel.ModuleName = sModuleName;
            objDIModel.TaskManagerXml = sTaskManagerXML;

            sTemp = txtOptionName.Text;
            if (sTemp == "")
            {
                err.Add("DataIntegrator.Error", "OptionSet Name cannot be blank", BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }

            if (Funds_Flag.Checked)     // csingh7 for MITS 21245 : Implementing Payee Matching criteris Check.
            {
                if (!Funds_Match_Payee_Tax_Id.Checked && !Funds_Match_Payee_Name.Checked && !Funds_Match_Entity_Id.Checked)
                {
                    err.Add("DataIntegrator.Error", "Payee matching criteria is required", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
            }
            //----sgupta320:Add code for more then 2GB 2GB enhancement
            if (chk2GBData.Checked)
            {
                if (txtBatchID.Text == "")
                {
                    err.Add("DataIntegrator.Error", "Please Enter BatchID", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                else
                {
                    objDIModel.ImportFlage = true;
                    objDIModel.BatchID = Convert.ToInt32(txtBatchID.Text);//---sgupta320 for more then 2GB Enhancement 
                    objDIModel.AdminTrackTable = AdminTrack_Area.SelectedValue.ToString();//---sgupta320 for more then 2GB Enhancement 
                }
            }
            else
            {
                if (DBPath.HasFile)
                    objDIModel.ImportFlage = true;
                else
                {
                    err.Add("DataIntegrator.Error", "Access Database Path cannot be blank", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
            }
              //----------2GB enhancment end

            // Start: sbhayana for MITS 36426: Validation to pop up error if no option is selected & Save Button is clicked

            if (!(Employees_Flag.Checked) && !(OrgHier_Flag.Checked) && !(Funds_Flag.Checked) && !(FundsDeposit_Flag.Checked) && !(Vehicles_Flag.Checked) && !(Entities_Flag.Checked) && !(Policies_Flag.Checked) && !(Reserves_Flag.Checked) && !(OrgExpo_Flag.Checked) && !(Patients_Flag.Checked) && !(Physicians_Flag.Checked) && !(MedicalStaff_Flag.Checked) && !(AdminTrack_Flag.Checked))
            {
                err.Add("DataIntegrator.Error", "Select one area you wish to import", BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
            // End: sbhayana for MITS 36426

            //sagarwal54 09/10/2014 MITS 37048 start
            if (Entities_Flag.Checked == true)
            {
                if ((Entities_Match_By_TaxId.Checked == false) && (Entities_Match_By_Name.Checked == false) && (Entities_Match_By_Abbrev.Checked == false))
                {
                    err.Add("DataIntegrator.Error", "Select atleast one of the Entity Matching Criteria: TaxId, Name or Abbreviation", BusinessAdaptorErrorType.Message);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
            }
            //sagarwal54 09/10/2014 MITS 37048 end

            //sagarwal54 09/10/2014 MITS 37049, JIRA 2351 start
            if (bCarrier == true)
            {
                if (Policies_Flag.Checked == true)
                {
                    bool bErrPol = false;
                    if (!Policies_Insured_EntityID.Checked)
                    {
                        if (Policies_Match_Insured_TaxID.Checked == false && Policies_Match_Insured_Name.Checked == false && Policies_Match_Insured_Abbrev.Checked == false)
                        {
                            err.Add("DataIntegrator.Error", "Select atleast one of the Insured Matching Criteria: TaxId, Name or Abbreviation", BusinessAdaptorErrorType.Message);
                            bErrPol = true;

                        }
                    }
                    if (!Policies_Insurer_EntityID.Checked)
                    {
                        if (Policies_Match_Insurer_TaxID.Checked == false && Policies_Match_Insurer_Name.Checked == false && Policies_Match_Insurer_Abbrev.Checked == false)
                        {
                            err.Add("DataIntegrator.Error", "Select atleast one of the Insurer Matching Criteria: TaxId, Name or Abbreviation", BusinessAdaptorErrorType.Message);
                            bErrPol = true;

                        }
                    }

                    if (Policies_Import_Additional_Interest.Checked)
                    {
                        if (Policies_Import_Additional_Interest_TaxID.Checked == false && Policies_Import_Additional_Interest_Name.Checked == false)
                        {
                            err.Add("DataIntegrator.Error", "Select atleast one of the Additional Interest Matching Criteria: TaxId or Name", BusinessAdaptorErrorType.Message);
                            bErrPol = true;
                        }
                    }

                    if (bErrPol == true)
                    {
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        bErrPol = false;
                        return;
                    }

                }
            }
            //sagarwal54 09/10/2014 MITS 37049, JIRA 2351 end

            /// Vsoni5 : 01/29/2011 : MITS 23358 : Commented the block of code. As per the new logix this block is no more required.
                //smahajan6 - 06/28/2010 - MITS# 21261 : Start
                //if(objDIModel.ImportFlage)
                //{
                //    objDIModel.FilePath = objDIService.GetRMConfiguratorDATempPath(objDIModel);
                    //sTempImportFile = sFileName + "_Temp" + string.Format("{0:MMddyyyyhhmmss}", System.DateTime.Now) + sFileExt;

                //    try
                //    {
                //        //Copy the Database file to Temp Folder
                //        //DBPath.PostedFile.SaveAs(sTempImportFile); -- Commented by Vsoni5 for MITS 22699 : Unused code.

                //        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                //        //data = DBPath.PostedFile.InputStream;
                //        //objDAImportFile.fileStream = data;
                //        //objDAImportFile.fileName = sTempImportFile;
                //        //objDAImportFile.filePath = objDIModel.FilePath;
                //        //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                //        //Vsoni5 : End of MITS 22699

                //        //Validate the Database Schema
                //        //sTempImportFile = objDIModel.FilePath + sTempImportFile;
                //        //objDIModel.sDAStagingDBImportFile = sTempImportFile;
                //        //objDIService.PeekDAStagingDatabase(objDIModel);
                //        try
                //        {
                //            //Delete the Temp Database file
                //            System.IO.File.Delete(sTempImportFile);
                //        }
                //        catch (Exception ex) { }
                //    }
                //    catch (Exception ex)
                //    {
                //        try
                //        {
                //            //Delete the Temp Database file
                //            System.IO.File.Delete(sTempImportFile);
                //        }
                //        catch (Exception ex1) { }
                //        err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                //        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                //        return;
                //    }
                //}
            //smahajan6 - 06/28/2010 - MITS# 21261 : End
            //if (bErr == true)      commented by csingh7  06/10/2010
            //{
            //    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            //    return;
            //}
            sTemp = sTemp.Replace("'", "");
            if (sTemp.Length > 50)
            {
                sTemp = sTemp.Substring(1, 50);
            }
            objDIModel.OptionSetName = sTemp;

            //smahajan6 - 06/22/2010 - MITS# 21191 : Start
            if (objDIModel.OptionSetID > 0)
            {
                objDIModel.bUpdateFlag = true;
            }
            //smahajan6 - 06/22/2010 - MITS# 21191 : End
            //ipuri REST Service Conversion 12/06/2014 Start
            //objDIModel = objDIService.SaveSettings(objDIModel);
            objDAImportFile.ModuleName = "DIS";
            objDAImportFile.DocumentType = "Import";
            objDAImportFile.OptionsetId = objDIModel.OptionSetID;
            objDIModel.ClientId = AppHelper.ClientId;
            objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
            //ipuri REST Service Conversion 12/06/2014 End
            if (objDIModel.OptionSetID == -1)
            {
                err.Add("DataIntegrator.Error", "Optionset name already exists", BusinessAdaptorErrorType.Message);
                bErr = true;
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            else if (objDIModel.ImportFlage)
            {
                //smahajan6 - 06/22/2010 - MITS# 21191 : Start
                //put were you want the file to be saved

                //----sgupta320: add for 2GB enhancement
                if (!chk2GBData.Checked)
                {
                    sFileName = sFileName + sFileExt + "." + objDIModel.OptionSetID;
                    objDIModel.sDAStagingDBImportFile = objDIModel.FilePath + sFileName;
                    //sFileName = sFileName + "_" + objDIModel.OptionSetID + ".mdb";
                    //DBPath.PostedFile.SaveAs(objDIModel.FilePath + sFileName); -- Commented by Vsoni5 for MITS 22699 : Unused code.

                    //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
                    data = DBPath.PostedFile.InputStream;
                    objDAImportFile.FileContents = Riskmaster.BusinessHelpers.DocumentBusinessHelper.StreamToByte(data);
                    objDAImportFile.fileName = sFileName;
                    objDAImportFile.filePath = objDIModel.FilePath;
                    objDAImportFile.ModuleName = "DIS";
                    objDAImportFile.DocumentType = "Import";
                    objDAImportFile.OptionsetId = objDIModel.OptionSetID;
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objDIService.UploadDAImportFile(objDAImportFile.Errors, objDAImportFile.Token, objDAImportFile.fileName, objDAImportFile.filePath, objDAImportFile.fileStream);
                    objDAImportFile.ClientId = AppHelper.ClientId;

		   //kkaur25 JIRA 10996 REST-SOA change
                    objDAImportFileS.FileContents = objDAImportFile.FileContents;
                    objDAImportFileS.fileName = sFileName;
                    objDAImportFileS.filePath = objDIModel.FilePath;
                    objDAImportFileS.ModuleName = "DIS";
                    objDAImportFileS.DocumentType = "Import";
                    objDAImportFileS.OptionsetId = objDIModel.OptionSetID;
                    objDAImportFileS.Token = AppHelper.Token;
                    objDAImportFileS.ClientId = AppHelper.ClientId;
                    //Call SOAP/XML service
                    objDIService.UploadDAImportFile(objDAImportFileS);

		    //kkaur25 JIRA 10996 REST-SOA change

                    // AppHelper.GetResponse("RMService/DAIntegration/uploaddaimportfile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDAImportFile);
                    //ipuri REST Service Conversion 12/06/2014 End
                    //Vsoni5 : End of MITS 22699

                    //ipuri REST Service Conversion 12/06/2014 Start   
                    //objDIService.PeekDAStagingDatabase(objDIModel);
                    //objDIService.UpdateDAStagingDatabase(objDIModel);
                    objDIModel.ClientId = AppHelper.ClientId;
                    //kkaur25 start UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder
                    if (objDIModel.ClientId > 0)
                    {
                        //----sgupta320: add for 2GB enhancement
                        if (!chk2GBData.Checked)
                        {
                            AppHelper.GetResponse("RMService/DAIntegration/RetrieveFile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                        }
                    }

                }
				//kkaur25 end UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder
                AppHelper.GetResponse("RMService/DAIntegration/peekdastagingdatabase", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                
                //----sgupta320: add for 2GB enhancement
                if (!chk2GBData.Checked)
                {
                    AppHelper.GetResponse("RMService/DAIntegration/updatedastagingdatabase", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                }
                //ipuri REST Service Conversion 12/06/2014 End
                //smahajan6 - 06/22/2010 - MITS# 21191 : End
            }
            if (objDIModel.OptionSetID > 0)
            {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx",false);
                }
            }
            catch (Exception eRM)
            {                
                bErr = true;
                /// Vsoni5 : 01/29/2011 : MITS 23358
                try
                {
                    if (iOptionSetId <= 0 && objDIModel.OptionSetID > 0)
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIService.SaveSettingsCleanup(objDIModel);
                        objDIModel.ClientId = AppHelper.ClientId;
                    AppHelper.GetResponse("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                }
                catch (Exception eCleanup)
                {                    
                }
                throw eRM;
                //return;
            }
        }

        private void UpdateFlagsCollection(ref Dictionary<string, string> DIS_Setting, ControlCollection oControls)
        {
            string sControlType = string.Empty;
            string sControlName = string.Empty;

            foreach (Control oControl in oControls)
            {
                if (oControl.Controls.Count == 0)
                {
                    sControlType = oControl.GetType().ToString();
                    sControlType = sControlType.Substring(sControlType.LastIndexOf(".") + 1);
                    sControlName = oControl.ID;

                    switch(sControlType)
                    {
                        case "CheckBox":
                            if (((CheckBox)oControl).Checked)
                                DIS_Setting.Add(sControlName, "1");
                            else
                                DIS_Setting.Add(sControlName, "0");
                            break;
                        case "RadioButton":
                            switch (sControlName)
                            {
                                case "Add": //"Add" and "Add_Update" radiobuttons are mutually exclusive
                                    if (((RadioButton)oControl).Checked)
                                        DIS_Setting.Add("AdminTrack_Add_Update", "0");
                                    else  
                                        DIS_Setting.Add("AdminTrack_Add_Update", "1");
                                    break;
                                case "Add_Update":      // Added by csingh7 06/10/2010 : To remove Add_Update tag in the XML
                                    break;
                                case "AcctNo": //"AcctNo" and "MedRcdNo" radiobuttons are mutually exclusive
                                    if (((RadioButton)oControl).Checked)
                                        DIS_Setting.Add("Patients_Match_Patient", "0");
                                    else
                                        DIS_Setting.Add("Patients_Match_Patient", "1");
                                    break;
                                case "MedRcdNo":        // Added by csingh7 06/10/2010 : To remove MedRcdNo tag in the XML
                                    break;
                                default:
                                    if (((RadioButton)oControl).Checked)
                                        DIS_Setting.Add(sControlName, "1");
                                    else
                                        DIS_Setting.Add(sControlName, "0");
                                    break;
                            }
                            break;
                        case "DropDownList":
                            DIS_Setting.Add(sControlName, ((DropDownList)oControl).SelectedValue);
                            break;
                    }
                }
                else
                {
                    UpdateFlagsCollection(ref DIS_Setting, oControl.Controls);
                }
            }
        }

        private void CalcNoOfImportedAreas(ref int iNoOfImportedAreas, ControlCollection oControls)
        {
            string sControlType = string.Empty;
            foreach (Control oControl in oControls)
            {
                //if (oControl.Controls.Count == 0)
                if (oControl.Controls.Count > 1)   // csingh7 06/10/2010 Defect # 4 : Calculated No. of Areas coming wrong
                {
                    foreach (Control oControl1 in oControl.Controls)
                    {
                        if (oControl1.Controls.Count == 1)
                        {
                            sControlType = oControl1.Controls[0].GetType().ToString();
                            sControlType = sControlType.Substring(sControlType.LastIndexOf(".") + 1);
                            //if (sControlType == "CheckBox")   // csingh7 06/10/2010 Defect # 4 : Calculated No. of Areas coming wrong
                            if (sControlType == "RadioButton")
                            {
                                if (((RadioButton)oControl1.Controls[0]).Checked)
                                    iNoOfImportedAreas = iNoOfImportedAreas + 1;
                            }
                        }
                    }
                }
                else
                {
                    CalcNoOfImportedAreas(ref iNoOfImportedAreas, oControl.Controls);
                }
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx");
        }

        protected void Employees_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Employees_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                EmployeeSettings.Visible = true;
                EmployeeSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "EmployeeSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void OrgHier_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (OrgHier_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                OrgHierSettings.Visible = true;
                OrgHierSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "OrgHierSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void Funds_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Funds_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                FundsSettings.Visible = true;
                FundsSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "FundsSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void FundsDeposit_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (FundsDeposit_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                FundsDepositsSettings.Visible = true;
                FundsDepositsSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "FundsDepositsSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void Vehicles_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Vehicles_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                VehiclesSettings.Visible = true;
                VehiclesSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "VehiclesSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void Entities_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Entities_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                EntitiesSettings.Visible = true;
                EntitiesSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "EntitiesSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void Policies_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Policies_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                PoliciesSettings.Visible = true;
                PoliciesCarrierSettings.Visible = true;//Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 
                PoliciesSettingsPanel.Update();
                PoliciesCarrierSettingsPanel.Update();//Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "PoliciesSettings" && str != "PoliciesCarrierSettings") //Added by mkaur24 : 16/05/2014 :  MITS 33475,33476, 33477 
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }

                }
            }
        }

        protected void Reserves_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Reserves_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                ReservesSettings.Visible = true;
                ReservesSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "ReservesSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void OrgExpo_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (OrgExpo_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                OrgExpoSettings.Visible = true;
                OrgExpoSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "OrgExpoSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void Patients_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Patients_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                PatientsSettings.Visible = true;
                PatientsSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "PatientsSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void Physicians_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (Physicians_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                PhysiciansSettings.Visible = true;
                PhysiciansSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "PhysiciansSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void MedicalStaff_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (MedicalStaff_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Enabled = true;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End
                MedicalStaffSettings.Visible = true;
                MedicalStaffSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "MedicalStaffSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void AdminTrack_Flag_CheckedChanged(object sender, EventArgs e)
        {
            if (AdminTrack_Flag.Checked)
            {
				//Mits:37529 Start
                Verification_Flag.Checked = false;
                Verification_Flag.Enabled = false;
                ((UpdatePanel)FindControl("VerificationPanel")).Update();
				//Mits:37529 End

                AdminTrackSettings.Visible = true;
                AdminTrackSettingsPanel.Update();
                foreach (string str in arrModulesSettings) //All settings except current should disappear
                {
                    if (str != "AdminTrackSettings")
                    {
                        FindControl(str).Visible = false;
                        ((UpdatePanel)FindControl(str + "Panel")).Update();
                    }
                }
            }
        }

        protected void Funds_Match_Entity_Id_CheckedChanged(object sender, EventArgs e)
        {
            if (Funds_Match_Entity_Id.Checked)
            {
                Funds_Match_Payee_Name.Checked = false;
                Funds_Match_Payee_Name.Enabled = false; 
                Funds_Match_Payee_Tax_Id.Checked = false;
                Funds_Match_Payee_Tax_Id.Enabled = false;
            }
            else
            {
                Funds_Match_Payee_Name.Enabled = true;
                Funds_Match_Payee_Tax_Id.Enabled = true; 
            }
        }

        protected void Funds_Match_Payee_Tax_Id_CheckedChanged(object sender, EventArgs e)  // Added by csingh7 : 06/10/2010 
        {
            if (Funds_Match_Payee_Tax_Id.Checked)
            {
                Funds_Match_Entity_Id.Checked = false;
                Funds_Match_Entity_Id.Enabled = false;
            }
            else
            {
                if (!Funds_Match_Payee_Name.Checked)
                    Funds_Match_Entity_Id.Enabled = true;
            }
        }

        protected void Funds_Match_Payee_Name_CheckedChanged(object sender, EventArgs e)     // Added by csingh7 : 06/10/2010 
        {
            if (Funds_Match_Payee_Name.Checked)
            {
                Funds_Match_Entity_Id.Checked = false;
                Funds_Match_Entity_Id.Enabled = false;
            }
            else
            {
                if (!Funds_Match_Payee_Tax_Id.Checked)
                    Funds_Match_Entity_Id.Enabled = true;
            }
        }

        protected void Employees_Match_Entity_Id_CheckedChanged(object sender, EventArgs e)
        {
            if (Employees_Match_Entity_Id.Checked)
            {
                Employees_Match_Employee_ID.Checked = false;
                Employees_Match_Employee_ID.Enabled = false;
            }
            else
            {
                Employees_Match_Employee_ID.Enabled = true;
            }
        }

        protected void Employees_Match_Employee_ID_CheckedChanged(object sender, EventArgs e)   // Added by csingh7 : 06/10/2010 : Defect # 7 : Disabling Match Entity ID checkbox on selecting Match Employye ID
        {
            if (Employees_Match_Employee_ID.Checked)
            {
                Employees_Match_Entity_Id.Checked = false;
                Employees_Match_Entity_Id.Enabled = false;
            }
            else
            {
                Employees_Match_Entity_Id.Enabled = true;
            }
        }
        protected void Policies_Insured_EntityID_CheckedChanged(object sender, EventArgs e) // Added by mkaur24 : MITS:33475,33476,33477 9/5/2014
        {
            if (Policies_Insured_EntityID.Checked)
            {
                Policies_Match_Insured_TaxID.Checked = false;
                Policies_Match_Insured_TaxID.Enabled = false;
                Policies_Match_Insured_Name.Checked = false;
                Policies_Match_Insured_Name.Enabled = false;
                Policies_Match_Insured_Abbrev.Checked = false;
                Policies_Match_Insured_Abbrev.Enabled = false;
            }
            else
            {
                if (!Policies_Insured_EntityID.Checked)
                    Policies_Insured_EntityID.Enabled = true;
                Policies_Match_Insured_TaxID.Enabled = true;
                Policies_Match_Insured_Name.Enabled = true;
                Policies_Match_Insured_Abbrev.Enabled = true;
            }
        }

        protected void Policies_Insurer_EntityID_CheckedChanged(object sender, EventArgs e)   // Added by mkaur24 : MITS:33475,33476,33477 9/5/2014
        {
            if (Policies_Insurer_EntityID.Checked)
            {
                Policies_Match_Insurer_TaxID.Checked = false;
                Policies_Match_Insurer_TaxID.Enabled = false;
                Policies_Match_Insurer_Name.Checked = false;
                Policies_Match_Insurer_Name.Enabled = false;
                Policies_Match_Insurer_Abbrev.Checked = false;
                Policies_Match_Insurer_Abbrev.Enabled = false;
            }
            else
            {
                if (!Policies_Insurer_EntityID.Checked)
                    Policies_Insurer_EntityID.Enabled = true;
                Policies_Match_Insurer_TaxID.Enabled = true;
                Policies_Match_Insurer_Name.Enabled = true;
                Policies_Match_Insurer_Abbrev.Enabled = true;
            }
        }
        protected void Policies_Import_Additional_Interest_CheckedChanged(object sender, EventArgs e)   // Added by mkaur24 : MITS:33475,33476,33477 16/05/2014
        {
            if (Policies_Import_Additional_Interest.Checked)
            {
                Policies_Import_Additional_Interest_TaxID.Checked = false;
                Policies_Import_Additional_Interest_TaxID.Enabled = true;
                Policies_Import_Additional_Interest_TaxID.Visible = true;
                Policies_Import_Additional_Interest_Name.Visible = true;
                lbl_Policies_Import_Additional_Interest.Visible = true;

                Policies_Import_Additional_Interest_Name.Checked = false;
                Policies_Import_Additional_Interest_Name.Enabled = true;

            }
            else
            {
                if (!Policies_Import_Additional_Interest.Checked)
                    Policies_Import_Additional_Interest.Enabled = true;
                Policies_Import_Additional_Interest_TaxID.Visible = false;
                Policies_Import_Additional_Interest_Name.Visible = false;
                lbl_Policies_Import_Additional_Interest.Visible = false;

            }
        }

        protected void AdminTrack_Add_Update_CheckedChanged(object sender, EventArgs e)
        {
            if (Add_Update.Checked)
            {
                AdminTrack_FieldSettings.Visible = true;
                //ipuri REST Service Conversion 12/06/2014 Start
                //Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objDIService = null;
                //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = null;
                Riskmaster.Models.DataIntegratorModel objDIModel = null;
                //ipuri REST Service Conversion 12/06/2014 End
                string sSelectedVal = string.Empty;
                DataSet dsFieldList = null;
                BusinessAdaptorErrors berr = null;
                bool bErr = false;
                try
                {
                        berr = new BusinessAdaptorErrors();
                        objDIModel = new Riskmaster.Models.DataIntegratorModel();
                        //objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                        //objDIService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();

                        if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                        {
                            objDIModel.Token = AppHelper.GetSessionId();
                        }

                        //Retreive Fields for Admin Tracking Table selected 
                        sSelectedVal = AdminTrack_Area.SelectedValue;
                        objDIModel.sAdminTrackTableName = sSelectedVal;
                        objDIModel.ModuleName = sModuleName;
                        //ipuri REST Service Conversion 12/06/2014 Start
                        //objDIModel = objDIService.GetAdminTrackFields(objDIModel);
                        objDIModel.ClientId = AppHelper.ClientId;
                        objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/getadmintrackfields", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                        //ipuri REST Service Conversion 12/06/2014 End

                        dsFieldList = objDIModel.dsAdminTrackTableFields;

                        AdminTrack_Match_Field.DataSource = dsFieldList;
                        AdminTrack_Match_Field.DataValueField = dsFieldList.Tables[0].Columns[0].ToString();
                        AdminTrack_Match_Field.DataTextField = dsFieldList.Tables[0].Columns[0].ToString();
                        AdminTrack_Match_Field.DataBind();
                }
                catch (Exception eRm)
                {
                    bErr = true;
                }
            }
            else
            {
                AdminTrack_FieldSettings.Visible = false;
            }
        }

        protected void AdminTrack_Area_SelectionChanged(object sender, EventArgs e)
        {
            //ipuri REST Service Conversion 12/06/2014 Start
            //Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objDIService = null;
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objDIModel = null;
            Riskmaster.Models.DataIntegratorModel objDIModel = null;
            //ipuri REST Service Conversion 12/06/2014 End
            string sSelectedVal = string.Empty;
            DataSet dsFieldList = null;
            BusinessAdaptorErrors berr = null;
            bool bErr = false;
            try
            {
                if (AdminTrack_FieldSettings.Visible)
                {
                    berr = new BusinessAdaptorErrors();
                    objDIModel = new Riskmaster.Models.DataIntegratorModel();
                    //objDIModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                    //objDIService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();

                    if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                    {
                        objDIModel.Token = AppHelper.GetSessionId();
                    }

                    //Retreive Fields for Admin Tracking Table selected 
                    sSelectedVal = AdminTrack_Area.SelectedValue;
                    objDIModel.sAdminTrackTableName = sSelectedVal;
                    objDIModel.ModuleName = sModuleName;

                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objDIModel = objDIService.GetAdminTrackFields(objDIModel);
                    objDIModel.ClientId = AppHelper.ClientId;
                    objDIModel = AppHelper.GetResponse<Riskmaster.Models.DataIntegratorModel>("RMService/DAIntegration/getadmintrackfields", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objDIModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                    dsFieldList = objDIModel.dsAdminTrackTableFields;

                    AdminTrack_Match_Field.DataSource = dsFieldList;
                    AdminTrack_Match_Field.DataValueField = dsFieldList.Tables[0].Columns[0].ToString();
                    AdminTrack_Match_Field.DataTextField = dsFieldList.Tables[0].Columns[0].ToString();
                    AdminTrack_Match_Field.DataBind();
                }
            }
            catch (Exception eRm)
            {
                bErr = true;
            }
      
        }

        private bool IsValidExtension(string sFileName)
        {
            string sFileExt = string.Empty;
            bool bValid = true;
            sFileExt = sFileName.Substring(sFileName.LastIndexOf(".") + 1, sFileName.Length - sFileName.LastIndexOf(".") - 1);
            if (sFileExt.ToLower() != "mdb" && sFileExt.ToLower() != "accdb")  // added check for accdb foile also : csingh7 06/10/2010
            {
                bValid = false;
            }
            return bValid;
        }

        protected void chk2GBData_CheckedChanged(object sender, EventArgs e)
        {
            if (chk2GBData.Checked)
            {
                lblBatchID.Visible = true;
                txtBatchID.Visible = true;
                lbl_SelAccessDb.Visible = false;
                DBPath.Visible = false;
                DBPath.Attributes.Clear();
            }
            else
            {
                lblBatchID.Visible = false;
                txtBatchID.Visible = false;
                lbl_SelAccessDb.Visible = true;
                DBPath.Visible = true;
            }
        }
    }
}
