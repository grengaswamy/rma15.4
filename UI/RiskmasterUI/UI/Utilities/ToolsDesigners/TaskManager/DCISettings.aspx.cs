﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Models;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class DCISettings : System.Web.UI.Page
    {
        protected string sJurisdictionState;
        private int iOptionSetIDParam;
        private const string sModuleName = "DCI";
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                iOptionSetIDParam = CommonFunctions.OptionsetId;
                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                RetriveSettingsFromDB();
            }
        }

        /// <summary>
        /// Retrives the settings from database.
        /// </summary>
        private void RetriveSettingsFromDB()
        {
            string sClaimTypes = string.Empty;
            DataIntegratorModel objReturnModel = null;
            
            try
            {
                objReturnModel = new DataIntegratorModel();
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objReturnModel.Token = AppHelper.GetSessionId();
                }
                

                objReturnModel.OptionSetID = iOptionSetIDParam;
                objReturnModel.ModuleName = sModuleName;
                objReturnModel.ClientId = AppHelper.ClientId;
                objReturnModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objReturnModel);

                Dictionary<string, string> DCI_Setting = new Dictionary<string, string>();
                DCI_Setting = (Dictionary<string, string>)objReturnModel.Parms;

                lstAllStates.DataSource = objReturnModel.dtJurisStates;
                lstAllStates.DataTextField = objReturnModel.dtJurisStates.Columns[0].ToString();
                lstAllStates.DataValueField = objReturnModel.dtJurisStates.Columns[1].ToString();
                lstAllStates.DataBind();

                lstClaimType.DataSource = objReturnModel.dtClaimTypes;
                lstClaimType.DataTextField = objReturnModel.dtClaimTypes.Columns[1].ToString();
                lstClaimType.DataValueField = objReturnModel.dtClaimTypes.Columns[0].ToString();
                lstClaimType.DataBind();

                ddlTPInterfaceType.DataSource = objReturnModel.dtTPInterfaceType;
                ddlTPInterfaceType.DataTextField = objReturnModel.dtTPInterfaceType.Columns[1].ToString();
                ddlTPInterfaceType.DataValueField = objReturnModel.dtTPInterfaceType.Columns[0].ToString();
                ddlTPInterfaceType.DataBind();
                ddlTPInterfaceType.Items.Insert(0,new ListItem("--Select--","0"));

                if (!string.IsNullOrWhiteSpace(objReturnModel.sLastRuntime))
                    txtFromDate.Value = Conversion.GetDBDateFormat(objReturnModel.sLastRuntime, "d");
                else
                    txtFromDate.Value = Conversion.GetDBDateFormat("19000101", "d");

                txtToDate.Value = DateTime.Now.Date.ToShortDateString();

                if (objReturnModel.OptionSetID > 0)
                {
                    if (!string.IsNullOrWhiteSpace(objReturnModel.OptionSetName))
                    {
                        txtOptionSetName.Text = objReturnModel.OptionSetName;
                        txtOptionSetName.ReadOnly = true;
                    }

                    PopulateControl(DCI_Setting["JURISDICTION_STATES"], "JuriState");
                    PopulateControl(DCI_Setting["CLAIM_TYPES"], "ClaimType");

                    txtLastRunDate.Text = Conversion.GetDBDTTMFormat(objReturnModel.sLastRuntime, "d", "t");
                    txtCarrierCode.Text = DCI_Setting["DCI_CARRIER_CODE"];
                    txtFromDate.Value = Conversion.GetDBDateFormat(DCI_Setting["CLAIM_DATE_RPTD_FROM"], "d");
                    txtToDate.Value = Conversion.GetDBDateFormat(DCI_Setting["CLAIM_DATE_RPTD_TO"], "d");

                    if (!string.IsNullOrWhiteSpace(DCI_Setting["CLAIM_NUM"]))
                    {
                        BindClaimNumberListBox(DCI_Setting["CLAIM_NUM"]);
                        lstClaimNumber_Ids.Text = DCI_Setting["CLAIM_NUM"];
                    }
                    txtEmailNotification.Text = DCI_Setting["EMAIL_NOTICE"];

                    foreach (ListItem li in ddlTPInterfaceType.Items)
                    {
                        if (li.Value == DCI_Setting["TP_INTERFACE_TYPE"])
                            li.Selected = true;
                    }

                    if (DCI_Setting["TEST_RUN"] == "-1")
                        chkTestRun.Checked = true;
                    else
                        chkTestRun.Checked = false;

                    
                }
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add("DataIntagrator.Error", "Error in retrieving setting.", BusinessAdaptorErrorType.Message);
                err.Add("DataIntagrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }

        /// <summary>
        /// Binds the claim number ListBox.
        /// </summary>
        /// <param name="p_sClaimNumbers">The P_S claim numbers.</param>
        private void BindClaimNumberListBox(string p_sClaimNumbers)
        {
            if (!string.IsNullOrWhiteSpace(p_sClaimNumbers))
            {
                lstClaimNumber.Items.Clear();
                foreach (String sClaimNumber in p_sClaimNumbers.Split(','))
                {
                    lstClaimNumber.Items.Add(sClaimNumber);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnSave_Click(object sender, EventArgs e) 
        {
            SaveSetting();
            //For Rebinding the Listbox with Selected Claim Numbers in case of PostBack.
            BindClaimNumberListBox(lstClaimNumber_Ids.Text);
        }

        /// <summary>
        /// Saves the setting.
        /// </summary>
        private void SaveSetting() 
        {
            string sTaskManagerXml = string.Empty;
            DataIntegratorModel objModel = new DataIntegratorModel();
            
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            bool bErr = false;
            string sCurrentTime = Convert.ToString(DateTime.Now.TimeOfDay);
            try
            {

                sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetIDParam = Conversion.ConvertObjToInt(hdOptionsetId.Value);

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }

                if (string.IsNullOrWhiteSpace(txtOptionSetName.Text))
                {
                    err.Add("DataIntegrator.Error", "Optionset Name cannot be blank. ", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }


                if (lstSelectedStates.Items.Count == 0)
                {
                    err.Add("DataIntegrator.Error", "Jurisdiction State is required.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                if (!string.IsNullOrWhiteSpace(txtFromDate.Value) && !string.IsNullOrWhiteSpace(txtToDate.Value))
                {
                    DateTime FromDate = DateTime.ParseExact(txtFromDate.Value, "d", null);
                    DateTime ToDate = DateTime.ParseExact(txtToDate.Value, "d", null);

                    if(DateTime.Compare(FromDate, ToDate) > 0)
                    {
                        err.Add("DataIntegrator.Error", "From date cannot be later than To date. ", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                }

                if (Convert.ToInt32(ddlTPInterfaceType.SelectedValue) == 0)
                {
                    err.Add("DataIntegrator.Error", "Thrid Party Interface Type is required.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }

                if (bErr == true)
                {
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                Dictionary<string, string> DCI_Setting = new Dictionary<string, string>();

                objModel.OptionSetName = txtOptionSetName.Text;

                DCI_Setting.Add("DCI_CARRIER_CODE", txtCarrierCode.Text);

                if (!string.IsNullOrWhiteSpace(txtFromDate.Value))
                    DCI_Setting.Add("CLAIM_DATE_RPTD_FROM", Conversion.GetDate(txtFromDate.Value));
                else if (!string.IsNullOrWhiteSpace(txtLastRunDate.Text))
                    DCI_Setting.Add("CLAIM_DATE_RPTD_FROM", Conversion.GetDate(txtLastRunDate.Text));
                else
                    DCI_Setting.Add("CLAIM_DATE_RPTD_FROM", Conversion.GetDate("19000101"));
                                        
                if (!string.IsNullOrWhiteSpace(txtToDate.Value))
                    DCI_Setting.Add("CLAIM_DATE_RPTD_TO", Conversion.GetDate(txtToDate.Value));
                else
                    DCI_Setting.Add("CLAIM_DATE_RPTD_TO", Conversion.GetDate(DateTime.Now.Date.ToShortDateString()));

                DCI_Setting.Add("JURISDICTION_STATES", GenerateCommaSepratedID(lstSelectedStates));
                DCI_Setting.Add("CLAIM_TYPES", GenerateCommaSepratedID(lstSelectedClaimType));
                DCI_Setting.Add("CLAIM_NUM", lstClaimNumber_Ids.Text);
                DCI_Setting.Add("EMAIL_NOTICE", txtEmailNotification.Text);
                DCI_Setting.Add("TP_INTERFACE_TYPE", ddlTPInterfaceType.SelectedValue);

                if (chkTestRun.Checked)
                    DCI_Setting.Add("TEST_RUN", "-1");
                else
                    DCI_Setting.Add("TEST_RUN", "0");


                objModel.Parms = DCI_Setting;
                objModel.OptionSetID = iOptionSetIDParam;
                objModel.ModuleName = sModuleName;

                objModel.TaskManagerXml = sTaskManagerXml;
                objModel.ClientId = AppHelper.ClientId;
                objModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);

                if (objModel.OptionSetID == -1)
                {
                    err.Add("DataIntagrator.Error", "OptionSet Name already exists.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }

                if (objModel.OptionSetID > 0)
                {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                }
            }
            catch (Exception ex)
            {
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                try
                {
                    if (iOptionSetIDParam <= 0 && objModel.OptionSetID > 0)
                    {
                        objModel.ClientId = AppHelper.ClientId;
                        AppHelper.GetResponse("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                    }
                }
                catch (Exception eCleanup)
                {
                }
                return;
            }
        }

        /// <summary>
        /// Adds the selected option.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void AddSelectedOption(object sender, EventArgs e)
        {
            ListBox lstAllOption = null;
            ListBox lstUserSelection = null;
            bool bAddSelectedState = false;
            List<ListItem> lstTemp = new List<ListItem>();

            if (((Button)sender).ID == "btnAddState")
            {
                lstAllOption = (ListBox)lstAllStates;
                lstUserSelection = (ListBox)lstSelectedStates;
            }
            else if (((Button)sender).ID == "btnAddClaim")
            {
                lstAllOption = (ListBox)lstClaimType;
                lstUserSelection = (ListBox)lstSelectedClaimType;
            }

            foreach (ListItem li in lstAllOption.Items)
            {
                if (li.Selected)
                {
                    if (lstUserSelection.Items.IndexOf(li) == -1)
                    {
                        lstUserSelection.Items.Add(li);
                        lstTemp.Add(li);
                        bAddSelectedState = true;
                    }
                }
            }

            if (bAddSelectedState)
            {
                foreach (ListItem li in lstTemp)
                {
                    lstAllOption.Items.Remove(li);
                }
                SortListBox(lstUserSelection);
            }

            lstAllOption.ClearSelection();
            lstUserSelection.ClearSelection();
        }

        /// <summary>
        /// Removes the selected option.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void RemoveSelectedOption(object sender, EventArgs e)
        {
            ListBox lstAllOption = null;
            ListBox lstUserSelection = null;
            bool bAddSelectedState = false;
            List<ListItem> lstTemp = new List<ListItem>();

            if (((Button)sender).ID == "btnRemoveState")
            {
                lstAllOption = (ListBox)lstAllStates;
                lstUserSelection = (ListBox)lstSelectedStates;
            }
            else if (((Button)sender).ID == "btnRemoveClaim")
            {
                lstAllOption = (ListBox)lstClaimType;
                lstUserSelection = (ListBox)lstSelectedClaimType;
            }

            foreach (ListItem li in lstUserSelection.Items)
            {
                if (li.Selected)
                {
                    lstAllOption.Items.Add(li);
                    lstTemp.Add(li);
                    bAddSelectedState = true;
                }
            }

            foreach (ListItem li in lstTemp)
            {
                lstUserSelection.Items.Remove(li);
            }
            if (bAddSelectedState)
                SortListBox(lstAllOption);
            lstAllOption.ClearSelection();
            lstUserSelection.ClearSelection();
        }

        /// <summary>
        /// Sorts the ListBox.
        /// </summary>
        /// <param name="lbSortList">The lb sort list.</param>
        private void SortListBox(ListBox lbSortList)
        {
            var list = lbSortList.Items.Cast<ListItem>().OrderBy(item => item.Text).ToList();
            lbSortList.Items.Clear();
            foreach (ListItem listItem in list)
            {
                lbSortList.Items.Add(listItem);
            }
        }

        /// <summary>
        /// Generates the comma seprated identifier.
        /// </summary>
        /// <param name="lbControl">The lb control.</param>
        /// <returns></returns>
        private string GenerateCommaSepratedID(ListBox lbControl)
        {
            string sReturn = string.Empty;

            if (lbControl.Items.Count > 0)
            {
                foreach (ListItem itemSelectedItem in lbControl.Items)
                {
                    if (!String.IsNullOrEmpty(sReturn))
                        sReturn = sReturn + "," + itemSelectedItem.Value.ToString();
                    else
                        sReturn = itemSelectedItem.Value.ToString();
                }
            }

            return sReturn;
        }

        /// <summary>
        /// Populates the control.
        /// </summary>
        /// <param name="p_sControlValue">The P_S control value.</param>
        /// <param name="sControlName">Name of the s control.</param>
        private void PopulateControl(string p_sControlValue, string sControlName)
        {
            ListBox lstAllOption = null;
            ListBox lstUserSelection = null;

            switch (sControlName)
            {
                case "JuriState":
                    lstAllOption = (ListBox)lstAllStates;
                    lstUserSelection = (ListBox)lstSelectedStates;
                    break;
                case "ClaimType":
                    lstAllOption = (ListBox)lstClaimType;
                    lstUserSelection = (ListBox)lstSelectedClaimType;
                    break;
            }

            if (!string.IsNullOrWhiteSpace(p_sControlValue))
            {
                foreach (string str in p_sControlValue.Split(','))
                {
                    if (!string.IsNullOrEmpty(str))
                    {
                        ListItem liAdd = lstAllOption.Items.FindByValue(str);
                        lstUserSelection.Items.Add(liAdd);
                    }
                }
                SortListBox(lstUserSelection);

                foreach (ListItem li in lstUserSelection.Items)
                {
                    lstAllOption.Items.Remove(li);
                }

                lstAllOption.ClearSelection();
                lstUserSelection.ClearSelection();
                SortListBox(lstAllOption);
            }
        }

        /// <summary>
        /// Adds all option.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void AddAllOption(object sender, EventArgs e)
        {
            ListBox lstAllOption = null;
            ListBox lstUserSelection = null;
            bool bAddSelectedState = false;
            List<ListItem> lstTemp = new List<ListItem>();

            if (((Button)sender).ID == "btnAddAllState")
            {
                lstAllOption = (ListBox)lstAllStates;
                lstUserSelection = (ListBox)lstSelectedStates;
            }
            else if (((Button)sender).ID == "btnAddAllClaim")
            {
                lstAllOption = (ListBox)lstClaimType;
                lstUserSelection = (ListBox)lstSelectedClaimType;
            }

            foreach (ListItem li in lstAllOption.Items)
            {
                if (lstUserSelection.Items.IndexOf(li) == -1)
                {
                    lstUserSelection.Items.Add(li);
                    lstTemp.Add(li);
                    bAddSelectedState = true;
                }
            }

            if (bAddSelectedState)
            {
                foreach (ListItem li in lstTemp)
                {
                    lstAllOption.Items.Remove(li);
                }
                SortListBox(lstUserSelection);
            }

            lstAllOption.ClearSelection();
            lstUserSelection.ClearSelection();
        }

        /// <summary>
        /// Removes all option.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void RemoveAllOption(object sender, EventArgs e)
        {
            ListBox lstAllOption = null;
            ListBox lstUserSelection = null;
            bool bAddSelectedState = false;
            List<ListItem> lstTemp = new List<ListItem>();

            if (((Button)sender).ID == "btnRemoveAllState")
            {
                lstAllOption = (ListBox)lstAllStates;
                lstUserSelection = (ListBox)lstSelectedStates;
            }
            else if (((Button)sender).ID == "btnRemoveAllClaim")
            {
                lstAllOption = (ListBox)lstClaimType;
                lstUserSelection = (ListBox)lstSelectedClaimType;
            }

            foreach (ListItem li in lstUserSelection.Items)
            {
                lstAllOption.Items.Add(li);
                lstTemp.Add(li);
                bAddSelectedState = true;
            }

            foreach (ListItem li in lstTemp)
            {
                lstUserSelection.Items.Remove(li);
            }

            if (bAddSelectedState)
                SortListBox(lstAllOption);
            lstAllOption.ClearSelection();
            lstUserSelection.ClearSelection();
        }

    }
}