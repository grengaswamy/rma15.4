﻿<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-4042         | ajohari2   | Underwriters - EFT Payments Part 2
 **********************************************************************************************/--%>

<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="TMSettingsWeekly.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.TMSettingsWeekly" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc2" TagName="UserControlGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register Src="../../../Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc1" %>
<%@ Register Src="../../../Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Schedule a Task</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>

    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>

    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/TMSettings.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>

    <script language="javascript" type="text/javascript">

        function removeOption(selectName, id) {
            select = document.getElementById(selectName);
            selecttxtObject = document.getElementById('txtRelatedComponents');
            Ids = new Array();
            Names = new Array();

            for (var x = 0; x < select.options.length; x++) {
                if (select.options[x].value != id) {
                    Ids[Ids.length] = select.options[x].value;
                    Names[Names.length] = select.options[x].text;

                }
            }

            select.options.length = Ids.length;
            if (selectName == 'lstRelatedLossComponents') {
                selecttxtObject.value = ""
            }
            for (var x = 0; x < select.options.length; x++) {
                select.options[x].text = Names[x];
                select.options[x].value = Ids[x];
                if (selectName == 'lstRelatedLossComponents') {
                    if (selecttxtObject.value == "") {
                        selecttxtObject.value = Ids[x];
                    }
                    else {
                        selecttxtObject.value = selecttxtObject.value + ',' + Ids[x];
                    }
                }
            }
        }

        function removeOptionCType(selectName, id) {
            select = document.getElementById(selectName);
            selecttxtObject = document.getElementById('txtRelatedCTypeComponents');
            Ids = new Array();
            Names = new Array();

            for (var x = 0; x < select.options.length; x++) {
                if (select.options[x].value != id) {
                    Ids[Ids.length] = select.options[x].value;
                    Names[Names.length] = select.options[x].text;

                }
            }

            select.options.length = Ids.length;
            if (selectName == 'lstRelatedCTypeComponents') {
                selecttxtObject.value = ""
            }
            for (var x = 0; x < select.options.length; x++) {
                select.options[x].text = Names[x];
                select.options[x].value = Ids[x];
                if (selectName == 'lstRelatedCTypeComponents') {
                    if (selecttxtObject.value == "") {
                        selecttxtObject.value = Ids[x];
                    }
                    else {
                        selecttxtObject.value = selecttxtObject.value + ',' + Ids[x];
                    }
                }
            }
        }

        function AddFilter(mode) {
            var optionRank;
            var optionObject;

            selectObject = document.getElementById('lstRelatedLossComponents');
            selecttxtObject = document.getElementById('txtRelatedComponents');

            if (mode == "selected") {

                //Add selected Available Values
                select = document.getElementById('lstAvailableLossComponents');
                for (var x = select.options.length - 1; x >= 0 ; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);

                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        //remove from available list	
                        removeOption('lstAvailableLossComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //Add All Available Values
                select = document.getElementById('lstAvailableLossComponents');

                if (select.options.length > 0) {
                    for (var x = select.options.length - 1; x >= 0; x--) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        selectObject.options[optionRank] = optionObject;
                        setDataChanged(true);
                    }
                    select.options.length = 0;
                }
            }
            return false;
        }

        function AddFilterCType(mode) {
            var optionRank;
            var optionObject;

            selectObject = document.getElementById('lstRelatedCTypeComponents');
            selecttxtObject = document.getElementById('txtRelatedCTypeComponents');

            if (mode == "selected") {

                //Add selected Available Values
                select = document.getElementById('lstAvailableCTypeComponents');
                for (var x = select.options.length - 1; x >= 0 ; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);

                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        //remove from available list	
                        removeOptionCType('lstAvailableCTypeComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //Add All Available Values
                select = document.getElementById('lstAvailableCTypeComponents');

                if (select.options.length > 0) {
                    for (var x = select.options.length - 1; x >= 0; x--) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        if (selecttxtObject.value == "") {
                            selecttxtObject.value = select.options[x].value;
                        }
                        else {
                            selecttxtObject.value = selecttxtObject.value + ',' + select.options[x].value;
                        }
                        selectObject.options[optionRank] = optionObject;
                        setDataChanged(true);
                    }
                    select.options.length = 0;
                }
            }
            return false;
        }


        function RemoveFilter(mode) {
            var optionRank;
            var optionObject;
            selectObject = document.getElementById('lstAvailableLossComponents');
            selecttxtObject = document.getElementById('txtRelatedComponents');
            if (mode == "selected") {
                //Add selected Available Values
                select = document.getElementById('lstRelatedLossComponents');
                for (var x = select.options.length - 1; x >= 0; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;

                        //remove from available list	
                        removeOption('lstRelatedLossComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //ADD All Available Values
                select = document.getElementById('lstRelatedLossComponents');

                if (select.options.length > 0) {

                    for (var x = select.options.length - 1; x >= 0; x--) {

                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;


                        selectObject.options[optionRank] = optionObject;

                        selecttxtObject.value = "";

                        setDataChanged(true);


                    }
                    select.options.length = 0;
                }
            }
            return false;
        }

        function RemoveFilterCType(mode) {
            var optionRank;
            var optionObject;
            selectObject = document.getElementById('lstAvailableCTypeComponents');
            selecttxtObject = document.getElementById('txtRelatedCTypeComponents');
            if (mode == "selected") {
                //Add selected Available Values
                select = document.getElementById('lstRelatedCTypeComponents');
                for (var x = select.options.length - 1; x >= 0; x--) {
                    if (select.options[x].selected == true) {
                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;
                        selectObject.options[optionRank] = optionObject;

                        //remove from available list	
                        removeOptionCType('lstRelatedCTypeComponents', select.options[x].value);
                        setDataChanged(true);
                    }
                }
            }
            else {
                //ADD All Available Values
                select = document.getElementById('lstRelatedCTypeComponents');

                if (select.options.length > 0) {

                    for (var x = select.options.length - 1; x >= 0; x--) {

                        //add to selected list
                        optionObject = new Option(select.options[x].text, select.options[x].value);
                        optionRank = selectObject.options.length;


                        selectObject.options[optionRank] = optionObject;

                        selecttxtObject.value = "";

                        setDataChanged(true);


                    }
                    select.options.length = 0;
                }
            }
            return false;
        }

        //asharma326 MITS 32386 FAS starts
        function ShowHidefFASdiv() {
            if (document.getElementById('txtFASFileLocation') != null) {
                var SelectedOption = document.getElementById('txtFASFileLocation').value;
                if ((SelectedOption != null) && (SelectedOption != "")) {
                    if (SelectedOption == "N") {
                        document.getElementById('tbodyFASNonediv').style.display = 'block';
                    }
                    else if (SelectedOption == "F") {
                        document.getElementById('tbodyFASSettings').style.display = 'block';
                    }
                    else if (SelectedOption == "S") {
                        document.getElementById('tbodyFASSharedDiv').style.display = 'block';
                    }
                }
            }
        }
        //asharma326 MITS 32386 FAS Ends
    </script>

    <style type="text/css">
        .auto-style1 {
            width: 199px;
        }

        .auto-style2 {
            width: 195px;
        }

        .auto-style3 {
            width: 285px;
        }

        .auto-style4 {
            width: 78px;
        }

        .auto-style5 {
            width: 63px;
        }
    </style>
</head>
<body onload="onTMLoaded();ShowHidefFASdiv();">
    <form id="frmData" runat="server">
        <!--nsachdeva2 - MITS:26428 - 12/28/2011 -->
        <input type="hidden" id="hdnId" runat="server" />
        <input type="hidden" id="hdnPrintSettings" runat="server" />
        <input type="hidden" id="hdnDeteleSetting" runat="server" />
        <input type="hidden" id="hdnRepeat" runat="server" />
        <!--End - MITS:26428 - 12/28/2011 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                <td colspan="2">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="msgheader" colspan="4">
                    <!--Yukti -Ml Changes Start -->
                    <!-- Schedule a Task-->
                    <asp:Label ID="lblScheduleTask" runat="Server" Text="<%$ Resources:lblScheduleTask %>" />
                    <!--Yukti -Ml Changes End -->
                </td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <!-- Shruti starts-->
            <tr>
                <td align="left" width="20%">
                    <!--Yukti - ML Changes Start-->
                    <!--Task Type:-->
                    <asp:Label ID="lblTaskType" runat="server" Text="<%$ Resources:lblTaskType %>" />
                    <!-- yukti ML Changes End-->
                </td>
                <td width="80%" align="left">
                    <b>
                        <asp:Label ID="lblTaskTypeText" runat="server" rmxref="/Instance/Document/Details/TaskTypeText" />
                    </b>
                </td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <tr>
                <td align="left" width="20%">
                    <!-- Yukti - ML Chnages Start-->
                    <!--Task Name:-->
                    <asp:Label ID="lblTaskNam1" runat="Server" Text="<%$ Resources:lblTaskNam1 %>" />
                    <!--Yukti-ML Changes End -->
                </td>
                <td width="80%" align="left">
                    <b>
                        <asp:Label ID="lblTaskName" runat="server" rmxref="/Instance/Document/Details/TaskNameLabel" />
                    </b>
                </td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <%if (hdnaction.Text == "EditWeekly")
              { %>
            <tr>
                <td align="left">
                    <!--Yukti ML Changes Start-->
                    <!--Schedule Type:-->
                    <asp:Label ID="lblSchedType" runat="server" Text="<%$ Resources:lblSchedType %>" />
                    <!--Yukti ML Changes End-->
                </td>
                <td>
                    <asp:DropDownList ID="ScheduleType" runat="server" rmxref="/Instance/Document/Details/ScheduleTypeId" ItemSetRef="/Instance/Document/Details/ScheduleType" onchange="return ScheduleTypeChanged();"></asp:DropDownList>
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <%}
              else
              { %>
            <tr>
                <td align="left">
                    <!-- Yukti ML Changes Start-->
                    <!-- Schedule Type:-->
                    <asp:Label ID="lblScheduleType" runat="server" Text="<%$ Resources:lblSchedType%>" />
                    <!--Yukti ML Changes End-->
                </td>
                <td align="left">
                    <b>
                        <asp:Label ID="lblScheduleTypeText" runat="server" rmxref="/Instance/Document/Details/ScheduleTypeText" />
                    </b>
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <%} %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
            <!--Yukti-ML Changes Start-->
                    <!-- Enter Start Date and Time:-->
                    <asp:Label runat="server" ID="lblDateTime" Text="<%$ Resources:lblDateTime %>" />
                    <!--Yukti-ML Changes End-->
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr>
                <td align="left">
                    <!--Yukti-ML Changes Start-->
                    <%--<u>Date</u>:--%>
                    <asp:Label runat="server" ID="lblDate" Font-Underline="True" Text="<%$ Resources:lblDate %>" />
                </td>
                <td>
                    <%--<asp:TextBox runat="server" FormatAs="date" ID="txtDate" RMXRef="/Instance/Document/Details/Date"
                    RMXType="date" onblur="dateLostFocus(this.id);"/>
                <input type="button" class="DateLookupControl" name="datebtn" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					    {
					        inputField: "txtDate",
					        ifFormat: "%m/%d/%Y",
					        button: "datebtn"
					    }
					    );
                </script>--%>
                    <asp:TextBox runat="server" RMXRef="/Instance/Document/Details/Date"
                        onblur="dateLostFocus(this.id);"
                        ID="txtDate" />
                    <%--<asp:Button runat="server" class="DateLookupControl" ID="datebtn"/>

                                            <script type="text/javascript">
                                                Zapatec.Calendar.setup(
											{
											    inputField: "txtDate",
											    ifFormat: "%m/%d/%Y",
											    button: "datebtn"
											}
											);
                                            </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#txtDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../../Images/calendar.gif",
				            //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
				        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>

                </td>
                <td>&#160;&#160;
                </td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <tr>
                <td align="left">
                    <!--Yukti-ML Changes Start-->
                    <!--<u>Time</u>:-->
                    <asp:Label runat="server" ID="lblTime" Font-Underline="true" Text="<%$ Resources:lblTime %>" />
                    <!--Yukti-ML Changes End-->
                </td>
                <td>
                    <asp:TextBox runat="server" size="12" formatas="time" onchange="setDataChanged(true);"
                        ID="txtTime" rmxref="/Instance/Document/Details/Time" rmxtype="time" onblur="timeLostFocus(this.id);" />
                </td>
                <td>&#160;&#160;
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
            <!--Yukti-ML Changes Start-->
                    <!-- Select the days you would like to Run the Task on:-->
                    <asp:Label ID="lblDaysSelected" runat="Server" Text="<%$ Resources:lblDaysSelected %>" />
                    <!--Yukti-ML Changes End-->
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr>
                <td align="left">&#160;
                </td>
                <td>
                    <!--Yukti-ML Changes Start-->
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkMon_Run" rmxref="/Instance/Document/Details/Mon_Run"
                        rmxtype="checkbox" Text="<%$ Resources:chkMonday%>" />
                    <!--Yukti-ML Changes End-->
                </td>
            </tr>
            <tr>
                <td align="left">&#160;
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkTue_Run" rmxref="/Instance/Document/Details/Tue_Run"
                        rmxtype="checkbox" Text="<%$ Resources:chkTuesday %>" />
                </td>
            </tr>
            <tr>
                <td align="left">&#160;
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkWed_Run" rmxref="/Instance/Document/Details/Wed_Run"
                        rmxtype="checkbox" Text="<%$ Resources:chkWednesday %>" />
                </td>
            </tr>
            <tr>
                <td align="left">&#160;
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkThu_Run" rmxref="/Instance/Document/Details/Thu_Run"
                        rmxtype="checkbox" Text="<%$ Resources:chkThursday%>" />
                </td>
            </tr>
            <tr>
                <td align="left">&#160;
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkFri_Run" rmxref="/Instance/Document/Details/Fri_Run"
                        rmxtype="checkbox" Text="<%$ Resources:chkFriday %>" />
                </td>
            </tr>
            <tr>
                <td align="left">&#160;
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkSat_Run" rmxref="/Instance/Document/Details/Sat_Run"
                        rmxtype="checkbox" Text="<%$ Resources:chkSaturday %>" />
                </td>
            </tr>
            <tr>
                <td align="left">&#160;
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkSun_Run" rmxref="/Instance/Document/Details/Sun_Run"
                        rmxtype="checkbox" Text="<%$ Resources:chkSunday %>" />
                </td>
            </tr>
            <tr>
                <td align="left" id="tdAdditionalParams">
                    <!--Yukti-ML Changes Start-->
                    <!--Additional Parameters :-->
                    <asp:Label runat="server" ID="lblAddParameters" Text="<%$ Resources:lblAddParameters %>" />
                    <!--Yukti-ML Changes End-->
                </td>
                <td>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkAdd_Param" rmxref="/Instance/Document/Details/bParams"
                        rmxtype="checkbox" onclick="AddParams(this);" />
                </td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <tr>
                <td align="left" id="tdArgLabel">
                    <!--Yukti-Ml Changes Start-->
                    <!--Arguments:-->
                    <asp:Label runat="server" ID="lblArguments" Text="<%$ Resources:lblArguments %>" />
                    <!--Yukti-ML Changes End-->
                </td>
                <td>
                    <asp:TextBox ID="txtArgs" runat="server" rmxref="/Instance/Document/Details/Arguments" size="50" />
                </td>
                <td>&#160;&#160;
                </td>
            </tr>
            <%if (hdTaskType.Text == "2")
              { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
            <!--Yukti-ML Changes Start-->
                    <!-- Financial History Related Parameters:-->
                    <asp:Label runat="server" ID="lblFinRelatedParams" Text="<%$ Resources:lblFinRelatedParams %>" />
                    <!--Yukti-ML Changes End-->
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <!--Yukti-ML Changes Start-->
                    <%-- <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkZBFinHist_Param"
                    rmxref="/Instance/Document/Details/ZeroBasedFinHist" rmxtype="checkbox" onclick="CheckFinHistParams();"
                    Text="Create Zero-Based Financial History" />--%>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkZBFinHist_Param"
                        rmxref="/Instance/Document/Details/ZeroBasedFinHist" rmxtype="checkbox" onclick="CheckFinHistParams();"
                        Text="<%$Resources:chkCreateZeroFinHist %>" />
                    <!--Yukti-ML Changes End-->
                </td>
                <td></td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <tr id="trZeroBasedOptions">
                <td>
                    <!--Yukti-ML Changes Start-->
                    <%-- <asp:RadioButton ID="optClaimBasedFinHist" runat="server" onclick="SelectClaimBasedFinHist();"
                    rmxref="/Instance/Document/Details/ClaimBasedFinHist" Text="Based On Date Of Claim" value="1" />--%>
                    <asp:RadioButton ID="optClaimBasedFinHist" runat="server" onclick="SelectClaimBasedFinHist();"
                        rmxref="/Instance/Document/Details/ClaimBasedFinHist" Text="<%$ Resources:rbtnClaimDateBased %>" value="1" />
                    <!--Yukti-ML Changes End-->
                </td>
                <td>
                    <!--Yukti-ML Changes Start-->
                    <%--<asp:RadioButton ID="optEventBasedFinHist" runat="server" onclick="SelectEventBasedFinHist();"
                    rmxref="/Instance/Document/Details/EventBasedFinHist" Text="Based On Date Of Event" value="2" />--%>
                    <asp:RadioButton ID="optEventBasedFinHist" runat="server" onclick="SelectEventBasedFinHist();"
                        rmxref="/Instance/Document/Details/EventBasedFinHist" Text="<%$ Resources:rbtnEventDateBased %>" value="2" />
                </td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <%-- <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkRecFinHist_Param"
                    rmxref="/Instance/Document/Details/RecreateFinHist" rmxtype="checkbox" Text="Recreate Financial History" />--%>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkRecFinHist_Param"
                        rmxref="/Instance/Document/Details/RecreateFinHist" rmxtype="checkbox" Text="<%$ Resources:chkRecFinHist %>" />
                </td>
                <td></td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>

            <%--akaushik5 Commented for MITS 33420 Starts--%>
            <%-- tkatsarski: 03/26/15 RMA-8219: Comment sequence string (< % - -) was causing issues displaying the page --%>
            <%--<tr>
            <td colspan="2">
                <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkCreateLog_Param"
                    rmxref="/Instance/Document/Details/CreateLog" rmxtype="checkbox" Text="Generate Log in user's temp directory" />
            <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkCreateLog_Param"
                rmxref="/Instance/Document/Details/CreateLog" rmxtype="checkbox" Text="<%$ Resources:chkGenLogDir %>" />
            </td>
            <td></td>
            </tr>--%>
        <%--akaushik5 Commented for MITS 33420 Ends--%>
            <%} %>
            <%if (hdTaskType.Text == "3")
              { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
                <!--Billing Scheduler Related Parameter:-->
                    <asp:Label runat="server" ID="lblBillingSchedParam" Text="<%$ Resources:lblBillingSchedParam %>" />
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr>
                <td align="left">
                    <!--Yukti-ML Changes Start-->
                    <!--<u>Generate</u>:-->
                    <asp:Label runat="server" ID="lblGenerate" Font-Underline="True" Text="<%$ Resources:lblGenerate %>" />
                    <!--Yukti-ML Changes End-->
                </td>
                <td colspan="2">
                    <!--Yukti-ML Changes Start-->
                    <asp:DropDownList ID="BillOption" runat="server" rmxref="/Instance/Document/Details/BillingOption">
                        <asp:ListItem Value="Installment" Text="<%$ Resources:liInstallment %>"></asp:ListItem>
                        <asp:ListItem Value="Invoice" Text="<%$ Resources:liInvoice %>"></asp:ListItem>
                        <asp:ListItem Value="Notice" Text="<%$ Resources:liNotice %>"></asp:ListItem>
                    </asp:DropDownList>
                    <!--Yukti-ML Changes End-->
                </td>
                <td>
                    <td></td>
            </tr>
            <%} %>
            <%if (hdSystemModuleName.Text == "ProcessOverDueDiary")
              { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
            <!--Yukti-ML Changes Start-->
                    <!-- Manually Assigned OverDue Diary Processing Related Parameters:-->
                    <asp:Label runat="server" ID="lblDiaryOverDue" Text="<%$ Resources:lblDiaryOverDue %>" />
                    <!--Yukti-Ml Changes End-->
                </td>
            </tr>

            <tr>
                <td align="left">
                    <!--Yukti-ML Changes Start-->
                    <!--After How many Days the overdue-->
                    <asp:Label runat="server" ID="lblDaysOverdue" Text="<%$ Resources:lblDaysOverdue %>" />
                    <br />
                    <!--Diary Notification should be sent:-->
                    <asp:Label runat="server" ID="lblDiaryNotification" Text="<%$ Resources:lblDiaryNotification %>" />
                    <br />
                    <!--(Enter -1 to set Due date to the same date)-->
                    <asp:Label runat="server" ID="lblEnterDueDate" Text="<%$ Resources:lblEnterDueDate %>" />
                </td>
                <td>
                    <asp:TextBox ID="tbNoOfOverDueDays" runat="server" rmxref="/Instance/Document/Details/OverDueDays"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <!--Yukti-ML Chnages Start-->
                    <%--The OverDue Diary Notification--%>
                    <asp:Label runat="server" ID="lblDiaryOverDueNote" Text="<%$ Resources:lblDiaryOverDueNote %>" />
                    <br />
                    <%--Should be sent as:--%>
                    <asp:Label runat="server" ID="lblSent" Text="<%$ Resources:lblSent %>" />
                </td>


                <td colspan="2">
                    <!--Yukti-ML Changes Start-->
                    <%--<asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkSysDiary"
                    rmxref="/Instance/Document/Details/CreateSysDiary" rmxtype="checkbox" Text="System Diary" onclick="onCheckChanged(this)" />--%>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkSysDiary"
                        rmxref="/Instance/Document/Details/CreateSysDiary" rmxtype="checkbox" Text="<%$ Resources:chkCreateSysDiary %>" onclick="onCheckChanged(this)" />
                    <!--Yukti-ML Changes End-->

                    <br />
                    <!--Yukti-ML Changes Start-->

                    <%--                            <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkEmailNotify"
                    rmxref="/Instance/Document/Details/SendEmailNotify" rmxtype="checkbox" Text="Email Notification"  onclick="onCheckChanged(this)"/>--%>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkEmailNotify"
                        rmxref="/Instance/Document/Details/SendEmailNotify" rmxtype="checkbox" Text="<%$ Resources:chkEmailNotification %>" onclick="onCheckChanged(this)" />
                    <!--Yukti-ML Changes End-->
                    <br />
                    <!--Yukti-ML Changes Start-->

                    <%--<asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkBoth"
                    rmxref="/Instance/Document/Details/BothDiaryAndEmail" rmxtype="checkbox" Text="Both" onclick="onCheckChanged(this)"/>--%>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="chkBoth"
                        rmxref="/Instance/Document/Details/BothDiaryAndEmail" rmxtype="checkbox" Text="<%$ Resources:chkBoth %>" onclick="onCheckChanged(this)" />
                    <!--Yukti-ML Changes End-->
                    <br />




                </td>
                <td></td>
            </tr>
            <%} %>

            <%if (hdSystemModuleName.Text == "PolicySystemUpdate")
              { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160; 
                <!--Yukti-ML Changes Start-->
                    <!--Policy System Financials Batch Update Related Parameters:-->
                    <asp:Label runat="server" ID="lblPolicySysFin" Text="<%$ Resources:lblPolicySysFin %>" />
                    <!--Yukti-ML Changes End-->
                </td>
            </tr>

            <tr>
                <td align="left">
                    <!--Yukti-ML changes Start-->
                    <%-- Select Policy System--%>
                    <asp:Label runat="server" ID="lblSelectPolicy" Text="<%$ Resources:lblSelectPolicy %>" />
                </td>
                <td>
                    <asp:DropDownList ID="PolicySystems" runat="server" rmxref="/Instance/Document/Details/PolicySystemList/PolicySystems" ItemSetRef="/Instance/Document/Details/PolicySystemList/PolicySystems"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <%--<u>Date</u>:--%>
                    <asp:Label runat="server" ID="lblPolDate" Text="<%$ Resources:lblDate %>" />
                    <br />
                    <%--**ksahu5 ML change**  Records before this date will be uploaded.--%>
                    <asp:Label runat="server" ID="lblPolRecUpload" Text="<%$ Resources:lblPolRecUpload %>" />
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="dtActivity" RMXRef="/Instance/Document/Details/dtActivity"
                        RMXType="date" onblur="dateLostFocus(this.id);" />
                    <%--  <input type="button" class="DateLookupControl" name="datebtnActivity" />

                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
                            {
                                inputField: "dtActivity",
                                ifFormat: "%m/%d/%Y",
                                button: "datebtnActivity"
                            }
                        );
                    </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#dtActivity").datepicker({
                                showOn: "button",
                                buttonImage: "../../../../Images/calendar.gif",
                                 //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>

                </td>
            </tr>
            <%} %>


            <%if (hdSystemModuleName.Text == "ClaimBalancing")
              { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160; 
                 <%--** ksahu5 ML change** Claim Balancing Related Parameters:--%>
                    <asp:Label runat="server" ID="lblClmParam" Text="<%$ Resources:lblClmParam %>" />
                </td>
            </tr>

            <tr>
                <td align="left">
                    <%--** ksahu5 ML change**  Select Policy System--%>
                    <asp:Label runat="server" ID="lblPolSys" Text="<%$ Resources:lblPolSys %>" />
                </td>
                <td>
                    <asp:DropDownList ID="PolicySystemsClaimsBalancing" runat="server" rmxref="/Instance/Document/Details/PolicySystemList/PolicySystemsClaimsBalancing" ItemSetRef="/Instance/Document/Details/PolicySystemList/PolicySystems"></asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td align="left">
                    <%--** ksahu5 ML change** Date of Claim--%>
                    <asp:Label runat="server" ID="lblDtClm" Text="<%$ Resources:lblDtClm %>" />
                    <br />
                    <%--  (Claims on/after this date will--%>
                    <asp:Label runat="server" ID="lblClmBal" Text="<%$ Resources:lblClmBal %>" />
                    <br />
                    <%--be balanced)--%>
                    <asp:Label runat="server" ID="lblClmBal1" Text="<%$ Resources:lblClmBal1 %>" />
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="txtClmBalanceDate" RMXRef="/Instance/Document/Details/ClmBalanceDate"
                        RMXType="date" onblur="dateLostFocus(this.id);" />
                    <%-- <input type="button" class="DateLookupControl" name="ClmBalanceDatebtn" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
                        {
                            inputField: "txtClmBalanceDate",
                            ifFormat: "%m/%d/%Y",
                            button: "ClmBalanceDatebtn"
                        }
                    );
                </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#txtClmBalanceDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../../../Images/calendar.gif",
                            // buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                         }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>

                </td>
            </tr>
            <tr>
                <td align="left">
                    <%--** ksahu5 ML change** Select Claim Type--%>
                    <asp:Label runat="server" ID="lblSelClmType" Text="<%$ Resources:lblSelClmType %>" />
                </td>
                <td>
                    <uc5:CodeLookUp runat="server" CodeTable="CLAIM_TYPE" ControlName="ClaimBalancingClaimType" ID="ClaimBalancingClaimType" RMXref="/Instance/Document/Details/ClaimBalance/ClaimType[@name = 'ClaimBalancingClaimType']" />
                </td>
            </tr>
            <tr>
                <td>
                    <%--<asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="SendExceltoEmail"
                rmxref="/Instance/Document/Details/SendEmail" rmxtype="checkbox" Text="Send the out of balance Report via Email"  onclick="onCheckChanged(this)"/>--%>
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="SendExceltoEmail"
                        rmxref="/Instance/Document/Details/SendEmail" rmxtype="checkbox" Text="<%$ Resources:chkSendExceltoEmail %>" onclick="onCheckChanged(this)" />
                </td>
            </tr>
            <%} %>

            <%--Ankit Start for Point Balancing --%>
            <%if (hdSystemModuleName.Text == "BES" || hdSystemModuleName.Text == "ClaimBalancing")
              { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
                    <!--BES Scheduler Related Parameters:-->
                    <asp:Label runat="server" ID="lblBESSchedParams" Text="<%$ Resources:lblBESSchedParams %>" />
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr>
                <td align="left">
                    <%-- <u>Admin User Id</u>:--%>
                    <asp:Label runat="server" ID="lblAdminUserId" Font-Underline="true" Text="<%$ Resources:lblAdminUserId %>" />
                </td>
                <td>
                    <asp:TextBox runat="server" size="12" onchange="setDataChanged(true);"
                        ID="txtLogin" rmxref="/Instance/Document/Details/AdminLogin" />
                </td>
                <td>&#160;&#160;
                </td>
            </tr>
            <tr>
                <td align="left">
                    <%--<u>Admin Password</u>:--%>
                    <asp:Label runat="server" ID="lblAdminPwd" Font-Underline="true" Text="<%$ Resources:lblAdminPwd %>" />
                </td>
                <td>
                    <asp:TextBox runat="server" size="12" onchange="setDataChanged(true);"
                        ID="txtPassword" TextMode="Password" rmxref="/Instance/Document/Details/AdminPassword" autocomplete="off" />
                </td>
                <td>&#160;&#160;
                </td>
            </tr>
            <tr height="10">
                <td></td>
            </tr>
            <%} %>
             <%--JIRA RMA-4606 nshah28 start--%>
            <% if (hdSystemModuleName.Text == "CurrencyExchangeInterface")
               { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160;
                    <asp:label id="lblFileSource" runat="server" text="<%$ Resources:lblFileSource %>"></asp:label>
                    <%--Choose File Source--%>
                </td>
            </tr>
            <tr height="20">
                <td>
                  
                </td>
            </tr>

            <tr>
                <td align="left" id="tdSharedFilePath">
                    <asp:label id="lblSharedFilePath" runat="server" text="<%$ Resources:lblSharedFilePath %>"></asp:label>
                    <%--Shared File Path--%>
                </td>
                <td>
                    <asp:radiobutton id="rdoSharedFilePath" runat="server" groupname="rdoFileSource" checked="true"  onclick="ShowHideControlsForFileSource(this);" rmxref="/Instance/Document/Details/FileSource" value="1"></asp:radiobutton>
                </td>
            </tr>

           <tr>
                <td align="left" id="tdFTP">
                    <asp:label id="lblFTP" runat="server" text="<%$ Resources:lblFTP %>"></asp:label>
                </td>
                <td>
                    <asp:radiobutton id="rdoFTP" runat="server" groupname="rdoFileSource"  onclick="ShowHideControlsForFileSource(this);" rmxref="/Instance/Document/Details/FileSource" value="2"></asp:radiobutton>
                </td>
            </tr>
        <tr></tr>
            <%--Section for Shared File Path--%>
           <div id="divSharedFilePath">
            <table id="tblSharedFilePath" width="100%">
                <tbody>
                   <tr width="100%" height="20">
                        <td class="ctrlgroup2" colspan="10">&#160;
                    <asp:label id="lblEnterSharedFilePath" runat="server" text="<%$ Resources:lblEnterSharedFilePath %>"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFilePath" runat="server" text="<%$ Resources:lblFilePath %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="50"
                                id="txtFilePath" rmxref="/Instance/Document/Details/FilePath" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFileName" runat="server" text="<%$ Resources:lblFileName %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20"
                                id="txtFileName" rmxref="/Instance/Document/Details/FileName" />
                        </td>
                    </tr>
                   <tr height="10">
                        <td class="auto-style8" />
                    </tr>
                    </tbody>
               </table>
           </div>
               
            <%--Section for FTP --%>
            <div id="divFTP" style="display: none">
                <table width="100%">
                    <tbody>
                    <tr width="100%" height="20">
                        <td class="ctrlgroup2" colspan="10">&#160;
                    <asp:label id="lblEnterFTP" runat="server" text="<%$ Resources:lblEnterFTP %>"></asp:label>
                        </td>
                    </tr>
                    <tr height="20">
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPServer" runat="server" text="<%$ Resources:lblFTPServer %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="50"
                                id="txtFTPServer" rmxref="/Instance/Document/Details/FTPServerName" />

                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPPort" runat="server" text="<%$ Resources:lblFTPPort %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20" onchange="setDataChanged(true);"
                                id="txtFTPPort" rmxref="/Instance/Document/Details/FTPPort" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPUserName" runat="server" text="<%$ Resources:lblFTPUserName %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20" onchange="setDataChanged(true);"
                                id="txtFTPUserName" rmxref="/Instance/Document/Details/FTPUserName" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPPass" runat="server" text="<%$ Resources:lblFTPPass %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20" onchange="setDataChanged(true);" type="text"
                                id="txtFTPPass" textmode="Password" rmxref="/Instance/Document/Details/FTPPassword" />
                        </td>
                    </tr>

                        <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPFilePath" runat="server" text="<%$ Resources:lblFTPFilePath %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="50"
                                id="txtFTPFilePath" rmxref="/Instance/Document/Details/FTPFilePath" />
                        </td>
                    </tr>

                         <tr>
                        <td align="left">
                            <u>
                                <asp:label id="lblFTPFileName" runat="server" text="<%$ Resources:lblFTPFileName %>"></asp:label>
                            </u>
                        </td>
                        <td>
                            <asp:textbox runat="server" size="20"
                                id="txtFTPFileName" rmxref="/Instance/Document/Details/FTPFileName" />
                        </td>
                    </tr>

                    <tr height="10">
                        <td class="auto-style8" />
                    </tr>
                        </tbody>
                </table>
            </div>
            <br /> <br />
            <% } %>
            <%--JIRA RMA-4606 nshah28 End--%>
            <% if (hdSystemModuleName.Text == "PrintBatchFroi")
               { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160; 
                <!--Yukti-ML Changes Start-->
                    <%-- FROI/ACORD Batch Print Related Parameters:--%>
                    <asp:Label runat="server" ID="lblFROIAcord" Text="<%$ Resources:lblFROIAcord %>" />
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr>
                <td colspan="2">&#160; 
             <!--Yukti-Ml Changes Start-->
                    <%-- Select for FROI: --%>
                    <asp:Label runat="server" ID="lblSelectFROI" Text="<%$ Resources:lblSelectFROI %>" />
                    <!--Yukti-ML Changes End-->
                    &#160;&#160;
                <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="CheckBox1"
                    rmxref="/Instance/Document/Details/IsFroiBatch" rmxtype="checkbox" />
                </td>
                <td>
                    <td></td>
            </tr>
            <tr>
                <td colspan="2">&#160; 
            <!--Yukti-ML Changes Start-->
                    <%--Select for ACORD:--%>
                    <asp:Label runat="server" ID="lblSelectACORD" Text="<%$ Resources:lblSelectACORD %>" />
                    <asp:CheckBox runat="server" onchange="ApplyBool(this);" ID="CheckBox2"
                        rmxref="/Instance/Document/Details/IsAcordBatch" rmxtype="checkbox" />
                </td>
                <td></td>
            </tr>
            <%} %><!-- rsushilaggar modified the code to fix the issue-->
            <!--nsachdeva2 - MITS:26428 - 12/28/2011 -->
            <% if (hdSystemModuleName.Text == "PrintCheckBatch")
               { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="10">&#160; 
                <!--Yukti-ML Changes Start-->
                    <%--Print Check Batch Related Parameters:--%>
                    <asp:Label runat="server" ID="lblPrintCheck" Text="<%$ Resources:lblPrintCheck %>" />
                    <!--Yukti-ML Changes End-->
                </td>
            </tr>
            <tr height="20">
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divGrid" style="overflow: auto; height: 470px">
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" Width="100%" AutoGenerateColumns="false" OnRowDataBound="GridView1_RowDataBound"
                            GridLines="None">
                            <HeaderStyle CssClass="msgheader" />
                            <AlternatingRowStyle CssClass="data2" />
                            <Columns>
                                <asp:TemplateField ItemStyle-CssClass="data">
                                    <ItemTemplate>
                                        <input type="radio" id="selectrdo" name="ChkBatchPrintOTGrid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- Yukti-ML Changes Start
                          <asp:TemplateField HeaderText="Bank Account" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAccount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Account")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Check Stock" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblChkStock" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChkStock")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Org. Hierarchy" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblOrg" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Org")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="Order Field" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblOrder" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Order")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="Combined Payment" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblCombinedPay" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.CombinedPayment").ToString())) ? "Yes" : "No" %>'  dataformatstring="{0:Yes/No}"></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="Auto Payment" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White"  >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAutoPay" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.AutoPayment").ToString())) ? "Yes" : "No" %>' dataformatstring="{0:Yes/No}"></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="Report Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblRptType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RptType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  --%>
                                <asp:TemplateField HeaderText="<%$ Resources:gvHdrAccount %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAccount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Account")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:gvHdrChkStock %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblChkStock" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChkStock")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:gvHdrOrgHier %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOrg" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Org")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:gvHdrOrder %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOrder" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Order")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:gvHdrCombinedPay %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCombinedPay" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.CombinedPayment").ToString())) ? "Yes" : "No" %>' dataformatstring="{0:Yes/No}"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:gvHdrAutoPay %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAutoPay" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.AutoPayment").ToString())) ? "Yes" : "No" %>' dataformatstring="{0:Yes/No}"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:gvHdrRptType %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="headerlink2" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRptType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RptType")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--YUkti-ML changes End--%>
                          <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment--%>
                          <%--//JIRA:4042 START: ajohari2--%>
                           <%--<asp:TemplateField HeaderText="<%$ Resources:gvHdrEFTPayment %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblEFTPayment" runat="server" Text='<%# (Boolean.Parse(DataBinder.Eval(Container,"DataItem.EFTPayment").ToString())) ? "Yes" : "No" %>'  dataformatstring="{0:Yes/No}" ></asp:Label>
                                     </ItemTemplate> 
                          </asp:TemplateField>--%>  
                          <%--//JIRA:4042 End: --%>
                          <asp:TemplateField HeaderText="<%$ Resources:gvHdrDistributionType %>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblDistributionType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DistributionType")%>' ></asp:Label>
                                       <asp:Label ID="lblDistributionTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DistributionTypeId")%>' style="display:none" ></asp:Label>
                                     </ItemTemplate> 
                           <%--npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment--%>
                          </asp:TemplateField> 
                            </Columns>
                        </asp:GridView>
                    </div>

                </td>
                <td width="5%" valign="top" colspan="1">
                    <%----**ksahu5-ML-Change Start**--%>
                    <asp:ImageButton runat="server" src="../../../../Images/new.gif" alt=""
                        ID="New_SettingsGrid"
                        onmouseover="javascript:document.all['New_SettingsGrid'].src='../../../../Images/new2.gif'"
                        onmouseout="javascript:document.all['New_SettingsGrid'].src='../../../../Images/new.gif'"
                        title="<%$ Resources:ttNew %>"
                        OnClientClick="return openGridWindowAddEdit('ChkBatchPrintOTGrid','Add');" /><br />
                    <asp:ImageButton runat="server" src="../../../../Images/edittoolbar.gif" alt=""
                        ID="Edit_SettingsGrid"
                        onmouseover="javascript:document.all['Edit_SettingsGrid'].src='../../../../Images/edittoolbar2.gif'"
                        onmouseout="javascript:document.all['Edit_SettingsGrid'].src='../../../../Images/edittoolbar.gif'"
                        title="<%$ Resources:ttEdit %>"
                        OnClientClick="return openGridWindowAddEdit('ChkBatchPrintOTGrid','Edit');" /><br />
                    <asp:ImageButton runat="server" src="../../../../Images/delete.gif" alt=""
                        ID="Delete_SettingsGrid"
                        onmouseover="javascript:document.all['Delete_SettingsGrid'].src='../../../../Images/delete2.gif'"
                        onmouseout="javascript:document.all['Delete_SettingsGrid'].src='../../../../Images/delete.gif'"
                        title="<%$ Resources:ttDelete %>"
                        OnClientClick="return GridForDeletion('ChkBatchPrintOTGrid');" />
                    <%----**ksahu5-ML-Change End**--%>
                </td>
            </tr>
            <%} %>
            <!--End - MITS:26428 - 12/28/2011 -->
            <%--<Start averma62  MITS 32386 FAS Filters>--%>
            <%if (hdSystemModuleName.Text == "FASScheduler")
              { %>
            <div>
                <table width="100%">
                    <tr height="20">
                        <td colspan="5" class="ctrlgroup2">&#160; 
                         <%--**ksahu5-ML-Change** FAS Scheduler Parameters--%>
                            <asp:Label runat="server" ID="lblFASSchParam" Text="<%$ Resources:lblFASSchParam %>" />
                        </td>
                    </tr>
                    <tr height="10">
                        <td class="auto-style1"></td>
                    </tr>
                    <div id="FASSettings" runat="server">
                        <tbody id="tbodyFASSettings" style="display: none">
                            <tr id="">
                                <td>
                                    <%--**ksahu5-ML-Change** FAS Server:--%>
                                    <asp:Label runat="server" ID="lblFASServer" Text="<%$ Resources:lblFASServer %>" />
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="txtFASServer" Enabled="false" size="20" value="" onchange="setDataChanged(true);" rmxref="/Instance/Document/Details/control[@name ='FASServer']"
                                            type="text" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr id="Tr1">
                                <td>
                                    <%--**ksahu5-ML-Change**  FAS User Id:--%>
                                    <asp:Label runat="server" ID="lblFASUserId" Text="<%$ Resources:lblFASUserId %>" />
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="txtFASUserId" Enabled="false" size="20" value="" onchange="setDataChanged(true);" rmxref="/Instance/Document/Details/control[@name ='FASUserId']"
                                            type="text" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr id="Tr2">
                                <td>
                                    <%--**ksahu5-ML-Change** FAS Password:--%>
                                    <asp:Label runat="server" ID="lblFASPassword" Text="<%$ Resources:lblFASPassword %>" />
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="txtFASPassword" Enabled="false" size="20" TextMode="Password" EnableViewState="true" type="text" onchange="setDataChanged(true);" value=""
                                            rmxref="/Instance/Document/Details/control[@name ='FASPassword']"
                                            runat="server"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr id="Tr3">
                                <td>
                                    <%--**ksahu5-ML-Change**  FAS Folder:--%>
                                    <asp:Label runat="server" ID="lblFASFolder" Text="<%$ Resources:lblFASFolder %>" />
                                </td>
                                <td>
                                    <div title="" style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="txtFASFolder" Enabled="false" size="20" type="text" onchange="setDataChanged(true);" value=""
                                            rmxref="/Instance/Document/Details/control[@name ='FASFolder']"
                                            runat="server"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </div>
                    <%--asharma326 MITS 32386 FAS starts--%>
                    <div id="FASSharedDiv">
                        <tbody id="tbodyFASSharedDiv" style="display: none">
                            <tr id="trSharedLocation">
                                <td>
                                    <%--**ksahu5-ML-Change** Shared location:--%>
                                    <asp:Label runat="server" ID="lblSharedLocation" Text="<%$ Resources:lblSharedLocation %>" />
                                </td>
                                <td>
                                    <div style="padding: 0px; margin: 0px">
                                        <asp:TextBox ID="txtFASSharedLocation" size="20" Enabled="false" type="text"
                                            rmxref="/Instance/Document/Details/control[@name ='FASSharedLocation']"
                                            runat="server"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </div>
                    <tr id="trfilelocation" style="display: none">
                        <td>
                            <%-- **ksahu5-ML-Change** File location:--%>
                            <asp:Label runat="server" ID="lblFileLocation" Text="<%$ Resources:lblFileLocation %>" />
                        </td>
                        <td>
                            <div style="padding: 0px; margin: 0px">
                                <asp:TextBox ID="txtFASFileLocation" size="20" type="text"
                                    rmxref="/Instance/Document/Details/control[@name ='FASFileLocation']"
                                    runat="server"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <div id="FASNonediv">
                        <tbody id="tbodyFASNonediv" style="display: none">
                            <tr id="trNone">
                                <td>
                                    <%-- RMA Location:--%>
                                    <asp:Label runat="server" ID="lblRMALocation" Text="<%$ Resources:lblRMALocation %>" />
                                </td>
                                <td>
                                    <div style="padding: 0px; margin: 0px" disabled="false">
                                        <asp:TextBox ID="txtRMALocation" size="20" type="text"
                                            rmxref="/Instance/Document/Details/control[@name ='RMALocation']"
                                            runat="server"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </div>
                    <%--asharma326 MITS 32386 FAS Ends--%>
                    <tr>
                        <table>
                            <tr>
                                <td width="20%" align="left" valign="top" colspan="3">
                                    <asp:Label ID="lblCode2" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="5%" nowrap="true">
                                    <%--Available Claim Status(es):--%>
                                    <asp:Label runat="server" ID="lblAvlClmStatus" Text="<%$ Resources:lblAvlClmStatus %>" />
                                </td>
                                <td class="style1"></td>
                                <td width="*" nowrap="true">
                                    <%-- Related Claim Status(es):--%>
                                    <asp:Label runat="server" ID="lblRtdClmStatus" Text="<%$ Resources:lblRtdClmStatus %>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="5%" nowrap="true" valign="top">
                                    <asp:ListBox type="combobox"
                                        rmxref="/Instance/Document/Details/control[@name='lstAvailableLossComponents']"
                                        rmxignoreset="true" runat="server"
                                        ID="lstAvailableLossComponents" TabIndex="1"
                                        size="10" Height="150px" Style="margin-top: 0px" Width="300px"
                                        SelectionMode="Multiple"></asp:ListBox>
                                    <asp:HiddenField ID="hndPagetype" Value="N" runat="server" />
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/Details/control[@name='txtRelatedComponents']" ID="txtRelatedComponents" Style="display: none"></asp:TextBox>
                                </td>
                                <td valign="top" align="center" class="style1">
                                    <asp:Button runat="server" ID="btnAddAll" Text="&gt;&gt;" class="button" TabIndex="2"
                                        Style="width: 95" OnClientClick="return AddFilter('all');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                    <asp:Button runat="server" ID="btnAddSelected" Text="&gt;" class="button" TabIndex="2"
                                        Style="width: 95" OnClientClick="return AddFilter('selected');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                    <asp:Button runat="server" ID="btnRemoveSelected" Text="&lt;" class="button" TabIndex="4"
                                        Style="width: 95; margin-left: 0px;"
                                        OnClientClick="return RemoveFilter('selected');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                    <asp:Button runat="server" ID="btnRemoveAll" Text="&lt;&lt;" class="button" TabIndex="4"
                                        Style="width: 95; margin-left: 0px;"
                                        OnClientClick="return RemoveFilter('all');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                </td>
                                <td nowrap="true" width="*" valign="top">
                                    <asp:ListBox
                                        type="combobox" rmxref="/Instance/Document/Details/control[@name='lstRelatedLossComponents']"
                                        runat="server" TabIndex="6"
                                        ID="lstRelatedLossComponents" rmxignoreset="true"
                                        size="10" Height="150px" Style="margin-top: 0px" Width="300px" SelectionMode="Multiple" EnableViewState="true"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </tr>
                    <tr height="10">
                        <td class="auto-style1"></td>
                    </tr>
                    <tr>
                        <table>
                            <tr>
                                <td width="20%" align="left" valign="top" colspan="3">
                                    <asp:Label ID="lblCode3" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="5%" nowrap="true">
                                    <%--Available Claim Type(s):--%>
                                    <asp:Label runat="server" ID="lblAvlClmType" Text="<%$ Resources:lblAvlClmType %>" />
                                </td>
                                <td class="style1"></td>
                                <td width="*" nowrap="true">
                                    <%--    Related Claim Type(s):--%>
                                    <asp:Label runat="server" ID="lblRldClmType" Text="<%$ Resources:lblRldClmType %>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="5%" nowrap="true" valign="top">
                                    <asp:ListBox type="combobox"
                                        rmxref="/Instance/Document/Details/control[@name='lstAvailableCTypeComponents']"
                                        rmxignoreset="true" runat="server"
                                        ID="lstAvailableCTypeComponents" TabIndex="1"
                                        size="10" Height="150px" Style="margin-top: 0px" Width="300px"
                                        SelectionMode="Multiple"></asp:ListBox>
                                    <asp:HiddenField ID="hndPagetypeCType" Value="N" runat="server" />
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/Details/control[@name='txtRelatedCTypeComponents']" ID="txtRelatedCTypeComponents" Style="display: none"></asp:TextBox>
                                </td>
                                <td valign="top" align="center" class="style1">
                                    <asp:Button runat="server" ID="Button1" Text="&gt;&gt;" class="button" TabIndex="2"
                                        Style="width: 95" OnClientClick="return AddFilterCType('all');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                    <asp:Button runat="server" ID="Button2" Text="&gt;" class="button" TabIndex="2"
                                        Style="width: 95" OnClientClick="return AddFilterCType('selected');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                    <asp:Button runat="server" ID="Button3" Text="&lt;" class="button" TabIndex="4"
                                        Style="width: 95; margin-left: 0px;"
                                        OnClientClick="return RemoveFilterCType('selected');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                    <asp:Button runat="server" ID="Button4" Text="&lt;&lt;" class="button" TabIndex="4"
                                        Style="width: 95; margin-left: 0px;"
                                        OnClientClick="return RemoveFilterCType('all');" Height="26px"
                                        Width="50px" UseSubmitBehavior="False" /><br />
                                    <br />
                                </td>
                                <td nowrap="true" width="*" valign="top">
                                    <asp:ListBox
                                        type="combobox" rmxref="/Instance/Document/Details/control[@name='lstRelatedCTypeComponents']"
                                        runat="server" TabIndex="6"
                                        ID="lstRelatedCTypeComponents" rmxignoreset="true"
                                        size="10" Height="150px" Style="margin-top: 0px" Width="300px" SelectionMode="Multiple" EnableViewState="true"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </tr>
                    <tr height="10">
                        <td class="auto-style1" />
                    </tr>
                </table>
            </div>
            <div>
                <table>
                    <tr>
                        <td class="auto-style2">
                            <%-- Date of Claim--%>
                            <asp:Label runat="server" ID="lblDtClaim" Text="<%$ Resources:lblDtClaim %>" />
                        </td>
                        <td class="auto-style4">
                            <%--From--%>
                            <asp:Label runat="server" ID="lblFrom" Text="<%$ Resources:lblFrom %>" />
                        </td>
                        <td class="auto-style3">
                            <asp:TextBox runat="server" FormatAs="date" ID="txtFrmDate" RMXRef="/Instance/Document/Details/FromDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="21" />
                            <%-- <input type="button" class="DateLookupControl" name="frmdatebtn" />
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                                {
                                    inputField: "txtFrmDate",
                                    ifFormat: "%m/%d/%Y",
                                    button: "frmdatebtn"
                                }
                            );
                        </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtFrmDate").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                     //buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "22");
                                });
                            </script>
                        </td>
                        <td class="auto-style5">
                            <%--To--%>
                            <asp:Label runat="server" ID="lblTo" Text="<%$ Resources:lblTo %>" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" FormatAs="date" ID="txtToDate" RMXRef="/Instance/Document/Details/ToDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="23" />
                            <%-- <input type="button" class="DateLookupControl" name="todatebtn" />
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                                {
                                    inputField: "txtToDate",
                                    ifFormat: "%m/%d/%Y",
                                    button: "todatebtn"
                                }
                            );
                        </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtToDate").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                     //buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "24");
                                });
                            </script>
                        </td>
                    </tr>
                    <tr height="10">
                        <td class="auto-style2"></td>
                    </tr>
                    <tr>
                        <td class="auto-style2">
                            <%--Date Claim Updated--%>
                            <asp:Label runat="server" ID="lblDtClmUpdated" Text="<%$ Resources:lblDtClmUpdated %>" />
                        </td>
                        <td class="auto-style4">
                            <%--   From--%>
                            <asp:Label runat="server" ID="lblFrom1" Text="<%$ Resources:lblFrom %>" />
                        </td>
                        <td class="auto-style3">
                            <asp:TextBox runat="server" FormatAs="date" ID="txtUFrmDate" RMXRef="/Instance/Document/Details/UFromDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="25" />
                            <%-- <input type="button" class="DateLookupControl" name="ufrmdatebtn" />
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                                {
                                    inputField: "txtUFrmDate",
                                    ifFormat: "%m/%d/%Y",
                                    button: "ufrmdatebtn"
                                }
                            );
                        </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtUFrmDate").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                    // buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "26");
                                });
                            </script>
                        </td>
                        <td class="auto-style5">
                            <%-- To--%>
                            <asp:Label runat="server" ID="lblTo1" Text="<%$ Resources:lblTo %>" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" FormatAs="date" ID="txtUToDate" RMXRef="/Instance/Document/Details/UToDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="27" />
                            <%-- <input type="button" class="DateLookupControl" name="utodatebtn" />
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                                {
                                    inputField: "txtUToDate",
                                    ifFormat: "%m/%d/%Y",
                                    button: "utodatebtn"
                                }
                            );
                        </script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#txtUToDate").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../../../Images/calendar.gif",
                                    // buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "28");
                                });
                            </script>
                        </td>
                    </tr>
                    <tr height="10">
                        <td class="auto-style2"></td>
                    </tr>
                    <tr>
                        <td class="auto-style2">&nbsp;</td>
                        <td class="auto-style4">&nbsp;</td>
                        <td class="auto-style3">&nbsp;</td>
                        <td class="auto-style5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
            <%} %>
            <%-- <End averma62  MITS 32386 FAS Filters>--%>
            <%--akaushik5 Added for MITS 36381 Starts--%>
            <%if (hdSystemModuleName.Text.Equals("Resbal"))
              { %>
            <tr width="100%" height="20">
                <td class="ctrlgroup2" colspan="5">&#160; 
               <%--ksahu5- MITS 33827 Start --%>
                    <%-- Reserve Balance Related Parameters:--%>
                    <asp:Label ID="lblReserveBalanceParamWeekly" runat="server" Text="<%$ Resources:lblReserveBalanceParamWeekly %>" />
                    <%--ksahu5- MITS 33827 End --%>
                </td>
            </tr>
            <tr height="20">
                <td class="auto-style14"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="optResbalAllClaims" runat="server" onclick="SelectResbalOptions(this);"
                        rmxref="/Instance/Document/Details/ResbalOption" Text="<%$ Resources:rbtnOptResbalAllClaims %>" value="1" />
                    <%--ksahu5- MITS 33827  --%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="optResbalSingleClaim" runat="server" onclick="SelectResbalOptions(this);"
                        rmxref="/Instance/Document/Details/ResbalOption" Text="<%$ Resources:rbtnOptResbalSingleClaim %>" value="2" /><%--ksahu5- MITS 33827  --%>
                </td>
            </tr>
            <tr id="trResbalSingleClaim" style="display: none">
                <td align="left">
                    <%--ksahu5- MITS 33827  --%>
                    <%-- Claim Number:--%>
                    <asp:Label ID="lblClmNumber" runat="server" Text="<%$ Resources:lblClmNumber %>" />
                    <%--ksahu5- MITS 33827 End --%>
                </td>
                <td class="auto-style8">
                    <asp:TextBox ID="txtClaimNumber" runat="server"
                        rmxref="/Instance/Document/Details/ResbalClaimNumber" />
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:RadioButton ID="optResbalOnClaimDate" runat="server" onclick="SelectResbalOptions(this);"
                        rmxref="/Instance/Document/Details/ResbalOption" Text="<%$ Resources:rbtnOptResbalOnClaimDate %>" value="3" />
                    <%--ksahu5- MITS 33827 --%>
                </td>
            </tr>
            <tr id="trResbalClaimDate" style="display: none">
                <td colspan="2">
                    <table width="100%">
                        <tr>
                            <td width="20%">
                                <asp:Label ID="lblResBalFrom" runat="server" Text="<%$ Resources:lblResBalFrom %>" />
                            </td>
                            <%--ksahu5- MITS 33827 --%>
                            <td class="auto-style8">
                                <asp:TextBox runat="server" FormatAs="date" ID="txtResbalFromDate" RMXRef="/Instance/Document/Details/FromDate"
                            RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="29" />
                                <%-- <input type="button" class="DateLookupControl" name="resbalfrmdatebtn" />

                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                            {
                                inputField: "txtResbalFromDate",
                                ifFormat: "%m/%d/%Y",
                                button: "resbalfrmdatebtn"
                            }
                            );
                        </script>--%>
                                <script type="text/javascript">
                                    $(function () {
                                        $("#txtResbalFromDate").datepicker({
                                            showOn: "button",
                                            buttonImage: "../../../../Images/calendar.gif",
                                     //buttonImageOnly: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true,
                                            changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "30");
                                    });
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <asp:Label ID="lblResBalTo" runat="server" Text="<%$ Resources:lblResBalTo %>" />
                            </td>
                            <%--ksahu5- MITS 33827 --%>
                            <td class="auto-style8">
                                <asp:TextBox runat="server" FormatAs="date" ID="txtResbalToDate" RMXRef="/Instance/Document/Details/ToDate"
                    RMXType="date" onblur="dateLostFocus(this.id);" tabIndex="31"/>
                                <%--  <input type="button" class="DateLookupControl" name="resbaltodatebtn" />

                <script type="text/javascript">
                    Zapatec.Calendar.setup(
                    {
                        inputField: "txtResbalToDate",
                        ifFormat: "%m/%d/%Y",
                        button: "resbaltodatebtn"
                    }
                    );
                </script>--%>
                                <script type="text/javascript">
                                    $(function () {
                                        $("#txtResbalToDate").datepicker({
                                            showOn: "button",
                                            buttonImage: "../../../../Images/calendar.gif",
                             //buttonImageOnly: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true,
                                            changeYear: true
                         }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "32");
                                    });
                                </script>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <%--akaushik5 Added for MITS 36381 Ends--%>
            <tr height="20">
                <td></td>
            </tr>

            <tr>
                <td>&#160;&#160;
                </td>
                <td align="left" colspan="2">
                    <%--Yukti-ML Changes Start--%>
                    <%--<asp:Button ID="btnOptionset" Text="Optionset" class="button" OnClientClick="return OpenOptionset();" OnClick="MoveToOptionset" runat="server" />--%>
                    <asp:Button ID="btnOptionset" Text="<%$ Resources:btnOptionset %>" class="button" OnClientClick="return OpenOptionset();" OnClick="MoveToOptionset" runat="server" />
                    &#160;&#160;
                <%--<asp:Button ID="btnSave" Text="Save" class="button" OnClientClick="SaveWeeklySettings();return false;" runat="server" />--%>
                    <asp:Button ID="btnSave" Text="<%$ Resources:btnSave %>" class="button" OnClientClick="SaveWeeklySettings();return false;" runat="server" />
                    &#160;&#160;
                <%--<asp:Button ID="btnCancel" Text="Cancel" class="button" OnClientClick="OnCancel();return false;" runat="server" />--%>
                    <asp:Button ID="btnCancel" Text="<%$ Resources:btnCancel %>" class="button" OnClientClick="OnCancel();return false;" runat="server" />
                    <%--YUkti-ML Changes End--%>
                </td>
            </tr>
        </table>
        <asp:TextBox value="" Style="display: none" ID="hdnaction" runat="server" />
        <asp:TextBox value="" Style="display: none" ID="hdnScheduleId" runat="server" rmxref="/Instance/Document/Details/ScheduleId" />
        <asp:TextBox value="" Style="display: none" ID="hdnsaved" rmxref="/Instance/Document/Details/saved" runat="server" />
        <asp:TextBox value="" Style="display: none" ID="hdTaskType" rmxref="/Instance/Document/Details/TaskType"
            runat="server" />
        <asp:TextBox value="" Style="display: none" ID="hdUserArguments" rmxref="/Instance/Document/Details/UserArguments"
            runat="server" />
        <asp:TextBox value="" Style="display: none" ID="hdTaskName" rmxref="/Instance/Document/Details/TaskName" runat="server" />
        <asp:TextBox value="" Style="display: none" ID="hdIsDataIntegratorTask" runat="server" rmxref="/Instance/Document/Details/IsDataIntegratorTask" />
        <asp:TextBox value="" Style="display: none" ID="hdOptionsetId" runat="server" rmxref="/Instance/Document/Details/OptionsetId" />
        <asp:HiddenField ID="hdScheduleTypeId" runat="server" />
        <asp:TextBox value="" Style="display: none" ID="hdSystemModuleName" runat="server" rmxref="/Instance/Document/Details/SystemModuleName" />
        <asp:TextBox value="" Style="display: none" ID="hdnLangCode" runat="server" rmxref="/Instance/Document/Details/LangCode" />
        <asp:TextBox value="" Style="display: none" ID="hdnCulture" rmxref="/Instance/Document/Details/Culture" runat="server" />

    </form>
</body>
</html>
