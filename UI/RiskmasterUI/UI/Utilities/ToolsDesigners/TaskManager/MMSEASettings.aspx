<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMSEASettings.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.MMSEASettings" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../../../../Content/dhtml-div.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/utilities.js"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
    </script>

    <script src="../../../../Scripts/TMSettings.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

    <style type="text/css">
        .style2
        {
            width: 113px;
        }

        .style3
        {
            text-decoration: underline;
            width: 113px;
        }

        .style4
        {
            width: 478px;
        }

        .style5
        {
            width: 337px;
        }

        .style7
        {
            text-decoration: underline;
            width: 116px;
        }

        .style8
        {
            width: 116px;
        }

        .style9
        {
            width: 362px;
        }

        .style10
        {
            width: 116px;
            height: 19px;
        }

        .style11
        {
            height: 19px;
			
        }
        .auto-style1
        {
            width: 369px;
        }

        .auto-style2
        {
            height: 19px;
            width: 301px;
        }

        .auto-style3
        {
            width: 301px;
        }
        .auto-style4
        {
            height: 29px;
        }
        .auto-style5
        {
            height: 29px;
            width: 369px;
        }
		
    </style>

</head>
<body>
    <form id="frmData" name="frmData" method="post" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                <td colspan="2">
                    <uc1:errorcontrol id="ErrorControl1" runat="server" />

                </td>
                <tr height="10">
                    <td></td>
                </tr>
        </table>
        <div>
            <div>

                <asp:textbox style="display: none" runat="server" name="hTabName" id="TextBox1" />
                <div class="msgheader" id="div_formtitle" runat="server">
                    <asp:label id="Label1" runat="server" text="MMSEA Optionset" />
                    <asp:label id="formsubtitle" runat="server" text="" />
                </div>
                <div id="Div1" class="errtextheader" runat="server">
                    <asp:label id="formdemotitle" runat="server" text="" />
                </div>

            </div>
            </br>
    <table>
        <tr>

            <td class="style4">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="style10" nowrap="true" name="TABSexportsetting"
                            id="TABSexportsetting">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="exportsetting"
                                id="LINKTABSexportsetting"><span style="text-decoration: none">Export Optionset 
                                </span></a>
                        </td>
                        <!-- Vsoni5 : 12/14/2010 : MITS 23165: "return false;" added with onClick event for both the tabs to 
                        prevent the page to move up. -->
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none; border-top: none;"
                            class="style11">&nbsp;&nbsp;
                        </td>
                        <td class="style10" nowrap="true" name="TABSimportsetting" id="TABSimportsetting">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="importsetting"
                                id="LINKTABSimportsetting"><span style="text-decoration: none">Import 
                            Optionset</span></a>
                        </td>

                        <td valign="top" nowrap="true" class="style11"></td>
                    </tr>

                </table>
                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABimportsetting"
                    id="FORMTABimportsetting" style="display: none">
                    <tr>
                        <td class="style2">&nbsp;&nbsp;</td>
                        <td class="style5">&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">Optionset Name</td>
                        <td class="style5">

                            <asp:textbox id="txtImportOptionset" runat="server"></asp:textbox>

                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">RRE Id</td>
                        <td class="style5">

                            <asp:dropdownlist id="ddlImportRREId" runat="server">
                            </asp:dropdownlist>

                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">File Format</td>
                        <td class="style5">

                            <asp:dropdownlist id="ddlImportFileFormat" runat="server" autopostback="true" onselectedindexchanged="ddlImportFileFormat_OnSelectedIndexChanged">
                                <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem>Claim</asp:ListItem>
                                <asp:ListItem>Query</asp:ListItem>
                            </asp:dropdownlist>

                            &nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="style3" style="font-weight: bold">Import File</td>
                        <td class="style5">
                            <!-- Vsoni5 : 04/05/2010 : MITS 17486: Events added to prevent user, entering import file name manually. -->
                            <input id="FUImport" type="file" runat="server" oncontextmenu="return false;" onkeypress="return false;" onpaste="return false" />



                        </td>

                    </tr>
                    <tr id="Claim_TIN" runat="server" visible="true">
                        <td class="style3" style="font-weight: bold">TIN Import File</td>
                        <td class="style5">
                            <input id="FUTinImport" type="file" runat="server" oncontextmenu="return false;" onkeypress="return false;" onpaste="return false" />

                        </td>
                    </tr>
                    <tr>
                        <td class="style2">&nbsp;</td>
                        <td class="style5"></td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <!-- Vsoni5 : 04/05/2010 : MITS 17486: OnClientClick events handled to validate import file extension. -->
                            <asp:button id="btnSaveImport" runat="server" class="button" text="Save"
                                causesvalidation="True"
                                validationgroup="vImport" onclick="btnSaveImport_Click" onclientclick="return validateFileType(document.forms[0].FUImport.value);" />
                            <asp:button id="btnImportCancel" runat="server" text="Cancel"
                                class="button" onclientclick="OnCancel();return false;" causesvalidation="false" />
                        </td>
                        <td class="style5">&nbsp;</td>
                    </tr>
                </table>
                </br>
                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABexportsetting" id="FORMTABexportsetting"
                    style="width: 449px">
                    <tr>
                        <td class="style7" style="font-weight: bold">Optionset Name</td>
                        <td class="auto-style3">

                            <asp:textbox id="txtExportOptionset" runat="server" ontextchanged="txtExportOptionset_TextChanged"></asp:textbox>

                            &nbsp;
                        
                        </td>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style7" style="font-weight: bold">RRE Id</td>
                        <td class="auto-style3">

                            <asp:dropdownlist id="ddlExportRREId" runat="server" autopostback="True" OnSelectedIndexChanged="ddlExportRREId_SelectedIndexChanged">
                            </asp:dropdownlist>

                            &nbsp;</td>
                    
                    </tr>
                    <tr>
                        <td class="style7" style="font-weight: bold">File Format</td>
                        <td class="auto-style3">

                            <asp:dropdownlist id="ddlExportFileFormat" runat="server" autopostback="True" onselectedindexchanged="ddlExportFileFormat_OnSelectedIndexChanged">
                               <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem>Claim Input</asp:ListItem>
                                <asp:ListItem>Query Input</asp:ListItem>
                            </asp:dropdownlist>

                            &nbsp;</td>
                    
                    </tr>
                    <tr>
                        <td class="style8">&nbsp;</td>
                        <td class="auto-style3">

                            <asp:checkbox id="chkTestExport" runat="server" text="Test Export" />

                        </td>
                     
                    </tr>
                    <tr>
                        <td class="style8" style="font-weight: bold">Claimant Filters</td>
                        <td class="auto-style3">

                            <asp:checkbox id="chkPrimayClaimant" runat="server" text="Export Only Primary Claimants" />

                        </td>
                 
                    </tr>

                    <tr id="Claim_TPOC" runat="server" visible="true">
                        <td class="style10" style="font-weight: bold">Claim Option
                        </td>
                        <td class="auto-style2">

                            <asp:checkbox id="chkTPOCSThreshold" runat="server" text="Don&#8217;t Include TPOCs if under the Threshold" oncheckedchanged="chkTPOCSThreshold_CheckedChanged" />

                        </td>
                    </tr>
                    <%--mrishi2 - JIRA 16461 BEGINS --%>
                    <tr>
                        <td colspan="2">
                            <table runat="server" style="width: 100%; height: 145px;">
                                <tr>
                                    <td class="auto-style1">&nbsp;&nbsp;</td>
                                    <td style="text-align: left">&nbsp;</td>
                                    <%--  <td style="text-align: left">
                                                        &nbsp;</td>--%>
                                    <td style="text-align: left">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td class="auto-style1" style="text-decoration: underline">
                                        <b style="text-align: left">Claim Type Criteria:</b></td>
                                    <td style="text-align: left">&nbsp;</td>
                                    <td style="text-align: left">&nbsp;</td>
                                    <td style="text-align: left">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style5">

                                        General Claims
                                    </td>
                                    <td style="text-align: left" class="auto-style4">
                                            Vehicle Accident Claims
                                  </td>
                                   
                                    <td style="text-align: left" class="auto-style4">

                                            Workers' Compensation Claims
                                </td>
                                        </tr>
                                <tr>
                                    <td class="auto-style1">
                                        <asp:listbox id="ListBox1" runat="server" width="181px" 
                                            selectionmode="Multiple" disabled="True" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged"></asp:listbox>
                                    </td>
                                    <td style="text-align: left">
                                        <asp:listbox id="ListBox2" runat="server" width="179px"
                                            selectionmode="Multiple" disabled="True" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged"></asp:listbox>
                                    </td>
                                   
                                    <td style="text-align: left">
                                        <asp:listbox id="ListBox3" runat="server" width="181px"
                                            selectionmode="Multiple" disabled="True" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged"></asp:listbox>
                                    </td>

                                    <%--Subhendu 11/01/2010 Added new ListBox for Property Claims(MITS 22785)--%>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <%--mrishi2 - JIRA 16461 ENDS --%>
                    <tr>
                        <td class="style8">
                            
                            <asp:button id="btnSave" runat="server" class="button" text="Save"
                                onclick="btnSave_Click" causesvalidation="True" validationgroup="vExport" />
                            <asp:button id="btnCancel" runat="server" text="Cancel"
                                class="button" onclientclick="OnCancel();return false;" causesvalidation="false" />
                        </td>
<td>
    &nbsp;
</td>
                    </tr>


                    <tr>
                        <td>
                            <input type="hidden" name="hTabName" id="hTabName" runat="server" value="exportsetting" />
                        </td>

                    </tr>

                </table>
                <asp:hiddenfield id="hdTaskManagerXml" runat="server" />
                <asp:hiddenfield id="hdClaimLOB" runat="server" />
                <asp:hiddenfield id="bIsCarrier" runat="server" />
                <asp:hiddenfield id="hdOptionsetId" runat="server" />
                <asp:hiddenfield id="ListBox1Val" runat="server"/>
                <asp:hiddenfield id="ListBox2Val" runat="server"/>
                <asp:hiddenfield id="ListBox3Val" runat="server"/>
        </div>
    </form>
</body>
</html>
