//******************************************************************************
//Developer: Jasmeet Singh Bawa
//Date: 4/3/2009
//Revision Number:
//Description: 1099 Optionset Screen. Save the settings provided by user for the job
//******************************************************************************

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.ServiceHelpers;
using Riskmaster.Models;    //ipuri 12/07/2014

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class RM1099Preferences : System.Web.UI.Page
    {
        private int iOptionset;
        private string sModuleName = "RM1099";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes
            if (!Page.IsPostBack)
            {
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                iOptionset = CommonFunctions.OptionsetId;

                fRefresh();                 //Refreshes the form controls

                fExtractSettings();         //Extract the preferences from DB
            }
            //Tanu : MITS 17006
            //Initializing events for Confirmation messages
            ChkList1.Items[0].Attributes.Add("onclick", "ExporPaymentsConfirm();");
            // ChkList2.Items[2].Attributes.Add("onclick", "ExportPayeesConfirm();");  //vgupta20 08/05/2009- confirmation message not required
            //Vsoni5 - MITS 25787 : uncommented the existing code for IgnoreTaxIDErros checkbox
            ChkList2.Items[1].Attributes.Add("onclick", "IgnoreTaxIDConfirm();");  //- vchaturvedi2 : MITS-21868 not required in DA.
            //OptSelect.Items[1].Attributes.Add("onclick", "ExportSpecificReserveConfirm();");         

            // Vsoni5 : MITS 25790 : Refresh Payer and Account List on Payer Selection.
            // This is an ugly hack but it is required to avoid synchronization before deletion.             
            // It will make sure that the SyncPayerAndBankAccts is called only if it is AsyncPostBack and caused by 
            // Controls except Delete button available in front of Payer List Box and Account List Box.
            // otherwise it loads the Payer and Account list prior to deletion and we loose the selected Payer/Account.
            // In such case SyncPayerAndBankAccts function will be called in BtnDelPayer_Click event.
            if (ScriptManager1.IsInAsyncPostBack
                && !ScriptManager1.AsyncPostBackSourceElementID.ToString().Equals("BtnDelPayer")
                && !ScriptManager1.AsyncPostBackSourceElementID.ToString().Equals("BtnDelAccount"))
            {
                SyncPayerAndBankAccts();
            }
            
        }

        //******************************************************************************
        //Developer: Jasmeet Singh Bawa
        //Revision Number:
        //Description: Call to function fExtract Settings. This will fetch settings from default optionset stored in database
        //******************************************************************************
        protected void fExtractSettings()
        {
            int iCnt = 0;
            string sTemp = "";
            string[] aTemp;
            ListItem lstTemp = null;
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModel = null;

            //DataIntegratorService.DataIntegratorServiceClient objService = null;
            DataIntegratorModel objModel = null;
            
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            bool isError = false;
            try
            {
                string sCheckTaxYear = "";  //Tanu: MITS 17010
                objModel = new DataIntegratorModel();
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }
                objModel.ModuleName = sModuleName;
                objModel.OptionSetID = iOptionset;

                //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                //ipuri REST Service Conversion 12/06/2014 Start
                //objModel = objService.RetrieveSettings(objModel);
                objModel.ClientId = AppHelper.ClientId;

                objModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                //ipuri REST Service Conversion 12/06/2014 End

                Dictionary<string, string> RM1099_Parms = new Dictionary<string, string>();
                RM1099_Parms = (Dictionary<string, string>)objModel.Parms;

                sTemp = RM1099_Parms["Transaction_Type_Box13"];

                ddlBox13.DataSource = objModel.dsTransTypes;
                ddlBox13.DataTextField = objModel.dsTransTypes.Tables[0].Columns[1].ToString();
                ddlBox13.DataValueField = objModel.dsTransTypes.Tables[0].Columns[0].ToString();
                ddlBox13.DataBind();
                ddlBox13.Items.Insert(0, "---Select---");

                //Voni5 : MITS 25791 : 
                ddlBox14.DataSource = objModel.dsTransTypes;
                ddlBox14.DataTextField = objModel.dsTransTypes.Tables[0].Columns[1].ToString();
                ddlBox14.DataValueField = objModel.dsTransTypes.Tables[0].Columns[0].ToString();
                ddlBox14.DataBind();
                ddlBox14.Items.Insert(0, "---Select---");

                // Set Trans type for Box 13 ListBox
                if (sTemp.Length > 0)
                {
                    DataRow[] drBox13 = objModel.dsTransTypes.Tables[0].Select("CODE_ID IN (" + sTemp + ")");
                    foreach (DataRow OneRowBox13 in drBox13)
                    {
                        //Vsoni5 : MITS 25791 : Remove selected Trans types from Box 13 and 14.
                        lstTemp = new ListItem(OneRowBox13[1].ToString(), OneRowBox13[0].ToString());
                        LstBox13.Items.Add(lstTemp);
                        if (ddlBox13.Items.Contains(lstTemp)) 
                        {
                            ddlBox13.Items.Remove(lstTemp);
                        }
                        if (ddlBox14.Items.Contains(lstTemp))
                        {
                            ddlBox14.Items.Remove(lstTemp);
                        }
                    }
                }

                sTemp = RM1099_Parms["Transaction_Type_Box14"];

                //The dataset recieved can be further filtered to get desired records. 
                //Filtered records will move to Datarow[] array from where it can be 
                //populated to list box.
                // Set Trans type for Box 14 ListBox
                if (sTemp.Length > 0)
                {
                    DataRow[] drBox14 = objModel.dsTransTypes.Tables[0].Select("CODE_ID IN (" + sTemp + ")");
                    foreach (DataRow OneRowBox14 in drBox14)
                    {
                        lstTemp = new ListItem(OneRowBox14[1].ToString(), OneRowBox14[0].ToString());
                        LstBox14.Items.Add(lstTemp);
                        //Vsoni5 : MITS 25791 : Remove selected Trans types from Box 13 and 14.
                        if (ddlBox13.Items.Contains(lstTemp))
                        {
                            ddlBox13.Items.Remove(lstTemp);
                        }
                        if (ddlBox14.Items.Contains(lstTemp))
                        {
                            ddlBox14.Items.Remove(lstTemp);
                        }
                    }
                }

                //start npradeepshar 02/17/2011 MITS:23729 added medical reserve Types
                ddlReserveTypeBox6.DataSource = objModel.dsReserveTypes;
                ddlReserveTypeBox6.DataTextField = objModel.dsReserveTypes.Tables[0].Columns[1].ToString();
                ddlReserveTypeBox6.DataValueField = objModel.dsReserveTypes.Tables[0].Columns[0].ToString();
                ddlReserveTypeBox6.DataBind();
                ddlReserveTypeBox6.Items.Insert(0, "---Select---");

                //The dataset recieved can be further filtered to get desired records. 
                //Filtered records will move to Datarow[] array from where it can be 
                //populated to list box.
                sTemp = RM1099_Parms["Medical_Reserve_Type_Box6"];
                if (sTemp.Length > 0)
                {
                    DataRow[] drReserveCodes = objModel.dsReserveTypes.Tables[0].Select("CODE_ID IN (" + sTemp + ")");
                    foreach (DataRow OneRowReserveCode in drReserveCodes)
                    {
                        lstReserveTypeBox6.Items.Add(new ListItem(OneRowReserveCode[1].ToString(), OneRowReserveCode[0].ToString()));
                    }
                }
                //Npradeepshar end 02/17/2011

                sTemp = RM1099_Parms["Payers_To_Export"];
                if (sTemp.Length > 0)
                {
                    DataRow[] drPayer = objModel.dsEntity.Tables[0].Select(" ENTITY_ID IN (" + sTemp + ") ");
                    sTemp = "";
                    foreach (DataRow OneRowPayer in drPayer)
                    {
                        LstPayer.Items.Add(new ListItem(OneRowPayer[1].ToString() + "-" + OneRowPayer[2].ToString(), OneRowPayer[0].ToString()));
                        LstPayer_lst.Text = LstPayer_lst.Text + " " + OneRowPayer[0].ToString();
                        if (sTemp.Length > 0)
                        {
                            sTemp = sTemp + " " + OneRowPayer[0].ToString();
                        }
                        else
                        {
                            sTemp = OneRowPayer[0].ToString();
                        }
                    }
                //Vsoni5 : MITS 25790 - Start.
                    tdLstExpBkAccts.Disabled = false;
                }
                else
                {
                    tdLstExpBkAccts.Disabled = false;
                }
                //Vsoni5 : MITS 25790 - End.

                //Bank Accounts 
                sTemp = RM1099_Parms["Bank_Accounts"];
                if (sTemp.Length > 0)
                {
                    DataRow[] drBA = objModel.dsAccount.Tables[0].Select(" ACCOUNT_ID IN (" + sTemp + ") ");
                    sTemp = "";
                    foreach (DataRow OneRowBA in drBA)
                    {
                        LstBankAcc.Items.Add(new ListItem(OneRowBA[0].ToString(), OneRowBA[1].ToString()));
                    }
                    chkBankAccount.Checked = true;
                    //Vsoni5 : MITS 25790 
                    tdLstExpBkAccts.Disabled = false;
                }
                else
                {
                    chkBankAccount.Checked = false;
                    //Vsoni5 : MITS 25790 
                    tdLstExpBkAccts.Disabled = true;
                }

                TxtOptionSetName.Text = objModel.OptionSetName;
                //Anand 10/22/2009: Change starts.

                //vchaturvedi2: MITS 22822 
                if (TxtOptionSetName.Text != "")
                {
                    TxtOptionSetName.ReadOnly = true;
                }

                sTemp = RM1099_Parms["Fiscal_Year"];
                if (sTemp=="0")
                {
                    TxtDateTo.BackColor = System.Drawing.Color.White;
                    TxtDateFrom.BackColor = System.Drawing.Color.White;
                    TxtFiscalYear.BackColor = System.Drawing.Color.WhiteSmoke;
                    TxtDateFrom.Enabled = true;
                    TxtDateTo.Enabled = true;
                    // btnCalendarFrom.Enabled = true; //vkumar258 - RMA-6037
                    //btnCalendarTo.Enabled = true;  //Anand Change ends. //vkumar258 - RMA-6037
                    divFromDate.Visible = true;
                    divToDate.Visible = true;//vkumar258 ML Changes
                  
                }
                //Tanu: MITS 17010
                sCheckTaxYear = RM1099_Parms["Fiscal_Year"];
                if (sCheckTaxYear == "0")       //Means Date Range is selected
                {
                    //Fill date range fields
                    ChkDtRange.Checked = true;
                    TxtFiscalYear.Enabled = false;

                    sTemp = RM1099_Parms["Date_From"];
                    sTemp = sTemp.Substring(4, 2) + "/" + sTemp.Substring(6, 2) + "/" + sTemp.Substring(0, 4);
                    TxtDateFrom.Text = sTemp;

                    sTemp = RM1099_Parms["Date_To"];
                    sTemp = sTemp.Substring(4, 2) + "/" + sTemp.Substring(6, 2) + "/" + sTemp.Substring(0, 4);
                    TxtDateTo.Text = sTemp;
                }
                else                    //When Tax Year is selected
                {
                    TxtFiscalYear.Text = RM1099_Parms["Fiscal_Year"];
                }

                if (RM1099_Parms["Export_Printed_Payments_Only"] == "1")
                    chkPrintedPaymentsOnly.Checked = true;
                else
                    chkPrintedPaymentsOnly.Checked = false;

                if (RM1099_Parms["Export_Payments_Without_Claim"] == "1")
                    ChkList1.Items[0].Selected = true;
                else
                    ChkList1.Items[0].Selected = false;

                if (RM1099_Parms["Exclude_Payments_To_OrgHier"] == "1")
                    ChkList1.Items[1].Selected = true;
                else
                    ChkList1.Items[1].Selected = false;

                if (RM1099_Parms["Exclude_Payments_To_Claimants"] == "1")
                    ChkList1.Items[2].Selected = true;
                else
                    ChkList1.Items[2].Selected = false;

                if (RM1099_Parms["Exclude_Deleted_Entities"] == "1")
                    ChkList1.Items[3].Selected = true;
                else
                    ChkList1.Items[3].Selected = false;

                if (RM1099_Parms["Export_Payees_With_TotalPaid"] != "0")
                {
                    chkTotalPaid.Checked = true;
                    TxtTotalPaid.Text = RM1099_Parms["Export_Payees_With_TotalPaid"];
                    TxtTotalPaid.Enabled = true;
                }
                else
                {
                    chkTotalPaid.Checked = false;
                }

                // Vsoni5 : MITS 25786 : 
                //Roll Up checkbox moved from checkbox list to a seperate control.
                if (RM1099_Parms["RollUp_Payments"] == "1")
                    chkBxRollUpPymt.Checked = true;
                else
                    chkBxRollUpPymt.Checked = false;

                //Vsoni5 - MITS 25787 : uncommented the existing code for IgnoreTaxIDErros checkbox
                if (RM1099_Parms["Ignore_TaxID_Errors"] == "1")
                    ChkList2.Items[1].Selected = true;
                else
                    ChkList2.Items[1].Selected = false;

                if (RM1099_Parms["Check_RM1099_Flag"] == "1")
                    //ChkList2.Items[1].Selected = true;npradeepshar 02/17/2011 MITS 23729 Changed position of checkboxes
                    ChkList1.Items[4].Selected = true;
                else
                    //ChkList2.Items[1].Selected = false;npradeepshar 02/17/2011 MITS 23729 Changed position of checkboxes
                    ChkList1.Items[4].Selected = false;

                if (RM1099_Parms["Include_EID_In_Payee_File"] == "1")
                    //ChkList2.Items[2].Selected = true;npradeepshar 02/17/2011 MITS 23729 Changed position of checkboxes
                    ChkList1.Items[5].Selected = true;
                else
                    //ChkList2.Items[2].Selected = false;npradeepshar 02/17/2011 MITS 23729 Changed position of checkboxes
                    ChkList1.Items[5].Selected = false;
                //npradeepshar 02/18/2011 MITS 23753  added 'use double quotes
                if (RM1099_Parms["Use_Double_Quotes"] == "1")
                    ChkList2.Items[0].Selected = true;
                else
                    ChkList2.Items[0].Selected = false;
                //end MITS 23753
                //ddlSelectDynamic.Items.Add(new ListItem(" ", "0"));

                #region MITS 26117
                // Set Export filters checkboxes
                chkBxExpAllPymtTxYr.Checked = RM1099_Parms["ExportAllPymts"] == "1" ? true : false;
                //chkBxExpTransType.Checked = RM1099_Parms["ExportTransTypes"] == "1" ? true : false;
                //chkBxExpReserveType.Checked = RM1099_Parms["ExportReserveTypes"] == "1" ? true : false;
                //chkBxExpLOB.Checked = RM1099_Parms["Line_Of_Business"] == "1" ? true : false;

                //Bind Transaction type list
                ddlExpTransType.DataSource = objModel.dsTransTypes;
                ddlExpTransType.DataTextField = objModel.dsTransTypes.Tables[0].Columns[1].ToString();
                ddlExpTransType.DataValueField = objModel.dsTransTypes.Tables[0].Columns[0].ToString();
                ddlExpTransType.DataBind();
                ddlExpTransType.Items.Insert(0, "---Select---");

                //Bind Reserve type list
                ddlExpReserveType.DataSource = objModel.dsReserveTypes;
                ddlExpReserveType.DataTextField = objModel.dsReserveTypes.Tables[0].Columns[1].ToString();
                ddlExpReserveType.DataValueField = objModel.dsReserveTypes.Tables[0].Columns[0].ToString();
                ddlExpReserveType.DataBind();
                ddlExpReserveType.Items.Insert(0, "---Select---");

                //Bind LOB Codes list
                ddlExpLOB.DataSource = objModel.dsLOB;
                ddlExpLOB.DataTextField = objModel.dsLOB.Tables[0].Columns[1].ToString();
                ddlExpLOB.DataValueField = objModel.dsLOB.Tables[0].Columns[0].ToString();
                ddlExpLOB.DataBind();
                ddlExpLOB.Items.Insert(0, "---Select---");

                sTemp = RM1099_Parms["Transaction_Type"];

                if (sTemp.Length > 0)
                {
                    //The dataset recieved can be further filtered to get desired records. 
                    //Filtered records will move to Datarow[] array from where it can be 
                    //populated to list box.
                    DataRow[] drTransCodes = objModel.dsTransTypes.Tables[0].Select("CODE_ID IN (" + sTemp + ")");
                    foreach (DataRow OneRowTransCode in drTransCodes)
                    {
                        lstBxExpTransType.Items.Add(new ListItem(OneRowTransCode[1].ToString(), OneRowTransCode[0].ToString()));
                    }
                }

                sTemp = RM1099_Parms["Line_Of_Business"];
                if (sTemp.Length > 0)
                {
                    //The dataset recieved can be further filtered to get desired records. 
                    //Filtered records will move to Datarow[] array from where it can be 
                    //populated to list box.
                    DataRow[] drLOB = objModel.dsLOB.Tables[0].Select("CODE_ID IN (" + sTemp + ")");
                    foreach (DataRow OneRowLOB in drLOB)
                    {
                        lstBxExpLOB.Items.Add(new ListItem(OneRowLOB[1].ToString(), OneRowLOB[0].ToString()));
                    }
                }

                sTemp = RM1099_Parms["Reserve_Type"];
                if (sTemp.Length > 0)
                {
                    //The dataset recieved can be further filtered to get desired records. 
                    //Filtered records will move to Datarow[] array from where it can be 
                    //populated to list box.
                    DataRow[] drReserveCodes = objModel.dsReserveTypes.Tables[0].Select("CODE_ID IN (" + sTemp + ")");
                    foreach (DataRow OneRowReserveCode in drReserveCodes)
                    {
                        lstBxExpReserveType.Items.Add(new ListItem(OneRowReserveCode[1].ToString(), OneRowReserveCode[0].ToString()));
                    }
                }
                #endregion MITS 26117

            }
            catch (Exception eRM1099)
            {
                err.Add("DataIntegrator.Error", eRM1099.Message, BusinessAdaptorErrorType.Message);
                isError = true;
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }

        }

        protected void fRefresh()
        {
            TxtTotalPaid.Enabled = false;
            ChkDtRange.Checked = false;         //Tanu 05/06/2009: MITS 16005
            TxtDateFrom.BackColor = System.Drawing.Color.WhiteSmoke;
            TxtDateTo.BackColor = System.Drawing.Color.WhiteSmoke;
            TxtDateFrom.Enabled = false;
            TxtDateTo.Enabled = false;
            // RMA-6037 - ML changes by Sravan - Starts
           // btnCalendarFrom.Enabled = false;    //Tanu 05/06/2009: MITS 16005
           // btnCalendarTo.Enabled = false;      //Tanu 05/06/2009: MITS 16005
            //OptSelect.Items[0].Selected = true;
            //ddlSelectDynamic.Enabled = false;
            divFromDate.Visible = false;
            divToDate.Visible = false;
			//RMA-6037 ML changes by sravan end
        }

        protected void ChkDtRange_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkDtRange.Checked)
            {
                TxtFiscalYear.BackColor = System.Drawing.Color.WhiteSmoke;
                TxtFiscalYear.Enabled = false;
                TxtFiscalYear.Text = "";
                TxtDateFrom.Enabled = true;     //Tanu 05/06/2009: MITS 16005
                TxtDateTo.Enabled = true;       //Tanu 05/06/2009: MITS 16005
                TxtDateTo.BackColor = System.Drawing.Color.White;
                TxtDateFrom.BackColor = System.Drawing.Color.White;
                // RMA-6037 - ML changes by Sravan - Starts
                //btnCalendarFrom.Enabled = true;
                //btnCalendarTo.Enabled = true;
                divFromDate.Visible = true;
                divToDate.Visible = true;
                // RMA-6037 - ML changes by Sravan - End
            }
            else
            {
                TxtFiscalYear.BackColor = System.Drawing.Color.White;
                TxtFiscalYear.Enabled = true;       //Tanu 05/06/2009: MITS 16005
                TxtFiscalYear.Text = "";
                TxtDateFrom.BackColor = System.Drawing.Color.WhiteSmoke;
                TxtDateTo.BackColor = System.Drawing.Color.WhiteSmoke;
                TxtDateFrom.Enabled = false;
                TxtDateTo.Enabled = false;
                TxtDateFrom.Text = "";          //Tanu 05/06/2009: MITS 16005
                TxtDateTo.Text = "";            //Tanu 05/06/2009: MITS 16005
                // RMA-6037 - ML changes by Sravan - Starts
                //btnCalendarFrom.Enabled = false;
                //btnCalendarTo.Enabled = false;
                divFromDate.Visible = false;
                divToDate.Visible = false;				
                // RMA-6037 - ML changes by Sravan - End

            }
            fLoadEntities();
        }

        protected void chkBankAccount_CheckedChanged(object sender, EventArgs e)
        {
            //Vsoni5 : MITS 25790 : Moved the code to SyncPayerAndBankAccts function for reusability.
            SyncPayerAndBankAccts();
        }

        
        protected void BtnDelTrans14_Click(object sender, EventArgs e)
        {
            LstBox14.Items.Remove(new ListItem(LstBox14.SelectedItem.Text));
            fLoadEntities();
        }

        /// <summary>
        /// Event for save button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            try
            {
                SaveSettings();
            }
            catch (Exception ex)
            {
                //npradeepshar 01/17/2010 MITS 23304 : Log the error into log file as well display it on UI
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void ddlBox13_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Vsoni5 : MITS 25791 : Add Selected trans type in ListBox for Box 13 and 
            // remove it from Box 13 and Box 14 Dropdowns
            ListItem lstItem;            
            if (ddlBox13.SelectedItem.Text.Length > 0 && ddlBox13.SelectedIndex != 0)//11/20/2009 Anand:Condition Altered.
            {
                if(LstBox13.Items.FindByText(ddlBox13.SelectedItem.Text) == null)
                {
                    lstItem = new ListItem(ddlBox13.SelectedItem.Text, ddlBox13.SelectedItem.Value);
                    LstBox13.Items.Add(lstItem);
                    ddlBox13.Items.Remove(lstItem);
                    ddlBox14.Items.Remove(lstItem);
                }
            }
            //commented function call as it is not required.
            //fLoadEntities();
        }

        protected void ddlBox14_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Vsoni5 : MITS 25791 : Add Selected trans type in ListBox for Box 14 and 
            // remove it from Box 13 and Box 14 Dropdowns
            ListItem lstItem;
            if (ddlBox14.SelectedItem.Text.Length > 0 && ddlBox14.SelectedIndex != 0)//11/20/2009 Anand:Condition Altered.
            {
                if (LstBox14.Items.FindByText(ddlBox14.SelectedItem.Text) == null)
                {
                    lstItem = new ListItem(ddlBox14.SelectedItem.Text, ddlBox14.SelectedItem.Value);
                    LstBox14.Items.Add(lstItem);
                    ddlBox13.Items.Remove(lstItem);
                    ddlBox14.Items.Remove(lstItem);
                }
            }
        }
       

        //******************************************************************************
        //Developer: Jasmeet Singh Bawa
        //Revision Number:
        //Description: Save settings into the database
        //******************************************************************************
        private void SaveSettings()
        {
            //10/20/2009 Anand :Added Hidden field.
            if (JavaScriptcheck.Value == "Error")
            {
                JavaScriptcheck.Value = "Corrected";
                fLoadEntities();
                return;
            }
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModel = null;
            //Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objService = null;
            DataIntegratorModel objModel = null;
            bool isError = false;
            int iOptionSetID;
            string sTemp;
            string sTaskManagerXml;
            // Vsoni5 : MITS 25785 - Start
            DataSet dsPayer = null;
            Regex regSSN1 = new Regex(@"^\d{3}-\d{2}-\d{4}$");
            Regex regSSN2 = new Regex(@"^\d{2}-\d{7}$");
            // Vsoni5 : MITS 25785 - End

            BusinessAdaptorErrors err = new BusinessAdaptorErrors();    //vgupta20 08/05/2009 - initiate the error handler 
            bool bErr = false;

            sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
            iOptionset = Conversion.ConvertObjToInt(hdOptionsetId.Value);
            //objModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
            objModel = new DataIntegratorModel();
            if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
            {
                objModel.Token = AppHelper.GetSessionId();
            }

            //objModel.OptionSetID = iOptionset;
            //objModel.ModuleName = sModuleName;
            //objModel.TaskManagerXml = sTaskManagerXml;

            Dictionary<string, string> RM1099_Setting = new Dictionary<string, string>();

            sTemp = TxtOptionSetName.Text;

            sTemp = sTemp.Replace("'", "");
            if (sTemp.Length > 50)
            {
                sTemp = sTemp.Substring(1, 50);
            }
            objModel.OptionSetName = sTemp;
            if (sTemp == "")              // vgupta20 08/05/2009duplicate OptionSet name is checked below
            {
                err.Add("DataIntegrator.Error", "OptionSet Name cannot be blank", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            if (TxtFiscalYear.Enabled == true)      //vgupta20 08/05/2009 - Validate tax year 
            {
                if (TxtFiscalYear.Text.Trim() == "")
                {
                    err.Add("DataIntegrator.Error", "Tax Year cannot be blank", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                else
                {
                    //Hassan - 01/15/2010 - MITS 17531. Date shouldn't be less than 1900.
                    if ((System.Int32.Parse(TxtFiscalYear.Text.Trim()) < 1900)) //|| (System.Int32.Parse(TxtFiscalYear.Text.Trim()) > 2050)
                    {
                        err.Add("DataIntegrator.Error", "Invalid Tax Year", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                }
            }

            if (ChkDtRange.Checked)
            {
                if (TxtDateFrom.Text != "" && TxtDateTo.Text != "")
                {
                    RM1099_Setting.Add("Fiscal_Year", "0");    // Anand 09/24/2009 : Chnaged for correct xml order.filling it with 0 just to verify the selection 
                    //Date From  
                    sTemp = TxtDateFrom.Text;

                    //Hassan - 01/15/2010 - MITS 17531. From Date shouldn't be less than 01/01/1900. To date can be anything.
                    if (DateTime.Compare(Convert.ToDateTime(TxtDateFrom.Text), new DateTime(1900, 01, 01, 0, 0, 0)) < 0) //|| (System.Int32.Parse(TxtFiscalYear.Text.Trim()) > 2050)
                    {
                        err.Add("DataIntegrator.Error", "Invalid Tax Year", BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                    else
                    {
                        //sTemp = sTemp.Substring(6, 4) + sTemp.Substring(0, 2) + sTemp.Substring(3, 2);
                        sTemp = AppHelper.GetRMDate(sTemp);
                        RM1099_Setting.Add("Date_From", sTemp);

                        //Date To
                        sTemp = TxtDateTo.Text;
                        //sTemp = sTemp.Substring(6, 4) + sTemp.Substring(0, 2) + sTemp.Substring(3, 2);
                        sTemp = AppHelper.GetRMDate(sTemp);
                        RM1099_Setting.Add("Date_To", sTemp);
                    }
                }
                else
                {
                    err.Add("DataIntegrator.Error", "Please enter the date Range.", BusinessAdaptorErrorType.Message);
                    bErr = true;
                }
                //Fiscal Year
                //sTemp = TxtDateTo.Text;
                //sTemp = sTemp.Substring(6, 4);
                //RM1099_Setting.Add("Fiscal_Year", sTemp);
                //Tanu: MITS 17010
                //RM1099_Setting.Add("Fiscal_Year", "0");    Anand 09/24/2009 : Chnaged for correct xml order.//filling it with 0 just to verify the selection                   
            }
            else
            {
                sTemp = TxtFiscalYear.Text;
                RM1099_Setting.Add("Fiscal_Year", sTemp);    //Anand 09/24/2009: MITS 17420.Changing the order once again to run the DA job correctly.     //vgupta20 08/05/2009: The order should be: "FromDate", "ToDate", "FiscalYear"
                RM1099_Setting.Add("Date_From", sTemp + "0101");    //needs to fill as it required in processing
                RM1099_Setting.Add("Date_To", sTemp + "1231");      // but it would not show on page
                //RM1099_Setting.Add("Fiscal_Year", sTemp);
            }

            //Payee File Path
            RM1099_Setting.Add("Payee_File", "");

            //Payer File Path
            RM1099_Setting.Add("Payer_File", "");

            //Data File Path
            RM1099_Setting.Add("Data_File", "");

            //Log File Path
            RM1099_Setting.Add("Log_File", "");

            //Payers_To_Export
            sTemp = LstPayer_lst.Text;
            sTemp = sTemp.Trim();

            if (sTemp == "")        //vgupta20 08/05/2009 - Payer field cannot be left blank
            {
                err.Add("DataIntegrator.Error", "Payer field cannot be blank", BusinessAdaptorErrorType.Message);
                bErr = true;
            }

            sTemp = sTemp.Replace("  ", " ");
            sTemp = sTemp.Replace(" ", ",");
            RM1099_Setting.Add("Payers_To_Export", sTemp);
            // Vsoni5 : MITS 25785 - Start
            if (!sTemp.Equals(string.Empty))
            {
                //dsPayer = objService.GetTaxIds(objModel, sTemp);
                objModel.EntityList = sTemp;
                objModel.ClientId = AppHelper.ClientId;
                dsPayer = AppHelper.GetResponse<DataSet>("RMService/DAIntegration/gettaxids", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                foreach (DataRow drPayer in dsPayer.Tables[0].Rows)
                {
                    if (drPayer["TAX_ID"].ToString().Equals(String.Empty))
                    {
                        err.Add("DataIntegrator.Error",
                            string.Format("TaxID is blank for payer {1}", drPayer["TAX_ID"], drPayer["ABBREVIATION"] + "-" + drPayer["LAST_NAME"]),
                            BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                    else if (!regSSN1.IsMatch(drPayer["TAX_ID"].ToString()) && !regSSN2.IsMatch(drPayer["TAX_ID"].ToString()))
                    {
                        err.Add("DataIntegrator.Error",
                            string.Format("TaxID : {0}, is not valid for payer {1}", drPayer["TAX_ID"], drPayer["ABBREVIATION"] + "-" + drPayer["LAST_NAME"]),
                            BusinessAdaptorErrorType.Message);
                        bErr = true;
                    }
                }
            }
            
            if (bErr == true)       //vgupta20 08/05/2009 - return page if any errors found
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                //akishore 09/17/2009: MITS 17000.
                fLoadEntities();
                return;
            }
            // Vsoni5 : MITS 25785 - End

            //Bank_Accounts vchaturvedi2 MITS-19820
            sTemp = "";
            for (int iCnt = 0; iCnt < LstBankAcc.Items.Count; iCnt++)
            {
                //if(LstBankAcc.Items[iCnt].Selected) 
                //{MITS 23731: each value selected should be saved irrespective of whether it is selected or not.
                sTemp = sTemp + LstBankAcc.Items[iCnt].Value + ",";
                //}            

            }
            if (sTemp.Length > 0)
            {
                sTemp = sTemp.Substring(0, (sTemp.Length) - 1);
            }
            sTemp = sTemp.Replace(" ", ",");
            RM1099_Setting.Add("Bank_Accounts", sTemp);

            //Export Printed Payments
            if (chkPrintedPaymentsOnly.Checked)
            {
                RM1099_Setting.Add("Export_Printed_Payments_Only", "1");
            }
            else
            {
                RM1099_Setting.Add("Export_Printed_Payments_Only", "0");
            }

            //Export_Payments_Without_Claim
            if (ChkList1.Items[0].Selected)
            {
                RM1099_Setting.Add("Export_Payments_Without_Claim", "1");
            }
            else
            {
                RM1099_Setting.Add("Export_Payments_Without_Claim", "0");
            }

            //Exclude_Payments_To_OrgHier
            if (ChkList1.Items[1].Selected)
            {
                RM1099_Setting.Add("Exclude_Payments_To_OrgHier", "1");
            }
            else
            {
                RM1099_Setting.Add("Exclude_Payments_To_OrgHier", "0");
            }

            //Exclude_Payments_To_Claimants
            if (ChkList1.Items[2].Selected)
            {
                RM1099_Setting.Add("Exclude_Payments_To_Claimants", "1");
            }
            else
            {
                RM1099_Setting.Add("Exclude_Payments_To_Claimants", "0");
            }

            //Exclude_Deleted_Entities
            if (ChkList1.Items[3].Selected)
            {
                RM1099_Setting.Add("Exclude_Deleted_Entities", "1");
            }
            else
            {
                RM1099_Setting.Add("Exclude_Deleted_Entities", "0");
            }

            //Export_Payees_With_TotalPaid
            if (chkTotalPaid.Checked)
            {
                RM1099_Setting.Add("Export_Payees_With_TotalPaid", TxtTotalPaid.Text);
            }
            else
            {
                RM1099_Setting.Add("Export_Payees_With_TotalPaid", "0");
            }

            // Vsoni5 : MITS 25786 : 
            //Roll Up checkbox moved from checkbox list to a seperate control.
            if (chkBxRollUpPymt.Checked)
            {
                RM1099_Setting.Add("RollUp_Payments", "1");
            }
            else
            {
                RM1099_Setting.Add("RollUp_Payments", "0");
            }

            //Vsoni5 - MITS 25787 : uncommented the existing code for IgnoreTaxIDErros checkbox
            if (ChkList2.Items[1].Selected)
            {
                RM1099_Setting.Add("Ignore_TaxID_Errors", "1");
            }
            else
            {
                RM1099_Setting.Add("Ignore_TaxID_Errors", "0");
            }

            //Check_RM1099_Flag
            //if (ChkList2.Items[1].Selected)npradeepshar 02/17/2011 MITS:23729 Changed position of checkboxes
            if (ChkList1.Items[4].Selected)
            {
                RM1099_Setting.Add("Check_RM1099_Flag", "1");
            }
            else
            {
                RM1099_Setting.Add("Check_RM1099_Flag", "0");
            }

            //Include_EID_In_Payee_File
            //if (ChkList2.Items[2].Selected)
            if (ChkList1.Items[5].Selected)
            {
                RM1099_Setting.Add("Include_EID_In_Payee_File", "1");
            }
            else
            {
                RM1099_Setting.Add("Include_EID_In_Payee_File", "0");
            }
            # region MITS 26117
            if (!chkBxExpAllPymtTxYr.Checked)
            {
                // Export All Payments for Tax year
                RM1099_Setting.Add("ExportAllPymts", "0");
                //Transaction_Type
                sTemp = "";
                for (int icnt = 0; icnt < lstBxExpTransType.Items.Count; icnt++)
                {
                    sTemp = sTemp + lstBxExpTransType.Items[icnt].Value + ",";
                }
                if (sTemp.Length > 0)
                {
                    sTemp = sTemp.Substring(0, (sTemp.Length) - 1);
                }
                RM1099_Setting.Add("Transaction_Type", sTemp);
                
                //Reserve_Type
                sTemp = "";
                for (int icnt = 0; icnt < lstBxExpReserveType.Items.Count; icnt++)
                {
                    sTemp = sTemp + lstBxExpReserveType.Items[icnt].Value + ",";
                }
                if (sTemp.Length > 0)
                {
                    sTemp = sTemp.Substring(0, (sTemp.Length) - 1);
                }
                RM1099_Setting.Add("Reserve_Type", sTemp);                                             
            }
            else
            {
                RM1099_Setting.Add("ExportAllPymts", "1");
                RM1099_Setting.Add("Transaction_Type", string.Empty);
                RM1099_Setting.Add("Reserve_Type", string.Empty);
            }

            //Line_Of_Business : Must be passed even if Export All Payees for Tax Year check box is selected
            sTemp = "";
            for (int icnt = 0; icnt < lstBxExpLOB.Items.Count; icnt++)
            {
                sTemp = sTemp + lstBxExpLOB.Items[icnt].Value + ",";
            }
            if (sTemp.Length > 0)
            {
                sTemp = sTemp.Substring(0, (sTemp.Length) - 1);
            }
            RM1099_Setting.Add("Line_Of_Business", sTemp);   

            //Transaction_Type_Box13
            sTemp = "";
            if (LstBox13.Items.Count > 0)
            {
                for (int icnt = 0; icnt < LstBox13.Items.Count; icnt++)
                {
                    sTemp = sTemp + LstBox13.Items[icnt].Value + ",";
                }
                if (sTemp.Length > 0)
                {
                    sTemp = sTemp.Substring(0, (sTemp.Length) - 1);
                }
                RM1099_Setting.Add("Transaction_Type_Box13", sTemp);
            }
            else
            {
                RM1099_Setting.Add("Transaction_Type_Box13", sTemp);
            }

            //Transaction_Type_Box14
            sTemp = "";
            if (LstBox14.Items.Count > 0)
            {
                for (int icnt = 0; icnt < LstBox14.Items.Count; icnt++)
                {
                    sTemp = sTemp + LstBox14.Items[icnt].Value + ",";
                }
                if (sTemp.Length > 0)
                {
                    sTemp = sTemp.Substring(0, (sTemp.Length) - 1);
                }
                RM1099_Setting.Add("Transaction_Type_Box14", sTemp);
            }
            else
            {
                RM1099_Setting.Add("Transaction_Type_Box14", sTemp);
            }

            #endregion MITS 26117
            //start npradeepshar 02/17/2011 MITS:23729 added medical reserve Types
            sTemp = "";
            if (lstReserveTypeBox6.Items.Count > 0)
            {
                for (int icnt = 0; icnt < lstReserveTypeBox6.Items.Count; icnt++)
                {
                    sTemp = sTemp + lstReserveTypeBox6.Items[icnt].Value + ",";
                }
                if (sTemp.Length > 0)
                {
                    sTemp = sTemp.Substring(0, (sTemp.Length) - 1);
                }
                RM1099_Setting.Add("Medical_Reserve_Type_Box6", sTemp);
            }
            else
            {
                RM1099_Setting.Add("Medical_Reserve_Type_Box6", sTemp);
            }
            //end npradeepshar 02/17/2011 MITS:23729 added medical reserve Types
            // npradeepshar 02/18/2011 MITS 23753  added 'use double quotes
            if (ChkList2.Items[0].Selected)
            {
                RM1099_Setting.Add("Use_Double_Quotes", "1");
            }
            else
            {
                RM1099_Setting.Add("Use_Double_Quotes", "0");
            }//End Mits 23753
            objModel.Parms = RM1099_Setting;
            objModel.OptionSetID = iOptionset;
            objModel.ModuleName = sModuleName;
            objModel.TaskManagerXml = sTaskManagerXml;                                    
           
            //akishore 09/17/2009: MITS 17000.
            //ipuri REST Service Conversion 12/06/2014 Start
            //objModel = objService.SaveSettings(objModel);
            objModel.ClientId = AppHelper.ClientId;

            objModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/savesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
            //ipuri REST Service Conversion 12/06/2014 End
            if (objModel.OptionSetID == -1)
            {

                //vgupta20 08/05/2009 - All errors to be dislayed at the top
                //formdemotitle.Text = "Optionset name already exists.";
                TxtOptionSetName.Focus();   //Vsoni5: MITS 17012
                err.Add("DataIntegrator.Error", "Optionset name already exists", BusinessAdaptorErrorType.Message);
                bErr = true;
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                fLoadEntities();

            }


            ////akishore 09/17/2009: MITS 17000.
            //objModel = objService.SaveSettings(objModel);
            if (objModel.OptionSetID > 0)
            {
                Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx");
            }

        }

        protected void BtnDelDynamic_Click(object sender, ImageClickEventArgs e)
        {
            //if (LstSelectDynamic.SelectedItem != null)
            //{
            //    LstSelectDynamic.Items.Remove(new ListItem(LstSelectDynamic.SelectedItem.Text, LstSelectDynamic.SelectedItem.Value));
            //}
            fLoadEntities();
        }

        protected void BtnDelBox13_Click(object sender, ImageClickEventArgs e)
        {
            // Vsoni5 : 09/20/2011 : MITS 25791 - Modified function defination
            ListItem lstItem;
            if (LstBox13.SelectedItem != null)
            {
                lstItem = new ListItem(LstBox13.SelectedItem.Text, LstBox13.SelectedItem.Value);
                LstBox13.Items.Remove(lstItem);
                ddlBox13.Items.Add(lstItem);
                ddlBox14.Items.Add(lstItem);
                SyncTransTypeBoxes();
            }
            //fLoadEntities();
        }

        protected void BtnDelBox14_Click(object sender, ImageClickEventArgs e)
        {
            // Vsoni5 : 09/20/2011 : MITS 25791 - Modified function defination
            ListItem lstItem;
            if (LstBox14.SelectedItem != null)
            {
                lstItem = new ListItem(LstBox14.SelectedItem.Text, LstBox14.SelectedItem.Value);
                LstBox14.Items.Remove(lstItem);
                ddlBox14.Items.Add(lstItem);
                ddlBox13.Items.Add(lstItem);
                SyncTransTypeBoxes();
            }
            //fLoadEntities();
        }

        /// <summary>
        /// Vsoni5 : 09/20/2011 : MITS 25791
        /// Add the deleted TransTypes from Box 13 & 14 back to Trans Type 13 & 14 dropdowns.
        /// </summary>
        /// <param name="lstItem"></param>
        private void SyncTransTypeBoxes()
        {           
            ddlBox13.Items.RemoveAt(0);
            SortDropDown(ddlBox13);
            ddlBox13.Items.Insert(0, "---Select---");

            ddlBox14.Items.RemoveAt(0);
            SortDropDown(ddlBox14);
            ddlBox14.Items.Insert(0, "---Select---");
        }

        /// <summary>
        /// Vsoni5 : 09/20/2011 : MITS 25791
        /// Sort the dropdown list alphabetically.
        /// </summary>
        /// <param name="ddlToSort"></param>
        private void SortDropDown(DropDownList ddlToSort)
        {
            Dictionary<String, String> dcTransType = new Dictionary<string,string> ();

            foreach (ListItem liTemp in ddlToSort.Items)
            {
                dcTransType.Add(liTemp.Text, liTemp.Value);
            }
            ddlToSort.Items.Clear();
            ddlToSort.DataSource = dcTransType.OrderBy(transType => transType.Key);
            ddlToSort.DataTextField = "Key";
            ddlToSort.DataValueField = "value";
            ddlToSort.DataBind();

        }
        // Vsoni5 : MITS 25786 : Commented this function as it is not required now.
        //protected void chkTotalPaid_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkTotalPaid.Checked)
        //    {
        //        TxtTotalPaid.Text = "";
        //        TxtTotalPaid.Enabled = true;
        //        //ChkList2.Items[0].Selected = true;
        //        TxtTotalPaid.Focus();
        //    }
        //    else
        //    {
        //        TxtTotalPaid.Text = "";
        //        TxtTotalPaid.Enabled = false;
        //    }
        //    fLoadEntities();
        //}

        protected void BtnDelAccount_Click(object sender, ImageClickEventArgs e)
        {
            if (LstBankAcc.SelectedItem != null)
            {
                LstBankAcc.Items.Remove(new ListItem(LstBankAcc.SelectedItem.Text, LstBankAcc.SelectedItem.Value));
            }
            fLoadEntities();
        }

        protected void BtnDelPayer_Click(object sender, ImageClickEventArgs e)
        {
            if (LstPayer.SelectedItem != null)
            {
                LstPayer_lst.Text = LstPayer_lst.Text.Replace(LstPayer.SelectedItem.Value.ToString(), "");
                LstPayer.Items.Remove(new ListItem(LstPayer.SelectedItem.Text, LstPayer.SelectedItem.Value));
            }
            //Vsoni5 : MITS 25790 : Replaced fLoadEntities function call with SyncPayerAndBankAccts.
            SyncPayerAndBankAccts();
        }

        //******************************************************************************
        //Developer: Jasmeet Singh Bawa
        //Revision Number:
        //Description: Load entities into Payer listBox
        //******************************************************************************
        protected void fLoadEntities()
        {
            int iCnt = 0;
            string sTemp = "";
            string[] aTemp;
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModel = null;
            //DataIntegratorService.DataIntegratorServiceClient objService = null;
            DataIntegratorModel objModel = null;

            bool isError = false;
            try
            {
                sTemp = LstPayer_lst.Text;
                sTemp = sTemp.Trim();

                if (sTemp != "")
                {
                    //objModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                    objModel = new DataIntegratorModel();
                    if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                    {
                        objModel.Token = AppHelper.GetSessionId();
                    }
                    objModel.ModuleName = sModuleName;
                    objModel.OptionSetID = iOptionset;

                    //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objModel = objService.RetrieveSettings(objModel);
                    objModel.ClientId = AppHelper.ClientId;

                    objModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                    //ipuri REST Service Conversion 12/06/2014 End

                    sTemp = sTemp.Replace("  ", " ");
                    sTemp = sTemp.Replace(" ", ",");
                    DataRow[] drPayer = objModel.dsEntity.Tables[0].Select(" ENTITY_ID IN (" + sTemp + ") ");
                    sTemp = "";
                    LstPayer.Items.Clear();
                    foreach (DataRow OneRowPayer in drPayer)
                    {
                        LstPayer.Items.Add(new ListItem(OneRowPayer[1].ToString() + "-" + OneRowPayer[2].ToString(), OneRowPayer[0].ToString()));
                        sTemp = sTemp + " " + OneRowPayer[0].ToString();
                    }
                    LstPayer_lst.Text = sTemp.Trim();
                }
            }
            catch (Exception eRM1099)
            {
            }

        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx");
        }

        protected void ChkList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Tanu : MITS 17006
            //for Ingore Tax ID Confirmation 
            //Vsoni5 - MITS 25787 : uncommented the existing code for IgnoreTaxIDErros checkbox
            if (ChkList2.Items[1].Selected == true)
            {
                ChkList2.Items[1].Attributes.Add("onclick", "IgnoreTaxIDConfirm();");
            }

            //vgupta20 08/05/2009 -- This validation check is not required.
            ////for Export payees based on RM1099 Reportable confirmation
            //if (ChkList2.Items[2].Selected == true)
            //{
            //    ChkList2.Items[2].Attributes.Add("onclick", "ExportPayeesConfirm();");
            //}

            //kdchoudhary 12/06/2012 MITS 30661 --start  commented the below code
            //if (ChkList2.Items[0].Selected == false)
            //{
            //    chkTotalPaid.Checked = false;
            //    TxtTotalPaid.Enabled = false;
            //    TxtTotalPaid.Text = "";
            //}
            //kdchoudhary 12/06/2012 MITS 30661 --end
            fLoadEntities();
        }

        //Tanu : MITS 17006
        protected void ChkList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //for Export Without claim link
            if (ChkList1.Items[0].Selected == true)
            {
                ChkList1.Items[0].Attributes.Add("onclick", "ExporPaymentsConfirm();");
            }

            fLoadEntities();//vchaturvedi2 MITS 21794
        }

        /// <summary>
        /// //npradeepshar 02/17/2011 MITS:23729 added medical reserve Types
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlReserveTypeBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstReserveTypeBox6.Items.Remove(new ListItem(ddlReserveTypeBox6.SelectedItem.Text, ddlReserveTypeBox6.SelectedItem.Value));
            if (ddlReserveTypeBox6.SelectedItem.Text.Length > 0 & ddlReserveTypeBox6.SelectedIndex != 0)
            {
                lstReserveTypeBox6.Items.Add(new ListItem(ddlReserveTypeBox6.SelectedItem.Text, ddlReserveTypeBox6.SelectedItem.Value));
            }
            fLoadEntities();
        }

        protected void imgbtnReserveTypeBox6_Click(object sender, ImageClickEventArgs e)
        {
            if (lstReserveTypeBox6.SelectedItem != null)
            {
                lstReserveTypeBox6.Items.Remove(new ListItem(lstReserveTypeBox6.SelectedItem.Text, lstReserveTypeBox6.SelectedItem.Value));
            }
            fLoadEntities();

        }

        /// <summary>
        /// Vsoni5 : MITS 26117 : Add selected Transaction type to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlExpTransType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem lstItemTransType;
            if (ddlExpTransType.SelectedIndex > 0 && lstBxExpTransType.Items.FindByText(ddlExpTransType.SelectedItem.Text) == null)
            {
                lstItemTransType = new ListItem(ddlExpTransType.SelectedItem.Text, ddlExpTransType.SelectedItem.Value);
                lstBxExpTransType.Items.Add(lstItemTransType);
            }
        }

        /// <summary>
        /// Vsoni5 : MITS 26117 : Add selected Reserve type to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlExpReserveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem lstItemResType;
            if (ddlExpReserveType.SelectedIndex > 0 && lstBxExpReserveType.Items.FindByText(ddlExpReserveType.SelectedItem.Text) == null)
            {
                lstItemResType = new ListItem(ddlExpReserveType.SelectedItem.Text, ddlExpReserveType.SelectedItem.Value);
                lstBxExpReserveType.Items.Add(lstItemResType);
            }
        }

        /// <summary>
        /// Vsoni5 : MITS 26117 : Add selected LOB codes to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlExpLOB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem lstItemLOB;
            if (ddlExpLOB.SelectedIndex > 0 && lstBxExpLOB.Items.FindByText(ddlExpLOB.SelectedItem.Text) == null)
            {
                lstItemLOB = new ListItem(ddlExpLOB.SelectedItem.Text, ddlExpLOB.SelectedItem.Value);
                lstBxExpLOB.Items.Add(lstItemLOB);
            }
        }

        /// <summary>
        /// Vsoni5 : MITS 26117 : Remove selected LOB Codes from the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgBtnDelLOB_Click(object sender, ImageClickEventArgs e)
        {
            if (lstBxExpLOB.SelectedItem != null)
            {
                lstBxExpLOB.Items.Remove(new ListItem(lstBxExpLOB.SelectedItem.Text, lstBxExpLOB.SelectedItem.Value));
            }
        }

        /// <summary>
        ///  Vsoni5 : MITS 26117 : Remove selected Reserve types from the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgBtnDelResTypes_Click(object sender, ImageClickEventArgs e)
        {
            if (lstBxExpReserveType.SelectedItem != null)
            {
                lstBxExpReserveType.Items.Remove(new ListItem(lstBxExpReserveType.SelectedItem.Text, lstBxExpReserveType.SelectedItem.Value));
            }
        }

        /// <summary>
        ///  Vsoni5 : MITS 26117 : Remove selected Transaction types from the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgBtnDelTransTypes_Click(object sender, ImageClickEventArgs e)
        {
            if (lstBxExpTransType.SelectedItem != null)
            {
                lstBxExpTransType.Items.Remove(new ListItem(lstBxExpTransType.SelectedItem.Text, lstBxExpTransType.SelectedItem.Value));
            }
        }

        //Vsoni5 : 09/19/2011 : MITS 26161
        #region Add Tooltips to List Controls
        protected void ddlExpTransType_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem lstItem in ddlExpTransType.Items)
            {
                lstItem.Attributes.Add("title", lstItem.Text);
            }
        }

        protected void ddlExpReserveType_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem lstItem in ddlExpReserveType.Items)
            {
                lstItem.Attributes.Add("title", lstItem.Text);
            }
        }

        protected void ddlExpLOB_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem lstItem in ddlExpLOB.Items)
            {
                lstItem.Attributes.Add("title", lstItem.Text);
            }
        }

        protected void ddlReserveTypeBox6_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem lstItem in ddlReserveTypeBox6.Items)
            {
                lstItem.Attributes.Add("title", lstItem.Text);
            }
        }

        protected void ddlBox13_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem lstItem in ddlBox13.Items)
            {
                lstItem.Attributes.Add("title", lstItem.Text);
            }
        }

        protected void ddlBox14_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem lstItem in ddlBox14.Items)
            {
                lstItem.Attributes.Add("title", lstItem.Text);
            }
        }
        #endregion

        /// <summary>
        /// Vsoni5 : MITS 25790 : Added this function and moved the code from chkBankAccount_CheckedChanged to this function
        /// This function will be reused for Synchronizing Bank Account list with the Selected Payer List
        /// </summary>
        private void SyncPayerAndBankAccts()
        {
            string sTemp;
            fLoadEntities();
            if (chkBankAccount.Checked)
            {
                //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModel = null;
                //Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient objService = null;
                DataIntegratorModel objModel = new DataIntegratorModel();
                LstBankAcc.Items.Clear();
                try
                {
                    //objModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                    objModel = new DataIntegratorModel();
                    if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                    {
                        objModel.Token = AppHelper.GetSessionId();
                    }

                    objModel.OptionSetID = iOptionset;
                    objModel.ModuleName = sModuleName;

                    //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
                    
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objModel = objService.RetrieveSettings(objModel);
                    objModel.ClientId = AppHelper.ClientId;

                    objModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                    sTemp = LstPayer_lst.Text;
                    sTemp = sTemp.Trim();
                    sTemp = sTemp.Replace("  ", " ");
                    sTemp = sTemp.Replace(" ", ",");
                    DataRow[] drBankAccount = objModel.dsAccount.Tables[0].Select("OWNER_EID IN (" + sTemp + ")");
                    foreach (DataRow drBA in drBankAccount)
                    {
                        LstBankAcc.Items.Add(new ListItem(drBA[0].ToString(), drBA[1].ToString()));
                    }
                    tdLstExpBkAccts.Disabled = false;
                }
                catch (Exception eRM1099)
                {
                }
            }
            else
            {
                LstBankAcc.Items.Clear();
                tdLstExpBkAccts.Disabled = true;
            }
            
        }
    }

}


