<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimExportOrgGrid.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.TaskManager.ClaimExportOrgGrid" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Org Hierarchy Related Info</title>

    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js">        { var i; }</script>
    <%--vkumar258 - RMA-6037 - Starts --%>

    <%-- <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>--%>
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>


    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js">        { var i; }</script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

    <script language="javascript" type="text/javascript">
        function fPassOptionsetIDtoParentPage(sOptionset) {
            //debugger;

            var hdOptionset = window.opener.document.getElementById('hdOptionset');
            var hdOptionsetpopup = document.getElementById('hdOptionsetpopup');

            hdOptionset.value = hdOptionsetpopup.value;
            window.opener.document.forms[0].submit();
            window.close();
        }

//        function fSelectOrg() {
//            debugger;
//            alert("1");
//            var Org = document.getElementById('Org');
//            var OrgBtn = document.getElementById('');
//            if (Org.value != "" || Org.value != null) {
//                alert(Org.value);
//                OrgBtn.style.visibility = "hidden";
//            }
//        }

        function Validate() {
            debugger;
            var Org = document.getElementById('Org');
            var FromDate = document.getElementById('FromDate');
            var ToDate = document.getElementById('ToDate');

//            if (Org.options[0].value == "")
//                alert("Select one Org Entity");
            if (FromDate.value == "") {
                if (ToDate.value == "") {
                    alert("Provide CLAIM FROM DATE and CLAIM TO DATE");
                }
                else {
                    alert("Provide CLAIM FROM DATE");
                }
                document.execCommand('Stop');

                window.stop;
            }
            else {
                if (ToDate.value == "") {
                    alert("Provide CLAIM TO DATE");
                    document.execCommand('Stop');

                    window.stop;
                }                
            }            
        }

    </script>

</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Org Hierarchy Related Info" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    
    <br />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/ClaimExportOrgList/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />

    <div id="div_Active" class="full">
        <asp:Label runat="server" class="required" ID="lbl_Active" 
            Text="Org Hierarchy Active : " Width="150px" />
        <span>
            <%--<asp:TextBox runat="server" onchange="setDataChanged(true);" ID="Description" RMXRef="/Instance/Document/Holidays/control[@name ='Desc']"
                TabIndex="3" />--%>
            <asp:CheckBox runat="server" ID="cbOrgActive" Text="" Checked="True" RMXRef="/Instance/Document/ClaimExportOrgList/control[@name ='Active']"/>
        </span>
    </div>

    <div id="div_OrgId" class="full">
        <asp:Label runat="server" class="required" ID="lblOrgId" 
            Text="Org Hierarchy Level : " Width="150px" />
        <span>
            <%--<asp:ListBox SelectionMode="Single" Rows="3" orgid="" ID="Org" runat="server" RMXRef="/Instance/Document/ClaimExportOrgList/control[@name ='Org Hierarchy']" />--%>
            <asp:TextBox ID="Org" runat="server"  RMXRef="/Instance/Document/ClaimExportOrgList/control[@name ='Org Hierarchy']" />
            <asp:TextBox Style="display: none" ID="Org_lst" runat="server" />
            <asp:Button ID="Orgbtn" OnClientClick="return selectCode('orgh','Org','ALL')" Text="Org Hierarchy" runat="server" />
            <%--OnClientClick="return selectCode('orgh','Org','ALL')"--%>
            <%--<asp:Button ID="Orgbtndel" OnClientClick="return deleteSelCode('Org')" Text="-" runat="server" />--%>
            <asp:TextBox Style="display: none" ID="Org_cid" runat="server" />
            <asp:TextBox Style="display: none" ID="Org_ceorglist" runat="server" RMXRef="/Instance/Document/ClaimExportOrgList/control[@name ='Org_orglist']" />
            <asp:TextBox Style="display: none" ID="OrgId" runat="server" />
        </span>
    </div>

    <div id="div_ToDate" class="full">
        <asp:Label runat="server" class="required" ID="lblClaimFromDate" 
            Text="Claim From Date : " Width="150px" />
            <%--class="required"--%>
        <span>
            <asp:TextBox runat="server" FormatAs="date" ID="FromDate" RMXRef="/Instance/Document/ClaimExportOrgList/control[@name ='Claim From Date']"
                TabIndex="1" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                <%--vkumar258 - RMA-6037 - Starts --%>
                <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnFromDate" TabIndex="2" />

            <script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "FromDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btnFromDate"
					    }
					    );
            </script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#FromDate").datepicker({
                            showOn: "button",
                            buttonImage: "../../../../Images/calendar.gif",
                           // buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");
                    });
                </script>
                <%--vkumar258 - RMA_6037- End--%>

        </span>
    </div>

    <div id="div_FromDate" class="full">
        <asp:Label runat="server" class="required" ID="lblClaimToDate" 
            Text="Claim To Date : " Width="150px" />
            <%--class="required"--%>
        <span>
            <asp:TextBox runat="server" FormatAs="date" ID="ToDate" RMXRef="/Instance/Document/ClaimExportOrgList/control[@name ='Claim To Date']"
                TabIndex="3" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                <%--vkumar258 - RMA-6037 - Starts --%>
                <%--            <asp:Button class="DateLookupControl" runat="server" ID="btnToDate" TabIndex="2" />--%>
                <%--<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" 
                    errormessage="Please Enter To Date" controltovalidate="ToDate" />--%>

                <%--<script type="text/javascript">
                Zapatec.Calendar.setup(
					    {
					        inputField: "ToDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btnToDate"
					    }
					    );
            </script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#ToDate").datepicker({
                            showOn: "button",
                            buttonImage: "../../../../Images/calendar.gif",
                           // buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
                    });
                </script>
                <%--vkumar258 - RMA_6037- End--%>

        </span>
    </div>
    
    <br />
    <%--<br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />--%>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButton">  <!-- MITS Changed class from 'FromButtonGroup' which uses absolute positioning  -->
        <div class="formButton" id="div_btnOk">
            <%--<asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return Holidays_onOk();"
                OnClick="btnOk_Click" />--%>
                <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px"  OnClick="btnOk_Click" />
                <%--OnClientClick="Validate();"--%>
        </div>
        <div class="formButton" id="div_btnCancel">
            <%--<asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return Holidays_onCancel();" OnClick="btnCancel_Click" />--%>
                <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:pleasewaitdialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
     <asp:HiddenField ID="hdOptionsetpopup" runat="server" />
    </form>
</body>
</html>
