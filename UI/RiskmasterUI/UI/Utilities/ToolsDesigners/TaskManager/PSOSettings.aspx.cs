﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;    //ipuri 12/07/2014

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager
{
    public partial class PSOSettings : System.Web.UI.Page
    {
        string sFromDateDefaultValue = AppHelper.GetDate("01/01/1999");
        string sDefaultUpdatedByUser = "DA_PSO";
        string sToDateDefaultValue =AppHelper.GetDate(Convert.ToString(DateTime.Now));
        private int iOptionSetIDParam;
        private const string sModuleName = "PSO";
        private string PageID = RMXResourceManager.RMXResourceProvider.PageId("PSOSettings.aspx");            //Yukti-ML Changes MITS 34000
        protected void Page_Load(object sender, EventArgs e)
        {

            //Start - Yukti-ML Changes MITS 34000
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //Yukti-ML Changes End

            if (!IsPostBack)
            {
                hdTaskManagerXml.Value = Server.HtmlEncode(CommonFunctions.GetTaskManagerXml(Request));
                iOptionSetIDParam = CommonFunctions.OptionsetId;
                hdOptionsetId.Value = Conversion.ConvertObjToStr(CommonFunctions.OptionsetId);
                RetriveSettingsFromDB();
            }
        }
        private void RetriveSettingsFromDB()
        {
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objReturnModel = null;
            //DataIntegratorService.DataIntegratorServiceClient objService = null;
            DataIntegratorModel objReturnModel = null;
            BusinessAdaptorErrors err = null;
            bool bErr = false;
            Dictionary<string, string> PSO_Setting = new Dictionary<string, string>();

            try
            {
                //objReturnModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
                objReturnModel = new DataIntegratorModel();
                err = new BusinessAdaptorErrors();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objReturnModel.Token = AppHelper.GetSessionId();
                }
                //objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();

                objReturnModel.OptionSetID = iOptionSetIDParam;


                objReturnModel.ModuleName = sModuleName;
                try
                {
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objReturnModel = objService.RetrieveSettings(objReturnModel);
                    objReturnModel.ClientId = AppHelper.ClientId;

                    objReturnModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/retrievesettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objReturnModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                    PSO_Setting = (Dictionary<string, string>)objReturnModel.Parms;
                    if (PSO_Setting.Count > 0)
                    {
                        txtExportOptionset.Text = objReturnModel.OptionSetName;
                        txtProviderId.Text = PSO_Setting["PROVIDER_ID"];
                        txtPSOId.Text = PSO_Setting["PSO_ID"];
                        txtCompany_cid.Text = PSO_Setting["COMPANY_ID"];
                        if (string.IsNullOrEmpty(PSO_Setting["EVENTDATE_FROM"]))
                        {
                            txtEvtDateFrom.Text = sFromDateDefaultValue;
                        }
                        else
                        {
                            txtEvtDateFrom.Text = AppHelper.GetDate(PSO_Setting["EVENTDATE_FROM"]);
                        }
                        if (iOptionSetIDParam > 0)
                        {
                            if (string.IsNullOrEmpty(PSO_Setting["EVENTDATE_TO"]))
                            {
                                txtEvtDateTo.Text = string.Empty;
                            }
                            else
                            {
                                txtEvtDateTo.Text = AppHelper.GetDate(PSO_Setting["EVENTDATE_TO"]);
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(PSO_Setting["EVENTDATE_TO"]))
                            {
                                txtEvtDateTo.Text = AppHelper.GetDate(Conversion.GetDate(sToDateDefaultValue));
                            }
                        }
                        
                        if (string.IsNullOrEmpty(PSO_Setting["EVENTDATEADDED_FROM"]))
                        {
                            txtEvtDateAddedFrm.Text = sFromDateDefaultValue;
                        }
                        else
                        {
                            txtEvtDateAddedFrm.Text = AppHelper.GetDate(PSO_Setting["EVENTDATEADDED_FROM"]);
                        }

                        if (iOptionSetIDParam > 0)
                        {
                            if (string.IsNullOrEmpty(PSO_Setting["EVENTDATEADDED_TO"]))
                            {
                                txtEvtDateAddedTo.Text = string.Empty;
                            }
                            else
                            {
                                txtEvtDateAddedTo.Text = AppHelper.GetDate(PSO_Setting["EVENTDATEADDED_TO"]);
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(PSO_Setting["EVENTDATEADDED_TO"]))
                            {
                                txtEvtDateAddedTo.Text = AppHelper.GetDate(Conversion.GetDate(sToDateDefaultValue));
                            }
                        }

                        
                        txtEventNumbers.Text = PSO_Setting["EVENT_NUMBERS"];
                        txtEmailNotice.Text = PSO_Setting["EMAIL_NOTICE"];
                        txtCompany.Text = PSO_Setting["COMPANY_NAME"];
                        if (string.IsNullOrEmpty(PSO_Setting["UPDATED_BY_USER"]))
                        {
                            txtUpdByUser.Text = sDefaultUpdatedByUser;
                        }
                        else
                        {
                            txtUpdByUser.Text = PSO_Setting["UPDATED_BY_USER"];
                        }
                        string sTestExport = PSO_Setting["TEST_EXPORT"];
                        if (sTestExport == "-1")
                        {
                            chkTestExport.Checked = true;
                        }
                    }
                }
                catch (Exception exp)
                {
                    err.Add("DataIntegrator.Error", exp.Message, BusinessAdaptorErrorType.Message);
                    bErr = true;
                }


            }
            catch (Exception ex)
            {
            }
            if (bErr == true)      
            {
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveSetting();
        }


        private void SaveSetting()
        {
            string sTaskManagerXml = string.Empty;
            //Riskmaster.UI.DataIntegratorService.DataIntegratorModel objModel = new Riskmaster.UI.DataIntegratorService.DataIntegratorModel();
            //DataIntegratorService.DataIntegratorServiceClient objService = new Riskmaster.UI.DataIntegratorService.DataIntegratorServiceClient();
            DataIntegratorModel objModel = null;
            BusinessAdaptorErrors err = null;
            bool bErr = false;
            string sCurrentTime = Convert.ToString(DateTime.Now.TimeOfDay);
            try
            {
                objModel = new DataIntegratorModel();
                err = new BusinessAdaptorErrors();

                sTaskManagerXml = Server.HtmlDecode(hdTaskManagerXml.Value);
                iOptionSetIDParam = Conversion.ConvertObjToInt(hdOptionsetId.Value);
                        
                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objModel.Token = AppHelper.GetSessionId();
                }
                if (txtExportOptionset.Text == "")
                {
                    //Yukti-ML Changes Start MITS 34000
                    //err.Add("DataIntegrator.Error", "Optionset Name cannot be blank. ", BusinessAdaptorErrorType.Message);
                    err.Add("DataIntegrator.Error", AppHelper.GetResourceValue(PageID,"ErrorOptionSetNameBlank", "3"), BusinessAdaptorErrorType.Message);
                    bErr = true;
                    //Yukti-ML Changes End
                }
                if (txtProviderId.Text == "")
                {
                    //Yukti-ML Changes Start MITS 34000
                    //err.Add("DataIntegrator.Error", "Provider ID cannot be blank. ", BusinessAdaptorErrorType.Message);
                    err.Add("DataIntegrator.Error",AppHelper.GetResourceValue(PageID,"ErrorProviderIDBlank","3"), BusinessAdaptorErrorType.Message);
                    bErr = true;
                    //Yukti-ML Changes End MITS 34000
                }
                if (txtPSOId.Text == "")
                {
                    //Yukti-ML Changes Start MITS 34000
                    //err.Add("DataIntegrator.Error", "PSO ID cannot be blank. ", BusinessAdaptorErrorType.Message);
                    err.Add("DataIntegrator.Error",AppHelper.GetResourceValue(PageID,"ErrorPSOIDBlank", "3"), BusinessAdaptorErrorType.Message);
                    bErr = true;
                    //Yukti-ML Changes end MITS 34000
                }
                if (txtUpdByUser.Text.Length > 50)
                {
                    //Yukti-ML Changes Start MITS 34000
                    //err.Add("DataIntegrator.Error", "Updated by User cannot exceed 50 characters.", BusinessAdaptorErrorType.Message);
                    err.Add("DataIntegrator.Error", AppHelper.GetResourceValue(PageID,"ErrorUpdByUser","3"), BusinessAdaptorErrorType.Message);
                    bErr = true;
                    //Yukti-ML Changes End MITS 34000
                }
                if (bErr == true)
                {
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }
                Dictionary<string, string> PSO_Setting = new Dictionary<string, string>();

                objModel.OptionSetName = txtExportOptionset.Text;

                PSO_Setting.Add("PROVIDER_ID", txtProviderId.Text);
                PSO_Setting.Add("PSO_ID", txtPSOId.Text);
                PSO_Setting.Add("COMPANY_ID", txtCompany_cid.Text);
                if (string.IsNullOrEmpty(txtEvtDateFrom.Text))
                {
                    PSO_Setting.Add("EVENTDATE_FROM", Conversion.GetDate(sFromDateDefaultValue));
                }
                else
                {
                    PSO_Setting.Add("EVENTDATE_FROM", Conversion.GetDate(txtEvtDateFrom.Text));
                }
                if (string.IsNullOrEmpty(txtEvtDateTo.Text))
                {
                    PSO_Setting.Add("EVENTDATE_TO", string.Empty);
                }
                else
                {
                    PSO_Setting.Add("EVENTDATE_TO", Conversion.GetDate(txtEvtDateTo.Text));
                }
                if (string.IsNullOrEmpty(txtEvtDateAddedFrm.Text))
                {
                    PSO_Setting.Add("EVENTDATEADDED_FROM", Conversion.GetDateTime(sFromDateDefaultValue));
                }
                else
                {
                    PSO_Setting.Add("EVENTDATEADDED_FROM", Conversion.GetDateTime(txtEvtDateAddedFrm.Text));
                }
                if (string.IsNullOrEmpty(txtEvtDateAddedTo.Text))
                {
                    PSO_Setting.Add("EVENTDATEADDED_TO", string.Empty);
                }
                else
                {
                    PSO_Setting.Add("EVENTDATEADDED_TO", Conversion.GetDateTime(txtEvtDateAddedTo.Text + " " + sCurrentTime));
                }
                if (string.IsNullOrEmpty(txtUpdByUser.Text))
                {
                    PSO_Setting.Add("UPDATED_BY_USER", sDefaultUpdatedByUser);
                }
                else
                {
                    PSO_Setting.Add("UPDATED_BY_USER", txtUpdByUser.Text);
                }
                PSO_Setting.Add("EVENT_NUMBERS", txtEventNumbers.Text.Trim(','));
                PSO_Setting.Add("EMAIL_NOTICE", txtEmailNotice.Text.Trim(','));
                if (chkTestExport.Checked)
                    PSO_Setting.Add("TEST_EXPORT", "-1");
                else
                    PSO_Setting.Add("TEST_EXPORT", "0");


                objModel.Parms = PSO_Setting;
                objModel.OptionSetID = iOptionSetIDParam;
                objModel.ModuleName = sModuleName;

                objModel.TaskManagerXml = sTaskManagerXml;

                //ipuri REST Service Conversion 12/06/2014 Start
                //objModel = objService.SaveSettings(objModel);
                objModel.ClientId = AppHelper.ClientId;

                objModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/SaveSettings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                //ipuri REST Service Conversion 12/06/2014 End
                if (objModel.OptionSetID == -1)
                {
                    //Yukti-ML Changes Start MITS 34000
                    //err.Add("DataIntagrator.Error", "OptionSet Name already exists.", BusinessAdaptorErrorType.Message);
                    err.Add("DataIntagrator.Error", AppHelper.GetResourceValue(PageID,"ErrorDupOptionSetName", "3"), BusinessAdaptorErrorType.Message);
                    bErr = true;
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                    return;
                }

                if (objModel.OptionSetID > 0)
                {
                    Response.Redirect("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx", false);
                }
            }
            catch (Exception ex)
            {
                err.Add("DataIntegrator.Error", ex.Message, BusinessAdaptorErrorType.Message);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                try
                {
                    if (iOptionSetIDParam <= 0 && objModel.OptionSetID > 0)
                        
                    //ipuri REST Service Conversion 12/06/2014 Start
                    //objService.SaveSettingsCleanup(objModel);
                    objModel.ClientId = AppHelper.ClientId;

                    objModel = AppHelper.GetResponse<DataIntegratorModel>("RMService/DAIntegration/savesettingscleanup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objModel);
                    //ipuri REST Service Conversion 12/06/2014 End
                }
                catch (Exception eCleanup)
                {
                }
                return;
            }

        }
    }
}