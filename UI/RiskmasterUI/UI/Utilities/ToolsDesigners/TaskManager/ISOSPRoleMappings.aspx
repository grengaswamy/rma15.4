<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ISOSPRoleMappings.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.TaskManager.ISOSPRoleMappingGrid"
    ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ISO Service Provider mappings</title>
    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>
    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js">        { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <script language="javascript" type="text/javascript">

        function ValidateDropDownSelection() {
            var ddlrmASPRoleType = document.getElementById('ddlrmASPRoleType');
            var ddlISOSPRoleType = document.getElementById('ddlISOSPRoleType');
            var ClientError = document.getElementById('ClientError');
            if (ddlrmASPRoleType != null && ddlrmASPRoleType.selectedIndex == 0) {
                debugger;
                if (ClientError != null) {
                    ClientError.style.display = 'block';
                    ClientError.innerHTML = "Please Select MMSEA Claimant Representative Type."
                    return false;
                }

            }
            if (ddlISOSPRoleType != null && ddlISOSPRoleType.selectedIndex == 0) {
                if (ClientError != null) {
                    ClientError.style.display = 'block';
                    ClientError.innerHTML = "Please Select ISO Service Provider Type."
                    return false;
                }

            }

        }

    </script>
    <style type="text/css">
        .auto-style1 {
            FONT-WEIGHT: bold;
            TEXT-DECORATION: underline;
            width: 190px;
        }
        .auto-style2 {
            width: 190px;
        }
    </style>
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="ISO Service Provider Mapping" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    &nbsp;
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/ISOSPRoleMappingGrid/control[@name ='RowId']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" RMXRef="/Instance/Document/ISOSPRoleMappingGrid/control[@name ='mode']" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    &nbsp;
    <table style="width: 100%;">
        <tr id="div_rmASPRole" class="full" >
            <td class="auto-style1" Font-Names="Times New Roman" Font-Size="Medium">
                MMSEA Claimant Representative Type
            </td>
            <td>
                <asp:DropDownList ID="ddlrmASPRoleType" runat="server" Width="250px"
                RMXRef="/Instance/Document/ISOSPRoleMappingGrid/control[@name ='rmA Service Proivder Type']" >
            </asp:DropDownList>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr id="div_ISOSPRole" >
        <td class="auto-style1" Font-Names="Times New Roman" Font-Size="Medium">
            ISO Service Provider Type
             
            </td>
            <td>
        <span>
            <asp:DropDownList ID="ddlISOSPRoleType" runat="server" Width="250px"
                RMXRef="/Instance/Document/ISOSPRoleMappingGrid/control[@name ='ISO Service Proivder Type']" >
            </asp:DropDownList>
        </span>
        </td>
         <td>
                &nbsp;
            </td>

        </tr>
        <tr  class="formButton" id="div_btnOk" align="center">
            
            <td align="right" class="auto-style2">
                 <asp:Button class="button" runat="server" ID="btnSave" Text="Save" Width="75px" OnClick="btnSave_Click" 
                 OnClientClick="return ValidateDropDownSelection();"/>
            </td>
            <td align="left">            
            <input type="button" value="Cancel" class="button" onclick="self.close()" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="line-height: 1px">
        <td colspan="3"></td>
        </tr>
        <tr >
        <td class="auto-style2"><asp:TextBox ID="txtISORowID"  runat="server" Visible="False"></asp:TextBox></td>
            
        <td >
            <asp:Label ID="ClientError" runat="server" Text="" ForeColor="#FF3300" Font-Bold="True"></asp:Label></td>
        <td></td>
        </tr>
    </table> 
   
    
    
    </form>
</body>
</html>
