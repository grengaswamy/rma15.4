﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using System.Collections.Generic;


namespace Riskmaster.UI.Utilities.ToolsDesigners
{
    public partial class TransactionTypeChange : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        private string sCWSresponse = "";
        private XElement rootElement = null;
        public IEnumerable result = null;
        private string RowId = "";
        bool bResult;
        public bool RecordExists = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetMessageTemplate();
                bResult = CallCWS("TransTypeChangeAdaptor.Get", XmlTemplate, out sCWSresponse, false, true);
                if (bResult)
                {
                    rootElement=XElement.Parse(sCWSresponse);
                    if (rootElement.XPathSelectElement("//TransTypeChange/FundsTransSplit") != null)
                    {
                        if(rootElement.XPathSelectElement("//TransTypeChange/FundsTransSplit").Attribute("payee")!=null)
                            Payee.Text = rootElement.XPathSelectElement("//TransTypeChange/FundsTransSplit").Attribute("payee").Value;
                        if (rootElement.XPathSelectElement("//TransTypeChange/FundsTransSplit/TransDetail") != null)
                        {
                            result = from c in rootElement.XPathSelectElements("//TransTypeChange/FundsTransSplit/TransDetail")
                                     select c;
                            foreach (XElement item in result)
                            {
                                RowId = item.Attribute("id").Value;
                                RegisterFromDateClientScript(RowId);
                                RegisterToDateClientScript(RowId);

                            }
                            RecordExists = true;
                        }
                        else
                        {
                            RecordExists = false;
                        }
                    }

                }

                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 3);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes


            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><TransTypeChange>");
            sXml = sXml.Append("<FundsTransSplit>");
            sXml = sXml.Append("</FundsTransSplit>");
            sXml = sXml.Append("<PaymentCtlNum>");
            sXml = sXml.Append(txtSearch.Text);
            sXml = sXml.Append("</PaymentCtlNum>");
            sXml = sXml.Append("</TransTypeChange></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        protected void SaveChanges(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetSaveMessageTemplate();
                bResult=CallCWS("TransTypeChangeAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                if (bResult)
                {
                    Page_Load(sender, e);
                }
                

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                txtFrom_Changed.Text = "";
                txtTo_Changed.Text = "";
                lstTransNam_Changed.Text = "";
            }

        }
        private XElement GetSaveMessageTemplate()
        {
            string sControlId = "";
            string sCurrentAttrValue = string.Empty;
            string sTransValue = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><TransTypeChange>");
            foreach (XElement item in result)
            {
                sControlId = item.Attribute("id").Value;
                sCurrentAttrValue = item.Attribute("transcode").Value;

                sTransValue = GetControlValue("lstTransNam", sCurrentAttrValue, sControlId);

                sCurrentAttrValue = item.Attribute("fromdt").Value;
                sFromDate = GetControlValue("txtFrom", sCurrentAttrValue, sControlId);

                sCurrentAttrValue = item.Attribute("todt").Value;
                sToDate = GetControlValue("txtTo", sCurrentAttrValue, sControlId);

               //comented by Sravan for RMA-8028 Starts

                //asatishchand changes for mits 33823 starts
                //if (!String.IsNullOrWhiteSpace(sFromDate) && !sFromDate.Contains('/'))  //bpaskova MITS 37587 
                //{
                //    sFromDate = sFromDate.Substring(0,2)+"/"+sFromDate.Substring(2,2)+"/"+sFromDate.Substring(4,4);
                //}
                //if (!String.IsNullOrWhiteSpace(sToDate) && !sToDate.Contains('/')) //bpaskova MITS 37587
                //{
                //    sToDate = sToDate.Substring(0, 2) + "/" + sToDate.Substring(2, 2) + "/" + sToDate.Substring(4, 4);
                //}
                //asatishchand changes for mits 33823 ends

                //RMA-8028 Ends
                sXml = sXml.Append("<TransDetail id='" + sControlId + "' transcode='" + sTransValue + "' fromdt='" + sFromDate + "' todt='" + sToDate + "'>");
                sXml = sXml.Append("</TransDetail>");
            }
            sXml = sXml.Append("</TransTypeChange></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        private string GetControlValue(string sControlName, string sCurrentValue, string sControlId)
        {
            string sChangedIds = "";
            string[] arrChangedIds = new String[50];
            string sChangedValue = sCurrentValue;
            TextBox HiddenControl = (TextBox)this.Form.FindControl(sControlName + "_Changed");

            if (HiddenControl != null)
            {
                sChangedIds = HiddenControl.Text;
                arrChangedIds = sChangedIds.Split(new Char[] { ';' }); //IdValues are separated with ;
                foreach (string sChangedIdValue in arrChangedIds)
                {
                    string[] sIdValue = new string[2];
                    sIdValue = sChangedIdValue.Split(new Char[] { '|' });//Each IdValue is | separated.
                    if (sIdValue.Length == 2)
                    {
                        string sControl = sControlName + "_" + sControlId;
                        if (sControl == sIdValue[0])
                        {
                            sChangedValue = sIdValue[1];
                            break;
                        }
                    }

                }
            }
            return sChangedValue;
        }
        private void RegisterFromDateClientScript(string rowid)
        {
            StringBuilder sbScript = new StringBuilder();
            //sbScript.Append("Zapatec.Calendar.setup({");
            //sbScript.Append("inputField :'txtFrom_" + rowid + "',");
            //sbScript.Append(" ifFormat  :'%m/%d/%Y',");
            //sbScript.Append("button     :'dateFrom_" + rowid + "'");
            //sbScript.Append("}");
            //sbScript.Append(");");

            //vkumar258 RMA-6037 ML-Changes
            sbScript.Append("$(function () {");
            sbScript.Append("$(\"#txtFrom_" + rowid +"\").datepicker({");
            sbScript.Append(" showOn: " + "\"button\"" + ",");
            sbScript.Append("buttonImage: " + "\"../../../Images/calendar.gif\"" + ",");
           // sbScript.Append(" buttonImageOnly: true,");
            sbScript.Append("showOtherMonths: true,");
            sbScript.Append("selectOtherMonths: true,");
            sbScript.Append("changeYear: true");
            sbScript.Append("}).next('button.ui-datepicker-trigger').css({border: 'none', background:'none'});");
            sbScript.Append("});");
            //vkumar258 RMA-6037 ML-Changes
            Page.ClientScript.RegisterStartupScript(this.GetType(), "'SetFromDate_" + rowid + " '", sbScript.ToString(), true);

        }
        private void RegisterToDateClientScript(string rowid)
        {
            StringBuilder sbScript = new StringBuilder();
            //sbScript.Append("Zapatec.Calendar.setup({");
            //sbScript.Append("inputField :'txtTo_" + rowid + "',");
            //sbScript.Append(" ifFormat  :'%m/%d/%Y',");
            //sbScript.Append("button     :'dateTo_" + rowid + "'");
            //sbScript.Append("}");
            //sbScript.Append(");");

            //vkumar258 RMA-6037 ML-Changes
             sbScript.Append("$(function () {");
             sbScript.Append("$(\"#txtTo_" + rowid + "\").datepicker({");
             sbScript.Append(" showOn: " + "\"button\"" + ",");
             sbScript.Append("buttonImage: " + "\"../../../Images/calendar.gif\"" + ",");
            //sbScript.Append(" buttonImageOnly: true,");
            sbScript.Append("showOtherMonths: true,");
            sbScript.Append("selectOtherMonths: true,");
            sbScript.Append("changeYear: true");
            sbScript.Append("}).next('button.ui-datepicker-trigger').css({border: 'none', background:'none'});");
            sbScript.Append("});");
            //vkumar258 RMA-6037 ML-Changes

            Page.ClientScript.RegisterStartupScript(this.GetType(), "'SetToDate_" + rowid + " '", sbScript.ToString(), true);

        }
    }
}
