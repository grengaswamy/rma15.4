﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdjusterTransfer.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.Manager.AdjusterTransfer" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
  <link rel="stylesheet" href="/../../Content/system.css" type="text/css"/>
  <script type="text/JavaScript" src="../../../Scripts/form.js"></script>
  <script type="text/JavaScript" src="/csc-Theme/riskmaster/common/javascript/drift.js"></script>
  <script type="text/javascript">
				function AddFilter(mode)
					{
						var optionRank;
						var optionObject;
						
						selectObject=document.getElementById('lstTabs');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstTopDowns');
							for (var x = select.options.length - 1; x >= 0 ; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('lstTopDowns',select.options[x].value);							
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstTopDowns');
							if (select.options.length > 0)
							{
							    for (var x = select.options.length-1; x >= 0; x--)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
								select.options.length=0;
							}
						}
						return false;
					}
					
					function RemoveFilter(mode)
					{
						var optionRank;
						var optionObject;
						selectObject=document.getElementById('lstTopDowns');
						
						if (mode=="selected")
						{
							//Add selected Available Values
							select=document.getElementById('lstTabs');
							for (var x = select.options.length-1; x >=0; x--)
							{
								if(select.options[x].selected == true)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
									
									//remove from available list	
									removeOption('lstTabs',select.options[x].value);							
								}
							}
						}
						else
						{
							//Add All Available Values
							select=document.getElementById('lstTabs');
							if (select.options.length > 0)
							{
							    for (var x = select.options.length-1; x >= 0; x--)
								{
									//add to selected list
									optionObject = new Option(select.options[x].text,select.options[x].value);
									optionRank = selectObject.options.length;
									selectObject.options[optionRank]=optionObject;
								}
								select.options.length=0;
							}
						}
						return false;
					}
					
					function removeOption(selectName,id)
					{
						select=document.getElementById(selectName);
						Ids=new Array();
						Names=new Array();

						for(var x = 0; x < select.options.length ; x++)
						{
							if(select.options[x].value!=id)
							{
								Ids[Ids.length] = select.options[x].value;
								Names[Names.length]=select.options[x].text;
							}
						}

						select.options.length = Ids.length;

						for(var x = 0;x < select.options.length; x++)
						{
							select.options[x].text=Names[x];
							select.options[x].value=Ids[x];
						}
					}
				function startUp()
				{
				  if (document.forms[0].chkOpenClaims.checked)
				  {
				     document.forms[0].chkOpenClaims.checked=true;
				  }
				  document.forms[0].hdnActionClaims.value="";
				}
								
				function AddToTabLayout()
				{
					var selIndex = document.forms[0].lstTopDowns.selectedIndex;
					if(document.forms[0].lstTopDowns.selectedIndex < 0)
						return false;
					var sValue=document.forms[0].lstTopDowns.options[document.forms[0].lstTopDowns.selectedIndex].value;
					var sText=document.forms[0].lstTopDowns.options[document.forms[0].lstTopDowns.selectedIndex].text;
					for(var f=0;f<document.forms[0].lstTabs.options.length;f++)
					if(document.forms[0].lstTabs.options[f].value==sValue)
					return false;
					document.forms[0].lstTopDowns.options[selIndex] = null;
					document.forms[0].lstTopDowns.selectedIndex = selIndex - 1;
					if(document.forms[0].lstTopDowns.selectedIndex == -1)
						document.forms[0].lstTopDowns.selectedIndex = 0;
					var opt=new Option(sText, sValue, false, false);
					document.forms[0].lstTabs.options[document.forms[0].lstTabs.options.length]=opt;
					return false;
				}
			function AddToTopdownLayout() {
				var selIndex = document.forms[0].lstTabs.selectedIndex;
				if(document.forms[0].lstTabs.selectedIndex<0)
					return false;
				var sValue=document.forms[0].lstTabs.options[document.forms[0].lstTabs.selectedIndex].value;
				var sText=document.forms[0].lstTabs.options[document.forms[0].lstTabs.selectedIndex].text;
				
				for(var f=0;f<document.forms[0].lstTopDowns.options.length;f++)
					if(document.forms[0].lstTopDowns.options[f].value==sValue)
						return false;

				document.forms[0].lstTabs.options[selIndex] = null;
				document.forms[0].lstTabs.selectedIndex = selIndex - 1;
				if(document.forms[0].lstTabs.selectedIndex == -1)
					document.forms[0].lstTabs.selectedIndex = 0;

				var opt=new Option(sText, sValue, false, false);
				document.forms[0].lstTopDowns.options[document.forms[0].lstTopDowns.options.length]=opt;
				m_DataChanged=true;
				return false;
				}
				function SelectAll() 
				{
				if (!Validate())
					{
						return false;
					}
					hndlstTabs1 = document.getElementById('hndlstTabs');
					hndTopDown = document.getElementById('hndTopDown');
					var values = "";

					for (var f = 0; f < document.forms[0].lstTopDowns.options.length; f++) {
					    values += document.forms[0].lstTopDowns.options[f].value + "=" + document.forms[0].lstTopDowns.options[f].text + ";";
					}
					hndTopDown.value += values;

					values = "";
					for (var f = 0; f < document.forms[0].lstTabs.options.length; f++) {
					    values += document.forms[0].lstTabs.options[f].value + "=" + document.forms[0].lstTabs.options[f].text + ";";
					}
					hndlstTabs1.value = values;
					return true;
									
				}
				function Validate()
				{
				    if (document.forms[0].lstFromAdjuster.value == "" || document.forms[0].lstFromAdjuster.value == 0)
					{
						alert("Please select From Adjuster");
						return false;
		            }
		            //mits:14618(amrit) ;added condition : document.forms[0].lstToAdjuster.value==0 to populate alert message
					if (document.forms[0].lstToAdjuster.value == "" || document.forms[0].lstToAdjuster.value==0)
					{
						alert("Please select To Adjuster");
						return false;
					}
					//mits:14618 ends
					if (document.forms[0].lstFromAdjuster.value==document.forms[0].lstToAdjuster.value)
					{
						alert("From Adjuster and To Adjuster cannot be same.");
						return false;
					}
					if (document.forms[0].lstTabs.options.length ==0)
					{
						alert("Please select Claim(s) to transfer");
						return false;
					}
					return true;
				}
				function GetClaims()
				{
				  document.forms[0].hdnAction.value="GetClaims";
				  document.forms[0].hdnActionClaims.value="Y";
				  document.forms[0].submit();				  
				}
				</script>
				<script type="text/javascript" src="/../../Scripts/zapatec/utils/zapatec.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpwin/src/window.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpwin/src/dialog.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpcal/src/calendar.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
				<script type="text/javascript" src="/../../Scripts/calendar-alias.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
				<script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
				<script type="text/javascript" src="/../../Scripts/rmx-common-ref.js"></script>
</head>
<body onload="parent.MDIScreenLoaded()">


<form id="frmData" name="frmData" method="post" runat="server">
<uc1:ErrorControl ID="ErrorControl" runat="server" />
<asp:TextBox Style="display: none" runat="server" id="wsrp_rewrite_action_1"></asp:TextBox>
<table border="0" CELLPADDING="0" CELLSPACING="0">
    <tr>
     <td><asp:TextBox Style="display: none" runat="server" id="hdnAction"></asp:TextBox></td>
     <td><asp:TextBox Style="display: none" runat="server" id="hdnActionClaims"></asp:TextBox></td>
     <td><asp:TextBox Style="display: none" runat="server" id="hdnToAdjuster"></asp:TextBox></td>
     <td><asp:TextBox Style="display: none" runat="server" id="hdnSelectedClaims"></asp:TextBox></td>  
      <td><asp:TextBox Style="display: none"  runat="server" id="hdnOnlyOpenClaims"/></td>
    </tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td class="msgheader" colspan="4">Current Adjuster Transfer</td>
    </tr>
   </table>
   <table border="0">
    <tr>
     <td>
      <div style="position:relative;left:0;top:0;width:950px;height:350px;overflow:auto" class="singletopborder">
       <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
         <td>
          <table border="0" cellspacing="0" cellpadding="0" width="100%" name="FORMTABTransferDiary" id="FORMTABTransferDiary">
           <tr>
            <td>
             <table border="0" cellspacing="0" cellpadding="0" width="70%">
              <tr>
               <td colspan="1">
                <table>
                 <tr>
                  <td>
                   Only Open Claims: 
                   <asp:CheckBox type="checkbox" appearance="full" runat="server" AutoPostBack="true"  rmxref="/Instance/Document/Adjuster/control[@name='OnlyOpenClaims']" 
                        rmxignoreget="true"  id="chkOpenClaims"  oncheckedchanged="chkOpenClaims_CheckedChanged" 
                          Checked="false"/></td>
                 </tr>
                </table>
               </td>
               <td colspan="1">
                <table>
                 <tr>
                  <td>
                   <td class="ctrlgroup">Choose Claim(s) you want to be transferred to another Adjuster:
                   </td>
                  </td>
                 </tr>
                </table>
               </td>
              </tr>
              <tr>
               <td>
                <table>
                 <tr>
                  <td><b><u>From Adjuster :</u></b></td>
                 </tr>
                 <tr>
                  <td><asp:DropDownList ID="lstFromAdjuster" selected="true" type="combobox"  runat="server" rmxref="/Instance/Document/Adjuster/control[@name='FromAdjuster']"
                    ItemSetRef="/Instance/Document/Adjuster/control[@name='FromAdjuster']" onselectedindexchanged="lstFromAdjuster_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                    <asp:TextBox Style="display: none" runat="server" id="hdnFromAdjuster"></asp:TextBox>
                    </td>
                    
                 </tr>
                 <tr>
                 <td><asp:Image ID="ArrowWPAUtil1" ImageUrl="../../../Images/ArrowWPAUtil1.jpg" runat="server"/></td>
                   
                 </tr>
                 <tr>
                  <td><b><u>To Adjuster</u></b> :                  																	
                   																	
                  </td>
                 </tr>
                 <tr>
                  <td><asp:DropDownList ID="lstToAdjuster" selected="true" type="combobox" runat="server"
                    rmxref="/Instance/Document/Adjuster/control[@name='ToAdjuster']" 
                    ItemSetRef="/Instance/Document/Adjuster/control[@name='ToAdjuster']"></asp:DropDownList></td>
                 </tr>
                </table>
               </td>
               <td>
                <table>
                 <tr>
                  <td width="5%" nowrap="true"><b>Claims available:</b></td>
                  <td width="5%"></td>
                  <td width="*" nowrap="true"><b>Claims selected:</b></td>
                 </tr>
                 <tr>
                  <td width="5%" nowrap="true">
                  <!--smishra25:09/16/2010 MITS 19668 removing inline width attribute from the control-->
                  <asp:ListBox type="combobox" 
                          rmxref="/Instance/Document/Adjuster/control[@name='AvailClaims']" 
                          rmxignoreset="true" runat="server" ID="lstTopDowns" EnableViewState="false" 
                          size="10" Height="163px" style="margin-top: 0px" 
                          SelectionMode="Multiple"></asp:ListBox>
                  <asp:HiddenField  ID ="hndTopDown" runat="server" />  
                  <asp:TextBox runat ="server" rmxref="/Instance/Document/Adjuster/control[@name='AvailClaims']" rmxignoreget="true" ID="txtTopDown" style="display :none"></asp:TextBox>
                 
                  <td width="40%" valign="middle" align="center">
                  <asp:Button runat="server" ID="btn1" Text="Add &gt;" class="button" 
                          style="width:80" OnClientClick="return AddFilter('selected');" Height="20px" 
                          Width="80px"/><br/><br/>
                  <asp:Button runat="server" ID="btn2" Text="Add All &gt;" class="button" 
                          style="width:80; margin-left: 0px;" OnClientClick="return AddFilter('all');" 
                          Height="20px" Width="80px"/><br/><br/>
                  <asp:Button runat="server" ID="btn3" Text="< Remove" class="button" 
                          style="width:80; margin-left: 0px;" 
                          OnClientClick="return RemoveFilter('selected');" Height="20px" Width="80px"/><br/><br/>
                  <asp:Button runat="server" ID="btn4" Text="< Remove All" class="button" 
                          style="width:80" OnClientClick="return RemoveFilter('all');" Height="20px" 
                          Width="80px"/>
                  </td>
                  <td nowrap="true" width="*">
                   <!--smishra25:09/16/2010 MITS 19668 removing inline width attribute from the control-->
                  <asp:ListBox type="combobox" rmxref="/Instance/Document/Adjuster/control[@name='SelectedClaims']"  
                          runat="server" OnClientClick="return AddToTopdownLayout();" ID="lstTabs" rmxignoreset="true"  
                          size="10" Height="163px" style="margin-top: 0px"  SelectionMode="Multiple" 
                          ></asp:ListBox>
                          <asp:HiddenField  ID ="hndlstTabs" runat="server" />  
                 <asp:TextBox runat ="server" ID="txtlstTabs" rmxref="/Instance/Document/Adjuster/control[@name='SelectedClaims']" rmxignoreget="true" style="display :none"></asp:TextBox>
                 
                 </tr>
                </table>
               </td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td><asp:Button runat="server" Text="Go" style="width:80" Height="20px" 
                    id="btnSave" OnClick=" Save" class="button" OnClientClick="return SelectAll();" 
                    Width="80px"/>
             	&nbsp;&nbsp;&nbsp;&nbsp;		
             																
            </td>
           </tr>
          </table>
         </td>
        </tr>
       </table>
      </div>
     </td>
    </tr>
    <tr>
     <td><script type="text/javascript">
             startUp();
						</script></td>
    </tr>
   </table><asp:TextBox Style="display: none" runat="server" value="rmx-widget-handle-5" id="SysWindowId"></asp:TextBox>
   <asp:TextBox Style="display: none" runat="server"  value=""></asp:TextBox>
   </form>
</body>
</html>
