﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor
{
    public partial class NewScript : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sReturnValue = "";
                XmlDocument tableXDoc = new XmlDocument();
                XElement XmlTemplate = null;
                XmlDocument XmlDoc = new XmlDocument();
                if (!IsPostBack)
                {

                    XmlTemplate = GetMessageTemplate();
                    sReturnValue = AppHelper.CallCWSService(XmlTemplate.ToString());
                    XmlDoc.LoadXml(sReturnValue);
                    XmlNodeList objtypelist = XmlDoc.SelectNodes("//DataModelObjects/DataModelObject");
                    foreach (XmlNode objCode in objtypelist)
                    {
                        cboObjectType.Items.Add(new ListItem(objCode.SelectSingleNode("FriendlyName").InnerText , objCode.SelectSingleNode("Name").InnerText));
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function>ScriptEditorAdaptor.LoadDataModelObjects</Function>
                </Call>
                <Document>
					<ScriptEditor /> 
                </Document>
            </Message>
            ");
            return oTemplate;
        }
    }
}
