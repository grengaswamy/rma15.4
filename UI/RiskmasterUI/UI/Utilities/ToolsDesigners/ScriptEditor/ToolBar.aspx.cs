﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor
{
    public partial class ToolBar : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    string sReturn = "";
            //    XmlDocument tableXDoc = new XmlDocument();
            //    XElement XmlTemplate = null;
            //    if (!IsPostBack)
            //    {
                    
            //        XmlTemplate  = GetMessageTemplate();
            //        sReturn = AppHelper.CallCWSService(XmlTemplate.ToString());   
            //    }
            //}
            //catch (Exception ee)
            //{
            //    ErrorHelper.logErrors(ee);
            //    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
            //    err.Add(ee, BusinessAdaptorErrorType.SystemError);
            //}
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function>ScriptEditorAdaptor.LoadDataModelObjects</Function>
                </Call>
                <Document>

                </Document>
            </Message>
            ");
            return oTemplate;
        }
        
    }
}
