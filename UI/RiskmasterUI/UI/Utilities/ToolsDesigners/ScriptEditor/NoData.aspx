﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="NoData.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor.NoData" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reports</title>
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
</head>
    
<body >
    <form id="frmData" runat="server">
    <uc1:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading"/>
    <div>
        <table border="0" width="100%" height="93%" cellpadding="0" cellspacing="0">
            <tr>
             <td align="left" valign="top"><img border="0" alt="" src="../../../../Images/ScriptEditor/SciptEditorLogo.jpg"/></td>
             <td align="center" valign="top"></td>
            </tr>
            <tr>
             <td colspan="2" align="right" valign="bottom"><img border="0" alt=""  src="../../../../Images/ScriptEditor/RulesEngine.jpg"/></td>
            </tr>
	    </table>	
    </div>
    </form>
</body>
</html>

	
