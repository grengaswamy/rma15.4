﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="LeftTreeLoad.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.ScriptEditor.LeftTreeLoad" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="../../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Reports</title>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" language="javascript">
        var m_nodeText = '';
        var m_nodeValue = '';
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            //zmohammad MITS 20275
            if (DataSaveAlert())
            { return false; }
            //alert(window.event.srcElement);
            var nodeClick = src.tagName.toLowerCase() == 'a';
            if (nodeClick) {
                var nodeText = src.innerText;
                var ret = true;
                m_nodeText = nodeText;
                var nodeValue = GetNodeValue(src);
                m_nodeValue = nodeValue;
                ret= ShowScript(nodeValue); 
                return ret;
            }
            return true;
        }
        function GetNodeValue(node) {
            var nodeValue = "";
            var nodePath = node.href.substring(node.href.indexOf(",") + 2, node.href.length - 2);
            var nodeValues = nodePath.split("\\");
            if (nodeValues.length > 1) {
                nodeValue = nodeValues[nodeValues.length - 1];
                m_nodeDepth = (nodeValues.length - 1) / 2;
            }
            else {
                nodeValue = nodeValues[0].substr(1);
                m_nodeDepth = 0;
            }
            return nodeValue;
        }
    </script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/ScriptEditor.js"></script>
</head>

<body onload="TreeOnLoad()">
<form id="frmLeftTree" name="frmLeftTree" runat="server" method="post" >
    <div style="overflow:auto" >
        <uc1:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading"/>
            <asp:TreeView ID="TreeScript" runat="server" NodeStyle-Font-Names="Tahoma, Arial, Helvetica, sans-serif" NodeStyle-Font-Size="10pt" NodeStyle-ForeColor="Black" NodeStyle-BackColor="" NodeStyle-Height="10pt" SelectedNodeStyle-BackColor="DarkBlue" SelectedNodeStyle-ForeColor="White" ExpandDepth="1">
                <Nodes >
                  <asp:TreeNode Text="Script Editor" Value="0" ImageUrl="~/Images/ScriptEditor/icon.folder.gif" SelectAction="None">
                    <asp:TreeNode Text="Validation" Value="1" ImageUrl="~/Images/ScriptEditor/icon.folder.gif"  SelectAction="None"></asp:TreeNode>
                    <asp:TreeNode Text="Calculation" Value="2" ImageUrl="~/Images/ScriptEditor/icon.folder.gif" SelectAction="None"></asp:TreeNode>
                    <asp:TreeNode Text="Initialization" Value="3" ImageUrl="~/Images/ScriptEditor/icon.folder.gif" SelectAction="None"></asp:TreeNode>
                    <asp:TreeNode Text="Before Save" Value="4" ImageUrl="~/Images/ScriptEditor/icon.folder.gif" SelectAction="None"></asp:TreeNode>
                    <asp:TreeNode Text="After Save" Value="5" ImageUrl="~/Images/ScriptEditor/icon.folder.gif" SelectAction="None"></asp:TreeNode>
                    <asp:TreeNode Text="Before Delete" Value="6" ImageUrl="~/Images/ScriptEditor/icon.folder.gif" SelectAction="None"></asp:TreeNode>
                  </asp:TreeNode>
                </Nodes>
            </asp:TreeView>
        <asp:HiddenField runat="server" id="hdnAction" />
        <asp:HiddenField runat="server" id="RowID" />
        <asp:TextBox runat="server" id="ValidationList"  style="display:none" RMXRef="/Instance/Document/ValidationList" />
        <asp:TextBox runat="server" id="CalculationList"  style="display:none" RMXRef="/Instance/Document/CalculationList" />
        <asp:TextBox runat="server" id="InitializationList"  style="display:none" RMXRef="/Instance/Document/InitializationList" />
        <asp:TextBox runat="server" id="BeforeSaveList"  style="display:none" RMXRef="/Instance/Document/BeforeSaveList" />
        <asp:TextBox runat="server" id="AfterSaveList"  style="display:none" RMXRef="/Instance/Document/AfterSaveList" />
        <asp:TextBox runat="server" id="BeforeDeleteList"  style="display:none" RMXRef="/Instance/Document/BeforeDeleteList" />
        <asp:TextBox runat="server" id="FocusScriptType"  style="display:none" RMXRef="/Instance/Document/FocusScriptType" />
        <asp:TextBox runat="server" id="FocusScriptId"  style="display:none" RMXRef="/Instance/Document/FocusScriptId" />
    </div> 
</form>
</body>
</html>
