﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.ToolsDesigners
{
    public partial class WPAAutoDiary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    RefreshListBox();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement AutoDiaryListTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                  <Authorization></Authorization>
                  <Call>
                    <Function>WPAAutoDiaryAdaptor.Get</Function>
                  </Call>
                  <Document>
                    <WPAAutoDiariesList>
                        <listhead>
                            <AutoName>Auto Name</AutoName>  
                            <RowId>Row Id</RowId>   
                        </listhead>
                    </WPAAutoDiariesList>
                  </Document>
                </Message>
            ");

            return oTemplate;
        }

        private XElement DeleteDiaryTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            
            <Message>
                  <Authorization></Authorization>
                  <Call>
                    <Function>WPAAutoDiaryAdaptor.Delete</Function>
                  </Call>
                  <Document>
                    <AutoDiaryId></AutoDiaryId> 
                  </Document>
                </Message>
            ");

            return oTemplate;
        }

        private string GetInputXmlForAutoDiaryList()
        {
            XElement objInputMessageTemplate = null;
            
            objInputMessageTemplate = AutoDiaryListTemplate();
            return objInputMessageTemplate.ToString();
        }

        private string GetInputXmlForDeleteDiary()
        {
            XElement objInputMessageTemplate = null;

            objInputMessageTemplate = DeleteDiaryTemplate();
            objInputMessageTemplate.XPathSelectElement("./Document/AutoDiaryId").Value = WPAAutoDiaryList.SelectedItem.Value;

            return objInputMessageTemplate.ToString();
        }

        private XmlDocument ProcessRequestForAutoDiary(string inputMessageForProcess)
        {
            string processResponse = string.Empty;
            XmlDocument autoDiaryDoc = null;
                
            processResponse = AppHelper.CallCWSService(inputMessageForProcess);

            if (processResponse != string.Empty)
            {
                autoDiaryDoc = new XmlDocument();
                autoDiaryDoc.LoadXml(processResponse);
            }
            return autoDiaryDoc;  
        }

        private void PopulateListBoxFromAutoDiaryList(XmlNode autoDiaryListNode)
        {
            XmlNodeList listRowNodeList = null;
            ListItem autoDiaryListItem = null;

            if (autoDiaryListNode != null)
            {
                listRowNodeList = autoDiaryListNode.SelectNodes("listrow");

                WPAAutoDiaryList.Items.Clear();

                foreach (XmlNode rowNode in listRowNodeList)
                {
                    autoDiaryListItem = new ListItem();

                    autoDiaryListItem.Text = rowNode.SelectSingleNode("AutoName").InnerText;
                    autoDiaryListItem.Value = rowNode.SelectSingleNode("RowId").InnerText;

                    WPAAutoDiaryList.Items.Add(autoDiaryListItem);
                }
            }
        }

        private void DeleteSelectedAutoDiary()
        {
            string inputMesasgeProcess = string.Empty;

            inputMesasgeProcess = GetInputXmlForDeleteDiary();
            ProcessRequestForAutoDiary(inputMesasgeProcess);
            RefreshListBox();
        }

        private void RefreshListBox()
        {
            XmlDocument autoDiaryListDoc = null;
            XmlNode wpaAutoDiaryListNode = null;
            string inputMessageForProcess = string.Empty;

            inputMessageForProcess = GetInputXmlForAutoDiaryList();
            autoDiaryListDoc = ProcessRequestForAutoDiary(inputMessageForProcess);
           
            if (autoDiaryListDoc != null)
            {
                wpaAutoDiaryListNode = autoDiaryListDoc.SelectSingleNode("ResultMessage/Document/WPAAutoDiariesList");
                PopulateListBoxFromAutoDiaryList(wpaAutoDiaryListNode);
            }
        }

        protected void Delete_WPAAutoDiaryList_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DeleteSelectedAutoDiary();
                RefreshListBox();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

    }
}
