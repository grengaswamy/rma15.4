﻿<%@ Page Title="" theme="RMX_Default" Language="C#" MasterPageFile="~/UI/Utilities/UtilityTemplate.Master" AutoEventWireup="true" CodeBehind="TransactionTypeChange.aspx.cs" Inherits="Riskmaster.UI.Utilities.TransactionTypeChange" %>
<asp:content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">

    <title>Transaction Type Change</title>

</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="cphUtilityBody" runat="server">

    <table border="0" width="100%">
    
        <tr>
            <td class="msgheader" colspan="3">
                Transaction Type Change Utility
            </td>
        </tr>
        
        <tr>
            <td class="fieldRightAligned">
                Payment Control Number :
            </td>
            <td>
                <asp:textbox id="txtPaymentControlNumber" name="txtPaymentControlNumber" validationgroup="vgPCNLookup" runat="server" />
            </td>
            <td>
                <asp:button id="btLookupPCN" name="btLookupPCN" text="Lookup" runat="server" />
                <asp:button id="btSearchPCN" name="btSearchPCN" text="Search" runat="server" />
            </td>
        </tr>
        
        <tr>
            <td colspan="3" class="msgheader">
                Payee :
            </td>
        </tr>
        
        <tr>
            <td colspan="3" class="ctrlgroup">
                Transaction Detail :
            </td>
        </tr>
    
        <tr>
            <td colspan="3">
                <asp:gridview id="gvTransactionDetail" runat="server" width="100%" />
            </td>
        </tr>
        
        <tr>
            <td colspan="3" align="center">
                <asp:button id="btSaveChanges" name="btSaveChanges" text="Save Changes" runat="server" />
            </td>
        </tr>
        
    </table>

</asp:content>
