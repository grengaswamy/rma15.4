﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InitialPreFillFolderUtilityPage.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.InitialPreFillFolderUtilityPage" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="../../Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript" language="javascript" src="../../../Scripts/WaitDialog.js"></script>
    <script language="JavaScript" type="text/javascript">
          function TransferDocuments()
          {
            document.getElementById('Mode').value = "Submit";
            window.forms[0].btnOk.disabled="true";
            document.forms[0].submit();
            return true;
          }
          
          function showDialogue()
          {
            pleaseWait.Show();
            return true;
          }
     </script>
</head>
<body onload="javascript:if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading"/>
    <div>
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
    <input type="hidden" name="$node^8" value="" id="ouraction"/>
    <input type="hidden" id="Mode" name="" value="" runat="server" />
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="msgheader" colspan="4">MCM PreFill Folder Utility</td>
    </tr>
   </table>
   <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
     <td align="center">
      <asp:Label ID="lblStatusMessage" runat="server"></asp:Label>   
     </td>
    </tr>
    &nbsp;
    
    <tr>
     <td align="center">
      <asp:label ID="lblDocumentPathType_label" runat="server" ></asp:label><b>
       <asp:Label id="lblDocumentPathType" runat="server"></asp:Label>
       </b></td>
    </tr>
    <tr>
     <td align="center">
      <asp:label ID="lblDocumentPath_label" runat="server"></asp:label>
      <asp:Label id="lblDocumentPath" runat="server"></asp:Label><b></b></td>
    </tr>
    <tr>
     <td align="center">
     <asp:Button ID="btnOk" runat="server" Text="Continue" Width="100px" OnClientClick="showDialogue(); return TransferDocuments();" />
     </td>
    </tr>
    <tr>
     <td align="center">
      <asp:label ID="lblNote" runat="server"></asp:label>      					
     </td>
    </tr>
   </table><input type="hidden" name="$node^5" value="rmx-widget-handle-8" id="SysWindowId"><input type="hidden" name="$instance" value="H4sIAAAAAAAAAIVUyWrDMBA9p19hfE+mtLeiGroQSBcobUrPqjxORGWN0cjY+fvKUdbWTgxG0rw3&#xA;i94MEjPLXlqFSVsayzdtfZsuva9uAJqmmQRj/j0ht4C2rnLpMd3wqC2OiOS+kexEUQkBgsqRQmZy&#xA;vHWoTtJ1hUbbXfS2IFfykUdzvS7j6vLyCiKcZkmSXIxGo7AkokQvs4tk4BOsHKId6zwDp/mnlOzR&#xA;wfvrp9dGe40MMxtWad4cTrUxUzI5uoiu3uQCBexDDKepGV3HUKwEbA8nqgoaaepiQj9LNNrm1HRR&#xA;BOz3O3iO7J9xFfNtD2sE+gURtT5Rj1Q+lBNSbTbDzBdtf0KugbLj5cj5sSJTl/YsjVxQOzu0/68c&#xA;DphnwoVJlWO/qhD2MU+4LIOq6FyY1qyQhrteH5iGHSOh53Kh9z0yi4/Y7pktqM/pkVRdovVZN9Sj&#xA;nVmR9Y5MYmWJt+nXw1MdJjjX6w5xCmfY92ix0P48ce6k5dj3eVDuH//OmFdZVdou+C/0yfggjfpr&#xA;fkSDHjdOEYziHF00/MG0fYayX3t72ymRBAAA">
    </div>
    </form>
</body>
</html>
