﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.FDM;
using System.Xml;
using System.Xml.Linq;
using AjaxControlToolkit;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.HistorySetUp
{
    public partial class PurgeHistoryPopUp : GridPopupBase
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oXETableList = null;
            string sOutXml = string.Empty;
            string sValue = string.Empty;
            string sText = string.Empty;
            bool bReturnStatus = false;
            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes
                if (!IsPostBack)
                {
                    //Getting Table List                    
                    bReturnStatus = CallCWSFunction("PurgeHistorySetUpAdaptor.GetTableList");
                    sOutXml = Data.SelectSingleNode("//TableList").OuterXml;
                    if (sOutXml != null)
                    {                        
                        oXETableList = XElement.Parse(sOutXml);
                    }                        
                   
                    // Bind Combo with TableList
                    if (oXETableList != null)
                    {
                        foreach (XElement oXETable in oXETableList.Elements())
                        {
                            sValue = oXETable.Attribute("Id").Value;
                            sText = oXETable.Attribute("Name").Value;
                            //ddlTableName.Items.Add(new ListItem(sText.Replace("<","").Replace(">",""), AppHelper.HTMLCustomEncode(sValue)));//Reverting : 35831 bkuzhanthaim 
							ddlTableName.Items.Add(new ListItem(sText, sValue));
                        }
                    }
                    //txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    txtToDate.Text =AppHelper.GetDate( DateTime.Now.ToString()); 
                }
                hdnSystemDate.Value  = AppHelper.GetDate( DateTime.Now.ToString()); 
                GridPopupPageload();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {
            string sResponse = string.Empty;
            XElement objXmlIn = null;
            XmlDocument objReturnXml = null;
            string sMsgStatus = null;

            try
            {
                objXmlIn = GetMsgTemplateForSave();
                CallCWS("PurgeHistorySetUpAdaptor.Save", objXmlIn, out sResponse, false, false);
                objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sResponse);
                sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMsgTemplateForSave()
        {
            XElement oElement = null;
            try
            {
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document><PurgeHistory>");
                sXml = sXml.AppendFormat("<RecordId>{0}</RecordId>", hdnRecordId.Text);
				//sXml = sXml.AppendFormat("<TableId>{0}</TableId>", AppHelper.HTMLCustomDecode(ddlTableName.SelectedValue));//Reverting : 35831 bkuzhanthaim 
                sXml = sXml.AppendFormat("<TableId>{0}</TableId>", ddlTableName.SelectedValue);
                sXml = sXml.AppendFormat("<FromDate>{0}</FromDate>", txtFromDate.Text);
                sXml = sXml.AppendFormat("<ToDate>{0}</ToDate>", txtToDate.Text);
                sXml = sXml.Append("</PurgeHistory></Document></Message>");

               oElement = XElement.Parse(sXml.ToString());                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            return oElement;
        }
    }
}
