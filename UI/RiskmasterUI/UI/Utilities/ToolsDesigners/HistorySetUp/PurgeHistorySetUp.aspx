﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurgeHistorySetUp.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.HistorySetUp.PurgeHistorySetUp" ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="UserControlGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purge History SetUp</title>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>
<script type="text/javascript" language="JavaScript" src="../../../../Scripts/grid.js"></script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <table width="90%">
        <tr>
            <td class="msgheader">
                Purge History SetUp
            </td>
        </tr>
        <tr>
            <td>
                <uc2:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <uc1:UserControlGrid runat="server" ID="PurgeHistoryGrid" GridName="PurgeHistoryGrid"
                        GridTitle="" Target="/Document/Document/Form/PurgeHistoryGrid" Unique_Id="RecordId"
                        ShowRadioButton="true" Height="350px" Width="680px" HideNodes="|RecordId|TableId|"
                        ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="250" Type="GridAndButtons"
                        HideButtons="" />
                    <asp:TextBox Style="display: none" runat="server" ID="PurgeHistorySelectedId" RMXType="id" />
                    <asp:TextBox Style="display: none" runat="server" ID="PurgeHistoryGrid_RowDeletedFlag"
                        RMXType="id" Text="false" />
                    <asp:TextBox Style="display: none" runat="server" ID="PurgeHistoryGrid_Action" RMXType="id" />
                    <asp:TextBox Style="display: none" runat="server" ID="PurgeHistoryGrid_RowAddedFlag"
                        RMXType="id" Text="false" />
                </div>
               
            </td>
        </tr>
    </table>
    <asp:TextBox runat="server" Style="display: none" ID="hdntemp" rmxref="/Document/Document/Form/hdnTemp" />
    </form>
</body>
</html>
