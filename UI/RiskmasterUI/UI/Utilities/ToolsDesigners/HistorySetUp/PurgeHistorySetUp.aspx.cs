﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
 using System.Xml.Linq ;
using System.Text;
namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.HistorySetUp
{
    public partial class PurgeHistorySetUp : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";

            try
            {
                if (PurgeHistoryGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = PurgeHistorySelectedId.Text;
                    XmlTemplate = GetDeletionTemplate(selectedRowId);
                    CallCWS("PurgeHistorySetUpAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                    PurgeHistoryGrid_RowDeletedFlag.Text = "false";
                }
                bool bReturnStatus = CallCWSFunction("PurgeHistorySetUpAdaptor.GetPurgeEntries");                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetDeletionTemplate(string p_sRowId)
        {
            XElement oElement = null;
            try
            {
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document><PurgeHistory>");
                sXml = sXml.AppendFormat("<RecordId>{0}</RecordId>", p_sRowId);
                sXml = sXml.Append("</PurgeHistory></Document></Message>");

               oElement = XElement.Parse(sXml.ToString());
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            return oElement;
        }
    }
}
