﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurgeHistoryPopUp.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.HistorySetUp.PurgeHistoryPopUp" ValidateRequest="false" %>

<%@ Register TagName="ErrorControl" TagPrefix="uc1" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purge History SetUp</title>

    <script src="../../../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <%--vkumar258 - RMA-6037 - Starts --%>

    <%-- <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="../../../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <link rel="stylesheet" href="../../../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>


    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../../../Scripts/grid.js"></script>

<script type="text/javascript" language="JavaScript" src="../../../../Scripts/HistoryDesigner.js"></script>

    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" runat="server">
   <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Purge History SetUp" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    
        <table>        
             <tr>
                <td>
                    <asp:ImageButton ID="Save" onMouseOver="this.src='../../../../Images/tb_save_mo.png';this.style.zoom='110%'"
                        onMouseOut="this.src='../../../../Images/tb_save_active.png';this.style.zoom='100%'"
                        src="../../../../Images/tb_save_active.png" class="bold" ToolTip="Save" runat="server"
                        OnClientClick="return fnValidatePurgeRecord();" TabIndex="6" onclick="Save_Click"/>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblTableName" Text="Table Name:" />
                </td>
                <td>                    
                    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" /> 
                    <cc1:ComboBox ID="ddlTableName" runat="server" AutoCompleteMode="SuggestAppend" RMXRef="./TableId" TabIndex="1">                                          
                    </cc1:ComboBox>                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblFromDate" Text="From Date:" />
                </td>
                <td>
                    <span>
                        <asp:TextBox runat="server" FormatAs="date" ID="txtFromDate" RMXRef="./FromDate"
                            TabIndex="2" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                        <%--vkumar258 - RMA-6037 - Starts --%>
                        <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnFromDate" TabIndex="3" />

                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
					    {
					        inputField: "txtFromDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btnFromDate"
					    }
					    );
                        </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#txtFromDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "3");
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- End--%>
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblToDate" Text="To Date:" />
                </td>
                <td>
                    <span>
                        <asp:TextBox runat="server" FormatAs="date" ID="txtToDate" RMXRef="./ToDate" TabIndex="4"
                            onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                        <%--vkumar258 - RMA-6037 - Starts --%>
                        <%-- <asp:Button class="DateLookupControl" runat="server" ID="btnToDate" TabIndex="5" />

                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
					    {
					        inputField: "txtToDate",
					        ifFormat: "%m/%d/%Y",
					        button: "btnToDate"
					    }
					    );
                        </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#txtToDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "5");
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- End--%>
                    </span>
                </td>
            </tr>
        </table>
    
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="hdnRecordId"  RMXRef="./RecordId" />   
    <asp:TextBox runat="server" Style="display: none" ID="hdntemp" rmxref="/Document/Document/Form/hdnTemp" /> 
    <asp:HiddenField runat ="server" ID ="hdnSystemDate" />
    </form>
</body>
</html>
