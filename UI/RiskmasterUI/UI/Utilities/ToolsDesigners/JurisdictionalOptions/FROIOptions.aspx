﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FROIOptions.aspx.cs" Inherits="Riskmaster.UI.Utilities.ToolsDesigners.JurisdictionalOptions.FROIOptions" ValidateRequest = "false" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>First Report Of Injury Options</title>
    <uc:CommonTasks id="CommonTasks1" runat="server" />
    <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css"/>
    <style type="text/css">@import url(../../../../Content/dhtml-div.css);</style>
    <script type="text/javascript" src="../../../../Scripts/froi.js">{var i;}</script>
</head>
 <body class="10pt" onload="Javascript:loadTabList();PageLoad('SelEntityGrid');parent.MDIScreenLoaded()">
  <form id="frmData" runat="server" method="post">
    <uc1:ErrorControl id="ErrorControl" runat="server" />
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
    <input type="hidden" name="hTabName"/>
   <table width="98%">
   <tr>
   <td>
   
    <div id="toolbardrift" class="toolbardrift">
     <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
     <tr>
      <td align="center" valign="middle" height="32">
          <asp:ImageButton runat="server" name="" src="../../../../Images/save.gif" alt="" id="IconSave" onmouseover="javascript:document.all[&#34;IconSave&#34;].src='../../../../Images/save2.gif';" onmouseout="javascript:document.all[&#34;IconSave&#34;].src='../../../../Images/save.gif';" title="Save" OnClientClick="return ValidateAndSave(); " OnClick="Save" />
       </td>
     </tr>
    </table>
   </div><br/>
   <div class="msgheader" id="formtitle">First Report Of Injury Options</div>
   <div class="errtextheader"></div>
   <table border="0">
    <tr>
     <td>
        <asp:TextBox runat="server" type="text" name="" value="" style="display:none" id="hdnAction"/>
      </td>
      <td>
        <asp:TextBox runat="server" type="text" name="" value="" style="display:none" id="hdnKey" rmxref="/Instance/Document/FROI/GetValueForKey"/>
      </td>
      <td>
        <asp:TextBox runat="server" type="text" name="" value="FROI" style="display:none" id="hdnForm" rmxref="/Instance/Document/FROI/Form"/>
      </td>
      <td>
        <asp:TextBox runat="server" style="display:none" id="hdnCode" />
      </td>
      </tr>
      <table border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td class="Selected"  nowrap="nowrap" id="TABScriteria"><a class="Selected" href="#" onclick="tabChange(this.name);return false;" name="criteria" id="LINKTABScriteria"><span style="text-decoration:none">Criteria Settings</span></a></td>
        <td   nowrap="nowrap" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td class="NotSelected" nowrap="nowrap" id="TABSFROIForms"><a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="FROIForms" id="LINKTABSFROIForms"><span style="text-decoration:none">First Report of Injury Forms</span></a></td>
        <td nowrap="nowrap" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
        <td valign="top" nowrap="nowrap"></td>
       </tr>
      </table>
      <%--<div class="singletopborder" style="position:relative;left:0;top:0;width:800px;height:315px;overflow:auto">--%>
       <table border="0" cellspacing="0" cellpadding="0">
        <tr>
         <td>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABcriteria" id="FORMTABcriteria">
           <tr>
        <td colspan=2>
        <table>
        <tr><td>Jurisdiction</td><td>Entity</td></tr>
        <tr><td>
        <asp:TextBox runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="JurisState" RMXRef="/Instance/Document/FROI/JurisState" RMXType="orgh" name="JurisState" cancelledvalue="" />
        <asp:button runat="server" class="CodeLookupControl" Text="" id="JurisStatebtn" onclientclick="return selectCode('STATES','JurisState');" OnClick="JurisState_Click" />
        <asp:TextBox style="display:none" runat="server" id="JurisState_cid" rmxref = "/Instance/Document/FROI/JurisState/@codeid" cancelledvalue="" />
        </td><td>
        <asp:TextBox runat="server" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="EntityOrgHier" RMXRef="/Instance/Document/FROI/SelOrgHier" RMXType="orgh" name="EntityOrgHier" cancelledvalue="" />
        <asp:button runat="server" class="CodeLookupControl" Text="" id="EntityOrgHierbtn" onclientclick="return selectCode('orgh','EntityOrgHier','ALL');" onclick="EntityOrgHier_Click" />
        <asp:TextBox style="display:none" runat="server" id="EntityOrgHier_cid" cancelledvalue="" rmxref ="/Instance/Document/FROI/SelOrgHier/@codeid" />
        </td><td>
        <%--MGaba2:MITS 19613--%>
        <asp:button class="button" runat="server" Text="Filter" ID="btnFilter" OnClick="btnFilter_Click" OnClientClick="fnMakeKeyValueBlank();"/>
        </td></tr>
        </table>
        </td>
        </tr>
        <tr><td colspan="2"><asp:label class="errortext1" runat="server" id="lblmessage" ForeColor="Red" Visible="false" text="Resultset is very large to load.Please filter for Jurisdiction/Entity"/></td></tr>
           <tr id="">
           <td>
            <table width="100%">
              <tr>
               <td width="80%">
                <div style="height:20%;overflow:inherit"> <%-- igupta3 Mits: 33301--%>
                  <dg:UserControlDataGrid runat="server" ID="SelEntityGrid" GridName="SelEntityGrid" GridTitle="Selected Entity" Target="/Document/FROI/OptionsList" Ref="/Instance/Document/form//control[@name='SelEntityGrid']" Unique_Id="Key" ShowRadioButton="true" Width="650px" Height="150px" hidenodes="|Key|" HideButtons="New|Edit|Delete" ShowHeader="True" LinkColumn="" Type="Grid" RowDataParam="listrow" OnClick="KeepRowForEdit('SelEntityGrid');LoadValues('SelEntityGrid');" />
                </div>
                </td>
                </tr>
                </table>
               </td>
              </tr>
            </td>
           <tr id="">
            <td colspan="2">
                 <script type="text/javascript" src="">{var i;}</script>
                 <asp:Button class="button"  runat="server" type="buttonscript" id="NewOpt" OnClientClick="Javascript: return NewRecord('SelEntityGrid');" Text="New"/>
                 <script type="text/javascript" src="">{var i;}</script>
                 <asp:Button class="button"  runat="server" type="buttonscript" id="DelOpt" OnClientClick="Javascript:return DeleteRecord('SelEntityGrid');" OnClick = "DeleteOptions" Text="Delete"/>
           </tr>
            <tr id="Tr1">
            <td colspan="2">&nbsp;</td>
            </tr>
           <tr id="">
            <td colspan="2">Select Which Description Will Appear On The Report (Where Applicable):</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Desc" name="Description" value="1" onclick="" id="OshaDesc" Checked="true" Text="OSHA Description" rmxref="/Instance/Document/FROI/Description"/></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Desc" name="Description" value="0" onclick="" id="EventDesc" Text="Event Description" rmxref="/Instance/Document/FROI/Description"/></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Claims Administrator (Where Applicable):</td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" type="radio" GroupName="ClaimsAdmin" name="ClaimAdmin" value="2" onclick="Javascript:Enable()" id="ClmAdm2" rmxref="/Instance/Document/FROI/ClaimAdministrator" Text="Client/Company Name (Org.Hiearchy):"/>
                <label style="width:10px"></label>
                <asp:DropDownList runat="server" type="combobox" name="ClmAdminOrgHier" id="ClmAdminOrgHier" onchange="setDataChanged(true);" rmxref="/Instance/Document/FROI/ClAdmOrgLvl">
                 <asp:ListItem value="1005">Client</asp:ListItem>
                 <asp:ListItem value="1006">Company</asp:ListItem>
                 <asp:ListItem value="1007">Operation</asp:ListItem>
                 <asp:ListItem value="1008">Region</asp:ListItem>
                 <asp:ListItem value="1009">Division</asp:ListItem>
                 <asp:ListItem value="1010">Location</asp:ListItem>
                 <asp:ListItem value="1011">Facility</asp:ListItem>
                 <asp:ListItem value="1012">Department</asp:ListItem>
               </asp:DropDownList>
            </td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="ClaimAdmin" type="radio" value="5" onclick="Javascript:Enable()" id="ClmAdm5" checked="true" Text="TPA (Third Party Administrator):" rmxref="/Instance/Document/FROI/ClaimAdministrator"/>
                <label style="width:10px"></label>
                <asp:TextBox runat="server" name="ClmAdmTPA" type="eidlookup" tableid="CLAIM_ADMIN_TPA" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="ClmAdmTPA" cancelledvalue="" rmxref="/Instance/Document/FROI/ClAdmTPAEID"/>
                <%--bkumar33:Cosmetic Changes--%>
                <asp:Button class="EllipsisControl"  runat="server" text="..." id="ClmAdmTPAbtn" OnClientClick=" return lookupData('ClmAdmTPA','CLAIM_ADMIN_TPA',4,'ClmAdmTPA',2)"/>
                <asp:TextBox runat="server" name="" style="display:none" id="ClmAdmTPA_cid" cancelledvalue="" rmxref="/Instance/Document/FROI/ClAdmTPAEID/@codeid"/>
            </td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="" value="4" onclick="Javascript:Enable()" id="ClmAdm4" Text="WC Claim Administrator - (Default):" rmxref="/Instance/Document/FROI/ClaimAdministrator"/>
                <label style="width:10px"></label>
                <asp:TextBox runat="server" name="" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="ClmAdmDef" cancelledvalue="" rmxref="/Instance/Document/FROI/ClAdmDefEID"/>
                <%--bkumar33:Cosmetic Changes--%>
                <asp:Button class="EllipsisControl"  runat="server" Text="..." id="ClmAdmDefbtn" OnClientClick="return lookupData('ClmAdmDef','WC_DEF_CLAIM_ADMIN',4,'ClmAdmDef',2)"/>
                <asp:TextBox runat="server" name=""  style="display:none" id="ClmAdmDef_cid" cancelledvalue="" rmxref="/Instance/Document/FROI/ClAdmDefEID/@codeid"/>
            </td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="" value="3" onclick="Javascript:Enable()" id="ClmAdm3" Text="Current Adjuster" rmxref="/Instance/Document/FROI/ClaimAdministrator"/>
            </td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ClaimsAdmin" name="" value="1" onclick="Javascript:Enable()" id="ClmAdm1" Text="Nothing" rmxref="/Instance/Document/FROI/ClaimAdministrator"/>
            </td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Carrier</td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="Carrier" name="" value="1" onclick="Javascript:Enable()" id="Carrier1" Text="Client/Company Name (Org.Hiearchy):" rmxref="/Instance/Document/FROI/Carrier" />
                <label style="width:10px"></label>
                <asp:DropDownList runat="server" name="" id="CarrierOrgHier" onchange="setDataChanged(true);" type="combobox" rmxref="/Instance/Document/FROI/CarrierOrgLevel">
                    <asp:ListItem value="1005">Client</asp:ListItem>
                    <asp:ListItem value="1006">Company</asp:ListItem>
                    <asp:ListItem value="1007">Operation</asp:ListItem>
                    <asp:ListItem value="1008">Region</asp:ListItem>
                    <asp:ListItem value="1009">Division</asp:ListItem>
                    <asp:ListItem value="1010">Location</asp:ListItem>
                    <asp:ListItem value="1011">Facility</asp:ListItem>
                    <asp:ListItem value="1012">Department</asp:ListItem>
                </asp:DropDownList>
            </td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="Carrier" name="" value="2" onclick="Javascript:Enable()" id="Carrier2" Text="Default Carrier:" rmxref="/Instance/Document/FROI/Carrier"/>
                <label style="width:10px"></label>
                <asp:TextBox runat="server" type="eidlookup" name="" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="CarrierDef" cancelledvalue="" rmxref="/Instance/Document/FROI/CarrierDefEID" />
                <%--bkumar33:Cosmetic Changes--%>
                <asp:Button class="EllipsisControl"  runat="server" type="button" text="..." id="CarrierDefbtn" OnClientClick="return lookupData('CarrierDef','INSURERS',4,'CarrierDef',2)" />
                <asp:TextBox runat="server" type="text" name="" style="display:none" id="CarrierDef_cid" cancelledvalue="" rmxref="/Instance/Document/FROI/CarrierDefEID/@codeid" />
            </td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton  runat="server" type="radio" GroupName="Carrier" name="" value="3" onclick="Javascript:Enable()" id="Carrier3" checked="true" Text="Linked By Policy Number" rmxref="/Instance/Document/FROI/Carrier" />
            </td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Contact Information</td>
           </tr>
           <tr id="">
            <td colspan="2">
                <asp:RadioButton runat="server" GroupName="ContactInfo" type="radio" name="ContactInfo" value="1" onclick="" id="ci1" Text="Use Only Supplemental Fields" rmxref="/Instance/Document/FROI/ContactInfo" />
            </td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="ContactInfo" type="radio" name="ContactInfo" value="2" onclick="" id="ci2" checked="true" rmxref="/Instance/Document/FROI/ContactInfo" Text="Use Only Org-Hierarchy Contact" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="ContactInfo" type="radio" name="ContactInfo" value="3" onclick="" id="ci3" Text="Use Supplemental Fields If Filled In, Else Use Org-Hierarchy" rmxref="/Instance/Document/FROI/ContactInfo" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Use Title or Position Code</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="UseTitle" type="radio" name="UseTitle" value="0" onclick="" id="ut1" rmxref="/Instance/Document/FROI/UseTitle" Text="Use Title If It Exists" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="UseTitle" type="radio" name="UseTitle" value="1" onclick="" id="ut2" checked="true" rmxref="/Instance/Document/FROI/UseTitle" Text="Always Use Position Code" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Source of Preparer Information:</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="5" onclick="" id="pi1" rmxref="/Instance/Document/FROI/PreparerInfo" Text="Adjuster" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="3" onclick="" id="pi2" rmxref="/Instance/Document/FROI/PreparerInfo" Text="Claim Created By" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="2" onclick="" id="pi3" rmxref="/Instance/Document/FROI/PreparerInfo" Text="Claim Last Edited By" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="1" onclick="" id="pi4" rmxref="/Instance/Document/FROI/PreparerInfo" Text="Workstation User" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PreparerInfo" type="radio" name="PreparerInfo" value="4" onclick="" id="pi5" checked="true" rmxref="/Instance/Document/FROI/PreparerInfo" Text="TPA Intake" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Attach To Claim By Default</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="AttachToClaim" type="radio" name="AttachToClaim" value="0" onclick="" id="atc1" checked="true" rmxref="/Instance/Document/FROI/AttachToClaim" Text="Yes" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="AttachToClaim" type="radio" name="AttachToClaim" value="1" onclick="" id="atc2" rmxref="/Instance/Document/FROI/AttachToClaim" Text="No" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Print Claim Number At Top Of Page:</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PrintClNumTop" type="radio" name="PrintClNumTop" value="1" onclick="" id="pc1" checked="true" Text="Yes" rmxref="/Instance/Document/FROI/PrintClaimNum" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="PrintClNumTop" type="radio" name="PrintClNumTop" value="0" onclick="" id="pc2" Text="No" rmxref="/Instance/Document/FROI/PrintClaimNum" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Claim Number Options</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="CarrClmNumOpt" type="radio" name="CarrClmNumOpt" value="1" onclick="" id="JurisSupp" checked="true" rmxref="/Instance/Document/FROI/CarrClmNumOpt" Text="Use Jurisdictional Supp Field" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="CarrClmNumOpt" type="radio" name="CarrClmNumOpt" value="0" onclick="" id="RMClaim" Text="Use RISKMASTER Claim Number" rmxref="/Instance/Document/FROI/CarrClmNumOpt" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="CarrClmNumOpt" type="radio" name="CarrClmNumOpt" value="2" onclick="" id="RMClaimNotJurisSupp" Text="Use RISKMASTER Claim Number if Jurisdictional Supp Field is Empty" rmxref="/Instance/Document/FROI/CarrClmNumOpt" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Force Printing of NCCI Codes:</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="NCCINode" type="radio" name="NCCINode" value="1" onclick="" id="nc1" Text="Yes" rmxref="/Instance/Document/FROI/PrintNCCI" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="NCCINode" type="radio" name="NCCINode" value="2" onclick="" id="nc2" checked="true" Text="No" rmxref="/Instance/Document/FROI/PrintNCCI" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Select The Organization Hierarchy Level for Employer Information:</td>
           </tr>
           <tr id="">
            <td><asp:DropDownList runat="server" type="combobox" name="EmployerInfo" id="EmployerInfo" onchange="setDataChanged(true);" rmxref="/Instance/Document/FROI/EmployerLevel" >
              <asp:ListItem value="1005">Client</asp:ListItem>
              <asp:ListItem value="1006">Company</asp:ListItem>
              <asp:ListItem value="1007">Operation</asp:ListItem>
              <asp:ListItem value="1008">Region</asp:ListItem>
              <asp:ListItem value="1009">Division</asp:ListItem>
              <asp:ListItem value="1010">Location</asp:ListItem>
              <asp:ListItem value="1011">Facility</asp:ListItem>
              <asp:ListItem value="1012" selected="True">Department</asp:ListItem></asp:DropDownList></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Select TPA As TPA Where Applicable</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="TPA" type="radio" name="BlankTPA" value="0" onclick="Javascript:Enable()" id="BlankTPA" Text="Leave Blank" Checked ="true" rmxref="/Instance/Document/FROI/TpaUseOpt" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="TPA" type="radio" name="DefaultTPA" value="1" onclick="Javascript:Enable()" id="DefaultTPA" checked="true" Text="Use A Default TPA" rmxref="/Instance/Document/FROI/TpaUseOpt" /> 
            <label style="width:10px"></label>
            <asp:textBox runat="server" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="TPALookup" cancelledvalue="" name="TPALookup" type="eidlookup" tableid="CLAIM_ADMIN_TPA" rmxref="/Instance/Document/FROI/FROITPAEID" />
            <%--bkumar33:Cosmetic Changes--%>
            <asp:Button class="EllipsisControl"  runat="server" type="button" Text="..." id="TPALookupbtn" OnClientClick="return lookupData('TPALookup','CLAIM_ADMIN_TPA',4,'TPALookup',2)" />
            <asp:TextBox runat="server" name="" style="display:none" id="TPALookup_cid" cancelledvalue="" rmxref="/Instance/Document/FROI/FROITPAEID/@codeid" /></td>
           </tr>
           <tr id="">
            <td colspan="2">Work Loss</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="WorkLoss" type="radio" name="WorkLoss" value="1" onclick="" id="wl1" checked="true" Text="If No Work Loss, Print '(No Work Loss)'" rmxref="/Instance/Document/FROI/WorkLoss" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="WorkLoss" type="radio" name="WorkLoss" value="2" onclick="" id="wl2" Text="If No Work Loss, Print Dates If They Exist Else '(No Work Loss)'" rmxref="/Instance/Document/FROI/WorkLoss" /></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" GroupName="WorkLoss" type="radio" name="WorkLoss" value="3" onclick="" id="wl3" Text="If No Work Loss And No Dates, Leave Fields Blank" rmxref="/Instance/Document/FROI/WorkLoss" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Jurisdiction of Nebraska: Insured or Parent Corporation</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Nebraska" name="Nebraska" value="1" onclick="Javascript:Enable()" id="Neb1" Text="Client/Company Name (Org.Hiearchy):" rmxref="/Instance/Document/FROI/NEParentOption" />
            <label style="width:10px"></label>
            <asp:DropDownList runat="server" name="" type="combobox" id="NebraskaOrgHier" onchange="setDataChanged(true);" rmxref="/Instance/Document/FROI/NEOrgHier" >
              <asp:ListItem value="1005">Client</asp:ListItem>
              <asp:ListItem value="1006">Company</asp:ListItem>
              <asp:ListItem value="1007">Operation</asp:ListItem>
              <asp:ListItem value="1007">Operation</asp:ListItem>
              <asp:ListItem value="1008">Region</asp:ListItem>
              <asp:ListItem value="1009">Division</asp:ListItem>
              <asp:ListItem value="1010">Location</asp:ListItem>
              <asp:ListItem value="1011">Facility</asp:ListItem>
              <asp:ListItem value="1012">Department</asp:ListItem></asp:DropDownList></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Nebraska" name="Nebraska" value="2" onclick="Javascript:Enable()" id="Neb2" Text="Parent Company (Default):" rmxref="/Instance/Document/FROI/NEParentOption" />
            <label style="width:10px"></label>
            <asp:TextBox runat="server" value="" size="30" onclick="Javascript:Enable()" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="NebraskaDef" cancelledvalue="" name="NebraskaDef" type="eidlookup" tableid="DEFAULT_PARENT" rmxref="/Instance/Document/FROI/NEDefID" />
            <%--bkumar33:Cosmetic Changes--%>
            <asp:Button class="EllipsisControl" runat="server" type="button" Text="..." id="NebraskaDefbtn" OnClientClick="return lookupData('NebraskaDef','DEFAULT_PARENT',4,'NebraskaDef',2)"/>
            <asp:TextBox runat="server" type="text" name="" style="display:none" id="NebraskaDef_cid" cancelledvalue=""  rmxref="/Instance/Document/FROI/NEDefID/@codeid"/></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Nebraska" name="Nebraska" value="3" onclick="Javascript:Enable()" id="Neb3" checked="true" Text="Linked By Policy (Traditional):" rmxref="/Instance/Document/FROI/NEParentOption" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Jurisdiction of Tennessesse: Insured or Parent Corporation</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Tennessesse" name="Tennessesse" value="1" onclick="Javascript:Enable()" id="Ten1" Text="Client/Company Name (Org.Hiearchy):" rmxref="/Instance/Document/FROI/TNParentOption" />
            <label style="width:10px"></label>
            <asp:DropDownList runat="server" name="TennessesseOrgHier" id="TennessesseOrgHier" type="combobox" onchange="setDataChanged(true);" rmxref="/Instance/Document/FROI/TNOrgHier">
              <asp:ListItem value="1005">Client</asp:ListItem>
              <asp:ListItem value="1006">Company</asp:ListItem>
              <asp:ListItem value="1007">Operation</asp:ListItem>
              <asp:ListItem value="1008">Region</asp:ListItem>
              <asp:ListItem value="1009">Division</asp:ListItem>
              <asp:ListItem value="1010">Location</asp:ListItem>
              <asp:ListItem value="1011">Facility</asp:ListItem>
              <asp:ListItem value="1012">Department</asp:ListItem></asp:DropDownList></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Tennessesse" name="Tennessesse" value="2" onclick="Javascript:Enable()" id="Ten2" Text="Parent Company (Default):" rmxref="/Instance/Document/FROI/TNParentOption" />
            <label style="width:10px"></label>
            <asp:TextBox runat="server" name="" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="TennessesseDef" cancelledvalue="" type="eidlookup" tableid="DEFAULT_PARENT" rmxref="/Instance/Document/FROI/TNDefID" />
            <%--bkumar33:Cosmetic Changes--%>
            <asp:Button class="EllipsisControl" runat="server" Text="..." id="TennessesseDefbtn" OnClientClick="return lookupData('TennessesseDef','DEFAULT_PARENT',4,'TennessesseDef',2)" />
            <asp:TextBox runat="server" type="text" name="" style="display:none" id="TennessesseDef_cid" cancelledvalue=""  rmxref="/Instance/Document/FROI/TNDefID/@codeid"/></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" name="Tennessesse" GroupName="Tennessesse" value="3" onclick="Javascript:Enable()" id="Ten3" checked="true" Text="Linked By Policy (Traditional):" rmxref="/Instance/Document/FROI/TNParentOption" /></td>
           </tr>
           <tr id="">
            <td colspan="2">&nbsp;</td>
           </tr>
           <tr id="">
            <td colspan="2">Jurisdiction of Virginia: Parent Corporation</td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" GroupName="Virginia" name="Virginia" value="1" onclick="Javascript:Enable()" id="Vir1" checked="true" Text="Client/Company Name (Org.Hiearchy):" rmxref="/Instance/Document/FROI/VAParentOption"/>
            <label style="width:10px"></label>
            <asp:DropDownList runat="server" type="combobox" name="VirginiaOrgHier" id="VirginiaOrgHier" onchange="setDataChanged(true);" rmxref="/Instance/Document/FROI/VAOrgHier" >
              <asp:ListItem value="1005" selected="True">Client</asp:ListItem>
              <asp:ListItem value="1006">Company</asp:ListItem>
              <asp:ListItem value="1007">Operation</asp:ListItem>
              <asp:ListItem value="1008">Region</asp:ListItem>
              <asp:ListItem value="1009">Division</asp:ListItem>
              <asp:ListItem value="1010">Location</asp:ListItem>
              <asp:ListItem value="1011">Facility</asp:ListItem>
              <asp:ListItem value="1012">Department</asp:ListItem></asp:DropDownList></td>
           </tr>
           <tr id="">
            <td colspan="2"><asp:RadioButton runat="server" type="radio" name="Virginia" GroupName="Virginia" value="2" id="Vir2" onclick="Javascript:Enable()" Text="Parent Company (Default):" rmxref="/Instance/Document/FROI/VAParentOption" />
            <label style="width:10px"></label>
            <asp:TextBox runat="server" name="" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="VirginiaDef" cancelledvalue=""  type="eidlookup" tableid="DEFAULT_PARENT" rmxref="/Instance/Document/FROI/VADefID" />
            <%--bkumar33:Cosmetic Changes--%>
            <asp:Button class="EllipsisControl" runat="server" Text="..." id="VirginiaDefbtn" OnClientClick="return lookupData('VirginiaDef','DEFAULT_PARENT',4,'VirginiaDef',2)" />
            <asp:TextBox runat="server" type="text" name="" style="display:none" id="VirginiaDef_cid" cancelledvalue="" rmxref="/Instance/Document/FROI/VADefID/@codeid"  /></td>
           </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" name="FORMTABFROIForms" id="FORMTABFROIForms" style="display:none;">
           <tr id="">
            <td colspan="2"><b></b><table width="100%">
              <tr>
               <td width="80%">
                <div style="overflow:auto;">
               <%-- athapa2:added function KeepRowForEdit() on onclick of UserControlDataGrid--%>
                <dg:UserControlDataGrid runat="server" ID="FROIFormsGrid" GridName="FROIFormsGrid" OnClick="KeepRowForEdit('FROIFormsGrid');" GridTitle="" Target="/Document/FROI/FROIFormsList" ref="/Instance/Document/form//control[@name='FROIForms']" Unique_Id="FormID" ShowRadioButton="true" Width="80%" Height="100%" hidenodes="|FormID|" HideButtons="New|Edit|Delete" ShowHeader="false" LinkColumn="" Type="Grid" RowDataParam="listrow" /><%-- igupta3 Mits: 33301--%>
                </div>
               </td>
              </tr>
              <%--Bijender has changed for Mits 15669--%>
             </table><script type="text/javascript" src="">{var i;}</script><asp:Button class="button" runat="server" id="PrintHis" OnClientClick="Javascript:return PrintHistory();" Text="Print History" /></td>
             <%--Bijender End Mits 15669--%>
           </tr>
          </table>
         </td>
         <td valign="top"></td>
        </tr>
       </table>
       
      <table>
       <tr>
        <td></td>
       </tr>
      </table>
      <asp:TextBox style="display:none" runat="server" id="SelEntitySelectedId"  RMXType="id" />
      <asp:TextBox Style="display:none" runat="server" ID="FROIFormsSelectedId" RMXType="id" />
      <asp:TextBox style="display:none" runat="server" id="RecordCount" rmxref = "/Instance/Document/FROI/RecordCount" RMXType="id" />
      <input type="text" name="" value="" id="SysViewType" style="display:none"/>
      <input type="text" name="" value="" id="SysCmd" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdConfirmSave" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdQueue" style="display:none"/>
      <input type="text" name="" value="" id="SysCmdText" style="display:none" rmxforms:value="Navigate"/>
      <input type="text" name="" value="" id="SysClassName" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysSerializationConfig" style="display:none"/>
      <input type="text" name="" value="" id="SysFormIdName" style="display:none" rmxforms:value="Key"/>
      <input type="text" name="" value="" id="SysFormPIdName" style="display:none" rmxforms:value="Key" />
      <input type="text" name="" value="" id="SysFormPForm" style="display:none" rmxforms:value="FROIOptions"/>
      <input type="text" name="" value="" id="SysInvisible" style="display:none" rmxforms:value=""/>
      <input type="text" name="" value="" id="SysFormName" style="display:none" rmxforms:value="FROIOptions"/>
      <input type="text" name="" value="" id="SysRequired" style="display:none"/>
      <input type="text" name="" value="" id="SysFocusFields" style="display:none"/>
      
     <input type="hidden" name="" value="rmx-widget-handle-7" id="SysWindowId" />
    </form>
</body>
</html>
