﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.JurisdictionalOptions
{
    public partial class JurisOptions : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        private XElement oMessageElement = null;
        private string message = string.Empty; 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (hdnAction.Value != "Delete" && hdnAction.Value != "Save")
                {
                    CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
                    if (Int32.Parse(RecordCount.Text) > 100)
                        lblmessage.Visible = true;
                    else
                        lblmessage.Visible = false;
                    BindRemainingControls(sCWSresponse);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        private void BindRemainingControls(string sResponse)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sResponse);

            XmlNode xmlNode = xmlDoc.SelectSingleNode("ResultMessage/Document/Juris/EmployerLevel");
            EmployerInfo.SelectedValue = xmlNode.InnerXml;

            DataTable dtGridData = new DataTable();
            GridHeaderAndData(xmlDoc.SelectSingleNode("ResultMessage/Document/Juris/OptionsList/listhead"), xmlDoc.SelectNodes("ResultMessage/Document/Juris/OptionsList/listrow"), dtGridData);

            JurisGrid.DataSource = dtGridData;
            JurisGrid.DataBind();
        }

        private void GridHeaderAndData(XmlNode objGridHeaders, XmlNodeList objGridData, DataTable dtGridData)
        {
            DataColumn dcGridColumn;
            DataRow dr = null;
            int iColumnIndex = 0;


            if (objGridHeaders != null && objGridHeaders.InnerXml != "")
            {
                // Create the Columns corresponding to the Headers specified in ListHead section of Xml
                foreach (XmlElement objHeaderDetails in objGridHeaders)
                {
                    //Add Column to be associated with Datasource of grid
                    dcGridColumn = new DataColumn();
                    // Assign the Column Name from XmlNode
                    dcGridColumn.ColumnName = objHeaderDetails.Name;

                    dtGridData.Columns.Add(dcGridColumn);

                }
            }
            //Add rows to be associated with Datasource of grid
            if (objGridData != null && objGridData.Count != 0)
            {
                foreach (XmlNode objNodes in objGridData)
                {
                    dr = dtGridData.NewRow();

                    iColumnIndex = 0;
                    foreach (XmlElement objElem in objNodes)
                    {
                        dr[iColumnIndex++] = objElem.InnerXml;
                    }
                    dtGridData.Rows.Add(dr);                   
                }
                //MGaba2:MITS 19800:showing blank entry after deletion of all the criterias
                //Resetting the cssclass in case data is present in the grid
                JurisGrid.RowStyle.CssClass = "data2"; //igupta3 Mits# 33301 - changed class from visiblerowcol to data2
            }
            else
            {
                dr = dtGridData.NewRow();
                dtGridData.Rows.Add(dr);
                //MGaba2:MITS 19800:showing blank entry after deletion of all the criterias
                //Hiding first row in case no row is present in the grid
                JurisGrid.RowStyle.CssClass = "hiderowcol";          
               
            }



        }


        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            try
            {

                using (XmlReader reader = Xelement.CreateReader())
                {
                    xmlNodeDoc.Load(reader);
                }
                XmlDocument objDoc = new XmlDocument();
                XmlElement newEle = xmlNodeDoc.CreateElement("OptionsList");
                newEle.InnerXml = (@"
                            <listhead>
                            <Jurisdiction>Jurisdictions</Jurisdiction> 
                            <Entity>Entity</Entity> 
                            <Org-level>Org-Level</Org-level> 
                            <Key>key</Key> 
                            </listhead>
                              ");



                XmlNode objXmlNode = xmlNodeDoc.SelectSingleNode("Message/Document/Juris");
                objXmlNode.AppendChild((XmlNode)newEle);

                Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
            }
            catch(Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void DelOpt_Click(object sender, EventArgs e)
        {
             XmlDocument xmlInput = new XmlDocument();
             xmlInput.LoadXml(@"
                <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>FROIOptionsAdaptor.Delete</Function> 
                </Call>
                <Document>
                <Juris>
                <GetValueForKey>" + hdnKey.Text + @"</GetValueForKey> 
                </Juris></Document>
                </Message>");
             hdnAction.Value = "";
             AppHelper.CallCWSService(xmlInput.InnerXml);
             hdnKey.Text = "";
            CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
            if (Int32.Parse(RecordCount.Text) > 100)
                lblmessage.Visible = true;
            else
                lblmessage.Visible = false;
            BindRemainingControls(sCWSresponse);

        }
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                oMessageElement = null;
                CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
                if (Int32.Parse(RecordCount.Text) > 100)
                    lblmessage.Visible = true;
                else
                    lblmessage.Visible = false;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void JurisState_Click(object sender, EventArgs e)
        {
            CallCWSFunction("CodesListAdaptor.GetCodes");
        }

        protected void EntityOrgHier_Click(object sender, EventArgs e)
        {
            CallCWSFunction("OrgHierarchyAdaptor.GetOrgHierarchyXml");
        }

        protected void Save_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                hdnAction.Value = "";
                CallCWS("FROIOptionsAdaptor.Save", oMessageElement, out sCWSresponse, true, true);
                BindRemainingControls(sCWSresponse);

                oMessageElement = null;
                sCWSresponse = "";

                CallCWS("FROIOptionsAdaptor.GetFROIOptions", oMessageElement, out sCWSresponse, true, true);
                if (Int32.Parse(RecordCount.Text) > 100)
                    lblmessage.Visible = true;
                else
                    lblmessage.Visible = false;
                BindRemainingControls(sCWSresponse);

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
