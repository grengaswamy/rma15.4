﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Xml.XPath;
namespace Riskmaster.UI.UI.Utilities.Manager
{
    public partial class AdjusterTransfer : NonFDMBasePageCWS
    {
        XElement oMessageElement = null;
        private string sCWSresponse = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hdnFromAdjuster.Text = "";
                hdnToAdjuster.Text = "";
                CallCWS("AdjusterTransferAdaptor.Get", null, out sCWSresponse, true, true);
            }
            else
            {
                hdnFromAdjuster.Text = lstFromAdjuster.SelectedValue;
                hdnToAdjuster.Text = lstToAdjuster.SelectedValue;
                hdnSelectedClaims.Text = lstTabs.SelectedValue;
                hdnOnlyOpenClaims.Text = chkOpenClaims.Checked.ToString();
            }

        }

        protected void lstFromAdjuster_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("AdjusterTransferAdaptor.Get", null, out sCWSresponse, true, true);
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        oMessageElement = GetMessageTemplate();
                        CallCWS("AdjusterTransferAdaptor.GetAvailableClaims", oMessageElement, out sCWSresponse, false, true);
                        lstFromAdjuster.SelectedValue = hdnFromAdjuster.Text;
                        lstToAdjuster.SelectedValue = hdnToAdjuster.Text;
                        chkOpenClaims.Checked = Convert.ToBoolean(hdnOnlyOpenClaims.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            UpdateListItems();
            bReturnStatus = CallCWS("AdjusterTransferAdaptor.Save", null, out sCWSresponse, true, false);
            if (bReturnStatus)
            {
                try
                {
                    XElement oMessageElement = GetMessageTemplate();
                    lstTabs.Items.Clear();
                    lstTopDowns.Items.Clear();
                    CallCWS("AdjusterTransferAdaptor.Get", oMessageElement, out sCWSresponse, false, true);

                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        protected void UpdateListItems()
        {
            lstTabs.Items.Clear();
            lstTopDowns.Items.Clear();
            txtTopDown.Text = "";
            txtlstTabs.Text = "";

            char[] splitter = { ';' };
            string svalues = string.Empty;
            string[] lsthndValues = hndlstTabs.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (!lstTabs.Items.Contains(listitem))
                            {
                                lstTabs.Items.Add(listitem);
                                svalues += svalue[0] + " ";

                            }

                            if (lstTopDowns.Items.Contains(listitem))
                            {
                                lstTopDowns.Items.Remove(listitem);

                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        //MGaba2:MITS 21665:Client gets an Index was out of range error
                        //Commenting below statement as when we have duplicate claim entries for an adjuster,it was crashing
                        //lstTabs.Items[Icount].Selected = true;
                    }
                }
                if (svalues.Length > 0)
                {
                    txtlstTabs.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndlstTabs.Value = "";
            }


            lsthndValues = hndTopDown.Value.Split(splitter);
            if (lsthndValues.Length > 1)
            {
                char[] splitterEqual = { '=' };
                for (int Icount = 0; Icount < lsthndValues.Length - 1; Icount++)
                {
                    string[] svalue = lsthndValues[Icount].Split(splitterEqual);
                    if (svalue.Length > 1)
                    {
                        ListItem listitem = new ListItem(svalue[1], svalue[0]);
                        try
                        {
                            if (lstTabs.Items.Contains(listitem))
                            {
                                lstTabs.Items.Remove(listitem);

                            }
                            if (!lstTopDowns.Items.Contains(listitem))
                            {
                                lstTopDowns.Items.Add(listitem);
                                svalues += svalue[0] + " ";
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHelper.logErrors(ex);
                            BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                            err.Add(ex, BusinessAdaptorErrorType.SystemError);
                            ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                        }
                        //MGaba2:MITS 21665:Client gets an Index was out of range error
                        //Commenting below statement as when we have duplicate claim entries for an adjuster,it was crashing
                        //lstTopDowns.Items[Icount].Selected = true;
                    }
                }
                if (svalues.Length > 0)
                {
                    txtTopDown.Text = svalues.Substring(0, svalues.Length - 1);
                }
                svalues = "";
                hndTopDown.Value = "";
            }



        }


        protected void chkOpenClaims_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                bReturnStatus = CallCWS("AdjusterTransferAdaptor.Get", null, out sCWSresponse, true, true);

                hdnSelectedClaims.Text = "";
                if (bReturnStatus)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        oMessageElement = GetMessageTemplate();
                        CallCWS("AdjusterTransferAdaptor.GetAvailableClaims", oMessageElement, out sCWSresponse, false, true);
                        lstFromAdjuster.SelectedValue = hdnFromAdjuster.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>AdjusterTransferAdaptor.GetAvailableClaims</Function></Call>");
            sXml = sXml.Append("<Document><Adjuster><control name=\"AvailClaims\">");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"FromAdjuster\">");
            sXml = sXml.Append(hdnFromAdjuster.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"ToAdjuster\" >");
            sXml = sXml.Append(hdnToAdjuster.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"SelectedClaims\" >");
            sXml = sXml.Append(hdnSelectedClaims.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name=\"OnlyOpenClaims\">");
            sXml = sXml.Append(hdnOnlyOpenClaims.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</Adjuster></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}

