﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System.Collections.Generic;
using System.Drawing.Printing;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.ToolsDesigners.AutoMailMergeSetUp
{
    public partial class AutoMailMergefilterWizard : System.Web.UI.Page
    {
        //RMA -18414
        private string sPageID = RMXResourceProvider.PageId("AutoMailMergefilterWizard.aspx");
        public string sLangId = AppHelper.GetLanguageCode();              

        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument autoMailMergeDoc = null;
            Button stepFinishedButton = null;
            //RMA -18414
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AutoMailMergefilterWizard.aspx"), "AutoMailMergeValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "AutoMailMergeValidations", sValidationResources, true);
            

            try
            {
                //Start - vkumar258-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 4);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }
                //End - vkumar258-ML Changes

                if (!IsPostBack)
                {
                    if (Request.QueryString["defid"] != null)
                    {
                        ViewState["defid"] = Request.QueryString["defid"];
                    }
                    else
                    {
                        ViewState["defid"] = "";
                    }

                    autoMailMergeDoc = GetAutoMailMergeFilter();
                    ViewState["autoFilterMailMergeDoc"] = autoMailMergeDoc.OuterXml;

                    //setting style of finish button in case of edit
                    if (ViewState["defid"].ToString() != "")
                    {
                        if (CtrlAutoMailMergeWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoMailMergeWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton");
                            stepFinishedButton.Enabled = true;
                        }

                        ShowAutoFilterWizard();
                    }
                    else //in case of new 
                    {
                        if (CtrlAutoMailMergeWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoMailMergeWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StepFinishButton");
                            stepFinishedButton.Enabled = false;
                        }

                        ShowBestPracticeDiaryWizard();

                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        #region Wizard Control Navigation Buttons Events

        protected void CtrlAutoMailMergeWizard_NextButtonClick(object sender, EventArgs e)
        {
            Button stepFinishedButton = null;

            try
            {
                if (CtrlAutoMailMergeWizard.ActiveStepIndex == 0)
                {
                    PopulateAutoFilterStep2Controls();

                    //enabling and disabling Finish button
                    if (CtrlAutoMailMergeWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                    {
                        stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoMailMergeWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton");

                        if (ViewState["defid"].ToString() != "")
                        {
                            stepFinishedButton.Enabled = true;
                            lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 2 of 5 (Business Rule Definition)";
                        }
                        else
                        {
                            stepFinishedButton.Enabled = false;
                            lblMessgeTitle.Text = "New Auto Mail Merge - Step 2 of 5 (Business Rule Definition)";
                        }
                    }
                }
                else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 1)
                {
                    PopulateAutoFilterStep3Controls();

                    //enabling and disabling Finish button
                    if (CtrlAutoMailMergeWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                    {
                        stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoMailMergeWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton");

                        if (ViewState["defid"].ToString() != "")
                        {
                            stepFinishedButton.Enabled = true;
                            lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 3 of 5 (Select Template & Apply Additional Business Rule)";
                        }
                        else
                        {
                            stepFinishedButton.Enabled = false;
                            lblMessgeTitle.Text = "New Auto Mail Merge - Step 3 of 5 (Select Template & Apply Additional Business Rule)";
                        }
                    }
                }

                else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 2)
                {
                    PopulateAutoFilterStep4Controls();

                    //enabling and disabling Finish button
                    if (CtrlAutoMailMergeWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                    {
                        stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoMailMergeWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepFinishButton");

                        if (ViewState["defid"].ToString() != "")
                        {
                            stepFinishedButton.Enabled = true;
                            lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 4 of 5 (Select Entity Roles)";
                        }
                        else
                        {
                            stepFinishedButton.Enabled = false;
                            lblMessgeTitle.Text = "New Auto Mail Merge - Step 4 of 5 (Select Entity Roles)";
                        }
                    }

                    Button nextButton = (Button)this.FindControl("CtrlAutoMailMergeWizard$StepNavigationTemplateContainerID$StepNextButton");
                    nextButton.Attributes.Add("onclick", "return AutoMailMergeFilterRole();");
                }

                else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 3)
                {
                    PopulateAutoFilterStep5Controls();

                        //enabling and disabling Finish button
                        if (CtrlAutoMailMergeWizard.FindControl("FinishNavigationTemplateContainerID").FindControl("StepFinishButton") != null)
                        {
                            stepFinishedButton = (System.Web.UI.WebControls.Button)CtrlAutoMailMergeWizard.FindControl("FinishNavigationTemplateContainerID").FindControl("StepFinishButton");

                        //if (AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value) == "<SelectedFilters></SelectedFilters>") //Bharani - MITS : 35879
                        if (hdnSelectedFiltersNode.Value == "<SelectedFilters></SelectedFilters>")
                        {
                            stepFinishedButton.Enabled = false;
                        }
                        else
                        {
                            stepFinishedButton.Enabled = true;
                        }

                        if (ViewState["defid"].ToString() != "")
                        {
                            lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 5 of 5 (Select Printer)";
                        }
                        else
                        {
                            lblMessgeTitle.Text = "New Auto Mail Merge - Step 5 of 5 (Select Printer)";
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void CtrlAutoMailMergeWizard_PreviousButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (CtrlAutoMailMergeWizard.ActiveStepIndex == 1)
                {
                    PopulateAutoFilterStep1Controls();

                    if (ViewState["defid"].ToString() != "")
                    {
                        lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 1 of 5 (Best Practice Scenerio)";
                    }
                    else
                    {
                        lblMessgeTitle.Text = "New Auto Mail Merge - Step 1 of 5 (Best Practice Scenerio)";
                    }
                }
                else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 2)
                {
                    PopulateAutoFilterStep2Controls();

                    if (ViewState["defid"].ToString() != "")
                    {
                        lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 2 of 5 (Business Rule Definition)";

                    }
                    else
                    {
                        lblMessgeTitle.Text = "New Auto Mail Merge - Step 2 of 5 (Business Rule Definition)";
                    }
                }
                else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 3)
                {
                    PopulateAutoFilterStep3Controls();

                    if (ViewState["defid"].ToString() != "")
                    {
                        lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 3 of 5 (Select Template & Apply Additional Business Rule)";

                    }
                    else
                    {
                        lblMessgeTitle.Text = "New Auto Mail Merge - Step 3 of 5 (Select Template & Apply Additional Business Rule)";
                    }
                }
                else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 4)
                {
                    PopulateAutoFilterStep4Controls();

                    if (ViewState["defid"].ToString() != "")
                    {
                        lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 4 of 5 (Select Entity Roles)";

                    }
                    else
                    {
                        lblMessgeTitle.Text = "New Auto Mail Merge - Step 4 of 5 (Select Entity Roles)";
                    }
                }

                else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 5)
                {
                    PopulateAutoFilterStep5Controls();

                    if (ViewState["defid"].ToString() != "")
                    {
                        lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 5 of 5 (Select Printer)";

                    }
                    else
                    {
                        lblMessgeTitle.Text = "New Auto Mail Merge - Step 5 of 5 (Select Printer)";
                    }
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void CtrlAutoMailMergeWizard_FinishButtonClick(object sender, EventArgs e)
        {
            try
            {
                SaveAutoMailMergeFinalData();
              
                Page.ClientScript.RegisterClientScriptBlock(typeof(string), "ee", "<Script>windowclose()</Script>");
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void CtrlAutoMailMergeWizard_CancelButtonClick(object sender, EventArgs e)
        {

        }

        private void ShowAutoFilterWizard()
        {
            CtrlAutoMailMergeWizard.Visible = true;

            CtrlAutoMailMergeWizard.ActiveStepIndex = 0;

            PopulateAutoFilterStep1Controls();

            lblMessgeTitle.Visible = true;
            if (ViewState["defid"].ToString() != "")
            {
                lblMessgeTitle.Text = "Edit Auto Mail Merge - Step 1 of 5 (Best Practice Scenerio)";
            }
            else
            {
                lblMessgeTitle.Text = "New Auto Mail Merge - Step 1 of 5 (Best Practice Scenerio)";
            }

            CtrlBestPracticeMailMergeWizard.Visible = false;
        }

        private void ShowBestPracticeDiaryWizard()
        {
            CtrlBestPracticeMailMergeWizard.Visible = true;
            CtrlBestPracticeMailMergeWizard.ActiveStepIndex = 0;
            CtrlAutoMailMergeWizard.Visible = false;
            lblMessgeTitle.Visible = true;
            lblMessgeTitle.Text = "Auto mail Merge Creation - Step 1 of 5 (Introduction)";
        }

        #endregion Wizard Control Navigation Buttons Events

        #region AutoMailMergeWizard Steps

        #region WizardStep1

        private void PopulateAutoFilterStep1Controls()
        {
            XmlDocument autoMailMergeDoc = null;
            XmlNode mailMergeWizardNode = null;
            XmlNode mailMergeTemplateNode = null;
            XmlNodeList mailMergeTemplateNodeList = null;
            ListItem cboTemplateItem = null;

            if (hdnAutoFilterStep1Visited.Value == "")
            {
                autoMailMergeDoc = GetStoredAutoMailMergeDoc();
                mailMergeWizardNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard");

                mailMergeTemplateNode = mailMergeWizardNode.SelectSingleNode("AutoMailMergeTemplate");
                mailMergeTemplateNodeList = mailMergeTemplateNode.SelectNodes("level");

                if (mailMergeTemplateNodeList.Count > 0)
                {
                    cboTemplate.Items.Clear();
                    foreach (XmlNode levelNode in mailMergeTemplateNodeList)
                    {
                        cboTemplateItem = new ListItem();
                        cboTemplateItem.Text = levelNode.Attributes["name"].Value;
                        cboTemplateItem.Value = levelNode.Attributes["value"].Value;
                        cboTemplate.Items.Add(cboTemplateItem);
                    }

                    if (mailMergeTemplateNode.Attributes["selectedid"].Value != "")
                    {
                        cboTemplate.SelectedValue = mailMergeTemplateNode.Attributes["selectedid"].Value;
                    }
                }


                txtMailMergeName.Text = mailMergeWizardNode.SelectSingleNode("AutoMailMergeName").InnerText;

                txtMailMergeDate.Value = mailMergeWizardNode.SelectSingleNode("AutoMailMergeDate").InnerText;

                hdnMailMergeId.Value = mailMergeWizardNode.SelectSingleNode("DefId").InnerText;
            }

            hdnAutoFilterStep1Visited.Value = "true";

        }  //Step1

        #endregion

        #region WizardStep2

        private void PopulateAutoFilterStep2Controls()
        {
            XmlDocument autoMailMergeDoc = null;
            XmlNodeList availableFilterNodeList = null;
            XmlNode selectedFiltersNode = null;
            XmlNodeList selectedFilterNodeList = null;
            ListItem availableFilterItem = null;
            ListItem selectedFilterItem = null;
            hdnAutoFilterStep2.Value = "1";
            hdnAutoFilterStep3.Value = null;
            if (hdnTemplateId.Value != cboTemplate.SelectedItem.Value)
            {
                autoMailMergeDoc = GetStoredAutoMailMergeDoc();
                availableFilterNodeList = autoMailMergeDoc.SelectNodes("ResultMessage/Document/AutoMailMergeWizard/Filters/AvlFilters/level[@templateid ='" + cboTemplate.SelectedItem.Value + "']");

                AvailableFilters.Items.Clear();

                if (availableFilterNodeList.Count > 0)
                {
                    foreach (XmlNode levelNode in availableFilterNodeList)
                    {
                        availableFilterItem = new ListItem();

                        availableFilterItem.Text = levelNode.Attributes["name"].Value;
                        availableFilterItem.Value = levelNode.Attributes["id"].Value;

                        AvailableFilters.Items.Add(availableFilterItem);
                    }

                }

                hdnSelectedFiltersNode.Value = "";
                selectedFiltersNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/Filters/SelectedFilters");
                hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                SelectedFilters.Items.Clear();
                if (AvailableFilters.Items.Count > 0)
                {
                    selectedFilterNodeList = selectedFiltersNode.SelectNodes("level");
                    if (selectedFilterNodeList.Count > 0)
                    {
                        foreach (XmlNode levelNode in selectedFilterNodeList)
                        {
                            selectedFilterItem = new ListItem();

                            selectedFilterItem.Text = levelNode.Attributes["name"].Value;
                            selectedFilterItem.Value = levelNode.Attributes["id"].Value;

                            SelectedFilters.Items.Add(selectedFilterItem);
                            AvailableFilters.Items.Remove(selectedFilterItem);
                        }
                    }
                }
            }

            hdnTemplateId.Value = cboTemplate.SelectedItem.Value;
        } //DiaryWizardstep4

        #endregion

        #region WizardStep3

        private void PopulateAutoFilterStep3Controls()
        {
            XmlDocument autoMailMergeDoc = null;
            XmlNode availableTemplateNode = null;
            XmlNodeList availableTemplateNodeList = null;
            XmlNode selectedFiltersNode = null;
            XmlNodeList selectedFilterNodeList = null;
            ListItem availableFilterItem = null;
            ListItem selectedFilterItem = null;
            hdnAutoFilterStep2.Value = null;
            hdnAutoFilterStep3.Value = "1";

            XmlNode availablecategoryIDNode = null;
            XmlNode availablecategoryNameNode = null;

            string sDefID = string.Empty;
            sDefID =  ViewState["defid"].ToString();
            XmlNode editAvailableTemplateNode = null;
            XmlNodeList editAvailableTemplateNodeList = null;
            XmlNode selectEditTempFilterNode = null;
            XmlNode editAvailableFilterNode = null;
            XmlNode editSelectedFilterNode = null;
            XmlNode editAvailableFilterNodeList = null;
            XmlNode editSelectedFilterNodeList = null;
            XmlNode editselectedAvailableLevelNode= null;
            ListItem editavailableFilterItem = null;
            ListItem editselectedFilterItem = null;

            XmlNodeList editSelectedTemplateNodeList = null;
             
            autoMailMergeDoc = GetAutoMailMergeFilter();
            ViewState["autoMailMergeTemplate"] = autoMailMergeDoc.OuterXml;

            XmlNode selectFilter = null;
            XmlNodeList selectFilterList = null;
            selectFilter = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList");
            selectFilterList = selectFilter.SelectNodes("Template");

            if (sDefID == string.Empty)
            {
                //if (selectFilterList.Count == 0 && string.IsNullOrEmpty(AppHelper.HTMLCustomDecode(hdnSelectedTemplateFiltersNode.Value)))//Bharani - MITS : 35879
                if (selectFilterList.Count == 0 && string.IsNullOrEmpty(hdnSelectedTemplateFiltersNode.Value))
                {
                    hdnSelectedTemplateFiltersNode.Value = selectFilter.OuterXml;
                    //hdnSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectFilter.OuterXml);//Bharani - MITS : 35879
                }
            }
            else
            {
                if (selectFilterList.Count == 0 && string.IsNullOrEmpty(hdneditSelectedTemplateFiltersNode.Value))
                {
                    hdneditSelectedTemplateFiltersNode.Value = selectFilter.OuterXml;
                    //hdneditSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectFilter.OuterXml);//Bharani - MITS : 35879
                }
            }


            if (sDefID == string.Empty)
            {
                if (string.IsNullOrEmpty(hdnTemplateFormID.Value))
                {
                    cbomailmergetemp.Items.Clear();
                    availableTemplateNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableTemplateList");
                    availableTemplateNodeList = availableTemplateNode.SelectNodes("Template");
                    if (availableTemplateNodeList.Count > 0)
                    {
                        foreach (XmlNode levelNode in availableTemplateNodeList)
                        {
                            availableFilterItem = new ListItem();

                            availableFilterItem.Text = levelNode.Attributes["TemplateDesc"].Value;
                            availableFilterItem.Value = levelNode.Attributes["TemplateId"].Value;

                            cbomailmergetemp.Items.Add(availableFilterItem);
                        }

                    }

                    lstAvTempFilter.Items.Clear();
                    lstSelTempFilter.Items.Clear();

                }
                else
                {
                    cbomailmergetemp.SelectedItem.Value = hdnTemplateFormID.Value;
                    XmlDocument selectedFiltersDoc = new XmlDocument();

                    selectedFiltersDoc.LoadXml(hdnSelectedTemplateFiltersNode.Value);
                    //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879
                    selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                    autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard").RemoveChild(selectFilter);
                    autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard").AppendChild(autoMailMergeDoc.ImportNode(selectedFiltersNode, true));

                    lstSelTempFilter.Items.Clear();
                    
                    selectedFilterNodeList = selectedFiltersNode.SelectNodes("Template");

                    string sFilterName = string.Empty;
                    string sFilterID = string.Empty;
                    foreach (XmlNode levelNodeFilters in selectedFilterNodeList)
                    {
                        sFilterName = levelNodeFilters.Attributes["SelectedFilterName"].Value;
                        sFilterID = levelNodeFilters.Attributes["SelectedFilterID"].Value;
                    }

                    if (lstAvTempFilter.Items.Count >= 0 && !string.IsNullOrEmpty(sFilterName) && !string.IsNullOrEmpty(sFilterID))
                    {
                        selectedFilterNodeList = selectedFiltersNode.SelectNodes("Template");
                        if (selectedFilterNodeList.Count > 0)
                        {
                            foreach (XmlNode levelNode in selectedFilterNodeList)
                            {
                                selectedFilterItem = new ListItem();

                                selectedFilterItem.Text = levelNode.Attributes["SelectedFilterName"].Value;
                                selectedFilterItem.Value = levelNode.Attributes["SelectedFilterID"].Value;

                                lstSelTempFilter.Items.Add(selectedFilterItem);
                                lstAvTempFilter.Items.Remove(selectedFilterItem);
                            }
                        }

                    }

                    MultiCodeLookUp MuserControl = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstClaimantType");
                    TextBox txtUserControl = (TextBox)MuserControl.FindControl("multicode_lst");
                    if (!string.IsNullOrEmpty(txtUserControl.Text))
                    {
                        hdnClaimantvalues.Value = MuserControl.CodeId;
                    }

                    MultiCodeLookUp MuserControlDefendant = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstDefendantType");
                    TextBox txtUserControlDefendant = (TextBox)MuserControlDefendant.FindControl("multicode_lst");
                    if (!string.IsNullOrEmpty(txtUserControlDefendant.Text))
                    {
                        hdnDefendantvalues.Value = MuserControlDefendant.CodeId;
                    }

                    MultiCodeLookUp MuserControlExpert = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstExpertType");
                    TextBox txtUserControlExpert = (TextBox)MuserControlExpert.FindControl("multicode_lst");
                    if (!string.IsNullOrEmpty(txtUserControlExpert.Text))
                    {
                        hdnExpertvalues.Value = MuserControlExpert.CodeId;
                    }

                    ListBox lstPersonInvolued = (ListBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued");
                    TextBox txtPersonInvolved = (TextBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued_lst");
                    if (!string.IsNullOrEmpty(txtPersonInvolved.Text))
                    {
                            string sPersonInvolvedValue = txtPersonInvolved.Text.ToString();
                            string sPersonInvolvedTmp = string.Empty;
                            string sPersonInvolvedCodes = string.Empty;
                            if (sPersonInvolvedValue != string.Empty)
                            {
                                sPersonInvolvedTmp = sPersonInvolvedValue.Trim();
                                sPersonInvolvedCodes = sPersonInvolvedTmp.Replace(" ", ",");
                                sPersonInvolvedCodes = sPersonInvolvedCodes + ",";
                                hdnPersonInvolvedValue.Value = sPersonInvolvedCodes.Substring(0, sPersonInvolvedCodes.Length - 1);
                            }
                            else
                            {
                                hdnPersonInvolvedValue.Value = sPersonInvolvedCodes;
                            }
                           
                            
                     }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(hdneditTemplateFormID.Value))
                {
                    string[] editDisplayCategory;
                    string sEditDisplayCategoryValue = string.Empty;

                    selectEditTempFilterNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AutoMailMergeSelFormTemplate");
                    cbomailmergetemp.Items.Clear();
                    editAvailableTemplateNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableTemplateList");
                    editAvailableTemplateNodeList = editAvailableTemplateNode.SelectNodes("Template");
                    if (editAvailableTemplateNodeList.Count > 0)
                    {
                        foreach (XmlNode levelNode in editAvailableTemplateNodeList)
                        {
                            availableFilterItem = new ListItem();

                            availableFilterItem.Text = levelNode.Attributes["TemplateDesc"].Value;
                            availableFilterItem.Value = levelNode.Attributes["TemplateId"].Value;

                            cbomailmergetemp.Items.Add(availableFilterItem);
                        }

                    }

                    cbomailmergetemp.SelectedItem.Value = selectEditTempFilterNode.Attributes["selectedid"].Value;
                    cbomailmergetemp.SelectedItem.Text = selectEditTempFilterNode.Attributes["selectedname"].Value;


                    editSelectedFilterNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList");
                    hdneditSelectedTemplateFiltersNode.Value = editSelectedFilterNode.OuterXml;
                    //hdneditSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(editSelectedFilterNode.OuterXml);//Bharani - MITS : 35879

                    lstAvTempFilter.Items.Clear();
                    lstSelTempFilter.Items.Clear();

                    editselectedAvailableLevelNode = editAvailableTemplateNode.SelectSingleNode("Template[@TemplateId ='" + selectEditTempFilterNode.Attributes["selectedid"].Value + "']");

                    sEditDisplayCategoryValue = editselectedAvailableLevelNode.Attributes["DisplayCategory"].Value;

                    editDisplayCategory = sEditDisplayCategoryValue.Split(',');

                    foreach (string sEditDisplayName in editDisplayCategory)
                    {
                        if (sEditDisplayName.Contains("Litigation") || sEditDisplayName.Contains("Litigation Supplemental"))
                        {
                            if (lstAvTempFilter.Items.FindByValue("1") == null)
                            {
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Litigation Types";
                                editavailableFilterItem.Value = "1";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Litigation Statuses ";
                                editavailableFilterItem.Value = "2";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                            }

                        }

                        else if (sEditDisplayName.Contains("Subrogation Supplemental") || sEditDisplayName.Contains("Subrogation Info") || sEditDisplayName.Contains("Subrogation Financial"))
                        {
                            if (lstAvTempFilter.Items.FindByValue("3") == null)
                            {
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Subrogation Types";
                                editavailableFilterItem.Value = "3";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Subrogation Statuses";
                                editavailableFilterItem.Value = "4";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Subrogation Status Descriptions";
                                editavailableFilterItem.Value = "5";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                            }
                        }
                        else if (sEditDisplayName.Contains("Liability Info") || sEditDisplayName.Contains("Liability Damage Type") || sEditDisplayName.Contains("Liability Supplemental"))
                        {
                            if (lstAvTempFilter.Items.FindByValue("6") == null)
                            {
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Liability Types";
                                editavailableFilterItem.Value = "6";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                            }
                        }
                        else if (sEditDisplayName.Contains("Treatment Plan"))
                        {
                            if (lstAvTempFilter.Items.FindByValue("7") == null)
                            {
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Plan Statuses";
                                editavailableFilterItem.Value = "7";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                            }
                        }

                        else if (sEditDisplayName.Contains("Property Information") || sEditDisplayName.Contains("Property Salvage Info") || sEditDisplayName.Contains("Property Info") || sEditDisplayName.Contains("Property Supplemental"))
                        {
                            if (lstAvTempFilter.Items.FindByValue("8") == null)
                            {
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Type(s) of Property";
                                editavailableFilterItem.Value = "8";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                            }
                        }

                        else if (sEditDisplayName.Contains("Claimant") || sEditDisplayName.Contains("Claimant Supplementals"))
                        {
                            if (lstAvTempFilter.Items.FindByValue("9") == null)
                            {
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Claimant Types";
                                editavailableFilterItem.Value = "9";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                            }
                        }

                        else if (sEditDisplayName.Contains("Defendant") || sEditDisplayName.Contains("Defendant Supplementals"))
                        {
                            if (lstAvTempFilter.Items.FindByValue("10") == null)
                            {
                                editavailableFilterItem = new ListItem();
                                editavailableFilterItem.Text = "Defendant Types";
                                editavailableFilterItem.Value = "10";
                                lstAvTempFilter.Items.Add(editavailableFilterItem);
                            }
                        }
                        
                    }

                    editSelectedFilterNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList");
                    editSelectedTemplateNodeList = editSelectedFilterNode.SelectNodes("Template");
                    XmlNode attributNode = null;
                    if (editSelectedTemplateNodeList.Count > 0)
                    {
                        foreach (XmlNode leveleditNode in editSelectedTemplateNodeList)
                        {
                            string sSelectedFilterID = leveleditNode.Attributes["SelectedFilterID"].Value;
                            if (lstAvTempFilter.Items.FindByValue(sSelectedFilterID) != null)
                            {
                                ListItem item = new ListItem();
                                item.Value = lstAvTempFilter.Items.FindByValue(sSelectedFilterID).Value;
                                item.Text = lstAvTempFilter.Items.FindByValue(sSelectedFilterID).Text;
                                lstAvTempFilter.Items.Remove(item);
                                lstSelTempFilter.Items.Add(item);
                                XmlAttribute xautoSelectedFiltername = autoMailMergeDoc.CreateAttribute("SelectedFilterName");
                                xautoSelectedFiltername.Value = item.Text;
                                //leveleditNode.Attributes.Append(attr);
                                attributNode = editSelectedFilterNode.SelectSingleNode("Template[@SelectedFilterID ='" + sSelectedFilterID + "']");
                                attributNode.Attributes.Append(xautoSelectedFiltername);
                                autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList").RemoveChild(leveleditNode);
                                autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList").AppendChild(autoMailMergeDoc.ImportNode(attributNode, true));
                            }
                        }
                    }

                    hdneditSelectedTemplateFiltersNode.Value = editSelectedFilterNode.OuterXml;
                    //hdneditSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(editSelectedFilterNode.OuterXml);//Bharani - MITS : 35879
                    hdneditTemplateFormID.Value = cbomailmergetemp.SelectedItem.Value;

                    if (!string.IsNullOrEmpty(hdnSelTemplateFilterID.Value) && !string.IsNullOrEmpty(hdnSelTemplateFilterName.Value))
                    {
                        availablecategoryIDNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableCategory/AvailableCategoryID");
                        availablecategoryIDNode.InnerText = hdnSelTemplateFilterID.Value;
                        availablecategoryNameNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableCategory/AvailableCategoryName");
                        availablecategoryNameNode.InnerText = hdnSelTemplateFilterName.Value;

                        string[] selFilterid ;
                        string[] selFiltername;
                        selFilterid = hdnSelTemplateFilterID.Value.Split(',');
                        selFiltername = hdnSelTemplateFilterName.Value.Split(',');

                        foreach (string sId in selFilterid)
                        {
                            ListItem Filter = new ListItem();
                            Filter.Value = sId;
                            foreach (string name in selFiltername)
                            {
                                Filter.Text = name;
                            }

                            lstAvTempFilter.Items.Add(Filter);
                        }
                    }


                }

                else
                {
                    cbomailmergetemp.SelectedItem.Value = hdneditTemplateFormID.Value;
                    XmlDocument selectedFiltersDoc = new XmlDocument();

                    selectedFiltersDoc.LoadXml(hdneditSelectedTemplateFiltersNode.Value);
                    //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdneditSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879
                    selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                    autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard").RemoveChild(selectFilter);
                    autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard").AppendChild(autoMailMergeDoc.ImportNode(selectedFiltersNode, true));

                    XmlNode selectedTemplateFiltersNode = null;
                 
                    XmlNodeList storedhdnSelectedTemplateNodeList = null;
                    XmlNode removeTemplateFiltersNode = null;
                    XmlNode selectTempFiltersNode = null;
                    

                    selectedTemplateFiltersNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList");
                    hdnselectedvalue.Value = selectedTemplateFiltersNode.ToString();
                    removeTemplateFiltersNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList");
                    autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard").RemoveChild(removeTemplateFiltersNode);

                    XmlNode objTempList = autoMailMergeDoc.CreateNode(XmlNodeType.Element, "SelectedTemplateList", null);
                    autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard").AppendChild(autoMailMergeDoc.ImportNode(objTempList, true));

                    selectTempFiltersNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedTemplateList");

                    storedhdnSelectedTemplateNodeList = selectedTemplateFiltersNode.SelectNodes("Template");
                 
                    if (storedhdnSelectedTemplateNodeList.Count > 0)
                    {
                        foreach (XmlNode levelhdnNode in storedhdnSelectedTemplateNodeList)
                        {
                            string sFilterID = string.Empty;
                            string sTemplateID = string.Empty;
                            sTemplateID = levelhdnNode.Attributes["TemplateId"].Value;
                            sFilterID = levelhdnNode.Attributes["SelectedFilterID"].Value;

                            if (lstSelTempFilter.Items.FindByValue(sFilterID) != null)
                            {
                                if (sTemplateID == hdneditTemplateFormID.Value)
                                {
                                    selectTempFiltersNode.AppendChild(autoMailMergeDoc.ImportNode(levelhdnNode, false));
                                }
                            }

                        }
                    }

                    lstSelTempFilter.Items.Clear();
                    if (lstAvTempFilter.Items.Count >= 0)
                    {
                        selectedFilterNodeList = selectTempFiltersNode.SelectNodes("Template");
                        if (selectedFilterNodeList.Count > 0)
                        {
                            foreach (XmlNode levelNode in selectedFilterNodeList)
                            {
                                selectedFilterItem = new ListItem();

                                selectedFilterItem.Text = levelNode.Attributes["SelectedFilterName"].Value;
                                selectedFilterItem.Value = levelNode.Attributes["SelectedFilterID"].Value;

                                lstSelTempFilter.Items.Add(selectedFilterItem);
                                lstAvTempFilter.Items.Remove(selectedFilterItem);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(hdnSelTemplateFilterID.Value) && !string.IsNullOrEmpty(hdnSelTemplateFilterName.Value))
                    {
                        lstAvTempFilter.Items.Clear();
                        availablecategoryIDNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableCategory/AvailableCategoryID");
                        availablecategoryIDNode.InnerText = hdnSelTemplateFilterID.Value;
                        availablecategoryNameNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableCategory/AvailableCategoryName");
                        availablecategoryNameNode.InnerText = hdnSelTemplateFilterName.Value;

                        string[] selectedFilterid;
                        string[] selectedFiltername;
                        selectedFilterid = hdnSelTemplateFilterID.Value.Split(',');
                        selectedFiltername = hdnSelTemplateFilterName.Value.Split(',');

                       
                        for (int i = 0; i < selectedFilterid.Count(); i++)
                        {
                            ListItem item = new ListItem();
                            item.Text = selectedFiltername[i];
                            item.Value = selectedFilterid[i];
                            lstAvTempFilter.Items.Add(item);
                        }

                    }
                }
            }

           

        }

        #endregion

        #region WizardStep4

        private void PopulateAutoFilterStep4Controls()
        {
            XmlDocument autoMailMergeDoc = null;

            XmlNode availablecategoryIDNode = null;
            XmlNode availablecategoryNameNode = null;

            XmlNode selTempMergefldNode = null;
            XmlNodeList availableTemplateNodeList = null;
            XmlNodeList selTempMergefldNodeList = null;
            XmlNode selectedFiltersNode = null;
            XmlNodeList selectedFilterNodeList = null;
            ListItem availableFilterItem = null;
            ListItem selectedFilterItem = null;

            string sDefID = string.Empty;
            sDefID =  ViewState["defid"].ToString();

            string sAutoMailMergeDoc = string.Empty;
            sAutoMailMergeDoc = ViewState["autoMailMergeTemplate"].ToString();
            autoMailMergeDoc = new XmlDocument();
            autoMailMergeDoc.LoadXml(sAutoMailMergeDoc);

            string sCommaSeparateID = string.Empty;
            string sCommaSeparateName = string.Empty;
            foreach (ListItem item in lstAvTempFilter.Items)
            {
                if (item != null)
                {
                    if (sCommaSeparateID == "" && sCommaSeparateName == "")
                    {
                        sCommaSeparateID = item.Value;
                        sCommaSeparateName = item.Text;

                    }
                    else
                    {
                        sCommaSeparateID = sCommaSeparateID + "," + item.Value;
                        sCommaSeparateName = sCommaSeparateName + "," + item.Text;
                    }
                }
            }

            hdnSelTemplateFilterID.Value = sCommaSeparateID;
            hdnSelTemplateFilterName.Value = sCommaSeparateName;

            availablecategoryIDNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableCategory/AvailableCategoryID");
            availablecategoryIDNode.InnerText = hdnSelTemplateFilterID.Value;
            availablecategoryNameNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableCategory/AvailableCategoryName");
            availablecategoryNameNode.InnerText = hdnSelTemplateFilterName.Value;

            ViewState["autoMailMergeTemplate"] = autoMailMergeDoc.OuterXml;

            selTempMergefldNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableTemplateList");

            selTempMergefldNodeList = selTempMergefldNode.SelectNodes("Template");

            string[] displayCategory;

            Button btnClaimant = (Button)this.FindControl("CtrlAutoMailMergeWizard$lstClaimantType$multicodebtn");
            Button btnClaimantRemove = (Button)this.FindControl("CtrlAutoMailMergeWizard$lstClaimantType$multicodebtndel");
            Button btnDefendant = (Button)this.FindControl("CtrlAutoMailMergeWizard$lstDefendantType$multicodebtn");
            Button btnDefendantRemove = (Button)this.FindControl("CtrlAutoMailMergeWizard$lstDefendantType$multicodebtndel");
            Button btnExpert = (Button)this.FindControl("CtrlAutoMailMergeWizard$lstExpertType$multicodebtn");
            Button btnExpertRemove = (Button)this.FindControl("CtrlAutoMailMergeWizard$lstExpertType$multicodebtndel");

            btnPersonCodeLookup.Enabled = false;
            btnPersonRemove.Enabled = false;

            btnClaimant.Enabled = false;
            btnClaimantRemove.Enabled = false;

            btnDefendant.Enabled = false;
            btnDefendantRemove.Enabled = false;

            btnExpert.Enabled = false;
            btnExpertRemove.Enabled = false;

            MultiCodeLookUp MuserControl = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstClaimantType");
            ListBox lstUserControl = (ListBox)MuserControl.FindControl("multicode");
            lstUserControl.Items.Clear();

            MultiCodeLookUp MuserControlDefendant = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstDefendantType");
            ListBox lstUserControlDefendant = (ListBox)MuserControlDefendant.FindControl("multicode");
            lstUserControlDefendant.Items.Clear();

            MultiCodeLookUp MuserControlExpert = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstExpertType");
            ListBox lstUserControlExpert = (ListBox)MuserControlExpert.FindControl("multicode");
            lstUserControlExpert.Items.Clear();

            ListBox lstPersonInvolued = (ListBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued");
            lstPersonInvolued.Items.Clear();

            if (sDefID == string.Empty)
            {
                if (selTempMergefldNodeList.Count > 0)
                {
                    string sDisplayCategoryValue = string.Empty;
                    foreach (XmlNode levelNode in selTempMergefldNodeList)
                    {
                        string sTemplateID = string.Empty;
                        sTemplateID = levelNode.Attributes["TemplateId"].Value;
                        if (sTemplateID == hdnTemplateFormID.Value)
                        {
                            sDisplayCategoryValue = levelNode.Attributes["DisplayCategory"].Value;
                            displayCategory = sDisplayCategoryValue.Split(',');
                            foreach (string DisplayName in displayCategory)
                            {
                                if (DisplayName.Contains("Person Involved"))
                                {
                                    lstPersonInvolued.Enabled = true;
                                    btnPersonCodeLookup.Enabled = true;
                                    btnPersonRemove.Enabled = true;


                                    if (!string.IsNullOrEmpty(hdnPersonInvolvedValue.Value) && btnPersonCodeLookup.Enabled == true)
                                    {
                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetPeopleInvolvedList();
                                        XmlNode selPeopleInvolvedNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedPersonInvoluedList") != null)
                                        {
                                            selPeopleInvolvedNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedPersonInvoluedList");
                                            XmlNodeList selPeopleInvolvedList = null;
                                            if (selPeopleInvolvedNode.SelectNodes("PersonList") != null)
                                            {
                                                selPeopleInvolvedList = selPeopleInvolvedNode.SelectNodes("PersonList");
                                                if (selPeopleInvolvedList.Count > 0)
                                                {
                                                    lstPersonInvolued.Items.Clear();

                                                    foreach (XmlNode PeopleInvolvedNode in selPeopleInvolvedList)
                                                    {
                                                        ListItem item = new ListItem();
                                                        item.Text = PeopleInvolvedNode.Attributes["Name"].Value;
                                                        item.Value = PeopleInvolvedNode.Attributes["ID"].Value;
                                                        lstPersonInvolued.Items.Add(item);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (DisplayName.Contains("Claimant") || DisplayName.Contains("Claimant Supplemental"))
                                {
                                    lstClaimantType.Enabled = true;
                                    btnClaimant.Enabled = true;
                                    btnClaimantRemove.Enabled = true;

                                    if (!string.IsNullOrEmpty(hdnClaimantvalues.Value) && btnClaimant.Enabled == true)
                                    {
                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetAutoMailMergeFilterEntityRoles();
                                        XmlNode selNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ClaimantList") != null)
                                        {
                                            selNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ClaimantList");
                                            XmlNodeList selNodeList = null;
                                            if (selNode.SelectNodes("Option") != null)
                                            {
                                                selNodeList = selNode.SelectNodes("Option");

                                                if (selNodeList.Count > 0)
                                                {
                                                    string sID = string.Empty;
                                                    string sShortCode = string.Empty;
                                                    string sDescription = string.Empty;
                                                    string sClaimantTypeDescription = string.Empty;
                                                    lstUserControl.Items.Clear();
                                                    foreach (XmlNode ClaimantTypeNode in selNodeList)
                                                    {
                                                        ListItem item = new ListItem();
                                                        sID = ClaimantTypeNode.Attributes["ID"].Value;
                                                        sShortCode = ClaimantTypeNode.Attributes["ShortCode"].Value;
                                                        sDescription = ClaimantTypeNode.Attributes["Description"].Value;

                                                        sClaimantTypeDescription = sShortCode + " " + sDescription;
                                                        item.Text = sClaimantTypeDescription;
                                                        item.Value = sID;



                                                        lstUserControl.Items.Add(item);
                                                    }
                                                }
                                            }
                                        }


                                    }
                                }
                                else if (DisplayName.Contains("Defendant") || DisplayName.Contains("Defendant Supplemental"))
                                {
                                    lstDefendantType.Enabled = true;

                                    btnDefendant.Enabled = true;

                                    btnDefendantRemove.Enabled = true;

                                    if (!string.IsNullOrEmpty(hdnDefendantvalues.Value) && btnDefendant.Enabled == true)
                                    {

                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetAutoMailMergeFilterEntityRoles();
                                        XmlNode selDefendantNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/DefendantList") != null)
                                        {
                                            selDefendantNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/DefendantList");

                                            XmlNodeList selNodeDefendantList = null;
                                            if (selDefendantNode.SelectNodes("Option") != null)
                                            {
                                                selNodeDefendantList = selDefendantNode.SelectNodes("Option");
                                                if (selNodeDefendantList.Count > 0)
                                                {
                                                    string sID = string.Empty;
                                                    string sShortCode = string.Empty;
                                                    string sDescription = string.Empty;
                                                    string sDefTypeDescription = string.Empty;
                                                    lstUserControlDefendant.Items.Clear();

                                                    foreach (XmlNode DefendantTypeNode in selNodeDefendantList)
                                                    {
                                                        ListItem item = new ListItem();

                                                        sID = DefendantTypeNode.Attributes["ID"].Value;
                                                        sShortCode = DefendantTypeNode.Attributes["ShortCode"].Value;
                                                        sDescription = DefendantTypeNode.Attributes["Description"].Value;

                                                        sDefTypeDescription = sShortCode + " " + sDescription;
                                                        item.Text = sDefTypeDescription;
                                                        item.Value = sID;

                                                        lstUserControlDefendant.Items.Add(item);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                                else if (DisplayName.Contains("Expert"))
                                {
                                    lstExpertType.Enabled = true;
                                    btnExpert.Enabled = true;
                                    btnExpertRemove.Enabled = true;

                                    if (!string.IsNullOrEmpty(hdnExpertvalues.Value) && btnExpert.Enabled == true)
                                    {

                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetAutoMailMergeFilterEntityRoles();
                                        XmlNode selExpertNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ExpertWitnessList") != null)
                                        {
                                            selExpertNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ExpertWitnessList");

                                            XmlNodeList selNodeExpertList = null;
                                            if (selExpertNode.SelectNodes("Option") != null)
                                            {
                                                selNodeExpertList = selExpertNode.SelectNodes("Option");

                                                if (selNodeExpertList.Count > 0)
                                                {
                                                    string sID = string.Empty;
                                                    string sShortCode = string.Empty;
                                                    string sDescription = string.Empty;
                                                    string sExpertTypeDescription = string.Empty;
                                                    lstUserControlExpert.Items.Clear();

                                                    foreach (XmlNode ExpertTypeNode in selNodeExpertList)
                                                    {
                                                        ListItem item = new ListItem();

                                                        sID = ExpertTypeNode.Attributes["ID"].Value;
                                                        sShortCode = ExpertTypeNode.Attributes["ShortCode"].Value;
                                                        sDescription = ExpertTypeNode.Attributes["Description"].Value;

                                                        sExpertTypeDescription = sShortCode + " " + sDescription;
                                                        item.Text = sExpertTypeDescription;
                                                        item.Value = sID;

                                                        lstUserControlExpert.Items.Add(item);
                                                    }
                                                }
                                            }
                                        }




                                    }

                                }

                                else if (DisplayName.Contains("Adjuster") || DisplayName.Contains("Adjuster Text"))
                                {
                                    XmlNode selAdjusterNode = null;
                                    selAdjusterNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AdjusterValue");
                                    selAdjusterNode.InnerText = "1";
                                    hdnAdjustervalue.Value = selAdjusterNode.InnerText;
                                }

                                else if (DisplayName.Contains("Case Manager") || DisplayName.Contains("Case Manager Notes"))
                                {
                                    XmlNode selcasemanagerNode = null;
                                    selcasemanagerNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/CaseManagerValue");
                                    selcasemanagerNode.InnerText = "1";
                                    hdnCasemanagervalue.Value = selcasemanagerNode.InnerText;
                                }


                            }




                        }
                    }

                    if (cboprinter.SelectedValue != null)
                    {
                        hdnprinter.Value = cboprinter.SelectedValue;
                    }

                    if (cboPaperBin.SelectedValue != null)
                    {
                        hdnpaper.Value = cboPaperBin.SelectedValue;
                    }
                }
            }
            else
            {
                if (selTempMergefldNodeList.Count > 0)
                {
                    string sDisplayCategoryValue = string.Empty;
                    foreach (XmlNode levelNode in selTempMergefldNodeList)
                    {
                        string sTemplateID = string.Empty;
                        sTemplateID = levelNode.Attributes["TemplateId"].Value;
                        if (sTemplateID == hdneditTemplateFormID.Value)
                        {
                            sDisplayCategoryValue = levelNode.Attributes["DisplayCategory"].Value;
                            displayCategory = sDisplayCategoryValue.Split(',');
                            foreach (string sDisplayName in displayCategory)
                            {
                                if (sDisplayName.Contains("Person Involved"))
                                {
                                    lstPersonInvolued.Enabled = true;
                                    btnPersonCodeLookup.Enabled = true;
                                    btnPersonRemove.Enabled = true;

                                    if (hdnPersonInvolvedValue.Value == string.Empty)
                                    {
                                        XmlNode peopleInvolvedNode = null;
                                        string sAutoMailMerge = string.Empty;
                                        XmlDocument autoMailMerge = null;
                                        sAutoMailMerge = ViewState["autoMailMergeTemplate"].ToString();
                                        autoMailMerge = new XmlDocument();
                                        autoMailMerge.LoadXml(sAutoMailMerge);
                                        peopleInvolvedNode = autoMailMerge.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/PersonInvolvedValues");
                                        if (!string.IsNullOrEmpty(peopleInvolvedNode.InnerText))
                                        {
                                            hdnPersonInvolvedValue.Value = peopleInvolvedNode.InnerText;
                                            hdnPersonInvolvedValue.Value = hdnPersonInvolvedValue.Value.Substring(0, hdnPersonInvolvedValue.Value.Length - 1);
                                        }
                                        else
                                        {
                                            hdnPersonInvolvedValue.Value = peopleInvolvedNode.InnerText;
                                        }
                                    }

                                    if (btnPersonCodeLookup.Enabled == true)
                                    {
                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetPeopleInvolvedList();
                                        XmlNode selPeopleInvolvedNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedPersonInvoluedList") != null)
                                        {
                                            selPeopleInvolvedNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/SelectedPersonInvoluedList");

                                            XmlNodeList selPeopleInvolvedList = null;
                                            if (selPeopleInvolvedNode.SelectNodes("PersonList") != null)
                                            {
                                                selPeopleInvolvedList = selPeopleInvolvedNode.SelectNodes("PersonList");

                                                ListBox lstPersoninvolve = (ListBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued");
                                                TextBox txtPersoninvolve = (TextBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued_lst");
                                                txtPersoninvolve.Text = string.Empty;

                                                if (selPeopleInvolvedList.Count > 0)
                                                {
                                                    lstPersonInvolued.Items.Clear();

                                                    foreach (XmlNode peopleInvolvedNode in selPeopleInvolvedList)
                                                    {
                                                        ListItem item = new ListItem();
                                                        item.Text = peopleInvolvedNode.Attributes["Name"].Value;
                                                        item.Value = peopleInvolvedNode.Attributes["ID"].Value;

                                                        if (string.IsNullOrEmpty(txtPersoninvolve.Text))
                                                        {
                                                            txtPersoninvolve.Text = item.Value;
                                                        }
                                                        else
                                                        {
                                                            txtPersoninvolve.Text = txtPersoninvolve.Text + " " + item.Value;
                                                        }

                                                        lstPersonInvolued.Items.Add(item);
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }

                                else if (sDisplayName.Contains("Claimant") || sDisplayName.Contains("Claimant Supplemental"))
                                {
                                    lstClaimantType.Enabled = true;
                                    btnClaimant.Enabled = true;
                                    btnClaimantRemove.Enabled = true;

                                    if (hdnClaimantvalues.Value == string.Empty)
                                    {
                                        XmlNode claimantInvolvedNode = null;
                                        string sAutoMailMerge = string.Empty;
                                        XmlDocument autoMailMerge = null;
                                        sAutoMailMerge = ViewState["autoMailMergeTemplate"].ToString();
                                        autoMailMerge = new XmlDocument();
                                        autoMailMerge.LoadXml(sAutoMailMerge);
                                        claimantInvolvedNode = autoMailMerge.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ClaimantValues");
                                        if (!string.IsNullOrEmpty(claimantInvolvedNode.InnerText))
                                        {
                                            hdnClaimantvalues.Value = claimantInvolvedNode.InnerText;
                                            hdnClaimantvalues.Value = hdnClaimantvalues.Value.Substring(0, hdnClaimantvalues.Value.Length - 1);
                                        }
                                        else
                                        {
                                            hdnClaimantvalues.Value = claimantInvolvedNode.InnerText;
                                        }
                                    }
                                    if (btnClaimant.Enabled == true)
                                    {
                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetAutoMailMergeFilterEntityRoles();
                                        XmlNode selNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ClaimantList") != null)
                                        {
                                            selNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ClaimantList");

                                            XmlNodeList selNodeList = null;
                                            if (selNode.SelectNodes("Option") != null)
                                            {
                                                selNodeList = selNode.SelectNodes("Option");

                                                if (selNodeList.Count > 0)
                                                {
                                                    string sID = string.Empty;
                                                    string sShortCode = string.Empty;
                                                    string sDescription = string.Empty;
                                                    string sClaimantTypeDescription = string.Empty;
                                                    lstUserControl.Items.Clear();
                                                    MultiCodeLookUp MuserControlClaimant = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstClaimantType");
                                                    TextBox txtUserControlClaimant = (TextBox)MuserControlClaimant.FindControl("multicode_lst");
                                                    txtUserControlClaimant.Text = string.Empty;

                                                    foreach (XmlNode claimantTypeNode in selNodeList)
                                                    {
                                                        ListItem item = new ListItem();
                                                        sID = claimantTypeNode.Attributes["ID"].Value;
                                                        sShortCode = claimantTypeNode.Attributes["ShortCode"].Value;
                                                        sDescription = claimantTypeNode.Attributes["Description"].Value;

                                                        sClaimantTypeDescription = sShortCode + " " + sDescription;
                                                        item.Text = sClaimantTypeDescription;
                                                        item.Value = sID;

                                                        if (string.IsNullOrEmpty(txtUserControlClaimant.Text))
                                                        {
                                                            txtUserControlClaimant.Text = sID;
                                                        }
                                                        else
                                                        {
                                                            txtUserControlClaimant.Text = txtUserControlClaimant.Text + " " + sID;
                                                        }

                                                        lstUserControl.Items.Add(item);
                                                    }
                                                }
                                            }
                                        }




                                    }
                                }

                                else if (sDisplayName.Contains("Defendant") || sDisplayName.Contains("Defendant Supplemental"))
                                {
                                    lstDefendantType.Enabled = true;
                                    btnDefendant.Enabled = true;
                                    btnDefendantRemove.Enabled = true;

                                    if (hdnDefendantvalues.Value == string.Empty)
                                    {
                                        XmlNode defendantInvolvedNode = null;
                                        string sAutoMailMerge = string.Empty;
                                        XmlDocument autoMailMerge = null;
                                        sAutoMailMerge = ViewState["autoMailMergeTemplate"].ToString();
                                        autoMailMerge = new XmlDocument();
                                        autoMailMerge.LoadXml(sAutoMailMerge);
                                        defendantInvolvedNode = autoMailMerge.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/DefendantValues");
                                        if (!string.IsNullOrEmpty(defendantInvolvedNode.InnerText))
                                        {
                                            hdnDefendantvalues.Value = defendantInvolvedNode.InnerText;
                                            hdnDefendantvalues.Value = hdnDefendantvalues.Value.Substring(0, hdnDefendantvalues.Value.Length - 1);
                                        }
                                        else
                                        {
                                            hdnDefendantvalues.Value = defendantInvolvedNode.InnerText;
                                        }
                                    }

                                    if (btnDefendant.Enabled == true)
                                    {
                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetAutoMailMergeFilterEntityRoles();
                                        XmlNode selDefendantNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/DefendantList") != null)
                                        {
                                            selDefendantNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/DefendantList");

                                            XmlNodeList selNodeDefendantList = null;
                                            if (selDefendantNode.SelectNodes("Option") != null)
                                            {
                                                selNodeDefendantList = selDefendantNode.SelectNodes("Option");

                                                if (selNodeDefendantList.Count > 0)
                                                {
                                                    string sID = string.Empty;
                                                    string sShortCode = string.Empty;
                                                    string sDescription = string.Empty;
                                                    string sDefTypeDescription = string.Empty;
                                                    lstUserControlDefendant.Items.Clear();

                                                    MultiCodeLookUp MuserControlDefen = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstDefendantType");
                                                    TextBox txtUserControlDefendant = (TextBox)MuserControlDefen.FindControl("multicode_lst");
                                                    txtUserControlDefendant.Text = string.Empty;

                                                    foreach (XmlNode DefendantTypeNode in selNodeDefendantList)
                                                    {
                                                        ListItem item = new ListItem();
                                                        sID = DefendantTypeNode.Attributes["ID"].Value;
                                                        sShortCode = DefendantTypeNode.Attributes["ShortCode"].Value;
                                                        sDescription = DefendantTypeNode.Attributes["Description"].Value;
                                                        sDefTypeDescription = sShortCode + " " + sDescription;
                                                        item.Text = sDefTypeDescription;
                                                        item.Value = sID;

                                                        if (string.IsNullOrEmpty(txtUserControlDefendant.Text))
                                                        {
                                                            txtUserControlDefendant.Text = sID;
                                                        }
                                                        else
                                                        {
                                                            txtUserControlDefendant.Text = txtUserControlDefendant.Text + " " + sID;
                                                        }

                                                        lstUserControlDefendant.Items.Add(item);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                else if (sDisplayName.Contains("Expert"))
                                {
                                    lstExpertType.Enabled = true;
                                    btnExpert.Enabled = true;
                                    btnExpertRemove.Enabled = true;

                                    if (hdnExpertvalues.Value == string.Empty) 
                                    {
                                        XmlNode expertInvolvedNode = null;
                                        string sAutoMailMerge = string.Empty;
                                        XmlDocument autoMailMerge = null;
                                        sAutoMailMerge = ViewState["autoMailMergeTemplate"].ToString();
                                        autoMailMerge = new XmlDocument();
                                        autoMailMerge.LoadXml(sAutoMailMerge);
                                        expertInvolvedNode = autoMailMerge.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ExpertWitnessValues");

                                        if (!string.IsNullOrEmpty(expertInvolvedNode.InnerText))
                                        {
                                            hdnExpertvalues.Value = expertInvolvedNode.InnerText;
                                            hdnExpertvalues.Value = hdnExpertvalues.Value.Substring(0, hdnExpertvalues.Value.Length - 1);
                                        }
                                        else
                                        {
                                            hdnExpertvalues.Value = expertInvolvedNode.InnerText;
                                        }
                                    }

                                    if (btnExpert.Enabled == true)
                                    {
                                        autoMailMergeDoc = new XmlDocument();
                                        autoMailMergeDoc = GetAutoMailMergeFilterEntityRoles();
                                        XmlNode selExpertNode = null;
                                        if (autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ExpertWitnessList") != null)
                                        {
                                            selExpertNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ExpertWitnessList");

                                            XmlNodeList selNodeExpertList = null;
                                            if (selExpertNode.SelectNodes("Option") != null)
                                            {
                                                selNodeExpertList = selExpertNode.SelectNodes("Option");

                                                if (selNodeExpertList.Count > 0)
                                                {
                                                    string sID = string.Empty;
                                                    string sShortCode = string.Empty;
                                                    string sDescription = string.Empty;
                                                    string sExpertTypeDescription = string.Empty;
                                                    lstUserControlExpert.Items.Clear();

                                                    MultiCodeLookUp MuserControlExp = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstExpertType");
                                                    TextBox txtUserControlExp = (TextBox)MuserControlExp.FindControl("multicode_lst");
                                                    txtUserControlExp.Text = string.Empty;

                                                    foreach (XmlNode expertTypeNode in selNodeExpertList)
                                                    {
                                                        ListItem item = new ListItem();

                                                        sID = expertTypeNode.Attributes["ID"].Value;
                                                        sShortCode = expertTypeNode.Attributes["ShortCode"].Value;
                                                        sDescription = expertTypeNode.Attributes["Description"].Value;

                                                        sExpertTypeDescription = sShortCode + " " + sDescription;
                                                        item.Text = sExpertTypeDescription;
                                                        item.Value = sID;

                                                        if (string.IsNullOrEmpty(txtUserControlExp.Text))
                                                        {
                                                            txtUserControlExp.Text = sID;
                                                        }
                                                        else
                                                        {
                                                            txtUserControlExp.Text = txtUserControlExp.Text + " " + sID;
                                                        }

                                                        lstUserControlExpert.Items.Add(item);
                                                    }
                                                }
                                            }
                                        }

                                    }

                                }


                                else if (sDisplayName.Contains("Adjuster") || sDisplayName.Contains("Adjuster Text"))
                                {
                                    XmlNode selAdjusterNode = null;
                                    selAdjusterNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AdjusterValue");
                                    selAdjusterNode.InnerText = "1";
                                    hdnAdjustervalue.Value = selAdjusterNode.InnerText;
                                }

                                else if (sDisplayName.Contains("Case Manager") || sDisplayName.Contains("Case Manager Notes"))
                                {
                                    XmlNode selCasemanagerNode = null;
                                    selCasemanagerNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/CaseManagerValue");
                                    selCasemanagerNode.InnerText = "1";
                                    hdnCasemanagervalue.Value = selCasemanagerNode.InnerText;
                                }
                            }
                        }
                    }
                }
            }

            
    }

        #endregion

        #region WizardStep5

        private void PopulateAutoFilterStep5Controls()
        {
            string sDefID = string.Empty;
            sDefID =  ViewState["defid"].ToString();

            XmlDocument autoMailMergeDoc = null;

            XmlNode defendantCodeIDNode = null;
            XmlNode expertCodeIDNode = null;
            XmlNode claimantCodeIDNode = null;
            XmlNode personInvolvedIDNode = null;
            XmlNode printerNameNode = null;
            XmlNode paperBinNode = null;
          

            string sAutoMailMergeDoc = string.Empty;
            sAutoMailMergeDoc = ViewState["autoMailMergeTemplate"].ToString();
            autoMailMergeDoc = new XmlDocument();
            autoMailMergeDoc.LoadXml(sAutoMailMergeDoc);

            if (string.IsNullOrEmpty(sDefID))
            {
                MultiCodeLookUp MuserControlClaimantType = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstClaimantType");
                string sClaimanttypeCode = MuserControlClaimantType.CodeId;
                hdnClaimantvalues.Value = sClaimanttypeCode;

                MultiCodeLookUp MuserControlExpertType = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstExpertType");
                string sExperttypeCode = MuserControlExpertType.CodeId;
                hdnExpertvalues.Value = sExperttypeCode;

                MultiCodeLookUp MuserControlDefendantType = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstDefendantType");
                string sDefendanttypeCode = MuserControlDefendantType.CodeId;
                hdnDefendantvalues.Value = sDefendanttypeCode;

                string sPersonInvolvedTemp = string.Empty;
                string sTmp = string.Empty;
                string sPersonInvolvedCodes = string.Empty;
                string sPersonInvolvedvalue = string.Empty;
                TextBox txtPersonInvolved = (TextBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued_lst");
                sPersonInvolvedTemp = txtPersonInvolved.Text;

                if (sPersonInvolvedTemp != string.Empty)
                {
                    sTmp = sPersonInvolvedTemp.Trim();
                    sPersonInvolvedCodes = sTmp.Replace(" ", ",");
                    sPersonInvolvedCodes = sPersonInvolvedCodes + ",";
                    sPersonInvolvedvalue = sPersonInvolvedCodes.Substring(0, sPersonInvolvedCodes.Length - 1);
                }

                hdnPersonInvolvedValue.Value = sPersonInvolvedvalue;

                defendantCodeIDNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/DefendantValues");
                defendantCodeIDNode.InnerText = hdnDefendantvalues.Value;
                expertCodeIDNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ExpertWitnessValues");
                expertCodeIDNode.InnerText = hdnExpertvalues.Value;
                claimantCodeIDNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/ClaimantValues");
                claimantCodeIDNode.InnerText = hdnClaimantvalues.Value;
                personInvolvedIDNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/PersonInvolvedValues");
                personInvolvedIDNode.InnerText = hdnPersonInvolvedValue.Value;



                PrintDocument oPrintDocument = new PrintDocument();

                if (!string.IsNullOrEmpty(hdnprinter.Value))
                {
                    cboprinter.SelectedValue = hdnprinter.Value;
                    if (!string.IsNullOrEmpty(hdnpaper.Value))
                    {
                        cboPaperBin.SelectedValue = hdnpaper.Value;
                    }

                    printerNameNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/PrinterName");
                    printerNameNode.InnerText = hdnprinter.Value;
                    paperBinNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/PaperBin");
                    paperBinNode.InnerText = hdnpaper.Value;
                }
                else
                {
                    cboprinter.Items.Clear();
                    cboprinter.Items.Add("");
                    //tkatsarski: 02/17/15 - RMA-6802: When the server's printing service is stopped ("print spooler"), PrinterSettings.InstalledPrinters is throwing exception
                    try
                    {
                        foreach (string printer in PrinterSettings.InstalledPrinters)
                        {

                            ListItem item = new ListItem(printer);
                            cboprinter.Items.Add(printer);
                        }
                    }
                    catch (System.ComponentModel.Win32Exception ex)
                    {
                        ErrorHelper.logErrors(ex);
                    }

                    cboPaperBin.Items.Clear();
                    cboPaperBin.Items.Add("");
                    foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                    {
                        ListItem lstitem = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                        cboPaperBin.Items.Add(lstitem);

                    }
                }
            }

            else
            {
                MultiCodeLookUp MuserControl = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstClaimantType");
                TextBox txtUserControlClaimant = (TextBox)MuserControl.FindControl("multicode_lst");

                
                string sClaimantTypeCode = string.Empty;
                sClaimantTypeCode = MuserControl.CodeId.Replace(" ", ",");
                hdnClaimantvalues.Value = sClaimantTypeCode;
                
                MultiCodeLookUp MuserControlDefendant = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstDefendantType");
                TextBox txtUserControlDefendant = (TextBox)MuserControlDefendant.FindControl("multicode_lst");


                string sDefTypeCode = MuserControlDefendant.CodeId.Replace(" ", ",");
                hdnDefendantvalues.Value = sDefTypeCode;

                MultiCodeLookUp MuserControlExpert = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstExpertType");
                TextBox txtUserControlExpert = (TextBox)MuserControlDefendant.FindControl("multicode_lst");

                string sExpertTypeCode = MuserControlExpert.CodeId.Replace(" ", ",");
                hdnExpertvalues.Value = sExpertTypeCode;
                
                ListBox lstPersonInvolued = (ListBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued");
                TextBox txtPersonInvolved = (TextBox)CtrlAutoMailMergeWizard.FindControl("lstPersonInvolued_lst");

                string sPersonTypeCode = txtPersonInvolved.Text.Replace(" ", ",");
                hdnPersonInvolvedValue.Value = sPersonTypeCode;
                
                string sAutoMailMerge = string.Empty;
                XmlDocument autoMailMerge = null;
                
                sAutoMailMerge = ViewState["autoMailMergeTemplate"].ToString();
                autoMailMerge = new XmlDocument();
                autoMailMerge.LoadXml(sAutoMailMerge);
                printerNameNode = autoMailMerge.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/PrinterName");
                paperBinNode = autoMailMerge.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/PaperBin");

                PrintDocument oPrintDocument = new PrintDocument();
                cboprinter.Items.Clear();
                cboprinter.Items.Add("");
                //tkatsarski: 02/17/15 - RMA-6802: When the server's printing service is stopped ("print spooler"), PrinterSettings.InstalledPrinters is throwing exception
                try
                {
                    foreach (string sprinter in PrinterSettings.InstalledPrinters)
                    {

                        ListItem item = new ListItem(sprinter);
                        cboprinter.Items.Add(sprinter);
                    }
                }
                catch (System.ComponentModel.Win32Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                }

                if (printerNameNode.InnerText != "")
                {
                    ListItem item = new ListItem(printerNameNode.InnerText);
                    if (cboprinter.Items.Contains(item))
                    {
                        cboprinter.SelectedValue = printerNameNode.InnerText;
                        oPrintDocument.PrinterSettings.PrinterName = cboprinter.SelectedValue;
                    }
                    else
                    {
                        cboprinter.SelectedValue = "";
                        oPrintDocument.PrinterSettings.PrinterName = cboprinter.SelectedValue;
                    }

                }
                else
                {
                    cboprinter.SelectedValue = printerNameNode.InnerText;
                    oPrintDocument.PrinterSettings.PrinterName = cboprinter.SelectedValue;

                }

                cboPaperBin.Items.Clear();
                foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                {
                    ListItem item = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                    cboPaperBin.Items.Add(item);
                }

                if (paperBinNode.InnerText != "0")
                {
                    if (cboPaperBin.Items.FindByValue(paperBinNode.InnerText.ToString()) != null)
                    {
                        cboPaperBin.SelectedValue = paperBinNode.InnerText;
                    }
                    else
                    {
                        cboPaperBin.SelectedValue = "";
                    }
                }
                else
                {
                    cboPaperBin.SelectedValue = paperBinNode.InnerText;
                }

                
            }

           


        }

        #endregion

        protected void UpatePaneForAutoFilterStep2_Load(object sender, EventArgs e)
        {
            string sclientArgument = string.Empty;
            XmlDocument selectedFiltersDoc = null;
            XmlDocument selectedLevelDoc = null;
            XmlNode selectedLevelNode = null;
            XmlNode selectedFiltersNode = null;
            string sDefID = string.Empty;
            sDefID =  ViewState["defid"].ToString();

            try
            {
                sclientArgument = Request.Params.Get("__EVENTARGUMENT");

                if (!string.IsNullOrEmpty(sclientArgument))
                {
                    if (!string.IsNullOrEmpty(hdnAutoFilterStep2.Value))
                    {
                        if (!sclientArgument.Contains("FiltersDoubleClickedMode"))
                        {
                            hdnData.Value = sclientArgument;
                            selectedLevelDoc = new XmlDocument();
                            selectedLevelDoc.LoadXml(hdnSelectedLevelNode.Value);
                            //selectedLevelDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedLevelNode.Value));//Bharani - MITS : 35879

                            selectedLevelNode = selectedLevelDoc.SelectSingleNode("level");

                            if (selectedLevelNode.Attributes["data"] != null)
                            {
                                selectedLevelNode.Attributes["data"].Value = hdnData.Value;
                            }
                            else
                            {
                                XmlAttribute newDataAttribute = null;
                                newDataAttribute = selectedLevelDoc.CreateAttribute("data");
                                newDataAttribute.Value = hdnData.Value;


                                selectedLevelNode.Attributes.Append(newDataAttribute);
                            }

                            hdnSelectedLevelNode.Value = selectedLevelNode.OuterXml;
                            //hdnSelectedLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35879

                            selectedFiltersDoc = new XmlDocument();
                            selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                            //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35879

                            selectedLevelNode = null;

                            selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                            selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + hdnId.Value + "']");

                            if (selectedLevelNode == null)
                            {
                                selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + hdnSelectedLevelNode.Value;
                                hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                                //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                                //adding this item in SelectedFilters listbox
                                SelectedFilters.Items.Add(new ListItem(hdnFilterName.Value, hdnId.Value));

                                //removig this item from AvaibleFilters listbox
                                AvailableFilters.Items.Remove(new ListItem(hdnFilterName.Value, hdnId.Value));
                            }
                            else
                            {
                                selectedLevelNode.Attributes["data"].Value = hdnData.Value;
                                hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                                //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879
                            }

                            hdnId.Value = "";
                            hdnFilterName.Value = "";
                            hdnSqlFill.Value = "";
                            hdnFilterType.Value = "";
                            hdnData.Value = "";
                            hdnMode.Value = "";
                            hdnDefValue.Value = "";
                            hdnSelectedLevelNode.Value = "";
                        }
                        else
                        {
                            string[] arrArg;
                            arrArg = sclientArgument.Split('=');

                            UpdateControlsOnAddEditAutoFilter(arrArg[1]);
                        }
                    }

                    if (!string.IsNullOrEmpty(hdnAutoFilterStep3.Value))
                    {
                        if (!sclientArgument.Contains("FiltersDoubleClickedMode"))
                        {
                            if (sDefID == string.Empty)
                            {
                                hdnTemplateData.Value = sclientArgument;
                                selectedLevelDoc = new XmlDocument();
                                selectedLevelDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedTemplateLevelNode.Value));

                                selectedLevelNode = selectedLevelDoc.SelectSingleNode("Template");

                                if (selectedLevelNode.Attributes["data"] != null)
                                {
                                    selectedLevelNode.Attributes["data"].Value = hdnTemplateData.Value;
                                }
                                else
                                {
                                    XmlAttribute newSelectedFilterIDAttribute = null;
                                    newSelectedFilterIDAttribute = selectedLevelDoc.CreateAttribute("SelectedFilterID");
                                    newSelectedFilterIDAttribute.Value = hdnSelAdditionalFilterId.Value;

                                    XmlAttribute newSelectedFilterNameAttribute = null;
                                    newSelectedFilterNameAttribute = selectedLevelDoc.CreateAttribute("SelectedFilterName");
                                    newSelectedFilterNameAttribute.Value = hdnSelAdditionalFilterName.Value;

                                    XmlAttribute newDataAttribute = null;
                                    newDataAttribute = selectedLevelDoc.CreateAttribute("data");
                                    newDataAttribute.Value = hdnTemplateData.Value;

                                    selectedLevelNode.Attributes.Append(newSelectedFilterIDAttribute);
                                    selectedLevelNode.Attributes.Append(newSelectedFilterNameAttribute);
                                    selectedLevelNode.Attributes.Append(newDataAttribute);
                                }

                                hdnSelectedTemplateLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);

                                selectedFiltersDoc = new XmlDocument();
                                selectedFiltersDoc.LoadXml(hdnSelectedTemplateFiltersNode.Value);
                                //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879

                                selectedLevelNode = null;

                                selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                                selectedLevelNode = selectedFiltersNode.SelectSingleNode("Template[@TemplateId ='" + hdnTemplateFormID.Value + "' and @SelectedFilterID = '" + hdnSelAdditionalFilterId.Value + "']");

                                if (selectedLevelNode == null)
                                {
                                    selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + AppHelper.HTMLCustomDecode(hdnSelectedTemplateLevelNode.Value);
                                    hdnSelectedTemplateFiltersNode.Value = selectedFiltersNode.OuterXml;
                                    //hdnSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                                    //adding this item in SelectedFilters listbox
                                    lstSelTempFilter.Items.Add(new ListItem(hdnSelAdditionalFilterName.Value, hdnSelAdditionalFilterId.Value));

                                    //removig this item from AvaibleFilters listbox
                                    lstAvTempFilter.Items.Remove(new ListItem(hdnSelAdditionalFilterName.Value, hdnSelAdditionalFilterId.Value));
                                }
                                else
                                {
                                    selectedLevelNode.Attributes["data"].Value = hdnTemplateData.Value;
                                    hdnSelectedTemplateFiltersNode.Value = selectedFiltersNode.OuterXml;
                                    //hdnSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879
                                }

                                hdnId.Value = "";
                                hdnSelAdditionalFilterName.Value = "";
                                hdnTemplateData.Value = "";
                                hdnSelAdditionalFilterId.Value = "";
                                hdnSelectedTemplateLevelNode.Value = "";

                            }

                            else
                            {
                                hdneditTemplateData.Value = sclientArgument;
                                selectedLevelDoc = new XmlDocument();
                                selectedLevelDoc.LoadXml(hdneditSelectedTemplateLevelNode.Value);
                                //selectedLevelDoc.LoadXml(AppHelper.HTMLCustomDecode(hdneditSelectedTemplateLevelNode.Value));//Bharani - MITS : 35879

                                selectedLevelNode = selectedLevelDoc.SelectSingleNode("Template");

                                if (selectedLevelNode.Attributes["data"] != null)
                                {
                                    selectedLevelNode.Attributes["data"].Value = hdneditTemplateData.Value;
                                }
                                else
                                {
                                    XmlAttribute neweditSelectedFilterIDAttribute = null;
                                    neweditSelectedFilterIDAttribute = selectedLevelDoc.CreateAttribute("SelectedFilterID");
                                    neweditSelectedFilterIDAttribute.Value = hdneditSelAdditionalFilterId.Value;

                                    XmlAttribute neweditSelectedFilterNameAttribute = null;
                                    neweditSelectedFilterNameAttribute = selectedLevelDoc.CreateAttribute("SelectedFilterName");
                                    neweditSelectedFilterNameAttribute.Value = hdneditSelAdditionalFilterName.Value;

                                    XmlAttribute newDataAttribute = null;
                                    newDataAttribute = selectedLevelDoc.CreateAttribute("data");
                                    newDataAttribute.Value = hdneditTemplateData.Value;

                                    selectedLevelNode.Attributes.Append(neweditSelectedFilterIDAttribute);
                                    selectedLevelNode.Attributes.Append(neweditSelectedFilterNameAttribute);
                                    selectedLevelNode.Attributes.Append(newDataAttribute);
                                }

                                hdneditSelectedTemplateLevelNode.Value = selectedLevelNode.OuterXml;
                                //hdneditSelectedTemplateLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35879

                                selectedFiltersDoc = new XmlDocument();
                                selectedFiltersDoc.LoadXml(hdneditSelectedTemplateFiltersNode.Value);
                                //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdneditSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879

                                selectedLevelNode = null;

                                selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                                selectedLevelNode = selectedFiltersNode.SelectSingleNode("Template[@TemplateId ='" + hdneditTemplateFormID.Value + "' and @SelectedFilterID = '" + hdneditSelAdditionalFilterId.Value + "']");

                                if (selectedLevelNode == null)
                                {
                                    selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + hdneditSelectedTemplateLevelNode.Value;
                                    hdneditSelectedTemplateFiltersNode.Value = selectedFiltersNode.OuterXml;
                                    //selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + AppHelper.HTMLCustomDecode(hdneditSelectedTemplateLevelNode.Value);//Bharani - MITS : 35879
                                    //hdneditSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                                    //adding this item in SelectedFilters listbox
                                    lstSelTempFilter.Items.Add(new ListItem(hdneditSelAdditionalFilterName.Value, hdneditSelAdditionalFilterId.Value));

                                    //removig this item from AvaibleFilters listbox
                                    lstAvTempFilter.Items.Remove(new ListItem(hdneditSelAdditionalFilterName.Value, hdneditSelAdditionalFilterId.Value));
                                }
                                else
                                {
                                    selectedLevelNode.Attributes["data"].Value = hdneditTemplateData.Value;
                                    hdneditSelectedTemplateFiltersNode.Value = selectedFiltersNode.OuterXml;
                                    //hdneditSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879
                                }

                                hdneditTemplateData.Value = "";
                                hdneditSelAdditionalFilterId.Value = "";
                                hdneditSelectedTemplateLevelNode.Value = "";
                                hdneditSelAdditionalFilterName.Value = "";
                            }
                        }
                        else
                        {
                            string[] arrArg;
                            arrArg = sclientArgument.Split('=');

                            UpdateControlsOnAddEditAutoTemplateFilter(arrArg[1]);
                        }

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnAddAvlFilter_Click(object sender, EventArgs e)
        {
            UpdateControlsOnAddEditAutoFilter("add");
        }

        protected void btnEditAvlFilter_Click(object sender, EventArgs e)
        {
            UpdateControlsOnAddEditAutoFilter("edit");
        }

        protected void btnRemoveAvlFilter_Click(object sender, EventArgs e)
        {
            UpdateControlsOnAddEditAutoFilter("remove");
        }

        protected void btnAddAvltempFilter_Click(object sender, EventArgs e)
        {
            UpdateControlsOnAddEditAutoTemplateFilter("add");
        }

        protected void btnEditAvltempFilter_Click(object sender, EventArgs e)
        {
            UpdateControlsOnAddEditAutoTemplateFilter("edit");
        }

        protected void btnRemoveAvltempFilter_Click(object sender, EventArgs e)
        {
            UpdateControlsOnAddEditAutoTemplateFilter("remove");
        }

        protected void cbomailmergetemp_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlDocument autoMailMergeDoc = null;


            XmlNode selTempMergefldNode = null;
            XmlNodeList availableTemplateNodeList = null;
            XmlNodeList selTempMergefldNodeList = null;
            XmlNode selectedFiltersNode = null;
            XmlNodeList selectedFilterNodeList = null;
            ListItem availableFilterItem = null;
            ListItem selectedFilterItem = null;

            hdnTemplateFormID.Value = cbomailmergetemp.SelectedValue;

            string sAutoMailMergeDoc = string.Empty;
            sAutoMailMergeDoc = ViewState["autoMailMergeTemplate"].ToString();
            autoMailMergeDoc = new XmlDocument();
            autoMailMergeDoc.LoadXml(sAutoMailMergeDoc);

            selTempMergefldNode = autoMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableTemplateList");

            lstAvTempFilter.Items.Clear();
            lstSelTempFilter.Items.Clear();

            hdnSelectedTemplateFiltersNode.Value = "<SelectedTemplateList></SelectedTemplateList>";
            //hdnSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode("<SelectedTemplateList></SelectedTemplateList>");//Bharani - MITS : 35879

            selTempMergefldNodeList = selTempMergefldNode.SelectNodes("Template");

            string[] DisplayCategory;

            if (selTempMergefldNodeList.Count > 0)
            {
                string sDisplayCategoryValue = string.Empty;
                foreach (XmlNode levelNode in selTempMergefldNodeList)
                {
                    string sTemplateID = string.Empty;
                    sTemplateID = levelNode.Attributes["TemplateId"].Value;
                    if (sTemplateID == hdnTemplateFormID.Value)
                    {
                        sDisplayCategoryValue = levelNode.Attributes["DisplayCategory"].Value;
                        DisplayCategory = sDisplayCategoryValue.Split(',');
                        foreach (string sDisplayName in DisplayCategory)
                        {
                            if (sDisplayName.Contains("Litigation") || sDisplayName.Contains("Litigation Supplemental"))
                            {
                                if (lstAvTempFilter.Items.FindByValue("1") == null)
                                {
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Litigation Types";
                                    availableFilterItem.Value = "1";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Litigation Statuses ";
                                    availableFilterItem.Value = "2";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                }

                            }
                            else if (sDisplayName.Contains("Subrogation Supplemental") || sDisplayName.Contains("Subrogation Info") || sDisplayName.Contains("Subrogation Financial"))
                            {
                                if (lstAvTempFilter.Items.FindByValue("3") == null)
                                {
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Subrogation Types";
                                    availableFilterItem.Value = "3";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Subrogation Statuses";
                                    availableFilterItem.Value = "4";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Subrogation Status Descriptions";
                                    availableFilterItem.Value = "5";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                }
                            }
                            else if (sDisplayName.Contains("Liability Info") || sDisplayName.Contains("Liability Damage Type") || sDisplayName.Contains("Liability Supplemental"))
                            {
                                if (lstAvTempFilter.Items.FindByValue("6") == null)
                                {
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Liability Types";
                                    availableFilterItem.Value = "6";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                }
                            }
                            else if (sDisplayName.Contains("Treatment Plan"))
                            {
                                if (lstAvTempFilter.Items.FindByValue("7") == null)
                                {
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Plan Statuses";
                                    availableFilterItem.Value = "7";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                }
                            }

                            else if (sDisplayName.Contains("Property Information") || sDisplayName.Contains("Property Salvage Info") || sDisplayName.Contains("Property Info") || sDisplayName.Contains("Property Supplemental"))
                            {
                                if (lstAvTempFilter.Items.FindByValue("8") == null)
                                {
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Type(s) of Property";
                                    availableFilterItem.Value = "8";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                }
                            }

                            else if (sDisplayName.Contains("Claimant") || sDisplayName.Contains("Claimant Supplementals"))
                            {
                                if (lstAvTempFilter.Items.FindByValue("9") == null)
                                {
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Claimant Types";
                                    availableFilterItem.Value = "9";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                }
                            }

                            else if (sDisplayName.Contains("Defendant") || sDisplayName.Contains("Defendant Supplementals"))
                            {
                                if (lstAvTempFilter.Items.FindByValue("10") == null)
                                {
                                    availableFilterItem = new ListItem();
                                    availableFilterItem.Text = "Defendant Types";
                                    availableFilterItem.Value = "10";
                                    lstAvTempFilter.Items.Add(availableFilterItem);
                                }
                            }


                        }


                    }
                }
            }
        }

        protected void cboprinter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PrintDocument oPrintDocument = new PrintDocument();

                oPrintDocument.PrinterSettings.PrinterName = cboprinter.SelectedValue;
               
                cboPaperBin.Items.Clear();
                foreach (PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
                {
                    ListItem item = new ListItem(oPaperSource.SourceName, oPaperSource.RawKind.ToString());
                    cboPaperBin.Items.Add(item);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void UpdateControlsOnAddEditAutoFilter(string mode)
        {
            XmlDocument storedMailMergeDoc = null;
            XmlDocument selectedFiltersDoc = null;
            XmlNode selectedLevelNode = null;
            XmlNode selectedFiltersNode = null;

            string surlToOpen = string.Empty;
            string sscriptForOpen = string.Empty;

            hdnMode.Value = mode;
            if (mode == "add")
            {
                if (AvailableFilters.SelectedItem != null)
                {
                    hdnFilterName.Value = AvailableFilters.SelectedItem.Text;
                    hdnId.Value = AvailableFilters.SelectedItem.Value;
                    hdnTemplateId.Value = cboTemplate.SelectedItem.Value;

                    storedMailMergeDoc = GetStoredAutoMailMergeDoc();
                    selectedLevelNode = storedMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/Filters/AvlFilters/level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + hdnId.Value + "']");

                    PopulateHiddenFieldsForAddEditAutoFilter(selectedLevelNode);
                }
                else
                {
                    return;
                }
            }
            else if (mode == "edit")
            {
                if (SelectedFilters.SelectedItem != null)
                {
                    hdnFilterName.Value = SelectedFilters.SelectedItem.Text;
                    hdnId.Value = SelectedFilters.SelectedItem.Value;
                    hdnTemplateId.Value = cboTemplate.SelectedItem.Value;

                    selectedFiltersDoc = new XmlDocument();
                    selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                    //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35879
                    selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                    selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + SelectedFilters.SelectedItem.Value + "']");

                    PopulateHiddenFieldsForAddEditAutoFilter(selectedLevelNode);
                }
                else
                {
                    return;
                }
            }
            else if (mode == "remove")
            {
                if (SelectedFilters.SelectedItem != null)
                {
                    hdnTemplateId.Value = cboTemplate.SelectedItem.Value;

                    selectedFiltersDoc = new XmlDocument();
                    selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                    //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35879
                    selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                    selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + SelectedFilters.SelectedItem.Value + "']");

                    if (selectedLevelNode != null)
                    {
                        selectedFiltersNode.RemoveChild(selectedLevelNode);

                        hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                        //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                        AvailableFilters.Items.Add(SelectedFilters.SelectedItem);
                        SelectedFilters.Items.Remove(SelectedFilters.SelectedItem);
                        AvailableFilters.SelectedIndex = -1;
                    }
                    return;
                }
                else
                {
                    return;
                }
            }

            if (hdnFilterType.Value != "6") //No option
            {
                hdnSqlFill.Value = hdnSqlFill.Value.Replace("'", "`").Replace("(", "^^^").Replace(")", "@@@");
                hdnFilterName.Value = hdnFilterName.Value.Replace("(", "^^^").Replace(")", "@@@");
                //surlToOpen = "AddEditAutoMailMerge.aspx?selectedid=" + hdnId.Value + "&templateid=" + hdnTemplateId.Value + "&mode=" + hdnMode.Value + "&filtername=" + hdnFilterName.Value + "&sqlfill=" + hdnSqlFill.Value + "&filtertype=" + hdnFilterType.Value + "&defvalue=" + hdnDefValue.Value + "&data=" + hdnData.Value + "&database=" + hdnDatabase.Value;
                //asharma326 MITS 35669 
                surlToOpen = "AddEditAutoMailMerge.aspx?selectedid=" + hdnId.Value + "&templateid=" + hdnTemplateId.Value + "&mode=" + hdnMode.Value + "&filtername=" + hdnFilterName.Value + "&filtertype=" + hdnFilterType.Value + "&defvalue=" + hdnDefValue.Value + "&data=" + hdnData.Value + "&database=" + hdnDatabase.Value;
                sscriptForOpen = "OpenAddEditAutoMailMergeFilter('" + surlToOpen + "');";
                ScriptManager.RegisterStartupScript(UpatePaneForAutoFilterStep2, typeof(string), "AddEditAutoMailMerge", sscriptForOpen, true);
                hdnFilterName.Value = hdnFilterName.Value.Replace("^^^", "(").Replace("@@@", ")");
                hdnSqlFill.Value = hdnSqlFill.Value.Replace("'", "`").Replace("^^^", "(").Replace("@@@", ")");
            }
            else
            {
                UpateForAutoFilterStep2();
            }

        }

        private void UpdateControlsOnAddEditAutoTemplateFilter(string smode)
        {
            XmlDocument storedMailMergeDoc = null;
            XmlDocument selectedFiltersDoc = null;
            XmlNode selectedLevelNode = null;
            XmlNode selectedFiltersNode = null;

            string surlToOpen = string.Empty;
            string sscriptForOpen = string.Empty;
            XmlAttribute dataNodeAtb = null;

            string sDefID = string.Empty;
            sDefID = ViewState["defid"].ToString();

            hdnSelMode.Value = smode;
            if (smode == "add")
            {
                if (sDefID == string.Empty)
                {
                    if (lstAvTempFilter.SelectedItem != null)
                    {
                        hdnSelAdditionalFilterName.Value = lstAvTempFilter.SelectedItem.Text;
                        hdnSelAdditionalFilterId.Value = lstAvTempFilter.SelectedItem.Value;
                        hdnTemplateId.Value = cboTemplate.SelectedItem.Value;
                        hdnTemplateFormID.Value = cbomailmergetemp.SelectedItem.Value;

                        storedMailMergeDoc = GetStoredAutoMailMergeDoc();
                        string sAutoMailMergeDoc = string.Empty;
                        sAutoMailMergeDoc = ViewState["autoMailMergeTemplate"].ToString();
                        storedMailMergeDoc.LoadXml(sAutoMailMergeDoc);

                        selectedLevelNode = storedMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableTemplateList/Template[@TemplateId ='" + hdnTemplateFormID.Value + "']");

                        if (selectedLevelNode != null)
                        {
                            hdnSelectedTemplateLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);
                            dataNodeAtb = selectedLevelNode.Attributes["data"];
                            if (dataNodeAtb != null)
                            {
                                hdnTemplateData.Value = dataNodeAtb.Value;
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    if (lstAvTempFilter.SelectedItem != null)
                    {
                        hdneditSelAdditionalFilterName.Value = lstAvTempFilter.SelectedItem.Text;
                        hdneditSelAdditionalFilterId.Value = lstAvTempFilter.SelectedItem.Value;
                        hdneditTemplateID.Value = cboTemplate.SelectedItem.Value;
                        hdneditTemplateFormID.Value = cbomailmergetemp.SelectedItem.Value;

                        storedMailMergeDoc = GetStoredAutoMailMergeDoc();
                        string sAutoMailMergeDoc = string.Empty;
                        sAutoMailMergeDoc = ViewState["autoMailMergeTemplate"].ToString();
                        storedMailMergeDoc.LoadXml(sAutoMailMergeDoc);

                        selectedLevelNode = storedMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard/AvailableTemplateList/Template[@TemplateId ='" + hdneditTemplateFormID.Value + "']");

                        if (selectedLevelNode != null)
                        {
                            hdneditSelectedTemplateLevelNode.Value = selectedLevelNode.OuterXml;
                            //hdneditSelectedTemplateLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35879
                            dataNodeAtb = selectedLevelNode.Attributes["data"];
                            if (dataNodeAtb != null)
                            {
                                hdneditTemplateData.Value = dataNodeAtb.Value;
                            }
                            else
                            {
                                hdneditTemplateData.Value = "";
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                
            }
            else if (smode == "edit")
            {
                if (sDefID == string.Empty)
                {
                    if (lstSelTempFilter.SelectedItem != null)
                    {
                        hdnSelAdditionalFilterName.Value = lstSelTempFilter.SelectedItem.Text;
                        hdnSelAdditionalFilterId.Value = lstSelTempFilter.SelectedItem.Value;
                        hdnTemplateId.Value = cboTemplate.SelectedItem.Value;
                        hdnTemplateFormID.Value = cbomailmergetemp.SelectedItem.Value;

                        selectedFiltersDoc = new XmlDocument();
                        selectedFiltersDoc.LoadXml(hdnSelectedTemplateFiltersNode.Value);
                        //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879
                        selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                        selectedLevelNode = selectedFiltersNode.SelectSingleNode("Template[@Category ='" + hdnTemplateId.Value + "' and @SelectedFilterID = '" + lstSelTempFilter.SelectedItem.Value + "']");

                        if (selectedLevelNode != null)
                        {
                            hdnSelectedTemplateLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);
                            dataNodeAtb = selectedLevelNode.Attributes["data"];
                            if (dataNodeAtb != null)
                            {
                                hdnTemplateData.Value = dataNodeAtb.Value;
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    if (lstSelTempFilter.SelectedItem != null)
                    {
                        hdneditSelAdditionalFilterName.Value = lstSelTempFilter.SelectedItem.Text;
                        hdneditSelAdditionalFilterId.Value = lstSelTempFilter.SelectedItem.Value;
                        hdneditTemplateID.Value = cboTemplate.SelectedItem.Value;
                        hdneditTemplateFormID.Value = cbomailmergetemp.SelectedItem.Value;

                        selectedFiltersDoc = new XmlDocument();
                        selectedFiltersDoc.LoadXml(hdneditSelectedTemplateFiltersNode.Value);
                        //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdneditSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879
                        selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                        selectedLevelNode = selectedFiltersNode.SelectSingleNode("Template[@Category ='" + 1 + "' and @SelectedFilterID = '" + lstSelTempFilter.SelectedItem.Value + "']");


                        if (selectedLevelNode != null)
                        {
                            hdneditSelectedTemplateLevelNode.Value = selectedLevelNode.OuterXml;
                            //hdneditSelectedTemplateLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35879
                            dataNodeAtb = selectedLevelNode.Attributes["data"];
                            if (dataNodeAtb != null)
                            {
                                hdneditTemplateData.Value = dataNodeAtb.Value;
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                
            }
            else if (smode == "remove")
            {
                if (sDefID == string.Empty)
                {
                    if (lstSelTempFilter.SelectedItem != null)
                    {
                        hdnSelAdditionalFilterName.Value = lstSelTempFilter.SelectedItem.Text;
                        hdnSelAdditionalFilterId.Value = lstSelTempFilter.SelectedItem.Value;
                        hdnTemplateId.Value = cboTemplate.SelectedItem.Value;
                        hdnTemplateFormID.Value = cbomailmergetemp.SelectedItem.Value;

                        selectedFiltersDoc = new XmlDocument();
                        selectedFiltersDoc.LoadXml(hdnSelectedTemplateFiltersNode.Value);
                        //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879
                        selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                        selectedLevelNode = selectedFiltersNode.SelectSingleNode("Template[@Category ='" + hdnTemplateId.Value + "' and @SelectedFilterID = '" + lstSelTempFilter.SelectedItem.Value + "']");

                        if (selectedLevelNode != null)
                        {
                            selectedFiltersNode.RemoveChild(selectedLevelNode);

                            hdnSelectedTemplateFiltersNode.Value = selectedFiltersNode.OuterXml;
                            //hdnSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                            lstAvTempFilter.Items.Add(lstSelTempFilter.SelectedItem);
                            lstSelTempFilter.Items.Remove(lstSelTempFilter.SelectedItem);
                            lstAvTempFilter.SelectedIndex = -1;
                        }
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    if (lstSelTempFilter.SelectedItem != null)
                    {
                        hdnSelAdditionalFilterName.Value = lstSelTempFilter.SelectedItem.Text;
                        hdnSelAdditionalFilterId.Value = lstSelTempFilter.SelectedItem.Value;
                        hdnTemplateId.Value = cboTemplate.SelectedItem.Value;
                        hdnTemplateFormID.Value = cbomailmergetemp.SelectedItem.Value;

                        selectedFiltersDoc = new XmlDocument();
                        selectedFiltersDoc.LoadXml(hdneditSelectedTemplateFiltersNode.Value);
                        //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdneditSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879
                        selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedTemplateList");
                        selectedLevelNode = selectedFiltersNode.SelectSingleNode("Template[@Category ='" + 1 + "' and @SelectedFilterID = '" + lstSelTempFilter.SelectedItem.Value + "']");

                        if (selectedLevelNode != null)
                        {
                            selectedFiltersNode.RemoveChild(selectedLevelNode);

                            hdneditSelectedTemplateFiltersNode.Value = selectedFiltersNode.OuterXml;
                            //hdneditSelectedTemplateFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                            lstAvTempFilter.Items.Add(lstSelTempFilter.SelectedItem);
                            lstSelTempFilter.Items.Remove(lstSelTempFilter.SelectedItem);
                            lstAvTempFilter.SelectedIndex = -1;
                        }
                        return;
                    }
                    else
                    {
                        return;
                    }

                }
            }

    
            if (sDefID == string.Empty)
            {
                surlToOpen = "AddEditAutoMailMergeAddSelection.aspx?selectedid=" + hdnSelAdditionalFilterId.Value + "&templateid=" + hdnTemplateId.Value + "&mode=" + hdnSelMode.Value + "&filtername=" + hdnSelAdditionalFilterName.Value + "&selectedtemplateFormID=" + hdnTemplateFormID.Value + "&data=" + hdnTemplateData.Value;
            }
            else
            {
                surlToOpen = "AddEditAutoMailMergeAddSelection.aspx?selectedid=" + hdneditSelAdditionalFilterId.Value + "&templateid=" + hdneditTemplateID.Value + "&mode=" + hdnSelMode.Value + "&filtername=" + hdneditSelAdditionalFilterName.Value + "&selectedtemplateFormID=" + hdneditTemplateFormID.Value + "&data=" + hdneditTemplateData.Value;
            }
            sscriptForOpen = "OpenAddEditAutoMailMergeTemplateFilter('" + surlToOpen + "');";
            ScriptManager.RegisterStartupScript(UpatePaneForAutoFilterStep2, typeof(string), "AddEditAutoMailMergeAddSelection", sscriptForOpen, true);

        }

        private void PopulateHiddenFieldsForAddEditAutoFilter(XmlNode selectedLevelNode)
        {
            XmlAttribute sqlFillAtb = null;
            XmlAttribute filterTypeAtb = null;
            XmlAttribute defValueAtb = null;
            XmlAttribute dataNodeAtb = null;

            if (selectedLevelNode != null)
            {
                hdnSelectedLevelNode.Value = selectedLevelNode.OuterXml;
                //hdnSelectedLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35879

                sqlFillAtb = selectedLevelNode.Attributes["SQLFill"];
                if (sqlFillAtb != null)
                {
                    hdnSqlFill.Value = sqlFillAtb.Value;
                }

                filterTypeAtb = selectedLevelNode.Attributes["FilterType"];
                if (filterTypeAtb != null)
                {
                    hdnFilterType.Value = filterTypeAtb.Value;
                }

                defValueAtb = selectedLevelNode.Attributes["DefValue"];
                if (defValueAtb != null)
                {
                    hdnDefValue.Value = defValueAtb.Value;
                }

                dataNodeAtb = selectedLevelNode.Attributes["data"];
                if (dataNodeAtb != null)
                {
                    hdnData.Value = dataNodeAtb.Value;
                }

                if (selectedLevelNode.Attributes["Database"] != null)
                {
                    hdnDatabase.Value = selectedLevelNode.Attributes["Database"].Value;
                }

            }
        }

        protected void UpateForAutoFilterStep2()
        {
            XmlDocument selectedFiltersDoc = null;
            XmlDocument selectedLevelDoc = null;
            XmlNode selectedLevelNode = null;
            XmlNode selectedFiltersNode = null;

            try
            {
                selectedLevelDoc = new XmlDocument();
                selectedLevelDoc.LoadXml(hdnSelectedLevelNode.Value);
                //selectedLevelDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedLevelNode.Value));//Bharani - MITS : 35879

                selectedLevelNode = selectedLevelDoc.SelectSingleNode("level");

                hdnSelectedLevelNode.Value = selectedLevelNode.OuterXml;
                //hdnSelectedLevelNode.Value = AppHelper.HTMLCustomEncode(selectedLevelNode.OuterXml);//Bharani - MITS : 35879

                selectedFiltersDoc = new XmlDocument();
                selectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                //selectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35879

                selectedLevelNode = null;

                selectedFiltersNode = selectedFiltersDoc.SelectSingleNode("SelectedFilters");
                selectedLevelNode = selectedFiltersNode.SelectSingleNode("level[@templateid ='" + hdnTemplateId.Value + "' and @id = '" + hdnId.Value + "']");

                if (selectedLevelNode == null)
                {
                    selectedFiltersNode.InnerXml = selectedFiltersNode.InnerXml + hdnSelectedLevelNode.Value;
                    hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                    //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879

                    //adding this item in SelectedFilters listbox
                    SelectedFilters.Items.Add(new ListItem(hdnFilterName.Value, hdnId.Value));

                    //removig this item from AvaibleFilters listbox
                    AvailableFilters.Items.Remove(new ListItem(hdnFilterName.Value, hdnId.Value));
                }
                else
                {
                    hdnSelectedFiltersNode.Value = selectedFiltersNode.OuterXml;
                    //hdnSelectedFiltersNode.Value = AppHelper.HTMLCustomEncode(selectedFiltersNode.OuterXml);//Bharani - MITS : 35879
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                hdnId.Value = "";
                hdnFilterName.Value = "";
                hdnSqlFill.Value = "";
                hdnFilterType.Value = "";
                hdnData.Value = "";
                hdnMode.Value = "";
                hdnDefValue.Value = "";
                hdnSelectedLevelNode.Value = "";

                selectedFiltersDoc = null;
                selectedLevelDoc = null;
                selectedLevelNode = null;
                selectedFiltersNode = null;
            }
        }

        #endregion

        #region BestPracticeDiaryWizard

        protected void CtrlBestPracticeMailMergeWizard_NextButtonClick(object sender, EventArgs e)
        {
            if (CtrlBestPracticeMailMergeWizard.ActiveStepIndex == 0)
            {
                ShowAutoFilterWizard();
            }
           
        }

        protected void CtrlBestPracticeMailMergeWizard_PreviousButtonClick(object sender, EventArgs e)
        {

        }

        protected void CtrlBestPracticeMailMergeWizard_CancelButtonClick(object sender, EventArgs e)
        {

        }

        protected void CtrlBestPracticeMailMergeWizard_FinishButtonClick(object sender, EventArgs e)
        {

        }

        #endregion

        #region Auto MailMerge Wizard

        #region Fetching AutoMailMergeWizard Details From Webservice

        private XmlDocument GetAutoMailMergeFilter()
        {
            string scwsResponse = string.Empty;
            XmlDocument autoMailMergeDoc = null;
            XmlNode inputDocNode = null;
            string sserviceMethodToCall = string.Empty;

            try
            {
                sserviceMethodToCall = "AutoMailMergeAdaptor.GetMergeDefList";

                inputDocNode = GetInputDocForAutoMailMerge();

                scwsResponse = CallCWSFunctionFromAppHelper(inputDocNode, sserviceMethodToCall);


                autoMailMergeDoc = new XmlDocument();
                autoMailMergeDoc.LoadXml(scwsResponse);

                return autoMailMergeDoc;


            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument GetAutoMailMergeFilterEntityRoles()
        {
            string scwsResponse = string.Empty;
            XmlDocument autoMailMergeDoc = null;
            XmlNode inputDocNode = null;
            string sserviceMethodToCall = string.Empty;

            try
            {
                sserviceMethodToCall = "AutoMailMergeAdaptor.GetEntityRoleList";

                inputDocNode = GetInputDocForAutoMailMerge();

                scwsResponse = CallCWSFunctionFromAppHelper(inputDocNode, sserviceMethodToCall);


                autoMailMergeDoc = new XmlDocument();
                autoMailMergeDoc.LoadXml(scwsResponse);

                return autoMailMergeDoc;


            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlDocument GetPeopleInvolvedList()
        {
            string scwsResponse = string.Empty;
            XmlDocument autoMailMergeDoc = null;
            XmlNode inputDocNode = null;
            string serviceMethodToCall = string.Empty;

            try
            {
                serviceMethodToCall = "AutoMailMergeAdaptor.GetPeopleInvolvedList";

                inputDocNode = GetInputDocForAutoMailMerge();

                scwsResponse = CallCWSFunctionFromAppHelper(inputDocNode, serviceMethodToCall);


                autoMailMergeDoc = new XmlDocument();
                autoMailMergeDoc.LoadXml(scwsResponse);

                return autoMailMergeDoc;


            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetInputDocForAutoMailMerge()
        {
            string smessageTemplateString = string.Empty;

            smessageTemplateString = GetAutoMailMergeFilterTemplate();

            XmlDocument messageDoc = new XmlDocument();
            messageDoc.LoadXml(smessageTemplateString);

            messageDoc.SelectSingleNode("AutoMailMergeWizard/DefId").InnerText = ViewState["defid"].ToString();
            messageDoc.SelectSingleNode("AutoMailMergeWizard/TemplateTypeID").InnerText = hdnTemplateId.Value.ToString();
            messageDoc.SelectSingleNode("AutoMailMergeWizard/TemplateFormID").InnerText = hdnTemplateFormID.Value.ToString();

            if (string.IsNullOrEmpty(ViewState["defid"].ToString()))
            {
                if (!string.IsNullOrEmpty(hdnClaimantvalues.Value))
                {
                    string sClaimantvalue = hdnClaimantvalues.Value;
                    string sTmp = string.Empty;
                    string sClaimantCodes = string.Empty;
                    if (sClaimantvalue != string.Empty)
                    {
                        sTmp = sClaimantvalue.Trim();
                        sClaimantCodes = sTmp.Replace(" ", ",");
                        sClaimantCodes = sClaimantCodes + ",";

                    }

                    messageDoc.SelectSingleNode("AutoMailMergeWizard/ClaimantValues").InnerText = sClaimantCodes;
                }

                if (!string.IsNullOrEmpty(hdnDefendantvalues.Value))
                {
                    string sDefendantvalue = hdnDefendantvalues.Value;
                    string sTmp = string.Empty;
                    string sDefendantCodes = string.Empty;
                    if (sDefendantvalue != string.Empty)
                    {
                        sTmp = sDefendantvalue.Trim();
                        sDefendantCodes = sTmp.Replace(" ", ",");
                        sDefendantCodes = sDefendantCodes + ",";

                    }

                    messageDoc.SelectSingleNode("AutoMailMergeWizard/DefendantValues").InnerText = sDefendantCodes;
                }

                if (!string.IsNullOrEmpty(hdnExpertvalues.Value))
                {
                    string sExpertvalue = hdnExpertvalues.Value;
                    string sTmp = string.Empty;
                    string sExpertCodes = string.Empty;
                    if (sExpertvalue != string.Empty)
                    {
                        sTmp = sExpertvalue.Trim();
                        sExpertCodes = sTmp.Replace(" ", ",");
                        sExpertCodes = sExpertCodes + ",";

                    }

                    messageDoc.SelectSingleNode("AutoMailMergeWizard/ExpertWitnessValues").InnerText = sExpertCodes;
                }
            }
            else
            {
                messageDoc.SelectSingleNode("AutoMailMergeWizard/ClaimantValues").InnerText = hdnClaimantvalues.Value;
                messageDoc.SelectSingleNode("AutoMailMergeWizard/DefendantValues").InnerText = hdnDefendantvalues.Value;
                messageDoc.SelectSingleNode("AutoMailMergeWizard/ExpertWitnessValues").InnerText = hdnExpertvalues.Value;
            }
            
            if (!string.IsNullOrEmpty(hdnPersonInvolvedValue.Value))
            {
                messageDoc.SelectSingleNode("AutoMailMergeWizard/PersonInvolvedValues").InnerText = hdnPersonInvolvedValue.Value;
            }

            return messageDoc.SelectSingleNode("/");
        }

        private string GetAutoMailMergeFilterTemplate() 
        {
            XElement oTemplate = XElement.Parse(@"
                    <AutoMailMergeWizard name='AutoMailMergeSetup'>
                    <DefId />
                    <AutoMailMergeTemplate selectedid='' selectedname=''></AutoMailMergeTemplate>
                    <AutoMailMergeName></AutoMailMergeName>
                    <AutoMailMergeDate></AutoMailMergeDate>
                    <Filters>
                    <AvlFilters></AvlFilters>
                    <SelectedFilters></SelectedFilters>
                    </Filters>
                    <TemplateTypeID></TemplateTypeID>
                    <TemplateFormID></TemplateFormID>
                    <SelTempMergeList></SelTempMergeList>
                    <AvailableTemplateList></AvailableTemplateList>
                    <SelectedTemplateList></SelectedTemplateList>
                    <AutoMailMergeSelFormTemplate selectedid='' selectedname=''></AutoMailMergeSelFormTemplate>
                    <AvailableCategory>
                          <AvailableCategoryID></AvailableCategoryID>
                           <AvailableCategoryName></AvailableCategoryName>
                    </AvailableCategory>
		            <TempSpec>
                        <TempSpecFilterLevelList /> 
                        <TempSpecFilterIDList/> 
                        <TempSpecFilterNameList /> 
                    </TempSpec>
		            <SelectedPersonInvoluedList></SelectedPersonInvoluedList>
		            <DefendantValues></DefendantValues>
		            <ClaimantValues></ClaimantValues>
		            <ExpertWitnessValues></ExpertWitnessValues>
                    <PersonInvolvedValues></PersonInvolvedValues>
                    <AdjusterValue></AdjusterValue>
                    <CaseManagerValue></CaseManagerValue> 
		            <PrinterName></PrinterName>
		            <PaperBin></PaperBin>
                    </AutoMailMergeWizard>
                    ");
            return oTemplate.ToString();
        }

        private XmlDocument GetStoredAutoMailMergeDoc()
        {
            string sAutoMailMergeDoc = string.Empty;
            XmlDocument autoMailMergeDoc = null;

            sAutoMailMergeDoc = ViewState["autoFilterMailMergeDoc"].ToString();

            autoMailMergeDoc = new XmlDocument();
            autoMailMergeDoc.LoadXml(sAutoMailMergeDoc);

            return autoMailMergeDoc;
        }

        #endregion

        #endregion

        #region ContactingWebService

        private string GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                        <Message>
                          <Authorization></Authorization>
                          <Call>
                            <Function></Function>
                          </Call>
                          <Document>
                          </Document>
                        </Message>
                    ");

            return oTemplate.ToString();
        }

        public string CallCWSFunctionFromAppHelper(XmlNode inputDocumentNode, string serviceMethodToCall)
        {
            XmlNode tempNode = null;
            string sMessageElement = string.Empty;
            string sresponseCWS = string.Empty;

            XmlDocument messageTemplateDoc = null;

            try
            {
                sMessageElement = GetMessageTemplate();
                messageTemplateDoc = new XmlDocument();
                messageTemplateDoc.LoadXml(sMessageElement);

                if (inputDocumentNode != null)
                {
                    tempNode = messageTemplateDoc.SelectSingleNode("Message/Document");
                    tempNode.InnerXml = inputDocumentNode.OuterXml;
                }

                if (!string.IsNullOrEmpty(serviceMethodToCall))
                {
                    tempNode = messageTemplateDoc.SelectSingleNode("Message/Call/Function");
                    tempNode.InnerText = serviceMethodToCall;
                }

                //get webservice call from Apphelper.cs
                sresponseCWS = AppHelper.CallCWSService(messageTemplateDoc.InnerXml);

                return sresponseCWS;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        #endregion


        #region Save AutoDiaryWizard

        private XmlDocument SaveAutoMailMergeFinalData()
        {
            string scwsResponse = string.Empty;
            XmlDocument autoMailMergeDoc = null;
            XmlNode inputDocNode = null;
            string sserviceMethodToCall = string.Empty;

            try
            {
                sserviceMethodToCall = "AutoMailMergeAdaptor.Save";

                inputDocNode = GetFinalUpdatedAutoMailMergeXmlOnFinish();
                scwsResponse = CallCWSFunctionFromAppHelper(inputDocNode, sserviceMethodToCall);

                autoMailMergeDoc = new XmlDocument();
                autoMailMergeDoc.LoadXml(scwsResponse);

                return autoMailMergeDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XmlNode GetFinalUpdatedAutoMailMergeXmlOnFinish()
        {
            XmlDocument autoMailMergeDoc = null;
            XmlDocument storedMailMergeDoc = null;
            XmlNode storedMailMergeWizardNode = null;
            string sautoMailMergeTemplateString = string.Empty;
            bool bPopulateFromControls = false;

            try
            {
                sautoMailMergeTemplateString = GetAutoMailMergeFilterTemplate();
                autoMailMergeDoc = new XmlDocument();

                autoMailMergeDoc.LoadXml(sautoMailMergeTemplateString);

                storedMailMergeDoc = GetStoredAutoMailMergeDoc();
                storedMailMergeWizardNode = storedMailMergeDoc.SelectSingleNode("ResultMessage/Document/AutoMailMergeWizard");


                if (ViewState["defid"].ToString() != "")
                {
                    bPopulateFromControls = false;

                    if (CtrlAutoMailMergeWizard.ActiveStepIndex == 0)
                    {
                        UpdateOnFinishAutoFilterStep1(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep2(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        UpdateOnFinishAutoFilterStep3(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        UpdateOnFinishAutoFilterStep4(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        UpdateOnFinishAutoFilterStep5(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        
                    }
                    else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 1)
                    {
                        UpdateOnFinishAutoFilterStep1(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep2(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep3(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        UpdateOnFinishAutoFilterStep4(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        UpdateOnFinishAutoFilterStep5(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        //UpdateOnFinishAutoFilterStep6(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                    }
                    else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 2)
                    {
                        UpdateOnFinishAutoFilterStep1(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep2(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep3(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep4(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        UpdateOnFinishAutoFilterStep5(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        //UpdateOnFinishAutoFilterStep6(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                    }
                    else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 3)
                    {
                        UpdateOnFinishAutoFilterStep1(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep2(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep3(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep4(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep5(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                        //UpdateOnFinishAutoFilterStep6(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                    }
                    else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 4)
                    {
                        UpdateOnFinishAutoFilterStep1(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep2(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep3(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep4(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep5(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        //UpdateOnFinishAutoFilterStep6(ref autoMailMergeDoc, storedMailMergeWizardNode, false);
                    }
                    else if (CtrlAutoMailMergeWizard.ActiveStepIndex == 5)
                    {
                        UpdateOnFinishAutoFilterStep1(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep2(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep3(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep4(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        UpdateOnFinishAutoFilterStep5(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                        //UpdateOnFinishAutoFilterStep6(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                    }
                }
                else
                {
                    UpdateOnFinishAutoFilterStep1(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                    UpdateOnFinishAutoFilterStep2(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                    UpdateOnFinishAutoFilterStep3(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                    UpdateOnFinishAutoFilterStep4(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                    UpdateOnFinishAutoFilterStep5(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                    //UpdateOnFinishAutoFilterStep6(ref autoMailMergeDoc, storedMailMergeWizardNode, true);
                }

                return autoMailMergeDoc.SelectSingleNode("/");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateOnFinishAutoFilterStep1(ref XmlDocument autoMailMergeDoc, XmlNode storedMailMergeWizardNode, bool populateFromControls)
        {
            XmlNode mailMergeWizardNode = null;
            XmlNode mailMergeTemplateNode = null;
            XmlNode mailMergeNameNode = null;
            XmlNode mailMergeDateNode = null;
            XmlNode defIdNode = null;

            XmlNode storedMailMergeTemplateNode = null;
            XmlNode storedMailMergeNameNode = null;
            XmlNode storedMailMergeDateNode = null;

            mailMergeWizardNode = autoMailMergeDoc.SelectSingleNode("AutoMailMergeWizard");

            if (mailMergeWizardNode != null)
            {
                defIdNode = mailMergeWizardNode.SelectSingleNode("DefId");
                if (defIdNode != null)
                {
                    defIdNode.InnerText = ViewState["defid"].ToString();
                }

                mailMergeTemplateNode = mailMergeWizardNode.SelectSingleNode("AutoMailMergeTemplate");

                if (mailMergeTemplateNode != null)
                {
                    if (populateFromControls)
                    {
                        mailMergeTemplateNode.Attributes["selectedname"].Value = cboTemplate.SelectedItem.Text;
                        mailMergeTemplateNode.Attributes["selectedid"].Value = cboTemplate.SelectedItem.Value;
                    }
                    else //populate from stored doc
                    {
                        storedMailMergeTemplateNode = storedMailMergeWizardNode.SelectSingleNode("AutoMailMergeTemplate");

                        if (storedMailMergeTemplateNode != null)
                        {
                            mailMergeTemplateNode.Attributes["selectedname"].Value = storedMailMergeTemplateNode.Attributes["selectedname"].Value;
                            mailMergeTemplateNode.Attributes["selectedid"].Value = storedMailMergeTemplateNode.Attributes["selectedid"].Value;
                        }
                    }
                }

                mailMergeNameNode = mailMergeWizardNode.SelectSingleNode("AutoMailMergeName");
                if (mailMergeNameNode != null)
                {
                    if (populateFromControls)
                    {
                        mailMergeNameNode.InnerText = txtMailMergeName.Text;
                    }
                    else //populate from stored doc
                    {
                        storedMailMergeNameNode = storedMailMergeWizardNode.SelectSingleNode("AutoMailMergeName");
                        if (storedMailMergeNameNode != null)
                        {
                            mailMergeNameNode.InnerText = storedMailMergeNameNode.InnerText;
                        }
                    }
                }

                mailMergeDateNode = mailMergeWizardNode.SelectSingleNode("AutoMailMergeDate");
                if (mailMergeDateNode != null)
                {
                    if (populateFromControls)
                    {
                        mailMergeDateNode.InnerText = txtMailMergeDate.Value;
                    }
                    else
                    {
                        storedMailMergeDateNode = mailMergeWizardNode.SelectSingleNode("AutoMailMergeDate");

                        if (storedMailMergeDateNode != null)
                        {
                            mailMergeDateNode.InnerText = storedMailMergeDateNode.InnerText;
                        }
                    }
                }
            }
        }

        private void UpdateOnFinishAutoFilterStep2(ref XmlDocument autoMailMergeDoc, XmlNode storedMailMergeWizardNode, bool populateFromControls)
        {
            XmlDocument hdnSelectedFiltersDoc = null;
            XmlNode hdnSelectedNode = null;
            XmlNode selectedFiltersNode = null;

            XmlNode storedSelectedFiltersNOde = null;

            selectedFiltersNode = autoMailMergeDoc.SelectSingleNode("AutoMailMergeWizard/Filters/SelectedFilters");

            if (populateFromControls)
            {
                //fetching selected filters from hdnField
                hdnSelectedFiltersDoc = new XmlDocument();
                hdnSelectedFiltersDoc.LoadXml(hdnSelectedFiltersNode.Value);
                //hdnSelectedFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedFiltersNode.Value));//Bharani - MITS : 35879
                hdnSelectedNode = hdnSelectedFiltersDoc.SelectSingleNode("SelectedFilters");

                selectedFiltersNode.InnerXml = hdnSelectedNode.InnerXml;
            }
            else
            {
                storedSelectedFiltersNOde = storedMailMergeWizardNode.SelectSingleNode("Filters/SelectedFilters");
                selectedFiltersNode.InnerXml = storedSelectedFiltersNOde.InnerXml;
            }
        }

        private void UpdateOnFinishAutoFilterStep3(ref XmlDocument autoMailMergeDoc, XmlNode storedMailMergeWizardNode, bool populateFromControls)
        {
            string sDefID = string.Empty;
            sDefID = ViewState["defid"].ToString();

            XmlNode mailMergeWizardNode = null;
            XmlNode mailMergeSelTemplateNode = null;
            XmlNode mailMergeCategoryIDNode = null;
            XmlNode mailMergeCategoryNameNode = null;

            XmlNode storedMailMergeSelTemplateNode = null;
            XmlNode storedMailMergeCategoryNameNode = null;
            XmlNode storedMailMergeCategoryIDNode = null;

            XmlDocument hdnSelectedTemplateFiltersDoc = null;
            XmlNode hdnSelectedTemplateNode = null;
            XmlNode selectedTemplateFiltersNode = null;

            XmlNode storedSelectedTemplateFiltersNode = null;

            XmlNodeList storedSelectedTemplateNodeList = null;
            XmlNodeList storedhdnSelectedTemplateNodeList = null;
            

            mailMergeWizardNode = autoMailMergeDoc.SelectSingleNode("AutoMailMergeWizard");

            if (mailMergeWizardNode != null)
            {
                mailMergeSelTemplateNode = mailMergeWizardNode.SelectSingleNode("AutoMailMergeSelFormTemplate");

                if (mailMergeSelTemplateNode != null)
                {
                    if (populateFromControls)
                    {
                        mailMergeSelTemplateNode.Attributes["selectedname"].Value = cbomailmergetemp.SelectedItem.Text;
                        mailMergeSelTemplateNode.Attributes["selectedid"].Value = cbomailmergetemp.SelectedItem.Value;
                    }
                    else //populate from stored doc
                    {
                        storedMailMergeSelTemplateNode = storedMailMergeWizardNode.SelectSingleNode("AutoMailMergeSelFormTemplate");

                        if (storedMailMergeSelTemplateNode != null)
                        {
                            mailMergeSelTemplateNode.Attributes["selectedname"].Value = storedMailMergeSelTemplateNode.Attributes["selectedname"].Value;
                            mailMergeSelTemplateNode.Attributes["selectedid"].Value = storedMailMergeSelTemplateNode.Attributes["selectedid"].Value;
                        }
                    }
                }

                mailMergeCategoryIDNode = mailMergeWizardNode.SelectSingleNode("AvailableCategory/AvailableCategoryID");

                if (mailMergeCategoryIDNode != null)
                {
                    mailMergeCategoryIDNode.InnerText = hdnSelTemplateFilterID.Value;
                }

                else //populate from stored doc
                {
                    storedMailMergeCategoryIDNode = storedMailMergeWizardNode.SelectSingleNode("AvailableCategory/AvailableCategoryID");

                    if (storedMailMergeCategoryIDNode != null)
                    {
                        mailMergeCategoryIDNode.InnerText = storedMailMergeCategoryIDNode.InnerText;
                    }
                }


                mailMergeCategoryNameNode = mailMergeWizardNode.SelectSingleNode("AvailableCategory/AvailableCategoryName");

                if (mailMergeCategoryNameNode != null)
                {
                    mailMergeCategoryNameNode.InnerText = hdnSelTemplateFilterName.Value; ;
                }

                else //populate from stored doc
                {
                    storedMailMergeCategoryNameNode = storedMailMergeWizardNode.SelectSingleNode("AvailableCategory/AvailableCategoryName");

                    if (storedMailMergeCategoryNameNode != null)
                    {
                        mailMergeCategoryNameNode.InnerText = storedMailMergeCategoryNameNode.InnerText;
                    }
                }

                selectedTemplateFiltersNode = autoMailMergeDoc.SelectSingleNode("AutoMailMergeWizard/SelectedTemplateList");

                if (string.IsNullOrEmpty(sDefID))
                {
                    if (populateFromControls)
                    {
                        //fetching selected filters from hdnField
                        hdnSelectedTemplateFiltersDoc = new XmlDocument();
                        hdnSelectedTemplateFiltersDoc.LoadXml(hdnSelectedTemplateFiltersNode.Value);
                        //hdnSelectedTemplateFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdnSelectedTemplateFiltersNode.Value)); //Bharani - MITS : 35879
                        hdnSelectedTemplateNode = hdnSelectedTemplateFiltersDoc.SelectSingleNode("SelectedTemplateList");

                        selectedTemplateFiltersNode.InnerXml = hdnSelectedTemplateNode.InnerXml;
                    }
                    else
                    {
                        storedSelectedTemplateFiltersNode = storedMailMergeWizardNode.SelectSingleNode("SelectedTemplateList");
                        selectedTemplateFiltersNode.InnerXml = storedSelectedTemplateFiltersNode.InnerXml;
                    }
                }
                else
                {
                    if (populateFromControls)
                    {
                        //XmlDocument cc = new XmlDocument();
                        //string mm = storedMailMergeWizardNode.InnerXml;
                        //cc.LoadXml(mm);
                        //storedSelectedTemplateFiltersNode = storedMailMergeWizardNode.SelectSingleNode("SelectedTemplateList");
                        //storedSelectedTemplateNodeList = storedSelectedTemplateFiltersNode.SelectNodes("Template");
                        //if (storedSelectedTemplateNodeList.Count > 0)
                        //{
                        //    foreach (XmlNode levelNode in storedSelectedTemplateNodeList)
                        //    {
                        //       string ss = levelNode.Attributes["SelectedFilterID"].Value;
                        //       if (lstSelTempFilter.Items.FindByValue(ss) != null)
                        //       {
                        //           selectedTemplateFiltersNode.AppendChild(autoMailMergeDoc.ImportNode(levelNode, false));
                        //       }
                        //    }
                        //}
                        //fetching selected filters from hdnField
                        hdnSelectedTemplateFiltersDoc = new XmlDocument();
                        hdnSelectedTemplateFiltersDoc.LoadXml(hdneditSelectedTemplateFiltersNode.Value);
                        //hdnSelectedTemplateFiltersDoc.LoadXml(AppHelper.HTMLCustomDecode(hdneditSelectedTemplateFiltersNode.Value));//Bharani - MITS : 35879
                        hdnSelectedTemplateNode = hdnSelectedTemplateFiltersDoc.SelectSingleNode("SelectedTemplateList");
                        storedhdnSelectedTemplateNodeList = hdnSelectedTemplateNode.SelectNodes("Template");
                        if (storedhdnSelectedTemplateNodeList.Count > 0)
                        {
                            foreach (XmlNode levelhdnNode in storedhdnSelectedTemplateNodeList)
                            {
                                string sFilterID = string.Empty;
                                string sTemplateID = string.Empty;
                                sFilterID = levelhdnNode.Attributes["SelectedFilterID"].Value;
                                sTemplateID = levelhdnNode.Attributes["TemplateId"].Value;
                                if (lstSelTempFilter.Items.FindByValue(sFilterID) != null)
                                {
                                    if (cbomailmergetemp.SelectedItem.Value == sTemplateID)
                                    {
                                        selectedTemplateFiltersNode.AppendChild(autoMailMergeDoc.ImportNode(levelhdnNode, false));
                                    }
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        storedSelectedTemplateFiltersNode = storedMailMergeWizardNode.SelectSingleNode("SelectedTemplateList");
                        selectedTemplateFiltersNode.InnerXml = storedSelectedTemplateFiltersNode.InnerXml;
                    }
                }
            }
        }

        private void UpdateOnFinishAutoFilterStep4(ref XmlDocument autoMailMergeDoc, XmlNode storedMailMergeWizardNode, bool populateFromControls)
        {
            string sDefID = string.Empty;
            sDefID = ViewState["defid"].ToString();

            XmlNode mailMergeWizardNode = null;
            XmlNode mailMergeDefendantNode = null;
            XmlNode mailMergeClaimantNode = null;
            XmlNode mailMergeExpertWitnessNode = null;
            XmlNode mailMergePersonInvolvedNode = null;
            XmlNode mailMergeAdjusterNode = null;
            XmlNode mailMergeCasemanagerNode = null;

            XmlNode storedMailMergeDefendantNode = null;
            XmlNode storedMailMergeClaimantNode = null;
            XmlNode storedMailMergeExpertWitnessNode = null;
            XmlNode storedMailMergePersonInvolvedNode = null;
            XmlNode storedMailMergeAdjusterNode = null;
            XmlNode storedMailMergeCasemanagerNode = null;

            mailMergeWizardNode = autoMailMergeDoc.SelectSingleNode("AutoMailMergeWizard");

            if (mailMergeWizardNode != null)
            {
                mailMergePersonInvolvedNode = mailMergeWizardNode.SelectSingleNode("PersonInvolvedValues");

                if (mailMergePersonInvolvedNode != null)
                {
                    if (populateFromControls)
                    {
                        string strTemp = hdnPersonInvolvedValue.Value;

                        if (!string.IsNullOrEmpty(strTemp))
                        {
                            mailMergePersonInvolvedNode.InnerText =  hdnPersonInvolvedValue.Value + ",";
                        }
                        else
                        {
                            mailMergePersonInvolvedNode.InnerText = string.Empty;
                        }
                        
                    }

                    else
                    {
                        storedMailMergePersonInvolvedNode = storedMailMergeWizardNode.SelectSingleNode("PersonInvolvedValues");

                        if (storedMailMergePersonInvolvedNode != null)
                        {
                            mailMergePersonInvolvedNode.InnerText = storedMailMergePersonInvolvedNode.InnerText;
                        }
                    }
                }

                mailMergeClaimantNode = mailMergeWizardNode.SelectSingleNode("ClaimantValues");

                if (mailMergeClaimantNode != null)
                {
                    if (string.IsNullOrEmpty(sDefID))
                    {
                        if (populateFromControls)
                        {

                            MultiCodeLookUp MuserControl = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstClaimantType");
                            string sClaimantType = MuserControl.CodeId;
                            string sClaimant = string.Empty;
                            string sClaimantTypeCode = string.Empty;
                            if (sClaimantType != string.Empty)
                            {
                                sClaimant = sClaimantType.Trim();
                                sClaimantTypeCode = sClaimant.Replace(" ", ",");
                                sClaimantTypeCode = sClaimantTypeCode + ",";

                            }

                            mailMergeClaimantNode.InnerText = sClaimantTypeCode;
                        }

                        else
                        {
                            storedMailMergeClaimantNode = storedMailMergeWizardNode.SelectSingleNode("ClaimantValues");

                            if (storedMailMergeClaimantNode != null)
                            {
                                mailMergeClaimantNode.InnerText = storedMailMergeClaimantNode.InnerText;
                            }
                        }
                    }

                    else
                    {
                        if (populateFromControls)
                        {
                            MultiCodeLookUp MuserControl = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstClaimantType");
                            string sClaimantType = MuserControl.CodeId;
                            string sClaimant = string.Empty;
                            string sClaimantTypeCode = string.Empty;
                            if (sClaimantType != string.Empty)
                            {
                                sClaimant = sClaimantType.Trim();
                                sClaimantTypeCode = sClaimant.Replace(" ", ",");
                                sClaimantTypeCode = sClaimantTypeCode + ",";

                            }

                            mailMergeClaimantNode.InnerText = sClaimantTypeCode;

                            //if (string.IsNullOrEmpty(hdnClaimantvalues.Value))
                            //{
                            //    MailMergeClaimantNode.InnerText = hdnClaimantvalues.Value + strTmpCodes;
                            //}
                            //else
                            //{
                            //    MailMergeClaimantNode.InnerText = hdnClaimantvalues.Value + "," + strTmpCodes;
                            //}

                        }

                        else
                        {
                            storedMailMergeClaimantNode = storedMailMergeWizardNode.SelectSingleNode("ClaimantValues");

                            if (storedMailMergeClaimantNode != null)
                            {
                                mailMergeClaimantNode.InnerText = storedMailMergeClaimantNode.InnerText;
                            }
                        }
                    }
                    }

                    mailMergeDefendantNode = mailMergeWizardNode.SelectSingleNode("DefendantValues");

                    if (mailMergeDefendantNode != null)
                    {
                        if (string.IsNullOrEmpty(sDefID))
                        {
                            if (populateFromControls)
                            {
                                MultiCodeLookUp MuserControlDefendant = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstDefendantType");
                                string sDefendantType = MuserControlDefendant.CodeId;
                                string sDefendant = string.Empty;
                                string sDefendantCodes = string.Empty;
                                if (sDefendantType != string.Empty)
                                {
                                    sDefendant = sDefendantType.Trim();
                                    sDefendantCodes = sDefendant.Replace(" ", ",");
                                    sDefendantCodes = sDefendantCodes + ",";
                                }

                                mailMergeDefendantNode.InnerText = sDefendantCodes;
                            }

                            else
                            {
                                storedMailMergeDefendantNode = storedMailMergeWizardNode.SelectSingleNode("DefendantValues");

                                if (storedMailMergeDefendantNode != null)
                                {
                                    mailMergeDefendantNode.InnerText = storedMailMergeDefendantNode.InnerText;
                                }
                            }
                        }
                        else
                        {
                            if (populateFromControls)
                            {
                                MultiCodeLookUp MuserControlDefendant = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstDefendantType");
                                string sDefendantType = MuserControlDefendant.CodeId;
                                string sDefendant = string.Empty;
                                string sDefendantCodes = string.Empty;
                                if (sDefendantType != string.Empty)
                                {
                                    sDefendant = sDefendantType.Trim();
                                    sDefendantCodes = sDefendant.Replace(" ", ",");
                                    sDefendantCodes = sDefendantCodes + ",";
                                }

                                mailMergeDefendantNode.InnerText = sDefendantCodes;

                                //if (string.IsNullOrEmpty(hdnDefendantvalues.Value))
                                //{
                                //    MailMergeDefendantNode.InnerText = hdnDefendantvalues.Value + strTmpDefendantCodes;
                                //}
                                //else
                                //{
                                //    MailMergeDefendantNode.InnerText = hdnDefendantvalues.Value + "," + strTmpDefendantCodes;
                                //}

                            }

                            else
                            {
                                storedMailMergeDefendantNode = storedMailMergeWizardNode.SelectSingleNode("DefendantValues");

                                if (storedMailMergeDefendantNode != null)
                                {
                                    mailMergeDefendantNode.InnerText = storedMailMergeDefendantNode.InnerText;
                                }
                            }
                        }
                }

                mailMergeExpertWitnessNode = mailMergeWizardNode.SelectSingleNode("ExpertWitnessValues");

                if (mailMergeExpertWitnessNode != null)
                {
                    if (string.IsNullOrEmpty(sDefID))
                    {
                        if (populateFromControls)
                        {
                            MultiCodeLookUp MuserControlExpert = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstExpertType");
                            string sExpertType = MuserControlExpert.CodeId;
                            string sExpert = string.Empty;
                            string sExpertCodes = string.Empty;
                            if (sExpertType != string.Empty)
                            {
                                sExpert = sExpertType.Trim();
                                sExpertCodes = sExpert.Replace(" ", ",");
                                sExpertCodes = sExpertCodes + ",";

                            }

                            mailMergeExpertWitnessNode.InnerText = sExpertCodes;
                        }

                        else
                        {
                            storedMailMergeExpertWitnessNode = storedMailMergeWizardNode.SelectSingleNode("ExpertWitnessValues");

                            if (storedMailMergeExpertWitnessNode != null)
                            {
                                mailMergeExpertWitnessNode.InnerText = storedMailMergeExpertWitnessNode.InnerText;
                            }
                        }
                    }

                    else
                    {
                        if (populateFromControls)
                        {
                            MultiCodeLookUp MuserControlExpert = (MultiCodeLookUp)CtrlAutoMailMergeWizard.FindControl("lstExpertType");
                            string sExpertType = MuserControlExpert.CodeId;
                            string sExpert = string.Empty;
                            string sExpertCodes = string.Empty;
                            if (sExpertType != string.Empty)
                            {
                                sExpert = sExpertType.Trim();
                                sExpertCodes = sExpert.Replace(" ", ",");
                                sExpertCodes = sExpertCodes + ",";
                               
                            }

                            mailMergeExpertWitnessNode.InnerText = sExpertCodes;

                        }

                        else
                        {
                            storedMailMergeExpertWitnessNode = storedMailMergeWizardNode.SelectSingleNode("ExpertWitnessValues");

                            if (storedMailMergeExpertWitnessNode != null)
                            {
                                mailMergeExpertWitnessNode.InnerText = storedMailMergeExpertWitnessNode.InnerText;
                            }
                        }
                     
                    }
                }

                mailMergeAdjusterNode = mailMergeWizardNode.SelectSingleNode("AdjusterValue");

                if (mailMergeAdjusterNode != null)
                {
                    if (string.IsNullOrEmpty(sDefID))
                    {
                        if (populateFromControls)
                        {
                            mailMergeAdjusterNode.InnerText = hdnAdjustervalue.Value;
                        }

                        else
                        {
                            storedMailMergeAdjusterNode = storedMailMergeWizardNode.SelectSingleNode("AdjusterValue");

                            if (storedMailMergeAdjusterNode != null)
                            {
                                mailMergeAdjusterNode.InnerText = storedMailMergeAdjusterNode.InnerText;
                            }
                        }
                    }

                    else
                    {
                        if (populateFromControls)
                        {
                            mailMergeAdjusterNode.InnerText = hdnAdjustervalue.Value;
                        }

                        else
                        {
                            storedMailMergeAdjusterNode = storedMailMergeWizardNode.SelectSingleNode("AdjusterValue");

                            if (storedMailMergeAdjusterNode != null)
                            {
                                mailMergeAdjusterNode.InnerText = storedMailMergeAdjusterNode.InnerText;
                            }
                        }

                    }
                }


                mailMergeCasemanagerNode = mailMergeWizardNode.SelectSingleNode("CaseManagerValue");

                if (mailMergeCasemanagerNode != null)
                {
                    if (string.IsNullOrEmpty(sDefID))
                    {
                        if (populateFromControls)
                        {
                            mailMergeCasemanagerNode.InnerText = hdnCasemanagervalue.Value;
                        }

                        else
                        {
                            storedMailMergeCasemanagerNode = storedMailMergeWizardNode.SelectSingleNode("CaseManagerValue");

                            if (storedMailMergeCasemanagerNode != null)
                            {
                                mailMergeCasemanagerNode.InnerText = storedMailMergeCasemanagerNode.InnerText;
                            }
                        }
                    }

                    else
                    {
                        if (populateFromControls)
                        {
                            mailMergeCasemanagerNode.InnerText = hdnCasemanagervalue.Value;
                        }

                        else
                        {
                            storedMailMergeCasemanagerNode = storedMailMergeWizardNode.SelectSingleNode("CaseManagerValue");

                            if (storedMailMergeCasemanagerNode != null)
                            {
                                mailMergeCasemanagerNode.InnerText = storedMailMergeCasemanagerNode.InnerText;
                            }
                        }

                    }
                }



            }

        }

        private void UpdateOnFinishAutoFilterStep5(ref XmlDocument autoMailMergeDoc, XmlNode storedMailMergeWizardNode, bool populateFromControls)
        {

            XmlNode MailMergeWizardNode = null;
            XmlNode MailMergePrinterNameNode = null;
            XmlNode MailMergepaperNameNode = null;


            XmlNode storedMailMergePrinterNameNode = null;
            XmlNode storedMailMergepaperNameNode = null;

            MailMergeWizardNode = autoMailMergeDoc.SelectSingleNode("AutoMailMergeWizard");

            if (MailMergeWizardNode != null)
            {
                MailMergePrinterNameNode = MailMergeWizardNode.SelectSingleNode("PrinterName");

                if (MailMergePrinterNameNode != null)
                {
                    if (populateFromControls)
                    {
                        MailMergePrinterNameNode.InnerText = cboprinter.SelectedValue;
                    }

                    else
                    {
                        storedMailMergePrinterNameNode = storedMailMergeWizardNode.SelectSingleNode("PrinterName");

                        if (storedMailMergePrinterNameNode != null)
                        {
                            MailMergePrinterNameNode.InnerText = storedMailMergePrinterNameNode.InnerText;
                        }
                    }
                }

                MailMergepaperNameNode = MailMergeWizardNode.SelectSingleNode("PaperBin");

                if (MailMergepaperNameNode != null)
                {
                    if (populateFromControls)
                    {
                        MailMergepaperNameNode.InnerText = cboPaperBin.SelectedValue;
                    }

                    else
                    {
                        storedMailMergepaperNameNode = storedMailMergeWizardNode.SelectSingleNode("PaperBin");

                        if (storedMailMergepaperNameNode != null)
                        {
                            MailMergepaperNameNode.InnerText = storedMailMergepaperNameNode.InnerText;
                        }
                    }
                }

               
            }

        }

        # endregion
    }
}
