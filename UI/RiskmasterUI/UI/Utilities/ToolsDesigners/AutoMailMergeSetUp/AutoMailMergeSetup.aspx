﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoMailMergeSetup.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.ToolsDesigners.AutoMailMergeSetUp.AutoMailMergeSetup" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Auto Mail Merge Setup</title>
    <script type="text/javascript" src="../../../../Scripts/form.js"></script>
    
    <script language="javascript">

        function SetImageOnMouseOver(selectedImageControlId) {
            var selectedControl = document.getElementById(selectedImageControlId);

            if (selectedImageControlId == 'New_AutoMailMergeList') {
                selectedControl.src = '../../../../Images/new2.gif';
            }
            else if (selectedImageControlId == 'Edit_AutoMailMergeList') {
                selectedControl.src = '../../../../Images/edittoolbar2.gif';
            }
            else if (selectedImageControlId == 'Delete_AutoMailMergeList') {
                selectedControl.src = '../../../../Images/delete2.gif';
            }
        }

        function SetImageOnMouseOut(selectedImageControlId) {
            var selectedControl = document.getElementById(selectedImageControlId);

            if (selectedImageControlId == 'New_AutoMailMergeList') {
                selectedControl.src = '../../../../Images/new.gif';
            }
            else if (selectedImageControlId == 'Edit_AutoMailMergeList') {
                selectedControl.src = '../../../../Images/edittoolbar.gif';
            }
            else if (selectedImageControlId == 'Delete_AutoMailMergeList') {
                selectedControl.src = '../../../../Images/delete.gif';
            }
        }
        
    </script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <uc1:ErrorControl runat="server" ID="ErrorControl"></uc1:ErrorControl>
        <div class="msgheader" id="formtitle">
            Auto Mail Merge Setup
        </div>
         <table border="0" cellspacing="0" cellpadding="0" width="100%" 
            style="height: 500px">
             <tr>
                <td width="70%" align="left" valign="top">
                    <br />
                        Current Auto Mail Merge Definitions
      			    <br />
                    <asp:ListBox ID="AutoMailMergeList" runat="server" Width="95%"  Visible="true" ondblclick="return openGridAddEditWindow(&#34;AutoMailMergeList&#34;,&#34;edit&#34;,600,500);" Rows="20"></asp:ListBox>
                </td>
                <td width="40%" valign="top"><br>
                    <table width="100%" valign="top" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="image"  src="../../../../Images/new.gif" alt="" id="New_AutoMailMergeList" onmouseover="SetImageOnMouseOver('New_AutoMailMergeList')" onmouseout="SetImageOnMouseOut('New_AutoMailMergeList')" title="New"   onclick="return openGridAddEditWindow('AutoMailMergeList','add','600','560')" />
                                <br />
                                <input type="image" src="../../../../Images/edittoolbar.gif" alt="" id="Edit_AutoMailMergeList" onmouseover="SetImageOnMouseOver('Edit_AutoMailMergeList')" onmouseout="SetImageOnMouseOut('Edit_AutoMailMergeList')" title="Edit" onclick="return openGridAddEditWindow('AutoMailMergeList','edit','600','500')" />
                                <br />
                                    <asp:ImageButton ImageUrl="../../../../Images/delete.gif" ID="Delete_AutoMailMergeList" runat="server" onmouseover="SetImageOnMouseOver('Delete_AutoMailMergeList')" onmouseout="SetImageOnMouseOut('Delete_AutoMailMergeList')" title="Delete" 
                                    OnClientClick="return validateGridForDeletion('AutoMailMergeList')" onclick="Delete_AutoMailMergeList_Click" />
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
