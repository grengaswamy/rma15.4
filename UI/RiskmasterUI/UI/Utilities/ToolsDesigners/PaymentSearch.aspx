﻿<%@ Page Title="" Language="C#" theme="RMX_Default" MasterPageFile="~/UI/Utilities/UtilityTemplate.Master" AutoEventWireup="true" CodeBehind="PaymentSearch.aspx.cs" Inherits="Riskmaster.UI.Utilities.PaymentSearch" %>
<asp:content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    
    <title>Payment Search</title>

</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="cphUtilityBody" runat="server">

    <table border="0" width="100%">
        <tr>
            <td colspan="3" class="ctrlgroup">
                Standard Payment Search
                <br />
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                Search Criteria
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
            
                <table border="0" width="100%">
                    <tr>
                        <td>
                            Check Control Number :
                        </td>
                        <td>
                            <asp:dropdownlist id="ddlCheckControlNumber" name="ddlCheckControlNumber" runat="server" />
                        </td>
                        <td>
                            <asp:textbox id="txtCheckControlNumber" name="txtCheckControlNumber" width="100%" runat="server" />
                        </td>
                    </tr>
                
                    <tr>
                        <td>
                            Check Transaction Date :
                        </td>
                        <td>
                            <asp:dropdownlist id="ddlCheckTransactionDate" name="ddlCheckTransactionDate" runat="server" />
                        </td>
                        <td>
                            <asp:textbox id="txtCheckTransactionFromDate" name="txtCheckTransactionFromDate" runat="server" />
                            <asp:button id="btSearchCheckTransactionFromDate" name="btSearchCheckTransactionFromDate" text="..." runat="server" />
                            <asp:textbox id="txtCheckTransactionToDate" name="txtCheckTransactionToDate" runat="server" />
                            <asp:button id="btSearchCheckTransactionToDate" name="btSearchCheckTransactionToDate" text="..." runat="server" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Check Date :
                        </td>
                        <td>
                            <asp:dropdownlist id="ddlCheckDate" name="ddlCheckDate" runat="server" />
                        </td>
                        <td>
                            <asp:textbox id="txtCheckDateFrom" name="txtCheckDateFrom" runat="server" />
                            <asp:button id="btSearchCheckDateFrom" name="btSearchCheckDateFrom" text="..." runat="server" />
                            <asp:textbox id="txtCheckDateTo" name="txtCheckDateTo" runat="server" />
                            <asp:button id="btSearchCheckDateTo" name="btSearchCheckDateTo" text="..." runat="server" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Check Amount :
                        </td>
                        <td>
                            <asp:dropdownlist id="ddlCheckAmount" name="ddlCheckAmount" runat="server" />
                        </td>
                        <td>
                            <asp:textbox id="txtCheckAmount" name="txtCheckAmount" runat="server" width="250px" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Payee Last Name :
                        </td>
                        <td>
                            <asp:dropdownlist id="ddlPayeeLastName" name="ddlPayeeLastName" runat="server" />
                        </td>
                        <td>
                            <asp:textbox id="txtPayeeLastName" name="txtPayeeLastName" width="100%" runat="server" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Payee First Name :
                        </td>
                        <td>
                            <asp:dropdownlist id="ddlPayeeFirstName" name="ddlPayeeFirstName" runat="server" />
                        </td>
                        <td>
                            <asp:textbox id="txtPayeeFirstName" name="txtPayeeFirstName" width="100%" runat="server" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Payee SSN/Tax ID :
                        </td>
                        <td>
                            <asp:dropdownlist id="ddlPayeeSSNTaxID" name="ddlPayeeSSNTaxID" runat="server" />
                        </td>
                        <td>
                            <asp:textbox id="txtPayeeSSNTaxID" name="txtPayeeSSNTaxID" width="100%" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td colspan="3" class="ctrlgroup">
                Sort By
            </td>
        </tr>
        
        <tr>
            <td>
                <asp:dropdownlist id="ddlSortByCol1" name="ddlSortByCol1" width="100%" runat="server" />
            </td>
            <td>
                <asp:dropdownlist id="ddlSortByCol2" name="ddlSortByCol2" width="100%" runat="server" />
            </td>
            <td>
                <asp:dropdownlist id="ddlSortByCol3" name="ddlSortByCol3" width="100%" runat="server" />
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <asp:checkbox id="cbUseSoundAlike" name="cbUseSoundAlike" text="Use Sound-Alike (Soundex) Match on Last Names" runat="server" />
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <asp:checkbox id="cbDefaultSearchView" name="cbDefaultSearchView" text="Default search view" runat="server" />
            </td>
        </tr>
        
        <tr>
            <td colspan="3">
                <asp:button id="btSubmitQuery" name="btSubmitQuery" text="Submit Query" runat="server" />
                <asp:button id="btCancel" name="btCancel" text="Cancel" runat="server" />
                <asp:button id="btClear" name="btClear" text="Clear" runat="server" />
            </td>
        </tr>
    </table>

</asp:content>
