﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QueryDesigner.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.QueryDesigner.QueryDesigner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script language="JavaScript" src="../../../Scripts/query_design.js">
    </script>
    <%--abansal23: MITS 16225 on 05/06/2009 Starts--%>
    <script language="javascript" type="text/javascript">
        function ReSubmitPage() 
        {
            window.location.href = window.location;
        }
    </script>
    <%--abansal23: MITS 16225 on 05/06/2009 Stops--%>
</head>
<body onload="parent.MDIScreenLoaded();" style="height:93%">
    <form id="frmdata" runat="server" name="frmdata" >
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">
     <input type="hidden" id="hdnDelete" name="" runat="server">
    <table width="99.5%">
        <tr class="msgheader">
            <td colspan="2">                
                <asp:label Id="lblSearchViews" runat = "Server" Text= "<%$ Resources:lblSearchViews %>" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <input type="submit" name="$action^" value="<%$ Resources:btnNew %>" id="btnNew" class="button" runat="server"
                    onclick=" return openWizard()"/>
                &nbsp;&nbsp;
                <input type="submit" name="$action^" value="<%$ Resources:btnDelete %>" id="btnDelete" class="button"
                    onclick="JavaScript:return DeleteSelected();" runat="server" onserverclick = "DeleteSearches"/>
            </td>
        </tr>
    </table>
    <%--<div class="divScroll">--%>
        <asp:GridView ID="grdSearchCategory1" AutoGenerateColumns="true" runat="server">
        </asp:GridView>
        <table width="99%">
            <asp:Repeater ID="rptSearchCategory" runat="server" OnItemDataBound ="rptSearchCategory_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td colspan="2" class="colheader5">
                            <%# DataBinder.Eval(Container.DataItem, "CatName")%>
                        </td>
                    </tr>
                    <tr>
                        <asp:Repeater ID="rptSearchNames" runat="server">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td width="16">
                                        <input type="checkbox" name="$node^25" value='<%#DataBinder.Eval(Container.DataItem, "SearchId")%>' />
                                    </td>
                                    <td>
                                        <a href="/oxf/home" onclick="return editWizard(this.id)"
                                            id='<%#DataBinder.Eval(Container.DataItem, "SearchId")%>'><%#DataBinder.Eval(Container.DataItem, "SearchName")%></a><%--"; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%2625%26content%261021';         event.returnValue=false;         if (editWizard(this.id))          document.forms['wsrp_rewrite_form_1'].submit();         "--%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    <%--</div>--%>
    <asp:TextBox ID="hdnsearchname" runat ="server" style="display:none" ></asp:TextBox>
    </form>
</body>
</html>
