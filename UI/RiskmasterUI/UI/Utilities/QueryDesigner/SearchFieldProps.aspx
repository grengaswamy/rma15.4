﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchFieldProps.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.QueryDesigner.SearchFieldProps" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">  
  <title>Search Field Properties</title>    
  <script type ="text/javascript" language="JavaScript" src="../../../Scripts/query_design.js"></script>
</head>
<body>
    <form id="wsrp_rewrite_form_1" runat ="server" >
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <div>
        <table width="100%" border="0">
            <tr>
               <td>
                    <br />
      			    Field Type:&nbsp;&nbsp;      			
      		       <asp:DropDownList runat ="server" id="fieldprops" rmxref="/Instance/Document//SearchFieldProps/SentToProps" style="width:150px">      		        
      		       </asp:DropDownList>                   
               </td>
            </tr>
            
            <tr>
               <td>
                    <hr />
               </td>
            </tr>
            
            <tr>
               <td>
                  <asp:Button Text="OK" runat ="server" class="button" style="width:50px" OnClientClick ="Javascript:return PropOK();" />
      		      &nbsp;
      		     <asp:Button Text="Cancel"  runat ="server" class="button" OnClientClick ="Javascript:return window.close();" style="width:50px" />
      		   </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
