﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.BenefitCalculator
{
    public partial class BenCalc : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                if (!IsPostBack)
                {
                    FunctionToCall.Text = "BenefitCalculatorAdaptor.Get";
                    txtClaimID.Text = AppHelper.GetQueryStringValue("ClaimID");
                    bReturnStatus = CallCWS(FunctionToCall.Text, XmlTemplate, out sreturnValue, true, true); 
                }
                else if(FunctionToCall.Text == "BenefitCalculatorAdaptor.Save")
                {
                    bReturnStatus = CallCWS(FunctionToCall.Text, XmlTemplate, out sreturnValue, true, true); 

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
            
            //BenefitCalculatorAdaptor.Save
        }
    }
}
