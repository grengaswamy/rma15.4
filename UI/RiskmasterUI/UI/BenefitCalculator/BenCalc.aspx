﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BenCalc.aspx.cs" Inherits="Riskmaster.UI.BenefitCalculator.BenCalc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Benefit Calculator</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>

     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript" language="javascript"></script>
	
	<uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" src="../../Scripts/BenCalc.js"></script>
</head>
<body onload="BenCalc_PageLoad();">
    <form id="frmData" runat="server">
    <div>
        <asp:TextBox rmxref="/Instance/Document/BenefitCalculator/FunctionToCall" id="FunctionToCall" runat="server" style="display:none"   />
        <asp:TextBox ID="txtClaimID" rmxref="/Instance/Document/BenefitCalculator/ClaimID" runat="server" style="display:none"/> 
        <table  width="100%" border="0">
            <tr>
                <td colspan="2">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
                <td>
                    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />
                </td>
            </tr>
            <tr>
              <td class="ctrlgroup2" colspan="3">Basic Data</td>
            </tr>
            <tr>
              <td width="100%">
                <table  cellspacing="3" style="border: 1px solid ;">
                  <tr>
                    <td width="20%">Avg Weekly Wage:</td>
                    <td width="20%">
                      <asp:TextBox ID="txtAvgWeeklyWage"  rmxref="/Instance/Document/BenefitCalculator/AvgWeeklyWage" size="10" readonly="true" style="background-color: #F2F2F2" runat ="server" ></asp:TextBox>&#160;
                    </td>
                    <td width="10%">Waiting Period:</td>
                    <td width="3%">
                      <asp:TextBox id="txtWaitingPeriod" rmxref="/Instance/Document/BenefitCalculator/WaitingPeriod" size="2" readonly="true" style="background-color: #F2F2F2" runat="server" ></asp:TextBox> 
                    </td>
                    <td width="20%">
                        <asp:checkbox id="chkWaitingCheck" rmxref="/Instance/Document/BenefitCalculator/WaitSatisfied" appearance="full" onchange="setDataChanged(true);" disabled="true"  value="true" runat="server" /><asp:Label Text= "Satisfied" runat ="server" /> 
                    </td>
                    <td width="15%">Current MMI Date:</td>

                    <td width="15%">
                        <asp:TextBox runat="server" FormatAs="date" id="txtCurrentMMIDate" RMXRef="/Instance/Document/BenefitCalculator/CurrentMMIDate" size="10" RMXType="date" style="background-color: #F2F2F2" readonly="true" runat="server"/>&#160;
                        <%--<input type="button" class="DateLookupControl" name="btnCurrentMMIDate" value="..." disabled="true"/>
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
					            {
					                inputField: "txtCurrentMMIDate",
					                ifFormat: "%m/%d/%Y",
					                button: "btnCurrentMMIDate"
					            }
					            );
			            </script> --%>
                         <script type="text/javascript">
                             $(function () {
                                 $("#txtCurrentMMIDate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../Images/calendar.gif",
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                             });
                    </script>
                    </td>
                  </tr>
                  <tr>
                    <td width="15%">Accident Date:</td>                  
                    <td width="15%">
                        <asp:TextBox runat="server" FormatAs="date" id="txtAccidentDate" RMXRef="/Instance/Document/BenefitCalculator/AccidentDate" size="10" RMXType="date" style="background-color: #F2F2F2" readonly="true"/>&#160;
                        <%--<input type="button" class="DateLookupControl" name="btnAccidentDate" value="..." disabled="true"/>
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
					            {
					                inputField: "txtAccidentDate",
					                ifFormat: "%m/%d/%Y",
					                button: "btnAccidentDate"
					            }
					            );
			            </script> --%>
                         <script type="text/javascript">
                             $(function () {
                                 $("#txtAccidentDate").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../Images/calendar.gif",
                                    // buttonImageOnly: true,
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                             });
                    </script>
                    </td>
                    <td width="10%">Retroactive Period:</td>
                    <td width="3%">
                      <asp:TextBox ID="txtRetroactivePeriod" rmxref="/Instance/Document/BenefitCalculator/RetroactivePeriod" size="2" readonly="true"  style="background-color: #F2F2F2" runat="server"/>
                    </td>
                    <td width="20%">
                      <asp:checkbox id="chkRetroCheck" rmxref="/Instance/Document/BenefitCalculator/RetroSatisfied" appearance="full" onchange="setDataChanged(true);" disabled="true"  value="true" runat="server" /><asp:Label Text= "Satisfied" runat="server"/> 
                    </td>
                    <td width="15%">Current Disability%:</td>

                    <td width="15%">
                      <asp:TextBox  ID="txtCurrentDisabilityPercent" rmxref="/Instance/Document/BenefitCalculator/CurrentDisabilityPercent" size="10" readonly="true"  style="background-color: #F2F2F2" runat="server"/>&#160;
                    </td>

                  </tr>
                  <tr>
                    <td width="15%">Curr. Disability Date (From Work Loss):</td>
                     <td width="15%">
                        <asp:TextBox runat="server" FormatAs="date" id="txtCurrDisabilityDate" RMXRef="/Instance/Document/BenefitCalculator/CurrentDisabilityDateDBFormat" size="10" RMXType="date" style="background-color: #F2F2F2" readonly="true" runat="server"/>&#160;
                       <%-- <input type="button" class="DateLookupControl" name="btnCurrDisabilityDate" value="..." disabled="true"/>
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
					            {
					                inputField: "txtCurrDisabilityDate",
					                ifFormat: "%m/%d/%Y",
					                button: "btnCurrDisabilityDate"
					            }
					            );
			            </script> --%>
                          <script type="text/javascript">
                              $(function () {
                                  $("#txtCurrDisabilityDate").datepicker({
                                      showOn: "button",
                                      buttonImage: "../../Images/calendar.gif",
                                     // buttonImageOnly: true,
                                      showOtherMonths: true,
                                      selectOtherMonths: true,
                                      changeYear: true
                                  }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                              });
                    </script>
                    </td>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td width="20%">Most Recent RTW (From Work Loss):</td>
                    <td width="20%">
                        <asp:TextBox runat="server" FormatAs="date" id="txtMostRecentRTW" RMXRef="/Instance/Document/BenefitCalculator/MostRecentRTW" size="10" RMXType="date" style="background-color: #F2F2F2" readonly="true" runat="server"/>&#160;
                        <%--<input type="button" class="DateLookupControl" name="btnMostRecentRTW" value="..." disabled="true"/>
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
					            {
					                inputField: "txtMostRecentRTW",
					                ifFormat: "%m/%d/%Y",
					                button: "btnMostRecentRTW"
					            }
					            );
			            </script> --%>
                         <script type="text/javascript">
                             $(function () {
                                 $("#txtMostRecentRTW").datepicker({
                                     showOn: "button",
                                     buttonImage: "../../Images/calendar.gif",
                                    // buttonImageOnly: true,
                                     showOtherMonths: true,
                                     selectOtherMonths: true,
                                     changeYear: true
                                 }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                             });
                    </script>
                    </td>
                    
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>&#160;&#160;</td>
            </tr>
            <tr>
              <td>&#160;&#160;</td>
            </tr>
            <tr>
              <td>&#160;&#160;</td>
            </tr>
            <tr>
              <table border="0" align="center" style="border: 1px solid ;"  >

                <tr>

                  <td/>
                  <td >Jurisdiction's Abbr  &#160;&#160;</td>
                  <td >Jurisdictional Rate  &#160;&#160;</td>
                  <td >Override Rate  &#160;&#160;</td>
                </tr>
                <tr>

                  <td>TT Rate:</td>
                  <td>
                    <asp:TextBox runat="server" id="txtJurisAbbrTT" rmxref="/Instance/Document/BenefitCalculator/JurisAbbrTT" size="15" readonly="true" style="background-color: #F2F2F2"></asp:TextBox>&#160;
                  </td>
                  <td>
                    <asp:TextBox runat="server" id="txtJurisRateTT" rmxref="/Instance/Document/BenefitCalculator/JurisRateTT" size="15" readonly="true" style="background-color: #F2F2F2"></asp:TextBox>&#160;
                  </td>
                  <td>
                    <asp:TextBox runat="server" id="txtOvrRateTT" rmxref="/Instance/Document/BenefitCalculator/OverRateTT" size="15" ></asp:TextBox>&#160;
                  </td>
                </tr>
                <tr>

                  <td>PP Rate:</td>
                  <td>
                    <asp:TextBox runat="server" id="txtJurisAbbrPP" rmxref="/Instance/Document/BenefitCalculator/JurisAbbrPP" size="15" readonly="true" style="background-color: #F2F2F2"></asp:TextBox>&#160;
                  </td>
                  <td>
                    <asp:TextBox runat="server" id="txtJurisRatePP" rmxref="/Instance/Document/BenefitCalculator/JurisRatePP" size="15" readonly="true" style="background-color: #F2F2F2"></asp:TextBox>&#160;
                  </td>
                  <td>
                    <asp:TextBox runat="server" id="txtOvrRatePP" rmxref="/Instance/Document/BenefitCalculator/OverRatePP" size="15" ></asp:TextBox>&#160;
                  </td>
                </tr>
                <tr>

                  <td>PT Rate:</td>
                  <td>
                    <asp:TextBox runat="server" id="txtJurisAbbrPT" rmxref="/Instance/Document/BenefitCalculator/JurisAbbrPT" size="15" readonly="true" style="background-color: #F2F2F2"></asp:TextBox>&#160;
                  </td>
                  <td>
                    <asp:TextBox runat="server" id="txtJurisRatePT" rmxref="/Instance/Document/BenefitCalculator/JurisRatePT" size="15" readonly="true" style="background-color: #F2F2F2"></asp:TextBox>&#160;
                  </td>
                  <td>
                    <asp:TextBox runat="server" id="txtOvrRatePT" rmxref="/Instance/Document/BenefitCalculator/OverRatePT" size="15" ></asp:TextBox>&#160;
                  </td>
                  <td>
                    <asp:TextBox runat="server" ID="txtLastWorkWeekDate" rmxref="/Instance/Document/BenefitCalculator/LastWorkWeekDate" style="display:none"/>
                  </td>
                </tr>

              </table>
            </tr>
          </table>
          <table align="center">
            <tr>
              <td>
                  <asp:Button runat="server"  class="button" Text="Save" OnClientClick="return Benefit_Progress();" ID="btnSave"/>         
              </td>
              <td>
                <asp:Button runat="server" class="button" id="btnCancel" OnClientClick="return Benefit_Cancel();" Text="Reset"/>
              </td>
              </tr>
          </table>
    </div>
    </form>
</body>
</html>
