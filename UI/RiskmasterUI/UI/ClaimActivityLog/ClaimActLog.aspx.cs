﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.ClaimActivityLog
{
    public partial class ClaimActLog : System.Web.UI.Page
    {
        private string PageId = RMXResourceProvider.PageId("ClaimActLog.aspx");
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                   ViewState["FormName"] = AppHelper.GetQueryStringValue("SysFormName");
                   ViewState["ClaimId"] = AppHelper.GetQueryStringValue("ClaimId");
                   BindGridWithRecords("");
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindGridWithRecords(string sortExpression)
        {
            string oMessageElement = string.Empty;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;
            DataSet resultDataSet = null;
            XmlDocument oTemplate = new XmlDocument();
            DataView objView = null;
            try
            {
                oMessageElement = GetMessageTemplate().ToString();
                oTemplate.LoadXml(oMessageElement);
                if (ViewState["ClaimId"] != null)
                {
                    oTemplate.SelectSingleNode("//ClaimId").InnerText = ViewState["ClaimId"].ToString();
                }
                if (ViewState["FormName"] != null)
                {
                    oTemplate.SelectSingleNode("//FormName").InnerText = ViewState["FormName"].ToString();
                }
                oTemplate.SelectSingleNode("//LangCode").InnerText = AppHelper.GetLanguageCode() ;
                sReturn = AppHelper.CallCWSService(oTemplate.InnerXml);
                if (sReturn != string.Empty)
                {
                    resultDoc = new XmlDocument();
                    resultDoc.LoadXml(sReturn);

                    resultDataSet = ConvertXmlDocToDataSet(resultDoc);

                    if (resultDataSet != null)
                    {
                        if (resultDataSet.Tables.Count > 2)
                        {
                            objView = new DataView(resultDataSet.Tables[3]);
                            if (!string.IsNullOrEmpty(sortExpression))
                            {
                                objView.Sort = sortExpression;
                            }
                            else
                            {
                                objView = resultDataSet.Tables[3].DefaultView;
                            }
                            grdClaimActLog.DataSource = objView;
                            grdClaimActLog.DataBind();
                        }
                    }
                }
                else
                {   //St:Yukti ML Changes MITS 34069
                    //throw new ApplicationException("Error occured while fetching activity log");
                    throw new ApplicationException(AppHelper.GetResourceValue(PageId,"ErrorFetchingActivityLog","3"));
                }

            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                objView = null;
                resultDataSet = null;
                resultDoc = null;
            }
        }

        private DataSet ConvertXmlDocToDataSet(XmlDocument resultDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(resultDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>ClaimActLogAdaptor.GetClaimActivityLog</Function> 
                    </Call>
                    <Document>
                        <ClaimActLogList>
                            <ClaimId></ClaimId>
                            <FormName></FormName>
                            <LangCode></LangCode>
                        </ClaimActLogList>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }
        protected void grdClaimActLog_sorting(Object sender, GridViewSortEventArgs e)
		{
        
            if (!string.IsNullOrEmpty(GridSortExpression.Value) && GridSortExpression.Value == e.SortExpression)
				{
					if (GridSortDirection.Value == "ASC")
						GridSortDirection.Value = "DESC";
					else
						GridSortDirection.Value = "ASC";
				}
				else
				{
					GridSortDirection.Value = "ASC";
				}
				BindGridWithRecords(e.SortExpression + " " +GridSortDirection.Value);
				GridSortExpression.Value = e.SortExpression;
        }

        protected void grdClaimActLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdClaimActLog.PageIndex = e.NewPageIndex;
            hdnPageNumber.Value = e.NewPageIndex.ToString();
            if (GridSortExpression.Value == "")
                BindGridWithRecords("");
            else
                BindGridWithRecords(GridSortExpression.Value + " " + GridSortDirection.Value);
        }
    }
}