﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimActLog.aspx.cs" Inherits="Riskmaster.UI.UI.ClaimActivityLog.ClaimActLog" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Claim Activity Log</title>
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type ="text/jscript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;}</script> 
</head>
<body>
    <form id="frmClaimActLog" runat="server">
    <div>
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <table width ="100%">
            <tr>
			    <td class="msgheader" nowrap="" colspan="">
                <%--Yukti-ML Changes Start,MITS 34069--%>
				    <%--Claim Activity Log--%>
                    <asp:Label runat="server" ID="lblClaimActLog" Text="<%$ Resources:lblClaimActLog %>" />
                <%--Yukti-ML Changes End--%>
			    </td>
		    </tr>
		    <tr>
		        <td>
		            <asp:GridView ID="grdClaimActLog" runat="server" CellPadding="4" Width="100%" 
                        AutoGenerateColumns="False" GridLines="None" HorizontalAlign="Left" AllowSorting ="true" AllowPaging="true" 
                        OnSorting="grdClaimActLog_sorting" onpageindexchanging="grdClaimActLog_PageIndexChanging" 
                        PagerSettings-Mode="NextPreviousFirstLast" PageSize="20" PagerSettings-Position="TopAndBottom">
                        <%--Yukti-ML Changes Start,MITS 34069--%>
                        <%--<PagerSettings Mode="NextPreviousFirstLast" FirstPageText="First" NextPageText ="Next" LastPageText ="Last" PreviousPageText="Previous" />--%>
                        <PagerSettings Mode="NextPreviousFirstLast" FirstPageText="<%$ Resources:lblFirst %>" NextPageText ="<%$ Resources:lblNext %>" LastPageText ="<%$ Resources:lblLast %>" PreviousPageText="<%$ Resources:lblPrevious %>" />
                         <%--Yukti-ML Changes End--%>
                    <HeaderStyle CssClass="colheader6" ForeColor="White" HorizontalAlign="Left"/>
                    <AlternatingRowStyle CssClass="data2" /> 
                    <Columns>
                    <%--Yukti-ML Changes Start,MITS 34069--%>
<%--                        <asp:BoundField HeaderText="Date/Time" HeaderStyle-CssClass="headerlink2"  ItemStyle-CssClass="data" SortExpression="dttm_rcd_added" DataField="dttm_rcd_added"/>
                        <asp:BoundField HeaderText="User" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" SortExpression = "added_by_user" DataField="added_by_user"/>
                        <asp:BoundField HeaderText="Activity" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" SortExpression="type" DataField="type"/>
                        <asp:BoundField HeaderText="Description" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" SortExpression="log_text" DataField="log_text"/>--%>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrDtTime %>" HeaderStyle-CssClass="headerlink2"  ItemStyle-CssClass="data" SortExpression="dttm_rcd_added" DataField="dttm_rcd_added"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrUser %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" SortExpression = "added_by_user" DataField="added_by_user"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrActivity %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" SortExpression="type" DataField="type"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrDescription %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" SortExpression="log_text" DataField="log_text"/>
                        <%--Yukti-ML Changes End--%>
                    </Columns>  
                    </asp:GridView>
		        </td>
		    </tr>
		</table>
    </div>
    <asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortExpression" />
	<asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortDirection" Value="ASC" />
     <asp:HiddenField ID="hdnPageNumber" runat="server" Value="0" />
    </form>
</body>
</html>
