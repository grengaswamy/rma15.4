﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AsyncClaimActLog.aspx.cs" Inherits="Riskmaster.UI.UI.ClaimActivityLog.AsyncClaimActLog" ValidateRequest="false"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>


<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org">
<head runat="server">
 <meta charset="utf-8" http-equiv="X-UA-Compatible" content="IE=Edge">
   <title>Claim Activity Log</title>
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <script src="../../Scripts/angularjs/angular.min.js"></script>
     <script type="text/javascript" src="../../Scripts/angularjs/lib/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>
    
     <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Include Export to excel Js in code -->
  <%--  <script src="../../Scripts/angularjs/ng-grid-csv-export.js"></script>--%> 
     <!-- End CUSTOMIZATION-->
   <!--RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts -->
        <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
    <!--RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality ends -->
       
    <script type="text/javascript" src="../../Scripts/angularjs/ClaimActLog.js"></script>
    <script type="text/javascript" src="../../Scripts/form.js" ></script>

</head>
<body >
    <form id="form1" runat="server">
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
     <div style="clear: both"></div>
        <input type="hidden" runat="server" id="hdnFormName" clientidmode="Static"  />
        <input type="hidden" runat="server" id="hdnClaimId" clientidmode="Static"/>
        <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA8068 Start:Showing Header  -->
        <table width ="100%">
            <tr>
          <td class="msgheader" nowrap="" colspan="">
                <%--Yukti-ML Changes Start,MITS 34069--%>
				    <%--Claim Activity Log--%>
                    <asp:Label runat="server" ID="lblClaimActLog" Text="<%$ Resources:lblClaimActLog %>" />
              </td>
		    </tr>
            </table>
          <!-- End CUSTOMIZATION-->
    </form>
     
 <div ng-app="rmaAppCAL" id="ng-app" >
         <div ng-controller="ClaimActLogController">
              <div id="divImage" style="display: none;">
              <div class="image">
                    <input type="image" src="../../Images/tb_save_active.png" style="width: 28px; height: 28px; border: 0px;" id="save" alt="Save" runat="server"
                        title="Save Preferences" onmouseover="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'" ng-click="SavePreferences()" tooltip="<%$ Resources:ttSavePref %>" />
                </div>  
                  <!-- Raman : Gatekeeper Action: Export to Excel functionality need to be reworked upon. It should be exporting to excel and not csv as its currently doing.
                    Also formatting need to be retained and export should be readable. Currently it does not add any value to the client.
                CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Add button to redirect click event, better UI  -->
                <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start: Add button to redirect click event, better UI  -->
                  <div class="image">
                    <input type="image" src="../../Images/tb_exportexcel_active.png" style="width: 28px; height: 28px; border: 0px;" id="btnExport" alt="Export" runat="server"
                        title="<%$ Resources:ttExport %>" onmouseover="this.src='../../Images/tb_exportexcel_mo.png';this.style.zoom='110%'"  
                         onmouseout="this.src='../../Images/tb_exportexcel_active.png';this.style.zoom='100%'" ng-click="ExportToExcel()" tooltip="<%$ Resources:ttExport %>" />
                </div>
                <!-- End CUSTOMIZATION-->
               
            </div>
            <div style="clear: both"></div>
            <div class="gridStyle" ng-grid="gridOptions" style="display: none;" id="divActLogGrid"></div>
             
             <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA8068 Start: error message when No records found in Claim Activity Log  -->
             <div style="text-align:center;" >
             <label style="display: none; " id="lblEmptyData" />
             </div>
            <!-- End CUSTOMIZATION-->
               
             <!-- CUSTOMIZATION by vchouhan6 for JIRA-RMA4307 Start:Added a footer  -->
            <div class="footer">
                <iframe id="csvDownloadFrame" style="display:none" />
            </div>
            <!-- End CUSTOMIZATION-->
        </div>
    </div>
</body>
</html>
