<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="claimgc.aspx.cs" Inherits="Riskmaster.UI.FDM.Claimgc"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>General Claim</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                        height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                        height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                        height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                        height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                        height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_esumm" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/esumm.gif" width="28"
                        height="28" border="0" id="esumm" alternatetext="Executive Summary" onmouseover="this.src='Images/esumm2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/esumm.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="Images/attach.gif"
                        width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                        onmouseover="this.src='Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/attach.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_search" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doSearch()" src="Images/search.gif"
                        width="28" height="28" border="0" id="search" alternatetext="Search" onmouseover="this.src='Images/search2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/search.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="FilteredDiary()" src="Images/filtereddiary.gif"
                        width="28" height="28" border="0" id="filtereddiary" alternatetext="View Record Diaries"
                        onmouseover="this.src='Images/filtereddiary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/filtereddiary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                        height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_comments" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Comments()" src="Images/comments.gif"
                        width="28" height="28" border="0" id="comments" alternatetext="Comments" onmouseover="this.src='Images/comments2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/comments.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_enhancednotes" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="EnhancedNotes()" src="Images/progressnotes.gif"
                        width="28" height="28" border="0" id="enhancednotes" alternatetext="Enhanced Notes"
                        onmouseover="this.src='Images/progressnotes2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/progressnotes.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_mailmerge" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="MailMerge()" src="Images/mailmerge.gif"
                        width="28" height="28" border="0" id="mailmerge" alternatetext="Mail Merge" onmouseover="this.src='Images/mailmerge2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/mailmerge.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_commentsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="CommentSummary()" src="Images/commentsummary.gif"
                        width="28" height="28" border="0" id="commentsummary" alternatetext="Claim Comment Summary"
                        onmouseover="this.src='Images/commentsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/commentsummary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_eventexplorer" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="EventExplorer()" src="Images/eventexplorer.gif"
                        width="28" height="28" border="0" id="eventexplorer" alternatetext="Quick Summary"
                        onmouseover="this.src='Images/eventexplorer2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/eventexplorer.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_sendmail" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="SendMail()" src="Images/sendregarding.gif"
                        width="28" height="28" border="0" id="sendmail" alternatetext="Send Mail" onmouseover="this.src='Images/sendregarding2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/sendregarding.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_deletealldiaries" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="DeleteAllDiaries()" src="Images/diary_deleteall.gif"
                        width="28" height="28" border="0" id="deletealldiaries" alternatetext="Delete All Diaries"
                        onmouseover="this.src='Images/diary_deleteall2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/diary_deleteall.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="formtitle">
        General Claim</div>
    <div class="errtextheader">
    </div>
    <div id="Div1" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSclaiminfo" id="TABSclaiminfo">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="claiminfo"
                id="LINKTABSclaiminfo"><span style="text-decoration: none">Claim Info</span></a></div>
        <div id="Div2" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSeventdetail" id="TABSeventdetail">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="eventdetail"
                id="LINKTABSeventdetail"><span style="text-decoration: none">Event Detail</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABclaiminfo" id="FORMTABclaiminfo">
        <asp:textbox style="display: none" runat="server" id="eventid" rmxref="/Instance/Claim/EventId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="claimid" rmxref="/Instance/Claim/ClaimId"
                rmxtype="id" /><asp:textbox style="display: none" runat="server" id="ev_eventid"
                    rmxref="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventId" rmxtype="id" /><asp:textbox
                        style="display: none" runat="server" id="primaryClaimant" rmxref="/Instance/UI/FormVariables/SysExData/DiaryMessage"
                        rmxtype="id" /><div runat="server" class="half" id="div_ev_eventnumber" xmlns="">
                            <span class="required">Event Number</span><span class="formw"><asp:textbox runat="server"
                                size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"
                                id="ev_eventnumber" rmxref="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber"
                                rmxtype="eventlookup" tabindex="1" cancelledvalue="" /><asp:requiredfieldvalidator
                                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="ev_eventnumber" /></span></div>
        <div runat="server" class="half" id="div_ev_dateofevent" xmlns="">
            <span class="required">Date Of Event</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="date" onchange="setDataChanged(true);" id="ev_dateofevent"
                rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DateOfEvent"
                rmxtype="date" tabindex="20" /><asp:requiredfieldvalidator validationgroup="vgSave"
                    errormessage="Required" runat="server" controltovalidate="ev_dateofevent" /><cc1:CalendarExtender
                        runat="server" ID="ev_dateofevent_ajax" TargetControlID="ev_dateofevent" />
            </span>
        </div>
        <div runat="server" class="half" id="div_claimnumber" xmlns="">
            <span class="required">Claim Number</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="setDataChanged(true);" id="claimnumber" rmxref="/Instance/Claim/ClaimNumber"
                rmxtype="text" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                    runat="server" controltovalidate="claimnumber" /></span></div>
        <div runat="server" class="half" id="div_ev_timeofevent" xmlns="">
            <span class="required">Time Of Event</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="time" onchange="setDataChanged(true);" id="ev_timeofevent"
                rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/TimeOfEvent"
                rmxtype="time" tabindex="22" /><cc1:MaskedEditExtender runat="server" Enabled="True"
                    MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="ev_timeofevent"
                    ID="ev_timeofevent_ajax" />
                <cc1:MaskedEditValidator ValidationGroup="vgSave" runat="server" AcceptAMPM="true"
                    InvalidValueMessage="Time is invalid" Display="Dynamic" TooltipMessage="" ID="ev_timeofevent_ajaxvalidator"
                    ControlExtender="ev_timeofevent_ajax" ControlToValidate="ev_timeofevent" IsValidEmpty="False"
                    EmptyValueMessage="Required" /></span></div>
        <div runat="server" class="half" id="div_claimtypecode" xmlns="">
            <span class="required">Claim Type</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="claimtypecode" rmxref="/Instance/Claim/ClaimTypeCode"
                rmxtype="code" cancelledvalue="" tabindex="4" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="claimtypecodebtn" onclientclick="return selectCode('CLAIM_TYPE','claimtypecode');"
                    tabindex="5" /><asp:textbox style="display: none" runat="server" id="claimtypecode_cid"
                        rmxref="/Instance/Claim/ClaimTypeCode/@codeid" cancelledvalue="" /><asp:requiredfieldvalidator
                            validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="claimtypecode" /></span></div>
        <div runat="server" class="half" id="div_dateofclaim" xmlns="">
            <span class="required">Date Of Claim</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="date" onchange="setDataChanged(true);" id="dateofclaim" rmxref="/Instance/Claim/DateOfClaim"
                rmxtype="date" tabindex="23" /><asp:requiredfieldvalidator validationgroup="vgSave"
                    errormessage="Required" runat="server" controltovalidate="dateofclaim" /><cc1:CalendarExtender
                        runat="server" ID="dateofclaim_ajax" TargetControlID="dateofclaim" />
            </span>
        </div>
        <div runat="server" class="half" id="div_ev_depteid" xmlns="">
            <span class="required">Department</span><span class="formw"><asp:textbox runat="server"
                size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                id="ev_depteid" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DeptEid"
                rmxtype="orgh" name="ev_depteid" cancelledvalue="" /><asp:button runat="server" class="CodeLookupControl"
                    text="..." id="ev_depteidbtn" onclientclick="return selectCode('orgh','ev_depteid','Department');" /><asp:button
                        runat="server" class="button" text="Instr" id="ev_depteidbtnInstructions" onclientclick="return ClaimInstructions();" /><asp:textbox
                            style="display: none" runat="server" id="ev_depteid_cid" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DeptEid/@codeid"
                            cancelledvalue="" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                                runat="server" controltovalidate="ev_depteid" /></span></div>
        <div runat="server" class="half" id="div_timeofclaim" xmlns="">
            <span class="required">Time Of Claim</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="time" onchange="setDataChanged(true);" id="timeofclaim" rmxref="/Instance/Claim/TimeOfClaim"
                rmxtype="time" tabindex="25" /><cc1:MaskedEditExtender runat="server" Enabled="True"
                    MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="timeofclaim"
                    ID="timeofclaim_ajax" />
                <cc1:MaskedEditValidator ValidationGroup="vgSave" runat="server" AcceptAMPM="true"
                    InvalidValueMessage="Time is invalid" Display="Dynamic" TooltipMessage="" ID="timeofclaim_ajaxvalidator"
                    ControlExtender="timeofclaim_ajax" ControlToValidate="timeofclaim" IsValidEmpty="False"
                    EmptyValueMessage="Required" /></span></div>
        <div runat="server" class="half" id="div_claimstatuscode" xmlns="">
            <span class="required">Claim Status</span><span class="formw"><asp:textbox runat="server"
                size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                cancelledvalue="" id="claimstatuscode" rmxref="/Instance/Claim/ClaimStatusCode"
                rmxtype="codewithdetail" tabindex="9" /><asp:textbox style="display: none" runat="server"
                    id="claimstatuscode_creatable" /><asp:button class="CodeLookupControl" runat="Server"
                        id="claimstatuscodebtn" onclientclick="return selectCode('CLAIM_STATUS','claimstatuscode');"
                        tabindex="10" /><asp:button class="button" runat="Server" id="claimstatuscodedetailbtn"
                            onclientclick="return selectCodeWithDetail('claimstatuscode',1);" tabindex="11"
                            text="Detail" /><asp:textbox style="display: none" runat="server" id="claimstatuscode_cid"
                                cancelledvalue="" oldvalue="" rmxref="/Instance/Claim/ClaimStatusCode/@codeid" /><asp:requiredfieldvalidator
                                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="claimstatuscode" /></span></div>
        <div runat="server" class="half" id="div_ev_datereported" xmlns="">
            <span class="label">Date Reported</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="date" onchange="setDataChanged(true);" id="ev_datereported"
                rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DateReported"
                rmxtype="date" tabindex="26" /><cc1:CalendarExtender runat="server" ID="ev_datereported_ajax"
                    TargetControlID="ev_datereported" />
            </span>
        </div>
        <div runat="server" class="half" id="div_statusdiaryclose" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="statusdiaryclose" rmxref="Instance/UI/FormVariables/SysExData/CloseDiary"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_containsopendiaries" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="containsopendiaries" rmxref="Instance/UI/FormVariables/SysExData/ContainsOpenDiaries"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_statuschangeapprovedby" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="statuschangeapprovedby" rmxref="/Instance/UI/FormVariables/SysExData/StatusApprovedBy"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_statuschangedate" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="statuschangedate" rmxref="/Instance/UI/FormVariables/SysExData/StatusDateChg"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_statuschangereason" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="statuschangereason" rmxref="/Instance/UI/FormVariables/SysExData/StatusReason"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_AllCodes" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="AllCodes" rmxref="Instance/UI/FormVariables/SysExData/AllCodes"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_deleteautocheck" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="deleteautocheck" rmxref="Instance/UI/FormVariables/SysExData/DeleteAutoCheck"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_containsautochecks" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="containsautochecks" rmxref="Instance/UI/FormVariables/SysExData/ContainsAutoChecks"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_initialclaimstatus" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="initialclaimstatus" rmxref="Instance/UI/FormVariables/SysExData/ClaimStatusCode"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_dttmclosed" xmlns="">
            <span class="label">Date Closed</span><span class="formw"><asp:textbox size="30"
                runat="Server" rmxref="/Instance/Claim/DttmClosed" id="dttmclosed" tabindex="12"
                style="background-color: silver;" readonly="true" /></span></div>
        <div runat="server" class="half" id="div_ev_timereported" xmlns="">
            <span class="label">Time Reported</span><span class="formw"><asp:textbox runat="server"
                size="30" formatas="time" onchange="setDataChanged(true);" id="ev_timereported"
                rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/TimeReported"
                rmxtype="time" tabindex="28" /><cc1:MaskedEditExtender runat="server" Enabled="True"
                    MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="ev_timereported"
                    ID="ev_timereported_ajax" />
                <cc1:MaskedEditValidator ValidationGroup="vgSave" runat="server" AcceptAMPM="true"
                    InvalidValueMessage="Time is invalid" Display="Dynamic" TooltipMessage="" ID="ev_timereported_ajaxvalidator"
                    ControlExtender="ev_timereported_ajax" ControlToValidate="ev_timereported" IsValidEmpty="True" /></span></div>
        <div runat="server" class="half" id="div_methodclosedcode" xmlns="">
            <span class="label">Close Method</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="methodclosedcode" rmxref="/Instance/Claim/MethodClosedCode"
                rmxtype="code" cancelledvalue="" tabindex="13" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="methodclosedcodebtn" onclientclick="return selectCode('CLOSE_METHOD','methodclosedcode');"
                    tabindex="14" /><asp:textbox style="display: none" runat="server" id="methodclosedcode_cid"
                        rmxref="/Instance/Claim/MethodClosedCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_estcollection" xmlns="">
            <span class="label">Est. Collection</span><span class="formw"><asp:textbox runat="server"
                size="30" id="estcollection" rmxref="/Instance/Claim/EstCollection" rmxtype="currency"
                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="29" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_currentadjuster" xmlns="">
            <span class="label">Current Adjuster</span><span class="formw"><asp:textbox size="30"
                runat="Server" rmxref="/Instance/UI/FormVariables/SysExData/CurrentAdjuster"
                id="currentadjuster" tabindex="15" style="background-color: silver;" readonly="true" /></span></div>
        <div runat="server" class="half" id="div_filenumber" xmlns="">
            <span class="label">File Number</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="setDataChanged(true);" id="filenumber" rmxref="/Instance/Claim/FileNumber"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_primarypolicyid" xmlns="">
            <span class="label">Policy Name</span><span class="formw"><asp:textbox runat="server"
                size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);"
                onkeydown="eatKeystrokes();" id="primarypolicyid" rmxref="/Instance/Claim/PrimaryPolicyId/@defaultdetail"
                rmxtype="policylookup" enabled="false" backcolor="Silver" /><input type="button"
                    class="button" value="..." name="primarypolicyidbtn" onclientclick="lookupClaimPolicy('primarypolicyid','')" /><asp:textbox
                        style="display: none" runat="server" id="primarypolicyid_cid" rmxref="/Instance/Claim/PrimaryPolicyId" /><input
                            type="button" class="button" id="primarypolicyid_open" value="Open" tabindex="18"
                            onclick="&#xA;                  XFormHandler('SysFormPIdName=claimid&SysFormName=policy&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','1','policylookup')&#xA;                " /></span></div>
        <div runat="server" class="half" id="div_servicecode" xmlns="">
            <span class="label">Service Code</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="servicecode" rmxref="/Instance/Claim/ServiceCode"
                rmxtype="code" cancelledvalue="" tabindex="31" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="servicecodebtn" onclientclick="return selectCode('SERVICE_CODE','servicecode');"
                    tabindex="32" /><asp:textbox style="display: none" runat="server" id="servicecode_cid"
                        rmxref="/Instance/Claim/ServiceCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_paymntfrozenflag" xmlns="">
            <span class="label">Payments Frozen</span><span class="formw"><asp:checkbox onchange="setDataChanged(true);"
                runat="Server" id="paymntfrozenflag" rmxref="/Instance/Claim/PaymntFrozenFlag"
                rmxtype="checkbox" tabindex="19" /></span></div>
        <div runat="server" class="half" id="div_lssclaimind" xmlns="">
            <span class="label">LSS Claim</span><span class="formw"><asp:checkbox onchange="setDataChanged(true);"
                runat="Server" id="lssclaimind" rmxref="/Instance/Claim/LssClaimInd" rmxtype="checkbox"
                tabindex="80" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABeventdetail"
        id="FORMTABeventdetail">
        <div runat="server" class="half" id="div_ev_addr1" xmlns="">
            <span class="label">Location Address 1</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="setDataChanged(true);" id="ev_addr1" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/Addr1"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_ev_onpremiseflag" xmlns="">
            <span class="label">Event On Premises</span><span class="formw"><asp:checkbox onchange="setDataChanged(true);"
                runat="Server" id="ev_onpremiseflag" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/OnPremiseFlag"
                rmxtype="checkbox" onclick="FillEventLocationDetails(this)" tabindex="46" /></span></div>
        <div runat="server" class="half" id="div_ev_addr2" xmlns="">
            <span class="label">Location Address 2</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="setDataChanged(true);" id="ev_addr2" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/Addr2"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_ev_primaryloccode" xmlns="">
            <span class="label">Primary Location</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="ev_primaryloccode" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/PrimaryLocCode"
                rmxtype="code" cancelledvalue="" tabindex="47" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="ev_primaryloccodebtn" onclientclick="return selectCode('PRIMARY_LOCATION','ev_primaryloccode');"
                    tabindex="48" /><asp:textbox style="display: none" runat="server" id="ev_primaryloccode_cid"
                        rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/PrimaryLocCode/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_ev_city" xmlns="">
            <span class="label">City</span><span class="formw"><asp:textbox runat="server" size="30"
                onchange="setDataChanged(true);" id="ev_city" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/City"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_ev_locationtypecode" xmlns="">
            <span class="label">Location Type</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="ev_locationtypecode" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationTypeCode"
                rmxtype="code" cancelledvalue="" tabindex="49" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="ev_locationtypecodebtn" onclientclick="return selectCode('LOCATION_TYPE','ev_locationtypecode');"
                    tabindex="50" /><asp:textbox style="display: none" runat="server" id="ev_locationtypecode_cid"
                        rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationTypeCode/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_ev_stateid" xmlns="">
            <span class="label">State</span><span class="formw"><asp:textbox runat="server" size="30"
                onchange="lookupTextChanged(this);" id="ev_stateid" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/StateId"
                rmxtype="code" cancelledvalue="" tabindex="36" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="ev_stateidbtn" onclientclick="return selectCode('states','ev_stateid');"
                    tabindex="37" /><asp:textbox style="display: none" runat="server" id="ev_stateid_cid"
                        rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/StateId/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_ev_causecode" xmlns="">
            <span class="label">Cause Code</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="ev_causecode" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CauseCode"
                rmxtype="code" cancelledvalue="" tabindex="51" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="ev_causecodebtn" onclientclick="return selectCode('CAUSE_CODE','ev_causecode');"
                    tabindex="52" /><asp:textbox style="display: none" runat="server" id="ev_causecode_cid"
                        rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CauseCode/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_ev_zipcode" xmlns="">
            <span class="label">Zip</span><span class="formw"><asp:textbox runat="server" size="30"
                onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="ev_zipcode"
                rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/ZipCode" rmxtype="zip"
                tabindex="38" /></span></div>
        <div runat="server" class="half" id="div_ev_noofinjuries" xmlns="">
            <span class="label">Number of Injuries</span><span class="formw"><asp:textbox runat="server"
                size="30" onblur="numLostFocus(this);" id="ev_noofinjuries" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/NoOfInjuries"
                rmxtype="numeric" tabindex="53" min="0" onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_ev_countyofinjury" xmlns="">
            <span class="label">County of Injury</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="setDataChanged(true);" id="ev_countyofinjury" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountyOfInjury"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_ev_nooffatalities" xmlns="">
            <span class="label">Number of Fatalities</span><span class="formw"><asp:textbox runat="server"
                size="30" onblur="numLostFocus(this);" id="ev_nooffatalities" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/NoOfFatalities"
                rmxtype="numeric" tabindex="54" min="0" onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_ev_countrycode" xmlns="">
            <span class="label">Country</span><span class="formw"><asp:textbox runat="server"
                size="30" onchange="lookupTextChanged(this);" id="ev_countrycode" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountryCode"
                rmxtype="code" cancelledvalue="" tabindex="40" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="ev_countrycodebtn" onclientclick="return selectCode('COUNTRY','ev_countrycode');"
                    tabindex="41" /><asp:textbox style="display: none" runat="server" id="ev_countrycode_cid"
                        rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountryCode/@codeid"
                        cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_ev_locationareadesc" xmlns="">
            <span class="label">Location Description</span><span class="formw"><asp:textbox runat="Server"
                id="ev_locationareadesc" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationAreaDesc"
                rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                tabindex="42" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                    value="..." name="ev_locationareadescbtnMemo" tabindex="43" id="ev_locationareadescbtnMemo"
                    onclick=" EditHTMLMemo('ev_locationareadesc','yes')" /><asp:textbox style="display: none"
                        runat="server" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationAreaDesc_HTMLComments"
                        id="ev_locationareadesc_HTML" /></span></div>
        <div runat="server" class="half" id="div_ev_eventdescription" xmlns="">
            <span class="label">Event Description</span><span class="formw"><asp:textbox runat="Server"
                id="ev_eventdescription" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventDescription"
                rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                tabindex="55" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                    value="..." name="ev_eventdescriptionbtnMemo" tabindex="56" id="ev_eventdescriptionbtnMemo"
                    onclick=" EditHTMLMemo('ev_eventdescription','yes')" /><asp:textbox style="display: none"
                        runat="server" rmxref="Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventDescription_HTMLComments"
                        id="ev_eventdescription_HTML" /></span></div>
        <div runat="server" class="half" id="div_dupeoverride" xmlns="">
            <span class="formw">
                <asp:textbox style="display: none" runat="server" id="dupeoverride" rmxref="Instance/UI/FormVariables/SysExData/dupeoverride"
                    rmxtype="hidden" /></span></div>
        <div runat="server" class="half" id="div_PoliceAgencyEid" xmlns="">
            <span class="label">Authority Contacted</span><span class="formw"><asp:textbox runat="server"
                size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                id="PoliceAgencyEid" rmxref="Instance/Claim/PoliceAgencyEid" rmxtype="eidlookup"
                cancelledvalue="" tabindex="44" /><input type="button" class="button" value="..."
                    name="PoliceAgencyEidbtn" tabindex="45" onclick="lookupData('PoliceAgencyEid','POLICE_AGENCY',4,'PoliceAgencyEid',2)" /><asp:textbox
                        style="display: none" runat="server" id="PoliceAgencyEid_cid" rmxref="Instance/Claim/PoliceAgencyEid/@codeid"
                        cancelledvalue="" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_claim_id" rmxref="/Instance/*/Supplementals/CLAIM_ID"
            rmxtype="id" /><div runat="server" class="half" id="div_supp_empl_enrl_mco_code"
                xmlns="">
                <span class="label">Employer MCO Enrolled</span><span class="formw"><asp:textbox
                    runat="server" size="30" onchange="lookupTextChanged(this);" id="supp_empl_enrl_mco_code"
                    rmxref="/Instance/*/Supplementals/EMPL_ENRL_MCO_CODE" rmxtype="code" cancelledvalue=""
                    tabindex="1896" onblur="codeLostFocus(this.id);" /><asp:button class="CodeLookupControl"
                        runat="Server" id="supp_empl_enrl_mco_codebtn" onclientclick="return selectCode('YES_NO','supp_empl_enrl_mco_code');"
                        tabindex="1897" /><asp:textbox style="display: none" runat="server" id="supp_empl_enrl_mco_code_cid"
                            rmxref="/Instance/*/Supplementals/EMPL_ENRL_MCO_CODE/@codeid" cancelledvalue="" /></span></div>
        <asp:textbox style="display: none" runat="server" name="supp_empl_enrl_mco_code_GroupAssoc"
            id="supp_empl_enrl_mco_code_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" /><div
                runat="server" class="half" id="div_supp_typeofreport_code" xmlns="">
                <span class="label">Type Of Report</span><span class="formw"><asp:textbox runat="server"
                    size="30" onchange="lookupTextChanged(this);" id="supp_typeofreport_code" rmxref="/Instance/*/Supplementals/TYPEOFREPORT_CODE"
                    rmxtype="code" cancelledvalue="" tabindex="1897" onblur="codeLostFocus(this.id);" /><asp:button
                        class="CodeLookupControl" runat="Server" id="supp_typeofreport_codebtn" onclientclick="return selectCode('GA_REPORT_TYP_CODE','supp_typeofreport_code');"
                        tabindex="1898" /><asp:textbox style="display: none" runat="server" id="supp_typeofreport_code_cid"
                            rmxref="/Instance/*/Supplementals/TYPEOFREPORT_CODE/@codeid" cancelledvalue="" /></span></div>
        <asp:textbox style="display: none" runat="server" name="supp_typeofreport_code_GroupAssoc"
            id="supp_typeofreport_code_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" /><div
                runat="server" class="half" id="div_supp_able_rtw_date" xmlns="">
                <span class="label">Date Able to RTW</span><span class="formw"><asp:textbox runat="server"
                    size="30" formatas="date" onchange="setDataChanged(true);" id="supp_able_rtw_date"
                    rmxref="/Instance/*/Supplementals/ABLE_RTW_DATE" rmxtype="date" tabindex="1898" /><cc1:CalendarExtender
                        runat="server" ID="supp_able_rtw_date_ajax" TargetControlID="supp_able_rtw_date" />
                </span>
            </div>
        <asp:textbox style="display: none" runat="server" name="supp_able_rtw_date_GroupAssoc"
            id="supp_able_rtw_date_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" /><div runat="server"
                class="half" id="div_supp_perm_loss_numb" xmlns="">
                <span class="label">Permanent Percent Loss</span><span class="formw"><asp:textbox
                    runat="server" size="30" onblur="numLostFocus(this);" id="supp_perm_loss_numb"
                    rmxref="/Instance/*/Supplementals/PERM_LOSS_NUMB" rmxtype="numeric" tabindex="1899"
                    onchange="setDataChanged(true);" /></span></div>
        <asp:textbox style="display: none" runat="server" name="supp_perm_loss_numb_GroupAssoc"
            id="supp_perm_loss_numb_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" /><div runat="server"
                class="half" id="div_supp_perm_loss_text" xmlns="">
                <span class="label">Permanent Body Part Loss</span><span class="formw"><asp:textbox
                    runat="server" size="30" onchange="setDataChanged(true);" id="supp_perm_loss_text"
                    rmxref="/Instance/*/Supplementals/PERM_LOSS_TEXT" rmxtype="text" /></span></div>
        <asp:textbox style="display: none" runat="server" name="supp_perm_loss_text_GroupAssoc"
            id="supp_perm_loss_text_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" /></div>
    <div id="Div3" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnClaimant">
            <asp:button id="Button1" class="button" runat="Server" text="Claimants" onclientclick="&#xA;            XFormHandler('','SysFormPIdName=claimid&SysFormName=claimantlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=claimantrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;          " /></div>
        <div class="formButton" runat="server" id="div_btnAdjusters">
            <asp:button id="Button2" class="button" runat="Server" text="Adjusters" onclientclick="&#xA;            XFormHandler('','SysFormPIdName=claimid&SysFormName=adjuster&SysCmd=1&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;          " /></div>
        <div class="formButton" runat="server" id="div_btnPI">
            <asp:button id="Button3" class="button" runat="Server" text="Persons Involved" onclientclick="&#xA;            XFormHandler('','SysFormName=personinvolvedlist&SysCmd=1&SysViewType=controlsonly&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber | /Instance/Claim/EventId | /Instance/Claim/EventNumber','','')&#xA;          " /></div>
        <div class="formButton" runat="server" id="div_btnLitigation">
            <asp:button id="Button4" class="button" runat="Server" text="Litigation" onclientclick="&#xA;            XFormHandler('','SysFormPIdName=claimid&SysFormName=litigation&SysCmd=1&SysFormIdName=litigationrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;          " /></div>
        <div class="formButton" runat="server" id="div_btnDefendant">
            <asp:button id="Button5" class="button" runat="Server" text="Defendant" onclientclick="&#xA;            XFormHandler('','SysFormPIdName=claimid&SysFormName=defendant&SysCmd=1&SysFormIdName=defendantrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;          " /></div>
        <div class="formButton" runat="server" id="div_btnFinancials">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnFinancials" rmxref="" text="Financials"
                onclientclick="gotoreserves();" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormId"
            rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
                    rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                        style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                            rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                                style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                                rmxtype="hidden" text="Claim" /><asp:textbox style="display: none" runat="server"
                                    id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                                    rmxtype="hidden" name="SysSerializationConfig" text="&lt;Claim&gt;&lt;CurrentAdjuster&gt;&lt;/CurrentAdjuster&gt;&lt;PrimaryClaimant&gt;&lt;/PrimaryClaimant&gt;&lt;Parent&gt;&lt;/Parent&gt;&lt;Jurisdictionals&gt;&lt;/Jurisdictionals&gt;&lt;Acord&gt;&lt;/Acord&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Claim&gt;" /><asp:textbox
                                        style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                        rmxtype="hidden" text="claimgc" /><asp:textbox style="display: none" runat="server"
                                            id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName" rmxtype="hidden"
                                            text="claimid" /><asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                rmxtype="hidden" text="150" /><asp:textbox style="display: none" runat="server" id="SysEx"
                                                    rmxref="Instance/UI/FormVariables/SysEx" rmxtype="hidden" text="" /><asp:textbox
                                                        style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName"
                                                            rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden" text="eventid" /><asp:textbox
                                                                style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                                                                rmxtype="hidden" text="event" /><asp:textbox style="display: none" runat="server"
                                                                    id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden"
                                                                    text="claimnumber|ev_eventnumber|ev_timeofevent|timeofclaim" /><asp:textbox style="display: none"
                                                                        runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden"
                                                                        text="" /><asp:textbox style="display: none" runat="server" id="SysLOB" rmxref="Instance/UI/FormVariables/SysLOB"
                                                                            rmxtype="hidden" text="241" /><asp:textbox style="display: none" runat="server" id="line_of_bus_code"
                                                                                rmxref="Instance/UI/FormVariables/SysExData/MissingRef" rmxtype="hidden" text="" /><asp:textbox
                                                                                    style="display: none" runat="server" id="OH_FACILITY_EID" rmxref="Instance/UI/FormVariables/SysExData/OH_FACILITY_EID"
                                                                                    rmxtype="hidden" text="0" /><asp:textbox style="display: none" runat="server" id="OH_LOCATION_EID"
                                                                                        rmxref="Instance/UI/FormVariables/SysExData/OH_LOCATION_EID" rmxtype="hidden"
                                                                                        text="0" /><asp:textbox style="display: none" runat="server" id="OH_DIVISION_EID"
                                                                                            rmxref="Instance/UI/FormVariables/SysExData/OH_DIVISION_EID" rmxtype="hidden"
                                                                                            text="0" /><asp:textbox style="display: none" runat="server" id="OH_REGION_EID" rmxref="Instance/UI/FormVariables/SysExData/OH_REGION_EID"
                                                                                                rmxtype="hidden" text="0" /><asp:textbox style="display: none" runat="server" id="OH_OPERATION_EID"
                                                                                                    rmxref="Instance/UI/FormVariables/SysExData/OH_OPERATION_EID" rmxtype="hidden"
                                                                                                    text="0" /><asp:textbox style="display: none" runat="server" id="OH_COMPANY_EID"
                                                                                                        rmxref="Instance/UI/FormVariables/SysExData/OH_COMPANY_EID" rmxtype="hidden"
                                                                                                        text="0" /><asp:textbox style="display: none" runat="server" id="OH_CLIENT_EID" rmxref="Instance/UI/FormVariables/SysExData/OH_CLIENT_EID"
                                                                                                            rmxtype="hidden" text="0" /><asp:textbox style="display: none" runat="server" name="formname"
                                                                                                                value="claimgc" /><asp:textbox style="display: none" runat="server" name="SysRequired"
                                                                                                                    value="ev_eventnumber|ev_dateofevent|claimnumber|ev_timeofevent|claimtypecode_cid|dateofclaim|ev_depteid_cid|timeofclaim|claimstatuscode|" /><asp:textbox
                                                                                                                        style="display: none" runat="server" name="SysFocusFields" value="ev_eventnumber|ev_addr1|" /><input
                                                                                                                            type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                                                                style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                                                                    runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                                                        id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                                                            id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                                                                id="SysWindowId" /></form>
</body>
</html>
