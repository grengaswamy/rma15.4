﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
namespace Riskmaster.UI.FDM
{
    public partial class Event : FDMBasePage
    {
        /*
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                string sRawPath = VirtualPathUtility.GetDirectory(Request.RawUrl)
                    + VirtualPathUtility.GetFileName(Request.RawUrl);
                Context.RewritePath(sRawPath);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        } 
        */

        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            //JIRA 1342 ajohari2: Start
            
            if (!IsPostBack)
            {

                //JIRA 1342 ajohari2: Start
                Control hdndateofevent = this.Form.FindControl("hdndateofevent");
                Control dateofevent = this.Form.FindControl("dateofevent");

                if (hdndateofevent != null && dateofevent != null)
                {
                    ((TextBox)hdndateofevent).Text = ((TextBox)dateofevent).Text;
                }
                //JIRA 1342 ajohari2: End
            }
            //JIRA 1342 ajohari2: End
        }


    }
}