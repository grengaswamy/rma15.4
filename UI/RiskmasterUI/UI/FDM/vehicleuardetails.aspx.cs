﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class vehicleuardetails : GridPopupBase
    {
        private const string m_FUNCTION_CALL_POPULATE_UAR = "EnhancePolicyAdaptor.PopulateSearchVehUAR";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Sumit - Start(03/16/2010) - MITS# 18229 - Updated flow on postback
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            string sreturnValue = string.Empty;
            XmlDocument objVehicleUAR = null;

            //Sumit: Start(10/19/2010) - MITS# 21996
            int iUnitFieldID = 0;
            if (AppHelper.GetQueryStringValue("UnitFieldID") != "")
                iUnitFieldID = Int32.Parse(AppHelper.GetQueryStringValue("UnitFieldID"));

            HtmlInputButton btnvehicleidcode = (HtmlInputButton)this.FindControl("vehicleidcodebtn");
            if (btnvehicleidcode != null)
                btnvehicleidcode.Attributes.Add("onclick", "return searchUAR('vehicle','" + iUnitFieldID.ToString() + "')");
            //End:Sumit

            if (!IsPostBack)
            {
                int iUARId = 0;
                if (AppHelper.GetQueryStringValue("selectedid") != "")
                    iUARId = Int32.Parse(AppHelper.GetQueryStringValue("selectedid"));

                string sSessionId = string.Empty;
                if (AppHelper.GetQueryStringValue("sessionid") != "")
                    sSessionId = AppHelper.GetQueryStringValue("sessionid");

                TextBox txtGridName = (TextBox)this.FindControl("gridname");
                if (txtGridName != null)
                {
                    txtGridName.Text = AppHelper.GetQueryStringValue("gridname");
                }

                TextBox txtmode = (TextBox)this.FindControl("mode");
                if (txtmode != null)
                {
                    txtmode.Text = AppHelper.GetQueryStringValue("mode");
                }

                TextBox txtselectedrowposition = (TextBox)this.FindControl("selectedrowposition");
                if (txtselectedrowposition != null)
                {
                    txtselectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                }

                TextBox txtUARId = (TextBox)this.FindControl("selectedid");
                if (txtUARId != null)
                {
                    txtUARId.Text = iUARId.ToString();
                }

                string shdnCodeId = string.Empty;
                if (AppHelper.GetQueryStringValue("CodeID") != "")
                {
                    shdnCodeId = AppHelper.GetQueryStringValue("CodeID");
                    hdnCodeID.Text = shdnCodeId;
                }

                //Sumit - End
            }
            else
            {
                TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");
                if (txtFunctionToCall.Text == m_FUNCTION_CALL_POPULATE_UAR)
                {
                    XmlTemplate = null;
                    bReturnStatus = CallCWS(m_FUNCTION_CALL_POPULATE_UAR, XmlTemplate, out sreturnValue, false, true);

                    
                }
                
                //Sumit - Start(03/16/2010) - MITS# 18229 - Enable OK Button
                TextBox txthdnOkEnabled = (TextBox)this.FindControl("hdnOkEnabled");
                if (txthdnOkEnabled != null)
                {
                    txthdnOkEnabled.Text = "true";
                }
                //Sumit - End
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            TextBox txtFunctionToCall = (TextBox)this.FindControl("FunctionToCall");

            if (txtFunctionToCall.Text == m_FUNCTION_CALL_POPULATE_UAR)
            {
                XElement xmlVehicleUAR = Xelement.XPathSelectElement("./Document");
                XElement xmlTarget = new XElement("UnitID");

                TextBox txtVehicleID = (TextBox)this.FindControl("UnitID");
                string sVehicleID = txtVehicleID.Text;

                xmlTarget.Value = sVehicleID;

                if (xmlVehicleUAR != null)
                    xmlVehicleUAR.Add(xmlTarget);
                //rkaur27 : JIRA RMA-9178:Start
                XElement xmlVehicleUARId = Xelement.XPathSelectElement("./Document/PolicyXUar");
                if (xmlVehicleUARId != null)
                    Xelement.XPathSelectElement("./Document/PolicyXUar/UarId").Value = UnitID.Text;
                //rkaur27 : JIRA RMA-9178:End
            }
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 03/16/2010
        /// MITS : 18229
        /// Description : This function will set the Session ID and refresh the parent page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUAROk_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            TextBox txtSessionId;
            bReturnStatus = CallCWS("EnhancePolicyAdaptor.SetUAR", XmlTemplate, out sreturnValue, true, false);
            if (bReturnStatus)
            {
                // Set the Session Id
                XmlDoc.LoadXml(sreturnValue);
                txtSessionId = (TextBox)this.FindControl("SessionId");
                txtSessionId.Text = XmlDoc.SelectSingleNode("//Document/SetUAR/SessionId").InnerText;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "UAROkClick", "<script>fnRefreshParentUAROk();</script>");
            }
        }
        //Sumit - End
    }
}
