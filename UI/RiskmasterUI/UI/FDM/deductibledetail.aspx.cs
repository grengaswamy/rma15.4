﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.ServiceHelpers;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using MultiCurrencyCustomControl;

namespace Riskmaster.UI.FDM
{
    public partial class Deductibledetail : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string sDedId = string.Empty;
            string sCvgId = string.Empty;
            string sClaimId = string.Empty;
          
           
            // In case of Diminshing Data we do not need FDMPageLoad
            // We need to call the policy service from here. So instead of from the Adaptor we call service if we ned to
            if (IsPostBack)
            {
                //if (AppHelper.GetQueryStringValue("DedId") != null)
                //{
                //    sDedId = AppHelper.GetQueryStringValue("DedId");
                //  //  AssignControlValue("claimxpoldedid", sDedId);
                //}
                //if (AppHelper.GetQueryStringValue("CvgId") != null)
                //{
                //    sCvgId = AppHelper.GetQueryStringValue("CvgId");
                //    AssignControlValue("polcvgrowid", sCvgId);
                //}
                //if (Request.QueryString["ClaimId"] != null)
                //{
                //    sClaimId = AppHelper.GetQueryStringValue("ClaimId");
                //    AssignControlValue("claimid", sClaimId);

                //}
                TextBox txtPostBackAction = (TextBox) this.FindControl("PostBackAction");

                if (txtPostBackAction != null && string.Compare(txtPostBackAction.Text, "LoadDiminishingData", true) == 0)
                {
                    LoadDiminishingData();
                }
                if (txtPostBackAction != null && string.Compare(txtPostBackAction.Text, "LoadDeductibleData", true) == 0)
                {
                    FDMPageLoad();
                    LoadDeductibleData();
                }
               
                else
                    FDMPageLoad();
            }
            else
            {
                TextBox ClaimantListClaimantEID = (TextBox)Page.FindControl("ClaimantListClaimantEID");
                if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimantListClaimantEID")))
                    ClaimantListClaimantEID.Text = AppHelper.GetQueryStringValue("ClaimantListClaimantEID");
                FDMPageLoad();
            }
        }

        private void LoadDiminishingData()
        {
            PolicyEnquiry oRequest = null;
            PolicyInterfaceServiceHelper interfaceHelper = null;
            XElement xReturnDoc = null;
            XElement xCvgEvaluationDate = null;
            XElement xDimEvalDate = null;
            XElement xDimPercent = null;
            XElement xReducedAmt = null;
            XElement xRemainingAmt = null;
            bool bSuccess = false;

            oRequest = new PolicyEnquiry();
            interfaceHelper = new PolicyInterfaceServiceHelper();

            oRequest.Token = AppHelper.GetSessionId();
            xReturnDoc = interfaceHelper.GetDimEvalDate(GetMessageTemplate(), oRequest);

            xCvgEvaluationDate = xReturnDoc.XPathSelectElement("//control[@name='CoverageEvaluationDate']");
            xDimEvalDate = xReturnDoc.XPathSelectElement("//control[@name='DiminishingEvaluationDate']");
            xDimPercent = xReturnDoc.XPathSelectElement("//control[@name='DiminishingPercentage']");
            xReducedAmt = xReturnDoc.XPathSelectElement("//control[@name='RemainingAmt']");
            xRemainingAmt = xReturnDoc.XPathSelectElement("//control[@name='ReducedAmt']");

            if (xCvgEvaluationDate != null)
            {
                AssignControlValue("cvgevaluationdate", xCvgEvaluationDate.Value);
            }
            if (xDimEvalDate != null)
            {
                AssignControlValue("dimevaluationdate", xDimEvalDate.Value);
            }
            if (xDimPercent != null)
            {
                AssignControlValue("diminishingpercent", xDimPercent.Value);
               // TODO Enable control
                Control ctlDiminishingPercent = this.Form.FindControl("diminishingpercent");

                if (ctlDiminishingPercent != null)
                {
                    DatabindingHelper.EnableControl(ctlDiminishingPercent);
                }
            }
            if (xReducedAmt != null)
            {
                if (!string.IsNullOrEmpty(xReducedAmt.Value))
                {
                    if (string.Compare(xReducedAmt.Value, "NA") == 0)
                    {
                        AssignControlValue("dimreducedamount", xReducedAmt.Value);
                    }
                    else
                    {
                        // TODO Assign Formatting
                        AssignControlValue("dimreducedamount", xReducedAmt.Value);
                        //txtReducedAmt.Text = string.Format("{0:c}", Conversion.CastToType<double>(xReducedAmt.Value, out bSuccess));
                    }
                }

            }
            if (xRemainingAmt != null)
            {
                AssignControlValue("remainingamount", xRemainingAmt.Value);
                // TODO Assign Formatting
                //txtRemainingAmt.Amount = Conversion.CastToType<decimal>(xRemainingAmt.Value, out bSuccess);

            }

           // TODO Need to upodate the value of Prev Diminishing 
            // prevdiminishingtypecode.Text = DiminishingTypeCode.CodeId;
            string sCurrDimTypeId = string.Empty;
            GetControlValue("diminishingtype", "Code", ref sCurrDimTypeId);
            AssignControlValue("prevdiminishingtypecode", sCurrDimTypeId);

        }

        private void LoadDeductibleData()
        {
            double dblCurrentDiminishingAmount = 0d;
            string sDedTpeCodeId = string.Empty;
            string sCurrentDiminishingCode = string.Empty;
            string sCurrentDiminishingCodeDesc = string.Empty;
            string sCurrentDiminishingAmount = string.Empty;
            string sCurrentCvgEvalDate = string.Empty;
            string sCurrentDimEvalDate = string.Empty;

            GetControlValue("deductibletype", "Code", ref sDedTpeCodeId);

            if (string.Compare(GetControlValue("firstparty"), sDedTpeCodeId) != 0)
            {
                // Store the current values in variables
                sCurrentDiminishingCodeDesc = GetControlValue("diminishingtype", "Code", ref sCurrentDiminishingCode);
                sCurrentDiminishingAmount = GetControlValue("diminishingpercent");
                sCurrentCvgEvalDate = GetControlValue("cvgevaluationdate");
                sCurrentDimEvalDate = GetControlValue("dimevaluationdate");

                // Assign the values to controls
                AssignControlValue("diminishingtype", "", "Code", "0");
                AssignControlValue("diminishingpercent", "0");
                AssignControlValue("cvgevaluationdate", "");
                AssignControlValue("dimevaluationdate", "");

                // Assign the original values to controls to persist them
                AssignControlValue("prevdiminishingtypecode", sCurrentDiminishingCode);
                AssignControlValue("prevdiminishingtypecodedesc", sCurrentDiminishingCodeDesc);
                AssignControlValue("prevdiminishingpercent", sCurrentDiminishingAmount);
                AssignControlValue("prevcvgevaluationdate", sCurrentCvgEvalDate);
                AssignControlValue("prevdimevaluationdate", sCurrentDimEvalDate);
            }
            else
            {
                sCurrentDiminishingCode = GetControlValue("prevdiminishingtypecode");
                sCurrentDiminishingCodeDesc = GetControlValue("prevdiminishingtypecodedesc");
                sCurrentDiminishingAmount = GetControlValue("prevdiminishingpercent");
                sCurrentCvgEvalDate = GetControlValue("prevcvgevaluationdate");
                sCurrentDimEvalDate = GetControlValue("prevdimevaluationdate");

                AssignControlValue("diminishingtype", sCurrentDiminishingCodeDesc, "Code", sCurrentDiminishingCode);

                if (Double.TryParse(sCurrentDiminishingAmount, out dblCurrentDiminishingAmount))
                    AssignControlValue("diminishingpercent", sCurrentDiminishingAmount);
                else
                    AssignControlValue("diminishingpercent", "0");
                AssignControlValue("cvgevaluationdate", sCurrentCvgEvalDate);
                AssignControlValue("dimevaluationdate", sCurrentDimEvalDate);
            }
        }
        //private void LoadDeductibleData()
        //{
            
        //    string sCWSresponse = string.Empty;
        //    string sDedTpeCodeId = string.Empty;
        //    string sDimTypeId = string.Empty;
        //    string sDimTypeText = string.Empty;
          
        //    GetControlValue("DedTypeCode", "Code", ref sDedTpeCodeId);
        // sDimTypeId=   GetControlValue("prevdiminishingtypecode");
        // sDimTypeText=   GetControlValue("prevdiminishingtypecodedesc");
          
        //    if (string.Compare(GetControlValue("firstparty"), sDedTpeCodeId) == 0)
        //    {

               
        //        AssignControlValue("diminishingtype", sDimTypeId);
        //      //  DiminishingTypeCode.CodeId = hdn_DimTypeCode.Text;
        //      //  DiminishingTypeCode.CodeText = hdn_DimTypeText.Text;
        //       // txtDiminishingPercentage.Text = hdn_DimPercent.Text;
        //        //txtCvgEvalDate.Text = hdn_CvgEvalDate.Text;
        //      //  txtDimEvalDate.Text = hdn_DimEvalDate.Text;
        //      //  txtCovGroupId.Text = "NA";
        //       // txtDeductibleAmt.ReadOnly = false;
        //       // txtDeductibleAmt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");

        //    }
        //    else
        //    {
        //     sDimTypeText=   GetControlValue("diminishingtype", "Code", ref sDimTypeId);
        //     AssignControlValue("prevdiminishingtypecodedesc", sDimTypeText);
           

        //     ((TextBox)this.Page.FindControl("diminishingtype").FindControl("codelookup_cid")).Text = string.Empty;
        //     ((TextBox)this.Page.FindControl("diminishingtype").FindControl("codelookup")).Text = string.Empty;
        //     AssignControlValue("diminishingpercent", string.Empty);
        //     AssignControlValue("cvgevaluationdate", string.Empty);
        //     AssignControlValue("dimevaluationdate", string.Empty);

        //     TextBox shdnCovgrp = ((TextBox)Page.FindControl("coveragegroup"));
        //      if (string.Compare(GetControlValue("thirdparty"), sDedTpeCodeId) == 0 && !String.IsNullOrEmpty(shdnCovgrp.Text))
        //        {
        //            AssignControlValue("coveragegroupid", shdnCovgrp.Text);
        //           // txtCovGroupId.Text = shdnCovgrp.Text;
        //        }
        //        else
        //        {
        //            AssignControlValue("coveragegroupid",string.Empty);
        //        }
        //        //PCR changes by Nikhil on 17/12/2013 for retaining Coverage group - End
               
        //      if (!string.IsNullOrEmpty(shdnCovgrp.Text))
        //        {
                                 
        //            if (shdnCovgrp.Attributes["IsDedPerEventEnabled"] != null)
        //            {
        //                if (string.Compare(shdnCovgrp.Attributes["IsDedPerEventEnabled"], "-1") == 0)
        //                {
        //                    ((TextBox)Page.FindControl("diminishingpercent")).ReadOnly = true;
        //                    ((TextBox)Page.FindControl("dedamount")).Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
        //                    bool bSuccess = false;

                         
        //                   // CallCWS("DeductibleManagementAdaptor.LoadThirdPartyData", null, out sCWSresponse, true, true);
        //                }
        //                else
        //                {
        //                    ((TextBox)Page.FindControl("dedamount")).ReadOnly = false;
        //                }
        //            }
        //            //End: added by Nitin goel, For PCRs changes.

        //        }

        //    }


        //}

        public override void ModifyControls()
        {
            base.ModifyControls();


        }
        /// <summary>
        /// GetMessageTemplate
        /// </summary>
        /// <param name=""></param>
        private XElement GetMessageTemplate()
        {
            TextBox ClaimId = null;
            TextBox DeductibleId = null;
            string sDiminishingTypeCodeId = string.Empty;
            string sDiminishingTypeCode = string.Empty;
            string sDeductibleAmt = string.Empty;
            string sRemainingAmt = string.Empty;
            string sDiminishingPercentage = string.Empty;

            XElement oTemplate = null;
            try
            {
                ClaimId = (TextBox)this.FindControl("claimid");
                DeductibleId = (TextBox)this.FindControl("claimxpoldedid");
                sDiminishingTypeCode = GetControlValue("diminishingtype", "Code", ref  sDiminishingTypeCodeId);
                sDeductibleAmt = GetControlValue("dedsiramount");
                sRemainingAmt = GetControlValue("remainingamount");
                sDiminishingPercentage = GetControlValue("diminishingpercent");

                oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Doc>
                        <ClaimId>" + ClaimId.Text + @"</ClaimId>
                        <CLM_X_POL_DED_ID>" + DeductibleId.Text + @"</CLM_X_POL_DED_ID>
                        <control name=""DiminishingTypeCode"" codeid=""" + sDiminishingTypeCodeId + @""">" + sDiminishingTypeCode + @"</control>
                        <control name=""SirDedAmt"">" + sDeductibleAmt + @"</control>
                        <control name=""ReducedAmt"">" + sDeductibleAmt + @"</control>
                        <control name=""RemainingAmt"">" + sRemainingAmt + @"</control>
                        <control name=""DiminishingPercentage"">" + sDiminishingPercentage + @"</control>
                        <control name=""CoverageEvaluationDate"">" + @"</control>
                        <control name=""DiminishingEvaluationDate"">" + @"</control>
                    </Doc>
                </Document>
            </Message>
            ");
            }
            catch (Exception ex)
            {
                
                throw;
            }
            

            return oTemplate;
        }

        private string GetControlValue(string sCtrlName)
        {
            string sDummy = string.Empty;
            return GetControlValue(sCtrlName, string.Empty, ref sDummy);
        }
        private string GetControlValue(string sCtrlName, string sCtrlType, ref string p_sCodeValue)
        {
            string sCtrlValue = string.Empty;
            if (sCtrlType == "Code")
            {
                if (this.Page.FindControl(sCtrlName) != null)
                {
                    // Page is readonly
                    if (this.Page.FindControl(sCtrlName).GetType().Name == "Label")
                    {
                        p_sCodeValue = ((TextBox)this.Page.FindControl(sCtrlName + "_cid")).Text;
                        sCtrlValue = ((Label)this.Page.FindControl(sCtrlName)).Text;
                    }
                    // Page is not Readonly
                    else
                    {
                        p_sCodeValue = ((TextBox)this.Page.FindControl(sCtrlName).FindControl("codelookup_cid")).Text;
                        sCtrlValue = ((TextBox)this.Page.FindControl(sCtrlName).FindControl("codelookup")).Text;
                    }
                }
            }
            else
            {
                if (this.Page.FindControl(sCtrlName) != null)
                {
                    // Page is readonly
                    if (this.Page.FindControl(sCtrlName).GetType().Name == "Label")
                    {
                        sCtrlValue = ((Label)this.Page.FindControl(sCtrlName)).Text;
                    }
                    // Page is not Readonly
                    else   if (this.Page.FindControl(sCtrlName).GetType().Name == "CurrencyTextbox")
                    {
                        sCtrlValue = ((CurrencyTextbox)this.Page.FindControl(sCtrlName)).Text;
                    }
                    else
                    {
                        sCtrlValue = ((TextBox)this.Page.FindControl(sCtrlName)).Text;
                    }
                }
            }
            return sCtrlValue;
        }

        private void AssignControlValue(string sCtrlName, string sCtrlValue, string sCtrlType = "", string sCtrlIdValue = "")
        {
            if (sCtrlType == "Code")
            {
                if (this.Page.FindControl(sCtrlName) != null)
                {
                    // Page is readonly
                    if (this.Page.FindControl(sCtrlName).GetType().Name == "Label")
                    {
                        ((TextBox)this.Page.FindControl(sCtrlName + "_cid")).Text = sCtrlIdValue;
                        sCtrlValue = ((Label)this.Page.FindControl(sCtrlName)).Text = sCtrlValue;
                    }
                    // Page is not Readonly
                    else
                    {
                        ((TextBox)this.Page.FindControl(sCtrlName).FindControl("codelookup_cid")).Text = sCtrlIdValue;
                        ((TextBox)this.Page.FindControl(sCtrlName).FindControl("codelookup")).Text = sCtrlValue;
                    }
                }
            }
            else
            {
                if (this.Page.FindControl(sCtrlName) != null)
                {
                    // Page is readonly
                    if (this.Page.FindControl(sCtrlName).GetType().Name == "Label")
                    {
                        ((Label)this.Page.FindControl(sCtrlName)).Text = sCtrlValue;
                    }
                    // Page is not Readonly
                    else if (this.Page.FindControl(sCtrlName).GetType().Name == "CurrencyTextbox")
                    {
                        sCtrlValue = ((CurrencyTextbox)this.Page.FindControl(sCtrlName)).Text;
                    }
                    else
                    {
                        ((TextBox)this.Page.FindControl(sCtrlName)).Text = sCtrlValue;
                    }
                }
            }
        }
    }
}