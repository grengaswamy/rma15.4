﻿/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 33574   | pgupta93   | Changes req for Continuous Trigger and Policy Search func
**********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.UI.FDM
{
    public partial class Claimgc : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            //MITS:33574 START

            if (!IsPostBack)
            {
                int iClaimId = 0;//bkuzhanthaim : RMA-12187/RMA-12152
                //Ashish Ahuja: Claims Made Jira 1342
                Control dateofclaim = this.Form.FindControl("dateofclaim");
                Control ev_dateofevent = this.Form.FindControl("ev_dateofevent");
                Control clm_datereported = this.Form.FindControl("clm_datereported");
                Control hdnClaimRptDateType = this.Form.FindControl("hdnClaimRptDateType");
                Control hdnTransStatus = this.Form.FindControl("hdnTransStatus");

                //JIRA 1342 ajohari2: Start
                Control hdndateofevent = this.Form.FindControl("hdndateofevent");
                Control hdndateofclaim = this.Form.FindControl("hdndateofclaim");
                Control hdnclm_datereported = this.Form.FindControl("hdnclm_datereported");
                //JIRA 1342 ajohari2: End

                iClaimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);//iClaimId is -1 for unsaved claims
                if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("Policynumber")) && (iClaimId <= 0))
                {
                    string sPolicyId = string.Empty;
                    string sPolicySystemID = string.Empty;
                    string sLoBCode = string.Empty;
                    string sSource = string.Empty; //RMA-10039
                    string sFormName = "Claimgc";
                    int iLoBId = 0;
                    int iClaimstatusId = 0;
                    string sPolicyNumber = AppHelper.GetQueryStringValue("Policynumber");
                    sPolicyNumber = sPolicyNumber.Replace("*", "|");

                    if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicyId")))
                        sPolicyId = AppHelper.GetQueryStringValue("PolicyId");

                    if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicySystemID")))
                        sPolicySystemID = AppHelper.GetQueryStringValue("PolicySystemID");

                    if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("LOB")))
                        sLoBCode = AppHelper.GetQueryStringValue("LOB");

                    if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("LOBID")))
                        iLoBId = Convert.ToInt32(AppHelper.GetQueryStringValue("LOBID"));

                    if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimstatusId")))
                        iClaimstatusId = Convert.ToInt32(AppHelper.GetQueryStringValue("ClaimstatusId"));

                    // RMA-10039 - policy API
                    if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicySource")))
                        sSource = AppHelper.GetQueryStringValue("PolicySource").ToString();

                    if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("DateOfLoss")))
                    {
                        //Ashish Ahuja: Claims Made Jira 1342
                        //Control dateofclaim = this.Form.FindControl("dateofclaim");
                        //Control ev_dateofevent = this.Form.FindControl("ev_dateofevent");
                        //Control clm_datereported = this.Form.FindControl("clm_datereported");
                        //JIRA RMA-8409 ajohari2:Start
                        if (dateofclaim != null && (dateofclaim is TextBox)) //ajohari2 FDM PowerView Fixes
                        {
                            ((TextBox)dateofclaim).Text = AppHelper.GetQueryStringValue("DateOfLoss");
                        }
                        else if (dateofclaim != null && (dateofclaim is Label)) 
                        {
                            ((Label)dateofclaim).Text = AppHelper.GetQueryStringValue("DateOfLoss");
                        }

                        if (ev_dateofevent != null && (ev_dateofevent is TextBox)) //ajohari2 FDM PowerView Fixes
                        {
                            ((TextBox)ev_dateofevent).Text = AppHelper.GetQueryStringValue("DateOfLoss");
                        }
                        else if (ev_dateofevent != null && (ev_dateofevent is Label)) 
                        {
                            ((Label)ev_dateofevent).Text = AppHelper.GetQueryStringValue("DateOfLoss");
                        }

                        if (clm_datereported != null && (clm_datereported is TextBox)) //ajohari2 FDM PowerView Fixes
                        {
                            //((TextBox)clm_datereported).Text = DateTime.Today.ToString("MM/dd/yyyy");
                            ((TextBox)clm_datereported).Text = AppHelper.GetQueryStringValue("ClaimReportedDate");//jira 1342
                        }
                        else if (clm_datereported != null && (clm_datereported is Label)) 
                        {
                            ((Label)clm_datereported).Text = AppHelper.GetQueryStringValue("ClaimReportedDate");
                        }
                        //JIRA RMA-8409 ajohari2:End

                    }

                    ClientScript.RegisterStartupScript(this.GetType(), "script", "SetClaimFromPolicyList('" + sLoBCode + "','" + sPolicyNumber + "', '" + sPolicyId + "', '" + sPolicySystemID + "', '" + sFormName + "' ,'" + iLoBId + "','" + iClaimstatusId + "','" + sSource + "' );", true); // RMA-10039 - policy API
                }
                //Ashish Ahuja: Claims Made Jira 1342
                //JIRA RMA-8409 ajohari2:Start
                if (dateofclaim != null && hdnClaimRptDateType != null && clm_datereported != null && (dateofclaim is TextBox) && (clm_datereported is TextBox))
                {
                    if ((((TextBox)dateofclaim).Text != null) && (((TextBox)hdnClaimRptDateType).Text == "1"))
                    {
                        if (((TextBox)clm_datereported).Text.Trim() == "" || ((TextBox)clm_datereported).Text.Trim() == null)
                        {
                            ((TextBox)clm_datereported).Text = ((TextBox)dateofclaim).Text;
                        }

                    }
                }
                else if (dateofclaim != null && hdnClaimRptDateType != null && clm_datereported != null && (dateofclaim is Label) && (clm_datereported is Label))
                {
                    if ((((Label)dateofclaim).Text != null) && (((TextBox)hdnClaimRptDateType).Text == "1"))
                    {
                        if (((Label)clm_datereported).Text.Trim() == "" || ((Label)clm_datereported).Text.Trim() == null)
                        {
                            ((Label)clm_datereported).Text = ((Label)dateofclaim).Text;
                        }

                    }
                }
                //Ashish Ahuja: Claims Made Jira 1342
                //JIRA 1342 ajohari2: Start

                if (hdndateofevent != null && ev_dateofevent != null && (ev_dateofevent is TextBox))
                {
                    ((TextBox)hdndateofevent).Text = ((TextBox)ev_dateofevent).Text;
                }
                else if (hdndateofevent != null && ev_dateofevent != null && (ev_dateofevent is Label))
                {
                    ((TextBox)hdndateofevent).Text = ((Label)ev_dateofevent).Text;
                }

                if (hdndateofclaim != null && dateofclaim != null && (dateofclaim is TextBox))
                {
                    ((TextBox)hdndateofclaim).Text = ((TextBox)dateofclaim).Text;
                }
                else if (hdndateofclaim != null && dateofclaim != null && (dateofclaim is Label))
                {
                    ((TextBox)hdndateofclaim).Text = ((Label)dateofclaim).Text;
                }

                if (hdnclm_datereported != null && clm_datereported != null && (clm_datereported is TextBox))
                {
                    ((TextBox)hdnclm_datereported).Text = ((TextBox)clm_datereported).Text;
                }
                else if (hdnclm_datereported != null && clm_datereported != null && (clm_datereported is Label))
                {
                    ((TextBox)hdnclm_datereported).Text = ((Label)clm_datereported).Text;
                }
                //JIRA RMA-8409 ajohari2:End
                //JIRA 1342 ajohari2: End

            }
            //MITS:33574 END
            //Start jramkumar MITS 34419
            if (IsPostBack)
            {
                string txtFocusScript = "var focusField=false; if((document.getElementById('ev_depteid') != null && document.getElementById('ev_depteid').value != '') || (document.getElementById('ev_countrycode_codelookup') != null && document.getElementById('ev_countrycode_codelookup').value != '')) { focusField=true; }";
                this.ClientScript.RegisterStartupScript(this.GetType(), "gc", txtFocusScript, true);
            }
            //End jramkumar MITS 34419
            
            //added by gmallick JIRA -12965 Start
            Button claimstatuscodedetail = this.Form.FindControl("claimstatuscodedetailbtn") as Button;
            if (claimstatuscodedetail != null)
            {
                claimstatuscodedetail.Attributes.Add("Visible", "True");
                claimstatuscodedetail.Enabled = true;
                claimstatuscodedetail.Attributes.Add("onclientclick", "return selectCodeWithDetail('claimstatuscode',1);");
                claimstatuscodedetail.CssClass = "ClaimStatusButton";
            }
            //added by gmallick JIRA -12965 End


        }
        //Mridul 05/28/09 MITS 16745 (Chubb Ack)
        /// <summary>
        /// Override base and generates claim letter if claim is found eligible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {   
                int iClaimId = 0;
                string sCLMLtrTmplType = "";
                string sClaimNo = "";
                base.NavigateSave(sender,e);
                iClaimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);
                sCLMLtrTmplType = Convert.ToString(((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text);
                sClaimNo = Convert.ToString(((TextBox)this.Form.FindControl("claimnumber")).Text);
                if (sCLMLtrTmplType == "ACK" || sCLMLtrTmplType == "CL")
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClaimLetterMerge", "DoClaimLetterMerge("+iClaimId+",'"+sCLMLtrTmplType+"','"+sClaimNo+"');", true);
                sCLMLtrTmplType = "";

                // rrachev JIRA 5021 Get current value of ContainsOpenDiaries
                Control openDiaries = this.FindControl("containsopendiaries");
                if (openDiaries != null && this.Data != null && !String.IsNullOrEmpty(this.Data.OuterXml))
                {
                    BindData2Control(XElement.Parse(this.Data.OuterXml), new Control[] { openDiaries });
                }
            }
            catch(Exception ex)
            {
            
            }
            finally{}
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oUseAdvancedClaim = oMessageElement.XPathSelectElement("//UseAdvancedClaim");
            Control oPolicyControl = this.FindControl("div_primarypolicyid");
            if (oUseAdvancedClaim != null)//Deb : MITS 25242
            {
                if (oUseAdvancedClaim.Value == "-1")
                {
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
                else
                {
                    oPolicyControl = this.FindControl("div_multipolicyid");
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
            }
            //Ashish Ahuja: Claims Made Jira 1342
            XElement oClaimDateRptType = oMessageElement.XPathSelectElement("//ClaimRptDateType");
            XElement oClaimDateReported =  oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Claim/DateRptdToRm");
            
            if (oClaimDateRptType != null)
            {
                //((TextBox)this.Form.FindControl("hdnClaimRptDateType")).Text = oClaimDateRptType.Value;
                if (oClaimDateRptType.Value == "2" && (oClaimDateReported.Value.Trim() == "" || oClaimDateReported.Value.Trim() == null))
                {
                    oClaimDateReported.Value = DateTime.Now.ToString("MM/dd/yyyy");
                }
                
            }

            //Ashish Ahuja: Claims Made Jira 1342
            //MITS 14169:To open Policy from a claim:Setting client click attribute of _open button:Start
            int iPolicyId = 0;
            int iEnhPolicyId = 0;
            Button btnPolicyOpen = (Button)(this.Form.FindControl("primarypolicyid_open"));
            
            if (btnPolicyOpen != null)
            {
                XElement oPolicyId = oMessageElement.XPathSelectElement("//PrimaryPolicyId");
                if (oPolicyId != null)
                {
                    iPolicyId = Convert.ToInt32(oPolicyId.Value);
                }

                XElement oPolicyEnhId = oMessageElement.XPathSelectElement("//PrimaryPolicyIdEnh");
                if (oPolicyEnhId != null)
                {
                    iEnhPolicyId = Convert.ToInt32(oPolicyEnhId.Value);
                }

                XElement oUseEnhPolFlag = oMessageElement.XPathSelectElement("//UseEnhPolFlag");
                if (oUseEnhPolFlag != null)
                {
                    if (oUseEnhPolFlag.Value == "0" )
                    {

                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                       
                    }
                    else if (oUseEnhPolFlag.Value == "-1" )
                    {
                        
                        //Start-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.
						                      
						btnPolicyOpen.OnClientClick = "fnOpenPolicy('policyenhgl'); return false;";
						//End-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.
                       
                    }
                    
                }
            }

            btnPolicyOpen = (Button)(this.Form.FindControl("multipolicyid_open"));
            if (btnPolicyOpen != null)
            {
                if (btnPolicyOpen != null)
                {
                    btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                }
            }
            //MITS 14169:End
        }

    }
}
