﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class Combinedpayment : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            FDMPageLoad();

            //if( Page.FindControl("IsSchDatesChecked") != null)
            //{
            //    TextBox tbIsSchDatesChecked = (TextBox)Page.FindControl("IsSchDatesChecked");
            //    if (Page.FindControl("adjustprintdatesflag") != null )
            //    {
            //        if(tbIsSchDatesChecked.Text == "true")
            //        {
            //            ((CheckBox)Page.FindControl("adjustprintdatesflag")).Checked = true;
            //        }
            //        else
            //        {

            //            ((CheckBox)Page.FindControl("adjustprintdatesflag")).Checked = false;

            //            ((CheckBox)Page.FindControl("adjustprintdatesflag")).Visible = false;
            //            ((Label)Page.FindControl("lbl_adjustprintdatesflag")).Visible = false;


            //        }
            //    }
            //}

            //RMA-18306
            //TextBox txtpye_lastname = (TextBox)this.FindControl("pye_lastname");
            //if (txtpye_lastname != null)
            //    txtpye_lastname.Enabled = false;
            
        }

        public override void ModifyControls()
        {
            base.ModifyControls();

            //Element oCombinedPayRowId = oMessageElement.XPathSelectElement("//CombPayRowId");

            if (Page.FindControl("CombPayRowId") != null)
            {
                TextBox txtCombPayRowId = (TextBox)Page.FindControl("CombPayRowId");
                if (txtCombPayRowId.Text == "0")
                {
                    ((CheckBox)Page.FindControl("adjustprintdatesflag")).Checked = true;
                }
            }

            if (Page.FindControl("pye_lastname") != null)
            {
                if (((TextBox)Page.FindControl("pye_lastname")).Text.Trim() != "")
                {
                    DatabindingHelper.DisableControl(Page.FindControl("pye_lastname"));
                }
            }

        }
    }
}
