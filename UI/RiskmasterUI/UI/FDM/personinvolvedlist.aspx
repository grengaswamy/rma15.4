<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="personinvolvedlist.aspx.cs"
    Inherits="Riskmaster.UI.FDM.PersoninvolvedList" ValidateRequest="false" EnableViewStateMac="false" EnableEventValidation="false"%>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>

<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Xml.XPath" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript"src="../../Scripts/jquery/jquery-1.8.0.js"></script>
    <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script>    
    <script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript"></script> 
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script> 
    <script language="JavaScript" type="text/javascript">

        var m_Wnd = null;
        var m_PiType = "";
        var m_sForm = "";
        function PickPI(sPiType) {
            m_PiType = sPiType;
            if (m_Wnd != null)
                m_Wnd.close();

            if (sPiType == "EMPLOYEES")
                /*m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;formname=employee&amp;screenflag=2','searchWnd',
                                'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');*/
                m_Wnd = window.open("../Search/searchmain.aspx?formname=employee&screenflag=2", "Employee",
                                    "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");

            else if (sPiType == "PATIENTS") {
                //		    m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;formname=patient&amp;screenflag=2','searchWnd',
                //			 				'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
                m_Wnd = window.open("../Search/searchmain.aspx?formname=patient&screenflag=2", "Patient",
                                  "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");

                //pmittal5 MITS 11623 3/11/2008
                //m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;viewid=4&amp;formname=PATIENTS&amp;screenflag=2','searchWnd',
                //						'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
            }
            else if (sPiType == "PHYSICIANS") {
                //			m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;formname=physician&amp;screenflag=2','searchWnd',
                //							'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
                m_Wnd = window.open("../Search/searchmain.aspx?formname=physician&screenflag=2", "Physician",
                    "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");
            }
            else if (sPiType == "MEDICAL_STAFF") {
                //			m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;formname=medstaff&amp;screenflag=2','searchWnd',
                //							'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
                m_Wnd = window.open("../Search/searchmain.aspx?formname=medstaff&screenflag=2", "Medstaff",
                    "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");
            }
            else if (sPiType == "WITNESS") {
                //			m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;viewid=4&amp;formname=witness&amp;screenflag=2','searchWnd',
                //							'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
                m_Wnd = window.open("../Search/searchmain.aspx?formname=witness&screenflag=2", "Witness",
                    "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");
            }
                //MITS-10998 - Added condition for Other Persons
            else if (sPiType == "OTHERPEOPLE") {
                //            m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;viewid=4&amp;formname=otherpeople&amp;screenflag=2','searchWnd',
                //                              'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
                m_Wnd = window.open("../Search/searchmain.aspx?formname=otherpeople&screenflag=2", "OtherPeople",
                    "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");
            }
                //MITS-10998 End              
                //mbahl3  
            else if (sPiType == "DRIVER") {
                //            m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;viewid=4&amp;formname=otherpeople&amp;screenflag=2','searchWnd',
                //                              'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
                m_Wnd = window.open("../Search/searchmain.aspx?formname=driver&screenflag=2", "Driver",
                    "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");
            }
            else {
                //		    m_Wnd=window.open('home?pg=riskmaster/Search/MainPage&amp;viewid=4&amp;tableid='+sPiType+'&amp;screenflag=2','searchWnd',
                //							'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
                m_Wnd = window.open("../Search/searchmain.aspx?formname=otherpeople&screenflag=2", "OtherPeople",
                                    "width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");
            }

            self.lookupCallback = "onSearchCallback";
        }

        function onSearchCallback(sRecordId) {
            if (m_Wnd != null)
                m_Wnd.close();

            m_Wnd = null;
            var sForm = "";
            if (sRecordId != "") {
                if (m_PiType=="WITNESS")
                    sForm ="piwitness";
                else if (m_PiType=="EMPLOYEES")
                    sForm ="piemployee";
                else if (m_PiType=="MEDICAL_STAFF")
                    sForm ="pimedstaff";
                else if (m_PiType=="PATIENTS")
                    sForm ="pipatient";
                else if (m_PiType=="PHYSICIANS")
                    sForm ="piphysician";
                    //MITS-10998 - Added conditions for the OtherPeople and Witness field
                else if (m_PiType=="OTHERPEOPLE")
                    sForm ="piother";
                else if (m_PiType=="WITNESS")
                    sForm ="piwitness";
                else if (m_PiType=="DRIVER")  //mbahl3
                    sForm ="pidriver";
                    //MITS-10998 End
                else
                    sForm = "piother";

                document.forms[0].SysFormName.value = sForm;
                document.forms[0].entityid.value = sRecordId;
                pleaseWait.Show();
                m_sForm = sForm;
                SetExistingPIUrl();
                document.getElementById('addexisting').click();

            }


            m_PiType = "";
            return true;
        }
        function SetExistingPIUrl() {

            window.location.href = "../FDM/" + m_sForm + ".aspx?SysFormPIdName=eventid&SysFormName=" + document.getElementById('SysFormName').value + "&SysCmd=&SysFormIdName=pirowid&pieid=" + document.getElementById('entityid').value + "&SysFormId=" + document.getElementById('SysFormId').value + "&eventid=" + document.getElementById('eventid').value + "&txtScreenFlowStack=" + document.getElementById('txtScreenFlowStack').value + "&SysViewType=tab&DataChange=true";
            return false;
        }
	</script>
</head>

<body class="10pt" onload="pageLoaded();">                                        
 <form id="frmData" runat="server">
  
     <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
      <%        
if (sScreenName != "AddPersonInvolved")
{%>
        <div>
            <h2><%=sFormTitle%></h2>
          <table border="0" id="tbllist" width="95%" cellspacing="0" cellpadding="2">
            <tr>
                <td class="ctrlgroup" width="50px"></td>    
                <%--Bijender Has add "return false" in <a> tag for Mits 14894--%>   
                    <td class="ctrlgroup"><a href="#" id="HeaderColumn1" class="ctrlgroup" onclick="SortList( document.all.tbllist,1,'lnkPersonInvolved',true );">
                        <asp:label id="lblName" runat="server" text="<%$ Resources:lblName %>" > </asp:label>
                    </a></td>
                    <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn2" class="ctrlgroup" onclick="SortList( document.all.tbllist,2,'lnkPersonInvolved',true );">
                        <asp:label id="lblPersnType" runat="server" text="<%$ Resources:lblPersnType %>"> </asp:label>
                    </a></td>
                    <%--<td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn2" class="ctrlgroup" onclick="SortList( document.all.tbllist,2,'lnkPersonInvolved',true );">
                        <asp:label id="lblPersnLevel" runat="server" text="<%$ Resources:lblPersnLevel %>"> </asp:label>
                    </a></td>--%>
                <%--Bijender End Mits 14894--%>
            </tr>

             <%int i = 0; foreach (XElement item in result)
               {
                   string rowclass = "";
                   string screenName = "";
                   if ((i % 2) == 1) rowclass = "datatd1";
                   else rowclass = "datatd";
                   i++;      
            %>
            <tr>
                <td class="<%=rowclass%>">
                    <table border="0" cellspacing="0" cellpadding="2">
                     <tr>
                    <td>  
                       <%
                                    if (sShowDeleteButton)
                                    {%>
                                <%
                                    PageId.ID = "PageId" + i;
                                    PageId.Value = item.Element("PiRowId").Value;
                                %>
                                    <input type="checkbox" id="PageId" runat="server" name="PageId" title="Claim List" style="display: none" />
                                <%} %>
                            </td>                      
                    </tr>
                    </table>
                </td>
                <td class="<%=rowclass%>">
                 <%lnkPersonInvolved.Text = item.Element("PiEid").Value;
                   lnkPersonInvolved.ID = "lnkPersonInvolved" + i;  %>                         
                  
                    <%                    
            if ((item.Element("PiTypeCode").Value).Contains("MED"))
            {
                lnkPersonInvolved.OnClientClick = "parent.MDIShowScreen('" + item.Element("PiRowId").Value + "', 'pimedstaff');return false;";
                screenName = "pimedstaff";
            }
            else if ((item.Element("PiTypeCode").Value).Contains("O"))
            {
                string slnkPersonInvolved = "parent.MDIShowScreen('" + item.Element("PiRowId").Value + "', 'piother');return false;"; 
                lnkPersonInvolved.OnClientClick = slnkPersonInvolved;
                
                screenName = "piother";
            }
            else if ((item.Element("PiTypeCode").Value).Contains("W"))
            {
                lnkPersonInvolved.OnClientClick = "parent.MDIShowScreen('" + item.Element("PiRowId").Value + "', 'piwitness');return false;";                
                screenName = "piwitness";
            }
            else if ((item.Element("PiTypeCode").Value).Contains("E"))
            {
                lnkPersonInvolved.OnClientClick = "parent.MDIShowScreen('" + item.Element("PiRowId").Value + "', 'piemployee');return false;";
                screenName = "piemployee";
            }
            else if ((item.Element("PiTypeCode").Value).Contains("PHYS"))
            {
                lnkPersonInvolved.OnClientClick = "parent.MDIShowScreen('" + item.Element("PiRowId").Value + "', 'piphysician');return false;";                
                screenName = "piphysician";
            }
            else if ((item.Element("PiTypeCode").Value).Contains("P"))
            {
                lnkPersonInvolved.OnClientClick = "parent.MDIShowScreen('" + item.Element("PiRowId").Value + "', 'pipatient');return false;";                
                screenName = "pipatient";
            }
                //mbahl3
            else if ((item.Element("PiTypeCode").Value).Contains("D"))
            {
                lnkPersonInvolved.OnClientClick = "parent.MDIShowScreen('" + item.Element("PiRowId").Value + "', 'pidriver');return false;";
                screenName = "pidriver";
            }%>
                        <asp:LinkButton runat="server" ID="lnkPersonInvolved"></asp:LinkButton>
           </td>                 
          <td class="<%=rowclass%>">
                       <%-- <%
               if ((item.Element("PiTypeCode").Value).Contains("O"))
               {%>O 
               <% string sPitype ="";
                  foreach (XElement SysexItem in resultSysEx)
                  {
                                  if (item.Element("PiEid").Attribute("codeid").Value == SysexItem.Attribute("EntityId").Value)
                                     // && item.Element("PiErRowID").Value == SysexItem.Attribute("PiErRowID").Value) //Payal : Added for RMA: 10062
                          sPitype = SysexItem.Value;
                  } %>
                  <%=sPitype %>
                <%}
               else
                      {%>--%>
                <%--   <%=item.Element("PiTypeCode").Value%>--%>
        

            <%--  Show the Entity role type for other person // jira 17911 pgupta215--%>
            <%     string RoleTableName ="";
                if ((item.Element("PiTypeCode").Value).Contains("O"))
                {
                    foreach (XElement SysexItem in resultRoleTableName)
                  {
                      if ((item.Element("RoleTableId").Value) == SysexItem.Attribute("RoleTableId").Value)
                          RoleTableName = SysexItem.Attribute("RoleSysTableName").Value; 
                    }%>
                      <%=item.Element("PiTypeCode").Value %> <% if(RoleTableName!="") %> <%=" - "  %>  <% =RoleTableName  %>
                <%}
                  else{ %>  
                      <%=item.Element("PiTypeCode").Value%>  
                    <%} %>
                       
           </td>
                 <%--skhare7 Policy interface--%>
                   <%-- <td class="<%=rowclass%>">
         
                        <% string sPiLevel = sLevel;
                  foreach (XElement PILevelitem in levelresult)
                  {
                      if (item.Element("PiEid").Attribute("codeid").Value == PILevelitem.Attribute("PiEid").Value)
                          sPiLevel = PILevelitem.Value;
                     
                  }
                 
                  
                    %>
                 &nbsp;&nbsp;&nbsp
                        <asp:label id="lblEventLevel" runat="server" text="<%$ Resources:lblEventLevel %>"></asp:label>
                    </td>--%>
           <!--Added for MITS:15713:Sorting of Person Involved-->
                    <td style="display: none">
              <%
              lnkPersonInvolved_RowId.ID = "lnkPersonInvolved" + i + "_RowId";%>
                        <asp:TextBox runat="server" ID="lnkPersonInvolved_RowId" Style="display: none" />
           </td> 
                    <td style="display: none">
            <% 
                lnkPersonInvolved_ScreenType.ID = "lnkPersonInvolved" + i + "_ScreenType";
              %>
                        <asp:TextBox runat="server" ID="lnkPersonInvolved_ScreenType" Style="display: none" />
           </td> 
           <!--Added for MITS:15713:Sorting of Person Involved-->                
                    <td style="display: none"></td>
                    <td style="display: none">
             <%=item.Element("PiRowId").Value + "," + screenName%> <!--Changed for MITS:15713:Sorting of Person Involved-->
           </td>    
                    <td style="display: none">
              <%=item.Element("PiRowId").Value%>
           </td>                             
            </tr>
            <%}%>
        </table> 
            <center>
                <div id="pageNavPosition"></div>
            </center>
       <table>
        <asp:TextBox runat="server" ID="DeleteListTemplate" Style="display: none" Text="Instance/PersonInvolvedList/PersonInvolved/PiRowId" />        
        <tr>
        <td>
        <%if (sShowAddButton)
                      {%>
                        <asp:Button class="button" runat="Server" ID="btnPI" Text="<%$ Resources:btnAdd %>" OnClick="btnPI_Click" Visible="false" />
                    <%} %>
                    <%if (sShowDeleteButton)
                      {%>
                        <asp:Button class="button" runat="server" ID="btnDelete" Text="<%$ Resources:btnDelete %>" OnClick="NavigateListDelete"
                        OnClientClick="return DeleteList();" Visible="false" />
                    <%} %>
                        <asp:Button Style="display: none" class="button" runat="server" ID="BackToParent" Text="<%$ Resources:btnBack %>" OnClientClick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;" />
        </td>
        </tr>
        </table>
        </div>
        <script type="text/javascript">
            var listpaging = new ListPaging('tbllist', 10);
            listpaging.init();
            listpaging.showPageNavigation('listpaging', 'pageNavPosition');
            listpaging.showPage(1);
        </script>
        <% }%>      
         <% if (sScreenName == "AddPersonInvolved")
         {%>       
         <div>
            <h2>
                <asp:label id="lblAddNewPrsnInvolved" runat="server" text="<%$ Resources:lblAddNewPrsnInvolved %>"></asp:label>
            </h2>
            <h5>
                <asp:label id="lblTypeOfPrsnInvolved" runat="server" text="<%$ Resources:lblTypeOfPrsnInvolved %>"></asp:label>
         </<h5>
         <br />
         <br />              
        <table border="0" width="40%" cellspacing="0" cellpadding="2">
          <% int i = 0;
             foreach (XElement item in resultPiType)
             {
                 string rowclass = "";
                 if ((i % 2) == 1) rowclass = "datatd1";
                 else rowclass = "";
                 i++;      
            %>
            <tr>
                <td nowrap="" class="caption">E - <%=item.Element("E").Value%></td>   
               	<td colspan="3" class="caption"></td>              
            </tr>
                 <tr>
                     <td width="100"></td>
                            <td width="100"><a class="LightBold" href="javascript:PickPI('EMPLOYEES');">
                                <asp:label id="lblAddExistingemployee" runat="server" text="<%$ Resources:lblAddExisting %>"></asp:label>
                            </a></td>
                     <td>
                         <asp:LinkButton ID="lnkPiEmployee" runat="server"
                             PostBackUrl="piemployee.aspx?SysFormName=piemployee&SysCmd=&SysViewType=controlsonly&SysFormIdName=pirowid&SysFormPIdName=eventid&SysEx=EventId|EventNumber&SysExMapCtl=EventId|EventNumber"
                                    text="<%$ Resources:lblAddNew %>" font-bold="False"></asp:linkbutton>
                            </td>
                 </tr>
			  <tr>
                 <td nowrap="" class="caption">MED - <%=item.Element("MED").Value%></td>   
                 <td colspan="3" class="caption"></td>              
              </tr>
              <tr>
				<td width="100"></td>
				<td width="100"><a class="LightBold" href="javascript:PickPI('MEDICAL_STAFF');">Add Existing</a></td>
                     <td>
                                <asp:linkbutton id="lnkPiMedStaff" runat="server" postbackurl="pimedstaff.aspx?SysFormName=pimedstaff&SysCmd=&SysViewType=controlsonly&SysFormIdName=pirowid&SysFormPIdName=eventid&SysEx=EventId|EventNumber&SysExMapCtl=EventId|EventNumber" text="<%$ Resources:lblAddNew %>" font-bold="False"></asp:linkbutton>
                            </td>
			  </tr>
			  <tr>
               <td nowrap="" class="caption">O - <%=item.Element("O").Value%></td>   
               <td colspan="3" class="caption"></td>              
              </tr>
              <tr>
			    <td width="100"></td>
				<td width="100"><a class="LightBold" href="javascript:PickPI('OTHERPEOPLE');">Add Existing</a></td>
                     <td width="100">
                                <asp:linkbutton id="lnkPiOther" runat="server" postbackurl="piother.aspx?SysFormName=piother&SysCmd=&SysViewType=controlsonly&SysFormIdName=pirowid&SysFormPIdName=eventid&SysEx=EventId|EventNumber&SysExMapCtl=EventId|EventNumber" text="<%$ Resources:lblAddNew %>" font-bold="False"><asp:Label ID="lblPiOther" runat="server" Text="<%$ Resources:lblAddNew %>"></asp:Label></asp:linkbutton>
                            </td>
			  </tr>
			  <tr>
                <td nowrap="" class="caption"> P - <%=item.Element("P").Value%></td>   
               	<td colspan="3" class="caption"></td>              
              </tr>
              <tr>
				<td width="100"></td>
				<td width="100"><a class="LightBold" href="javascript:PickPI('PATIENTS');">Add Existing</a></td>
                     <td width="100">
                                <asp:linkbutton id="lnkPiPatient" runat="server" postbackurl="pipatient.aspx?SysFormName=pipatient&SysCmd=&SysViewType=controlsonly&SysFormIdName=pirowid&SysFormPIdName=eventid&SysEx=EventId|EventNumber&SysExMapCtl=EventId|EventNumber" text="<%$ Resources:lblAddNew %>" font-bold="False"></asp:linkbutton>
                            </td>
        	  </tr>
			  <tr>
                <td nowrap="" class="caption">PHYS - <%=item.Element("PHYS").Value%></td>   
               	<td colspan="3" class="caption"></td>              
              </tr>
              <tr>
				<td width="100"></td>
				<td width="100"><a class="LightBold" href="javascript:PickPI('PHYSICIANS');">Add Existing</a></td>
                     <td width="100">
                                <asp:linkbutton id="lnkPiPhysician" runat="server" postbackurl="piphysician.aspx?SysFormName=piphysician&SysCmd=&SysViewType=controlsonly&SysFormIdName=pirowid&SysFormPIdName=eventid&SysEx=EventId|EventNumber&SysExMapCtl=EventId|EventNumber" text="<%$ Resources:lblAddNew %>" font-bold="False"></asp:linkbutton>
                            </td>
			  </tr>
			  <tr>
              <td nowrap="" class="caption">W - <%=item.Element("W").Value%></td>   
              <td colspan="3" class="caption"></td>              
            </tr>
             <tr>
				<td width="100"></td>
				<td width="100"><a class="LightBold" href="javascript:PickPI('WITNESS');">Add Existing</a></td>
                     <td width="100">
                                <asp:linkbutton id="lnkPiWitness" runat="server" postbackurl="piwitness.aspx?SysFormName=piwitness&SysCmd=&SysViewType=controlsonly&SysFormIdName=pirowid&SysFormPIdName=eventid&SysEx=EventId|EventNumber&SysExMapCtl=EventId|EventNumber" text="<%$ Resources:lblAddNew %>" font-bold="False"></asp:linkbutton>
                            </td>
			 </tr>
             <tr>
                <td nowrap="" class="caption">D - <%=item.Element("D").Value%></td>   
               	<td colspan="3" class="caption"></td>              
              </tr>
              <tr>
				<td width="100"></td>
				<td width="100"><a class="LightBold" href="javascript:PickPI('DRIVER');">Add Existing</a></td>
                     <td width="100">
                                <asp:linkbutton id="lnkPiDriver" runat="server" postbackurl="pidriver.aspx?SysFormName=pidriver&SysCmd=&SysViewType=controlsonly&SysFormIdName=pirowid&SysFormPIdName=eventid&SysEx=EventId|EventNumber&SysExMapCtl=EventId|EventNumber" text="<%$ Resources:lblAddNew %>" font-bold="False"></asp:linkbutton>
                            </td>
			  </tr>
			 <tr>
			 <td>
                         <asp:Button class="button" runat="server" ID="Back" Text="<%$ Resources:btnBack %>" OnClientClick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;" />
			 </td>
			 </tr>
			 <%}%> 
           </table>        
        <%} %>
                
        <asp:TextBox runat="server" ID="SysInvisible" Style="display: none" />
        <asp:TextBox runat="server" ID="TextBox1" Style="display: none" />
        <asp:TextBox runat="server" ID="SysCmd" Style="display: none" RMXRef="Instance/UI/FormVariables/SysCmd" />
        <asp:TextBox runat="server" ID="SysCmdConfirmSave" Style="display: none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" />
        <asp:TextBox runat="server" ID="SysCmdQueue" Style="display: none" RMXRef="Instance/UI/FormVariables/SysCmdQueue" />
        <asp:TextBox runat="server" ID="SysCmdText" Style="display: none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText" />
        <asp:TextBox runat="server" ID="SysClassName" Style="display: none" Text="PersonInvolvedList" RMXRef="Instance/UI/FormVariables/SysClassName" />
        <asp:TextBox runat="server" ID="SysSerializationConfig" Style="display: none" Text="&lt;PersonInvolvedList&gt;&lt;PersonInvolved&gt;&lt;/PersonInvolved&gt;&lt;/PersonInvolvedList&gt;" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" />
        <asp:TextBox runat="server" ID="SysSerializationConfigIgnore" Style="display: none" Text="Claim.Comments" RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore" />
        <asp:TextBox runat="server" ID="SysFormPForm" Style="display: none" RMXRef="Instance/UI/FormVariables/SysFormPForm" />
                    <asp:textbox runat="server" id="SysFormPIdName" text="parentid" style="display: none" rmxref="Instance/UI/FormVariables/SysFormIdName" />
        <asp:TextBox runat="server" ID="SysFormPId" Style="display: none" RMXRef="Instance/UI/FormVariables/SysFormPId" />
        <asp:TextBox runat="server" ID="SysPSid" Style="display: none" RMXRef="Instance/UI/FormVariables/SysPSid" />
        <asp:TextBox runat="server" ID="SysEx" Style="display: none" RMXRef="Instance/UI/FormVariables/SysEx" />
        <asp:TextBox runat="server" ID="SysNotReqNew" Style="display: none" Text="eventnumber|timeofevent|timereported" />
        <asp:TextBox runat="server" ID="SysFormName" Style="display: none" Text="personinvolvedlist" RMXRef="Instance/UI/FormVariables/SysFormName" />
        <asp:TextBox runat="server" ID="SysFormIdName" Style="display: none" Text="eventid" RMXRef="Instance/UI/FormVariables/SysFormIdName" />
        <asp:TextBox runat="server" ID="SysFormId" Style="display: none" RMXRef="Instance/UI/FormVariables/SysFormId" />
        <asp:TextBox runat="server" ID="SysSid" Style="display: none" Text="11150" RMXRef="Instance/UI/FormVariables/SysSid" /> <!--Person Involved SecurityId under Event-->
             <!--Person Involved SecurityId under Event-->
        <asp:TextBox runat="server" ID="SysViewType" Style="display: none" RMXRef="Instance/UI/FormVariables/SysViewType" />
        <asp:TextBox runat="server" ID="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" Style="display: none" />
        <asp:TextBox runat="server" ID="eventid" RMXRef="Instance/UI/FormVariables/SysExData/EventId" Style="display: none" />
        <asp:TextBox runat="server" ID="depteid" RMXRef="Instance/UI/FormVariables/SysExData/DeptEid" Style="display: none" />
        <asp:TextBox runat="server" ID="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId" Style="display: none" />
        <asp:TextBox runat="server" ID="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" Style="display: none" />
        <asp:TextBox runat="server" ID="entityid" RMXRef="Instance/UI/FormVariables/SysExData/PiEid" Style="display: none" />
                    <asp:textbox runat="server" id="parentid" rmxref="Instance/UI/FormVariables/SysExData/ParentId" style="display: none" />
                    <input type="text" runat="server" id="PageIds" style="display: none" />
             <asp:TextBox runat="server" ID="txtScreenFlowStack" Style="display: none" />
             <asp:Button runat="server" ID="addexisting" Style="display: none" PostBackUrl="javascript:SetExistingPIUrl()" />
             <asp:TextBox runat="server" ID="PageName" Style="display: none" Text="PersonInvolvedList" />
    </div>
    
	<uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form> 
</body>
</html>
