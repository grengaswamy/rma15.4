<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="employee.aspx.cs" Inherits="Riskmaster.UI.FDM.Employee"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Employee Maintenance</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}  </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}  </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift">
                <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                                height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                                src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                                validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                                height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                                height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                                height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                                height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                                width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                                width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="doSearch()" src="Images/search.gif"
                                width="28" height="28" border="0" id="search" alternatetext="Search" onmouseover="this.src='Images/search2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/search.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="attach()" src="Images/attach.gif"
                                width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                                onmouseover="this.src='Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/attach.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                                height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="Comments()" src="Images/comments.gif"
                                width="28" height="28" border="0" id="comments" alternatetext="Comments" onmouseover="this.src='Images/comments2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/comments.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="MailMerge()" src="Images/mailmerge.gif"
                                width="28" height="28" border="0" id="mailmerge" alternatetext="Mail Merge" onmouseover="this.src='Images/mailmerge2.gif';this.style.zoom='110%'"
                                onmouseout="this.src='Images/mailmerge.gif';this.style.zoom='100%'" />
                        </td>
                        <td align="center" valign="middle" height="32" width="28" xmlns="">
                            <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                                width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                                onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
                        </td>
                    </tr>
                </table>
            </div>
    <br />
    <div class="msgheader" id="formtitle">
        Employee Maintenance</div>
    <div class="errtextheader">
    </div>
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0">
                    <tr>
                        <td class="Selected" nowrap="true" name="TABSemployee" id="TABSemployee">
                            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="employee"
                                id="LINKTABSemployee"><span style="text-decoration: none">Employee</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSemploymentinfo" id="TABSemploymentinfo">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="employmentinfo"
                                id="LINKTABSemploymentinfo"><span style="text-decoration: none">Employment Info</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSemployeeinfo" id="TABSemployeeinfo">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="employeeinfo"
                                id="LINKTABSemployeeinfo"><span style="text-decoration: none">Employee Info</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSsuppgroup" id="TABSsuppgroup">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                        <td valign="top" nowrap="true" />
                    </tr>
                </table>
                <div style="position: relative; left: 0; top: 0; width: 870px; height: 350px; overflow: auto"
                    class="singletopborder">
                    <table border="0" cellspacing="0" celpadding="0" width="95%" height="95%">
                        <tr>
                            <td valign="top">
                                <table border="0" name="FORMTABemployee" id="FORMTABemployee">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="employeeeid" rmxref="/Instance/Employee/EmployeeEid" /><asp:textbox
                                            style="display: none" runat="server" id="entityid" rmxref="/Instance/Employee/EmployeeEntity/EntityId" /><td
                                                nowrap="true" id="employeenumber_ctlcol" xmlns="">
                                                <b><u>Employee Number</u></b>
                                            </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="employeenumber"
                                                rmxref="/Instance/Employee/EmployeeNumber" /><asp:requiredfieldvalidator validationgroup="vgSave"
                                                    errormessage="Required" runat="server" controltovalidate="employeenumber" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="abbreviation_ctlcol" xmlns="">
                                            Initials
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="abbreviation"
                                                rmxref="/Instance/Employee/EmployeeEntity/Abbreviation" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="lastname_ctlcol" xmlns="">
                                            <b><u>Last Name</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="lastname"
                                                rmxref="/Instance/Employee/EmployeeEntity/LastName" /><asp:requiredfieldvalidator
                                                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="lastname" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="alsoknownas_ctlcol" xmlns="">
                                            Also Known as
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="alsoknownas"
                                                rmxref="/Instance/Employee/EmployeeEntity/AlsoKnownAs" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="firstname_ctlcol" xmlns="">
                                            First Name
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="firstname"
                                                rmxref="/Instance/Employee/EmployeeEntity/FirstName" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="taxid_ctlcol" xmlns="">
                                            <b><u>Soc.Sec No.</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);"
                                                id="taxid" rmxref="/Instance/Employee/EmployeeEntity/TaxId" name="taxid" /><asp:requiredfieldvalidator
                                                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="taxid" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="middlename_ctlcol" xmlns="">
                                            Middle Name
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="middlename"
                                                rmxref="/Instance/Employee/EmployeeEntity/MiddleName" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="birthdate_ctlcol" xmlns="">
                                            Date of Birth
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" formatas="date" size="30" onchange="calculateage(this);;setDataChanged(true);&#xA;              "
                                                id="birthdate" rmxref="/Instance/Employee/EmployeeEntity/BirthDate" name="birthdate" /><script
                                                    language="JavaScript" src="">{var i;}</script><asp:button runat="server" type="button"
                                                        class="button" id="btnbirthdate" text="..." tabindex="20" onclientclick="calculateage(this);" /><cc1:CalendarExtender
                                                            runat="server" ID="birthdate_ajax" TargetControlID="birthdate" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" xmlns="">
                                            <asp:label runat="server" id="age" text="" />
                                        </td>
                                        <td nowrap="true" id="entityage_ctlcol" xmlns="">
                                            Age
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox size="30" runat="Server" rmxref="Instance/UI/FormVariables/SysExData/EmployeeAge"
                                                id="entityage" tabindex="21" style="background-color: silver;" readonly="true" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="addr1_ctlcol" xmlns="">
                                            Address 1
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="addr1"
                                                rmxref="/Instance/Employee/EmployeeEntity/Addr1" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="phone1_ctlcol" xmlns="">
                                            Office Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="phone1" rmxref="/Instance/Employee/EmployeeEntity/Phone1"
                                                tabindex="22" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="addr2_ctlcol" xmlns="">
                                            Address 2
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="addr2"
                                                rmxref="/Instance/Employee/EmployeeEntity/Addr2" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="phone2_ctlcol" xmlns="">
                                            Home Phone
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="phone2" rmxref="/Instance/Employee/EmployeeEntity/Phone2"
                                                tabindex="23" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="city_ctlcol" xmlns="">
                                            City
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="city"
                                                rmxref="/Instance/Employee/EmployeeEntity/City" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="faxnumber_ctlcol" xmlns="">
                                            Fax
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);"
                                                onfocus="phoneGotFocus(this);" id="faxnumber" rmxref="/Instance/Employee/EmployeeEntity/FaxNumber"
                                                tabindex="24" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="stateid_ctlcol" xmlns="">
                                            State
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="stateid"
                                                rmxref="/Instance/Employee/EmployeeEntity/StateId" cancelledvalue="" tabindex="9"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="stateidbtn" onclick="selectCode('states','stateid')" /><asp:textbox style="display: none"
                                                        runat="server" id="stateid_cid" rmxref="/Instance/Employee/EmployeeEntity/StateId/@codeid"
                                                        cancelledvalue="" tabindex="10" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="zipcode_ctlcol" xmlns="">
                                            Zip/Postal Code
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"
                                                id="zipcode" rmxref="/Instance/Employee/EmployeeEntity/ZipCode" tabindex="25" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="countrycode_ctlcol" xmlns="">
                                            Country
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="countrycode"
                                                rmxref="/Instance/Employee/EmployeeEntity/CountryCode" cancelledvalue="" tabindex="12"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="countrycodebtn" onclick="selectCode('COUNTRY','countrycode')" /><asp:textbox
                                                        style="display: none" runat="server" id="countrycode_cid" rmxref="/Instance/Employee/EmployeeEntity/CountryCode/@codeid"
                                                        cancelledvalue="" tabindex="13" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="title_ctlcol" xmlns="">
                                            Title
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="title"
                                                rmxref="/Instance/Employee/EmployeeEntity/Title" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="county_ctlcol" xmlns="">
                                            County
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="county"
                                                rmxref="/Instance/Employee/EmployeeEntity/County" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="sexcode_ctlcol" xmlns="">
                                            Sex
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="sexcode"
                                                rmxref="/Instance/Employee/EmployeeEntity/SexCode" cancelledvalue="" tabindex="27"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="sexcodebtn" onclick="selectCode('SEX_CODE','sexcode')" /><asp:textbox style="display: none"
                                                        runat="server" id="sexcode_cid" rmxref="/Instance/Employee/EmployeeEntity/SexCode/@codeid"
                                                        cancelledvalue="" tabindex="28" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="freezepayments_ctlcol" xmlns="">
                                            Freeze Payments
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="freezepayments"
                                                rmxref="/Instance/Employee/EmployeeEntity/FreezePayments" tabindex="15" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="maritalstatcode_ctlcol" xmlns="">
                                            Marital Status
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="maritalstatcode"
                                                rmxref="/Instance/Employee/MaritalStatCode" cancelledvalue="" tabindex="29" onblur="codeLostFocus(this.id);" /><input
                                                    type="button" class="CodeLookupControl" name="maritalstatcodebtn" onclick="selectCode('MARITAL_STATUS','maritalstatcode')" /><asp:textbox
                                                        style="display: none" runat="server" id="maritalstatcode_cid" rmxref="/Instance/Employee/MaritalStatCode/@codeid"
                                                        cancelledvalue="" tabindex="30" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="display: none;" name="FORMTABemploymentinfo" id="FORMTABemploymentinfo">
                                    <tr>
                                        <td nowrap="true" id="datehired_ctlcol" xmlns="">
                                            Date Hired
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="datehired" rmxref="/Instance/Employee/DateHired" tabindex="31" /><cc1:CalendarExtender
                                                    runat="server" ID="datehired_ajax" TargetControlID="datehired" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="termdate_ctlcol" xmlns="">
                                            Termination Date
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="termdate" rmxref="/Instance/Employee/TermDate" tabindex="49" /><cc1:CalendarExtender
                                                    runat="server" ID="termdate_ajax" TargetControlID="termdate" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="positioncode_ctlcol" xmlns="">
                                            Position Code
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="positioncode"
                                                rmxref="/Instance/Employee/PositionCode" cancelledvalue="" tabindex="33" onblur="codeLostFocus(this.id);" /><input
                                                    type="button" class="CodeLookupControl" name="positioncodebtn" onclick="selectCode('POSITIONS','positioncode')" /><asp:textbox
                                                        style="display: none" runat="server" id="positioncode_cid" rmxref="/Instance/Employee/PositionCode/@codeid"
                                                        cancelledvalue="" tabindex="34" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="activeflag_ctlcol" xmlns="">
                                            Active
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="activeflag" rmxref="/Instance/Employee/ActiveFlag"
                                                tabindex="51" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="deptassignedeid_ctlcol" xmlns="">
                                            <b><u>Department</u></b>
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                                                id="deptassignedeid" rmxref="/Instance/Employee/DeptAssignedEid" name="deptassignedeid"
                                                cancelledvalue="" /><asp:button id="Button1" runat="server" class="CodeLookupControl"
                                                    text="..." name="deptassignedeidbtn" onclientclick="&#xA;                    selectCode('orgh','deptassignedeid','Department')&#xA;                  " /><asp:textbox
                                                        style="display: none" runat="server" id="deptassignedeid_cid" rmxref="/Instance/Employee/DeptAssignedEid/@codeid"
                                                        cancelledvalue="" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                                                            runat="server" controltovalidate="deptassignedeid" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="fulltimeflag_ctlcol" xmlns="">
                                            Full Time Employee
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="fulltimeflag" rmxref="/Instance/Employee/FullTimeFlag"
                                                tabindex="52" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="supervisoreid_ctlcol" xmlns="">
                                            Supervisor
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                                                id="supervisoreid" rmxref="/Instance/Employee/SupervisorEid" cancelledvalue=""
                                                tabindex="37" /><input type="button" class="button" value="..." name="supervisoreidbtn"
                                                    tabindex="38" onclick="lookupData('supervisoreid','EMPLOYEES',4,'supervisoreid',2)" /><asp:textbox
                                                        style="display: none" runat="server" id="supervisoreid_cid" rmxref="/Instance/Employee/SupervisorEid/@codeid"
                                                        cancelledvalue="" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="exemptstatusflag_ctlcol" xmlns="">
                                            Exempt
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="exemptstatusflag"
                                                rmxref="/Instance/Employee/ExemptStatusFlag" tabindex="53" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="paytypecode_ctlcol" xmlns="">
                                            Pay Type
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="paytypecode"
                                                rmxref="/Instance/Employee/PayTypeCode" cancelledvalue="" tabindex="39" onblur="codeLostFocus(this.id);" /><input
                                                    type="button" class="CodeLookupControl" name="paytypecodebtn" onclick="selectCode('PAY_TYPES','paytypecode')" /><asp:textbox
                                                        style="display: none" runat="server" id="paytypecode_cid" rmxref="/Instance/Employee/PayTypeCode/@codeid"
                                                        cancelledvalue="" tabindex="40" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="noofexemptions_ctlcol" xmlns="">
                                            No of Exemptions
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="numLostFocus(this);" id="noofexemptions"
                                                rmxref="/Instance/Employee/NoOfExemptions" tabindex="54" onchange="setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="payamount_ctlcol" xmlns="">
                                            Pay Amount
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="payamount" rmxref="/Instance/Employee/PayAmount"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="41" onchange=";setDataChanged(true);" /><cc1:MaskedEditExtender
                                                    runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="payamount"
                                                    ID="payamount_ajax" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="weeklyhours_ctlcol" xmlns="">
                                            Hours Per Week
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="numLostFocus(this);" id="weeklyhours"
                                                rmxref="/Instance/Employee/WeeklyHours" tabindex="55" onchange="javascript: return calculateWeeklyRate('employee');" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="hourlyrate_ctlcol" xmlns="">
                                            Hourly Rate
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="hourlyrate" rmxref="/Instance/Employee/HourlyRate"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="42" onchange="javascript: return calculateWeeklyRate('employee');;setDataChanged(true);" /><cc1:MaskedEditExtender
                                                    runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="hourlyrate"
                                                    ID="hourlyrate_ajax" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="weeklyrate_ctlcol" xmlns="">
                                            Weekly Rate
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="weeklyrate" rmxref="/Instance/Employee/WeeklyRate"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="56" onchange=";setDataChanged(true);" /><cc1:MaskedEditExtender
                                                    runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="weeklyrate"
                                                    ID="weeklyrate_ajax" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="monthlyrate_ctlcol" xmlns="">
                                            Monthly Rate
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" id="monthlyrate" rmxref="/Instance/Employee/MonthlyRate"
                                                onblur=";currencyLostFocus(this);" rmxforms:as="currency" tabindex="43" onchange=";setDataChanged(true);" /><cc1:MaskedEditExtender
                                                    runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="monthlyrate"
                                                    ID="monthlyrate_ajax" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="jobclassification_ctlcol" xmlns="">
                                            Job Class
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="jobclassification"
                                                rmxref="/Instance/Employee/JobClassCode" cancelledvalue="" tabindex="57" onblur="codeLostFocus(this.id);" /><input
                                                    type="button" class="CodeLookupControl" name="jobclassificationbtn" onclick="selectCode('JOB_CLASS','jobclassification')" /><asp:textbox
                                                        style="display: none" runat="server" id="jobclassification_cid" rmxref="/Instance/Employee/JobClassCode/@codeid"
                                                        cancelledvalue="" tabindex="58" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="noteligdisbene_ctlcol" xmlns="">
                                            Not Eligible for Disability Benefits
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="noteligdisbene"
                                                rmxref="/Instance/Employee/EligDisBenFlag" tabindex="44" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="empdisabilityoption_ctlcol" xmlns="">
                                            Disability Option
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="empdisabilityoption"
                                                rmxref="/Instance/Employee/DisOptionCode" cancelledvalue="" tabindex="59" onblur="codeLostFocus(this.id);" /><input
                                                    type="button" class="CodeLookupControl" name="empdisabilityoptionbtn" onclick="selectCode('DIS_OPTION_CODE','empdisabilityoption')" /><asp:textbox
                                                        style="display: none" runat="server" id="empdisabilityoption_cid" rmxref="/Instance/Employee/DisOptionCode/@codeid"
                                                        cancelledvalue="" tabindex="60" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" xmlns="">
                                            <asp:label runat="server" id="labelBlank" text="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" xmlns="">
                                            <asp:label runat="server" id="labelWeek" text="Work Week" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="worksunflag_ctlcol" xmlns="">
                                            Sunday
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="worksunflag" rmxref="/Instance/Employee/WorkSunFlag"
                                                tabindex="45" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="workthuflag_ctlcol" xmlns="">
                                            Thursday
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="workthuflag" rmxref="/Instance/Employee/WorkThuFlag"
                                                tabindex="61" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="workmonflag_ctlcol" xmlns="">
                                            Monday
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="workmonflag" rmxref="/Instance/Employee/WorkMonFlag"
                                                tabindex="46" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="workfriflag_ctlcol" xmlns="">
                                            Friday
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="workfriflag" rmxref="/Instance/Employee/WorkFriFlag"
                                                tabindex="62" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="worktueflag_ctlcol" xmlns="">
                                            Tuesday
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="worktueflag" rmxref="/Instance/Employee/WorkTueFlag"
                                                tabindex="47" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="worksatflag_ctlcol" xmlns="">
                                            Saturday
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="worksatflag" rmxref="/Instance/Employee/WorkSatFlag"
                                                tabindex="63" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="workwedflag_ctlcol" xmlns="">
                                            Wednesday
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="workwedflag" rmxref="/Instance/Employee/WorkWedFlag"
                                                tabindex="48" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="display: none;" name="FORMTABemployeeinfo" id="FORMTABemployeeinfo">
                                    <tr>
                                        <td nowrap="true" id="driverslicno_ctlcol" xmlns="">
                                            License Number
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="driverslicno"
                                                rmxref="/Instance/Employee/DriversLicNo" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="emailtypecode_ctlcol" xmlns="">
                                            EMail Type
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="emailtypecode"
                                                rmxref="/Instance/Employee/EmployeeEntity/EmailTypeCode" cancelledvalue="" tabindex="81"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="emailtypecodebtn" onclick="selectCode('EMAIL_TYPE','emailtypecode')" /><asp:textbox
                                                        style="display: none" runat="server" id="emailtypecode_cid" rmxref="/Instance/Employee/EmployeeEntity/EmailTypeCode/@codeid"
                                                        cancelledvalue="" tabindex="82" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="driverslictypecode_ctlcol" xmlns="">
                                            License Type
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="driverslictypecode"
                                                rmxref="/Instance/Employee/Driverslictypecode" cancelledvalue="" tabindex="66"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="driverslictypecodebtn" onclick="selectCode('LICENSE_TYPE_CODE','driverslictypecode')" /><asp:textbox
                                                        style="display: none" runat="server" id="driverslictypecode_cid" rmxref="/Instance/Employee/Driverslictypecode/@codeid"
                                                        cancelledvalue="" tabindex="67" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="emailaddress_ctlcol" xmlns="">
                                            EMail Address
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="emailaddress"
                                                rmxref="/Instance/Employee/EmployeeEntity/EmailAddress" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="drivlicstate_ctlcol" xmlns="">
                                            License Jurisdiction
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="drivlicstate"
                                                rmxref="/Instance/Employee/DrivlicState" cancelledvalue="" tabindex="68" onblur="codeLostFocus(this.id);" /><input
                                                    type="button" class="CodeLookupControl" name="drivlicstatebtn" onclick="selectCode('STATES','drivlicstate')" /><asp:textbox
                                                        style="display: none" runat="server" id="drivlicstate_cid" rmxref="/Instance/Employee/DrivlicState/@codeid"
                                                        cancelledvalue="" tabindex="69" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="costcentercode_ctlcol" xmlns="">
                                            Cost Center
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="costcentercode"
                                                rmxref="/Instance/Employee/EmployeeEntity/CostCenterCode" cancelledvalue="" tabindex="84"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="costcentercodebtn" onclick="selectCode('COST_CENTER','costcentercode')" /><asp:textbox
                                                        style="display: none" runat="server" id="costcentercode_cid" rmxref="/Instance/Employee/EmployeeEntity/CostCenterCode/@codeid"
                                                        cancelledvalue="" tabindex="85" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="datedriverslicexp_ctlcol" xmlns="">
                                            Expiration Date
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="datedriverslicexp" rmxref="/Instance/Employee/DateDriverslicexp" tabindex="70" /><cc1:CalendarExtender
                                                    runat="server" ID="datedriverslicexp_ajax" TargetControlID="datedriverslicexp" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="workpermitnumber_ctlcol" xmlns="">
                                            Work Permit #
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="setDataChanged(true);" id="workpermitnumber"
                                                rmxref="/Instance/Employee/WorkPermitNumber" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="drivlicrstrctcode_ctlcol" xmlns="">
                                            Restrictions
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="drivlicrstrctcode"
                                                rmxref="/Instance/Employee/DrivlicRstrctcode" cancelledvalue="" tabindex="72"
                                                onblur="codeLostFocus(this.id);" /><input type="button" class="CodeLookupControl"
                                                    name="drivlicrstrctcodebtn" onclick="selectCode('LIC_RESTRICTION','drivlicrstrctcode')" /><asp:textbox
                                                        style="display: none" runat="server" id="drivlicrstrctcode_cid" rmxref="/Instance/Employee/DrivlicRstrctcode/@codeid"
                                                        cancelledvalue="" tabindex="73" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="workpermitdate_ctlcol" xmlns="">
                                            Work Permit Date
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="workpermitdate" rmxref="/Instance/Employee/WorkPermitDate" tabindex="87" /><cc1:CalendarExtender
                                                    runat="server" ID="workpermitdate_ajax" TargetControlID="workpermitdate" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="ncciclasscode_ctlcol" xmlns="">
                                            NCCI Class
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onchange="lookupTextChanged(this);" id="ncciclasscode"
                                                rmxref="/Instance/Employee/NcciClassCode" cancelledvalue="" tabindex="74" onblur="codeLostFocus(this.id);" /><input
                                                    type="button" class="CodeLookupControl" name="ncciclasscodebtn" onclick="selectCode('NCCI_CLASS_CODE','ncciclasscode')" /><asp:textbox
                                                        style="display: none" runat="server" id="ncciclasscode_cid" rmxref="/Instance/Employee/NcciClassCode/@codeid"
                                                        cancelledvalue="" tabindex="75" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td nowrap="true" id="numofviolations_ctlcol" xmlns="">
                                            Number of Violations
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" onblur="numLostFocus(this);" id="numofviolations"
                                                rmxref="/Instance/Employee/NumOfViolations" tabindex="89" onchange="setDataChanged(true);" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="lastverifieddate_ctlcol" xmlns="">
                                            Date Last Verified
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="lastverifieddate" rmxref="/Instance/Employee/LastVerifiedDate" tabindex="76" /><cc1:CalendarExtender
                                                    runat="server" ID="lastverifieddate_ajax" TargetControlID="lastverifieddate" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="dateofdeath_ctlcol" xmlns="">
                                            Date of Death
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:textbox runat="server" size="30" formatas="date" onchange="setDataChanged(true);"
                                                id="dateofdeath" rmxref="/Instance/Employee/DateOfDeath" tabindex="78" /><cc1:CalendarExtender
                                                    runat="server" ID="dateofdeath_ajax" TargetControlID="dateofdeath" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="true" id="insurableflag_ctlcol" xmlns="">
                                            Insurable
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <asp:checkbox onchange="setDataChanged(true);" runat="Server" id="insurableflag"
                                                rmxref="/Instance/Employee/InsurableFlag" tabindex="80" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="display: none;" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
                                    <tr>
                                        <asp:textbox style="display: none" runat="server" id="supp_employee_eid" rmxref="/Instance/*/Supplementals/EMPLOYEE_EID" /></tr>
                                    <tr>
                                        <td nowrap="true" id="supp_ent_supp_da_memo_ctlcol" xmlns="">
                                            enter supp
                                        </td>
                                        <td nowrap="true" xmlns="">
                                            <textarea cols="" rows="" id="supp_ent_supp_da_memo" rmxref="/Instance/*/Supplementals/ENT_SUPP_DA_MEMO"
                                                name="supp_ent_supp_da_memo" onchange="setDataChanged(true);"></textarea><input type="button"
                                                    class="button" value="..." name="supp_ent_supp_da_memobtnMemo" onclick="EditMemo('supp_ent_supp_da_memo')" />
                                        </td>
                                        <td xmlns="">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" />
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:button id="Button2" class="button" runat="Server" text="Violations" onclientclick="&#xA;            XFormHandler('','SysFormName=violation&SysCmd=1&SysEx=/Instance/Employee/EmployeeEid','','')&#xA;          " /><asp:button
                                id="Button3" class="button" runat="Server" text="Dependents" onclientclick="&#xA;            XFormHandler('','SysFormName=dependent&SysCmd=1&SysEx=/Instance/Employee/EmployeeEid | /Instance/Employee/EmployeeEntity/LastName','','')&#xA;          " />
                        </td>
                    </tr>
                </table>
                <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
                    text="" />
                <asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                    text="14500" />
                <asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave" rmxref="Instance/UI/FormVariables/SysCmdConfirmSave"
                    text="" />
                <asp:textbox style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                    text="" />
                <asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText"
                    text="Navigate" />
    </form>
</body>
</html>
