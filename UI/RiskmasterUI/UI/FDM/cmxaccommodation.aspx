<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxaccommodation.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Cmxaccommodation" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Accommodations</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">
          {var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">
          {var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" />
    <asp:hiddenfield runat="server" id="wsrp_rewrite_action_1" value="" />
    <asp:textbox style="display: none" runat="server" name="hTabName" id="hTabName" />
    <asp:scriptmanager id="SMgr" runat="server" />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
            <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
            <asp:imagebutton runat="server" onclientclick="attach()" src="../../Images/attach.gif"
                width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                onmouseover="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/attach.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
            <asp:imagebutton runat="server" onclientclick="FilteredDiary()" src="../../Images/filtereddiary.gif"
                width="28" height="28" border="0" id="filtereddiary" alternatetext="View Record Diaries"
                onmouseover="this.src='../../Images/filtereddiary2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/filtereddiary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
            <asp:imagebutton runat="server" onclientclick="Diary()" src="../../Images/diary.gif"
                width="28" height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='../../Images/diary2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/diary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
            <asp:imagebutton runat="server" onclientclick="recordSummary()" src="../../Images/recordsummary.gif"
                width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" />
        </div>
    </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Accommodations" />
        <asp:label id="formsubtitle" runat="server" text="" />
    </div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" />
    </div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSCaseMngHis" id="TABSCaseMngHis">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="CaseMngHis"
                id="LINKTABSCaseMngHis"><span style="text-decoration: none">Accommodations</span>
            </a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span>
            </a>
        </div>
        <div class="tabSpace">
            &nbsp;&nbsp;
        </div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABCaseMngHis" id="FORMTABCaseMngHis">
        <asp:textbox style="display: none" runat="server" id="cmaccommrowid" rmxref="/Instance/CmXAccommodation/CmAccommRowId"
            rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="cmrowid" rmxref="/Instance/CmXAccommodation/CmRowId"
            rmxtype="id" />
            <asp:TextBox runat="server" id="casemgtrowid" RMXRef="/Instance/CmXAccommodation/CasemgtRowId" style="display:none"/>
        <div runat="server" class="half" id="div_accommstatus" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommstatus" text="Accomm. Status" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="accommstatus"
                    rmxref="/Instance/CmXAccommodation/AccommStatus" rmxtype="code" cancelledvalue=""
                    tabindex="1" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="accommstatusbtn" onclientclick="return selectCode('ACCOMM_STATUS','accommstatus');"
                    tabindex="2" />
                <asp:textbox style="display: none" runat="server" id="accommstatus_cid" rmxref="/Instance/CmXAccommodation/AccommStatus/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommrequested" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommrequested" text="Accomm. Requested" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);" id="accommrequested"
                    rmxref="/Instance/CmXAccommodation/AccommRequested" rmxtype="date" tabindex="13" />
                <cc1:calendarextender runat="server" id="accommrequested_ajax" targetcontrolid="accommrequested" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommscope" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommscope" text="Accomm. Scope" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="accommscope"
                    rmxref="/Instance/CmXAccommodation/AccommScope" rmxtype="code" cancelledvalue=""
                    tabindex="3" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="accommscopebtn" onclientclick="return selectCode('ACCOMM_SCOPE','accommscope');"
                    tabindex="4" />
                <asp:textbox style="display: none" runat="server" id="accommscope_cid" rmxref="/Instance/CmXAccommodation/AccommScope/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommoffered" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommoffered" text="Accomm. Offered" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);" id="accommoffered"
                    rmxref="/Instance/CmXAccommodation/AccommOffered" rmxtype="date" tabindex="15" />
                <cc1:calendarextender runat="server" id="accommoffered_ajax" targetcontrolid="accommoffered" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommtype" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommtype" text="Accomm. Type" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="accommtype" rmxref="/Instance/CmXAccommodation/AccommType"
                    rmxtype="code" cancelledvalue="" tabindex="5" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="accommtypebtn" onclientclick="return selectCode('ACCOMM_TYPE','accommtype');"
                    tabindex="6" />
                <asp:textbox style="display: none" runat="server" id="accommtype_cid" rmxref="/Instance/CmXAccommodation/AccommType/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommaccepted" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommaccepted" text="Accomm. Accepted" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);" id="accommaccepted"
                    rmxref="/Instance/CmXAccommodation/AccommAccepted" rmxtype="date" tabindex="17" />
                <cc1:calendarextender runat="server" id="accommaccepted_ajax" targetcontrolid="accommaccepted" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommposition" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommposition" text="Position" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="accommposition"
                    rmxref="/Instance/CmXAccommodation/AccommPosition" rmxtype="code" cancelledvalue=""
                    tabindex="7" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="accommpositionbtn" onclientclick="return selectCode('POSITIONS','accommposition');"
                    tabindex="8" />
                <asp:textbox style="display: none" runat="server" id="accommposition_cid" rmxref="/Instance/CmXAccommodation/AccommPosition/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_netaccommcost" xmlns="">
            <asp:label runat="server" class="label" id="lbl_netaccommcost" text="Net Cost of Accomm." />
            <span class="formw">
                <asp:textbox runat="server" onblur="numLostFocus(this);" id="netaccommcost" rmxref="/Instance/CmXAccommodation/NetAccommCost"
                    rmxtype="numeric" tabindex="19" onchange="setDataChanged(true);" />
            </span>
        </div>
        <div runat="server" class="half" id="div_jobclasscode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_jobclasscode" text="Job Classification" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="jobclasscode"
                    rmxref="/Instance/CmXAccommodation/JobClassCode" rmxtype="code" cancelledvalue=""
                    tabindex="9" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="jobclasscodebtn" onclientclick="return selectCode('JOB_CLASS','jobclasscode');"
                    tabindex="10" />
                <asp:textbox style="display: none" runat="server" id="jobclasscode_cid" rmxref="/Instance/CmXAccommodation/JobClassCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommdepartment" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommdepartment" text="Department" />
            <span class="formw">
                <asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                    id="accommdepartment" rmxref="/Instance/CmXAccommodation/AccommDepartment" rmxtype="orgh"
                    name="accommdepartment" cancelledvalue="" />
                <asp:button runat="server" class="CodeLookupControl" text="..." id="accommdepartmentbtn"
                    onclientclick="return selectCode('orgh','accommdepartment','Department');" />
                <asp:textbox style="display: none" runat="server" id="accommdepartment_cid" rmxref="/Instance/CmXAccommodation/AccommDepartment/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommrestriction" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommrestriction" text="Restrictions" />
            <span class="formw">
                <asp:textbox runat="Server" id="accommrestriction" rmxref="/Instance/CmXAccommodation/AccommRestriction"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="11" textmode="MultiLine" columns="30" rows="5" />
                <input type="button" class="button" value="..." name="accommrestrictionbtnMemo" tabindex="12"
                    id="accommrestrictionbtnMemo" onclick="EditMemo('accommrestriction','')" />
            </span>
        </div>
        <div runat="server" class="half" id="div_accommtext" xmlns="">
            <asp:label runat="server" class="label" id="lbl_accommtext" text="Accommodations" />
            <span class="formw">
                <asp:textbox runat="Server" id="accommtext" rmxref="/Instance/CmXAccommodation/AccommText"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="23" textmode="MultiLine" columns="30" rows="5" />
                <input type="button" class="button" value="..." name="accommtextbtnMemo" tabindex="24"
                    id="accommtextbtnMemo" onclick="EditMemo('accommtext','')" />
            </span>
        </div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_cm_accomm_row_id" rmxref="/Instance/*/Supplementals/CM_ACCOMM_ROW_ID"
            rmxtype="id" />
    </div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnToCmXAccommodation">
            <asp:button class="button" runat="Server" id="btnToCmXAccommodation" text="Back to Accommodations List"
                onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"
                postbackurl="?SysViewType=controlsonly&SysCmd=1" />
        </div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave" rmxref="Instance/UI/FormVariables/SysCmdConfirmSave"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText"
        rmxtype="hidden" text="Navigate" />
    <asp:textbox style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
        rmxtype="hidden" text="CmXAccommodation" />
    <asp:textbox style="display: none" runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
        rmxtype="hidden" text="&lt;CmXAccommodation&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/CmXAccommodation&gt;" />
    <asp:textbox style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
        rmxtype="hidden" text="11150" />
    <asp:textbox style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
        rmxtype="hidden" text="CmXAccommodation" />
    <asp:textbox style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
        rmxtype="hidden" text="cmaccommrowid" />
    <asp:textbox style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName"
        rmxtype="hidden" text="cmrowid" />
    <asp:textbox style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
        rmxtype="hidden" text="eventnumber,claimnumber" />
    <asp:textbox style="display: none" runat="server" name="formname" value="CmXAccommodation" />
    <asp:textbox style="display: none" runat="server" name="SysRequired" value="" />
    <asp:textbox style="display: none" runat="server" name="SysFocusFields" value="accommstatus|" />
    <input type="hidden" id="hdSaveButtonClicked" />
    <asp:textbox runat="server" id="SysInvisible" style="display: none" />
    <asp:textbox runat="server" id="SysLookupClass" style="display: none" />
    <asp:textbox runat="server" id="SysLookupRecordId" style="display: none" />
    <asp:textbox runat="server" id="SysLookupAttachNodePath" style="display: none" />
    <asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" />
    <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
    <asp:textbox style="display: none" runat="server" id="eventnumber" rmxref="/Instance/UI/FormVariables/SysExData/EventNumber" rmxtype="id" />
    </form>
</body>
</html>
