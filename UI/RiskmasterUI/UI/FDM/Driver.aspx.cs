﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;

// created by mbahl3
namespace Riskmaster.UI.FDM
{

    public partial class Driver : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();

        }

        /// <summary>
        /// This function has been writhen to control the DriverType value for power views.
        /// Rahul Aggarwal
        /// RMA-19560 
        /// </summary>
        /// <param name="Xelement"></param>
        public override void ModifyXml(ref XElement Xelement)
        {
            string sFormId = string.Empty;
            XElement objElement = Xelement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/DriverType");
            Control ctl1 = Page.FindControl("DriverRowId");
            if(ctl1 != null)
            {
                TextBox tb = (TextBox)ctl1;
                sFormId = tb.Text;
            }


            if (objElement != null && (objElement.Value == "0" || string.IsNullOrEmpty(sFormId) || sFormId.Equals("0")))
            {
                Control ctl2 = Page.FindControl("entitytableid");
                if (ctl2 != null)
                {
                    DropDownList ddList = (DropDownList)ctl2;

                    if (ddList != null)
                        objElement.Value = ddList.SelectedValue;
                }
            }
        
        }
    }
}


