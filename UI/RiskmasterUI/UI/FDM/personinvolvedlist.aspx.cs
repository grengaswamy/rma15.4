﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.FDM
{
    public partial class PersoninvolvedList : FDMBasePage
    {
        public IEnumerable result = null;
        public IEnumerable resultPiType = null;
        public IEnumerable resultSysEx = null;
        public IEnumerable resultRoleTableName = null;
        public XElement rootElement = null;

        public string sScreenName = "";
        public bool sShowAddButton = false;
        public bool sShowDeleteButton = false;
        public string sFormTitle = string.Empty;
        XmlDocument oFDMPageDom = null;
        public IEnumerable levelresult = null;
        public XElement levelrootElement = null;
        string sReturn = "";
        public string sLevel = "EVENT LEVEL";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string PageId = RMXResourceProvider.PageId("personinvolvedlist.aspx"); //add ttumula2 for RMA-7829
                LoadInitialPage();

                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/PersonInvolvedList/PersonInvolved")
                         select c;

                resultPiType = from c in rootElement.XPathSelectElements("//Param/FormVariables/SysExData/PersonInvolvedTypeList")
                               select c;
                // jira 17807 pgupta215
                resultRoleTableName = from c in rootElement.XPathSelectElements("//Param/FormVariables/SysExData/RoleTableName")
                                      select c;

                //MITS 15918 : Umesh
                resultSysEx = from c in rootElement.XPathSelectElements("//Param/FormVariables/SysExData/EntityTableName")
                              select c;
                if (rootElement.XPathSelectElement("//PersonInvolved/EventId") != null)
                {
                    eventid.Text = rootElement.XPathSelectElement("//PersonInvolved/EventId").Value;
                }
                if(rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Add")!=null)
                {

                    sShowAddButton = Convert.ToBoolean(rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Add").Attribute("type").Value);
                }
                if(rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Delete")!=null)
                {

                    sShowDeleteButton = Convert.ToBoolean(rootElement.XPathSelectElement("//Param/FormVariables/SysExData/Delete").Attribute("type").Value);
                }
                if(rootElement.XPathSelectElement("//EventNumber")!=null)
                {
                    sFormTitle="Persons Involved for Event:" + rootElement.XPathSelectElement("//EventNumber").Value;
                }
                else if (rootElement.XPathSelectElement("//ClaimNumber") != null)
                {
                    sFormTitle = "Persons Involved for Claim:" + rootElement.XPathSelectElement("//ClaimNumber").Value;
                }

                    //XElement oMessageElement = GetMessageTemplate();

                    //XElement oElement = oMessageElement.XPathSelectElement("./Document/GetPILevel/EventId");
                    //if (oElement != null)
                    //{
                    //    oElement.Value = eventid.Text;

                    //}

                    //XElement oPageIdElement = oMessageElement.XPathSelectElement("./Document/GetPILevel/PageId"); //add ttumula2 for RMA-7829
                    //if (oPageIdElement != null)
                    //{
                    //    oPageIdElement.Value = PageId;



                    //Calling Service to get all PreBinded Data 
                    //sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                    //oFDMPageDom = new XmlDocument();
                    //oFDMPageDom.LoadXml(sReturn);
                    //XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    //XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    //oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    //levelrootElement = XElement.Parse(oFDMPageDom.OuterXml);
                    //levelresult = from c in levelrootElement.XPathSelectElements("//PersonInvolvedLevel/PILevel")
                    //         select c;
                }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>b9fcbb13-9b1d-4a29-b216-42b069b87a52</Authorization> 
                     <Call>
                        <Function>ListPersonInvolvedAdaptor.GetPILevel</Function> 
                     </Call>
                     <Document>
                        <GetPILevel>
                            <EventId></EventId> 
                            <PageId></PageId>
                            <ParentRowId></ParentRowId>
                            <ParentTableName></ParentTableName>
                        </GetPILevel>
                    </Document>
            </Message>

            ");

            return oTemplate;
        }

        protected void NavigateListDelete(object sender, EventArgs e)
        {
            try
            {
                base.NavigateListDelete(sender, e);
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/PersonInvolvedList/PersonInvolved")
                         let xPersonInvolved = (int)Convert.ToInt64(c.Element("PiRowId").Value)
                         orderby xPersonInvolved
                         select c;
                if (rootElement.XPathSelectElement("//PersonInvolved/EventId") != null)
                {
                    eventid.Text = rootElement.XPathSelectElement("//PersonInvolved/EventId").Value;
                }

                //Raman: This call is not needed now
                //LoadInitialPage();

            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

        protected void btnPI_Click(object sender, EventArgs e)
        {
            sScreenName = "AddPersonInvolved";
        }

    }
}

