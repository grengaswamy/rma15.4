﻿/// <summary>
/// Author - Saurav Mahajan
/// Date - 11/26/09
/// MITS# - 18230
/// Description - Property Claim Schedule Info tab.
/// </summary>

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.Common;

namespace Riskmaster.UI.FDM
{
    public partial class Claimpc : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            //Start jramkumar MITS 34419
            if (IsPostBack)
            {
                string txtFocusScript = "var focusField=false; if((document.getElementById('ev_depteid') != null && document.getElementById('ev_depteid').value != '') || (document.getElementById('ev_countrycode_codelookup') != null && document.getElementById('ev_countrycode_codelookup').value != '')) { focusField=true; }";
                this.ClientScript.RegisterStartupScript(this.GetType(), "pc", txtFocusScript, true);
            }
            //End jramkumar MITS 34419
            //added by gmallick for JIRA -12965 Start
            Button claimstatuscodedetail = this.Form.FindControl("claimstatuscodedetailbtn") as Button;
            if (claimstatuscodedetail != null)
            {
                claimstatuscodedetail.Attributes.Add("Visible", "True");
                claimstatuscodedetail.Enabled = true;
                claimstatuscodedetail.Attributes.Add("onclientclick", "return selectCodeWithDetail('claimstatuscode',1);");
                claimstatuscodedetail.CssClass = "ClaimStatusButton";
            }
			//added by gmallick for JIRA -12965 End
            
        }
        //Neha Suresh Jain, 05/25/2010, MITS 20811
        /// <summary>
        /// Override base and generates claim letter if claim is found eligible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {
                int iClaimId = 0;
                string sCLMLtrTmplType = string.Empty;
                string sClaimNo = string.Empty;
                bool bIsSuccess = false;
                base.NavigateSave(sender, e);
                if (this.Form.FindControl("claimid") != null)
                {
                    //iClaimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);
                    iClaimId = Conversion.CastToType<int>(((TextBox)this.Form.FindControl("claimid")).Text, out bIsSuccess);
                }
                if (this.Form.FindControl("ClaimLetterTmplId") != null)
                {
                    //sCLMLtrTmplType = Convert.ToString(((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text);
                    sCLMLtrTmplType = ((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text;
                }
                if (this.Form.FindControl("claimnumber") != null)
                {
                    //sClaimNo = Convert.ToString(((TextBox)this.Form.FindControl("claimnumber")).Text);
                    sClaimNo = ((TextBox)this.Form.FindControl("claimnumber")).Text;
                }
                ////TEMP BASIS - HARD CODING
                //sCLMLtrTmplType = "CL";
                ////TEMP BASIS- HARD CODING
                if (sCLMLtrTmplType == "ACK" || sCLMLtrTmplType == "CL")
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClaimLetterMerge", "DoClaimLetterMerge(" + iClaimId + ",'" + sCLMLtrTmplType + "','" + sClaimNo + "');", true);
                //this.ClientScript.IsClientScriptBlockRegistered(this.GetType(), "ClaimLetterMerge");
                sCLMLtrTmplType = string.Empty;

                // rrachev JIRA 5021 Get current value of ContainsOpenDiaries
                Control openDiaries = this.FindControl("containsopendiaries");
                if (openDiaries != null && this.Data != null && !String.IsNullOrEmpty(this.Data.OuterXml))
                {
                    BindData2Control(XElement.Parse(this.Data.OuterXml), new Control[] { openDiaries });
                }
            }
            catch (Exception ex)
            {

            }
            finally { }
        }

        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oUseAdvancedClaim = oMessageElement.XPathSelectElement("//UseAdvancedClaim");
            Control oPolicyControl = this.FindControl("div_primarypolicyid");
            if (oUseAdvancedClaim != null)//Deb : MITS 25242
            {
                if (oUseAdvancedClaim.Value == "-1")
                {
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
                else
                {
                    oPolicyControl = this.FindControl("div_multipolicyid");
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
            }
            //MITS 14169:To open Policy from a claim:Setting client click attribute of _open button:Start
            int iPolicyId = 0;
            int iEnhPolicyId = 0;
            Control oControl = null;
            bool bIsSuccess = false;
            XElement oIsPolicyFilter = oMessageElement.XPathSelectElement("//IsPolicyFilter");
            XElement oUseEnhPolFlag = oMessageElement.XPathSelectElement("//UseEnhPolFlag");
            Button btnPolicyOpen = (Button)(this.Form.FindControl("primarypolicyid_open"));
            if (btnPolicyOpen != null)
            {
                XElement oPolicyId = oMessageElement.XPathSelectElement("//PrimaryPolicyId");
                if (oPolicyId != null)
                {
                    //iPolicyId = Convert.ToInt32(oPolicyId.Value);
                    iPolicyId = Conversion.CastToType<int>(oPolicyId.Value, out bIsSuccess);
                }

                XElement oPolicyEnhId = oMessageElement.XPathSelectElement("//PrimaryPolicyIdEnh");
                if (oPolicyEnhId != null)
                {
                    //iEnhPolicyId = Convert.ToInt32(oPolicyEnhId.Value);
                    iEnhPolicyId = Conversion.CastToType<int>(oPolicyEnhId.Value, out bIsSuccess);
                }

                if (oUseEnhPolFlag != null)
                {
                    if (oUseEnhPolFlag.Value == "0")
                    {

                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";

                    }
                    else if (oUseEnhPolFlag.Value == "-1")
                    {

                        //Start-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.

                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policyenhpc'); return false;";
                        //End-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.

                    }

                }
            }
            btnPolicyOpen = (Button)(this.Form.FindControl("multipolicyid_open"));
            if (btnPolicyOpen != null)
            {
                if (btnPolicyOpen != null)
                {
                    btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                }
            }
            //MITS 14169:End

            //smahajan6 - 12/01/2010 : Start

            //If Enhanced Policy(PC Claim) and Policy Filter is checked , Modify Button OnClientClick event
            if (oUseEnhPolFlag != null && oIsPolicyFilter != null)
            {
                if (oUseEnhPolFlag.Value == "-1" && oIsPolicyFilter.Value == "-1")
                {
                    oControl = this.FindControl("pi_propertyidbtn");
                    if (oControl != null)
                    {
                        ((Button)oControl).OnClientClick = "return PropSelect();";
                        ((Button)oControl).CssClass = "CodeLookupControl";
                    }
                }
            }

            //For Read-only textbox controls , values becomes blank on postback 
            //Removed the web control read only attribute (if there) and addded HTML read only attribute
            //Start: Neha Suresh Jain,06/04/2010,When we create a Powerview and edit the form as readonly form, Unable to cast object of type 
            //'System.Web.UI.WebControls.Label' to type 'System.Web.UI.WebControls.TextBox' exception occurs. Thus, type of the control compared before conversion
            oControl = this.FindControl("pi_propertyid");
            if (oControl != null)
            {
                if (oControl.GetType().FullName == "System.Web.UI.WebControls.TextBox")
                {
                    ((TextBox)oControl).Attributes.Add("readonly", "readonly");
                    ((TextBox)oControl).BackColor = System.Drawing.Color.FromName("#f2f2f2");
                }
            }

            oControl = this.FindControl("pi_description");
            if (oControl != null)
            {
                if (oControl.GetType().FullName == "System.Web.UI.WebControls.TextBox")
                {
                    if (((TextBox)oControl).ReadOnly)
                    {
                        ((TextBox)oControl).ReadOnly = false;
                        ((TextBox)oControl).Attributes.Remove("readonly");
                        ((TextBox)oControl).Attributes.Add("readonly", "readonly");
                    }
                }
            }
            //End: Neha Suresh Jain
            //smahajan6 - 12/01/2010 : End
        }
    }
}
