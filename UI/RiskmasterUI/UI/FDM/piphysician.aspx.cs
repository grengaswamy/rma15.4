﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.FDM
{
    public partial class Piphysician : FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();

            //If existing physician has been added then datachange flag has to be true
            if (AppHelper.GetQueryStringValue("DataChange") == "true")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "datachangescript", "<script type='text/javascript'>setDataChanged(true);</script>");
            }
        }
    }
}
