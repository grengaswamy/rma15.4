﻿    /**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/16/2013 | 34082   | pgupta93   | Changes req for MultiCurrency
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.AppHelpers;
using System.Text;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common;
using System.Globalization;
using System.Threading;
using Riskmaster.BusinessHelpers;
using System.Web.Services;//MITS:34082 MultiCurrency 
using System.Web.Script.Services;//MITS:34082 MultiCurrency 
using Riskmaster.Security;
using Riskmaster.Models;//MITS:34082 MultiCurrency 

namespace Riskmaster.UI.FDM
{
    public partial class Split : NonFDMBasePageCWS
    {
        //Variables dectlare for the new setting UseResFilter in utility--shilpi 05/15/09
        bool bReturnStatus = false;
        string sreturnValue = "";
        XElement XmlTemplate = null;
        XElement XmlTemplateForCheckDupCriteria = null; // MITS 32087 - nnithiyanand
        //ends here--shilpi
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument XmlDoc=new XmlDocument();   // MITS 32087 - nnithiyanand
            string sNode = null;					// MITS 32087 - nnithiyanand
            //JIRA 17537
            CheckBox chkFirstFinalPayment = (CheckBox)DatabindingHelper.FindControlRecursive(this.Form, "FirstFinalPayment");
            if (chkFirstFinalPayment != null)
            {
                chkFirstFinalPayment.Enabled = false;
            }
            //JIRA 17537:end

            MakeWebServiceCall(); //Call to the function to make a hit to the web service for the UseResFilter setting in Utility--stara Mits 16667 05/15/09
            //34082 Start
            string sCurrCulture = string.Empty;
            TextBox hdcurrencytype = (TextBox)this.FindControl("currencytype");
            if ((hdcurrencytype != null) && (!string.IsNullOrEmpty(hdcurrencytype.Text)))
            {
                sCurrCulture = hdcurrencytype.Text;

                if (!string.IsNullOrEmpty(sCurrCulture))
                {
                    Culture = sCurrCulture.Split('|')[1];
                    UICulture = sCurrCulture.Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(sCurrCulture.Split('|')[1]);
                }
            }
            //34082 End

            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes

            if (!IsPostBack)
            {
				// MITS 32087 - nnithiyanand -Starts
				
                XmlTemplateForCheckDupCriteria = GetMessageTemplateForCriterea();
                bReturnStatus = CallCWS("CheckOptionsAdaptor.GetDuplicateCriteria", XmlTemplateForCheckDupCriteria, out sreturnValue, false, false);
                XmlDoc.LoadXml(sreturnValue);
                sNode = XmlDoc.SelectSingleNode("//ResultMessage/Document/GetCriteria/control[@name='DupCriteria']").InnerText;
                Control objDupCriteria = this.FindControl("DupCriteria");
                if (objDupCriteria != null)
                {
                    ((TextBox)objDupCriteria).Text = sNode;
                }
				// MITS 32087 - nnithiyanand -Ends
				
                //srajindersin Security fix MITS 35888 dt 4/14/2014
                TextBox txtSysSerialization = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "SysSerializationConfig");
                txtSysSerialization.Text = AppHelper.HTMLCustomEncode(txtSysSerialization.Text);
                TextBox txtGridName = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "gridname");
                txtGridName.Text = Request.QueryString["gridname"];
                TextBox txtmode = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "mode");
                txtmode.Text = Request.QueryString["mode"];
                TextBox txtselectedrowposition = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "selectedrowposition");
                txtselectedrowposition.Text = Request.QueryString["selectedrowposition"];
                //Commented bu Shivendu for MITS 17520
                //if (string.Compare(txtmode.Text, "add", true) != 0)
                //{
                    this.Form.Style.Add("display", "none");
                //}
                //rsushilaggar MITS 25353 Date 07/06/2011
                Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
                //Deb MITS 26077
                if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                {
                    ((CodeLookUp)objReserveTypeCode).Enabled = false;
                }
                //Deb MITS 26077
                TextBox objclaimid = (TextBox)this.FindControl("claimid");
                if (objclaimid != null)//ajohari2 FDM PowerView Fixes
                {
                    objclaimid.Text = AppHelper.GetQueryStringValue("claimid");
                }
                //34082 Start
                TextBox objExchangeRate = (TextBox)this.FindControl("ExchangeRate");
                TextBox objExchangeRateDate = (TextBox)this.FindControl("ExchangeRateDate");
                if (objExchangeRateDate != null)//ajohari2 FDM PowerView Fixes
                {
                    objExchangeRateDate.Text = DateTime.Now.Date.ToString().Split(' ')[0].Replace('.', '/');
                }
                if (objExchangeRate != null)//ajohari2 FDM PowerView Fixes
                {
                    objExchangeRate.Text = "1";
                }
                TextBox PaymenmtCurrencyType = (TextBox)this.FindControl("PaymenmtCurrencyType");
                if (PaymenmtCurrencyType != null)//ajohari2 FDM PowerView Fixes
                {
                    PaymenmtCurrencyType.Text = AppHelper.GetQueryStringValue("pmtcurrencytype");
                }
                //34082 End

                // Fix for 14498
                if (this.FindControl("IsManualDed") != null && AppHelper.GetQueryStringValue("IsManDed") != null)
                {
                    //string sReturnManualDedDetails = string.Empty;
                    ((TextBox)this.FindControl("IsManualDed")).Text = AppHelper.GetQueryStringValue("IsManDed");
                    if (AppHelper.GetQueryStringValue("IsManDed") == "1")
                    {
                        HideOverrideDeductible();
                    }

                }
            }
            else
            {
                this.Form.Style.Remove("display");
                TextBox txtPostBackParent = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "txtPostBack");
                txtPostBackParent.Text = "Done";

                //Check if the postback is initiated by button click
                Control c = DatabindingHelper.GetPostBackControl(this.Page);
                if (c == null || c.ID=="Policy")
                {
                    TextBox txtDataParent = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "txtData");
                    string sXml = txtDataParent.Text;
                    sXml = sXml.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'").Replace("&amp;amp;", "&amp;"); //Parijat :Showing &amp; in place of & while edit
                    //rupal:start, for first & final payment
                    //we need to refresh controls on page if First Final flag is changed (when FirstFinalpayment checkbox checked or unchecked)
                   
                    //Raman: When Policy dropdown changes value then we should wipe out the coverage , transaction type and reserve type values
                    if(c!= null && c.ID =="Policy")
                    {
                        XElement objElement = XElement.Parse(sXml);

                        XNode objXCoverageTypeCodeNode = objElement.XPathSelectElement("//CoverageTypeCode");
                        if (objXCoverageTypeCodeNode != null)
                            objXCoverageTypeCodeNode.Remove();

                        XNode objXDisabilityCatCodeNode = objElement.XPathSelectElement("//DisabilityCatCode");
                        if (objXDisabilityCatCodeNode != null)
                            objXDisabilityCatCodeNode.Remove();
                        XNode objXLossTypeCodeNode = objElement.XPathSelectElement("//LossTypeCode");
                        if (objXLossTypeCodeNode != null)
                            objXLossTypeCodeNode.Remove();

                        XNode objXReserveTypeCodeNode = objElement.XPathSelectElement("//ReserveTypeCode");
                        if (objXReserveTypeCodeNode != null)
                            objXReserveTypeCodeNode.Remove();

                        XNode objXTransTypeCodeNode = objElement.XPathSelectElement("//TransTypeCode");
                        if (objXTransTypeCodeNode != null)
                            objXTransTypeCodeNode.Remove();

                        //MITS:34082 MultiCurrency START
                        XNode objXCurrencyTypeForClaimNode = objElement.XPathSelectElement("//SplitCurrCode");
                        if (objXCurrencyTypeForClaimNode != null)
                            objXCurrencyTypeForClaimNode.Remove();
                        //MITS:34082 MultiCurrency END
                        sXml = objElement.ToString();
                    }
                    
                    TextBox txtPostBackAction = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "PostBackAction");
                    if (txtPostBackAction != null)
                    {
                        if (this.FindControl("FirstFinalPayment") != null && string.Compare(txtPostBackAction.Text, "FIRST&FINAL_PAYMENT") == 0)
                        {
                           
                            string[] sIgnoreSetIds = new string[] { "SplitRowId", "transid" };
                            for (int i = 0; i < sIgnoreSetIds.Length; i++)
                            {
                                WebControl oControl = (WebControl)DatabindingHelper.FindControlRecursive(this.Form, sIgnoreSetIds[i]);
                                oControl.Attributes.Add("rmxignoreset", "true");
                            }
                            sXml = BindControls2Data(sXml);
                            DropDownList dPolicy = (DropDownList)DatabindingHelper.FindControlRecursive(this.Form, "Policy");
                            dPolicy.Items.Clear();                           
                        }
                    }

                    //rupal:end, for first & final payment
                                        
                    XElement objElem = XElement.Parse(sXml);
                    //Payment can be marked as first & final from financial reserve grid or can be marked by user
                    //so we will set the isfirstFinal flag at common access point @ 'IsFirstFinalQueryString' textbox //IsFirstFinalQS
                    TextBox txtIsFirstFinalQS = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "IsFirstFinalQueryString");
                    if (txtIsFirstFinalQS != null)
                    {
                        if (objElem.XPathSelectElement("//IsFirstFinal") != null)
                            txtIsFirstFinalQS.Text = objElem.XPathSelectElement("./IsFirstFinal").Value;
                    }
                    TextBox objPolicyid = (TextBox)this.FindControl("PolicyID");
                    if (objPolicyid != null && String.IsNullOrEmpty(objPolicyid.Text))
                    {
                        if (objElem.XPathSelectElement("//PolicyID") != null)
                            objPolicyid.Text = objElem.XPathSelectElement("./PolicyID").Attribute("value").Value;
                    }
                    //MITS:34082 MultiCurrency START
                    TextBox objExchangeRate = (TextBox)this.FindControl("ExchangeRate");
                    //MITS:34082 MultiCurrency END
                    //start:rupal, for first & final payment
                    if (objElem.XPathSelectElement("//CarrierClaims") != null)
                    {
                        if (string.Compare(objElem.XPathSelectElement("./CarrierClaims").Value, "True") == 0)
                        {
                            //carrier claim setting is on
                            //populate policy
                            DropDownList dPolicy = (DropDownList)DatabindingHelper.FindControlRecursive(this.Form, "Policy");
                            dPolicy.Items.Clear();    
                            DropDownList dUnit = (DropDownList)DatabindingHelper.FindControlRecursive(this.Form, "Unit");
                            dUnit.Items.Clear();                           
                            MakeWebServiceCall(objElem.XPathSelectElement("./TransId").Value, ref objElem);
                            //enable/disable FirstfinalPaymnt checkbox based on conditions
                            //EnableDisableFirstFinalPayment(objElem);
                            Control objDivLossTypeCode = this.FindControl("div_LossType");
                            Control objDivDisabilityCat = this.FindControl("div_DisabilityCat");
                            Control objDivDisLossTypeCode = this.FindControl("div_DisabilityLossType");
                       string    objLobCode= AppHelper.GetQueryStringValue("LOB");
                            if (objLobCode != null && objLobCode != string.Empty)
                            {
                                if (objLobCode != "243")
                                {
                                    objDivDisLossTypeCode.Visible = false;
                                    objDivDisabilityCat.Visible = false;
                                    objDivLossTypeCode.Visible = true;
                                }
                                else
                                {
                                    objDivLossTypeCode.Visible = false;
                                    objDivDisLossTypeCode.Visible = true;
                                    objDivDisabilityCat.Visible = true;
                                
                                }
                            }
                            //tanwar2 - mits 30910 - start
                            XElement xCarrier = objElem.XPathSelectElement("//CarrierClaims");
                            if (xCarrier != null)
                            {
                                if (xCarrier.Attribute("OverrideDedFlagVisible")!=null)
                                {
                                    if (string.Compare(xCarrier.Attribute("OverrideDedFlagVisible").Value,"-1")!=0)
                                    {
                                        Control objDivDedProcessingFlag = this.FindControl("div_overridedeductible");

                                        if (objDivDedProcessingFlag != null)
                                        {
                                            objDivDedProcessingFlag.Visible = false;
                                        }
                                    }
                                }
                            }
                            //tanwar2 - mits 30910 - end
                        }
                        else
                        {
                            //carrier claim setting is off
                            //if carrier claims is off, we will make policy, covrage and firstfinalpayment controls invisible
                            MarkCarrierClaimControlsInvisible();
                        }
                    }
                    //end:rupal, for first & final payment
                    //Deb Multi Currency
                    //MITS:34082 start
                    /*string sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype");
                    if (!string.IsNullOrEmpty(sCurrCulture))
                    {
                        Culture = sCurrCulture.Split('|')[1];
                        UICulture = sCurrCulture.Split('|')[1];
                        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo(sCurrCulture.Split('|')[1]);*/
                    if (string.IsNullOrEmpty(sCurrCulture))
                    {
                        sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype");
                        if (!string.IsNullOrEmpty(sCurrCulture))
                        {
                            sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype"); //34082
                            Culture = sCurrCulture.Split('|')[1];
                            UICulture = sCurrCulture.Split('|')[1];
                            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                            Thread.CurrentThread.CurrentUICulture = new CultureInfo(sCurrCulture.Split('|')[1]);
                        }
                    }

                    if (!string.IsNullOrEmpty(sCurrCulture))
                    {
                        CodeLookUp objSplitCurrencyType = null;
                        //Label lblcurrencytype = (Label)this.FindControl("lblcurrencytype");
                        //Control objDivlblCurrType = this.FindControl("div_lblcurrencytype");
                    //MITS:34082  END
                        Control objDivClaimCurrBal = this.FindControl("div_ClaimCurrReserveBal");
                        //MITS:34082 MultiCurrency START
                        //if condition for jira 9627 for form read only.
                        if (this.FindControl("CurrencyTypeForClaim") != null && this.FindControl("CurrencyTypeForClaim").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                        {
                            ((Label)Page.FindControl("CurrencyTypeForClaim")).Text = ((Label)this.FindControl("CurrencyTypeForClaim")).Text;
                        }
                        else
                        {
                            objSplitCurrencyType = (CodeLookUp)this.FindControl("CurrencyTypeForClaim");
                        }
                        Control objDivCurrencyTypeForClaim = this.FindControl("div_CurrencyTypeForClaim");
                        Control objDivSplitToPaymentCurrRate = this.FindControl("div_SplitToPaymentCurrRate");
                        //if (lblcurrencytype != null)
                        //{
                        //    if ((objElem.XPathSelectElement("//UseMultiCurrency") != null))
                        //    {
                        //        if (string.Compare(objElem.XPathSelectElement("./UseMultiCurrency").Value, Boolean.TrueString) != 0)
                        //        {
                        //            if (objDivClaimCurrBal != null)
                        //                objDivClaimCurrBal.Visible = false;
                        //            if (objDivlblCurrType != null)
                        //                objDivlblCurrType.Visible = false;
                        //        }
                        //        else
                        //        {
                        //            if (objDivlblCurrType != null)
                        //                lblcurrencytype.Text = sCurrCulture.Split('|')[0].Substring(0, 3) + " " + Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                        //        }
                        //    }
                        //}
                        if (!(sXml.Contains("<SplitToPaymentCurrRate>")))
                        {
                            if (objDivSplitToPaymentCurrRate != null)//ajohari2 FDM PowerView Fixes
                            {
                                objDivSplitToPaymentCurrRate.Visible = false;
                            }
                        }
                        else
                        {
                            if (objExchangeRate != null  )//ajohari2 FDM PowerView Fixes
                            {
                                if (objElem.XPathSelectElement("//SplitToPaymentCurrRate") != null)
                                {
                                    objElem.XPathSelectElement("//SplitToPaymentCurrRate").Value = objExchangeRate.Text;
                                }
                            }
                        }
                        if ((sXml.Contains("<SplitToPaymentCurrRate></SplitToPaymentCurrRate>")))
                        {
                            if (objDivSplitToPaymentCurrRate != null)//ajohari2 FDM PowerView Fixes
                            {
                                objDivSplitToPaymentCurrRate.Visible = false;
                            }
                        }

                        if (objSplitCurrencyType != null)
                        {
                            if ((objElem.XPathSelectElement("//UseMultiCurrency") != null))
                            {
                                if (string.Compare(objElem.XPathSelectElement("./UseMultiCurrency").Value, Boolean.TrueString) != 0)
                                {
                                    if (objDivCurrencyTypeForClaim != null)//ajohari2 FDM PowerView Fixes
                                    {
                                        objDivCurrencyTypeForClaim.Visible = false;
                                    }
                                    if (objDivSplitToPaymentCurrRate != null)//ajohari2 FDM PowerView Fixes
                                    {
                                        objDivSplitToPaymentCurrRate.Visible = false;
                                    }
                                    if (objDivClaimCurrBal != null)
                                        objDivClaimCurrBal.Visible = false;
                                }
                                else
                                {
                                    objSplitCurrencyType.CodeText = sCurrCulture.Split('|')[0].Substring(0, 3) + " " + Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                                    if (objElem.XPathSelectElement("//SplitCurrCode") != null)
                                    {
                                        objElem.XPathSelectElement("//SplitCurrCode").Value = objSplitCurrencyType.CodeText;
                                    }
                                }
                            }
                        }
                        //MITS:34082 MultiCurrency END
                    }
                    //Deb Multi Currency
                    BindData2Control(objElem, this.Controls);
                    //Start by Shivendu for MITS 17520
                    IEnumerable<XElement> oModifiedControlsList = objElem.XPathSelectElements("./ModifiedControlsSplitList/Control");
                    DatabindingHelper.UpdateModifiedControls(oModifiedControlsList, this);
                    DatabindingHelper.PerformGroupAssociation(this);
                    //End by Shivendu for MITS 17520
                    //Ashish Ahuja : Mits 30880 Start
                    if (objElem.XPathSelectElement("//CarrierClaims") != null)
                    {
                        if (string.Compare(objElem.XPathSelectElement("./CarrierClaims").Value, "True") != 0)
                        {
                            AutoFill();
                        }
                    }
                    //Ashish Ahuja : Mits 30880 End
                }
            }
            if (this.FindControl("IsManualDed") != null && AppHelper.GetQueryStringValue("IsManDed") != null)
            {
                //string sReturnManualDedDetails = string.Empty;
                ((TextBox)this.FindControl("IsManualDed")).Text = AppHelper.GetQueryStringValue("IsManDed");
                if (AppHelper.GetQueryStringValue("IsManDed") == "1")
                {
                     MakeCarrierControlsReadOnly();


                } 
                
            }
            //Ashish Ahuja : Mits 30880
            //CodesBusinessHelper cb = new CodesBusinessHelper();
            //string sLobCode = AppHelper.GetQueryStringValue("LOB");
            //string sClaimId = AppHelper.GetQueryStringValue("claimid");
            //string sClaimantEId = AppHelper.GetQueryStringValue("claimid");

            //string reserveType = "";
            //string transType = "";
            //string reserveTypeCode = "";
            //CodeListType objList = cb.GetCode("", "RESERVE_TYPE", sLobCode, "", "split", "", "", "", sLobCode, sClaimId, "", "", "", "", "0", "", "", "", "", "0", sClaimantEId, "", "", "", "", "");

            //if (objList.Codes.Length == 2)
            //{
            //    reserveType = ((Riskmaster.UI.CodesService.CodeType)(objList.Codes.GetValue(0))).Desc;
            //    reserveTypeCode = ((Riskmaster.UI.CodesService.CodeType)(objList.Codes.GetValue(0))).Id.ToString();
            //    if (this.FindControl("ReserveTypeCodeFt") != null)
            //    {
            //        ((CodeLookUp)this.FindControl("ReserveTypeCodeFt")).CodeText = reserveType;
            //        ((TextBox)this.FindControl("ReserveTypeCodeFt$codelookup_cid")).Text = reserveTypeCode.ToString();
            //    }
            //}

            //objList = cb.GetCode("", "TRANS_TYPES", sLobCode, "", "split", "", "", "", sLobCode, sClaimId, "", "", "", "", "0", "", "", "", "", "0", sClaimantEId, "", "", "", "", "");

            //int transTypeCode = 0;

            //if (objList.Codes.Length == 2)
            //{
            //    transType = ((Riskmaster.UI.CodesService.CodeType)(objList.Codes.GetValue(0))).Desc;
            //    transTypeCode = ((Riskmaster.UI.CodesService.CodeType)(objList.Codes.GetValue(0))).Id;

            //    if (this.FindControl("TransTypeCode") != null)
            //    {
            //        ((CodeLookUp)this.FindControl("TransTypeCode")).CodeText = transType;
            //        ((TextBox)this.FindControl("TransTypeCode$codelookup_cid")).Text = transTypeCode.ToString();
            //        TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
            //        if (objTxt != null)
            //        {
            //            objTxt.Enabled = true;
            //            objTxt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");

            //        }
            //        Button objBtn = (Button)this.FindControl("TransTypeCode$codelookupbtn");
            //        if (objBtn != null)
            //        {
            //            objBtn.Enabled = true;
            //        }
            //        if (UseResFilter != null)
            //        {
            //            if (UseResFilter.InnerText == "")
            //            {
            //                oMessageElement = GetMessageTemplateReserveInfo();
            //                XElement oSessionCmdElement = null;
            //                XmlDocument objXml = null;
            //                XElement objReservesData = null;

            //                if (sClaimId != null)
            //                {
            //                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimId");
            //                    oSessionCmdElement.Value = sClaimId;
            //                }
            //                oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/TransTypeCode");
            //                oSessionCmdElement.Value = transTypeCode.ToString();

            //                objXml = GetReservesInfo();
            //                if (objXml.OuterXml != string.Empty)
            //                {
            //                    objReservesData = XElement.Parse(objXml.OuterXml.ToString());
            //                    AssignReserveValues(objReservesData);
            //                }
            //            }
            //        }
            //    }

            //}
        }

        private void HideOverrideDeductible()
        {
            Control objDivDedProcessingFlag = this.FindControl("div_overridedeductible");

            if (objDivDedProcessingFlag != null)
            {
                objDivDedProcessingFlag.Visible = false;
            }
        }

        private void MakeCarrierControlsReadOnly()
        {
            Control objPolicy = this.FindControl("Policy");
            Control objDivPolicy = this.FindControl("div_Policy");
            Control objUnit = this.FindControl("Unit");
            Control objDivUnit = this.FindControl("div_Unit");
            Control objCoverageTypeCode = this.FindControl("Coverage");
            Control objDivCoverageTypeCode = this.FindControl("div_Coverage");
            Control objLossTypeCode = this.FindControl("LossType");
            Control objDivLossTypeCode = this.FindControl("div_LossType");
            Control objDivDisabilityCat = this.FindControl("div_DisabilityCat");
            Control objDivDisLossTypeCode = this.FindControl("div_DisabilityLossType");
            Control objLob = this.FindControl("lineofbusinesscode");
            //tanwar2 - mits 30910 - start
            Control objDedProcessingFlag = this.FindControl("overridedeductible");
            Control objDivDedProcessingFlag = this.FindControl("div_overridedeductible");
            Control objTransTypeCode = this.FindControl("TransTypeCode");
            Control objdivTransTypeCode = this.FindControl("div_TransTypeCode");
            Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
            Control objReserveTypeCodeFt = this.FindControl("ReserveTypeCodeFt");
            //Control objCtrlNumber = this.FindControl("ctlnumber");


            DatabindingHelper.DisableControl(objPolicy);
            DatabindingHelper.DisableControl(objDivPolicy);
            DatabindingHelper.DisableControl(objUnit);
            DatabindingHelper.DisableControl(objDivUnit);
            DatabindingHelper.DisableControl(objCoverageTypeCode);
            DatabindingHelper.DisableControl(objDivCoverageTypeCode);
            DatabindingHelper.DisableControl(objLossTypeCode);
            DatabindingHelper.DisableControl(objDivLossTypeCode);
            DatabindingHelper.DisableControl(objDivDisabilityCat);
            DatabindingHelper.DisableControl(objDivDisLossTypeCode);
            DatabindingHelper.DisableControl(objDedProcessingFlag);
            DatabindingHelper.DisableControl(objDivDedProcessingFlag);
            DatabindingHelper.DisableControl(objTransTypeCode);
            DatabindingHelper.DisableControl(objdivTransTypeCode);
            DatabindingHelper.DisableControl(objReserveTypeCode);
            DatabindingHelper.DisableControl(objReserveTypeCodeFt);

            // In Case of Manual Deductible we do not need Override Deductible so Hide the control as well
            HideOverrideDeductible();
              
        }

		//MITS 32087  nnithiyanand - Starts
        private XElement GetMessageTemplateForCriterea()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><GetCriteria>");
            sXml.Append("<LOB>");
            sXml.Append("");
            sXml.Append("</LOB>");
            sXml.Append("<control name=\"DupCriteria\" type=\"\" value=\"\">");
            sXml.Append("");
            sXml.Append("</control>");
            sXml.Append("<DupNoOfDays>");
            sXml.Append("");
            sXml.Append("</DupNoOfDays>");
            sXml.Append("</GetCriteria>");
            sXml.Append("</Document>");
            sXml.Append("</Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate; 
        }
		//MITS 32087  nnithiyanand - Ends
        //Ashish Ahuja : Mits 30880 Start
        XElement oMessageElement = null;
        private void AutoFill()
        {
            CodesBusinessHelper cb = new CodesBusinessHelper();
            string sLobCode = AppHelper.GetQueryStringValue("LOB");
            string sClaimId = AppHelper.GetQueryStringValue("claimid");
            string sClaimantEId = AppHelper.GetQueryStringValue("claimid");

            string reserveType = "";
            string transType = "";
            string reserveTypeCode = "";
			//modified by swati for additional parameter added MITS# 36929
            //CodeListType objList = cb.GetCode("", "RESERVE_TYPE", sLobCode, "", "split", "", "", "", sLobCode, sClaimId, "", "", "", "", "0", "", "", "", "", "0", sClaimantEId, "", "", "", "", "", "", "", "", "", "", "", "");
            CodeListType objList = cb.GetCode("", "RESERVE_TYPE", sLobCode, "", "", "split", "", "", "", sLobCode, sClaimId, "", "", "", "", "0", "", "", "", "", "0", sClaimantEId, "", "", "", "", "", "", "", "", "", "", "", "","");

            if (objList != null && objList.Codes != null && objList.Codes.Count == 2)
            {
                reserveType = ((Riskmaster.Models.CodeType)(objList.Codes[0])).Desc;
                reserveTypeCode = ((Riskmaster.Models.CodeType)(objList.Codes[0])).Id.ToString();
                if (this.FindControl("ReserveTypeCodeFt") != null)
                {
                    ((CodeLookUp)this.FindControl("ReserveTypeCodeFt")).CodeText = reserveType;
                    ((TextBox)this.FindControl("ReserveTypeCodeFt$codelookup_cid")).Text = reserveTypeCode.ToString();
                }
            }

			//modified by swati for additional parameter added MITS# 36929
            //objList = cb.GetCode("", "TRANS_TYPES", sLobCode, "", "split", "", "", "", sLobCode, sClaimId, "", "", "", "", "0", "", "", "", "", "0", sClaimantEId, "", "", "", "", "", "", "", "", "", "", "", "");
            objList = cb.GetCode("", "TRANS_TYPES", sLobCode, "", "", "split", "", "", "", sLobCode, sClaimId, "", "", "", "", "0", "", "", "", "", "0", sClaimantEId, "", "", "", "", "", "", "", "", "", "", "", "","");

            int transTypeCode = 0;

            if (objList != null && objList.Codes != null && objList.Codes.Count  == 2)
            {
                transType = ((Riskmaster.Models.CodeType)(objList.Codes[0])).Desc;
                transTypeCode = ((Riskmaster.Models.CodeType)(objList.Codes[0])).Id;

                if (this.FindControl("TransTypeCode") != null)
                {
                    ((CodeLookUp)this.FindControl("TransTypeCode")).CodeText = transType;
                    ((TextBox)this.FindControl("TransTypeCode$codelookup_cid")).Text = transTypeCode.ToString();
                    TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
                    if (objTxt != null)
                    {
                        DatabindingHelper.EnableControls("TransTypeCode$codelookup", this);
                        //objTxt.Enabled = true;
                        //objTxt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");

                    }
                    Button objBtn = (Button)this.FindControl("TransTypeCode$codelookupbtn");
                    if (objBtn != null)
                    {
                        DatabindingHelper.EnableControls("TransTypeCode$codelookupbtn", this);
                        //objBtn.Enabled = true;
                    }
                    if (UseResFilter != null)
                    {
                        if (UseResFilter.InnerText == "")
                        {
                            oMessageElement = GetMessageTemplateReserveInfo();
                            XElement oSessionCmdElement = null;
                            XmlDocument objXml = null;
                            XElement objReservesData = null;

                            if (sClaimId != null)
                            {
                                oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimId");
                                oSessionCmdElement.Value = sClaimId;
                            }
                            oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/TransTypeCode");
                            oSessionCmdElement.Value = transTypeCode.ToString();

                            objXml = GetReservesInfo();
                            if (objXml.OuterXml != string.Empty)
                            {
                                objReservesData = XElement.Parse(objXml.OuterXml.ToString());
                                AssignReserveValues(objReservesData);
                            }
                        }
                    }
                }

            }
        }
        private XmlDocument GetReservesInfo()
        {
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;

            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
            if (ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                objXml.LoadXml(oInstanceNode.InnerXml);

            }
            return objXml;
        }
        private void AssignReserveValues(XElement p_objReservesData)
        {
            if (this.FindControl("ReserveTypeCode") != null)
            {
                ((TextBox)this.FindControl("ReserveTypeCode$codelookup")).Text = p_objReservesData.Element("ReserveType").Value;
            }

            if (p_objReservesData.Element("ReserveBalance") != null)
            {
                ((TextBox)this.FindControl("reservebalance")).Text = p_objReservesData.Element("ReserveBalance").Value;
            }

        }
        private XElement GetMessageTemplateReserveInfo()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>FundManagementAdaptor.GetReservesInfo</Function> 
                        </Call>
                    <Document>
                        <FundManager>
                            <ClaimId></ClaimId> 
                            <TransTypeCode></TransTypeCode>
                            <ReserveTypeCode></ReserveTypeCode> 
                            <ClaimantEid>0</ClaimantEid> 
                            <UnitId>0</UnitId> 
                            <TransID>0</TransID> 
                            <PolicyId></PolicyId>
                            <CovTypeCode></CovTypeCode>
                            <PolicyUnitRowId></PolicyUnitRowId>
                            <PmtCurrCode></PmtCurrCode>
                            <CovgSeqNum></CovgSeqNum>
                            <LossCode></LossCode>
                        </FundManager>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }
        //Ashish Ahuja : End

        private void BindData2Controls(string sOptionData)
        {
            XElement oOptionData = XElement.Parse(sOptionData);
            BindData2Control(oOptionData, this.Form.Controls);
        }

        private string BindControls2Data(string sOptionData)
        {
            XElement oOptionData = XElement.Parse(sOptionData);
            BindNonFDMCWSControlCollection2Data(this.Form.Controls, oOptionData);
            DropDownList dPolicy = (DropDownList)DatabindingHelper.FindControlRecursive(this.Form, "Policy");
            XElement oPolicy = oOptionData.XPathSelectElement("./PolicyID");
            if (dPolicy != null && dPolicy.SelectedIndex != -1) //rupal
                oPolicy.Value = dPolicy.SelectedItem.Text;
            DropDownList dUnit = (DropDownList)DatabindingHelper.FindControlRecursive(this.Form, "Unit");
            XElement oUnit = oOptionData.XPathSelectElement("./UnitID");
            if (oUnit != null && dUnit.SelectedIndex != -1) //rupal
                oUnit.Value = dUnit.SelectedItem.Text;
            //MITS:34082 MultiCurrency START
            TextBox SplitCurrAmount = (TextBox)this.FindControl("SplitCurrAmount");
            TextBox SplitCurrInvoiceAmount = (TextBox)this.FindControl("SplitCurrencyInvAmount");
            string sPaymentCurrencytype = AppHelper.GetQueryStringValue("pmtcurrencytype");
            sPaymentCurrencytype = CultureInfo.CreateSpecificCulture(sPaymentCurrencytype.Split('|')[1]).NumberFormat.CurrencySymbol;
            string sSplitCurrencytype = Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencySymbol;
            if (SplitCurrAmount != null && SplitCurrAmount.Text != "")//ajohari2 FDM PowerView Fixes
            {
                //ajohari2 FDM PowerView Fixes:Start
                //oOptionData.XPathSelectElement("./SplitCurrAmount").Value = SplitCurrAmount.Text.Replace(sSplitCurrencytype, "");
                //oOptionData.XPathSelectElement("./Amount").Attribute("CurrencyValue").Value = sPaymentCurrencytype + String.Format("{0:#,0.00}", Double.Parse(oOptionData.XPathSelectElement("./Amount").Value, CultureInfo.CreateSpecificCulture("en-US").NumberFormat));
                XElement oSplitCurrAmount = oOptionData.XPathSelectElement("./SplitCurrAmount");
                if (oSplitCurrAmount != null)
                {
                    oSplitCurrAmount.Value = SplitCurrAmount.Text.Replace(sSplitCurrencytype, "");
                }
                XElement oAmount = oOptionData.XPathSelectElement("./Amount");
                if (oAmount != null)
                {
                    oAmount.Attribute("CurrencyValue").Value = sPaymentCurrencytype + String.Format("{0:#,0.00}", Double.Parse(oOptionData.XPathSelectElement("./Amount").Value, CultureInfo.CreateSpecificCulture("en-US").NumberFormat));
                }
                //ajohari2 FDM PowerView Fixes:End
            }
            if (SplitCurrInvoiceAmount != null && SplitCurrInvoiceAmount.Text != "")//ajohari2 FDM PowerView Fixes
            {
                //ajohari2 FDM PowerView Fixes:Start
                //oOptionData.XPathSelectElement("./SplitCurrencyInvAmount").Value = SplitCurrInvoiceAmount.Text.Replace(sSplitCurrencytype, "");
                //oOptionData.XPathSelectElement("./InvoiceAmount").Attribute("CurrencyValue").Value = sPaymentCurrencytype + String.Format("{0:#,0.00}", Double.Parse(oOptionData.XPathSelectElement("./InvoiceAmount").Value, CultureInfo.CreateSpecificCulture("en-US").NumberFormat));
                XElement oSplitCurrencyInvAmount = oOptionData.XPathSelectElement("./SplitCurrencyInvAmount");
                if (oSplitCurrencyInvAmount != null)
                {
                    oSplitCurrencyInvAmount.Value = SplitCurrInvoiceAmount.Text.Replace(sSplitCurrencytype, "");
                }
                XElement oInvoiceAmount = oOptionData.XPathSelectElement("./InvoiceAmount");
                if (oInvoiceAmount != null)
                {
                    oInvoiceAmount.Attribute("CurrencyValue").Value = sPaymentCurrencytype + String.Format("{0:#,0.00}", Double.Parse(oOptionData.XPathSelectElement("./InvoiceAmount").Value, CultureInfo.CreateSpecificCulture("en-US").NumberFormat));
                }
                //ajohari2 FDM PowerView Fixes:End
            }
            //MITS:34082 MultiCurrency END
            //start:rupal. for first & final payment
            CheckBox chkFirstFinalPayment = (CheckBox)DatabindingHelper.FindControlRecursive(this.Form, "FirstFinalPayment");
            if (chkFirstFinalPayment != null)
            {
                XElement oFirstFinalPayment = oOptionData.XPathSelectElement("./IsFirstFinal");
                if (oFirstFinalPayment != null)
                {
                    if (chkFirstFinalPayment.Checked)
                        oFirstFinalPayment.Value = "True";
                    else
                        oFirstFinalPayment.Value = "False";
                }
            }
            //end:rupal
            return oOptionData.ToString();
        }
        //Deb Multi Currency
        protected override void InitializeCulture()
        {
            string sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype");
            if (!string.IsNullOrEmpty(sCurrCulture))
            {
                Culture = sCurrCulture.Split('|')[1];
                UICulture = sCurrCulture.Split('|')[1];
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(sCurrCulture.Split('|')[1]);
            }
            base.InitializeCulture();
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            TextBox txtPostBackParent = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "txtPostBack");
            txtPostBackParent.Text = "DoneOK";

            TextBox txtDataParent = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "txtData");
            string sXml = txtDataParent.Text;
            sXml = sXml.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
            
            string sResultXml = BindControls2Data(sXml);

            //set SplitRowId to negative ticks if it's 0 or null
            XElement oOptionData = XElement.Parse(sResultXml);
            XElement oSplitRowId = oOptionData.XPathSelectElement("//SplitRowId");
            XElement oCurrCode = oOptionData.XPathSelectElement("//SplitCurrCode"); //MITS:34082 MultiCurrency
            string sSplitRowId = oSplitRowId.Value;
            long lSplitRowId = 0;
            bool bParse = long.TryParse(sSplitRowId, out lSplitRowId);
            if (!bParse)
                lSplitRowId = 0;
            if (lSplitRowId <= 0)
            {
                long ticks = DateTime.Now.Ticks * (-1);
                oSplitRowId.Value = ticks.ToString();
            }
            //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -start
            XElement oCoverageTypeCode = oOptionData.XPathSelectElement("//CoverageTypeCode");
            if (oCoverageTypeCode != null)           //avipinsrivas Start : Worked for JIRA - 9608
                oCoverageTypeCode.Value = AppHelper.HTMLCustomEncode(oCoverageTypeCode.Value);

            XElement oUnitID = oOptionData.XPathSelectElement("//UnitID");
            if (oUnitID != null)                     //avipinsrivas Start : Worked for JIRA - 9608
                oUnitID.Value = AppHelper.HTMLCustomEncode(oUnitID.Value);
            //mkaran2 - [JIRA] (RMA-4110) MITS 37158 -end
            //Start by Shivendu for MITS 17520
            IEnumerable<XElement> oModifiedControlsList = oOptionData.XPathSelectElements("./ModifiedControlsSplitList/Control");
            SaveSuppGridXml(ref oModifiedControlsList, ref oOptionData);
            //End by Shivendu for MITS 17520
            //srajindersin Security fix MITS 35888 dt 4/14/2014
            txtDataParent.Text = oOptionData.ToString();

            string script = "<script>GetDataFromOpener();</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
        }

        /// <summary>
        /// Added by Shivendu for MITS 17520
        /// </summary>
        /// <param name="oModifiedControlsList"></param>
        /// <param name="oOptionData"></param>
        private void SaveSuppGridXml(ref IEnumerable<XElement> oModifiedControlsList, ref XElement oOptionData)
        {
            string sControlType = "";
            string sControlID = "";
            int colCount = 0;
            int counter = 0;
            bool bNewRow = true;
            XmlDocument xGridXml = new XmlDocument();
            XmlElement objXMLCellElement = null;
            XmlElement objXMLRowElement = null;
            XmlNode objXMLRowsNode = null;
            XmlNode objXMLRowNode = null;

            try
            {
                if (oModifiedControlsList != null)
                {
                    foreach (XElement oEle in oModifiedControlsList)
                    {
                        sControlType = oEle.Attribute("ControlType").Value;
                        sControlID = oEle.Attribute("id").Value;

                        switch (sControlType)
                        {
                            case "Hidden":
                                if (oEle.Attribute("id") != null && oEle.Attribute("id").Value.IndexOf("_zapatecgridxml") > 0)
                                {
                                    xGridXml.LoadXml(oEle.Attribute("Text").Value.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'"));
                                    objXMLRowNode = xGridXml.SelectSingleNode("/grid/table/rows/row");
                                    xGridXml.SelectSingleNode("/grid/table/rows").RemoveChild(objXMLRowNode);
                                    objXMLRowsNode = xGridXml.SelectSingleNode("/grid/table/rows");
                                    XmlNodeList xColcOunt = (XmlNodeList)xGridXml.SelectNodes("/grid/table/fields/field");
                                    colCount = xColcOunt.Count;
                                    IEnumerable<XElement> oGridValues = oOptionData.XPathSelectElements("./Supplementals/" + oEle.Attribute("id").Value.Replace("_zapatecgridxml", "").ToUpper());
                                    foreach (XElement oGridEle in oGridValues)
                                    {
                                        string[] sGridValues = oGridEle.Value.Split(',');
                                        while (bNewRow)
                                        {
                                            objXMLRowElement = xGridXml.CreateElement("row");
                                            for (int i = 0; i < colCount; i++)
                                            {
                                                objXMLCellElement = xGridXml.CreateElement("cell");
                                                if (sGridValues[counter] == "KNALB")
                                                {
                                                    objXMLCellElement.InnerText = "";

                                                }
                                                else
                                                {
                                                    objXMLCellElement.InnerText = sGridValues[counter];
                                                }
                                                objXMLRowElement.AppendChild(objXMLCellElement);
                                                objXMLRowsNode.AppendChild(objXMLRowElement);
                                                counter++;
                                                if (counter == sGridValues.Length)
                                                {
                                                    bNewRow = false;
                                                    break;
                                                }
                                            }

                                        }


                                    }
                                    oEle.Attribute("Text").Value = xGridXml.InnerXml.ToString().Replace("amp;", "").Replace("<", "&lt;").Replace(">", "&gt;").Replace("'", "&quot;").Replace("\"", "&quot;");
                                }


                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        //Function to make a call to Webservice--stara Mits 16667 05/15/09
        private void MakeWebServiceCall()
        {
            XmlTemplate = GetMessageTemplate();
            //bReturnStatus = CallCWSFunction("SplitForm.GetUtilityUseResFilterValue", out sreturnValue, XmlTemplate);

            bReturnStatus = CallCWS("FundManagementAdaptor.CheckUtility", XmlTemplate, out sreturnValue, false, false);
            EnableDisbleReserveTypeFt(sreturnValue);
        
        }

        private void MakeWebServiceCall(string sTransId, ref XElement objElem)
        {
            TextBox txtmode = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "mode");
            XmlTemplate = GetTemplateForPolicyList(sTransId);

            bReturnStatus = CallCWS("FundManagementAdaptor.GetPolicyList", XmlTemplate, out sreturnValue, false, false);
            XElement oTemplate = XElement.Parse(sreturnValue.ToString());

            if (string.Compare(txtmode.Text, "add", true) == 0)
            {
                foreach (XElement oTemp in oTemplate.XPathSelectElements("//PolicyList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true")
                    {
                        objElem.XPathSelectElement("//PolicyID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//PolicyID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
                    }
                }

                foreach (XElement oTemp in oTemplate.XPathSelectElements("//UnitList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true")
                    {
                        objElem.XPathSelectElement("//UnitID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//UnitID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
                    }
                }

                XNode objXCoverageTypeCodeNode = objElem.XPathSelectElement("//CoverageTypeCode");
                if (objXCoverageTypeCodeNode != null)
                    objXCoverageTypeCodeNode.Remove();
                objXCoverageTypeCodeNode = oTemplate.XPathSelectElement("//CoverageTypeCode");
                objElem.Add(objXCoverageTypeCodeNode);

                XNode objXReserveTypeCodeNode = objElem.XPathSelectElement("//ReserveTypeCode");
                if (objXReserveTypeCodeNode != null)
                    objXReserveTypeCodeNode.Remove();
                objXReserveTypeCodeNode = oTemplate.XPathSelectElement("//ReserveTypeCode");
                objElem.Add(objXReserveTypeCodeNode);
                XNode objXDisabilitycatCodeNode = objElem.XPathSelectElement("//DisabilityCatCode");
                if (objXDisabilitycatCodeNode != null)
                    objXDisabilitycatCodeNode.Remove();
                objXDisabilitycatCodeNode = oTemplate.XPathSelectElement("//DisabilityCatCode");
                objElem.Add(objXDisabilitycatCodeNode);
                XNode objXDisabilityTypeCodeNode = objElem.XPathSelectElement("//DisabilityTypeCode");
                if (objXDisabilityTypeCodeNode != null)
                    objXDisabilityTypeCodeNode.Remove();
                objXDisabilityTypeCodeNode = oTemplate.XPathSelectElement("//DisabilityTypeCode");
                objElem.Add(objXDisabilityTypeCodeNode);
                XNode objXLossTypeCodeNode = objElem.XPathSelectElement("//LossTypeCode");
                if (objXLossTypeCodeNode != null)
                    objXLossTypeCodeNode.Remove();
                objXLossTypeCodeNode = oTemplate.XPathSelectElement("//LossTypeCode");
                objElem.Add(objXLossTypeCodeNode);
                //MITS:34082 MultiCurrency START
                XNode objXCurrCodeCodeNode = objElem.XPathSelectElement("//SplitCurrCode");
                if (objXCurrCodeCodeNode != null)
                    objXCurrCodeCodeNode.Remove();
                objXCurrCodeCodeNode = oTemplate.XPathSelectElement("//SplitCurrCode");
                objElem.Add(objXCurrCodeCodeNode);
                //MITS:34082 MultiCurrency END

            }

            if (string.Compare(txtmode.Text, "edit", true) == 0)
            {
                foreach (XElement oTemp in oTemplate.XPathSelectElements("//PolicyList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true")
                    {
                        objElem.XPathSelectElement("//PolicyID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//PolicyID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
                    }
                }

                foreach (XElement oTemp in oTemplate.XPathSelectElements("//UnitList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true")
                    {
                        objElem.XPathSelectElement("//UnitID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//UnitID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
                    }
                }
            }
            XElement xNode = oTemplate.XPathSelectElement("//PolicyList");
            objElem.Add(xNode);

            xNode = oTemplate.XPathSelectElement("//UnitList");
            objElem.Add(xNode);

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>SplitForm.GetUtilityUseResFilterValue</Function></Call><Document><GetSettings><UseResFilter></UseResFilter><UseCarrierClaims></UseCarrierClaims></GetSettings></Document></Message>");//anahl3 11468,11121,11120
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetTemplateForPolicyList(string sTransId)
        {
            TextBox txtmode = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "mode");
            TextBox objclaimid = (TextBox)this.FindControl("claimid");
            TextBox objPolicyid = (TextBox)this.FindControl("PolicyID");
            TextBox objPolCvgID = (TextBox)this.FindControl("PolCvgID");
            TextBox ResTypeCodeId = (TextBox)this.FindControl("ResTypeCode");
            TextBox unitID = (TextBox)this.FindControl("UnitID");

            TextBox RcRowID = (TextBox)this.FindControl("RcRowID");
            
            TextBox CvgLossRowID = (TextBox)this.FindControl("CvgLossID");
            TextBox LobCode = (TextBox)this.FindControl("lineofbusinesscode");
       

            //rupal:start , for first & final payment
            //CheckBox chkFirstfinalPayment = (CheckBox)this.FindControl("FFPayment");
            TextBox IsFirstFinalQS = (TextBox)this.FindControl("IsFirstFinalQueryString");
            //rupal:end, for first & final payment
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><Policy><TransId>");
            sXml = sXml.Append(sTransId);
            sXml = sXml.Append("</TransId>");
            if (string.Compare(txtmode.Text, "add", true) == 0)
            {
                sXml = sXml.Append("<ClaimId>");
                sXml = sXml.Append(objclaimid.Text);
                sXml = sXml.Append("</ClaimId>");
                sXml = sXml.Append("<PolicyId>");
                sXml = sXml.Append(objPolicyid.Text);
                sXml = sXml.Append("</PolicyId>");
                sXml = sXml.Append("<UnitId>");
                sXml = sXml.Append(unitID.Text);
                sXml = sXml.Append("</UnitId>");
                sXml = sXml.Append("<PolCvgID>");
                sXml = sXml.Append(objPolCvgID.Text);
                sXml = sXml.Append("</PolCvgID>");
                sXml = sXml.Append("<ResTypeCodeId>");
                sXml = sXml.Append(ResTypeCodeId.Text);
                sXml = sXml.Append("</ResTypeCodeId>");
                sXml = sXml.Append("<RCRowID>");
                sXml = sXml.Append(RcRowID.Text);
                sXml = sXml.Append("</RCRowID>");
                sXml = sXml.Append("<CvgLossID>");
                sXml = sXml.Append(CvgLossRowID.Text);
                sXml = sXml.Append("</CvgLossID>");
                sXml = sXml.Append("<LobCode>");
                sXml = sXml.Append(LobCode.Text);
                sXml = sXml.Append("</LobCode>");
                

            }
            if((string.Compare(txtmode.Text, "edit", true) == 0))  //&& (string.Compare(sTransId,"0") == 0))
            {
                sXml = sXml.Append("<ClaimId>");
                sXml = sXml.Append(objclaimid.Text);
                sXml = sXml.Append("</ClaimId>");
                sXml = sXml.Append("<PolicyId>");
                sXml = sXml.Append(objPolicyid.Text);
                sXml = sXml.Append("</PolicyId>");

            }
            //rupal:start , for first & final payment
            //indicate that the payment is marked as first & final payment
            if (IsFirstFinalQS != null)
            {
                sXml = sXml.Append("<IsFirstFinal>");
                sXml = sXml.Append(IsFirstFinalQS.Text);
                sXml = sXml.Append("</IsFirstFinal>");
            }
            //rupal:end, for first & final payment
            sXml = sXml.Append("<PolicyList></PolicyList></Policy></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
         //Ashish Ahuja
        XmlNode UseResFilter = null;
        //--stara Mits 1666705/15/09
        private void EnableDisbleReserveTypeFt(string sXml)
        {
            XmlDocument objXmlDoc = new XmlDocument();

            objXmlDoc.LoadXml(sXml);
            //XmlNode UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/UseResFilter");
            //abahl3 starts, 11468,11121,11120
            UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/GetSettings/UseResFilter");
            XmlNode objIsCarrierClaim = objXmlDoc.SelectSingleNode("//ResultMessage/Document/GetSettings/UseCarrierClaims");
            //abahl3 ends, 11468,11121,11120           
            Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
            Control objDivReserveTypeCode = this.FindControl("div_ReserveTypeCode");
            Control objReserveTypeCodeFt = this.FindControl("ReserveTypeCodeFt");
            Control objDivReserveTypeCodeFt = this.FindControl("div_ReserveTypeCodeFt");
            Control objTransTypeCode = this.FindControl("TransTypeCode");
            Control objReserveTypeLabel = this.FindControl("lbl_ReserveTypeCode");
            //if (UseResFilter != null)
            //abahl3 starts, 11468,11121,11120
            //if (UseResFilter != null)
            //{
            if (((objIsCarrierClaim != null) && (objIsCarrierClaim.InnerText == "True") && Conversion.ConvertStrToInteger(((TextBox)this.FindControl("RcRowID")).Text) > 0) || ((UseResFilter != null) && (UseResFilter.InnerText == "True")))////abahl3 ends, 11468,11121,11120
            {


                if (objDivReserveTypeCodeFt != null)
                {
                    objDivReserveTypeCodeFt.Visible = true;
                }
                //rsushilaggar MITS 25353 Date 07/06/*2011
                //Deb MITS 26077
                if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                {
                    ((CodeLookUp)objReserveTypeCode).ShowHide = false;
                }
                if (objReserveTypeLabel != null)
                {
                    ((Label)objReserveTypeLabel).Visible = false;
                }
                //Deb MITS 26077
                if (Request.QueryString["mode"] == "edit")
                {
                    if (objTransTypeCode != null && objTransTypeCode.GetType() == typeof(System.Web.UI.WebControls.TextBox))
                    {
                        //((TextBox)objTransTypeCode).Enabled = true;
                        DatabindingHelper.EnableControls("TransTypeCode", this);
                    }
                }
                else
                {
                    TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
                    if (objTxt != null)
                    {
                        objTxt.Enabled = false;
                        objTxt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                    }
                    Button objBtn = (Button)this.FindControl("TransTypeCode$codelookupbtn");
                    if (objBtn != null)
                    {
                        objBtn.Enabled = false;
                    }
                }
                if (objReserveTypeCodeFt != null)
                {
                    RemoveIgnoreValueAttribute(objReserveTypeCodeFt);
                }
                if (objReserveTypeCode != null)
                {
                    SetIgnoreValueAttribute(objReserveTypeCode);
                }
            }
            else
            {
                if (objDivReserveTypeCode != null)
                {
                    objDivReserveTypeCode.Visible = true;
                }
                if (objDivReserveTypeCodeFt != null)
                {
                    objDivReserveTypeCodeFt.Visible = false;
                }
                if (objReserveTypeCode != null)
                {
                    RemoveIgnoreValueAttribute(objReserveTypeCode);
                }
                if (objReserveTypeCodeFt != null)
                {
                    SetIgnoreValueAttribute(objReserveTypeCodeFt);
                }
            }
            //}   //abahl3 11468,11121,11120            

        }
        //start:rupal, for first & final payment
        private void MarkCarrierClaimControlsInvisible()
        {
            Control objPolicy = this.FindControl("Policy");
            Control objDivPolicy = this.FindControl("div_Policy");
            Control objUnit = this.FindControl("Unit");
            Control objDivUnit = this.FindControl("div_Unit");
            Control objCoverageTypeCode = this.FindControl("Coverage");
            Control objDivCoverageTypeCode = this.FindControl("div_Coverage");
            Control objDivLossTypeCode = this.FindControl("div_LossType");
            Control objDivDisabilityCat = this.FindControl("div_DisabilityCat");
            Control objDivDisLossTypeCode = this.FindControl("div_DisabilityLossType");
            Control objLob = this.FindControl("lineofbusinesscode");

            //tanwar2 - mits 30910 - start
            Control objDivDedProcessingFlag = this.FindControl("div_overridedeductible");

            if (objDivDedProcessingFlag != null)
            {
                objDivDedProcessingFlag.Visible = false;
            }
            //tanwar2 - mits 30910 - end
            if (objDivLossTypeCode != null)
                objDivLossTypeCode.Visible = false;
            if (objDivDisabilityCat != null)
                objDivDisabilityCat.Visible = false;

            if (objDivDisLossTypeCode != null)
                objDivDisLossTypeCode.Visible = false;



            if (objDivPolicy != null)
            {
                objDivPolicy.Visible = false;
            }
            if (objDivUnit != null)
            {
                objDivUnit.Visible = false;
            }
            if (objDivCoverageTypeCode != null)
            {
                objDivCoverageTypeCode.Visible = false;
            }
            Control objFirstFinalPayment = this.FindControl("FirstFinalPayment");
            Control objDivFFPayment = this.FindControl("div_FirstFinalPayment");
            if (objDivFFPayment != null)
            {
                objDivFFPayment.Visible = false;
            }   
        }
        
        /*private void EnableDisableFirstFinalPayment(XElement objElem)
        {
            CheckBox objFirstFinalPayment = (CheckBox)this.FindControl("FirstFinalPayment");            

            bool bConverted = false;
            bool bEnabledValue = false;
            bool bCheckedValue = false;
            if (objElem.XPathSelectElement("//IsFirstFinal") != null)
            {
                bCheckedValue = Conversion.ConvertStrToBool(objElem.XPathSelectElement("./IsFirstFinal").Value);
            }
            if (objElem.XPathSelectElement("//SplitRowId") != null)
            {
                long lSplitRowID = Conversion.CastToType<long>(objElem.XPathSelectElement("./SplitRowId").Value, out bConverted);

                if (objFirstFinalPayment != null)
                {  
                    if (lSplitRowID > 0)
                    {
                        //split is saved in db so make firstffinalpayment disabled
                        //objFFPayment.Checked = bCheckedValue;
                        objFirstFinalPayment.Enabled = false;
                    }
                    else
                    {
                        //check if querystring has any value
                        if (objElem.XPathSelectElement("//IsFirstFinalControlReradOnlyQueryString") != null)
                        {
                            //objFFPayment.Checked = bCheckedValue;
                            bEnabledValue = Conversion.ConvertStrToBool(objElem.XPathSelectElement("./IsFirstFinalControlReradOnlyQueryString").Value);
                            //objFirstFinalPayment.Enabled = !(bEnabledValue);
                            if (!(bEnabledValue))
                            {
                                DatabindingHelper.EnableControls("FirstFinalPayment", this);
                            }
                            else
                            {
                                objFirstFinalPayment.Enabled = !(bEnabledValue);
                            }
                        }
                        else
                        {
                            //no value supplied, default behaviour                           
                            //objFirstFinalPayment.Enabled = true;
                            DatabindingHelper.EnableControls("FirstFinalPayment", this);
                        }
                    }
                }              
            }
        }*/
        //rupal:end, for first & final payment

        //Add by kuladeep for rmA14.1 Performance Start
        [System.Web.Services.WebMethod]
        public static string GetReserveData(string ClaimId, string ReserveTransLookUpType, string ReserveTransTypeCode, string ClaimantEid, string UnitId, string policyid, string lCoveCodeId, string DataChanged, string PolicyUnitRowId,
            string PmtCurrCode, string CovgSeqNum, string LossCode, string TransSeqNum, string CoverageKey, string sIsManualDed)
        {

            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;
            XElement oMessageElement = null;
            XElement oSessionCmdElement = null;
            string sReturn = string.Empty;

            oMessageElement = GetMessageTemplateData();

            if(!string.IsNullOrEmpty(ClaimId))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimId");
                 oSessionCmdElement.Value = ClaimId;
             }
             if (!string.IsNullOrEmpty(ReserveTransLookUpType))
             {
                 if (ReserveTransLookUpType=="ReserveTypeCode")
                 {
                     if (!string.IsNullOrEmpty(ReserveTransTypeCode))
                     {
                         oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ReserveTypeCode");
                         oSessionCmdElement.Value = ReserveTransTypeCode;

                     }
                 }
                 else if (ReserveTransLookUpType == "TransTypeCode")
                 {
                     if (!string.IsNullOrEmpty(ReserveTransTypeCode))
                     {
                         oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/TransTypeCode");
                         oSessionCmdElement.Value = ReserveTransTypeCode;
                     }
                 }
                 
             }
             if (!string.IsNullOrEmpty(ClaimantEid))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimantEid");
                 oSessionCmdElement.Value = ClaimantEid;
             }
             if (!string.IsNullOrEmpty(UnitId))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/UnitId");
                 oSessionCmdElement.Value = UnitId;
             }
             if (!string.IsNullOrEmpty(policyid))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PolicyId");
                 oSessionCmdElement.Value = policyid;
             }
             if (!string.IsNullOrEmpty(lCoveCodeId))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CovTypeCode");
                 oSessionCmdElement.Value = lCoveCodeId;
             }
             if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                 oSessionCmdElement.Value = AppHelper.ReadCookieValue("SessionId");
             }
             if (!string.IsNullOrEmpty(PolicyUnitRowId))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PolicyUnitRowId");
                 oSessionCmdElement.Value = PolicyUnitRowId;
             }
             if (!string.IsNullOrEmpty(PmtCurrCode))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PmtCurrCode");
                 oSessionCmdElement.Value = PmtCurrCode;
             }
             if (!string.IsNullOrEmpty(CovgSeqNum))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CovgSeqNum");
                 oSessionCmdElement.Value = CovgSeqNum;
             }
             if (!string.IsNullOrEmpty(LossCode))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/LossCode");
                 oSessionCmdElement.Value = LossCode;
             }
             if (!string.IsNullOrEmpty(TransSeqNum))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/TransSeqNum");
                 oSessionCmdElement.Value = TransSeqNum;
             }

             if (!string.IsNullOrEmpty(CoverageKey))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CoverageKey");
                 oSessionCmdElement.Value = CoverageKey;
             }
             if (!string.IsNullOrEmpty(sIsManualDed))
             {
                 oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ManualDed");
                 oSessionCmdElement.Value = sIsManualDed;
             }
            sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            return sReturn;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private static XElement GetMessageTemplateData()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>FundManagementAdaptor.GetReservesInfo</Function> 
                        </Call>
                    <Document>
                        <FundManager>
                            <ClaimId></ClaimId> 
                            <TransTypeCode></TransTypeCode>
                            <ReserveTypeCode></ReserveTypeCode> 
                            <ReserveParentCode></ReserveParentCode>
                            <ClaimantEid>0</ClaimantEid> 
                            <UnitId>0</UnitId> 
                            <TransID>0</TransID> 
                            <PolicyId></PolicyId>
                            <CovTypeCode></CovTypeCode>
                            <PolicyUnitRowId></PolicyUnitRowId>
                            <PmtCurrCode></PmtCurrCode>
                            <CovgSeqNum></CovgSeqNum>
                            <LossCode></LossCode>
                            <TransSeqNum></TransSeqNum>
                            <CoverageKey></CoverageKey>
                            <ManualDed></ManualDed>
                        </FundManager>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }
        //Add by kuladeep for rmA14.1 Performance End
    }
}
