﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.FDM
{
    public partial class CmXAccommodationList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
                    LoadInitialPage();
                    rootElement = XElement.Parse(Data.OuterXml);
                    result = from c in rootElement.XPathSelectElements("//Param/CmXAccommodationList/CmXAccommodation")
                             let xClaim = (int)Convert.ToInt64(c.Element("CmAccommRowId").Value)
                             orderby xClaim descending
                             select c;
                    if (rootElement.XPathSelectElement("//CmXAccommodation/CmRowId") != null)
                    {
                        cmrowid.Text = rootElement.XPathSelectElement("//CmXAccommodation/CmRowId").Value;
                    }
                
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

        protected void NavigateListDelete(object sender, EventArgs e)
        {
            try
            {
                base.NavigateListDelete(sender, e);
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/CmXAccommodationList/CmXAccommodation")
                         let xClaim = (int)Convert.ToInt64(c.Element("CmAccommRowId").Value)
                         orderby xClaim descending
                         select c;
                if (rootElement.XPathSelectElement("//CmXAccommodation/CmRowId") != null)
                {
                    cmrowid.Text = rootElement.XPathSelectElement("//CmXAccommodation/CmRowId").Value;
                }
                //LoadInitialPage();

            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }
        }

    }
}
