<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orghierarchymaint.aspx.cs"  Inherits="Riskmaster.UI.FDM.Orghierarchymaint" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Edit Org Hierarchy</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="" xmlns:cul="remove">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" src="../../Images/delete.gif" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/delete.gif';this.style.zoom='100%'" />
        </div>
      </div>
      <br />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Edit Org Hierarchy" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSdepartment" id="TABSdepartment">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="department" id="LINKTABSdepartment">Organization</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSdeptinfo" id="TABSdeptinfo">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="deptinfo" id="LINKTABSdeptinfo">Organization Info</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSclientlimit" id="TABSclientlimit">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="clientlimit" id="LINKTABSclientlimit">Client Limits</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSoperatingas" id="TABSoperatingas">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="operatingas" id="LINKTABSoperatingas">Operating As</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSContactInfo" id="TABSContactInfo">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="ContactInfo" id="LINKTABSContactInfo">Contact Into</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" style="height:310px;overflow:auto" name="FORMTABdepartment" id="FORMTABdepartment">
        <asp:TextBox style="display:none" runat="server" id="entityid" RMXRef="/Instance/Entity/EntityId" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_lastname" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_lastname" Text="Name" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="lastname" RMXRef="/Instance/Entity/LastName" RMXType="text" tabindex="1" />
          </span>
        </div>
        <div runat="server" class="half" id="div_abbreviation" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_abbreviation" Text="Abbreviation" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="abbreviation" RMXRef="/Instance/Entity/Abbreviation" RMXType="text" tabindex="11" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="entitytableid" RMXRef="/Instance/Entity/EntityTableId" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_alsoknownas" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_alsoknownas" Text="DBA" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="alsoknownas" RMXRef="/Instance/Entity/AlsoKnownAs" RMXType="text" tabindex="2" />
          </span>
        </div>
        <div runat="server" class="half" id="div_city" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_city" Text="City" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="city" RMXRef="/Instance/Entity/City" RMXType="text" tabindex="12" />
          </span>
        </div>
        <div runat="server" class="half" id="div_addr1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_addr1" Text="Address 1" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="addr1" RMXRef="/Instance/Entity/Addr1" RMXType="text" tabindex="3" />
          </span>
        </div>
        <div runat="server" class="half" id="div_stateid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_stateid" Text="State" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="stateid" CodeTable="states" ControlName="stateid" RMXRef="/Instance/Entity/StateId" RMXType="code" tabindex="13" />
          </span>
        </div>
        <div runat="server" class="half" id="div_addr2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_addr2" Text="Address 2" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="addr2" RMXRef="/Instance/Entity/Addr2" RMXType="text" tabindex="4" />
          </span>
        </div>
        <div runat="server" class="half" id="div_zipcode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_zipcode" Text="Zip/Postal Code" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="zipcode" RMXRef="/Instance/Entity/ZipCode" RMXType="zip" TabIndex="14" />
          </span>
        </div>
        <div runat="server" class="half" id="div_county" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_county" Text="County" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="county" RMXRef="/Instance/Entity/County" RMXType="text" tabindex="5" />
          </span>
        </div>
        <div runat="server" class="half" id="div_countrycode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_countrycode" Text="Country" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="countrycode" CodeTable="COUNTRY" ControlName="countrycode" RMXRef="/Instance/Entity/CountryCode" RMXType="code" tabindex="13" />
          </span>
        </div>
        <div runat="server" class="half" id="div_siccode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_siccode" Text="SIC Code" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="siccode" CodeTable="SIC_CODE" ControlName="siccode" RMXRef="/Instance/Entity/SicCode" RMXType="code" tabindex="16" />
          </span>
        </div>
        <div runat="server" class="half" id="div_freezepayments" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_freezepayments" Text="Freeze Payments" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="freezepayments" RMXRef="/Instance/Entity/FreezePayments" RMXType="checkbox" tabindex="32" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_naicscode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_naicscode" Text="NAICS Code" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="naicscode" CodeTable="NAICS_CODES" ControlName="naicscode" RMXRef="/Instance/Entity/NaicsCode" RMXType="code" tabindex="18" />
          </span>
        </div>
        <div runat="server" class="half" id="div_businesstype" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_businesstype" Text="Business Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="businesstype" CodeTable="BUSINESS_TYPE" ControlName="businesstype" RMXRef="/Instance/Entity/BusinessTypeCode" RMXType="code" tabindex="17" />
          </span>
        </div>
        <div runat="server" class="half" id="div_parentoperation" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="required" id="lbl_parentoperation" Text="Parent" />
          <span class="formw">
            <asp:DropDownList runat="server" id="parentoperation" tabindex="7" RMXRef="/Instance/Entity/ParentEid/@codeid" RMXType="combobox" onchange="setDataChanged(true);" />
          </span>
        </div>
        <div runat="server" class="half" id="div_natureofbusiness" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_natureofbusiness" Text="Nature Of Business" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="natureofbusiness" RMXRef="/Instance/Entity/NatureOfBusiness" RMXType="text" tabindex="33" />
          </span>
        </div>
        <div runat="server" class="half" id="div_triggerdate" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_triggerdate" Text="Effective Date Trigger" />
          <span class="formw">
            <asp:DropDownList runat="server" id="triggerdate" tabindex="8" RMXRef="/Instance/Entity/TriggerDateField" RMXType="combobox" onchange="setDataChanged(true);">
              <asp:ListItem Value="" Text="" />
              <asp:ListItem Value="SYSTEM_DATE" Text="Current System Date" />
              <asp:ListItem Value="EVENT.DATE_OF_EVENT" Text="Event Date" />
              <asp:ListItem Value="CLAIM.DATE_OF_CLAIM" Text="Claim Date" />
              <asp:ListItem Value="POLICY.EFFECTIVE_DATE,SYSTEM_DATE" Text="Policy Effective Date" />
            </asp:DropDownList>
          </span>
        </div>
        <div runat="server" class="half" id="div_orgtypecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_orgtypecode" Text="Organization Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="orgtypecode" CodeTable="ORGANIZATION_TYPE" ControlName="orgtypecode" RMXRef="/Instance/Entity/OrganizationType" RMXType="code" tabindex="19" />
          </span>
        </div>
        <div runat="server" class="half" id="div_labeldr" xmlns="" xmlns:cul="remove">
          <span class="label">
            <asp:Label runat="server" id="labeldr" Text="Effective Date Range" />
          </span>
        </div>
        <div runat="server" class="half" id="div_todaterange" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_todaterange" Text="From" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="todaterange" RMXRef="/Instance/Entity/EffStartDate" RMXType="date" tabindex="10" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="todaterangebtn" tabindex="11" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "todaterange",
					ifFormat : "%m/%d/%Y",
					button : "todaterangebtn"
					}
					);
				</script>
          </span>
        </div>
        <div runat="server" class="half" id="div_fromdaterange" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_fromdaterange" Text="To" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="fromdaterange" RMXRef="/Instance/Entity/EffEndDate" RMXType="date" tabindex="20" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
            <input type="button" class="DateLookupControl" name="fromdaterangebtn" tabindex="21" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "fromdaterange",
					ifFormat : "%m/%d/%Y",
					button : "fromdaterangebtn"
					}
					);
				</script>
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABdeptinfo" id="FORMTABdeptinfo">
        <div runat="server" class="half" id="div_wcfn" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_wcfn" Text="WC Filing Number" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="wcfn" RMXRef="/Instance/Entity/WcFillingNumber" RMXType="text" tabindex="1" />
          </span>
        </div>
        <div runat="server" class="half" id="div_departmentcontact" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_departmentcontact" Text="Contact" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="departmentcontact" RMXRef="/Instance/Entity/Contact" RMXType="text" tabindex="2" />
          </span>
        </div>
        <div runat="server" class="half" id="div_taxid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_taxid" Text="Tax Id" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="taxidLostFocus(this);" id="taxid" RMXRef="/Instance/Entity/TaxId" RMXType="taxid" name="taxid" value="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_phone1" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_phone1" Text="Office Phone" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="phone1" RMXRef="/Instance/Entity/Phone1" RMXType="phone" tabindex="21" />
          </span>
        </div>
        <div runat="server" class="half" id="div_phone2" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_phone2" Text="Alt. Phone" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="phone2" RMXRef="/Instance/Entity/Phone2" RMXType="phone" tabindex="22" />
          </span>
        </div>
        <div runat="server" class="half" id="div_faxnumber" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_faxnumber" Text="Fax" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="faxnumber" RMXRef="/Instance/Entity/FaxNumber" RMXType="phone" tabindex="23" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emailtypecode" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emailtypecode" Text="EMail Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="emailtypecode" CodeTable="EMAIL_TYPE" ControlName="emailtypecode" RMXRef="/Instance/Entity/EmailTypeCode" RMXType="code" tabindex="24" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emailaddress" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_emailaddress" Text="EMail" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="emailaddress" RMXRef="/Instance/Entity/EmailAddress" RMXType="text" tabindex="26" />
          </span>
        </div>
        <div runat="server" class="half" id="div_entity1099reportable" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_entity1099reportable" Text="1099 Reportable" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="entity1099reportable" RMXRef="/Instance/Entity/Entity1099Reportable" RMXType="checkbox" tabindex="29" />
            <img src="../../Images/Blank.JPG" id="Img1" class="half" />
          </span>
        </div>
        <div runat="server" class="half" id="div_parent1099eid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_parent1099eid" Text="1099 Parent" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="parent1099eid" RMXRef="/Instance/Entity/Parent1099EID" RMXType="eidlookup" cancelledvalue="" tabindex="30" />
            <input type="button" class="button" value="..." name="parent1099eidbtn" tabindex="31" onclick="lookupData('parent1099eid','',4,'parent1099eid',2)" />
            <asp:TextBox style="display:none" runat="server" id="parent1099eid_cid" RMXref="/Instance/Entity/Parent1099EID/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_claiminstruction" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_claiminstruction" Text="Claim Instruction" />
          <span class="formw">
            <asp:TextBox runat="Server" id="claiminstruction" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimInstruction" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="11" TextMode="MultiLine" Columns="30" rows="5" />
            <input type="button" class="button" value="..." name="claiminstructionbtnMemo" tabindex="12" id="claiminstructionbtnMemo" onclick="EditMemo('claiminstruction','')" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto;position:relative;" runat="server" name="FORMTABclientlimit" id="FORMTABclientlimit">
        <div runat="server" class="partial" id="div_ClientLimitsGrid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_ClientLimitsGrid" Text="" />
          <span>
            <dg:UserControlDataGrid runat="server" ID="ClientLimitsGrid" GridName="ClientLimitsGrid" GridTitle="Client Limits" Target="/Instance/UI/FormVariables/SysExData/ClientLimits" Ref="/Instance/Document/OrgHierarchy/target_CL" Unique_Id="ClientLimitsRowId" ShowRadioButton="" Width="680px" Height="150px" hidenodes="|ClientLimitsRowId|ClientEid|PaymentFlag|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="250" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="ClientLimitsSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/ClientLimitsSelectedId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="ClientLimitsGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/ClientLimitsGrid_Action" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="ClientLimitsGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/ClientLimitsGrid_RowAddedFlag" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="ClientLimitsGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/ClientLimitsGrid_RowDeletedFlag" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="ClientLimitsGrid_ComboRelations" RMXRef="" RMXType="id" Text="PaymentFlag|PaymentFlag_Text;" xmlns:cul="remove" />
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABoperatingas" id="FORMTABoperatingas">
        <div runat="server" class="partial" id="div_EntityXOperatingAsGrid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_EntityXOperatingAsGrid" Text="" />
          <span>
            <dg:UserControlDataGrid runat="server" ID="EntityXOperatingAsGrid" GridName="EntityXOperatingAsGrid" GridTitle="Operating As" Target="/Instance/UI/FormVariables/SysExData/EntityXOperatingAs" Ref="/Instance/Document/OrgHierarchy/target_OA" Unique_Id="OperatingId" ShowRadioButton="" Width="680px" Height="150px" hidenodes="|OperatingId|EntityId|" ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="250px" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsSelectedId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsGrid_Action" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsGrid_RowAddedFlag" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsGrid_RowDeletedFlag" RMXType="id" xmlns:cul="remove" />
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABContactInfo" id="FORMTABContactInfo">
        <div runat="server" class="partial" id="div_EntityXContactInfoGrid" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_EntityXContactInfoGrid" Text="" />
          <span>
            <dg:UserControlDataGrid runat="server" ID="EntityXContactInfoGrid" GridName="EntityXContactInfoGrid" GridTitle="Contact Information" Target="/Instance/UI/FormVariables/SysExData/EntityXContactInfo" Ref="/Instance/Document/OrgHierarchy/target_CI" Unique_Id="ContactId" ShowRadioButton="" Width="680px" Height="150px" hidenodes="|ContactId|EntityId|" ShowHeader="True" LinkColumn="" PopupWidth="500px" PopupHeight="500px" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoSelectedId" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoGrid_Action" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoGrid_RowAddedFlag" RMXType="id" xmlns:cul="remove" />
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoGrid_RowDeletedFlag" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_hidden_sessionInfo" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="hidden_sessionInfo" RMXRef="/Instance/Document/session" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hidden_currCI" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="hidden_currCI" RMXRef="/Instance/Document/curr_CI" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hidden_currOA" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="hidden_currOA" RMXRef="/Instance/Document/curr_OA" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hidden_currCL" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="hidden_currCL" RMXRef="/Instance/Document/curr_CL" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hidden_OrgName" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="hidden_OrgName" RMXRef="/Instance/Document/value" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hidden_ExpInfoViewSec" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="hidden_ExpInfoViewSec" RMXRef="Instance/Document/OrgHierarchy/CompanyInfo/DepartmentInfo/ExpInfoSecurity/View" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_action" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="action" RMXRef="/Instance/UI/FormVariables/SysExData/action" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_parenteid" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="parenteid" RMXRef="/Instance/UI/FormVariables/SysExData/ParentEid" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_orglevel" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="orglevel" RMXRef="/Instance/UI/FormVariables/SysExData/orglevel" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_orglevelinfo" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="orglevelinfo" RMXRef="/Instance/UI/FormVariables/SysExData/orglevelinfo" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_hdnOnSave" style="display:none;" xmlns="" xmlns:cul="remove">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="hdnOnSave" RMXRef="Instance/Document/ToExposure" RMXType="hidden" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;height:310px;overflow:auto" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <asp:TextBox style="display:none" runat="server" id="supp_entity_id" RMXRef="/Instance/*/Supplementals/ENTITY_ID" RMXType="id" xmlns:cul="remove" />
        <div runat="server" class="half" id="div_supp_index_bureau_numb" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_index_bureau_numb" Text="Claim Index Bureau Number" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_index_bureau_numb" RMXRef="/Instance/*/Supplementals/INDEX_BUREAU_NUMB" RMXType="text" tabindex="1653" />
          </span>
        </div>
        <div runat="server" class="half" id="div_supp_ste_agnt_bru_idnt" xmlns="" xmlns:cul="remove">
          <asp:label runat="server" class="label" id="lbl_supp_ste_agnt_bru_idnt" Text="Pennsylvania Bureau Code" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_ste_agnt_bru_idnt" RMXRef="/Instance/*/Supplementals/STE_AGNT_BRU_IDNT" RMXType="text" tabindex="1654" />
          </span>
        </div>
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnBack" RMXRef="" Text="Back" onClientClick="return backOrgHierarchy();" />
        </div>
        <div class="formButton" runat="server" id="div_btnExp">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnExp" RMXRef="" Text="Exposure Information" onClientClick="return gotoExposure();" />
        </div>
        <div class="formButton" runat="server" id="div_btnSelf">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnSelf" RMXRef="" Text="Self Insured" onClientClick="return gotoSelfInsured();" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Entity" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Entity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Entity&gt;" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="16500" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="entityid" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="entitytableid,entitytype" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="orghierarchymaint" xmlns:cul="remove" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="orghierarchymaint" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="lastname|abbreviation|parentoperation|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>