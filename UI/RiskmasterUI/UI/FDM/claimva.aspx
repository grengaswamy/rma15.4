<%@ Page="" Language="C#" AutoEventWireup="true" CodeBehind="claimva.aspx.cs"  Inherits="Riskmaster.UI.FDM.Claimva" ValidateRequest="false" %>
  <%@ Register="" assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
      <head id="Head1" runat="server">
        <title>Vehicle Accident</title>
        <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
        <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
        <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />
        <script language="JavaScript" src="Scripts/form.js">{var i;}  </script>
        <script language="JavaScript" src="Scripts/drift.js">{var i;}  </script>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      </head>
      <body class="10pt" onload="pageLoaded();">
        <form name="frmData" id="frmData" runat="server">
          <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
          <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
          <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
          <asp:ScriptManager ID="SMgr" runat="server" />
          <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
            <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateNew" src="Images/new.gif" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='Images/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/new.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/save.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateFirst" src="Images/first.gif" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='Images/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/first.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigatePrev" src="Images/prev.gif" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='Images/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/prev.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateNext" src="Images/next.gif" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='Images/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/next.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateLast" src="Images/last.gif" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='Images/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/last.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateDelete" src="Images/delete.gif" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/delete.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClick="NavigateFirst" src="Images/esumm.gif" width="28" height="28" border="0" id="esumm" AlternateText="Executive Summary" onMouseOver="this.src='Images/esumm2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/esumm.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="attach()" src="Images/attach.gif" width="28" height="28" border="0" id="attach" AlternateText="Attach Documents" onMouseOver="this.src='Images/attach2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/attach.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="doSearch()" src="Images/search.gif" width="28" height="28" border="0" id="search" AlternateText="Search" onMouseOver="this.src='Images/search2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/search.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="doLookup()" src="Images/lookup.gif" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='Images/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/lookup.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="FilteredDiary()" src="Images/filtereddiary.gif" width="28" height="28" border="0" id="filtereddiary" AlternateText="View Record Diaries" onMouseOver="this.src='Images/filtereddiary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/filtereddiary.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="Diary()" src="Images/diary.gif" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='Images/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/diary.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="Comments()" src="Images/comments.gif" width="28" height="28" border="0" id="comments" AlternateText="Comments" onMouseOver="this.src='Images/comments2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/comments.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="EnhancedNotes()" src="Images/progressnotes.gif" width="28" height="28" border="0" id="enhancednotes" AlternateText="Enhanced Notes" onMouseOver="this.src='Images/progressnotes2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/progressnotes.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="MailMerge()" src="Images/mailmerge.gif" width="28" height="28" border="0" id="mailmerge" AlternateText="Mail Merge" onMouseOver="this.src='Images/mailmerge2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/mailmerge.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="recordSummary()" src="Images/recordsummary.gif" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/recordsummary.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="CommentSummary()" src="Images/commentsummary.gif" width="28" height="28" border="0" id="commentsummary" AlternateText="Claim Comment Summary" onMouseOver="this.src='Images/commentsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/commentsummary.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="EventExplorer()" src="Images/eventexplorer.gif" width="28" height="28" border="0" id="eventexplorer" AlternateText="Quick Summary" onMouseOver="this.src='Images/eventexplorer2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/eventexplorer.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="SendMail()" src="Images/sendregarding.gif" width="28" height="28" border="0" id="sendmail" AlternateText="Send Mail" onMouseOver="this.src='Images/sendregarding2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/sendregarding.gif';this.style.zoom='100%'" />
                </td>
                <td align="center" valign="middle" height="32" width="28" xmlns="">
                  <asp:ImageButton runat="server" OnClientClick="DeleteAllDiaries()" src="Images/diary_deleteall.gif" width="28" height="28" border="0" id="deletealldiaries" AlternateText="Delete All Diaries" onMouseOver="this.src='Images/diary_deleteall2.gif';this.style.zoom='110%'" onMouseOut="this.src='Images/diary_deleteall.gif';this.style.zoom='100%'" />
                </td>
              </tr>
            </table>
          </div>
          <br />
          <div class="msgheader" id="formtitle">Vehicle Accident</div>
          <div class="errtextheader"></div>
          <table border="0">
            <tr>
              <td>
                <table border="0" cellspacing="0" celpadding="0">
                  <tr>
                    <td class="Selected" nowrap="true" name="TABSclaiminfo" id="TABSclaiminfo">
                      <a class="Selected" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="claiminfo" id="LINKTABSclaiminfo">
                        <span style="text-decoration:none">Claim Info</span>
                      </a>
                    </td>
                    <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
                    <td class="NotSelected" nowrap="true" name="TABSeventdetail" id="TABSeventdetail">
                      <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="eventdetail" id="LINKTABSeventdetail">
                        <span style="text-decoration:none">Event Detail</span>
                      </a>
                    </td>
                    <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
                    <td class="NotSelected" nowrap="true" name="TABSvehicleaccident" id="TABSvehicleaccident">
                      <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="vehicleaccident" id="LINKTABSvehicleaccident">
                        <span style="text-decoration:none">Vehicle Accident</span>
                      </a>
                    </td>
                    <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
                    <td class="NotSelected" nowrap="true" name="TABSsuppgroup" id="TABSsuppgroup">
                      <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">
                        <span style="text-decoration:none">Supplementals</span>
                      </a>
                    </td>
                    <td nowrap="true" style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
                    <td valign="top" nowrap="true" />
                  </tr>
                </table>
                <div style="position:relative;left:0;top:0;width:870px;height:350px;overflow:auto" class="singletopborder">
                  <table border="0" cellspacing="0" celpadding="0" width="95%" height="95%">
                    <tr>
                      <td valign="top">
                        <table border="0" name="FORMTABclaiminfo" id="FORMTABclaiminfo">
                          <tr>
                            <asp:TextBox style="display:none" runat="server" id="eventid" RMXRef="/Instance/Claim/EventId" />
                            <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/Claim/ClaimId" />
                            <asp:TextBox style="display:none" runat="server" id="folder" RMXRef="/Instance/UI/FormVariables/SysExData/folder" />
                            <asp:TextBox style="display:none" runat="server" id="primaryClaimant" RMXRef="/Instance/UI/FormVariables/SysExData//DiaryMessage" />
                            <td nowrap="true" id="ev_eventnumber_ctlcol" xmlns="">
                              <b>
                                <u>Event Number</u>
                              </b>
                            </td>
                            <td nowrap="true" id="ev_eventnumber_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" id="ev_eventnumber" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventNumber" tabindex="1" cancelledvalue="" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="ev_eventnumber" />
                            </td>
                            <td id="ev_eventnumber_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_dateofevent_ctlcol" xmlns="">
                              <b>
                                <u>Date Of Event</u>
                              </b>
                            </td>
                            <td nowrap="true" id="ev_dateofevent_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="date" onchange="setDataChanged(true);" id="ev_dateofevent" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DateOfEvent" tabindex="18" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="ev_dateofevent" />
                              <cc1:CalendarExtender runat="server" id="ev_dateofevent_ajax" TargetControlID="ev_dateofevent" />
                            </td>
                            <td id="ev_dateofevent_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="claimnumber_ctlcol" xmlns="">
                              <b>
                                <u>Claim Number</u>
                              </b>
                            </td>
                            <td nowrap="true" id="claimnumber_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="claimnumber" RMXRef="/Instance/Claim/ClaimNumber" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="claimnumber" />
                            </td>
                            <td id="claimnumber_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_timeofevent_ctlcol" xmlns="">
                              <b>
                                <u>Time Of Event</u>
                              </b>
                            </td>
                            <td nowrap="true" id="ev_timeofevent_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="time" onchange="setDataChanged(true);" id="ev_timeofevent" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/TimeOfEvent" />
                              <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="ev_timeofevent" id="ev_timeofevent_ajax" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="ev_timeofevent" />
                            </td>
                            <td id="ev_timeofevent_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="claimtypecode_ctlcol" xmlns="">
                              <b>
                                <u>Claim Type</u>
                              </b>
                            </td>
                            <td nowrap="true" id="claimtypecode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="claimtypecode" RMXRef="/Instance/Claim/ClaimTypeCode" cancelledvalue="" tabindex="4" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="claimtypecodebtn" onclick="selectCode('CLAIM_TYPE','claimtypecode')" />
                              <asp:TextBox style="display:none" runat="server" id="claimtypecode_cid" RMXref="/Instance/Claim/ClaimTypeCode/@codeid" cancelledvalue="" tabindex="5" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="claimtypecode" />
                            </td>
                            <td id="claimtypecode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="dateofclaim_ctlcol" xmlns="">
                              <b>
                                <u>Date Of Claim</u>
                              </b>
                            </td>
                            <td nowrap="true" id="dateofclaim_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="date" onchange="setDataChanged(true);" id="dateofclaim" RMXRef="/Instance/Claim/DateOfClaim" tabindex="21" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="dateofclaim" />
                              <cc1:CalendarExtender runat="server" id="dateofclaim_ajax" TargetControlID="dateofclaim" />
                            </td>
                            <td id="dateofclaim_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_depteid_ctlcol" xmlns="">
                              <b>
                                <u>Department</u>
                              </b>
                            </td>
                            <td nowrap="true" id="ev_depteid_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="ev_depteid" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DeptEid" name="ev_depteid" cancelledvalue="" />
                              <asp:button id="Button1" runat="server" class="CodeLookupControl" Text="..." name="ev_depteidbtn" onclientclick="&#xA;                    selectCode('orgh','ev_depteid','Department')&#xA;                  " />
                              <asp:button runat="server" class="button" value="Instr" id="ev_depteidbtnInstructions" onclientclick="ClaimInstructions()" />
                              <asp:TextBox style="display:none" runat="server" id="ev_depteid_cid" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DeptEid/@codeid" cancelledvalue="" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="ev_depteid" />
                            </td>
                            <td id="ev_depteid_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="timeofclaim_ctlcol" xmlns="">
                              <b>
                                <u>Time Of Claim</u>
                              </b>
                            </td>
                            <td nowrap="true" id="timeofclaim_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="time" onchange="setDataChanged(true);" id="timeofclaim" RMXRef="/Instance/Claim/TimeOfClaim" />
                              <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="timeofclaim" id="timeofclaim_ajax" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="timeofclaim" />
                            </td>
                            <td id="timeofclaim_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="claimstatuscode_ctlcol" xmlns="">
                              <b>
                                <u>Claim Status</u>
                              </b>
                            </td>
                            <td nowrap="true" id="claimstatuscode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" cancelledvalue="" id="claimstatuscode" RMXRef="/Instance/Claim/ClaimStatusCode" tabindex="9" />
                              <input type="button" class="CodeLookupControl" id="claimstatuscodebtn" name="claimstatuscodebtn" onClick="selectCode('CLAIM_STATUS','claimstatuscode')" tabindex="10" />
                              <input type="button" class="button" value="Detail" name="claimstatuscodedetailbtn" onClick="selectCodeWithDetail('claimstatuscode',1)" tabindex="11" id="claimstatuscodedetailbtn" />
                              <asp:TextBox style="display:none" runat="server" id="claimstatuscode_cid" cancelledvalue="" oldvalue="" RMXref="/Instance/Claim/ClaimStatusCode/@codeid" />
                              <asp:RequiredFieldValidator ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="claimstatuscode" />
                            </td>
                            <td id="claimstatuscode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_datereported_ctlcol" xmlns="">Date Reported</td>
                            <td nowrap="true" id="ev_datereported_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="date" onchange="setDataChanged(true);" id="ev_datereported" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/DateReported" tabindex="24" />
                              <cc1:CalendarExtender runat="server" id="ev_datereported_ajax" TargetControlID="ev_datereported" />
                            </td>
                            <td id="ev_datereported_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="dttmclosed_ctlcol" xmlns="">Date Closed</td>
                            <td nowrap="true" id="dttmclosed_td" xmlns="">
                              <asp:TextBox size="30" runat="Server" RMXref="/Instance/Claim/DttmClosed" id="dttmclosed" tabindex="12" style="background-color: silver;" readonly="true" />
                            </td>
                            <td id="dttmclosed_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_timereported_ctlcol" xmlns="">Time Reported</td>
                            <td nowrap="true" id="ev_timereported_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="time" onchange="setDataChanged(true);" id="ev_timereported" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/TimeReported" />
                              <cc1:MaskedEditExtender runat="server" Enabled="True" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" TargetControlID="ev_timereported" id="ev_timereported_ajax" />
                            </td>
                            <td id="ev_timereported_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="methodclosedcode_ctlcol" xmlns="">Close Method</td>
                            <td nowrap="true" id="methodclosedcode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="methodclosedcode" RMXRef="/Instance/Claim/MethodClosedCode" cancelledvalue="" tabindex="13" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="methodclosedcodebtn" onclick="selectCode('CLOSE_METHOD','methodclosedcode')" />
                              <asp:TextBox style="display:none" runat="server" id="methodclosedcode_cid" RMXref="/Instance/Claim/MethodClosedCode/@codeid" cancelledvalue="" tabindex="14" />
                            </td>
                            <td id="methodclosedcode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="estcollection_ctlcol" xmlns="">Est. Collection</td>
                            <td nowrap="true" id="estcollection_td" xmlns="">
                              <asp:TextBox runat="server" size="30" id="estcollection" RMXRef="/Instance/Claim/EstCollection" onblur="&#xA;                ;currencyLostFocus(this);&#xA;              " rmxforms:as="currency" tabindex="27" onchange="&#xA;                ;setDataChanged(true);&#xA;              " />
                              <cc1:MaskedEditExtender runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="estcollection" id="estcollection_ajax" />
                            </td>
                            <td id="estcollection_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="filenumber_ctlcol" xmlns="">File Number</td>
                            <td nowrap="true" id="filenumber_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="filenumber" RMXRef="/Instance/Claim/FileNumber" />
                            </td>
                            <td id="filenumber_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="primarypolicyid_ctlcol" xmlns="">Policy Name</td>
                            <td nowrap="true" id="primarypolicyid_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeydown="eatKeystrokes();" id="primarypolicyid" RMXRef="/Instance/Claim/PrimaryPolicyId/@defaultdetail" />
                              <input type="button" class="button" value="..." name="primarypolicyidbtn" onclientclick="lookupClaimPolicy('primarypolicyid','')" />
                              <asp:TextBox style="display:none" runat="server" id="primarypolicyid_cid" RMXRef="/Instance/Claim/PrimaryPolicyId" />
                              <input type="button" class="button" id="primarypolicyid_open" value="Open" tabindex="30" onClick="&#xA;                  XFormHandler('SysFormPIdName=claimid&SysFormName=policy&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','1','policylookup')&#xA;                " />
                            </td>
                            <td id="primarypolicyid_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="currentadjuster_ctlcol" xmlns="">Current Adjuster</td>
                            <td nowrap="true" id="currentadjuster_td" xmlns="">
                              <asp:TextBox size="30" runat="Server" RMXref="/Instance/UI/FormVariables/SysExData//CurrentAdjuster" id="currentadjuster" tabindex="16" style="background-color: silver;" readonly="true" />
                            </td>
                            <td id="currentadjuster_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="servicecode_ctlcol" xmlns="">Service Code</td>
                            <td nowrap="true" id="servicecode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="servicecode" RMXRef="/Instance/Claim/ServiceCode" cancelledvalue="" tabindex="31" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="servicecodebtn" onclick="selectCode('SERVICE_CODE','servicecode')" />
                              <asp:TextBox style="display:none" runat="server" id="servicecode_cid" RMXref="/Instance/Claim/ServiceCode/@codeid" cancelledvalue="" tabindex="32" />
                            </td>
                            <td id="servicecode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="paymntfrozenflag_ctlcol" xmlns="">Payments Frozen</td>
                            <td nowrap="true" id="paymntfrozenflag_td" xmlns="">
                              <asp:CheckBox onchange="setDataChanged(true);" runat="Server" id="paymntfrozenflag" RMXRef="/Instance/Claim/PaymntFrozenFlag" tabindex="17" />
                            </td>
                            <td id="paymntfrozenflag_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="lssclaimind_ctlcol" xmlns="">LSS Claim</td>
                            <td nowrap="true" id="lssclaimind_td" xmlns="">
                              <asp:CheckBox onchange="setDataChanged(true);" runat="Server" id="lssclaimind" RMXRef="/Instance/Claim/LssClaimInd" tabindex="80" />
                            </td>
                            <td id="lssclaimind_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="statuschangeapprovedby_ctlcol" xmlns="" />
                            <td nowrap="true" id="statuschangeapprovedby_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="statuschangeapprovedby" RMXRef="/Instance/UI/FormVariables/SysExData/StatusApprovedBy" />
                            </td>
                            <td id="statuschangeapprovedby_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="statuschangedate_ctlcol" xmlns="" />
                            <td nowrap="true" id="statuschangedate_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="statuschangedate" RMXRef="/Instance/UI/FormVariables/SysExData/StatusDateChg" />
                            </td>
                            <td id="statuschangedate_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="statuschangereason_ctlcol" xmlns="" />
                            <td nowrap="true" id="statuschangereason_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="statuschangereason" RMXRef="/Instance/UI/FormVariables/SysExData/StatusReason" />
                            </td>
                            <td id="statuschangereason_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="statusdiaryclose_ctlcol" xmlns="" />
                            <td nowrap="true" id="statusdiaryclose_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="statusdiaryclose" RMXRef="Instance/UI/FormVariables/SysExData/CloseDiary" />
                            </td>
                            <td id="statusdiaryclose_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="containsopendiaries_ctlcol" xmlns="" />
                            <td nowrap="true" id="containsopendiaries_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="containsopendiaries" RMXRef="Instance/UI/FormVariables/SysExData/ContainsOpenDiaries" />
                            </td>
                            <td id="containsopendiaries_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="AllCodes_ctlcol" xmlns="" />
                            <td nowrap="true" id="AllCodes_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="AllCodes" RMXRef="Instance/UI/FormVariables/SysExData/AllCodes" />
                            </td>
                            <td id="AllCodes_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="deleteautocheck_ctlcol" xmlns="" />
                            <td nowrap="true" id="deleteautocheck_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="deleteautocheck" RMXRef="Instance/UI/FormVariables/SysExData/DeleteAutoCheck" />
                            </td>
                            <td id="deleteautocheck_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="containsautochecks_ctlcol" xmlns="" />
                            <td nowrap="true" id="containsautochecks_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="containsautochecks" RMXRef="Instance/UI/FormVariables/SysExData/ContainsAutoChecks" />
                            </td>
                            <td id="containsautochecks_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="initialclaimstatus_ctlcol" xmlns="" />
                            <td nowrap="true" id="initialclaimstatus_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="initialclaimstatus" RMXRef="Instance/UI/FormVariables/SysExData/ClaimStatusCode" />
                            </td>
                            <td id="initialclaimstatus_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="currentadjuster1_ctlcol" xmlns="">Current Adjuster</td>
                            <td nowrap="true" id="currentadjuster1_td" xmlns="">
                              <asp:TextBox size="30" runat="Server" RMXref="/Instance/UI/FormVariables/SysExData//CurrentAdjuster" id="currentadjuster1" tabindex="16" style="background-color: silver;" readonly="true" />
                            </td>
                            <td id="currentadjuster1_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="empncciclasscode_empinfo_ctlcol" xmlns="">NCCI Class</td>
                            <td nowrap="true" id="empncciclasscode_empinfo_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="empncciclasscode_empinfo" RMXRef="/Instance/Claim/PrimaryPiEmployee/NcciClassCode" cancelledvalue="" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="button" id="empncciclasscode_empinfobtn" OnClick="&#xA;                    selectCode('NCCI_CLASS_CODE','empncciclasscode_empinfo','','1')&#xA;                  " value="..." />
                              <asp:TextBox style="display:none" runat="server" id="empncciclasscode_empinfo_cid" cancelledvalue="" oldvalue="" />
                            </td>
                            <td id="empncciclasscode_empinfo_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                        </table>
                        <table border="0" style="display:none;" name="FORMTABeventdetail" id="FORMTABeventdetail">
                          <tr>
                            <td nowrap="true" id="ev_onpremiseflag_ctlcol" xmlns="">Event On Premise</td>
                            <td nowrap="true" id="ev_onpremiseflag_td" xmlns="">
                              <asp:CheckBox onchange="setDataChanged(true);" runat="Server" id="ev_onpremiseflag" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/OnPremiseFlag" onclick="FillEventLocationDetails(this)" tabindex="33" />
                            </td>
                            <td id="ev_onpremiseflag_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_locationtypecode_ctlcol" xmlns="">Location Type</td>
                            <td nowrap="true" id="ev_locationtypecode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="ev_locationtypecode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationTypeCode" cancelledvalue="" tabindex="49" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="ev_locationtypecodebtn" onclick="selectCode('LOCATION_TYPE','ev_locationtypecode')" />
                              <asp:TextBox style="display:none" runat="server" id="ev_locationtypecode_cid" RMXref="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationTypeCode/@codeid" cancelledvalue="" tabindex="50" />
                            </td>
                            <td id="ev_locationtypecode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_locationareadesc_ctlcol" xmlns="">Location Description</td>
                            <td nowrap="true" id="ev_locationareadesc_td" xmlns="">
                              <asp:TextBox runat="Server" id="ev_locationareadesc" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationAreaDesc" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="34" cols="30" rows="5"></asp:TextBox>
                              <input type="button" class="button" value="..." name="ev_locationareadescbtnMemo" tabindex="35" id="ev_locationareadescbtnMemo" onclick=" EditHTMLMemo('ev_locationareadesc','yes')" />
                              <asp:TextBox style="display:none" runat="server" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/LocationAreaDesc_HTMLComments" id="ev_locationareadesc_HTML" />
                            </td>
                            <td id="ev_locationareadesc_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_eventdescription_ctlcol" xmlns="">Event Description</td>
                            <td nowrap="true" id="ev_eventdescription_td" xmlns="">
                              <asp:TextBox runat="Server" id="ev_eventdescription" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventDescription" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="51" cols="30" rows="5"></asp:TextBox>
                              <input type="button" class="button" value="..." name="ev_eventdescriptionbtnMemo" tabindex="52" id="ev_eventdescriptionbtnMemo" onclick=" EditHTMLMemo('ev_eventdescription','yes')" />
                              <asp:TextBox style="display:none" runat="server" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventDescription_HTMLComments" id="ev_eventdescription_HTML" />
                            </td>
                            <td id="ev_eventdescription_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_addr1_ctlcol" xmlns="">Location Address 1</td>
                            <td nowrap="true" id="ev_addr1_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="ev_addr1" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/Addr1" />
                            </td>
                            <td id="ev_addr1_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_causecode_ctlcol" xmlns="">Cause Code</td>
                            <td nowrap="true" id="ev_causecode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="ev_causecode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CauseCode" cancelledvalue="" tabindex="53" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="ev_causecodebtn" onclick="selectCode('CAUSE_CODE','ev_causecode')" />
                              <asp:TextBox style="display:none" runat="server" id="ev_causecode_cid" RMXref="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CauseCode/@codeid" cancelledvalue="" tabindex="54" />
                            </td>
                            <td id="ev_causecode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_addr2_ctlcol" xmlns="">Location Address 2</td>
                            <td nowrap="true" id="ev_addr2_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="ev_addr2" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/Addr2" />
                            </td>
                            <td id="ev_addr2_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_noofinjuries_ctlcol" xmlns="">Number of Injuries</td>
                            <td nowrap="true" id="ev_noofinjuries_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="ev_noofinjuries" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/NoOfInjuries" tabindex="55" min="0" onChange="setDataChanged(true);" />
                              <cc1:MaskedEditExtender runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="ev_noofinjuries" id="ev_noofinjuries_ajax" />
                            </td>
                            <td id="ev_noofinjuries_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_city_ctlcol" xmlns="">City</td>
                            <td nowrap="true" id="ev_city_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="ev_city" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/City" />
                            </td>
                            <td id="ev_city_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="ev_nooffatalities_ctlcol" xmlns="">Number Of Fatalities</td>
                            <td nowrap="true" id="ev_nooffatalities_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="ev_nooffatalities" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/NoOfFatalities" tabindex="56" min="0" onChange="setDataChanged(true);" />
                              <cc1:MaskedEditExtender runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="ev_nooffatalities" id="ev_nooffatalities_ajax" />
                            </td>
                            <td id="ev_nooffatalities_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_stateid_ctlcol" xmlns="">State</td>
                            <td nowrap="true" id="ev_stateid_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="ev_stateid" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/StateId" cancelledvalue="" tabindex="39" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="ev_stateidbtn" onclick="selectCode('states','ev_stateid')" />
                              <asp:TextBox style="display:none" runat="server" id="ev_stateid_cid" RMXref="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/StateId/@codeid" cancelledvalue="" tabindex="40" />
                            </td>
                            <td id="ev_stateid_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_zipcode_ctlcol" xmlns="">Zip</td>
                            <td nowrap="true" id="ev_zipcode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="ev_zipcode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/ZipCode" TabIndex="41" />
                            </td>
                            <td id="ev_zipcode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_countyofinjury_ctlcol" xmlns="">County of Injury</td>
                            <td nowrap="true" id="ev_countyofinjury_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="ev_countyofinjury" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountyOfInjury" />
                            </td>
                            <td id="ev_countyofinjury_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_countrycode_ctlcol" xmlns="">Country</td>
                            <td nowrap="true" id="ev_countrycode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="ev_countrycode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountryCode" cancelledvalue="" tabindex="43" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="ev_countrycodebtn" onclick="selectCode('COUNTRY','ev_countrycode')" />
                              <asp:TextBox style="display:none" runat="server" id="ev_countrycode_cid" RMXref="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/CountryCode/@codeid" cancelledvalue="" tabindex="44" />
                            </td>
                            <td id="ev_countrycode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="ev_primaryloccode_ctlcol" xmlns="">Primary Location</td>
                            <td nowrap="true" id="ev_primaryloccode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="ev_primaryloccode" RMXRef="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/PrimaryLocCode" cancelledvalue="" tabindex="45" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="ev_primaryloccodebtn" onclick="selectCode('PRIMARY_LOCATION','ev_primaryloccode')" />
                              <asp:TextBox style="display:none" runat="server" id="ev_primaryloccode_cid" RMXref="/Instance/UI/FormVariables/SysExData/Parent/Instance/Event/PrimaryLocCode/@codeid" cancelledvalue="" tabindex="46" />
                            </td>
                            <td id="ev_primaryloccode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="PoliceAgencyEid_ctlcol" xmlns="">Authority Contacted</td>
                            <td nowrap="true" id="PoliceAgencyEid_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="PoliceAgencyEid" RMXRef="Instance/Claim/PoliceAgencyEid" cancelledvalue="" tabindex="47" />
                              <input type="button" class="button" value="..." name="PoliceAgencyEidbtn" tabindex="48" onclick="lookupData('PoliceAgencyEid','POLICE_AGENCY',4,'PoliceAgencyEid',2)" />
                              <asp:TextBox style="display:none" runat="server" id="PoliceAgencyEid_cid" RMXref="Instance/Claim/PoliceAgencyEid/@codeid" cancelledvalue="" />
                            </td>
                            <td id="PoliceAgencyEid_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                        </table>
                        <table border="0" style="display:none;" name="FORMTABvehicleaccident" id="FORMTABvehicleaccident">
                          <tr>
                            <td nowrap="true" id="accidenttypecode_ctlcol" xmlns="">Accident Type</td>
                            <td nowrap="true" id="accidenttypecode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="accidenttypecode" RMXRef="/Instance/Claim/AccidentTypeCode" cancelledvalue="" tabindex="58" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="accidenttypecodebtn" onclick="selectCode('ACCIDENT_TYPE','accidenttypecode')" />
                              <asp:TextBox style="display:none" runat="server" id="accidenttypecode_cid" RMXref="/Instance/Claim/AccidentTypeCode/@codeid" cancelledvalue="" tabindex="59" />
                            </td>
                            <td id="accidenttypecode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="intrafficflag_ctlcol" xmlns="">In Traffic</td>
                            <td nowrap="true" id="intrafficflag_td" xmlns="">
                              <asp:CheckBox onchange="setDataChanged(true);" runat="Server" id="intrafficflag" RMXRef="/Instance/Claim/InTrafficFlag" tabindex="67" />
                            </td>
                            <td id="intrafficflag_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="accidentdesccode_ctlcol" xmlns="">Accident Desc</td>
                            <td nowrap="true" id="accidentdesccode_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="accidentdesccode" RMXRef="/Instance/Claim/AccidentDescCode" cancelledvalue="" tabindex="60" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="accidentdesccodebtn" onclick="selectCode('ACCIDENT_DESC_CODE','accidentdesccode')" />
                              <asp:TextBox style="display:none" runat="server" id="accidentdesccode_cid" RMXref="/Instance/Claim/AccidentDescCode/@codeid" cancelledvalue="" tabindex="61" />
                            </td>
                            <td id="accidentdesccode_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="reportableflag_ctlcol" xmlns="">Reportable</td>
                            <td nowrap="true" id="reportableflag_td" xmlns="">
                              <asp:CheckBox onchange="setDataChanged(true);" runat="Server" id="reportableflag" RMXRef="/Instance/Claim/ReportableFlag" tabindex="68" />
                            </td>
                            <td id="reportableflag_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="datefddotrpt_ctlcol" xmlns="">Date of Federal DOT Report</td>
                            <td nowrap="true" id="datefddotrpt_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="date" onchange="setDataChanged(true);" id="datefddotrpt" RMXRef="/Instance/Claim/DateFddotRpt" tabindex="62" />
                              <cc1:CalendarExtender runat="server" id="datefddotrpt_ajax" TargetControlID="datefddotrpt" />
                            </td>
                            <td id="datefddotrpt_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="preventableflag_ctlcol" xmlns="">Preventable</td>
                            <td nowrap="true" id="preventableflag_td" xmlns="">
                              <asp:CheckBox onchange="setDataChanged(true);" runat="Server" id="preventableflag" RMXRef="/Instance/Claim/PreventableFlag" tabindex="69" />
                            </td>
                            <td id="preventableflag_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="datestdotrpt_ctlcol" xmlns="">Date of State Spill Report</td>
                            <td nowrap="true" id="datestdotrpt_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="date" onchange="setDataChanged(true);" id="datestdotrpt" RMXRef="/Instance/Claim/DateStdotRpt" tabindex="64" />
                              <cc1:CalendarExtender runat="server" id="datestdotrpt_ajax" TargetControlID="datestdotrpt" />
                            </td>
                            <td id="datestdotrpt_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                          <tr>
                            <td nowrap="true" id="stdotrptid_ctlcol" xmlns="">DOT Report #</td>
                            <td nowrap="true" id="stdotrptid_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="stdotrptid" RMXRef="/Instance/Claim/StDotRptId" />
                            </td>
                            <td id="stdotrptid_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td nowrap="true" id="dupeoverride_ctlcol" xmlns="" />
                            <td nowrap="true" id="dupeoverride_td" xmlns="">
                              <asp:TextBox style="display:none" runat="server" id="dupeoverride" RMXRef="/Instance/UI/FormVariables/SysExData/dupeoverride" />
                            </td>
                            <td id="dupeoverride_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          </tr>
                        </table>
                        <table border="0" style="display:none;" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
                          <tr>
                            <asp:TextBox style="display:none" runat="server" id="supp_claim_id" RMXRef="/Instance/*/Supplementals/CLAIM_ID" />
                          </tr>
                          <tr>
                            <td nowrap="true" id="supp_empl_enrl_mco_code_ctlcol" xmlns="">Employer MCO Enrolled</td>
                            <td nowrap="true" id="supp_empl_enrl_mco_code_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="supp_empl_enrl_mco_code" RMXRef="/Instance/*/Supplementals/EMPL_ENRL_MCO_CODE" cancelledvalue="" tabindex="1896" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="supp_empl_enrl_mco_codebtn" onclick="selectCode('YES_NO','supp_empl_enrl_mco_code')" />
                              <asp:TextBox style="display:none" runat="server" id="supp_empl_enrl_mco_code_cid" RMXref="/Instance/*/Supplementals/EMPL_ENRL_MCO_CODE/@codeid" cancelledvalue="" tabindex="1897" />
                            </td>
                            <td id="supp_empl_enrl_mco_code_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <asp:TextBox style="display:none" runat="server" name="supp_empl_enrl_mco_code_GroupAssoc" id="supp_empl_enrl_mco_code_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" />
                          </tr>
                          <tr>
                            <td nowrap="true" id="supp_typeofreport_code_ctlcol" xmlns="">Type Of Report</td>
                            <td nowrap="true" id="supp_typeofreport_code_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="supp_typeofreport_code" RMXRef="/Instance/*/Supplementals/TYPEOFREPORT_CODE" cancelledvalue="" tabindex="1897" onblur="codeLostFocus(this.id);" />
                              <input type="button" class="CodeLookupControl" name="supp_typeofreport_codebtn" onclick="selectCode('GA_REPORT_TYP_CODE','supp_typeofreport_code')" />
                              <asp:TextBox style="display:none" runat="server" id="supp_typeofreport_code_cid" RMXref="/Instance/*/Supplementals/TYPEOFREPORT_CODE/@codeid" cancelledvalue="" tabindex="1898" />
                            </td>
                            <td id="supp_typeofreport_code_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <asp:TextBox style="display:none" runat="server" name="supp_typeofreport_code_GroupAssoc" id="supp_typeofreport_code_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" />
                          </tr>
                          <tr>
                            <td nowrap="true" id="supp_able_rtw_date_ctlcol" xmlns="">Date Able to RTW</td>
                            <td nowrap="true" id="supp_able_rtw_date_td" xmlns="">
                              <asp:TextBox runat="server" size="30" FormatAs="date" onchange="setDataChanged(true);" id="supp_able_rtw_date" RMXRef="/Instance/*/Supplementals/ABLE_RTW_DATE" tabindex="1898" />
                              <cc1:CalendarExtender runat="server" id="supp_able_rtw_date_ajax" TargetControlID="supp_able_rtw_date" />
                            </td>
                            <td id="supp_able_rtw_date_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <asp:TextBox style="display:none" runat="server" name="supp_able_rtw_date_GroupAssoc" id="supp_able_rtw_date_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" />
                          </tr>
                          <tr>
                            <td nowrap="true" id="supp_perm_loss_numb_ctlcol" xmlns="">Permanent Percent Loss</td>
                            <td nowrap="true" id="supp_perm_loss_numb_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="supp_perm_loss_numb" RMXRef="/Instance/*/Supplementals/PERM_LOSS_NUMB" tabindex="1899" onChange="setDataChanged(true);" />
                              <cc1:MaskedEditExtender runat="server" MaskType="Number" Mask="9999999.99" TargetControlID="supp_perm_loss_numb" id="supp_perm_loss_numb_ajax" />
                            </td>
                            <td id="supp_perm_loss_numb_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <asp:TextBox style="display:none" runat="server" name="supp_perm_loss_numb_GroupAssoc" id="supp_perm_loss_numb_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" />
                          </tr>
                          <tr>
                            <td nowrap="true" id="supp_perm_loss_text_ctlcol" xmlns="">Permanent Body Part Loss</td>
                            <td nowrap="true" id="supp_perm_loss_text_td" xmlns="">
                              <asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" id="supp_perm_loss_text" RMXRef="/Instance/*/Supplementals/PERM_LOSS_TEXT" />
                            </td>
                            <td id="supp_perm_loss_text_td_sp" xmlns="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <asp:TextBox style="display:none" runat="server" name="supp_perm_loss_text_GroupAssoc" id="supp_perm_loss_text_GroupAssoc" value="CLAIM.FILING_STATE_ID:|12|" />
                          </tr>
                        </table>
                      </td>
                      <td valign="top" />
                    </tr>
                  </table>
                </div>
                <table>
                  <tr>
                    <td id="btnPI_td">
                      <asp:Button class="button" runat="Server" id="btnPI" Text="Persons Involved" OnClientClick="&#xA;              XFormHandler('','SysFormName=personinvolvedlist&SysCmd=1&SysViewType=controlsonly&SysEx=/Instance/Claim/EventId | /Instance/Claim/EventNumber','','')&#xA;            " />
                    </td>
                    <td id="btnAdjuster_td">
                      <asp:Button class="button" runat="Server" id="btnAdjuster" Text="Adjuster" OnClientClick="&#xA;              XFormHandler('','SysFormPIdName=claimid&SysFormName=adjuster&SysCmd=1&SysViewType=tab&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;            " />
                    </td>
                    <td id="btnLitigation_td">
                      <asp:Button class="button" runat="Server" id="btnLitigation" Text="Litigation" OnClientClick="&#xA;              XFormHandler('','SysFormPIdName=claimid&SysFormName=litigation&SysCmd=1&SysViewType=tab&SysFormIdName=litigationrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;            " />
                    </td>
                    <td id="btnDefendant_td">
                      <asp:Button class="button" runat="Server" id="btnDefendant" Text="Defendant" OnClientClick="&#xA;              XFormHandler('','SysFormPIdName=claimid&SysFormName=defendant&SysCmd=1&SysViewType=tab&SysFormIdName=defendantrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;            " />
                    </td>
                    <td id="btnClaimant_td">
                      <asp:Button class="button" runat="Server" id="btnClaimant" Text="Claimants" OnClientClick="&#xA;              XFormHandler('','SysFormPIdName=claimid&SysFormName=claimantlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=claimantrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;            " />
                    </td>
                    <td id="btnUnit_td">
                      <asp:Button class="button" runat="Server" id="btnUnit" Text="Unit" OnClientClick="&#xA;              XFormHandler('','SysFormName=unit&SysCmd=1&SysEx=/Instance/Claim/ClaimId','','')&#xA;            " />
                    </td>
                    <td id="btnClaimForms_td">
                      <asp:Button class="button" runat="Server" id="btnClaimForms" Text="Claim Forms" OnClientClick="&#xA;              XFormHandler('','SysFormPIdName=claimid&SysFormName=&SysCmd=1&SysViewType=tab&SysFormIdName=&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber','','')&#xA;            " />
                    </td>
                    <td id="btnFinancials_td">
                      <script language="JavaScript" src="">{var i;}          </script>
                      <input type="button" class="button" id="btnFinancials" RMXRef="" value="Financials" onClick="gotoreserves();" />
                    </td>
                  </tr>
                </table>
                <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" Text="Navigate" />
                <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" Text="Claim" />
                <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" Text="6000" />
                <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" name="SysSerializationConfig" Text="&lt;Claim&gt;&lt;PrimaryClaimant&gt;&lt;/PrimaryClaimant&gt;&lt;Acord&gt;&lt;/Acord&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;Jurisdictionals&gt;&lt;/Jurisdictionals&gt;&lt;/Claim&gt;" />
                <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" Text="claimid" />
                <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" Text="claimva" />
                <asp:TextBox style="display:none" runat="server" id="SysKilledNodes" RMXRef="Instance/UI/FormVariables/SysKilledNodes" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" Text="" />
                <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" Text="claimnumber|ev_eventnumber|ev_timeofevent|timeofclaim" />
                <asp:TextBox style="display:none" runat="server" id="SysSupplemental" RMXRef="Instance/UI/FormVariables/SysSupplemental" Text="formname=claim_supp&sys_formidname=claim_id&claim_id=%claimid%&sys_formpform=claim&sys_formpidname=claim_id" />
                <asp:TextBox style="display:none" runat="server" id="SysLOB" RMXRef="Instance/UI/FormVariables/SysLOB" Text="242" />
                <asp:TextBox style="display:none" runat="server" id="line_of_bus_code" RMXRef="Instance/UI/MissingRef" Text="" />
                <asp:TextBox style="display:none" runat="server" id="OH_FACILITY_EID" RMXRef="Instance/UI/FormVariables/SysExData//OH_FACILITY_EID" Text="0" />
                <asp:TextBox style="display:none" runat="server" id="OH_LOCATION_EID" RMXRef="Instance/UI/FormVariables/SysExData//OH_LOCATION_EID" Text="0" />
                <asp:TextBox style="display:none" runat="server" id="OH_DIVISION_EID" RMXRef="Instance/UI/FormVariables/SysExData//OH_DIVISION_EID" Text="0" />
                <asp:TextBox style="display:none" runat="server" id="OH_REGION_EID" RMXRef="Instance/UI/FormVariables/SysExData//OH_REGION_EID" Text="0" />
                <asp:TextBox style="display:none" runat="server" id="OH_OPERATION_EID" RMXRef="Instance/UI/FormVariables/SysExData//OH_OPERATION_EID" Text="0" />
                <asp:TextBox style="display:none" runat="server" id="OH_COMPANY_EID" RMXRef="Instance/UI/FormVariables/SysExData//OH_COMPANY_EID" Text="0" />
                <asp:TextBox style="display:none" runat="server" id="OH_CLIENT_EID" RMXRef="Instance/UI/FormVariables/SysExData//OH_CLIENT_EID" Text="0" />
                <asp:TextBox style="display:none" runat="server" name="formname" value="claimva" />
                <asp:TextBox style="display:none" runat="server" name="SysRequired" value="ev_eventnumber|ev_dateofevent|claimnumber|ev_timeofevent|claimtypecode_cid|dateofclaim|ev_depteid_cid|timeofclaim|claimstatuscode|" />
                <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="ev_eventnumber|ev_onpremiseflag|accidenttypecode|" />
                <input type="hidden" id="hdSaveButtonClicked" />
                <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupClass" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupRecordId" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupAttachNodePath" style="display:none" />
                <asp:TextBox runat="server" id="SysLookupResultConfig" style="display:none" />
                <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
              </td>
            </tr>
          </table>
        </form>
      </body>
    </html>
