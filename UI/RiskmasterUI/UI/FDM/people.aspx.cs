﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.AppHelpers;
using System.Web.UI.HtmlControls;
////dbisht6 start on mits35331
//using Riskmaster.AppHelpers;

////dbisht6 end

namespace Riskmaster.UI.FDM
{
    public partial class People : FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Added Rakhi forR7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            TextBox txtPrimaryAddressChanged = (TextBox)this.FindControl("PrimaryAddressChanged");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                if (txtPrimaryAddressChanged != null)
                    IncludeLastRecord(txtPrimaryAddressChanged.Text);
            }
            //Added Rakhi forR7:Add Emp Data Elements
            //RMA-6871
            string sSysCmd = string.Empty;
            TextBox txtSysCmd = ((TextBox)this.Form.FindControl("SysCmd"));
            if (txtSysCmd != null)
            {
                if (!string.IsNullOrEmpty(txtSysCmd.Text))
                {
                    sSysCmd = txtSysCmd.Text;
                }
            }
            //RMA-6871
            FDMPageLoad();

            //Added Rakhi forR7:Add Emp Data Elements
            if (txtPrimaryAddressChanged != null)
                txtPrimaryAddressChanged.Text = "false"; //Setting it false after the processing is over.
            //Added Rakhi forR7:Add Emp Data Elements
            //mbahl3 mits:29316
                       
                    //if (this.FindControl("typeid") != null)
                    //{
                    //    var sSelectedValue = ((DropDownList)this.FindControl("typeid")).SelectedValue;

                    //    if(this.FindControl("ssnCodeId") != null)
                    //    {
                    //    if (sSelectedValue == ((TextBox)this.FindControl("ssnCodeId")).Text)
                    //    {
                    //        if (this.FindControl("lbl_taxid") != null)
                    //        {
                    //            ((Label)this.FindControl("lbl_taxid")).Text = "SSN";
                    //        }
                    //        if (this.FindControl("taxid") != null)
                    //        {
                    //            ((TextBox)this.FindControl("taxid")).Attributes["onblur"] = "ssnLostFocus(this);"; 
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (this.FindControl("lbl_taxid") != null)
                    //        {
                    //            ((Label)this.FindControl("lbl_taxid")).Text = "FEIN";
                    //        }
                    //        if (this.FindControl("taxid") != null)
                    //        {
                    //            ((TextBox)this.FindControl("taxid")).Attributes["onblur"] = "FEINLostFocus(this);";
                    //        }
                    //    }
                    //        }
                //  }
            //mbahl3 mits:29316
            //RMA-6871
            if (Request.QueryString["IsFromQuickLookup"] != null)
            {
                if (Request.QueryString["IsFromQuickLookup"].ToString() == "true")
                {
                    foreach (Control c in this.FindControl("toolbardrift").Controls)
                    {
                        if ((c as HtmlGenericControl) == null) continue;
                        if (((HtmlGenericControl)c).ID.ToLower() != "div_save")
                        {
                            ((HtmlGenericControl)c).Visible = false;
                        }
                    }
                    if (Request.QueryString["lastname"] != null)
                    {
                        if (Request.QueryString["lastname"].ToString() != string.Empty)
                        {
                            if (this.FindControl("lastname") != null)
                            {
                                ((TextBox)this.FindControl("lastname")).Text = Request.QueryString["lastname"].ToString();
                            }
                        }
                    }
                }
            }
            if (sSysCmd == "9")
            {
                int iEntityId = Convert.ToInt32(((TextBox)this.Form.FindControl("entityid")).Text);
                if (iEntityId > 0)
                {
                    string sLastName = ((TextBox)this.FindControl("lastname")).Text;
                    sLastName = AppHelper.HTMLCustomEncode(sLastName);//RMA-10824
                    string sScript = "if(window.opener != null){ if (window.opener.document.forms[0].SysFormName.value == 'funds' || window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks' || 'thirdpartypayments' == window.opener.document.forms[0].SysFormName.value) { window.opener.RequestRestore('" + sLastName + "');window.opener.entitySelected(" + iEntityId + ");window.close();}}";
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "EntitySelected", sScript, true);
                }
            }
            if (sSysCmd == "7")
            {
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetDataChanged", "if(window.opener != null){ if (window.opener.document.forms[0].SysFormName.value == 'funds' || window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks' || 'thirdpartypayments' == window.opener.document.forms[0].SysFormName.value) { setDataChanged(true); } }", true);
            }
            //RMA-6871
        }
        //RMA-6871
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {
                base.NavigateSave(sender, e);
                int iEntityId = Convert.ToInt32(((TextBox)this.Form.FindControl("entityid")).Text);
                if (iEntityId > 0)
                {
                    string sLastName = ((TextBox)this.FindControl("lastname")).Text;
                    sLastName = AppHelper.HTMLCustomEncode(sLastName);//RMA-10824
                    string sScript = "if(window.opener != null){ if (window.opener.document.forms[0].SysFormName.value == 'funds' || window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks' || 'thirdpartypayments' == window.opener.document.forms[0].SysFormName.value) { window.opener.RequestRestore('" + sLastName + "');window.opener.entitySelected(" + iEntityId + ");window.close();}}";
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "EntitySelected", sScript, true);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
            }
            finally { }
        }
        //RMA-6871
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oEntityId = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Entity/EntityId");            
            if (oEntityId != null)
            {
                Control oEntityTabelId = this.FindControl("entitytableid");
                Control oEntityTabelIdText = this.FindControl("entitytableidtext");
                Control oSysPostBack = this.FindControl("SysPostback");
                if (oEntityId.Value == "0")
                {
                    if (oEntityTabelId != null)
                    {
                        RemoveIgnoreValueAttribute(oEntityTabelId);
                    }
                    if (oEntityTabelIdText != null)
                    {
                        SetIgnoreValueAttribute(oEntityTabelIdText);
                    }
                    if (oSysPostBack != null)
                    {
                        ((TextBox)oSysPostBack).Text = "entitytableid";
                    }
                }
                else
                {
                    if (oEntityTabelId != null)
                    {
                        RemoveIgnoreValueAttribute(oEntityTabelIdText);
                    }
                    if (oEntityTabelIdText != null)
                    {
                        SetIgnoreValueAttribute(oEntityTabelId);
                    }
                    if (oSysPostBack != null)
                    {
                        ((TextBox)oSysPostBack).Text = "entitytableidtext";
                    }
                }
            }
            XElement oSysSkipBindToControl = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysSkipBindToControl");
            Control oDupSSN = this.FindControl("DupSSN");
            if (oSysSkipBindToControl != null && oDupSSN != null)
             {
                 if (oSysSkipBindToControl.Value == "true")
                 {
                     ((TextBox)oDupSSN).Text = "BeforeIgnore";
                 }
             }
        }

        public override void ModifyControls()
        {
            //rjhamb 16453:Commented the code added by gagnihotri MITS 16453 05/12/2009.This is now getting handled in function UpdateSysReadOnlyFields
            //// npadhy Start MITS 20608 Check whether the Control is Textbox, If it is then only Make it readonly
            //WebControl wbEntityTableIdTextName = ((WebControl)this.FindControl("entitytableidtextname"));
            //TextBox txtEntityTableIdTextName = null;
            //if (wbEntityTableIdTextName != null)
            //{
            //    if (AppHelper.GetControlType(wbEntityTableIdTextName.GetType().ToString()) == "TextBox")
            //    {
            //        txtEntityTableIdTextName = (TextBox)wbEntityTableIdTextName;
            //        // npadhy End MITS 20608 Check whether the Control is Textbox, If it is then only Make it readonly
            //        if (txtEntityTableIdTextName.Visible)
            //        {
            //            txtEntityTableIdTextName.ReadOnly = true;
            //            txtEntityTableIdTextName.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
            //        }
            //    }
            //}
            //rjhamb 16453:Commented the code added by gagnihotri MITS 16453 05/12/2009.This is now getting handled in function UpdateSysReadOnlyFields

            //rjhamb 22226:To enable Type of Entity combobox from getting disabled in powerviews
            DropDownList cmbEntityTableId = null;
            if (this.FindControl("entitytableid") != null)
            {
                if (this.FindControl("entitytableid").GetType().ToString().Equals("System.Web.UI.WebControls.DropDownList"))
                {
                    cmbEntityTableId = ((DropDownList)this.FindControl("entitytableid"));
                }
                if (cmbEntityTableId != null && cmbEntityTableId.Visible)
                {
                    //cmbEntityTableId.Enabled = true;
                    DatabindingHelper.EnableControls("entitytableid", this);
                }
            }
            //rjhamb 22226:To enable Type of Entity combobox from getting disabled in powerviews
            //Added Rakhi For R7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                SetPrimaryAddress();
            }
            //Added Rakhi For R7:Add Emp Data Elements

            ////dbisht6 mits35331
           
            //Control ocontrol = Page.FindControl("naicscode");


            //if (ocontrol != null)
            //{
            //    DatabindingHelper.EnableControl(ocontrol);
            //}


            ////dbisht6 end
        
        
        }
        
        private void IncludeLastRecord(string sPrimaryAddressChanged)
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");
            if (objGridControl != null)
            {
                if (sPrimaryAddressChanged.ToLower() == "true")
                {

                    objGridControl.IncludeLastRecord = true;
                }
                else
                {
                    objGridControl.IncludeLastRecord = false;
                }
            }
        }
        private void SetPrimaryAddress()
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");//Capturing Primary Flag
            bool bPrimaryFlag = false;
            if (objGridControl != null)
            {
                GridView gvData = (GridView)objGridControl.GridView;
                int iGridRowCount = gvData.Rows.Count;
                int iPrimaryColIndex = 0;
                int iUniqueIdIndex = 0;
                if (iGridRowCount > 1)
                {
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    for (int i = 0; i < gvData.Columns.Count - 1; i++)
                    {
                        if (gvData.Columns[i].HeaderText.ToLower() == "primary address")
                        {
                            iPrimaryColIndex = i;
                            continue;
                        }
                        else if (gvData.Columns[i].HeaderText.ToLower() == "uniqueid_hidden")
                        {
                            iUniqueIdIndex = i;
                            continue;
                        }
                    }
                    for (int i = 0; i < iGridRowCount - 1; i++)
                    {
                        string sValue = gvData.Rows[i].Cells[iPrimaryColIndex].Text.ToLower();
                        if (sValue == "true")
                        {
                            bPrimaryFlag = true;
                            if (txtPrimaryRow != null)
                                txtPrimaryRow.Text = gvData.Rows[i].Cells[iUniqueIdIndex].Text;
                            break;
                        }
                    }
                    if (!bPrimaryFlag)
                    {
                        if (txtPrimaryRow != null)
                            txtPrimaryRow.Text = "";
                    }

                }
                else
                {
                    bPrimaryFlag = false;
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    if (txtPrimaryRow != null)
                        txtPrimaryRow.Text = "";
                }
            }
            TextBox txtPrimaryFlag = (TextBox)this.FindControl("PrimaryFlag");
            if (txtPrimaryFlag != null)
                txtPrimaryFlag.Text = bPrimaryFlag.ToString();
        }
    }
}
