﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.FDM
{
    public partial class Piother : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            XElement rootElement = null;  //Neha R8 Injury implementation
             
            //If existing other has been added then datachange flag has to be true
            if (AppHelper.GetQueryStringValue("DataChange") == "true")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "datachangescript", "<script type='text/javascript'>setDataChanged(true);</script>");
            }
            
        }
        protected override void OnUpdateForm(XElement oMessageElement) // csingh7 for MITS 15116
        {
            base.OnUpdateForm(oMessageElement);

            //avipinsrivas Start : Worked for JIRA 340
            XElement oHideTypeTextBox = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/HideTypeTextBox");
            if (oHideTypeTextBox != null)
            {
                Control oEntityTabelId = this.FindControl("entitytableid");
                Control oEntityTabelIdText = this.FindControl("entitytableidtext");
                if (string.Equals(oHideTypeTextBox.Value.ToUpper(), "TRUE"))
                {
                    if (oEntityTabelId != null)
                    {
                        RemoveIgnoreValueAttribute(oEntityTabelId);
                    }
                    if (oEntityTabelIdText != null)
                    {
                        SetIgnoreValueAttribute(oEntityTabelIdText);
                    }
                }
                else
                {
                    if (oEntityTabelId != null)
                    {
                        RemoveIgnoreValueAttribute(oEntityTabelIdText);
                    }
                    if (oEntityTabelIdText != null)
                    {
                        SetIgnoreValueAttribute(oEntityTabelId);
                    }

                }
            }
            //avipinsrivas End
        }

      ////dbisht6 mits35331
      //  public override void ModifyControls()
      //  {
               
      //          Control ocontrol = Page.FindControl("naicscode");
            

      //          if (ocontrol != null)
      //          {
      //              DatabindingHelper.EnableControl(ocontrol);
      //          }

      //  }
      //  //dbisht6 end
    }
}
