﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common;//abahl3 11468,11121,11120


namespace Riskmaster.UI.FDM
{
    public partial class Thirdpartypayments : GridPopupBase
    {
     //IC Retrofit for REM Transaction Code , MITS 20093
     //--added by rkaur7 on 02/18/2010 REM-MITS #19821
        bool bReturnStatus = false;
        string sreturnValue = "";
        XElement XmlTemplate = null;
	//end rkaur7
        XElement oMessageElement = null;
        //Deb Multi Currency
        protected override void InitializeCulture()
        {
            string sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype");
            if (!string.IsNullOrEmpty(sCurrCulture))
            {
                Culture = sCurrCulture.Split('|')[1];
                UICulture = sCurrCulture.Split('|')[1];
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(sCurrCulture.Split('|')[1]);
            }
            base.InitializeCulture();
        }
        //Deb Multi Currency
        protected void Page_Load(object sender, EventArgs e)
        {
 //IC Retrofit for REM Transaction Code , MITS 20093
 //--added by rkaur7 on 02/18/2010 REM-MITS #19821 - WebService Called
            MakeWebServiceCall();
            //end rkaur7
            GridPopupPageload();
            if (Request.QueryString["LOB"] != null)
            {
                string sLOB = Request.QueryString["LOB"];
                TextBox lineofbusinesscode = (TextBox)this.FindControl("lineofbusinesscode");
                lineofbusinesscode.Text = sLOB;
            }


            TextBox objLOB = (TextBox)this.FindControl("lineofbusinesscode");
            Control objDivDisabilityCat = this.FindControl("div_DisabilityCat");
            Control objDivDisabilityLossType = this.FindControl("div_DisabilityLossType");
            Control objDivLossType = this.FindControl("div_LossType");

            if (objLOB.Text == "243")
            {
                if (objDivLossType != null)
                {
                    objDivLossType.Visible = false;
                }

            }
            else
            {
                if (objDivDisabilityCat != null)
                {
                    objDivDisabilityCat.Visible = false;
                }

                if (objDivDisabilityLossType != null)
                {
                    objDivDisabilityLossType.Visible = false;
                }

            }
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes

            if (!IsPostBack)
            {
                Control objReserveTypeCode = this.FindControl("ReserveTypeCode");                
                if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                {
                    ((CodeLookUp)objReserveTypeCode).Enabled = false;
                }                
                string sText = AppHelper.GetQueryStringValue("PolIdDesc");
                string sCode = AppHelper.GetQueryStringValue("PolId");
                Control objPolicy = this.FindControl("Policy");

                DropDownList oLBControl = (DropDownList)objPolicy;
                TextBox txtPolicyID = (TextBox)FindControlRecursive(this.Form, "PolicyID");
                if (txtPolicyID != null)//JIRA 1394
                {
                    txtPolicyID.Text = sCode;
                    oLBControl.Items.Add(new ListItem(sText, sCode));
                }

                //rupal:start, r8 unit implementation
                sText = AppHelper.GetQueryStringValue("PolUnitDesc");
                sCode = AppHelper.GetQueryStringValue("PolUnitRowID");
                Control dUnit = this.FindControl("Unit");
                if(dUnit!=null)//JIRA 1394
                oLBControl = (DropDownList)dUnit;
                TextBox txtUnitID = (TextBox)FindControlRecursive(this.Form, "UnitID");
                if (txtUnitID != null)//JIRA 1394
                {
                    txtUnitID.Text = sCode;
                    oLBControl.Items.Add(new ListItem(sText, sCode));
                }
                //rupal:end
           //Added by Amitosh for EFT
                            if (AppHelper.GetQueryStringValue("IsEFTBank") != null)
                            {
                                if (Page.FindControl("IsEFTBank") != null)
                                {
                                        ((TextBox)Page.FindControl("IsEFTBank")).Text = AppHelper.GetQueryStringValue("IsEFTBank");
                                }
                            }
           
                            if (AppHelper.GetQueryStringValue("CovgSeqNum") != null)
                            {
                                if (Page.FindControl("CovgSeqNum") != null)
                                {
                                    ((TextBox)Page.FindControl("CovgSeqNum")).Text = AppHelper.GetQueryStringValue("CovgSeqNum");
                                }
                            }

                //End Amitosh 
            }

            //rupal:start
            string sCarrierClaims = AppHelper.GetQueryStringValue("CarrierClaims");            
            if (sCarrierClaims == "" || sCarrierClaims == "False" || sCarrierClaims == "0")
            {
                EnableDisablePolicy();
            }
            //rupal:end

            //else
            //{
            //    TextBox txtPostBackParent = (TextBox)FindControlRecursive(this.Form, "txtPostBack");
            //    txtPostBackParent.Text = "Done";

            //    Control c = DatabindingHelper.GetPostBackControl(this.Page);
            //    //if (c == null || (c.ID.ToLower() == "delete" && objGridControl != null && objGridControl.PopupGrid && !objGridControl.SessionGrid))
            //    if (c == null)
            //    {
            //        TextBox txtDataParent = (TextBox)FindControlRecursive(this.Form, "txtData");
            //        string sXml = txtDataParent.Text;
            //        sXml = sXml.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
            //        XmlDocument objXml = new XmlDocument();
            //        objXml.LoadXml(sXml);

            //        //added by Navdeep - Modify xml to Map Line of Business List Box to Pop Up from Grid (Chubb Ack)
            //        ModifyXml(ref objXml);
            //        sXml = objXml.InnerXml;
            //        //end by Navdeep
            //        XElement objElem = XElement.Parse(sXml);
            //        if (objElem.XPathSelectElement("//PolicyID") != null)
            //        {
            //            MakeWebServiceCall(objElem.XPathSelectElement("./AutoSplitId").Value, ref objElem);
            //        }
            //        else
            //        {
            //            EnableDisblePolicy();
            //        }
            //        BindData2Control(objElem, this.Controls);
            //    }
            //}
        }
        //IC Retrofit for REM Transaction Code , MITS 20093
        //Function to make a call to Webservice--//added by rkaur7 on 02/18/2010 REM-MITS #19821  
        private void MakeWebServiceCall()
        {
            XmlTemplate = GetMessageTemplate();
            //bReturnStatus = CallCWSFunction("SplitForm.GetUtilityUseResFilterValue", out sreturnValue, XmlTemplate);

            bReturnStatus = CallCWS("FundManagementAdaptor.CheckUtility", XmlTemplate, out sreturnValue, false, false);
            EnableDisbleReserveTypeFt(sreturnValue);
           
            //Populate Reserve Balance in Third Party screen, if Utility Setting is on
            XmlDocument objXmlDoc = new XmlDocument();
            XElement objReservesData = null;
            XmlDocument objXml = null;
            XElement oSessionCmdElement = null;


            string sClaimId = string.Empty;
            //string sTransTypeCode = string.Empty;
            string sClaimantEid = string.Empty;
            string sUnitId = string.Empty;
            string sReserveTypeCode = string.Empty;
            //rupal:start, r8 unit implementation and bob change
            string sPolicyID = string.Empty;
            string sCovTypeCode = string.Empty;
            string sPolicyUnitRowId = string.Empty;
            //rupal:end
            string sCovgSeqNum = string.Empty;

            //Control oReserveTypeCode = this.FindControl("ReserveTypeCode");
            //Control oReserveTypeCodeFt = this.FindControl("ReserveTypeCodeFt");

            try
            {

            objXmlDoc.LoadXml(sreturnValue);
            XmlNode UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/UseResFilter");
            if (UseResFilter != null)
            {
                if (UseResFilter.InnerText == "True")
                {
                    ////Remove the Binding for ReserveTypeCode and Set the Binding for ReserveTypeCodeft
                    //// SetIgnoreValueAttribute will add the Ignorevalue attribute to ReserveTypeCode
                    //SetIgnoreValueAttribute(oReserveTypeCode);
                    //// SetIgnoreValueAttribute will add the Ignorevalue attribute to ReserveTypeCode
                    //RemoveIgnoreValueAttribute(oReserveTypeCodeFt);

                    // Fetch the request parameters and set the corresponding XElement nodes.
                    oMessageElement = GetMessageTemplate2();
                    //bReturnStatus = CallCWS("FundManagementAdaptor.GetReservesInfo", oMessageElement, out sreturnValue, false, false);
                    if (Request.QueryString["ClaimId"] != null)
                    {
                        sClaimId = Request.QueryString["ClaimId"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimId");
                        oSessionCmdElement.Value = sClaimId;
                    }
                    //if (Request.QueryString["transtypecode"] != null)
                    //{
                    //    sTransTypeCode = Request.QueryString["transtypecode"];
                    //    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/TransTypeCode");
                    //    oSessionCmdElement.Value = sTransTypeCode;
                    //}

                    if (Request.QueryString["ReserveTypeCode"] != null)
                    {
                        sReserveTypeCode = Request.QueryString["ReserveTypeCode"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ReserveTypeCode");
                        oSessionCmdElement.Value = sReserveTypeCode;

                    }

                    if (Request.QueryString["ClaimantEid"] != null)
                    {
                        sClaimantEid = Request.QueryString["ClaimantEid"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/ClaimantEid");
                        oSessionCmdElement.Value = sClaimantEid;
                    }
                    if (Request.QueryString["UnitId"] != null)
                    {
                        sUnitId = Request.QueryString["UnitId"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/UnitId");
                        oSessionCmdElement.Value = sUnitId;
                    }

                    //rupal:start, r8 unit implementation and bob change
                    if (Request.QueryString["PolId"] != null)
                    {
                        sPolicyID = Request.QueryString["PolId"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PolicyId");
                        oSessionCmdElement.Value = sPolicyID;
                    }                 

                    if (Request.QueryString["CovID"] != null)
                    {
                        sCovTypeCode = Request.QueryString["CovID"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CovTypeCode");
                        oSessionCmdElement.Value = sCovTypeCode;
                    }

                    if (Request.QueryString["PolUnitRowID"] != null)
                    {
                        sPolicyUnitRowId = Request.QueryString["PolUnitRowID"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/PolicyUnitRowId");
                        oSessionCmdElement.Value = sPolicyUnitRowId;
                    }
                    //rupal:end

                    if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
                    {
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                        oSessionCmdElement.Value = AppHelper.ReadCookieValue("SessionId");
                    }

   
                    if (Request.QueryString["CovgSeqNum"] != null)
                    {
                        sCovgSeqNum = Request.QueryString["CovgSeqNum"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/CovgSeqNum");
                        oSessionCmdElement.Value = sCovgSeqNum;
                    }
                    //rupal:end

                    if (Request.QueryString["RcRowId"] != null)
                    {
                      string sRcRowId = Request.QueryString["RcRowId"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/FundManager/RCRowID");
                        oSessionCmdElement.Value = sRcRowId;
                    }

                    objXml = GetReservesInfo();
            
                    objReservesData = XElement.Parse(objXml.OuterXml.ToString());

                    TextBox txtReserveType = (TextBox)this.FindControl("reservebalance");
                    txtReserveType.Text = objReservesData.Element("ReserveBalance").Value;
                    if (objReservesData.XPathSelectElement("//CarrierClaims") == null)
                    {
                        EnableDisablePolicy();
                    }

                    //Start - Added by Nikhil.Show/hide override Deductible processing option 
                    if (objReservesData.XPathSelectElement("OverrideDedFlagVisible") != null)
                    {
                        if (string.Compare(objReservesData.XPathSelectElement("OverrideDedFlagVisible").Value, "-1") != 0)
                        {
                            Control objDivDedProcessingFlag = this.FindControl("div_IsOverrideDedProcessing");

                            if (objDivDedProcessingFlag != null)
                            {
                                objDivDedProcessingFlag.Visible = false;
                            }
                        }
                    }
                    //End - Added by Nikhil.Show/hide override Deductible processing option
                }
            }
        }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        //private void MakeWebServiceCall(string sAutoSplitId, ref XElement objElem)
        //{
        //    TextBox txtmode = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "mode");
        //    XmlTemplate = GetTemplateForPolicyList(sAutoSplitId);

        //    bReturnStatus = CallCWS("FundManagementAdaptor.GetPolicyList", XmlTemplate, out sreturnValue, false, false);
        //    XElement oTemplate = XElement.Parse(sreturnValue.ToString());

        //    if (string.Compare(txtmode.Text, "add", true) == 0)
        //    {
        //        foreach (XElement oTemp in oTemplate.XPathSelectElements("//PolicyList/option"))
        //        {
        //            if (oTemp.LastAttribute.Value == "true")
        //            {
        //                objElem.XPathSelectElement("//PolicyID").Value = oTemp.Value;
        //                objElem.XPathSelectElement("//PolicyID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
        //            }
        //        }

        //        XNode objXCoverageTypeCodeNode = objElem.XPathSelectElement("//CoverageTypeCode");
        //        if (objXCoverageTypeCodeNode != null)
        //            objXCoverageTypeCodeNode.Remove();
        //        objXCoverageTypeCodeNode = oTemplate.XPathSelectElement("//CoverageTypeCode");
        //        objElem.Add(objXCoverageTypeCodeNode);

        //        XNode objXReserveTypeCodeNode = objElem.XPathSelectElement("//ReserveTypeCode");
        //        if (objXReserveTypeCodeNode != null)
        //            objXReserveTypeCodeNode.Remove();
        //        objXReserveTypeCodeNode = oTemplate.XPathSelectElement("//ReserveTypeCode");
        //        objElem.Add(objXReserveTypeCodeNode);
        //    }
        //    XElement xNode = oTemplate.XPathSelectElement("//PolicyList");
        //    objElem.Add(xNode);

        //}

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            //abahl3 11468,11121,11120 starts
            //sXml = sXml.Append("<Call><Function>SplitForm.GetUtilityUseResFilterValue</Function></Call><Document><UseResFilter></UseResFilter></Document></Message>");
            sXml = sXml.Append("<Call><Function>SplitForm.GetUtilityUseResFilterValue</Function></Call><Document><GetSettings><UseResFilter></UseResFilter><UseCarrierClaims></UseCarrierClaims></GetSettings></Document></Message>");
            //abahl3 11468,11121,11120 ends

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        //private XElement GetTemplateForPolicyList(string sAutoSplitId)
        //{
        //    TextBox txtmode = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "mode");
        //    TextBox objclaimid = (TextBox)this.FindControl("claimid");
        //    TextBox objPolicyid = (TextBox)this.FindControl("PolicyID");
        //    TextBox objPolCvgID = (TextBox)this.FindControl("PolCvgID");
        //    TextBox ResTypeCodeId = (TextBox)this.FindControl("ResTypeCode");
        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml = sXml.Append("<Authorization></Authorization>");
        //    sXml = sXml.Append("<Call><Function></Function></Call><Document><Policy><AutoSplitId>");
        //    sXml = sXml.Append(sAutoSplitId);
        //    sXml = sXml.Append("</AutoSplitId>");
        //    if (string.Compare(txtmode.Text, "add", true) == 0)
        //    {
        //        sXml = sXml.Append("<ClaimId>");
        //        sXml = sXml.Append(objclaimid.Text);
        //        sXml = sXml.Append("</ClaimId>");
        //        sXml = sXml.Append("<PolicyId>");
        //        sXml = sXml.Append(objPolicyid.Text);
        //        sXml = sXml.Append("</PolicyId>");
        //        sXml = sXml.Append("<PolCvgID>");
        //        sXml = sXml.Append(objPolCvgID.Text);
        //        sXml = sXml.Append("</PolCvgID>");
        //        sXml = sXml.Append("<ResTypeCodeId>");
        //        sXml = sXml.Append(ResTypeCodeId.Text);
        //        sXml = sXml.Append("</ResTypeCodeId>");
        //    }
        //    if ((string.Compare(txtmode.Text, "edit", true) == 0) && ((string.Compare(sAutoSplitId, "0") == 0) || (string.Compare(sAutoSplitId, "-1") == 0)))
        //    {
        //        sXml = sXml.Append("<ClaimId>");
        //        sXml = sXml.Append(claimid.Text);
        //        sXml = sXml.Append("</ClaimId>");
        //    }
        //    sXml = sXml.Append("<PolicyList></PolicyList></Policy></Document></Message>");
        //    XElement oTemplate = XElement.Parse(sXml.ToString());
        //    return oTemplate;
        //}

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate2()
        {
            //updated by rkaur7 - MITS 17241 (added new tag ReserveTypeCode)
            //updated by rupal, added policyId and CovTypeCode and PolicyUnitRowId for r8 unit implementation
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Authorization></Authorization> 
                        <Call>
                            <Function>FundManagementAdaptor.GetReservesInfo</Function> 
                        </Call>
                    <Document>
                        <FundManager>
                            <ClaimId></ClaimId> 
                            <TransTypeCode></TransTypeCode>
                            <ReserveTypeCode></ReserveTypeCode> 
                            <ClaimantEid>0</ClaimantEid> 
                            <UnitId>0</UnitId> 
                            <TransID>0</TransID> 
                            <PolicyId></PolicyId>
                            <CovTypeCode></CovTypeCode>
                            <PolicyUnitRowId></PolicyUnitRowId>
                            <CovgSeqNum></CovgSeqNum>
                            <RCRowID></RCRowID>

                            <LobCode></LobCode>


                            <PmtCurrCode></PmtCurrCode>
                            <LossCode></LossCode>
                            <TransSeqNum></TransSeqNum>
                            <CoverageKey></CoverageKey>
                        </FundManager>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }
        
        private XmlDocument GetReservesInfo()
        {
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;

            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            XmlDocument oFDMPageDom = new XmlDocument();
            oFDMPageDom.LoadXml(sReturn);
            XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");

            objXml.LoadXml(oInstanceNode.InnerXml);
            return objXml;
        }
        
   /*
        //commented by rupal
        private void EnableDisbleReserveTypeFt(string sXml)
        {
            XmlDocument objXmlDoc = new XmlDocument();

            objXmlDoc.LoadXml(sXml);
            XmlNode UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/UseResFilter");
            Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
            Control objDivReserveTypeCode = this.FindControl("div_ReserveTypeCode");            
            Control objReserveTypeCodeFt = this.FindControl("FORMTABThirdPartyPaymentGroup").FindControl("ReserveTypeCodeFt");
            Control objDivReserveTypeCodeFt = this.FindControl("FORMTABThirdPartyPaymentGroup").FindControl("div_ReserveTypeCodeFt");

            Control objReserveTypeLabel = this.FindControl("lbl_ReserveTypeCode");
            Control objTransTypeCode = this.FindControl("TransTypeCode");

            if (UseResFilter != null)
            {
                if (UseResFilter.InnerText == "True")
                {
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = true;
                    }
                    //rsushilaggar MITS 25353 Date 07/06/*2011
                    //Deb MITS 26077
                    if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                    {
                        ((CodeLookUp)objReserveTypeCode).ShowHide = false;
                    }
                    if (objReserveTypeLabel != null)
                    {
                        ((Label)objReserveTypeLabel).Visible = false;
                    }
                    //Deb MITS 26077
                    if (Request.QueryString["mode"] == "edit")
                    {
                        if (objTransTypeCode != null && objTransTypeCode.GetType() == typeof(System.Web.UI.WebControls.TextBox))
                        {
                            ((TextBox)objTransTypeCode).Enabled = true;
                        }
                    }
                    else
                    {
                        TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
                        if (objTxt != null)
                        {
                            objTxt.Enabled = false;
                            objTxt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                        }
                        Button objBtn = (Button)this.FindControl("TransTypeCode$codelookupbtn");
                        if (objBtn != null)
                        {
                            objBtn.Enabled = false;
                        }
                    }
                    if (objReserveTypeCodeFt != null)
                    {
                        RemoveIgnoreValueAttribute(objReserveTypeCodeFt);
                    }
                    if (objReserveTypeCode != null)
                    {
                        SetIgnoreValueAttribute(objReserveTypeCode);
                    }
                }
                else
                {
                    if (objDivReserveTypeCode != null)
                    {
                        objDivReserveTypeCode.Visible = true;
                    }
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = false;
                    }
                    if (objReserveTypeCode != null)
                    {
                        RemoveIgnoreValueAttribute(objReserveTypeCode);
                    }
                    if (objReserveTypeCodeFt != null)
                    {
                        SetIgnoreValueAttribute(objReserveTypeCodeFt);
                    }
                }
            }     
        }
       
        */

  
        private void EnableDisbleReserveTypeFt(string sXml)
        {
            XmlDocument objXmlDoc = new XmlDocument();

            objXmlDoc.LoadXml(sXml);
            //abahl3 11468,11121,11120 starts
            //     XmlNode UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/UseResFilter");
            XmlNode UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/GetSettings/UseResFilter");
            XmlNode objIsCarrierClaim = objXmlDoc.SelectSingleNode("//ResultMessage/Document/GetSettings/UseCarrierClaims");
            //abahl3 11468,11121,11120 ends

            Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
            Control objDivReserveTypeCode = this.FindControl("div_ReserveTypeCode");
            Control objReserveTypeLabel = this.FindControl("lbl_ReserveTypeCode");
            

            //IC Retrofit for REM Transaction Code , MITS 20093
            //---Commented and Added by asingh264 for REM- MITS# 19821---
            //Control objReserveTypeCodeFt = this.FindControl("ReserveTypeCodeFt");
            //Control objDivReserveTypeCodeFt = this.FindControl("div_ReserveTypeCodeFt");
            Control objReserveTypeCodeFt = this.FindControl("FORMTABThirdPartyPaymentGroup").FindControl("ReserveTypeCodeFt");
            Control objDivReserveTypeCodeFt = this.FindControl("FORMTABThirdPartyPaymentGroup").FindControl("div_ReserveTypeCodeFt");
            Control TransTypeCode = this.FindControl("FORMTABThirdPartyPaymentGroup").FindControl("TransTypeCode");
            //-----MITS 19821 ends------

            //abahl3 11468,11121,11120 starts
            //if (UseResFilter != null)
            //{
            if (((objIsCarrierClaim != null) && (objIsCarrierClaim.InnerText == "True") && Riskmaster.Common.Conversion.ConvertStrToInteger(((TextBox)this.FindControl("RcRowID")).Text) > 0) || ((UseResFilter != null) && (UseResFilter.InnerText == "True")))//abahl3 11468,11121,11120 ends
            {


                    //enable reservetypecodeft and make it visible
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = true;
                    }

                    //Changed by Gagan for MITS 20093
                    TextBox txtReserveType = (TextBox)this.FindControl("ReserveTypeCodeFt$codelookup");
                    if (txtReserveType != null)
                    {
                        txtReserveType.Enabled = false;
                        txtReserveType.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                    }

                    Button btnReserveType = (Button)this.FindControl("ReserveTypeCodeFt$codelookupbtn");
                    if (btnReserveType != null)
                    {
                        btnReserveType.Enabled = false;
                    }

                   
                    //diable reservetypecode

                    if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                    {
                        ((CodeLookUp)objReserveTypeCode).ShowHide = false;
                    }
                    if (objReserveTypeLabel != null)
                    {
                        ((Label)objReserveTypeLabel).Visible = false;
                    }

                    //trans type
                    if (Request.QueryString["mode"] == "edit")
                    {
                        //Riskmaster.UI.Shared.Controls.CodeLookUp
                        //TransTypeCode.Enabled = true;

                        TextBox txtTransTypeCode = (TextBox)this.FindControl("TransTypeCodeFt$codelookup");
                        if (txtTransTypeCode != null)
                        {
                            txtTransTypeCode.Enabled = true;                            
                        }

                        Button btnTransTypeCode = (Button)this.FindControl("TransTypeCodeFt$codelookupbtn");
                        if (btnTransTypeCode != null)
                        {
                            btnTransTypeCode.Enabled = true;
                        }

                    }
                    else
                    {
                        //TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
                        //if (objTxt != null)
                        //{
                        //    objTxt.Enabled = false;
                        //    objTxt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                        //}
                        //Button objBtn = (Button)this.FindControl("TransTypeCode$codelookupbtn");
                        //if (objBtn != null)
                        //{
                        //    objBtn.Enabled = false;
                        //}
                    }
                    if (objReserveTypeCodeFt != null)
                    {
                        RemoveIgnoreValueAttribute(objReserveTypeCodeFt);
                    }
                    if (objReserveTypeCode != null)
                    {
                        SetIgnoreValueAttribute(objReserveTypeCode);
                    }
                }
                else
                {
                    if (objDivReserveTypeCode != null)
                    {
                        objDivReserveTypeCode.Visible = true;
                    }
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = false;
                    }
                    if (objReserveTypeCode != null)
                    {
                        RemoveIgnoreValueAttribute(objReserveTypeCode);
                    }
                    if (objReserveTypeCodeFt != null)
                    {
                        SetIgnoreValueAttribute(objReserveTypeCodeFt);
                    }
                }
            // }//abahl3 11468,11121,11120 starts

            //end rkaur7
        }
   
        private void EnableDisablePolicy()
        {
            Control objPolicy = this.FindControl("Policy");
            Control objDivPolicy = this.FindControl("div_Policy");
            Control objCoverageTypeCode = this.FindControl("Coverage");
            Control objDivCoverageTypeCode = this.FindControl("div_Coverage");
            //rupal:start r8 unit changes
            Control objUnit = this.FindControl("Unit");
            Control objDivUnit = this.FindControl("div_Unit");
            //Start -  Added by Nikhil.
            Control objDivDedProcessingFlag = this.FindControl("div_IsOverrideDedProcessing");

            if (objDivDedProcessingFlag != null)
            {
                objDivDedProcessingFlag.Visible = false;
            }
            //End - Added by Nikhil
            if (objDivUnit != null)
            {
                objDivUnit.Visible = false;
            }
            //rupal:end
            if (objDivPolicy != null)
            {
                objDivPolicy.Visible = false;
            }
            if (objDivCoverageTypeCode != null)
            {
                objDivCoverageTypeCode.Visible = false;
            }
            Control objDivDisabilityCat = this.FindControl("div_DisabilityCat");
            Control objDivDisabilityLossType = this.FindControl("div_DisabilityLossType");
            Control objDivLossType = this.FindControl("div_LossType");
           

           
                if (objDivDisabilityCat != null)
                {
                    objDivDisabilityCat.Visible = false;
                }

                if (objDivDisabilityLossType != null)
                {
                    objDivDisabilityLossType.Visible = false;
                }
                //Aman MITS 31093
                if (objDivLossType != null)
                {
                    objDivLossType.Visible = false;
                }
            //Aman MITS 31093
        }
    }
}
