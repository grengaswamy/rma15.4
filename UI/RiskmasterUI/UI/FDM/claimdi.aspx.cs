﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;

namespace Riskmaster.UI.FDM
{
    public partial class Claimdi : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            //Start jramkumar MITS 34419
            if (IsPostBack)
            {
                string txtFocusScript = "var focusField=false; if((document.getElementById('ev_depteid') != null && document.getElementById('ev_depteid').value != '') || (document.getElementById('ev_countrycode_codelookup') != null && document.getElementById('ev_countrycode_codelookup').value != '')) { focusField=true; }";
                this.ClientScript.RegisterStartupScript(this.GetType(), "di", txtFocusScript, true);
            }
            //End jramkumar MITS 34419
            //gmallick JIRA 12965 Start
            Button claimstatuscodedetail = this.Form.FindControl("claimstatuscodedetailbtn") as Button;
            if (claimstatuscodedetail != null)
            {
                claimstatuscodedetail.Attributes.Add("Visible", "True");
                claimstatuscodedetail.Enabled = true;
                claimstatuscodedetail.Attributes.Add("onclientclick", "return selectCodeWithDetail('claimstatuscode',1);");
                claimstatuscodedetail.CssClass = "ClaimStatusButton";
            }
            //gmallick JIRA 12965 End
        }
        //Mridul 05/28/09 MITS 16745 (Chubb Ack)
        /// <summary>
        /// Override base and generates claim letter if claim is found eligible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {
                int iClaimId = 0;
                string sCLMLtrTmplType = "";
                string sClaimNo = "";
                base.NavigateSave(sender, e);
                iClaimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);
                sCLMLtrTmplType = Convert.ToString(((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text);
                sClaimNo = Convert.ToString(((TextBox)this.Form.FindControl("claimnumber")).Text);
                if (sCLMLtrTmplType == "ACK" || sCLMLtrTmplType == "CL")
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClaimLetterMerge", "DoClaimLetterMerge(" + iClaimId + ",'" + sCLMLtrTmplType + "','" + sClaimNo + "');", true);
                sCLMLtrTmplType = "";

                // rrachev JIRA 5021 Get current value of ContainsOpenDiaries
                Control openDiaries = this.FindControl("containsopendiaries");
                if (openDiaries != null && this.Data != null && !String.IsNullOrEmpty(this.Data.OuterXml))
                {
                    BindData2Control(XElement.Parse(this.Data.OuterXml), new Control[] { openDiaries });
                }
            }
            catch (Exception ex)
            {

            }
            finally { }
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {   // ybhaskar MITS 15197: Opened new screen for plan
            base.OnUpdateForm(oMessageElement);
            Button plannameOpen = (Button)(this.Form.FindControl("planname_open"));
            //pmittal5 Mits 15752 06/15/09 - PlanId node is also present in SysView, but it should be picked from SysPropertyStore
            //XElement PlanIdValue = oMessageElement.XPathSelectElement("//PlanId");
            XElement PlanIdValue = oMessageElement.XPathSelectElement("ParamList/Param[@name='SysPropertyStore']//PlanId");
            string PlanId;

            if (plannameOpen != null)
            {
                if (PlanIdValue != null)
                {
                    PlanId = PlanIdValue.Value.ToString();
                    
                    if (PlanId != "0")
                        // npadhy Without this return statement the Claim page was posting back, and there was error coming in case of Zapatec
                        plannameOpen.OnClientClick = "parent.MDIShowScreen('" + PlanId + "','plan'); return false;";
                    else
                        plannameOpen.OnClientClick = "return fnNoRecordToOpen();";
                }
            }    


        }
    }
}
