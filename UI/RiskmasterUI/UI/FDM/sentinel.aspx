<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sentinel.aspx.cs" Inherits="Riskmaster.UI.FDM.Sentinel"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Sentinel Event</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="../../Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Sentinel Event" /><asp:label id="formsubtitle"
            runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSsentinelgrp" id="TABSsentinelgrp">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="sentinelgrp"
                id="LINKTABSsentinelgrp"><span style="text-decoration: none">Sentinel</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABsentinelgrp" id="FORMTABsentinelgrp">
        <asp:textbox style="display: none" runat="server" id="eventid" rmxref="Instance/EventSentinel/EventId"
            rmxtype="id" /><asp:textbox style="display: none" runat="server" id="syseventid"
                rmxref="Instance/UI/FormVariables/SysExData/EventId" rmxtype="id" /><asp:textbox
                    style="display: none" runat="server" id="reportlevel" rmxref="Instance/UI/FormVariables/SysExData/ReportLevelValue"
                    rmxtype="id" /><div runat="server" class="sentinel" id="div_sent_event_desc" xmlns="">
                        <asp:label runat="server" class="label" id="lbl_sent_event_desc" text="Please enter the summary of incident.  The event description has been loaded for use, but you need to remove names of patients, caregivers and other individuals for reporting to the JCAHO." /><span
                            class="formw"><asp:textbox runat="Server" id="sent_event_desc" rmxref="Instance/EventSentinel/SentEventDesc"
                                rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                                tabindex="1" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                                    value="..." name="sent_event_descbtnMemo" tabindex="2" id="sent_event_descbtnMemo"
                                    onclick="EditMemo('sent_event_desc','')" /></span></div>
        <asp:textbox style="display: none" runat="server" id="eventnumber" rmxref="Instance/UI/FormVariables/SysExData/EventNumber"
            rmxtype="id" /><div runat="server" class="sentinel" id="div_page1text1" xmlns="">
                <span class="label">
                    <asp:label runat="server" id="page1text1" rmxref="" rmxtype="labelonly" text="Please answer the questions about the Event to begin the Root Cause Analysis and to create the JCAHO Sentinel Event Self-Report Form." /></span></div>
        <div runat="server" class="sentinel" id="div_damage_cons" xmlns="">
            <asp:label runat="server" class="label" id="lbl_damage_cons" text="What was the damage or the consequence of the event?" /><span
                class="formw"><asp:textbox runat="Server" id="damage_cons" rmxref="Instance/EventSentinel/DamageCons"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="3" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                        value="..." name="damage_consbtnMemo" tabindex="4" id="damage_consbtnMemo" onclick="EditMemo('damage_cons','')" /></span></div>
        <div runat="server" class="sentinel" id="div_diff_changed" xmlns="">
            <asp:label runat="server" class="label" id="lbl_diff_changed" text="What may have been different or changed at the time of event?" /><span
                class="formw"><asp:textbox runat="Server" id="diff_changed" rmxref="Instance/EventSentinel/DiffChanged"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="5" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                        value="..." name="diff_changedbtnMemo" tabindex="6" id="diff_changedbtnMemo"
                        onclick="EditMemo('diff_changed','')" /></span></div>
        <div runat="server" class="sentinel" id="div_prevent" xmlns="">
            <asp:label runat="server" class="label" id="lbl_prevent" text="What might have prevented the incident?" /><span
                class="formw"><asp:textbox runat="Server" id="prevent" rmxref="Instance/EventSentinel/Prevent"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="7" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                        value="..." name="preventbtnMemo" tabindex="8" id="preventbtnMemo" onclick="EditMemo('prevent','')" /></span></div>
        <div runat="server" class="sentinel" id="div_proc_followedgroup" xmlns="">
            <span class="label">Were current procedures followed?</span><span class="label"></span><span
                class="label">Yes</span><span class="formw"><asp:radiobutton runat="server" id="proc_followed1"
                    rmxref="Instance/EventSentinel/ProcFollowed" rmxtype="radio" onclientclick="setDataChanged(true)"
                    text="" value="True" groupname="proc_followed" /></span><span class="label">No</span><span
                        class="formw"><asp:radiobutton runat="server" id="proc_followed2" rmxref="Instance/EventSentinel/ProcFollowed"
                            rmxtype="radio" onclientclick="setDataChanged(true)" text="" value="False" groupname="proc_followed" /></span></div>
        <div runat="server" class="sentinel" id="div_avoid_worsegroup" xmlns="">
            <span class="label">Did the event avoid a worse action or situation?</span><span
                class="label"></span><span class="label">Yes</span><span class="formw"><asp:radiobutton
                    runat="server" id="avoid_worse1" rmxref="Instance/EventSentinel/AvoidWorse" rmxtype="radio"
                    onclientclick="setDataChanged(true)" text="" value="True" groupname="avoid_worse" /></span><span
                        class="label">No</span><span class="formw"><asp:radiobutton runat="server" id="avoid_worse2"
                            rmxref="Instance/EventSentinel/AvoidWorse" rmxtype="radio" onclientclick="setDataChanged(true)"
                            text="" value="False" groupname="avoid_worse" /></span></div>
        <div runat="server" class="sentinel" id="div_staff_know_handlegroup" xmlns="">
            <span class="label">Did the involved staff know how to handle the situation?</span><span
                class="label"></span><span class="label">Yes</span><span class="formw"><asp:radiobutton
                    runat="server" id="staff_know_handle1" rmxref="Instance/EventSentinel/StaffKnowHandle"
                    rmxtype="radio" onclientclick="setDataChanged(true)" text="" value="True" groupname="staff_know_handle" /></span><span
                        class="label">No</span><span class="formw"><asp:radiobutton runat="server" id="staff_know_handle2"
                            rmxref="Instance/EventSentinel/StaffKnowHandle" rmxtype="radio" onclientclick="setDataChanged(true)"
                            text="" value="False" groupname="staff_know_handle" /></span></div>
        <div runat="server" class="sentinel" id="div_happened_beforegroup" xmlns="">
            <span class="label">Has this event or a similar event happened before?</span><span
                class="label"></span><span class="label">Yes</span><span class="formw"><asp:radiobutton
                    runat="server" id="happened_before1" rmxref="Instance/EventSentinel/HappenedBefore"
                    rmxtype="radio" onclientclick="setDataChanged(true)" text="" value="True" groupname="happened_before" /></span><span
                        class="label">No</span><span class="formw"><asp:radiobutton runat="server" id="happened_before2"
                            rmxref="Instance/EventSentinel/HappenedBefore" rmxtype="radio" onclientclick="setDataChanged(true)"
                            text="" value="False" groupname="happened_before" /></span></div>
        <div runat="server" class="sentinel" id="div_previous_fix" xmlns="">
            <asp:label runat="server" class="label" id="lbl_previous_fix" text="What changes were made to reduce the risk of this event occuring again?  If no changes were made, please state why none were made." /><span
                class="formw"><asp:textbox runat="Server" id="previous_fix" rmxref="Instance/EventSentinel/PreviousFix"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="25" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                        value="..." name="previous_fixbtnMemo" tabindex="26" id="previous_fixbtnMemo"
                        onclick="EditMemo('previous_fix','')" /></span></div>
        <div runat="server" class="sentinel" id="div_unusual_report" xmlns="">
            <asp:label runat="server" class="label" id="lbl_unusual_report" text="Was there anything unusual about the reporting of the event?" /><span
                class="formw"><asp:textbox runat="Server" id="unusual_report" rmxref="Instance/EventSentinel/UnusualReport"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="27" textmode="MultiLine" columns="30" rows="5" /><input type="button" class="button"
                        value="..." name="unusual_reportbtnMemo" tabindex="28" id="unusual_reportbtnMemo"
                        onclick="EditMemo('unusual_report','')" /></span></div>
        <div runat="server" class="sentinel" id="div_comments" xmlns="">
            <asp:label runat="server" class="label" id="lbl_comments" text="What might prevent the problem from happening in the future?  Also add any comments that may be useful to finishing the root cause analysis." /><span
                class="formw"><asp:textbox runat="Server" id="comments" rmxref="Instance/EventSentinel/Comments"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="29" textmode="MultiLine" columns="30" rows="3" /><input type="button" class="button"
                        value="..." name="commentsbtnMemo" tabindex="30" id="commentsbtnMemo" onclick="EditMemo('comments','')" /></span></div>
        <div runat="server" class="sentinel" id="div_report_level" xmlns="">
            <asp:label runat="server" class="label" id="lbl_report_level" text="What level of the Organization Hierarchy should be used as the name of the accredited organization?" /><span
                class="formw"><asp:dropdownlist runat="server" id="report_level" tabindex="31" rmxref="Instance/EventSentinel/ReportLevel"
                    rmxtype="combobox" onchange="setDataChanged(true);"><asp:listitem value="1005" text="CLIENT" />
                    <asp:listitem value="1006" text="COMPANY" />
                    <asp:listitem value="1007" text="OPERATION" />
                    <asp:listitem value="1008" text="REGION" />
                    <asp:listitem value="1009" text="DIVISION" />
                    <asp:listitem value="1010" text="LOCATION" />
                    <asp:listitem value="1011" text="FACILITY" />
                    <asp:listitem value="1012" text="DEPARTMENT" />
                </asp:dropdownlist></span></div>
    </div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button class="button" runat="Server" id="btnBack" text="Back to Event" onclientclick="if(!( XFormHandler('','1','back')))return false;"
                postbackurl="?" /></div>
        <div class="formButton" runat="server" id="div_btnSentinelReport">

            <script language="JavaScript" src="">{var i;}
            </script>

            <asp:button class="button" runat="server" id="btnSentinelReport" rmxref="" text="Print Root Cause Analysis Report"
                onclientclick="getsentinelreport();" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="EventSentinel" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormPForm"
                                rmxref="Instance/UI/FormVariables/SysFormPForm" rmxtype="hidden" text="event" /><asp:textbox
                                    style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName"
                                    rmxtype="hidden" text="eventid" /><asp:textbox style="display: none" runat="server"
                                        id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId" rmxtype="hidden"
                                        text="" /><asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                                            rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysNotReqNew"
                                                rmxref="Instance/UI/FormVariables/SysNotReqNew" rmxtype="hidden" text="" /><asp:textbox
                                                    style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
                                                    rmxtype="hidden" text="sentinel" /><asp:textbox style="display: none" runat="server"
                                                        id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName" rmxtype="hidden"
                                                        text="eventid" /><asp:textbox style="display: none" runat="server" id="SysFormId"
                                                            rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden" text="" /><asp:textbox
                                                                style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                                                rmxtype="hidden" text="11000" /><asp:textbox style="display: none" runat="server"
                                                                    id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden" text="11000" /><asp:textbox
                                                                        style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" name="formname"
                                                                            value="" /><asp:textbox style="display: none" runat="server" name="SysRequired" value="" /><asp:textbox
                                                                                style="display: none" runat="server" name="SysFocusFields" value="sent_event_desc|" /><input
                                                                                    type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                        style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                            runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                    id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                        id="SysWindowId" /></form>
</body>
</html>
