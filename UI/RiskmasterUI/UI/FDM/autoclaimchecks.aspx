<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="autoclaimchecks.aspx.cs"  Inherits="Riskmaster.UI.FDM.Autoclaimchecks" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Schedule Automatic Check Processing</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <script language="JavaScript" src="csc-Theme/riskmaster/common/javascript/autochecks.js">        { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" OnClientClick="return DeleteRecord();" src="../../Images/tb_delete_active.png" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/tb_lookup_active.png" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/tb_lookup_mo.png';" onMouseOut="this.src='../../Images/tb_lookup_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return FilteredDiary();" src="../../Images/tb_diaryviewrecord_active.png" width="28" height="28" border="0" id="filtereddiary" AlternateText="View Record Diaries" onMouseOver="this.src='../../Images/tb_diaryviewrecord_mo.png';" onMouseOut="this.src='../../Images/tb_diaryviewrecord_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Diary();" src="../../Images/tb_diary_active.png" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='../../Images/tb_diary_mo.png';" onMouseOut="this.src='../../Images/tb_diary_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_backtofinancials" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return BackToReserves();" src="../../Images/tb_backtofinancials_active.png" width="28" height="28" border="0" id="backtofinancials" AlternateText="Back To Financials" onMouseOver="this.src='../../Images/tb_backtofinancials_mo.png';" onMouseOut="this.src='../../Images/tb_backtofinancials_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_loadclaim" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return LoadClaim();" src="../../Images/tb_loadclaim_active.png" width="28" height="28" border="0" id="loadclaim" AlternateText="Load Claim" onMouseOver="this.src='../../Images/tb_loadclaim_mo.png';" onMouseOut="this.src='../../Images/tb_loadclaim_active.png';" />
        </div>
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Schedule Automatic Check Processing" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSscheduledchecks" id="TABSscheduledchecks">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="scheduledchecks" id="LINKTABSscheduledchecks">Scheduled Checks</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPscheduledchecks">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSdetailinfo" id="TABSdetailinfo">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="detailinfo" id="LINKTABSdetailinfo">Detailed Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPdetailinfo">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABStppayees" id="TABStppayees">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="tppayees" id="LINKTABStppayees">Third Party Payees</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPtppayees">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABscheduledchecks" id="FORMTABscheduledchecks">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdscheduledchecks" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="MaxNumberOfAutoChecks" RMXRef="/Instance/UI/FormVariables/SysExData/MaxNumberOfAutoChecks" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="lob" RMXRef="/Instance/UI/FormVariables/SysExData/Lob" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ClaimId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="autobatchid2" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/AutoBatchId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="fundsautolist" RMXRef="/Instance/FundsAutoBatch/FundsAutoList/@count" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="autobatchid" RMXRef="/Instance/FundsAutoBatch/AutoBatchId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="autotransid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/AutoTransId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ClaimantInformation" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantInformation" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ClaimantCount" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantCount" RMXType="id" />
              <div runat="server" class="half" id="div_reservetracking" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="reservetracking" RMXRef="/Instance/UI/FormVariables/SysExData/Reservetracking" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_claimantEIdPassedIn" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="claimantEIdPassedIn" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantEIdPassedIn" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_unitIdPassedIn" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="unitIdPassedIn" RMXRef="/Instance/UI/FormVariables/SysExData/UnitIdPassedIn" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_unitIDIsUnitRowId" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="unitIDIsUnitRowId" RMXRef="/Instance/UI/FormVariables/SysExData/UnitIDIsUnitRowId" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_claimnumber" xmlns="">
                <asp:label runat="server" class="required" id="lbl_claimnumber" Text="Claim Number" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" id="claimnumber" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ClaimNumber" RMXType="claimnumberlookup" name="claimnumber" value="" cancelledvalue="" tabindex="1" />
                  <asp:button runat="server" class="EllipsisControl" id="claimnumberbtn" tabindex="2" onclientclick="return lookupData('claimnumber','claim',1,'claimnumber',6);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_payment_interval" xmlns="">
                <asp:label runat="server" class="required" id="lbl_payment_interval" Text="Pay Interval" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="payment_interval" CodeTable="PERIOD_TYPES" ControlName="payment_interval" RMXRef="/Instance/FundsAutoBatch/PaymentInterval" RMXType="code" tabindex="22" Required="true" ValidationGroup="vgSave" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="clm_entityid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ClaimantEid/@codeid" RMXType="id" />
              <div runat="server" class="half" id="div_clm_lastname" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clm_lastname" Text="Claimant" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="setDataChanged(true);" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantLastNameFirstName" RMXType="claimantlookupforclaim" id="clm_lastname" />
                  <asp:button runat="server" class="EllipsisControl" id="clm_lastnamebtn" tabindex="4" onclientclick="return OnClaimantLastNameClick();" />
                </span>
              </div>
              <div runat="server" class="half" id="div_totalpayments" xmlns="">
                <asp:label runat="server" class="label" id="lbl_totalpayments" Text="# Of Payments" />
                <span class="formw">
                  <asp:TextBox runat="server" id="totalpayments" RMXRef="/Instance/FundsAutoBatch/TotalPayments" RMXType="numeric" tabindex="24" onChange="numLostFocus(this);return CalculateFinalPaymentDate();setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="unitid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/UnitId" RMXType="id" />
              <div runat="server" class="half" id="div_unit" xmlns="">
                <asp:label runat="server" class="label" id="lbl_unit" Text="Unit" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="setDataChanged(true);" RMXRef="/Instance/UI/FormVariables/SysExData/UnitName" RMXType="unitlookupforclaim" id="unit" />
                  <asp:button runat="server" class="EllipsisControl" id="unitbtn" tabindex="6" onclientclick="return OnUnitLastNameClick();" />
                </span>
              </div>
              <div runat="server" class="half" id="div_startdate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_startdate" Text="First Payment" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="startdate" RMXRef="/Instance/FundsAutoBatch/StartDate" RMXType="date" tabindex="25" onchange="dateLostFocus(this.id);setDataChanged(true);CalculateFinalPaymentDate()" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="startdatebtn" tabindex="26" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "startdate",
					    ifFormat: "%m/%d/%Y",
					    button: "startdatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_ctlnumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ctlnumber" Text="Control Number" />
                <span class="formw">
                  <asp:TextBox runat="server" id="ctlnumber" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/CtlNumber" RMXType="text" tabindex="7" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_enddate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_enddate" Text="Final Payment" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/FinalPaymentDate" id="enddate" tabindex="27" style="background-color: #F2F2F2;" readonly="true" FormatAs="date" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_bankaccount" xmlns="">
                <asp:label runat="server" class="required" id="lbl_bankaccount" Text="Bank Account" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="bankaccount" tabindex="8" RMXRef="" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/AccountList" onchange="setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_freezebatchflag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_freezebatchflag" Text="Freeze Payment Batch" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="freezebatchflag" RMXRef="/Instance/FundsAutoBatch/FreezeBatchFlag" RMXType="checkbox" onclick="return CheckFreezePermission()" tabindex="29" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_cbopayeetype" xmlns="">
                <asp:label runat="server" class="required" id="lbl_cbopayeetype" Text="Payee Type" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="cbopayeetype" tabindex="9" RMXRef="/Instance/UI/FormVariables/SysExData/PayeeTypeCode" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/PayeeTypeList" onchange="setDataChanged(true);PayeeChanged()" />
                </span>
              </div>
              <div runat="server" class="half" id="div_enclosureflag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_enclosureflag" Text="Enclosure?" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="enclosureflag" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/EnclosureFlag" RMXType="checkbox" tabindex="30" Height="24px" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="payeetypecode" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/PayeeTypeCode/@codeid" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="pye_entityid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/PayeeEid/@codeid" RMXType="id" />
              <div runat="server" class="half" id="div_pye_lastname" xmlns="">
                <asp:label runat="server" class="required" id="lbl_pye_lastname" Text="Last Name" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/LastName" RMXType="financiallastname" id="pye_lastname" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="pye_lastname_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="pye_lastnamebtn" tabindex="11" onclientclick="return OnLastNameClick();" />
                </span>
              </div>
              <div runat="server" class="half" id="div_settlementflag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_settlementflag" Text="Include Claimant in Payee" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="settlementflag" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/SettlementFlag" RMXType="checkbox" tabindex="31" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_firstname" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_firstname" Text="First Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_firstname" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/FirstName" RMXType="text" tabindex="12" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_grossamount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_grossamount" Text="Gross Amount" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/GrossAmount" id="grossamount" tabindex="32" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="amount" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/Amount" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="percentnumber" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/PercentNumber" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_middlename" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_middlename" Text="Middle Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_middlename" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/MiddleName" RMXType="text" tabindex="13" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_netamount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_netamount" Text="Net Amount" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/NetAmount" id="netamount" tabindex="33" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_taxid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_taxid" Text="Tax ID" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/FundsAuto/PayeeEntity/TaxId" id="pye_taxid" tabindex="14" style="background-color: #F2F2F2;" readonly="true" onchange="ssnLostFocus(this);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_noofthirdparties" xmlns="">
                <asp:label runat="server" class="label" id="lbl_noofthirdparties" Text="# Of Third Parties" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/NoOfThirdParties" id="noofthirdparties" tabindex="34" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_addr1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_addr1" Text="Address 1" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_addr1" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/Addr1" RMXType="text" tabindex="15" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_thirdpartytotal" xmlns="">
                <asp:label runat="server" class="label" id="lbl_thirdpartytotal" Text="Third Party Total" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/ThirdPartyTotal" id="thirdpartytotal" tabindex="35" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_addr2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_addr2" Text="Address 2" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_addr2" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/Addr2" RMXType="text" tabindex="16" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_paymentstodate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_paymentstodate" Text="# Of Checks Printed" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/FundsAutoBatch/PaymentsToDate" id="paymentstodate" tabindex="36" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_city" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_city" Text="City" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_city" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/City" RMXType="text" tabindex="17" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_grosstodate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_grosstodate" Text="Printed Amount" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/GrossToDate" id="grosstodate" tabindex="37" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_stateid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_stateid" Text="State" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="pye_stateid" CodeTable="states" ControlName="pye_stateid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/StateId" RMXType="code" tabindex="18" />
                </span>
              </div>
              <div runat="server" class="half" id="div_totnoofchecks" xmlns="">
                <asp:label runat="server" class="label" id="lbl_totnoofchecks" Text="Total # Of Checks" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/TotalNumberOfChecks" id="totnoofchecks" tabindex="38" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_zipcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_zipcode" Text="Zip" />
                <span class="formw">
                  <asp:TextBox runat="server" onblur="zipLostFocus(this);" onchange="setDataChanged(true);" id="pye_zipcode" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ZipCode" RMXType="zip" TabIndex="20" />
                </span>
              </div>
              <div runat="server" class="half" id="div_pytgrandtotal" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pytgrandtotal" Text="Payment Grand Total" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/PaymentGrandTotal" id="pytgrandtotal" tabindex="39" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_checkmemo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_checkmemo" Text="Check Memo" />
                <span class="formw">
                  <asp:TextBox runat="server" id="checkmemo" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/CheckMemo" RMXType="text" tabindex="21" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_InsuffReserve" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="InsuffReserve" RMXRef="/Instance/UI/FormVariables/SysExData/InsuffReserve" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_InsuffReserveNonNeg" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="InsuffReserveNonNeg" RMXRef="/Instance/UI/FormVariables/SysExData/InsuffReserveNonNeg" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_CompRate" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="CompRate" RMXRef="/Instance/UI/FormVariables/SysExData/CompRate" RMXType="hidden" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Reason" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="Reason" RMXRef="/Instance/UI/FormVariables/SysExData/Reason" RMXType="hidden" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_isDuplicatePayment" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="isDuplicatePayment" RMXRef="/Instance/UI/FormVariables/SysExData/IsDuplicatePayment" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_htmlDuplicatePayment" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="htmlDuplicatePayment" RMXRef="/Instance/UI/FormVariables/SysExData/HTMLDuplicatePayment" RMXType="hidden" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdAdded" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/DttmRcdAdded" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdLastUpd" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/DttmRcdLastUpd" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdupdatedbyuser" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/UpdatedByUser" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdaddedbyuser" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/AddedByUser" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABdetailinfo" id="FORMTABdetailinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hddetailinfo" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="partial" id="div_PaymentInformationGrid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_PaymentInformationGrid" Text="" />
                <span>
                  <dg:UserControlDataGrid runat="server" ID="PaymentInformationGrid" GridName="PaymentInformationGrid" GridTitle="Payment Information" Target="/Instance/UI/FormVariables/SysExData/PaymentInformation" DynamicHideNodes="" Ref="" Unique_Id="" ShowCloneButton="False" ShowRadioButton="False" ShowCheckBox="False" RefImplementation="False" OnClick="KeepRowForEdit('PaymentInformationGrid');;" Width="98%" Height="150px" hidenodes="" ShowHeader="True" ShowFooter="False" LinkColumn="" PopupWidth="500" PopupHeight="400" HideButtons="New|Edit|Delete" Type="Grid" IncludeLastRecord="False" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="PaymentInformationGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/PaymentInformationGrid_Action" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" id="div_table1" xmlns="">
                <div runat="server" class="onecontrol" id="div_btnPaymentInfoGridAdd">
                  <script language="JavaScript" src="">{var i;}
                </script>
                  <span class="formw">
                    <asp:button class="button" runat="server" id="btnPaymentInfoGridAdd" RMXRef="" RMXType="buttonscript" Text="Add" width="100px" onClientClick="IncrementPaymentCount();return false;" />
                  </span>
                </div>
                <div runat="server" class="onecontrol" id="div_btnPaymentInfoGridDelete">
                  <script language="JavaScript" src="">{var i;}
                </script>
                  <span class="formw">
                    <asp:button class="button" runat="server" id="btnPaymentInfoGridDelete" RMXRef="" RMXType="buttonscript" Text="Delete" width="100px" onClientClick="DecrementPaymentCount();return false;" />
                  </span>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_" xmlns="">
                <span class="formw">
                  <br />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="partial" id="div_FuturePaymentsGrid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FuturePaymentsGrid" Text="" />
                <span>
                  <dg:UserControlDataGrid runat="server" ID="FuturePaymentsGrid" GridName="FuturePaymentsGrid" GridTitle="Transaction Details for Future Payments" Target="/Instance/UI/FormVariables/SysExData/FuturePayments" DynamicHideNodes="" Ref="" Unique_Id="AutoSplitId" ShowCloneButton="False" ShowRadioButton="True" ShowCheckBox="False" RefImplementation="False" OnClick="KeepRowForEdit('FuturePaymentsGrid');;" Width="680px" Height="90px" hidenodes="|AutoSplitId|GlAccountCode|InvoicedBy|InvoiceNumber|InvoiceAmount|InvoiceDate|PoNumber|Tag|reservebalance|" ShowHeader="True" ShowFooter="False" LinkColumn="" PopupWidth="500" PopupHeight="400" Type="GridAndButtons" IncludeLastRecord="False" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="FuturePaymentsSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/FuturePaymentSelectedId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="FuturePaymentsGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/FuturePaymentsGrid_Action" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="FuturePaymentsGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/FuturePaymentsGrid_RowAddedFlag" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="FuturePaymentsGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/FuturePaymentsGrid_RowDeletedFlag" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="FuturePaymentsGrid_RowEditFlag" RMXRef="/Instance/UI/FormVariables/SysExData/FuturePaymentsGrid_RowEditFlag" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="futurepaymentcount" RMXRef="/Instance/UI/FormVariables/SysExData/FuturePaymentsCount" RMXType="id" />
              <div runat="server" class="half" id="div_orgEid" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="orgEid" RMXRef="/Instance/UI/FormVariables/SysExData/DeptEid" RMXType="hidden" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABtppayees" id="FORMTABtppayees">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdtppayees" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="partial" id="div_ThirdPartyPaymentsGrid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ThirdPartyPaymentsGrid" Text="" />
                <span>
                  <dg:UserControlDataGrid runat="server" ID="ThirdPartyPaymentsGrid" GridName="ThirdPartyPaymentsGrid" GridTitle="Third Party Payments" Target="/Instance/UI/FormVariables/SysExData/ThirdPartyPayments" DynamicHideNodes="" Ref="" Unique_Id="AutoTransId" ShowCloneButton="False" ShowRadioButton="True" ShowCheckBox="False" RefImplementation="False" OnClick="KeepRowForEdit('ThirdPartyPaymentsGrid');;" Width="680px" Height="90px" hidenodes="|AutoSplitId|PayeeEid|MiddleName|TaxId|Addr1|Addr2|City|StateId|ZipCode|CheckMemo|EnclosureFlag|AutoTransId|DeductAgainstPayeeFlag|FromDate|ToDate|InvoiceAmount|GlAccountCode|InvoicedBy|InvoiceDate|InvoiceNumber|PoNumber|Tag|" ShowHeader="True" ShowFooter="False" LinkColumn="" PopupWidth="500" PopupHeight="450" Type="GridAndButtons" IncludeLastRecord="False" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="ThirdPartyPaymentsSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/ThirdPartyPaymentSelectedId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ThirdPartyPaymentsGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/ThirdPartyPaymentsGrid_Action" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ThirdPartyPaymentsGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/ThirdPartyPaymentsGrid_RowAddedFlag" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ThirdPartyPaymentsGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/ThirdPartyPaymentsGrid_RowDeletedFlag" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ThirdPartyPaymentsGrid_RowEditFlag" RMXRef="/Instance/UI/FormVariables/SysExData/ThirdPartyPaymentsGrid_RowEditFlag" RMXType="id" />
              <div runat="server" class="half" id="div_CheckFreeze" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="CheckFreeze" RMXRef="/Instance/UI/FormVariables/SysExData/CheckFreeze" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_isDoNotSaveDups" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="isDoNotSaveDups" RMXRef="/Instance/UI/FormVariables/SysExData/IsDoNotSaveDups" RMXType="hidden" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnSelectBatch">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnSelectBatch" RMXRef="" Text="Select Batch" onClientClick="return SelectBatch();" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="skipreservetypes" RMXRef="/Instance/UI/FormVariables/SysExData/Skipreservetypes" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="unitrowIdPassedIn" RMXRef="/Instance/UI/FormVariables/SysExData/UnitrowIdPassedIn" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="NonThiredPartyAmount" RMXRef="/Instance/UI/FormVariables/SysExData/NonThiredPartyAmount" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="FundsAutoBatch" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;FundsAutoBatch&gt;&lt;/FundsAutoBatch&gt;" />
      <asp:TextBox style="display:none" runat="server" id="isClaimNumberReadOnly" RMXRef="/Instance/UI/FormVariables/SysExData/IsClaimNumberReadOnly" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="thisClaimNumber" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ClaimNumber" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="reserveID" RMXRef="/Instance/UI/FormVariables/SysExData/ReserveID" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="autoclaimchecks" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="autobatchid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="10400" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSscheduledchecks|TABSdetailinfo|TABStppayees" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="AddNewPayee" RMXRef="" RMXType="hidden" Text="false" />
      <asp:TextBox style="display:none" runat="server" id="SysPostBackAction" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="gettaxid" RMXRef="/Instance/UI/FormVariables/SysExData/Gettaxid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="foundeid" RMXRef="/Instance/UI/FormVariables/SysExData/Foundeid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="lookuptaxid" RMXRef="/Instance/UI/FormVariables/SysExData/Lookuptaxid" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" id="allowtaxidlookup" RMXRef="/Instance/UI/FormVariables/SysExData/Allowtaxidlookup" RMXType="hidden" Text="" xmlns:mc="remove" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="autoclaimchecks" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="claimnumber|payment_interval_codelookup_cid|bankaccount|cbopayeetype|pye_lastname|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="FuturePaymentsGrid|ThirdPartyPaymentsGrid|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="claimnumber|" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>