﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admintrackinglist.aspx.cs" Inherits="Riskmaster.UI.FDM.AdminTrackingList" ValidateRequest="false" EnableViewStateMac="true"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Riskmaster</title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="Scripts/drift.js"></script> 
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">
    <div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
    <h3>Administrative Tracking</h3>
    <table border="0" width="95%" cellspacing="0" cellpadding="2">
    
    <%int i = 0; foreach (XElement item in result)
      {
        %>
        <tr>
         <td class="td12" width="32"/>
                <td class="td12" width="18">
                  <img src="../../Images/arrow.gif" width="10" height="10" border="0" alt=""/>
                </td>
       
            <td class="td12">                          
            <%lnkAdminTab.Text = item.Element("TableName").Value;%> 
               <%
                   //rsushilaggar Pan Testing changes
                   //lnkAdminTab.PostBackUrl = "admintracking" + item.Element("SystemTableName").Value + ".aspx?SysFormName=admintracking|" + item.Element("SystemTableName").Value + "&SysCmd=" + item.Element("SysCmd").Value + "&SysViewType=controlsonly&SysSid=" + item.Element("TableSecurityId").Value + "&SysFormPIdName=eventid&SysViewType=controlsonly";
                   lnkAdminTab.PostBackUrl = "admintracking" + item.Element("SystemTableName").Value + ".aspx?SysFormName=admintracking|" + item.Element("SystemTableName").Value + "^SysCmd=" + item.Element("SysCmd").Value + "^SysViewType=controlsonly^SysSid=" + item.Element("TableSecurityId").Value + "^SysFormPIdName=eventid^SysViewType=controlsonly";
                   lnkAdminTab.PostBackUrl = "admintrackinglist.aspx?RedidectURL=" + lnkAdminTab.PostBackUrl ;
                  //End rsushilaggar          
                %>
              <asp:LinkButton runat="server" id="lnkAdminTab"></asp:LinkButton> 
            </td>
       </tr>  
                           
 <% }%> 
</table>
    <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
    <asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
    <asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
    <asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
    <asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
    <asp:TextBox runat="server" id="SysClassName" style="display:none" Text="ADMTableList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
    <asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
    <asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
    <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm" Text="event"/>
    <asp:TextBox runat="server" id="SysFormPIdName" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPIdName" Text="eventid"/>
    <asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
    <asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
    <asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
    <asp:TextBox runat="server" id="SysNotReqNew" style="display:none" RMXRef="Instance/UI/FormVariables/SysNotReqNew" />
    <asp:TextBox runat="server" id="SysFormName" style="display:none" Text="admintrackinglist" RMXRef="Instance/UI/FormVariables/SysFormName" />
    <asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="rowid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId" Text=""/>
    <asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
    <asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
    <asp:TextBox runat="server" id="rowid" style="display:none" RMXREF="Instance/UI/FormVariables/SysExData/RowId"/>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
