<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Combinedpayment.aspx.cs" Inherits="Riskmaster.UI.FDM.Combinedpayment"  ValidateRequest="false"  %>


<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Combined Payment</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
 <%--   <script language="JavaScript" src="csc-Theme/riskmaster/common/javascript/autochecks.js">        { var i; }</script>--%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      <style type="text/css">
          .style1
          {
              width: 1171px;
          }
      </style>
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
       
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/tb_lookup_active.png" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/tb_lookup_mo.png';" onMouseOut="this.src='../../Images/tb_lookup_active.png';" />
        </div>
        
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Combined Payment" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
    <%--  <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABScombinedpayment" id="TABSscombinedpayment">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="combinedpayment" id="LINKTABScombinedpayment">Combined Payment</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPcombinedpayment">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSschedulesetup" id="TABSschedulesetup">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="detailinfo" id="LINKTABSschedulesetup">Schedule Setup</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPschedulesetup">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSstoppayment" id="TABSstoppayment">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="tppayees" id="LINKTABSstoppayment">Stop Combined Payment</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPstoppayment">
          <nbsp />
          <nbsp />
        </div>
      </div>--%>
    
        <%--   <div  runat="server" name="FORMTABcombinedpayment" id="FORMTABcombinedpayment">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdcombinedpayment" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="MaxNumberOfAutoChecks" RMXRef="/Instance/UI/FormVariables/SysExData/MaxNumberOfAutoChecks" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="lob" RMXRef="/Instance/UI/FormVariables/SysExData/Lob" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ClaimId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="autobatchid2" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/AutoBatchId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="fundsautolist" RMXRef="/Instance/FundsAutoBatch/FundsAutoList/@count" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="autobatchid" RMXRef="/Instance/FundsAutoBatch/AutoBatchId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="autotransid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/AutoTransId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ClaimantInformation" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantInformation" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="ClaimantCount" RMXRef="/Instance/UI/FormVariables/SysExData/ClaimantCount" RMXType="id" />
            
          
           
            </td>
          </tr>
         
         
          <tr>
            <td>
              <div runat="server" class="half" id="div_bankaccount" xmlns="">
                <asp:label runat="server" class="required" id="lbl_bankaccount" Text="Bank Account" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="bankaccount" tabindex="8" RMXRef="" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/AccountList" onchange="setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_freezebatchflag" xmlns="">
              </div>
            </td>
          </tr>
       
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="pye_entityid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/PayeeEid/@codeid" RMXType="id" />
              <div runat="server" class="half" id="div_pye_lastname" xmlns="">
                <asp:label runat="server" class="required" id="lbl_pye_lastname" Text="Last Name" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/LastName" RMXType="financiallastname" id="pye_lastname" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="pye_lastname_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="pye_lastnamebtn" tabindex="11" onclientclick="return OnLastNameClick();" />
                </span>
              </div>
              <div runat="server" class="half" id="div_pye_firstname" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_firstname" Text="First Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_firstname" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/FirstName" RMXType="text" tabindex="12" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
         
         
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_taxid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_taxid" Text="Tax ID" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="/Instance/UI/FormVariables/SysExData/FundsAuto/PayeeEntity/TaxId" id="pye_taxid" tabindex="14" style="background-color: #F2F2F2;" readonly="true" onchange="ssnLostFocus(this);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_noofthirdparties" xmlns="">
                
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_pye_addr1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_addr1" Text="Address 1" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_addr1" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/Addr1" RMXType="text" tabindex="15" onchange=";setDataChanged(true);" />
                </span>
              </div>
             <div runat="server" class="half" id="div_pye_addr2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_addr2" Text="Address 2" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_addr2" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/Addr2" RMXType="text" tabindex="16" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
             
             <div runat="server" class="half" id="div_pye_city" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_city" Text="City" />
                <span class="formw">
                  <asp:TextBox runat="server" id="pye_city" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/City" RMXType="text" tabindex="17" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_pye_stateid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_stateid" Text="State" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="pye_stateid" CodeTable="states" ControlName="pye_stateid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/StateId" RMXType="code" tabindex="18" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
               <div runat="server" class="half" id="div_pye_zipcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_zipcode" Text="Zip" />
                <span class="formw">
                  <asp:TextBox runat="server" onblur="zipLostFocus(this);" onchange="setDataChanged(true);" id="pye_zipcode" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ZipCode" RMXType="zip" TabIndex="20" />
                </span>
              </div>
               <div runat="server" class="half" id="div_pye_country" xmlns="">
                <asp:label runat="server" class="label" id="lbl_pye_country" Text="Country" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="pye_countryid" CodeTable="states" ControlName="pye_countryid" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/StateId" RMXType="code" tabindex="18" />
                </span>
              </div>
            </td>
          </tr>
         
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdAdded" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/DttmRcdAdded" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdDttmRcdLastUpd" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/DttmRcdLastUpd" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdupdatedbyuser" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/UpdatedByUser" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="hdaddedbyuser" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/AddedByUser" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div class="msgheader" id="div_schedulesetup" runat="server">
          Schedule Setup</div>
          <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
          <td >
           <div runat="server" class="half" id="div_payment_interval" xmlns="">
                <asp:label runat="server" class="required" id="lbl_payment_interval" Text="Pay Interval" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="payment_interval" CodeTable="PERIOD_TYPES" ControlName="payment_interval" RMXRef="/Instance/FundsAutoBatch/PaymentInterval" RMXType="code" tabindex="22" Required="true" ValidationGroup="vgSave" />
                </span>
              </div>
              <div runat="server" class="half" id="div_nxtschdate" xmlns="">
                <asp:label runat="server" class="required" id="lbl_nxtschdate" Text="Next Scheduled Date" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="nxtschdate" RMXRef="Instance/Funds/TransDate" RMXType="date" tabindex="10" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="nxtschdatebtn" tabindex="11" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "nxtschdate",
					    ifFormat: "%m/%d/%Y",
					    button: "nxtschdatebtn"
					}
					);
				</script>
                </span>
              </div>
          </td>
          
          </tr>
             <tr>
          <td >
          <div runat="server" class="half" id="div_startingon" xmlns="">
                <asp:label runat="server" class="required" id="nxtstartingon" Text="Starting On" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="startingdate" RMXRef="Instance/Funds/TransDate" RMXType="date" tabindex="10" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="startingonbtn" tabindex="11" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "startingdate",
					    ifFormat: "%m/%d/%Y",
					    button: "startingonbtn"
					}
					);
				</script>
                </span>
              </div>
                <div runat="server" class="half" id="div_nxtschdprint" xmlns="">
                <asp:label runat="server" class="required" id="lbl_nxtschdprint" Text="Next Scheduled Print" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="nxtschdprint" RMXRef="Instance/Funds/TransDate" RMXType="date" tabindex="10" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="nxnchdprintbtn" tabindex="11" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "nxtschdprint",
					    ifFormat: "%m/%d/%Y",
					    button: "nxnchdprintbtn"
					}
					);
				</script>
                </span>
              </div>
          </td>
          
          </tr>
            <tr>
          <td >
          <div runat="server" class="half" id="div_startingonwords" xmlns="">
               
           
               
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="startingdatewords" RMXRef="Instance/Funds/TransDate" RMXType="date" tabindex="10" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                 
                </span>
              </div>
                <div runat="server" class="half" id="div_nxtoverrideprint" xmlns="">
                <asp:label runat="server" class="required" id="lbl_nxtprintoverride" Text="Next Print Override" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="nxtprintoverride" RMXRef="Instance/Funds/TransDate" RMXType="date" tabindex="10" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="nxtprintoverridebtn" tabindex="11" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "nxtprintoverride",
					    ifFormat: "%m/%d/%Y",
					    button: "nxtprintoverridebtn"
					}
					);
				</script>
                </span>
              </div>
          </td>
          
          </tr>
          </table>
           <div class="msgheader" id="div_stoppayment" runat="server">
          Stop combined Setup</div>
          <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
          <td >
        
              <div runat="server" class="half" id="div_stop" xmlns="">
                <asp:label runat="server" class="required" id="lbl_stopcombpay" Text="Stop Combined Pay" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="stopcombpay" RMXRef="Instance/Funds/TransDate" RMXType="date" tabindex="10" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="stopcombpaybtn" tabindex="11" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "stopcombpay",
					    ifFormat: "%m/%d/%Y",
					    button: "stopcombpaybtn"
					}
					);
				</script>
                </span>
              </div>
          </td>
          
          </tr>
             <tr>
         
          
          </tr>
         
          </table>
       --%>
    <%--  <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="FundsAutoBatch" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;FundsAutoBatch&gt;&lt;/FundsAutoBatch&gt;" />
      <asp:TextBox style="display:none" runat="server" id="isClaimNumberReadOnly" RMXRef="/Instance/UI/FormVariables/SysExData/IsClaimNumberReadOnly" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="thisClaimNumber" RMXRef="/Instance/UI/FormVariables/SysExData/FundsAuto/ClaimNumber" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="reserveID" RMXRef="/Instance/UI/FormVariables/SysExData/ReserveID" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
     
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="10400" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
    
   
      <asp:TextBox style="display:none" runat="server" id="SysPostBackAction" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="autoclaimchecks" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="claimnumber|payment_interval_codelookup_cid|bankaccount|cbopayeetype|pye_lastname|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="FuturePaymentsGrid|ThirdPartyPaymentsGrid|" />
     
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />--%>
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
     
      </div>
    </form>
  </body>
</html>