﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.Shared.Controls;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class Physician : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Added Rakhi forR7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            TextBox txtPrimaryAddressChanged = (TextBox)this.FindControl("PrimaryAddressChanged");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                if (txtPrimaryAddressChanged != null)
                    IncludeLastRecord(txtPrimaryAddressChanged.Text);
            }
            //Added Rakhi forR7:Add Emp Data Elements
            
            FDMPageLoad();

            //Added Rakhi forR7:Add Emp Data Elements
            if (txtPrimaryAddressChanged != null)
                txtPrimaryAddressChanged.Text = "false"; //Setting it false after the processing is over.
            //Added Rakhi forR7:Add Emp Data Elements
        }
        public override void ModifyControls()
        {
            //Added Rakhi For R7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                SetPrimaryAddress();
            }
            //Added Rakhi For R7:Add Emp Data Elements
        }
        //02/25/2010 MITS: Kuladeep code for Policy Tracking enable or disable  START:
        /// <summary>
        /// Override method for Update Form
        /// </summary>
        /// <param name="oMessageElement">Input XML</param>
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);
            Button btnPolicyOpen = (Button)(this.Form.FindControl("primarypolicyidbtn"));
            XElement oIsAnyPolicyDeActive = oMessageElement.XPathSelectElement("//IsAnyPolicyDeActive");
            if (oIsAnyPolicyDeActive != null)
            {
                if (oIsAnyPolicyDeActive.Value == "0")
                {
                    if (btnPolicyOpen != null)
                    {
                        btnPolicyOpen.OnClientClick = "return lookupData('primarypolicyid','policy',-1,'primarypolicyid',20)";
                    }
                }
            }

        }
        //02/25/2010 MITS: Kuladeep code for Policy Tracking enable or disable END

        private void IncludeLastRecord(string sPrimaryAddressChanged)
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");
            if (objGridControl != null)
            {
                if (sPrimaryAddressChanged.ToLower() == "true")
                {

                    objGridControl.IncludeLastRecord = true;
                }
                else
                {
                    objGridControl.IncludeLastRecord = false;
                }
            }
        }
        private void SetPrimaryAddress()
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");//Capturing Primary Flag
            bool bPrimaryFlag = false;
            if (objGridControl != null)
            {
                GridView gvData = (GridView)objGridControl.GridView;
                int iGridRowCount = gvData.Rows.Count;
                int iPrimaryColIndex = 0;
                int iUniqueIdIndex = 0;
                if (iGridRowCount > 1)
                {
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    for (int i = 0; i < gvData.Columns.Count - 1; i++)
                    {
                        if (gvData.Columns[i].HeaderText.ToLower() == "primary address")
                        {
                            iPrimaryColIndex = i;
                            continue;
                        }
                        else if (gvData.Columns[i].HeaderText.ToLower() == "uniqueid_hidden")
                        {
                            iUniqueIdIndex = i;
                            continue;
                        }
                    }
                    for (int i = 0; i < iGridRowCount - 1; i++)
                    {
                        string sValue = gvData.Rows[i].Cells[iPrimaryColIndex].Text.ToLower();
                        if (sValue == "true")
                        {
                            bPrimaryFlag = true;
                            if (txtPrimaryRow != null)
                                txtPrimaryRow.Text = gvData.Rows[i].Cells[iUniqueIdIndex].Text;
                            break;
                        }
                    }
                    if (!bPrimaryFlag)
                    {
                        if (txtPrimaryRow != null)
                            txtPrimaryRow.Text = "";
                    }
                }
                else
                {
                    bPrimaryFlag = false;
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    if (txtPrimaryRow != null)
                        txtPrimaryRow.Text = "";
                }
            }
            TextBox txtPrimaryFlag = (TextBox)this.FindControl("PrimaryFlag");
            if (txtPrimaryFlag != null)
                txtPrimaryFlag.Text = bPrimaryFlag.ToString();
        }
    }
}
