<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="thirdpartypayments.aspx.cs"  Inherits="Riskmaster.UI.FDM.Thirdpartypayments" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Third Party Payment Detail Entry</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <script language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>
    <script language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="CopyGridRowDataToPopup();ThirdPartyPayment_onLoad();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Third Party Payment Detail Entry" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSPayeeDetailsGroup" id="TABSPayeeDetailsGroup">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="PayeeDetailsGroup" id="LINKTABSPayeeDetailsGroup">Payee Details</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPPayeeDetailsGroup">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSThirdPartyPaymentGroup" id="TABSThirdPartyPaymentGroup">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="ThirdPartyPaymentGroup" id="LINKTABSThirdPartyPaymentGroup">Transaction</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPThirdPartyPaymentGroup">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="gridpopupborder" runat="server" name="FORMTABPayeeDetailsGroup" id="FORMTABPayeeDetailsGroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdPayeeDetailsGroup" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="PayeeEid" RMXRef="//PayeeEid" RMXType="id" />
              <div runat="server" class="popuprow" id="div_LastName" xmlns="">
                <asp:label runat="server" class="required" id="lbl_LastName" Text="Last Name" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="7" onblur="lookupLostFocus(this);" RMXRef="//LastName" RMXType="entitylookup" id="LastName" />
                  <asp:button runat="server" class="EllipsisControl" id="LastNamebtn" tabindex="8" onclientclick="return lookupData('LastName','',4,'LastName',1);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_FirstName" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FirstName" Text="First Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="FirstName" RMXRef="//FirstName" RMXType="text" tabindex="8" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_MiddleName" xmlns="">
                <asp:label runat="server" class="label" id="lbl_MiddleName" Text="Middle Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="MiddleName" RMXRef="//MiddleName" RMXType="text" tabindex="9" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_TaxId" xmlns="">
                <asp:label runat="server" class="label" id="lbl_TaxId" Text="Tax Id" />
                <span class="formw">
                  <asp:TextBox runat="server" id="TaxId" RMXRef="//TaxId" RMXType="text" tabindex="10" onchange=";setDataChanged(true);" readonly="true" style="&#xA;                                background-color: #F2F2F2;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_Addr1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Addr1" Text="Address" />
                <span class="formw">
                  <asp:TextBox runat="server" id="Addr1" RMXRef="//Addr1" RMXType="text" tabindex="11" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_Addr2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Addr2" Text="" />
                <span class="formw">
                  <asp:TextBox runat="server" id="Addr2" RMXRef="//Addr2" RMXType="text" tabindex="12" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_City" xmlns="">
                <asp:label runat="server" class="label" id="lbl_City" Text="City" />
                <span class="formw">
                  <asp:TextBox runat="server" id="City" RMXRef="//City" RMXType="text" tabindex="13" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_StateId" xmlns="">
                <asp:label runat="server" class="label" id="lbl_StateId" Text="State" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="StateId" CodeTable="states" ControlName="StateId" RMXRef="//StateId" RMXType="code" tabindex="14" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_ZipCode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ZipCode" Text="Zip" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="//ZipCode" id="ZipCode" tabindex="15" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_CheckMemo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_CheckMemo" Text="Check Memo" />
                <span class="formw">
                  <asp:TextBox runat="server" id="CheckMemo" RMXRef="//CheckMemo" RMXType="text" tabindex="16" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_DeductAgainstPayeeFlag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_DeductAgainstPayeeFlag" Text="Deduct Against Payee?" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="DeductAgainstPayeeFlag" RMXRef="//DeductAgainstPayeeFlag" RMXType="checkbox" tabindex="19" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_EnclosureFlag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_EnclosureFlag" Text="Third Party Payee Enclosure?" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="EnclosureFlag" RMXRef="//EnclosureFlag" RMXType="checkbox" tabindex="20" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="gridpopupborder" runat="server" style="display:none;" name="FORMTABThirdPartyPaymentGroup" id="FORMTABThirdPartyPaymentGroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdThirdPartyPaymentGroup" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_TransTypeCode" xmlns="">
                <asp:label runat="server" class="required" id="lbl_TransTypeCode" Text="Transaction Type:" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="TransTypeCode" CodeTable="TRANS_TYPES" ControlName="TransTypeCode" RMXRef="//TransTypeCode" RMXType="code" Required="true" ValidationGroup="vgSave" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_Amount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Amount" Text="Amount:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="Amount" RMXRef="//Amount" RMXType="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency" max="9999999999.99" onchange=";setDataChanged(true);" />
                  <input type="button" class="button" name="Enable" value="Enable" onClick="fnEnable();" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_PercentNumber" xmlns="">
                <asp:label runat="server" class="required" id="lbl_PercentNumber" Text="Percent:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="PercentNumber" RMXRef="//PercentNumber" RMXType="numeric" fixed="2" onChange="numLostFocus(this);fnCalculateAmount();;setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_ReserveTypeCode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ReserveTypeCode" Text="Reserve Type:" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="ReserveTypeCode" CodeTable="" ControlName="ReserveTypeCode" RMXRef="//ReserveTypeCode" RMXType="code" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_reservebalance" xmlns="">
                <asp:label runat="server" class="label" id="lbl_reservebalance" Text="Reserve Balance:" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="//reservebalance" id="reservebalance" style="background-color: #F2F2F2;" readonly="true" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_GlAccountCode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_GlAccountCode" Text="GL Account:" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="GlAccountCode" CodeTable="GL_ACCOUNTS" ControlName="GlAccountCode" RMXRef="//GlAccountCode" RMXType="code" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_FromDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FromDate" Text="From:" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="FromDate" RMXRef="//FromDate" RMXType="date" onchange="dateLostFocus(this.id);DateCompare(this);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="FromDatebtn" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "FromDate",
					    ifFormat: "%m/%d/%Y",
					    button: "FromDatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_ToDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ToDate" Text="To:" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="ToDate" RMXRef="//ToDate" RMXType="date" onchange="dateLostFocus(this.id);DateCompare(this);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="ToDatebtn" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "ToDate",
					    ifFormat: "%m/%d/%Y",
					    button: "ToDatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoicedBy" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoicedBy" Text="Invoiced By:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="InvoicedBy" RMXRef="//InvoicedBy" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoiceNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceNumber" Text="Invoice Number:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="InvoiceNumber" RMXRef="//InvoiceNumber" RMXType="text" onblur="InvNumLostFocus(this);" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoiceAmount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceAmount" Text="Invoice Amount:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="InvoiceAmount" RMXRef="//InvoiceAmount" RMXType="currency" onblur=";currencyLostFocus(this);" rmxforms:as="currency" max="9999999999.99" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoiceDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceDate" Text="Invoice Date:" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="InvoiceDate" RMXRef="//InvoiceDate" RMXType="date" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="InvoiceDatebtn" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "InvoiceDate",
					    ifFormat: "%m/%d/%Y",
					    button: "InvoiceDatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_PoNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_PoNumber" Text="P.O. Number:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="PoNumber" RMXRef="//PoNumber" RMXType="text" onblur="PONumberLostFocus(this);" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" width="75px" onClientClick="return ThirdPartyPayment_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return ThirdPartyPayment_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="AutoTransId" RMXRef="//AutoTransId" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="AutoSplitId" RMXRef="//AutoSplitId" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="lineofbusinesscode" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="orgEid" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="AutoSplitId" />
      <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="" RMXType="id" Text="thirdpartypayments" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSPayeeDetailsGroup|TABSThirdPartyPaymentGroup" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="thirdpartypayments" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="LastName|TransTypeCode_codelookup_cid|PercentNumber|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="TransTypeCode|" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>