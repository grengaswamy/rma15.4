<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="split.aspx.cs"  Inherits="Riskmaster.UI.FDM.Split" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Funds Split Detail</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <script language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>
    <script language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>

    <%--Add by kuladeep rmA14.1 performance Start--%>
    <link href="../../Scripts/jquery/themes/base/jquery-ui.div.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Scripts/jquery/json2.js"></script>
    <script  type="text/javascript" src="../../Scripts/jquery/ui/minified/jquery-ui.min.js"></script>
    <%--Add by kuladeep rmA14.1 performance End--%>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="CopyGridRowDataToPopup(); Split_onLoad();ShowSuppGrid();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Funds Split Detail" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSsplitdetaiilgroup" id="TABSsplitdetaiilgroup">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="splitdetaiilgroup" id="LINKTABSsplitdetaiilgroup">Transaction</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsplitdetaiilgroup">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsuppgroup">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="gridpopupborder" runat="server" name="FORMTABsplitdetaiilgroup" id="FORMTABsplitdetaiilgroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdsplitdetaiilgroup" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_FirstFinalPayment" xmlns="">
                <asp:label runat="server" class="required" id="lbl_FirstFinalPayment" Text="First And Final Payment" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="FirstFinalPayment" RMXRef="/option/IsFirstFinal/@value" RMXType="checkbox" onclick="FirstFinalPaymentFlagChanged(this)" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_Policy" xmlns="">
                <asp:label runat="server" class="required" id="lbl_Policy" Text="Policy" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="Policy" autopostback="true" RMXRef="/option/PolicyID/@value" RMXType="combobox" ItemSetRef="/option/PolicyList" onchange="setDataChanged(true);PolicyChanged()" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_Unit" xmlns="">
                <asp:label runat="server" class="required" id="lbl_Unit" Text="Unit" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="Unit" RMXRef="/option/UnitID/@value" RMXType="combobox" ItemSetRef="/option/UnitList" onchange="setDataChanged(true);UnitChanged()" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_Coverage" xmlns="">
                <asp:label runat="server" class="required" id="lbl_Coverage" Text="Coverage" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="Coverage" CodeTable="COVERAGE_TYPE" ControlName="Coverage" RMXRef="/option/CoverageTypeCode" CodeFilter="" RMXType="code" Required="true" ValidationGroup="vgSave" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_ReserveTypeCodeFt" xmlns="">
                <asp:label runat="server" class="required" id="lbl_ReserveTypeCodeFt" Text="Reserve Type:" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="ReserveTypeCodeFt" CodeTable="RESERVE_TYPE" ControlName="ReserveTypeCodeFt" RMXRef="/option/ReserveTypeCode" CodeFilter="" RMXType="code" Required="true" ValidationGroup="vgSave" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_TransTypeCode" xmlns="">
                <asp:label runat="server" class="required" id="lbl_TransTypeCode" Text="Transaction Type:" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="TransTypeCode" CodeTable="TRANS_TYPES" ControlName="TransTypeCode" RMXRef="/option/TransTypeCode" CodeFilter="" RMXType="code" Required="true" ValidationGroup="vgSave" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_ReserveTypeCode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ReserveTypeCode" Text="Reserve Type:" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="ReserveTypeCode" CodeTable="" ControlName="ReserveTypeCode" RMXRef="/option/ReserveTypeCode" CodeFilter="" RMXType="code" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_reservebalance" xmlns="">
                <asp:label runat="server" class="label" id="lbl_reservebalance" Text="Reserve Balance:" />
                <span class="formw">
                  <asp:TextBox runat="Server" RMXref="" id="reservebalance" style="background-color: #F2F2F2;" readonly="true" onChange="setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_Amount" xmlns="">
                <asp:label runat="server" class="required" id="lbl_Amount" Text="Amount:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="Amount" RMXRef="/option/Amount" RMXType="currency" rmxforms:as="currency" max="9999999999.99" onChange="currencyLostFocus(this);setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_GlAccountCode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_GlAccountCode" Text="GL Account:" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="GlAccountCode" CodeTable="GL_ACCOUNTS" ControlName="GlAccountCode" RMXRef="/option/GlAccountCode" CodeFilter="" RMXType="code" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_FromDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FromDate" Text="From:" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="FromDate" RMXRef="/option/FromDate" RMXType="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="FromDatebtn" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "FromDate",
					    ifFormat: "%m/%d/%Y",
					    button: "FromDatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_ToDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ToDate" Text="To:" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="ToDate" RMXRef="/option/ToDate" RMXType="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="ToDatebtn" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "ToDate",
					    ifFormat: "%m/%d/%Y",
					    button: "ToDatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoicedBy" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoicedBy" Text="Invoiced By:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="InvoicedBy" RMXRef="/option/InvoicedBy" RMXType="text" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoiceNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceNumber" Text="Invoice Number:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="InvoiceNumber" RMXRef="/option/InvoiceNumber" RMXType="text" onblur="InvNumLostFocus(this);" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoiceAmount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceAmount" Text="Invoice Amount:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="InvoiceAmount" RMXRef="/option/InvoiceAmount" RMXType="currency" rmxforms:as="currency" max="9999999999.99" onChange="currencyLostFocus(this);setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_InvoiceDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceDate" Text="Invoice Date:" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="InvoiceDate" RMXRef="/option/InvoiceDate" RMXType="date" onchange="dateLostFocus(this.id);validateDateYear(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="InvoiceDatebtn" />
                  <script type="text/javascript">
                      Zapatec.Calendar.setup(
					{
					    inputField: "InvoiceDate",
					    ifFormat: "%m/%d/%Y",
					    button: "InvoiceDatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="popuprow" id="div_PoNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_PoNumber" Text="P.O. Number:" />
                <span class="formw">
                  <asp:TextBox runat="server" id="PoNumber" RMXRef="/option/PoNumber" RMXType="text" onblur="PONumberLostFocus(this);" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div class="gridpopupborder" runat="server" style="display:none;" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdsuppgroup" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="SPLIT_ROW_ID" RMXRef="/option/Supplementals/SPLIT_ROW_ID" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" width="75px" onClientClick="SaveZapatecGridXmlForSplit();return Split_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return Split_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SplitRowId" RMXRef="/option/SplitRowId" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="SplitRowId" />
      <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="lineofbusinesscode" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="isReadOnly" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="orgEid" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="PostBackAction" RMXRef="" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="transid" RMXRef="/option/TransId" RMXType="id" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="" RMXType="hidden" Text="FundsTransSplit" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="" RMXType="hidden" Text="&lt;FundsTransSplit&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/FundsTransSplit&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="" RMXType="hidden" Text="reservetype" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="" RMXType="hidden" Text="split" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="" RMXType="hidden" Text="splitrowid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="clm_entityid" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSsplitdetaiilgroup|TABSsuppgroup" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="PolicyID" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="UnitID" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="RcRowID" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="PolCvgID" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="ResTypeCode" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="IsFirstFinalQueryString" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="ResTypeStatus" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="Splits" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="FirstFinalPayment|Policy|Unit|Coverage_codelookup_cid|ReserveTypeCodeFt_codelookup_cid|TransTypeCode_codelookup_cid|Amount|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="TransTypeCode|" />
      <asp:TextBox style="display:none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields" Text="ReserveTypeCode|reservebalance|" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>