<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="plan.aspx.cs" Inherits="Riskmaster.UI.FDM.Plan"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Non-occupational disability plan administration</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                        height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                        height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                        height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                        height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                        height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="Images/attach.gif"
                        width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                        onmouseover="this.src='Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/attach.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_search" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doSearch()" src="Images/search.gif"
                        width="28" height="28" border="0" id="search" alternatetext="Search" onmouseover="this.src='Images/search2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/search.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                        height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_comments" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Comments()" src="Images/comments.gif"
                        width="28" height="28" border="0" id="comments" alternatetext="Comments" onmouseover="this.src='Images/comments2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/comments.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_mailmerge" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="MailMerge()" src="Images/mailmerge.gif"
                        width="28" height="28" border="0" id="mailmerge" alternatetext="Mail Merge" onmouseover="this.src='Images/mailmerge2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/mailmerge.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Non-occupational disability plan administration" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSplaninfo" id="TABSplaninfo">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="planinfo"
                id="LINKTABSplaninfo"><span style="text-decoration: none">Plan Information</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABplaninfo" id="FORMTABplaninfo">
        <asp:textbox style="display: none" runat="server" id="planid" rmxref="/Instance/DisabilityPlan/PlanId"
            rmxtype="id" /><div runat="server" class="half" id="div_plannumber" xmlns="">
                <span class="required">Plan Number/Code</span><span class="formw"><asp:textbox runat="server"
                    onchange="setDataChanged(true);" id="plannumber" rmxref="/Instance/DisabilityPlan/PlanNumber"
                    rmxtype="text" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                        runat="server" controltovalidate="plannumber" /></span></div>
        <div runat="server" class="half" id="div_effectivedate" xmlns="">
            <span class="required">Effective Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="effectivedate" rmxref="/Instance/DisabilityPlan/EffectiveDate"
                rmxtype="date" tabindex="14" /><asp:requiredfieldvalidator validationgroup="vgSave"
                    errormessage="Required" runat="server" controltovalidate="effectivedate" /><cc1:CalendarExtender
                        runat="server" ID="effectivedate_ajax" TargetControlID="effectivedate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_planname" xmlns="">
            <span class="required">Plan Name</span><span class="formw"><asp:textbox runat="server"
                onchange="setDataChanged(true);" id="planname" rmxref="/Instance/DisabilityPlan/PlanName"
                rmxtype="text" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                    runat="server" controltovalidate="planname" /></span></div>
        <div runat="server" class="half" id="div_expirationdate" xmlns="">
            <span class="required">Expiration Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="expirationdate" rmxref="/Instance/DisabilityPlan/ExpirationDate"
                rmxtype="date" tabindex="16" /><asp:requiredfieldvalidator validationgroup="vgSave"
                    errormessage="Required" runat="server" controltovalidate="expirationdate" /><cc1:CalendarExtender
                        runat="server" ID="expirationdate_ajax" TargetControlID="expirationdate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_plandesc" xmlns="">
            <span class="label">Plan Description</span><span class="formw"><asp:textbox runat="server"
                onchange="setDataChanged(true);" id="plandesc" rmxref="/Instance/DisabilityPlan/PlanDescription"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_issuedate" xmlns="">
            <span class="label">Issue Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="issuedate" rmxref="/Instance/DisabilityPlan/IssueDate"
                rmxtype="date" tabindex="18" /><cc1:CalendarExtender runat="server" ID="issuedate_ajax"
                    TargetControlID="issuedate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_insuredlist" xmlns="">
            <span class="label">Insured(s)/Account(s)</span><span class="formw"><asp:listbox
                runat="server" size="3" id="insuredlist" rmxref="/Instance/DisabilityPlan/InsuredList"
                rmxtype="entitylist" style="" tabindex="4" /><input type="button" id="insuredlistbtn"
                    tabindex="5" onclick="lookupData('insuredlist','ORGANIZATIONS',4,'insuredlist',3)"
                    class=" button" value="..." /><input type="button" class="button" id="insuredlistbtndel"
                        tabindex="6" onclick="deleteSelCode('insuredlist')" value="-" /><asp:textbox runat="server"
                            style="display: none" rmxref="/Instance/DisabilityPlan/InsuredList/@codeid" id="insuredlist_lst" /></span></div>
        <div runat="server" class="half" id="div_reviewdate" xmlns="">
            <span class="label">Review Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="reviewdate" rmxref="/Instance/DisabilityPlan/ReviewDate"
                rmxtype="date" tabindex="20" /><cc1:CalendarExtender runat="server" ID="reviewdate_ajax"
                    TargetControlID="reviewdate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_planstatuscode" xmlns="">
            <span class="required">Plan Status</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="planstatuscode" rmxref="/Instance/DisabilityPlan/PlanStatusCode"
                rmxtype="code" cancelledvalue="" tabindex="7" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="planstatuscodebtn" onclientclick="return selectCode('PLAN_STATUS','planstatuscode');"
                    tabindex="8" /><asp:textbox style="display: none" runat="server" id="planstatuscode_cid"
                        rmxref="/Instance/DisabilityPlan/PlanStatusCode/@codeid" cancelledvalue="" /><asp:requiredfieldvalidator
                            validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="planstatuscode" /></span></div>
        <div runat="server" class="half" id="div_renewaldate" xmlns="">
            <span class="label">Renewal Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="renewaldate" rmxref="/Instance/DisabilityPlan/RenewalDate"
                rmxtype="date" tabindex="22" /><cc1:CalendarExtender runat="server" ID="renewaldate_ajax"
                    TargetControlID="renewaldate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_premium" xmlns="">
            <span class="label">Premium</span><span class="formw"><asp:textbox runat="server"
                id="premium" rmxref="/Instance/DisabilityPlan/Premium" rmxtype="currency" onblur=";currencyLostFocus(this);"
                rmxforms:as="currency" tabindex="9" onchange=";setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_canceldate" xmlns="">
            <span class="label">Cancel Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="canceldate" rmxref="/Instance/DisabilityPlan/CancelDate"
                rmxtype="date" tabindex="24" /><cc1:CalendarExtender runat="server" ID="canceldate_ajax"
                    TargetControlID="canceldate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_bankaccount" xmlns="">
            <span class="label">Bank Account</span><span class="formw"><asp:dropdownlist runat="server"
                id="bankaccount" tabindex="10" rmxref="/Instance/DisabilityPlan/BankAccId" rmxtype="combobox"
                itemsetref="/Instance/UI/FormVariables/SysExData/AccountList" width="205" onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_prefpaydaycode" xmlns="">
            <span class="label">Preferred Day of Week for Payments</span><span class="formw"><asp:textbox
                runat="server" onchange="lookupTextChanged(this);" id="prefpaydaycode" rmxref="/Instance/DisabilityPlan/PrefDayCode"
                rmxtype="code" cancelledvalue="" tabindex="26" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="prefpaydaycodebtn" onclientclick="return selectCode('PREF_PAYMENT_DAY','prefpaydaycode');"
                    tabindex="27" /><asp:textbox style="display: none" runat="server" id="prefpaydaycode_cid"
                        rmxref="/Instance/DisabilityPlan/PrefDayCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_startofpayperiod" xmlns="">
            <span class="label">Pay Period Start Date</span><span class="formw"><asp:textbox
                runat="server" formatas="date" onchange="setDataChanged(true);" id="startofpayperiod"
                rmxref="Instance/DisabilityPlan/StartPayPeriod" rmxtype="date" tabindex="11" /><cc1:CalendarExtender
                    runat="server" ID="startofpayperiod_ajax" TargetControlID="startofpayperiod" />
            </span>
        </div>
        <div runat="server" class="half" id="div_prefpayschedcode" xmlns="">
            <span class="label">Preferred Payment Schedule</span><span class="formw"><asp:dropdownlist
                runat="server" id="prefpayschedcode" tabindex="28" rmxref="/Instance/DisabilityPlan/PrefPaySchCode/@codeid"
                rmxtype="combobox" itemsetref="/Instance/UI/FormVariables/SysExData/PrefPaySchCode"
                onchange="checkPreferredMonthlyDayStatus();setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_printbeforepayendflag" xmlns="">
            <span class="label">Check Prints Before End of Pay Period</span><span class="formw"><asp:checkbox
                runat="server" onchange="setDataChanged(true);" id="printbeforepayendflag" rmxref="Instance/DisabilityPlan/PrintBeforeEnd"
                rmxtype="checkbox" tabindex="13" /></span></div>
        <div runat="server" class="half" id="div_PrefDayOfMCode" xmlns="">
            <span class="label">Preferred Monthly Day</span><span class="formw"><asp:dropdownlist
                runat="server" id="PrefDayOfMCode" tabindex="29" rmxref="/Instance/DisabilityPlan/PrefDayOfMCode/@codeid"
                rmxtype="combobox" itemsetref="/Instance/UI/FormVariables/SysExData/PrefDayOfMCode"
                onchange="setDataChanged(true);" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup" />
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
            <asp:button class="button" runat="Server" id="btnBack" text="Back " onclientclick="if(!( XFormHandler('','','back')))return false;"
                postbackurl="?" /></div>
        <div class="formButton" runat="server" id="div_btnPlanClasses">
            <asp:button class="button" runat="Server" id="btnPlanClasses" text="Plan Classes"
                rmxref="/Instance/DisabilityPlan/ClassList/@committedcount" onclientclick="if(!( XFormHandler('SysFormPIdName=planid&SysFormName=planclasses&SysCmd=1&SysFormIdName=classrowid&SysEx=/Instance/DisabilityPlan/PlanId','','')))return false;"
                postbackurl="?SysFormPIdName=planid&SysFormName=planclasses&SysCmd=1&SysFormIdName=classrowid&SysEx=/Instance/DisabilityPlan/PlanId" /></div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave"
            rmxref="Instance/UI/FormVariables/SysCmdConfirmSave" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdText"
                    rmxref="Instance/UI/FormVariables/SysCmdText" rmxtype="hidden" text="Navigate" /><asp:textbox
                        style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
                        rmxtype="hidden" text="DisabilityPlan" /><asp:textbox style="display: none" runat="server"
                            id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
                            rmxtype="hidden" text="&lt;DisabilityPlan&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/DisabilityPlan&gt;" /><asp:textbox
                                style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName"
                                    rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden" text="" /><asp:textbox
                                        style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysEx"
                                            rmxref="Instance/UI/FormVariables/SysEx" rmxtype="hidden" text="" /><asp:textbox
                                                style="display: none" runat="server" id="SysNotReqNew" rmxref="Instance/UI/FormVariables/SysNotReqNew"
                                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormName"
                                                    rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="plan" /><asp:textbox
                                                        style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                                        rmxtype="hidden" text="planid" /><asp:textbox style="display: none" runat="server"
                                                            id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden"
                                                            text="" /><asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
                                                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysSid"
                                                                    rmxref="Instance/UI/FormVariables/SysSid" rmxtype="hidden" text="11000" /><asp:textbox
                                                                        style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" name="formname"
                                                                            value="plan" /><asp:textbox style="display: none" runat="server" name="SysRequired"
                                                                                value="plannumber|effectivedate|planname|expirationdate|planstatuscode_cid|" /><asp:textbox
                                                                                    style="display: none" runat="server" name="SysFocusFields" value="" /><input type="hidden"
                                                                                        id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible" style="display: none" /><asp:textbox
                                                                                            runat="server" id="SysLookupClass" style="display: none" /><asp:textbox runat="server"
                                                                                                id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server" id="SysLookupAttachNodePath"
                                                                                                    style="display: none" /><asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" /><input
                                                                                                        type="hidden" value="rmx-widget-handle-2" id="SysWindowId" /></form>
</body>
</html>
