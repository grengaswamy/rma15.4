﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Event.aspx.cs" Inherits="Riskmaster.UI.FDM.Event" ValidateRequest="false"%>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
	
	<style type="text/css">
        
        div.half
        {
            float:left;
            width: 49%;
        }
        
        div.row
        {
            float:left;
            width: 100%;
        }
        
        div.half span.label {
            float: left;
            width: 32%;
            text-align: right;
            left: 1%;
            padding-right: 2%;
            font-size: 10pt;
            font-family: Tahoma, Arial, Helvetica, sans-serif;
            color: #333333
        }

        div.half span.required {
            float: left;
            width: 32%;
            text-align: right;
            left: 1%;
            padding-right: 2%;
            font-size: 10pt;
            font-family: Tahoma, Arial, Helvetica, sans-serif;
            color: #333333;
            text-decoration: underline;
        }

        div.half span.formw {
            float: left;
            width: 63%;
            text-align: left;
            left:35%;
        }
          
       div.image
       {
       	    float:left;
       	    width:28;
       	    height:32;
       }
       
       div.tab
       {
       	    width:50;
       }
    </style>
	
</head>
<body onload="pageLoaded()">
    <form id="frmData" runat="server">
        <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value=""/>
			<asp:TextBox runat="server" id="hTabName" style="display:none"/>
			<div id="toolbardrift" name="toolbardrift" class="toolbardrift" style="width:100%">
			    <div class="image">
			        <asp:ImageButton runat="server" OnClick="NavigateNew" ID="btnNew" ImageUrl="../../Images/new.gif" Width="28" Height="28" BorderStyle="None" AlternateText="New" ToolTip="New" />
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="NavigateSave" ID="btnSave" AlternateText="Save" ImageUrl="../../Images/save.gif" ToolTip="Save" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" ValidationGroup="vgSave" OnClientClick="return OnSaveButtonClicked();"/>
			    </div>
			    <div class="image">
			        <asp:ImageButton runat="server" OnClick="NavigateFirst" ID="btnFirst" ImageUrl="../../Images/first.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='../../Images/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/first.gif';this.style.zoom='100%'" ToolTip="Move First"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="NavigatePrev" ID="btnPrev" ImageUrl="../../Images/prev.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/prev.gif';this.style.zoom='100%'" ToolTip="Move Previous"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="NavigateNext" ID="btnNext" ImageUrl="../../Images/next.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='../../Images/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/next.gif';this.style.zoom='100%'" ToolTip="Move Next"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="NavigateLast" ID="btnLast" ImageUrl="../../Images/last.gif" border="0" alt="" width="28" height="28" onMouseOver="this.src='../../Images/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/last.gif';this.style.zoom='100%'" title="Move Last"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="NavigateDelete" ID="btnDelete" ImageUrl="../../Images/delete.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/delete.gif';this.style.zoom='100%'" title="Delete Record"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnAttach" ImageUrl="../../Images/attach.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/attach.gif';this.style.zoom='100%'" title="Attach Documents"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnExecSumm" ImageUrl="../../Images/esumm.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/esumm2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/esumm.gif';this.style.zoom='100%'" title="Executive Summary"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnSearch" ImageUrl="../../Images/search.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/search2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/search.gif';this.style.zoom='100%'" title="Search"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnLookup" ImageUrl="../../Images/lookup.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/lookup.gif';this.style.zoom='100%'" title="Lookup Data"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnFilteredDiary" ImageUrl="../../Images/filtereddiary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/filtereddiary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/filtereddiary.gif';this.style.zoom='100%'" title="View Record Diaries"/>
            		    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnDiary" ImageUrl="../../Images/diary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/diary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/diary.gif';this.style.zoom='100%'" title="Diary"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnComments" ImageUrl="../../Images/comments.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/comments2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/comments.gif';this.style.zoom='100%'" title="Comments" name="comments"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnEnhancedNotes" ImageUrl="../../Images/progressnotes.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/progressnotes2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/progressnotes.gif';this.style.zoom='100%'" title="Enhanced Notes" name="enhancednotes"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnMailMerge" ImageUrl="../../Images/mailmerge.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/mailmerge2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/mailmerge.gif';this.style.zoom='100%'" title="Mail Merge"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnRecordSummary" ImageUrl="../../Images/recordsummary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" title="Record Summary"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnCommentSummary" ImageUrl="../../Images/commentsummary.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/commentsummary2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/commentsummary.gif';this.style.zoom='100%'" title="Claim Comment Summary"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnEventExplorer" ImageUrl="../../Images/eventexplorer.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/eventexplorer2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/eventexplorer.gif';this.style.zoom='100%'" title="Quick Summary"/>
			    </div>
			    <div class="image">
				    <asp:ImageButton runat="server" OnClick="Navigate" ID="btnSendRegarding" ImageUrl="../../Images/sendregarding.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='../../Images/sendregarding2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/sendregarding.gif';this.style.zoom='100%'" title="Send Mail"/>
			    </div>
			</div>
			<br/>
			<div class="msgheader" id="formtitle">Event</div>
			<div class="errtextheader"/>
			<br/>
			<table border="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="Selected" nowrap="true" name="TABSeventinfo" id="TABSeventinfo">
									<a class="Selected" HREF="#" onClick="tabChange(this.name);" name="eventinfo" id="LINKTABSeventinfo">
										<span style="text-decoration:none">Event Info</span>
									</a>
								</td>
								<td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
								<td class="NotSelected" nowrap="true" name="TABSeventdetail" id="TABSeventdetail">
									<a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="eventdetail" id="LINKTABSeventdetail">
										<span style="text-decoration:none">Event Detail</span>
									</a>
								</td>
								<td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
								<td class="NotSelected" nowrap="true" name="TABSrepinfo" id="TABSrepinfo">
									<a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="repinfo" id="LINKTABSrepinfo">
										<span style="text-decoration:none">Reported Info</span>
									</a>
								</td>
								<td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
								<td class="NotSelected" nowrap="true" name="TABSfollowup" id="TABSfollowup">
									<a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="followup" id="LINKTABSfollowup">
										<span style="text-decoration:none">Follow Up</span>
									</a>
								</td>
								<td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
								<td class="NotSelected" nowrap="true" name="TABSsuppgroup" id="TABSsuppgroup">
									<a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" name="suppgroup" id="LINKTABSsuppgroup">
										<span style="text-decoration:none">Supplementals</span>
									</a>
								</td>
								<td nowrap="true" style="border-bottom:none;border-left:none;border-right:none;border-top:none;">&nbsp;&nbsp;</td>
								<td valign="top" nowrap="true"/>
							</tr>
						</table>
						<div class="singletopborder" style="position:relative;left:0;top:0;width:85%;height:315px;overflow:auto;margin-top:10px;">
							<div class="singletopborder" id="FORMTABeventinfo">
	                            <div class="half" id="div_eventnumber">
		                            <asp:TextBox runat="server" style="display:none" id="eventid" RMXRef="Instance/Event/EventId"/>
		                            <span class="required">Event Number:</span>
		                            <span class="formw">
		                                <asp:TextBox runat="server" tabindex="1" id="eventnumber" onchange="&#xA; ;setDataChanged(true);&#xA;  " maxlength="50" RMXRef="Instance/Event/EventNumber" />
                                    </span>
    	                        </div>
	                            <div class="half" id="div_datereported">
	                                <span class="required">Date Reported:</span>
		                            <span class="formw">
			                            <asp:TextBox runat="server" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="datereported" tabindex="16" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="Instance/Event/DateReported"/>
							            <cc1:CalendarExtender ID="datereported_CalendarExtender" runat="server" TargetControlID="datereported" />
							            <asp:RequiredFieldValidator runat="server" ID="rfv_datereported" ControlToValidate="datereported" ValidationGroup="vgSave" ErrorMessage="(Required)"></asp:RequiredFieldValidator>
		                            </span>
	                            </div>
	                            <div class="half" id="div_eventtypecode">
		                            <span class="required">Event Type:</span>
                                    <span class="formw">
			                            <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="eventtypecode" tabindex="2" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/EventTypeCode"/>
			                            <input type="button" id="eventtypecodebtn" class="button" onClick="return CodeSelect('EVENT_TYPE','eventtypecode')" value="..."/>
			                            <asp:TextBox runat="server" style="display:none" id="eventtypecode_cid" cancelledvalue="" RMXRef="Instance/Event/EventTypeCode/@codeid"/>
			                            <asp:RequiredFieldValidator runat="server" ID="rfv_eventtypecode" ControlToValidate="eventtypecode" ValidationGroup="vgSave" ErrorMessage="(Required)"></asp:RequiredFieldValidator>
	                                </span>
	                            </div> 
	                            <div class="half" id="div_timereported">
		                            <span class="label">Time Reported:</span>
                                    <span class="formw">
			                            <asp:TextBox runat="server" size="30" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);" FormatAs="time" id="timereported" tabindex="18" RMXRef="Instance/Event/TimeReported"/>
		                                <cc1:MaskedEditExtender ID="timereported_MaskedEditExtender" runat="server" Enabled="True" TargetControlID="timereported" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" />
		                            </span>
	                            </div>
	                            <div class="half" id="div_dateofevent">
		                            <span class="required">Date Of Event</span>
                                    <span class="formw">
			                            <asp:TextBox runat="server" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="dateofevent" tabindex="4" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="Instance/Event/DateOfEvent"/>
			                            <cc1:CalendarExtender ID="dateofevent_CalendarExtender" runat="server" TargetControlID="dateofevent" />
							            <asp:RequiredFieldValidator runat="server" ID="rfv_dateofevent" ControlToValidate="dateofevent" ValidationGroup="vgSave" ErrorMessage="(Required)"></asp:RequiredFieldValidator>
	                                </span>
	                            </div>
	                            <div class="half" id="div_injfromdate">
                                    <span class="label">Injury From:</span>
                                    <span class="formw">
			                            <asp:TextBox runat="server" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="injfromdate" tabindex="19" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="Instance/Event/InjuryFromDate"/>
			                            <cc1:CalendarExtender ID="injfromdate_CalendarExtender" runat="server" TargetControlID="injfromdate" />
	                                </span>
	                            </div>
	                            <div class="half" id="div_timeofevent">
			                            <span class="label">Time Of Event</span>
			                            <asp:TextBox runat="server" size="30" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);" FormatAs="time" id="timeofevent" tabindex="6" RMXRef="Instance/Event/TimeOfEvent"/>
	                                    <cc1:MaskedEditExtender ID="timeofevent_MaskedEditExtender" runat="server" Enabled="True" TargetControlID="timeofevent" MaskType="Time" Mask="99:99:99" AcceptAMPM="true" />
	                            </div>
	                            <div class="half" id="div_injtodate">
		                            <span class="label">Injury To:</span>
		                            <span class="formw">
			                            <asp:TextBox runat="server" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="injtodate" tabindex="21" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="Instance/Event/InjuryToDate"/>
			                            <cc1:CalendarExtender ID="injtodate_CalendarExtender" runat="server" TargetControlID="injtodate" />
	                                </span>
	                            </div>
	                            <div class="half" id="div_eventstatuscode">
		                            <span class="required">Event Status:</span>
                                    <span class="formw">
			                            <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="eventstatuscode" tabindex="7" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/EventStatusCode"/>
			                            <input type="button" id="eventstatuscodebtn" class="button" onClick="return CodeSelect('EVENT_STATUS','eventstatuscode')" value="..."/>
			                            <asp:TextBox runat="server" style="display:none" id="eventstatuscode_cid" cancelledvalue="" RMXRef="Instance/Event/EventStatusCode/@codeid"/>
	                                    <asp:RequiredFieldValidator runat="server" ID="rfv_eventstatuscode" ControlToValidate="eventstatuscode" ValidationGroup="vgSave" ErrorMessage="(Required)"></asp:RequiredFieldValidator>
	                                </span>
	                            </div>
	                            <div class="half" id="div_eventindcode">
		                            <span class="label">Event Indicator:</span>
		                            <span class="formw">
			                            <asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="eventindcode" tabindex="9" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id,'eventcategory');&#xA;" RMXRef="Instance/Event/EventIndCode"/>
			                            <input type="button" id="eventindcodebtn" class="button" onClick="return CodeSelect('EVENT_INDICATOR','eventindcode')" value="..."/>
			                            <asp:TextBox runat="server" style="display:none" id="eventindcode_cid" cancelledvalue="" RMXRef="Instance/Event/EventIndCode/@codeid"/>
	                                </span>
	                            </div>
	                            <div class="half" id="div_eventcategory">
		                            <span class="label">Event Category:</span>
		                            <span class="formw">
			                            <asp:TextBox runat="server" size="30" id="eventcategory" tabindex="11" style="background-color: silver;" readonly="true" RMXRef="Instance/UI/FormVariables/SysExData/EventCat"/>
		                            </span>
	                            </div>
	                            <div class="half" id="div_depteid">
		                            <span class="required">Department:</span>
                                    <span class="formw">
			                            <asp:TextBox runat="server" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="depteid" tabindex="12" cancelledvalue="" RMXRef="Instance/Event/DeptEid"/>
			                            <asp:Button runat="server" class="CodeLookupControl" id="depteidbtn" tabindex="13" />
			                            <asp:TextBox runat="server" style="display:none" id="depteid_cid" cancelledvalue="" RMXRef="Instance/Event/DeptEid/@codeid"/>
	                                    <asp:RequiredFieldValidator runat="server" ID="rfv_depteid" ControlToValidate="depteid" ValidationGroup="vgSave" ErrorMessage="(Required)"></asp:RequiredFieldValidator>
	                                </span>
	                            </div>
	                            <div class="half" id="div_deptinvolvedeid">
		                            <span class="label">Department Involved:</span>
		                            <span class="formw">
			                            <asp:TextBox runat="server" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="deptinvolvedeid" tabindex="14" cancelledvalue="" RMXRef="Instance/Event/DeptInvolvedEid"/>
			                            <asp:Button runat="server" class="CodeLookupControl" id="deptinvolvedeidbtn" tabindex="15" />
			                            <asp:TextBox runat="server" style="display:none" id="deptinvolvedeid_cid" cancelledvalue="" RMXRef="Instance/Event/DeptInvolvedEid/@codeid"/>
	                                </span>
	                            </div>
                            </div>
							
							<div id="FORMTABeventdetail" style="display:none;">
								<div class="half" id="div_locationareadesc">
		                            <span class="label">Location Description:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" TextMode="MultiLine" cols="30" tabindex="22" rows="5" id="locationareadesc" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" RMXRef="Instance/Event/LocationAreaDesc"/>
										<asp:Button runat="server" class="button" id="locationareadescbtnMemo" tabindex="23" value="..."/>
										<asp:TextBox runat="server" ID="locationareadesc_HTML" style="display:none" RMXRef="Instance/Event/LocationAreaDesc_HTMLComments"/>
									</span>
								</div>
								<div class="half" id="div_eventdescription">
	                                <span class="label">Event Description:</span>
	                                <span class="formw">
									    <asp:TextBox runat="server" TextMode="MultiLine" cols="30" tabindex="33" rows="5" id="eventdescription" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" RMXRef="Instance/Event/EventDescription"/>
									    <asp:Button runat="server" class="button" id="eventdescriptionbtnMemo" tabindex="34" value="..."/>
									    <asp:TextBox runat="server" ID="eventdescription_HTML" style="display:none" RMXRef="Instance/Event/EventDescription_HTMLComments"/>
								    </span>
								</div>
								<div class="half" id="div_addr1">
		                            <span class="label">Location Address 1:</span>
		                            <span class="formw">
											<asp:TextBox runat="server" size="30" tabindex="24" id="addr1" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/Addr1"/>
									</span>
								</div>
								<div class="half" id="div_onpremiseflag">
		                            <span class="label">Event On Premise:</span>
		                            <span class="formw">
										<asp:CheckBox runat="server" appearance="full" tabindex="35" id="onpremiseflag" onchange="ApplyBool(this);" onclick="FillEventLocationDetails(this)" RMXRef="Instance/Event/OnPremiseFlag" />
										<asp:TextBox runat="server" style="display:none" cancelledvalue="" id="onpremiseflag_mirror"/>
									</span>
								</div>
								<div class="half" id="div_addr2">
		                            <span class="label">Location Address 2:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" tabindex="25" id="addr2" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/Addr2"/>
									</span>
								</div>
								<div class="half" id="div_primaryloccode">
		                            <span class="label">Primary Location:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="primaryloccode" tabindex="36" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/PrimaryLocCode"/>
										<asp:Button runat="server" class="CodeLookupControl" id="primaryloccodebtn" tabindex="37" />
										<asp:TextBox runat="server" style="display:none" id="primaryloccode_cid" cancelledvalue="" RMXRef="Instance/Event/PrimaryLocCode/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_city">
		                            <span class="label">City:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" tabindex="26" id="city" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/City"/>
									</span>
								</div>
								<div class="half" id="div_locationtypecode">
		                            <span class="label">Location Type:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="locationtypecode" tabindex="38" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/LocationTypeCode"/>
										<asp:Button runat="server" class="CodeLookupControl" id="locationtypecodebtn" tabindex="39" />
										<asp:TextBox runat="server" style="display:none" id="locationtypecode_cid" cancelledvalue="" RMXRef="Instance/Event/LocationTypeCode/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_stateid">
		                            <span class="label">State:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="stateid" tabindex="27" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/StateId"/>
										<asp:Button runat="server" class="CodeLookupControl" id="stateidbtn" tabindex="28" />
										<asp:TextBox runat="server" style="display:none" id="stateid_cid" cancelledvalue="" RMXRef="Instance/Event/StateId/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_causecode">
		                            <span class="label">Cause Code:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" onchange="lookupTextChanged(this);" id="causecode" tabindex="40" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/CauseCode"/>
										<asp:Button runat="server" class="CodeLookupControl" id="causecodebtn" tabindex="41" />
										<asp:TextBox runat="server" style="display:none" id="causecode_cid" cancelledvalue="" RMXRef="Instance/Event/CauseCode/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_zipcode">
		                            <span class="label">Zip:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="zipcode" tabindex="29" RMXRef="Instance/Event/ZipCode"/>
									</span>
								</div>
								<div class="half" id="div_noofinjuries">
		                            <span class="label">Number of Injuries:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="noofinjuries" tabindex="42" min="0" onchange="setDataChanged(true);" RMXRef="Instance/Event/NoOfInjuries"/>
									</span>
								</div>
								<div class="half" id="div_countyofinjury">
		                            <span class="label">County of Injury:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" size="30" tabindex="30" id="countyofinjury" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/CountyOfInjury"/>
									</span>
								</div>
								<div class="half" id="div_nooffatalities">
		                            <span class="label">Number Of Fatalities:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" value="0" size="30" onblur="numLostFocus(this);" id="nooffatalities" tabindex="43" min="0" onchange="setDataChanged(true);" RMXRef="Instance/Event/NoOfFatalities"/>
									</span>
								</div>
								<div class="half" id="div_countrycode">
		                            <span class="label">Country:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" value=" " size="30" onchange="lookupTextChanged(this);" id="countrycode" tabindex="31" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/CountryCode"/>
										<asp:Button runat="server" class="CodeLookupControl" id="countrycodebtn" tabindex="32" />
										<asp:TextBox runat="server" value="0" style="display:none" id="countrycode_cid" cancelledvalue="" RMXRef="Instance/Event/CountryCode/@codeid"/>
									</span>
								</div>
							</div>
							<div id="FORMTABrepinfo" style="display:none;">
								<div class="half" id="div_repentityid">
									<asp:TextBox runat="server" value="0" style="display:none" id="repentityid" RMXRef="Instance/Event/RptdByEid/@codeid|Instance/Event/ReporterEntity/EntityId"/>
									<span class="label">Reporter's Last Name:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onchange="lookupTextChanged(this);" tabindex="44" onblur="&#xA;lookupLostFocus(this);&#xA;" id="replastname" RMXRef="Instance/Event/ReporterEntity/LastName"/>
										<input style="display:none" value="1" id="replastname_creatable"/>
										<input type="button" class="button" tabindex="45" id="replastnamebtn" value="..."/>
									</span>
								</div>
								<div class="half" id="div_repphone1">
		                            <span class="label">Office Phone:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="repphone1" tabindex="56" RMXRef="Instance/Event/ReporterEntity/Phone1"/>
									</span>
								</div>
								<div class="half" id="div_repfirstname">
		                            <span class="label">First Name:</span>
		                            <span class="formw">
										<asp:TextBox runat="server" value="" size="30" tabindex="46" id="repfirstname" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/ReporterEntity/FirstName"/>
									</span>
							    </div>
							    <div class="half" id="div_repphone2">
	                                <span class="label">Home Phone:</span>
	                                <span class="formw">
									    <asp:TextBox runat="server" value="" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="repphone2" tabindex="57" RMXRef="Instance/Event/ReporterEntity/Phone2"/>
								    </span>
							    </div>
							    <div class="half" id="div_repaddr1">
	                                <span class="label">Address 1:</span>
	                                <span class="formw">
										    <asp:TextBox runat="server" value="" size="30" tabindex="47" id="repaddr1" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/ReporterEntity/Addr1"/>
								    </span>
							    </div>
							    <div class="half" id="div_repfaxnumber">
	                                <span class="label">Fax:</span>
	                                <span class="formw">
									    <asp:TextBox runat="server" value="" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="repfaxnumber" tabindex="58" RMXRef="Instance/Event/ReporterEntity/FaxNumber"/>
								    </span>
							    </div>
							    <div class="half" id="div_repaddr2">
	                                <span class="label">Address 2:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" tabindex="48" id="repaddr2" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/ReporterEntity/Addr2"/>
									</span>
							    </div>
							    <div class="half" id="div_repemailaddress">
	                                <span class="label">EMail:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" tabindex="59" id="repemailaddress" onchange="&#xA;;setDataChanged(true);&#xA;" maxlength="100" RMXRef="/Instance/Event/ReporterEntity/EmailAddress"/>
								    </span>
							    </div>
							    <div class="half" id="div_repcity">
	                                <span class="label">City:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" tabindex="49" id="repcity" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="Instance/Event/ReporterEntity/City"/>
									</span>
							    </div>
							    <div class="half" id="div_repstateid">
	                                <span class="label">State:</span>
	                                <span class="formw">
									    <asp:TextBox runat="server" value=" " size="30" onchange="lookupTextChanged(this);" id="repstateid" tabindex="50" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/ReporterEntity/StateId"/>
									    <asp:Button runat="server" class="CodeLookupControl" id="repstateidbtn" tabindex="51" />
									    <asp:TextBox runat="server" value="0" style="display:none" id="repstateid_cid" cancelledvalue="" RMXRef="Instance/Event/ReporterEntity/StateId/@codeid"/>
								    </span>
							    </div>
							    <div class="half" id="div_repzipcode">
	                                <span class="label">Zip:</span>
	                                <span class="formw">
									    <asp:TextBox runat="server" value="" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="repzipcode" tabindex="52" RMXRef="Instance/Event/ReporterEntity/ZipCode"/>
								    </span>
							    </div>
							    <div class="half" id="div_repcountrycode">
	                                <span class="label">Country:</span>
	                                <span class="formw">
									    <asp:TextBox runat="server" value=" " size="30" onchange="lookupTextChanged(this);" id="repcountrycode" tabindex="53" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="Instance/Event/ReporterEntity/CountryCode"/>
									    <asp:Button runat="server" class="CodeLookupControl" id="repcountrycodebtn" tabindex="54" />
									    <asp:TextBox runat="server" value="0" style="display:none" id="repcountrycode_cid" cancelledvalue="" RMXRef="Instance/Event/ReporterEntity/CountryCode"/>
								</span>
							    </div>
							    <div class="half" id="div_reptaxid">
	                                <span class="label">Soc. Sec No:</span>
	                                <span class="formw">
									    <asp:TextBox runat="server" value="" size="30" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);" id="reptaxid" tabindex="55" RMXRef="Instance/Event/ReporterEntity/TaxId"/>
								    </span>
							    </div>
							</div>
							<div id="FORMTABfollowup" style="display:none;">
								<div class="half" id="div_datetofollowup">
	                                <span class="label">Follow up Date:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="datetofollowup" tabindex="59" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="Instance/Event/DateToFollowUp"/>
										<asp:Button runat="server" class="DateLookupControl" id="datetofollowupbtn" tabindex="60"/>													
									</span>
							    </div>
							    <div class="half" id="div_treatmentgiven">
	                                <span class="label">Treatment Given:</span>
	                                <span class="formw">
										<asp:CheckBox runat="server" appearance="full" tabindex="68" id="treatmentgiven" onchange="ApplyBool(this);" RMXRef="Instance/Event/TreatmentGiven"/>
										<asp:TextBox runat="server" value="" style="display:none" cancelledvalue="" id="treatmentgiven_mirror"/>
									</span>
							    </div>
							    <div class="half" id="div_datephysadvised">
	                                <span class="label">Date Physician Advised:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="datephysadvised" tabindex="61" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="Instance/Event/DatePhysAdvised"/>
										<asp:Button runat="server" class="DateLookupControl" id="datephysadvisedbtn" tabindex="62"/>
									</span>
							    </div>
							    <div class="half" id="div_relsigned">
	                                <span class="label">Release Signed:</span>
	                                <span class="formw">
										<asp:CheckBox runat="server" appearance="full" tabindex="69" id="relsigned" onchange="ApplyBool(this);" RMXRef="Instance/Event/ReleaseSigned"/>
										<asp:TextBox runat="server" value="" style="display:none" cancelledvalue="" id="relsigned_mirror"/>
									</span>
							    </div>
							    <div class="half" id="div_timephysadvised">
	                                <span class="label">Time Physician Advised:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="timeLostFocus(this.id);" onchange="setDataChanged(true);" FormatAs="time" id="timephysadvised" tabindex="63" RMXRef="Instance/Event/TimePhysAdvised"/>
									</span>
							    </div>
							    <div class="half" id="div_deptheadadvised">
	                                <span class="label">Department Head Advised:</span>
	                                <span class="formw">
										<asp:CheckBox runat="server" value="True" appearance="full" tabindex="70" id="deptheadadvised" onchange="ApplyBool(this);" RMXRef="Instance/Event/DeptHeadAdvised"/>
										<asp:TextBox runat="server" value="" style="display:none" cancelledvalue="" id="deptheadadvised_mirror"/>
									</span>
							    </div>
							    <div class="half" id="div_datecarriernotif">
	                                <span class="label">Carrier Notified Date:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="datecarriernotif" tabindex="64" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="Instance/Event/DateCarrierNotif"/>
										<asp:Button runat="server" class="DateLookupControl" id="datecarriernotifbtn" tabindex="65"/>
									</span>
							    </div>
							    <div class="half" id="div_physnotes">
	                                <span class="label">Physician Notes:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" TextMode="MultiLine" cols="30" tabindex="66" rows="5" id="physnotes" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" RMXRef="Instance/Event/PhysNotes"/>
										<asp:Button runat="server" class="button" id="physnotesbtnMemo" tabindex="67" value="..."/>
									</span>
							    </div>
							    <div class="half" id="div_actionslist">
	                                <span class="label">Actions:</span>
	                                <span class="formw">
										<asp:ListBox runat="server" Rows="3" id="actionslist" style="" tabindex="71" RMXRef="/Instance/Event/EventXAction">
										    <asp:ListItem Value="0">None Selected</asp:ListItem>
										</asp:ListBox>
										<asp:Button runat="server" class="CodeLookupControl" id="actionslistbtn" tabindex="72" />
										<asp:Button runat="server" class="button" id="actionslistbtndel" tabindex="73" value="-"/>
										<asp:TextBox runat="server" value="" style="display:none" id="actionslist_lst" RMXRef="/Instance/Event/EventXAction/@codeid"/>
									</span>
							    </div>
							    <div class="half" id="div_outcomelist">
	                                <span class="label">Outcome:</span>
	                                <span class="formw">
										<asp:ListBox runat="server" Rows="3" id="outcomelist" style="" tabindex="74" RMXRef="/Instance/Event/EventXOutcome">
											<asp:ListItem value="0">None Selected</asp:ListItem>
										</asp:ListBox>
										<asp:Button runat="server" class="CodeLookupControl" id="outcomelistbtn" tabindex="75" />
										<asp:Button runat="server" class="button" id="outcomelistbtndel" tabindex="76" value="-"/>
										<asp:TextBox runat="server" value="" style="display:none" id="outcomelist_lst" RMXRef="/Instance/Event/EventXOutcome/@codeid"/>
									</span>
							    </div>
							    <div class="half" id="div_RequireIntervention">
	                                <span class="label">Intervention Requested/Required:</span>
	                                <span class="formw">
										<asp:CheckBox runat="server" value="True" appearance="full" RMXRef="Instance/Event/RequireIntervention" tabindex="68" id="RequireIntervention" onchange="ApplyBool(this);"/>
										<asp:TextBox runat="server" value="" style="display:none" cancelledvalue="" id="RequireIntervention_mirror"/>
									</span>
							    </div>
							    <div class="half" id="div_btnInterventions">
	                                <span class="formw">
										<input type="submit" name="$action^setvalue%26node-ids%2648%26content%26formbutton&amp;setvalue%26node-ids%2665%26content%26eventintervention&amp;setvalue%26node-ids%2686%26content%26%2FInstance%2FEvent%2FEventNumber%20%7C%20%2FInstance%2FEvent%2FEventId&amp;setvalue%26node-ids%2658%26content%261" value="Intervention (0)&#xA;" class="button" onClick=" ; return XFormHandler('SysFormName=eventintervention&amp;SysEx=/Instance/Event/EventNumber | /Instance/Event/EventId&amp;SysCmd=1','','button')" id="btnInterventions" tabindex="257"/>
									</span>
								</div>
							</div>
							<div id="FORMTABsuppgroup" style="display:none;">
								<asp:TextBox runat="server" value="0" style="display:none" id="supp_event_id" RMXRef="/Instance/Event/Supplementals/EVENT_ID"/>
								<div class="half" id="div_supp_date_admin_date">
	                                <span class="label">Date of Admission:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="supp_date_admin_date" tabindex="2647" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="/Instance/Event/Supplementals/DATE_ADMIN_DATE"/>
										<asp:Button runat="server" class="DateLookupControl" id="supp_date_admin_datebtn" tabindex="2648"/>
									</span>
									<asp:HiddenField runat="server" id="supp_date_admin_date_GroupAssoc" value="EVENT.STATE_ID:|1|"/>
								</div>
								<div class="half" id="div_supp_pat_res_eid">
	                                <span class="label">Patient/Resident:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="supp_pat_res_eid" tabindex="2648" cancelledvalue="" RMXRef="/Instance/Event/Supplementals/PAT_RES_EID"/>
										<asp:Button runat="server" class="button" value="..." tabindex="2649" id="supp_pat_res_eidbtn" />
										<asp:TextBox runat="server" value="0" style="display:none" id="supp_pat_res_eid_cid" cancelledvalue="" RMXRef="/Instance/Event/Supplementals/PAT_RES_EID/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_supp_mas_diag_code">
	                                <span class="label">Diagnosis:</span>
									<span class="formw">
										<asp:TextBox runat="server" value=" " size="30" onchange="lookupTextChanged(this);" id="supp_mas_diag_code" tabindex="2649" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="/Instance/Event/Supplementals/MAS_DIAG_CODE"/>
										<asp:Button runat="server" class="CodeLookupControl" id="supp_mas_diag_codebtn" tabindex="2650" />
										<asp:TextBox runat="server" value="0" style="display:none" id="supp_mas_diag_code_cid" cancelledvalue="" RMXRef="/Instance/Event/Supplementals/MAS_DIAG_CODE/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_supp_cond_before_code">
	                                <span class="label">Condition Before Occurrence:</span>
									<span class="formw">
										<asp:TextBox runat="server" value=" " size="30" onchange="lookupTextChanged(this);" id="supp_cond_before_code" tabindex="2650" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="/Instance/Event/Supplementals/COND_BEFORE_CODE"/>
										<asp:Button runat="server" class="CodeLookupControl" id="supp_cond_before_codebtn" tabindex="2651" />
										<asp:TextBox runat="server" value="0" style="display:none" id="supp_cond_before_code_cid" cancelledvalue="" RMXRef="/Instance/Event/Supplementals/COND_BEFORE_CODE/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_supp_body_part_code">
	                                <span class="label">Body Part Injured:</span>
									<span class="formw">
										<asp:TextBox runat="server" value=" " size="30" onchange="lookupTextChanged(this);" id="supp_body_part_code" tabindex="2651" cancelledvalue="" onblur="&#xA;codeLostFocus(this.id);&#xA;" RMXRef="/Instance/Event/Supplementals/BODY_PART_CODE"/>
										<asp:Button runat="server" class="CodeLookupControl" id="supp_body_part_codebtn" tabindex="2652" />
										<asp:TextBox runat="server" value="0" style="display:none" id="supp_body_part_code_cid" cancelledvalue="" RMXRef="/Instance/Event/Supplementals/BODY_PART_CODE/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_supp_att_phys_eid">
	                                <span class="label">Attending Physician:</span>
									<span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="codeLostFocus(this.id);" onchange="lookupTextChanged(this);" id="supp_att_phys_eid" tabindex="2652" cancelledvalue="" RMXRef="/Instance/Event/Supplementals/ATT_PHYS_EID"/>
										<asp:Button runat="server" class="button" value="..." tabindex="2653" id="supp_att_phys_eidbtn" />
										<asp:TextBox runat="server" value="0" style="display:none" id="supp_att_phys_eid_cid" cancelledvalue="" RMXRef="/Instance/Event/Supplementals/ATT_PHYS_EID/@codeid"/>
									</span>
								</div>
								<div class="half" id="div_supp_freetext_memo">
	                                <span class="label">free text3:</span>
									<span class="formw">
										<asp:TextBox runat="server" TextMode="MultiLine" cols="30" rows="5" id="supp_freetext_memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" tabindex="2653" RMXRef="/Instance/Event/Supplementals/FREETEXT_MEMO"/>
										<asp:Button runat="server" class="button" value="..." id="supp_freetext_memobtnMemo" tabindex="2654"/>
									</span>
								</div>
								<div class="half" id="div_supp_testamt_amt">
	                                <span class="label">testAmt:</span>
									<span class="formw">
										<asp:TextBox runat="server" value="$0.00" size="30" FormatAs="currency" id="supp_testamt_amt" tabindex="3237" onblur="&#xA;;currencyLostFocus(this);&#xA;" onchange="&#xA;;setDataChanged(true);&#xA;" RMXRef="/Instance/Event/Supplementals/TESTAMT_AMT"/>
									</span>
								</div>
								
								<div class="half" id="div_supp_opensup_text">
	                                <span class="label">openSup:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="supp_opensup_text" tabindex="2647" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="/Instance/Event/Supplementals/OPENSUP_TEXT"/>
									</span>
									<asp:HiddenField runat="server" id="supp_opensup_text_GroupAssoc" value="EVENT.EVENT_STATUS_CODE:|12|"/>
								</div>
								
	                            <div class="half" id="div_supp_closesup_text">
	                                <span class="label">closeSup:</span>
	                                <span class="formw">
										<asp:TextBox runat="server" value="" size="30" onblur="dateLostFocus(this.id);&#xA;" FormatAs="date" id="supp_closesup_text" tabindex="2647" onchange="&#xA;setDataChanged(true);&#xA;" RMXRef="/Instance/Event/Supplementals/CLOSESUP_TEXT"/>
									</span>
									<asp:HiddenField runat="server" id="supp_closesup_text_GroupAssoc" value="EVENT.EVENT_STATUS_CODE:|13|"/>
								</div>
								
								<div class="row" id="div_supp_gridtest_grid">
								    <asp:TextBox runat="server" style="display:none" id="supp_gridtest_grid" RMXRef="/Instance/Event/Supplementals/GRIDTEST_GRID" />
								    <span class="label">gridTest:</span>
	                                <span class="formw">
										<script type="text/javascript">
                                            var id = new String("");
                                            var gridXML = new String("");
                                            var gridvalue = new String("");
                                            var minRows = new String("");
                                            var maxRows = new String("");
                                            var title = new String("");
                                            id = id + 'div_supp_gridtest_grid';
                                            gridXML = gridXML + '<!--<?xml version="1.0"?>--><grid><table style="border: solid black 1px"><fields><field><title>column1</title></field><field><title>column2</title></field><field><title>column3</title></field></fields><rows><row></row></rows></table></grid>';
                                            gridvalue = gridvalue + 'supp_gridtest_grid';
                                            minRows = minRows + '1';
                                            maxRows = maxRows + '4';
                                            title = title + 'gridTest';
                                            ZapatecGridDisplay(id,gridXML,gridvalue,minRows,maxRows,title);
                                        </script>
									</span>
									<input type="button" onclick="InsertRow('supp_gridtest_grid')" value="..." id="supp_gridtest_grid_addButton" />
                                    <input type="button" onclick="DeleteRow('supp_gridtest_grid')" value="-" id="supp_gridtest_grid_delButton" />
								</div>							
							</div>
						</div>
						<table>
							<tr>
								<td>
									<asp:Button runat="server" Text="Persons Involved (0)" class="button" onClick2=" ; return XFormHandler('SysFormName=personinvolvedlist&amp;SysCmd=1&amp;SysViewType=controlsonly&amp;SysEx=/Instance/Event/EventId | /Instance/Event/EventNumber | /Instance/Event/DeptEid','','')" id="btnPI" RMXRef="Instance/Event/PiList/@committedcount"/>
									<asp:Button runat="server" Text="OSHA" class="button" onClick2=" ; return XFormHandler('SysFormName=osha&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=eventid&amp;SysEx=/Instance/Event/EventId | /Instance/Claim/ClaimNumber','','')" id="btnOSHA"/>
									<asp:Button runat="server" Text="Dated Text (0)" class="button" onClick2=" ; return XFormHandler('SysFormName=eventdatedtext&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=evdtrowid&amp;SysEx=/Instance/Event/EventId | /Instance/Event/EventNumber','','')" id="btnDatedText" RMXRef="Instance/Event/EventXDatedTextList/@committedcount"/>
									<asp:Button runat="server" Text="Claims (0)" class="button" onClick2=" ; return XFormHandler('SysFormName=claimlist&amp;SysCmd=1&amp;SysViewType=controlsonly&amp;SysFormIdName=eventid&amp;SysEx=/Instance/Event/EventId | /Instance/Event/EventNumber','','')" id="btnClaim" RMXRef="Instance/Event/ClaimList/@committedcount"/>
								    <asp:Button runat="server" Text="Fall Info" class="button" id="btnFallInfo" onClick2=" ; return XFormHandler('SysFormName=fallinfo&amp;SysEx=/Instance/Event/EventNumber | /Instance/Event/EventId&amp;SysCmd=1','','')" Visible="true"/>
	                                <asp:Button runat="server" Text="MED Watch" class="button" id="btnMedWatch" onClick2=" ; return XFormHandler('SysFormName=medwatch&amp;SysEx=/Instance/Event/EventNumber | /Instance/Event/EventId&amp;SysCmd=1,'','')" Visible="true"/>
								</td>
							</tr>
						</table>
						<input type="hidden" id="hdSaveButtonClicked" />
						<asp:TextBox runat="server" id="SysInvisible" style="display:none" />
						<asp:TextBox runat="server" id="SysLookupClass" style="display:none"/>
						<asp:TextBox runat="server" id="SysLookupRecordId" style="display:none"/>
						<asp:TextBox runat="server" id="SysLookupAttachNodePath" style="display:none"/>
						<asp:TextBox runat="server" id="SysLookupResultConfig" style="display:none"/>
						<asp:TextBox runat="server" id="SysRequired" style="display:none" Text="eventnumber|datereported|eventtypecode_cid|timereported|dateofevent|timeofevent|eventstatuscode_cid|depteid_cid|"/>
						<asp:TextBox runat="server" id="SysFocusFields" style="display:none" Text="eventnumber|locationareadesc|replastname|datetofollowup|"/>
						<asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
						<asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
						<asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
						<asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
						<asp:TextBox runat="server" id="SysClassName" style="display:none" Text="Event" RMXRef="Instance/UI/FormVariables/SysClassName"/>
						<asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;Event&gt;&lt;EventXDatedTextList&gt;&lt;EventXDatedText/&gt;&lt;/EventXDatedTextList&gt;&lt;ReporterEntity/&gt;&lt;EventQM/&gt;&lt;EventMedwatch/&gt;&lt;EventXAction/&gt;&lt;EventXOutcome/&gt;&lt;Supplementals/&gt;&lt;/Event&gt;" RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>
						<asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
						<asp:TextBox runat="server" id="SysFormPIdName" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPIdName"/>
						<asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
						<asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
						<asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
						<asp:TextBox runat="server" id="SysNotReqNew" style="display:none" Text="eventnumber|timeofevent|timereported"/>
						<asp:TextBox runat="server" id="SysFormName" style="display:none" Text="event" RMXRef="Instance/UI/FormVariables/SysFormName"/>
						<asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="eventid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
						<asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
						<asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
						<asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
						<asp:TextBox runat="server" id="OH_FACILITY_EID" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/OH_FACILITY_EID"/>
						<asp:TextBox runat="server" id="OH_LOCATION_EID" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/OH_LOCATION_EID"/>
						<asp:TextBox runat="server" id="OH_DIVISION_EID" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/OH_DIVISION_EID"/>
						<asp:TextBox runat="server" id="OH_REGION_EID" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/OH_REGION_EID"/>
						<asp:TextBox runat="server" id="OH_OPERATION_EID" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/OH_OPERATION_EID"/>
						<asp:TextBox runat="server" id="OH_COMPANY_EID" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/OH_COMPANY_EID"/>
						<asp:TextBox runat="server" id="OH_CLIENT_EID" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/OH_CLIENT_EID"/>
						<asp:TextBox runat="server" id="containsopendiaries" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/ContainsOpenDiaries"/>
						<asp:TextBox runat="server" id="statusdiaryclose" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/CloseDiary"/>
						<asp:TextBox runat="server" id="AllCodes" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysExData/AllCodes"/>
						<asp:TextBox runat="server" id="SysKilledNodes" style="display:none" Text="0" RMXRef="Instance/UI/FormVariables/SysKilledNodes"/>
					</td>
				</tr>
			</table>
			<input type="hidden" value="rmx-widget-handle-2" id="SysWindowId"/>
			<asp:ScriptManager ID="ScriptManager1" runat="server" />
    </form>
</body>
</html>
