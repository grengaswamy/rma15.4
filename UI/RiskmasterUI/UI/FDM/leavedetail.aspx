<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="leavedetail.aspx.cs"  Inherits="Riskmaster.UI.FDM.Leavedetail" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Leave Detail</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script language="JavaScript" src="../../Scripts/supportscreens.js">{var i;}</script>
    <script language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Leave Detail" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSleavedetail" id="TABSleavedetail">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="leavedetail" id="LINKTABSleavedetail">Leave Detail</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABleavedetail" id="FORMTABleavedetail">
        <asp:TextBox style="display:none" runat="server" id="LDRowId" RMXRef="//LDRowId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="LeaveRowId" RMXRef="//LeaveRowId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" />
        <div runat="server" class="full" id="div_DateLeaveStart" xmlns="">
          <asp:label runat="server" class="label" id="lbl_DateLeaveStart" Text="Leave Date Range(From)" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="DateLeaveStart" RMXRef="//DateLeaveStart" RMXType="date" tabindex="1" />
            <cc1:CalendarExtender runat="server" id="DateLeaveStart_ajax" TargetControlID="DateLeaveStart" OnClientDateSelectionChanged="setDataChangedTrue" />
          </span>
        </div>
        <div runat="server" class="full" id="div_DateLeaveEnd" xmlns="">
          <asp:label runat="server" class="label" id="lbl_DateLeaveEnd" Text="To" />
          <span class="formw">
            <asp:TextBox runat="server" FormatAs="date" id="DateLeaveEnd" RMXRef="//DateLeaveEnd" RMXType="date" tabindex="2" />
            <cc1:CalendarExtender runat="server" id="DateLeaveEnd_ajax" TargetControlID="DateLeaveEnd" OnClientDateSelectionChanged="setDataChangedTrue" />
          </span>
        </div>
        <div runat="server" class="full" id="div_LeaveTypeCode" xmlns="">
          <asp:label runat="server" class="label" id="lbl_LeaveTypeCode" Text="Leave Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="LeaveTypeCode" CodeTable="LEAVE_TYPE" ControlName="LeaveTypeCode" RMXRef="//LeaveTypeCode" RMXType="code" tabindex="3" />
          </span>
        </div>
        <div runat="server" class="full" id="div_HoursLeave" xmlns="">
          <asp:label runat="server" class="label" id="lbl_HoursLeave" Text="Hours" />
          <span class="formw">
            <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="HoursLeave" RMXRef="//HoursLeave" tabindex="5" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" width="75px" onClientClick="return LeaveDetail_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return LeaveDetail_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" name="formname" value="leavedetail" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" value="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>