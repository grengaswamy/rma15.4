<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Leaveplan.aspx.cs" Inherits="Riskmaster.UI.FDM.Leaveplan"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Leave Plan Management</title>
    <link rel="stylesheet" href="Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="Scripts/form.js">{var i;}
    </script>

    <script language="JavaScript" src="Scripts/drift.js">{var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" /><asp:hiddenfield
        runat="server" id="wsrp_rewrite_action_1" value="" /><asp:textbox style="display: none"
            runat="server" name="hTabName" id="hTabName" /><asp:scriptmanager id="SMgr" runat="server" /><div
                id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
                <div class="toolBarButton" runat="server" id="div_new" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNew" src="Images/new.gif" width="28"
                        height="28" border="0" id="new" alternatetext="New" onmouseover="this.src='Images/new2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/new.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                        src="Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                        validationgroup="vgSave" onmouseover="this.src='Images/save2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/save.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateFirst" src="Images/first.gif" width="28"
                        height="28" border="0" id="movefirst" alternatetext="Move First" onmouseover="this.src='Images/first2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/first.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigatePrev" src="Images/prev.gif" width="28"
                        height="28" border="0" id="moveprevious" alternatetext="Move Previous" onmouseover="this.src='Images/prev2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/prev.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateNext" src="Images/next.gif" width="28"
                        height="28" border="0" id="movenext" alternatetext="Move Next" onmouseover="this.src='Images/next2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/next.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateLast" src="Images/last.gif" width="28"
                        height="28" border="0" id="movelast" alternatetext="Move Last" onmouseover="this.src='Images/last2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/last.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
                    <asp:imagebutton runat="server" onclick="NavigateDelete" src="Images/delete.gif"
                        width="28" height="28" border="0" id="delete" alternatetext="Delete Record" onmouseover="this.src='Images/delete2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/delete.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doLookup()" src="Images/lookup.gif"
                        width="28" height="28" border="0" id="lookup" alternatetext="Lookup Data" onmouseover="this.src='Images/lookup2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/lookup.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="attach()" src="Images/attach.gif"
                        width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                        onmouseover="this.src='Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/attach.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_search" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="doSearch()" src="Images/search.gif"
                        width="28" height="28" border="0" id="search" alternatetext="Search" onmouseover="this.src='Images/search2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/search.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Diary()" src="Images/diary.gif" width="28"
                        height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='Images/diary2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/diary.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_comments" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="Comments()" src="Images/comments.gif"
                        width="28" height="28" border="0" id="comments" alternatetext="Comments" onmouseover="this.src='Images/comments2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/comments.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_mailmerge" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="MailMerge()" src="Images/mailmerge.gif"
                        width="28" height="28" border="0" id="mailmerge" alternatetext="Mail Merge" onmouseover="this.src='Images/mailmerge2.gif';this.style.zoom='110%'"
                        onmouseout="this.src='Images/mailmerge.gif';this.style.zoom='100%'" /></div>
                <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
                    <asp:imagebutton runat="server" onclientclick="recordSummary()" src="Images/recordsummary.gif"
                        width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                        onmouseover="this.src='Images/recordsummary2.gif';this.style.zoom='110%'" onmouseout="this.src='Images/recordsummary.gif';this.style.zoom='100%'" /></div>
            </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Leave Plan Management" /><asp:label
            id="formsubtitle" runat="server" text="" /></div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" /></div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSleaveplan" id="TABSleaveplan">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="leaveplan"
                id="LINKTABSleaveplan"><span style="text-decoration: none">Plan Information</span></a></div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSeligibilityinfo"
            id="TABSeligibilityinfo">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="eligibilityinfo"
                id="LINKTABSeligibilityinfo"><span style="text-decoration: none">Eligibility & Update</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span></a></div>
        <div class="tabSpace">
            &nbsp;&nbsp;</div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABleaveplan" id="FORMTABleaveplan">
        <asp:textbox style="display: none" runat="server" id="lprowid" rmxref="/Instance/LeavePlan/LPRowId"
            rmxtype="id" /><div runat="server" class="half" id="div_plancode" xmlns="">
                <span class="required">Plan Code</span><span class="formw"><asp:textbox runat="server"
                    onchange="setDataChanged(true);" id="plancode" rmxref="/Instance/LeavePlan/LeavePlanCode"
                    rmxtype="text" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                        runat="server" controltovalidate="plancode" /></span></div>
        <div runat="server" class="half" id="div_effectivedate" xmlns="">
            <span class="required">Effective Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="effectivedate" rmxref="/Instance/LeavePlan/EffectiveDate"
                rmxtype="date" tabindex="16" /><asp:requiredfieldvalidator validationgroup="vgSave"
                    errormessage="Required" runat="server" controltovalidate="effectivedate" /><cc1:CalendarExtender
                        runat="server" ID="effectivedate_ajax" TargetControlID="effectivedate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_planname" xmlns="">
            <span class="required">Plan Name</span><span class="formw"><asp:textbox runat="server"
                onchange="setDataChanged(true);" id="planname" rmxref="/Instance/LeavePlan/LeavePlanName"
                rmxtype="text" /><asp:requiredfieldvalidator validationgroup="vgSave" errormessage="Required"
                    runat="server" controltovalidate="planname" /></span></div>
        <div runat="server" class="half" id="div_expirationdate" xmlns="">
            <span class="required">Expiration Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="expirationdate" rmxref="/Instance/LeavePlan/ExpirationDate"
                rmxtype="date" tabindex="18" /><asp:requiredfieldvalidator validationgroup="vgSave"
                    errormessage="Required" runat="server" controltovalidate="expirationdate" /><cc1:CalendarExtender
                        runat="server" ID="expirationdate_ajax" TargetControlID="expirationdate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_plantypecode" xmlns="">
            <span class="label">Plan Type</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="plantypecode" rmxref="/Instance/LeavePlan/LPTypeCode"
                rmxtype="code" cancelledvalue="" tabindex="3" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="plantypecodebtn" onclientclick="return selectCode('LEAVE_PLAN_TYPE','plantypecode');"
                    tabindex="4" /><asp:textbox style="display: none" runat="server" id="plantypecode_cid"
                        rmxref="/Instance/LeavePlan/LPTypeCode/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_issuedate" xmlns="">
            <span class="label">Issue Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="issuedate" rmxref="/Instance/LeavePlan/IssueDate"
                rmxtype="date" tabindex="20" /><cc1:CalendarExtender runat="server" ID="issuedate_ajax"
                    TargetControlID="issuedate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_plandesc" xmlns="">
            <span class="label">Plan Description</span><span class="formw"><asp:textbox runat="server"
                onchange="setDataChanged(true);" id="plandesc" rmxref="/Instance/LeavePlan/LeavePlanDesc"
                rmxtype="text" /></span></div>
        <div runat="server" class="half" id="div_reviewdate" xmlns="">
            <span class="label">Review Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="reviewdate" rmxref="/Instance/LeavePlan/ReviewDate"
                rmxtype="date" tabindex="22" /><cc1:CalendarExtender runat="server" ID="reviewdate_ajax"
                    TargetControlID="reviewdate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_defaultplan" xmlns="">
            <span class="label">Default Plan for All Employees</span><span class="formw"><asp:checkbox
                runat="server" onchange="setDataChanged(true);" id="defaultplan" rmxref="/Instance/LeavePlan/DefaultFlag"
                rmxtype="checkbox" onclick="DisableLeavePlanList()" tabindex="6" /></span></div>
        <div runat="server" class="half" id="div_renewaldate" xmlns="">
            <span class="label">Renewal Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="renewaldate" rmxref="/Instance/LeavePlan/RenewalDate"
                rmxtype="date" tabindex="24" /><cc1:CalendarExtender runat="server" ID="renewaldate_ajax"
                    TargetControlID="renewaldate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_insuredlist" xmlns="">
            <span class="label">Leave Plan Available to</span><span class="formw"><asp:listbox
                runat="server" size="3" id="insuredlist" rmxref="/Instance/LeavePlan/LeavePlanAvailabilityList"
                rmxtype="entitylist" style="" tabindex="7" /><input type="button" id="insuredlistbtn"
                    tabindex="8" onclick="lookupData('insuredlist','ORGANIZATIONS',4,'insuredlist',3)"
                    class=" button" value="..." /><input type="button" class="button" id="insuredlistbtndel"
                        tabindex="9" onclick="deleteSelCode('insuredlist')" value="-" /><asp:textbox runat="server"
                            style="display: none" rmxref="/Instance/LeavePlan/LeavePlanAvailabilityList/@codeid"
                            id="insuredlist_lst" /></span></div>
        <div runat="server" class="half" id="div_canceldate" xmlns="">
            <span class="label">Cancel Date</span><span class="formw"><asp:textbox runat="server"
                formatas="date" onchange="setDataChanged(true);" id="canceldate" rmxref="/Instance/LeavePlan/CancelDate"
                rmxtype="date" tabindex="26" /><cc1:CalendarExtender runat="server" ID="canceldate_ajax"
                    TargetControlID="canceldate" />
            </span>
        </div>
        <div runat="server" class="half" id="div_" xmlns="">
            <span class="label"></span><span class="formw">
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
            </span>
        </div>
        <div runat="server" class="half" id="div_planstatus" xmlns="">
            <span class="required">Plan Status</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="planstatus" rmxref="/Instance/LeavePlan/LPStatusCode"
                rmxtype="code" cancelledvalue="" tabindex="10" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="planstatusbtn" onclientclick="return selectCode('PLAN_STATUS','planstatus');"
                    tabindex="11" /><asp:textbox style="display: none" runat="server" id="planstatus_cid"
                        rmxref="/Instance/LeavePlan/LPStatusCode/@codeid" cancelledvalue="" /><asp:requiredfieldvalidator
                            validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="planstatus" /></span></div>
        <div runat="server" class="half" id="div_defaulthours" xmlns="">
            <span class="required">Default Hours Per Week</span><span class="formw"><asp:textbox
                runat="server" onblur="numLostFocus(this);" id="defaulthours" rmxref="/Instance/LeavePlan/HoursPerWeek"                
                    rmxtype="numeric" tabindex="28" maxlength="3" onchange="validateHoursField('defaulthours');" /><asp:requiredfieldvalidator
                    validationgroup="vgSave" errormessage="Required" runat="server" controltovalidate="defaulthours" /></span></div>
        <div runat="server" class="half" id="div_allowednumber" xmlns="">
            <span class="label">Allowed</span><span class="formw"><asp:textbox runat="server"
                size="30" onblur="numLostFocus(this);" id="allowednumber" rmxref="/Instance/LeavePlan/AllowedNumber"
                tabindex="11" maxlength="5" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_allowedprdtype" xmlns="">
            <span class="label">Allowed Period Type</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="allowedprdtype" rmxref="/Instance/LeavePlan/AllowedPeriodType"
                rmxtype="code" cancelledvalue="" tabindex="12" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="allowedprdtypebtn" onclientclick="return selectCode('DURATION_TYPE','allowedprdtype');"
                    tabindex="13" /><asp:textbox style="display: none" runat="server" id="allowedprdtype_cid"
                        rmxref="/Instance/LeavePlan/AllowedPeriodType/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_pernumber" xmlns="">
            <span class="label">Per</span><span class="formw"><asp:textbox runat="server" size="30"
                onblur="numLostFocus(this);" id="pernumber" rmxref="/Instance/LeavePlan/PerNumber"
                tabindex="13" maxlength="5" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_perprdtype" xmlns="">
            <span class="label">Per Period Type</span><span class="formw"><asp:textbox runat="server"
                onchange="lookupTextChanged(this);" id="perprdtype" rmxref="/Instance/LeavePlan/PerPeriodType"
                rmxtype="code" cancelledvalue="" tabindex="14" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="perprdtypebtn" onclientclick="return selectCode('DURATION_TYPE','perprdtype');"
                    tabindex="15" /><asp:textbox style="display: none" runat="server" id="perprdtype_cid"
                        rmxref="/Instance/LeavePlan/PerPeriodType/@codeid" cancelledvalue="" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABeligibilityinfo"
        id="FORMTABeligibilityinfo">
        <div runat="server" class="half" id="div_hoursworked" xmlns="">
            <span class="label">Hours Must Have Worked in Given Timeframe</span><span class="formw"><asp:textbox
                runat="server" onblur="numLostFocus(this);" id="hoursworked" rmxref="/Instance/LeavePlan/EligHours"
                rmxtype="numeric" tabindex="29" maxlength="8" onchange="setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_eligiblenumber" xmlns="">
            <span class="label">Eligible Duration for Leave from Date Hired</span><span class="formw"><asp:textbox
                runat="server" size="30" onblur="numLostFocus(this);" id="eligiblenumber" rmxref="/Instance/LeavePlan/EligBeneNumber"
                tabindex="30" maxlength="5" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" /></span></div>
        <div runat="server" class="half" id="div_nextmonth" xmlns="">
            <span class="label">or First of Next Month</span><span class="formw"><asp:checkbox
                runat="server" onchange="setDataChanged(true);" id="nextmonth" rmxref="/Instance/LeavePlan/BeneFirstFlag"
                rmxtype="checkbox" tabindex="31" /></span></div>
        <div runat="server" class="half" id="div_eligibleunits" xmlns="">
            <span class="label">Eligible Duration Type</span><span class="formw"><asp:textbox
                runat="server" onchange="lookupTextChanged(this);" id="eligibleunits" rmxref="/Instance/LeavePlan/BenePeriodType"
                rmxtype="code" cancelledvalue="" tabindex="32" onblur="codeLostFocus(this.id);" /><asp:button
                    class="CodeLookupControl" runat="Server" id="eligibleunitsbtn" onclientclick="return selectCode('DURATION_TYPE','eligibleunits');"
                    tabindex="33" /><asp:textbox style="display: none" runat="server" id="eligibleunits_cid"
                        rmxref="/Instance/LeavePlan/BenePeriodType/@codeid" cancelledvalue="" /></span></div>
        <div runat="server" class="half" id="div_recclaimstatus" xmlns="">
            <span class="label">Limit Leave Records Updates by Claim Status</span><span class="formw"><asp:listbox
                runat="server" size="3" id="recclaimstatus" rmxref="/Instance/LeavePlan/ClaimStatusList"
                rmxtype="codelist" style="" tabindex="33" /><input type="button" id="recclaimstatusbtn"
                    tabindex="34" class=" CodeLookupControl" onclick="selectCode('Claim_Status','recclaimstatus')" /><input
                        type="button" class="button" id="recclaimstatusbtndel" tabindex="35" onclick="deleteSelCode('recclaimstatus')"
                        value="-" /><asp:textbox runat="server" style="display: none" rmxref="/Instance/LeavePlan/ClaimStatusList/@codeid"
                            id="recclaimstatus_lst" /></span></div>
        <div runat="server" class="half" id="div_recleavestatus" xmlns="">
            <span class="label">Limit Leave Records Updates by Leave Status</span><span class="formw"><asp:listbox
                runat="server" size="3" id="recleavestatus" rmxref="/Instance/LeavePlan/LeaveStatusList"
                rmxtype="codelist" style="" tabindex="36" /><input type="button" id="recleavestatusbtn"
                    tabindex="37" class=" CodeLookupControl" onclick="selectCode('Leave_Status','recleavestatus')" /><input
                        type="button" class="button" id="recleavestatusbtndel" tabindex="38" onclick="deleteSelCode('recleavestatus')"
                        value="-" /><asp:textbox runat="server" style="display: none" rmxref="/Instance/LeavePlan/LeaveStatusList/@codeid"
                            id="recleavestatus_lst" /></span></div>
        <div runat="server" class="half" id="div_histclaimstatus" xmlns="">
            <span class="label">Limit Leave History Updates by Claim Status</span><span class="formw"><asp:listbox
                runat="server" size="3" id="histclaimstatus" rmxref="/Instance/LeavePlan/ClaimStatusHistList"
                rmxtype="codelist" style="" tabindex="39" /><input type="button" id="histclaimstatusbtn"
                    tabindex="40" class=" CodeLookupControl" onclick="selectCode('Claim_Status','histclaimstatus')" /><input
                        type="button" class="button" id="histclaimstatusbtndel" tabindex="41" onclick="deleteSelCode('histclaimstatus')"
                        value="-" /><asp:textbox runat="server" style="display: none" rmxref="/Instance/LeavePlan/ClaimStatusHistList/@codeid"
                            id="histclaimstatus_lst" /></span></div>
        <div runat="server" class="half" id="div_histleavestatus" xmlns="">
            <span class="label">Limit Leave History Updates by Leave Status</span><span class="formw"><asp:listbox
                runat="server" size="3" id="histleavestatus" rmxref="/Instance/LeavePlan/LeaveStatusHistList"
                rmxtype="codelist" style="" tabindex="42" /><input type="button" id="histleavestatusbtn"
                    tabindex="43" class=" CodeLookupControl" onclick="selectCode('Leave_Status','histleavestatus')" /><input
                        type="button" class="button" id="histleavestatusbtndel" tabindex="44" onclick="deleteSelCode('histleavestatus')"
                        value="-" /><asp:textbox runat="server" style="display: none" rmxref="/Instance/LeavePlan/LeaveStatusHistList/@codeid"
                            id="histleavestatus_lst" /></span></div>
        <div runat="server" class="half" id="div_useleaves" xmlns="">
            <span class="label">Use Existing Leaves to Update Leave History</span><span class="formw"><asp:checkbox
                runat="server" onchange="setDataChanged(true);" id="useleaves" rmxref="/Instance/LeavePlan/UseLeavesFlag"
                rmxtype="checkbox" tabindex="45" /></span></div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_lp_row_id" rmxref="/Instance/*/Supplementals/LP_ROW_ID"
            rmxtype="id" /></div>
    <div id="Div4" class="formButtonGroup" runat="server" />
    <asp:textbox style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormPIdName"
            rmxref="Instance/UI/FormVariables/SysFormPIdName" rmxtype="hidden" text="" /><asp:textbox
                style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysPSid"
                    rmxref="Instance/UI/FormVariables/SysPSid" rmxtype="hidden" text="" /><asp:textbox
                        style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
                        rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysFormName"
                            rmxref="Instance/UI/FormVariables/SysFormName" rmxtype="hidden" text="leaveplan" /><asp:textbox
                                style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
                                rmxtype="hidden" text="lprowid" /><asp:textbox style="display: none" runat="server"
                                    id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId" rmxtype="hidden"
                                    text="" /><asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
                                        rmxtype="hidden" text="24000" /><asp:textbox style="display: none" runat="server"
                                            id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd" rmxtype="hidden" text="" /><asp:textbox
                                                style="display: none" runat="server" id="SysCmdConfirmSave" rmxref="Instance/UI/FormVariables/SysCmdConfirmSave"
                                                rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" id="SysCmdQueue"
                                                    rmxref="Instance/UI/FormVariables/SysCmdQueue" rmxtype="hidden" text="" /><asp:textbox
                                                        style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText"
                                                        rmxtype="hidden" text="Navigate" /><asp:textbox style="display: none" runat="server"
                                                            id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName" rmxtype="hidden"
                                                            text="LeavePlan" /><asp:textbox style="display: none" runat="server" id="SysSerializationConfig"
                                                                rmxref="Instance/UI/FormVariables/SysSerializationConfig" rmxtype="hidden" text="&lt;LeavePlan&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/LeavePlan&gt;" /><asp:textbox
                                                                    style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
                                                                    rmxtype="hidden" text="" /><asp:textbox style="display: none" runat="server" name="formname"
                                                                        value="leaveplan" /><asp:textbox style="display: none" runat="server" name="SysRequired"
                                                                            value="plancode|effectivedate|planname|expirationdate|planstatus_cid|defaulthours|" /><asp:textbox
                                                                                style="display: none" runat="server" name="SysFocusFields" value="plancode|hoursworked|" /><input
                                                                                    type="hidden" id="hdSaveButtonClicked" /><asp:textbox runat="server" id="SysInvisible"
                                                                                        style="display: none" /><asp:textbox runat="server" id="SysLookupClass" style="display: none" /><asp:textbox
                                                                                            runat="server" id="SysLookupRecordId" style="display: none" /><asp:textbox runat="server"
                                                                                                id="SysLookupAttachNodePath" style="display: none" /><asp:textbox runat="server"
                                                                                                    id="SysLookupResultConfig" style="display: none" /><input type="hidden" value="rmx-widget-handle-2"
                                                                                                        id="SysWindowId" /></form>
</body>
</html>
