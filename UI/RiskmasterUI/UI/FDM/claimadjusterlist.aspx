﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="claimadjusterlist.aspx.cs" Inherits="Riskmaster.UI.FDM.ClaimAdjusterList" ValidateRequest="false" EnableViewStateMac="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Adjuster List</title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="Scripts/drift.js"></script>
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
  <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script>
    <script language="JavaScript" type="text/javascript">
    function SetAdjusterId() //Function is added as we need to send the AdjRowId corresponding to current link.
    {
     var strUrl=event.srcElement.href;
     var intLastPos=strUrl.lastIndexOf("SysFormId");
     strUrl=strUrl.substring(intLastPos);
	 var strFormId=strUrl.substring(0,strUrl.indexOf("&"));
	 document.getElementById('adjrowid').value=strFormId.substring(strFormId.indexOf("=")+1);
	}
    </script>
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">
    <div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
    <h2>Lookup results</h2>
    <table border="0" width="95%" cellspacing="0" cellpadding="2" id="tbllist">
    <tr>
    <td class="ctrlgroup"><a href="#"  id="HeaderColumn0" class="ctrlgroup" onclick="SortList(document.all.tbllist,0,'lnkAdjuster',true );">Last Name</a></td>
	<td class="ctrlgroup"><a href="#"  id="HeaderColumn1" class="ctrlgroup" onclick="SortList(document.all.tbllist,1,'lnkAdjuster',true );">First Name</a></td>
	<td class="ctrlgroup" nowrap="1"<a href="#"  id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkAdjuster',true );">Current</a></td>
	<td class="ctrlgroup"/>
    </tr>
    <%
       if (RecordExists.Value.ToLower() == "true")
      {
          int i = 0; foreach (XElement item in result)
          {
              string rowclass = "";
              string strValue = "";
              if ((i % 2) == 1) rowclass = "datatd1";
              else rowclass = "datatd";
              i++;
            %>
            <tr>
               <td class="<%=rowclass%>">  
                <%
                string strFullName = item.Element("AdjusterEid").Value;
                if (strFullName.Contains(","))
                {
                    int intPos = strFullName.IndexOf(",");
                    LastName.Value = strFullName.Substring(0, intPos);
                    FirstName.Value = strFullName.Substring(intPos + 1);
                }
                else
                {
                    LastName.Value = strFullName;
                }
                       %>                        
                        <%lnkAdjuster.Text = LastName.Value;%> 
                           <%
                               lnkAdjuster.ID = "lnkAdjuster" + i;
                               lnkAdjuster.PostBackUrl = "adjustdatedtextlist.aspx?SysFormName=AdjustDatedTextList&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysFormId=" + item.Element("AdjRowId").Value + "&SysFormPIdName=claimid&SysEx=AdjRowId|TextTypeCode|CodeId&SysExMapCtl=AdjRowId|TextTypeCode|CodeId"; 
                            %>
                          <asp:LinkButton runat="server" id="lnkAdjuster" OnClientClick="SetAdjusterId();"></asp:LinkButton> 
                        </td>
                        <td class="<%=rowclass%>">
                            <%=FirstName.Value%>
                        </td>
                        <td class="<%=rowclass%>">
                               <% if (item.Element("CurrentAdjFlag").Value == "False")
                                  strValue = "No";
                              else
                                  strValue = "Yes";
                                %> 
                        <%=strValue%>             
                </td>
                <td class="<%=rowclass%>">                          
                   <%
                        lnkDetails.ID = "lnkDetails" + i;
                        lnkDetails.PostBackUrl = "adjuster.aspx?SysFormName=Adjuster&SysCmd=0&SysViewType=controlsonly&SysFormIdName=adjrowid&SysFormId=" + item.Element("AdjRowId").Value + "&SysFormPIdName=claimid&SysEx=AdjRowId&SysExMapCtl=AdjRowId";
                    %>
                  <asp:LinkButton runat="server" id="lnkDetails">...</asp:LinkButton></td> 
                
                <td style="display:none">                           
                </td>
                <td style="display:none">                           
                   <%=lnkAdjuster.PostBackUrl%>
                </td>    
                <td style="display:none">
                  <%=item.Element("AdjRowId").Value%>
                </td>  
                <td style="display:none">lnkDetails</td>        
                <td style="display:none">
                  <%=lnkDetails.PostBackUrl%>
                </td>                 
           </tr>                          
 <% }
            }%> 
 <%else
   {%>
     <tr>
     <td align="center"><br /><br /><br /><br />
            There are no records to show.
      <br /><br /><br /><br /><br /></td>
    </tr>
     <% }%>
    </table>
    <center><div id="pageNavPosition"></div></center> 
    <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
    <asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
    <asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
    <asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
    <asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
    <asp:TextBox runat="server" id="SysClassName" style="display:none" Text="ClaimAdjusterList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
    <asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;ClaimAdjusterList&gt;&lt;ClaimAdjuster /&gt;&lt;/ClaimAdjusterList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
    <asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
    <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
    <asp:TextBox runat="server" id="SysFormPIdName" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPIdName" Text="claimid"/>
    <asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
    <asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
    <asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
    <asp:TextBox runat="server" id="SysNotReqNew" style="display:none" RMXRef="Instance/UI/FormVariables/SysNotReqNew" />
    <asp:TextBox runat="server" id="SysFormName" style="display:none" Text="claimadjusterlist" RMXRef="Instance/UI/FormVariables/SysFormName" />
    <asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="claimid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
    <asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
    <asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
    <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
    <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
    <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId" style="display:none"/>
    <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" style="display:none"/>
    <asp:TextBox runat="server" id="adjrowid" RMXRef="Instance/UI/FormVariables/SysExData/AdjRowId"   style="display:none"/>
    <asp:TextBox runat="server" id="texttypecode" RMXRef="Instance/UI/FormVariables/SysExData/TextTypeCode"   style="display:none"/>
    <asp:TextBox runat="server" id="codeid" RMXRef="Instance/UI/FormVariables/SysExData/CodeId"   style="display:none"/>                              
    <asp:Button class="button" runat="server" ID="BackToParent" Text="Back to Claim" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>   
    <asp:HiddenField ID="FirstName" runat="server" />
    <asp:HiddenField ID="LastName" runat="server" />
    <asp:HiddenField ID="RecordExists" runat="server" />
    <input type="hidden" id="PageName" value="claimadjusterlist" />
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
    <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>  
</body>
</html>
