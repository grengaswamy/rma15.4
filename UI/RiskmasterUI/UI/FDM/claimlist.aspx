﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="claimlist.aspx.cs" Inherits="Riskmaster.UI.FDM.ClaimList" ValidateRequest="false" EnableViewStateMac="false"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>    
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script> 
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">    
    <div>
     <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
            <h2>Claims for Event: <%=rootElement.XPathSelectElement("//EventNumber").Value%></h2>
                          <table border="0" class="sortable" id="tbllist" width="95%" cellspacing="0" cellpadding="2">
                          <tr>
                            <td class="ctrlgroup" width="50px" />
                            <td class="ctrlgroup"><a href="#" id="HeaderColumn1" class="ctrlgroup" onclick="SortList(document.all.tbllist,1,'lnkClaimNumber',true );">Claim Number</a></td>                                            
                            <td class="ctrlgroup"><a href="#" id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkClaimNumber',true );">Date of Claim</a></td>
                            <td class="ctrlgroup"><a href="#" id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,3,'lnkClaimNumber',true );">Claim Status</a></td>
                            <td class="ctrlgroup"><a href="#" id="HeaderColumn4" class="ctrlgroup" onclick="SortList(document.all.tbllist,4,'lnkClaimNumber',true );">Claimant</a></td>
                            <td class="ctrlgroup"><a href="#" id="HeaderColumn5" class="ctrlgroup" onclick="SortList(document.all.tbllist,5,'lnkClaimNumber',true );">Claim Type</a></td>
                            <td class="ctrlgroup"><a href="#" id="HeaderColumn6" class="ctrlgroup" onclick="SortList(document.all.tbllist,6,'lnkClaimNumber',true );">Date of Event</a></td>                                        
                          </tr>                         
                          <%int i = 0; foreach (XElement item in result)
                            {
                                string rowclass = "";                                
                                if ((i % 2) == 1) rowclass = "datatd1";
                                else rowclass = "datatd";
                                i++;
                                %>
                                
                          <tr>                          
                                <td class="<%=rowclass%>">                             
                            <%--    <table border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                  <td>  --%>
                                  <%                                
                                      PageId.ID = "PageId" + i;
                                      PageId.Value = item.Element("ClaimId").Value;
                                  %>                                                          
                                    <input type ="checkbox" id="PageId" runat="server" name="PageId"  title="Claim List"/></td>                       
                               <%--   </tr>
                                </table>
                              </td> --%>                                             
                          <td class="<%=rowclass%>">                                 
                          <%lnkClaimNumber.Text =item.Element("ClaimNumber").Value;
                            lnkClaimNumber.ID = "lnkClaimNumber" + i ; %>                                                                                      
                          <%                                                                                    
                          if ((item.Element("LineOfBusCode").Value).Contains("GC"))
                          {
                            lnkClaimNumber.PostBackUrl = "claimgc.aspx?SysFormName=claimgc&SysCmd=0&SysViewType=controlsonly&SysFormIdName=claimid&SysFormId=" + item.Element("ClaimId").Value + "&SysFormPIdName=eventid&SysEx=EventId&SysExMapCtl=EventId";                                                                                                                            
                          }
                          else if ((item.Element("LineOfBusCode").Value).Contains("VA"))
                          {
                            lnkClaimNumber.PostBackUrl = "claimva.aspx?SysFormName=claimva&SysCmd=0&SysViewType=controlsonly&SysFormIdName=claimid&SysFormId=" + item.Element("ClaimId").Value + "&SysFormPIdName=eventid&SysEx=EventId&SysExMapCtl=EventId";                                                                                                                                  
                          }
                          else if ((item.Element("LineOfBusCode").Value).Contains("WC"))
                          {
                            lnkClaimNumber.PostBackUrl = "claimwc.aspx?SysFormName=claimwc&SysCmd=0&SysViewType=controlsonly&SysFormIdName=claimid&SysFormId=" + item.Element("ClaimId").Value + "&SysFormPIdName=eventid&SysEx=EventId&SysExMapCtl=EventId";
                          }  
                          else
                          {
                            lnkClaimNumber.PostBackUrl = "claimdi.aspx?SysFormName=claimdi&SysCmd=0&SysViewType=controlsonly&SysFormIdName=claimid&SysFormId=" + item.Element("ClaimId").Value + "&SysFormPIdName=eventid&SysEx=EventId&SysExMapCtl=EventId";
                          }%>
                          <asp:LinkButton runat="server" id="lnkClaimNumber"></asp:LinkButton>    
                        </td>                         
                         <td class="<%=rowclass%>">
                         <%=AppHelper.GetUIDate(item.Element("DateOfClaim").Value)%>
                         </td>
                         <td class="<%=rowclass%>">
                         <%=item.Element("ClaimStatusCode").Value%>
                         </td>
                         <td class="<%=rowclass%>">
                         <%foreach (var ele in item.XPathSelectElements("ClaimantList/Claimant"))
                         {
                            if (ele.Element("PrimaryClmntFlag").Value == "True")
                            {%>
                                <%=ele.Element("ClaimantEid").Value%>
                            <%}
                         }%>
                         </td>
                         <td class="<%=rowclass%>">
                             <%=item.Element("ClaimTypeCode").Value%>
                         </td>                         
                         <td class="<%=rowclass%>">
                            <%=rootElement.XPathSelectElement("//DateofEvent").Value%>
                          </td>                        
                         <td style="display:none">                           
                          </td>
                         <td style="display:none">                           
                           <%=lnkClaimNumber.PostBackUrl%>
                         </td>    
                         <td style="display:none">
                          <%=item.Element("ClaimId").Value%>
                         </td>                                             
                         </tr>                                             
                         <%}%>
                    </table>                                         
                  <center><div id="pageNavPosition"></div></center>   
                        <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
                        <asp:TextBox runat="server" id="TextBox1" style="display:none" />
                        <asp:TextBox runat="server" id="DeleteListTemplate"  style="display:none" Text="Instance/ClaimList/Claim/ClaimId"/>					
						<asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
						<asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
						<asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
						<asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
						<asp:TextBox runat="server" id="SysClassName" style="display:none" Text="ClaimList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
						<asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;ClaimList&gt;&lt;Claim&gt;&lt;ClaimantList&gt;&lt;/ClaimantList&gt;&lt;/Claim&gt;&lt;/ClaimList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
						<asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
                        <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
						<asp:TextBox runat="server" id="SysFormPIdName" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPIdName"/>
						<asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
						<asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
						<asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
						<asp:TextBox runat="server" id="SysNotReqNew" style="display:none" Text="eventnumber|timeofevent|timereported"/>
						<asp:TextBox runat="server" id="SysFormName" style="display:none" Text="claimlist" RMXRef="Instance/UI/FormVariables/SysFormName"/>
						<asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="eventid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
						<asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
						<asp:TextBox runat="server" id="SysSid" style="display:none" Text="11000" RMXRef="Instance/UI/FormVariables/SysSid"/>
						<asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
                        <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
                        <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
                        <asp:TextBox runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" style="display:none"/>
                        <asp:TextBox runat="server" id="eventid" RMXRef="Instance/UI/FormVariables/SysExData/EventId" style="display:none"/>                                                                                                                                              
                        <asp:TextBox runat="server" id="pirowid" RMXRef="Instance/UI/FormVariables/SysExData/PiRowId" style="display:none"/>                                                                                                                                              
    <br /> 
    <asp:button class="button" runat="server" id="btnAddGC" Text="Add GC Claim" postbackurl="claimgc.aspx?SysFormName=claimgc&SysCmd=&SysViewType=controlsonly&SysFormIdName=claimid&SysFormPIdName=eventid&SysEx=EventId&SysExMapCtl=EventId" />
    <asp:button class="button" runat="server" id="btnAddVA" Text="Add VA Claim" postbackurl="claimva.aspx?SysFormName=claimva&SysCmd=&SysViewType=controlsonly&SysFormIdName=claimid&SysFormPIdName=eventid&SysEx=EventId&SysExMapCtl=EventId" />
    <asp:button class="button" runat="server" id="btnAddWC" Text="Add WC Claim" postbackurl="claimwc.aspx?SysFormName=claimwc&SysCmd=&SysViewType=controlsonly&SysFormIdName=claimid&SysFormPIdName=eventid&SysEx=EventId|PiRowId&SysExMapCtl=EventId|PiRowId" />
    <asp:button class="button" runat="server" id="btnAddDI" Text="Add DI Claim" postbackurl="claimdi.aspx?SysFormName=claimdi&SysCmd=&SysViewType=controlsonly&SysFormIdName=claimid&SysFormPIdName=eventid&SysEx=EventId|PiRowId&SysExMapCtl=EventId|PiRowId" />
    <asp:Button class="button" runat="server" ID="btnDelete" Text="Delete" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:Button class="button" runat="server" ID="BackToParent" Text="Back to Event" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>  
    <input id="sortorder" runat="server" type="text" style="display:none"/>
    <asp:TextBox ID="SortColumn" runat="server" Visible="false"></asp:TextBox>
    <input type="text" runat="server" id="PageIds" style="display:none"/>
    
    </div>
	<uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
  </form>   
    <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>
</body>
</html>
  
  