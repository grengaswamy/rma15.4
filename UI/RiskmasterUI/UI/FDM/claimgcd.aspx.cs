﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using MultiCurrencyCustomControl;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;


namespace Riskmaster.UI.FDM
{
    public partial class Claimgcd : FDMBasePage
    {
        /// <summary>
        /// Is first tab loaded. 
        /// </summary>
        private bool isFirstTabLoaded = false;

        /// <summary>
        /// Control collection for required tab.
        /// </summary>
        private Control controlRequired = null;

        /// <summary>
        /// Required tab list.
        /// </summary>
        private List<string> requiredTabs = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params.Get("__EVENTTARGET") != null)
            {
                this.controlRequired = this.Form.FindControl(Request.Params.Get("__EVENTTARGET"));
            }
            FDMPageLoad();
            //gmallick for JIRA -12965  Start
            Button claimstatuscodedetail = this.Form.FindControl("claimstatuscodedetailbtn") as Button;
            if (claimstatuscodedetail != null)
            {
                claimstatuscodedetail.Attributes.Add("Visible", "True");
                claimstatuscodedetail.Enabled = true;
                claimstatuscodedetail.Attributes.Add("onclientclick", "return selectCodeWithDetail('claimstatuscode',1);");
                claimstatuscodedetail.CssClass = "ClaimStatusButton";
            }
            // gmallick for JIRA -12965  End
        }

        /// <summary>
        /// Bind data from CWS response to ASP.NET server controls
        /// </summary>
        /// <param name="messageElement">Response message.</param>
        /// <param name="controls">Control collection.</param>
        protected override void BindData2Control(XElement messageElement, ICollection controls)
        {
            string rmxReference = string.Empty;
            if (this.Form.FindControl("txtDynamicRequiredTabs") == null)
            {
                base.BindData2Control(messageElement, controls);
            }
            else
            {
                if (this.controlRequired != null)
                {
                    controls = this.controlRequired.Parent.Controls;
                    this.controlRequired = null;
                }
                else
                {
                    if (object.ReferenceEquals(this.requiredTabs, null) && !object.ReferenceEquals(this.Form.FindControl("txtRequiredTabs"), null))
                    {
                        this.requiredTabs = (this.Form.FindControl("txtRequiredTabs") as TextBox).Text.ToString().Split(',').ToList(); ;
                    }

                    if (string.IsNullOrEmpty(((TextBox)this.Form.FindControl("txtDynamicRequiredTabs")).Text))
                    {
                        string parentTabs = string.Empty;
                        foreach (string field in this.GetRequiredFieldsTab(((TextBox)this.Form.FindControl("SysRequired")).Text))
                        {
                            string requiredField = field;
                            string parentTab = string.Empty;

                            if (requiredField.Contains("_codelookup_cid"))
                            {
                                requiredField = requiredField.Replace("_codelookup_cid", "$codelookup_cid");
                            }

                            if (this.Form.FindControl(requiredField) is WebControl)
                            {
                                rmxReference = ((WebControl)this.Form.FindControl(requiredField)).Attributes["RMXRef"];

                                if (string.IsNullOrEmpty(rmxReference))
                                {
                                    continue;
                                }

                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                string requiredFieldValue = GetReturnValue(messageElement, rmxReference);

                                if (string.IsNullOrEmpty(requiredFieldValue) || (requiredField.Contains("_cid") && requiredFieldValue.Equals("0")))
                                {
                                    parentTab = this.GetTab(requiredField);
                                    if (!parentTabs.Contains(parentTab))
                                    {
                                        parentTabs += string.Format("{0}|", parentTab);
                                    }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(parentTabs))
                        {
                            ((TextBox)this.Form.FindControl("txtDynamicRequiredTabs")).Text = parentTabs.TrimEnd('|');
                        }
                    }
                }

                foreach (Control ctrl in controls)
                {
                    if (ctrl is WebControl)
                    {
                        if (m_sFormReadOnly == "Enable")
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden" && ((WebControl)ctrl).Attributes["rmxtype"] != "textml" && ((WebControl)ctrl).Attributes["rmxtype"] != "readonlymemo")
                                {
                                    DatabindingHelper.EnableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                DatabindingHelper.EnableControls(controlId, this.Page);
                            }
                        }

                        rmxReference = ((WebControl)ctrl).Attributes["RMXRef"];
                        if (string.IsNullOrEmpty(rmxReference))
                        {
                            continue;
                        }

                        if (((WebControl)ctrl).Attributes["rmxignorevalue"] != null)
                        {
                            continue;
                        }

                        string rmxIgnoreGet = ((WebControl)ctrl).Attributes["rmxignoreget"];
                        if (!string.IsNullOrEmpty(rmxIgnoreGet))
                        {
                            if (rmxIgnoreGet == "true")
                            {
                                continue;
                            }
                        }

                        Type controlType = ctrl.GetType();
                        string type = controlType.ToString();
                        int index = type.LastIndexOf(".");
                        type = type.Substring(index + 1);
                        string itemSetRef;
                        string rmxType;
                        string valueCollection;
                        switch (type)
                        {
                            case "DropDownList":
                                rmxReference = GetResponseRefPath(rmxReference);
                                itemSetRef = ((WebControl)ctrl).Attributes["ItemSetRef"];
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                if (!string.IsNullOrEmpty(itemSetRef))
                                {
                                    itemSetRef = GetResponseRefPath(itemSetRef);
                                    if (itemSetRef.IndexOf(g_sXPathDelimiter) > 0)
                                    {
                                        string[] lstRMXRef = itemSetRef.Split(g_sXPathDelimiter.ToCharArray());
                                        itemSetRef = lstRMXRef[0];
                                    }

                                    if (itemSetRef.EndsWith("option"))
                                    {
                                        itemSetRef = itemSetRef.Replace("/option", string.Empty);
                                    }

                                    valueCollection = GetReturnValue(messageElement, itemSetRef);
                                }
                                else
                                {
                                    valueCollection = string.Empty;
                                }

                                string selectedValue = GetReturnValue(messageElement, rmxReference);
                                if (!string.IsNullOrEmpty(valueCollection))
                                {
                                    DatabindingHelper.SetValue2Control((WebControl)ctrl, selectedValue, valueCollection);
                                }
                                else
                                {
                                    DatabindingHelper.SetValue2Control((WebControl)ctrl, selectedValue);
                                }

                                break;
                            case "CheckBox":
                                ((CheckBox)ctrl).InputAttributes.Add("onchange", "return setDataChanged(true);");
                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                string value = GetReturnValue(messageElement, rmxReference);
                                DatabindingHelper.SetValue2Control((WebControl)ctrl, value);
                                break;
                            case "CurrencyTextbox":
                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                sCurrencyMode = ((WebControl)ctrl).Attributes["CurrencyMode"];
                                XElement xmlNode = messageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/BaseCurrencyType");
                                if (xmlNode != null)
                                {
                                    if (string.IsNullOrEmpty(sCurrencyMode))
                                    {
                                        ((CurrencyTextbox)ctrl).SetProperties(xmlNode.Value, xmlNode.Value);
                                    }
                                    else
                                    {
                                        ((CurrencyTextbox)ctrl).SetProperties(null, xmlNode.Value);
                                    }
                                }

                                value = GetReturnValue(messageElement, rmxReference);
                                DatabindingHelper.SetValue2Control((CurrencyTextbox)ctrl, value);
                                break;
                            default:
                                rmxReference = GetResponseRefPath(rmxReference);
                                if (rmxReference.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = rmxReference.Split(g_sXPathDelimiter.ToCharArray());
                                    rmxReference = lstRMXRef[0];
                                }

                                value = GetReturnValue(messageElement, rmxReference);
                                DatabindingHelper.SetValue2Control((WebControl)ctrl, value);
                                break;
                        }

                        rmxType = ((WebControl)ctrl).Attributes["RMXType"];
                        if (m_sCustomizablememofields.Contains(rmxType))
                        {
                            DatabindingHelper.UpdateCustomizedControls(messageElement, rmxType, ctrl);
                        }

                        if (m_smsSecurity && !m_smsSecurityForSupp)
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden" && ((WebControl)ctrl).GetType().Name != "Button" && !ctrl.ClientID.StartsWith("supp_"))
                                {
                                    DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                if (!ctrl.ClientID.StartsWith("supp_"))
                                {
                                    string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                    DatabindingHelper.DisableControls(controlId, this.Page);
                                }
                            }
                        }
                        else if (m_smsSecurity && m_smsSecurityForSupp)
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden" && ((WebControl)ctrl).GetType().Name != "Button")
                                {
                                    DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                DatabindingHelper.DisableControls(controlId, this.Page);
                            }
                        }
                        else if (!m_smsSecurity && m_smsSecurityForSupp)
                        {
                            if (ctrl.ClientID == ctrl.ID)
                            {
                                if (ctrl.ClientID.StartsWith("supp_"))
                                {
                                    DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                }
                            }
                            else
                            {
                                if (ctrl.ClientID.StartsWith("supp_"))
                                {
                                    string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                    DatabindingHelper.DisableControls(controlId, this.Page);
                                }
                            }
                        }
                        else
                        {
                            if (m_sFormReadOnly == "Disable")
                            {
                                if (ctrl.ClientID == ctrl.ID)
                                {
                                    if (((WebControl)ctrl).Attributes["rmxtype"] != "id" && ((WebControl)ctrl).Attributes["rmxtype"] != "hidden")
                                    {
                                        DatabindingHelper.DisableControls(ctrl.ClientID, this.Page);
                                    }
                                }
                                else
                                {
                                    string controlId = ctrl.ClientID.Substring(0, (ctrl.ClientID.Length - ctrl.ID.Length - 1));
                                    DatabindingHelper.DisableControls(controlId, this.Page);
                                }
                            }
                        }
                    }
                    else if (ctrl is UserControlDataGrid)
                    {
                        if (m_smsSecurity)
                        {
                            ((UserControlDataGrid)ctrl).HideButtons = "New|Edit|Delete|Clone";
                        }
                        else if (m_sFormReadOnly == "Disable")
                        {
                            ((UserControlDataGrid)ctrl).HideButtons = "New|Edit|Delete|Clone";
                        }

                        XmlDocument objXml = new XmlDocument();
                        XmlReader objReader;
                        objReader = messageElement.CreateReader();
                        objXml.Load(objReader);
                        ((UserControlDataGrid)ctrl).BindData(objXml);
                    }
                    else
                    {
                        if (ctrl.ID != null)
                        {
                            if (ctrl.ClientID.StartsWith("FORMTAB") && this.isFirstTabLoaded)
                            {
                                if (!ctrl.ClientID.Contains("pvjurisgroup")
                                    && (object.ReferenceEquals(this.requiredTabs, null) || !this.requiredTabs.Contains(ctrl.ClientID.Replace("FORMTAB", string.Empty))))
                                {
                                    continue;
                                }
                            }

                            if (!string.IsNullOrEmpty(Request.Params.Get("__EVENTTARGET")) && Request.Params.Get("__EVENTTARGET").Equals(ctrl.ClientID))
                            {
                                ctrl.Visible = true;
                            }

                            if (ctrl.ID.IndexOf("_RMXTable") != -1)
                            {
                                if (!object.ReferenceEquals(this.requiredTabs, null) && this.requiredTabs.Contains(ctrl.ClientID.Replace("_RMXTable", string.Empty)))
                                {
                                    ctrl.Visible = true;
                                }

                                if (ctrl.Visible == false)
                                {
                                    continue;
                                }
                            }
                        }

                        if (ctrl.Controls.Count > 0)
                        {
                            this.BindData2Control(messageElement, ctrl.Controls);
                            if (ctrl.ClientID.StartsWith("FORMTAB") && !this.isFirstTabLoaded)
                            {
                                this.isFirstTabLoaded = true;
                            }
                        }
                    }

                    if (bDoFieldMarkReadOnly)
                    {
                        updateControls(ctrl, oFieldMark);
                    }

                    if (bKillFieldMarkNodes)
                    {
                        HideFieldMarkNodes(ctrl, oKillNodesFieldMark);
                    }
                }
            }
        }


        //Mridul 05/28/09 MITS 16745 (Chubb Ack)
        /// <summary>
        /// Override base and generates claim letter if claim is found eligible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {   
                int iClaimId = 0;
                string sCLMLtrTmplType = "";
                string sClaimNo = "";
                base.NavigateSave(sender,e);
                iClaimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);
                sCLMLtrTmplType = Convert.ToString(((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text);
                sClaimNo = Convert.ToString(((TextBox)this.Form.FindControl("claimnumber")).Text);
                if (sCLMLtrTmplType == "ACK" || sCLMLtrTmplType == "CL")
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClaimLetterMerge", "DoClaimLetterMerge("+iClaimId+",'"+sCLMLtrTmplType+"','"+sClaimNo+"');", true);
                sCLMLtrTmplType = "";
            }
            catch(Exception ex)
            {
            
            }
            finally{}
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oUseAdvancedClaim = oMessageElement.XPathSelectElement("//UseAdvancedClaim");
            Control oPolicyControl = this.FindControl("div_primarypolicyid");
            if (oUseAdvancedClaim != null)//Deb : MITS 25242
            {
                if (oUseAdvancedClaim.Value == "-1")
                {
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
                else
                {
                    oPolicyControl = this.FindControl("div_multipolicyid");
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
            }
            //MITS 14169:To open Policy from a claim:Setting client click attribute of _open button:Start
            int iPolicyId = 0;
            int iEnhPolicyId = 0;
            Button btnPolicyOpen = (Button)(this.Form.FindControl("primarypolicyid_open"));
            
            if (btnPolicyOpen != null)
            {
                XElement oPolicyId = oMessageElement.XPathSelectElement("//PrimaryPolicyId");
                if (oPolicyId != null)
                {
                    iPolicyId = Convert.ToInt32(oPolicyId.Value);
                }

                XElement oPolicyEnhId = oMessageElement.XPathSelectElement("//PrimaryPolicyIdEnh");
                if (oPolicyEnhId != null)
                {
                    iEnhPolicyId = Convert.ToInt32(oPolicyEnhId.Value);
                }

                XElement oUseEnhPolFlag = oMessageElement.XPathSelectElement("//UseEnhPolFlag");
                if (oUseEnhPolFlag != null)
                {
                    if (oUseEnhPolFlag.Value == "0" )
                    {

                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                       
                    }
                    else if (oUseEnhPolFlag.Value == "-1" )
                    {
                        
                        //Start-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.
						                      
						btnPolicyOpen.OnClientClick = "fnOpenPolicy('policyenhgl'); return false;";
						//End-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.
                       
                    }
                    
                }
            }

            btnPolicyOpen = (Button)(this.Form.FindControl("multipolicyid_open"));
            if (btnPolicyOpen != null)
            {
                if (btnPolicyOpen != null)
                {
                    btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                }
            }
            //MITS 14169:End
        }

        /// <summary>
        /// Gets the required fields tab.
        /// </summary>
        /// <param name="requiredFileds">The required fileds.</param>
        /// <returns>The required fileds</returns>
        private IEnumerable<string> GetRequiredFieldsTab(string requiredFileds)
        {
            List<string> lstRequiredFields = requiredFileds.TrimEnd('|').Split('|').ToList();
            foreach (string field in lstRequiredFields)
            {
                yield return field;
            }
        }

        /// <summary>
        /// Gets the tab.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns>The tab having required field.</returns>
        private string GetTab(string field)
        {
            if (this.Form.FindControl(field).ClientID.Contains("_RMXTable"))
            {
                return field.Replace("_RMXTable", string.Empty);
            }
            else
            {
                return this.GetTab(this.Form.FindControl(field).Parent.ClientID);
            }
        }

    }
}
