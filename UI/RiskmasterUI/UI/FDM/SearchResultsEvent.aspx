﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchResultsEvent.aspx.cs" Inherits="Riskmaster.UI.FDM.SearchResultsEvent" %>

<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
  <title>Search Results</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" /><script language="javaScript" src="/oxf/csc-Pages/riskmaster/Search/Common/Javascript/searchresults.js"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js" type="text/javascript"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js" type="text/javascript"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script></head>
 <body class="Margin0">
  <form id="wsrp_rewrite_form_1" name="frmData" method="post" action="/oxf/home"><input type="hidden" id="wsrp_rewrite_action_1" name="" value=""><table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td class="Arial" nowrap="">1 - 10 of 119 matches<br></td>
     <td class="Arial" align="right" width="50%" nowrap="">
      			Page 1 of 12<span class="disabled">&lt;&lt; First Previous </span><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26124%26content%262&amp;setvalue%26node-ids%26131%26content%26&amp;setvalue%26node-ids%26125%26content%26119';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Next </a><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26124%26content%2612&amp;setvalue%26node-ids%26131%26content%26&amp;setvalue%26node-ids%26125%26content%26119';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Last </a><span class="pagescrolldec">&gt;&gt;</span></td>
    </tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="ctrlgroup4" nowrap="" colspan="9">Search 
      										
     </td>
    </tr>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%261&amp;setvalue%26node-ids%26131%26content%261';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Event Number</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%262&amp;setvalue%26node-ids%26131%26content%262';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">PI Last Name</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%263&amp;setvalue%26node-ids%26131%26content%263';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">PI First Name</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%264&amp;setvalue%26node-ids%26131%26content%264';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">PI SSN/Tax ID</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%265&amp;setvalue%26node-ids%26131%26content%265';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Event Date</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%266&amp;setvalue%26node-ids%26131%26content%266';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Event Status</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%267&amp;setvalue%26node-ids%26131%26content%267';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Event Indicator</a></td>
    <td nowrap="" class="colheader6"><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26Sort&amp;setvalue%26node-ids%26132%26content%26ascending&amp;setvalue%26node-ids%26130%26content%268&amp;setvalue%26node-ids%26131%26content%268';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="td8">Department</a></td>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="parent.MDI('open', '112 EVT');">EV2008000112</a></td>
     <td class="Arial" nowrap="">Ladd&nbsp;</td>
     <td class="Arial" nowrap="">Gary&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">7/31/2001&nbsp;</td>
     <td class="Arial" nowrap="">C - Closed&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6760 - Store 6760&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="parent.MDI('open', '111 EVT');">EV2008000111</a></td>
     <td class="Arial" nowrap="">Anderson22&nbsp;</td>
     <td class="Arial" nowrap="">James&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">7/31/2001&nbsp;</td>
     <td class="Arial" nowrap="">C - Closed&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6760 - Store 6760&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="parent.MDI('open', '107 EVT');">EV2008000107</a></td>
     <td class="Arial" nowrap="">a&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">111-11-1111&nbsp;</td>
     <td class="Arial" nowrap="">7/31/2001&nbsp;</td>
     <td class="Arial" nowrap="">C - Closed&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6760 - Store 6760&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(1)">dvbdffEV2006000001PP</a></td>
     <td class="Arial" nowrap="">a&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">111-11-1111&nbsp;</td>
     <td class="Arial" nowrap="">7/31/2001&nbsp;</td>
     <td class="Arial" nowrap="">C - Closed&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6760 - Store 6760&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(1)">dvbdffEV2006000001PP</a></td>
     <td class="Arial" nowrap="">dss&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">7/31/2001&nbsp;</td>
     <td class="Arial" nowrap="">C - Closed&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6760 - Store 6760&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(2)">EV2006000002</a></td>
     <td class="Arial" nowrap="">Walters&nbsp;</td>
     <td class="Arial" nowrap="">Barbara&nbsp;</td>
     <td class="Arial" nowrap="">123-45-6789&nbsp;</td>
     <td class="Arial" nowrap="">2/8/2006&nbsp;</td>
     <td class="Arial" nowrap="">C - Closed&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(3)">EV2006000003</a></td>
     <td class="Arial" nowrap="">Hamann&nbsp;</td>
     <td class="Arial" nowrap="">Michael&nbsp;</td>
     <td class="Arial" nowrap="">&nbsp;</td>
     <td class="Arial" nowrap="">2/8/2006&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(5)">EV2006000005</a></td>
     <td class="Arial" nowrap="">Dodich&nbsp;</td>
     <td class="Arial" nowrap="">Sandra&nbsp;</td>
     <td class="Arial" nowrap="">123-49-8765&nbsp;</td>
     <td class="Arial" nowrap="">2/13/2006&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowdark2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(7)">EV2006000007</a></td>
     <td class="Arial" nowrap="">Walters&nbsp;</td>
     <td class="Arial" nowrap="">Barbara&nbsp;</td>
     <td class="Arial" nowrap="">123-45-6789&nbsp;</td>
     <td class="Arial" nowrap="">2/9/2006&nbsp;</td>
     <td class="Arial" nowrap="">O - Open&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr class="rowlight2">
     <td class="Arial" nowrap=""><a href="#" onclick="selRecord(7)">EV2006000007</a></td>
     <td class="Arial" nowrap="">Widell&nbsp;</td>
     <td class="Arial" nowrap="">Carol&nbsp;</td>
     <td class="Arial" nowrap="">654-32-1789&nbsp;</td>
     <td class="Arial" nowrap="">2/9/2006&nbsp;</td>
     <td class="Arial" nowrap="">O - Open&nbsp;</td>
     <td class="Arial" nowrap=""> - &nbsp;</td>
     <td class="Arial" nowrap="">6666 - Store 6666&nbsp;</td>
    </tr>
    <tr>
     <td class="ctrlgroup4" nowrap="" colspan="9">&nbsp;
      										
     </td>
    </tr>
   </table>
   <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td class="Arial" nowrap="">1 - 10 of 119 matches<br></td>
     <td class="Arial" align="right" width="50%" nowrap="">
      			Page 1 of 12<span class="disabled">&lt;&lt; First Previous </span><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26124%26content%262&amp;setvalue%26node-ids%26131%26content%26&amp;setvalue%26node-ids%26125%26content%26119';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Next </a><a href="/oxf/home" onclick="; ; document.getElementById('wsrp_rewrite_action_1').name += '$action^setvalue%26node-ids%267%26content%26MoveToPage&amp;setvalue%26node-ids%26124%26content%2612&amp;setvalue%26node-ids%26131%26content%26&amp;setvalue%26node-ids%26125%26content%26119';         document.forms['wsrp_rewrite_form_1'].submit();         event.returnValue=false;         return false" class="pagescroll"> Last </a><span class="pagescrolldec">&gt;&gt;</span></td>
    </tr>
   </table><input type="hidden" name="$node^21" value="" id="sys_ex"><input type="hidden" name="$node^18" value="event" id="searchcat"><input type="hidden" name="$node^34" value="" id="admtable"><input type="hidden" name="$node^35" value="" id="entitytableid"><table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
     <td><br></td>
    </tr>
    <tr class="Arial">
     <td align="center"><input type="submit" name="$action^setvalue%26node-ids%267%26content%26Back&amp;setvalue%26node-ids%26130%26content%26&amp;setvalue%26node-ids%26129%26content%26True" value="Return To Search" id="btnBack" class="button" style="width:100px"><input type="submit" name="$action^setvalue%26node-ids%267%26content%26Print" value="Print" id="btnPrint" class="button" style="width:100px"></td>
    </tr>
   </table><input type="hidden" name="$node^5" value="rmx-widget-handle-3" id="SysWindowId"><input type="hidden" name="$instance" value="H4sIAAAAAAAAAO1YXY/aOBR9nv4KlPeOB5ju7FYhEkOCFO0MIJLp7j5FJrlANImNbNPA/vp1PshH&#xA;J6nTbdVK7eQFf5zje3OOsZWr24QLTHwYnOKI8PenLWUxn2h7IQ7vEUqS5DoZX1O2Q6ObmxHKpzXj&#xA;zdXV1aB49BgEzkbKoXKK+wyAvA0DA7GQP8eYC2DIAcz8ffGzBn6MhI4qaPtSRw4snfW5r6NLpyMq&#xA;cB7SdC3UyLQEJCEJaJIuoKOqXU6j8o2q6GFHLOwLGQp1zD6E5PlPOHdN872MDIxRxo0tjjhIGWpD&#xA;7aR8svlmUpGwOeDkGthkSz+BmtQ/xkCEUcOmRjzikBhvPg338tHt7QOlz08H40ZHZTvL9UWyFYes&#xA;GPVlThmp7ChYTrYn5hHeGUMd1XoK3ocQEjswRjoqWgr8XO7qBY5Bboey2UeJC9iF+IBUQVy8iUDu&#xA;dsFCXyjRzplbJ5lP/qsCgxAh2fF+OdimOjo9kkDGfZvKXrQVFBO2WP6Ri/92T3RujxKdwtQvN8Mi&#xA;Nz1vqDII+SHC53kIUXDZiJ3okrVkAbD78xB9KWP0xYyx8oVR2zt0onNnpDbKdadBnG0Uuf3KpoJi&#xA;ERGKc7690lO12VftyvMBlEnl50wvaP6mhSpdqBK9TXHF9ecn1d0nL5prn8ZoXV1cf8FGXjwfQx/Q&#xA;jMYxJVrBCyCCHU5vgpJO2QYoya5OetoiiUMV6sLjFB9KBvf3IANdy6l0PKOmDQTkI0RUvrg2yJIV&#xA;UoSJNtQGYTDR5g/m8EY+ssf58SCXk00iD6WJZn2wFq63eHq8t9baQOCNvOrglDFF6kyBkL1sQQEn&#xA;obXqpS8PwLLEjYmOqs4LHMry+zU0H75r0/xh6kjJp49WXfBRKfjK9qTitvvPq+j/U/Rhi+hze92i&#xA;+vhV9W+m+h8tqrvTvz3brCt++6r4Vyh+2zjQbxuKRyAZNe1Na+V6VlP9d+3Huoy6/zrlM6znp59T&#xA;7VNZ2Lbpn9MznwZQV9pz3Kn75DTMHDfMHHXezjnVmy3Nxtn1W7uXaeTv46WRxdVR2f9FrbUXpj2b&#xA;usv1Z9wdd7or2S+svXu19keftHd1+4Z3LfaZU9fylnPvYlHp3u/t7gVYdLincHQDIgEgKl9bV3EE&#xA;ZsKUkdsNb+VYJOjHUG0CHTU+s3pUKGjx0Z9XFeoD7YUuVKtL1RaqioZFUEXoFd7BgqpLI1TgaA0+&#xA;ZYG61PCIT3kKamga3gn/7fuB26MaYm/vsf/co4bDxIxGxzitTlbjfepaBXWFOUe9CmFZycLA3AcS&#xA;hGQnd3M28FlfCw/zobIoKSlpgTAvShv/AfeT4vWfFgAA"></form>
 </body>
</html>