﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cmxcmgrhistlist.aspx.cs" Inherits="Riskmaster.UI.FDM.CmXCmgrHistList" ValidateRequest="false" EnableViewStateMac="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
     <title>Untitled Page</title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script> 
  <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script>
</head>
<body onload="pageLoaded()" class="10pt">
    <form id="frmData" runat="server">
    <div>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
    <h2>Case Manager History</h2>
    <table border="0" width="95%" cellspacing="0" cellpadding="2" id="tbllist">
    <tr>
     <td class="ctrlgroup" width="50px"></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn1" class="ctrlgroup" onclick="SortList(document.all.tbllist,1,'lnkCaseMgrEid',true );">Case Manager</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkCaseMgrEid',true );">Referral Type</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,3,'lnkCaseMgrEid',true );">Referral Date</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn4" class="ctrlgroup" onclick="SortList(document.all.tbllist,4,'lnkCaseMgrEid',true );">Referred To</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn5" class="ctrlgroup" onclick="SortList(document.all.tbllist,5,'lnkCaseMgrEid',true );">Case Status</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn6" class="ctrlgroup" onclick="SortList(document.all.tbllist,6,'lnkCaseMgrEid',true );">Date Closed</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn7" class="ctrlgroup" onclick="SortList(document.all.tbllist,7,'lnkCaseMgrEid',true );">UR Due Date</a></td>
     <td class="ctrlgroup" nowrap="1"><a href="#" id="HeaderColumn8" class="ctrlgroup" onclick="SortList(document.all.tbllist,8,'lnkCaseMgrEid',true );">Primary Case Mgr?</a></td>
    </tr>
    <%int i = 0; foreach (XElement item in result)
    {
        string rowclass = "";
        string strvalue = "";
        if ((i % 2) == 1) rowclass = "datatd1";
        else rowclass = "datatd";
        i++;
        %>
        <tr>                          
            <td class="<%=rowclass%>">
            <%
                PageId.ID = "PageId" + i;
                PageId.Value = item.Element("CmcmhRowId").Value;
          
           %>                              
                <input type ="checkbox" name="PageId" id="PageId" title="Case Manager History" runat="server"/></td>                            
            
            <td class="<%=rowclass%>">
                <%
                    lnkCaseMgrEid.ID = "lnkCaseMgrEid" + i;
                    lnkCaseMgrEid.Text = item.Element("CaseMgrEid").Value;
                    lnkCaseMgrEid.PostBackUrl = "cmxcmgrhist.aspx?SysFormName=CmXCmgrHist&SysCmd=0&SysViewType=tab&SysFormIdName=cmcmhrowid&SysFormId=" + item.Element("CmcmhRowId").Value + "&SysFormPIdName=casemgtrowid&SysEx=casemgtrowid|EventNumber&SysExMapCtl=casemgtrowid|EventNumber";  
                %>                                                                                                                             
                 <asp:LinkButton runat="server" id="lnkCaseMgrEid"></asp:LinkButton> 
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("RefTypeCode").Value%>
            </td>
            <td class="<%=rowclass%>">
                <%=AppHelper.GetUIDate(item.Element("RefDate").Value)%>
            </td>
            <td class="<%=rowclass%>">
                <%=item.Element("RefToEid").Value%>
            </td>
            <td class="<%=rowclass%>">
               <%=item.Element("CaseStatusCode").Value%>
            </td>
            
            <td class="<%=rowclass%>">
                <%=AppHelper.GetUIDate(item.Element("DateClosed").Value)%> 
            </td>
            <td class="<%=rowclass%>">
                <%=AppHelper.GetUIDate(item.Element("DueDate").Value)%>
            </td>
            <td class="<%=rowclass%>">
                   <% if (item.Element("PrimaryCmgrFlag").Value == "True")
                        strvalue="YES";
                    else
                        strvalue="NO";
                    %> 
            <%=strvalue%>
                              
            </td>
            <td style="display:none">                           
            </td>
            <td style="display:none">                           
               <%=lnkCaseMgrEid.PostBackUrl%>
            </td>    
            <td style="display:none">
              <%=item.Element("CmcmhRowId").Value%>
            </td>
       </tr>                          
 <% }%>  
        
    </table>
    <center><div id="pageNavPosition"></div></center> 
    <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
    <asp:TextBox runat="server" id="DeleteListTemplate"  style="display:none" Text="Instance/CmXCmgrHistList/CmXCmgrHist/CmcmhRowId"/>				
    <asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
    <asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
    <asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
    <asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
    <asp:TextBox runat="server" id="SysClassName" style="display:none" Text="CmXAccommodationList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
    <asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;CmXCmgrHistList&gt;&lt;CmXCmgrHist /&gt;&lt;/CmXCmgrHistList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
    <asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
    <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
    <asp:TextBox runat="server" id="SysFormPIdName" Text="casemanagementid" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
    <asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
    <asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>
    <asp:TextBox runat="server" id="SysNotReqNew" style="display:none" RMXRef="Instance/UI/FormVariables/SysNotReqNew" />
    <asp:TextBox runat="server" id="SysFormName" style="display:none" Text="CmXCmgrHistList" RMXRef="Instance/UI/FormVariables/SysFormName"/>
    <asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="casemanagementid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
    <asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
    <asp:TextBox runat="server" id="SysSid" style="display:none" Text="" RMXRef="Instance/UI/FormVariables/SysSid"/><%--averma62 MITS 32103--%>
    <asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
    <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
    <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
    <asp:TextBox runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" style="display:none"/>
    <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId" style="display:none"/>
    <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" style="display:none"/>
    <asp:TextBox runat="server" id="casemgtrowid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="casemanagementid" RMXRef="Instance/UI/FormVariables/SysExData/CasemgtRowId" style="display:none"/>
    <asp:TextBox runat="server" id="cmcmhrowid" RMXRef="Instance/UI/FormVariables/SysExData/CmcmhRowId"   style="display:none"/> 
    <asp:button class="button" runat="server" id="btnAddNew" Text="Add New" postbackurl="cmxcmgrhist.aspx?SysFormName=CmXCmgrHist&SysCmd=&SysViewType=tab&SysFormIdName=cmcmhrowid&SysFormPIdName=casemgtrowid&SysEx=CasemgtRowId|EventNumber&SysExMapCtl=CasemgtRowId|EventNumber" />
    <asp:Button class="button" runat="server" ID="btnDelete" Text="Delete" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:Button class="button" runat="server" ID="Back" Text="Back" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>      
    <input type="text" runat="server" id="PageIds" style="display:none"/>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
     <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>
</body>
</html>
