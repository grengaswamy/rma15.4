<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entityxoperatingas.aspx.cs"  Inherits="Riskmaster.UI.FDM.Entityxoperatingas" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Operating As Information</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script language="JavaScript" src="../../Scripts/supportscreens.js">{var i;}</script>
    <script language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Operating As Information" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSentityxoperatingas" id="TABSentityxoperatingas">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="entityxoperatingas" id="LINKTABSentityxoperatingas">Operating As Information</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABentityxoperatingas" id="FORMTABentityxoperatingas">
        <asp:TextBox style="display:none" runat="server" id="OperatingId" RMXRef="//OperatingId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="EntityId" RMXRef="//EntityId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" />
        <div runat="server" class="full" id="div_Initials" xmlns="">
          <asp:label runat="server" class="required" id="lbl_Initials" Text="Initial" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="Initials" RMXRef="//Initials" RMXType="text" tabindex="1" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="Initials" ID="rfv_Initials" />
          </span>
        </div>
        <div runat="server" class="full" id="div_OperatingAs" xmlns="">
          <asp:label runat="server" class="required" id="lbl_OperatingAs" Text="Name" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="OperatingAs" RMXRef="//OperatingAs" RMXType="text" tabindex="2" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="OperatingAs" ID="rfv_OperatingAs" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" width="75px" onClientClick="return EntityXOperatingAs_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return EntityXOperatingAs_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" name="formname" value="entityxoperatingas" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" value="Initials|OperatingAs|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>