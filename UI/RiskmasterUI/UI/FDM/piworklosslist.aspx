﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="piworklosslist.aspx.cs" Inherits="Riskmaster.UI.FDM.PiworklossList" ValidateRequest="false" EnableViewStateMac="false"  %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <link href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript" ></script> 
	<script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script> 
   <script type="text/javascript" src="../../Scripts/pagingsorting.js"></script>
</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" runat="server">    
    <%--Mits 28937--%>
    <asp:ImageButton runat="server" id="imgBtnAddNew" Text="<%$ Resources:imgbtnAddNew %>" Tooltip ="<%$ Resources:imgbtnAddNew %>" src="../../Images/tb_new_active.png" onMouseOver="this.src='../../Images/tb_new_mo.png';" onMouseOut="this.src='../../Images/tb_new_active.png';"   width="28" height="28" border="0" postbackurl="piworkloss.aspx?SysFormName=piworkloss&SysCmd=&SysViewType=controlsonly&SysFormIdName=piwlrowid&SysFormPIdName=pirowid&SysEx=PiRowId|EventId&SysExMapCtl=PiRowId|EventId" />                           
    <asp:ImageButton runat="server" ID="imgBtnDelete" Text="<%$ Resources:imgbtnDelete %>" Tooltip="<%$ Resources:imgbtnDelete %>" src="../../Images/tb_delete_active.png" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';"  width="28" height="28" border="0" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:ImageButton runat="server" ID="BackToParent" Text="<%$ Resources:imgbtnBack %>" Tooltip="<%$ Resources:imgbtnBack %>" src="../../Images/tb_backrecord_active.png" onMouseOver="this.src='../../Images/tb_backrecord_mo.png';" onMouseOut="this.src='../../Images/tb_backrecord_active.png';" width="28" height="28" border="0" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>    
    <%--End of Mits 28937--%>
    <div>
     <asp:Label ID="lblError" runat="server" Text="" ForeColor=Red></asp:Label>
            <%--<h2>Work Loss List</h2>--%>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
                <tr class="msgheader">
                    <td class="msgheader" colspan=""><asp:Label id="lblHeader" runat="server" Class="required" Text="<%$ Resources:lblHeader %>"/></td>
                </tr>
            </table>
			 <!--rsushilaggar MITS 37986-->
        <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
                <tr >
                    <td  colspan=""><asp:Label ID="lblTotalDays" runat="server"></asp:Label></td>
                </tr>
            </table>
            </br>
                          	<table border="0" width="100%" cellspacing="0" cellpadding="2" id="tbllist">
                          <tr>
                            <td class="ctrlgroup" width="50px" />
                            <td class="ctrlgroup"><asp:HyperLink runat="server" href="#"  id="HeaderColumn1" class="ctrlgroup" onclick="SortList(document.all.tbllist,1,'lnkPiWorkLoss',true );" Text='<%$ Resources:hyplnklastworkday %>'></asp:HyperLink></td>                         
                            <td class="ctrlgroup" nowrap="1"><asp:HyperLink runat="server" href="#"  id="HeaderColumn2" class="ctrlgroup" onclick="SortList(document.all.tbllist,2,'lnkPiWorkLoss',true );" Text='<%$ Resources:hyplnkreturntowork %>'></asp:HyperLink></td>
                            <td class="ctrlgroup" nowrap="1"><asp:HyperLink runat="server" href="#"  id="HeaderColumn3" class="ctrlgroup" onclick="SortList(document.all.tbllist,3,'lnkPiWorkLoss',true );" Text='<%$ Resources:hyplnkduration %>'></asp:HyperLink></td>                           
                            <td class="ctrlgroup" nowrap="1"><asp:HyperLink runat="server" href="#"  id="HeaderColumn4" class="ctrlgroup" onclick="SortList(document.all.tbllist,4,'lnkPiWorkLoss',true );" Text='<%$ Resources:hyplnkstateduration %>'></asp:HyperLink></td>
                          </tr>
                          
                          <%int i = 0; foreach (XElement item in result)
                            {
                                string rowclass = "";
                                if ((i % 2) == 1) rowclass = "datatd1";
                                else rowclass = "datatd";
                                i++;
                                %>
                                
                          <tr>                          
                                <td class="<%=rowclass%>">                              
                                <table border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                  <td class="<%=rowclass%>">
                                  <%
                                      PageId.ID = "PageId" + i;
                                      PageId.Value = item.Element("PiWlRowId").Value;
                                  %>
                                    <input type ="checkbox" name="PageId" id="PageId" title="Work Loss List" runat="server" /></td>                       
                                  </tr>
                                </table>
                              </td>
                             <td class="<%=rowclass%>" nowrap="1">     
                               <%
                                 lnkPiWorkLoss.ID = "lnkPiWorkLoss" + i;
                                 lnkPiWorkLoss.Text = AppHelper.GetDate(item.Element("DateLastWorked").Value);
                                 lnkPiWorkLoss.PostBackUrl = "piworkloss.aspx?SysFormName=piworkloss&SysCmd=0&SysViewType=controlsonly&SysFormIdName=piwlrowid&SysFormId=" + item.Element("PiWlRowId").Value + "&SysFormPIdName=pirowid&SysEx=PiRowId|EventId&SysExMapCtl=PiRowId|EventId";%>                                                                                                                                                                             
                             <asp:LinkButton runat="server" id="lnkPiWorkLoss"></asp:LinkButton>                               
                            </td>                          
                           <td class="<%=rowclass%>">                          
                             <%=AppHelper.GetDate(item.Element("DateReturned").Value)%>
                          </td>
                               <td class="<%=rowclass%>">
                            <%=item.Element("Duration").Value%>
                          </td>                            
                          <td class="<%=rowclass%>">
                            <%=item.Element("StateDuration").Value%>
                          </td>  
                           <td style="display:none">                           
                            </td>
                            <td style="display:none">                           
                               <%=lnkPiWorkLoss.PostBackUrl%>
                            </td>    
                            <td style="display:none">
                              <%=item.Element("PiWlRowId").Value%>
                            </td>                                                                                               
                          </tr>                          
                            <% }%>                      
                       
                    </table>
                        <center><div id="pageNavPosition"></div></center> 
                        <asp:TextBox runat="server" id="SysInvisible" style="display:none" />
                        <asp:TextBox runat="server" id="TextBox1" style="display:none" />
                        <asp:TextBox runat="server" id="DeleteListTemplate"  style="display:none" Text="Instance/PiXWorkLossList/PiXWorkLoss/PiWlRowId"/>					
						<asp:TextBox runat="server" id="SysCmd"  style="display:none" RMXRef="Instance/UI/FormVariables/SysCmd"/>
						<asp:TextBox runat="server" id="SysCmdConfirmSave" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave"/>
						<asp:TextBox runat="server" id="SysCmdQueue" style="display:none" RMXRef="Instance/UI/FormVariables/SysCmdQueue"/>
						<asp:TextBox runat="server" id="SysCmdText" style="display:none" Text="Navigate" RMXRef="Instance/UI/FormVariables/SysCmdText"/>
						<asp:TextBox runat="server" id="SysClassName" style="display:none" Text="PiworklossList" RMXRef="Instance/UI/FormVariables/SysClassName"/>						
						<asp:TextBox runat="server" id="SysSerializationConfig" style="display:none" Text="&lt;PiXWorkLossList&gt;&lt;PiXWorkLoss/&gt;&lt;/PiXWorkLossList&gt;"	RMXRef="Instance/UI/FormVariables/SysSerializationConfig"/>	
						<asp:TextBox runat="server" id="SysSerializationConfigIgnore" style="display:none" Text="Claim.Comments"	RMXRef="Instance/UI/FormVariables/SysSerializationConfigIgnore"/>						                             
                        <asp:TextBox runat="server" id="SysFormPForm" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPForm"/>
						<asp:TextBox runat="server" id="SysFormPIdName" Text="pirowid" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
						<asp:TextBox runat="server" id="SysFormPId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormPId"/>
						<asp:TextBox runat="server" id="SysPSid" style="display:none" RMXRef="Instance/UI/FormVariables/SysPSid"/>
						<asp:TextBox runat="server" id="SysEx" style="display:none" RMXRef="Instance/UI/FormVariables/SysEx"/>						
						<asp:TextBox runat="server" id="SysFormName" style="display:none" Text="piworklosslist" RMXRef="Instance/UI/FormVariables/SysFormName"/>
						<asp:TextBox runat="server" id="SysFormIdName" style="display:none" Text="pirowid" RMXRef="Instance/UI/FormVariables/SysFormIdName"/>
						<asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
						<asp:TextBox runat="server" id="SysFormId" style="display:none" RMXRef="Instance/UI/FormVariables/SysFormId"/>
						<!-- pmittal5 12/28/09 Mits 18340 - Corrected the value of SysSid attribute-->
						<asp:TextBox runat="server" id="SysSid" style="display:none" Text="11600" RMXRef="Instance/UI/FormVariables/SysSid"/>
						<asp:TextBox runat="server" id="SysViewType" style="display:none" RMXRef="Instance/UI/FormVariables/SysViewType"/>
                        <asp:TextBox ID="paramstore" runat="server" style="display:none"></asp:TextBox>	
                        <asp:TextBox runat="server" id="pirowid" RMXRef="Instance/UI/FormVariables/SysExData/PiRowId" style="display:none"/>
                        <asp:TextBox runat="server" id="eventid" RMXRef="Instance/UI/FormVariables/SysExData/EventId"   style="display:none"/>                       
                        <asp:TextBox runat="server" id="piwlrowid" RMXRef="Instance/PiXWorkLossList/PiXWorkLoss/PiWlRowId" style="display:none"/>
                        <asp:TextBox runat="server" id="claimid" RMXRef="Instance/UI/FormVariables/SysExData/ClaimId"   style="display:none"/>
                        <asp:TextBox runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber"   style="display:none"/>
                        <asp:TextBox runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber"   style="display:none"/>                                                                  
                        <asp:TextBox runat="server" id="DateOfEvent" RMXRef="Instance/UI/FormVariables/SysExData/DateOfEvent"   style="display:none"/>         <!--rsushilaggar MITS 37986-->
                                                                 
                        <asp:TextBox runat="server" id="deptassignedeid" RMXRef="Instance/UI/FormVariables/SysExData/DeptAssignedEid" style="display:none" />                                           
    <%--Mits 28937--%>
    <%--<asp:button class="button" runat="server" id="btnAddNew" Text="Add New" postbackurl="piworkloss.aspx?SysFormName=piworkloss&SysCmd=&SysViewType=controlsonly&SysFormIdName=piwlrowid&SysFormPIdName=pirowid&SysEx=PiRowId|EventId&SysExMapCtl=PiRowId|EventId" />                           
    <asp:Button class="button" runat="server" ID="btnDelete" Text="Delete" OnClick="NavigateListDelete"  OnClientClick="return DeleteList();"/>
    <asp:Button class="button" runat="server" ID="BackToParent" Text="Back" onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"/>     --%>
    <%-- End of Mits 28937--%>
    <input type="text" runat="server" id="PageIds" style="display:none"/>
    </div>
	<uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
  </form>
  <script type="text/javascript">
        var listpaging = new ListPaging('tbllist', 3); 
        listpaging.init(); 
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        listpaging.showPage(1);
    </script>   
</body>
</html>
