﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.FDM
{
    public partial class Litigation : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }

        //MITS 17107
        public override void ModifyControls()
        {
            //Adding a null check in all parsing of controls as control types may change in readonly powerviews
            TextBox txtAttachDoc = null;
            if (this.FindControl("hdnAttachDocFlag") != null)
            {
                if (this.FindControl("hdnAttachDocFlag").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    txtAttachDoc = ((TextBox)this.FindControl("hdnAttachDocFlag"));
                }
            }

            HtmlGenericControl btnAttachDoc = null;
            if (this.FindControl("div_attach") != null)
            {
                if (this.FindControl("div_attach").GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlGenericControl"))
                {
                    btnAttachDoc = ((HtmlGenericControl)this.FindControl("div_attach"));
                }
            }
            if (btnAttachDoc != null)
            {
                if (txtAttachDoc.Text == "true")
                {
                    btnAttachDoc.Visible = true;
                }
                else if (txtAttachDoc.Text == "false")
                {
                    btnAttachDoc.Visible = false;
                }
            }
        }
    }
}
