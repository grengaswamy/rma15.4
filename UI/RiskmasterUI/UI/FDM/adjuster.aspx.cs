﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.FDM
{
    public partial class Adjuster : FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
        }

        //Mridul 05/28/09 MITS 16745 (Chubb Ack)
        /// <summary>
        /// Override base and generates claim letter if claim is found eligible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {
                int iClaimId = 0;
                string sCLMLtrTmplType = "";
                string sClaimNo = "";
                base.NavigateSave(sender, e);
                iClaimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);
                sCLMLtrTmplType = Convert.ToString(((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text);
                sClaimNo = Convert.ToString(((TextBox)this.Form.FindControl("claimnumber")).Text);
                if (sCLMLtrTmplType == "ACK" || sCLMLtrTmplType == "CL")
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClaimLetterMerge", "DoClaimLetterMerge(" + iClaimId + ",'" + sCLMLtrTmplType + "','" + sClaimNo + "');", true);
            }
            catch (Exception ex)
            {

            }
            finally { }
        }
    }
}
