﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageNotFound.aspx.cs" Inherits="Riskmaster.UI.FDM.PageNotFound" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Page Not Found</title>
    <link href='/RiskmasterUI/Content/rmnet.css' rel="stylesheet" type="text/css" />
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="form1" runat="server">
    <div>
        <div align="center" class="errtextheader">The page can not be found in the PowerView.</div>
        <br />
   		<br />
   		<!--input type="button" class="button" value="  Close  " onClick="window.close();"--></div>
    </div>
    </form>
</body>
</html>
