﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.FDM
{
    public partial class Claimva : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
            //Start jramkumar MITS 34419
            if (IsPostBack)
            {
                string txtFocusScript = "var focusField=false; if((document.getElementById('ev_depteid') != null && document.getElementById('ev_depteid').value != '') || (document.getElementById('ev_countrycode_codelookup') != null && document.getElementById('ev_countrycode_codelookup').value != '')) { focusField=true; }";
                this.ClientScript.RegisterStartupScript(this.GetType(), "va", txtFocusScript, true);
            }
            //End jramkumar MITS 34419
            //gmallick for JIRA -12965  Start
            Button claimstatuscodedetail = this.Form.FindControl("claimstatuscodedetailbtn") as Button;
            if (claimstatuscodedetail != null)
            {
                claimstatuscodedetail.Attributes.Add("Visible", "True");
                claimstatuscodedetail.Enabled = true;
                claimstatuscodedetail.Attributes.Add("onclientclick", "return selectCodeWithDetail('claimstatuscode',1);");
                claimstatuscodedetail.CssClass = "ClaimStatusButton";
            }
		  // gmallick for JIRA -12965  End
           
        }
        //Mridul 05/28/09 MITS 16745 (Chubb Ack)
        /// <summary>
        /// Override base and generates claim letter if claim is found eligible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {
                int iClaimId = 0;
                string sCLMLtrTmplType = "";
                string sClaimNo = "";
                base.NavigateSave(sender, e);
                iClaimId = Convert.ToInt32(((TextBox)this.Form.FindControl("claimid")).Text);
                sCLMLtrTmplType = Convert.ToString(((TextBox)this.Form.FindControl("ClaimLetterTmplId")).Text);
                sClaimNo = Convert.ToString(((TextBox)this.Form.FindControl("claimnumber")).Text);
                if (sCLMLtrTmplType == "ACK" || sCLMLtrTmplType == "CL")
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClaimLetterMerge", "DoClaimLetterMerge(" + iClaimId + ",'" + sCLMLtrTmplType + "','" + sClaimNo + "');", true);
                sCLMLtrTmplType = "";

                // rrachev JIRA 5021 Get current value of ContainsOpenDiaries
                Control openDiaries = this.FindControl("containsopendiaries");
                if (openDiaries != null && this.Data != null && !String.IsNullOrEmpty(this.Data.OuterXml))
                {
                    BindData2Control(XElement.Parse(this.Data.OuterXml), new Control[] { openDiaries });
                }
            }
            catch (Exception ex)
            {

            }
            finally { }
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            //MITS 14169:To open Policy from a claim:Setting client click attribute of _open button:Start
            int iPolicyId = 0;
            int iEnhPolicyId = 0;

            XElement oUseAdvancedClaim = oMessageElement.XPathSelectElement("//UseAdvancedClaim");
            Control oPolicyControl = this.FindControl("div_primarypolicyid");
            if (oUseAdvancedClaim != null)//Deb : MITS 25242
            {
                if (oUseAdvancedClaim.Value == "-1")
                {
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
                else
                {
                    oPolicyControl = this.FindControl("div_multipolicyid");
                    if (oPolicyControl != null)
                    {
                        oPolicyControl.Visible = false;
                    }
                }
            }

            Button btnPolicyOpen = (Button)(this.Form.FindControl("primarypolicyid_open"));

            if (btnPolicyOpen != null)
            {
                XElement oPolicyId = oMessageElement.XPathSelectElement("//PrimaryPolicyId");
                if (oPolicyId != null)
                {
                    iPolicyId = Convert.ToInt32(oPolicyId.Value);
                }

                XElement oPolicyEnhId = oMessageElement.XPathSelectElement("//PrimaryPolicyIdEnh");
                if (oPolicyEnhId != null)
                {
                    iEnhPolicyId = Convert.ToInt32(oPolicyEnhId.Value);
                }

                XElement oUseEnhPolFlag = oMessageElement.XPathSelectElement("//UseEnhPolFlag");
                if (oUseEnhPolFlag != null)
                {
                    if (oUseEnhPolFlag.Value == "0")
                    {

                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";

                    }
                    else if (oUseEnhPolFlag.Value == "-1")
                    {

                        //Start-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.

                        btnPolicyOpen.OnClientClick = "fnOpenPolicy('policyenhal'); return false;";
                        //End-Mridul Bansal. 01/11/10. MITS#18229. Open LOB specific window.

                    }

                }
            }

            btnPolicyOpen = (Button)(this.Form.FindControl("multipolicyid_open"));
            if (btnPolicyOpen != null)
            {
                if (btnPolicyOpen != null)
                {
                    btnPolicyOpen.OnClientClick = "fnOpenPolicy('policy'); return false;";
                }
            }

            //MITS 14169:End
        }
    }
}
