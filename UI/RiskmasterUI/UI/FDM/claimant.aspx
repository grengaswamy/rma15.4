﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="claimant.aspx.cs"  Inherits="Riskmaster.UI.FDM.Claimant" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Claimant</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/tb_save_active.png" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/tb_save_mo.png';" onMouseOut="this.src='../../Images/tb_save_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" OnClientClick="return DeleteRecord();" src="../../Images/tb_delete_active.png" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/tb_delete_mo.png';" onMouseOut="this.src='../../Images/tb_delete_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return attach();" src="../../Images/tb_attach_active.png" width="28" height="28" border="0" id="attach" AlternateText="Attach Documents" onMouseOver="this.src='../../Images/tb_attach_mo.png';" onMouseOut="this.src='../../Images/tb_attach_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return FilteredDiary();" src="../../Images/tb_diaryviewrecord_active.png" width="28" height="28" border="0" id="filtereddiary" AlternateText="View Record Diaries" onMouseOver="this.src='../../Images/tb_diaryviewrecord_mo.png';" onMouseOut="this.src='../../Images/tb_diaryviewrecord_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Diary();" src="../../Images/tb_diary_active.png" width="28" height="28" border="0" id="diary" AlternateText="Diary" onMouseOver="this.src='../../Images/tb_diary_mo.png';" onMouseOut="this.src='../../Images/tb_diary_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_comments" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Comments();" src="../../Images/tb_comments_active.png" width="28" height="28" border="0" id="comments" AlternateText="Comments" onMouseOver="this.src='../../Images/tb_comments_mo.png';" onMouseOut="this.src='../../Images/tb_comments_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return recordSummary();" src="../../Images/tb_recordsummary_active.png" width="28" height="28" border="0" id="recordsummary" AlternateText="Record Summary" onMouseOver="this.src='../../Images/tb_recordsummary_mo.png';" onMouseOut="this.src='../../Images/tb_recordsummary_active.png';" />
        </div>
        <div class="toolBarButton" runat="server" id="div_ofac" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OFACLookUp();" src="../../Images/tb_ofac_active.png" width="28" height="28" border="0" id="ofac" AlternateText="OFAC Check" onMouseOver="this.src='../../Images/tb_ofac_mo.png';" onMouseOut="this.src='../../Images/tb_ofac_active.png';" />
        </div>
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Claimant" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSclmntinfo" id="TABSclmntinfo">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="clmntinfo" id="LINKTABSclmntinfo">Claimant Info</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPclmntinfo">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSclmntattorney" id="TABSclmntattorney">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="clmntattorney" id="LINKTABSclmntattorney">Claimant Attorney</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPclmntattorney">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);return false;" runat="server" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPsuppgroup">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABclmntinfo" id="FORMTABclmntinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="claimid" RMXRef="Instance/Claimant/ClaimId|Instance/UI/FormVariables/SysExData/ClaimId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="claimantrowid" RMXRef="Instance/Claimant/ClaimantRowId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="claimnumber" RMXRef="Instance/UI/FormVariables/SysExData/ClaimNumber" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="clmntentityid" RMXRef="Instance/Claimant/ClaimantEntity/EntityId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="primaryClaimant" RMXRef="Instance/UI/FormVariables/SysExData/DiaryMessage" RMXType="id" />
              <div runat="server" class="half" id="div_clmntlastname" xmlns="">
                <asp:label runat="server" class="required" id="lbl_clmntlastname" Text="Claimant Last Name" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="1" onblur="lookupLostFocus(this);" RMXRef="Instance/Claimant/ClaimantEntity/LastName" RMXType="pientitylookup" id="clmntlastname" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="clmntlastname_creatable" />
                  <asp:button runat="server" class="CodeLookupControl" id="clmntlastnamepibtn" tabindex="2" onclientclick="return lookupData('clmntlastname','PIListclaimid=%claimid%&claimnumber=%claimnumber%&callingentity=Claimant',4,'clmnt',1);" />
                  <asp:button runat="server" class="EllipsisControl" id="clmntlastnamebtn" tabindex="2" onclientclick="return lookupData('clmntlastname','-4',4,'clmnt',1);" />
                  <img src="img\spacer.gif" width="10" height="1" />
                  <a href="#" name="clmntlastname" onClick="LinkHandler('','')&#xA;                    " class="">
                  </a>
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntprimaryclmntflag" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntprimaryclmntflag" Text="Primary Claimant" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="clmntprimaryclmntflag" RMXRef="Instance/Claimant/PrimaryClmntFlag" RMXType="checkbox" tabindex="16" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntfirstname" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntfirstname" Text="First Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmntfirstname" RMXRef="Instance/Claimant/ClaimantEntity/FirstName" RMXType="text" tabindex="3" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntclaimanttypecode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntclaimanttypecode" Text="Claimant Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="clmntclaimanttypecode" CodeTable="CLAIMANT_TYPE" ControlName="clmntclaimanttypecode" RMXRef="Instance/Claimant/ClaimantTypeCode" RMXType="code" tabindex="17" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntmiddlename" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntmiddlename" Text="Middle Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmntmiddlename" RMXRef="Instance/Claimant/ClaimantEntity/MiddleName" RMXType="text" tabindex="4" maxlength="50" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmnttitle" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmnttitle" Text="Title" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmnttitle" RMXRef="Instance/Claimant/ClaimantEntity/Title" RMXType="text" tabindex="19" maxlength="25" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntaddr1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntaddr1" Text="Address 1" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmntaddr1" RMXRef="Instance/Claimant/ClaimantEntity/Addr1" RMXType="text" tabindex="5" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmnttaxid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmnttaxid" Text="Soc. Sec. No." />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);" id="clmnttaxid" RMXRef="Instance/Claimant/ClaimantEntity/TaxId" RMXType="ssn" name="clmnttaxid" tabindex="20" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntaddr2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntaddr2" Text="Address 2" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmntaddr2" RMXRef="Instance/Claimant/ClaimantEntity/Addr2" RMXType="text" tabindex="6" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntbirthdate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntbirthdate" Text="Date Of Birth" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="clmntbirthdate" RMXRef="Instance/Claimant/ClaimantEntity/BirthDate" RMXType="date" tabindex="21" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="clmntbirthdatebtn" tabindex="22" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "clmntbirthdate",
					ifFormat : "%m/%d/%Y",
					button : "clmntbirthdatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntcity" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntcity" Text="City" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmntcity" RMXRef="Instance/Claimant/ClaimantEntity/City" RMXType="text" tabindex="7" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntsexcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntsexcode" Text="Sex" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="clmntsexcode" CodeTable="SEX_CODE" ControlName="clmntsexcode" RMXRef="Instance/Claimant/ClaimantEntity/SexCode" RMXType="code" tabindex="23" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntstateid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntstateid" Text="State" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="clmntstateid" CodeTable="states" ControlName="clmntstateid" RMXRef="Instance/Claimant/ClaimantEntity/StateId" RMXType="code" tabindex="8" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntzipcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntzipcode" Text="Zip" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="clmntzipcode" RMXRef="Instance/Claimant/ClaimantEntity/ZipCode" RMXType="zip" TabIndex="25" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntcountrycode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntcountrycode" Text="Country" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="clmntcountrycode" CodeTable="COUNTRY" ControlName="clmntcountrycode" RMXRef="Instance/Claimant/ClaimantEntity/CountryCode" RMXType="code" tabindex="10" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntsoldate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntsoldate" Text="SOL Date" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="clmntsoldate" RMXRef="Instance/Claimant/SolDate" RMXType="date" tabindex="26" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="clmntsoldatebtn" tabindex="27" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "clmntsoldate",
					ifFormat : "%m/%d/%Y",
					button : "clmntsoldatebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntphone1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntphone1" Text="Office Phone" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="clmntphone1" RMXRef="Instance/Claimant/ClaimantEntity/Phone1" RMXType="phone" tabindex="12" />
                </span>
              </div>
              <div runat="server" class="half" id="div_insurereid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insurereid" Text="Insurer" />
                <span class="formw">
                  <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="insurereid" RMXRef="Instance/Claimant/InsurerEid" RMXType="eidlookup" cancelledvalue="" tabindex="28" />
                  <asp:Button runat="server" class="EllipsisControl" id="insurereidbtn" tabindex="29" onclientclick="return lookupData('insurereid','INSURERS',4,'insurereid',2)" />
                  <asp:TextBox style="display:none" runat="server" id="insurereid_cid" RMXref="Instance/Claimant/InsurerEid/@codeid" cancelledvalue="" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntphone2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntphone2" Text="Home Phone" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="clmntphone2" RMXRef="Instance/Claimant/ClaimantEntity/Phone2" RMXType="phone" tabindex="13" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntfaxnumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntfaxnumber" Text="Fax" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="clmntfaxnumber" RMXRef="Instance/Claimant/ClaimantEntity/FaxNumber" RMXType="phone" tabindex="30" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmntinjurydescription" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntinjurydescription" Text="Injury Desc" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="clmntinjurydescription" RMXRef="Instance/Claimant/InjuryDescription" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="14" TextMode="MultiLine" Columns="30" rows="5" />
                  <asp:button runat="server" class="MemoButton" name="clmntinjurydescriptionbtnMemo" id="clmntinjurydescriptionbtnMemo" onclientclick="return EditMemo('clmntinjurydescription','');" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmntdamagedescription" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmntdamagedescription" Text="Damage Desc" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="clmntdamagedescription" RMXRef="Instance/Claimant/DamageDescription" RMXType="memo" readonly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="31" TextMode="MultiLine" Columns="30" rows="5" />
                  <asp:button runat="server" class="MemoButton" name="clmntdamagedescriptionbtnMemo" id="clmntdamagedescriptionbtnMemo" onclientclick="return EditMemo('clmntdamagedescription','');" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_dateofclaim" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="dateofclaim" RMXRef="Instance/UI/FormVariables/SysExData/dateofclaim" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_ev_depteid_cid" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="ev_depteid_cid" RMXRef="Instance/UI/FormVariables/SysExData/ev_depteid_cid" RMXType="hidden" />
                </span>
              </div>
              <div runat="server" class="half" id="div_ev_dateofevent" style="display:none;" xmlns="">
                <span class="formw">
                  <asp:TextBox style="display:none" runat="server" id="ev_dateofevent" RMXRef="Instance/UI/FormVariables/SysExData/ev_dateofevent" RMXType="hidden" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABclmntattorney" id="FORMTABclmntattorney">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="clmtattentityid" RMXRef="Instance/Claimant/AttorneyEntity/EntityId" RMXType="id" />
              <div runat="server" class="half" id="div_clmtattlastname" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattlastname" Text="Attorney Last Name" />
                <span class="formw">
                  <asp:Textbox runat="server" onchange="lookupTextChanged(this);" tabindex="33" onblur="lookupLostFocus(this);" RMXRef="Instance/Claimant/AttorneyEntity/LastName" RMXType="entitylookup" id="clmtattlastname" />
                  <asp:Textbox runat="server" style="display:none" Text="1" id="clmtattlastname_creatable" />
                  <asp:button runat="server" class="EllipsisControl" id="clmtattlastnamebtn" tabindex="34" onclientclick="return lookupData('clmtattlastname','ATTORNEYS',4,'clmtatt',1);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtattparenteid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattparenteid" Text="Firm Name" />
                <span class="formw">
                  <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="clmtattparenteid" RMXRef="Instance/Claimant/AttorneyEntity/ParentEid" RMXType="eidlookup" cancelledvalue="" tabindex="44" />
                  <asp:Button runat="server" class="EllipsisControl" id="clmtattparenteidbtn" tabindex="45" onclientclick="return lookupData('clmtattparenteid','ATTORNEY_FIRMS',4,'clmtattparenteid',2)" />
                  <asp:TextBox style="display:none" runat="server" id="clmtattparenteid_cid" RMXref="Instance/Claimant/AttorneyEntity/ParentEid/@codeid" cancelledvalue="" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmtattfirstname" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattfirstname" Text="First Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmtattfirstname" RMXRef="Instance/Claimant/AttorneyEntity/FirstName" RMXType="text" tabindex="35" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtatttitle" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtatttitle" Text="Title" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmtatttitle" RMXRef="Instance/Claimant/AttorneyEntity/Title" RMXType="text" tabindex="46" maxlength="25" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmtattmiddlename" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattmiddlename" Text="Middle Name" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmtattmiddlename" RMXRef="Instance/Claimant/AttorneyEntity/MiddleName" RMXType="text" tabindex="36" maxlength="50" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtattemailtypecode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattemailtypecode" Text="EMail Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="clmtattemailtypecode" CodeTable="EMAIL_TYPE" ControlName="clmtattemailtypecode" RMXRef="/Instance/Claimant/AttorneyEntity/EmailTypeCode" RMXType="code" tabindex="47" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmtattaddr1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattaddr1" Text="Address 1" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmtattaddr1" RMXRef="Instance/Claimant/AttorneyEntity/Addr1" RMXType="text" tabindex="37" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtattemailaddress" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattemailaddress" Text="EMail" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmtattemailaddress" RMXRef="Instance/Claimant/AttorneyEntity/EmailAddress" RMXType="text" tabindex="48" maxlength="100" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmtattaddr2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattaddr2" Text="Address 2" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmtattaddr2" RMXRef="Instance/Claimant/AttorneyEntity/Addr2" RMXType="text" tabindex="38" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtattphone1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattphone1" Text="Office Phone" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="clmtattphone1" RMXRef="Instance/Claimant/AttorneyEntity/Phone1" RMXType="phone" tabindex="49" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmtattcity" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattcity" Text="City" />
                <span class="formw">
                  <asp:TextBox runat="server" id="clmtattcity" RMXRef="Instance/Claimant/AttorneyEntity/City" RMXType="text" tabindex="39" onchange=";setDataChanged(true);" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtattphone2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattphone2" Text="Home Phone" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="clmtattphone2" RMXRef="Instance/Claimant/AttorneyEntity/Phone2" RMXType="phone" tabindex="50" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmtattstateid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattstateid" Text="State" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="clmtattstateid" CodeTable="states" ControlName="clmtattstateid" RMXRef="Instance/Claimant/AttorneyEntity/StateId" RMXType="code" tabindex="40" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtattfaxnumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattfaxnumber" Text="Fax" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="clmtattfaxnumber" RMXRef="Instance/Claimant/AttorneyEntity/FaxNumber" RMXType="phone" tabindex="51" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_clmtattcountrycode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattcountrycode" Text="Country" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="clmtattcountrycode" CodeTable="COUNTRY" ControlName="clmtattcountrycode" RMXRef="Instance/Claimant/AttorneyEntity/CountryCode" RMXType="code" tabindex="42" />
                </span>
              </div>
              <div runat="server" class="half" id="div_clmtattzipcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_clmtattzipcode" Text="Zip" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="clmtattzipcode" RMXRef="Instance/Claimant/AttorneyEntity/ZipCode" RMXType="zip" TabIndex="52" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="supp_claim_id" RMXRef="/Instance/*/Supplementals/CLAIM_ID" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="supp_claimant_row_id" RMXRef="/Instance/*/Supplementals/CLAIMANT_ROW_ID" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="supp_claimant_eid" RMXRef="/Instance/*/Supplementals/CLAIMANT_EID" RMXType="id" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_supp_fgzsf_date" xmlns="">
                <asp:label runat="server" class="label" id="lbl_supp_fgzsf_date" Text="gg" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="supp_fgzsf_date" RMXRef="/Instance/*/Supplementals/FGZSF_DATE" RMXType="date" tabindex="3006" onchange="dateLostFocus(this.id);setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                  <asp:button class="DateLookupControl" runat="server" id="supp_fgzsf_datebtn" tabindex="3007" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "supp_fgzsf_date",
					ifFormat : "%m/%d/%Y",
					button : "supp_fgzsf_datebtn"
					}
					);
				</script>
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnClaimHistory">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnClaimHistory" RMXRef="" Text="Claim History" onClientClick="return displayclaimhistory('claimant');" />
        </div>
        <div class="formButton" runat="server" id="div_btnFinancials">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnFinancials" RMXRef="" Text="Financials" visible="false" onClientClick="return gotoreserves();" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Claimant&gt;&lt;ClaimantEntity&gt;&lt;/ClaimantEntity&gt;&lt;AttorneyEntity&gt;&lt;/AttorneyEntity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Claimant&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Claimant" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="claimant" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="claimantrowid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="claimantlist" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="claimid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="150" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="claimid,claimnumber" />
      <asp:TextBox style="display:none" runat="server" id="SysNotReqNew" RMXRef="Instance/UI/FormVariables/SysNotReqNew" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysLookupClass" RMXRef="Instance/UI/FormVariables/SysLookupClass" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysLookupRecordId" RMXRef="Instance/UI/FormVariables/SysLookupRecordId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysLookupAttachNodePath" RMXRef="Instance/UI/FormVariables/SysLookupAttachNodePath" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="claimant" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="clmntlastname|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="clmntlastname|clmtattlastname|" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>