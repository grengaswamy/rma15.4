<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entitymaint.aspx.cs"  Inherits="Riskmaster.UI.FDM.Entitymaint" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>
    </title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_new" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateNew" src="../../Images/new.gif" width="28" height="28" border="0" id="new" AlternateText="New" onMouseOver="this.src='../../Images/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/new.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return OnSaveButtonClicked();" OnClick="NavigateSave" src="../../Images/save.gif" width="28" height="28" border="0" id="save" AlternateText="Save" ValidationGroup="vgSave" onMouseOver="this.src='../../Images/save2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateFirst" src="../../Images/first.gif" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='../../Images/first2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/first.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigatePrev" src="../../Images/prev.gif" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='../../Images/prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/prev.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateNext" src="../../Images/next.gif" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='../../Images/next2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/next.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateLast" src="../../Images/last.gif" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='../../Images/last2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/last.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_delete" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateDelete" src="../../Images/delete.gif" width="28" height="28" border="0" id="delete" AlternateText="Delete Record" onMouseOver="this.src='../../Images/delete2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/delete.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_lookup" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="if(!doLookup()) return false;" src="../../Images/lookup.gif" width="28" height="28" border="0" id="lookup" AlternateText="Lookup Data" onMouseOver="this.src='../../Images/lookup2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/lookup.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_search" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return doSearch();" src="../../Images/search.gif" width="28" height="28" border="0" id="search" AlternateText="Search" onMouseOver="this.src='../../Images/search2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/search.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return attach();" src="../../Images/attach.gif" width="28" height="28" border="0" id="attach" AlternateText="Attach Documents" onMouseOver="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/attach.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_comments" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return Comments();" src="../../Images/comments.gif" width="28" height="28" border="0" id="comments" AlternateText="Comments" onMouseOver="this.src='../../Images/comments2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/comments.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_mailmerge" xmlns="">
          <asp:ImageButton runat="server" OnClientClick="return MailMerge();" src="../../Images/mailmerge.gif" width="28" height="28" border="0" id="mailmerge" AlternateText="Mail Merge" onMouseOver="this.src='../../Images/mailmerge2.gif';this.style.zoom='110%'" onMouseOut="this.src='../../Images/mailmerge.gif';this.style.zoom='100%'" />
        </div>
      </div>
      <br />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSentity" id="TABSentity">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="entity" id="LINKTABSentity">Entity</a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSoperatingas" id="TABSoperatingas">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="operatingas" id="LINKTABSoperatingas">Operating As</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABScontactinfo" id="TABScontactinfo">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="contactinfo" id="LINKTABScontactinfo">Contact Info</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="suppgroup" id="LINKTABSsuppgroup">Supplementals</a>
        </div>
        <div class="tabSpace">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABentity" id="FORMTABentity">
        <asp:TextBox style="display:none" runat="server" id="entityid" RMXRef="/Instance/Entity/EntityId" RMXType="id" />
        <div runat="server" class="half" id="div_entitytableidtext" style="display:none;" xmlns="">
          <span class="formw">
            <asp:TextBox style="display:none" runat="server" id="entitytableidtext" RMXRef="Instance/UI/FormVariables/SysExData/EntityType" RMXType="hidden" />
          </span>
        </div>
        <div runat="server" class="half" id="div_entitytableid" xmlns="">
          <asp:label runat="server" class="required" id="lbl_entitytableid" Text="Type Of Entity" />
          <span class="formw">
            <asp:DropDownList runat="server" id="entitytableid" tabindex="1" RMXRef="Instance/UI/FormVariables/SysExData/EntityType" RMXType="combobox" ItemSetRef="/Instance/UI/FormVariables/SysExData/EntityTypeList/option" onchange="onCboChange();" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="entitytableid" ID="rfv_entitytableid" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="hdPostBack" RMXRef="/Instance/UI/FormVariables/SysExData/IsPostBack" RMXType="id" />
        <script language="JavaScript" xmlns="">
            if (document.forms[0].hdPostBack.value == "1")
			{
				setDataChanged(true);
				document.forms[0].hdPostBack.value = "0";
			}
		</script>
        <div runat="server" class="half" id="div_entitytableidtextname" xmlns="">
          <asp:label runat="server" class="label" id="lbl_entitytableidtextname" Text="Type Of Entity" />
          <span class="formw">
            <asp:TextBox runat="Server" RMXref="Instance/UI/FormVariables/SysExData/EntityTypeName" id="entitytableidtextname" tabindex="1" style="background-color: silver;" readonly="true" />
          </span>
        </div>
        <div runat="server" class="half" id="div_lastname" xmlns="">
          <asp:label runat="server" class="required" id="lbl_lastname" Text="Name" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="lastname" RMXRef="/Instance/Entity/LastName" RMXType="text" tabindex="2" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="lastname" ID="rfv_lastname" />
          </span>
        </div>
        <div runat="server" class="half" id="div_taxid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_taxid" Text="Tax Id" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="taxidLostFocus(this);" id="taxid" RMXRef="/Instance/Entity/TaxId" RMXType="taxid" name="taxid" value="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_contact" xmlns="">
          <asp:label runat="server" class="label" id="lbl_contact" Text="Contact" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="contact" RMXRef="/Instance/Entity/Contact" RMXType="text" tabindex="3" />
          </span>
        </div>
        <div runat="server" class="half" id="div_phone1" xmlns="">
          <asp:label runat="server" class="label" id="lbl_phone1" Text="Office Phone" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="phone1" RMXRef="/Instance/Entity/Phone1" RMXType="phone" tabindex="21" />
          </span>
        </div>
        <div runat="server" class="half" id="div_alsoknownas" xmlns="">
          <asp:label runat="server" class="label" id="lbl_alsoknownas" Text="DBA" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="alsoknownas" RMXRef="/Instance/Entity/AlsoKnownAs" RMXType="text" tabindex="4" />
          </span>
        </div>
        <div runat="server" class="half" id="div_phone2" xmlns="">
          <asp:label runat="server" class="label" id="lbl_phone2" Text="Alt. Phone" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="phone2" RMXRef="/Instance/Entity/Phone2" RMXType="phone" tabindex="22" />
          </span>
        </div>
        <div runat="server" class="half" id="div_abbreviation" xmlns="">
          <asp:label runat="server" class="required" id="lbl_abbreviation" Text="Abbreviation" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="abbreviation" RMXRef="/Instance/Entity/Abbreviation" RMXType="text" tabindex="5" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="vgSave" ErrorMessage="Required" runat="server" ControlToValidate="abbreviation" ID="rfv_abbreviation" />
          </span>
        </div>
        <div runat="server" class="half" id="div_faxnumber" xmlns="">
          <asp:label runat="server" class="label" id="lbl_faxnumber" Text="Fax" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="faxnumber" RMXRef="/Instance/Entity/FaxNumber" RMXType="phone" tabindex="23" />
          </span>
        </div>
        <div runat="server" class="half" id="div_addr1" xmlns="">
          <asp:label runat="server" class="label" id="lbl_addr1" Text="Address 1" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="addr1" RMXRef="/Instance/Entity/Addr1" RMXType="text" tabindex="6" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emailtypecode" xmlns="">
          <asp:label runat="server" class="label" id="lbl_emailtypecode" Text="EMail Type" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="emailtypecode" CodeTable="EMAIL_TYPE" ControlName="emailtypecode" RMXRef="/Instance/Entity/EmailTypeCode" RMXType="code" tabindex="24" />
          </span>
        </div>
        <div runat="server" class="half" id="div_addr2" xmlns="">
          <asp:label runat="server" class="label" id="lbl_addr2" Text="Address 2" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="addr2" RMXRef="/Instance/Entity/Addr2" RMXType="text" tabindex="7" />
          </span>
        </div>
        <div runat="server" class="half" id="div_emailaddress" xmlns="">
          <asp:label runat="server" class="label" id="lbl_emailaddress" Text="EMail" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="emailaddress" RMXRef="/Instance/Entity/EmailAddress" RMXType="text" tabindex="26" />
          </span>
        </div>
        <div runat="server" class="half" id="div_city" xmlns="">
          <asp:label runat="server" class="label" id="lbl_city" Text="City" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="city" RMXRef="/Instance/Entity/City" RMXType="text" tabindex="8" />
          </span>
        </div>
        <div runat="server" class="half" id="div_parenteid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_parenteid" Text="Parent" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="parenteid" RMXRef="/Instance/Entity/ParentEid" RMXType="eidlookup" cancelledvalue="" tabindex="27" />
            <input type="button" class="button" value="..." name="parenteidbtn" tabindex="28" onclick="lookupData('parenteid','',4,'parenteid',2)" />
            <asp:TextBox style="display:none" runat="server" id="parenteid_cid" RMXref="/Instance/Entity/ParentEid/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_stateid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_stateid" Text="State" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="stateid" CodeTable="states" ControlName="stateid" RMXRef="/Instance/Entity/StateId" RMXType="code" tabindex="9" />
          </span>
        </div>
        <div runat="server" class="half" id="div_zipcode" xmlns="">
          <asp:label runat="server" class="label" id="lbl_zipcode" Text="Zip/Postal Code" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="zipcode" RMXRef="/Instance/Entity/ZipCode" RMXType="zip" TabIndex="11" />
          </span>
        </div>
        <div runat="server" class="half" id="div_county" xmlns="">
          <asp:label runat="server" class="label" id="lbl_county" Text="County" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="county" RMXRef="/Instance/Entity/County" RMXType="text" tabindex="12" />
          </span>
        </div>
        <div runat="server" class="half" id="div_entity1099reportable" xmlns="">
          <asp:label runat="server" class="label" id="lbl_entity1099reportable" Text="1099 Reportable" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="entity1099reportable" RMXRef="/Instance/Entity/Entity1099Reportable" RMXType="checkbox" tabindex="29" />
          </span>
        </div>
        <div runat="server" class="half" id="div_countrycode" xmlns="">
          <asp:label runat="server" class="label" id="lbl_countrycode" Text="Country" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="countrycode" CodeTable="COUNTRY" ControlName="countrycode" RMXRef="/Instance/Entity/CountryCode" RMXType="code" tabindex="13" />
          </span>
        </div>
        <div runat="server" class="half" id="div_parent1099eid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_parent1099eid" Text="1099 Parent" />
          <span class="formw">
            <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);" id="parent1099eid" RMXRef="/Instance/Entity/Parent1099EID" RMXType="eidlookup" cancelledvalue="" tabindex="30" />
            <input type="button" class="button" value="..." name="parent1099eidbtn" tabindex="31" onclick="lookupData('parent1099eid','',4,'parent1099eid',2)" />
            <asp:TextBox style="display:none" runat="server" id="parent1099eid_cid" RMXref="/Instance/Entity/Parent1099EID/@codeid" cancelledvalue="" />
          </span>
        </div>
        <div runat="server" class="half" id="div_title" xmlns="">
          <asp:label runat="server" class="label" id="lbl_title" Text="Title" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="title" RMXRef="/Instance/Entity/Title" RMXType="text" tabindex="15" />
          </span>
        </div>
        <div runat="server" class="half" id="div_freezepayments" xmlns="">
          <asp:label runat="server" class="label" id="lbl_freezepayments" Text="Freeze Payments" />
          <span class="formw">
            <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="freezepayments" RMXRef="/Instance/Entity/FreezePayments" RMXType="checkbox" tabindex="32" />
          </span>
        </div>
        <div runat="server" class="half" id="div_siccode" xmlns="">
          <asp:label runat="server" class="label" id="lbl_siccode" Text="SIC Code" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="siccode" CodeTable="SIC_CODE" ControlName="siccode" RMXRef="/Instance/Entity/SicCode" RMXType="code" tabindex="16" />
          </span>
        </div>
        <div runat="server" class="half" id="div_natureofbusiness" xmlns="">
          <asp:label runat="server" class="label" id="lbl_natureofbusiness" Text="Nature Of Business" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="natureofbusiness" RMXRef="/Instance/Entity/NatureOfBusiness" RMXType="text" tabindex="33" />
          </span>
        </div>
        <div runat="server" class="half" id="div_naicscode" xmlns="">
          <asp:label runat="server" class="label" id="lbl_naicscode" Text="NAICS Code" />
          <span class="formw">
            <uc:CodeLookUp runat="server" ID="naicscode" CodeTable="NAICS_CODES" ControlName="naicscode" RMXRef="/Instance/Entity/NaicsCode" RMXType="code" tabindex="18" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="checkssn" RMXRef="/Instance/UI/FormVariables/SysExData/CheckSSN" RMXType="id" />
        <div runat="server" class="half" id="div_npinumber" xmlns="">
          <asp:label runat="server" class="label" id="lbl_npinumber" Text="NPI Number" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="npinumber" RMXRef="/Instance/Entity/NPINumber" RMXType="text" tabindex="19" />
          </span>
        </div>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABoperatingas" id="FORMTABoperatingas">
        <div runat="server" class="partial" id="div_EntityXOperatingAsGrid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_EntityXOperatingAsGrid" Text="" />
          <span>
            <dg:UserControlDataGrid runat="server" ID="EntityXOperatingAsGrid" GridName="EntityXOperatingAsGrid" GridTitle="Operating Names" Target="/Instance/UI/FormVariables/SysExData/EntityXOperatingAs" Unique_Id="OperatingId" ShowRadioButton="" Width="680px" Height="150px" hidenodes="|OperatingId|EntityId|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsSelectedId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsGrid_Action" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsGrid_RowAddedFlag" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="EntityXOperatingAsGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXOperatingAsGrid_RowDeletedFlag" RMXType="id" />
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABcontactinfo" id="FORMTABcontactinfo">
        <div runat="server" class="partial" id="div_EntityXContactInfoGrid" xmlns="">
          <asp:label runat="server" class="label" id="lbl_EntityXContactInfoGrid" Text="" />
          <span>
            <dg:UserControlDataGrid runat="server" ID="EntityXContactInfoGrid" GridName="EntityXContactInfoGrid" GridTitle="Contact Names" Target="/Instance/UI/FormVariables/SysExData/EntityXContactInfo" Unique_Id="ContactId" ShowRadioButton="" Width="680px" Height="150px" hidenodes="|ContactId|EntityId|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" />
          </span>
        </div>
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoSelectedId" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoGrid_Action" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoGrid_Action" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoGrid_RowAddedFlag" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="EntityXContactInfoGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/EntityXContactInfoGrid_RowDeletedFlag" RMXType="id" />
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABsuppgroup" id="FORMTABsuppgroup">
        <asp:TextBox style="display:none" runat="server" id="supp_entity_id" RMXRef="/Instance/*/Supplementals/ENTITY_ID" RMXType="id" />
        <div runat="server" class="half" id="div_supp_index_bureau_numb" xmlns="">
          <asp:label runat="server" class="label" id="lbl_supp_index_bureau_numb" Text="Claim Index Bureau Number" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_index_bureau_numb" RMXRef="/Instance/*/Supplementals/INDEX_BUREAU_NUMB" RMXType="text" tabindex="1653" />
          </span>
        </div>
        <div runat="server" class="half" id="div_supp_ste_agnt_bru_idnt" xmlns="">
          <asp:label runat="server" class="label" id="lbl_supp_ste_agnt_bru_idnt" Text="Pennsylvania Bureau Code" />
          <span class="formw">
            <asp:TextBox runat="server" onchange="setDataChanged(true);" id="supp_ste_agnt_bru_idnt" RMXRef="/Instance/*/Supplementals/STE_AGNT_BRU_IDNT" RMXType="text" tabindex="1654" />
          </span>
        </div>
      </div>
      <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:Button class="button" runat="Server" id="BackToParent" Text="Back " OnClientClick="if(!( XFormHandler('','','back')))return false;" PostBackUrl="?" />
        </div>
        <div class="formButton" runat="server" id="div_btnProviderContracts">
          <asp:Button class="button" runat="Server" id="btnProviderContracts" Text="Contracts" RMXRef="/Instance/UI/FormVariables/SysExData/ContractCount" OnClientClick="if(!( XFormHandler('SysFormName=providercontract&SysCmd=1&SysEx=/Instance/Entity/EntityId | /Instance/Entity/EntityTableId','','')))return false;" PostBackUrl="?SysFormName=providercontract&SysCmd=1&SysEx=/Instance/Entity/EntityId | /Instance/Entity/EntityTableId" />
        </div>
        <div class="formButton" runat="server" id="div_btnCodeOrLicenseJurisdictions">
          <asp:Button class="button" runat="Server" id="btnCodeOrLicenseJurisdictions" Text="Jurisdictional License Or Code" OnClientClick="if(!( XFormHandler('SysFormName=jurisdictionlicensecodes&SysCmd=1&SysEx=/Instance/Entity/EntityId | /Instance/Entity/EntityTableId','','')))return false;" PostBackUrl="?SysFormName=jurisdictionlicensecodes&SysCmd=1&SysEx=/Instance/Entity/EntityId | /Instance/Entity/EntityTableId" />
        </div>
        <div class="formButton" runat="server" id="div_btnExposureInformation">
          <asp:Button class="button" runat="Server" id="btnExposureInformation" Text="Exposure Information" OnClientClick="if(!( XFormHandler('SysFormName=entityexposure&SysCmd=1&SysEx=/Instance/Entity/EntityId | /Instance/Entity/EntityTableId | /Instance/Entity/LastName','','')))return false;" PostBackUrl="?SysFormName=entityexposure&SysCmd=1&SysEx=/Instance/Entity/EntityId | /Instance/Entity/EntityTableId | /Instance/Entity/LastName" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="Entity" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;Entity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/Entity&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="16500" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="entityid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="entitytableid,entitytype" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="entitymaint" />
      <asp:TextBox style="display:none" runat="server" id="SysPostback" RMXRef="" RMXType="hidden" Text="entitytableidtext" />
      <asp:TextBox style="display:none" runat="server" name="formname" value="entitymaint" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" value="entitytableid|lastname|abbreviation|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" value="entitytableid|entitytableidtextname|" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>