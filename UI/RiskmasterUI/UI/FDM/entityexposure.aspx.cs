﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace Riskmaster.UI.FDM
{
    public partial class Entityexposure : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //MGaba2:In case exposure is opened from org hierarchy maintenance screen,back should call another function
            //MITS 16352:05/11/2009:Start
            if (!IsPostBack)
            {
                if (AppHelper.GetQueryStringValue("SysExternalParam") != string.Empty)
                {
                    Control oBtnBack = this.Page.FindControl("BackToParent");
                    //Mits 28737
                    if (oBtnBack.ToString().Contains("ImageButton"))
                    {
                        ((ImageButton)oBtnBack).OnClientClick = "return BackToOrgAddEdit();";
                    }
                    else if (oBtnBack != null)
                    {
                        ((Button)oBtnBack).OnClientClick = "return BackToOrgAddEdit();";
                    }
                        
                }
            }
            //MITS 16352:05/11/2009:End
            FDMPageLoad();
        }        
    }
}
