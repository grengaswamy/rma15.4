<%@  Page Language="C#" AutoEventWireup="true" CodeBehind="cmxtreatmentpln.aspx.cs"
    Inherits="Riskmaster.UI.FDM.Cmxtreatmentpln" ValidateRequest="false" %>
<%@  Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Treatment Plan / Utilization Review</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js">
          {var i;}
    </script>

    <script language="JavaScript" src="../../Scripts/drift.js">
          {var i;}
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
    <asp:label id="lblError" runat="server" text="" forecolor="Red" />
    <asp:hiddenfield runat="server" id="wsrp_rewrite_action_1" value="" />
    <asp:textbox style="display: none" runat="server" name="hTabName" id="hTabName" />
    <asp:scriptmanager id="SMgr" runat="server" />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
            <asp:imagebutton runat="server" onclientclick="return OnSaveButtonClicked();" onclick="NavigateSave"
                src="../../Images/save.gif" width="28" height="28" border="0" id="save" alternatetext="Save"
                validationgroup="vgSave" onmouseover="this.src='../../Images/save2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/save.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_attach" xmlns="">
            <asp:imagebutton runat="server" onclientclick="attach()" src="../../Images/attach.gif"
                width="28" height="28" border="0" id="attach" alternatetext="Attach Documents"
                onmouseover="this.src='../../Images/attach2.gif';this.style.zoom='110%'" onmouseout="this.src='../../Images/attach.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_filtereddiary" xmlns="">
            <asp:imagebutton runat="server" onclientclick="FilteredDiary()" src="../../Images/filtereddiary.gif"
                width="28" height="28" border="0" id="filtereddiary" alternatetext="View Record Diaries"
                onmouseover="this.src='../../Images/filtereddiary2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/filtereddiary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_diary" xmlns="">
            <asp:imagebutton runat="server" onclientclick="Diary()" src="../../Images/diary.gif"
                width="28" height="28" border="0" id="diary" alternatetext="Diary" onmouseover="this.src='../../Images/diary2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/diary.gif';this.style.zoom='100%'" />
        </div>
        <div class="toolBarButton" runat="server" id="div_recordsummary" xmlns="">
            <asp:imagebutton runat="server" onclientclick="recordSummary()" src="../../Images/recordsummary.gif"
                width="28" height="28" border="0" id="recordsummary" alternatetext="Record Summary"
                onmouseover="this.src='../../Images/recordsummary2.gif';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/recordsummary.gif';this.style.zoom='100%'" />
        </div>
    </div>
    <br />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Treatment Plan / Utilization Review" />
        <asp:label id="formsubtitle" runat="server" text="" />
    </div>
    <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" text="" />
    </div>
    <div id="Div2" class="tabGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSTreatmentPln" id="TABSTreatmentPln">
            <a class="Selected" href="#" onclick="tabChange(this.name);" rmxref="" name="TreatmentPln"
                id="LINKTABSTreatmentPln"><span style="text-decoration: none">Treatment Plan</span>
            </a>
        </div>
        <div id="Div3" class="tabSpace" runat="server">
            &nbsp;&nbsp;
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSProvider" id="TABSProvider">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="Provider"
                id="LINKTABSProvider"><span style="text-decoration: none">Provider</span> </a>
        </div>
        <div class="tabSpace">
            &nbsp;&nbsp;
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSReview" id="TABSReview">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="Review"
                id="LINKTABSReview"><span style="text-decoration: none">Review</span> </a>
        </div>
        <div class="tabSpace">
            &nbsp;&nbsp;
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSAppeal" id="TABSAppeal">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="Appeal"
                id="LINKTABSAppeal"><span style="text-decoration: none">Appeal</span> </a>
        </div>
        <div class="tabSpace">
            &nbsp;&nbsp;
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSCriteria" id="TABSCriteria">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="Criteria"
                id="LINKTABSCriteria"><span style="text-decoration: none">Criteria</span> </a>
        </div>
        <div class="tabSpace">
            &nbsp;&nbsp;
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSsuppgroup" id="TABSsuppgroup">
            <a class="NotSelected1" href="#" onclick="tabChange(this.name);" rmxref="" name="suppgroup"
                id="LINKTABSsuppgroup"><span style="text-decoration: none">Supplementals</span>
            </a>
        </div>
        <div class="tabSpace">
            &nbsp;&nbsp;
        </div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABTreatmentPln" id="FORMTABTreatmentPln">
        <asp:textbox style="display: none" runat="server" id="cmtprowid" rmxref="/Instance/CmXTreatmentPln/CmtpRowId"
            rmxtype="id" />
        <asp:textbox style="display: none" runat="server" id="casemgtrowid" rmxref="/Instance/CmXTreatmentPln/CasemgtRowId"
            rmxtype="id" />
        <div runat="server" class="half" id="div_planstatuscode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_planstatuscode" text="Plan Status" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="planstatuscode"
                    rmxref="/Instance/CmXTreatmentPln/PlanStatusCode" rmxtype="code" cancelledvalue=""
                    tabindex="1" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="planstatuscodebtn" onclientclick="return selectCode('TR_PLAN_STATUS','planstatuscode');"
                    tabindex="2" />
                <asp:textbox style="display: none" runat="server" id="planstatuscode_cid" rmxref="/Instance/CmXTreatmentPln/PlanStatusCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_daterequested" xmlns="">
            <asp:label runat="server" class="label" id="lbl_daterequested" text="Date Requested" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="calculatedaysbetweendates('dateapproved','daterequested','daysfromrequesttoapprove');;setDataChanged(true);&#xA;              "
                    id="daterequested" rmxref="/Instance/CmXTreatmentPln/DateRequested" rmxtype="datebuttonscript"
                    name="daterequested" />

                <script language="JavaScript" src="">{var i;}</script>

                <asp:button runat="server" type="button" class="button" id="daterequestedbtn" text="..."
                    tabindex="18" onclientclick="calculatedaysbetweendates('dateapproved','daterequested','daysfromrequesttoapprove');" />
                <cc1:CalendarExtender runat="server" ID="daterequested_ajax" TargetControlID="daterequested" />
            </span>
        </div>
        <div runat="server" class="half" id="div_requesttypecode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_requesttypecode" text="Request Type" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="requesttypecode"
                    rmxref="/Instance/CmXTreatmentPln/RequestTypeCode" rmxtype="code" cancelledvalue=""
                    tabindex="3" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="requesttypecodebtn" onclientclick="return selectCode('REQUEST_TYPE','requesttypecode');"
                    tabindex="4" />
                <asp:textbox style="display: none" runat="server" id="requesttypecode_cid" rmxref="/Instance/CmXTreatmentPln/RequestTypeCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_noreqvisits" xmlns="">
            <asp:label runat="server" class="label" id="lbl_noreqvisits" text="Requested No.Visits" />
            <span class="formw">
                <asp:textbox runat="server" onblur="numLostFocus(this);" id="noreqvisits" rmxref="/Instance/CmXTreatmentPln/NoReqVisits"
                    rmxtype="numeric" tabindex="19" onchange="calculatesavedvisits()" />
            </span>
        </div>
        <div runat="server" class="half" id="div_requestedtreatment" xmlns="">
            <asp:label runat="server" class="label" id="lbl_requestedtreatment" text="Requested Treatment" />
            <span class="formw">
                <asp:textbox runat="Server" id="requestedtreatment" rmxref="/Instance/CmXTreatmentPln/RequestedTreatment"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="5" textmode="MultiLine" columns="30" rows="5" />
                <input type="button" class="button" value="..." name="requestedtreatmentbtnMemo"
                    tabindex="6" id="requestedtreatmentbtnMemo" onclick="EditMemo('requestedtreatment','')" />
            </span>
        </div>
        <div runat="server" class="half" id="div_treatmenttypecode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_treatmenttypecode" text="Treatment Type" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="treatmenttypecode"
                    rmxref="/Instance/CmXTreatmentPln/TreatmentTypeCode" rmxtype="code" cancelledvalue=""
                    tabindex="7" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="treatmenttypecodebtn" onclientclick="return selectCode('TREAT_TYPE','treatmenttypecode');"
                    tabindex="8" />
                <asp:textbox style="display: none" runat="server" id="treatmenttypecode_cid" rmxref="/Instance/CmXTreatmentPln/TreatmentTypeCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_procedurecode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_procedurecode" text="Procedure" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="procedurecode"
                    rmxref="/Instance/CmXTreatmentPln/ProcedureCode" rmxtype="code" cancelledvalue=""
                    tabindex="20" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="procedurecodebtn" onclientclick="return selectCode('CPT_PROC','procedurecode');"
                    tabindex="21" />
                <asp:textbox style="display: none" runat="server" id="procedurecode_cid" rmxref="/Instance/CmXTreatmentPln/ProcedureCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_reason" xmlns="">
            <asp:label runat="server" class="label" id="lbl_reason" text="Reason" />
            <span class="formw">
                <asp:textbox runat="Server" id="reason" rmxref="/Instance/CmXTreatmentPln/Reason"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="9" textmode="MultiLine" columns="30" rows="5" />
                <input type="button" class="button" value="..." name="reasonbtnMemo" tabindex="10"
                    id="reasonbtnMemo" onclick="EditMemo('reason','')" />
            </span>
        </div>
        <div runat="server" class="half" id="div_approvedtreatment" xmlns="">
            <asp:label runat="server" class="label" id="lbl_approvedtreatment" text="Approved Treatment" />
            <span class="formw">
                <asp:textbox runat="Server" id="approvedtreatment" rmxref="/Instance/CmXTreatmentPln/ApprovedTreatment"
                    rmxtype="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);"
                    tabindex="22" textmode="MultiLine" columns="30" rows="5" />
                <input type="button" class="button" value="..." name="approvedtreatmentbtnMemo" tabindex="23"
                    id="approvedtreatmentbtnMemo" onclick="EditMemo('approvedtreatment','')" />
            </span>
        </div>
        <div runat="server" class="half" id="div_noappvisits" xmlns="">
            <asp:label runat="server" class="label" id="lbl_noappvisits" text="Approved No.Visits" />
            <span class="formw">
                <asp:textbox runat="server" onblur="numLostFocus(this);" id="noappvisits" rmxref="/Instance/CmXTreatmentPln/NoAppVisits"
                    rmxtype="numeric" tabindex="11" onchange="calculatesavedvisits()" />
            </span>
        </div>
        <div runat="server" class="half" id="div_savedvisits" xmlns="">
            <asp:label runat="server" class="label" id="lbl_savedvisits" text="Saved Visits" />
            <span class="formw">
                <asp:textbox runat="Server" rmxref="/Instance/UI/FormVariables/SysExData/SavedVisits"
                    id="savedvisits" tabindex="24" style="background-color: silver;" readonly="true" />
            </span>
        </div>
        <div runat="server" class="half" id="div_dateauthstart" xmlns="">
            <asp:label runat="server" class="label" id="lbl_dateauthstart" text="Authorized Date Range" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);" id="dateauthstart"
                    rmxref="/Instance/CmXTreatmentPln/DateAuthStart" rmxtype="date" tabindex="12" />
                <cc1:CalendarExtender runat="server" ID="dateauthstart_ajax" TargetControlID="dateauthstart" />
            </span>
        </div>
        <div runat="server" class="half" id="div_dateauthend" xmlns="">
            <asp:label runat="server" class="label" id="lbl_dateauthend" text="" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);" id="dateauthend"
                    rmxref="/Instance/CmXTreatmentPln/DateAuthEnd" rmxtype="date" tabindex="25" />
                <cc1:CalendarExtender runat="server" ID="dateauthend_ajax" TargetControlID="dateauthend" />
            </span>
        </div>
        <div runat="server" class="half" id="div_approvedbyeidlist" xmlns="">
            <asp:label runat="server" class="label" id="lbl_approvedbyeidlist" text="Approved By" />
            <span class="formw">
                <asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                    id="approvedbyeidlist" rmxref="/Instance/CmXTreatmentPln/ApprovedByEid" rmxtype="eidlookup"
                    cancelledvalue="" tabindex="14" />
                <input type="button" class="button" value="..." name="approvedbyeidlistbtn" tabindex="15"
                    onclick="lookupData('approvedbyeidlist','0',4,'approvedbyeidlist',2)" />
                <asp:textbox style="display: none" runat="server" id="approvedbyeidlist_cid" rmxref="/Instance/CmXTreatmentPln/ApprovedByEid/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_dateapproved" xmlns="">
            <asp:label runat="server" class="label" id="lbl_dateapproved" text="Date Approved" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="calculatedaysbetweendates('dateapproved','daterequested','daysfromrequesttoapprove');;setDataChanged(true);&#xA;              "
                    id="dateapproved" rmxref="/Instance/CmXTreatmentPln/ApprovedDate" rmxtype="datebuttonscript"
                    name="dateapproved" />

                <script language="JavaScript" src="">{var i;}</script>

                <asp:button runat="server" type="button" class="button" id="dateapprovedbtn" text="..."
                    tabindex="28" onclientclick="calculatedaysbetweendates('dateapproved','daterequested','daysfromrequesttoapprove');" />
                <cc1:CalendarExtender runat="server" ID="dateapproved_ajax" TargetControlID="dateapproved" />
            </span>
        </div>
        <div runat="server" class="half" id="div_daysfromrequesttoapprove" xmlns="">
            <asp:label runat="server" class="label" id="lbl_daysfromrequesttoapprove" text="Days from Request to Approve" />
            <span class="formw">
                <asp:textbox runat="Server" rmxref="/Instance/UI/FormVariables/SysExData/DaysFromReqToApp"
                    id="daysfromrequesttoapprove" tabindex="16" style="background-color: silver;"
                    readonly="true" />
            </span>
        </div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABProvider"
        id="FORMTABProvider">
        <div runat="server" class="half" id="div_providerlastname" xmlns="">
            <asp:label runat="server" class="label" id="lbl_providerlastname" text="Provider Group" />
            <span class="formw">
                <asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                    id="providerlastname" rmxref="/Instance/CmXTreatmentPln/ProviderEid" rmxtype="eidlookup"
                    cancelledvalue="" tabindex="29" />
                <input type="button" class="button" value="..." name="providerlastnamebtn" tabindex="30"
                    onclick="lookupData('providerlastname','',4,'provider',1)" />
                <asp:textbox style="display: none" runat="server" id="providerlastname_cid" rmxref="/Instance/CmXTreatmentPln/ProviderEid/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_providerphone1" xmlns="">
            <asp:label runat="server" class="label" id="lbl_providerphone1" text="Provider Phone" />
            <span class="formw">
                <asp:textbox runat="Server" rmxref="/Instance/UI/FormVariables/SysExData/ProviderPhone"
                    id="providerphone1" tabindex="32" style="background-color: silver;" readonly="true" />
            </span>
        </div>
        <div runat="server" class="half" id="div_treatmentprovider" xmlns="">
            <asp:label runat="server" class="label" id="lbl_treatmentprovider" text="Treatment Provider" />
            <span class="formw">
                <asp:textbox runat="server" onchange="setDataChanged(true);" id="treatmentprovider"
                    rmxref="/Instance/CmXTreatmentPln/TreatmentProvider" rmxtype="text" />
            </span>
        </div>
        <asp:textbox style="display: none" runat="server" id="providerentityid" rmxref="/Instance/CmXTreatmentPln/ProviderEid/@codeid"
            rmxtype="id" />
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABReview"
        id="FORMTABReview">
        <div runat="server" class="half" id="div_reviewtypecode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_reviewtypecode" text="Review Type" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="reviewtypecode"
                    rmxref="/Instance/CmXTreatmentPln/ReviewTypeCode" rmxtype="code" cancelledvalue=""
                    tabindex="33" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="reviewtypecodebtn" onclientclick="return selectCode('REVIEW_TYPE','reviewtypecode');"
                    tabindex="34" />
                <asp:textbox style="display: none" runat="server" id="reviewtypecode_cid" rmxref="/Instance/CmXTreatmentPln/ReviewTypeCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_reviewereidlist" xmlns="">
            <asp:label runat="server" class="label" id="lbl_reviewereidlist" text="Reviewed By" />
            <span class="formw">
                <asp:textbox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                    id="reviewereidlist" rmxref="/Instance/CmXTreatmentPln/ReviewerEid" rmxtype="eidlookup"
                    cancelledvalue="" tabindex="38" />
                <input type="button" class="button" value="..." name="reviewereidlistbtn" tabindex="39"
                    onclick="lookupData('reviewereidlist','0',4,'reviewereidlist',2)" />
                <asp:textbox style="display: none" runat="server" id="reviewereidlist_cid" rmxref="/Instance/CmXTreatmentPln/ReviewerEid/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
        <div runat="server" class="half" id="div_datereferral" xmlns="">
            <asp:label runat="server" class="label" id="lbl_datereferral" text="Date Referred" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="calculatedaysbetweendates('datereceived','datereferral','daysforreview');;setDataChanged(true);&#xA;              "
                    id="datereferral" rmxref="/Instance/CmXTreatmentPln/DateReferral" rmxtype="datebuttonscript"
                    name="datereferral" />

                <script language="JavaScript" src="">{var i;}</script>

                <asp:button runat="server" type="button" class="button" id="datereferralbtn" text="..."
                    tabindex="36" onclientclick="calculatedaysbetweendates('datereceived','datereferral','daysforreview');" />
                <cc1:CalendarExtender runat="server" ID="datereferral_ajax" TargetControlID="datereferral" />
            </span>
        </div>
        <div runat="server" class="half" id="div_datereceived" xmlns="">
            <asp:label runat="server" class="label" id="lbl_datereceived" text="Date Reviewed" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="calculatedaysbetweendates('datereceived','datereferral','daysforreview');;setDataChanged(true);&#xA;              "
                    id="datereceived" rmxref="/Instance/CmXTreatmentPln/DateReceived" rmxtype="datebuttonscript"
                    name="datereceived" />

                <script language="JavaScript" src="">{var i;}</script>

                <asp:button runat="server" type="button" class="button" id="datereceivedbtn" text="..."
                    tabindex="41" onclientclick="calculatedaysbetweendates('datereceived','datereferral','daysforreview');" />
                <cc1:CalendarExtender runat="server" ID="datereceived_ajax" TargetControlID="datereceived" />
            </span>
        </div>
        <div runat="server" class="half" id="div_daysforreview" xmlns="">
            <asp:label runat="server" class="label" id="lbl_daysforreview" text="Days for Review" />
            <span class="formw">
                <asp:textbox runat="Server" rmxref="/Instance/UI/FormVariables/SysExData/DaysforReview"
                    id="daysforreview" tabindex="37" style="background-color: silver;" readonly="true" />
            </span>
        </div>
        <div runat="server" class="half" id="div_nohoursbilled" xmlns="">
            <asp:label runat="server" class="label" id="lbl_nohoursbilled" text="Hours Billable" />
            <span class="formw">
                <asp:textbox runat="server" onblur="numLostFocus(this);" id="nohoursbilled" rmxref="/Instance/CmXTreatmentPln/NoHoursBilled"
                    rmxtype="numeric" tabindex="42" onchange="setDataChanged(true);" />
            </span>
        </div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABAppeal"
        id="FORMTABAppeal">
        <div runat="server" class="half" id="div_dateappeal" xmlns="">
            <asp:label runat="server" class="label" id="lbl_dateappeal" text="Date Appealed" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" onchange="setDataChanged(true);" id="dateappeal"
                    rmxref="/Instance/CmXTreatmentPln/DateAppeal" rmxtype="date" tabindex="43" />
                <cc1:CalendarExtender runat="server" ID="dateappeal_ajax" TargetControlID="dateappeal" />
            </span>
        </div>
        <div runat="server" class="half" id="div_appdecisioncode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_appdecisioncode" text="Appeal Decision" />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="appdecisioncode"
                    rmxref="/Instance/CmXTreatmentPln/AppDecisionCode" rmxtype="code" cancelledvalue=""
                    tabindex="45" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="appdecisioncodebtn" onclientclick="return selectCode('APPEAL_DECISION','appdecisioncode');"
                    tabindex="46" />
                <asp:textbox style="display: none" runat="server" id="appdecisioncode_cid" rmxref="/Instance/CmXTreatmentPln/AppDecisionCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABCriteria"
        id="FORMTABCriteria">
        <div runat="server" class="half" id="div_CriteriaCode" xmlns="">
            <asp:label runat="server" class="label" id="lbl_CriteriaCode" text="Criteria " />
            <span class="formw">
                <asp:textbox runat="server" onchange="lookupTextChanged(this);" id="CriteriaCode"
                    rmxref="/Instance/CmXTreatmentPln/CriteriaCode" rmxtype="code" cancelledvalue=""
                    tabindex="47" onblur="codeLostFocus(this.id);" />
                <asp:button class="CodeLookupControl" runat="Server" id="CriteriaCodebtn" onclientclick="return selectCode('TREATMENT_CRITERIA','CriteriaCode');"
                    tabindex="48" />
                <asp:textbox style="display: none" runat="server" id="CriteriaCode_cid" rmxref="/Instance/CmXTreatmentPln/CriteriaCode/@codeid"
                    cancelledvalue="" />
            </span>
        </div>
    </div>
    <div class="singletopborder" style="display: none;" runat="server" name="FORMTABsuppgroup"
        id="FORMTABsuppgroup">
        <asp:textbox style="display: none" runat="server" id="supp_cmtp_row_id" rmxref="/Instance/*/Supplementals/CMTP_ROW_ID"
            rmxtype="id" />
    </div>
    <div id="Div4" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnToCmXTreatmentPln">
            <asp:button class="button" runat="Server" id="btnToCmXTreatmentPln" text="Back to Treatment Plan List"
                onclientclick="if(!( XFormHandler('SysViewType=controlsonly&SysCmd=1','1','back')))return false;"
                postbackurl="?SysViewType=controlsonly&SysCmd=1" />
        </div>
    </div>
    <asp:textbox style="display: none" runat="server" id="SysCmd" rmxref="Instance/UI/FormVariables/SysCmd"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysCmdConfirmSave" rmxref="Instance/UI/FormVariables/SysCmdConfirmSave"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysCmdQueue" rmxref="Instance/UI/FormVariables/SysCmdQueue"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysCmdText" rmxref="Instance/UI/FormVariables/SysCmdText"
        rmxtype="hidden" text="Navigate" />
    <asp:textbox style="display: none" runat="server" id="SysClassName" rmxref="Instance/UI/FormVariables/SysClassName"
        rmxtype="hidden" text="CmXTreatmentPln" />
    <asp:textbox style="display: none" runat="server" id="SysSerializationConfig" rmxref="Instance/UI/FormVariables/SysSerializationConfig"
        rmxtype="hidden" text="&lt;CmXTreatmentPln&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/CmXTreatmentPln&gt;" />
    <asp:textbox style="display: none" runat="server" id="SysFormPId" rmxref="Instance/UI/FormVariables/SysFormPId"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysPSid" rmxref="Instance/UI/FormVariables/SysPSid"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysFormId" rmxref="Instance/UI/FormVariables/SysFormId"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysSid" rmxref="Instance/UI/FormVariables/SysSid"
        rmxtype="hidden" text="11150" />
    <asp:textbox style="display: none" runat="server" id="SysViewType" rmxref="Instance/UI/FormVariables/SysViewType"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysFormName" rmxref="Instance/UI/FormVariables/SysFormName"
        rmxtype="hidden" text="CmXTreatmentPln" />
    <asp:textbox style="display: none" runat="server" id="SysFormIdName" rmxref="Instance/UI/FormVariables/SysFormIdName"
        rmxtype="hidden" text="cmtprowid" />
    <asp:textbox style="display: none" runat="server" id="SysFormPIdName" rmxref="Instance/UI/FormVariables/SysFormPIdName"
        rmxtype="hidden" text="casemgtrowid" />
    <asp:textbox style="display: none" runat="server" id="SysFormPForm" rmxref="Instance/UI/FormVariables/SysFormPForm"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="SysEx" rmxref="Instance/UI/FormVariables/SysEx"
        rmxtype="hidden" text="eventnumber,claimnumber" />
    <asp:textbox style="display: none" runat="server" id="providerlastname_cid" rmxref="/Instance/CmXTreatmentPln/ProviderEid"
        rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" name="formname" value="CmXTreatmentPln" />
    <asp:textbox style="display: none" runat="server" name="SysRequired" value="" />
    <asp:textbox style="display: none" runat="server" name="SysFocusFields" value="providerlastname|" />
    <input type="hidden" id="hdSaveButtonClicked" />
    <asp:textbox runat="server" id="SysInvisible" style="display: none" />
    <asp:textbox runat="server" id="SysLookupClass" style="display: none" />
    <asp:textbox runat="server" id="SysLookupRecordId" style="display: none" />
    <asp:textbox runat="server" id="SysLookupAttachNodePath" style="display: none" />
    <asp:textbox runat="server" id="SysLookupResultConfig" style="display: none" />
    <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
    <asp:TextBox runat="server" id="eventnumber" RMXRef="Instance/UI/FormVariables/SysExData/EventNumber" style="display:none"/>
    
    </form>
</body>
</html>
