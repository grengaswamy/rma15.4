﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.UI.Shared.Controls;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using System.Collections.Generic;
namespace Riskmaster.UI.FDM
{
    public partial class Entitymaint : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Added Rakhi forR7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            TextBox txtPrimaryAddressChanged = (TextBox)this.FindControl("PrimaryAddressChanged");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                if (txtPrimaryAddressChanged != null)
                    IncludeLastRecord(txtPrimaryAddressChanged.Text);
            }
            //Added Rakhi forR7:Add Emp Data Elements
            //RMA-6871
            string sSysCmd = string.Empty;
            TextBox txtSysCmd = ((TextBox)this.Form.FindControl("SysCmd"));
            if (txtSysCmd != null)
            {
                if (!string.IsNullOrEmpty(txtSysCmd.Text))
                {
                    sSysCmd = txtSysCmd.Text;
                }
            }
            //RMA-6871
            FDMPageLoad();

            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            if (!IsPostBack)
            {
                HtmlGenericControl divClaimaAjusterLookup = (HtmlGenericControl)this.FindControl("div_claimadjusterlookup");
                TextBox txtClaimAdjusterLookup = (TextBox)this.FindControl("claimadjusterlookup");
                if (Request.QueryString["attachtable"] != null && Request.QueryString["claimid"] != null && string.Equals(Request.QueryString["attachtable"].ToString(), "claim", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(Request.QueryString["claimid"].ToString()))
                {
                    if (divClaimaAjusterLookup != null)
                        divClaimaAjusterLookup.Visible = true;

                    TextBox txtClaimID = (TextBox)this.FindControl("claimid");
                    if (txtClaimID != null)
                        txtClaimID.Text = Request.QueryString["claimid"].ToString();
                }
                else
                {
                    if (divClaimaAjusterLookup != null)
                        divClaimaAjusterLookup.Visible = false;
                }
            }
            //Ankit End

           //Added Rakhi forR7:Add Emp Data Elements
           if(txtPrimaryAddressChanged != null)
               txtPrimaryAddressChanged.Text = "false"; //Setting it false after the processing is over.
            //Added Rakhi forR7:Add Emp Data Elements
           //mbahl3 mits:29316

           //if (this.FindControl("typeid") != null)
           //{
           //    var sSelectedValue = ((DropDownList)this.FindControl("typeid")).SelectedValue;

           //    if (this.FindControl("ssnCodeId") != null)
           //    {
           //        if (sSelectedValue == ((TextBox)this.FindControl("ssnCodeId")).Text)
           //        {
           //            if (this.FindControl("lbl_taxid") != null)
           //            {
           //                ((Label)this.FindControl("lbl_taxid")).Text = "SSN";
           //            }
           //            if (this.FindControl("taxid") != null)
           //            {
           //                ((TextBox)this.FindControl("taxid")).Attributes["onblur"] = "ssnLostFocus(this);";
           //            }
           //        }
           //        else
           //        {
           //            if (this.FindControl("lbl_taxid") != null)
           //            {
           //                ((Label)this.FindControl("lbl_taxid")).Text = "FEIN";
           //            }
           //            if (this.FindControl("taxid") != null)
           //            {
           //                ((TextBox)this.FindControl("taxid")).Attributes["onblur"] = "FEINLostFocus(this);";
           //            }
           //        }
           //    }
           //}
            //mbahl3 mits:29316
           //RMA-6871
           string sQSValues = "false";         //avipinsrivas start : Worked for JIRA - 1147
           if (Request.QueryString["IsFromQuickLookup"] != null || this.FindControl("hdFromQuickLookup") != null)
            {
               string[] arrHiddenValues = ((TextBox)this.FindControl("hdFromQuickLookup")).Text.Trim().Split('|');

                if ((Request.QueryString["IsFromQuickLookup"] != null && Request.QueryString["IsFromQuickLookup"].ToString() == "true") || (arrHiddenValues != null && arrHiddenValues.Length > 0 && !string.IsNullOrEmpty(arrHiddenValues[0].ToString()) && string.Equals(arrHiddenValues[0].ToString(),"true",StringComparison.OrdinalIgnoreCase)))
                {
                    sQSValues = "true";
                    foreach (Control c in this.FindControl("toolbardrift").Controls)
                    {
                        if ((c as HtmlGenericControl) == null) continue;
                        if (((HtmlGenericControl)c).ID.ToLower() != "div_save")
                        {
                            ((HtmlGenericControl)c).Visible = false;
                        }
                    }
                    if (Request.QueryString["lastname"] != null)
                    {
                        if (Request.QueryString["lastname"].ToString() != string.Empty)
                        {
                            sQSValues = string.Concat(sQSValues, "|", Request.QueryString["lastname"].ToString());
                            //avipinsrivas start : Worked for JIRA - 16438
                            if (this.FindControl("lastname") != null)
                                ((TextBox)this.FindControl("lastname")).Text = Request.QueryString["lastname"].ToString();
                            if (this.FindControl("erlastname") != null)
                                ((TextBox)this.FindControl("erlastname")).Text = Request.QueryString["lastname"].ToString();
                            //avipinsrivas end
                            if (this.FindControl("abbreviation") != null)
                            {
                                ((TextBox)this.FindControl("abbreviation")).Text = Request.QueryString["lastname"].ToString().Substring(0, 1);
                            }
                            if (this.FindControl("LegalName") != null)
                            {
                                ((TextBox)this.FindControl("LegalName")).Text = Request.QueryString["lastname"].ToString();
                            }
                        }
                    }
                }
            }
            //avipinsrivas start : Worked for JIRA - 1147
            if (this.FindControl("hdFromQuickLookup") != null && string.IsNullOrEmpty(((TextBox)this.FindControl("hdFromQuickLookup")).Text))
                ((TextBox)this.FindControl("hdFromQuickLookup")).Text = sQSValues;
            //avipinsrivas end
            if (sSysCmd == "9")
            {
                int iEntityId = Convert.ToInt32(((TextBox)this.Form.FindControl("entityid")).Text);
                if (iEntityId > 0)
                {
                    string sLastName = ((TextBox)this.FindControl("lastname")).Text;
                    sLastName = AppHelper.HTMLCustomEncode(sLastName);//RMA-10824
                    string sScript = "if(window.opener != null){ if (window.opener.document.forms[0].SysFormName.value == 'funds' || window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks' || 'thirdpartypayments' == window.opener.document.forms[0].SysFormName.value) { window.opener.RequestRestore('" + sLastName + "');window.opener.entitySelected(" + iEntityId + ");window.close();}}";
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "EntitySelected", sScript, true);
                }
            }
            if (sSysCmd == "7")
            {
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetDataChanged", "if(window.opener != null){ if (window.opener.document.forms[0].SysFormName.value == 'funds' || window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks' || 'thirdpartypayments' == window.opener.document.forms[0].SysFormName.value) { setDataChanged(true); } }", true);
            }
            //RMA-6871
        }
        //RMA-6871
        protected new void NavigateSave(object sender, EventArgs e)
        {
            try
            {
                base.NavigateSave(sender, e);
                int iEntityId = Convert.ToInt32(((TextBox)this.Form.FindControl("entityid")).Text);
                if (iEntityId > 0)
                {
                    //$(window.opener.document.getElementById('overlaydiv')).hide();
                    string sLastName = ((TextBox)this.FindControl("lastname")).Text;
                    sLastName = AppHelper.HTMLCustomEncode(sLastName);//RMA-10824
                    string sScript = "if(window.opener != null){ if (window.opener.document.forms[0].SysFormName.value == 'funds' || window.opener.document.forms[0].SysFormName.value == 'autoclaimchecks' || 'thirdpartypayments' == window.opener.document.forms[0].SysFormName.value) { window.opener.RequestRestore('" + sLastName + "');window.opener.entitySelected(" + iEntityId + ");window.close();}}";
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "EntitySelected", sScript, true);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
            }
            finally { }
        }
        //RMA-6871
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            XElement oEntityId = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Entity/EntityId");
            if (oEntityId != null)
            {
                Control oEntityTabelId = this.FindControl("entitytableid");
                Control oEntityTabelIdText = this.FindControl("entitytableidtext");
                Control oSysPostBack = this.FindControl("SysPostback");
                if (oEntityId.Value == "0")
                {
                    if (oEntityTabelId != null)
                    {
                        RemoveIgnoreValueAttribute(oEntityTabelId);
                    }
                    if (oEntityTabelIdText != null)
                    {
                        SetIgnoreValueAttribute(oEntityTabelIdText);
                    }                    
                    if (oSysPostBack != null)
                    {
                        //Raman 07/08/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                        if (oSysPostBack.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox")) 
                        ((TextBox)oSysPostBack).Text = "entitytableid";
                    }
                }
                else
                {
                    if (oEntityTabelId != null)
                    {
                        //Raman 07/08/2009 : Adding null checks
                        if (oEntityTabelIdText != null)
                        {
                            RemoveIgnoreValueAttribute(oEntityTabelIdText);
                        }
                    }
                    if (oEntityTabelIdText != null)
                    {
                        //Raman 07/08/2009 : Adding null checks
                        if (oEntityTabelId != null)
                        {
                            SetIgnoreValueAttribute(oEntityTabelId);
                        }
                    }
                    if (oSysPostBack != null)
                    {
                        //Raman 07/08/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                        if (oSysPostBack.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                        ((TextBox)oSysPostBack).Text = "entitytableidtext";
                    }
                }
            }
            XElement oSysSkipBindToControl = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysSkipBindToControl");
            Control oDupSSN = this.FindControl("DupSSN");
            if (oSysSkipBindToControl != null && oDupSSN != null)
            {
                if (oSysSkipBindToControl.Value == "true")
                {
                    //Raman 07/08/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                    if (oDupSSN.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    ((TextBox)oDupSSN).Text = "BeforeIgnore";
                }
            }
        }

        public override void ModifyControls()
        {
            //rjhamb 16453:Commented the code added by gagnihotri MITS 16453 05/12/2009.This is now getting handled in function UpdateSysReadOnlyFields
            ////Raman 07/08/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
            //TextBox txtEntityTableIdTextName = null;
            //if (this.FindControl("entitytableidtextname") != null)
            //{
            //    if (this.FindControl("entitytableidtextname").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
            //    {
            //        txtEntityTableIdTextName = ((TextBox)this.FindControl("entitytableidtextname"));
            //    }

            //    //Raman 07/08/2009: Null check missing 
            //    if (txtEntityTableIdTextName != null && txtEntityTableIdTextName.Visible)
            //    {
            //        txtEntityTableIdTextName.ReadOnly = true;
            //        txtEntityTableIdTextName.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
            //    }
            //}
            //rjhamb 16453:Commented the code added by gagnihotri MITS 16453 05/12/2009.This is now getting handled in function UpdateSysReadOnlyFields
            //rjhamb 22226:To enable Type of Entity combobox from getting disabled in powerviews
            DropDownList cmbEntityTableId = null;
            if (this.FindControl("entitytableid") != null)
            {
                if (this.FindControl("entitytableid").GetType().ToString().Equals("System.Web.UI.WebControls.DropDownList"))
                {
                    cmbEntityTableId = ((DropDownList)this.FindControl("entitytableid"));
                }
                if (cmbEntityTableId != null && cmbEntityTableId.Visible)
                {
                    //cmbEntityTableId.Enabled = true;
                    DatabindingHelper.EnableControls("entitytableid", this);//asharma326 jira 6411
                }
            }
            //rjhamb 22226:To enable Type of Entity combobox from getting disabled in powerviews

            //Added Rakhi For R7:Add Emp Data Elements
            TextBox txtUseMultipleAddresses = (TextBox)this.FindControl("UseMultipleAddresses");
            if (txtUseMultipleAddresses != null && txtUseMultipleAddresses.Text.ToLower() == "true")
            {
                SetPrimaryAddress();
            }
            //Added Rakhi For R7:Add Emp Data Elements
            ////avipinsrivas start : Worked for JIRA - 7767
            //TextBox hdEntType = (TextBox)this.FindControl("hdEntType");
            //if (hdEntType != null)
            //{
            //    Label lblLastName = (Label)this.FindControl("lbl_lastname");
            //    if (string.Equals(hdEntType.Text.Trim(), "IND"))
            //        lblLastName.Text = "Last Name";
            //    else
            //        lblLastName.Text = "Name";
            //}
            ////avipinsrivas end
        }
        private void IncludeLastRecord(string sPrimaryAddressChanged)
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");
            if (objGridControl != null)
            {
                if (sPrimaryAddressChanged.ToLower() == "true")
                {

                    objGridControl.IncludeLastRecord = true;
                }
                else
                {
                    objGridControl.IncludeLastRecord = false;
                }
            }
        }
        private void SetPrimaryAddress()
        {
            UserControlDataGrid objGridControl = (UserControlDataGrid)this.FindControl("EntityXAddressesInfoGrid");//Capturing Primary Flag
            bool bPrimaryFlag = false;
            if (objGridControl != null)
            {
                GridView gvData = (GridView)objGridControl.GridView;
                int iGridRowCount = gvData.Rows.Count;
                int iPrimaryColIndex = 0;
                int iUniqueIdIndex = 0;
                if (iGridRowCount > 1)
                {
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    for (int i = 0; i < gvData.Columns.Count - 1; i++)
                    {
                        if (gvData.Columns[i].HeaderText.ToLower() == "primary address")
                        {
                            iPrimaryColIndex = i;
                            continue;
                        }
                        else if (gvData.Columns[i].HeaderText.ToLower() == "uniqueid_hidden")
                        {
                            iUniqueIdIndex = i;
                            continue;
                        }
                    }
                    for (int i = 0; i < iGridRowCount - 1; i++)
                    {
                        string sValue = gvData.Rows[i].Cells[iPrimaryColIndex].Text.ToLower();
                        if (sValue == "true")
                        {
                            bPrimaryFlag = true;
                            if (txtPrimaryRow != null)
                                txtPrimaryRow.Text = gvData.Rows[i].Cells[iUniqueIdIndex].Text;
                            break;
                        }
                    }
                    if (!bPrimaryFlag)
                    {
                        if (txtPrimaryRow != null)
                            txtPrimaryRow.Text = "";
                    }
                }
                else
                {
                    bPrimaryFlag = false;
                    TextBox txtPrimaryRow = (TextBox)this.FindControl("PrimaryRow");
                    if (txtPrimaryRow != null)
                        txtPrimaryRow.Text = "";
                }
            }
            TextBox txtPrimaryFlag = (TextBox)this.FindControl("PrimaryFlag");
            if (txtPrimaryFlag != null)
                txtPrimaryFlag.Text = bPrimaryFlag.ToString();
        }
    }
}
