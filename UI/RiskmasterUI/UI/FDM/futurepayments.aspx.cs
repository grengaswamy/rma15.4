﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common;

namespace Riskmaster.UI.FDM
{
    public partial class Futurepayments : GridPopupBase
    {
     //IC Retrofit for REM Transaction Code , MITS 20093
     //--added by rkaur7 on 02/18/2010 REM-MITS #19821
        bool bReturnStatus = false;
        string sreturnValue = "";
        XElement XmlTemplate = null;
	//end rkaur7
        //Deb Multi Currency
        protected override void InitializeCulture()
        {
            string sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype");
            if (!string.IsNullOrEmpty(sCurrCulture))
            {
                Culture = sCurrCulture.Split('|')[1];
                UICulture = sCurrCulture.Split('|')[1];
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(sCurrCulture.Split('|')[1]);
            }
            base.InitializeCulture();
        }
        //Deb Multi Currency
        protected void Page_Load(object sender, EventArgs e)
        {
     //IC Retrofit for REM Transaction Code , MITS 20093
     //--added by rkaur7 on 02/18/2010 REM-MITS #19821 - WebService Called
            MakeWebServiceCall();
	//end rkaur7
            //GridPopupPageload();
            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes

            if (!IsPostBack)
            {
                TextBox txtGridName = (TextBox)FindControlRecursive(this.Form, "gridname");
                txtGridName.Text = Request.QueryString["gridname"];
                TextBox txtmode = (TextBox)FindControlRecursive(this.Form, "mode");
                txtmode.Text = Request.QueryString["mode"];
                TextBox txtselectedrowposition = (TextBox)FindControlRecursive(this.Form, "selectedrowposition");
                txtselectedrowposition.Text = Request.QueryString["selectedrowposition"];
                //ReserveTypeCode.Enabled = false;
                Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
                //Deb MITS 26077
                if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                {
                    ((CodeLookUp)objReserveTypeCode).Enabled = false;
                }
                //Deb MITS 26077
            }
            else
            {
                TextBox txtPostBackParent = (TextBox)FindControlRecursive(this.Form, "txtPostBack");
                txtPostBackParent.Text = "Done";

                Control c = DatabindingHelper.GetPostBackControl(this.Page);
                //if (c == null || (c.ID.ToLower() == "delete" && objGridControl != null && objGridControl.PopupGrid && !objGridControl.SessionGrid))
                if (c == null || c.ID == "Policy")
                {                    
                    TextBox txtUnitID = (TextBox)this.FindControl("UnitID");
                   
                    TextBox txtDataParent = (TextBox)FindControlRecursive(this.Form, "txtData");
                    string sXml = txtDataParent.Text;
                    sXml = sXml.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                    XmlDocument objXml = new XmlDocument();
                    objXml.LoadXml(sXml);

                    //added by Navdeep - Modify xml to Map Line of Business List Box to Pop Up from Grid (Chubb Ack)
                    ModifyXml(ref objXml);
                    sXml = objXml.InnerXml;
                    //end by Navdeep
                   // XElement objElem = XElement.Parse(sXml);

                    //rupal:start When Policy dropdown changes value then we should wipe out the coverage , transaction type and reserve type values
                    if (c != null && c.ID == "Policy")
                    {
                        XElement objElement = XElement.Parse(sXml);

                        XNode objXCoverageTypeCodeNode = objElement.XPathSelectElement("//CoverageTypeCode");
                        if (objXCoverageTypeCodeNode != null)
                            objXCoverageTypeCodeNode.Remove();


                        XNode objXReserveTypeCodeNode = objElement.XPathSelectElement("//ReserveTypeCode");
                        if (objXReserveTypeCodeNode != null)
                            objXReserveTypeCodeNode.Remove();

                        XNode objXTransTypeCodeNode = objElement.XPathSelectElement("//TransTypeCode");
                        if (objXTransTypeCodeNode != null)
                            objXTransTypeCodeNode.Remove();

                        sXml = objElement.ToString();                                              
                                             
                    }
                    //rupal:end
                    XElement objElem = XElement.Parse(sXml);
                    //rupal:start, r8 unit implmentation
                    //since the policy can be changed than the values passed from parent page, we need to update hidden feilds to reflect selected/changed policy and unit
                    TextBox objPolicyid = (TextBox)this.FindControl("PolicyID");
                    if (objPolicyid != null && String.IsNullOrEmpty(objPolicyid.Text))
                    {
                        if (objElem.XPathSelectElement("//PolicyID") != null)
                            objPolicyid.Text = objElem.XPathSelectElement("./PolicyID").Attribute("value").Value;
                    }


                    if (txtUnitID != null && String.IsNullOrEmpty(txtUnitID.Text))
                    {
                        if (objElem.XPathSelectElement("//UnitID") != null)
                            txtUnitID.Text = objElem.XPathSelectElement("./UnitID").Attribute("value").Value;
                    }
                    //rupal:end

                    //rupal:start, r8 unit implementation
                    if (objElem.XPathSelectElement("//PolicyID") != null && objElem.XPathSelectElement("//UnitID") != null)
                    {
                        DropDownList dPolicy = (DropDownList)DatabindingHelper.FindControlRecursive(this.Form, "Policy");
                        DropDownList dUnit = (DropDownList)DatabindingHelper.FindControlRecursive(this.Form, "Unit");
                        if (dPolicy != null && dUnit != null)
                        {
                            dPolicy.Items.Clear();
                            dUnit.Items.Clear();

                            MakeWebServiceCall(objElem.XPathSelectElement("./AutoSplitId").Value, ref objElem);
                        }

                    } //rupal:end
                    else
                    {
                        EnableDisblePolicy();
                    }
                    //Deb Multi Currency
                    string sCurrCulture = AppHelper.GetQueryStringValue("pmtcurrencytype");
                    if (!string.IsNullOrEmpty(sCurrCulture))
                    {
                        Culture = sCurrCulture.Split('|')[1];
                        UICulture = sCurrCulture.Split('|')[1];
                        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(sCurrCulture.Split('|')[1]);
                        Label lblcurrencytype = (Label)this.FindControl("lblcurrencytype");
                        Control objDivlblCurrType = this.FindControl("div_lblcurrencytype");
                        Control objDivClaimCurrBal = this.FindControl("div_ClaimCurrReserveBal");
                        if (lblcurrencytype != null)
                        {
                            if ((objElem.XPathSelectElement("//UseMultiCurrency") != null))
                            {
                                if (string.Compare(objElem.XPathSelectElement("./UseMultiCurrency").Value, Boolean.TrueString) != 0)
                                {
                                    objDivClaimCurrBal.Visible = false;
                                    objDivlblCurrType.Visible = false;
                                }
                                else
                                {
                                    lblcurrencytype.Text = sCurrCulture.Split('|')[0].Substring(0, 3) + " " + System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                                }
                            }
                        }
                    }

                    //Start - Added by Nikhil.Show/hide override Deductible processing option 
                    Control objDivDedProcessingFlag = this.FindControl("div_IsOverrideDedProcessing");
                    if (objElem.XPathSelectElement("OverrideDedFlagVisible") != null)
                    {
                        if (string.Compare(objElem.XPathSelectElement("OverrideDedFlagVisible").Value, "-1") != 0)
                        {
                            
                            if (objDivDedProcessingFlag != null)
                            {
                                objDivDedProcessingFlag.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        if (objDivDedProcessingFlag != null)
                        {
                            objDivDedProcessingFlag.Visible = false;
                        }
                    }
                    //End - Added by Nikhil.Show/hide override Deductible processing option

                    //Deb Multi Currency
                    BindData2Control(objElem, this.Controls);
                }
            }
            //if (!IsPostBack)
            //{
            //    ReserveTypeCode.Enabled = false;
            //}
            
        }
        //IC Retrofit for REM Transaction Code , MITS 20093
        //Function to make a call to Webservice--added by rkaur7 on 02/15/2010 REM-MITS #19821  
        private void MakeWebServiceCall()
        {
            XmlTemplate = GetMessageTemplate();
            //bReturnStatus = CallCWSFunction("SplitForm.GetUtilityUseResFilterValue", out sreturnValue, XmlTemplate);

            bReturnStatus = CallCWS("FundManagementAdaptor.CheckUtility", XmlTemplate, out sreturnValue, false, false);
            EnableDisbleReserveTypeFt(sreturnValue);

        }

        private void MakeWebServiceCall(string sAutoSplitId, ref XElement objElem)
        {
            TextBox txtmode = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "mode");
            XmlTemplate = GetTemplateForPolicyList(sAutoSplitId);

            bReturnStatus = CallCWS("FundManagementAdaptor.GetPolicyList", XmlTemplate, out sreturnValue, false, false);
            XElement oTemplate = XElement.Parse(sreturnValue.ToString());

            if (string.Compare(txtmode.Text, "add", true) == 0)
            {
                //rupal:start, r8 unit changes
                foreach (XElement oTemp in oTemplate.XPathSelectElements("//PolicyList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true")
                    {
                        objElem.XPathSelectElement("//PolicyID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//PolicyID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
                        
						
                        //rupal:start, r8 unit implementation
                        TextBox txtPolicyID = (TextBox)this.FindControl("PolicyID");
                        if (txtPolicyID != null)
                        {
                            txtPolicyID.Text = oTemp.FirstAttribute.Value;                            
                        }
                        //rupal:end						
                    }
                }

                bool bUnitIDHasValue = false;
                TextBox txtUnitID = (TextBox)this.FindControl("UnitID");
                foreach (XElement oTemp in oTemplate.XPathSelectElements("//UnitList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true") //here LastAttribute means "Selected"
                    {
                        objElem.XPathSelectElement("//UnitID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//UnitID").SetAttributeValue("value", oTemp.FirstAttribute.Value);                        
                  		
				        //rupal:start, r8 unit implementation, 
                        if (txtUnitID != null)
                        {
                            txtUnitID.Text = oTemp.FirstAttribute.Value;
                            bUnitIDHasValue = true; //set true if "Selected" Attribute is set to true
                        }
                        //rupal:end
						
                    }				
                    //rupal:start, r8 unit implementation
                    if (!bUnitIDHasValue) 
                    {
                        if (txtUnitID != null)
                        {
                            txtUnitID.Text = oTemp.FirstAttribute.Value;
                            bUnitIDHasValue = true; //will store first unitid
                        }
                    }                
                }
                //rupal:end
                XNode objXCoverageTypeCodeNode = objElem.XPathSelectElement("//CoverageTypeCode");
                if (objXCoverageTypeCodeNode != null)
                    objXCoverageTypeCodeNode.Remove();
                objXCoverageTypeCodeNode = oTemplate.XPathSelectElement("//CoverageTypeCode");
                objElem.Add(objXCoverageTypeCodeNode);

                XNode objXReserveTypeCodeNode = objElem.XPathSelectElement("//ReserveTypeCode");
                if (objXReserveTypeCodeNode != null)
                    objXReserveTypeCodeNode.Remove();
                objXReserveTypeCodeNode = oTemplate.XPathSelectElement("//ReserveTypeCode");
                objElem.Add(objXReserveTypeCodeNode);
                XNode objXCovgSeqNumNode = objElem.XPathSelectElement("//CovgSeqNum");
                if (objXCovgSeqNumNode != null)
                    objXCovgSeqNumNode.Remove();
                objXCovgSeqNumNode = oTemplate.XPathSelectElement("//CovgSeqNum");
                objElem.Add(objXCovgSeqNumNode);
                //Ankit : End
                XNode objXDisabilitycatCodeNode = objElem.XPathSelectElement("//DisabilityCatCode");
                if (objXDisabilitycatCodeNode != null)
                    objXDisabilitycatCodeNode.Remove();
                objXDisabilitycatCodeNode = oTemplate.XPathSelectElement("//DisabilityCatCode");
                objElem.Add(objXDisabilitycatCodeNode);
                XNode objXDisabilityTypeCodeNode = objElem.XPathSelectElement("//DisabilityTypeCode");
                if (objXDisabilityTypeCodeNode != null)
                    objXDisabilityTypeCodeNode.Remove();
                objXDisabilityTypeCodeNode = oTemplate.XPathSelectElement("//DisabilityTypeCode");
                objElem.Add(objXDisabilityTypeCodeNode);
                XNode objXLossTypeCodeNode = objElem.XPathSelectElement("//LossTypeCode");
                if (objXLossTypeCodeNode != null)
                    objXLossTypeCodeNode.Remove();
                objXLossTypeCodeNode = oTemplate.XPathSelectElement("//LossTypeCode");
                objElem.Add(objXLossTypeCodeNode);
            }

            if (string.Compare(txtmode.Text, "edit", true) == 0)
            {
                foreach (XElement oTemp in oTemplate.XPathSelectElements("//PolicyList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true")
                    {
                        objElem.XPathSelectElement("//PolicyID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//PolicyID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
                    }
                }

                foreach (XElement oTemp in oTemplate.XPathSelectElements("//UnitList/option"))
                {
                    if (oTemp.LastAttribute.Value == "true")
                    {
                        objElem.XPathSelectElement("//UnitID").Value = oTemp.Value;
                        objElem.XPathSelectElement("//UnitID").SetAttributeValue("value", oTemp.FirstAttribute.Value);
                    }
                }
            }

            XElement xNode = oTemplate.XPathSelectElement("//PolicyList");
            objElem.Add(xNode);

            xNode = oTemplate.XPathSelectElement("//UnitList");
            objElem.Add(xNode);

            Control objDivDisabilityCat = this.FindControl("div_DisabilityCat");
            Control objDivDisabilityLossType = this.FindControl("div_DisabilityLossType");
            Control objDivLossType = this.FindControl("div_LossType");

            TextBox objLOB = (TextBox)this.FindControl("lineofbusinesscode");

            if (objLOB.Text == "243")
            {
                if (objDivLossType != null)
                {
                    objDivLossType.Visible = false;
                }

            }
            else
            {
                if (objDivDisabilityCat != null)
                {
                    objDivDisabilityCat.Visible = false;
                }

                if (objDivDisabilityLossType != null)
                {
                    objDivDisabilityLossType.Visible = false;
                }

            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
          //  sXml = sXml.Append("<Call><Function>SplitForm.GetUtilityUseResFilterValue</Function></Call><Document><UseResFilter></UseResFilter></Document></Message>");
            sXml = sXml.Append("<Call><Function>SplitForm.GetUtilityUseResFilterValue</Function></Call><Document><GetSettings><UseResFilter></UseResFilter><UseCarrierClaims></UseCarrierClaims></GetSettings></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetTemplateForPolicyList(string sAutoSplitId)
        {
            TextBox txtmode = (TextBox)DatabindingHelper.FindControlRecursive(this.Form, "mode");
            TextBox objclaimid = (TextBox)this.FindControl("claimid");            
            TextBox objPolicyid = (TextBox)this.FindControl("PolicyID");
            TextBox objPolCvgID = (TextBox)this.FindControl("PolCvgID");
            TextBox ResTypeCodeId = (TextBox)this.FindControl("ResTypeCode");
            TextBox unitID = (TextBox)this.FindControl("UnitID");
            TextBox CvgLossID = (TextBox)this.FindControl("CvgLossID");   
            //Ankit : Start Policy System Interface
            TextBox objCovgSeqNum = (TextBox)this.FindControl("CovgSeqNum");
            //Ankit : End
            TextBox RcRowID = (TextBox)this.FindControl("RcRowID");
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><Policy><AutoSplitId>");
            sXml = sXml.Append(sAutoSplitId);
            sXml = sXml.Append("</AutoSplitId>");
            TextBox LobCode = (TextBox)this.FindControl("lineofbusinesscode");
            bool bSuccess = false;
            int iAutoSplitId = Common.Conversion.CastToType<Int32>(sAutoSplitId, out bSuccess);
            if (string.Compare(txtmode.Text, "add", true) == 0)
            {
                sXml = sXml.Append("<ClaimId>");
                sXml = sXml.Append(objclaimid.Text);
                sXml = sXml.Append("</ClaimId>");
                sXml = sXml.Append("<PolicyId>");
                sXml = sXml.Append(objPolicyid.Text);
                sXml = sXml.Append("</PolicyId>");
                sXml = sXml.Append("<UnitId>");
                sXml = sXml.Append(unitID.Text);
                sXml = sXml.Append("</UnitId>");
                //Ankit : Start Policy System Interface
                sXml = sXml.Append("<CovgSeqNum>");
                sXml = sXml.Append(objCovgSeqNum.Text);
                sXml = sXml.Append("</CovgSeqNum>");
                //Ankit : End
                sXml = sXml.Append("<PolCvgID>");
                sXml = sXml.Append(objPolCvgID.Text);
                sXml = sXml.Append("</PolCvgID>");
                sXml = sXml.Append("<ResTypeCodeId>");
                sXml = sXml.Append(ResTypeCodeId.Text);
                sXml = sXml.Append("</ResTypeCodeId>");
                sXml = sXml.Append("<RCRowID>");
                sXml = sXml.Append(RcRowID.Text);
                sXml = sXml.Append("</RCRowID>");
                sXml = sXml.Append("<CvgLossID>");
                sXml = sXml.Append(CvgLossID.Text);
                sXml = sXml.Append("</CvgLossID>");
                sXml = sXml.Append("<LobCode>");
                sXml = sXml.Append(LobCode.Text);
                sXml = sXml.Append("</LobCode>");
            }
            //if ((string.Compare(txtmode.Text, "edit", true) == 0) && ((string.Compare(sAutoSplitId, "0") == 0) || (string.Compare(sAutoSplitId, "-1") == 0)))
            if ((string.Compare(txtmode.Text, "edit", true) == 0))// && iAutoSplitId<=0)
            {
                sXml = sXml.Append("<ClaimId>");
                sXml = sXml.Append(objclaimid.Text);
                sXml = sXml.Append("</ClaimId>");
                //rupal:start,r8 unit implemenation
                sXml = sXml.Append("<PolicyId>");
                sXml = sXml.Append(objPolicyid.Text);
                sXml = sXml.Append("</PolicyId>");
                //rupal:end
            }
            sXml = sXml.Append("<PolicyList></PolicyList></Policy></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private void EnableDisbleReserveTypeFt(string sXml)
        {
            XmlDocument objXmlDoc = new XmlDocument();

            objXmlDoc.LoadXml(sXml);
           //  UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/UseResFilter");
             XmlNode UseResFilter = objXmlDoc.SelectSingleNode("//ResultMessage/Document/GetSettings/UseResFilter");
            XmlNode objIsCarrierClaim = objXmlDoc.SelectSingleNode("//ResultMessage/Document/GetSettings/UseCarrierClaims");

            Control objReserveTypeCode = this.FindControl("ReserveTypeCode");
            Control objDivReserveTypeCode = this.FindControl("div_ReserveTypeCode");
            Control objReserveTypeCodeFt = this.FindControl("ReserveTypeCodeFt");
            Control objDivReserveTypeCodeFt = this.FindControl("div_ReserveTypeCodeFt");
            Control objReserveTypeLabel = this.FindControl("lbl_ReserveTypeCode");
            Control objTransTypeCode = this.FindControl("TransTypeCode");
            //if (UseResFilter != null)
            //{
            //    if (UseResFilter.InnerText == "True")
            //    {
                        //if (UseResFilter != null)
            //{
            if (((objIsCarrierClaim != null) && (objIsCarrierClaim.InnerText == "True") && Riskmaster.Common.Conversion.ConvertStrToInteger(((TextBox)this.FindControl("RcRowID")).Text) > 0) || ((UseResFilter != null) && (UseResFilter.InnerText == "True")))// //abahl3 starts 11468,11121,11120 ends
            {
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = true;
                    }
                    if (objReserveTypeCode != null && objReserveTypeCode.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                    {
                        ((CodeLookUp)objReserveTypeCode).ShowHide = false;
                    }
                    if (objReserveTypeLabel != null)
                    {
                        ((Label)objReserveTypeLabel).Visible = false;
                    }

                    if (Request.QueryString["mode"] == "edit")
                    {
                        if (objTransTypeCode != null && objTransTypeCode.GetType() == typeof(System.Web.UI.WebControls.TextBox))
                        {
                            ((TextBox)objTransTypeCode).Enabled = true;
                        }
                    }
                    else
                    {
                        TextBox objTxt = (TextBox)this.FindControl("TransTypeCode$codelookup");
                        if (objTxt != null)
                        {
                            objTxt.Enabled = false;
                            objTxt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                        }
                        Button objBtn = (Button)this.FindControl("TransTypeCode$codelookupbtn");
                        if (objBtn != null)
                        {
                            objBtn.Enabled = false;
                        }
                    }
                    if (objReserveTypeCodeFt != null)
                    {
                        RemoveIgnoreValueAttribute(objReserveTypeCodeFt);
                    }
                    if (objReserveTypeCode != null)
                    {
                        SetIgnoreValueAttribute(objReserveTypeCode);
                    }
                }
                else
                {
                    if (objDivReserveTypeCode != null)
                    {
                        objDivReserveTypeCode.Visible = true;
                    }
                    if (objDivReserveTypeCodeFt != null)
                    {
                        objDivReserveTypeCodeFt.Visible = false;
                    }
                    if (objReserveTypeCode != null)
                    {
                        RemoveIgnoreValueAttribute(objReserveTypeCode);
                    }
                    if (objReserveTypeCodeFt != null)
                    {
                        SetIgnoreValueAttribute(objReserveTypeCodeFt);
                    }
                }
           // }
        }
//end rkaur7
        private void EnableDisblePolicy()
        {
            Control objPolicy = this.FindControl("Policy");
            Control objDivPolicy = this.FindControl("div_Policy");
            Control objCoverageTypeCode = this.FindControl("Coverage");
            Control objDivCoverageTypeCode = this.FindControl("div_Coverage");
            //rupal:start r8 unit changes
            Control objUnit = this.FindControl("Unit");
            Control objDivUnit = this.FindControl("div_Unit");
            //rupal:end
            Control objDivDisabilityCat = this.FindControl("div_DisabilityCat");
            Control objDivDisabilityLossType = this.FindControl("div_DisabilityLossType");
            Control objDivLossType = this.FindControl("div_LossType");

            if (objDivPolicy != null)
            {
                objDivPolicy.Visible = false;
            }
            if (objDivCoverageTypeCode != null)
            {
                objDivCoverageTypeCode.Visible = false;
            }
            if (objDivUnit != null)
            {
                objDivUnit.Visible = false;
            }

           
                if (objDivLossType != null)
                {
                    objDivLossType.Visible = false;
                }

                if (objDivDisabilityCat != null)
                {
                    objDivDisabilityCat.Visible = false;
                }

                if (objDivDisabilityLossType != null)
                {
                    objDivDisabilityLossType.Visible = false;
                }
        }
    }
}
