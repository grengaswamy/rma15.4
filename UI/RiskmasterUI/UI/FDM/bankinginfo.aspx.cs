﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.FDM
{
    public partial class Bankinginfo : FDMBasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                SetInitialValues();
            FDMPageLoad();
        }

        private void SetInitialValues()
        {
            if (this.FindControl("parentid") != null)
            {
                TextBox oControl = (TextBox)this.FindControl("parentid");
                oControl.Text = AppHelper.GetQueryStringValue("parentid");
                ViewState["Parent_id"] = oControl.Text; //nsachdeva2 - MITS:27918 - 03/22/2012
            }
            if (this.FindControl("parentFormName") != null)
            {
                TextBox oParentForm = (TextBox)this.FindControl("parentFormName");
                oParentForm.Text = AppHelper.GetQueryStringValue("parentFormName");
            }
        }

        //nsachdeva2 - MITS:27918 - 03/22/2012
        public override void ModifyControls()
        {
            if (this.FindControl("parentid") != null && ViewState["Parent_id"] != null)
            {
                TextBox oControl = (TextBox)this.FindControl("parentid");
                if(!oControl.Text.Equals( ViewState["Parent_id"].ToString()))
                    oControl.Text = ViewState["Parent_id"].ToString();
            }
        }
        //End MITS:27918

       
    }
}