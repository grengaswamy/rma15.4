﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/10/2014 | 34276  | achouhan3   | GAP - 18 Entity ID Number and ID Type Number Functionality
 **********************************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.IO;
using Riskmaster.AppHelpers;
using AjaxControlToolkit;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.BusinessHelpers;
using MultiCurrencyCustomControl;
using System.Threading;
using System.Globalization;

using Riskmaster.Common;
//Add by kuladeep for rmA14.1 performance
using Riskmaster.Models;
namespace Riskmaster.UI.FDM
{
    public class FDMBasePage : System.Web.UI.Page
    {
        //rsolanki2 : extensibility updates 
        private class FormMappingCacheRecord
        {
            public string BaseFormName;	// Name key. This is the form name we're customizing.
            public string FormName;		// The new customized formname.
            public string TypeName;		// Class implementing the custom form.
            public string Assembly;
            public string PageLoadMethod;
            public string PageInitMethod;
            public string PagePreRenderMethod;
            public string PageRenderMethod;
        }
        private static Hashtable _mappings = new Hashtable();
        // akaushik5 Changed for MITS 30290 Starts
        //private static string sCurrencyMode = string.Empty;//Deb
        protected static string sCurrencyMode = string.Empty;
        // akaushik5 Changed for MITS 30290 Ends
        // rsolanki2 : extensibility updates
        private static string sCustomViewList = string.Empty;
        private static string m_sPageName = string.Empty;
        enum ePageCycle 
        { 
            Init =0,
            Load=1,
            Prerender=2,
            Render=3
        };

        private string g_sRMXREF = "RMXRef";
        // akaushik5 Changed for MITS 30290 Starts
        //private string g_sXPathDelimiter = "|";
        protected string g_sXPathDelimiter = "|";
        // akaushik5 Changed for MITS 30290 Ends
        //Done by Psarin2 for sharing Data for List Screens
        public XmlDocument Data = new XmlDocument();
        private ArrayList sMissingRefs = new ArrayList();
        private Hashtable m_RadioValues = new Hashtable();
        Stack<string[]> sScreenFlowStack = new Stack<string[]>();
        // akaushik5 Changed for MITS 30290 Starts
        //private string m_sFormReadOnly = "";
        protected string m_sFormReadOnly = "";
        // akaushik5 Changed for MITS 30290 Ends
        //Done by Psarin2 for sharing Data for List Screens

        // Added by Amitosh for mits 23441 (03/10/2011)
        // akaushik5 Changed for MITS 30290 Starts
        //private Boolean bDoFieldMarkReadOnly = false;
        protected Boolean bDoFieldMarkReadOnly = false;
        // akaushik5 Changed for MITS 30290 Ends
        // Added by Amitosh for R8 enhancement of LiabilityLoss
        // akaushik5 Changed for MITS 30290 Starts
        //private Boolean bKillFieldMarkNodes = false;
        //private XElement oKillNodesFieldMark = null;
        //private XElement oFieldMark = null;
        protected Boolean bKillFieldMarkNodes = false;
        protected XElement oKillNodesFieldMark = null;
        protected XElement oFieldMark = null;
        // akaushik5 Changed for MITS 30290 Ends
      
        public enum NavType : int
        {
            NAV_NEW = 99,
            NAV_TO = 0,
            NAV_FIRST = 1,
            NAV_PREVIOUS = 2,
            NAV_NEXT = 3,
            NAV_LAST = 4,
            NAV_SAVE = 5,
            NAV_DELETE = 6,
            NAV_SUBMIT = 7,
            NAV_LOOKUP = 8
        }

        public string[] m_sCustomizablememofields = { "memo", "readonlymemo", "textml", "freecode", "htmltext" };//added htmltext jira 6422
        // akaushik5 Changed for MITS 30290 Starts
        //private bool m_smsSecurity;//deb :MITS 20003
        //private bool m_smsSecurityForSupp;//deb :MITS 20003
        protected bool m_smsSecurity;
        protected bool m_smsSecurityForSupp;
        // akaushik5 Changed for MITS 30290 Ends
        //rsolanki2 : extensibility 
        protected void Page_Init(object sender, EventArgs e)
        {               
            string sBasePageName = string.Empty;            
                        
            try
            {
                //Assembly a = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + adaptor.Assembly); 
                //if   ((((System.RuntimeType)(this.GetType().BaseType)).Name).ToString().Equals( "defendant", StringComparison.OrdinalIgnoreCase)))
                sBasePageName = this.GetType().BaseType.FullName.ToString();
                m_sPageName = sBasePageName.Substring(sBasePageName.LastIndexOf(".") + 1);

                //if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("CustomViewNames")))
                //{
                sCustomViewList = 
                    //Application["CustomViewNames"].ToString();
                    AppHelper.ReadCookieValue("CustomViewNames");
                //}
                if (!string.IsNullOrEmpty( sCustomViewList)
                    && sCustomViewList.ToLower().Contains(string.Concat("|", m_sPageName.Replace(".aspx",string.Empty).ToLower(), ".xml|"))
                    )
                {
                    RMExtensibilityCollection oColl =
                        Riskmaster.AppHelpers.ConfigHelper.GetRMExtensibilityForms();
                        //RMConfigurationManager.GetRMExtensibilityForms();
                    if (oColl.Count > 0)
                    {
                        //rsolanki2: Cache the <formmap> tags into a static hashtable (for the subsequent page events)
                        FormMappingCacheRecord MapItem;
                        foreach (RMExtensibilityElement xmlMapping in oColl)
                        {
                            MapItem = new FormMappingCacheRecord();
                            MapItem.BaseFormName = xmlMapping.BaseFormName;
                            MapItem.FormName = xmlMapping.FormName;
                            MapItem.TypeName = xmlMapping.TypeName;
                            MapItem.Assembly = xmlMapping.Assembly;
                            MapItem.PageLoadMethod = xmlMapping.PageLoadMethod;
                            MapItem.PageInitMethod = xmlMapping.PageInitMethod;
                            MapItem.PagePreRenderMethod = xmlMapping.PagePreRenderMethod;
                            MapItem.PageRenderMethod = xmlMapping.PageRenderMethod;

                            if (!_mappings.Contains(MapItem.BaseFormName))
                            {
                                // todo: we may want to refresh the mappings.. 
                                _mappings.Add(MapItem.BaseFormName, MapItem);
                            }

                        }//foreach
                    }

                    InvokeExtendedMethod(ePageCycle.Init);
                }

            }
            catch (Exception ex)
            {
                //todo: remove the try catch block
            }

        }

        private void InvokeExtendedMethod(ePageCycle p_PageCycle)
        {
            if (_mappings.Contains(m_sPageName))
            {                   
                FormMappingCacheRecord objFormMap = new FormMappingCacheRecord();
                objFormMap = (FormMappingCacheRecord)_mappings[m_sPageName];
                
                //rsolanki2: no call to be made if the custom method is not defined in ext config file.
                switch (p_PageCycle)
                {
                    case ePageCycle.Init:
                        if (string.IsNullOrEmpty(objFormMap.PageInitMethod)) return;
                        break;
                    case ePageCycle.Load:
                        if (string.IsNullOrEmpty(objFormMap.PageLoadMethod)) return;
                        break;
                    case ePageCycle.Prerender:
                        if (string.IsNullOrEmpty(objFormMap.PagePreRenderMethod)) return;                        
                        break;
                    case ePageCycle.Render:
                        if (string.IsNullOrEmpty(objFormMap.PageRenderMethod)) return;                        
                        break;                    
                }
               
                Assembly a = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + objFormMap.Assembly);                    

                // Get the type to use.
                Type myType = a.GetType(objFormMap.TypeName);  // e.g. "Riskmaster.UI.UI.Custom.Defendant"

                // Get the method to call.
                MethodInfo myMethod = null ;
                switch (p_PageCycle)
                {
                    case ePageCycle.Init:
                        myMethod = myType.GetMethod(objFormMap.PageInitMethod);
                        break;
                    case ePageCycle.Load:
                        myMethod = myType.GetMethod(objFormMap.PageLoadMethod);
                        break;
                    case ePageCycle.Prerender:
                        myMethod = myType.GetMethod(objFormMap.PagePreRenderMethod);
                        break;
                    case ePageCycle.Render:
                        myMethod = myType.GetMethod(objFormMap.PageRenderMethod);
                        break;
                    //default:
                    //    break;
                }
               
                // Create an instance.
                object obj = Activator.CreateInstance(myType);

                object[] parameters = new object[1];

                //string reply = null;
                parameters[0] = Page;

                // Execute the method.
                myMethod.Invoke(obj, parameters);
            }
        
        }

        //rsolanki2 : extensibility 
        protected void Page_PreRender(object sender, EventArgs e)
        {
            InvokeExtendedMethod(ePageCycle.Prerender);          
        }

        //rsolanki2 : extensibility 
        protected void Page_Render(object sender, EventArgs e)
        {
            InvokeExtendedMethod(ePageCycle.Render);            
        }
        protected override void InitializeCulture()
        {
            if (Request.Form["currencytype"] != null)
            {
                try
                {                    
                    if (!string.IsNullOrEmpty(Request.Form["currencytype"]) && (Request.Form["currencytype"].IndexOf("|") > -1))
                    {
                        Culture = Request.Form["currencytype"].Split('|')[1];
                        UICulture = Request.Form["currencytype"].Split('|')[1];
                        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["currencytype"].Split('|')[1]);
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["currencytype"].Split('|')[1]);
                    }                    
                }
                catch (Exception ex)
                {
                }
            }
            base.InitializeCulture();
        }//Deb
        protected void FDMPageLoad()
        {
            string sCMDVal = string.Empty;
            Control oControl = null;                                                

            if (!IsPostBack)
            {
                LoadInitialPage();
            }
            else
            {
                oControl = this.Form.FindControl("SysCmd");
                if ((oControl != null) && (oControl is WebControl))
                {
                    sCMDVal = ((TextBox)oControl).Text;
                }
               if(sCMDVal=="7")
                    NavigateRecord(NavType.NAV_SUBMIT);
                //Start by Shivendu for lookup for MoveTo
               if (sCMDVal == "0")
                   NavigateRecord(NavType.NAV_TO);
               //End by Shivendu for lookup
               if (sCMDVal == "9")
                   NavigateRecord(NavType.NAV_SAVE);
               
                if (sCMDVal == "8")
                   NavigateRecord(NavType.NAV_LOOKUP);
            }
            //oControl = this.Form.FindControl("currencytype");
            //if ((oControl != null) && (oControl is WebControl))
            //{
            //    if (!string.IsNullOrEmpty(((TextBox)oControl).Text) && ((TextBox)oControl).Text.IndexOf("|") > -1)
            //    {
            //        Culture = ((TextBox)oControl).Text.Split('|')[1];
            //        UICulture = ((TextBox)oControl).Text.Split('|')[1];
            //        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(((TextBox)oControl).Text.Split('|')[1]);
            //        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(((TextBox)oControl).Text.Split('|')[1]);
            //    }
            //}//Deb
            //Change the action attribute so the postback page url will stay the same.
            if (HttpContext.Current.Items["OriginalPageName"] != null)
            {
                Page.Form.Attributes["action"] = HttpContext.Current.Items["OriginalPageName"].ToString();
            }

            //Resetting SysCmd
            ResetSysCmd();

            //rsolanki2: extensibility updates.
            InvokeExtendedMethod(ePageCycle.Load);
            
            //Deb: ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScript(sCulture, this);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture,this);
                
            }
            //Register the ZipCode and Phone Format
            if (Request.QueryString["CountrySelectedId"] != null)
            {
                AppHelper.RegionalFormatsScript(Request.QueryString["CountrySelectedId"], this);
                //PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
                //isreadonly.SetValue(this.Request.QueryString, false, null);
                //this.Request.QueryString.Remove("CountrySelectedId");
                //isreadonly.SetValue(this.Request.QueryString, true, null);
            }
            else
            {
                AppHelper.RegionalFormatsScript(string.Empty, this);
            }
            //Deb: ML Changes
        }


        /// <summary>
        /// Retrieve values from query string and load the page
        /// </summary>
        protected void LoadInitialPage()
        {
            //Retrieve from Query string the values
            string sRecordID = string.Empty;
            string sParentID = string.Empty;
            string sParentSysFormName = string.Empty;
            string sParamValue = string.Empty;
            string sCMDVal = string.Empty;
            Control oControl = null;

            //Except recordID and parentID, all the other parameters are for textbox controls
            foreach (string sKey in Request.QueryString)
            {
                switch (sKey)
                {
                    case "recordID":
                        sRecordID = Request.QueryString[sKey];
                        break;
                    case "parentID":
                        sParentID = Request.QueryString[sKey];
                        break;
                    case "parentsysformname":
                        sParentSysFormName = Request.QueryString[sKey];
                        break;
                    case "SysCmd":
                        sCMDVal = Request.QueryString[sKey];
                        break;
                    default:
                        sParamValue = Request.QueryString[sKey];
                        // npadhy MITS 17678 If there is & in the Querystring passed from the Javascript, then we repace the &
                        // by the Character pair of "^@" in javascript and while retrieving the value from Querystring, we replace
                        // the Character pair of "^@" by &
                        sParamValue = sParamValue.Replace("^@", "&");
                        //ybhaskar MITS 14172
                        if (sParamValue.StartsWith("%") && sParamValue.EndsWith("%"))
                        {
                            // Get the Control from the Calling Page
                            sParamValue = sParamValue.Substring(1, sParamValue.Length - 2);
                            // Get the Value From Control
                            sParamValue = Request.Form[sParamValue];
                        }
                        oControl = this.Form.FindControl(sKey);
                        if ((oControl != null) && (oControl is WebControl))
                        {
                            DatabindingHelper.SetValue2Control((WebControl)oControl, sParamValue);
                        }
                        break;
                }
            }

            //For MDI navigation, the record id could be negative for new record
            if (!string.IsNullOrEmpty(sRecordID))
            {
                int iRecordID = int.Parse(sRecordID);
                if (iRecordID < 0)
                    sRecordID = "0";
            }
            if (!string.IsNullOrEmpty(sParentID))
            {
                int iParentID = int.Parse(sParentID);
                if (iParentID < 0)
                    sParentID = "0";
            }

            if ((!string.IsNullOrEmpty(sRecordID)) && sRecordID != "0")
            {
                //Set SysFormId value
                string sSysFormIdName = ((TextBox)this.Form.FindControl("SysFormIdName")).Text;
                Control ctrlSysFormID = this.Form.FindControl(sSysFormIdName);
                if (ctrlSysFormID != null)
                {
                    ((TextBox)ctrlSysFormID).Text = sRecordID;
                }
                Control oParentSysName = this.Form.FindControl("SysFormPForm");
                if (oParentSysName != null && !string.IsNullOrEmpty(sParentSysFormName))
                {
                    if (oParentSysName != null)
                        ((TextBox)oParentSysName).Text = sParentSysFormName;
                } 
                
                NavigateRecord(NavType.NAV_TO);
            }
            else
            {
                //Set parent ID
                if ((!string.IsNullOrEmpty(sParentID)) && sParentID != "0")
                {
                    Control oSysFormPIdName = this.Form.FindControl("SysFormPIdName");
                    if (oSysFormPIdName != null)
                    {
                        string sSysFormPIdName = ((TextBox)oSysFormPIdName).Text;
                        if (!string.IsNullOrEmpty(sSysFormPIdName))
                        {
                            Control oParentID = this.Form.FindControl(sSysFormPIdName);
                            if (oParentID != null)
                                ((TextBox)oParentID).Text = sParentID;
                        }
                    }
                    Control oParentSysName = this.Form.FindControl("SysFormPForm");
                    if (oParentSysName != null && !string.IsNullOrEmpty(sParentSysFormName))
                    {
                        if (oParentSysName != null)
                            ((TextBox)oParentSysName).Text = sParentSysFormName;
                    } 
                }
                //Mjain8 Added: Earlier Nav_new was overriding syscmd values for all page open requests while in case of child windows it will need to navigate on first record 
                switch (sCMDVal)
                {
                    case "1":
                        NavigateRecord(NavType.NAV_FIRST);
                        break;
                    case "0":
                        //Geeta : Added for Navigating to a record
                        NavigateRecord(NavType.NAV_TO);
                        break;
                    default:
                        NavigateRecord(NavType.NAV_NEW);
                        break;
                }
            }

        }

        /// <summary>
        /// Page-level error handling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Error(Object sender, EventArgs e)
        {
            Server.Transfer("~/GenericErrorPage.aspx");
        }


        /// <summary>
        /// Called when New button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateNew(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_NEW);
        }


        /// <summary>
        /// Called when Move First button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateFirst(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_FIRST);
        }

        /// <summary>
        /// Called when Move Previous button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigatePrev(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_PREVIOUS);
        }

        /// <summary>
        /// Called when Move Next button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateNext(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_NEXT);
        }

        /// <summary>
        /// Called when Move Last button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateLast(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_LAST);
        }

        /// <summary>
        /// Called when Save button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateSave(object sender, EventArgs e)
        {
            #region MMSEA Hidden Edit Flag
            //sgoel6 Medicare 05/22/2009
            bool bResetMMSEAEditFlag = false;
            TextBox oMMSEASysFormName = (TextBox)Page.FindControl("SysFormName");
            if (oMMSEASysFormName != null)
            {
                if (oMMSEASysFormName.Text == "entitymaint" || oMMSEASysFormName.Text == "orghierarchymaint")
                {
                    bResetMMSEAEditFlag = true;
                }
            }            
            #endregion
            NavigateRecord(NavType.NAV_SAVE);
            //sgoel6 Medicare 05/22/2009
            if (bResetMMSEAEditFlag)
            {
                TextBox ohdnMMSEAEditFlag = (TextBox)Page.FindControl("hdnMMSEAEditFlag");
                ohdnMMSEAEditFlag.Text = "false";
            }

            //MITS 24647 : Raman Bhatia : changed on 3/1/2012
            //We need to open Edit screen on a new MDI window so no need to redirect now.. below MITS is not valid now
            //commenting code
            
            //MITS 14152 Raman Bhatia: After Org Hierarchy Edit is complete we should be redirected to main page

            //TextBox sSysFormName = (TextBox)this.FindControl("SysFormName");
            //if (sSysFormName != null && sSysFormName.Text == "orghierarchymaint")
            //{
            //    TextBox oSysIsServiceError = (TextBox)this.FindControl("SysIsServiceError");
            //    if (oSysIsServiceError != null)
            //    {
            //        if (oSysIsServiceError.Text == "1")
            //        {
            //            TextBox sLastEntityId = (TextBox)this.FindControl("entityid");
            //            string sPath = "/RiskmasterUI/UI/OrganisationHierarchy/OrgHierarchyMaintenance.aspx";
            //            if (sLastEntityId != null)
            //            {
            //                sPath = sPath + "?lastviewedentityid=" + sLastEntityId.Text;
            //            }
            //            Server.Transfer(sPath , false);
         
            //        }
            //    }
            //}

        }

        /// <summary>
        /// Called when Delete button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateDelete(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_DELETE);

            //MITS 24647 : Raman Bhatia : changed on 3/1/2012
            //We need to open Edit screen on a new MDI window so no need to redirect now.. below MITS is not valid now
            //commenting code
            
            //TextBox sSysFormName=(TextBox)this.FindControl("SysFormName");
            //TextBox oSysIsServiceError = (TextBox)this.FindControl("SysIsServiceError");
            //if (sSysFormName != null && sSysFormName.Text == "orghierarchymaint" && oSysIsServiceError != null && oSysIsServiceError.Text == "1") 
            //{
            //    Server.Transfer("/RiskmasterUI/UI/OrganisationHierarchy/OrgHierarchyMaintenance.aspx");
            //}
        }

        protected void NavigateSubmit(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_SUBMIT);
        }

        protected void NavigateAdmin(object sender, EventArgs e)
        {
            NavigateRecord(NavType.NAV_TO);
        }

        protected void Navigate(object sender, EventArgs e)
        {

        }

        //Done by Psarin2 for List Screen Delete
        protected void NavigateListDelete(object sender, EventArgs e)
        {
            XElement oMessageElement = GetMessageTemplate();
            GetFormVariables(oMessageElement);
            TextBox DeleteListTemplate = (TextBox)this.FindControl("DeleteListTemplate");
            string Deletedvalues = Request.Form["PageIds"];
            string[] arrDeletedvalues = Deletedvalues.Split(" ".ToCharArray()[0]);
            string[] arrNodes = GetRequestRefPath(DeleteListTemplate.Text).Split("/".ToCharArray()[0]);
            string LastNode = arrNodes[arrNodes.Length - 1];
            string othernodespath = "";
            string parentnodespath = "";
            XElement oCmdElement = null;
            int iKeyId = 0;
            int iListCount = 0;

            if (((XmlElement)Data.SelectSingleNode("//" + arrNodes[arrNodes.Length - 3])).GetAttribute("count") != "0")
            {
                //Geeta : Modified for Adding the all nodes in propertystore      
                XmlNodeList objNodeList = Data.SelectNodes("//" + arrNodes[arrNodes.Length - 2] + "[@remove='false']");
                foreach (XmlNode objNode in objNodeList)
                {
                    iKeyId = Convert.ToInt32(objNode.SelectSingleNode(LastNode).InnerText);
                    if (arrDeletedvalues.Contains(iKeyId.ToString()))
                    {
                        ((XmlElement)objNode).SetAttribute("remove", "True");
                    }
                }
                //oMessageElement.RemoveAll();
                XElement objeNodeDel = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysPropertyStore']/Instance");
                objeNodeDel.Remove();
                objeNodeDel = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysPropertyStore']");

                foreach (XmlNode objNode in objNodeList)
                {
                    if (iListCount == 0)
                    {
                        objeNodeDel.Add(XElement.Parse(objNode.ParentNode.OuterXml));
                    }
                    iListCount++;
                }

                oCmdElement = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysCmd");
                oCmdElement.Value = "6";
                //Raman Bhatia : 07/31/2008
                //Setting SessionID from QueryString into Input XML for Web-Service 
                //This is needed to ensure that correct data source is picked

                //Call WCF wrapper for cws
                string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                XmlDocument oFDMPageDom = new XmlDocument();
                oFDMPageDom.LoadXml(sReturn);
                XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                oFDMPageDom.LoadXml(oInstanceNode.OuterXml);
                Data = oFDMPageDom;
                BindData2Control(oMessageRespElement, this.Form.Controls);
                BindData2ErrorControl(sReturn);

                //Raman Bhatia: Incorporating all customizations to the page
                this.OnUpdateForm(oMessageRespElement);

                ResetSysCmd();
            }
                
        }
        //Done by Psarin2 for List Screen Delete


        /// <summary>
        /// Helper function for navigation
        /// </summary>
        /// <param name="sDirection"></param>
        // akaushik5 Changed for MITS 30290 Starts
        //private void NavigateRecord(NavType ntDirection)
        protected void NavigateRecord(NavType ntDirection)
        // akaushik5 Changed for MITS 30290 Ends
        {
            string sPageName = string.Empty; //Mridul. 12/04/09. MITS:18229
            try
            {
                //Handle interupted save. If the data changed and user want to save the change before
                //navigate away from the current record, just save the value first
                //if (ntDirection != NavType.NAV_SAVE)
                //{
                //    string sCmdQueue = string.Empty;
                //    TextBox oSysCmdQueue = (TextBox)this.FindControl("SysCmdQueue");
                //    sCmdQueue = oSysCmdQueue.Text;
                //    Type myType = this.GetType();
                //    MethodInfo myMethod = myType.GetMethod(sCmdQueue, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                //    if (myMethod != null)
                //    {
                //        myMethod.Invoke(this, new object[] { null, null });
                //    }
                //}

                string sFormNameVal = "";
                XElement oMessageElement = GetMessageTemplate();
                GetFormVariables(oMessageElement);

                //For save, get the template first
                if (ntDirection == NavType.NAV_SAVE || ntDirection == NavType.NAV_SUBMIT || ntDirection == NavType.NAV_LOOKUP)
                {
                    #region jurisdictionals research
                    //Parijat
                    //if(Session["FORMTABpvjurisgroup_controls"]!= null)
                    //{
                    //    Control ctrlCollection = (Control)Session["FORMTABpvjurisgroup_controls"];
                    //    ctrlCollection = IterateThroughChildren(ctrlCollection);
                        
                    //   Control ctrl = this.Form.FindControl("FORMTABpvjurisgroup");
                    //   ctrl.Controls.Add(ctrlCollection); 
                    //}
                    
                    #endregion
                    BindControlCollection2Data(this.Form.Controls, oMessageElement);

                    //For Modifying XML
                    ModifyXml(ref oMessageElement);

                    //Parijat: Jurisdictional --Adding table name attribute & claim_id node for saving the data
                    if (oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value == "claimwc")
                    {
                        if (oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Jurisdictionals") != null)
                        {
                            if(ViewState["Juris_TableName"]!= null)
                            {
                                if(! oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Jurisdictionals").HasAttributes)
                                    oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Jurisdictionals").SetAttributeValue("tablename", ViewState["Juris_TableName"].ToString());
   
                            }
                            if (ViewState["Juris_FirstChild_ClaimID"] != null)
                            {
                                oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Jurisdictionals").SetElementValue("CLAIM_ID", ViewState["Juris_FirstChild_ClaimID"].ToString());
                            }
                        }
                    }

                    //Geeta : Mits 13482 Acord Form
                    sFormNameVal = oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value;

                    if (sFormNameVal == "claimgc" || sFormNameVal == "claimva")
                    {
                        if (oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Acord") != null)
                        {
                            if (!oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Acord").HasAttributes)
                            {
                                oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Acord").SetAttributeValue("tablename", "CLAIM_ACCORD_SUPP");
                            }

                            oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/Acord").SetElementValue("CLAIM_ID", ViewState["Acord_FirstChild_ClaimID"].ToString());
                               
                        }
                    }
                    //Geeta : Mits 13482 Acord Form End
                    //Raman: We need to remove elements whose references were missing at time of loading
                    RemoveMissingRefs(oMessageElement);
                }

                XElement oCmdElement = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysCmd");
                if (ntDirection == NavType.NAV_NEW)
                    oCmdElement.Value = string.Empty;
                else
                    oCmdElement.Value = ((int)ntDirection).ToString();

                //Call WCF wrapper for cws
                string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                BindData2ErrorControl(sReturn);
                XmlDocument oFDMPageDom = new XmlDocument();
                oFDMPageDom.LoadXml(sReturn);

                XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);

                // rrachev JIRA 5021 moved - see befor region Jurisdictionals)
                oFDMPageDom.LoadXml(oInstanceNode.OuterXml);
                Data = oFDMPageDom;

                //Added by Amitosh for Mits 23441 (03/10/2011)
                oFieldMark = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysReadOnlyFieldMarkNode");
                //if (!string.IsNullOrEmpty(oFieldMark.Value))
                // if(oFieldMark != null && oFieldMark.Value != "")  //Mits 32157
                if (oFieldMark != null && !string.IsNullOrEmpty( oFieldMark.Value))
                { bDoFieldMarkReadOnly = true; }
                //end Amitosh
                //Added by Amitosh for R8 enhancement of LiabilityLoss
                oKillNodesFieldMark = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysKillFieldMarkNode");
                //if (!string.IsNullOrEmpty(oKillNodesFieldMark.Value))
                //  if (oKillNodesFieldMark != null && oKillNodesFieldMark.Value != "") //Mits 32157
                if (oKillNodesFieldMark != null && !string.IsNullOrEmpty( oKillNodesFieldMark.Value) ) 
                { bKillFieldMarkNodes = true; }
                //end Amitosh
                //If the page contains UserControlDataGrid, it will need to be handled specially
                if (!ErrorHelper.IsCWSCallSuccess(sReturn))
                {
                    BindData2BindRequiredFields(oMessageRespElement);

                    // In case of Enhanced Policy, the Premium Calculation Tab is generated at run time
                    // So if a Validation Error Message comes, then because of this code the Premium Calculation Page which is 
                    // Generated by the function AddPremiumCalculation() below will not be exceuted
                    //abansal23 : MITS 15702 on 05/08/2009 - For leave plan on claim, databind is necessary
                    
                    //pmittal5 Mits 16284 05/11/09 - Disable buttons if error is thrown on Non-occ Payments screen, Using switch cases instead of If-Else.
                    //if ((oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value != "policyenh") && (oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value != "leave"))
                    string sFormName = oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value;
                    switch(sFormName)
                    {
                        case "policyenh":
                        case "policyenhal":
                        case "policyenhgl":
                        case "policyenhpc":
                        case "policyenhwc":
                            break;
                        case "leave":
                            break;
                        case "nonocc":
                            break;
                        case "policy":      //added by csingh7 : 11323 : In case of error , Insuredlist value was not retained.
                            break;
                        default:
                            {
                                //Raman Bhatia 02/09/2010: MITS 19739
                                //All textareas need to be binded again even in case of error
                                List<Control> oControls = new System.Collections.Generic.List<Control>();
                                
                                //Raman Bhatia 06/09/2010 : MITS 20999
                                //All ListBox too need to be binded again in case of error

                                //AddTextAreaControlsToList(oControls, this.Form.Controls);
                                AddTextAreaAndListBoxControlsToList(oControls, this.Form.Controls);
                                BindData2Control(oMessageRespElement, oControls);
                                return;
                            }
                    }
                    //End - pmittal5
                }
                //abansal23 on 04/29/2009, to handle all the cases where error comes and view state is required.
                if (ViewState["sMissingRefs"] != null && ViewState["sMissingRefs"].ToString() != String.Empty)
                {
                    ViewState.Remove("sMissingRefs");
                }
                //If NavType is not 7 and cws call is success, reset hidden field SysPageDataChanged
                if (ntDirection != NavType.NAV_SUBMIT && ErrorHelper.IsCWSCallSuccess(sReturn))
                {
                    TextBox oSysPageDataChanged = (TextBox)Page.FindControl("SysPageDataChanged");
                    if (oSysPageDataChanged != null)
                    {
                        oSysPageDataChanged.Text = string.Empty;
                    }
                }

                // rrachev JIRA 5021 Move after AppHelper.CallCWSService
                // oFDMPageDom.LoadXml(oInstanceNode.OuterXml);
                // Data = oFDMPageDom;

                #region Jurisdictionals
                
               // if(oFDMPageDom.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").InnerText == "claimwc")
                //Parijat-- taking Jurisdictionals into consideration
                if(oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value == "claimwc")
                {
                    // npadhy MITS 14133 Deletion of an existing WC Record was giving error.
                    if (ntDirection != NavType.NAV_DELETE)
                    {
                        // akaushik5 Added for MITS 30290 Starts
                        bool IsDynamic = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["DynamicViews"]) 
                            && ntDirection == NavType.NAV_TO 
                            && ConfigurationManager.AppSettings["DynamicViews"].Contains(oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value)
                            && !string.IsNullOrEmpty(Request.Params.Get("__EVENTTARGET"));
                        if (!IsDynamic)
                        {
                            // akaushik5 Added for MITS 30290 Ends
                            AddJurisdictionals(ref oFDMPageDom);
                        }
                    }
                    else
                    {
                        Control ctrlTemp = this.FindControl("FORMTABpvjurisgroup");
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = false;
                        }
                        else
                        {
                            // Can be Top Down Layout
                            ctrlTemp = this.FindControl("pvjurisgroup");
                            if (ctrlTemp != null)
                            {
                                // It is Top Down Layout, Hide the Control
                                ctrlTemp.Visible = false;

                                // Hide the Tabpvjurisgroup as well
                                ctrlTemp = this.FindControl("Tabpvjurisgroup");
                                if (ctrlTemp != null)
                                {
                                    // It is Top Down Layout, Hide the Control
                                    ctrlTemp.Visible = false;
                                }
                            }
                        }
                        ctrlTemp = this.FindControl("TABSpvjurisgroup");
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = false;
                        }
                        ctrlTemp = this.FindControl("TBSPpvjurisgroup");
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = false;
                        }

                        // Hide the Button for EDI history as well
                        ctrlTemp = this.FindControl("btnEDIHistory");
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = false;
                        }
                    }
                }
                    
                //End Parijat
                #endregion

                #region Premium Calculation
                //Mridul. 11/30/09. MITS:18229. Commented the Generic one and have added for specific form per LOB.
                //if (oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value == "policyenh")
                sPageName = oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value.ToString();
                //Commented by Amitosh for mits 28104.Code has been moved to override of OnUpdateForm() in all the policyEnh form
                //if (string.Compare(sPageName, 0, "policyenh", 0, 9, StringComparison.OrdinalIgnoreCase) == 0)
                //{
                //    AddPremiumCalculation(ref oFDMPageDom);
                //}
                #endregion
                // npadhy Resetting the SubTitle Caption. This Caption is populated in OnUpdateForm
                Control oCtrlSubTitle = this.FindControl("formsubtitle");

                if (oCtrlSubTitle != null)
                {
                    ((Label)oCtrlSubTitle).Text = "";
                }

                //Geeta : Mits 13482 Acord Form  
                sFormNameVal = oMessageElement.XPathSelectElement("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value;
                //   if (sFormNameVal  == "claimgc" || sFormNameVal == "claimva")  //Mits 32157
                if (sFormNameVal.Equals( "claimgc") || sFormNameVal.Equals("claimva"))
                {                 
                    if (ntDirection != NavType.NAV_DELETE)
                    {
                        if (oFDMPageDom.GetElementsByTagName("Acord") != null)
                        {
                            //rupal:if condition added
                            if (oFDMPageDom.GetElementsByTagName("Acord").Item(0) != null)
                                ViewState["Acord_FirstChild_ClaimID"] = oFDMPageDom.GetElementsByTagName("Acord").Item(0).FirstChild.InnerText;
                        }
                    }
                }              

                //Raman Bhatia: Incorporating all customizations to the page
                this.OnUpdateForm(oMessageRespElement);
                //mjain8 if oMessageRespElement is blank the it will not run binddata2control MITS 14137
                if (oMessageRespElement.HasElements)
                {
                    XElement oSysSkipBindToControl = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysSkipBindToControl");
                    if (oSysSkipBindToControl != null)
                    {  
                        if(oSysSkipBindToControl.Value == "false")
                        {

                            //gagnihotri MITS 16453 05/12/2009
                            XElement oFormReadOnly = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/FormReadOnly");
                            if (oFormReadOnly != null)
                            {
                                TextBox txtFormReadOnly = (TextBox)this.FindControl("FormReadOnly"); //rjhamb 16453:Added to keep track of the last value
                                if (txtFormReadOnly != null)
                                {
                                    if (oFormReadOnly.Value.ToLower() == "disable")
                                    {
                                        m_sFormReadOnly = "Disable";
                                        txtFormReadOnly.Text = "Disable";//rjhamb 16453:Added to keep track of the last value
                                    }
                                else
                                    {
                                        //rjhamb 16453:Changed to enable the entire form only if it was disabled
                                        //m_sFormReadOnly = "Enable";
                                        if(txtFormReadOnly.Text.ToLower() == "disable")
                                        {
                                            m_sFormReadOnly = "Enable";
                                            txtFormReadOnly.Text = "Enable";//rjhamb 16453:Added to keep track of the last value
                                        }
                                        else if (m_sFormReadOnly.ToLower() == "enable")//deb :MITS 20003
                                        {
                                            m_sFormReadOnly = "Enable";
                                        }
                                        else
                                            m_sFormReadOnly = "";
                                        //rjhamb 16453:Changed to enable the entire form only if it was disabled 
                                    }
                                }
                            }

                            BindData2Control(oMessageRespElement, this.Form.Controls);
                            //Ashish Ahuja : Mits 33436 Start
                            if (sFormNameVal == "deposit")
                            {
                                if (((TextBox)this.Form.FindControl("AccountId")) != null)
                                { 
                                        XElement oBankAccId = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/FundsDeposit/BankAccId");
                                        if (oBankAccId != null && oBankAccId.Value != "")
                                        {
                                            ((TextBox)this.Form.FindControl("AccountId")).Text = oBankAccId.Value;
                                        }
                                }
                            }
                            //Ashish Ahuja : Mits 33436 End

                            //Ashish Ahuja: Claims Made Jira 1342
                            //JIRA RMA-8409 ajohari2:Start
                            //if (((TextBox)this.Form.FindControl("clm_datereported")) != null)
                            //{
                            //    XElement oDateRptdToRm = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Claim/DateRptdToRm");
                            //    if (oDateRptdToRm != null && oDateRptdToRm.Value != "")
                            //    {
                            //        ((TextBox)this.Form.FindControl("clm_datereported")).Text = (Conversion.ToDate(oDateRptdToRm.Value)).ToString("MM/dd/yyyy");
                            //    }
                            //}
                            //if (((TextBox)this.Form.FindControl("hdnClaimRptDateType")) != null)
                            //{
                            //    XElement oClaimRptDateType = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ClaimRptDateType");
                            //    if (oClaimRptDateType != null && oClaimRptDateType.Value != "")
                            //    {
                            //        ((TextBox)this.Form.FindControl("hdnClaimRptDateType")).Text = oClaimRptDateType.Value;
                            //    }
                            //}

                            //if (((TextBox)this.Form.FindControl("hdnTransStatus")) != null)
                            //{
                            //    XElement oTransStatus = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/TransStatus");
                            //    if (oTransStatus != null && oTransStatus.Value != "")
                            //    {
                            //        ((TextBox)this.Form.FindControl("hdnTransStatus")).Text = oTransStatus.Value;
                            //    }
                            //}
                            
                            if (sFormNameVal.Equals("claimgc") || sFormNameVal.Equals("claimwc"))
                            {
                                Control oControl = this.Form.FindControl("clm_datereported");
                                if ((oControl != null) && (oControl is TextBox))
                                {
                                    XElement oDateRptdToRm = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Claim/DateRptdToRm");
                                    if (oDateRptdToRm != null && oDateRptdToRm.Value != "")
                                    {
                                        ((TextBox)this.Form.FindControl("clm_datereported")).Text = (Conversion.ToDate(oDateRptdToRm.Value)).ToString("MM/dd/yyyy");
                                    }
                                }
                                else if ((oControl != null) && (oControl is Label))
                                {
                                    XElement oDateRptdToRm = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysPropertyStore']/Claim/DateRptdToRm");
                                    if (oDateRptdToRm != null && oDateRptdToRm.Value != "")
                                    {
                                        ((Label)this.Form.FindControl("clm_datereported")).Text = (Conversion.ToDate(oDateRptdToRm.Value)).ToString("MM/dd/yyyy");
                                    }
                                }

                                oControl = this.Form.FindControl("hdnClaimRptDateType");
                                if ((oControl != null) && (oControl is TextBox))
                                {
                                    XElement oClaimRptDateType = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ClaimRptDateType");
                                    if (oClaimRptDateType != null && oClaimRptDateType.Value != "")
                                    {
                                        ((TextBox)this.Form.FindControl("hdnClaimRptDateType")).Text = oClaimRptDateType.Value;
                                    }
                                }

                                oControl = this.Form.FindControl("hdnClaimRptDateType");
                                if ((oControl != null) && (oControl is TextBox))
                                {
                                    XElement oTransStatus = oMessageRespElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/TransStatus");
                                    if (oTransStatus != null && oTransStatus.Value != "")
                                    {
                                        ((TextBox)this.Form.FindControl("hdnTransStatus")).Text = oTransStatus.Value;
                                    }
                                }
                            }
                            //JIRA RMA-8409 ajohari2:End
                            //Ashish Ahuja: Claims Made Jira 1342
                            //rjhamb 16453:To disable controls which have been explicitly marked readonly after the entire form has been enabled by the code implemented by gagnihotri.
                            if (m_sFormReadOnly.ToLower() == "enable")
                            {
                                    UpdateReadOnlyFields(oMessageRespElement);
                            }
                            //rjhamb 16453:To disable controls which have been explicitly marked readonly after the entire form has been enabled by the code implemented by gagnihotri..
                            ModifyControls();

                        }                        
                        //rjhamb 21516:To allow Grid Binding when SysSkipBindToControl is true
                        else if (String.Compare(oSysSkipBindToControl.Value, "true", true) == 0)
                        {
                            BindData2BindRequiredFields(oMessageRespElement);
                        }
                        //rjhamb 21516:To allow Grid Binding when SysSkipBindToControl is true
                    }
                }

                // Associate the On click Attribute for EventNumberLookup
                TextBox txtFormName = (TextBox)this.Page.FindControl("SysFormName");
                if (txtFormName.Text.Length > 5 && txtFormName.Text.ToLower().Substring(0, 5) == "claim")
                {
                    // Get the Button
                    Button btnOpen = (Button)this.Page.FindControl("ev_eventnumber_Open");

                    if (btnOpen != null)
                    {
                        // Get the Event Id to move to
                        TextBox txtEventId = (TextBox)this.Page.FindControl("eventid");

                        if (txtEventId != null)
                            btnOpen.PostBackUrl = "event.aspx?SysFormName=event&SysCmd=0&SysFormIdName=eventid&SysFormId=" + txtEventId.Text;
                    }
                }

                //perform group association
                DatabindingHelper.PerformGroupAssociation(this);

                //disable RequiredFieldValidator and style class for SysNotReqNew fields
                //UpdateSysNotReqNewFields();

               
                //ScreenFlowStack Implementation
                //UpdateScreenFlowStack();  //pmittal5 Mits 16121 05/04/09 - Function moved to finally block

                //Resetting SysCmd
                ResetSysCmd();
                //Deb Multi
                try
                {
                    Control oControl = this.Form.FindControl("currencytype");
                    if ((oControl != null) && (oControl is WebControl))
                    {
                    if (!string.IsNullOrEmpty(((TextBox)oControl).Text) && ((TextBox)oControl).Text.IndexOf("|") > -1)
                        {
                            Culture = ((TextBox)oControl).Text.Split('|')[1];
                            UICulture = ((TextBox)oControl).Text.Split('|')[1];
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(((TextBox)oControl).Text.Split('|')[1]);
                            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(((TextBox)oControl).Text.Split('|')[1]);
                        }
                    }
                    else
                    {
                        XElement xElm = oMessageRespElement.XPathSelectElement("//BaseCurrencyType");
                        if (xElm != null)
                        {
                            Culture = xElm.Value;
                            UICulture = xElm.Value;
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(xElm.Value);
                            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(xElm.Value);
                            TextBox currencytype = new TextBox();
                            currencytype.ID = "currencytype";
                            currencytype.Text = "|" + xElm.Value;
                            currencytype.Style.Add("display", "none");
                            this.Form.Controls.Add(currencytype);
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                ErrorHelper.logErrors(ex);
                BindData2ErrorControl(error);
                //UpdateScreenFlowStack();
            }
            //pmittal5 Mits 16121 05/04/09 - Allow Back button functionality when error is thrown
            finally
            {
                UpdateScreenFlowStack();
            }
        }


        /// <summary>
        /// Jurisdictional controls are added dynamically on to the page.
        /// Created By: Parijat
        /// </summary>
        /// <param name="oFDMPageDom">The filled XmlDocument object from where the data has to be extracted </param>
        void AddJurisdictionals(ref XmlDocument oFDMPageDom)
        {
            #region add jurisdictionals
            bool jurisFlag = false;
            XmlDocument xDoc = new XmlDocument();
            //Checking and taking out the xml for which dynamic controls need to be added.
            if (oFDMPageDom.GetElementsByTagName("SysViewSection") != null)
            {
                XmlNode xmlSections = oFDMPageDom.GetElementsByTagName("SysViewSection").Item(0);
                if (xmlSections != null)
                {
                    XmlNodeList xmlList = xmlSections.ChildNodes;
                    if (xmlList.Count != 0)
                    {
                        for (int i = 0; i < xmlList.Count; i++)
                        {
                            XmlNode xmlNode = xmlList.Item(i);
                            if (xmlNode.Attributes["name"].Value == "jurisdata")
                            {
                                if (xmlNode.InnerXml != "")
                                {
                                    //Show JurisdictionTab if hidden
                                    // npadhy MITS 16407 Check if the Tab or Top down Layout is there
                                    Control ctrlTemp = this.FindControl("FORMTABpvjurisgroup");
                                    if (ctrlTemp != null)
                                    {
                                        // Tab Layout
                                        ctrlTemp.Visible = true;
                                    }
                                    else
                                    {
                                        // Can be Top Down Layout
                                        ctrlTemp = this.FindControl("pvjurisgroup");
                                        if (ctrlTemp != null)
                                        {
                                            // It is Top Down Layout, Hide the Control
                                            ctrlTemp.Visible = true;

                                            // Hide the Tabpvjurisgroup as well
                                            ctrlTemp = this.FindControl("Tabpvjurisgroup");
                                            if (ctrlTemp != null)
                                            {
                                                // It is Top Down Layout, Hide the Control
                                                ctrlTemp.Visible = true;
                                            }
                                        }
                                    }
                                    ctrlTemp = this.FindControl("TABSpvjurisgroup");
                                    if (ctrlTemp != null)
                                    {
                                        ctrlTemp.Visible = true;
                                    }
                                    ctrlTemp = this.FindControl("TBSPpvjurisgroup");
                                    if (ctrlTemp != null)
                                    {
                                        ctrlTemp.Visible = true;
                                    }

                                    // Hide the Button for EDI history as well
                                    ctrlTemp = this.FindControl("btnEDIHistory");
                                    if (ctrlTemp != null)
                                    {
                                        ctrlTemp.Visible = true;
                                    }
                                    #region Fetch Juris
                                    //string str = "<?xml version=\"1.0\"?><form name=\"claimwc\" title=\"Workers Compensation Claim\" sid=\"3000\" topbuttons=\"1\" supp=\"CLAIM_SUPP\">";
                                    //str = str + xmlNode.InnerXml + "</form>";
                                    //xDoc.LoadXml(str);
                                    #endregion
                                    jurisFlag = true;
                                    break;
                                }
                                else //Remove Jurisdiction Tab
                                {
                                    Control ctrlTemp = this.FindControl("FORMTABpvjurisgroup");
                                    if (ctrlTemp != null)
                                    {
                                        // Tab Layout
                                        ctrlTemp.Visible = false;
                                    }
                                    else
                                    {
                                        // Can be Top Down Layout
                                        ctrlTemp = this.FindControl("pvjurisgroup");
                                        if (ctrlTemp != null)
                                        {
                                            // It is Top Down Layout, Hide the Control
                                            ctrlTemp.Visible = false;

                                            ctrlTemp = this.FindControl("Tabpvjurisgroup");
                                            if (ctrlTemp != null)
                                            {
                                                // It is Top Down Layout, Hide the Tabpvjurisgroup as well
                                                ctrlTemp.Visible = false;
                                            }
                                        }
                                    }
                                    ctrlTemp = this.FindControl("TABSpvjurisgroup");
                                    if (ctrlTemp != null)
                                    {
                                        ctrlTemp.Visible = false;
                                    }
                                    ctrlTemp = this.FindControl("TBSPpvjurisgroup");
                                    if (ctrlTemp != null)
                                    {
                                        ctrlTemp.Visible = false;
                                    }

                                    // Hide the Button for EDI history as well
                                    ctrlTemp = this.FindControl("btnEDIHistory");
                                    if (ctrlTemp != null)
                                    {
                                        ctrlTemp.Visible = false;
                                    }
                                }
                            }

                        }

                    }
                }
            }
            if (jurisFlag)
            {
                if (oFDMPageDom.GetElementsByTagName("Jurisdictionals") != null)
                {
                    //oFDMPageDom.GetElementsByTagName("Jurisdictionals").Item(0).
                    //rupal, if condition added
                    if (oFDMPageDom.GetElementsByTagName("Jurisdictionals").Item(0) != null)
                    {
                        if (oFDMPageDom.GetElementsByTagName("Jurisdictionals").Item(0).Attributes["tablename"] != null)
                            ViewState["Juris_TableName"] = oFDMPageDom.GetElementsByTagName("Jurisdictionals").Item(0).Attributes["tablename"].Value;
                        if (oFDMPageDom.GetElementsByTagName("Jurisdictionals").Item(0).FirstChild != null)
                            if (oFDMPageDom.GetElementsByTagName("Jurisdictionals").Item(0).FirstChild.Name == "CLAIM_ID")
                            {
                                //BinaryFormatter binFormat = new BinaryFormatter();
                                //binFormat.Serialize(
                                //XmlSerializer xmlSer = new XmlSerializer();
                                ViewState["Juris_FirstChild_ClaimID"] = oFDMPageDom.GetElementsByTagName("Jurisdictionals").Item(0).FirstChild.InnerText;
                            }
                    }
                }
                //transforming the xml to aspx controls
                
                #region Commented in order to meet the Fetch Juris solution
                //string converted = pn.UpgradeXmlTagToAspxForm(false, false, xDoc.OuterXml, "divJuris");
                //StringBuilder convertedAspx = new StringBuilder(converted);
                ////
                ////Extracting the required controls under the DIV
                //string response = AppHelper.HTMLExtractor(convertedAspx, "FORMTABjurisgroup");
                ////Importing the namespaces in order to parse the string
                //string namespaces = "<%@ Import Namespace=\"System.Data\" %> <%@ Import Namespace=\"System.Xml\" %> <%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>" ;
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>";
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>";
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>";
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>";
                //response = namespaces + response;
                #endregion
                //CodeLookUp ctrl1 = (CodeLookUp)this.Form.FindControl("filingstateid");
                //string response = pn.FetchJurisdictionalData(Convert.ToInt32(ctrl1.CodeIdValue));

                //taking out stateid in order to fetch the jurisdictional data
                #region Research roughwork
                //string decode = System.Web.HttpUtility.HtmlDecode(convertedAspx.ToString());
                //Regex objRegExp = new Regex("<(.|\n)+?>");
                //string replace = objRegExp.Replace(decode, "");
                //replace.Trim("\t\r\n ".ToCharArray());

                //string response = convertedAspx.ToString().Remove(0, convertedAspx.ToString().IndexOf("<form"));
                //response = response.Remove(response.IndexOf("/form>") + 6);
                //string response = convertedAspx.ToString().Remove(0, convertedAspx.ToString().IndexOf("id=\"FORMTABjurisgroup\">")+23);
                //response = response.Remove(response.IndexOf("runat=\"server\" />") + 17);
                //<%@ Page Language=\"C#\" Debug=\"true\" %>  
                //XmlNameTable xm = (XmlNameTable)xmlResponse;
                //XmlTextReader reader = new XmlTextReader(xmlResponse);
                //XmlNamespaceManager nsManager = new XmlNamespaceManager(reader.NameTable);
                //nsManager.AddNamespace(string.Empty, "System.Data");
                //nsmanager.AddNamespace("asp", "http://tempuri.org/myaspnamespace");
                //xmlResponse.LoadXml(response);
                #endregion
                Control ctrl = this.Form.FindControl("FORMTABpvjurisgroup");
                if (ctrl == null)
                {
                    //npadhy MITS 16407 FORMTABpvjurisgroup does not exist on the Page, So it should be Top Down 
                    ctrl = this.FindControl("pvjurisgroup");

                    if (ctrl != null)
                    {
                        ctrl = this.FindControl("Tabpvjurisgroup");
                    }
                }
                try
                {
                    // npadhy MITS 16407 If there is no Control for adding the Juris Controls, then skip it
                    if (ctrl != null)
                    {
                        //MITS 24418 Only retrieve Jurisdictional control when the tab is in the Powerview page
                        PowerViewUpgradeBusinessHelper pn = new PowerViewUpgradeBusinessHelper();
                        string response = pn.FetchJurisdictionalData(Convert.ToInt32(oFDMPageDom.SelectSingleNode("//Claim/FilingStateId/@codeid").Value));
                        Control ctrl2 = ParseControl(response);

                        ctrl.Controls.Clear();
                        ctrl.Controls.Add(ctrl2);
                    }

                    // npadhy MITS 15482 The Data in Jurisdictional Tab is not getting Saved.
                    // Add the State Id in the ViewState as in LoadViewState the State Id is not available
                    ViewState["SelectedStateId"] = Convert.ToInt32(oFDMPageDom.SelectSingleNode("//Claim/FilingStateId/@codeid").Value);

                    //ViewState["FORMTABpvjurisgroup_controls"] = AppHelper.Zip(response);
                    #region research for serializing
                    //LosFormatter lsFormatter = new LosFormatter();//Just inorder to serialize the data and store it in viewstate
                    //MemoryStream output = new MemoryStream();
                    //lsFormatter.Serialize(output, ctrl2.t);
                    //ViewState["FORMTABpvjurisgroup_controls"] = ctrl2.ToString(); ;
                    #endregion

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                //In case of error or anyother case jurisdictionals should not populate
                Control ctrl = this.Form.FindControl("FORMTABpvjurisgroup");
                if (ctrl != null)
                    ctrl.Controls.Clear();
                else
                {
                    // Can be Top Down Layout
                    ctrl = this.FindControl("pvjurisgroup");
                    if (ctrl != null)
                    {
                        // It is Top Down Layout, Clear the Control Collection from the Tabpvjurisgroup
                        ctrl = this.FindControl("Tabpvjurisgroup");
                        if (ctrl != null)
                        {
                            ctrl.Controls.Clear();
                        }
                    }
                }
                //ViewState["FORMTABpvjurisgroup_controls"] = null;
                ViewState["Juris_TableName"] = null;
                ViewState["Juris_FirstChild_ClaimID"] = null;
            }
            #endregion
        }


        /// <summary>
        /// Premium Calculation controls are added dynamically on to the page.
        /// Created By: Naresh
        /// </summary>
        /// <param name="oFDMPageDom">The filled XmlDocument object from where the data has to be extracted </param>
      protected  void AddPremiumCalculation(ref XmlDocument oFDMPageDom,ref string p_sReadonlyFields)
        {
            #region add Premium Calculation
            bool premcalcFlag = false;
            XmlDocument xDoc = new XmlDocument();
            //Checking and taking out the xml for which dynamic controls need to be added.
            if (oFDMPageDom.GetElementsByTagName("SysViewSection") != null)
            {
                XmlNode xmlSections = oFDMPageDom.GetElementsByTagName("SysViewSection").Item(0);
                if (xmlSections != null)
                {

                    XmlNode xmlNode = xmlSections.SelectSingleNode("//section[@name='premcalc']");
                    if (xmlNode != null && xmlNode.InnerXml != "")
                    {
                        //string str = "<?xml version=\"1.0\"?><form name=\"enhancepolicy\" title=\"Policy Management\" topbuttons=\"1\" includefilename=\"../../Scripts/EnhPolicy.js\" supp=\"POLICY_ENH_SUPP\">";
                        //str = str + xmlNode.InnerXml + "</form>";
                        
                        //rsolanki2 : removing string concats
                        xDoc.LoadXml(
                            string.Concat("<?xml version=\"1.0\"?><form name=\"enhancepolicy\" title=\"Policy Management\" topbuttons=\"1\" includefilename=\"../../Scripts/EnhPolicy.js\" supp=\"POLICY_ENH_SUPP\">"
                                , xmlNode.InnerXml
                                , "</form>"));
                        premcalcFlag = true;
                    }
                }
            }
            if (premcalcFlag)
            {
                
                //transforming the xml to aspx controls
                PowerViewUpgradeBusinessHelper pn = new PowerViewUpgradeBusinessHelper();

                string converted = pn.UpgradeXmlTagToAspxForm(false, false, xDoc.OuterXml, "");
                StringBuilder convertedAspx = new StringBuilder(converted);

                string response = "<%@ Import Namespace=\"System.Data\" %> <%@ Import Namespace=\"System.Xml\" %> <%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>"
                    + "<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>"
                    + "<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>"
                    + "<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>"
                    + "<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>"
                    + "<%@ Register Assembly=\"MultiCurrencyCustomControl\" Namespace=\"MultiCurrencyCustomControl\"  TagPrefix=\"mc\" %>"
                    + AppHelper.HTMLExtractor(convertedAspx, "FORMTABpremcalc"); 

                //Importing the namespaces in order to parse the string
                //string namespaces = "<%@ Import Namespace=\"System.Data\" %> <%@ Import Namespace=\"System.Xml\" %> <%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>";
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>";
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>";
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>";
                //namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>";
                //response = namespaces + response;
                
                //deb:20003
                HtmlNode sReadOnlyField = AppHelper.HTMLNodeExtractor(convertedAspx, "SysReadonlyFields");
                //Commented by Amitosh for mits 28104
                //string sReadOnly = sReadOnlyField.Attributes["Text"].Value.Trim();
                //StringBuilder sSysReadOnly =new StringBuilder((((TextBox)this.Form.FindControl("SysReadonlyFields"))).Text.Trim());
                //sSysReadOnly.Append(sReadOnly);
                //(((TextBox)this.Form.FindControl("SysReadonlyFields"))).Text = sSysReadOnly.ToString();
                p_sReadonlyFields = sReadOnlyField.Attributes["Text"].Value.Trim();

                Control ctrl = this.Form.FindControl("FORMTABpremcalc");
                try
                {

                    Control ctrl2 = ParseControl(response);
                    ctrl.Controls.Clear();
                    ctrl.Controls.Add(ctrl2);

                    ViewState["FORMTABpremcalc_controls"] = AppHelper.Zip(response);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                //In case of error or anyother case jurisdictionals should not populate
                Control ctrl = this.Form.FindControl("FORMTABpremcalc");
                if (ctrl != null)
                    ctrl.Controls.Clear();
                ViewState["FORMTABpremcalc_controls"] = null;
            }
            #endregion
        }

        public virtual void ModifyXml(ref XElement Xelement)
        {
            if (Xelement.XPathSelectElement("//ClaimNumber")!=null)
            {
                string sClaimNumber = Xelement.XPathSelectElement("//ClaimNumber").Value;
                Xelement.XPathSelectElement("//ClaimNumber").SetValue(StandardizeHyphens(sClaimNumber));
            }
        }

        //gagnihotri MITS 16453 05/12/2009
        public virtual void ModifyControls()
        {

        }

        /// <summary>
        /// Bind data from CWS response to ASP.NET server controls
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oControls"></param>
        // akaushik5 Changed for MITS 30290 Starts
        //private void BindData2Control(XElement oMessageElement, ICollection oControls)
        protected virtual void BindData2Control(XElement oMessageElement, ICollection oControls)
        // akaushik5 Changed for MITS 30290 Ends
        {
            string sRMXRef = string.Empty;
            foreach (Control oCtrl in oControls)
            {

                if (oCtrl is WebControl)
                {
                    //gagnihotri MITS 16453 05/12/2009
                    if (m_sFormReadOnly == "Enable")
                    {
                        if (oCtrl.ClientID == oCtrl.ID)
                        {
                            //if (((WebControl)oCtrl).Attributes["rmxtype"] != "id" && ((WebControl)oCtrl).Attributes["rmxtype"] != "hidden")
                            if (((WebControl)oCtrl).Attributes["rmxtype"] != "id" && ((WebControl)oCtrl).Attributes["rmxtype"] != "hidden" && ((WebControl)oCtrl).Attributes["rmxtype"] != "textml" && ((WebControl)oCtrl).Attributes["rmxtype"] != "readonlymemo")//deb :MITS 20003
                                DatabindingHelper.EnableControls(oCtrl.ClientID, this.Page);
                        }
                        else
                        {
                            string sControlId = oCtrl.ClientID.Substring(0, (oCtrl.ClientID.Length - oCtrl.ID.Length - 1));
                            DatabindingHelper.EnableControls(sControlId, this.Page);
                        }
                    }
                    sRMXRef = ((WebControl)oCtrl).Attributes["RMXRef"];
                    if (string.IsNullOrEmpty(sRMXRef))
                        continue;
                    //Binding functions should ignore such controls
                    if (((WebControl)oCtrl).Attributes["rmxignorevalue"] != null)
                        continue;

                    string sRMXIgnoreGet = ((WebControl)oCtrl).Attributes["rmxignoreget"];

                    if (!string.IsNullOrEmpty(sRMXIgnoreGet))
                    {
                        if (sRMXIgnoreGet == "true")
                            continue;
                    }



                    // We need to find the type of the Control. The implementation changes for Controls like Combobox
                    Type controlType = oCtrl.GetType();
                    string sType = controlType.ToString();
                    int index = sType.LastIndexOf(".");
                    sType = sType.Substring(index + 1);
                    string sItemSetRef;
                    string sRMXType;
                    string sValueCollection;
                    switch (sType)
                    {
                        // For Combobox we need to keep the Collection of elements also which is bound to itemsetref node.
                        case "DropDownList":
                            sRMXRef = GetResponseRefPath(sRMXRef);
                            sItemSetRef = ((WebControl)oCtrl).Attributes["ItemSetRef"];
                            if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                            {
                                string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                                sRMXRef = lstRMXRef[0];
                            }
                            if (!string.IsNullOrEmpty(sItemSetRef))
                            {
                                sItemSetRef = GetResponseRefPath(sItemSetRef);
                                //The same value needs to be set to multiple node in the xml string

                                if (sItemSetRef.IndexOf(g_sXPathDelimiter) > 0)
                                {
                                    string[] lstRMXRef = sItemSetRef.Split(g_sXPathDelimiter.ToCharArray());
                                    sItemSetRef = lstRMXRef[0];
                                }
                                // npadhy If the Path ends with option, the ItemSetRef does not point to a Collection
                                // Removed the Option from the ItemSetRef
                                if (sItemSetRef.EndsWith("option"))
                                    sItemSetRef = sItemSetRef.Replace("/option", "");

                                sValueCollection = GetReturnValue(oMessageElement, sItemSetRef);
                            }
                            else
                            {
                                sValueCollection = "";
                            }
                            string sSelectedValue = GetReturnValue(oMessageElement, sRMXRef);
                            if (!string.IsNullOrEmpty(sValueCollection))
                                DatabindingHelper.SetValue2Control((WebControl)oCtrl, sSelectedValue, sValueCollection);

                            else
                                DatabindingHelper.SetValue2Control((WebControl)oCtrl, sSelectedValue);

                            break;
                        case "CheckBox":
                            ((CheckBox)oCtrl).InputAttributes.Add("onchange", "return setDataChanged(true);");
                            sRMXRef = GetResponseRefPath(sRMXRef);
                            //The same value needs to be set to multiple node in the xml string
                            if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                            {
                                string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                                sRMXRef = lstRMXRef[0];
                            }

                            //msampathkuma start RMA-11602
                            string readonl = ((CheckBox)oCtrl).Attributes["Disabled"];
                            if (readonl == "True")
                            {
                                ((CheckBox)oCtrl).Enabled = false;
                            }
                            //msampathkuma end RMA-11602
                            
                            string sValue = GetReturnValue(oMessageElement, sRMXRef);
                            DatabindingHelper.SetValue2Control((WebControl)oCtrl, sValue);
                            break;
                        case "CurrencyTextbox":
                            sRMXRef = GetResponseRefPath(sRMXRef);
                            //The same value needs to be set to multiple node in the xml string
                            if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                            {
                                string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                                sRMXRef = lstRMXRef[0];
                            }
                            sCurrencyMode = ((WebControl)oCtrl).Attributes["CurrencyMode"];
                            XElement xElm = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/BaseCurrencyType");
                            if (xElm != null)
                            {
                                if (string.IsNullOrEmpty(sCurrencyMode))
                                {
                                    ((CurrencyTextbox)oCtrl).SetProperties(xElm.Value, xElm.Value);
                                }
                                else
                                {
                                    ((CurrencyTextbox)oCtrl).SetProperties(null, xElm.Value);
                                }
                            }
                            sValue = GetReturnValue(oMessageElement, sRMXRef);
                            DatabindingHelper.SetValue2Control((CurrencyTextbox)oCtrl, sValue);
                            break;
                        default:
                            sRMXRef = GetResponseRefPath(sRMXRef);
                            //The same value needs to be set to multiple node in the xml string
                            if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                            {
                                string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                                sRMXRef = lstRMXRef[0];
                            }

                            sValue = GetReturnValue(oMessageElement, sRMXRef);
                            DatabindingHelper.SetValue2Control((WebControl)oCtrl, sValue);
                            break;
                    }

                    //Raman Bhatia: Update customizations for rows and cols in freecode,memo,textml and readonly memo controls
                    sRMXType = ((WebControl)oCtrl).Attributes["RMXType"];
                    if (m_sCustomizablememofields.Contains(sRMXType))
                    {
                        DatabindingHelper.UpdateCustomizedControls(oMessageElement, sRMXType, oCtrl);
                    }
                    //deb :MITS 20003
                    if (m_smsSecurity && !m_smsSecurityForSupp)
                    {
                        if (oCtrl.ClientID == oCtrl.ID)
                        {
                            if (((WebControl)oCtrl).Attributes["rmxtype"] != "id" && ((WebControl)oCtrl).Attributes["rmxtype"] != "hidden" && ((WebControl)oCtrl).GetType().Name != "Button" && !oCtrl.ClientID.StartsWith("supp_"))//deb :MITS 20003
                                //if (((WebControl)oCtrl).Attributes["rmxtype"] != "id" && ((WebControl)oCtrl).Attributes["rmxtype"] != "hidden")    
                                DatabindingHelper.DisableControls(oCtrl.ClientID, this.Page);
                        }
                        else
                        {
                            if (!oCtrl.ClientID.StartsWith("supp_"))
                            {
                                string sControlId = oCtrl.ClientID.Substring(0, (oCtrl.ClientID.Length - oCtrl.ID.Length - 1));
                                DatabindingHelper.DisableControls(sControlId, this.Page);
                            }
                        }
                    }
                    else if (m_smsSecurity && m_smsSecurityForSupp)
                    {
                        if (oCtrl.ClientID == oCtrl.ID)
                        {
                            if (((WebControl)oCtrl).Attributes["rmxtype"] != "id" && ((WebControl)oCtrl).Attributes["rmxtype"] != "hidden" && ((WebControl)oCtrl).GetType().Name != "Button")//deb :MITS 20003
                                DatabindingHelper.DisableControls(oCtrl.ClientID, this.Page);
                        }
                        else
                        {
                                string sControlId = oCtrl.ClientID.Substring(0, (oCtrl.ClientID.Length - oCtrl.ID.Length - 1));
                                DatabindingHelper.DisableControls(sControlId, this.Page);
                        } 
                    }
                    else if (!m_smsSecurity && m_smsSecurityForSupp)
                    {
                        if (oCtrl.ClientID == oCtrl.ID)
                        {
                            if (oCtrl.ClientID.StartsWith("supp_"))//deb :MITS 20003
                                DatabindingHelper.DisableControls(oCtrl.ClientID, this.Page);
                        }
                        else
                        {
                            if (oCtrl.ClientID.StartsWith("supp_"))
                            {
                                string sControlId = oCtrl.ClientID.Substring(0, (oCtrl.ClientID.Length - oCtrl.ID.Length - 1));
                                DatabindingHelper.DisableControls(sControlId, this.Page);
                            }
                        }
                    }
                    //deb :MITS 20003
                    //gagnihotri MITS 16453 05/12/2009
                    else
                    {
                        if (m_sFormReadOnly == "Disable")
                        {
                            if (oCtrl.ClientID == oCtrl.ID)
                            {
                                if (((WebControl)oCtrl).Attributes["rmxtype"] != "id" && ((WebControl)oCtrl).Attributes["rmxtype"] != "hidden")
                                    DatabindingHelper.DisableControls(oCtrl.ClientID, this.Page);
                            }
                            else
                            {
                                string sControlId = oCtrl.ClientID.Substring(0, (oCtrl.ClientID.Length - oCtrl.ID.Length - 1));
                                DatabindingHelper.DisableControls(sControlId, this.Page);
                            }
                        }
                    }
                }
                else if (oCtrl is UserControlDataGrid)
                {
                    if (m_smsSecurity)
                        ((UserControlDataGrid)oCtrl).HideButtons = "New|Edit|Delete|Clone";//deb :MITS 20003
                    //gagnihotri MITS 16453 05/12/2009
                    else if (m_sFormReadOnly == "Disable")
                        ((UserControlDataGrid)oCtrl).HideButtons = "New|Edit|Delete|Clone";
                    //averma62 - 27468 else if (m_sFormReadOnly == "Enable") ;
                    //averma62 - 27468((UserControlDataGrid)oCtrl).HideButtons = "";
                    /*MITS #34276 Starts  Added for Entity ID Type Edit and Delete*/
                    string sFormNameVal = String.Empty;
                    sFormNameVal = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName").Value;
                    if (((UserControlDataGrid)oCtrl).GridName.ToLower() == "entityxentityidtypegrid")
                    {
                        XElement oSysExEntityPermission = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/EntityXEntityIDType");
                        if (oSysExEntityPermission != null)
                        {
                            ((UserControlDataGrid)oCtrl).ValidateButtons = String.Empty;
                            if (oSysExEntityPermission.Attribute("IsNew") != null && !Convert.ToBoolean(oSysExEntityPermission.Attribute("IsNew").Value))
                            {
                                ((UserControlDataGrid)oCtrl).ValidateButtons += "NewValidate,";
                            }
                            if (oSysExEntityPermission.Attribute("IsUpdate") != null && !Convert.ToBoolean(oSysExEntityPermission.Attribute("IsUpdate").Value))
                            {
                                ((UserControlDataGrid)oCtrl).ValidateButtons += "EditValidate,";
                            }
                            if (oSysExEntityPermission.Attribute("IsDelete") != null && !Convert.ToBoolean(oSysExEntityPermission.Attribute("IsDelete").Value))
                            {
                                ((UserControlDataGrid)oCtrl).ValidateButtons += "DeleteValidate,";
                            }
                            if (oSysExEntityPermission.Attribute("IsVendorNew") != null && !Convert.ToBoolean(oSysExEntityPermission.Attribute("IsVendorNew").Value))
                            {
                                ((UserControlDataGrid)oCtrl).ValidateButtons += "ValidateNewVendor";
                            }
                            ((UserControlDataGrid)oCtrl).ValidateButtons = ((UserControlDataGrid)oCtrl).ValidateButtons.TrimEnd(',');
                        }
                    }
                    /* Ends  MITS #34276    Added for ID Type Vendor Edit and Delete*/
                    XmlDocument objXml = new XmlDocument();
                    XmlReader objReader;
                    objReader = oMessageElement.CreateReader();
                    objXml.Load(objReader);
                    ((UserControlDataGrid)oCtrl).BindData(objXml);
                }
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        BindData2Control(oMessageElement, oCtrl.Controls);
                }
                // Added by Amitosh for Mits 23441
                if (bDoFieldMarkReadOnly)
                {
                    updateControls(oCtrl, oFieldMark);
                }
                // Added by Amitosh for R8 enhancement of LiabilityLoss
                if (bKillFieldMarkNodes)
                {
                    HideFieldMarkNodes(oCtrl, oKillNodesFieldMark);
                }
            }
        }
        /// <param name="oMessageElement"></param>
        private void BindData2BindRequiredFields(XElement oMessageElement)
        {
            string sSysRequiredBindFields = string.Empty;
            TextBox oSysRequiredBindFields = (TextBox)this.FindControl("SysBindingRequiredFields");
            if (oSysRequiredBindFields != null)
                sSysRequiredBindFields = oSysRequiredBindFields.Text;
            if (string.IsNullOrEmpty(sSysRequiredBindFields))
                return;

            string[] arSysRequiredBindFields = sSysRequiredBindFields.Split('|');
            List<Control> oControls = new System.Collections.Generic.List<Control>();
            //ControlCollection oControCollection = new ControlCollection(this);
            foreach (string sControlName in arSysRequiredBindFields)
            {
                if (!string.IsNullOrEmpty(sControlName))
                {
                    Control oControl = this.FindControl(sControlName);
                    if (oControl != null)
                    {
                        oControls.Add(oControl);
                    }
                }
            }
            if (oControls.Count > 0)
            {
                BindData2Control(oMessageElement, oControls);
            }
        }

        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(diaryDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        private bool bListRowPresent(DataSet RecordsSet)
        {
            try
            {
                if (RecordsSet.Tables["listrow"] != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                return false;
            }
        }
        // akaushik5 Changed for MITS 30290 Starts
        //private string GetReturnValue(XElement oMessageElement, string sPath)
        /// <summary>
        /// Gets the return value.
        /// </summary>
        /// <param name="oMessageElement">The o message element.</param>
        /// <param name="sPath">The s path.</param>
        /// <returns></returns>
        protected string GetReturnValue(XElement oMessageElement, string sPath)
        // akaushik5 Changed for MITS 30290 Ends
        {

            //check if XPath is for an attribute
            string sValue = string.Empty;
            XElement oElement = null;
            int index = sPath.LastIndexOf("/");
            string sName = sPath.Substring(index + 1);
            if (sName.StartsWith("@"))
            {
                sName = sName.Substring(1);
                string sElementName = sPath.Substring(0, index);
                oElement = oMessageElement.XPathSelectElement(sElementName);
                if (oElement != null)
                {
                    sValue = oElement.Attribute(sName).Value;
                }
            }
            else
            {
                oElement = oMessageElement.XPathSelectElement(sPath);
                if (oElement != null)
                {
                    //For muti-code data type
                    if (oElement.HasElements)
                    {
                        string sChildName = ((XElement)(oElement.FirstNode)).Name.ToString();
                        switch (sChildName)
                        {
                            // npadhy Added a case for option to cater the combobox control, 
                            // which has the item collection in option node.
                            case "Item":
                            case "option":
                                sValue = oElement.ToString();
                                break;
                            default:
                                sValue = oElement.FirstNode.ToString();
                                break;
                        }
                    }
                    else
                        sValue = oElement.Value;
                }
                else
                {
                    sMissingRefs.Add(sPath);

                    if (sPath.Contains("SysPropertyStore"))
                    {
                        sPath = sPath.Replace("Param[@name='SysPropertyStore']", "Param[@name='SysPropertyStore']/Instance");
                    }
                    sMissingRefs.Add(sPath);
                }
            }
            ViewState["sMissingRefs"] = sMissingRefs;
            return sValue;
        }         
    
        //Done by Psarin2 for Navigation ..
        //Created a new function to retrieve QueryStringValues bcoz we needed a centralized function for the future 
        //All changes related to retrieval would be done here
        private string GetValues(string value)
        {
            string act = "";
            if (Request.QueryString[value] != null)
            {
                act = Request.QueryString[value];
            }
            return act;
        }
        //Done by Psarin2 for Navigation
        //Get the values for SysFormVariables element

        /// <summary>
        /// Get the values for SysFormVariables element
        /// </summary>
        /// <param name="oMessageElement"></param>
        private void GetFormVariables(XElement oMessageElement)
        {
            XElement oSysFormName = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName");
            TextBox txtSysFormName = (TextBox)this.Form.FindControl("SysFormName");
            oSysFormName.Value = txtSysFormName.Text;

            //Set SysFormId value
            XElement oSysFormIdName = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormIdName");
            TextBox txtSysFormIdName = (TextBox)this.Form.FindControl("SysFormIdName");
            oSysFormIdName.Value = txtSysFormIdName.Text;

            //Set SysSid value
            XElement oSysSid = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysSid");
            TextBox txtSysSid = (TextBox)this.Form.FindControl("SysSid");
            oSysSid.Value = txtSysSid.Text;

            //Set SysSerializationConfig value
            XElement oSysSerializationConfig = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysSerializationConfig");
            TextBox txtSysSerializationConfig = (TextBox)this.Form.FindControl("SysSerializationConfig");
            oSysSerializationConfig.Value = txtSysSerializationConfig.Text;
            
            //Set SysNotReqNew
            XElement oSysNotReqNew = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysNotReqNew");
            TextBox txtSysNotReqNew = (TextBox)this.Form.FindControl("SysNotReqNew");
            if (txtSysNotReqNew != null)
                oSysNotReqNew.Value = txtSysNotReqNew.Text;

            //Set SysSkipBindToControl
            XElement oSysSkipBindToControl = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysSkipBindToControl");
            //Mona: We dont need a control.We will just set the default value of this node as false and will set it as
            // true, in business layer, whenever we wish to skip binding to control.
            //TextBox txtSysSkipBindToControl = (TextBox)this.Form.FindControl("SysSkipBindToControl");
            oSysSkipBindToControl.Value = "false";

            XElement oSysClassName = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysClassName");
            TextBox txtSysClassName = (TextBox)this.Form.FindControl("SysClassName");
            oSysClassName.Value = txtSysClassName.Text;

            XElement oSysLookupClass = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysLookupClass");
            TextBox txtSysLookupClass = (TextBox)this.Form.FindControl("SysLookupClass");
            if(txtSysLookupClass != null)
                oSysLookupClass.Value = txtSysLookupClass.Text;

            XElement oSysLookupRecordId = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysLookupRecordId");
            TextBox txtSysLookupRecordId = (TextBox)this.Form.FindControl("SysLookupRecordId");
            if (txtSysLookupRecordId != null)
                oSysLookupRecordId.Value = txtSysLookupRecordId.Text;

            XElement oSysLookupAttachNodePath = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysLookupAttachNodePath");
            TextBox txtSysLookupAttachNodePath = (TextBox)this.Form.FindControl("SysLookupAttachNodePath");
            if (txtSysLookupAttachNodePath != null)
                oSysLookupAttachNodePath.Value = txtSysLookupAttachNodePath.Text;

            XElement oSysLookupResultConfig = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysLookupResultConfig");
            TextBox txtSysLookupResultConfig = (TextBox)this.Form.FindControl("SysLookupResultConfig");
            if (txtSysLookupResultConfig != null)
                oSysLookupResultConfig.Value = txtSysLookupResultConfig.Text;

            
            //Done by Psarin2 for Navigation
            //Mjain8 changed for buttons on screen for getting right value when control have different name than the existing ones
            // npadhy Changed for Buttons on screen for getting the right value when Control name on Parent page is different
            // than the Control name on child page. SysexMapCtrl is for Mapping the control with different sysex.
            Control oControl = null;
            XElement oSysExData = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData");
            string sPath = GetValues("SysEx");
            string sSysExChildControl = GetValues("SysExChildControl");
            if (sPath != "")
            {
                string[] Paths = GetValues("SysEx").Split("|".ToCharArray()[0]);
                string[] sSysControls = GetValues("SysExMapCtl").Split("|".ToCharArray()[0]);

                if (sSysExChildControl == "")
                {
                    for (int i = 0; i < Paths.Length; i++)
                    {
                        // This Condition will be met if the Controls on both Pages are same and Path in Sysex is different
                        string value = string.Empty;
                        if (Request.Form[sSysControls[i].ToLower()] != null)
                        {
                             value = Request.Form[sSysControls[i].ToLower()].ToString();
                        }
                        DatabindingHelper.SetValue2Message(oMessageElement, "./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData" + "/" + Paths[i], value);
                        //Mjain8 added setting control's values on child page as in oSysFormPIdName case it may override message elemnt with blank value
                        oControl = this.Form.FindControl(sSysControls[i].ToLower());
                        if ((oControl != null) && (oControl is WebControl))
                        {
                            DatabindingHelper.SetValue2Control((WebControl)oControl, value);
                        }
                    }
                }
                else
                {
                    string[] sSysChildControls = sSysExChildControl.Split("|".ToCharArray()[0]);

                    for (int i = 0; i < Paths.Length; i++)
                    {
                        // This Condition will be met if the Controls on both Pages are same and Path in Sysex is different
                        string value = string.Empty;
                        if (!IsPostBack)
                        {
                            if (Request.Form[sSysControls[i].ToLower()] != null)
                            {
                                value = Request.Form[sSysControls[i].ToLower()].ToString();
                            }
                        }
                        else
                        {
                            if (Request.Form[sSysChildControls[i].ToLower()] != null)
                            {
                                value = Request.Form[sSysChildControls[i].ToLower()].ToString();
                            }
                        }
                        DatabindingHelper.SetValue2Message(oMessageElement, "./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData" + "/" + Paths[i], value);
                        //Mjain8 added setting control's values on child page as in oSysFormPIdName case it may override message elemnt with blank value
                        oControl = this.Form.FindControl(sSysChildControls[i].ToLower());
                        if ((oControl != null) && (oControl is WebControl))
                        {
                            DatabindingHelper.SetValue2Control((WebControl)oControl, value);
                        }
                    }
                }
            }
            //Done by Psarin2 for Navigation

            //Done by Raman Bhatia for handling SysExternalParam
            string sSysExternalParam = Request.QueryString["SysExternalParam"];
            if (!String.IsNullOrEmpty(sSysExternalParam))
            {
                //Added Rakhi for MITS 18939 on similar lines of MITS 17678:If there is & in the Querystring passed from the Javascript, then we repace the &
                // by the Character pair of "^@" in javascript and while retrieving the value from Querystring, we replace
                // the Character pair of "^@" by &
                sSysExternalParam =  sSysExternalParam.Replace("^@", "&amp;");       
                XElement oSysExtParam = XElement.Parse(sSysExternalParam);
                oSysExData.Add(oSysExtParam);
            }
            
            //nnorouzi; for prper Navigation we make sure both Parent Name and Parent Id are set
            Control oSysFormPIdName = this.Form.FindControl("SysFormPIdName");
            string sParentID = string.Empty;
            if (oSysFormPIdName != null)
            {
                string sSysFormPIdName = ((TextBox)oSysFormPIdName).Text;
                if (!string.IsNullOrEmpty(sSysFormPIdName))
                {
                    BindSingleControl2Data(this.Form.FindControl(sSysFormPIdName), oMessageElement);
                    //Mona
                    XElement oMessageSysFormPIDName = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormPIdName");
                    
                    oMessageSysFormPIDName.Value = sSysFormPIdName;

                    BindSingleControl2Data(this.Form.FindControl("SysFormPForm"), oMessageElement);
                }
            }

            XElement oSysFormId = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormId']");
            Control ctrlSysFormID = this.Form.FindControl(oSysFormIdName.Value);
            if (ctrlSysFormID != null)
            {
                oSysFormId.Value = ((TextBox)ctrlSysFormID).Text;
            }
            
            Control ctrlSysFormIdTemp = this.Form.FindControl("SysFormId");
            if ((oSysFormId.Value == "" || oSysFormId.Value == "0") && ctrlSysFormIdTemp != null)
            {
                oSysFormId .Value = ((TextBox)ctrlSysFormIdTemp).Text;
            }

            //Except recordID and parentID, all the other parameters are for textbox controls
            foreach (string sKey in Request.QueryString)
            {
                switch (sKey)
                {
                    case "recordID":
                        break;
                    case "parentID":
                        break;
                    default:
                        BindSingleControl2Data(this.Form.FindControl(sKey), oMessageElement);
                        break;
                }
            }
            //case for postback pages where we want to retain some controls
            Control oSysPostback = this.Form.FindControl("SysPostback");
            //Control oCtrl = null;
            if (oSysPostback != null)
            {
                string sSysPostback = ((TextBox)oSysPostback).Text;
                if (sSysPostback != "")
                {
                      string[] sContrls = sSysPostback.Split("|".ToCharArray()[0]);
                      foreach (string sCtrl in sContrls)
                      {
                          //oCtrl = this.Form.FindControl(sCtrl);
                          BindSingleControl2Data(this.Form.FindControl(sCtrl), oMessageElement);
                      } 
                }
 
            }

            // Geeta 19341  : Split architecture issue  ACORD and FROI
            if (oSysFormName.Value == "claimgc" || oSysFormName.Value == "claimwc" || oSysFormName.Value == "claimva" || oSysFormName.Value == "adjuster")
            {
                XElement oRequestHost = oMessageElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/RequestHost");
                oRequestHost.Value = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("RiskmasterUI") + 12);
            }
        }

        internal void BindData2ErrorControl(string oMessageElement)
        {
            Label lblError = (Label)this.FindControl("lblError");
            bool bIsSuccess = false;
            lblError.Text = ErrorHelper.FormatServiceErrors(oMessageElement, ref bIsSuccess);
            TextBox oSysIsServiceError = (TextBox)this.FindControl("SysIsServiceError");
            if (oSysIsServiceError != null)
            {
                if (bIsSuccess == true)
                {
                    oSysIsServiceError.Text = "1";
                }
                else
                {

                    oSysIsServiceError.Text = "0";
                }
            }
            //Raman: if an error has occurred then we should not redirect to the new page in the case of interrupted save
            if (lblError.Text != String.Empty)
            {
                TextBox oSysCmdQueue = (TextBox)this.FindControl("SysCmdQueue");
                if (oSysCmdQueue != null)
                {
                    oSysCmdQueue.Text = "";
                }
            }
            //Raman: Handling all permission violation errors
            //tmalhotra2 mits 25435 start
            bool bBackbutton;
            XmlDocument objDoc =new XmlDocument();
            objDoc.LoadXml(oMessageElement);
            XmlNode oNode = objDoc.SelectSingleNode("//ExtendedStatus/ExtendedStatusCd");
            //if (oNode != null && (oNode.InnerText == "PermissionFailure" || oNode.InnerText == "LimitError"))
            if (oNode != null && (oNode.InnerText.Contains("PermissionFailure") || oNode.InnerText.Contains("LimitError")))
            {
                XmlNode oErrorDesc = objDoc.SelectSingleNode("//ExtendedStatus/ExtendedStatusDesc");
                //if ((oErrorDesc != null) && !string.Equals(oErrorDesc.InnerText.ToLower(), "no delete permission.", StringComparison.InvariantCultureIgnoreCase))
                if ((oErrorDesc != null) && !oErrorDesc.InnerText.ToLower().Contains("no delete permission"))
                {
                  bool bPermissionViolation = ErrorHelper.PermissionViolationErrors(oMessageElement, out bBackbutton);
                    if (bPermissionViolation)
                    {
                        StringWriter sw = new StringWriter();
                        Html32TextWriter oWriter = new Html32TextWriter(sw);
                        oWriter.RenderBeginTag("html");
                        oWriter.AddAttribute("runat", "server");
                        oWriter.RenderBeginTag("head");
                        oWriter.AddAttribute("rel", "stylesheet");
                        oWriter.AddAttribute("href", "../../App_Themes/RMX_Default/rmnet.css");
                        oWriter.AddAttribute("type", "text/css");
                        oWriter.RenderBeginTag("link");
                        oWriter.RenderEndTag();
                        oWriter.RenderEndTag();
                        oWriter.AddAttribute("onload", "if(parent.MDISetUnDirty != null) { parent.MDISetUnDirty(0); }"); // This way we tell MDI an error occurred while saving or deleting
                        oWriter.RenderBeginTag("body");
                        oWriter.Write(lblError.Text);
                        oWriter.RenderBeginTag("form");
                        oWriter.AddAttribute("runat", "server");
                        oWriter.AddAttribute("id", "form1");
                        //Back button
                        TextBox txtSysFormName = (TextBox)this.Form.FindControl("SysFormName");
                        TextBox txtSysFormIdName = (TextBox)this.Form.FindControl("SysFormIdName");
                        TextBox txtSysFormPIdName = (TextBox)this.Form.FindControl("SysFormPIdName");
                        if (txtSysFormName != null && (txtSysFormIdName != null || txtSysFormPIdName != null))
                        {
                            TextBox txtSysFormId = null;
                            if (txtSysFormName.Text.ToLower().EndsWith("list") && !String.IsNullOrEmpty(txtSysFormPIdName.Text))
                            {
                                txtSysFormId = (TextBox)this.Form.FindControl(txtSysFormPIdName.Text);
                            }
                            else
                            {
                                txtSysFormId = (TextBox)this.Form.FindControl(txtSysFormIdName.Text);
                            }
                            if (txtSysFormId != null && txtSysFormId.Text != "" && txtSysFormId.Text != "0" && bBackbutton)
                            {
                                oWriter.AddAttribute("type", "button");
                                oWriter.AddAttribute("class", "button");
                                oWriter.AddAttribute("value", "Back");
                                //MGaba2:MITS 22185: In case of Admin Tracking,in case module security fails for some permission say Delete
                                //and Back button is clicked,page was crashing bcoz formname was coming like admintracking|BOND_ABS
                                //string sOnclick = "window.location.href='" + txtSysFormName.Text + ".aspx?recordID=" + txtSysFormId.Text + "';";
                                string sOnclick = "window.location.href='" + txtSysFormName.Text.Replace("|", "") + ".aspx?recordID=" + txtSysFormId.Text + "';";
                                oWriter.AddAttribute("onclick", sOnclick);
                                oWriter.RenderBeginTag("input");
                                oWriter.RenderEndTag();
                            }
                        }
                        oWriter.RenderEndTag();
                        oWriter.RenderEndTag();
                        oWriter.RenderEndTag();
                        Response.Write(sw.ToString());

                        try
                        {
                            Response.End();
                        }
                        catch (Exception e)
                        { }
                    }
                }
            }//tmalhotra2 end
        }

        /// <summary>
        /// Get values from server controls to create CWS request message
        /// </summary>
        /// <param name="oControls"></param>
        /// <param name="oMessageElement"></param>
        private void BindControlCollection2Data(ControlCollection oControls, XElement oMessageElement)
        {
            string sRMXRef = "";
            try
            {
                foreach (Control oCtrl in oControls)
                {
                    // npadhy MITS 16407, For top down layout the name of jurisdictional div is pvjurisgroup whereas for Tab layout 
                    //Parijat: Added it for Jurisdictional Tab: finds all controls under the div tag and binds that to data 
                    if (oCtrl.ID == "FORMTABpvjurisgroup" || oCtrl.ID == "Tabpvjurisgroup")
                    {
                        if (oCtrl.Controls.Count > 0)
                            BindControlCollection2Data(oCtrl.Controls, oMessageElement);
                    }
                    if (oCtrl is WebControl)
                    {
                         sRMXRef = ((WebControl)oCtrl).Attributes["RMXRef"];
                        if (!string.IsNullOrEmpty(sRMXRef))
                        {
                            //Binding functions should ignore such controls
                            if (((WebControl)oCtrl).Attributes["rmxignorevalue"] != null)
                                continue;

                            string sRMXIgnoreSet = ((WebControl)oCtrl).Attributes["rmxignoreset"];

                            if (!string.IsNullOrEmpty(sRMXIgnoreSet))
                            {
                                if (sRMXIgnoreSet == "true")
                                    continue;
                            }
                            //Ashish Ahuja Mits 33867 : Added trim function
                            string sValue = DatabindingHelper.GetControlValue((WebControl)oCtrl, m_RadioValues).Trim();
                            //The same value needs to be set to multiple node in the xml string
                            if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                            {
                                string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                                foreach (string sXPath in lstRMXRef)
                                {
                                    string sRefPath = GetRequestRefPath(sXPath);
                                    DatabindingHelper.SetValue2Message(oMessageElement, sRefPath, sValue);
                                }
                            }
                            else
                            {
                                sRMXRef = GetRequestRefPath(sRMXRef);

                                //MITS 20077 hlv 9/17/12 begin
                                if (oCtrl != null && oCtrl.ID != null && oCtrl.ID.ToLower().Equals("pieid")
                                    && sRMXRef.Equals(@"./Document/ParamList/Param[@name='SysPropertyStore']/Instance/Claim/PrimaryPiEmployee/PiEid/@codeid"))
                                {
                                    DatabindingHelper.IsFirst = true;
                                }
                                //MITS 20077 hlv 9/17/12 end

                                DatabindingHelper.SetValue2Message(oMessageElement, sRMXRef, sValue);

                                DatabindingHelper.IsFirst = false; //MITS 20077 hlv 9/17/12
                            }
                        }
                    }
                    else if (oCtrl is UserControlDataGrid)
                    {
                        // npadhy Start MITS 22730 When the Grid is not Visible on the Page we do not need to generate the XML from Grid.
                        // If we need to do have Grid with Visible = false, then we also need to provide the binding XML for it.
                        if (oCtrl.Visible)
                        {
                            XmlDocument objXml = ((UserControlDataGrid)oCtrl).GenerateXml();
                            XElement objElem = XElement.Parse(objXml.InnerXml);
                            string sPath = ((UserControlDataGrid)oCtrl).Target;
                            sPath = sPath.Substring(0, sPath.LastIndexOf("/"));
                            sPath = GetRequestRefPath(sPath);



                            DatabindingHelper.SetObject2Message(oMessageElement, sPath, objElem);

                            if (((UserControlDataGrid)oCtrl).RefImplementation == true)
                            {
                                // Get the Value of the Control from Selected Id
                                string sControlName = ((UserControlDataGrid)oCtrl).GridName.Substring(0, ((UserControlDataGrid)oCtrl).GridName.Length - 4);
                                TextBox txtSelectedId = (TextBox)this.FindControl(sControlName + "Selectedid");
                                string sSelectedValue = string.Empty;
                                if (txtSelectedId != null)
                                {
                                    sSelectedValue = txtSelectedId.Text;
                                }
                                else
                                {
                                    sSelectedValue = "";
                                }
                                 sRMXRef = ((UserControlDataGrid)oCtrl).Ref;
                                if (sRMXRef != null && sRMXRef != "")
                                {
                                    sRMXRef = GetRequestRefPath(sRMXRef);
                                    DatabindingHelper.SetValue2Message(oMessageElement, sRMXRef, sSelectedValue);
                                }
                            }
                        }
                        // npadhy End MITS 22730 When the Grid is not Visible on the Page we do not need to generate the XML from Grid.
                        // If we need to do have Grid with Visible = false, then we also need to provide the binding XML for it.
                    }
                    else
                    {
                        // akaushik5 Added for MITS 30290 Starts
                        if (oCtrl.ID != null)
                        {
                            if (oCtrl.ID.IndexOf("_RMXTable") != -1)
                            {
                                if (oCtrl.Visible == false)
                                    continue;
                            }
                        }
                        // akaushik5 Added for MITS 30290 Ends
                        if (oCtrl.Controls.Count > 0)
                            BindControlCollection2Data(oCtrl.Controls, oMessageElement);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Get values from server controls to create CWS request message
        /// </summary>
        /// <param name="oControls"></param>
        /// <param name="oMessageElement"></param>
        private void BindSingleControl2Data(Control oControl, XElement oMessageElement)
        {
            if (oControl == null)
                return;

            string sRMXRef = ((WebControl)oControl).Attributes["RMXRef"];
            if (!string.IsNullOrEmpty(sRMXRef))
            {
                string sValue = DatabindingHelper.GetControlValue((WebControl)oControl, m_RadioValues);
                //The same value needs to be set to multiple node in the xml string
                if (sRMXRef.IndexOf(g_sXPathDelimiter) > 0)
                {
                    string[] lstRMXRef = sRMXRef.Split(g_sXPathDelimiter.ToCharArray());
                    foreach (string sXPath in lstRMXRef)
                    {
                        string sRefPath = GetRequestRefPath(sXPath);
                        DatabindingHelper.SetValue2Message(oMessageElement, sRefPath, sValue);
                    }
                }
                else
                {
                    sRMXRef = GetRequestRefPath(sRMXRef);
                    DatabindingHelper.SetValue2Message(oMessageElement, sRMXRef, sValue);
                }
            }
        }

        //Moved the GetRMDate() Method to Apphelper by Deb

        //Moved the GetRMTime() Method to Apphelper by Deb

        /// <summary>
        /// Convert RMXRef value to XPath of CWS request message
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        private string GetRequestRefPath(string sPath)
        {
            string sRMXRef = string.Empty;

            //If the path start with Instance/UI/FormVariables, it's should be param[@name='SysFormVariables']
            if (sPath.StartsWith("/"))
                sPath = sPath.Substring(1);

            if (sPath.StartsWith("Instance/UI/"))
            {
                sRMXRef = sPath.Replace("Instance/UI/FormVariables/", "./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/");
            }
            else
            {
                sRMXRef = "./Document/ParamList/Param[@name='SysPropertyStore']/" + sPath;
            }

            return sRMXRef;
        }

        /// <summary>
        /// Convert RMXRef to XPath of CWS response message
        /// </summary>
        /// <param name="sPath"></param>
        /// <returns></returns>
        // akaushik5 Changed for MITS 30290 Starts
        //private string GetResponseRefPath(string sPath)
        protected string GetResponseRefPath(string sPath)
        // akaushik5 Changed for MITS 30290 Ends
        {
            string sRMXRef = string.Empty;

            //If the path start with Instance/UI/FormVariables, it's should be param[@name='SysFormVariables']
            if (sPath.StartsWith("/"))
                sPath = sPath.Substring(1);

            if (sPath.StartsWith("Instance/UI/"))
            {
                sRMXRef = sPath.Replace("Instance/UI/FormVariables/", "./ParamList/Param[@name='SysFormVariables']/FormVariables/");
            }
            else
            {
                sPath = sPath.Replace("Instance/", string.Empty);
                sRMXRef = "./ParamList/Param[@name='SysPropertyStore']/" + sPath;
            }

            return sRMXRef;
        }
        

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            // Amitosh Mits 23441 01/20/2011 Modified the template to add ReadonlyLookUp Fields(SysReadOnlyLookUpNodes)
            // Amitosh for LiabilityLoss  Modified the template to add SysKilledNodes Fields(SysKillFieldMarkNode)
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>57c850c4-30a2-48ba-a7f3-449bdbbfaa50</Authorization>
                <Call>
                    <Function>FormDataAdaptor.Navigate</Function>
                </Call>
                <Document>
                    <ParamList>
                        <Param name='SysFormVariables'>
	                        <FormVariables>
		                        <SysCmd/>
		                        <SysViewType/>
		                        <SysInvisible/>
		                        <SysFocusFields/>
		                        <SysCmdText/>
		                        <SysCmdConfirmSave/>
		                        <SysCmdQueue/>
		                        <SysFormName></SysFormName>
		                        <SysRequired/>
		                        <SysFormIdName/>
		                        <SysFormId/>
		                        <SysFormPId/>
		                        <SysFormPIdName/>
		                        <SysFormPForm/>
		                        <SysNotReqNew/>
		                        <SysSupplemental/>
		                        <SysSerializationConfig/>
		                        <SysClassName/>
		                        <SysFilter/>
		                        <SysEx/>
		                        <SysExData/>
		                        <SysSid/>
		                        <SysPSid/>
		                        <SysLookupClass/>
		                        <SysLookupRecordId/>
		                        <SysLookupAttachNodePath/>
		                        <SysLookupResultConfig/>
		                        <SysViewSection/>
		                        <SysView/>
		                        <SysSelectedTab/>
                                <SysKilledNodes/>
                                <SysDisplayNodes/>
                                <SysReadOnlyNodes/>
                                <SysReadWriteNodes/>
                                <SysReadOnlyFieldMarkNode/>
                                <SysKillFieldMarkNode/>                               
                                <SysSkipBindToControl/>
                                <UpdateSysRequired/>
                                <RequestHost/>
	                        </FormVariables>
                        </Param>
                        <Param name='SysScreenAction'>
	                        <SysScreenAction/>
                        </Param>
                        <Param name='SysFormId'/>
                        <Param name='SysPropertyStore'>
	                        <Instance/>
                        </Param>
                        <Param name='ScreenFlowStack'>
	                        <ScreenFlowStack>
		                        <ScreenFlow>
			                        <SysFormId/>
			                        <SysFormName></SysFormName>
		                        </ScreenFlow>
	                        </ScreenFlowStack>
                        </Param>
                        <Param name='SysViewId'/>
                    </ParamList>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
        /// <summary>
        /// Remove all elements from xml whose bindings were missing
        /// </summary>
        /// <param name="oMessageElement"></param>
        private void RemoveMissingRefs(XElement oMessageElement)
        {
            if (ViewState["sMissingRefs"] != null && ViewState["sMissingRefs"].ToString() != String.Empty)
            {
                sMissingRefs = (ArrayList) ViewState["sMissingRefs"];
                foreach (string sMissingElement in sMissingRefs)
                {
                    XElement oMissingEle = oMessageElement.XPathSelectElement("./Document/" + sMissingElement.Replace("." , ""));
                    if (oMissingEle != null)
                    {
                        oMissingEle.Remove();
                    }
                }
                //ViewState.Remove("sMissingRefs");//abansal23 on 04/29/2009 : ViewState is now removed after BindData2ErrorControl
            }
            else
                return;
        }


        /// <summary>
        /// This function handles all customizition done in legacy OnUpdateForm of FDM classes
        /// </summary>
        /// <param name="oMessageElement"></param>
        protected virtual void OnUpdateForm(XElement oMessageElement)
        {
            //Raman Bhatia: Remove All the killed nodes from the page
            XElement oSysKilledNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysKilledNodes");
            DatabindingHelper.HideNodes(oSysKilledNodes, this);

            //Raman Bhatia: Update All the display nodes in the page
            XElement oSysDisplayNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysDisplayNodes");
            DatabindingHelper.UpdateDisplayNodes(oSysDisplayNodes, this);

            //Raman Bhatia: Update all nodes meant to be read only in the page
            //XElement oSysReadOnlyNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysReadOnlyNodes");
            //DatabindingHelper.UpdateReadOnlyControls(oSysReadOnlyNodes, this);

            //deb :MITS 20003
            XElement oSysReadOnlyNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysReadOnlyNodes");
            switch (oSysReadOnlyNodes.Value)
            {
                case "readOnlyPage":
                    m_sFormReadOnly = "";
                    m_smsSecurity = true;
                    m_smsSecurityForSupp = true;
                    break;
                case "readOnlyPageExceptSupp":
                    m_sFormReadOnly = "";
                    m_smsSecurity = true;
                    m_smsSecurityForSupp = false;
                    break;
                case "readOnlyPageForSupp":
                    m_sFormReadOnly = "";
                    m_smsSecurity = false;
                    m_smsSecurityForSupp = true;
                    break;
                default:
                    if (!string.IsNullOrEmpty((oMessageElement.XPathSelectElement(@"//ParamList/Param[@name='SysFormId']").Value.Trim().ToString())))
                    {
                        if (int.Parse(oMessageElement.XPathSelectElement(@"//ParamList/Param[@name='SysFormId']").Value) > 0)
                        {
                            // npadhy - Jira RMA-6411 - We should not enable the controls by default. Putting m_sFormReadOnly = "Enable" will enable all the controls which are coming disabled from Business Layer
                            // The Value of this variable is handled in DatabindingHelper.cs
                            m_sFormReadOnly = "";
                            m_smsSecurity = false;
                            m_smsSecurityForSupp = false;
                            //Added by bhsarma33 for MITS 24775
						    //dbisht6 start working for mits 35331
							// changes had to made since the above three cases were not working in case of pipe seperated combination of individual contral and pagereadonly case 
                                if (oSysReadOnlyNodes.Value.Contains("readOnlyPageExceptSupp"))
                                {
                                    m_sFormReadOnly = "";
                                    m_smsSecurity = true;
                                    m_smsSecurityForSupp = false;
                                }
                                else if (oSysReadOnlyNodes.Value.Contains("readOnlyPageForSupp"))
                                {
                                    // m_sFormReadOnly = "";
                                    m_smsSecurity = false;
                                    m_smsSecurityForSupp = true;
                                }
								//dbisht6 end
                                else if (oSysReadOnlyNodes.Value.Contains("readOnlyPage"))
                                {
                                    m_sFormReadOnly = "";
                                    m_smsSecurity = true;
                                    m_smsSecurityForSupp = true;
                                }                                
                                

                                //End additions by bhsarma33 for MITS 24775

                                //break;
                            }
                        }
                        //Added by bhsarma33 for MITS 24775
                        if (oSysReadOnlyNodes.Value.Contains("readOnlyPageExceptSupp"))
                        {
                            m_sFormReadOnly = "";
                            m_smsSecurity = true;
                            m_smsSecurityForSupp = false;
                        }
                            //dbisht6 start working for mits 35331
							// changes had to made since the above three cases were not working in case of pipe seperated combination of individual contral and pagereadonly case 

                        else if (oSysReadOnlyNodes.Value.Contains("readOnlyPageForSupp"))
                        {
                            // m_sFormReadOnly = "";
                            m_smsSecurity = false;
                            m_smsSecurityForSupp = true;
                        }

                        else if (oSysReadOnlyNodes.Value.Contains("readOnlyPage"))
                        {
                            m_sFormReadOnly = "";
                            m_smsSecurity = true;
                            m_smsSecurityForSupp = true;
                        }
                           //dbisht6 end
                    //End additions by bhsarma33 for MITS 24775
                    DatabindingHelper.UpdateReadOnlyControls(oSysReadOnlyNodes, this);

                    break;
            }
            //deb

            //Raman Bhatia: Update all nodes meant to be read write in the page
            XElement oSysReadWriteNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysReadWriteNodes");
            DatabindingHelper.UpdateReadWriteControls(oSysReadWriteNodes, this);
            
            //Raman Bhatia: Modifies all nodes as per legacy OnUpdateForm and FormBase
            IEnumerable<XElement> oModifiedControlsList = oMessageElement.XPathSelectElements("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ModifiedControlsList/Control");
            DatabindingHelper.UpdateModifiedControls(oModifiedControlsList, this);

            // Naresh Incorporated the changes in ControlAppendAttribute List
            XElement oControlAppendAttributeList = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ControlAppendAttributeList");
            DatabindingHelper.UpdateControlAttributes(oControlAppendAttributeList, this);
          
            DatabindingHelper.UpdateTabFocus(this);

            XElement oUpdateSysRequiredNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/UpdateSysRequired");
            DatabindingHelper.UpdateSysRequired(oUpdateSysRequiredNodes, this);
        }

        /// <summary>
        /// This function will make those controls readonly whose ID startswith fieldmark
        /// But for LookUp controls it will only make the textbox readonly and willnot disable the link button.
        /// </summary>
        /// <author>Amitosh</author>
        /// <mits> 23441 </mits>
        /// <param name="oControl"></param>
        /// <param name="oMessageElement"></param>
        // akaushik5 Changed for MITS 30290 Starts
        //private void updateControls(Control oControl, XElement oSysReadOnlyLookUpNodes)
        protected void updateControls(Control oControl, XElement oSysReadOnlyLookUpNodes)
        // akaushik5 Changed for MITS 30290 Ends
        {
            string sControlId = string.Empty;
            string sRMXType = string.Empty;
           // XElement oSysReadOnlyLookUpNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysReadOnlyFieldMarkNode");
             string sSysReadFieldMark = string.Empty;
             sSysReadFieldMark = oSysReadOnlyLookUpNodes.Value;
             if (!string.IsNullOrEmpty(sSysReadFieldMark))
             {
                 string[] arSysReadFieldMark = sSysReadFieldMark.Split('|');
            
                sControlId = oControl.ClientID;

            foreach (string sFieldMark in arSysReadFieldMark)
            {
                try
                {
                    if (oControl.GetType().ToString() == "System.Web.UI.WebControls.HiddenField")
                    {
                        continue;
                    }
                    else
                    {
                        if (sControlId == null)
                        {
                            return;
                        }

                        if (sControlId.StartsWith(sFieldMark))
                        {
                            if (oControl is WebControl)
                            {
                                sRMXType = ((WebControl)oControl).Attributes["RMXType"];
                                if (sRMXType != null && sRMXType.EndsWith("lookup"))
                                {
                                    DatabindingHelper.DisableControl(oControl);
                                    return;
                                }
                                else if (oControl != null)
                                {
                                    DatabindingHelper.DisableControl(oControl);
                                }
                            }
                            else
                            {
                                DatabindingHelper.DisableControl(oControl);
                                                            }
                        }

                        else
                        {
                            continue;
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
                    }
             }
             else
             {
                 return;
             }       
            
        }

        /// <summary>
        /// Handling SysNotReqNew fields (these fields are not required for a new record)
        /// Change the label caption for these fields
        /// </summary>
        private void UpdateSysNotReqNewFields()
        {
            //Reset all the required field label to original css class in case they are changed previously
            //It will handle the following case: step 1. Bring up a new record page (these fields are
            //NOT required), step 2. Save a record (these fields need to be required again)
            //step 3. Add a new record (these fields are not required)
            TextBox oSysRequiredFields = (TextBox)this.Form.FindControl("SysRequired");
            Label oLabel = null;
            if (oSysRequiredFields != null)
            {
                string sSysRequiredFields = oSysRequiredFields.Text;
                if (string.IsNullOrEmpty(sSysRequiredFields))
                    return;

                string[] arrSysRequiredFields = sSysRequiredFields.Split('|');

                foreach (string sFieldName in arrSysRequiredFields)
                {
                    oLabel = (Label)this.Form.FindControl("lbl_" + sFieldName);
                    if (oLabel != null)
                    {
                        oLabel.Attributes["class"] = "required";
                    }
                }
            }

            //Check if it's a new record or not, only need to handle it for new record
            TextBox txtSysFormIdName = (TextBox)this.Form.FindControl("SysFormIdName");
            if (txtSysFormIdName == null)
                return;

            string sSysFormIdName = txtSysFormIdName.Text;
            if( string.IsNullOrEmpty(sSysFormIdName) )
                return;

            TextBox txtSysFormId = (TextBox)this.Form.FindControl(sSysFormIdName);
            string sSysFormId = txtSysFormId.Text;
            if (string.IsNullOrEmpty(sSysFormId) || sSysFormId == "0" || sSysFormId == "-1")
            {
                TextBox oSysNotReqNew = (TextBox)this.Form.FindControl("SysNotReqNew");
                if (oSysNotReqNew != null)
                {
                    string sSysNotReqNew = oSysNotReqNew.Text;
                    if (string.IsNullOrEmpty(sSysNotReqNew))
                        return;

                    string[] arSysNotReqNew = sSysNotReqNew.Split('|');
                    foreach (string sField in arSysNotReqNew)
                    {
                        //Change the style for the lable field
                        oLabel = (Label)this.Form.FindControl("lbl_" + sField);
                        if (oLabel != null)
                        {
                            oLabel.Attributes["class"] = "label";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Function to maintain the screen flow stack.. added by Raman Bhatia
        /// TO DO: COMPLETE THIS FUNCTIONALITY
        /// </summary>
        private void UpdateScreenFlowStack()
        {
            //Button btnBottomBack = null;
            //Button btnBack = (Button)this.Form.FindControl("BackToParent");
            //if (btnBack == null)
            //{
            //    btnBack = (Button)this.Form.FindControl("Back");
            //}
            //Mits 28937 : adding conditional check for Image button used in the tool bar 
            Button btnBottomBack = null;
            ImageButton imgBtnBack = null;
            Button btnBack = null;
            if (this.Form.FindControl("BackToParent") != null)
            {
                if (this.Form.FindControl("BackToParent").ToString().Contains("ImageButton"))
                {
                    imgBtnBack = (ImageButton)this.Form.FindControl("BackToParent");
                    if (imgBtnBack == null)
                    {
                        imgBtnBack = (ImageButton)this.Form.FindControl("Back");
                    }

                }
                else
                {
                    btnBack = (Button)this.Form.FindControl("BackToParent");
                    if (btnBack == null)
                    {
                        btnBack = (Button)this.Form.FindControl("Back");
                    }
                }
            }
            else if (this.Form.FindControl("Back") != null)
            {
                btnBack = (Button)this.Form.FindControl("Back");
            }
            //End of Mits 28937 
            TextBox txtScreenFlowStack = (TextBox)this.Form.FindControl("txtScreenFlowStack");
            string sOldScreenFlowStack = Request.Form["txtScreenFlowStack"];
            //TO DO..test thoroughly
            //Catering to cases where stack values flow through querystring
            if (String.IsNullOrEmpty(sOldScreenFlowStack))
            {
                sOldScreenFlowStack = AppHelper.GetQueryStringValue("txtScreenFlowStack");
            }
            if (txtScreenFlowStack != null && !String.IsNullOrEmpty(sOldScreenFlowStack))
            {
                txtScreenFlowStack.Text = sOldScreenFlowStack;

                string[] sRecords = sOldScreenFlowStack.Split('*');
                foreach (string item in sRecords.Reverse())
                {
                    string[] sItem = item.Split('@'); //Yatharth: MITS 19157: Seperator Changed from '|' to '@'
                    if (!String.IsNullOrEmpty(sItem[0]))
                    {
                        string[] sCurrentItem = new string[3] { sItem[0], sItem[1],sItem[2] };//Modified by Shivendu for MITS 11893
                        sScreenFlowStack.Push(sCurrentItem);
                    }
                }
            }
            
            TextBox txtSysFormName = (TextBox)this.Form.FindControl("SysFormName");
            TextBox txtSysFormIdName = (TextBox)this.Form.FindControl("SysFormIdName");
            TextBox txtSysFormPIdName = (TextBox)this.Form.FindControl("SysFormPIdName");
            //Start by Shivendu for MITS 11893
            TextBox txtSysFormTabName = (TextBox)this.Form.FindControl("hTabName");
            string sSysFormTabName = "";
            if (txtSysFormTabName != null)
                sSysFormTabName = txtSysFormTabName.Text;
            if (txtScreenFlowStack!= null && txtScreenFlowStack.Text != "" && sSysFormTabName == "")
            {
               
                string[] sScreensArray = txtScreenFlowStack.Text.Split('*');
                string sFormName = "";
                for (int i = 0; i < sScreensArray.Length; i++)
                {
                    if (sScreensArray[i] != null && sScreensArray[i] != "")
                    {
                        string[] sScreensElementsArray = sScreensArray[i].Split('@'); //Yatharth: MITS 19157: Seperator Changed from '|' to '@'
                        for (int j = 0; j < sScreensElementsArray.Length; j++)
                        { 
                                   if(j==0)
                                     {
                                     sFormName = sScreensElementsArray[0].ToLower().Trim();
                                     }
                                   if (txtSysFormName != null && sFormName == txtSysFormName.Text && j == 2)
                                     {
                                         sSysFormTabName = sScreensElementsArray[2].Trim();
                                     }
                         }
                    }
                }
            }
            if (txtSysFormTabName != null)
              txtSysFormTabName.Text =  sSysFormTabName  ;
            //End by Shivendu for MITS 11893
            if (txtSysFormName != null && (txtSysFormIdName != null || txtSysFormPIdName != null))
            {
                TextBox txtSysFormId = null;
                if (txtSysFormName.Text.ToLower().EndsWith("list") && !String.IsNullOrEmpty(txtSysFormPIdName.Text))
                {
                    txtSysFormId = (TextBox)this.Form.FindControl(txtSysFormPIdName.Text);
                }
                else
                {
                    txtSysFormId = (TextBox)this.Form.FindControl(txtSysFormIdName.Text);
                }
                if (txtSysFormId != null)
                {
                    string[] sCurrentForm = new string[3] { txtSysFormName.Text, txtSysFormId.Text, sSysFormTabName };//Modified by Shivendu for MITS 11893

                    if (sScreenFlowStack.Count > 0)
                    {
                        string[] sOldRecord = sScreenFlowStack.Peek();
                        if (txtSysFormName.Text.ToLower() == sOldRecord[0].ToLower().ToString())
                        {
                            sScreenFlowStack.Pop();
                            if (sScreenFlowStack.Count > 0)
                            {
                                sOldRecord = sScreenFlowStack.Peek();
                            }
                        }
                        
                        if (btnBack != null && sScreenFlowStack.Count > 0)
                        {
                            btnBack.PostBackUrl = sOldRecord[0].ToString() + ".aspx?SysCmd=0&recordID=" + sOldRecord[1].ToString();
                        }
                        //Mits 28937 : adding conditional check for Image button used in the tool bar 
                        else if (imgBtnBack != null && sScreenFlowStack.Count > 0)
                        {
                            imgBtnBack.PostBackUrl = sOldRecord[0].ToString() + ".aspx?SysCmd=0&recordID=" + sOldRecord[1].ToString();
                        }
                        //MITS 17688 :Raman Bhatia: checking for Bottom Back button

                        btnBottomBack = (Button)this.Form.FindControl("BackToParent_Bottom");
                        if (btnBottomBack != null && sScreenFlowStack.Count > 0)
                        {
                            btnBottomBack.PostBackUrl = sOldRecord[0].ToString() + ".aspx?SysCmd=0&recordID=" + sOldRecord[1].ToString();
                        }

                    }
                    //We need to verify that current entry does not exist previously..
                    if (sScreenFlowStack.Count >= 2)
                    {
                        string[] sFirstPrevRecord = sScreenFlowStack.Pop();
                        string[] sSecondPrevRecord = sScreenFlowStack.Pop();

                        if (sCurrentForm[0].ToLower().ToString() != sSecondPrevRecord[0].ToLower().ToString() || sCurrentForm[1].ToString() != sSecondPrevRecord[1].ToString())
                        {
                            sScreenFlowStack.Push(sSecondPrevRecord);
                            sScreenFlowStack.Push(sFirstPrevRecord);
                        }
                        else
                        {
                            string[] sOldRecord = null;
                            if (btnBack != null && sScreenFlowStack.Count > 0)
                            {
                                sOldRecord = sScreenFlowStack.Peek();
                                btnBack.PostBackUrl = sOldRecord[0].ToString() + ".aspx?SysCmd=0&recordID=" + sOldRecord[1].ToString();
                            }
                            //Mits 28937 : adding conditional check for Image button used in the tool bar 
                            else if (imgBtnBack != null && sScreenFlowStack.Count > 0)
                            {
                                sOldRecord = sScreenFlowStack.Peek();
                                imgBtnBack.PostBackUrl = sOldRecord[0].ToString() + ".aspx?SysCmd=0&recordID=" + sOldRecord[1].ToString();
                            }
                            //end of Mits 28937
                            if (btnBottomBack != null && sScreenFlowStack.Count > 0)
                            {
                                btnBottomBack.PostBackUrl = sOldRecord[0].ToString() + ".aspx?SysCmd=0&recordID=" + sOldRecord[1].ToString();
                            }
                        }
                    }

                    sScreenFlowStack.Push(sCurrentForm);
                    //Neha Added achange for piemployee screen . PiInjury and PiEmployee have same data model and adapter layer 
                    TextBox txtActualFormName = (TextBox)this.Form.FindControl("hdActualFormName");
                    if (txtActualFormName != null)
                    {

                        sCurrentForm = new string[3] { txtActualFormName.Text, txtSysFormId.Text, sSysFormTabName };
                        sScreenFlowStack.Push(sCurrentForm);

                    }
                    //Neha End
                }
                
                
                //display on screen
                String[][] sDisplayData = new string[10][] ;
                sScreenFlowStack.CopyTo(sDisplayData, 0);

                if (txtScreenFlowStack != null)
                {
                    txtScreenFlowStack.Text = "";
                    for (int i = 0; i < sDisplayData.Length; i++)
                    {
                        if (sDisplayData[i] == null)
                        {
                            break;
                        }
                        //Modified by Shivendu for MITS 11893
                        txtScreenFlowStack.Text = txtScreenFlowStack.Text + sDisplayData[i][0] + '@' + sDisplayData[i][1] + '@' + sDisplayData[i][2] + "*"; //Yatharth: MITS 19157: Seperator Changed from '|' to '@'
                        
                    }
                    // Response.Write(txtScreenFlowStack.Text);
                }
            }
        }

        /// <summary>
        /// Function to reset SysCmd
        /// </summary>
        private void ResetSysCmd()
        {
            Control oControl = this.Form.FindControl("SysCmd");
            if ((oControl != null) && (oControl is WebControl))
            {
                if(((TextBox)oControl).Text != "7")
                    ((TextBox)oControl).Text = string.Empty;
            }
        }
        /// <summary>
        /// This function will set rmxignorevalue attribute to a control and all controls in it.
        /// Binding functions would ignore such controls
        /// </summary>
        /// <param name="oControl"></param>
        protected void SetIgnoreValueAttribute(Control oControl)
        {
            // Add the Attribute in the Control Itself
            if (oControl is WebControl)
            {
                ((WebControl)oControl).Attributes.Add("rmxignorevalue", "true");
            }
            // Add attribute to all the Children
            foreach (Control oCtrl in oControl.Controls)
            {
                if (oCtrl is WebControl)
                {
                    ((WebControl)oCtrl).Attributes.Add("rmxignorevalue", "true");
                }
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        SetIgnoreValueAttribute(oCtrl);
                }
            }
        }
        /// <summary>
        /// This function will remove rmxignorevalue attribute in a control and all controls in it.
        /// Binding functions would no longer ignore such controls
        /// </summary>
        /// <param name="oControl"></param>
        protected void RemoveIgnoreValueAttribute(Control oControl)
        {
            // Remove the Attribute from the Control Itself
            if (oControl is WebControl)
            {
                ((WebControl)oControl).Attributes.Remove("rmxignorevalue");
            }
            // Remove attribute from all the Children
            foreach (Control oCtrl in oControl.Controls)
            {
                if (oCtrl is WebControl)
                {
                    if (((WebControl)oCtrl).Attributes["rmxignorevalue"] != null)
                    {
                        ((WebControl)oCtrl).Attributes.Remove("rmxignorevalue");
                    }

                }
                else
                {
                    if (oCtrl.Controls.Count > 0)
                        RemoveIgnoreValueAttribute(oCtrl);
                }
            }
        }
        /// <summary>
        /// MITS 19739 : Raman Bhatia
        /// 09/02/2010 : This function would return a list of all textarea controls on form
        /// MITS 20999 : Raman Bhatia
        /// 06/09/2010 : Modified to also return listbox controls on form
        /// </summary>
        /// <param name="oControls"></param>
        /// <param name="oParentCtrlList"></param>
        private void AddTextAreaAndListBoxControlsToList(List<Control> oControls , ControlCollection oParentCtrlList)
        {
            foreach (Control oCtrl in oParentCtrlList)
            {
                if (oCtrl is WebControl)
                {
                    Type controlType = oCtrl.GetType();
                    string sType = controlType.ToString();
                    int index = sType.LastIndexOf(".");
                    sType = sType.Substring(index + 1);
                    string sItemSetRef;
                    string sRMXType;
                    string sValueCollection;
                    if (sType.ToLower() == "textbox")
                    {
                        TextBox oTxtCtrl = (TextBox)oCtrl;
                        if (oTxtCtrl.TextMode == TextBoxMode.MultiLine)
                        {
                            oControls.Add(oCtrl);
                        }
                    }
                    // MITS 19739 : Raman Bhatia
                    // All ListBox too need to be binded again in case of error
                    if (sType.ToLower() == "listbox")
                    {
                        oControls.Add(oCtrl);
                    }
                }
                else if(oCtrl.Controls.Count > 0)
                {
                    AddTextAreaAndListBoxControlsToList(oControls, oCtrl.Controls);
                }
            }
        }

        //rjhamb 16453:To disable controls which have been explicitly marked readonly after the entire form has been enabled.
        private void UpdateReadOnlyFields(XElement oMessageElement)
        {
            string sSysReadOnlyFields = string.Empty;
           
            //Fields marked as type='readonly' in the view xml
            UpdateSysReadOnlyFields();
          
            //Fields marked as readonly through base.AddReadOnlyNode
            XElement oSysReadOnlyNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysReadOnlyNodes"); 
            DatabindingHelper.UpdateReadOnlyControls(oSysReadOnlyNodes, this);

            //Fields marked as readonly in their control's attribute
            XElement oControlAppendAttributeList = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ControlAppendAttributeList");
            if(oControlAppendAttributeList!=null)
                UpdateReadOnlyControlAttributeFields(oControlAppendAttributeList);

            //Fields marked as readonly in ModifiedControl List
            IEnumerable<XElement> oModifiedControlsList = oMessageElement.XPathSelectElements("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/ModifiedControlsList/Control");
            if (oModifiedControlsList != null)
                UpdateReadOnlyModifiedControlsFields(oModifiedControlsList);
  
        }
        private void UpdateSysReadOnlyFields()
        {
            string sSysReadOnlyFields = string.Empty;
            try
            {
                TextBox oSysReadOnlyFields = (TextBox)this.FindControl("SysReadOnlyFields");
                if (oSysReadOnlyFields != null)
                    sSysReadOnlyFields = oSysReadOnlyFields.Text;
                if (!string.IsNullOrEmpty(sSysReadOnlyFields))
                {
                    string[] arSysReadOnlyFields = sSysReadOnlyFields.Split('|');
                    foreach (string sControlName in arSysReadOnlyFields)
                    {
                        if (!string.IsNullOrEmpty(sControlName))
                        {
                            Control oControl = this.FindControl(sControlName);
                            if (oControl != null)
                            {
                                DatabindingHelper.DisableControl(oControl);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        private void UpdateReadOnlyControlAttributeFields(XElement oControlAppendAttributeList) 
        {
            bool bReadOnlyAttribute = false;
            try
            {
                foreach (XElement objNode in oControlAppendAttributeList.Nodes())
                {
                    foreach (XElement objAttListNode in objNode.Nodes())
                    {
                        if (objAttListNode != null)
                        {
                            switch (objAttListNode.Name.LocalName.ToLower())
                            {
                                case "readonlytext":
                                case "disabled":
                                    if (string.Compare(objAttListNode.Attribute("value").Value, "true", true) == 0)
                                    {
                                        Control ctrlTemp = (Control)this.FindControl(objNode.Name.LocalName);
                                        if (ctrlTemp != null)
                                        {
                                            DatabindingHelper.DisableControl(ctrlTemp);
                                        }
                                    }
                                    bReadOnlyAttribute = true;
                                    break;
                            }
                            if (bReadOnlyAttribute)
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }
        private void UpdateReadOnlyModifiedControlsFields(IEnumerable<XElement> oModifiedControlsList)
        {
            string[] sModifiedControls = new String[50];
            string sControlType = "";
            string sControlID = "";
            Control ctrlTemp = null;
            try
            {
                if (oModifiedControlsList != null)
                {
                    foreach (XElement oEle in oModifiedControlsList)
                    {
                        sControlType = oEle.Attribute("ControlType").Value;
                        sControlID = oEle.Attribute("id").Value;

                        switch (sControlType)
                        {
                            case "TextBox":
                                ctrlTemp = this.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("ReadOnly");
                                    if (oAttr != null && oAttr.Value.ToLower() == "true")
                                    {
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                        {
                                            ((TextBox)ctrlTemp).ReadOnly = Convert.ToBoolean(oAttr.Value);
                                        }
                                    }
                                }
                                break;
                            case "Date":
                                ctrlTemp = this.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("readonly");
                                    if (oAttr != null && oAttr.Value.ToLower() == "true")
                                    {
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                        {
                                            ((TextBox)ctrlTemp).ReadOnly = true;
                                            ((TextBox)ctrlTemp).Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                                        }
                                        ctrlTemp = this.FindControl(sControlID + "btn");
                                        if (ctrlTemp != null)
                                        {
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Button"))
                                            {
                                                ((Button)ctrlTemp).Enabled = false;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "Checkbox":
                                ctrlTemp = this.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("readonly");
                                    if (oAttr != null && oAttr.Value.ToLower() == "true")
                                    {
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))
                                        {
                                            ((CheckBox)ctrlTemp).Enabled = false;
                                        }
                                    }
                                }
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        //rjhamb 16453:To disable controls which have been explicitly marked readonly after the entire form has been enabled.
        
        /// <summary>
        /// MGaba2:12/08/2010:MITS 20184:All Employee information was not getting saved in powerview 
        /// It will extract node whose XPath is specified by p_sPath from XmlOut and save in Viewstate  
        /// </summary>
        /// <param name="p_sFormName">Form Name like claimwc</param>
        /// <param name="p_sPath">Xpath of nodes separated with pipe symbol</param>
        /// <param name="p_xleXmlOut">XmlOut</param>
        protected void ExtractFromXmlOut(string p_sFormName, string p_sPath, XElement p_xleXmlOut)
        {
            string sPath = string.Empty;
            string sViewStateKey = string.Empty;
            XElement objNode = null;
            string[] sXpaths = p_sPath.Split(new char[] { '|' });

            foreach (string sXpath in sXpaths)
            {
                sPath = "//ParamList/Param[@name='SysPropertyStore']/" + sXpath;

                objNode = p_xleXmlOut.XPathSelectElement(sPath);
                if (objNode != null)
                {
                    //Everytime we have to replace it
                    if (sXpath.LastIndexOf("/") != -1)
                        sViewStateKey = sXpath.Substring(sXpath.LastIndexOf("/") + 1, sXpath.Length - (sXpath.LastIndexOf("/") + 1));
                    else
                        sViewStateKey = sXpath;

                    ViewState[p_sFormName + "_" + sViewStateKey] = objNode.ToString();
                }
            }
        }

        /// <summary>
        /// MGaba2:12/08/2010:MITS 20184:All Employee information was not getting saved in powerview 
        /// in case field doesnt exist in powerview of claim WC
        /// This function will fetch value of nodes keyed by form name and p_sPath like claimwc_employee
        /// and append in syspropertystore node of XMLIn(p_xleXmlIn)
        /// </summary>
        /// <param name="p_sFormName">form name like claimwc</param>
        /// <param name="p_sPath">xpaths of nodes separated with | symbol</param>
        /// <param name="p_xleXmlIn">xmlIn</param>
        protected void AppendToXmlIn(string p_sFormName, string p_sPath, XElement p_xleXmlIn)
        {
            string sViewStateKey = string.Empty;
            string sViewStateValue = string.Empty;
            string sRootPath = "./Document/ParamList/Param[@name='SysPropertyStore']/Instance/";
            XElement objXEFromViewState = null;
            string sPath = string.Empty;

            string[] sXpaths = p_sPath.Split(new char[] { '|' });

            foreach (string sXpath in sXpaths)
            {
                if (sXpath.LastIndexOf("/") != -1)
                    sViewStateKey = sXpath.Substring(sXpath.LastIndexOf("/") + 1, sXpath.Length - (sXpath.LastIndexOf("/") + 1));
                else
                    sViewStateKey = sXpath;

                if (ViewState[p_sFormName + "_" + sViewStateKey] != null)
                {
                    sViewStateValue = ViewState[p_sFormName + "_" + sViewStateKey].ToString();

                    if (sXpath.LastIndexOf("/") != -1)
                        sPath = sRootPath + sXpath.Substring(0, sXpath.LastIndexOf("/"));
                    else
                        sPath = sRootPath + sXpath;

                    objXEFromViewState = XElement.Parse(sViewStateValue);
                    InsertNodeIfNotExists(p_xleXmlIn, sPath, objXEFromViewState);
                }
            }
        }

        /// <summary>
        /// MGaba2:12/08/2010:MITS 20184:All Employee information was not getting saved in powerview 
        /// This Recursive function will check whether node and all its children are present in xmlIn or not
        /// and inserts only if they are not already present
        /// </summary>
        /// <param name="p_objXmlIn">XMLIn</param>
        /// <param name="p_sRootXpath">XPath</param>
        /// <param name="p_objXEFromViewState">Node from XMLOut saved in viewstate</param>
        private void InsertNodeIfNotExists(XElement p_objXmlIn, string p_sRootXpath, XElement p_objXEFromViewState)
        {
            string sXPath = p_sRootXpath + "/" + p_objXEFromViewState.Name;

            if (p_objXmlIn.XPathSelectElement(sXPath) == null)
            {
                XElement objTemp = p_objXmlIn.XPathSelectElement(p_sRootXpath);
                objTemp.Add(p_objXEFromViewState);
            }
            else if (p_objXEFromViewState.HasElements == false)
            {
                return;//Exit Criteria for Recursive Function
            }

            foreach (XElement objChildElement in p_objXEFromViewState.Elements())
            {
                InsertNodeIfNotExists(p_objXmlIn, sXPath, objChildElement);
            }
        }
        /// <summary>
        /// To Hide the controls having fieldmark as their prefix.
        /// Added for R8 enhancement ofLiabilityLoss
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="oControl"></param>
        /// <param name="oSysReadOnlyLookUpNodes"></param>
        // akaushik5 Changed for MITS 30290 Starts
        //private void HideFieldMarkNodes(Control oControl, XElement oSysReadOnlyLookUpNodes)
        protected void HideFieldMarkNodes(Control oControl, XElement oSysReadOnlyLookUpNodes)
        // akaushik5 Changed for MITS 30290 Ends
        {
            string sControlId = string.Empty;
            string sRMXType = string.Empty;
            // XElement oSysReadOnlyLookUpNodes = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysReadOnlyFieldMarkNode");
            string sSysReadFieldMark = string.Empty;
            sSysReadFieldMark = oSysReadOnlyLookUpNodes.Value;
            if (!string.IsNullOrEmpty(sSysReadFieldMark))
            {
                string[] arSysReadFieldMark = sSysReadFieldMark.Split('|');

                sControlId = oControl.ClientID;

                foreach (string sFieldMark in arSysReadFieldMark)
                {
                    try
                    {
                        if (oControl.GetType().ToString() == "System.Web.UI.WebControls.HiddenField")
                        {
                            continue;
                        }
                        else
                        {
                            if (sControlId == null)
                            {
                                return;
                            }

                            if (sControlId.StartsWith("div_"+sFieldMark))
                            {
                                oControl.Visible = false;
                                //if (oControl is WebControl)
                                //{
                                //    sRMXType = ((WebControl)oControl).Attributes["RMXType"];
                                //    if (sRMXType != null && sRMXType.EndsWith("lookup"))
                                //    {
                                //        DatabindingHelper.DisableControl(oControl);
                                //        return;
                                //    }
                                //    else if (oControl != null)
                                //    {
                                //        DatabindingHelper.DisableControl(oControl);
                                //    }
                                //}
                                //else
                                //{
                                //    DatabindingHelper.DisableControl(oControl);
                                //}
                            }

                            else
                            {
                                continue;
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message, ex);
                    }
                }
            }
            else
            {
                return;
            }

        }

        /// <summary>
        /// Removes en-dash and em-dash from the string supplied
        /// Added for mits 26371
        /// </summary>
        /// <param name="sStringToStandardize"></param>
        /// <returns></returns>
        public static string StandardizeHyphens(string sStringToStandardize)
        {
            StringBuilder sbStandardizedString = new StringBuilder(sStringToStandardize.Length);
            foreach (char c in sStringToStandardize)
            {
                switch (c)
                {
                    case '\u2013':
                    case '\u2014':
                        sbStandardizedString.Append('-');
                        break;
                    default:
                        sbStandardizedString.Append(c);
                        break;
                }
            }
            return sbStandardizedString.ToString();
        }
        //Add by kuladeep for rmA14.1 Performance Start
        /// <summary>
        ///  //TODO --Currently it is working for Split (Reserve/Transaction) quick lookup.
        /// //We have to add some condition also for make it generic in future.
        /// </summary>
        /// <param name="sCodetype"></param>
        /// <param name="sLookupstring"></param>
        /// <param name="sDescSearch"></param>
        /// <param name="sFilter"></param>
        /// <param name="sLOB"></param>
        /// <param name="sSessionlob"></param>
        /// <param name="sDeptEid"></param>
        /// <param name="sFormname"></param>
        /// <param name="sTriggerDate"></param>
        /// <param name="sEventdate"></param>
        /// <param name="sTitle"></param>
        /// <param name="sSessionClaimId"></param>
        /// <param name="sOrgLevel"></param>
        /// <param name="sInsuredEid"></param>
        /// <param name="sEventId"></param>
        /// <param name="sParentCodeid"></param>
        /// <param name="iCurrentPageNumber"></param>
        /// <param name="sPolicyId"></param>
        /// <param name="sCovTypeCodeId"></param>
        /// <param name="sTransId"></param>
        /// <param name="sClaimantEid"></param>
        /// <param name="sIsFirstFinal"></param>
        /// <param name="sPolUnitRowId"></param>
        /// <param name="sPolicyLOB"></param>
        /// <param name="sCovgSeqNum"></param>
        /// <param name="sRsvStatusParent"></param>
        /// <param name="sTransSeqNum"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static List<Riskmaster.Models.CodeType> GetQuickLookUpData(
           string sCodetype,
           string sLookupstring,
           string sDescSearch,
           string sFilter,
           string sLOB,
           string sJurisdiction,    //added by swati MITS # 36929
           string sSessionlob,
           string sDeptEid,
           string sFormname,
           string sTriggerDate,
           string sEventdate,
           string sTitle,
           string sSessionClaimId,
           string sOrgLevel,
           string sInsuredEid,
           string sEventId,
           string sParentCodeid,
           int iCurrentPageNumber,
           string sPolicyId,
           string sCovTypeCodeId,
           string sLossTypeCodeId,           //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
           string sTransId,
           string sClaimantEid,
           string sIsFirstFinal,
           string sPolUnitRowId,
           string sPolicyLOB,
           string sCovgSeqNum,
           string sRsvStatusParent,
           string sTransSeqNum,string sFieldName, string sCoverageKey,string sPayCol)
        {
            List<Riskmaster.Models.CodeType> codes = null;
            CodeListType objList = null;
            CodesBusinessHelper cb = new CodesBusinessHelper();
            //sCodetype = "code." + sCodetype;
            //sJurisdiction added by swati MITS # 36929
            //objList = cb.QuickLookup(sCodetype, sLookupstring, sDescSearch, sFilter, sLOB, sSessionlob, sDeptEid, sFormname, sTriggerDate, sEventdate, sTitle,
            objList = cb.QuickLookup(sCodetype, sLookupstring, sDescSearch, sFilter, sLOB, sJurisdiction, sSessionlob, sDeptEid, sFormname, sTriggerDate, sEventdate, sTitle,
                                        sSessionClaimId, sOrgLevel, sInsuredEid, sEventId, sParentCodeid, iCurrentPageNumber, sPolicyId, sCovTypeCodeId, sTransId,
                                        sClaimantEid, sIsFirstFinal, sPolUnitRowId, sPolicyLOB, sCovgSeqNum, sRsvStatusParent, sTransSeqNum, sFieldName, sCoverageKey, sLossTypeCodeId,sPayCol);
            codes = objList.Codes.ToList();
            System.Web.HttpContext.Current.Response.AddHeader("PageCount", objList.PageCount);
            return codes;

        }

        //Add by kuladeep for rmA14.1 Performance End

        [System.Web.Services.WebMethod]

        public static string GetDataForSelectedEntity(int p_iEntityId, string p_sType, int p_iLookUpType, string p_sFormName)
        {
            XElement oSessionCmdElement = null;
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlIn = null;
            XElement oMessageElement = null;
            //Call WCF wrapper for cws
            //CommonWCFService.CommonWCFServiceClient oWCF = new CommonWCFService.CommonWCFServiceClient();
            //string sReturn = oWCF.ProcessRequest(oMessageElement.ToString());
            
                    oMessageElement = GetMessageTemplateforLookup();

                    if (p_iEntityId != 0)
                    {
                       // sEntityId = Request.QueryString["entityid"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/EntityTableId");
                        oSessionCmdElement.Value = p_iEntityId.ToString(); 
                    }
                    if (p_sType != string.Empty)
                    {
                        //sType = Request.QueryString["type"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/Type");
                        oSessionCmdElement.Value = p_sType;
                    }
                    if (p_iLookUpType != 0)
                    {
                      //  sLookupType = Request.QueryString["lookuptype"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/LookUpType");
                        oSessionCmdElement.Value = p_iLookUpType.ToString();
                    }
                    if (p_sFormName != string.Empty)
                    {
                        //sFormName = Request.QueryString["formname"];
                        oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/GetEntityData/FormName");
                        oSessionCmdElement.Value = p_sFormName;
                    }

                

                if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("SessionId")))
                {
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Authorization");
                    oSessionCmdElement.Value = AppHelper.ReadCookieValue("SessionId");
                }
            //string sReturn = AppHelper.CallWCFService(oMessageElement.ToString());
            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            //XmlDocument oFDMPageDom = new XmlDocument();
            //oFDMPageDom.LoadXml(sReturn);
            //XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");

            //objXml.LoadXml(oInstanceNode.InnerXml);
            return sReturn;
        }

        private static XElement GetMessageTemplateforLookup()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>SearchAdaptor.GetSearchResultsForSelectedEntity</Function> 
                    </Call>
                    <Document>
                        <GetEntityData>
                            <EntityTableId></EntityTableId> 
                            <Type></Type> 
                            <LookUpType></LookUpType> 
                            <FormName></FormName> 
                        </GetEntityData>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }
    }
}
