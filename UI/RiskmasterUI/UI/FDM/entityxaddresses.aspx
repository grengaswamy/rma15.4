<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entityxaddresses.aspx.cs"  Inherits="Riskmaster.UI.FDM.Entityxaddresses" ValidateRequest="false" EnableViewState="true" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Addresses</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <script language="JavaScript" src="../../Scripts/supportscreens.js">{var i;}</script>
    <script language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Addresses" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSentityxaddresses" id="TABSentityxaddresses">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="entityxaddresses" id="LINKTABSentityxaddresses">Contact Addresses</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPentityxaddresses">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSphonenumbers" id="TABSphonenumbers">
          <a class="NotSelected1" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="phonenumbers" id="LINKTABSphonenumbers">Phone Numbers</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPphonenumbers">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="gridpopupborder" runat="server" name="FORMTABentityxaddresses" id="FORMTABentityxaddresses">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdentityxaddresses" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="ContactId" RMXRef="//ContactId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="EntityId" RMXRef="//EntityId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="ContactId" />
              <div runat="server" class="full" id="div_Address1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Address1" Text="Address1" />
                <span class="formw">
                  <asp:TextBox runat="server" id="Address1" RMXRef="//Address1" RMXType="text" tabindex="1" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_Address2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Address2" Text="Address2" />
                <span class="formw">
                  <asp:TextBox runat="server" id="Address2" RMXRef="//Address2" RMXType="text" tabindex="2" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_City" xmlns="">
                <asp:label runat="server" class="label" id="lbl_City" Text="City" />
                <span class="formw">
                  <asp:TextBox runat="server" id="City" RMXRef="//City" RMXType="text" tabindex="3" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_State" xmlns="">
                <asp:label runat="server" class="label" id="lbl_State" Text="State" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="State" CodeTable="states" ControlName="State" RMXRef="//State" RMXType="code" tabindex="4" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_Country" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Country" Text="Country" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="Country" CodeTable="country" ControlName="Country" RMXRef="//Country" RMXType="code" tabindex="5" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_County" xmlns="">
                <asp:label runat="server" class="label" id="lbl_County" Text="County" />
                <span class="formw">
                  <asp:TextBox runat="server" id="County" RMXRef="//County" RMXType="text" tabindex="6" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_ZipCode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_ZipCode" Text="Zip/Postal Code" />
                <span class="formw">
                  <asp:TextBox runat="server" onblur="zipLostFocus(this);" onchange="setDataChanged(true);" id="ZipCode" RMXRef="//ZipCode" RMXType="zip" TabIndex="7" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_Email" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Email" Text="Email Address" />
                <span class="formw">
                  <asp:TextBox runat="server" id="Email" RMXRef="//Email" RMXType="text" tabindex="8" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_Fax" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Fax" Text="Fax" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="Fax" RMXRef="//Fax" RMXType="phone" tabindex="9" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="full" id="div_PrimaryAddress" xmlns="">
                <asp:label runat="server" class="label" id="lbl_PrimaryAddress" Text="Primary Address(Mail To)" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="PrimaryAddress" RMXRef="//PrimaryAddress" RMXType="checkbox" tabindex="10" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div class="gridpopupborder" runat="server" style="display:none;" name="FORMTABphonenumbers" id="FORMTABphonenumbers">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdphonenumbers" />
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="partial" id="div_AddressXPhoneInfoGrid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_AddressXPhoneInfoGrid" Text="" />
                <span>
                  <dg:UserControlDataGrid runat="server" ID="AddressXPhoneInfoGrid" GridName="AddressXPhoneInfoGrid" GridTitle="Phone Numbers" Target="/Instance/UI/FormVariables/SysExData/AddressXPhoneInfo" DynamicHideNodes="" Ref="" Unique_Id="PhoneId" ShowCloneButton="False" ShowRadioButton="True" ShowCheckBox="False" RefImplementation="False" OnClick="KeepRowForEdit('AddressXPhoneInfoGrid');;" Width="300px" Height="200px" hidenodes="|ContactId|PhoneId|" ShowHeader="True" ShowFooter="False" LinkColumn="" PopupWidth="500" PopupHeight="400" Type="GridAndButtons" IncludeLastRecord="False" />
                </span>
              </div>
              <asp:TextBox style="display:none" runat="server" id="AddressXPhoneInfoSelectedId" RMXRef="/Instance/UI/FormVariables/SysExData/AddressXPhoneInfoSelectedId" RMXType="id" rmxignoreget="true" />
              <asp:TextBox style="display:none" runat="server" id="AddressXPhoneInfoGrid_Action" RMXRef="" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="AddressXPhoneInfoGrid_RowAddedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/AddressXPhoneInfoGrid_RowAddedFlag" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="AddressXPhoneInfoGrid_RowDeletedFlag" RMXRef="/Instance/UI/FormVariables/SysExData/AddressXPhoneInfoGrid_RowDeletedFlag" RMXType="id" />
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" width="75px" onClientClick="return EntityXAddressesInfo_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="btnCancel" RMXRef="" Text="Cancel" width="75px" onClientClick="return EntityXAddressesInfo_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" name="formname" Text="entityxaddresses" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="AddressXPhoneInfoGrid|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>