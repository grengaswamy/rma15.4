﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common;
using System.Xml;

namespace Riskmaster.UI.FDM
{
    public partial class Autoclaimchecks : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Control cPostBackControl = null;
            //Amitosh for Combined Payment
            string[] array = null;
            DropDownList bankaccount = (DropDownList)Page.FindControl("bankaccount");
            //skhare7 Combined Payment
            TextBox CombPayeeAccountId = (TextBox)Page.FindControl("CombPayeeAccountId");
            TextBox MailToPayee = (TextBox)Page.FindControl("MailToPayee");//AA 7199            
            TextBox PmtcurrencyType = (TextBox)Page.FindControl("currencytype");
            ListBox entitylist = (ListBox)Page.FindControl("entitylist");
            string[] arrCurrtype = null;
            TextBox CombPayeeCurrtype = (TextBox)Page.FindControl("CombPayeeCurrtype");
            //RMA-8490 START
            if (!Page.IsPostBack)
            {
                TextBox ClaimantListClaimantEID = (TextBox)Page.FindControl("ClaimantListClaimantEID");
                if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("ClaimantListClaimantEID")))
                    ClaimantListClaimantEID.Text = AppHelper.GetQueryStringValue("ClaimantListClaimantEID");
                TextBox FromFinancial = (TextBox)Page.FindControl("FromFinancial");
                if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("FromFinancial")))
                    FromFinancial.Text = AppHelper.GetQueryStringValue("FromFinancial");
            }
            //RMA-8490 END
            //pgupta93: RA-4608 START                     
            if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormName")))
            {
                if (Page.FindControl("FDHButton") != null)
                {
                    if (Page.FindControl("FDHButton").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("FDHButton")).Text = AppHelper.GetQueryStringValue("FormName");
                    }
                    else if (Page.FindControl("FDHButton").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                    {
                        ((Label)Page.FindControl("FDHButton")).Text = AppHelper.GetQueryStringValue("FormName");
                    }
                }
            }
            //pgupta93: RA-4608 END

            //rkotak:start, mits 29024
            //had to take two controls with same ref as hidden control will be updated by javascript and server control textbox will be used to set and get values through databinder functions
            Control EntityListOrder = Page.FindControl("EntityListOrder");
            TextBox txtEntityListOrder = (TextBox)Page.FindControl("txtEntityListOrder");
            if (EntityListOrder != null && Page.FindControl("entitylist") != null)
            {
                ListBox lstEntityList = (ListBox)Page.FindControl("entitylist");
                HtmlInputHidden hdnEntityListOrder = (HtmlInputHidden)EntityListOrder;

                if (hdnEntityListOrder.Value == string.Empty)
                {
                    string sID = string.Empty;
                    string[] sIds;
                    for (int i = 0; i < lstEntityList.Items.Count; i++)
                    {
                        sIds = lstEntityList.Items[i].Value.Split('_');
                        if (sIds.Length > 1)
                        {
                            if (sIds[1] != string.Empty)
                                sID = sIds[1];
                        }
                        if (hdnEntityListOrder.Value == string.Empty)
                        {
                            hdnEntityListOrder.Value = sID;
                        }
                        else
                        {
                            hdnEntityListOrder.Value = hdnEntityListOrder.Value + "#" + sID;
                        }
                    }
                }
                if (txtEntityListOrder != null)
                {
                    txtEntityListOrder.Text = hdnEntityListOrder.Value;
                }
            }
            //rkotak:end, mits 29024

            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScript(sCulture, this);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes

            if (IsPostBack)
            {
                Control paytotheorder = Page.FindControl("paytotheorder");
                if (paytotheorder != null && Request.Form["entitylist"] != null)
                {
                    TextBox paytotheorderTxt = (TextBox)paytotheorder;
                    paytotheorderTxt.Text = Request.Form["paytotheorder"];
                }


             
                //skhare7 changed by shobhana as text was added to entity list but value was not there
               
                
                //End Amitosh
                bool bKeyFound = false;
                //Amitosh for EFT Payments


                //JIRA:438 START: ajohari2  
                //TextBox EftBank = (TextBox)Page.FindControl("EFTBanks");
                //if (EftBank != null)
                //{
                //    string[] seftbanks = EftBank.Text.Split(',');
                //    foreach (string sbank in seftbanks)
                //    {
                //        if (sbank.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                //        {
                //            ((CheckBox)Page.FindControl("IsEFTPayment")).Checked = true;
                //            break;
                //        }
                //        else
                //        {
                //            ((CheckBox)Page.FindControl("IsEFTPayment")).Checked = false;
                //        }
                //    }
                //}
                //JIRA:438 End: 

            if (CombPayeeAccountId != null)
            {
                if (((CheckBox)Page.FindControl("combinedpayflag")).Checked)
                {
                    if (CombPayeeAccountId.Text.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                    {
                        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = true;

                    }
                    else
                    {
                        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = false;
                    }
                }
            }
            if ((((CodeLookUp)Page.FindControl("currencytypetext")).Visible == true))
            {
                if (CombPayeeCurrtype != null)
                {
                    //mkaran2 MITS 32663 code merged for skhare7 MITS 28081
                    //if (((CheckBox)Page.FindControl("combinedpayflag")).Checked)
                    //{
                    //    arrCurrtype = CombPayeeCurrtype.Text.Split("_".ToCharArray()[0]);

                    //    if (arrCurrtype[1].Equals(PmtcurrencyType.Text, StringComparison.InvariantCultureIgnoreCase))
                    //    {
                    //        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = true;

                    //    }
                    //    else
                    //    {
                    //        ((CheckBox)Page.FindControl("combinedpayflag")).Checked = false;
                    //    }
                    //}                  
                    if (((TextBox)Page.FindControl("currencytypetext$codelookup_cid")) != null)
                    {
                        if (CombPayeeCurrtype.Text.Equals(((TextBox)Page.FindControl("currencytypetext$codelookup_cid")).Text, StringComparison.InvariantCultureIgnoreCase))
                        {
                            ((CheckBox)Page.FindControl("combinedpayflag")).Checked = true;
                        }
                        else
                        {
                            ((CheckBox)Page.FindControl("combinedpayflag")).Checked = false;
                        }
                     }
                 }
                    //mkaran2 MITS 32663 code merged for skhare7 MITS 28081
              }
                //End Amitosh
            }
           
            //End Amitosh
            // Get the Required Grid
            UserControlDataGrid dgFuturePayments = (UserControlDataGrid)this.FindControl("FuturePaymentsGrid");
            UserControlDataGrid dgThirdPartyPayments = (UserControlDataGrid)this.FindControl("ThirdPartyPaymentsGrid");

                cPostBackControl = Page.FindControl("SysPostBackAction");

                if (cPostBackControl != null)
                {
                    string sPopUpName = ((TextBox)cPostBackControl).Text;

                    switch (sPopUpName)
                    {
                        case "futurepayments":
                            dgThirdPartyPayments.IncludeLastRecord = false;
                            dgFuturePayments.IncludeLastRecord = true;
                            break;
                        case "thirdpartypayments":
                            dgFuturePayments.IncludeLastRecord = false;
                            dgThirdPartyPayments.IncludeLastRecord = true;
                            break;
                        default:
                            dgFuturePayments.IncludeLastRecord = false;
                            dgThirdPartyPayments.IncludeLastRecord = false;
                            break;
                    }

                    ((TextBox)cPostBackControl).Text = string.Empty;
                }

                foreach (string sKey in Request.QueryString)
                {
                    switch (sKey)
                    {
                        case "PolicyID":
                            if (AppHelper.GetQueryStringValue("PolicyID") != null)
                            {
                                if (Page.FindControl("PolicyID") != null)
                                {
                                    if (Page.FindControl("PolicyID").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("PolicyID")).Text = AppHelper.GetQueryStringValue("PolicyID");
                                    }
                                    else if (Page.FindControl("PolicyID").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("PolicyID")).Text = AppHelper.GetQueryStringValue("PolicyID");
                                    }

                                }
                            }
                            break;
                        case "RcRowId":
                            if (AppHelper.GetQueryStringValue("RcRowId") != null)
                            {
                                if (Page.FindControl("RcRowID") != null)
                                {
                                    if (Page.FindControl("RcRowID").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("RcRowID")).Text = AppHelper.GetQueryStringValue("RcRowId");
                                    }
                                    else if (Page.FindControl("RcRowID").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("RcRowID")).Text = AppHelper.GetQueryStringValue("RcRowId");
                                    }

                                }
                            }
                            break;
                        case "PolCvgId":
                            if (AppHelper.GetQueryStringValue("PolCvgId") != null)
                            {
                                if (Page.FindControl("PolCvgID") != null)
                                {
                                    if (Page.FindControl("PolCvgID").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("PolCvgID")).Text = AppHelper.GetQueryStringValue("PolCvgId");
                                    }
                                    else if (Page.FindControl("PolCvgID").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("PolCvgID")).Text = AppHelper.GetQueryStringValue("PolCvgId");
                                    }

                                }
                            }
                            break;
                        case "ResTypeCode":
                            if (AppHelper.GetQueryStringValue("ResTypeCode") != null)
                            {
                                if (Page.FindControl("ResTypeCode") != null)
                                {
                                    if (Page.FindControl("ResTypeCode").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("ResTypeCode")).Text = AppHelper.GetQueryStringValue("ResTypeCode");
                                    }
                                    else if (Page.FindControl("ResTypeCode").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("ResTypeCode")).Text = AppHelper.GetQueryStringValue("ResTypeCode");
                                    }

                                }
                            }
                            break;
                        //rupal:start, claimantEid is passed in querystring from BOB financial/reserves grid, so we need to receive it to get claimant id
                        case "ClaimantEid":
                            if (AppHelper.GetQueryStringValue("ClaimantEid") != null)
                            {
                                if (Page.FindControl("clm_entityid") != null)
                                {
                                    if (Page.FindControl("clm_entityid").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("clm_entityid")).Text = AppHelper.GetQueryStringValue("ClaimantEid");
                                    }
                                    else if (Page.FindControl("clm_entityid").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("clm_entityid")).Text = AppHelper.GetQueryStringValue("ClaimantEid");
                                    }

                                }
                            }
                            break;
                        //rupal:end
                        //Ankit : Start Policy System Interface
                        case "RsvStatusParent":
                            if (AppHelper.GetQueryStringValue("RsvStatusParent") != null)
                            {
                                if (Page.FindControl("ResStatusParent") != null)
                                {
                                    if (Page.FindControl("ResStatusParent").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("ResStatusParent")).Text = AppHelper.GetQueryStringValue("RsvStatusParent");
                                    }
                                }
                            }
                            break;
                        case "CovgSeqNum" :
                            if (AppHelper.GetQueryStringValue("CovgSeqNum") != null)
                            {
                                if (Page.FindControl("CovgSeqNum") != null)
                                {
                                    if (Page.FindControl("CovgSeqNum").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("CovgSeqNum")).Text = AppHelper.GetQueryStringValue("CovgSeqNum");
                                    }
                                    else if (Page.FindControl("CovgSeqNum").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("CovgSeqNum")).Text = AppHelper.GetQueryStringValue("CovgSeqNum");
                                    }

                                }
                            }
                            break;
                        //Ankit : End
                        case "CvgLossId":
                            if (AppHelper.GetQueryStringValue("CvgLossId") != null)
                            {
                                if (Page.FindControl("CvgLossID") != null)
                                {
                                    if (Page.FindControl("CvgLossID").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("CvgLossID")).Text = AppHelper.GetQueryStringValue("CvgLossId");
                                    }
                                    else if (Page.FindControl("CvgLossID").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("CvgLossID")).Text = AppHelper.GetQueryStringValue("CvgLossId");
                                    }

                                }
                            }
                            break;
                        case "TransSeqNum":
                            if (AppHelper.GetQueryStringValue("TransSeqNum") != null)
                            {
                                if (Page.FindControl("TransSeqNum") != null)
                                {
                                    if (Page.FindControl("TransSeqNum").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("TransSeqNum")).Text = AppHelper.GetQueryStringValue("TransSeqNum");
                                    }
                                    else if (Page.FindControl("TransSeqNum").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("TransSeqNum")).Text = AppHelper.GetQueryStringValue("TransSeqNum");
                                    }

                                }
                            }
                            break;
                        case "CoverageKey":             //Ankit Start : Worked on MITS - 34297
                            if (AppHelper.GetQueryStringValue("CoverageKey") != null)
                            {
                                if (Page.FindControl("CoverageKey") != null)
                                {
                                    if (Page.FindControl("CoverageKey").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    {
                                        ((TextBox)Page.FindControl("CoverageKey")).Text = AppHelper.GetQueryStringValue("CoverageKey");
                                    }
                                    else if (Page.FindControl("CoverageKey").GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                    {
                                        ((Label)Page.FindControl("CoverageKey")).Text = AppHelper.GetQueryStringValue("CoverageKey");
                                    }
                                }
                            }
                            break;
                    }
                }
                //Start by Shivendu for MITS 19278:resetting the hidden field
                if (Page.FindControl("InsuffReserve") != null)
                {
                    if (Page.FindControl("InsuffReserve").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        ((TextBox)Page.FindControl("InsuffReserve")).Text = "false"; ;
                    }
                }
                //End by Shivendu for MITS 19278
                if ((Page.FindControl("isClaimNumberReadOnly") != null) && (Page.FindControl("claimnumber") != null))
                {
                    if (Page.FindControl("isClaimNumberReadOnly").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        if (((TextBox)Page.FindControl("isClaimNumberReadOnly")).Text == "True")
                        {
                            if (Page.FindControl("claimnumber").GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                            {
                                ((TextBox)Page.FindControl("claimnumber")).Attributes.Add("readonly", "readonly");
                            }
                        }
                    }
                }
                FDMPageLoad();

                //Ankit Start : Financial Enhancements - Payee Phrase Change
                if (Page.FindControl("DefaultPayeePhrase") != null)
                {
                    if (base.Data != null)
                    {
                        XmlNode oPayeePhraseHiddenField = base.Data.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/PayeePhraseList");
                        if (oPayeePhraseHiddenField != null)
                            ((TextBox)Page.FindControl("DefaultPayeePhrase")).Text = base.Data.SelectSingleNode("/Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/PayeePhraseList").InnerText;
                    }
                }
                //Ankit End

                // npadhy In case the postback is done by the Save, we do not want to include the Last Record, as this will be same
                // as the second last record, This Last record is kept for the format.
                cPostBackControl = DatabindingHelper.GetPostBackControl(this.Page);

                if (cPostBackControl != null)
                {
                    // Get the PostBackControl 
                    if (cPostBackControl.ID == "save")
                    {
                        dgFuturePayments.IncludeLastRecord = false;
                        dgThirdPartyPayments.IncludeLastRecord = false;
                    }
                }
                if (entitylist != null && Request.Form["entitylist"] != null)
                {
                    string[] payeeText = Request.Form["entitylist"].Split(',');
                    entitylist.Items.Clear();

                    DataTable dt = new DataTable();

                    dt.Columns.Add("PayeeText");

                    dt.Columns.Add("PayeeValue");
                    foreach (string payee in payeeText)
                    {
                        if (payee.Contains("_"))
                        {
                            array = payee.Split("_".ToCharArray()[0]);

                            if (array[0].Trim() != "" && array[1].Trim() != "")
                            {

                                DataRow dr = dt.NewRow();

                                dr["PayeeText"] = array[0].ToString();

                                dr["PayeeValue"] = array[0].ToString() + "_" + array[1].ToString();

                                dt.Rows.Add(dr);

                                dt.AcceptChanges();

                            }
                        }

                    }

                    entitylist.DataTextField = dt.Columns[0].ToString();
                    entitylist.DataValueField = dt.Columns[1].ToString();

                    entitylist.DataSource = dt;

                    entitylist.DataBind();
                }
                //Deb Multi Currency
                if (dgFuturePayments.GridData != null)
                {
                    if (dgFuturePayments.GridData.SelectSingleNode("//FuturePayments") != null)
                    {
                        if (dgFuturePayments.GridData.SelectSingleNode("//FuturePayments").Attributes["count"].Value == "0")
                        {
                            DatabindingHelper.EnableControls("currencytypetext", this);
                        }
                    }
                }
                //Deb Multi Currency
                UserControlDataGrid dgPaymentInfo = (UserControlDataGrid)this.FindControl("PaymentInformationGrid");
                UserControlDataGrid dgThirdPayGrid = (UserControlDataGrid)this.FindControl("ThirdPartyPaymentsGrid");
                if (dgPaymentInfo != null)
                {
                    Control tbxCurrTpe = this.FindControl("currencytypetext$codelookup");
                    if (tbxCurrTpe != null)
                    {
                        if (tbxCurrTpe.Visible && !string.IsNullOrEmpty(((TextBox)tbxCurrTpe).Text))
                        {
                            dgPaymentInfo.CurrencyTypeTitle = ((TextBox)tbxCurrTpe).Text.Split('|')[0].Substring(0, 3) + " " + System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                        }
                    }
                }
                if (dgThirdPayGrid != null)
                {
                    Control tbxCurrTpe = this.FindControl("currencytypetext$codelookup");
                    if (tbxCurrTpe != null)
                    {
                        if (tbxCurrTpe.Visible && !string.IsNullOrEmpty(((TextBox)tbxCurrTpe).Text))
                        {
                            dgThirdPayGrid.CurrencyTypeTitle = ((TextBox)tbxCurrTpe).Text.Split('|')[0].Substring(0, 3) + " " + System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol;
                        }
                    }
                }
                //Deb Multi Currency
                //JIRA:438 START: ajohari2
                TextBox EftBank = (TextBox)Page.FindControl("EFTBanks");
                TextBox hdnIsEFTPayment = (TextBox)Page.FindControl("hdnIsEFTPayment"); //JIRA:438 : ajohari2
                int iautotransid = 0;
                string sctlnumber = string.Empty;
                if (((TextBox)Page.FindControl("autotransid")) != null)
                {
                    int.TryParse(((TextBox)Page.FindControl("autotransid")).Text, out iautotransid);
                }
                if (((TextBox)Page.FindControl("ctlnumber")) != null)
                {
                     sctlnumber = ((TextBox)Page.FindControl("ctlnumber")).Text.Trim();
                }
                //Aa 7199
                if ((Page.FindControl("mailtopye")) != null && (Page.FindControl("MailToSamePayee")) != null && ((CheckBox)Page.FindControl("mailtopye")).Checked == true)
                    ((TextBox)(Page.FindControl("MailToSamePayee"))).Text = "true";

                if (iautotransid <= 0 && (Page.FindControl("mailtopye")) != null && MailToPayee.Text != String.Empty)
                {
                    ((CheckBox)Page.FindControl("mailtopye")).Checked = Convert.ToBoolean(MailToPayee.Text);
                }
                else if (iautotransid <= 0 && !IsPostBack)
                {
                    ((CheckBox)Page.FindControl("mailtopye")).Checked = true;
                }
			// npadhy JIRA 6418 Starts As we have removed the EFTPayment control from Funds Auto page, we remove any refernce of the control from the code behind as well
            //if (((CheckBox)Page.FindControl("IsEFTPayment")) != null)
            //{
            if (EftBank != null)
            {
                string[] seftbanks = EftBank.Text.Split(',');
                foreach (string sbank in seftbanks)
                {
                    if (sbank.Equals(bankaccount.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (iautotransid > 0 || sctlnumber.CompareTo(string.Empty) != 0 || Page.IsPostBack)
                        {
                            if ((hdnIsEFTPayment != null && hdnIsEFTPayment.Text.ToLower() == "false"))
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = false;
                                hdnIsEFTPayment.Text = "false";
                            }
                            else
                            {
                                //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = true;
                                if (hdnIsEFTPayment != null)
                                    hdnIsEFTPayment.Text = "true";
                            }
                        }
                        else
                        {
                            //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = true;
                            if (hdnIsEFTPayment != null)
                                hdnIsEFTPayment.Text = "true";
                        }
                        //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = true;
                        break;
                    }
                    else
                    {
                        //((CheckBox)Page.FindControl("IsEFTPayment")).Checked = false;
                        //((CheckBox)Page.FindControl("IsEFTPayment")).Enabled = false;
                        if (hdnIsEFTPayment != null && hdnIsEFTPayment.Text.Trim() == "")
                            hdnIsEFTPayment.Text = "false";
                    }
                }
            }

            //}
            //JIRA:438 End: 
			// npadhy JIRA 6418 Ends As we have removed the EFTPayment control from Funds Auto page, we remove any refernce of the control from the code behind as well
        }

        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

            Control BankAccount = this.FindControl("bankaccount");


            if(BankAccount != null)
                ((DropDownList)BankAccount).Attributes.Add("style", "WIDTH: 72%");

          
        }

        public override void ModifyControls()
        {
            base.ModifyControls();
            //RMA-8490 START
            ImageButton obacktofinancials = (ImageButton)Form.FindControl("backtofinancials");
            ImageButton ofundspaymenthistory = (ImageButton)Form.FindControl("fundspaymenthistory");
            Control oControlFromFinancial = this.Form.FindControl("FromFinancial");
            //RMA-8490 END

            if (Page.FindControl("autotransid") != null)
            {
                TextBox txtAutoTransId = (TextBox)Page.FindControl("autotransid");
                if (txtAutoTransId.Text == "0")
                {
                    ((CheckBox)Page.FindControl("adjustprintdatesflag")).Checked = true;
                }
            }
            //RMA-8490 START
            if (oControlFromFinancial != null)
            {
                if (string.Equals(((TextBox)oControlFromFinancial).Text, ""))
                {
                    if (obacktofinancials != null)
                      obacktofinancials.Visible = false;
                    if (ofundspaymenthistory != null)
                      ofundspaymenthistory.Visible = false;
                }
            }
            //RMA-8490 END
        }
        
    }
}
