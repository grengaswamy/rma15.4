﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.FDM
{
    public partial class AdminTrackingList : FDMBasePage
    {
        public IEnumerable result = null;
        public XElement rootElement = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //rsushilaggar changes for Pan testing
                if (!string.IsNullOrEmpty(Request.QueryString["RedidectURL"]))
                {
                    string strUrl = Request.QueryString["RedidectURL"].Replace('^', '&');
                    Response.Redirect(strUrl);
                }
                //end rsushilaggar
                LoadInitialPage();
                rootElement = XElement.Parse(Data.OuterXml);
                result = from c in rootElement.XPathSelectElements("//Param/ADMTableList/ADMTable")
                         let xClaim = c.Element("TableName").Value
                         orderby xClaim 
                         select c;
            }
            catch (Exception ex)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                string error = ErrorHelper.formatUIErrorXML(err);
                BindData2ErrorControl(error);
            }

        }

    }
}
