﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
namespace Riskmaster.UI.FDM
{
    public partial class Vehicle : FDMBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FDMPageLoad();
           
        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);
            if (this.FindControl("ParentPage") != null)
                ((TextBox)(this.FindControl("ParentPage"))).Text = AppHelper.GetQueryStringValue("OpenedFromPolicy");

        }

        protected void Page_PreRender()
        {
            if (this.Page.FindControl("vehicleyear") != null)
            {
                if (this.Page.FindControl("vehicleyear").GetType().Name == "TextBox")
                {
                    if (((TextBox)this.Page.FindControl("vehicleyear")).Text == "0")
                    {
                        ((TextBox)this.Page.FindControl("vehicleyear")).Text = "";
                    }
                }
                else
                {
                    if (((Label)this.Page.FindControl("vehicleyear")).Text == "0")
                    {
                        ((Label)this.Page.FindControl("vehicleyear")).Text = "";
                    }
                }
            }
        }

    }
}
