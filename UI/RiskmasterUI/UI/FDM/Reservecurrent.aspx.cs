﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.UI.Shared.Controls;
using MultiCurrencyCustomControl;
using System.Threading;
using System.Globalization;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.FDM
{
    public partial class Reservecurrent : FDMBasePage
    {
        //for jason object
        public struct JData
        {
            public string Name { get; set; }
            public string Value { get; set; }
            public string Level { get; set; }
            public string SelectedValue { get; set; }
        }

        private string sMode = string.Empty;
        private static XElement oResSta = null;
        private string sReturn = String.Empty;
        public string sCWSOutput = String.Empty;
        XmlDocument objXmlDoc = null;
        private static XmlDocument _document;
        private static int iReserveID = 0;
        JData objData;

        private static string sSelectedUnitId = string.Empty;
        private static string sSelectedCoverageId = string.Empty;
        private static string sSelectedReserveTypeCode = string.Empty;
        private static string sSelectedReserveSubType = string.Empty;
        private static string sSelectedLossTypeCode = string.Empty;
        private static string sSelectedDisablityCat = string.Empty;
        private static string sSelectedPolicy = string.Empty;
        private static string sSelectedClaimant = string.Empty;//RMA-11468 pgupta93
        private static XElement xEleDDLData = null;
        private static bool bClearSelected = false;

     

        protected void Page_Load(object sender, EventArgs e)
        {

            TextBox sxClaimId = (TextBox)this.FindControl("sxClaimId");
            TextBox sxLOB = (TextBox)this.FindControl("sxLOB");
            TextBox claimid = (TextBox)this.FindControl("claimid");
            TextBox lob = (TextBox)this.FindControl("lob");
            TextBox rcClaimId = (TextBox)this.FindControl("rcClaimId");
            TextBox sxClaimantEID = (TextBox)this.FindControl("sxClaimantEID");
            TextBox claimanteid = (TextBox)this.FindControl("claimanteid");
            TextBox rcClaimantEid = (TextBox)this.FindControl("rcClaimantEid");
            TextBox sxClaimCurrency = (TextBox)this.FindControl("sxClaimCurrency");
            TextBox rcClaimCurrency = (TextBox)this.FindControl("rcClaimCurrency");
            TextBox txtAction = (TextBox)this.FindControl("txtAction");
            TextBox currencytype = (TextBox)this.FindControl("currencytype");
            string sCurrCulture = string.Empty;
            //pgupta93  RMA-11468 starts
            TextBox claimnumber = (TextBox)this.FindControl("claimnumber");

            if (Request.QueryString["claimnumber"] != null)
            {
                claimnumber.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimnumber"));                

            }
            //RMA-8490 START
            TextBox ClaimantListClaimantEID = (TextBox)this.FindControl("ClaimantListClaimantEID");
            if (Request.QueryString["ClaimantListClaimantEID"] != null)
            {
                ClaimantListClaimantEID.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantListClaimantEID"));
                claimanteid.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantListClaimantEID"));
                sxClaimantEID.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantListClaimantEID"));
                rcClaimantEid.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantListClaimantEID")); 
            }
            //RMA-8490 END
            //pgupta93  RMA-11468 ends

            if (sxClaimId != null && rcClaimId != null)
            {
                if (Request.QueryString["claimId"] != null)
                {
                    sxClaimId.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimId"));
                    rcClaimId.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimId"));
                }
                else
                {
                    rcClaimId.Text = sxClaimId.Text;
                }
            }

            //if (sxLOB != null)
            //{
            //    if (Request.QueryString["lob"] != null)
            //    {
            //        sxLOB.Text = Request.QueryString["lob"].ToString();
            //    }
            //}

            if (sxClaimantEID != null && rcClaimantEid != null)
            {
                if (Request.QueryString["claimanteid"] != null)
                {
                    sxClaimantEID.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimanteid"));
                    rcClaimantEid.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimanteid"));
                }
                else
                {
                    rcClaimantEid.Text = sxClaimantEID.Text;
                }
            }

            if (sxClaimCurrency != null && rcClaimCurrency != null && currencytype != null)
            {
                if (Request.QueryString["claimcurrencytype"] != null)
                {
                    sxClaimCurrency.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimcurrencytype"));
                    rcClaimCurrency.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimcurrencytype"));
                    currencytype.Text = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("claimcurrencytype"));
                }
                else
                {
                    rcClaimCurrency.Text = sxClaimCurrency.Text;
                }
            }
            if ((currencytype != null) && (!string.IsNullOrEmpty(currencytype.Text)))
            {
                sCurrCulture = currencytype.Text;

                if (!string.IsNullOrEmpty(sCurrCulture))
                {
                    Culture = sCurrCulture.Split('|')[1];
                    UICulture = sCurrCulture.Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(sCurrCulture.Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(sCurrCulture.Split('|')[1]);
                }
            }
           if (txtAction != null && !string.IsNullOrEmpty(txtAction.Text))
            {
                if (txtAction.Text == "approve")
                {
                    ApproveReserve();
                    sMode = "Approve";
                    RefreshParentClosePopUp();
                }
                else if (txtAction.Text == "reject")
                {
                    RejectReserve();
                    sMode = "Reject";
                    RefreshParentClosePopUp();
                    
                }
                else if (txtAction.Text == "addmore")
                {
                    sMode = "AddMore";
                }
                else if (txtAction.Text == "currencychange")
                {
                    sMode = "CurrencyChange";
                }
                txtAction.Text = string.Empty;
            }
            
            FDMPageLoad();
            TextBox UnitName = (TextBox)this.FindControl("UnitName");
            if (UnitName != null)
            {
                UnitName.Text = UnitName.Text.Replace(" ^*^*^ ", "&amp;");
            }
            TextBox rcrowid = (TextBox)this.FindControl("rcrowid");
            if (rcrowid != null && rcrowid.Text != "" && rcrowid.Text != "0")
            {
                if (string.IsNullOrEmpty(sMode) && sMode != "CurrencyChange")
                sMode = "Edit";
                DisableEnableControls(false);
            }
            else
            {
                if (string.IsNullOrEmpty(sMode) && sMode != "addmore")
                sMode = "Add";
                DisableEnableControls(true);
            }
            
            if (!IsPostBack)
            {
               BindData();
            }
            else if (sMode == "CurrencyChange")
            {
                bClearSelected = true;
                SetSelectedValues();
            }

            //rma 16852 starts
            TextBox txtclm_lastname = (TextBox)this.FindControl("clm_lastname");
            if (txtclm_lastname != null)
                txtclm_lastname.Enabled = false;
            //rma 16852 ends


        }

        private bool RefreshParentClosePopUp()
        {
            Label lblError = (Label)this.FindControl("lblError");
            if (lblError != null && string.IsNullOrEmpty(lblError.Text))
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "RefreshParentCloseFDMPopup('" + sMode + "');", true);
                return true;
            }
            else
            {
                BindData();
                bClearSelected = true;
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "ReserveCurrentPageLoad();", true);
            }

            return false;
        }

        private void DisableEnableControls(bool bVisible)
        {
            DropDownList cmbPolicy = (DropDownList)this.FindControl("cmbPolicy");
            DropDownList cmbUnit = (DropDownList)this.FindControl("cmbUnit");
            DropDownList cmbCoverageType = (DropDownList)this.FindControl("cmbCoverageType");
            DropDownList cmbCoverageLossType = (DropDownList)this.FindControl("cmbCoverageLossType");
            DropDownList cmbReserveType = (DropDownList)this.FindControl("cmbReserveType");
            DropDownList cmbReserveCategoryAdd = (DropDownList)this.FindControl("cmbReserveCategoryAdd");
            DropDownList cmbdisablitycat = (DropDownList)this.FindControl("cmbdisablitycat");
            CodeLookUp disabilitytype = (CodeLookUp)this.FindControl("disabilitytype");


            if (bVisible)
            {
                if (cmbPolicy != null)
                {
                    DatabindingHelper.EnableControls("cmbPolicy", this);
                }
                if (cmbUnit != null)
                {
                    DatabindingHelper.EnableControls("cmbUnit", this);
                }
                if (cmbCoverageType != null)
                {
                    DatabindingHelper.EnableControls("cmbCoverageType", this);
                }
                if (cmbCoverageLossType != null)
                {
                    DatabindingHelper.EnableControls("cmbCoverageLossType", this);
                }
                if (cmbReserveType != null)
                {
                    DatabindingHelper.EnableControls("cmbReserveType", this);
                }
                if (cmbReserveCategoryAdd != null)
                {
                    DatabindingHelper.EnableControls("cmbReserveCategoryAdd", this);
                }
                if (cmbdisablitycat != null)
                {
                    DatabindingHelper.EnableControls("cmbdisablitycat", this);
                }
                if (disabilitytype != null)
                {
                    DatabindingHelper.EnableControls("disabilitytype", this);
                }

            }
            else
            {
                if (cmbPolicy != null)
                {
                    cmbPolicy.Enabled = bVisible;
                }
                if (cmbUnit != null)
                {
                    cmbUnit.Enabled = bVisible;
                }
                if (cmbCoverageType != null)
                {
                    cmbCoverageType.Enabled = bVisible;
                }
                if (cmbCoverageLossType != null)
                {
                    cmbCoverageLossType.Enabled = bVisible;
                }
                if (cmbReserveType != null)
                {
                    cmbReserveType.Enabled = bVisible;
                }
                if (cmbReserveCategoryAdd != null)
                {
                    cmbReserveCategoryAdd.Enabled = bVisible;
                }
                if (cmbdisablitycat != null)
                {
                    cmbdisablitycat.Enabled = bVisible;
                }
                if (disabilitytype != null)
                {
                    disabilitytype.Enabled = bVisible;
                }
            } 
        }

        private void SetSelectedValues()
        {
            TextBox PolicyId = (TextBox)this.FindControl("PolicyId");
            TextBox sxLob = (TextBox)this.FindControl("sxLob");
            TextBox UnitName = (TextBox)this.FindControl("UnitName");
            TextBox CoverageId = (TextBox)this.FindControl("CoverageId");
            TextBox ReserveTypeCode = (TextBox)this.FindControl("ReserveTypeCode");
            TextBox ReserveSubType = (TextBox)this.FindControl("ReserveSubType");
            TextBox LossTypeCode = (TextBox)this.FindControl("LossTypeCode");
            TextBox DisablityCat = (TextBox)this.FindControl("DisablityCat");
            TextBox txtAction = (TextBox)this.FindControl("txtAction");
            //TextBox DisabilityLossType = (TextBox)this.FindControl("DisabilityLossType");
            //rkotak:starrts, preeti's code  merged for reserve listing ng grid
            TextBox ClaimantEid = (TextBox)this.FindControl("sxClaimantEID");
            TextBox clm_entityid = (TextBox)this.FindControl("clm_entityid");
            TextBox ClaimantName = (TextBox)this.FindControl("ClaimantName");
            TextBox clm_lastname = (TextBox)this.FindControl("clm_lastname");
            //rkotak:ends, preeti's code  merged for reserve listing ng grid

            if (UnitName != null)
            {
                sSelectedUnitId = UnitName.Text;
            }
            if (CoverageId != null)
            {
                sSelectedCoverageId = CoverageId.Text;
            }
            if (ReserveTypeCode != null)
            {
                sSelectedReserveTypeCode = ReserveTypeCode.Text;
            }
            if (ReserveSubType != null)
            {
                sSelectedReserveSubType = ReserveSubType.Text;
            }
            if (LossTypeCode != null)
            {
                sSelectedLossTypeCode = LossTypeCode.Text;
            }
            if (DisablityCat != null)
            {
                sSelectedDisablityCat = DisablityCat.Text;
            }
            //rkotak:starts,  preeti's code mreged for reserve listing ng grid
            clm_entityid.Text = ClaimantEid.Text;
            clm_lastname.Text = ClaimantName.Text;
            //rkotAK:ends,  PREETI'S CODE merged for reserve listing grid

            if (sMode == "Add")
            {
                sSelectedPolicy = "0";
            }
            else if (PolicyId != null)
            {
                sSelectedPolicy = PolicyId.Text;
            }
            if ((bClearSelected == true && sMode == "Add") || sMode == "CurrencyChange")
            {
                PolicyId.Text = string.Empty;
                UnitName.Text = string.Empty;
                CoverageId.Text = string.Empty;
                ReserveTypeCode.Text = string.Empty;
                ReserveSubType.Text = string.Empty;
                LossTypeCode.Text = string.Empty;
                DisablityCat.Text = string.Empty;
                sxLob.Text = string.Empty;
                if (sMode == "CurrencyChange")
                {
                    sMode = string.Empty;

                }
            }
        
        }
        private void BindData()
        {

            TextBox PolicyId = (TextBox)this.FindControl("PolicyId");
            TextBox sxClaimId = (TextBox)this.FindControl("sxClaimId");
            TextBox claimid = (TextBox)this.FindControl("claimid");
            TextBox sxClaimantEID = (TextBox)this.FindControl("sxClaimantEID");
            TextBox claimanteid = (TextBox)this.FindControl("claimanteid");
            TextBox rcClaimantEid = (TextBox)this.FindControl("rcClaimantEid");
            TextBox IsHold = (TextBox)this.FindControl("IsHold");

            if (sxClaimId != null && claimid != null)
            {
                sxClaimId.Text = claimid.Text;
            }

            if (sxClaimantEID != null && claimanteid != null)
            {
                //rma 16569 starts, claimanteid.Text gets populated with query string value. in case of add reserve, we get claimant id from business layer if there is only one claimant available for the claim. we dont want want that to be overriden by empty query string in case of add reserve
                if (sMode == "Add")
                    claimanteid.Text = sxClaimantEID.Text;
                else//rma 16569 ends
                    sxClaimantEID.Text = claimanteid.Text;

            }
            SetSelectedValues();
           
            objData = new JData();
            Dictionary<string, string> dictOptions = new Dictionary<string, string>();
            TextBox txtDDLData = (TextBox)this.FindControl("DDLData");
            if (txtDDLData != null)
            {
               xEleDDLData = XElement.Parse(txtDDLData.Text.Replace("\\", ""));
            }
            TextBox ReserveStatusReason = (TextBox)this.FindControl("ReserveStatusReason");
            if (ReserveStatusReason != null)
            {
               oResSta = XElement.Parse(ReserveStatusReason.Text.Replace("\\",""));
            }
            objData.Level = "Policy";
            objData.SelectedValue = sSelectedPolicy;
            List<JData> objList = BindDropDown(objData, xEleDDLData);
            dictOptions = GetDropDownData(objList);
            
            DropDownList cmbPolicy = (DropDownList)this.FindControl("cmbPolicy");
          
            if (cmbPolicy != null)
            {
                cmbPolicy.DataSource = dictOptions;
                cmbPolicy.DataTextField = "key";
                cmbPolicy.DataValueField = "value";
                cmbPolicy.DataBind();
                cmbPolicy.Items.Insert(0, new ListItem("", ""));

                if (PolicyId != null && !string.IsNullOrEmpty(PolicyId.Text) && PolicyId.Text != "0")
                    cmbPolicy.SelectedValue = PolicyId.Text;
                else
                {
                    if (dictOptions.Count == 1)
                       cmbPolicy.SelectedIndex = 1;
                }
            }

            DropDownList cmbStatusAdd = (DropDownList)this.FindControl("cmbStatusAdd");
            cmbStatusAdd.Items.Clear();
            TextBox ResStatusCode = (TextBox)this.FindControl("ResStatusCode");

            if (cmbStatusAdd != null)
            {
                TextBox status = (TextBox)this.FindControl("status");
                if (IsHold.Text == "-1")
                {
                    //JIRA RMA-13983 (RMA-18291) ajohari2
                    DropDownList ddlReasonAdd = (DropDownList)this.FindControl("cmbReasonAdd");
                    TextBox txtReasonAdd = (TextBox)this.FindControl("txtReasonAdd");

                    if (ddlReasonAdd != null)
                        ddlReasonAdd.Enabled = false;

                    if (txtReasonAdd != null)
                        txtReasonAdd.Enabled = false;
                    //JIRA RMA-13983 (RMA-18291) ajohari2 END

                    cmbStatusAdd.Enabled = false;
                }
                    
                    objData.Level = "ReserveStatus";
                    objList = BindDropDown(objData, oResSta);
                    TextBox rcrowid = (TextBox)this.FindControl("rcrowid");
                    ListItem objItem = null;
                    objItem = new ListItem();
                    objItem.Text = "";
                    objItem.Value = "";
                    objItem.Attributes.Add("Tag", "");
                    cmbStatusAdd.Items.Add(objItem);
                    Dictionary<string, string> dictData = new Dictionary<string, string>();
                    foreach (JData ob in objList)
                    {
                        string[] str = ob.Value.Split('#');
                        objItem = new ListItem();
                        objItem.Text = ob.Name;
                        objItem.Value = str[0];
                        objItem.Attributes.Add("Tag", str[1].ToUpper());
                        if (str[1] != string.Empty)
                        {
                            if (rcrowid != null && (rcrowid.Text == "" || rcrowid.Text == "0") && !str[1].ToUpper().Equals("F") && !str[1].ToUpper().Equals("CN"))
                            {   //add reserve
                                cmbStatusAdd.Items.Add(objItem);
                            }

                            if (rcrowid != null && (rcrowid.Text != "" && rcrowid.Text != "0") && !str[1].ToUpper().Equals("F"))
                            {   //edit reserve
                                cmbStatusAdd.Items.Add(objItem);
                            }
                        }
                        else
                        {
                            cmbStatusAdd.Items.Add(objItem);
                        }
                    }
                    if (ResStatusCode != null)
                        cmbStatusAdd.SelectedValue = ResStatusCode.Text;                  
            }
            DropDownList cmbReasonAdd = (DropDownList)this.FindControl("cmbReasonAdd");

            if (cmbReasonAdd != null)
            {
                objData = new JData();
                objData.Level = "ResChangeReason";
                objList = BindDropDown(objData, oResSta);
                dictOptions = GetDropDownData(objList);
                cmbReasonAdd.DataSource = dictOptions;
                cmbReasonAdd.DataTextField = "key";
                cmbReasonAdd.DataValueField = "value";
                cmbReasonAdd.DataBind();
                cmbReasonAdd.Items.Insert(0, new ListItem("", "0"));
            }
        }

        Dictionary<string, string> GetDropDownData(List<JData> objData)
        {
            Dictionary<string, string> dictData = new Dictionary<string, string>();
            foreach (JData ob in objData)
            {
                dictData.Add(ob.Name, ob.Value);
            }

            return dictData;
        }
        protected void NavigateSave(object sender, EventArgs e)
        {
            base.NavigateSave(sender, e);
            RefreshParentClosePopUp();
        }


        [WebMethod]
        public static List<JData> FetchDDLData(string Name, string Value, string Level)
        {
            List<JData> objData = null;
            JData obj = new JData { Name = Name, Value = Value, Level = Level };
            XElement res = xEleDDLData;
            objData = BindDropDown(obj, res);

            return objData;
        }

        public static List<JData> BindDropDown(JData obj, XElement res)
        {

            List<JData> objData = null;
            string[] sKeys = null;

            if (obj.Level == "Policy")
            {
                if (obj.SelectedValue != "0")
                {
                    objData = (from p in res.Elements("policy")
                               where p.Attribute("value").Value == obj.SelectedValue
                               select new JData
                               {
                                   Name = p.Attribute("name").Value,
                                   Value = p.Attribute("value").Value
                               }).ToList<JData>();
                }
                else
                {
                    objData = (from p in res.Elements("policy")
                               select new JData
                               {
                                   Name = p.Attribute("name").Value,
                                   Value = p.Attribute("value").Value
                               }).ToList<JData>();
                }

            }
            else if (obj.Level == "Unit")
            {

                objData =
                    (from p1 in
                         (from p in res.Elements("policy")
                          where p.Attribute("value").Value == obj.Value
                          select p).Elements("unit")
                     select new JData
                     {
                         Name = p1.Attribute("name").Value,
                         Value = p1.Attribute("value").Value,
                         SelectedValue = sSelectedUnitId

                     }).ToList<JData>();
                if (bClearSelected)
                {
                    sSelectedUnitId = string.Empty;
                }

            }
            else if (obj.Level == "Coverage")
            {

                sKeys = obj.Value.Split(' ');
                objData = (from p2 in
                               (from p1 in
                                    (from p in res.Elements("policy")
                                     where p.Attribute("value").Value == sKeys[0]
                                     select p).Elements("unit")
                                where p1.Attribute("value").Value == sKeys[1]
                                select p1).Elements("coverage")
                           select new JData
                           {
                               Name = p2.Attribute("name").Value,
                               Value = p2.Attribute("value").Value,
                               SelectedValue = sSelectedCoverageId

                           }
                 ).ToList<JData>();
                if (bClearSelected)
                {
                    sSelectedCoverageId = string.Empty;
                }
            }
            else if (obj.Level == "Loss")
            {

                sKeys = obj.Value.Split(' ');
                objData = (from p3 in
                               (from p2 in
                                    (from p1 in
                                         (from p in res.Elements("policy")
                                          where p.Attribute("value").Value == sKeys[0]
                                          select p).Elements("unit")
                                     where p1.Attribute("value").Value == sKeys[1]
                                     select p1).Elements("coverage")
                                where p2.Attribute("value").Value == sKeys[2]
                                select p2).Elements("loss")
                           select new JData
                           {
                               Name = p3.Attribute("name").Value,
                               Value = p3.Attribute("value").Value,
                               SelectedValue = sSelectedLossTypeCode

                           }
                     ).ToList<JData>();
                if (bClearSelected)
                {
                    sSelectedLossTypeCode = string.Empty;
                }

            }
            else if (obj.Level == "DisabilityCategory")
            {

                sKeys = obj.Value.Split(' ');
                objData = (from p3 in
                               (from p2 in
                                    (from p1 in
                                         (from p in res.Elements("policy")
                                          where p.Attribute("value").Value == sKeys[0]
                                          select p).Elements("unit")
                                     where p1.Attribute("value").Value == sKeys[1]
                                     select p1).Elements("coverage")
                                where p2.Attribute("value").Value == sKeys[2]
                                select p2).Elements("disabilitycat")
                           select new JData
                           {
                               Name = p3.Attribute("name").Value,
                               Value = p3.Attribute("value").Value,
                               SelectedValue = sSelectedDisablityCat

                           }
                     ).ToList<JData>();
                if (bClearSelected)
                {
                    sSelectedDisablityCat = string.Empty;
                }
            }
            else if (obj.Level == "ResChangeReason")
            {
                objData = (from p in res.Element("Reason").Elements("ReasonCode")
                           select new JData
                           {
                               Name = p.Value,
                               Value = p.Attribute("value").Value
                           }).ToList<JData>();
            }
            else if (obj.Level == "ReserveStatus")
            {
                objData = (from p in res.Element("Status").Elements("StatusCode")
                           select new JData
                           {
                               Name = p.Value,
                               Value = p.Attribute("value").Value + "#" + (p.Attribute("shortcode") != null ? p.Attribute("shortcode").Value : string.Empty)
                           }).ToList<JData>();
            }
            else if (obj.Level == "ReserveType")
            {
                sKeys = obj.Value.Split(' ');
                objData = (from p3 in
                               (from p2 in
                                    (from p1 in
                                         (from p in res.Elements("policy")
                                          where p.Attribute("value").Value == sKeys[0]
                                          select p).Elements("unit")
                                     where p1.Attribute("value").Value == sKeys[1]
                                     select p1).Elements("coverage")
                                where p2.Attribute("value").Value == sKeys[2]
                                select p2).Elements("reserve")
                           select new JData
                           {
                               Name = p3.Attribute("name").Value,
                               Value = p3.Attribute("value").Value,
                               SelectedValue = sSelectedReserveTypeCode

                           }
                     ).ToList<JData>();
                if (bClearSelected)
                {
                    sSelectedReserveTypeCode = string.Empty;
                    bClearSelected = false;
                }
            }
            else if (obj.Level == "ReserveSubType")
            {
                sKeys = obj.Value.Split(' ');
                objData =
                    (from p4 in
                         (from p3 in
                              (from p2 in
                                   (from p1 in
                                        (from p in res.Elements("policy")
                                         where p.Attribute("value").Value == sKeys[0]
                                         select p).Elements("unit")
                                    where p1.Attribute("value").Value == sKeys[1]
                                    select p1).Elements("coverage")
                               where p2.Attribute("value").Value == sKeys[2]
                               select p2).Elements("reserve")
                          where p3.Attribute("value").Value == sKeys[3]
                          select p3).Elements("reservesubtype")
                     select new JData
                          {
                              Name = p4.Attribute("name").Value,
                              Value = p4.Attribute("value").Value,
                              SelectedValue = sSelectedReserveSubType

                          }).ToList<JData>();
                if (bClearSelected)
                {
                    sSelectedReserveSubType = string.Empty;
                }
            }

            return objData;


        }
        protected override void OnUpdateForm(XElement oMessageElement)
        {
            base.OnUpdateForm(oMessageElement);

        }
        private void ApproveReserve()
        {
            try
            {
                string sTemplate = GetXmlInForApproveOrReject("Approve");
                sReturn = AppHelper.CallCWSService(sTemplate);
                ErrorExist(sReturn);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
            }
        }
        private void ErrorExist(string sReturn)
        {
            if (!ErrorHelper.IsCWSCallSuccess(sReturn))
            {
                Label lblError = (Label)this.FindControl("lblError");
                bool bIsSuccess = false;
                if(lblError !=null)
                lblError.Text = ErrorHelper.FormatServiceErrors(sReturn, ref bIsSuccess);
            }
        }

        private void RejectReserve()
        {
            try
            {
                string sTemplate = GetXmlInForApproveOrReject("Reject");
                sReturn = AppHelper.CallCWSService(sTemplate);
                ErrorExist(sReturn);
            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
            }

        }

        private string GetXmlInForApproveOrReject(string p_sAction)
        {
            TextBox rcClaimId = (TextBox)this.FindControl("rcClaimId");
            TextBox rcClaimantEid = (TextBox)this.FindControl("rcClaimantEid");
            TextBox CoverageId = (TextBox)this.FindControl("CoverageId");
            TextBox ReserveTypeCode = (TextBox)this.FindControl("ReserveTypeCode");
            TextBox rcrowid = (TextBox)this.FindControl("rcrowid");
            CurrencyTextbox txtAmount = (CurrencyTextbox)this.FindControl("txtAmount");
            TextBox txtAppRejReqCom = (TextBox)this.FindControl("txtAppRejReqCom");
            TextBox LossTypeCode = (TextBox)this.FindControl("LossTypeCode");
            
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>ReserveFundsAdaptor.ApproveOrRejectReserve</Function></Call><Document>");
            sXml = sXml.Append("<BOB>");

            if (rcClaimId != null)
            {
                sXml = sXml.Append("<ClaimID>");
                sXml = sXml.Append(rcClaimId.Text);
                sXml = sXml.Append("</ClaimID>");
            }
            if (rcClaimantEid != null)
            {
                sXml = sXml.Append("<ClaimantEID>");
                sXml = sXml.Append(rcClaimantEid.Text);
                sXml = sXml.Append("</ClaimantEID>");
            }
            if (CoverageId != null)
            {
                sXml = sXml.Append("<CoverageID>");
                sXml = sXml.Append(CoverageId.Text);
                sXml = sXml.Append("</CoverageID>");
            }
            if (ReserveTypeCode != null)
            {
                sXml = sXml.Append("<ReserveTypeCode>");
                sXml = sXml.Append(ReserveTypeCode.Text);
                sXml = sXml.Append("</ReserveTypeCode>");
            }
            if (rcClaimId != null)
            {
                sXml = sXml.Append("<RCRowId>");
                sXml = sXml.Append(rcrowid.Text);
                sXml = sXml.Append("</RCRowId>");
            }
            if (txtAmount != null)
            {
                sXml = sXml.Append("<Amount>");
                sXml = sXml.Append(txtAmount.AmountInString);
                sXml = sXml.Append("</Amount>");
            }

            sXml = sXml.Append("<ApproveorRejectRsv>");
            sXml = sXml.Append(p_sAction);
            sXml = sXml.Append("</ApproveorRejectRsv>");

            if (txtAppRejReqCom != null)
            {
                sXml = sXml.Append("<ApproveReasonCom>");
                sXml = sXml.Append(txtAppRejReqCom.Text);
                sXml = sXml.Append("</ApproveReasonCom>");
            }
            if (LossTypeCode != null && LossTypeCode.Text != "undefined")
            {
                sXml = sXml.Append("<CvgLossId>");
                sXml = sXml.Append(LossTypeCode.Text);
                sXml = sXml.Append("</CvgLossId>");
            }
            sXml = sXml.Append("</BOB></Document></Message>");

            return sXml.ToString();


        }

        public override void ModifyControls()
        {
            base.ModifyControls();

            Button oclmlastnamebtn = (Button)Form.FindControl("clm_lastnamebtn");
            TextBox oclm_lastname = (TextBox)Form.FindControl("clm_lastname");

            if (oclmlastnamebtn != null)
            {
                if (!string.IsNullOrEmpty(AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("ClaimantListClaimantEID"))))
                {
                    oclmlastnamebtn.Enabled = false;
                    if (oclm_lastname != null)
                        oclm_lastname.Enabled = false;
                }
            }
        } 
    }

}