<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PremiumFrame.aspx.cs" Inherits="Riskmaster.UI.Billing.PremiumFrame" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagPrefix="uc2" TagName="ErrorControl"  %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>PremiumItem</title>
    <base target="_self"/>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>
    <script language="JavaScript" src="../../Scripts/Billing.js">{var i;}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      <script type="text/javascript">
          //RMA-10257 : bkuzhanthaim : Show modal dialog
          var IEbrowser = false || !!document.documentMode; //work upto IE6;
          function OnPageUnload() {
              if (!IEbrowser) {
                  var parentWindow = null;
                  if (window.opener.parent.parent.document.getElementById("overlaydiv") != null)
                      window.opener.parent.parent.document.getElementById("overlaydiv").remove();
              }
			   return false;
          }
    </script> 
  </head>
  <body class="10pt">
    <form name="frmData" onpagehide="OnPageUnload();" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <uc2:ErrorControl ID="ErrorControl1" runat="server" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="PremiumItem" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSBillingItem" id="TABSBillingItem">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="BillingItem" id="LINKTABSBillingItem">
            Billing Item Details</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPBillingItem">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSPremiumItem" id="TABSPremiumItem">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="PremiumItem" id="LINKTABSPremiumItem">
            Premium Item Details</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPPremiumItem">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSReconcilationDetails" id="TABSReconcilationDetails">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="ReconcilationDetails" id="LINKTABSReconcilationDetails">
            Reconcilation Details</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPReconcilationDetails">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABBillingItem" id="FORMTABBillingItem">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <div runat="server" class="half" id="div_policyName" xmlns="">
                <asp:label runat="server" class="label" id="lbl_policyName" Text="Policy Name" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="policyName" RMXRef="/Instance/Document/PremiumItem/PolicyName" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
              <div runat="server" class="half" id="div_DateEntered" xmlns="">
                <asp:label runat="server" class="label" id="lbl_DateEntered" Text="Date Entered" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="DateEntered" RMXRef="/Instance/Document/PremiumItem/DateEntered" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_policyNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_policyNumber" Text="Policy Number" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="policyNumber" RMXRef="/Instance/Document/PremiumItem/PolicyNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
              <div runat="server" class="half" id="div_TermNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_TermNumber" Text="Term Number" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="TermNumber" RMXRef="/Instance/Document/PremiumItem/TermNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABPremiumItem" id="FORMTABPremiumItem">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Amount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Amount" Text="Amount" />
                <span class="formw">
                  <mc:CurrencyTextbox runat="server" id="Amount" RMXRef="/Instance/Document/PremiumItem/Amount" RMXType="currency"  rmxforms:as="currency" onchange=";setDataChanged(true);" readonly="true" style="background-color: silver;" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_PremiumItemType" xmlns="">
                <asp:label runat="server" class="label" id="lbl_PremiumItemType" Text="Premium Item Type" />
                <span class="formw">
                  <uc:CodeLookUp runat="server" ID="PremiumItemType" CodeTable="PREMIUM_ITEM_TYPES" ControlName="PremiumItemType" RMXRef="/Instance/Document/PremiumItem/PaymentMethod" RMXType="code" tabindex="1" enabled = "false"   />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_InvoiceNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceNumber" Text="Invoice Number" />
                <span class="formw">
                  <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="InvoiceNumber" RMXRef="/Instance/Document/PremiumItem/InvoiceNumber" RMXType="text" readonly="true" style="&#xA;background-color: silver;&#xA;" onchange="&#xA;AllowIntOnly(this);&#xA;setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_NoticeNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_NoticeNumber" Text="Notice Number" />
                <span class="formw">
                  <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="NoticeNumber" RMXRef="/Instance/Document/PremiumItem/NoticeNumber" RMXType="text" readonly="true" style="&#xA;background-color: silver;&#xA;" onchange="&#xA;AllowIntOnly(this);&#xA;setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_DueDate" xmlns="">
                <asp:label runat="server" class="label" id="lbl_DueDate" Text="Due Date" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="DueDate" RMXRef="/Instance/Document/PremiumItem/DueDate" RMXType="text" readonly="true" style="&#xA;background-color: silver;&#xA;" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="singletopborder" style="display:none;" runat="server" name="FORMTABReconcilationDetails" id="FORMTABReconcilationDetails">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Status" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Status" Text="Status" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="Status" RMXRef="/Instance/Document/PremiumItem/Status" RMXType="text" readonly="true" style="&#xA;background-color: silver;&#xA;" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_DateReconciled" xmlns="">
                <asp:label runat="server" class="label" id="lbl_DateReconciled" Text="Date Reconciled" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="DateReconciled" RMXRef="/Instance/Document/PremiumItem/DateReconciled" RMXType="text" readonly="true" style="&#xA;background-color: silver;&#xA;" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_FinalBatchType" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FinalBatchType" Text="Final Batch Type" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="FinalBatchType" RMXRef="/Instance/Document/PremiumItem/FinalBatchType" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_FinalBatchNo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FinalBatchNo" Text="Final Batch No" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="FinalBatchNo" RMXRef="/Instance/Document/PremiumItem/FinalBatchNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_EntryBatchType" xmlns="">
                <asp:label runat="server" class="label" id="lbl_EntryBatchType" Text="Entry Batch Type" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="EntryBatchType" RMXRef="/Instance/Document/PremiumItem/EntryBatchType" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_EntryBatchNo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_EntryBatchNo" Text="Entry Batch No" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="EntryBatchNo" RMXRef="/Instance/Document/PremiumItem/EntryBatchNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_Cancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="Cancel" RMXRef="" Text="Cancel" width="100" onClientClick="window.close();" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="hdnPolciyId" RMXRef="/Instance/Document/PolicyId" RMXType="hidden" Text="" />



      <asp:TextBox style="display:none" runat="server" name="formname" Text="frmData" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>