﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.XPath;

namespace Riskmaster.UI.Billing
{
    public partial class PrintInvoicesNoticesFrame : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            filename.Text = AppHelper.GetQueryStringValue("FileName");
        }
    }
}
