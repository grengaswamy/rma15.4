﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.ProgressNoteService;
using Riskmaster.ServiceHelpers;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Infragistics.WebUI.UltraWebGrid;
using System.Text;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;
using System.Collections.Generic;
using Riskmaster.Common;
using Riskmaster.Models;


namespace Riskmaster.UI.ProgressNotes
{
    public partial class ProgressNotes : NonFdmFormBase
    {

        private bool _Errorflag = false;//Ashish Ahuja MITS 34537 
        //Start by SHivendu for MITS 18098
        private const string sPageLoad = "PageLoad";
        private const string sFirst = "First";
        private const string sNext = "Next";
        private const string sPrev = "Prev";
        private const string sLast = "Last";
        private const string sSort = "Sort";
        private const string sNoSort = "NoSort";
        private bool bNotesLevelDropdown = false; //mbahl3 mits 30513
        //End by SHivendu for MITS 18098
        //rsolanki2: using stringbuilder
        private StringBuilder g_sbTemp = new StringBuilder();

        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ProgressNotes.aspx"), "ProgressNotesValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "ProgressNotesValidationScripts", sValidationResources, true);
            string sFormName = string.Empty; //mbahl3 mits 34160
            if (AppHelper.GetValue("formsubtitle") != String.Empty)
            {
                this.hdHeader.Text = AppHelper.GetValue("formsubtitle");
                //msampathkuma jira-11851
                if (this.hdHeader.Text.Contains("^@"))
                {
                    this.hdHeader.Text = AppHelper.HTMLCustomEncode(AppHelper.GetValue("formsubtitle"));
                }
                else
                {
                    this.hdHeader.Text = AppHelper.HTMLCustomDecode(AppHelper.GetValue("formsubtitle"));//msampathkuma jira-11170
                }

            }

            //MITS 26441 : Client requests change to custom print process for Large Notes 
            //modified by Raman Bhatia on 12/11/2011
            string control = Request.Form["__EVENTTARGET"];
            if (IsPostBack && !String.IsNullOrEmpty(control) && control == "btnChoices")
            {
                Response.Clear();
                if (rdr1.Checked)
                {
                    if (!_Errorflag)//MITS 34537
                    {
                        //string str = "alert('Your request has been scheduled successfully.');";
                        string str = "alert('" + GetResourceValue("alertMsgResrc", "0") + "');";
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "messageKey", str, true);
                        PrintNotesMain();
                    }
                }
                else if (rdr3.Checked)
                {
                    rdr1.Checked = true;
                    rdr3.Checked = false;
                    OnPageLoad(sPageLoad, "", "");
                }
                //Response.Flush();
                //Response.End();

                div_NotesScreen.Visible = true;
                divChoices.Visible = false;

            }


            //rsolanki2: Enhc notes updates
            if (!ScriptManager1.IsInAsyncPostBack)
            {
                base.Page_Load(sender, e);
                if (!IsPostBack || Request.Form["txtsubmit"] == "SUBMIT")//Parijat:EnhancedBES
                {

                    OnPageLoad(sPageLoad, "", "");//Added by Shivendu fo MITS 18098

                    //UltraWebGrid1.DisplayLayout.AllowColumnMovingDefault = AllowColumnMoving.OnServer;

                    //UltraWebGrid1.DisplayLayout.AllowSortingDefault = AllowSorting.OnClient;
                }
            }

            //Changed for mits 15503 by Gagan : start             
            ApplySecurityPermissions();//Parijat :19934-security Permissions-moved this from Onpageload to pageload for each postback
            //Changed for mits 15503 by Gagan : End 
            //mbahl3 mits 34160
            //   if ((Request.QueryString["notesdropdownchanged"] == null)  || (Request.QueryString["notesdropdownchanged"] != null && string.Equals(AppHelper.HTMLCustomEncode(Request.QueryString["FormName"].ToString()), "false", StringComparison.InvariantCultureIgnoreCase)))
            //   {
            //       if ((Request.QueryString["FormName"] != null))
            //           sFormName = AppHelper.HTMLCustomEncode(Request.QueryString["FormName"].ToString());
            //       // mbahl3 Added for MITS 30513 
            //       if ((Request.QueryString["Tablename"] != null) ||
            //             sFormName == "pimedstaff" || sFormName == "pipatient" || sFormName == "piphysician"
            //           || sFormName == "unit" || sFormName == "salvage"
            //               )
            //       {
            //           ProgressNoteBusinessHelper objHelper = new ProgressNoteBusinessHelper();
            //           ProgressNotesTypeChange objRequest = new ProgressNotesTypeChange();
            //           ProgressNotesTypeChange objResponse = new ProgressNotesTypeChange();
            //           string sTemp = string.Empty;

            //           objRequest.PIEId = "0"; //mits 34160
            //           if (sFormName == "pimedstaff" || sFormName == "pipatient" || sFormName == "piphysician" || string.Equals(AppHelper.GetQueryStringValue("Tablename"), "event", StringComparison.InvariantCultureIgnoreCase))
            //           {
            //               objRequest.RecordType = "EVENT";
            //               objRequest.RecordId = eventid.Value;
            //               if ((Request.QueryString["PIEId"] != null))
            //               {
            //                   objRequest.PIEId = AppHelper.HTMLCustomEncode(Request.QueryString["PIEId"].ToString());
            //                   hdnPIEId.Text = objRequest.PIEId;
            //               } //mits 34160
            //               sTemp = "Event Enhanced Notes ";
            //           }
            //           else if (sFormName == "unit" || sFormName == "salvage" || string.Equals(AppHelper.GetQueryStringValue("Tablename"), "claim", StringComparison.InvariantCultureIgnoreCase))
            //           {
            //               objRequest.RecordType = "CLAIM";
            //               objRequest.RecordId = claimid.Value;

            //                sTemp = "Claim Enhanced Notes ";
            //            }
            //           /*Added by gbindra MITS#34104*/
            //           //else if (string.Equals(AppHelper.GetQueryStringValue("Tablename"), "claim", StringComparison.InvariantCultureIgnoreCase))
            //           //{
            //           //    objRequest.RecordType = "CLAIMANT";
            //           //    objRequest.RecordId = hdClaimantId.Value;

            //           //    sTemp = "Claimant Enhanced Notes ";
            //           //}
            //           /*Added by gbindra MITS#34104 END*/

            //           objResponse = objHelper.GetNotesCaption(objRequest);
            //           this.hdHeader.Text = sTemp + objResponse.CaptionText;

            //       }
            //   }

            if ((Request.QueryString["PIEId"] != null))
            {
                hdnPIEId.Text = AppHelper.HTMLCustomEncode(Request.QueryString["PIEId"].ToString());
            }
            //}         
        }
        // mbahl3 Added for MITS 30513 
        /// <summary>
        /// Populates the attached to drop down list.
        /// </summary>
        private void PopulateNotesLevelDropDown()
        {
            bNotesLevelDropdown = true;
            SelectClaimObject selectClaimObject = this.GetClaimList();
            SelectClaimantObject selectClaimantObject = this.GetClaimantList(); //Added by GBINDRA MITS#34104 WWIG Gap15 02052014

            if (!object.ReferenceEquals(selectClaimObject, null))
            {
                selNotesLevel.Items.Clear();
                this.PopulateNotesLevelDropDown(selectClaimObject); //, eventid.Value, eventnumber.Value, claimid.Value, policyid.Value, policyname.Value, LOB.Value, policynumber.Value);
                if (!object.ReferenceEquals(selectClaimantObject, null))
                {
                    this.PopulateNotesLevelDropDown(selectClaimantObject);
                }
            }
        }

        /// <summary>
        /// Gets the claim list.
        /// </summary>
        /// <returns>Select claim object</returns>
        private SelectClaimObject GetClaimList()
        {
            ProgressNoteBusinessHelper progressNoteBusinessHelper = null;
            SelectClaimObject selectClaimObject = new SelectClaimObject();
            selectClaimObject.EventID = eventid.Value;
            selectClaimObject.ClaimID = claimid.Value;
            selectClaimObject.LOB = LOB.Value;
            selectClaimObject.NewRecord = "false";
            selectClaimObject.SelectEvent = true;
            selectClaimObject.PolicyID = policyid.Value;
            selectClaimObject.NotesLevelDropdown = bNotesLevelDropdown;
            try
            {
                progressNoteBusinessHelper = new ProgressNoteBusinessHelper();
                return progressNoteBusinessHelper.SelectClaim(selectClaimObject);
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                return null;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                return null;
            }
        }
        //Added by GBINDRA MITS#34104 WWIG Gap15 02052014 START
        /// <summary>
        /// Gets the claimant list.
        /// </summary>
        /// <returns>Select claimant object</returns>
        private SelectClaimantObject GetClaimantList()
        {
            ProgressNoteBusinessHelper progressNoteBusinessHelper = null;
            SelectClaimantObject selectClaimantObject = new SelectClaimantObject();
            selectClaimantObject.EventID = eventid.Value;
            selectClaimantObject.ClaimID = claimid.Value;
            selectClaimantObject.LOB = LOB.Value;
            selectClaimantObject.NewRecord = "false";
            selectClaimantObject.SelectEvent = true;
            selectClaimantObject.PolicyID = policyid.Value;
            selectClaimantObject.NotesLevelDropdown = bNotesLevelDropdown;
            try
            {
                progressNoteBusinessHelper = new ProgressNoteBusinessHelper();
                return progressNoteBusinessHelper.SelectClaimant(selectClaimantObject);
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                return null;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                return null;
            }
        }
        //Added by GBINDRA MITS#34104 WWIG Gap15 02052014 END
        // mbahl3 Added for MITS 30513 Ends
        /// <summary>
        /// Enables and disables Page links and shows current page number and total number of
        /// pages
        /// </summary>
        /// <param name="objProgressNote"></param>
        public void EnableDisablePageLinks(ProgressNotesType objProgressNote)
        {
            StringBuilder sPageDetails = new StringBuilder();//Added by Shivendu for MITS 18098
            //Start by Shivendu for MITS 18098
            hdTotalNumberOfPages.Value = objProgressNote.TotalNumberOfPages.ToString();
            hdPageNumber.Value = objProgressNote.PageNumber.ToString();
            //sPageDetails.Append("Page ");
            sPageDetails.Append(GetResourceValue("lblPageDetails1Resrc", "0") + " ");
            sPageDetails.Append(hdPageNumber.Value);
            //sPageDetails.Append(" of ");
            sPageDetails.Append(" " + GetResourceValue("lblPageDetails2Resrc", "0") + " ");
            sPageDetails.Append(hdTotalNumberOfPages.Value);
            lblPageDetails.Text = sPageDetails.ToString();
            lnkFirst.Enabled = true;
            lnkNext.Enabled = true;
            lnkPrev.Enabled = true;
            lnkLast.Enabled = true;


            if (string.Compare(hdTotalNumberOfPages.Value, "1") == 0)
            {
                lnkFirst.Enabled = false;
                lnkNext.Enabled = false;
                lnkPrev.Enabled = false;
                lnkLast.Enabled = false;

            }
            if (string.Compare(hdPageNumber.Value, "1") == 0)
            {
                lnkFirst.Enabled = false;
                lnkPrev.Enabled = false;
            }
            if (string.Compare(hdPageNumber.Value, hdTotalNumberOfPages.Value) == 0)
            {
                lnkLast.Enabled = false;
                lnkNext.Enabled = false;
            }

            //End by Shivendu for MITS 18098

        }
        /// <summary>
        /// Function will be called on Page_Load and postbacks on pagination
        /// </summary>
        public void OnPageLoad(string sCallee, string sortExpression, string direction)
        {
            try//Parijat :Mits 18677 (Exception Handling )
            {
                //Parijat:EnhancedBES
                FillHiddenControls();

                //if (ouraction.Value != "Filter")
                //{
                txtSubmit.Value = "";
                //Changed by Amitosh for mits 23691(05/11/2011)
                //UltraWebGrid1.Width = Unit.Percentage(100);
                UltraWebGrid1.Width = Unit.Percentage(97);

                ProgressNotesType objProgressNote = new ProgressNotesType();

                //14972 :no constructor generated after removing model references and genrating the same thru reference.svcmap
                objProgressNote.objFilter = new Filter();
                //Start by Shivendu for MITS 18098
                switch (sCallee)
                {
                    case sPageLoad:
                        objProgressNote.PageNumber = 1;
                        // atavaragiri MITS 28264//
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri MITS 28264//
                        if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                        {
                            objProgressNote.SortColumn = hdSortColumn.Value;
                            objProgressNote.Direction = hdDirection.Value;
                        }
                        else
                        {
                            objProgressNote.SortColumn = sNoSort;
                        }
                        //rsolanki2: start Enhc notes Ajax updates                        
                        string sPageNumber = AppHelper.GetQueryStringValue("PageNumber");
                        int iPageNumber = 1, iMaxPages = 1;

                        if (!string.IsNullOrEmpty(sPageNumber))
                        {
                            int.TryParse(sPageNumber, out iPageNumber);
                            objProgressNote.PageNumber = iPageNumber;
                            // atavaragiri MITS 28264//
                            txtPageNumber.Text = Convert.ToString(iPageNumber);
                            // atavaragiri MITS 28264//
                        }
                        //rsolanki2: end Enhc notes Ajax updates  
                        break;
                    case sFirst:
                        objProgressNote.PageNumber = 1;
                        // atavaragiri MITS 28264//
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri MITS 28264//
                        if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                        {
                            objProgressNote.SortColumn = hdSortColumn.Value;
                            objProgressNote.Direction = hdDirection.Value;
                        }
                        else
                        {
                            objProgressNote.SortColumn = sNoSort;
                        }
                        break;
                    case sNext:
                        if (Convert.ToInt32(hdPageNumber.Value) < Convert.ToInt32(hdTotalNumberOfPages.Value))
                        {
                            hdPageNumber.Value = Convert.ToString(Convert.ToInt32(hdPageNumber.Value) + 1);
                        }
                        objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);
                        // atavaragiri MITS 28264//
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri MITS 28264//
                        if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                        {
                            objProgressNote.SortColumn = hdSortColumn.Value;
                            objProgressNote.Direction = hdDirection.Value;
                        }
                        else
                        {
                            objProgressNote.SortColumn = sNoSort;
                        }
                        break;
                    case sPrev:
                        if (Convert.ToInt32(hdPageNumber.Value) > 1)
                        {
                            hdPageNumber.Value = Convert.ToString(Convert.ToInt32(hdPageNumber.Value) - 1);
                        }
                        objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);
                        // atavaragiri MITS 28264//
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri MITS 28264//
                        if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                        {
                            objProgressNote.SortColumn = hdSortColumn.Value;
                            objProgressNote.Direction = hdDirection.Value;
                        }
                        else
                        {
                            objProgressNote.SortColumn = sNoSort;
                        }
                        break;
                    case sLast:
                        hdPageNumber.Value = hdTotalNumberOfPages.Value;
                        objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);
                        // atavaragiri MITS 28264//
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri MITS 28264//
                        if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                        {
                            objProgressNote.SortColumn = hdSortColumn.Value;
                            objProgressNote.Direction = hdDirection.Value;
                        }
                        else
                        {
                            objProgressNote.SortColumn = sNoSort;
                        }
                        break;
                    case sSort:
                        objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);
                        // atavaragiri MITS 28264//
                        txtPageNumber.Text = Convert.ToString(objProgressNote.PageNumber);
                        // atavaragiri MITS 28264//
                        objProgressNote.SortColumn = sortExpression;
                        objProgressNote.Direction = direction;
                        break;
                    default:
                        break;

                }

                //End by Shivendu for MITS 18098
                if (ouraction.Value == "Filter")
                    FilterSettings(ref objProgressNote);
                else
                    NonFilterSettings();
                LoadPage_Explicit(ref objProgressNote);
                //MITS 15992 and MITS 16014
                // SortGridView("DateEntered"," DESC") should not be called when we have set sorting order in utilities and also when we are doing Advance search.

                //rsolanki2 : removing this entry as sorting is now being taken care pf at sql query level
                if (objProgressNote.PrintOrder1 == "0" && objProgressNote.PrintOrder2 == "0" && objProgressNote.PrintOrder3 == "0" && ouraction.Value != "Filter" && string.Compare(sCallee, sPageLoad) == 0)
                    SortGridData("DateEntered", " DESC");//13550

                //}
                //else
                //{
                //    txtSubmit.Value == "";
                //    ProgressNotesType objProgressNote = new ProgressNotesType();
                //    FilterSettings(ref objProgressNote);
                //    LoadPage_Explicit(ref objProgressNote);
                //}
                //changes done for 12334
                freezetext.Value = objProgressNote.FreezeText.ToString();
                //username.Value = objProgressNote.UserName.ToString();
                //changes done for 12334
                //Changed for mits 14626 by Gagan : Start
                showDateStamp.Value = objProgressNote.ShowDateStamp.ToString();
                //Changed for mits 14626 by Gagan : End
                //Added by Amitosh for mits 23691 (05/11/2011)
                if (this.FindControl("NoteType") != null)
                {
                    ((System.Web.UI.WebControls.TextBox)((this.FindControl("NoteType")).FindControl("codelookup"))).Attributes["style"] = "width:210px;";
                }
                EnableDisablePageLinks(objProgressNote);//Added by Shivendu for MITS 18098

            }
            catch (FaultException<RMException> ee)
            {
                tdNoNotes.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;


                //Added for MITS 16779 by Gagan : start
                //If View Permission error occurs dont show the entire screen
                //if (ee.Message.Contains("No Permission to view Progress Notes"))
                if (ee.Message.Contains(GetResourceValue("htmlMsg2Resrc", "0")))
                {

                    StringWriter sw = new StringWriter();
                    Html32TextWriter oWriter = new Html32TextWriter(sw);
                    oWriter.RenderBeginTag("html");
                    oWriter.AddAttribute("runat", "server");
                    oWriter.RenderBeginTag("head");
                    oWriter.AddAttribute("rel", "stylesheet");
                    oWriter.AddAttribute("href", "../../App_Themes/RMX_Default/rmnet.css");
                    oWriter.AddAttribute("type", "text/css");
                    oWriter.RenderBeginTag("link");
                    oWriter.RenderEndTag();
                    oWriter.RenderEndTag();
                    oWriter.AddAttribute("onload", "if(parent.MDISetUnDirty != null) { parent.MDISetUnDirty(0); }"); // This way we tell MDI an error occurred while saving or deleting
                    oWriter.RenderBeginTag("body");


                    string errorhandle = @"<font class='warntext'>" + GetResourceValue("htmlMsg1Resrc", "0") + "</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                                  "<tr>" +
                                    "<td valign='top'>" +
                                        "<img src='" + AppHelper.GetApplicationRootPath() + "/Images/error-large.gif'/>" +
                                    "</td>" +
                                    "<td valign='top' style='padding-left: 1em'>" +
                                      "<ul><li class='warntext'>" + GetResourceValue("htmlMsg2Resrc", "0") + "</li></ul></td></tr></table>";
                    oWriter.Write(errorhandle);

                    oWriter.RenderEndTag();
                    oWriter.RenderEndTag();

                    Response.Write(sw.ToString());
                    Response.End();
                }

                /*Control ctrlTemp = null;
                ctrlTemp = this.FindControl("div_NotesScreen");
                if (ctrlTemp != null)
                    ctrlTemp.Visible = false;*/
                //Added for MITS 16779 by Gagan : end

            }
            catch (Exception ee)
            {
                tdNoNotes.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }//End Parijat :Mits 18677 (Exception Handling )
        }

        //Changed for mits 15503 by Gagan : start

        void ApplySecurityPermissions()//Parijat :19934-security Permissions
        {
            //williams-neha goel:MITS 21704--start:added or condition to all if conditions for policy enhanced notes
            if (Convert.ToBoolean(ViewState["bCreatePermission"]) || (Convert.ToBoolean(ViewState["bPolicyTrackingCreatePermission"])))//Parijat :19934-security Permissions
                btnCreateNote.Enabled = true;
            else
                btnCreateNote.Enabled = false;

            if ((Convert.ToBoolean(ViewState["bDeletePermission"]) || (Convert.ToBoolean(ViewState["bPolicyTrackingDeletePermission"]))) && btnDeleteNotes.Enabled == true)//Parijat :19934-security Permissions
                btnDeleteNotes.Enabled = true;
            else
                btnDeleteNotes.Enabled = false;


            if ((Convert.ToBoolean(ViewState["bEditPermission"]) || (Convert.ToBoolean(ViewState["bPolicyTrackingEditPermission"]))) && btnEditNote.Enabled == true)//Parijat :19934-security Permissions
                btnEditNote.Enabled = true;
            else
                btnEditNote.Enabled = false;


            if (Convert.ToBoolean(ViewState["bPrintPermission"]) || (Convert.ToBoolean(ViewState["bPolicyTrackingPrintPermission"])))//Parijat :19934-security Permissions
            {
                btnPrintNotes.Enabled = true;
                //btnPrintSelectedNotes.Enabled = true; //Commented Rakhi R6-Print Note By Note Type
                //tanwar2 - Print HTML Notes
                btnPrintHtmlNotes.Enabled = true;
            }
            else
            {
                btnPrintNotes.Enabled = false;
                //btnPrintSelectedNotes.Enabled = false; //Commented Rakhi R6-Print Note By Note Type
                //tanwar2 - Print HTML Notes
                btnPrintHtmlNotes.Enabled = false;
            }

            if (Convert.ToBoolean(ViewState["bViewAllNotesPermission"]) || (Convert.ToBoolean(ViewState["bPolicyTrackingViewAllNotesPermission"])))//Parijat :19934-security Permissions
            {
                btnViewNotes.Enabled = true;
            }
            else
            {
                btnViewNotes.Enabled = false;
            }
            //Start rsushilaggar MITS 21119 06/18/2010
            if (Convert.ToBoolean(ViewState["bTemplatesPermission"]))
            {
                btnTemplates.Enabled = true;
            }
            else
            {
                btnTemplates.Enabled = false;
            }
            //End rsushilaggar MITS 21119 06/18/2010
            // mits 34158
            if ((!Convert.ToBoolean(ViewState["bViewAllNotesPermission"])) && (!Convert.ToBoolean(ViewState["bCreatePermission"])) && (!Convert.ToBoolean(ViewState["bDeletePermission"])) && (!Convert.ToBoolean(ViewState["bEditPermission"])) && (!Convert.ToBoolean(ViewState["bPrintPermission"])))
            {
                selNotesLevel.Enabled = false;
            }
            // mits 34158


        }

        //Changed for mits 15503 by Gagan : end

        void FillHiddenControls()
        {
            if (Request.Form["activatefilter"] != null)
                activatefilter.Value = Request.Form["activatefilter"];
            if (Request.Form["activityfromdate"] != null)
                activityfromdate.Value = Request.Form["activityfromdate"];
            if (Request.Form["activitytodate"] != null)
                activitytodate.Value = Request.Form["activitytodate"];
            if (Request.Form["claimidlist"] != null)
                claimidlist.Value = Request.Form["claimidlist"];
            if (Request.Form["enteredbylist"] != null)
                enteredbylist.Value = Request.Form["enteredbylist"];
            if (Request.Form["txtnotestextcontains"] != null)
                txtnotestextcontains.Value = Request.Form["txtnotestextcontains"];
            if (Request.Form["notetypelist"] != null)
                notetypelist.Value = Request.Form["notetypelist"];
            if (Request.Form["sSortBy"] != null)
                sSortBy.Value = Request.Form["sSortBy"];
            if (Request.Form["usertypelist"] != null)
                usertypelist.Value = Request.Form["usertypelist"];
            if (Request.Form["ouraction"] != null)
                ouraction.Value = Request.Form["ouraction"];
            if (Request.Form["applysort"] != null)
                applysort.Value = Request.Form["applysort"];
            if (Request.Form["txtSubmit"] != null)
                txtSubmit.Value = Request.Form["txtSubmit"];
            //zmohammad MITS 30218
            if (Request.Form["subjectlist"] != null)
                subjectlist.Value = Request.Form["subjectlist"];
            // Added by akaushik5 for MITS 30789 Starts
            if (!object.ReferenceEquals(Request.Form["sOrderBy"], null))
            {
                sOrderBy.Value = Request.Form["sOrderBy"];
            }
            // Added by akaushik5 for MITS 30789 Ends 

            /*added by gbindra MITS#34104 GAP15 WWIG START*/
            if (!string.IsNullOrEmpty(Request.QueryString["ClaimantId"]))
                hdClaimantId.Value = Request.QueryString["ClaimantId"];
            else
                hdClaimantId.Value = "-1";
            /*added by gbindra MITS#34104 GAP15 WWIG END*/

        }
        void NonFilterSettings()
        {
            btnBackToNote.Visible = false;
            btnAdvSearch.Visible = true;
            btnCreateNote.Visible = true;
        }
        void FilterSettings(ref ProgressNotesType objProgressNote)
        {
            btnAdvSearch.Visible = false;
            btnBackToNote.Visible = true;
            btnCreateNote.Visible = false;
            bool bSuccess = false;// Added by gbindra WWIG GAP15 MITS#34104
            //Page.Header.Title = "Advanced Search Results";
            Page.Header.Title = GetResourceValue("pageHdrResrc", "0");
            //Added by Amitosh for mits 23473 (05/11/2011)
            ProgressNoteBusinessHelper pn = new ProgressNoteBusinessHelper();
            ProgressNoteSettings objProgressNoteSettings = new ProgressNoteSettings();
            //end Amitosh
            if (activatefilter.Value != null || activatefilter.Value != "")
                objProgressNote.ActivateFilter = Convert.ToBoolean(activatefilter.Value);
            if (activityfromdate.Value != null || activityfromdate.Value != "")
                objProgressNote.objFilter.ActivityFromDate = activityfromdate.Value;
            if (activitytodate.Value != null || activitytodate.Value != "")
                objProgressNote.objFilter.ActivityToDate = activitytodate.Value;
            if (claimidlist.Value != null || claimidlist.Value != "")
                objProgressNote.objFilter.ClaimIDList = claimidlist.Value;
            /*added by Gbindra MITS#34104 WWIG GAP15 02122014 START*/
            if (hdClaimantId.Value != null || hdClaimantId.Value != "")
                objProgressNote.objFilter.ClaimantIDList = Conversion.CastToType<int>(hdClaimantId.Value, out bSuccess);
            /*added by Gbindra MITS#34104 WWIG GAP15 02122014 END*/
            if (enteredbylist.Value != null || enteredbylist.Value != "")
                objProgressNote.objFilter.EnteredByList = enteredbylist.Value;
            //zmohammad MITS 30218
            if (subjectlist.Value != null || subjectlist.Value != "")
                objProgressNote.objFilter.SubjectList = subjectlist.Value;
            if (txtnotestextcontains.Value != null || txtnotestextcontains.Value != "")
                objProgressNote.objFilter.NotesTextContains = txtnotestextcontains.Value;
            if (notetypelist.Value != null || notetypelist.Value != "")
                objProgressNote.objFilter.NoteTypeList = notetypelist.Value;

            if (usertypelist.Value != null || usertypelist.Value != "")
                objProgressNote.objFilter.UserTypeList = usertypelist.Value;
            if (sSortBy.Value != null || sSortBy.Value != "")
            {
                objProgressNote.objFilter.SortBy = sSortBy.Value;
                objProgressNoteSettings.sortby = sSortBy.Value;//Added by Amitosh for mits 23473 (05/11/2011)
                // Added by akaushik5 for MITS 30789 Starts
                if (!object.ReferenceEquals(sOrderBy, null))
                {
                    objProgressNote.objFilter.OrderBy = sOrderBy.Value;
                    objProgressNoteSettings.OrderBy = sOrderBy.Value;
                }
                else
                {
                    objProgressNote.objFilter.OrderBy = string.Empty;
                    objProgressNoteSettings.OrderBy = string.Empty;
                }
                // Added by akaushik5 for MITS 30789 Ends
                objProgressNoteSettings = pn.SaveNotesSettings(objProgressNoteSettings);//Added by Amitosh for mits 23473 (05/11/2011)
            }
        }
        public void LoadPage_Explicit(ref ProgressNotesType objProgressNote)
        {

            int iOutResult = 0;
            //williams:MITS 21704-neha goel
            string sOutResult = string.Empty;
            //williams:MITS 21704-neha goel
            bool bResult = false;
            bool bSuccess = false; //added by gbindra MITS#34104 WWIG GAP15
            if (Request.QueryString["EventID"] != null)
            {

                //objProgressNote.EventID = Convert.ToInt32(Request.QueryString["EventID"].ToString()); //Changed for Mits 18843
                bResult = Int32.TryParse(Request.QueryString["EventID"].ToString(), out iOutResult);
                objProgressNote.EventID = iOutResult;
            }
            //williams-neha goel:MITS 21704---start
            if (Request.QueryString["PolicyId"] != null)
            {
                bResult = Int32.TryParse(Request.QueryString["PolicyId"].ToString(), out iOutResult);
                // bResult = Int32.TryParse(AppHelper.HTMLCustomEncode(Request.QueryString["PolicyId"].ToString()), out iOutResult);
                objProgressNote.PolicyID = iOutResult;
                //END :pen testing changes by atavaragiri :mits 27832
            }
            if (Request.QueryString["PolicyName"] != null)
            {
                //pen testing changes by atavaragiri :mits 27832
                objProgressNote.PolicyName = Request.QueryString["PolicyName"].ToString();
                //  objProgressNote.PolicyName = AppHelper.HTMLCustomEncode( Request.QueryString["PolicyName"].ToString());
                //END :pen testing changes by atavaragiri :mits 27832
            }
            if (Request.QueryString["PolicyNumber"] != null)
            {    //pen testing changes by atavaragiri :mits 27832
                objProgressNote.PolicyNumber = Request.QueryString["PolicyNumber"].ToString();
                //   objProgressNote.PolicyNumber = AppHelper.HTMLCustomEncode(Request.QueryString["PolicyNumber"].ToString());
                //END :pen testing changes by atavaragiri :mits 27832
            }
            //  RMA-17480, ijain4 - Fetching Parent row id and form name via query string            
            if (Request.QueryString["PIParent"] != null)
            {
                hdPIParent.Value = Request.QueryString["PIParent"].ToString();
            }
            // RMA-17480, ijain4 End
            //williams-neha goel:MITS 21704--end
            if (Request.QueryString["ClaimID"] != null)
            {
                //objProgressNote.ClaimID = Convert.ToInt32(Request.QueryString["ClaimID"].ToString()); //Changed for Mits 18843

                //pen testing changes by atavaragiri :mits 27832
                // bResult = Int32.TryParse(Request.QueryString["ClaimID"].ToString(), out iOutResult); 
                bResult = Int32.TryParse(AppHelper.HTMLCustomEncode(Request.QueryString["ClaimID"].ToString()), out iOutResult);
                //END :pen testing changes by atavaragiri :mits 27832
                objProgressNote.ClaimID = iOutResult;
            }
            //pmittal5 Mits 21514 07/19/10 - Pass Claims LOB in ProgressnotesType object to check Enhanced Note permissions separately for different LOBs.
            if (Request.QueryString["LOB"] != null)
            {   //pen testing changes by atavaragiri :mits 27832
                //objProgressNote.LOB = Request.QueryString["LOB"].ToString();
                objProgressNote.LOB = AppHelper.HTMLCustomEncode(Request.QueryString["LOB"].ToString());
                //END :pen testing changes by atavaragiri :mits 27832
            }
            if (Request.QueryString["FormName"] != null)
            {    //pen testing changes by atavaragiri :mits 27832
                //objProgressNote.FormName = Request.QueryString["FormName"].ToString();
                objProgressNote.FormName = AppHelper.HTMLCustomEncode(Request.QueryString["FormName"].ToString());
                //END :pen testing changes by atavaragiri :mits 27832
                //MGaba2:MITS 28450: Setting case mgr row id in case of children of Case Management
                if (objProgressNote.FormName == "CaseMgrNotes" || objProgressNote.FormName == "casemanagement" || objProgressNote.FormName == "CmXAccommodation" || objProgressNote.FormName == "CmXVocrehab" || objProgressNote.FormName == "CmXCmgrHist")
                {//ClaimId contains the value of case mgr row id
                    if (Request.QueryString["CaseMgrRowId"] != null)
                    {
                        bResult = Int32.TryParse(AppHelper.HTMLCustomEncode(Request.QueryString["CaseMgrRowId"].ToString()), out iOutResult);
                        objProgressNote.CaseMgtRowID = iOutResult;
                    }

                    objProgressNote.CaseMgrParent = AppHelper.GetQueryStringValue("CaseMgrParent");
                }
                if (objProgressNote.FormName == "demandoffer")
                {
                    if (Request.QueryString["DemanOfferParentId"] != null)
                    {
                        bResult = Int32.TryParse(AppHelper.HTMLCustomEncode(Request.QueryString["DemanOfferParentId"].ToString()), out iOutResult);
                        objProgressNote.DemanOfferParentId = iOutResult;
                    }

                    objProgressNote.DemanOfferParent = AppHelper.GetQueryStringValue("DemanOfferParent");
                }
            }
            /*ADDED BY GBINDRA MITS#34104 02112014 WWIG GAP15 START*/
            if (Request.QueryString["ClaimantId"] != null)
            {
                objProgressNote.ClaimantId = Conversion.CastToType<int>(Request.QueryString["ClaimantId"].ToString(), out bSuccess);
            }
            else
            {
                objProgressNote.ClaimantId = -1;
            }
            /*ADDED BY GBINDRA MITS#34104 02112014 WWIG GAP15 END*/

            ProgressNoteBusinessHelper pn = null;

            try
            {
                pn = new ProgressNoteBusinessHelper();
                //objProgressNote = pn.Load(objProgressNote);
                objProgressNote = pn.LoadPartial(objProgressNote);
                username.Value = objProgressNote.UserName.ToString();
                if (objProgressNote.ClaimID.ToString() != null && objProgressNote.ClaimID.ToString() != "")
                    claimid.Value = objProgressNote.ClaimID.ToString();
                else
                    claimid.Value = Request.QueryString["ClaimID"];

                if (objProgressNote.EventID.ToString() != null && objProgressNote.EventID.ToString() != "")
                    eventid.Value = objProgressNote.EventID.ToString();
                else
                    eventid.Value = Request.QueryString["EventID"];

                //pmittal5 Mits 21514
                if (objProgressNote.LOB != null && objProgressNote.LOB != "")
                    LOB.Value = objProgressNote.LOB.ToString();
                else
                    LOB.Value = Request.QueryString["LOB"];

                if (objProgressNote.EventNumber != null && objProgressNote.EventNumber != "")
                    eventnumber.Value = objProgressNote.EventNumber;
                else
                    eventnumber.Value = Request.QueryString["EventNumber"];
                //williams-enhanced notes--start:MITS 21704
                //if (objProgressNote.PolicyID.ToString() != null && objProgressNote.PolicyID.ToString() != "")
                if (!string.IsNullOrEmpty(objProgressNote.PolicyID.ToString()) && objProgressNote.PolicyID != 0)
                    policyid.Value = objProgressNote.PolicyID.ToString();
                else
                    policyid.Value = Request.QueryString["PolicyId"];

                if (!string.IsNullOrEmpty(objProgressNote.PolicyName))
                {
                    policyname.Value = objProgressNote.PolicyName;
                }
                else
                    policyname.Value = Request.QueryString["PolicyName"];

                if (!string.IsNullOrEmpty(objProgressNote.PolicyNumber))
                {
                    policynumber.Value = objProgressNote.PolicyNumber;
                }
                else
                    policynumber.Value = Request.QueryString["PolicyNumber"];

                //gbindra jira # rma-185
                if (objProgressNote.ClaimantId > 0)
                {
                    hdClaimantId.Value = objProgressNote.ClaimantId.ToString();
                }
                else
                {
                    if (Request.QueryString["ClaimantId"] != null)
                    {
                        hdClaimantId.Value = Request.QueryString["ClaimantId"];
                    }
                    else
                    {
                        hdClaimantId.Value = "-1";
                    }
                }
                //gbindra jira # rma-185

                //if (objProgressNote.objProgressNoteList.Length > 0)
                //{
                //    if (!string.IsNullOrEmpty(objProgressNote.objProgressNoteList[0].CodeId))
                //        NoteParentType.Value = objProgressNote.objProgressNoteList[0].CodeId;
                //}
                if (!string.IsNullOrEmpty(objProgressNote.CodeId))
                    NoteParentType.Value = objProgressNote.CodeId;

                hdnViewPolicyNotesInClaim.Value = objProgressNote.NotesPolicyClaimView.ToString();
                //williams enhanced notes---end:MITS 21704
                formname.Value = AppHelper.GetQueryStringValue("FormName");
                SysFormName.Value = AppHelper.GetQueryStringValue("FormName");  //csingh7 :MITS 19333
                if (objProgressNote.Claimant != null && objProgressNote.Claimant != "")//12821 for showing claimant in the title
                    claimant.Value = objProgressNote.Claimant.ToString();
                //Parijat : start Post Editable Enhanced Notes
                if (objProgressNote.EnhancedTimeLimit.ToString() != "")
                    hdnEnhancedTimeLimit.Value = objProgressNote.EnhancedTimeLimit.ToString();
                else
                    hdnEnhancedTimeLimit.Value = "0";
                hdnUserEditingRights.Value = objProgressNote.UserEditingRights.ToString();
                //Parijat : end Post Editable Enhanced Notes
                //Parijat :19934-security Permissions- Start
                //williams-neha goel:MITS 21704--start:Poicy enhnaced notes permissions
                //start vsharma65
                if (!string.IsNullOrEmpty(objProgressNote.PolicyID.ToString()) && objProgressNote.PolicyID != 0)
                {
                    ViewState["bPolicyTrackingCreatePermission"] = objProgressNote.bPolicyTrackingCreatePermission.ToString();
                    ViewState["bPolicyTrackingDeletePermission"] = objProgressNote.bPolicyTrackingDeletePermission.ToString();
                    ViewState["bPolicyTrackingEditPermission"] = objProgressNote.bPolicyTrackingEditPermission.ToString();
                    ViewState["bPolicyTrackingPrintPermission"] = objProgressNote.bPolicyTrackingPrintPermission.ToString();
                    ViewState["bPolicyTrackingViewAllNotesPermission"] = objProgressNote.bPolicyTrackingViewAllNotesPermission.ToString();
                    ViewState["bTemplatesPermission"] = objProgressNote.bTemplatesPermission.ToString();
                }
                else
                {
                    ViewState["bCreatePermission"] = objProgressNote.bCreatePermission.ToString();
                    ViewState["bDeletePermission"] = objProgressNote.bDeletePermission.ToString();
                    ViewState["bEditPermission"] = objProgressNote.bEditPermission.ToString();
                    ViewState["bPrintPermission"] = objProgressNote.bPrintPermission.ToString();
                    ViewState["bViewAllNotesPermission"] = objProgressNote.bViewAllNotesPermission.ToString();
                    //rsushilaggar MITS 21119 06/18/2010
                    ViewState["bTemplatesPermission"] = objProgressNote.bTemplatesPermission.ToString();
                    //Added by Amitosh for mits 23473 (05/11/2011)
                    if (objProgressNote.SortPref != null)
                    {
                        hdSortPref.Value = objProgressNote.SortPref;
                    }
                    // Added by akaushik5 for MITS 30789 Starts
                    if (!object.ReferenceEquals(objProgressNote.OrderPref, null))
                    {
                        hdOrderPref.Value = objProgressNote.OrderPref;
                    }
                    // Added by akaushik5 for MITS 30789 Ends
                }
                DataViewBind(objProgressNote.objProgressNoteList.ToList());
                //Ashish Ahujua - Mits 34383
                SortGridData("DateEntered", " DESC");
                // mbahl3 Added for MITS 30513 Starts
                this.PopulateNotesLevelDropDown();
                // mbahl3 Added for MITS 30513 Ends
            }//Parijat :Mits 18677 (Exception Handling )
            catch (Exception ee)
            {
                throw ee;
            }
            //End :Parijat :Mits 18677 (Exception Handling )
            //rsolanki2 : Enhc notes ajax updates 
            int index = 0; Boolean bTryParseTemp = false;
            if (AppHelper.GetQueryStringValue("hdCurrentNode") != null)
                hdCurrentNode.Value = AppHelper.GetQueryStringValue("hdCurrentNode");
            if (string.IsNullOrEmpty(hdCurrentNode.Value))
            {
                hdCurrentNode.Value = "0";
            }
            Int32.TryParse(hdCurrentNode.Value, out index);
            if (index > UltraWebGrid1.Rows.Count - 1)
            {
                index = 0;
            }

            //TemplatedColumn col;
            //CellItem item;

            if (UltraWebGrid1.Rows.HasVisibleRows)
            {
                UltraWebGrid1.Rows[index].Activate();
                UltraWebGrid1.Rows[index].Selected = true;
                UltraGridRow row = UltraWebGrid1.Rows[index];
                //col = (TemplatedColumn)row.Cells[0].Column;
                //item = (CellItem)col.CellItems[index];
                //item = (CellItem)col.CellItems[UltraWebGrid1.Rows[index].Index];
                //if (item.FindControl("RowSelector") != null) //Added for Mits:18267
                //{
                //((RadioButton)item.FindControl("RowSelector")).Checked = true;
                //ChangeOnRadioClick(UltraWebGrid1.Rows[index]);

                row.Style.BackColor = System.Drawing.Color.FromName("#DDDDDD");
                UltraWebGrid1.DisplayLayout.ActiveRow = UltraWebGrid1.Rows[index];
                ChangeOnRadioClick(row);
                hdCurrentNode.Value = index.ToString();
                //hdCurrentNode.Value = row.Index.ToString();


                // rsolanki2: load note details for initial load. 
                // right now, this is a separate call retriveing the notes details.
                // eventaully we will need to combine this with the rest of the call to reduce the number of calls to the business layers
                ProgressNoteBusinessHelper objPNBH = new ProgressNoteBusinessHelper();
                GetNoteDetailsObject objGNDO = new GetNoteDetailsObject();

                //objGNDO.ClaimProgressNoteId = UltraWebGrid1.Rows[row.Index].Cells.FromKey("ClaimProgressNoteId").Text;
                //claimprogressnoteid.Value = row.Cells.FromKey("ClaimProgressNoteId").Text;
                //objGNDO.ClaimProgressNoteId = claimprogressnoteid.Value;  


                objGNDO.ClaimProgressNoteId = row.Cells.FromKey("ClaimProgressNoteId").Text;
                objGNDO = objPNBH.GetNoteDetails(objGNDO);
                NoteMemoDiv.InnerHtml = objGNDO.HTML;
                //Ashish Ahuja Mits 33124
                Subject.Value = objGNDO.Subject;
            }

        }
        public void DataViewBind(System.Collections.Generic.List<ProgressNote> objPrgressNoteList)
        {
            if (objPrgressNoteList.Count > 0)
            {
                ViewState["Datagrid_Arraylist"] = objPrgressNoteList;
                tdNoNotes.Visible = false;
            }
            else
            {
                ViewState["Datagrid_Arraylist"] = "";
                tdNoNotes.Visible = true;
            }
            MoveHeaders();
            UltraWebGrid1.DataSource = objPrgressNoteList;
            UltraWebGrid1.DataBind();
            //UltraWebGrid1.Bands[0].Columns[0].Header.ClickAction = HeaderClickAction.Select;
            //rsolanki2: Enhc notes updates: removing the radio click postback 
            //SetGrdRadiosOnClick();
            //first radio click on load and also the event 
            if (!(UltraWebGrid1.Rows.Count > 0))
            {
                /**((RadioButton)GridView1.Rows[0].FindControl("RowSelector")).Checked = true;
                    ChangeOnRadioClick(GridView1.Rows[0]);**/


                // rsolanki2 : commenting below as we have a handler for this elsewhere

                //TemplatedColumn col = (TemplatedColumn)UltraWebGrid1.Rows[0].Cells[0].Column;
                //CellItem item = (CellItem)col.CellItems[UltraWebGrid1.Rows[0].Index];
                //if (item.FindControl("RowSelector") != null) //Added for Mits:18267
                //{
                //    ((RadioButton)item.FindControl("RowSelector")).Checked = true;
                //    ChangeOnRadioClick(UltraWebGrid1.Rows[0]);
                //    //Start by Shivendu for MITS 18420
                //    UltraGridRow row = UltraWebGrid1.Rows[0];
                //    row.Style.BackColor = System.Drawing.Color.FromName("#DDDDDD");
                //    UltraWebGrid1.DisplayLayout.ActiveRow = UltraWebGrid1.Rows[0];
                //    //End by Shivendu for MITS 18420
                //}
                //}
                //else
                //{
                //NoteMemoDiv.InnerHtml = "";//change for cleaning the div tag for note memo in case the last note has been deleted
                //DateEntered.InnerText = "";
                //DateCreated.InnerText = "";
                //TimeCreated.InnerText = "";
                //EnteredByName.InnerText = "";
                //NoteTypeCode.InnerText = "";
                //notetypecodeid.Value = "";
                //UserTypeCode.InnerText = "";
                //AttachedTo.InnerText = "";
                //claimprogressnoteid.Value = "";
                NoteMemoDiv.InnerHtml = "";//change for cleaning the div tag for note memo in case the last note has been deleted
                DateEntered.Value = "";
                DateCreated.Value = "";
                TimeCreated.Value = "";
                EnteredByName.Value = "";
                NoteTypeCode.Value = "";
                notetypecodeid.Value = "";
                UserTypeCode.Value = "";
                AttachedTo.Value = "";
                claimprogressnoteid.Value = "";
                //Ashish Ahuja Mits 33124
                Subject.Value = "";
            }



        }
        protected void UltraWebGrid1_ColumnMove(object sender, ColumnEventArgs e)
        {
            ArrayList arrlstHeaderOrder = new ArrayList();
            ProgressNoteBusinessHelper pn = null;
            ProgressNoteSettings objProgressNoteSettings = new ProgressNoteSettings();
            //Added for Mits:18297-Start
            ColumnMoveEventArgs moveargs = e as ColumnMoveEventArgs;
            //if (moveargs != null)
            //{
            //    if (moveargs.FromIndex == 0)
            //        e.Cancel = true;
            //    else if (moveargs.ToIndex == 0)
            //        e.Cancel = true;//Added for Mits:18297-End
            //    else 
            //    {
            //Deb MITS 30185
            ProgressNoteSettings objProgSettings = new ProgressNoteSettings();
            if (ViewState["ColHeaders"] != null)
            {
                objProgSettings = (ProgressNoteSettings)ViewState["ColHeaders"];
            }
            Dictionary<string, int> sRemvoeColPos = new Dictionary<string, int>();
            if (ViewState["RemovedColumns"] != null)
            {
                sRemvoeColPos = (Dictionary<string, int>)ViewState["RemovedColumns"];
            }
            foreach (UltraGridColumn col in UltraWebGrid1.Columns)
            {
                if (col.Hidden == false && col.BaseColumnName != "")
                {
                    arrlstHeaderOrder.Add(col.Key);
                }
            }
            foreach (KeyValuePair<string, int> sKeyValue in sRemvoeColPos)
            {
                if (!arrlstHeaderOrder.Contains(sKeyValue.Key))
                {
                    arrlstHeaderOrder.Insert(sKeyValue.Value - 1, sKeyValue.Key);
                }
            }
            objProgressNoteSettings.Header1 = arrlstHeaderOrder[0].ToString();
            objProgressNoteSettings.Header2 = arrlstHeaderOrder[1].ToString();
            objProgressNoteSettings.Header3 = arrlstHeaderOrder[2].ToString();
            objProgressNoteSettings.Header4 = arrlstHeaderOrder[3].ToString();
            objProgressNoteSettings.Header5 = arrlstHeaderOrder[4].ToString();
            //Added by Amitosh for mits 23691(05/11/2011)
            objProgressNoteSettings.Header6 = arrlstHeaderOrder[5].ToString();
            objProgressNoteSettings.Header7 = arrlstHeaderOrder[6].ToString();
            objProgressNoteSettings.Header8 = arrlstHeaderOrder[7].ToString();
            objProgressNoteSettings.Header9 = arrlstHeaderOrder[8].ToString();
            //end Amitosh
            //Deb MITS 30185
            try
            {
                pn = new ProgressNoteBusinessHelper();
                objProgressNoteSettings = pn.SaveNotesSettings(objProgressNoteSettings);
                UltraWebGrid1.DataSource = ViewState["Datagrid_Arraylist"];
                UltraWebGrid1.DataBind();
                ViewState["ColHeaders"] = objProgressNoteSettings;
                //SetGrdRadiosOnClick();
                //first radio click on load and also the event 
                if (UltraWebGrid1.Rows.Count > 0)
                {
                    TemplatedColumn col = (TemplatedColumn)UltraWebGrid1.Rows[0].Cells[0].Column;
                    CellItem item = (CellItem)col.CellItems[UltraWebGrid1.Rows[0].Index];
                    //if (item.FindControl("RowSelector") != null) //Added for Mits:18267
                    //{
                    //    ((RadioButton)item.FindControl("RowSelector")).Checked = true;
                    ChangeOnRadioClick(UltraWebGrid1.Rows[0]);
                    //Start by Shivendu for MITS 18420
                    UltraGridRow row = UltraWebGrid1.Rows[0];
                    row.Style.BackColor = System.Drawing.Color.FromName("#DDDDDD");
                    UltraWebGrid1.DisplayLayout.ActiveRow = UltraWebGrid1.Rows[0];
                    //End by Shivendu for MITS 18420
                    //}

                }
                //Ashish Ahuja Mits 33124
                SortGridView(m_strSortExp.ToString(), " DESC");
                if (m_strSortExp.ToString() != "")
                {
                    if (m_SortDirection == SortDirection.Ascending)
                        SortGridView(m_strSortExp.ToString(), " ASC");
                    else
                        SortGridView(m_strSortExp.ToString(), " DESC");
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                return;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }

        public void MoveHeaders()
        {
            ProgressNoteBusinessHelper pn = null;
            ProgressNoteSettings objProgressNoteSettings = new ProgressNoteSettings();

            try
            {
                if (ViewState["ColHeaders"] == null)
                {
                    pn = new ProgressNoteBusinessHelper();
                    objProgressNoteSettings = pn.GetNotesHeaderOrder(objProgressNoteSettings);
                    ViewState["ColHeaders"] = objProgressNoteSettings;
                }
                else
                {
                    objProgressNoteSettings = (ProgressNoteSettings)ViewState["ColHeaders"];

                }
                //Deb MITS 30185
                Dictionary<string, int> sRemvoeColPos = new Dictionary<string, int>();
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header1))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header1 != "DateCreated" && objProgressNoteSettings.Header1 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header1));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header1, 1);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header1).Move(1);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header1).Header.Caption = objProgressNoteSettings.Header1Text;
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header2))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header2 != "DateCreated" && objProgressNoteSettings.Header2 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header2));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header2, 2);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header2).Move(2);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header2).Header.Caption = objProgressNoteSettings.Header2Text;
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header3))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header3 != "DateCreated" && objProgressNoteSettings.Header3 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header3));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header3, 3);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header3).Move(3);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header3).Header.Caption = objProgressNoteSettings.Header3Text;
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header4))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header4 != "DateCreated" && objProgressNoteSettings.Header4 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header4));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header4, 4);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header4).Move(4);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header4).Header.Caption = objProgressNoteSettings.Header4Text;
                    //Syadav55 Jira 11785 start- fix related to overlapping of enhance note free text column note text
                    UltraWebGrid1.DisplayLayout.RowStyleDefault.TextOverflow = TextOverflow.Ellipsis;
                    //Syadav55 Jira 11785 ends
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header5))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header5 != "DateCreated" && objProgressNoteSettings.Header5 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header5));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header5, 5);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header5).Move(5);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header5).Header.Caption = objProgressNoteSettings.Header5Text;
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header6))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header6 != "DateCreated" && objProgressNoteSettings.Header6 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header6));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header6, 6);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header6).Move(6);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header6).Header.Caption = objProgressNoteSettings.Header6Text;
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header7))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header7 != "DateCreated" && objProgressNoteSettings.Header7 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header7));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header7, 7);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header7).Move(7);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header7).Header.Caption = objProgressNoteSettings.Header7Text;
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header8))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header8 != "DateCreated" && objProgressNoteSettings.Header8 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header8));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header8, 8);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header8).Move(8);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header8).Header.Caption = objProgressNoteSettings.Header8Text;
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains(objProgressNoteSettings.Header9))
                {
                    //Deb: MITS 30866 
                    if (objProgressNoteSettings.Header9 != "DateCreated" && objProgressNoteSettings.Header9 != "TimeCreated")
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header9));
                    }
                    sRemvoeColPos.Add(objProgressNoteSettings.Header9, 9);
                }
                else
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header9).Move(9);
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey(objProgressNoteSettings.Header9).Header.Caption = objProgressNoteSettings.Header9Text;
                }
                ViewState["RemovedColumns"] = sRemvoeColPos;
                //Deb MITS 30185
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                return;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return;
            }
        }

        public void SetGrdRadiosOnClick()
        {
            int i;
            RadioButton B;

            for (i = 0; i < UltraWebGrid1.Rows.Count; i++)
            {
                /**B = (RadioButton)GridView1.Rows[i].FindControl("RowSelector");
                B.Attributes.Add("OnClick", "SelectOnlyOneRadio(" + B.ClientID + ", " + "'UltraWebGrid1'" + ")");**/

                TemplatedColumn col = (TemplatedColumn)UltraWebGrid1.Rows[i].Cells[0].Column;
                CellItem item = (CellItem)col.CellItems[UltraWebGrid1.Rows[i].Index];
                B = ((RadioButton)item.FindControl("RowSelector"));
                if (B != null)//Added for Mits:18267
                    B.Attributes.Add("OnClick", "SelectOnlyOneRadio(" + B.ClientID + ", " + "'UltraWebGrid1'" + ")");

            }
        }
        public void ChangeOnRadioClick(UltraGridRow row)
        {
            //Label lbActDate = (Label)row.FindControl("lblActivityDate");//,"lblAttachedTo","lblNoteType""lblNoteText","lblEnteredBy"
            // //changed by Nitin for Mits 13518 on 11/05/2009
            // DateEntered.InnerText = lbActDate.Text;
            // DateCreated.InnerText = Convert.ToDateTime(((Label)row.FindControl("lblDateCreated")).Text).ToString("d");//Parijat:changes regarding mm/dd/yyyy format asked by Hope Lockhart 
            // TimeCreated.InnerText = ((Label)row.FindControl("lblTimeCreated")).Text;
            // EnteredByName.InnerText = ((Label)row.FindControl("lblEnteredBy")).Text;
            // NoteTypeCode.InnerText = ((Label)row.FindControl("lblNoteType")).Text;
            // notetypecodeid.Value = ((Label)row.FindControl("lblNoteTypeCodeId")).Text;
            // UserTypeCode.InnerText = ((Label)row.FindControl("lblUserTypeCode")).Text;
            // AttachedTo.InnerText = ((Label)row.FindControl("lblAttachedTo")).Text;
            // //NoteMemoDiv.InnerHtml = ((Label)row.FindControl("lblNoteMemoCareTech")).Text;
            // NoteMemoDiv.InnerHtml = ((Label)row.FindControl("lblNoteText")).Text;
            // claimprogressnoteid.Value = ((Label)row.FindControl("lblClaimProgressNoteId")).Text;
            if (row.Cells.FromKey("Activity Date") != null)
                //DateEntered.InnerText = Convert.ToDateTime((row.Cells.FromKey("Activity Date")).Text).ToString("d");
                DateEntered.Value = AppHelper.GetDate((row.Cells.FromKey("Activity Date")).Text);
            if (row.Cells.FromKey("DateCreated") != null)
                //DateCreated.InnerText = Convert.ToDateTime((row.Cells.FromKey("DateCreated")).Text).ToString("d");//Parijat:changes regarding mm/dd/yyyy format asked by Hope Lockhart 
                DateCreated.Value = AppHelper.GetDate((row.Cells.FromKey("DateCreated")).Text);//Parijat:changes regarding mm/dd/yyyy format asked by Hope Lockhart 
            if (row.Cells.FromKey("TimeCreated") != null)
                TimeCreated.Value = AppHelper.GetTime(row.Cells.FromKey("TimeCreated").Text);
            if (row.Cells.FromKey("Entered By") != null)
                EnteredByName.Value = row.Cells.FromKey("Entered By").Text;
            if (row.Cells.FromKey("Note Type") != null)
                NoteTypeCode.Value = row.Cells.FromKey("Note Type").Text;
            if (row.Cells.FromKey("NoteTypeCodeId") != null)
                notetypecodeid.Value = row.Cells.FromKey("NoteTypeCodeId").Text;
            if (row.Cells.FromKey("UserTypeCode") != null)
                UserTypeCode.Value = row.Cells.FromKey("UserTypeCode").Text;
            if (row.Cells.FromKey("Attached To") != null)
                AttachedTo.Value = row.Cells.FromKey("Attached To").Text;
            if (row.Cells.FromKey("NoteText") != null)
                NoteMemoDiv.InnerHtml = row.Cells.FromKey("NoteText").Text;
            if (row.Cells.FromKey("ClaimProgressNoteId") != null)
                claimprogressnoteid.Value = row.Cells.FromKey("ClaimProgressNoteId").Text;
            //Added Rakhi for R6-Progress Notes Templates
            if (row.Cells.FromKey("TemplateId") != null)
                TemplateId.Value = row.Cells.FromKey("TemplateId").Text;
            if (row.Cells.FromKey("TemplateName") != null)
                TemplateName.Value = row.Cells.FromKey("TemplateName").Text;
            //Added Rakhi for R6-Progress Notes Templates
            //Ashish Ahuja Mits 33124
            //zmohammad MITS 30218
            //if (row.Cells.FromKey("Subject") != null)
            //    Subject.Value = row.Cells.FromKey("Subject").Text;
            //Deb : MITS 30866
            ProgressNoteSettings objProgressNoteSettings = (ProgressNoteSettings)ViewState["ColHeaders"];
            if (objProgressNoteSettings != null)
            {
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains("DateCreated"))
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey("DateCreated"));
                }
                if (!objProgressNoteSettings.HeaderKeyShowList.Contains("TimeCreated"))
                {
                    UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey("TimeCreated"));
                }
            }
            //Deb : MITS 30866
            //Parijat :Start Post Editable Enhanced Notes
            //DateTime DateTimeCreated = Convert.ToDateTime(DateCreated.InnerText+ " " + TimeCreated.InnerText);
            DateTime DateTimeCreated = Convert.ToDateTime(DateCreated.Value + " " + TimeCreated.Value, System.Globalization.CultureInfo.CreateSpecificCulture(AppHelper.GetCulture()).DateTimeFormat);
            TimeSpan ts = DateTime.Now - DateTimeCreated;


            //williams -neha goel:MITS 21704--start:added policy permission condn
            //williams-nehagoel:MITS 21704 :disable policy enhanced notes when viewed from claim---start
            string sFormName = AppHelper.GetQueryStringValue("FormName");
            string sPiParent = AppHelper.GetQueryStringValue("PIParent");
            //if (AttachedTo.InnerText.Contains("POLICY") && (sFormName != "policy" || sFormName != "policyenhal" || sFormName != "policyenhgl" || sFormName != "policyenhpc" || sFormName != "policyenhwc"))
            if ((AttachedTo.Value.Contains("POLICY") && (sFormName != "policy" && sFormName != "policyenhal" && sFormName != "policyenhgl" && sFormName != "policyenhpc" && sFormName != "policyenhwc"
                && (sFormName != "pimedstaff") && (sFormName != "piemployee") && (sFormName != "pidriver") && (sFormName != "pipatient") && (sFormName != "piphysician") && (sFormName != "piwitness") && (sFormName != "piother")))// jira 18944
                || (!string.IsNullOrEmpty(sPiParent) && sPiParent.ToUpper() == "CLAIM" && AttachedTo.Value.Contains("POLICY"))) // JIRA 19340 
            {
                 hdnDeleteButtonState.Value = "disabled";
                 ScriptManager.RegisterStartupScript(this.upProgressNotes, this.GetType(), "DisableDelete", "DisableDeleteButton();", true);
                 hdnEditButtonState.Value = "disabled";
                 ScriptManager.RegisterStartupScript(this.upProgressNotes, this.GetType(), "DisableEdit", "DisableEditButton();", true);
            }
            else
            {
                if (Convert.ToBoolean(ViewState["bEditPermission"]) || Convert.ToBoolean(ViewState["bPolicyTrackingEditPermission"]))//Parijat :19934 So that other settings don't override the security management setting
                {
                    //if ((hdnUserEditingRights.Value == "true" || hdnUserEditingRights.Value == "True") ? (username.Value == EnteredByName.InnerText) : true)
                    if ((hdnUserEditingRights.Value == "true" || hdnUserEditingRights.Value == "True") ? (username.Value == EnteredByName.Value) : true)
                    {
                        if ((Convert.ToDouble(hdnEnhancedTimeLimit.Value) != 0) && (ts.TotalHours >= Convert.ToDouble(hdnEnhancedTimeLimit.Value)))//Parijat: 0 considered as infinite
                        {
                            btnEditNote.Enabled = false;
                        }
                        else
                        {
                            btnEditNote.Enabled = true;
                        }
                    }
                    else
                    {
                        btnEditNote.Enabled = false;
                    }
                    //Parijat :End Post Editable Enhanced Notes 
                }
                //williams-nehagoel:MITS 21704 :disable policy enhanced notes when viewed from claim---start
                if ((Convert.ToBoolean(ViewState["bDeletePermission"])) || (Convert.ToBoolean(ViewState["bPolicyTrackingDeletePermission"])))
                {
                    btnDeleteNotes.Enabled = true;
                }
                else
                {
                    btnDeleteNotes.Enabled = false;
                }
                //willimas-neha goel:end
            }
            //williams-nehagoel:MITS 21704 :disable policy enhanced notes when viewed from claim---end
            //row.Cells[0].Focus();//Added by Shivendu for MITS 17443
        }
        //public Decimal calculate(string PresentTime,string DateTimeCreated)
        //{
        //    DateTime date1 = Convert.ToDateTime(DateTimeCreated);
        //    DateTime date2 = Convert.ToDateTime(PresentTime);
        //    var sec = date2.getTime() - date1.getTime();

        //    var second = 1000, minute = 60 * second, hour = 60 * minute, day = 24 * hour;


        //    var days = Math.Floor(sec / day);
        //    sec -= days * day;
        //    Decimal hours = Math.Floor(sec / hour);
        //    sec -= hours * hour;
        //    var minutes = Math.Floor(sec / minute);
        //    sec -= minutes * minute;
        //    var seconds = Math.Floor(sec / second);
        //    var Total = days + hours + minutes + seconds 
        //    if(days >= 1)
        //    {
        //        days = days * 24;
        //        hours = days + hours;
        //    }
        //    return hours;
        //}
        protected void lnkFirst_Click(object sender, EventArgs e)
        {
            OnPageLoad(sFirst, "", "");//Added by Shivendu fo MITS 18098
        }
        protected void lnkNext_Click(object sender, EventArgs e)
        {
            OnPageLoad(sNext, "", "");//Added by Shivendu fo MITS 18098
        }
        protected void lnkPrev_Click(object sender, EventArgs e)
        {
            OnPageLoad(sPrev, "", "");//Added by Shivendu fo MITS 18098
        }
        protected void lnkLast_Click(object sender, EventArgs e)
        {
            OnPageLoad(sLast, "", "");//Added by Shivendu fo MITS 18098
        }

        protected void Grid_radioOncheckedChanged(object sender, EventArgs e)
        {
            #region test
            ////Page.RegisterClientScriptBlock("ee", "<Script>alert(\"" + sender.ToString() + "</Script>");
            //if (IsPostBack)
            //{
            #region Handling radio selection at server side
            //RadioButton rb = new RadioButton();
            //rb = (RadioButton)sender;
            //string sRbText = rb.ClientID;

            //foreach (GridViewRow r in GridView1.Rows)
            //{
            //    rb = (RadioButton)r.FindControl("RowSelector");
            //    rb.Checked = false;
            //    if (sRbText == rb.ClientID)
            //    {
            //        rb.Checked = true;
            //        //txtSiraNo.Text = rb.Text.Trim();
            //        // if you want to get a property of the selected id
            //    }
            //}
            #endregion

            //    Response.Write(Session["ht"].ToString());
            //    int i = Convert.ToInt32(Session["ht"]);
            //    i = ++i;
            //    Session["ht"] = i;
            ////}
            #endregion
            /**RadioButton rdButton = (RadioButton)sender;
            GridViewRow row = (GridViewRow)rdButton.NamingContainer;
            row.BackColor = System.Drawing.Color.FromName("#DDDDDD");
            ChangeOnRadioClick(row);**/

            RadioButton rdButton = (RadioButton)sender;
            rdButton.Focus();//19582-Parijat
            CellItem item = (CellItem)rdButton.NamingContainer;
            UltraGridRow row = item.Cell.Row;
            row.Style.BackColor = System.Drawing.Color.FromName("#DDDDDD");
            ChangeOnRadioClick(row);
            hdCurrentNode.Value = row.Index.ToString();

        }
        protected void MoveToAllNotes(object sender, EventArgs e)
        {
            activatefilter.Value = "false";
            activityfromdate.Value = "";
            activitytodate.Value = "";
            claimidlist.Value = "";
            enteredbylist.Value = "";
            txtnotestextcontains.Value = "";
            notetypelist.Value = "";
            sSortBy.Value = "";
            //if (Token.Value != null || sSortBy.Value != "")
            //objProgressNote.objFilter.Token
            usertypelist.Value = "";
            ouraction.Value = "";
            btnBackToNote.Visible = false;
            btnAdvSearch.Visible = true;
            btnCreateNote.Visible = true;
            ProgressNotesType objProgressNote = new ProgressNotesType();
            //14972 :no constructor generated after removing model references and genrating the same thru reference.svcmap
            objProgressNote.objFilter = new Filter();
            //Start by Shivendu for MITS 18098
            objProgressNote.PageNumber = 1;
            objProgressNote.SortColumn = sNoSort;
            //End by Shivendu for MITS 18098
            //Parijat :Mits 18677 (Exception Handling )
            try
            {
                LoadPage_Explicit(ref objProgressNote);
            }
            catch (FaultException<RMException> ee)
            {
                tdNoNotes.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                tdNoNotes.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            //End:Parijat :Mits 18677 (Exception Handling )
            //Page.Header.Title = "Enhanced Notes";
            Page.Header.Title = GetResourceValue("pageHdrTitleResrc", "0");
            EnableDisablePageLinks(objProgressNote);//Added by Shivendu for MITS 18098
        }
        protected void DeleteNote(object sender, EventArgs e)
        {
            ProgressNotesType objProgressNote = new ProgressNotesType();
            //14972 :no constructor generated after removing model references and genrating the same thru reference.svcmap
            objProgressNote.objFilter = new Filter();
            bool bSuccess = false; //added by gbindra MITS#34104 WWIG GAP15

            ProgressNoteBusinessHelper pn = null;

            try
            {
                pn = new ProgressNoteBusinessHelper();
                objProgressNote.EventID = Convert.ToInt32(eventid.Value);

                //objProgressNote.ClaimID = Convert.ToInt32(claimid.Value);

                //rsolanki2 : mits 22133: advanced search edit issue
                int iIndex = 0;
                System.Collections.Generic.List<ProgressNote> ObjProgList;
                if (ViewState["Datagrid_Arraylist"] != null)
                {
                    ObjProgList = ((System.Collections.Generic.List<ProgressNote>)ViewState["Datagrid_Arraylist"]);

                    if (int.TryParse(hdCurrentNode.Value, out iIndex))
                    {
                        if (ObjProgList != null && iIndex <= ObjProgList.Count)
                        {
                            objProgressNote.ClaimID = Convert.ToInt32(ObjProgList[iIndex].ClaimID);
                        }
                    }
                }
                else
                {
                    objProgressNote.ClaimID = Convert.ToInt32(claimid.Value);
                }

                objProgressNote.LOB = LOB.Value; //pmittal5 Mits 21514
                //williams---neha goel:MITS 21704
                if (!string.IsNullOrEmpty(policyid.Value))
                {
                    objProgressNote.PolicyID = Convert.ToInt32(policyid.Value);
                }
                //williams---neha goel:MITS 21704

                objProgressNote.ClaimantId = Conversion.CastToType<int>(hdClaimantId.Value, out bSuccess);//added by GBINDRA MITS#34104 02132014 WWIG GAP15 START
                objProgressNote.ClaimProgressNoteId = claimprogressnoteid.Value;
                //Start by Shivendu for MITS 18098

                if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                {
                    objProgressNote.SortColumn = hdSortColumn.Value;
                    objProgressNote.Direction = hdDirection.Value;
                }
                else
                {
                    objProgressNote.SortColumn = sNoSort;
                }
                if (!string.IsNullOrEmpty(hdPageNumber.Value))
                {
                    objProgressNote.PageNumber = Convert.ToInt32(hdPageNumber.Value);

                }
                else
                {
                    objProgressNote.PageNumber = 1;
                }

                //End by Shivendu for MITS 18098

                if (ouraction.Value == "Filter")
                    FilterSettings(ref objProgressNote);
                objProgressNote = pn.DeleteNote(objProgressNote);
                DataViewBind(objProgressNote.objProgressNoteList.ToList());
                EnableDisablePageLinks(objProgressNote);//Added by Shivendu for MITS 18098
                //Ashish Ahuja Mits 33124
                SortGridData("DateEntered", " DESC");
                //rsolanki2 : enhc notes ajax updates 
                hdCurrentNode.Value = "0";
                if (UltraWebGrid1.Rows.HasVisibleRows)
                {
                    UltraWebGrid1.Rows[0].Activate();
                    UltraWebGrid1.Rows[0].Selected = true;
                    UltraGridRow row = UltraWebGrid1.Rows[0];
                    //col = (TemplatedColumn)row.Cells[0].Column;
                    //item = (CellItem)col.CellItems[index];
                    //item = (CellItem)col.CellItems[UltraWebGrid1.Rows[index].Index];
                    //if (item.FindControl("RowSelector") != null) //Added for Mits:18267
                    //{
                    //((RadioButton)item.FindControl("RowSelector")).Checked = true;
                    //ChangeOnRadioClick(UltraWebGrid1.Rows[index]);

                    row.Style.BackColor = System.Drawing.Color.FromName("#DDDDDD");
                    UltraWebGrid1.DisplayLayout.ActiveRow = UltraWebGrid1.Rows[0];
                    ChangeOnRadioClick(row);
                    //hdCurrentNode.Value = index.ToString();
                    //hdCurrentNode.Value = row.Index.ToString();


                    // rsolanki2: load note details for initial load. 
                    // right now, this is a separate call retriveing the notes details.
                    // eventaully we will need to combine this with the rest of the call to reduce the number of calls to the business layers
                    ProgressNoteBusinessHelper objPNBH = new ProgressNoteBusinessHelper();
                    GetNoteDetailsObject objGNDO = new GetNoteDetailsObject();

                    //objGNDO.ClaimProgressNoteId = UltraWebGrid1.Rows[row.Index].Cells.FromKey("ClaimProgressNoteId").Text;
                    //claimprogressnoteid.Value = row.Cells.FromKey("ClaimProgressNoteId").Text;
                    //objGNDO.ClaimProgressNoteId = claimprogressnoteid.Value;                        

                    objGNDO.ClaimProgressNoteId = row.Cells.FromKey("ClaimProgressNoteId").Text;


                    objGNDO = objPNBH.GetNoteDetails(objGNDO);
                    NoteMemoDiv.InnerHtml = objGNDO.HTML;
                }
                //rsolanki2 : enhc notes ajax updates 

            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        //parijat:EnhancedMDI
        protected void CreateNote(object sender, EventArgs e)
        {
            //Changed for mits 14626 by Gagan : Start 
            //MGaba2:MITS 17027: Apostrophe was creating javascript errors
            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&SysFormName=" + formname.Value + "&EventNumber=" + eventnumber.Value + "&NoteType=" + NoteTypeCode.InnerText + "&NoteTypecodeid=" + notetypecodeid.Value + "&ClaimProgressNoteId=" + claimprogressnoteid.Value + "&ActivityDate=" + DateEntered.InnerText + "&CommentsFlag=true&NewRecord=" + hdNewRecord.Value + "&FreezeText=" + freezetext.Value + "&ShowDateStamp=" + showDateStamp.Value + "&UserName=" + username.Value);

            //Raman 11/09/2009 : MITS 17786
            //Spell check on editor screen fails to load correctly is server.tranfer is used 

            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&SysFormName=" + formname.Value + "&EventNumber=" + eventnumber.Value + "&NoteType=" + (Server.UrlEncode(NoteTypeCode.InnerText)).Replace("'","~~*~~")  + "&NoteTypecodeid=" + notetypecodeid.Value + "&ClaimProgressNoteId=" + claimprogressnoteid.Value + "&ActivityDate=" + DateEntered.InnerText + "&CommentsFlag=true&NewRecord=" + hdNewRecord.Value + "&FreezeText=" + freezetext.Value + "&ShowDateStamp=" + showDateStamp.Value + "&UserName=" + username.Value);




            string SubjectModUrl = ""; //zmohammad MITS 33446
            g_sbTemp.Length = 0;

            //rsolanki2 : mits 22133: advanced search edit issue
            int iIndex = 0;
            string sDateEntered = string.Empty;
            System.Collections.Generic.List<ProgressNote> ObjProgList;
            //= new System.Collections.Generic.List<Riskmaster.UI.ProgressNoteService.ProgressNote>();
            g_sbTemp.Append("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=");
            g_sbTemp.Append(eventid.Value);
            g_sbTemp.Append("&ClaimID=");

            if (UltraWebGrid1.Rows.Count > 0 && ViewState["Datagrid_Arraylist"] != null)
            {
                ObjProgList = ((System.Collections.Generic.List<ProgressNote>)ViewState["Datagrid_Arraylist"]);

                if (int.TryParse(hdCurrentNode.Value, out iIndex))
                {
                    if (ObjProgList != null && iIndex <= ObjProgList.Count)
                    {
                        //Added by amitosh for MITS 27562 
                        if (string.Equals(ObjProgList[iIndex].ClaimID, "0", StringComparison.InvariantCultureIgnoreCase) && formname.Value.Contains("claim"))
                            g_sbTemp.Append(claimid.Value);
                        else //End Amitosh
                            g_sbTemp.Append(ObjProgList[iIndex].ClaimID);
                        sDateEntered = ObjProgList[iIndex].DateEntered;      //Changes by Gaurav on 01/17/2013. MITS 30962.
                    }
                }
            }
            else
            {
                g_sbTemp.Append(claimid.Value);
            }

            g_sbTemp.Append("&LOB=");   //pmittal5 Mits 21514
            g_sbTemp.Append(LOB.Value);
            //Aman MITS 27310--Start
            g_sbTemp.Append("&CreateNotePermission=");
            g_sbTemp.Append(Convert.ToBoolean(ViewState["bCreatePermission"]));
            g_sbTemp.Append("&PolicyTrackingCreatePermission=");
            g_sbTemp.Append(Convert.ToBoolean(ViewState["bPolicyTrackingCreatePermission"]));
            //Aman MITS 27310--End
            g_sbTemp.Append("&SysFormName=");
            g_sbTemp.Append(formname.Value);
            g_sbTemp.Append("&EventNumber=");
            g_sbTemp.Append(eventnumber.Value);
            //williams-neha goel:MITS 21704
            g_sbTemp.Append("&PolicyId=");
            g_sbTemp.Append(policyid.Value);
            g_sbTemp.Append("&PolicyName=");
            g_sbTemp.Append(policyname.Value);
            g_sbTemp.Append("&PolicyNumber=");
            g_sbTemp.Append(policynumber.Value);
            g_sbTemp.Append("&CodeId=");
            g_sbTemp.Append(NoteParentType.Value);
            //williams-neha goel:end:MITS 21704
            g_sbTemp.Append("&NoteType=");
            //g_sbTemp.Append(  (Server.UrlEncode(NoteTypeCode.InnerText)).Replace("'", "~~*~~")  );
            g_sbTemp.Append((Server.UrlEncode(NoteTypeCode.Value)).Replace("'", "~~*~~"));
            g_sbTemp.Append("&NoteTypecodeid=");
            g_sbTemp.Append(notetypecodeid.Value);
            g_sbTemp.Append("&ClaimProgressNoteId=");
            g_sbTemp.Append(claimprogressnoteid.Value);
            //zmohammad MITS 30218 
            //zmohammad MITS 33446
            SubjectModUrl = HttpUtility.UrlEncode(Subject.Value);
            g_sbTemp.Append("&Subject=");
            g_sbTemp.Append(SubjectModUrl);
            g_sbTemp.Append("&ActivityDate=");
            //g_sbTemp.Append(DateEntered.InnerText);
            g_sbTemp.Append(sDateEntered);            //Changes by Gaurav on 01/17/2013. MITS 30962.
            g_sbTemp.Append("&CommentsFlag=true&NewRecord=");
            g_sbTemp.Append(hdNewRecord.Value);
            g_sbTemp.Append("&FreezeText=");
            g_sbTemp.Append(freezetext.Value);
            g_sbTemp.Append("&ShowDateStamp=");
            g_sbTemp.Append(showDateStamp.Value);
            g_sbTemp.Append("&UserName=");
            g_sbTemp.Append(username.Value);
            g_sbTemp.Append("&TemplateId=");
            g_sbTemp.Append(TemplateId.Value);
            g_sbTemp.Append("&TemplateName=");
            g_sbTemp.Append((Server.UrlEncode(TemplateName.Value)).Replace("'", "~~*~~"));
            g_sbTemp.Append("&hdCurrentNode=");
            g_sbTemp.Append(hdCurrentNode.Value);//rsolanki2 : Enhc notes ajax updates 
            g_sbTemp.Append("&PageNumber=");
            g_sbTemp.Append(hdPageNumber.Value);
            g_sbTemp.Append("&ClaimantId=");//Added by Gbindra MITS#34104 WWIG GAP 15
            g_sbTemp.Append(hdClaimantId.Value);//Added by Gbindra MITS#34104 WWIG GAP 15
            g_sbTemp.Append("&formsubtitle=");

            // RMA-17480, Enhanced Notes handling when PI parent is not event /claim
            
            // RMA-17480 ends
            //g_sbTemp.Append(this.hdHeader.Text);
            g_sbTemp.Append(this.hdHeader.Text.Replace("&", "^@").Replace("#", "*@")); //hlv MITS 28588 11/12/12 asharma326 MITS 34294
            //dbisht6 RMA-17480
            if (!(string.IsNullOrEmpty(hdPIParent.Value)))
            {
                g_sbTemp.Append("&PIParent=");
                g_sbTemp.Append(hdPIParent.Value);
            }
            //dbisht6 end

            Response.Redirect(g_sbTemp.ToString(), true);

            //Response.Redirect("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + eventid.Value + "&ClaimID="
            //    + claimid.Value + "&SysFormName=" + formname.Value + "&EventNumber=" + eventnumber.Value
            //    + "&NoteType=" + (Server.UrlEncode(NoteTypeCode.InnerText)).Replace("'", "~~*~~") + "&NoteTypecodeid="
            //    + notetypecodeid.Value + "&ClaimProgressNoteId=" + claimprogressnoteid.Value
            //    + "&ActivityDate=" + DateEntered.InnerText + "&CommentsFlag=true&NewRecord="
            //    + hdNewRecord.Value + "&FreezeText=" + freezetext.Value + "&ShowDateStamp=" + showDateStamp.Value + "&UserName="
            //    + username.Value + "&TemplateId=" + TemplateId.Value + "&TemplateName="
            //    + (Server.UrlEncode(TemplateName.Value)).Replace("'", "~~*~~")
            //    + "&hdCurrentNode=" + hdCurrentNode.Value //rsolanki2 : Enhc notes ajax updates 
            //    + "&PageNumber=" + hdPageNumber.Value
            //    , true);

            //Changed for mits 14626 by Gagan : End
        }

        protected void PrintNotes(object sender, EventArgs e)
        {
            PrintNotesMain();
        }

        private void PrintNotesMain()
        {
            bool blnSuccess = false;
            ProgressNotesType objProgressNote = new ProgressNotesType();
            //14972 :no constructor generated after removing model references and genrating the same thru reference.svcmap
            objProgressNote.objFilter = new Filter();
            // Added by Amitosh for Mits 19871 (04/05/2011)
            FillHiddenControls();
            if (ouraction.Value == "Filter")
                FilterSettings(ref objProgressNote);
            else
                NonFilterSettings();
            //end Amitosh
            ProgressNoteBusinessHelper pn = null;
            string sPrintOption = string.Empty;
            try
            {
                pn = new ProgressNoteBusinessHelper();
                objProgressNote.EventID = Convert.ToInt32(eventid.Value);
                objProgressNote.ClaimID = Convert.ToInt32(claimid.Value);
                // Pshekhawat 6/25/2014 - JIRA RMA-199
                objProgressNote.ClaimantId = Conversion.CastToType<int>(hdClaimantId.Value, out blnSuccess);
                // Added by Amitosh for Mits 19871 (04/05/2011)
                objProgressNote.objFilter.ActivityFromDate = (activityfromdate.Value).ToString();
                objProgressNote.objFilter.ActivityToDate = (activitytodate.Value).ToString();
                objProgressNote.objFilter.ClaimIDList = (claimidlist.Value).ToString();
                objProgressNote.objFilter.UserTypeList = (usertypelist.Value).ToString();
                objProgressNote.objFilter.NoteTypeList = (notetypelist.Value).ToString();
                objProgressNote.objFilter.EnteredByList = (enteredbylist.Value).ToString();
                objProgressNote.objFilter.ActivityFromDate = (activityfromdate.Value).ToString();
                objProgressNote.objFilter.ActivityToDate = (activitytodate.Value).ToString();
                objProgressNote.objFilter.NotesTextContains = (txtnotestextcontains.Value).ToString();
                objProgressNote.objFilter.SortBy = (sSortBy.Value).ToString();

                // end Amitosh
                objProgressNote.LOB = LOB.Value;  //pmittal5 Mits 21514
                //williams-neha goel:MITS 21704--start:added for policy enhanced notes
                if (!string.IsNullOrEmpty(policyid.Value))
                {
                    objProgressNote.PolicyID = Convert.ToInt32(policyid.Value);
                }
                //williams-neha goel:MITS 21704--start:added for policy enhanced notes
                //Commented and Added Rakhi for R6-Print Note by Note Type
                //if (((Button)sender).ID == "btnPrintSelectedNotes")
                //    objProgressNote.ClaimProgressNoteId = claimprogressnoteid.Value;
                //else
                //    objProgressNote.ClaimProgressNoteId = "";

                sPrintOption = hdnPrintOption.Value;
                switch (sPrintOption)
                {
                    case "1":
                        objProgressNote.ClaimProgressNoteId = claimprogressnoteid.Value;
                        objProgressNote.NoteTypeCodeId = "";
                        break;
                    case "2":
                        objProgressNote.NoteTypeCodeId = ((TextBox)NoteType.FindControl("codelookup_cid")).Text;
                        objProgressNote.ClaimProgressNoteId = "";
                        break;
                    default:
                        objProgressNote.ClaimProgressNoteId = "";
                        objProgressNote.NoteTypeCodeId = "";
                        break;
                }
                //MITS 26441 : Client requests change to custom print process for Large Notes 
                //modified by Raman Bhatia on 12/11/2011


                if (divChoices.Visible && rdr1.Checked)
                {
                    objProgressNote.iGenerateReportThresholdExceeded = 2;
                }
                else if (divChoices.Visible && rdr3.Checked)
                {
                    objProgressNote.iGenerateReportThresholdExceeded = 0;
                }




                //Commented and Added Rakhi for R6-Print Note by Note Type
                objProgressNote = pn.PrintProgressNotes(objProgressNote);
                if (!String.IsNullOrEmpty(objProgressNote.ProgressNoteReportPdf))
                {
                    string pdf = objProgressNote.ProgressNoteReportPdf;
                    #region Writing content in a file
                    //byte[] outArray = new byte[60000];
                    //outArray = Convert.FromBase64String(pdf);
                    //System.IO.FileStream sw;
                    //sw = new System.IO.FileStream(@"c:\temp\EnhancedNotesReport.pdf", System.IO.FileMode.CreateNew);

                    ////for (int i = 0; i < outArray.Length; i++)
                    ////    sw.WriteByte(outArray[i]);
                    ////sw.Close(); 
                    //BinaryWriter writer = new BinaryWriter(sw);
                    //writer.Write(outArray);
                    //sw.Flush();
                    //writer.Close();

                    //sw.Close();
                    #endregion
                    //asingh263 MITS 35962 starts
                    try
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Charset = "";
                        Response.AppendHeader("Content-Encoding", "none;");
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=EnhancedNotesReport.pdf");
                        Response.BinaryWrite(Convert.FromBase64String(pdf));
                        Response.Flush();
                        Response.Close();
                        //Response.End(); rsharma220 MITS 37161
                        Response.SuppressContent = true;
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    catch
                    {
                        //no need to catch
                    }
                    //asingh263 MITS 35962 ends
                }
                else
                {
                    //XElement oReturn = XElement.Parse(sResponse);
                    //  string sMsgStatusCode = oReturn.XPathSelectElement("//ExtendedStatusCd").Value;
                    //  //if (sMsgStatusCode.Contains"PrintPDF.ValidateCriteria.ThresholdExceeded", StringComparison.OrdinalIgnoreCase))
                    //  if (sMsgStatusCode.Contains("InvalidCriteria") || sMsgStatusCode.Contains("ThresholdExceeded"))
                    //  {

                    //MITS 26763 : Client requests change to custom print process for Large Notes 
                    //modified by Raman Bhatia on 12/06/2011

                    div_NotesScreen.Visible = false;
                    divChoices.Visible = true;

                    /*
                    Response.Clear();
                    Response.Write("Enhc Notes threshold limit was exceeded."
                        + "Enhc Notes printing has been scheduled.<br/>"
                        + "Once generated, it will be accessible from the Attachments list.<br/><br/>"
                        + "<input type='button' value='OK' style='align:center;' onclick='javascript:window.close()'/>"
                        );

                    Response.Flush();
                    Response.End();
                    */
                }

                //Response.BinaryWrite(document.Body);
                //Response.WriteFile(Server.MapPath(@"~/EnhancedNotesReport.pdf"));
                ////Response.TransmitFile(@"c:\temp\EnhancedNotesReport.pdf");
                //Response.SuppressContent = false;

            }
            catch (FaultException<RMException> ee)
            {
                //Ashish Ahuja MITS 34537 
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                _Errorflag = true;
                //Raman: NEVER WRITE BUSINESS LOGIC BY THROWING EXCEPTIONS!!

                //rsolanki2: ExecSummaryScheduler enhacement
                //XElement oReturntemp = XElement.Parse(ee.Message);
                //if (oReturntemp.XPathSelectElement("//ExtendedStatusCd") != null)
                //{
                //    string sMsgStatusCode2 = oReturntemp.XPathSelectElement("//ExtendedStatusCd").Value;
                //    if (sMsgStatusCode2.IndexOf("InvalidCriteriaException") >= 0)
                //    {
                //        Response.Clear();
                //        Response.Write("Enhc Notes threshold limit was exceeded."
                //            + "Enhc Notes printing has been scheduled.<br/>"
                //            + "Once generated, it will be accessible from the Attachments list.<br/><br/>"
                //            + "<input type='button' value='OK' style='align:center;' onclick='javascript:window.close()'/>"
                //            );

                //        Response.Flush();
                //        Response.End();
                //        return;
                //    }
                //}



            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                _Errorflag = true;//asharma326 MITS 34537
            }
        }
        //EnhancedMDI
        //protected void AdvancedSearch(object sender, EventArgs e)
        //{

        //}

        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/AdvancedSearch.aspx?EventID=" + eventid.Value +
                "&ClaimID=" + claimid.Value +
                "&LOB=" + LOB.Value +     //pmittal5 Mits 21514
                //williams-neha goel:MITS 21704:start :added for [olicy enahnced notes
                "&PolicyId=" + policyid.Value +
                "&PolicyName=" + policyname.Value +
                "&PolicyNumber=" + policynumber.Value +
                "&CodeId=" + NoteParentType.Value +
                //williams-neha goel:end:MITS 21704
                "&FormName=" + formname.Value +
                "&EventNumber=" + eventnumber.Value +
                "&ClaimantInfo=" + claimant.Value +
                //"&formsubtitle=" + this.hdHeader.Text+
                "&formsubtitle=" + this.hdHeader.Text.Replace("&", "^@").Replace("#", "*@") + //hlv MITS 28588 11/12/12 asharma326 MITS 34294
                "&sortPref=" + hdSortPref.Value + //Added by Amitosh for mits 23473 (05/11/2011)
                // Added by akaushik5 for mits 30789 Starts
                "&orderPref=" + hdOrderPref.Value +
                // Added by akaushik5 for mits 30789 Ends
                "&ClaimantId=" + hdClaimantId.Value +// JIRA issue RMA-359.

                "&SortColumn=" + hdSortColumn.Value + //mkaran2 - MITS 27038 - start 
                "&Direction=" + hdDirection.Value +
                "&claimprogressnoteid=" + claimprogressnoteid.Value +
                "&btnViewNotes=" + tdNoNotes.Visible.ToString() +
                "&freezetext=" + freezetext.Value +
                "&showDateStamp=" + showDateStamp.Value +
                "&username=" + username.Value //mkaran2 - MITS 27038 - end
                );
        }
        // mbahl3 Added For MITS 30513 Starts
        /// <summary>
        /// Handles the Change event of the selNoteAttachedTo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void selNotesLevel_Change(object sender, EventArgs e)
        {
            ProgressNoteBusinessHelper objHelper = new ProgressNoteBusinessHelper();
            ProgressNotesTypeChange objRequest = new ProgressNotesTypeChange();
            ProgressNotesTypeChange objResponse = new ProgressNotesTypeChange();
            string sTemp = string.Empty;
            try
            {

                if (selNotesLevel.SelectedValue.StartsWith("event"))// || selNotesLevel.SelectedValue.StartsWith("policy"))
                {
                    if (formname.Value == "piwitness" || formname.Value == "pidriver" || formname.Value == "piother" || formname.Value == "piemployee" ||
                  formname.Value == "PiInjury") //mbahl3 mits 34160
                    {
                        objRequest.RecordType = "EVENT";
                        objRequest.RecordId = eventid.Value;
                        claimid.Value = "0";
                        LOB.Value = string.Empty;
                        this.hdHeader.Text = hdnCaption.Text;
                        hdClaimantId.Value = "0";//Added by gbindra MITS#34104 02112014 WWIG GAP15
                    }
                    else
                    {
                        objRequest.RecordType = "EVENT";
                        objRequest.RecordId = eventid.Value;
                        claimid.Value = "0";
                        LOB.Value = string.Empty;
                        sTemp = "Event Enhanced Notes ";
                        objRequest.PIEId = hdnPIEId.Text; //mbahl3 mits 34160
                        objResponse = objHelper.GetNotesCaption(objRequest);
                        this.hdHeader.Text = sTemp + objResponse.CaptionText;
                        hdClaimantId.Value = "0";//Added by gbindra MITS#34104 02112014 WWIG GAP15
                    }
                }
                /*ADDED by gbindra MITS#34104 02112014 WWIG GAP15 START*/
                else if (selNotesLevel.SelectedValue.StartsWith("claimant", StringComparison.InvariantCultureIgnoreCase))
                {
                    string claimantValue = selNotesLevel.SelectedValue.Split(',')[1];
                    string claimValue = selNotesLevel.SelectedValue.Split(',')[2];
                    string lobValue = selNotesLevel.SelectedValue.Split(',')[3];

                    LOB.Value = lobValue;
                    hdClaimantId.Value = Convert.ToString(claimantValue);
                    objRequest.RecordType = "CLAIMANT";
                    objRequest.RecordId = hdClaimantId.Value;
                    sTemp = "Claimant Enhanced Notes";
                    objResponse = objHelper.GetNotesCaption(objRequest);
                    claimid.Value = claimValue;
                    objRequest.RecordId = eventid.Value;//Added by gbindra MITS#34104 02112014 WWIG GAP15

                    this.hdHeader.Text = sTemp + objResponse.CaptionText;
                }
                /*ADDED by gbindra MITS#34104 02112014 WWIG GAP15 END*/
                else if (selNotesLevel.SelectedValue.StartsWith("claim"))
                {
                    string[] claimValues = selNotesLevel.SelectedValue.Substring(5).Split(',');
                    if (!object.ReferenceEquals(claimValues, null) && claimValues.Length > 1)
                    {
                        claimid.Value = claimValues[0];
                        LOB.Value = this.GetClaimLOBFromCode(claimValues[1]);
                    }

                    objRequest.RecordType = "CLAIM";
                    objRequest.RecordId = claimid.Value;

                    sTemp = "Claim Enhanced Notes ";
                    objResponse = objHelper.GetNotesCaption(objRequest);
                    this.hdHeader.Text = sTemp + objResponse.CaptionText;

                    hdClaimantId.Value = "0";//Added by gbindra MITS#34104 02112014 WWIG GAP15
                }



                Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID=" + eventid.Value +
                    "&ClaimID=" + claimid.Value +
                    "&LOB=" + LOB.Value +
                    "&CodeId=" + NoteParentType.Value +
                    "&FormName=" + formname.Value +
                    "&EventNumber=" + eventnumber.Value +
                    "&ClaimantInfo=" + claimant.Value +
                    "&formsubtitle=" + this.hdHeader.Text.Replace("&", "^@") +
                    "&sortPref=" + hdSortPref.Value +
                    "&PIEId=" + hdnPIEId.Text +
                     "&notesdropdownchanged=true" +
                     "&ClaimantId=" + hdClaimantId.Value +
                     "&PIParent=" + hdPIParent.Value // ijain4,RMA-17480
                    );
                //mbahl3 mits 34160
            }

            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                //    return null;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                //return null; mini made change
            }
            finally
            {
                objHelper = null;
                objRequest = null;
                objResponse = null;
            }

        }
        // mbahl3 Added For MITS 30513 Ends


        /// <summary>
        /// Gets the claim LOB from code.
        /// </summary>
        /// <param name="lobCode">The lob code.</param>
        /// <returns>Corresponding LOB value</returns>
        private string GetClaimLOBFromCode(string lobCode)
        {
            string LOB = string.Empty;

            switch (lobCode)
            {
                case "241":
                    LOB = "GC";
                    break;
                case "243":
                    LOB = "WC";
                    break;
                case "242":
                    LOB = "VA";
                    break;
                case "844":
                    LOB = "DI";
                    break;
                case "845":
                    LOB = "PC";
                    break;
            }
            return LOB;
        }

        protected void GridChange(int ProgressNoteId)
        {
        }



        #region Sorting---Parijat
        protected void OnSort(object sender, SortColumnEventArgs e)
        {
            // There seems to be a bug in GridView sorting implementation. Value of
            // SortDirection is always set to "Ascending". Now we will have to play
            // little trick here to switch the direction ourselves.
            /**if (String.Empty != m_strSortExp) //Commented Rakhi-Change in Sort Functionality for UltraWebGrid
            // {
            //     if (String.Compare(e.SortExpression, m_strSortExp, true) == 0)
            //     {
            //         m_SortDirection =
            //             (m_SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
            //     }
            // }//Commented Rakhi-Change in Sort Functionality for UltraWebGrid**/

            string sSortColName = GetSortColumnName(e.ColumnNo);
            if (String.Empty != m_strSortExp)
            {
                if (String.Compare(sSortColName, m_strSortExp, true) == 0)
                {
                    m_SortDirection =
                        (m_SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
                }
            }
            //ViewState["_Direction_"] = m_SortDirection;
            //ViewState["_SortExp_"] = m_strSortExp = e.SortExpression;

            /**m_strSortExp=e.SortExpression**/
            m_strSortExp = sSortColName;
            //ProgressNotesType objProgressNote = new ProgressNotesType();
            //objProgressNote.SortCol = m_strSortExp;
            //if (m_SortDirection == SortDirection.Ascending)
            //    objProgressNote.Ascending = "true";
            //else
            //    objProgressNote.Ascending = "false";
            //if (ouraction.Value == "Filter")
            //    FilterSettings(ref objProgressNote);
            //else
            //    NonFilterSettings();
            //LoadPage_Explicit(ref objProgressNote);
            //this.bindGridView();
            if (m_SortDirection == SortDirection.Ascending)
                SortGridView(m_strSortExp.ToString(), " ASC");
            else
                SortGridView(m_strSortExp.ToString(), " DESC");

        }
        private void SortGridView(string sortExpression, string direction)
        {
            //smishra25 MITS 18098:Sorting to be done by query and not at grid level.
            //Commenting the lines below

            //Raman Bhatia: ProgressNotes crashes for claims/events having no attached records.
            // System.Collections.Generic.List<ProgressNote> arrProgressList = null;
            // if (ViewState["Datagrid_Arraylist"].ToString() != String.Empty)
            // {
            //     arrProgressList = (System.Collections.Generic.List<ProgressNote>)ViewState["Datagrid_Arraylist"];
            // }
            // else
            // {
            //     arrProgressList = new System.Collections.Generic.List<ProgressNote>();
            // }

            // DataTable dt = new DataTable();
            // dt.Columns.Add("ClaimProgressNoteId");
            // dt.Columns["ClaimProgressNoteId"].DataType = System.Type.GetType("System.Int32");
            // dt.Columns.Add("ClaimNumber");
            // dt.Columns.Add("EventNumber");
            // dt.Columns.Add("AttachedTo");
            // dt.Columns.Add("EnteredBy");
            // dt.Columns.Add("EnteredByName");
            // dt.Columns.Add("DateEntered",System.Type.GetType("System.DateTime"));
            // dt.Columns.Add("DateEnteredForSort");
            // dt.Columns.Add("DateCreated", System.Type.GetType("System.DateTime"));
            // dt.Columns.Add("TimeCreated");
            // dt.Columns.Add("TimeCreatedForSort");
            // dt.Columns.Add("NoteTypeCode");
            // dt.Columns.Add("NoteTypeCodeId");
            // dt.Columns.Add("UserTypeCodeId");
            // dt.Columns.Add("UserTypeCode");
            // dt.Columns.Add("NoteMemo");
            // dt.Columns.Add("NoteMemoTrimmed");
            // dt.Columns.Add("NoteMemoCareTech");
            // dt.Columns.Add("AdjusterName");
            // dt.Columns.Add("NewRecord");
            // dt.Columns.Add("EventID");
            // dt.Columns.Add("ClaimID");
            // dt.Columns.Add("TemplateId");
            // dt.Columns.Add("TemplateName");
            // foreach (ProgressNote prgNote in arrProgressList)
            // {

            //     DataRow dr = dt.NewRow();
            //     dr["ClaimProgressNoteId"] = prgNote.ClaimProgressNoteId;
            //     dr["ClaimNumber"] = prgNote.ClaimNumber;
            //     dr["EventNumber"] = prgNote.EventNumber;
            //     dr["AttachedTo"] = prgNote.AttachedTo;
            //     dr["EnteredBy"] = prgNote.EnteredBy;
            //     dr["EnteredByName"] = prgNote.EnteredByName;
            //     dr["DateEntered"] = prgNote.DateEntered;
            //     dr["DateEnteredForSort"] = prgNote.DateEnteredForSort;
            //     dr["DateCreated"] = prgNote.DateCreated;
            //     dr["TimeCreated"] = prgNote.TimeCreated;
            //     dr["TimeCreatedForSort"] = prgNote.TimeCreatedForSort;
            //     dr["NoteTypeCode"] = prgNote.NoteTypeCode;
            //     dr["NoteTypeCodeId"] = prgNote.NoteTypeCodeId;
            //     dr["UserTypeCodeId"] = prgNote.UserTypeCodeId;
            //     dr["UserTypeCode"] = prgNote.UserTypeCode;
            //     dr["NoteMemo"] = prgNote.NoteMemo;
            //     dr["NoteMemoTrimmed"] = prgNote.NoteMemoTrimmed;
            //     dr["NoteMemoCareTech"] = prgNote.NoteMemoCareTech;
            //     dr["AdjusterName"] = prgNote.AdjusterName;
            //     dr["NewRecord"] = prgNote.NewRecord;
            //     dr["EventID"] = prgNote.EventID;
            //     dr["ClaimID"] = prgNote.ClaimID;
            //     //Added Rakhi for R6-Progress Notes Templates
            //     dr["TemplateId"] = prgNote.TemplateID;
            //     dr["TemplateName"] = prgNote.TemplateName;
            //     //Added Rakhi for R6-Progress Notes Templates
            //     dt.Rows.Add(dr);

            // }
            // //DataView dv = new DataView(dt);
            ////changed by Nitin for Mits 13518 on 11/05/2009
            // if(sortExpression == "DateEntered")//13550
            //     dt.DefaultView.Sort = sortExpression + " " + direction + ", DateCreated " + direction + ", TimeCreatedForSort " + direction;
            // else
            //     dt.DefaultView.Sort = sortExpression + direction;
            // UltraWebGrid1.DataSource = dt;
            // UltraWebGrid1.DataBind();
            // SetGrdRadiosOnClick();
            // //first radio click on load and also the event 
            // if (UltraWebGrid1.Rows.Count > 0)
            // {
            //     /**((RadioButton)GridView1.Rows[0].FindControl("RowSelector")).Checked = true;
            //     ChangeOnRadioClick(GridView1.Rows[0]);**/

            //     TemplatedColumn col = (TemplatedColumn)UltraWebGrid1.Rows[0].Cells[0].Column;
            //     CellItem item = (CellItem)col.CellItems[UltraWebGrid1.Rows[0].Index];
            //     if (item.FindControl("RowSelector") != null)//Added for Mits:18267
            //     {
            //         ((RadioButton)item.FindControl("RowSelector")).Checked = true;
            //         ChangeOnRadioClick(UltraWebGrid1.Rows[0]);
            //     }
            // }
            hdDirection.Value = direction;
            hdSortColumn.Value = sortExpression;
            OnPageLoad(sSort, sortExpression, direction);
            SortGridData(sortExpression, direction);
        }
        private void SortGridData(string sortExpression, string direction)
        {
            //smishra25 MITS 18098:Sorting to be done by query and not at grid level.
            //Commenting the lines below

            // Raman Bhatia: ProgressNotes crashes for claims/events having no attached records.

            // rrachev JIRA RMA-12166 Sorting to be done by query and not at grid level.
            //Commenting the lines below
            //System.Collections.Generic.List<ProgressNote> arrProgressList = null;
            //if (ViewState["Datagrid_Arraylist"].ToString() != String.Empty)
            //{
            //    arrProgressList = (System.Collections.Generic.List<ProgressNote>)ViewState["Datagrid_Arraylist"];
            //}
            //else
            //{
            //    arrProgressList = new System.Collections.Generic.List<ProgressNote>();
            //}

            //DataTable dt = new DataTable();
            //dt.Columns.Add("ClaimProgressNoteId");
            //dt.Columns["ClaimProgressNoteId"].DataType = System.Type.GetType("System.Int32");
            //dt.Columns.Add("ClaimNumber");
            //dt.Columns.Add("EventNumber");
            //dt.Columns.Add("AttachedTo");
            //dt.Columns.Add("EnteredBy");
            //dt.Columns.Add("EnteredByName");
            //dt.Columns.Add("Subject");//zmohammad MITS 30218
            //dt.Columns.Add("DateEntered");
            //dt.Columns["DateEntered"].DataType = System.Type.GetType("System.DateTime");//srajindersin 2/2/2014 MITS 34084
            //dt.Columns.Add("DateEnteredForSort");
            //dt.Columns.Add("DateCreated");
            //dt.Columns["DateCreated"].DataType = System.Type.GetType("System.DateTime");//srajindersin 2/2/2014 MITS 34084
            //dt.Columns.Add("TimeCreated");
            //dt.Columns.Add("TimeCreatedForSort");
            //dt.Columns.Add("NoteTypeCode");
            //dt.Columns.Add("NoteTypeCodeId");
            //dt.Columns.Add("UserTypeCodeId");
            //dt.Columns.Add("UserTypeCode");
            //dt.Columns.Add("NoteMemo");
            //dt.Columns.Add("NoteMemoTrimmed");
            //dt.Columns.Add("NoteMemoCareTech");
            //dt.Columns.Add("AdjusterName");
            //dt.Columns.Add("NewRecord");
            //dt.Columns.Add("EventID");
            //dt.Columns.Add("ClaimID");
            //dt.Columns.Add("TemplateId");
            //dt.Columns.Add("TemplateName");
            ////williams--neha goel:MITS 21704--start:added policy info
            //dt.Columns.Add("PolicyID");
            //dt.Columns.Add("PolicyName");
            ////williams--neha goel:MITS 21704--end:added policy info
            //foreach (ProgressNote prgNote in arrProgressList)
            //{

            //    DataRow dr = dt.NewRow();
            //    dr["ClaimProgressNoteId"] = prgNote.ClaimProgressNoteId;
            //    dr["ClaimNumber"] = prgNote.ClaimNumber;
            //    dr["EventNumber"] = prgNote.EventNumber;
            //    dr["AttachedTo"] = prgNote.AttachedTo;
            //    dr["EnteredBy"] = prgNote.EnteredBy;
            //    dr["EnteredByName"] = prgNote.EnteredByName;
            //    //Ashish Ahuja Mits 33124
            //    if (prgNote.Subject.Length > 33)
            //        dr["Subject"] = prgNote.Subject.Substring(0, 30) + "...";
            //    else
            //        dr["Subject"] = !string.IsNullOrEmpty(prgNote.Subject) ? prgNote.Subject : "&nbsp;";//zmohammad MITS 30218//asharma326 add &nbsp; for MITS 31845
            //    //dr["Subject"] = prgNote.Subject; //zmohammad MITS 30218
            //    dr["DateEntered"] = Convert.ToDateTime(prgNote.DateEntered, System.Globalization.CultureInfo.CreateSpecificCulture(AppHelper.GetCulture()).DateTimeFormat);
            //    dr["DateEnteredForSort"] = prgNote.DateEnteredForSort;
            //    dr["DateCreated"] = Convert.ToDateTime(prgNote.DateCreated, System.Globalization.CultureInfo.CreateSpecificCulture(AppHelper.GetCulture()).DateTimeFormat);
            //    dr["TimeCreated"] = AppHelper.GetTime(prgNote.TimeCreated);
            //    dr["TimeCreatedForSort"] = prgNote.TimeCreatedForSort;
            //    dr["NoteTypeCode"] = prgNote.NoteTypeCode;
            //    dr["NoteTypeCodeId"] = prgNote.NoteTypeCodeId;
            //    dr["UserTypeCodeId"] = prgNote.UserTypeCodeId;
            //    dr["UserTypeCode"] = prgNote.UserTypeCode;
            //    dr["NoteMemo"] = prgNote.NoteMemo;
            //    dr["NoteMemoTrimmed"] = prgNote.NoteMemoTrimmed;
            //    dr["NoteMemoCareTech"] = prgNote.NoteMemoCareTech;
            //    dr["AdjusterName"] = prgNote.AdjusterName;
            //    dr["NewRecord"] = prgNote.NewRecord;
            //    dr["EventID"] = prgNote.EventID;
            //    dr["ClaimID"] = prgNote.ClaimID;
            //    //williams--neha goel:MITS 21704--start:added policy info
            //    dr["PolicyID"] = prgNote.PolicyID;
            //    dr["PolicyName"] = prgNote.PolicyName;
            //    //williams--neha goel:MITS 21704--end:added policy info
            //    //Added Rakhi for R6-Progress Notes Templates
            //    dr["TemplateId"] = prgNote.TemplateID;
            //    dr["TemplateName"] = prgNote.TemplateName;
            //    //Added Rakhi for R6-Progress Notes Templates
            //    dt.Rows.Add(dr);

            //}
            //DataView dv = new DataView(dt);
            //changed by Nitin for Mits 13518 on 11/05/2009
            // || condition added by Shivendu for MITS 18098

            //rsolanki2 : mits 21955: sorting is now being handled exclusively at the DB side
            //if (sortExpression == "DateEntered" || sortExpression == "NoteMemo")//13550
            //    dt.DefaultView.Sort = "DateEntered" + " " + direction + ", DateCreated " + direction + ", TimeCreatedForSort " + direction;
            //else
            //    dt.DefaultView.Sort = sortExpression + direction;
            // rrachev JIRA RMA-12166 Sorting to be done by query and not at grid level.
            //Commenting the lines below
            //UltraWebGrid1.DataSource = dt;
            //UltraWebGrid1.DataBind();

            //rsolanki2: Enhc notes ajax updates: remvoing the radio button bind as its no longer required.
            //SetGrdRadiosOnClick();

            //rsolanki2: Ench notes Ajax updates 
            int iRowIndex = 0;
            //first radio click on load and also the event 
            if (UltraWebGrid1.Rows.Count > 0)
            {
                /**((RadioButton)GridView1.Rows[0].FindControl("RowSelector")).Checked = true;
                ChangeOnRadioClick(GridView1.Rows[0]);**/
                //rsolanki2 : enhc notes ajax updates
                if (!string.IsNullOrEmpty(hdCurrentNode.Value))
                {
                    int.TryParse(hdCurrentNode.Value, out iRowIndex);
                    if (iRowIndex > UltraWebGrid1.Rows.Count - 1)
                    {
                        iRowIndex = 0;
                    }

                }


                TemplatedColumn col = (TemplatedColumn)UltraWebGrid1.Rows[iRowIndex].Cells[0].Column;
                CellItem item = (CellItem)col.CellItems[iRowIndex];
                //if (item.FindControl("RowSelector") != null)//Added for Mits:18267
                //{
                //    ((RadioButton)item.FindControl("RowSelector")).Checked = true;
                ChangeOnRadioClick(UltraWebGrid1.Rows[iRowIndex]);
                //Start by Shivendu for MITS 18420
                UltraGridRow row = UltraWebGrid1.Rows[iRowIndex];
                row.Style.BackColor = System.Drawing.Color.FromName("#DDDDDD");
                UltraWebGrid1.DisplayLayout.ActiveRow = UltraWebGrid1.Rows[iRowIndex];
                //End by Shivendu for MITS 18420


                // rsolanki2: load note details for initial load. 
                // right now, this is a separate call retriveing the notes details.
                // eventaully we will need to combine this with the rest of the call to reduce the number of calls to the business layer
                ProgressNoteBusinessHelper objPNBH = new ProgressNoteBusinessHelper();
                GetNoteDetailsObject objGNDO = new GetNoteDetailsObject();
                //objGNDO.ClaimProgressNoteId = UltraWebGrid1.Rows[row.Index].Cells.FromKey("ClaimProgressNoteId").Text;

                objGNDO.ClaimProgressNoteId = row.Cells.FromKey("ClaimProgressNoteId").Text;

                objGNDO = objPNBH.GetNoteDetails(objGNDO);
                NoteMemoDiv.InnerHtml = objGNDO.HTML;
                //Ashish Ahuja - Mits 33124
                Subject.Value = objGNDO.Subject;
                //}
            }
            //MITS 35290 
            else
            {
                ProgressNoteSettings objProgressNoteSettings = (ProgressNoteSettings)ViewState["ColHeaders"];
                if (objProgressNoteSettings != null)
                {
                    if (!objProgressNoteSettings.HeaderKeyShowList.Contains("DateCreated"))
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey("DateCreated"));
                    }
                    if (!objProgressNoteSettings.HeaderKeyShowList.Contains("TimeCreated"))
                    {
                        UltraWebGrid1.DisplayLayout.Bands[0].Columns.Remove(UltraWebGrid1.DisplayLayout.Bands[0].Columns.FromKey("TimeCreated"));
                    }
                }
            }
            //MITS 35290 
        }

        //private void SortGridView(string sortExpression, string direction)
        //{

        //    // You can cache the DataTable for improving performance

        //    //DataTable dt = GetData().Tables[0];

        //    //DataView dv = new DataView(dt);

        //    //dv.Sort = sortExpression + direction;
        //    //ArrayList arr = (ArrayList)Session["Datagrid_Arraylist"];
        //    //arr.Sort(
        //    //GridView1.DataSource = dv;

        //    //GridView1.DataBind();

        //}

        public string GetMaxLenString(object value, int maxLen)
        {
            string result = "&nbsp;";
            if (value != null)
            {
                string tmlVal = value.ToString().Trim();
                if (tmlVal.Length > (maxLen + 3))
                {
                    result = tmlVal.Substring(0, maxLen) + "...";
                }
                else
                {
                    result = tmlVal;
                }
            }

            return result;
        }

        #region Commented Rakhi-Functions not needed for UltraWebGrid
        //protected void OnRowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        if (String.Empty != m_strSortExp)
        //        {
        //            AddSortImage(e.Row);
        //        }
        //    }
        //}
        //void AddSortImage(GridViewRow headerRow)
        //{
        //    Int32 iCol = GetSortColumnIndex(m_strSortExp);
        //    if (-1 == iCol)
        //    {
        //        return;
        //    }
        //    // Create the sorting image based on the sort direction.
        //    Image sortImage = new Image();
        //    if (SortDirection.Ascending == m_SortDirection)
        //    {
        //        sortImage.ImageUrl = "../../Images/arrow_up_white.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/arrow_down_white.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }

        //    // Add the image to the appropriate header cell.
        //    headerRow.Cells[iCol].Controls.Add(sortImage);
        //    headerRow.Cells[iCol].ForeColor = System.Drawing.Color.FromName("#cccc33");
        //}

        //int GetSortColumnIndex(string str)
        //{
        //    int i = -1;
        //    switch (str)
        //    {
        //        case "DATE_ENTERED" :
        //            i = 1;
        //            break;
        //        case "DateEntered":
        //            i = 1;
        //            break;
        //        case "AttachedTo":
        //            i = 2;
        //            break;
        //        case "NOTE_TYPE_DESC" :
        //            i = 3;
        //            break;
        //        case "NoteTypeCode":
        //            i = 3;
        //            break;
        //        case "NoteMemo":
        //            i = 4;
        //            break;
        //        case "NoteMemoCareTech":
        //            i = 4;
        //            break;
        //        case "ENTERED_BY_NAME" :
        //            i = 6;
        //            break;
        //        case "AdjusterName":
        //            i = 6;
        //            break;
        //    }

        //    return i;
        //}
        #endregion
        public string GetSortColumnName(int iColIndex)
        {
            //ProgressNoteBusinessHelper pn = null;
            //ProgressNoteSettings arrProgressSettings = new ProgressNoteSettings();
            //string sColName = string.Empty;

            //if (ViewState["ColHeaders"].ToString() != String.Empty)
            //{
            //    arrProgressSettings = (ProgressNoteSettings)ViewState["ColHeaders"];
            //}
            //else
            //{
            //    pn = new ProgressNoteBusinessHelper();
            //    arrProgressSettings = pn.GetNotesHeaderOrder(arrProgressSettings);
            //}

            //switch (iColIndex)
            //{
            //    case 0://rsolanki2 : enhc notes updates
            //        sColName = arrProgressSettings.Header1;
            //        break;
            //    case 1:
            //        sColName = arrProgressSettings.Header2;
            //        break;
            //    case 2:
            //        sColName = arrProgressSettings.Header3;
            //        break;
            //    case 3:
            //        sColName = arrProgressSettings.Header4;
            //        break;
            //    case 4:
            //        sColName = arrProgressSettings.Header5;
            //        break;


            //}

            //switch (sColName)
            //{
            //    case "Activity Date":
            //        return "DateEntered";
            //    case "Attached To":
            //        return "AttachedTo";
            //    case "Note Type":
            //        return "NoteTypeCode";
            //    case "Note Text":
            //        return "NoteMemo";
            //    case "Entered By":
            //        return "AdjusterName";
            //    default:
            //        return "DateEntered";
            //}  

            //rsolanki2: MITS 22774 & 22774 - for sorting
            string sSortKey = UltraWebGrid1.Bands[0].Columns[iColIndex].ToString();

            switch (sSortKey)
            {
                case "Activity Date":
                    return "DateEntered";
                case "Attached To":
                    return "AttachedTo";
                case "Note Type":
                    return "NoteTypeCode";
                case "Note Text":
                    return "NoteMemo";
                case "Entered By":
                    return "AdjusterName";
                default:
                    return "DateEntered";
            }

        }
        public SortDirection m_SortDirection
        {

            get
            {

                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];

            }

            set { ViewState["sortDirection"] = value; }

        }
        public string m_strSortExp
        {

            get
            {

                if (ViewState["sortExpression"] == null)
                    return "";
                else
                    return ViewState["sortExpression"].ToString();

            }

            set { ViewState["sortExpression"] = value; }

        }
        #endregion
        protected void btnTemplates_Click(object sender, EventArgs e)
        {
            //williams-neha goel:MITS 21704:start :added for policy enahnced notes
            //Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesTemplates.aspx?LookUp=false&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + formname.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value + "&formsubtitle=" + this.hdHeader.Text);
            ////pmittal5 Mits 21514 :added 
            Server.Transfer("/RiskmasterUI/UI/ProgressNotes/ProgressNotesTemplates.aspx?LookUp=false&EventID=" + eventid.Value + "&ClaimID=" + claimid.Value + "&FormName=" + formname.Value + "&EventNumber=" + eventnumber.Value + "&ClaimantInfo=" + claimant.Value
                          + "&formsubtitle=" + this.hdHeader.Text.Replace("&", "^@").Replace("#", "*@") + "&PolicyId=" + policyid.Value + "&PolicyName=" + policyname.Value + "&PolicyNumber=" + policynumber.Value + "&LOB=" + LOB.Value);
            //hdHeader hlv MITS 28588 11/12/12 asharma326 MITS 34294
            //williams-neha goel:end :MITS 21704       
        }
        #region Events not being used currently
        //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    Label lbActDate = null;
        //    //GridView1.Rows[e.Row.RowIndex].Cells[0].Text
        //    //((HtmlInputRadioButton)e.Row.FindControl("RowSelector")).Attributes.Add(
        //    //       "onclick","fnCheckSel('" + e.Row.RowIndex + "')"); 

        //    //Page.RegisterClientScriptBlock("ee", "<Script>NoteSelectionChanged()</Script>");

        //    //added by Nitin for Mits 13518 on 11/May/2009
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        lbActDate = (Label)e.Row.Cells[0].FindControl("lblActivityDate");
        //        lbActDate.Text = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DateEntered").ToString()).ToString("d");//Parijat:changes regarding mm/dd/yyyy format asked by Hope Lockhart 
        //    }
        //}
        //protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    //RadioButton rdRowselector = (RadioButton)sender;
        //    //Page.RegisterClientScriptBlock("ee", "<Script>alert(\""+sender.ToString()+"</Script>");
        //    //Response.Write("GridView1_OnRowCommand");
        //}
        //protected void GridView1_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Response.Write("GridView1_OnSelectedIndexChanged");
        //}
        #endregion

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
            //OnPageLoad(sFirst, "", "");//Added by Shivendu fo MITS 18098
            if (ScriptManager1.IsInAsyncPostBack)
            {

                //btntest2.Text = "updated Async ";
                //bool stemp = false;
                int iIndex = 0;
                //stemp = ;
                //int iIndex = Riskmaster.Common.Conversion.CastToType<Int32>(hdCurrentNode.Value, out stemp);

                if (int.TryParse(hdCurrentNode.Value, out iIndex))
                {
                    //todo: remove try catch here.
                    //if (iIndex>0)
                    //{
                    //    //iIndex--;
                    //}
                    try
                    {
                        //if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("DateEntered"))
                        //{objProgressNote.objProgressNoteList[0].ClaimProgressNoteIdField

                        //} UltraWebGrid1.Rows[iIndex].Cells.FromKey("ClaimProgressNoteId")

                        //todo: move these sections to else section down.


                        //Changed by Amitosh for mits 23691(05/11/2011)
                        //DateCreated.InnerHtml = string.Empty;
                        //TimeCreated.InnerHtml = string.Empty;
                        //EnteredByName.InnerHtml = string.Empty;
                        //NoteTypeCode.InnerHtml = string.Empty;
                        //UserTypeCode.InnerHtml = string.Empty;
                        //AttachedTo.InnerHtml = string.Empty;
                        DateCreated.Value = string.Empty;
                        TimeCreated.Value = string.Empty;
                        EnteredByName.Value = string.Empty;
                        NoteTypeCode.Value = string.Empty;
                        UserTypeCode.Value = string.Empty;
                        AttachedTo.Value = string.Empty;
                        notetypecodeid.Value = string.Empty;

                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("Activity Date") != null)
                        {
                            //rsolanki2 : MITS 23052: striping the time out of the datetime info as we dont have any UI to change the time for the activity date.
                            //DateEntered.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("Activity Date").Text.Split(' ')[0].ToString();
                            DateEntered.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("Activity Date").Text.Split(' ')[0].ToString();
                        }
                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("DateCreated") != null)
                        {
                            //DateCreated.InnerText = Convert.ToDateTime(UltraWebGrid1.Rows[iIndex].Cells.FromKey("DateCreated").Text).ToString("d");
                            DateCreated.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("DateCreated").Text;
                        }
                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("TimeCreated") != null)
                        {
                            //TimeCreated.InnerText  = UltraWebGrid1.Rows[iIndex].Cells.FromKey("TimeCreated").Text;
                            TimeCreated.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("TimeCreated").Text;
                        }
                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("Entered By") != null)
                        {
                            //EnteredByName.Innertext = UltraWebGrid1.Rows[iIndex].Cells.FromKey("Entered By").Text;
                            EnteredByName.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("Entered By").Text;
                        }
                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("Note Type") != null)
                        {
                            NoteTypeCode.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("Note Type").Text;
                        }
                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("NoteTypeCodeId").Text != null)
                        {
                            notetypecodeid.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("NoteTypeCodeId").Text;
                        }
                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("UserTypeCode") != null)
                        {
                            UserTypeCode.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("UserTypeCode").Text;
                        }
                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("Attached To") != null)
                        {
                            AttachedTo.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("Attached To").Text;
                        }

                        if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("ClaimProgressNoteId") != null)
                        {
                            claimprogressnoteid.Value = UltraWebGrid1.Rows[iIndex].Cells.FromKey("ClaimProgressNoteId").Text;
                        }
                        //end Amitosh

                        //DateCreated.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("DateCreated").Text;
                        //TimeCreated.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("TimeCreated").Text;
                        //EnteredByName.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("EnteredByName").Text;
                        //NoteTypeCode.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("NoteTypeCode").Text;
                        //UserTypeCode.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("UserTypeCode").Text;
                        //AttachedTo.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("AttachedTo").Text;

                        //UpProgressbar.Visible = false;
                        //   UpProgressbar.Style["display"]= "none";
                        //NoteMemoDiv.Visible = true;
                        NoteMemoDiv.Style["display"] = "block";

                        //UltraWebGrid1.Rows[iIndex].Cells.FromKey("ClaimProgressNoteId").Text
                        ProgressNoteBusinessHelper objPNBH = new ProgressNoteBusinessHelper();
                        GetNoteDetailsObject objGNDO = new GetNoteDetailsObject();
                        objGNDO.ClaimProgressNoteId = UltraWebGrid1.Rows[iIndex].Cells.FromKey("ClaimProgressNoteId").Text;


                        objGNDO = objPNBH.GetNoteDetails(objGNDO);
                        NoteMemoDiv.InnerHtml = objGNDO.HTML;
                        //Ashish Ahuja -Mits 33124
                        Subject.Value = objGNDO.Subject;
                        ChangeOnRadioClick(UltraWebGrid1.Rows[iIndex]);
                        //if (UltraWebGrid1.Rows[iIndex].Cells.FromKey("NoteText") != null)
                        //{
                        //    NoteMemoDiv.InnerHtml = UltraWebGrid1.Rows[iIndex].Cells.FromKey("NoteText").Text;
                        //}
                        //else
                        //{
                        //    NoteMemoDiv.InnerHtml = string.Empty;
                        //}


                        //                        Page.FindControl("UpProgressbar").Visible = false;

                        //upProgressNotes.Update();

                        DateTime DateTimeCreated = Convert.ToDateTime(DateCreated.Value + " " + TimeCreated.Value, System.Globalization.CultureInfo.CreateSpecificCulture(AppHelper.GetCulture()).DateTimeFormat);
                        TimeSpan ts = DateTime.Now - DateTimeCreated;
                        //williams-neha goel:MITS 21704--start:added for policy enhanced notes; 
                        //williams-nehagoel:MITS 21704 :disable policy enhanced notes when viewed from claim---start
                        string sFormName = AppHelper.GetQueryStringValue("FormName");
                        string sPiParent = AppHelper.GetQueryStringValue("PIParent");// JIRA 19340 
                        if ((AttachedTo.Value.Contains("POLICY") && (sFormName != "policy" && sFormName != "policyenhal" && sFormName != "policyenhgl" && sFormName != "policyenhpc" && sFormName != "policyenhwc"
                           && (sFormName != "pimedstaff") && (sFormName != "piemployee") && (sFormName != "pidriver") && (sFormName != "pipatient") && (sFormName != "piphysician") && (sFormName != "piwitness") && (sFormName != "piother"))) // jira 18944
                          || (!string.IsNullOrEmpty(sPiParent) && sPiParent.ToUpper() == "CLAIM" && AttachedTo.Value.Contains("POLICY"))) // JIRA 19340 
                        {
                            //btnDeleteNotes.Enabled = false;
                            hdnDeleteButtonState.Value = "disabled";
                            ScriptManager.RegisterStartupScript(this.upProgressNotes, this.GetType(), "DisableDelete", "DisableDeleteButton();", true);
                            //btnEditNote.Enabled = false;
                            hdnEditButtonState.Value = "disabled";
                            ScriptManager.RegisterStartupScript(this.upProgressNotes, this.GetType(), "DisableEdit", "DisableEditButton();", true);
                        }
                        else
                        {
                            if (Convert.ToBoolean(ViewState["bEditPermission"]) || Convert.ToBoolean(ViewState["bPolicyTrackingEditPermission"]))//Parijat :19934 So that other settings don't override the security management setting
                            {
                                if ((hdnUserEditingRights.Value == "true" || hdnUserEditingRights.Value == "True") ? (username.Value == EnteredByName.Value) : true)
                                {
                                    if ((Convert.ToDouble(hdnEnhancedTimeLimit.Value) != 0) && (ts.TotalHours >= Convert.ToDouble(hdnEnhancedTimeLimit.Value)))//Parijat: 0 considered as infinite
                                    {
                                        //btnEditNote.Enabled = false;
                                        hdnEditButtonState.Value = "disabled";
                                    }
                                    else
                                    {
                                        //btnEditNote.Enabled = true;
                                        hdnEditButtonState.Value = "enabled";
                                    }
                                }
                                else
                                {
                                    //btnEditNote.Enabled = false;
                                    hdnEditButtonState.Value = "disabled";
                                }
                                ScriptManager.RegisterStartupScript(this.upProgressNotes, this.GetType(), "DisableEdit", "DisableEditButton();", true);

                            }
                            //williams-nehagoel:MITS 21704 :disable policy enhanced notes when viewed from claim---start
                            if ((Convert.ToBoolean(ViewState["bDeletePermission"])) || (Convert.ToBoolean(ViewState["bPolicyTrackingDeletePermission"])))
                            {
                                // btnDeleteNotes.Enabled = true;
                                hdnDeleteButtonState.Value = "enabled";
                                ScriptManager.RegisterStartupScript(this.upProgressNotes, this.GetType(), "DisableDelete", "DisableDeleteButton();", true);
                            }
                            else
                            {
                                // btnDeleteNotes.Enabled = false;
                                hdnDeleteButtonState.Value = "disabled";
                                ScriptManager.RegisterStartupScript(this.upProgressNotes, this.GetType(), "DisableDelete", "DisableDeleteButton();", true);
                            }
                            //willimas-neha goel:end:MITS 21704
                        }
                        //williams-nehagoel :disable policy enhanced notes when viewed from claim---end
                    }
                    catch (Exception ex)
                    {
                        //NoteMemoDiv.InnerHtml = "Notes details retrival Error :" + ex.Message.ToString();                        
                        NoteMemoDiv.InnerHtml = GetResourceValue("CatchNotesError", "0") + ex.Message.ToString();
                    }
                }


            }


            //upProgressNotes.Update();
        }

        //rsolanki2 : added for "date getting sorted as string" issue
        protected void UltraWebGrid1_InitializeLayout(object sender, Infragistics.WebUI.UltraWebGrid.LayoutEventArgs e)
        {
            //foreach (UltraGridColumn col in UltraWebGrid1.Columns)
            //{
            //    if (col.DataType == typeof(DateTime).ToString() || col.Key.Equals("Activity Date", StringComparison.OrdinalIgnoreCase))
            //    {
            //        col.EditorControlID = "WebDateTimeEdit1";
            //        col.Type = ColumnType.Custom;
            //    }
            //}


            UltraWebGrid1.DisplayLayout.AllowSortingDefault = AllowSorting.Yes;
            UltraWebGrid1.DisplayLayout.SortingAlgorithmDefault =
                  SortingAlgorithm.Custom;
            UltraWebGrid1.DisplayLayout.SortImplementation =
                    new SortRowsCollection(this.MySortAlgorithm);

        }

        //rsolanki2 : added for "date getting sorted as string" issue
        //deliberately left all code int he algo fucntion below comented as we dont actually need to sort. its been taken care of at the SQL level.
        public void MySortAlgorithm(Infragistics.WebUI.UltraWebGrid.RowsCollection rows, int[] array)
        {
            //bool hasSwapped = true;
            //    while (hasSwapped)
            //    {
            //        hasSwapped = false;
            //        for (int i = 0; i < array.Length - 1; i++)
            //        {
            //            if (rows[array[i]].CompareTo(rows[array[i + 1]]) > 0)
            //            {
            //                hasSwapped = true;
            //                int swap = array[i];
            //                array[i] = array[i + 1];
            //                array[i + 1] = swap;
            //            }
            //        }
            //    }
        }
        // start : atavaragiri MITS 28264
        protected void Show_Click(object sender, EventArgs e)
        {
            OnNavigate();
        }



        public void OnNavigate()
        {
            try
            {

                ProgressNotesType objProgressNote = new ProgressNotesType();
                objProgressNote.objFilter = new Filter();
                objProgressNote.PageNumber = Int32.Parse(txtPageNumber.Text);

                if (!string.IsNullOrEmpty(hdSortColumn.Value) && !string.IsNullOrEmpty(hdDirection.Value))
                {
                    objProgressNote.SortColumn = hdSortColumn.Value;
                    objProgressNote.Direction = hdDirection.Value;
                }
                else
                {
                    objProgressNote.SortColumn = sNoSort;
                }

                string sPageNumber = txtPageNumber.Text;
                int iPageNumber = 1, iMaxPages = 1;

                if (!string.IsNullOrEmpty(sPageNumber))
                {
                    int.TryParse(sPageNumber, out iPageNumber);
                    objProgressNote.PageNumber = iPageNumber;
                }

                FillHiddenControls();

                if (ouraction.Value == "Filter")
                    FilterSettings(ref objProgressNote);
                else
                    NonFilterSettings();

                LoadPage_Explicit(ref objProgressNote);
                freezetext.Value = objProgressNote.FreezeText.ToString();
                showDateStamp.Value = objProgressNote.ShowDateStamp.ToString();
                EnableDisablePageLinks(objProgressNote);

            }

            catch (Exception ee)
            {
                tdNoNotes.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }   // end : MITS 28264

        private string GetResourceValue(string strResourceKey, string strResourceType)
        {
            return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("ProgressNotes.aspx"), strResourceType).ToString();
        }

        // mbahl3 Added form MITS 30513 Starts
        /// <summary>
        /// Populates the Notes Level drop down list.
        /// </summary>
        /// <param name="selectClaimObject">The obj claim.</param>
        private void PopulateNotesLevelDropDown(SelectClaimObject selectClaimObject)//, string p_sEventID, string p_sEventNumber, string p_sClaimID, string p_sPolicyid, string p_sPolicyName, string p_sFormName, string p_sPolicyNumber)
        {
            //selNotesLevel.Items.Clear();
         //ijain4
            if (this.hdPIParent.Value.Equals("policy"))
            {
                selNotesLevel.Visible = false;
                lblNotesLevel.Visible = false;
            }
            else
            {
                selNotesLevel.Visible = true;
                lblNotesLevel.Visible = true;
            }
            if (!this.formname.Value.Equals("policy") && !this.formname.Value.Equals("policyenhal") && !this.formname.Value.Equals("policyenhgl") && !this.formname.Value.Equals("policyenhpc") && !this.formname.Value.Equals("policyenhwc") &&  !this.hdPIParent.Value.Equals("policy"))
            {
                if (selectClaimObject.SelectEvent && !(activatefilter.Value.Equals("true") && !string.IsNullOrEmpty(claimidlist.Value)))
                {
                    //  ListItem oEventItem = new ListItem("Event: " + this.eventnumber.Value, "event" + this.eventid.Value);
                    ListItem oEventItem = new ListItem("Event: " + this.eventnumber.Value, "event");
                    selNotesLevel.Items.Add(oEventItem);

                }

                if (!object.ReferenceEquals(selectClaimObject.objClaimList, null) && selectClaimObject.objClaimList.Count > 0)
                {
                    List<ClaimInfo> claimInfoList = null;
                    if (activatefilter.Value.Equals("true") && !string.IsNullOrEmpty(claimidlist.Value))
                    {
                        List<string> claimList = claimidlist.Value.Split(',').ToList();
                        claimInfoList = new List<ClaimInfo>();

                        claimInfoList = selectClaimObject.objClaimList.ToList().FindAll(x => claimList.Contains(x.ClaimID));

                    }
                    else
                    {
                        claimInfoList = selectClaimObject.objClaimList.ToList();
                    }


                    foreach (ClaimInfo claimInfo in claimInfoList)
                    {
                        // string value = string.Format("Claim: {0} * {1}", claimInfo.ClaimNumber, claimInfo.ClaimantName);
                        string value = string.Format("Claim: {0}", claimInfo.ClaimNumber);
                        string sId = string.Format("claim{0},{1}", claimInfo.ClaimID, claimInfo.LOB);
                        ListItem listItem = new ListItem(value, sId);

                        selNotesLevel.Items.Add(listItem);

                        if (claimInfo.ClaimID.Equals(this.claimid.Value))
                        {
                            listItem.Selected = true;
                        }
                    }
                }
            }
        }
        // mbahl3 Added form MITS 30513 Ends
        /// <summary>
        /// Populates the Notes Level drop down list for Claimant. Created by gbindra 02102014 MITS#34104 GAP15 WWIG
        /// </summary>
        /// <param name="selectClaimObject">The obj claimant.</param>
        private void PopulateNotesLevelDropDown(SelectClaimantObject selectClaimantObject)//, string p_sEventID, string p_sEventNumber, string p_sClaimID, string p_sPolicyid, string p_sPolicyName, string p_sFormName, string p_sPolicyNumber)
        {
            if (!this.formname.Value.Equals("policy") && !this.formname.Value.Equals("policyenhal") && !this.formname.Value.Equals("policyenhgl") && !this.formname.Value.Equals("policyenhpc") && !this.formname.Value.Equals("policyenhwc") && !this.hdPIParent.Value.Equals("policy"))
            {
                if (!object.ReferenceEquals(selectClaimantObject.objClaimantList, null) && selectClaimantObject.objClaimantList.Count > 0
                    && (string.Compare(selectClaimantObject.objClaimantList[0].ClaimID, "-1", true) != 0))
                {
                    List<ClaimantInfo> claimantInfoList = null;
                    claimantInfoList = selectClaimantObject.objClaimantList.ToList();
                    foreach (ClaimantInfo claimantInfo in claimantInfoList)
                    {
                        string value = string.Format("Claimant: {0}", claimantInfo.ClaimantLastName + " " + claimantInfo.ClaimantFirstName + " * " + claimantInfo.ClaimNumber);
                        string sId = string.Format("claimant,{0},{1},{2}", claimantInfo.ClaimantRowId, claimantInfo.ClaimID, claimantInfo.LOB);
                        ListItem listItem = new ListItem(value, sId);
                        selNotesLevel.Items.Add(listItem);
                        if (claimantInfo.ClaimantRowId.ToString().Equals(this.hdClaimantId.Value))
                        {
                            foreach (ListItem items in selNotesLevel.Items)
                            {
                                if (items.Selected.Equals(true))
                                    items.Selected = false;
                            }
                            listItem.Selected = true;
                        }
                    }
                }
            }
        }
    }
}
