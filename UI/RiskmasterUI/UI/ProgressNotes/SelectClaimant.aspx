﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectClaimant.aspx.cs" Inherits="Riskmaster.UI.ProgressNotes.SelectClaimant" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <title>Claimant Selection</title>
   <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css">
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css">
<script language="JavaScript" src="../../Scripts/ProgressNotes.js">
</script>
 <script language="JavaScript" src="../../Scripts/form.js">
    </script>
</head>
<body>
    <form id="form1" runat="server" name="frmData" method="post" >
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
    <input type="hidden" name="$node^9" value="" id="ouraction"/>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                      <td colspan="2">
                          <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                     </td>
             </tr>
            <tr>
                <td colspan="4" class="msgheader" bgcolor="#D5CDA4">
                    <asp:Label ID="lblSelectClaimant" runat="server" Text="<%$ Resources:lblSelectClaimantResrc %>"></asp:Label>
                </td>
            </tr>
        </table>
     <table colspan="4" border="0" cellspacing="0" celpadding="0" width="100%">
         <tr class="ctrlgroup">
           <asp:GridView ID="GridView1" runat="server" HeaderStyle-CssClass="ctrlgroup" AutoGenerateColumns ="false" 
           AllowSorting ="true" OnRowCommand = "GridView1_OnRowCommand" OnRowDataBound="GridView1_RowDataBound" Width = "100%"
           >
         <Columns>
         
            <asp:TemplateField HeaderText="ClaimantID" visible="false" >
                <ItemTemplate>
                    <asp:Label ID="lblClaimantEID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimantRowId")%>'></asp:Label>
                </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="<%$ Resources:gvHdrClaimantNameResrc %>"  >
                <ItemTemplate>
                    <asp:LinkButton ID="lblClaimantName" runat="server" CommandArgument = 'ClaimantName' Text='<%# string.Concat(DataBinder.Eval(Container.DataItem, "ClaimantFirstName"), " ", DataBinder.Eval(Container.DataItem, "ClaimantLastName")) %>'></asp:LinkButton>
                </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="<%$ Resources:gvHdrClaimantsClaimResrc %>"  >
                <ItemTemplate>
                    <asp:Label ID="lblClaimNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimNumber")%>'></asp:Label>
                    <input type="hidden" id="hdClaimantLOB" name="$node^1" value='<%# DataBinder.Eval(Container.DataItem, "LOB")%>' runat="server" />
                </ItemTemplate>
             </asp:TemplateField>
              </Columns>
           </asp:GridView>
           </tr>
       </table>
    <input type="hidden" name="$node^7" value="" id="SysWindowId"><input type="hidden"
        name="$instance" value="H4sIAAAAAAAAAHVSTW/CMAw9d78CcQczuKGsF9gBbZomsT8QpQYimgTZZoV/v7Rd6ce6nOz37Gfn&#xA;JWrnWbQ3OLm53PP6dgjk+GV6ErmsAYqimBereaAjLBeLJdT0NH1KkmQSj3IousqqtILYEKKf2SwF&#xA;snx2mgUJPikcCZk/giDDHnM0ssm1dQrahr7QlZFK1LBR0CSDWVHRhrIXBswXsrzh/R94CY8rVHBh&#xA;fRaKUl9BG1cUPK5YL2UHitpI3CD2/QZ99t3688gaHEhmJuRX50epQFn0bIzJtOiZ3C/4hz3FpZEo&#xA;EKcHnTNGYztQv7gGWxOiv7ZN9rWrO38InZJtMFeHXgZSr98R223T55WCJp70S+rXxqx67+7QrqSC&#xA;5iumP26dLbmVAgAA"></form>
</body>
</html>
