﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.ProgressNoteService;
using Riskmaster.ServiceHelpers;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;

namespace Riskmaster.UI.ProgressNotes
{
    public partial class SelectClaimant : NonFdmFormBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            //Amandeep MultiLingual Changes --start
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("SelectClaimant.aspx"), "SelectClaimantValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "SelectClaimantValidationsScripts", sValidationResources, true);
            //Amandeep MultiLingual Changes --end

            if (!IsPostBack)
            {
                ProgressNoteBusinessHelper pn = null;
                SelectClaimantObject selectClmntObj = new SelectClaimantObject();
                selectClmntObj.EventID = AppHelper.GetQueryStringValue("EventID");
                selectClmntObj.LOB = AppHelper.GetQueryStringValue("LOB");
                selectClmntObj.NewRecord = "AdvanceSearch";  
                try
                {
                    pn = new ProgressNoteBusinessHelper();
                    selectClmntObj = pn.SelectClaimant(selectClmntObj);
                    GridView1.DataSource = selectClmntObj.objClaimantList;
                    GridView1.DataBind();

                }
                catch (FaultException<RMException> ee)
                {
                
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                }
            }
        }
        protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument == "ClaimantName")
            {
                LinkButton lnkButton = (LinkButton)e.CommandSource;
                GridViewRow row = (GridViewRow)lnkButton.NamingContainer;
                string strClaimantId = ((Label)row.FindControl("lblClaimantEID")).Text;
                string strClaimantName = ((LinkButton)row.FindControl("lblClaimantName")).Text;
                string strClaimantLOB = ((HtmlInputHidden)row.FindControl("hdClaimantLOB")).Value;

                //Modified to no longer use obsolete RegisterClientScriptBlock method
                PageHelper.GetClientScriptManager(this.Page).RegisterClientScriptBlock(this.GetType(), "selectClaimantScript",
                    String.Format("<Script>CallClaimantParentPage(\"{0}\",\"{1}\",\"{2}\")</Script>", strClaimantName, strClaimantId, strClaimantLOB));
                Response.Write("GridView1_OnRowCommand");
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
    }
}
