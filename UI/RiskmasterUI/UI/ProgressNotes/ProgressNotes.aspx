﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressNotes.aspx.cs"
    Inherits="Riskmaster.UI.ProgressNotes.ProgressNotes" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/ProgressNotes.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript">
    
    </script>

    <script language="JavaScript" type="text/javascript">
        window.onresize = winResize;
        var enhancednotestimer;  //kkaur8:MITS 30838
        function winResize() {
            if (!document.all) {
                var winHeight = window.innerHeight;
                document.getElementById("div_NotesGrid").style.height = (winHeight * 0.43) + 'px';

            }
        }
        <%--aanandpraka2: Start Changes forr pop up getting resized every time--%>
        //self.resizeBy(0, 60);
        <%--aanandpraka2: End Changes--%>
        <%--gbindra : MITS#34104 START--%>
        $(document).ready(function () {
            $('#selNotesLevel').find('option').each(function () {
                $(this).attr('title', $(this).text());
            });
        });
        <%--gbindra : MITS#34104 END--%>

        //rsolanki2: mits 23102

        function scrollInitial() {
            var temp = document.getElementById('hdCurrentNode').value;
            if (temp < 2) {
                var myIframe = document.getElementById('div_NotesGrid');
                myIframe.scrollTop = 0;
            }
        }

        //rsolanki2: Enhc Notes ajax updated. this function is callde to select the clicked node and initiate the ajax call.
        function ClientSelectRow(event) {
            var temp;
            var temp2;
            var temp3;
            var i = 0;
            var val = 0;
            var clickedRowIndex;
            var clickedRowNumber;
            var selectedNode = document.getElementById('hdCurrentNode').value;
            temp3 = igtbl_getActiveRow("UltraWebGrid1");

            //srajindersin: 12/28/2012 Changes for MITS 27384(to fix arrow keys record change event)
            //if (window.event.keyCode == 40)
            if (event.keyCode == 40)
                clickedRowNumber = temp3.FirstRow.rowIndex + 1;
            else if (event.keyCode == 38)//else if (window.event.keyCode == 38)
                clickedRowNumber = temp3.FirstRow.rowIndex - 1;
            else
                clickedRowNumber = temp3.FirstRow.rowIndex;

            clickedRowIndex = clickedRowNumber - 1;
            temp2 = igtbl_getRowById('UltraWebGrid1_r_' + (clickedRowIndex))
            if (selectedNode != clickedRowIndex) {
                // selecting new row 
                temp2.FirstRow.style.backgroundColor = "#dddddd"; //

                document.getElementById('hdCurrentNode').value = clickedRowIndex;

                if (document.getElementById('UpProgressbar') != null) {
                    document.getElementById('UpProgressbar').style.display = "block";
                }
                if (document.getElementById('NoteMemoDiv')) {
                    document.getElementById('NoteMemoDiv').style.display = "none";
                }

                // deselecting previous row 

                temp = igtbl_getRowById('UltraWebGrid1_r_' + (selectedNode));
                if (temp != null) {

                    temp.FirstRow.style.backgroundColor = "";
                    val = temp.FirstRow.cells.length;

                    for (i == 0; i < val; i++) {
                        temp.FirstRow.cells[i].style.backgroundColor = "";
                        temp2.FirstRow.cells[i].style.backgroundColor = "#dddddd";

                    }
                }

                // scrolling into view using Infragistics methods
                //                var rowTemp = igtbl_getElementById('UltraWebGrid1_r_8');                
                //                igtbl_scrollToView("UltraWebGrid1", rowTemp);

                //loading Note details                
                //document.getElementById('lbUpdate').click();
                //kkaur8 :02/22/2013  MITS 30838 Start 
                //__doPostBack('lbUpdate', '');
                clearTimeout(enhancednotestimer);
                enhancednotestimer = setTimeout("__doPostBack('lbUpdate','')", 1000);
                //kkaur8 End
                //scroll Notes div into view using JS
                var myIframe = document.getElementById('div_NotesGrid');
                if (clickedRowIndex > 1) {
                    //Ankit Start : Worked for MITS - 33494 (Enhanced Notes Not Centered, scrolls after selecting a note)
                    //myIframe.scrollTop = 11 + clickedRowIndex * 15;
                    //Ankit End

                } else {
                    myIframe.scrollTop = 0;
                }

            }

        }
        function getPrintOption() {
            if (document.forms[0].claimprogressnoteid.value == '') {
                //alert('There is no note to be printed.');
                alert(ProgressNotesValidations.ValidNoNoteForPrint);
                return false;
            }
            else {
                var rdOptionList = eval('document.forms[0].rdbtnPrintNotes');
                var sRadioValue = document.getElementById('hdnPrintOption');
                for (var iItems = 0; iItems < rdOptionList.length; iItems++) {
                    if (rdOptionList[iItems].checked) {
                        sRadioValue.value = rdOptionList[iItems].value;
                        break;
                    }
                }
                if (sRadioValue.value == "2" && document.forms[0].NoteType_codelookup.value == '') {
                    //alert("Note Type cannot be blank!");
                    alert(ProgressNotesValidations.ValidIsBlank);
                    return false;
                }

            }
        }

        function setTitle() {


            document.title = document.getElementById("hdHeader").value; //MITS 28588 hlv 8/3/12 .replace(/\[ampersand\]/gi, "&")
            if (window.opener != null) { window.opener.window.parent.parent.iwintype = document.title; } //MITS 27964 hlv 4/10/2012
        }
        //mbahl3 mits 30513
        function selNotesLevel_IndexChange() {

            var FormName = document.getElementById("formname").value;

            if (FormName == 'piwitness' || FormName == 'piphysician' || FormName == 'pidriver' || FormName == 'piother'
            || FormName == 'pimedstaff' || FormName == 'piemployee' || FormName == 'pipatient' || FormName == 'PiInjury') {
                var formsubtitle = window.opener.document.getElementById('formsubtitle');
                var sformsubtitle = "";
                if (formsubtitle != null) {
                    sformsubtitle = formsubtitle.innerText || formsubtitle.textContent;
                }
                else {
                    formsubtitle = window.opener.document.getElementById('caption');
                    if (formsubtitle != null) {
                        sformsubtitle = formsubtitle.value;
                    }
                }

                var commentsTitle = "Event Enhanced Notes";
                var headertext = "";
                if ((sformsubtitle != "") && (sformsubtitle != undefined))
                    headertext = escape(commentsTitle) + " " + escape(sformsubtitle.replace(/\&/gi, '^@'));
                else
                    headertext = escape(commentsTitle);

                document.getElementById("hdnCaption").value = headertext;

            }
        }
        //MITS 27964 hlv 4/10/2012 begin
        window.onunload = function () {
            if (window.opener != null) {
                window.opener.window.parent.parent.iwintype = "";
            }
        }
        //MITS 27964 hlv 4/10/2012 end

        // atavaragiri MITS 28264 //
        function pagenavigate(event) {
            if (event.keyCode == 13) {
                //start sanoopsharma  - JIRA 9136
                var maxPageNo = document.getElementById("hdTotalNumberOfPages").value;
                var pageNo = document.getElementById("txtPageNumber").value;
                if (pageNo > maxPageNo || pageNo <= 0) {
                    alert(ProgressNotesValidations.ValidPageNo);
                }
                else
                    //end sanoopsharma
                    document.getElementById('btnShow').click();

            }

        } // END MITS 28264


    </script>

</head>
<!--mbahl3 Changed for MITS 30513 Starts-->
<!--body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();setTitle();if(document.getElementById('div_NotesScreen') != null){GridScrolltoView();winResize();scrollInitial();}"-->
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();setTitle();if(document.getElementById('div_NotesScreen') != null){GridScrolltoView();winResize();scrollInitial();};showHideElements();">
    <!--mbahl3 Changed for MITS 30513 Ends-->
    <%--onload="return OnLoad();">--%>
    <form id="frmData" name="frmData" method="post" runat="server">
        <asp:LinkButton ID="lbUpdate" runat="server" Style="display: block;"></asp:LinkButton>
        <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
        <input type="hidden" name="$node^20" value="DateEnteredForSort" id="SortCol" />
        <input type="hidden" name="$node^22" value="False" id="Ascending" />
        <%--Neha :MITS 21704: Williams Policy Enhc Notes updates start--%>
        <input type="hidden" name="effectivedate" value="" id="effectivedate" />
        <%--Neha:MITS 21704 : Williams Policy Enhc Notes updates end--%>
        <input type="hidden" name="$node^24" value='<%=Request.QueryString["FormName"]%>'
            id="formname" runat="server" />
        <input type="hidden" name="$node^43" value='<%=Request.QueryString["FormName"]%>'
            id="SysFormName" runat="server" />
        <%--csingh7 :Added for MITS 19333--%>
        <input type="hidden" name="$node^19" value='<%=Request.QueryString["ClaimID"]%>'
            id="claimid" runat="server" />
        <input type="hidden" name="$node^48" value='<%=Request.QueryString["ClaimantId"]%>'
            id="hdClaimantId" runat="server" /><!--Added by gbindra MITS#34104 02112014 WWIG GAP15-->
        <input type="hidden" name="$node^18" value='<%=Request.QueryString["EventID"]%>'
            id="eventid" runat="server" />
        <input type="hidden" name="$node^29" value="true" id="applysort" runat="server" /><%--Parijat :EnhancedMDI--%>
        <input type="hidden" name="$node^25" value='<%=Request.QueryString["EventNumber"]%>'
            id="eventnumber" runat="server" />
        <%--Williams neha goel:MITS 21704--%>
        <input type="hidden" name="$node^44" value='<%=Request.QueryString["PolicyId"]%>'
            id="policyid" runat="server" />
        <input type="hidden" name="$node^45" value='<%=Request.QueryString["PolicyName"]%>'
            id="policyname" runat="server" />
        <input type="hidden" name="$node^46" value='<%=Request.QueryString["PolicyNumber"]%>'
            id="policynumber" runat="server" />
        <input type="hidden" name="$node^47" value="" id="NoteParentType" runat="server" />
        <input type="hidden" id="hdnViewPolicyNotesInClaim" value="" runat="server" />
        <%--Williams neha goel:MITS 21704--%>
        <!--willimas-retrofit neha goel:start:MITS 21704-->
        <!-- pmittal5 Mits 21514-->
        <input type="hidden" name="$node^21" value='<%=Request.QueryString["LOB"]%>'
            id="LOB" runat="server" />
        <!--willimas-retrofit neha goel:end:MITS 21704-->
        <input type="hidden" name="$node^26" value="ProgressNotesAdaptor.OnLoad" id="functiontocall" />
        <%--<input type="hidden" name="$node^27" value="" id="claimprogressnoteid" runat="server" />--%>
        <input type="hidden" name="$node^9" value="" id="ouraction" runat="server" />
        <input type="hidden" name="$node^28" value="" id="activatefilter" runat="server" />
        <input type="hidden" name="$node^31" value="" id="claimidlist" runat="server" />
        <input type="hidden" name="$node^49" value="" id="claimantlistid" runat="server" /><!--Added by gbindra MITS#34104 WWIG GAP15-->
        <input type="hidden" name="$node^32" value="" id="usertypelist" runat="server" />
        <input type="hidden" name="$node^33" value="" id="notetypelist" runat="server" />
        <input type="hidden" name="$node^34" value="" id="enteredbylist" runat="server" />
        <input type="hidden" name="$node^35" value="" id="activityfromdate" runat="server" />
        <input type="hidden" name="$node^36" value="" id="activitytodate" runat="server" />
        <input type="hidden" name="$node^37" value="" id="txtnotestextcontains" runat="server" />
        <input type="hidden" name="$node^38" value="" id="sSortBy" runat="server" />
        <input type="hidden" name="$node^39" value="" id="claimant" runat="server" />
        <input type="hidden" name="$node^40" value="" id="PrintOrder1" runat="server" />
        <input type="hidden" name="$node^41" value="" id="PrintOrder2" runat="server" />
        <input type="hidden" name="$node^42" value="" id="PrintOrder3" runat="server" />
        <input type="hidden" name="$node^43" value="" id="subjectlist" runat="server" /><!--zmohammad MITS 30218-->
        <asp:TextBox runat="server" Style="display: none" ID="hdHeader" />
        <!-- Start by Shivendu for MITS 18098 -->
        <input type="hidden" value="" id="hdTotalNumberOfPages" runat="server" />
        <input type="hidden" value="" id="hdPageNumber" runat="server" />
        <input type="hidden" value="" id="hdSortColumn" runat="server" />
        <input type="hidden" value="" id="hdDirection" runat="server" />        
        <!-- End by Shivendu for MITS 18098 -->
        <!--Start by jramkumar for MITS 23076 on dt 04/25/2013-->
        <%--<asp:HiddenField ID="TemplateId" runat="server" />
     <asp:HiddenField ID="TemplateName" runat="server" />--%>
        <!--Ended by jramkumar for MITS 23076 on dt 04/25/2013-->
        <%--Parijat :12821  for showing claimant--%>
        <input type="hidden" id="txtSubmit" value="" runat="server" />
        <input type="hidden" id="freezetext" value="" runat="server" />
        <%--changes done for 12334--%>
        <%--changes done for 14626 : start--%>
        <input type="hidden" id="showDateStamp" value="" runat="server" />
        <%--changes done for 14626 : end--%>
        <input type="hidden" id="username" value="" runat="server" />
        <%--changes done for 12334--%>
        <input type="hidden" id="hdNewRecord" value="" runat="server" />
        <%--Parijat :start Post Editable Enhanced Notes --%>
        <input type="hidden" id="hdnEnhancedTimeLimit" runat="server">
        <input type="hidden" id="hdnUserEditingRights" value="" runat="server" />
        <%--Parijat :end Post Editable Enhanced Notes --%>
        <input type="hidden" id="hdCurrentNode" value="" runat="server" />
        <%--Added by Amitosh for mits 23473 (05/11/2011)--%>
        <input type="hidden" id="hdSortPref" value="" runat="server" />
        <%--Added by akaushik5 for MITS 30789 Starts--%>
        <input type="hidden" name="sOrderBy" value="" id="sOrderBy" runat="server" />
        <input type="hidden" id="hdOrderPref" value="" runat="server" />
        <%--Added by akaushik5 for MITS 30789 Ends--%>
        <asp:TextBox Style="display: none" runat="server" ID="hdnCaption" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="hdnPIEId" Text="" />
        <!--tanwar2 - Print HTML Notes - start -->
        <input type="hidden" id="btnPrintHtmlClick" value="" runat="server" />
        <%--tanwar2 - Print HTML Notes - end --%>
        <input type="hidden" value="" id="hdPIParent" runat="server" /><%--RMA-17480--%>

        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" />
        <table width="100%">
            <tr>
                <td colspan="2">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td colspan="2">
                                <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="msgheader" bgcolor="#D5CDA4">
                                <%if (hdHeader.Text == "")
                                    {%>Enhanced Notes<%}
                                                   else
                                                   {%>
                                <%=hdHeader.Text.Replace("[ampersand]", "&") /* MITS 28588 hlv 8/3/12 */ %><%} %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div id="div_NotesScreen" runat="server">
            <div style="position: relative; width: 99.3%; height: 51%; overflow: auto" class="divScroll" id="div_NotesGrid">
                <!-- rkaur27 : MITS 36119 -->
                <table width="97%" cellspacing="0" cellpadding="0" border="0">
                    <!-- Start by Shivendu for MITS 18098 -->
                    <tr>
                        <td align="left" colspan="2">
                            <asp:LinkButton ID="lnkFirst" Text="<%$ Resources:lnkFirstResrc %>" runat="server" OnClick="lnkFirst_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkPrev" Text="<%$ Resources:lnkPrevResrc %>" runat="server" OnClick="lnkPrev_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkNext" Text="<%$ Resources:lnkNextResrc %>" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                            <asp:LinkButton ID="lnkLast" Text="<%$ Resources:lnkLastResrc %>" runat="server" OnClick="lnkLast_Click"></asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           <asp:Label ID="lblPageDetails" runat="server"></asp:Label>
                            &nbsp;&nbsp;
           <!-- atavaragiri MITS 28264 -->
                            <asp:TextBox ID="txtPageNumber" runat="server" Width="5%" onkeydown="javascript:pagenavigate(event);"></asp:TextBox>
                            <asp:Button ID="btnShow" runat="server" OnClick="Show_Click" Text="<%$ Resources:btnShowResrc %>" Style="display: none" />
                            <!-- end MITS 28264 -->

                        </td>
                    </tr>
                    <!-- End by Shivendu for MITS 18098 -->
                    <!--Client side event tag added by Shivendu for MITS 18420 -->
                    <tr class="ctrlgroup">
                        <igtbl:UltraWebGrid ID="UltraWebGrid1" runat="server" OnColumnMove="UltraWebGrid1_ColumnMove" OnInitializeLayout="UltraWebGrid1_InitializeLayout" Width="100%"
                            OnSortColumn="OnSort" Browser="UpLevel">
                            <%--Jira RMA-7918--%>
                            <DisplayLayout Version="4.00" CellClickActionDefault="RowSelect" BorderCollapseDefault="Separate"
                                RowHeightDefault="20px" SelectTypeRowDefault="Extended" RowSelectorsDefault="No"
                                TableLayout="Fixed" AllowColumnMovingDefault="OnServer" HeaderClickActionDefault="SortSingle"
                                AllowSortingDefault="Yes" AutoGenerateColumns="false" GridLinesDefault="Both" RowStyleDefault-BorderStyle="Solid"
                                RowStyleDefault-BorderWidth="1px" HeaderStyleDefault-CssClass="GridViewHeader" HeaderStyleDefault-Cursor="Hand"
                                ClientSideEvents-CellClickHandler="ClientSelectRow" HeaderStyleDefault-Font-Bold="true" RowStyleDefault-Wrap="true">
                                <ClientSideEvents KeyDownHandler="SelectRowOnKeyPress(event)" />
                                <%--KeyDownHandler="SelectOnlyOneRadioKeyPress(event)" --%>
                            </DisplayLayout>
                            <Bands>
                                <igtbl:UltraGridBand AddButtonCaption="Table1" BaseTableName="Table1" Key="Table1">
                                    <Columns>

                                        <igtbl:TemplatedColumn HeaderText="Activity Date" BaseColumnName="DateEntered" Key="Activity Date" Width="11%">
                                            <CellTemplate>
                                                <asp:Label ID="lblActivityDate" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "DateEntered"),System.Globalization.CultureInfo.CreateSpecificCulture(AppHelper.GetCulture()).DateTimeFormat).ToShortDateString()%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="Attached To" BaseColumnName="AttachedTo" Key="Attached To" Width="11%">
                                            <CellTemplate>
                                                <asp:Label ID="lblAttachedTo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AttachedTo")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="Note Type" BaseColumnName="NoteTypeCode" Key="Note Type" Width="11%">
                                            <CellTemplate>
                                                <asp:Label ID="lblNoteType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NoteTypeCode")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <%-- mits 14738--%>
                                        <igtbl:TemplatedColumn HeaderText="Note Text" BaseColumnName="NoteMemoTrimmed" Key="Note Text" Width="11%">
                                            <CellTemplate>
                                                <asp:Label ID="lblNoteText1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NoteMemoTrimmed")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>

                                        <igtbl:TemplatedColumn HeaderText="Entered By" Key="Entered By" BaseColumnName="AdjusterName" Width="11%">
                                            <CellTemplate>
                                                <asp:Label ID="lblEnteredBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AdjusterName")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="<%$ Resources:hdrClaimProgressNoteIdResrc %>" Key="ClaimProgressNoteId" BaseColumnName="ClaimProgressNoteId" Hidden="true">
                                            <CellTemplate>
                                                <asp:Label ID="lblClaimProgressNoteId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClaimProgressNoteId")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <%-- <igtbl:TemplatedColumn HeaderText="EnteredBy"  Key="DateCreated" BaseColumnName="DateCreated" Hidden="true">
                                    <CellTemplate>
                                        <asp:Label ID="lblDateCreated" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DateCreated")%>'></asp:Label>
                                    </CellTemplate>
                                </igtbl:TemplatedColumn>--%>
                                        <igtbl:TemplatedColumn HeaderText="Date Created" Key="DateCreated" BaseColumnName="DateCreated" Width="10%">
                                            <CellTemplate>
                                                <asp:Label ID="lblDateCreated" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "DateCreated"),System.Globalization.CultureInfo.CreateSpecificCulture(AppHelper.GetCulture()).DateTimeFormat).ToShortDateString() %>'></asp:Label>
                                                <%--mbahl3 mits 31348--%>
                                                <%--<asp:Label ID="lblTimeCreated" runat="server"  Text='<%# DataBinder.Eval(Container.DataItem, "TimeCreated")%>'></asp:Label>--%>
                                                <%--mbahl3 mits 31348--%>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <%--  <igtbl:TemplatedColumn HeaderText="TimeCreated"  Key="TimeCreated" BaseColumnName="TimeCreated" Hidden="true">
                                    <CellTemplate>
                                        <asp:Label ID="lblTimeCreated" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TimeCreated")%>'></asp:Label>
                                    </CellTemplate>
                                </igtbl:TemplatedColumn>--%>
                                        <igtbl:TemplatedColumn HeaderText="Time Created" Key="TimeCreated" BaseColumnName="TimeCreated" Width="10%" SortIndicator="Disabled">
                                            <CellTemplate>
                                                <asp:Label ID="lblTimeCreated" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TimeCreated")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="NoteTypeCodeId" Key="NoteTypeCodeId" BaseColumnName="NoteTypeCodeId" Hidden="true">
                                            <CellTemplate>
                                                <asp:Label ID="lblNoteTypeCodeId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NoteTypeCodeId")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="NoteMemoCareTech" Key="NoteMemoCareTech" BaseColumnName="NoteMemoCareTech" Hidden="true">
                                            <CellTemplate>
                                                <asp:Label ID="lblNoteMemoCareTech" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NoteMemoCareTech")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <%-- <igtbl:TemplatedColumn HeaderText="UserTypeCode"  Key="UserTypeCode" BaseColumnName="UserTypeCode" Hidden="true">
                                    <CellTemplate>
                                        <asp:Label ID="lblUserTypeCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserTypeCode")%>'></asp:Label>
                                    </CellTemplate>
                                </igtbl:TemplatedColumn>--%>
                                        <igtbl:TemplatedColumn HeaderText="User Type" Key="UserTypeCode" BaseColumnName="UserTypeCode" Width="11%">
                                            <CellTemplate>
                                                <asp:Label ID="lblUserTypeCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserTypeCode")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="<%$ Resources:hdrUserTypeCodeIdResrc %>" Key="UserTypeCodeId" BaseColumnName="UserTypeCodeId" Hidden="true">
                                            <CellTemplate>
                                                <asp:Label ID="lblUserTypeCodeId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserTypeCodeId")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="<%$ Resources:hdrTemplateIdResrc %>" Key="TemplateId" BaseColumnName="TemplateId" Hidden="true">
                                            <CellTemplate>
                                                <asp:Label ID="lblTemplateId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TemplateId")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:TemplatedColumn HeaderText="Subject" Key="Subject" BaseColumnName="Subject" Width="14%">
                                            <CellTemplate>
                                                <asp:Label ID="lblSubject" runat="server" Text='<%# GetMaxLenString(DataBinder.Eval(Container.DataItem, "Subject"), 30)%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>

                                        <igtbl:TemplatedColumn HeaderText="<%$ Resources:hdrSubjectResrc %>" Key="TemplateName" BaseColumnName="TemplateName" Hidden="true">
                                            <CellTemplate>
                                                <asp:Label ID="lblTemplateName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TemplateName")%>'></asp:Label>
                                            </CellTemplate>
                                        </igtbl:TemplatedColumn>
                                    </Columns>
                                </igtbl:UltraGridBand>
                            </Bands>
                        </igtbl:UltraWebGrid>
                    </tr>

                    <tr>
                        <td align="center" class="PadLeft4" visible="false" id="tdNoNotes" runat="server">
                            <br />
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="lblNoNotesToShow" runat="server" Text="<%$ Resources:lblNoNotesToShowResrc %>" /><br />
                            <br />
                            <br />
                            &nbsp;

                    <script type="text/javascript">
                        UpdateRecordFlag('false');
                    </script>

                        </td>
                    </tr>
                </table>
            </div>

            <div style="height: 0.5%"></div>
            <div style="height: 34%">
                <asp:UpdatePanel ID="upProgressNotes" runat="server" OnLoad="UpdatePanel1_Load" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!--Start by jramkumar for MITS 23076 on dt 04/25/2013-->
                        <asp:HiddenField ID="TemplateId" runat="server" />
                        <asp:HiddenField ID="TemplateName" runat="server" />
                        <!--Ended by jramkumar for MITS 23076 on dt 04/25/2013-->
                        <asp:HiddenField ID="hdnEditButtonState" runat="server" Value="enabled" />
                        <asp:HiddenField ID="hdnDeleteButtonState" runat="server" Value="enabled" />
                        <input type="hidden" name="$node^27" value="" id="claimprogressnoteid" runat="server" />
                        <!--rjhamb Mits 21320:Note Type changes when you edit existing note-->
                        <input type="hidden" id="notetypecodeid" runat="server" />
                        <%--zmohammad MITS 30218--%>
                        <input type="hidden" value="" id="Subject" runat="server" />
                        <%--Added by amitosh for mits 23691(05/11/2011)--%>
                        <input type="hidden" value="" id="AttachedTo" runat="server" />
                        <input type="hidden" value="" id="UserTypeCode" runat="server" />
                        <input type="hidden" value="" id="NoteTypeCode" runat="server" />
                        <input type="hidden" value="" id="EnteredByName" runat="server" />
                        <input type="hidden" value="" id="TimeCreated" runat="server" />
                        <input type="hidden" value="" id="DateCreated" runat="server" />
                        <input type="hidden" value="" id="DateEntered" runat="server" />
                        <%--end Amitosh--%>
                        <!--rjhamb Mits 21320:Note Type changes when you edit existing note-->
                        <%--Commented by Amitosh for mits 23691 (05/11/2011)--%>
                        <%--<div id="divact" style="position: relative; float: left; width: 35%; height: 170px; overflow: auto"
            class="divScroll" >
           <table width="100%">
                <tr>
                    <%--<td width="70" align="left" valign="top">
                        Activity Date
                    </td>
                    <td width="5" valign="top">
                        :
                    </td>
                  <td id="DateEntered" runat="server" visible="false">
                      </td>
                </tr>
                <tr>
                    <%--<td align="left" valign="top">
                        Date Created
                    </td>
                    <td valign="top">
                        :
                    </td>
                    <td id="DateCreated" runat="server" visible="false">
                    </td>
                </tr>
                <tr>
                   <%-- <td align="left" valign="top">
                        Time Created
                    </td>
                    <td valign="top">
                        :
                    </td>
                    <td id="TimeCreated" runat="server" visible="false">
                    </td>
                </tr>
                <tr>
                   <%-- <td align="left" valign="top">
                        Entered By
                    </td>
                    <td valign="top">
                        :
                    </td>
                    <td id="EnteredByName" runat="server" visible="false">
                    </td>
                </tr>
                <tr>
                   <%-- <td align="left" valign="top">
                        Note Type
                    </td>
                    <td valign="top">
                        :
                    </td>
                    <td id="NoteTypeCode" runat="server" visible="false">
                    </td>
                </tr>
                <tr>
                    <%--<td align="left" valign="top">
                        User Type
                    </td>
                    <td valign="top">
                        :
                    </td>
                    <td id="UserTypeCode" runat="server" visible="false">
                    </td>
                </tr>
                <tr>
                    <%--<td align="left" valign="top" hidden="true">
                        Attached To
                    </td>
                    <td valign="top">
                        :
                    </td>
                    <td id="AttachedTo" runat="server" visible="false">
                    </td>
                </tr>
            </table>
        </div>--%>


                        <%-- </div>--%>
                        <%-- <asp:Label runat="server" Text="Loading...." id="UpProgressbar" style="display:none; color:Blue;"  />--%>
                        <%--aanandpraka2: Start Changes for pop up getting resized every time--%>
                        <%--<div id="Div1" style="float: left; position: relative; width: 99.5%;height:170px; overflow: auto" class="divScroll" runat="server"></div>    --%>
                        <%--akaushik5 Changed for MITS 32148 Starts --%>
                        <%-- <div id="NoteMemoDiv" style="float: left; position: relative; width: 99.5%;height:140px; overflow: auto" class="divScroll" runat="server"></div>    --%>
                        <%--akaushik5 Changed for MITS 30063 Removed pclass from div level--%>
                        <%--aravi5 fixes for Jira Id: 7149 Claim-> Enhanced Notes -> Create Note Button,  Issues in Button Border and Table Border in IE11--%>
                        <div id="NoteMemoDiv" style="float: left; position: relative; width: 99.5%; height: 100%; overflow: auto; margin-top: 3px;" class="divScroll" runat="server"></div>
                        <!-- rkaur27 : MITS 36119 -->
                        <%--akaushik5 Changed for MITS 32148 Ends --%>
                        <%--aanandpraka2:End changes--%>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="lbUpdate" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div style="height: 1%"></div>
            <table>
                <tr>
                    <td>
                        <asp:Button class="button" ID="btnPrintHtmlNotes" OnClientClick="return funcViewAsHtml('true')"
                            Text="Print HTML Notes" runat="server" />
                        &nbsp;
                <asp:Button class="button" ID="btnPrintNotes" OnClientClick="return getPrintOption()" OnClick="PrintNotes" Text="<%$ Resources:btnPrintNotesResrc %>" runat="server" />
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rdbtnPrintNotes" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$ Resources:rdbtnPrintNotes1Resrc %>" Selected="True" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:rdbtnPrintNotes2Resrc %>" Value="1"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:rdbtnPrintNotes3Resrc %>" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%--mbahl3 Added for MITS 30513 Starts --%>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Label1Resrc %>" class="required" /><!--Changed Text for Mits 18817-->
                        <uc1:CodeLookUp runat="server" CodeTable="NOTE_TYPE_CODE" ControlName="NoteType" ID="NoteType" Filter="CODES.RELATED_CODE_ID IN(NoteParentType,0)" />
                    </td>
                    <td>
                        <div class="half" id="divSelectNotesLevel">
                            <table>
                                <tr>
                                    <td class="label">
                                        <asp:Label Width="100" ID="lblNotesLevel" runat="server" Text="<%$ Resources:lblNotesLevelResrc %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="selNotesLevel" runat="server" Width="210" onchange="selNotesLevel_IndexChange()" OnSelectedIndexChanged="selNotesLevel_Change" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <%--mbahl3 Added for MITS 30513 Ends --%>
            </table>
            <div style="height: 6%">
                <asp:Button class="button" ID="btnAdvSearch" Text="<%$ Resources:btnAdvSearchResrc %>" runat="server"
                    OnClick="btnAdvSearch_Click" /><asp:Button class="button" ID="btnCreateNote" OnClientClick="return CreateNotes('true')"
                        Text="<%$ Resources:btnCreateNoteResrc %>" OnClick="CreateNote" runat="server" /><asp:Button class="button"
                            ID="btnEditNote" OnClientClick="return CreateNotes('false')" Text="<%$ Resources:btnEditNoteResrc %>"
                            OnClick="CreateNote" runat="server" /><asp:Button class="button" ID="btnDeleteNotes"
                                OnClientClick="return DeleteNote()" OnClick="DeleteNote" Text="<%$ Resources:btnDeleteNotesResrc %>" runat="server" /><asp:Button
                                    class="button" ID="btnViewNotes" Text="<%$ Resources:btnViewNotesResrc %>" OnClientClick="return funcViewAsHtml()"
                                    runat="server" /><asp:Button class="button" ID="btnBackToNote" Text="<%$ Resources:btnBackToNoteResrc %>"
                                        OnClick="MoveToAllNotes" runat="server" /><asp:Button class="button" ID="btnTemplates"
                                            Text="<%$ Resources:btnTemplatesResrc %>" runat="server" OnClick="btnTemplates_Click" />
            </div>





        </div>
        <div id="divChoices" runat="server" visible="false">
            <table>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="txtLabel" class="required" Text="<%$ Resources:txtLabelResrc %>"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton GroupName="Choices" runat="server" Checked="true" Text="<%$ Resources:rdr1Resrc %>" ID="rdr1" value="2" /></td>
                </tr>

                <tr>
                    <td>
                        <asp:RadioButton GroupName="Choices" runat="server" Text="<%$ Resources:rdr3Resrc %>" ID="rdr3" value="0" />
                    </td>
                </tr>


            </table>
            <asp:Button ID="btnChoices" runat="server" Text="<%$ Resources:btnChoicesResrc %>" UseSubmitBehavior="false" />
        </div>
        <input type="hidden" name="$node^7" value="rmx-widget-handle-3" id="SysWindowId" /><input
            type="hidden" name="$instance" value="" /><input type="hidden" id="hdnPrintOption"
                runat="server" />
    </form>
</body>
</html>
