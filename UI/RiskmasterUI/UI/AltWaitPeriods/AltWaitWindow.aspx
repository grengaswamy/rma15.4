﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AltWaitWindow.aspx.cs" Inherits="Riskmaster.UI.AltWaitPeriods.AltWaitWindow" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Alternate Waiting Period</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function AltWaitGrid_OK()
    {
        var bResult = false;
		
//	    if(getDataChanged()==false)
//	    {
//		    window.close();
//		    return true;
//	    }
	    if(AltWaitGrid_Validate())
	    {
	        return true;
	    }
	    else
	    {
		    return false;
	    }
    }
    
    function AltWaitGrid_Validate()
    {	
	    if(trim(document.getElementById('DisClndrWrkCode_codelookup').value) == '' ||
		   trim(document.getElementById('DisWaitPrd').value) == '' ||
		   trim(document.getElementById('DisPrdType_codelookup').value) == '' )
	    {
		    alert('Please enter all the values');
		    return false;
	    }
	    else
	    {
	        var anum=/(^\d+$)|(^\d+\.\d+$)/;
			var x = document.getElementById('DisWaitPrd').value;
			if (!anum.test(x))
			{
				alert('Please enter a valid number');
				document.getElementById('DisWaitPrd').focus();
				return false;
			}
			else
			{
		        return true;
		    }
	    }
    }
    
    function AltWaitGrid_onCancel()
    {
        var retval = false;
        window.close();
        retval = false;
        
////	    if(getDataChanged()==true)
////	    {
////		    if(ConfirmSave()==true)
////		    {
////		        if(AltWaitGrid_Validate())
////		        {
////			        retval = true;
////			    }
////			    else
////			    {
////			        retval = false;
////			    }
////    			
////		    }
////		    else
////		    {
////		         window.close();
////		        retval = false;
////		    }
    		
////	    }
////	    else
////	    {
////	        window.close();
////		    return false;
////	    }
////	    return retval;
    }
    
    function RefreshParent()
    {
        window.opener.document.forms[0].submit();
        window.close()
    }

    </script>
</head>
<body>
    <form id="frmData" runat="server">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
            
                <td colspan="2">
                <uc1:ErrorControl ID="ecErrorControl" runat="server" />
                    <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <b><u>Quantity</u></b>
                </td>
                <td>
                    <asp:TextBox id="DisWaitPrd" runat="server" TabIndex = "1" RMXRef="/Instance/Document//control[@name='DisWaitPrd']" onchange="setDataChanged(true);" />
                </td>
            </tr>
            <tr>
                <td>
                    <b><u>Period</u></b>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="DisClndrWrkCode" CodeTable="CALENDAR_WORK" ControlName="DisClndrWrkCode" RMXRef="/Instance/Document//control[@name='DisClndrWrkCode']" RMXType="code" tabindex="2" Required="true" />
                </td>
                &#160;
                <td>
                    <uc:CodeLookUp runat="server" ID="DisPrdType" CodeTable="DURATION_TYPE" ControlName="DisPrdType" RMXRef="/Instance/Document//control[@name='DisPrdType']" RMXType="code" tabindex="3" Required="true" />
                </td>
            </tr>  
            <tr>
				<td>
				    Disability Type
				</td>
				<td>                  
				    <uc:CodeLookUp runat="server" ID="DisTypeCode" CodeTable="BENEFIT_DIS_TYPE" ControlName="DisTypeCode" RMXRef="/Instance/Document//control[@name='DisTypeCode']" RMXType="code" tabindex="4" />
				</td>
			</tr>
			<tr>
			    <td colspan="2"><br/>
			    </td>
			</tr>
			<tr>
			    <td colspan="2">
				    <asp:button ID="btnOk" runat="server" Text="OK" CssClass="button" 
                        onClientClick="return AltWaitGrid_OK();"  style="width:60px" tabindex="5" 
                        onclick="btnOk_Click"/>
					&#160;
					<asp:button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" 
                        onClientClick="return AltWaitGrid_onCancel();"  style="width:60px" tabindex="6" 
                        onclick="btnCancel_Click"/>
				</td>
			</tr>
        </table>
        <asp:TextBox ID="hdnclassid" runat="server" RMXRef="/Instance/Document//control[@name='hdnclassid']" style="display:none" RMXType="id"></asp:TextBox>
	    <asp:TextBox id="DisWaitRowId" style="display:none" runat="server" RMXType="id" RMXRef="/Instance/Document//control[@name='DisWaitRowId']"/>
	    <asp:TextBox id="hdnaction" style="display:none" runat="server" RMXType="id" />
	    <asp:TextBox id="SysFormName" style="display:none" runat="server" RMXType="id" Text="AltWaitPrd"/>
	    <asp:TextBox id="FormMode" style="display:none" runat="server" RMXType="id" RMXRef="/Instance/Document//control[@name='FormMode']"/>
	    
    </form>
</body>
</html>
