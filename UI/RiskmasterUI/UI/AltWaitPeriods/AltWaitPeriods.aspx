﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AltWaitPeriods.aspx.cs" Inherits="Riskmaster.UI.AltWaitPeriods.AltWaitPeriods" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Alternate Waiting Period</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function Refresh()
    {
        document.forms[0].submit();
    }
    function SelectonEdit()
    {
        var radioSelect = document.getElementById('hdnSelectedId').value;
        if(radioSelect != "")
        {
            var inputs = document.getElementsByTagName("input");
	        for (i = 0; i < inputs.length; i++)
	        {
		        if ((inputs[i].type=="radio"))
		        {
    			    
				        selectedValue=inputs[i].value;
    				    
				        if(selectedValue==radioSelect)
				        {
				            inputs[i].checked = true;
				            document.getElementById('hdnSelectedId').value = "";
				        }
		        }
	        }
	    }
    }
    </script>
</head>
<body onload="SelectonEdit();">
    <form id="frmData" runat="server">
        <table width="100%" cellspacing="0" cellpadding="0">
		    <tr>
			    <td class="ctrlgroup">
				    Alternate Waiting Periods
			    </td>
			    <td>
			        <uc1:ErrorControl ID="ecErrorControl" runat="server" />
			    </td>
		    </tr>
	    </table>
	    <table width="100%">
		    <tr>
			    <td width="80%">
				    <div style="width:100%;height:150px;overflow:auto">
                        <asp:GridView ID="gvAltWaitGrid" runat="server"  AutoGenerateColumns="False" 
                            Font-Names="Tahoma" Font-Size="Smaller" 
                            GridLines="None" CssClass="singleborder" 
                            ondatabound="gvAltWaitGrid_DataBound" Width="403px">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <input  id="rdoAltWaitGrid" name="AltWaitGrid" type="radio" value='<%# Eval("WaitRowId")%>'/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="WaitRowId" HeaderText="RowId"/>
                                <asp:BoundField DataField="Quantity" HeaderText="Quantity"/>
                                <asp:BoundField DataField="Period" HeaderText="Period"/>
                                <asp:BoundField DataField="DisabilityType" HeaderText="Disability Type"/>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>
				    </div>
			    </td>
			    <td width="20%" valign="top">
			        <asp:ImageButton ID="gvAltWaitGrid_New" runat="server" Text="New" 
                        ImageUrl="~/Images/new.gif" ImageAlign="Middle"/>
                    <br />
        
                    <asp:ImageButton ID="gvAltWaitGrid_Edit" runat="server" Text="Edit" 
                        ImageUrl="~/Images/edittoolbar.gif" ImageAlign="Middle"/>
                    <br />
        
                    <asp:ImageButton ID="gvAltWaitGrid_Delete" runat="server" Text="Delete" 
                        ImageUrl="~/Images/delete.gif" ImageAlign="Middle" 
                        onclick="gvAltWaitGrid_Delete_Click"/>
			    </td>
		    </tr>
	    </table>
	    <asp:TextBox ID="hdnclassid" runat="server" style="display:none" RMXType="id"></asp:TextBox>
	    <asp:TextBox ID="hdnWaitRowId" runat="server" style="display:none" RMXType="id"></asp:TextBox>
	    <asp:TextBox ID="hdnSelectedId" runat="server" style="display:none" RMXType="id"></asp:TextBox>
	    <table>
	        <tr>
	            <td>
	                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();return false;" Text="Close" />
	            </td>
	        </tr>
	    </table>
    </form>
</body>
</html>
