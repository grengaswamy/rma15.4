﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.AltWaitPeriods
{
    public partial class AltWaitPeriods : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                XmlDocument XmlDoc = new XmlDocument();
                string sreturnValue = "";
                DataRow objRow;
                int iClassRowId = 0;
                if (!IsPostBack)
                {
                    gvAltWaitGrid.HeaderStyle.CssClass = "msgheader";
                    gvAltWaitGrid.RowStyle.CssClass = "datatd1";
                    gvAltWaitGrid.AlternatingRowStyle.CssClass = "datatd";

                    // Associate the Grid Buttons with the Attributes.
                    // We need to do this at run time as onmouseover and onmouseout attributes are not exposed for Imagebutton.
                    gvAltWaitGrid_New.Attributes.Add("onmouseover", "this.src='../../Images/new2.gif'");
                    gvAltWaitGrid_New.Attributes.Add("onmouseout", "this.src='../../Images/new.gif'");

                    // After the javascriopt function is called, we are returning false from here.
                    // as we do not want a postback here.

                    gvAltWaitGrid_New.Attributes.Add("onclick", "openGridAddEditWindow('AltWaitGrid','add','500','400');return false;");

                    gvAltWaitGrid_Edit.Attributes.Add("onmouseover", "this.src='../../Images/edittoolbar2.gif'");
                    gvAltWaitGrid_Edit.Attributes.Add("onmouseout", "this.src='../../Images/edittoolbar.gif'");
                    gvAltWaitGrid_Edit.Attributes.Add("onclick", "openGridAddEditWindow('AltWaitGrid','edit','500','400');return false;");

                    gvAltWaitGrid_Delete.Attributes.Add("onmouseover", "this.src='../../Images/delete2.gif'");
                    gvAltWaitGrid_Delete.Attributes.Add("onmouseout", "this.src='../../Images/delete.gif'");
                    gvAltWaitGrid_Delete.Attributes.Add("onclick", "validateGridForDeletion('AltWaitGrid');");

                    // Read the Values from Querystring
                    string sClassRowId = AppHelper.GetQueryStringValue("ClassRowId");


                    if (sClassRowId != "")
                    {
                        iClassRowId = Int32.Parse(sClassRowId);
                        hdnclassid.Text = sClassRowId;
                    }
                }
                else
                {
                    iClassRowId = Int32.Parse(hdnclassid.Text);

                }
                XmlTemplate = GetMessageTemplate(iClassRowId);
                bReturnStatus = CallCWSFunction("AltWaitPeriodsAdaptor.GetListAltWait", out sreturnValue, XmlTemplate);

                if (bReturnStatus)
                {
                    XmlDoc.LoadXml(sreturnValue);

                    XmlNode altWaitHeader = XmlDoc.SelectSingleNode("//AltWaitPeriods/List/listhead");

                    DataTable dtGridData = new DataTable();

                    foreach (XmlElement xmlElem in altWaitHeader)
                    {
                        dtGridData.Columns.Add(xmlElem.Name);
                    }

                    XmlNodeList altWaitData = XmlDoc.SelectNodes("//AltWaitPeriods/List/listrow");

                    foreach (XmlNode objNodes in altWaitData)
                    {
                        objRow = dtGridData.NewRow();
                        for (int i = 0; i < objNodes.ChildNodes.Count; i++)
                        {
                            objRow[i] = objNodes.ChildNodes[i].InnerText;
                        }
                        dtGridData.Rows.Add(objRow);
                    }

                    objRow = dtGridData.NewRow();
                    dtGridData.Rows.Add(objRow);

                    gvAltWaitGrid.DataSource = dtGridData;

                    gvAltWaitGrid.DataBind();

                }
                else
                {
                    ecErrorControl.errorDom = sreturnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        private XElement GetMessageTemplate(int iClassRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>AltWaitPeriodsAdaptor.GetListAltWait</Function></Call><Document><AltWaitPeriods><List>");
            sXml = sXml.Append("<listhead><WaitRowId>RowId</WaitRowId> <Quantity>Quantity</Quantity> <Period>Period</Period> <DisabilityType>");
            sXml = sXml.Append("Disability Type</DisabilityType></listhead></List><ClassRowId>");
            sXml = sXml.Append(iClassRowId);
            sXml = sXml.Append("</ClassRowId></AltWaitPeriods></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void gvAltWaitGrid_DataBound(object sender, EventArgs e)
        {
            gvAltWaitGrid.Columns[1].HeaderStyle.CssClass = "hiderowcol";
            gvAltWaitGrid.Columns[1].ItemStyle.CssClass = "hiderowcol";

            gvAltWaitGrid.Rows[gvAltWaitGrid.Rows.Count - 1].CssClass = "hiderowcol";
        }

        protected void gvAltWaitGrid_Delete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sReturnValue = "";

                XmlTemplate = GetDeleteMessageTemplate();
                bReturnStatus = CallCWS("AltWaitPeriodsAdaptor.Delete", XmlTemplate, out sReturnValue, false, false);

                if (bReturnStatus)
                {
                    string script = "<script>Refresh();</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
                }
                else
                {
                    ecErrorControl.errorDom = sReturnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        private XElement GetDeleteMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>AltWaitPeriodsAdaptor.Delete</Function></Call><Document><AltWaitPeriods><List>");
            sXml = sXml.Append("<listhead><WaitRowId>RowId</WaitRowId> <Quantity>Quantity</Quantity> <Period>Period</Period> <DisabilityType>");
            sXml = sXml.Append("Disability Type</DisabilityType></listhead></List><DisWaitRowId>");
            sXml = sXml.Append(hdnWaitRowId.Text);
            sXml = sXml.Append("</DisWaitRowId><ClassRowId>");
            sXml = sXml.Append(hdnclassid.Text);
            sXml = sXml.Append("</ClassRowId></AltWaitPeriods></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
