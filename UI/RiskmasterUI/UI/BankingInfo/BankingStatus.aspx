﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BankingStatus.aspx.cs" Inherits="Riskmaster.UI.BankingInfo.BankingStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Banking Status History</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <%--Yukti-ML Changes Start,MITS 34208--%>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>

    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="javascript" >
        //    YUkti-ML Changes End
        function fillDate() {
            //Mgaba2:Mits 10241: Current Date shouls be taken from server:Start
            //var today=new Date();
            //document.getElementById('changedate').value = today.getMonth() + 1 + "/" + today.getDate() + "/" + (today.getYear());-->
            document.getElementById('changedate').value = window.opener.document.getElementById('systemcurrentdate').value;
            //Mgaba2:Mits 10241:End 

        }

        function Print() {
            //Defect 001259 Fixed by Neelima Jan 2006
            //Not a good way
            document.getElementById('changedate').parentElement.parentElement.style.display = "none";
            document.getElementById('changedby').parentElement.parentElement.style.display = "none";
            document.getElementById('reason').parentElement.parentElement.style.display = "none";
            document.getElementById('approvedby').parentElement.parentElement.style.display = "none";
            document.getElementById('searchtype').parentElement.parentElement.style.display = "none";
            document.getElementById('btnOK').style.display = "none";
            document.getElementById('btnCancel').style.display = "none";
            document.getElementById('btnPrint').style.display = "none";
            self.print();
            document.getElementById('changedate').parentElement.parentElement.style.display = "";
            document.getElementById('changedby').parentElement.parentElement.style.display = "";
            document.getElementById('reason').parentElement.parentElement.style.display = "";
            document.getElementById('approvedby').parentElement.parentElement.style.display = "";
            document.getElementById('searchtype').parentElement.parentElement.style.display = "";
            document.getElementById('btnOK').style.display = "";
            document.getElementById('btnCancel').style.display = "";
            document.getElementById('btnPrint').style.display = "";
            return false;
        }

        function SaveValues() {

            var closeClaim = "";
            var objCtrlAllCodes;
            var element = document.getElementById('searchtype');
            if (element.options[element.selectedIndex].value == "0") {
                //Yukti-ML Changes Start,MITS 34208
                //alert('Please select some Banking Status');
                alert(BankingStatusValidations.SelectBankStatus);
                //Yukti-ML Changes End
                return false;
            }
            window.opener.document.getElementById('statuschangeapprovedby').value = document.getElementById('approvedby').value;
          //  window.opener.document.getElementById('dttmclosed').value = document.getElementById('changedate').value;
            // ABhateja, 06.29.2007
            // MITS 9946, Make sure that 'statuschangedate' gets updated.
            window.opener.document.getElementById('statuschangedate').value = document.getElementById('changedate').value;
            window.opener.document.getElementById('statuschangereason').value = document.getElementById('reason').value;
            window.opener.document.getElementById('bankingstatus_codelookup').value = document.getElementById('searchtype').options[document.getElementById('searchtype').selectedIndex].text;
            window.opener.document.getElementById('bankingstatus_codelookup_cid').value = document.getElementById('searchtype').value;

            //MITS 10241:Applying Module Security:Allow Backdating in Claim Status History-Start
            //BackdateClaimSetting is 0 if backdating is not allowed
       
                objcurdate = new Date(window.opener.document.getElementById('systemcurrentdate').value);
                objchangedate = new Date(window.opener.document.getElementById('statuschangedate').value);
                datediff = new Date();
                datediff = objcurdate - objchangedate;
                if (datediff > 0) {
                    alert("Changed Date " + window.opener.document.getElementById('statuschangedate').value + " is not permitted to be lesser than Today's Date " + window.opener.document.getElementById('systemcurrentdate').value);
                    document.forms[0].changedate.focus();
                    return false;
                }
          
            //MITS 10241-End            

           
           
            window.opener.EnableCloseMethod();
            window.opener.setDataChanged(true);
            window.close();
            return false;
        }
    </script>
</head>
<body onload="fillDate()">
    <form id="frmData" runat="server">
    <asp:TextBox ID="bankingrowid" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="triggerdate" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="curParams" runat="server" style="display:none"></asp:TextBox>
    <asp:scriptmanager id="SMgr" runat="server" />
     <table width="100%" cellspacing="0" cellpadding="0">
		<tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr>
		<tr class="msgheader">
		<!-- Label control added by Shivendu for MITS 16148 -->
        <%--Yukti-ML Changes Start,MITS 34208--%>
		    <%--<td colspan="2"><asp:Label ID="lblBankingStatusHistTitle" runat="server" Text="Label"></asp:Label></td>--%>
            <td colspan="2"><asp:Label ID="lblBankingStatusHistTitle" runat="server" Text="<%$ Resources:lblBankingStatusHistTitle %>"></asp:Label></td>
        <%--YUkti-ML Changes End--%>
		</tr>
	</table>
	<br/>
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td>	
            <%--Yukti-ML Changes Start,MITS 34208--%>
			    <%--<asp:label runat="server" class="label" id="lbl_status" text="Status:" />--%>
                <asp:label runat="server" class="label" id="lbl_status" text="<%$ Resources:lblStatus %>" />
            <%--Yukti-ML Changes End--%>
			</td>
			<td>
				<asp:DropDownList ID="searchtype" runat="server" rmxref="Instance/Document/BankingStatusHistory/StatusType"></asp:DropDownList>
 			</td> 
		</tr>
		<tr>
        <%--Yukti-ML Changes Start,MITS 34208--%>
		    <%--<td><asp:label runat="server" class="label" id="lbl_changedate" text="Date: " /></td>--%>
            <td><asp:label runat="server" class="label" id="lbl_changedate" text="<%$ Resources:lblChangedDt %>" /></td>
            <td>
                <asp:TextBox runat="server" FormatAs="date" id="changedate" RMXRef="Instance/Document/BankingStatusHistory/ChangeDate" RMXType="date"  onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
               <%-- <input type="button" class="DateLookupControl" name="changedatebtn" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					    {
					        inputField: "changedate",
					        ifFormat: "%m/%d/%Y",
					        button: "changedatebtn"
					    }
					    );
			    </script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#changedate").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                            //buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 
                    });
                    </script>
                    <%--Yukti-Ml Changes End--%>
			</td> 
		</tr>

		<tr>
        <%--Yukti-ML Changes Start,MITS 34208--%>
		    <%--<td><asp:label runat="server" class="label" id="lbl_changedby" text="Changed By: " /></td>--%>
            <td><asp:label runat="server" class="label" id="lbl_changedby" text="<%$ Resources:lblChangedBy %>" /></td>
        <%--yukti-ML Changes End--%>
			<td>
			    <asp:textbox runat="server" id="changedby" rmxref="Instance/Document/BankingStatusHistory/ChangedBy"  disabled="True"/>				
			</td>
		</tr>
		<tr>
        <%--Yukti-ML Changes Start,MITS 34208--%>
		    <%--<td><asp:label runat="server" class="label" id="lbl_approvedby" text="Approved By: " /></td>--%>
            <td><asp:label runat="server" class="label" id="lbl_approvedby" text="<%$ Resources:lblApprovedBy %>" /></td>
        <%--Yukti-ML Changes End--%>
			<td>
			    <asp:textbox runat="server" id="approvedby" rmxref="Instance/Document/BankingStatusHistory/ApprovedBy"  />				
			</td>
		</tr>
		<tr>
        <%--Yukti-ML Changes Start,MITS 34208--%>
		    <%--<td><asp:label runat="server" class="label" id="lbl_reason" text="Reason: " /></td>--%>
            <td><asp:label runat="server" class="label" id="lbl_reason" text="<%$ Resources:lblReason %>" /></td>
        <%--Yukti-ML Changes End--%>
			<td>
			    <asp:textbox runat="server" id="reason" rmxref="Instance/Document/BankingStatusHistory/Reason"  />				
			</td>
		</tr>
    </table> 
    <br/>
    <div class="divScroll" style="width: 100%; height: 42%;">
    <table width="100%" cellspacing="0" cellpadding="0">
		<tr class="ctrlgroup">
        <%--Yukti-ML Changes Start,MITS 34208--%>
	<%--	    <asp:GridView ID="GridViewBankStatus" AutoGenerateColumns="False" runat="server" Font-Bold="True" 
		    CellPadding="0" GridLines="None" CellSpacing="0" Width="100%"  EmptyDataText ="No Status History Available" >
		    <HeaderStyle CssClass="ctrlgroup"/>
            <rowstyle CssClass ="datatd" HorizontalAlign="Left" Font-Bold="false"  />
            <alternatingrowstyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />--%>

            <asp:GridView ID="GridViewBankStatus" AutoGenerateColumns="False" runat="server" Font-Bold="True" 
            CellPadding="0" GridLines="None" CellSpacing="0" Width="100%"  EmptyDataText ="<%$ Resources:lblNoStatusHist %>"> 
		    <HeaderStyle CssClass="ctrlgroup"/>
            <rowstyle CssClass ="datatd" HorizontalAlign="Left" Font-Bold="false"  />
            <alternatingrowstyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />
<%--Yukti-ML Changes End,MITS 34208--%>

                <Columns>
                <%--Yukti-ML Changes Start,MITS 34208--%>
                    <%--<asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >--%>
                    <asp:TemplateField HeaderText="<%$ Resources:gvHdrStatus %>" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("Status")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >--%>
                    <asp:TemplateField HeaderText="<%$ Resources:gvHdrDate %>" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("Date")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Changed By" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >--%>
                    <asp:TemplateField HeaderText="<%$ Resources:gvHdrChangedBy %>" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("ChangedBy")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Reason" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >--%>
                    <asp:TemplateField HeaderText="<%$ Resources:gvHdrReason %>" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                        <ItemTemplate>
                           <%# Eval("Reason")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                <%--Yukti-ML Changes End--%>
                </Columns> 
            </asp:GridView>     
		</tr>
	</table> 
    </div>
    
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
            <%--Yukti-ML Changes Start,MITS 34208--%>
			    <%--<asp:Button  class="button" id="btnOK" Text =" OK " runat="server" OnClientClick ="SaveValues();return false;" Width ="50px" />--%>
                <asp:Button  class="button" id="btnOK" Text ="<%$ Resources:btnOK %>" runat="server" OnClientClick ="SaveValues();return false;" Width ="50px" />
				&#160;
                <%--<asp:Button  class="button" id="btnCancel" Text ="Cancel" runat="server" OnClientClick ="window.close();" Width ="50px" />--%>
				<asp:Button  class="button" id="btnCancel" Text ="<%$ Resources:btnCancel %>" runat="server" OnClientClick ="window.close();" Width ="50px" />
				&#160;
				<%--<asp:Button  class="button" id="btnPrint" Text =" Print " runat="server" OnClientClick ="Print();return false;" Width ="50px" />--%>
                <asp:Button  class="button" id="btnPrint" Text =" <%$ Resources:btnPrint %>" runat="server" OnClientClick ="Print();return false;" Width ="50px" />
            <%--YUkti-ML Changes End--%>
			</td>
		</tr>
	</table>
    </form>
</body>
</html>
