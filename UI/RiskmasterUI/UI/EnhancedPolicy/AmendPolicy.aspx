﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AmendPolicy.aspx.cs" Inherits="Riskmaster.UI.EnhancedPolicy.AmendPolicy" %>

<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagPrefix="uc2" TagName="ErrorControl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Amend Policy</title>
    <base target="_self" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/rmnet.css" type="text/css" />
    <%--<script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/form.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />
    <%-- aravi5 RMA-8170 Chrome Issue - Getting an error while saving the issue date of Policy Management(ShowModalDialog) Starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
    </script>
    <%-- aravi5 RMA-8170 Chrome Issue - Getting an error while saving the issue date of Policy Management(ShowModalDialog) Ends --%>
</head>
<body onpagehide="HideOverlaydiv(); return false;" onload="AmendPolicyOnLoad()">
    <%-- aravi5 RMA-8170 --%>
    <form id="frmData" runat="server">
    <uc2:errorcontrol id="ErrorControl1" runat="server" />
    <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSamend_date" id="TABSamend_date">
            <a class="Selected" href="#" runat="server" rmxref="" name="amend_date" id="LINKTABSamend_date">
                Amend Date</a>
        </div>
        <div class="tabSpace" runat="server" id="TABamend_date">
            <nbsp />
            <nbsp />
        </div>
    </div>
    <div class="tabSpace">
        &nbsp;</div>
    <div class="singletopborder" runat="server" name="FORMTABamend_date" id="FORMTABamend_date">
        <div runat="server" id="div_TransDateTitle" xmlns="">
            <asp:label runat="server" class="label" id="TransDateTitle" text="What date would you like the transaction to be effective?" />
        </div>
        <div class="tabSpace">
            &nbsp;</div>
        <div class="tabSpace">
            &nbsp;</div>
        <div runat="server" id="div_TransDate" xmlns="">
            <asp:label runat="server" class="label" id="lbl_TransDate" text="Transaction Date" />
            <span class="formw">
                <asp:textbox runat="server" formatas="date" id="TransDate" rmxref="/Instance/Document/AmendTransDate/TransDate"
                    rmxtype="date" tabindex="11" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                <%--<asp:button class="DateLookupControl" runat="server" id="TransDatebtn" tabindex="12" />
            <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "TransDate",
					ifFormat : "%m/%d/%Y",
					button : "TransDatebtn"
					}
					);
				</script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#TransDate").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                            //buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "12");
                    });
                </script>
            </span>
        </div>
        <div class="formButton" runat="server" id="div_btnOk">
            <script language="JavaScript" src="">{var i;}
            </script>
            <asp:button class="button" runat="server" id="btnOk" rmxref="" text="OK" cssclass="button"
                onclick="btnOk_Click" usesubmitbehavior="false" onclientclick="javascript: AmendPolicyOk();" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
            <script language="JavaScript" src="">{var i;}
            </script>
            <asp:button class="button" runat="server" id="btnCancel" rmxref="" text="Cancel "
                onclientclick="return AmendPolicyCancel();" />
        </div>
    </div>
    <asp:textbox style="display: none" runat="server" id="FunctionToCall" value="EnhancePolicyAdaptor.AmendTransactionDate"
        rmxref="" rmxtype="hidden" text="" />
    <asp:textbox style="display: none" runat="server" id="Validate" value="" rmxref=""
        rmxtype="hidden" text="" />
    </form>
</body>
</html>
