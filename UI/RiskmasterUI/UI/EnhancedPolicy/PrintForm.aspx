﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintForm.aspx.cs" Inherits="Riskmaster.UI.EnhancedPolicy.PrintForm" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Print Forms</title>

    <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>

    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload = "return PrintPageLoaded();">

     <form id="frmData" runat="server" method="post">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <uc2:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr class="msgheader">
            <td colspan="2">Selected Forms</td>
        </tr>
        <%--<tr class="ctrlgroup">
            <td colspan="3">
            </td>
        </tr>--%>
    </table>    
    <table width="100%" border="0" id="selectedPrintForms">
	</table>
    <asp:TextBox Style="display: none" runat="server" name="policyid" ID="policyid" />   
    </form>
</body>
</html>
