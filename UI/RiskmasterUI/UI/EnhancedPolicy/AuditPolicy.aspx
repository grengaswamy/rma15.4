﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditPolicy.aspx.cs" Inherits="Riskmaster.UI.UI.EnhancedPolicy.AuditPolicy" %>

<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid" TagPrefix="dg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Audit Policy</title>
    <base target="_self"/>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>
    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload="AuditPolicyOnLoad()">
    <form id="frmData" runat="server">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <uc2:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSAuditPolicy" id="TABSAuditPolicy">
            <a class="Selected" href="#" runat="server" rmxref="" name="AuditPolicy" id="LINKTABSAuditPolicy">
                Select Term to Audit..</a>
        </div>
        <div class="tabSpace" runat="server" id="TABFORMTABAuditPolicySpace">
            <nbsp />
            <nbsp />
        </div>
    </div>
    <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 580px;height: 200px; overflow: auto">
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABAuditPolicy" id="FORMTABAuditPolicy">
            <tr>
                <td colspan="5" width="100%">
                <dg:UserControlDataGrid runat="server" ID="AuditPolicyGrid" GridName="AuditPolicyGrid" GridTitle="" Target="/Document/TermList" Ref="/Document/TermList" Unique_Id="UniqueTermNumber" ShowRadioButton="True" ShowHeader="True" LinkColumn="" PopupWidth="100" PopupHeight="100" Type="Grid" />
                </td>
            </tr>
        </table>
    </div>
    <table>
        <tr>
            <td>
                <asp:Button class="button" ID="btnOK" Text=" OK " runat="server" OnClientClick="return AuditPolicyOK();"
                    Width="50px"  />
            </td>
            <td>
                <asp:Button class="button" ID="btnCancel" Text=" Cancel " runat="server" OnClientClick="window.close();"
                    Width="50px" />
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" name="FunctionToCall" ID="FunctionToCall" Text="" />
    <asp:TextBox Style="display: none" runat="server" name="validate" ID="validate" Text="" />   
    <asp:TextBox Style="display: none" runat="server" name="PolicyId" ID="PolicyId" />
    <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="hTabName" />
    <asp:TextBox Style="display: none" runat="server" name="Selected" ID="Selected" />
    <uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />    
    </form>
</body>
</html>
