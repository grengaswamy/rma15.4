﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml;
namespace Riskmaster.UI.EnhancedPolicy
{
    public partial class PrintPolicy : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sreturnValue = "";
                XElement XmlTemplate=null ;
                bool bReturnStatus;

                if (!IsPostBack)
                {
                    policyid.Text = Request.QueryString["PolicyID"];
                    XmlTemplate = GetMessageTemplate("EnhancePolicyAdaptor.GetForms");
                    bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.GetForms", out sreturnValue, XmlTemplate);
                    BindingToGrid(sreturnValue);
                }
                else
                {
                    if (hdnAction.Text == "Add")
                    {
                        XmlTemplate = GetMessageTemplate("EnhancePolicyAdaptor.GetForms");
                        bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.GetForms", out sreturnValue, XmlTemplate);
                        BindingToGrid(sreturnValue);
                        DisplayFormIds.Text = "";
                        hdnAction.Text = "";                        
                    }
                }            
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate(string sFunctionToCall)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append(sFunctionToCall);
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><formDetails><policyid>");
            sXml = sXml.Append(policyid.Text);
            sXml = sXml.Append("</policyid><selectedFormIds>");
            sXml = sXml.Append(selectedformids.Text);
            sXml = sXml.Append("</selectedFormIds><SelectedFormNames/><DisplayFormIds>");
            sXml = sXml.Append(DisplayFormIds.Text);
            sXml = sXml.Append("</DisplayFormIds></formDetails></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string sreturnValue = "";
            XElement XmlTemplate = null;
            bool bReturnStatus;
            XmlTemplate = GetMessageTemplate("EnhancePolicyAdaptor.SaveForms");
            bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.SaveForms", out sreturnValue, XmlTemplate);
            BindingToGrid(sreturnValue);
            selectedformids.Text = "";
            DisplayFormIds.Text = "";
        }

        private void BindingToGrid(string sOutXml)
        {
            DataTable objTable = new DataTable();
            XmlDocument XmlDoc = new XmlDocument();
            DataRow objRow;
            XmlNode xNode = null;

            XmlDoc.LoadXml(sOutXml);
            XmlNodeList FormList = XmlDoc.SelectNodes("//FormName");
            objTable.Columns.Add("FormId");
            objTable.Columns.Add("FormTitle");
            objTable.Columns.Add("HdnPolicyId");
            for (int i = 0; i < FormList.Count; i++)
            {
                xNode = FormList[i];
                objRow = objTable.NewRow();
                objRow["FormId"] = xNode.ChildNodes[0].InnerText;
                objRow["FormTitle"] = xNode.ChildNodes[1].InnerText;
                objRow["HdnPolicyId"] = xNode.ChildNodes[2].InnerText;
                objTable.Rows.Add(objRow);

            }
            objTable.AcceptChanges();
            GridDisplayForms.Visible = true;
            GridDisplayForms.DataSource = objTable;
            GridDisplayForms.DataBind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string sreturnValue = "";
            XElement XmlTemplate = null;
            bool bReturnStatus;
            XmlTemplate = GetMessageTemplate("EnhancePolicyAdaptor.DeleteForms");
            bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.DeleteForms", out sreturnValue, XmlTemplate);
            BindingToGrid(sreturnValue);
            selectedformids.Text = "";
            DisplayFormIds.Text = "";
        }
    }
}