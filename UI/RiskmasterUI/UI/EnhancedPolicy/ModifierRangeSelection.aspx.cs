﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Web.UI.HtmlControls;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.UI.EnhancedPolicy
{
    public partial class ModifierRangeSelection : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        bool bReturnStatus = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    hdnExpRateRowId.Text = AppHelper.GetQueryStringValue("ExpRateRowId");
                    hdnModifierId.Text = AppHelper.GetQueryStringValue("ModifierId");
                }
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.ModifierRangeSelection", XmlTemplate);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.ModifierRangeSelection</Function></Call><Document><Document><FunctionToCall/><ModifierRangeSelection><ExpRateRowID>");
            sXml = sXml.Append(hdnExpRateRowId.Text);
            sXml = sXml.Append("</ExpRateRowID></ModifierRangeSelection></Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
