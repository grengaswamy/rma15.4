﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReinstatePolicy.aspx.cs"
    Inherits="Riskmaster.UI.EnhancedPolicy.ReinstatePolicy" %>

<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reinstate Policy</title>
    <base target="_self"/>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <%--vkumar258 - RMA-6037 - Starts --%>
   <%-- <script src="/RiskmasterUI/Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <script type="text/javascript" src="/RiskmasterUI/Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>

     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
     <script type="text/javascript" src="../../Scripts/form.js">        { var i; } </script>
       <%--vkumar258 - RMA-6037 - End --%>

    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" src="/RiskmasterUI/Scripts/EnhPolicy.js"></script>

    <uc1:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body onload="ReinstateDateOnLoad()">
    <form id="frmData" runat="server">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <uc2:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSReinstateDetails" id="TABSReinstateDetails">
            <a class="Selected" href="#" runat="server" rmxref="" name="ReinstateDetails" id="LINKTABSReinstateDetails">
                Please enter the Reinstatement details..</a>
        </div>
        <div class="tabSpace" runat="server" id="TABCancelDetailsSpace">
            <nbsp />
            <nbsp />
        </div>
    </div>
    <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 390px;
        height: 150px; overflow: auto">
        <table border="0" cellspacing="0" cellpadding="0" name="FORMTABReinstateDetails"
            id="FORMTABReinstateDetails">
            <tr>
                <td>
                    <asp:RadioButton runat="server" ID="rdoWithLapse" RMXType="radio" onclick="OnClickReinsRdoLapse(this);"
                        Text="With lapse" Checked="true" GroupName="reinsRadGroup" />
                </td>
                <td>
                    <asp:RadioButton runat="server" ID="rdoWithoutLapse" RMXType="radio" onclick="OnClickReinsRdoLapse(this);"
                        Text="Without lapse" GroupName="reinsRadGroup" />
                </td>
            </tr>
            <tr id="DateRow">
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_ReinstateDate" Text="Reinstate Date:" />&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="ReinstateDate" RMXType="date" TabIndex="2"
                        onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                    
                      <%--vkumar258 - RMA-6037 - Starts --%> 
                    <%-- <asp:Button class="DateLookupControl" runat="server" ID="ReinstateDatebtn" TabIndex="3" />
                  <script type="text/javascript">
				            Zapatec.Calendar.setup(
				            {
				            inputField : "ReinstateDate",
				            ifFormat : "%m/%d/%Y",
				            button : "ReinstateDatebtn"
				            }
				            );
                    </script>--%>

                     <script type="text/javascript">
                         $(function () {
                             $("#ReinstateDate").datepicker({
                                 showOn: "button",
                                 buttonImage: "../../Images/calendar.gif",
                                 //buttonImageOnly: true,
                                 showOtherMonths: true,
                                 selectOtherMonths: true,
                                 changeYear: true
                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "3");
                         });
                    </script>
                      <%--vkumar258 - RMA-6037 - End --%>
                </td>
            </tr>
        </table>
    </div>
    <table>
        <tr>
            <td>
                <asp:Button class="button" ID="btnOK" Text=" OK " runat="server" OnClientClick="return ReinstatePolicyOnOk();"
                    Width="50px" OnClick="btnOK_Click" />
            </td>
            <td>
                <asp:Button class="button" ID="btnCancel" Text=" Cancel " runat="server" OnClientClick="window.close();"
                    Width="50px" />
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" runat="server" name="FunctionToCall" ID="FunctionToCall"
        Text="" />
    <asp:TextBox Style="display: none" runat="server" name="validate" ID="validate" Text="" />
    <asp:TextBox Style="display: none" runat="server" name="EffDate" ID="EffDate" />
    <asp:TextBox Style="display: none" runat="server" name="PolicyId" ID="PolicyId" />
    <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="hTabName" />
    <uc3:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />
    </form>
</body>
</html>
