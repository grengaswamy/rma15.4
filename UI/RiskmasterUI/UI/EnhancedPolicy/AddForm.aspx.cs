﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Text;

namespace Riskmaster.UI.EnhancedPolicy
{
    public partial class AddForm : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable objTable = new DataTable();
                XmlNode xNode;
                DataRow objRow;
                XmlDocument XmlDoc = new XmlDocument();
                string sreturnValue = "";
                if (!IsPostBack)
                {
                    XElement XmlTemplate = GetMessageTemplate();
                    bool bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.LoadAddForms", out sreturnValue, XmlTemplate);
                    XmlDoc.LoadXml(sreturnValue);
                    XmlNodeList FormList = XmlDoc.SelectNodes("//FormName");
                    objTable.Columns.Add("FormId");
                    objTable.Columns.Add("FormTitle");

                    for (int i = 0; i < FormList.Count; i++)
                    {
                        xNode = FormList[i];
                        objRow = objTable.NewRow();
                        objRow["FormId"] = xNode.ChildNodes[0].InnerText;
                        objRow["FormTitle"] = xNode.ChildNodes[1].InnerText;
                        objTable.Rows.Add(objRow);

                    }
                    objTable.AcceptChanges();
                    GridDisplayForms.Visible = true;
                    GridDisplayForms.DataSource = objTable;
                    GridDisplayForms.DataBind();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("EnhancePolicyAdaptor.LoadAddForms");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><AvailableFormNames/></Document></Document></Message>");           
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
