﻿//Author    : Anu Tennyson
//Date      : 12/21/09
//MITS      : 18229

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.Common;


namespace Riskmaster.UI.UI.EnhancedPolicy
{
    public partial class Deductible : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = string.Empty;
            string sNavFormName = string.Empty;
            string sLob = string.Empty;
            string sOrg = string.Empty;
            string sEffdate = string.Empty;
            string sExpdate = string.Empty;
            string sFlatPercent = string.Empty;
            int iFlatPercent = 0;
            bool bIsSuccess = false;
            int iStateId = 0;
            int iCoverageTypeId = 0;
            if (!IsPostBack)
            {
                gvDeductibleList.HeaderStyle.CssClass = "msgheader";
                gvDeductibleList.RowStyle.CssClass = "datatd1";
                gvDeductibleList.AlternatingRowStyle.CssClass = "datatd";
                sNavFormName = AppHelper.GetQueryStringValue("prvformname");
                sLob = AppHelper.GetQueryStringValue("lob");
                sOrg = AppHelper.GetQueryStringValue("org");
                sEffdate = AppHelper.GetQueryStringValue("effdate");
                sExpdate = AppHelper.GetQueryStringValue("expdate");
                if (!(String.IsNullOrEmpty(AppHelper.GetQueryStringValue("state"))))
                    iStateId = Int32.Parse(AppHelper.GetQueryStringValue("state"));
                if (!(String.IsNullOrEmpty(AppHelper.GetQueryStringValue("coverageid"))))
                    iCoverageTypeId = Int32.Parse(AppHelper.GetQueryStringValue("coverageid"));

                XmlTemplate = GetMessageTemplate(sLob, sOrg, sEffdate, sExpdate, iStateId, iCoverageTypeId);
                bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.GetDeductible", out sreturnValue, XmlTemplate);
                XmlDocument objFlatPerc = new XmlDocument();
                objFlatPerc.LoadXml(sreturnValue);
                if (objFlatPerc.SelectSingleNode("//Deductibles/FlatOrPercentid") != null)
                {
                   iFlatPercent = Conversion.CastToType<int>(objFlatPerc.SelectSingleNode("//Deductibles/FlatOrPercentid").InnerText, out bIsSuccess);              
                }
                if (objFlatPerc.SelectSingleNode("//Deductibles/FlatOrPercent") != null)
                {
                   sFlatPercent = objFlatPerc.SelectSingleNode("//Deductibles/FlatOrPercent").InnerText;
                }
                TextBox txtFlatPerc = (TextBox)this.FindControl("flatorpercent");
                txtFlatPerc.Text = iFlatPercent.ToString();
                //Start: Neha Suresh Jain,05/11/2010,MITS 20772,to get short codes for flat/percent
                TextBox txtFlatPerS = (TextBox)this.FindControl("flatper");
                if (txtFlatPerS != null)
                {
                    txtFlatPerS.Text = sFlatPercent;
                }
                //End: Neha Suresh Jain

                if (bReturnStatus)
                {
                    XmlDoc.LoadXml(sreturnValue);
                    XmlNodeList deductibleRecords = XmlDoc.SelectNodes("/ResultMessage/Document/Deductibles/Deductible");
                    if (deductibleRecords.Count != 0)
                    {
                        DataTable dtGridData = new DataTable();
                        DataColumn dcGridColumn;
                        BoundField bfGridColumn;
                        ButtonField butGridLinkColumn;
                        XmlNode xNode;
                        DataRow objRow;
                        string sCssClass = string.Empty;

                        // Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();
                        dcGridColumn.ColumnName = "RangeType"; //index 0
                        dtGridData.Columns.Add(dcGridColumn);
                        butGridLinkColumn = new ButtonField();
                        // Associate the Grid Bound Column with the Column of the Datasource
                        butGridLinkColumn.DataTextField = dcGridColumn.ColumnName;
                        // Assign the HeaderText
                        butGridLinkColumn.HeaderText = dcGridColumn.ColumnName;
                        // Create the hyperlink for the column
                        butGridLinkColumn.ButtonType = ButtonType.Link;
                        gvDeductibleList.Columns.Add(butGridLinkColumn);
                        // Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();
                        dcGridColumn.ColumnName = "Modifier"; //index 1
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn = new BoundField();
                        // Associate the Grid Bound Column with the Column of the Datasource
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        // Assign the HeaderText
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        // Add the newly created bound field to the GridView. 
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        // Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();
                        dcGridColumn.ColumnName = "Deductible"; //index 2
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn = new BoundField();
                        // Associate the Grid Bound Column with the Column of the Datasource
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        // Assign the HeaderText
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        // Add the newly created bound field to the GridView. 
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        // Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();
                        dcGridColumn.ColumnName = "Amount"; //index 3
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn = new BoundField();
                        // Associate the Grid Bound Column with the Column of the Datasource
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        // Assign the HeaderText
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        // Add the newly created bound field to the GridView. 
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        // Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();
                        dcGridColumn.ColumnName = "Beginning Range"; //index 4
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn = new BoundField();
                        // Associate the Grid Bound Column with the Column of the Datasource
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        // Assign the HeaderText
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        // Add the newly created bound field to the GridView. 
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        // Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();
                        dcGridColumn.ColumnName = "End Range"; //index 5
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn = new BoundField();
                        // Associate the Grid Bound Column with the Column of the Datasource
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        // Assign the HeaderText
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        // Add the newly created bound field to the GridView. 
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        // Column to be associated with Datasource of grid
                        dcGridColumn = new DataColumn();
                        dcGridColumn.ColumnName = "Rate Row Id"; //index 6
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn = new BoundField();
                        // Associate the Grid Bound Column with the Column of the Datasource
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        // Assign the HeaderText
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        // Add the newly created bound field to the GridView. 
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        //rupal:start
                        //this is actually part of Multicurrency Enh
                        //these columns hold non-formatted real values
                        dcGridColumn = new DataColumn();
                        bfGridColumn = new BoundField();
                        dcGridColumn.ColumnName = "MODIFIER_VALUE"; //index 7
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn = new BoundField();                        
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        gvDeductibleList.Columns.Add(bfGridColumn);

                        dcGridColumn = new DataColumn();
                        bfGridColumn = new BoundField();
                        dcGridColumn.ColumnName = "DEDUCTIBLE_VALUE"; //index 8
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        

                        dcGridColumn = new DataColumn();
                        bfGridColumn = new BoundField();
                        dcGridColumn.ColumnName = "AMOUNT_VALUE"; //index 9
                        dtGridData.Columns.Add(dcGridColumn);
                        bfGridColumn.DataField = dcGridColumn.ColumnName;
                        bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                        gvDeductibleList.Columns.Add(bfGridColumn);
                        //rupal:end

                        for (int i = 0; i < deductibleRecords.Count; i++)
                        {
                            xNode = deductibleRecords[i];
                            objRow = dtGridData.NewRow();
                            objRow["RangeType"] = xNode.Attributes["RANGE_TYPE"].Value;
                            objRow["Modifier"] = xNode.Attributes["MODIFIER"].Value;
                            objRow["Amount"] = xNode.Attributes["AMOUNT"].Value;
                            objRow["Deductible"] = xNode.Attributes["DEDUCTIBLE"].Value;
                            objRow["Beginning Range"] = xNode.Attributes["BEGINNING_RANGE"].Value;
                            objRow["End Range"] = xNode.Attributes["END_RANGE"].Value;
                            objRow["Rate Row Id"] = xNode.Attributes["RATE_ROWID"].Value;

                            //rupal:start, mits 27502
                            objRow["MODIFIER_VALUE"] = xNode.Attributes["MODIFIER_VALUE"].Value;
                            objRow["DEDUCTIBLE_VALUE"] = xNode.Attributes["DEDUCTIBLE_VALUE"].Value;
                            objRow["AMOUNT_VALUE"] = xNode.Attributes["AMOUNT_VALUE"].Value;
                            //rupal:end

                            dtGridData.Rows.Add(objRow);
                        }
                        gvDeductibleList.DataSource = dtGridData;
                        gvDeductibleList.DataBind();
                        gvDeductibleList.Visible = true;

                        //start:Added by Neha Suresh Jain,03/10/2010
                        if (dtGridData.Rows.Count > 0)
                        {

                            if (dtGridData.Rows[0]["RangeType"].ToString() == "Deductible")
                            {
                                gvDeductibleList.Columns[2].Visible = true;
                                gvDeductibleList.Columns[4].Visible = false;
                                gvDeductibleList.Columns[5].Visible = false;
                            }
                            else if (dtGridData.Rows[0]["RangeType"].ToString() == "Range")
                            {
                                gvDeductibleList.Columns[2].Visible = false;
                                gvDeductibleList.Columns[4].Visible = true;
                                gvDeductibleList.Columns[5].Visible = true;
                            }
                        }
                        //End:Neha Suresh Jain

                    }
                    else
                    {
                        gvDeductibleList.Visible = false;
                        btnCancel.Visible = false;
                        lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No Deductible found for the selection!</h3></p>";
                    }
                }
                else
                {
                    gvDeductibleList.Visible = false;
                    btnCancel.Visible = false;
                    lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No Deductible found for the selection!</h3></p>";
                }
            }
        }

        private XElement GetMessageTemplate(string sLob, string sOrg, string sEffdate, string sExpdate, int iStateId, int iCoverageTypeId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetDeductible</Function></Call><Document><Document><Lob>");
            sXml = sXml.Append(sLob);
            sXml = sXml.Append("</Lob><Org>");
            sXml = sXml.Append(sOrg);
            sXml = sXml.Append("</Org><Effdate>");
            sXml = sXml.Append(sEffdate);
            sXml = sXml.Append("</Effdate><Expdate>");
            sXml = sXml.Append(sExpdate);
            sXml = sXml.Append("</Expdate><State>");
            sXml = sXml.Append(iStateId);
            sXml = sXml.Append("</State><CoverageTypeId>");
            sXml = sXml.Append(iCoverageTypeId);
            sXml = sXml.Append("</CoverageTypeId>");
            sXml = sXml.Append("</Document></Document></Message>");

            XElement oTemplate = XElement.Parse(Convert.ToString(sXml));
            return oTemplate;
        }
        protected void gvDeductibleList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                TableCell tcGridCell;
                LinkButton lbLinkColumn;
                tcGridCell = e.Row.Cells[0];
                lbLinkColumn = (LinkButton)tcGridCell.Controls[0];
                // href is assigned the value # to stop the postback on click.
                lbLinkColumn.Attributes.Add("href", "#");
                
                //rupal:start, mits 27502
                //this should be actually part of multicurrency enh
                //lbLinkColumn.Attributes.Add("onclick", "selectDeductible('" + e.Row.Cells[1].Text + "','" + e.Row.Cells[2].Text + "','" + lbLinkColumn.Text + "','" + e.Row.Cells[3].Text + "','" + e.Row.Cells[6].Text + "');");
                lbLinkColumn.Attributes.Add("onclick", "selectDeductible('" + e.Row.Cells[7].Text + "','" + e.Row.Cells[8].Text + "','" + lbLinkColumn.Text + "','" + e.Row.Cells[9].Text + "','" + e.Row.Cells[6].Text + "');");
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
            }
            //rupal:end
        }
        protected void gvDeductibleList_DataBound(object sender, EventArgs e)
        {
            gvDeductibleList.Columns[0].HeaderStyle.CssClass = string.Empty;
            gvDeductibleList.Columns[0].ItemStyle.CssClass = string.Empty;   
            gvDeductibleList.Columns[3].HeaderStyle.CssClass = "hiderowcol";
            gvDeductibleList.Columns[3].ItemStyle.CssClass = "hiderowcol"; 
            gvDeductibleList.Columns[6].HeaderStyle.CssClass = "hiderowcol";
            gvDeductibleList.Columns[6].ItemStyle.CssClass = "hiderowcol"; 
        }
    }
}
