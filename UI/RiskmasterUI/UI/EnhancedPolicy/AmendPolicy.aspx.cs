﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.EnhancedPolicy
{
    public partial class AmendPolicy : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Start - Yukti-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }

        }

        protected void btnOk_Click(object sender, EventArgs e)
        {            
            try
            {
                bool bReturnStatus = false;
                XElement oMessageElement = null;
                XElement oSessionCmdElement = null;
                string sreturnValue = "";
                string sPolicyId = string.Empty;
                string sTransDate = string.Empty;

                oMessageElement = GetMessageTemplate();

                if (AppHelper.GetQueryStringValue("PolicyID") != null)
                {
                    sPolicyId = AppHelper.GetQueryStringValue("PolicyID");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/AmendTransDate/PolicyID");
                    oSessionCmdElement.Value = sPolicyId;
                }
                if (TransDate.Text != null)
                {
                    sTransDate = TransDate.Text;
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/AmendTransDate/TransDate");
                    oSessionCmdElement.Value = sTransDate;
                }
                
                bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.AmendTransactionDate", out sreturnValue ,oMessageElement);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sreturnValue);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    Validate.Text = "true";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }       
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization> 
                    <Call>
                        <Function>EnhancePolicyAdaptor.AmendTransactionDate</Function> 
                    </Call>
                    <Document>
                        <AmendTransDate>
                            <TransDate></TransDate> 
                            <PolicyID></PolicyID> 
                        </AmendTransDate>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }



    }
}
