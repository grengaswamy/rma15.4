﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;

namespace Riskmaster.UI.EnhancedPolicy
{
    public partial class PayPlan : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            if (!IsPostBack)
            {
                gvPayPlanList.HeaderStyle.CssClass = "msgheader";
                gvPayPlanList.RowStyle.CssClass = "datatd1";
                gvPayPlanList.AlternatingRowStyle.CssClass = "datatd";
                // Read the Values from Querystring
                string sQuoteType = AppHelper.GetQueryStringValue("pqtype");
                string sQuoteState = AppHelper.GetQueryStringValue("pqstate");

                if (sQuoteType != "" && sQuoteState != "" )
                {
                    XmlTemplate = GetMessageTemplate(sQuoteType, sQuoteState);
                    bReturnStatus = CallCWSFunction("EnhancePolicyAdaptor.GetPayPlans", out sreturnValue, XmlTemplate);

                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);

                        XmlNodeList payplanRecords = XmlDoc.SelectNodes("/ResultMessage/Document/PayPlans/PayPlan");
                        if (payplanRecords.Count != 0)
                        {
                            DataTable dtGridData = new DataTable();
                            DataColumn dcGridColumn;
                            BoundField bfGridColumn;
                            ButtonField butGridLinkColumn;
                            XmlNode xNode;
                            DataRow objRow;
                            string sCssClass = string.Empty;

                            // PayPlan Row Id

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "PayPlan RowId";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPayPlanList.Columns.Add(bfGridColumn);

                            // PayPlan Code

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "Code";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPayPlanList.Columns.Add(bfGridColumn);

                            // PayPlan Description

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "Description";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            butGridLinkColumn = new ButtonField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            butGridLinkColumn.DataTextField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            butGridLinkColumn.HeaderText = dcGridColumn.ColumnName;

                            // Create the hyperlink for the column
                            butGridLinkColumn.ButtonType = ButtonType.Link;

                            gvPayPlanList.Columns.Add(butGridLinkColumn);

                            // BillingRule Row Id

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            dcGridColumn.ColumnName = "BillingRule RowId";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPayPlanList.Columns.Add(bfGridColumn);


                            // BillingRule Code

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            dcGridColumn.ColumnName = "BillingRule Code";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPayPlanList.Columns.Add(bfGridColumn);

                            // BillingRule Desc

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            dcGridColumn.ColumnName = "BillingRule Desc";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPayPlanList.Columns.Add(bfGridColumn);

                            // LOB

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "LOB";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPayPlanList.Columns.Add(bfGridColumn);

                            // State

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            dcGridColumn.ColumnName = "State";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPayPlanList.Columns.Add(bfGridColumn);

                            
                          


                            for (int i = 0; i < payplanRecords.Count; i++)
                            {
                                xNode = payplanRecords[i];
                                objRow = dtGridData.NewRow();
                                objRow["PayPlan RowId"] = xNode.Attributes["Pay_Plan_RowId"].Value;
                                objRow["Code"] = xNode.Attributes["Pay_Plan_ShortCode"].Value;
                                objRow["Description"] = xNode.Attributes["Pay_Plan_CodeDesc"].Value;
                                objRow["BillingRule RowId"] = xNode.Attributes["Billing_Rule_RowId"].Value;
                                objRow["BillingRule Code"] = xNode.Attributes["Billing_Rule_ShortCode"].Value;
                                objRow["BillingRule Desc"] = xNode.Attributes["Billing_Rule_CodeDesc"].Value;
                                objRow["LOB"] = xNode.Attributes["LOB"].Value;
                                objRow["State"] = xNode.Attributes["State"].Value;
                                dtGridData.Rows.Add(objRow);
                            }
                            gvPayPlanList.DataSource = dtGridData;
                            gvPayPlanList.DataBind();
                        }
                        else
                        {
                            gvPayPlanList.Visible = false;
                            btnCancel.Visible = false;
                            lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No PayPlan codes found for selected State and LOB!</h3></p>";
                        }

                    }
                }
                else
                {
                    gvPayPlanList.Visible = false;
                    btnCancel.Visible = false;
                    lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No PayPlan codes found for selected State and LOB!</h3></p>";
                }
            }

        }

        private XElement GetMessageTemplate(string sQuoteType, string sQuoteState)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>EnhancePolicyAdaptor.GetPayPlans</Function></Call><Document><Document><PQType>");
            sXml = sXml.Append(sQuoteType);
            sXml = sXml.Append("</PQType><PQState>");
            sXml = sXml.Append(sQuoteState);
            sXml = sXml.Append("</PQState></Document></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void gvPayPlanList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                TableCell tcGridCell;
                LinkButton lbLinkColumn;

                tcGridCell = e.Row.Cells[2];
                lbLinkColumn = (LinkButton)tcGridCell.Controls[0];

                // href is assigned the value # to stop the postback on click.
                lbLinkColumn.Attributes.Add("href", "#");

                lbLinkColumn.Attributes.Add("onclick", "selectPayPlan(" + e.Row.Cells[0].Text + ",'" + e.Row.Cells[1].Text + "','" + lbLinkColumn.Text + "'," + e.Row.Cells[3].Text + ",'" + e.Row.Cells[4].Text + "','" + e.Row.Cells[5].Text + "');");

            }
        }

        protected void gvPayPlanList_DataBound(object sender, EventArgs e)
        {
            gvPayPlanList.Columns[0].HeaderStyle.CssClass = "hiderowcol";
            gvPayPlanList.Columns[0].ItemStyle.CssClass = "hiderowcol";

            // Hiding Colunm
            //sCssClass = "hiderowcol";
            gvPayPlanList.Columns[3].HeaderStyle.CssClass = "hiderowcol";
            gvPayPlanList.Columns[3].ItemStyle.CssClass = "hiderowcol";

            gvPayPlanList.Columns[4].HeaderStyle.CssClass = "hiderowcol";
            gvPayPlanList.Columns[4].ItemStyle.CssClass = "hiderowcol";

            gvPayPlanList.Columns[5].HeaderStyle.CssClass = "hiderowcol";
            gvPayPlanList.Columns[5].ItemStyle.CssClass = "hiderowcol";
        }

    }
}
