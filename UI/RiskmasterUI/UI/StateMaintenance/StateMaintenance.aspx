<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StateMaintenance.aspx.cs" Inherits="Riskmaster.UI.UI.StateMaintenance.StateMaintenance"   %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc"  %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>States</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">{var i;}</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">{var i;}</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">{var i;}</script>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body onload="CopyGridRowDataToPopup();">
    <form name="frmData" id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle" >
        <asp:Label ID="formtitle" runat="server" Text="States" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='RowId']" rmxignoreset='true'/>
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="StateAbLoadvalue" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
       <tr> 
    <td><asp:Label runat="server"  ID="lblCounrtyAb" Text="Country:" /></td> 
    <td>&nbsp;</td>
    <td>
    
     <!-- atavaragiri :MITS 23657 -->
     <uc:CodeLookUp runat="server" ID="CountryId" CodeTable="COUNTRY" TabIndex="3"  ControlName="CountryId" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='CountryId']"
                        RMXType="code" />
     <!-- end MITS 23657 -->
      
 </td>
   </tr>
    <tr>
    <td>
        <asp:Label runat="server" class="required" ID="lblStateAb" Text="State Abbrv:" />
        </td>
        <td>&nbsp;</td>
        <td>
            <asp:TextBox runat="server" ID="StateAb" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='StateAb']"
                TabIndex="1" onblur="stateabbrLostFocus(this);" onchange="setDataChanged(true);" type="text"/>
        </td>
    </tr>
    <tr>
       <td> <asp:Label runat="server" class="required" ID="lblStateName" Text="State Name:" /></td>
        <td>&nbsp;</td>
        <td>
            <asp:TextBox runat="server" onchange="setDataChanged(true);"
                ID="StateName" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='StateName']"
                TabIndex="2" type="text"/>
        </td>
    </tr>
 
    <tr>
       <td> <asp:Label runat="server"  ID="lbl_lstWrkLoss" Text="Work Loss Calc:" /></td>
        <td>&nbsp;</td>
        <td> 
            <asp:dropdownlist runat="server" onchange="setDataChanged(true);"
                ID="lstWrkLoss" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='lstWrkLoss']/@value"
                TabIndex="4" type="combobox" 
                ItemSetRef="/Instance/Document/form/group/displaycolumn/control[@name ='lstWrkLoss']">
            </asp:dropdownlist>
        </td>
    </tr>
    <tr>
       <td> <asp:Label runat="server"  ID="lblFroi" Text="FROI:" /></td>
        <td>&nbsp;</td>
        <td>
            <asp:TextBox runat="server" 
                ID="Froi" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='Froi']"
                TabIndex="5" MaxLength="50" onblur="numLostFocus(this);" onChange="setDataChanged(true);"
                        type="text"/>
        </td>
    </tr>
    </tbody>
    </table>
    
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup" >
        <div class="formButton"  id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return States_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return States_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    </form>
</body>
</html>
