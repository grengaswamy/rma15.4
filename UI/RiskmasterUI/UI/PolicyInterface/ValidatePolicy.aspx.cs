﻿/***************************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 ***************************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 ***************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using System.Data;
using System.Collections;
using Riskmaster.Common;
using System.Xml;
using System.Text;


namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class ValidatePolicy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            PolicyValidateResponse objResponse = null;
            PolicyValidateInput objRequest = null;
            bool bSuccess = false;
            try
            {
                objRequest = new PolicyValidateInput();

                objRequest.DateOfClaim = AppHelper.GetQueryStringValue("DateOfClaim");
                objRequest.DateOfEvent = AppHelper.GetQueryStringValue("DateOfEvent");
                objRequest.CurrencyCode = AppHelper.GetQueryStringValue("CurrencyType");
                //nsachdeva2 - 7/26/2012
                objRequest.PolicyLobCode = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("PolicyLobCode"), out bSuccess);
                //MITS:33574 START
                if (objRequest.PolicyLobCode == 0)
                {
                    objRequest.PolicyLobShortcode = AppHelper.GetQueryStringValue("PolicyLobShortCode");
                }
                //MITS:33574 END
                //hdMDIClaimId.Value = AppHelper.GetQueryStringValue("MDIClaimId");
                //hdnMDIEventId.Value = AppHelper.GetQueryStringValue("MDIEventId");

                objRequest.PolicyId = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("PolicyId"), out bSuccess);
                txtPolicyId.Text = objRequest.PolicyId.ToString();
                //objResponse = objHelper.PolicyValidation(objRequest);
                //Added by swati agarwal 12/14/2013
                hdnPolicySrchSys.Value = AppHelper.GetQueryStringValue("policysearchsys");
                hdnClaimReportedDate.Text = AppHelper.GetQueryStringValue("ClaimReportedDate");////Ashish Ahuja: Claims Made Jira 1342
                objResponse = objHelper.PolicyValidation(objRequest);
                
                
                if (objResponse.IsPolicyValid)
                {
                    txtPolicyNumber.Text = objResponse.PolicyNumber;
                    //txtPolicySystemId.Text = objResponse.PolicySystemId.ToString();
                    //Added by swati
                    if (String.Compare(hdnPolicySrchSys.Value.ToLower(), "true", false) != 0)
                    {
                        txtPolicySystemId.Text = objResponse.PolicySystemId.ToString();
                    }
                    //change end here by swati
                    //MITS:33574 START
                    //ClientScript.RegisterStartupScript(this.GetType(), "script", "AttachPolicy();", true);
                    int iLOBID = objResponse.LoBId;
                    int iClaimStatusId = objResponse.ClaimStatusID;
                    if (!string.IsNullOrEmpty(objRequest.PolicyLobShortcode))
                    {
                        string LoBParentcode = objResponse.ParentLobCode;
                        ClientScript.RegisterStartupScript(this.GetType(), "script", "RedirectCliamPage('" + LoBParentcode + "','" + txtPolicyNumber.Text + "','" + objRequest.DateOfClaim + "', '" + objRequest.PolicyLobShortcode + "' , '" + iLOBID + "', '" + iClaimStatusId + "' , '" + hdnClaimReportedDate.Text + "');", true);//Jira 1342
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "script", "AttachPolicy();", true);
                    }
                    //MITS:33574 END                 
                }
                else
                {
                    //ClientScript.RegisterStartupScript(this.GetType(), "ShowClaimScript", "ShowClaimScreen();", true);
                    ClientScript.RegisterStartupScript(this.GetType(), "ClearPolicyRelatedData", "ClearPolicyRelatedData();", true);
                    lblValidationFailed.Text = GetValidationFailedMessage(objResponse);
                }
            }
            catch (Exception exp)
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "ShowClaimScript", "ShowClaimScreen();", true);
                ClientScript.RegisterStartupScript(this.GetType(), "ClearPolicyRelatedData", "ClearPolicyRelatedData();", true);
                lblValidationFailed.Text = GetValidationFailedMessage(objResponse);
            }

        }

        private string GetValidationFailedMessage(PolicyValidateResponse objResponse)
        {
            StringBuilder sbMessage = new StringBuilder("<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;Policy cannot be attached to this claim!</h3><br/>Please verify following parameters-<br/>");
            //string strMessage = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;Policy cannot be attached to this claim!</h3></p>";
            //sbMessage.Append("&#8226; Policy status should be In-Effect <br/>");
            if (String.Compare(hdnPolicySrchSys.Value.ToLower(), "true", false) != 0)
            {
                //RMA-7069 - smishra54, Corrected the validation messages
                //sbMessage.Append("&#8226; Policy should be primary policy <br/>");
                sbMessage.Append("&#8226; Policy status should be In-Effect respect to claim loss date <br/>");
                //smishra54-end
                //JIRA 1342 ajohari2: Start No need for this validation now with the implementation of claims made
                //if (objResponse.TriggerClaimFlag)
                //    sbMessage.Append("&#8226; Date Of Claim should be between Policy effective and expiration date <br/>");
                //else
                //    sbMessage.Append("&#8226; Date Of Event should be between Policy effective and expiration date <br/>");

                if (objResponse.IsMultiCurrencyEnable)
                    sbMessage.Append("&#8226; Claim currency should be same as Policy currency <br/>");
                //JIRA 1342 ajohari2: End
                sbMessage.Append("&#8226; Policy LOB should be same as Claim LOB(In case of External Policy)");
            }
            else
            {
                sbMessage.Append(objResponse.Errors);
            }
            sbMessage.Append("</p>");

            return sbMessage.ToString();
        }
    }
}