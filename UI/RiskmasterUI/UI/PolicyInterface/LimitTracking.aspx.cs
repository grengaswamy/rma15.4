﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.PolicyInterface
{
    public partial class LimitTracking : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sResponse=string.Empty;
           
            try
            {
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("LimitTracking.aspx"), "LimitTrackingValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "LimitTrackingValidations", sValidationResources, true);

                if (!IsPostBack)
                {
                    sResponse = AppHelper.CallCWSService(GetMessageTemplateForLoad());
                    BindData(sResponse);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private void BindData(string sResponse)
        {
            ListItem lvItem = null;
            XmlDocument objXmlDoc = null;
            lstPolLimitList .Items.Clear();

            objXmlDoc = new XmlDocument();
            objXmlDoc.LoadXml(sResponse);

            PolLimitUserIdStr.Value = objXmlDoc.SelectSingleNode("//PolicyLimit//UserIdStr").InnerText;
            PolLimitUserStr.Value = objXmlDoc.SelectSingleNode("//PolicyLimit//UserStr").InnerText;
            hdnPolLimitUserStr.Value = objXmlDoc.SelectSingleNode("//PolicyLimit//hdnUserStr").InnerText;
            hdnPolLimitGroupStr.Value = objXmlDoc.SelectSingleNode("//PolicyLimit//hdnGroupStr").InnerText;
            hdnPolLimitLstGroups.Value = hdnPolLimitGroupStr.Value;
            hdnPolLimitLstUsers.Value = hdnPolLimitUserStr.Value;


            if (objXmlDoc.SelectNodes("//PolicyLimit//ApproverList//Option").Count > 0)
            {
                foreach (XmlNode objNode in objXmlDoc.SelectNodes("//PolicyLimit//ApproverList//Option"))
                {
                    lvItem = new ListItem();
                    lvItem.Text = objNode.Attributes["DisplayName"].Value;
                    if (objNode.Attributes["GROUP_ID"].Value != "0")
                    {
                        lvItem.Value =  objNode.Attributes["GROUP_ID"].Value;
                    }
                    else if (objNode.Attributes["USER_ID"].Value != "0")
                    {
                        lvItem.Value = objNode.Attributes["USER_ID"].Value;
                    }

                    lstPolLimitList.Items.Add(lvItem);

                    lvItem = null;


                }
            }


            if (objXmlDoc.SelectSingleNode("//PolicyLimitExceeded") != null)
            {
                chkPolicyLimit.Checked = Conversion.ConvertStrToBool(objXmlDoc.SelectSingleNode("//PolicyLimitExceeded").InnerText);
            }
        }
        private string GetMessageTemplateForLoad()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>LimitTrackingSetupAdaptor.LoadData</Function></Call><Document><LimitTrackingSetUp><ApproverData>");

            sXml = sXml.Append("<PolicyLimit>");
            sXml = sXml.Append("<UserIdStr/>");
            sXml = sXml.Append("<UserStr/>");
            sXml = sXml.Append("<hdnUserStr/>");
            sXml = sXml.Append("<hdnGroupStr/>");

            sXml = sXml.Append("<ApproverList>");
            //sXml = sXml.Append("<GroupIds>");
            //sXml = sXml.Append("</GroupIds>");
            //sXml = sXml.Append("<UserIds>");
            //sXml = sXml.Append("</UserIds>");
            //sXml = sXml.Append("<Option/>");
            
            sXml = sXml.Append("</ApproverList>");
            sXml = sXml.Append("</PolicyLimit>");
          
            sXml = sXml.Append("</ApproverData>");

            sXml = sXml.Append("<PolicyLimitExceeded> </PolicyLimitExceeded>");
            sXml = sXml.Append("  </LimitTrackingSetUp></Document></Message>");


            return sXml.ToString();
        }
        private string GetMessageTemplateForSave()
        {
            string sGroupIds = string.Empty;
            string sUserIds = string.Empty;
            foreach (string sId in PolLimitUserIdStr.Value.Split(' '))
            {
                if (sId.StartsWith("G"))
                {
                    if (string.IsNullOrEmpty(sGroupIds))
                    {
                        sGroupIds = sId;
                    }
                    else
                    {
                        sGroupIds = sGroupIds + "," + sId;
                    }
                    
                }
                else if (sId.StartsWith("U"))
                {
                    if (string.IsNullOrEmpty(sUserIds))
                    {
                        sUserIds = sId;
                    }
                    else
                    {
                        sUserIds = sUserIds + "," + sId;
                    }

                   
                }
            }

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>LimitTrackingSetupAdaptor.SaveData</Function></Call><Document><LimitTrackingSetUp><ApproverData>");

            sXml = sXml.Append("<PolicyLimit>");
            sXml = sXml.Append("<ApproverList>");
                    sXml = sXml.Append("<GroupIds>");
                    sXml = sXml.Append(sGroupIds);
                    sXml = sXml.Append("</GroupIds>");
                
                    sXml = sXml.Append("<UserIds>");
                    sXml = sXml.Append(sUserIds);
                    sXml = sXml.Append("</UserIds>");
                
            sXml = sXml.Append("<Option/>");

            sXml = sXml.Append("</ApproverList>");
            sXml = sXml.Append("</PolicyLimit>");
           

            sXml = sXml.Append("</ApproverData>");

            sXml = sXml.Append("<PolicyLimitExceeded>" + chkPolicyLimit.Checked + "</PolicyLimitExceeded>");
            sXml = sXml.Append(" </LimitTrackingSetUp></Document></Message>");

            return sXml.ToString();
        }

        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            string sResponse = string.Empty;
             
            try
            {
           sResponse=     AppHelper.CallCWSService(GetMessageTemplateForSave());
           sResponse = AppHelper.CallCWSService(GetMessageTemplateForLoad());
           BindData(sResponse);
       //    BindData(GetMessageTemplateForSave());
                //BindData(sResponse);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}