<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_PolicyCoverageList.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_PolicyCoverageList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head id="Head1" runat="server">
    <title>Policy Coverage</title>
    <script type="text/javascript" language="javascript">
        function OpenCoverageDetails() {
            var CtrlList, val, m_window;
            val = "";
            CtrlList = document.getElementsByTagName("input");
            for (i = 0; i < CtrlList.length; i++) {
                if (CtrlList[i].type == "radio" && CtrlList[i].checked == true) {
                    val = CtrlList[i].value;
                    break;
                }
            }
            if (val == "") {
                alert("Please select a record");
            }
            else {
                m_window = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyCoverageDetail.aspx?val=" + val, "CoverageDetail", "width=900,height=430,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
            }
            return false;
        }

    </script>
  </head>
  <body class="10pt" >
    <form name="frmData" id="frmData" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    
      <div class="msgheader" id="formtitle">
        Coverage List Details
        </div>
        <br />
     <div>
        <asp:GridView id="gvData" runat="server" AutoGenerateColumns="false" 
             AllowPaging="false" AllowSorting="false" GridLines="both" CellPadding="8" 
             Width="100%" CellSpacing="2" EmptyDataText="No Coverage List Found" >
         <AlternatingRowStyle CssClass="datatd" />
          <RowStyle CssClass="datatd1" />
         <HeaderStyle CssClass="msgheader" ForeColor="White"/>
           <Columns>
           <asp:TemplateField >
           <ItemTemplate >
           <input type="radio" name="rdButton" value='<%# Eval("val") %>' />
           </ItemTemplate>
           </asp:TemplateField>
                    <asp:BoundField DataField="CovCode" HeaderText="Coverage Code" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" 
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Desc" HeaderText="Coverage Description" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" 
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" 
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Prem" HeaderText="Premium" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" 
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Unit" HeaderText="Coverage Seq Number" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" 
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Deduc" HeaderText="Exposure/Deductible" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="val" Visible="false" />
           </Columns>
        </asp:GridView>
       <br />
       <br />
         <asp:button runat="server" id="btnCovDetail" text="Coverage Detail" class="button" OnClientClick="return OpenCoverageDetails()" />
         <asp:HiddenField runat="server" ID="hfPolicyNum" Value="" />
         <asp:HiddenField runat="server" ID="hfLocCmpy" />
         <asp:HiddenField runat="server" ID="hfMasterCmpny" />
         <asp:HiddenField runat="server" ID="hfCmpnyProductCd" />
         <asp:HiddenField runat="server" ID="hfIssueCd" />
         <asp:HiddenField runat="server" ID="hfModule" />
         <asp:HiddenField runat="server" ID="hfUnitNum" />
         <asp:HiddenField runat="server" ID="hfLobCd" />
         <asp:HiddenField runat="server" ID="hfPolicySystemIdentifier" />
         <asp:HiddenField runat="server" ID="hfPolCompnay" />
         <asp:HiddenField runat="server" ID="hfStatUnitNum" />
    </div>
    </form>
  </body>
</html>
