﻿/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality
**********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Telerik.Web.UI;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Xml.Linq;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PolicySysDownloadResults : System.Web.UI.Page
    {
        private const string DATA_SET = "DATA_SET";
        private const string CLIENT_FILE = "ClientFileSetting";
        //Property to check ConfigFile Setting
        private string IsClientFile
        {
            get
            {
                if (ViewState[CLIENT_FILE] != null)
                    return ViewState[CLIENT_FILE].ToString();
                else
                    return string.Empty;

            }
            set
            {
                ViewState[CLIENT_FILE] = value;
            }
        }
        private DataSet PageData
        {
            get
            {
                return (DataSet)ViewState[DATA_SET];
            }
            set
            {
                ViewState[DATA_SET] = value;
            }
        }
        private DataTable GridDataSource
        {
            get
            {
                return PageData.Tables["Row"];
            }
            //set
            //{
            //    ViewState[DATA_SET] = value;
            //}
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorControl.Visible = false;
            string[] sValues;
            XElement xmlTemplate = null;
            if (!Page.IsPostBack)
            {
                hdnPolicySysName.Value = Request.Form["dlSystemName"].ToString(); //sanoopsharma
			//Paradyumna - MITS 36013 - Start
                hdnPolicySrchSys.Value = AppHelper.GetQueryStringValue("policysearchsys");
                if (String.Compare(hdnPolicySrchSys.Value.ToLower(), "true", false) != 0)
                {
                sValues = Request.Form["dlSystemName"].ToString().Split('|');
                hdnSystemName.Value = sValues[0];
                if (sValues[1] == "1")
                {
                    IsClientFile = "true";
                }
                else if (sValues[1] == "0")
                {
                    IsClientFile = "false";
                }
                hdnClientFile.Value = sValues[1]; //aaggarwal29 : MITS Download insured
                }
			//Paradyumna - MITS 36013 - End
                //hdnSystemName.Value = Request.Form["dlSystemName"].ToString();
                hdnClaimType.Value = Request.Form["hdnClaimType"].ToString();
                hdnClaimId.Value = Request.Form["hdnClaimId"].ToString();
                hdnPolSysType1.Value = Request.Form["hdnPolSysType"].ToString();
                //MITS:33574 START
                //if (Request.Form["hdnClaimId"].ToString() == "")
                //{
                //    imgSave.Visible = false;
                //}
                //skhare7 Point Policy Interface
                if (string.IsNullOrEmpty(hdnClaimType.Value))
                {
                    string sDateOfLoss = string.Empty;
                    sDateOfLoss = Request.QueryString["DateOfLoss"];
                    //Ashish Ahuja: Claims Made Jira 1342
                    string sClaimRptDateType = string.Empty;
                    hdnClaimRptDateType.Value = Request.QueryString["rptDate"];
                    if (hdnClaimRptDateType.Value != "")
                    {
                        if (hdnClaimRptDateType.Value == "2")
                            txtClaimDateReported.Value = DateTime.Now.ToString("MM/dd/yyyy");

                    }
                    if (sDateOfLoss != "")
                    {
                        txtLossDate.Value = sDateOfLoss;
                        if (hdnClaimRptDateType.Value == "1")
                            txtClaimDateReported.Value = txtLossDate.Value;
                    }
                }
                else
                {
                    txtLossDate.Style.Add("display", "none");
                    btnLossDate.Style.Add("display", "none");
                    lblDateOfLoss.Visible = false;
                    //Ashish Ahuja: Claims Made Jira 1342
                    txtClaimDateReported.Style.Add("display", "none");
                    btnClaimDateReported.Style.Add("display", "none");
                    lblClaimDateReported.Visible = false;

                }
                //MITS:33574 END
                //skhare7 Point Policy Interface
                hdnPolicyLobCode.Value = Request.Form["txtPolicyLOB"].ToString();
                hdnPolicyLobId.Value = Request.Form["txtPolicyLOB_cid"].ToString();

                hdnPolicyLossDate.Value = Request.Form["txtLossDate"].ToString();
                hdnClaimBasedFlag.Value = Request.QueryString["ClaimEventSettings"];//Ashish Ahuja: Claims Made Jira 1342
                hdnClaimReportedDate.Value = Request.QueryString["ReportedDate"];//Ashish Ahuja: Claims Made Jira 1342
                hdnClaimDate.Value = Request.QueryString["ClaimDate"];//Ashish Ahuja: Claims Made Jira 1342
                hdnEventDate.Value = Request.QueryString["EventDate"];//Ashish Ahuja: Claims Made Jira 1342
                hdnPolicySearchByDate.Value = Request.QueryString["PolicySearchByDate"];//JIRA 1342 ajohari2: End

                GetPolicySearchResults();
            }
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PolicySystemInterfaceAdaptor.GetPolicySearchSystem</Function></Call><Document><Document>");
            sXml = sXml.Append("</Document></Document></Message>");

            XElement oTemplate = XElement.Parse(Convert.ToString(sXml));
            return oTemplate;
        }
        
        protected void SaveClicked(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicySaveRequest objReq = null;
            PolicySaveRequest objRes = null;
            bool blnSuccess = false;
			//RMA-12047
            CheckDuplicateReq objChkDupReq = null;
            CheckDuplicateRes objChkDupRes = null;
            try
            {
                objHelper = new PolicyInterfaceBusinessHelper();
                objReq = new PolicySaveRequest();
				//RMA-12047
                objChkDupReq = new CheckDuplicateReq();
                objChkDupReq.PolicySymbol = hdnPolicySymbol.Value.Trim();
                objChkDupReq.PolicyNumber = hdSelectedPolicyNumber.Value.Trim();
                objChkDupReq.MasterCompany = hdSelectedMasterCompany.Value.Trim();
                objChkDupReq.LocationCompany = hdnLocation.Value.Trim();
                objChkDupReq.Module = hdSelectedModule.Value.Trim();
                objChkDupReq.SearchByDateType = Conversion.CastToType<int>(hdnPolicySearchByDate.Value, out blnSuccess);
                objChkDupReq.PolicySystemId = hdnSystemName.Value;
                objChkDupReq.Lob = hdSelectedLOB.Value;
                if (!string.IsNullOrEmpty(hdnClaimType.Value))
                {
                    if (objChkDupReq.SearchByDateType == 0)
                    {
                        objChkDupReq.LossDate = AppHelper.GetRMDate(hdnEventDate.Value);
                    }
                    else
                    {
                        objChkDupReq.LossDate = AppHelper.GetRMDate(hdnClaimDate.Value);
                    }
                }
                else
                {
                    objChkDupReq.LossDate = AppHelper.GetRMDate(txtLossDate.Value);
                }
                objChkDupReq.LOBCodeId = hdnPolicyLobId.Value.Trim();
                objChkDupReq.InsuredName = hfInsurerNm.Value.Trim();
                objChkDupReq.OverRideFlag = dupeoverride.Value == "IsSaveDup" ? true : false;
                objChkDupReq.PolicySystemType = "POINT";
                objChkDupRes = objHelper.CheckDuplicatePolicy(objChkDupReq);
                if (!string.IsNullOrEmpty(objChkDupRes.DuplicateClaimIds))
                {
                    dupeoverride.Value = objChkDupRes.DuplicateClaimIds;
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "CheckDuplicate();", true);
                    return;
                }
                else
                {
                    dupeoverride.Value = string.Empty;
                }
				//RMA-12047
                objReq.PolicySymbol = hdSelectedPolicyName.Value.Trim();
                    
                objReq.PolicySystemId = Conversion.CastToType<int>(hdnSystemName.Value, out blnSuccess);
                objReq.ClaimFormName = Request.Form["hdnClaimType"].ToString().Trim();
                //document.getElementById("hdSelectedPolicyNumber").value = policyNumber.innerText;
                ////   document.getElementById("hdSelectedPolicyName").value = policyName.innerText;
                ////                document.getElementById("hdSelectedPolicyHolders").value = policyHolders.innerText;
                //document.getElementById("hdSelectedEffDate").value = policyEffDate.innerText;
                ////                document.getElementById("hdSelectedExtPolId").value = policyId.innerText;
                //document.getElementById("hdSelectedMasterCompany").value = mastercompany.innerText;
                //document.getElementById("hdSelectedLOB").value = lob.innerText;
                //document.getElementById("hdSelectedModule").value = module.innerText;

                objReq.PolicyIdentfier = hdSelectedPolicyNumber.Value.Trim(); 
                objReq.PolicyNumber = hdSelectedPolicyNumber.Value.Trim();
                objReq.PolicySymbol = hdnPolicySymbol.Value.Trim();
                objReq.MasterCompany = hdSelectedMasterCompany.Value.Trim();
                objReq.LOB = hdSelectedLOB.Value.Trim();
                objReq.Location = hdnLocation.Value.Trim();
                objReq.Module = hdSelectedModule.Value.Trim();
                objReq.State = hdnState.Value.Trim();
                objReq.InsurerNm = hfInsurerNm.Value.Trim();
                objReq.InsurerCity = hfInsurerCity.Value.Trim();
                objReq.InsurerPostalCd = hfInsurerPostalCd.Value.Trim();
                objReq.InsurerAddr1 = hfInsurerAddr1.Value.Trim();
                objReq.TaxId = hdnTaxId.Value.Trim();
                objReq.ClientSeqNo = hdnClientSeqNo.Value.Trim();
                objReq.AddressSeqNo = hdnAddressSeqNo.Value.Trim();
                objReq.BirthDt = hdnBirthDt.Value.Trim();
                objReq.InceptionDate = hdnInceptionDt.Value.Trim();
                objReq.ClaimBasedFlag = hdnClaimBasedFlag.Value.Trim();//Ashish Ahuja: Claims Made Jira 1342
                objReq.PolLossDt = hdnPolicyLossDate.Value;
                objReq.NameType = hdnNameType.Value.Trim();
                objReq.PolicySearchByDate = Conversion.CastToType<int>(hdnPolicySearchByDate.Value, out blnSuccess);//JIRA 1342 ajohari2
                //if (CheckIfDriver(hdSelectedLOB.Value))
                //{
                //    objReq.IsDriverRequest = true;
                //}
                //else
                //{
                //    objReq.IsDriverRequest = false;
                //    //TABSdriverinfo.Attributes.Add("style", "display:none");
                //    //FORMTABdriverinfo.Attributes.Add("style", "display:none");
                //}

                objReq.ClaimId = Conversion.CastToType<int>(Request.Form["hdnClaimId"].ToString(), out blnSuccess);
                objRes = objHelper.SaveExternalPolicy(objReq);
                
                if (objRes != null && objRes.Result)
                {
                    hdAddedPolicyId.Value = objRes.AddedPolicyId.ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "SaveSucceeded();", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "SaveFailed();", true);
                }

            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;

            }
            catch (Exception ex)
            {
                PageData = null;
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objRes != null)
                    objRes = null;
                if (objReq != null)
                    objReq = null;
                if (objHelper != null)
                    objHelper = null;
            }
        }
        //private bool CheckIfDriver(string pLOB)
        //{
        //    pLOB = pLOB.ToUpper();

        //    if (string.Compare(pLOB, "APV") == 0 || string.Compare(pLOB, "CPP") == 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        private void CreateCustomColumns(DataTable objTable)
        {
            GridBoundColumn objGridBoundColumn;
            string sSortingColumnName = string.Empty;
            bool IsColumnExists = false;

            try
            {
                foreach (DataColumn dCol in objTable.Columns)
                {
                    objGridBoundColumn = new GridBoundColumn();
                    gvPolicyList.MasterTableView.Columns.Add(objGridBoundColumn);
                    objGridBoundColumn.UniqueName = dCol.ColumnName;
                    objGridBoundColumn.DataField = dCol.ColumnName;

                    // objGridBoundColumn.HeaderText = dCol.ColumnName.Replace("com.csc_","");
                    objGridBoundColumn.HeaderText = objTable.Rows[0][dCol].ToString();

                    objGridBoundColumn.SortExpression = dCol.ColumnName;
                    objGridBoundColumn.AllowFiltering = true;
                    objGridBoundColumn.AllowSorting = true;
                    objGridBoundColumn.ItemStyle.Width = 140;
                    objGridBoundColumn.HeaderStyle.Width = 140;
                    objGridBoundColumn.ItemStyle.CssClass = "data";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void GetPolicySearchResults()
        {
            DataSet oSearchRecords = null;
            PolicySearch oPolicySearchResponse = null;
            PolicyInterfaceBusinessHelper objHelper = null;
            try
            {
                oSearchRecords = new DataSet();
                objHelper = new PolicyInterfaceBusinessHelper();
                oPolicySearchResponse = objHelper.GetPolicySearchResults(GetSearchFilterParameters());

                if (oPolicySearchResponse != null && !string.IsNullOrEmpty(oPolicySearchResponse.ResponseXML))
                {
                    // this code is now getting set before response is coming so not required here.    //IsClientFile = oPolicySearchResponse.ClientFile;
                    oSearchRecords.ReadXml(new StringReader(oPolicySearchResponse.ResponseXML));
                }

                if (oSearchRecords != null && oSearchRecords.Tables.Count > 0)
                {
                    if ((oSearchRecords.Tables["AdditonalFields"] != null) && (oSearchRecords.Tables["AdditonalFields"].Rows.Count > 0))
                    {
                        CreateCustomColumns(oSearchRecords.Tables["AdditonalFields"]);
                    }
                    PageData = oSearchRecords;
                }
                else
                {
                    PageData = null;

                    CallNoResult();
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;

            }
            catch (Exception ex)
            {
                PageData = null;
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (oSearchRecords != null)
                    oSearchRecords = null;
                if (oPolicySearchResponse != null)
                    oPolicySearchResponse = null;
                if (objHelper != null)
                    objHelper = null;
            }
        }

        private PolicySearch GetSearchFilterParameters()
        {
            bool blnSuccess;
            string[] sValues;
            PolicySearch oPolicySearchRequest = new PolicySearch();
            oPolicySearchRequest.objSearchFilters = new SearchFilters();
			//Paradyumna - MITS 36013 - Start
            if (String.Compare(hdnPolicySrchSys.Value.ToLower(), "true", false) != 0)
            {
                if (Request.Form["dlSystemName"] != null)
                {
                    sValues = Request.Form["dlSystemName"].ToString().Split('|');
                    oPolicySearchRequest.objSearchFilters.PolicySystemId = Conversion.CastToType<int>(sValues[0], out blnSuccess);
                    //oPolicySearchRequest.objSearchFilters.PolicySystemId = Conversion.CastToType<int>(Request.Form["dlSystemName"], out blnSuccess);
                }
                oPolicySearchRequest.ClientFile = IsClientFile;
            }
            //jira RMA-15404: Adding wildcard * support for point policy filter criteria's
            if (Request.Form["txtPolicyNumber"]!= null && !string.IsNullOrEmpty(Request.Form["txtPolicyNumber"].ToString()))
                oPolicySearchRequest.objSearchFilters.PolicyNumber = Request.Form["txtPolicyNumber"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtPolicySymbol"] != null && !string.IsNullOrEmpty(Request.Form["txtPolicySymbol"].ToString()))
                oPolicySearchRequest.objSearchFilters.PolicySymbol = Request.Form["txtPolicySymbol"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtPolicyLOB"] != null && !string.IsNullOrEmpty(Request.Form["txtPolicyLOB"].ToString()))
                oPolicySearchRequest.objSearchFilters.LOB = Conversion.CastToType<int>(Request.Form["txtPolicyLOB_cid"].ToString(), out blnSuccess);
            if (Request.Form["txtAgentName"] != null && !string.IsNullOrEmpty(Request.Form["txtAgentName"].ToString()))
                oPolicySearchRequest.objSearchFilters.Agent = Request.Form["txtAgentName"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtState"] != null && !string.IsNullOrEmpty(Request.Form["txtState"].ToString()))
                oPolicySearchRequest.objSearchFilters.State = Conversion.CastToType<int>(Request.Form["txtState_cid"].ToString(), out blnSuccess);
            if (Request.Form["txtPolicyModule"] != null && !string.IsNullOrEmpty(Request.Form["txtPolicyModule"].ToString()))
                oPolicySearchRequest.objSearchFilters.Module = Request.Form["txtPolicyModule"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtMasterCompany"] != null && !string.IsNullOrEmpty(Request.Form["txtMasterCompany"].ToString()))
                oPolicySearchRequest.objSearchFilters.MasterCompany = Request.Form["txtMasterCompany"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtLocCompany"] != null && !string.IsNullOrEmpty(Request.Form["txtLocCompany"].ToString()))
                oPolicySearchRequest.objSearchFilters.LocationCompany = Request.Form["txtLocCompany"].Replace('*', '%').ToString().Trim();

            if (Request.Form["txtGroupNo"] != null && !string.IsNullOrEmpty(Request.Form["txtGroupNo"].ToString()))
                oPolicySearchRequest.objSearchFilters.GroupNo = Request.Form["txtGroupNo"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtZip"] != null && !string.IsNullOrEmpty(Request.Form["txtZip"].ToString()))
                oPolicySearchRequest.objSearchFilters.Zip = Request.Form["txtZip"].Replace('*', '%').ToString().Trim();
			//Paradyumna - MITS 36013 - End
            // Pradyumna MITS#36013 03062014 - Modified for WWIG GAP16 - Start
            if (String.Compare(hdnPolicySrchSys.Value.ToLower(), "true", false) != 0)
            {
            if (string.Equals(oPolicySearchRequest.ClientFile, "false", StringComparison.OrdinalIgnoreCase))
            {
                    if (Request.Form["txtInsuredCityAddr"] != null && !string.IsNullOrEmpty(Request.Form["txtInsuredCityAddr"].ToString()))
                        oPolicySearchRequest.objSearchFilters.Address = Request.Form["txtInsuredCityAddr"].Replace('*', '%').ToString().Trim();
                
                    if (Request.Form["txtNm"] != null)
                    {
                if (string.IsNullOrEmpty(Request.Form["txtNm"].ToString()))
                    oPolicySearchRequest.objSearchFilters.Name = "%";
                else
                    oPolicySearchRequest.objSearchFilters.Name = Request.Form["txtNm"].Replace('*', '%').ToString().Trim();
                    }
            }
            else
            {
                    if (Request.Form["txtInsuredCityAddr"] != null && !string.IsNullOrEmpty(Request.Form["txtInsuredCityAddr"].ToString()))
                        oPolicySearchRequest.objSearchFilters.City = Request.Form["txtInsuredCityAddr"].Replace('*', '%').ToString().Trim();
                    if (Request.Form["txtInsuredLastName"] != null)
                    {
                if (string.IsNullOrEmpty(Request.Form["txtInsuredLastName"].ToString()))
                    oPolicySearchRequest.objSearchFilters.InsuredLastName = "%";
                else
                    oPolicySearchRequest.objSearchFilters.InsuredLastName = Request.Form["txtInsuredLastName"].Replace('*', '%').ToString().Trim();
                    }
                    if (Request.Form["txtInsuredFirstName"] != null)
                    {
                if (string.IsNullOrEmpty(Request.Form["txtInsuredFirstName"].ToString()))
                    oPolicySearchRequest.objSearchFilters.InsuredFirstName = "%";
                else
                    oPolicySearchRequest.objSearchFilters.InsuredFirstName = Request.Form["txtInsuredFirstName"].Replace('*', '%').ToString().Trim();
                    }
                }
            }
            else
            {
                if (Request.Form["txtInsuredCityAddr"] != null && !string.IsNullOrEmpty(Request.Form["txtInsuredCityAddr"].ToString()))
                    oPolicySearchRequest.objSearchFilters.City = Request.Form["txtInsuredCityAddr"].Replace('*', '%').ToString().Trim();
                if (Request.Form["txtInsuredLastName"] != null && !string.IsNullOrEmpty(Request.Form["txtInsuredLastName"].ToString()))
                    oPolicySearchRequest.objSearchFilters.InsuredLastName = Request.Form["txtInsuredLastName"].Replace('*', '%').ToString().Trim();
                if (Request.Form["txtInsuredFirstName"] != null && !string.IsNullOrEmpty(Request.Form["txtInsuredFirstName"].ToString()))
                    oPolicySearchRequest.objSearchFilters.InsuredFirstName = Request.Form["txtInsuredFirstName"].Replace('*', '%').ToString().Trim();
            }
            // Pradyumna MITS#36013 03062014 - Modified for WWIG GAP16 - End
            if (Request.Form["txtInsuredSSN"] != null && !string.IsNullOrEmpty(Request.Form["txtInsuredSSN"].ToString()))
                oPolicySearchRequest.objSearchFilters.InsuredSSN = Request.Form["txtInsuredSSN"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtLossDate"] != null && !string.IsNullOrEmpty(Request.Form["txtLossDate"].ToString()))
                oPolicySearchRequest.objSearchFilters.LossDate =  Request.Form["txtLossDate"].Replace('*', '%').ToString().Trim();
            if (Request.Form["txtCustomerNo"] != null && !string.IsNullOrEmpty(Request.Form["txtCustomerNo"].ToString()))
                oPolicySearchRequest.objSearchFilters.CustomerNo = Request.Form["txtCustomerNo"].Replace('*', '%').ToString().Trim();

            return oPolicySearchRequest;
        }

        private void BindGrid()
        {
            if (GridDataSource != null && GridDataSource.Rows.Count > 0)
            {
                SetGridDisplay(true);
                gvPolicyList.DataSource = GridDataSource;
                gvPolicyList.DataBind();
            }
            else
            {
                CallNoResult();
            }
        }

        private void CallNoResult()
        {
            string sNoResult = "<span xhtml:class='msgheader1' align='center'>Your search produced no results.<br/>Please refine your criteria selections.<br/></span>";
            SetGridDisplay(false);
            ltNoResult.Text = sNoResult;
        }

        private void SetGridDisplay(bool Isdisplay)
        {
            if (Isdisplay)
            {
                gvPolicyList.Visible = true;
                ltNoResult.Visible = false;
                tblToolbar.Visible = true;
                this.ClientScript.RegisterStartupScript(this.GetType(), "setScroll", "window.scrollTo(0, document.body.scrollHeight)", true);
            }
            else
            {
                gvPolicyList.Visible = false;
                tblToolbar.Visible = false;
                ltNoResult.Visible = true;
            }
        }

        #region GridEvents
        protected void gvPolicyList_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (GridDataSource != null)
            {
                try
                {
                    gvPolicyList.DataSource = GridDataSource;
                }
                catch
                {
                    SetGridDisplay(false);
                }
            }
            else
            {
                dvResults.Visible = false;
            }
        }

        protected void gvPolicyList_PreRender(object sender, EventArgs e)
        {
            GridFilterMenu menu = gvPolicyList.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull")
                {
                    menu.Items.RemoveAt(i);
                }
                else
                {
                    i = (i + 1);
                }
            }
        }

        protected void gvPolicyList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridHeaderItem || e.Item is GridFilteringItem)
            {
                // Pradyumna MITS#34932 02/27/2014: Added 'Or' Condition for Staging Policy setting
                if (string.Equals(IsClientFile, "true", StringComparison.OrdinalIgnoreCase) || string.Equals(hdnPolicySrchSys.Value.ToLower(), "true", StringComparison.OrdinalIgnoreCase))
                {
                    gvPolicyList.Columns[5].Visible = false;
                    gvPolicyList.Columns[9].Visible = false;
                }
                else
                {
                    gvPolicyList.Columns[6].Visible = false;
                    gvPolicyList.Columns[7].Visible = false;
                    gvPolicyList.Columns[8].Visible = false;
                }
            }

            if (e.Item is GridDataItem)
            {
                Label lblControl = null;
                bool bResult = false;
                DateTime dtTemp = DateTime.MinValue;

                lblControl = (Label)e.Item.FindControl("lblStatus");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "status").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblPolicyName");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "policyname").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblPolicyNumber");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "policynumber").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                
                lblControl = (Label)e.Item.FindControl("lblModule");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "module").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblLastName");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "lastname").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblFirstName");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "firstname").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblMiddleName");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "middlename").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                                                    
                lblControl = (Label)e.Item.FindControl("lblCommercialName");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "commercialname").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblSortName");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "sortname").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                                
                lblControl = (Label)e.Item.FindControl("lblCity");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "city").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblState");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "state").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblEffDate");
                try
                {
                    bResult = DateTime.TryParse(DataBinder.Eval(e.Item.DataItem, "effectivedate").ToString(), out dtTemp);
                    if (bResult && !DateTime.Equals(dtTemp, DateTime.MinValue))
                        lblControl.Text = dtTemp.ToString("MM/dd/yyyy");
                    else
                        lblControl.Text = string.Empty;
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblExpDate");
                try
                {
                    bResult = DateTime.TryParse(DataBinder.Eval(e.Item.DataItem, "expirationdate").ToString(), out dtTemp);
                    if (bResult && !DateTime.Equals(dtTemp, DateTime.MinValue))
                        lblControl.Text = dtTemp.ToString("MM/dd/yyyy");
                    else
                        lblControl.Text = string.Empty;
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblLocation");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "location").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblMaster");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "mastercompany").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblAgency");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "agency").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblLOB");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "lob").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }


                lblControl = (Label)e.Item.FindControl("lblInsurerNm");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "InsurerName").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                lblControl = (Label)e.Item.FindControl("lblTaxId");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "TaxId").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }


                lblControl = (Label)e.Item.FindControl("lblInsurerAddr1");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "InsurerAddr1").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblInsurerCity");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "InsurerCity").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblInsurerPostalCd");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "InsurerPostalCode").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                lblControl = (Label)e.Item.FindControl("lblClientSeqNo");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "ClientSeqNo").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                lblControl = (Label)e.Item.FindControl("lblAddressSeqNo");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "AddressSeqNo").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                lblControl = (Label)e.Item.FindControl("lblBirthDt");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "BirthDt").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblInceptionDate");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "inceptiondate").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblNameType");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "NameType").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                // 12/13/2013 MITS 33414: Added by Pradyumna to retain Staging PolicyID for each row
                lblControl = (Label)e.Item.FindControl("lblStagingPolicyID");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "StagingPolicyID").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                //MITS:33574 START
                lblControl = (Label)e.Item.FindControl("lblPointClaimEventSetting");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "PointClaimEventSetting").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                //MITS:33574 END
                //JIRA 1342 ajohari2: Start
                lblControl = (Label)e.Item.FindControl("lblMinimumRetroDate");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "MinimumRetroDate").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblMaximumExtendDate");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "MaximumExtendDate").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }

                lblControl = (Label)e.Item.FindControl("lblIsClaimsMade");
                try
                {
                    lblControl.Text = DataBinder.Eval(e.Item.DataItem, "IsClaimsMade").ToString();
                }
                catch (System.Web.HttpException)
                {
                    lblControl.Text = string.Empty;
                }
                //JIRA 1342 ajohari2: End
            }
        }

        #endregion
    }
}
