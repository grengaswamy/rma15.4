﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Telerik.Web.UI;
using System.Text;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_PropertyUnit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadPropertyDetails();
            }
        }

        private void LoadPropertyDetails()
        {
            XmlDocument objPropertyXml = null;
            bool bInSuccess = false;
            string sUnitId = AppHelper.GetQueryStringValue("recordID");
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry objEnReq = null;
            PolicyEnquiry objEnRes = null;
            try
            {
                objHelper = new PolicyInterfaceBusinessHelper();
                objEnReq = new PolicyEnquiry();
                objEnRes = new PolicyEnquiry();
                objEnReq.PropertyPIN=sUnitId;
                objEnReq.PolicyIdentfier = AppHelper.GetQueryStringValue("PolicyId");
                objEnReq.PolicySymbol = AppHelper.GetQueryStringValue("PolicyName");

                //call the function to fetch the unit details
                objEnRes = objHelper.GetPropertyUnitDetailResult(objEnReq);
                objPropertyXml = new XmlDocument();
                objPropertyXml.LoadXml(objEnRes.PropertyListAcordXML);
                if (objPropertyXml != null)
                {
                    pin.Text = sUnitId;
                    addr.Text = objPropertyXml.SelectSingleNode("//com.csc_UnitDesc").InnerText;
                    ClassOfConstruction.Text = objPropertyXml.SelectSingleNode("//com.csc_ConstructionCd").InnerText;
                    FireDistrict.Text = objPropertyXml.SelectSingleNode("//com.csc_FireDistrict").InnerText;
                    NoOfStories.Text = objPropertyXml.SelectSingleNode("//com.csc_BuildingTotalFloorsNbr").InnerText;
                    YearOfConstruction.Text = objPropertyXml.SelectSingleNode("//com.csc_YearBuilt").InnerText;
                }
            }
            catch (Exception e)
            {
                ErrorHelper.logErrors(e);
                lblError.Text = e.Message;
            }
            finally
            {
                objPropertyXml = null;
            }
        }
    }
}