﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyLimits.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PolicyLimits" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script type="text/javascript" language="javascript">
      
    </script>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <script src="../../Scripts/angularjs/angular.min.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/Directives/rma-ng-grid.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/controllers/PolicyLimitsController.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
    <script type="text/javascript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" src="../../Scripts/WaitDialog.js"> { var i; }</script>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
</head>
<body>

    <form id="form1" runat="server">
        <div ng-app="rmaApp" id="ng-app">
            <uc1:errorcontrol id="ErrorControl" runat="server" />
            
            <asp:textbox style="display: none" runat="server" id="txtSelectedId" rmxtype="hidden" text="" />
            <asp:textbox style="display: none" runat="server" id="txtProcess" rmxtype="hidden" text="" />
            <div class="msgheader" id="formtitle">
                <asp:label text="<%$ Resources:lblPolicyLimitDetails %>" id="lblPolicyLimitDetails" runat="server"></asp:label>
            </div>
            <asp:textbox style="display: none" runat="server" id="txtPolicyId" rmxtype="hidden" text="" />
            <div id="divPolicyLimitController" ng-controller="PolicyLimitController">
                <div id="divgridPolicyLimits" class="divstyles">
                    <input type="hidden" id="hdnJsonData_gridPolicyLimits" runat="server" />
                    <input type="hidden" id="hdnJsonUserPref_gridPolicyLimits" runat="server" />
                    <input type="hidden" id="hdnJsonAdditionalData_gridPolicyLimits" runat="server" />
                    <rma-grid id="gridPolicyLimits" name="gridPolicyLimits" multiselect="false" showselectioncheckbox="false"></rma-grid>

                </div>
            </div>
             <div class="msgheader" id="Div2">
                <asp:label text="<%$ Resources:lblErosionDetails %>" id="lblErosionDetails" runat="server"></asp:label>
            </div>
            <div id="divErosions" ng-controller="ErosionsController" runat="server" >
                <div  id="div1" class="divstyles">
                    <input type="hidden" id="hdnJsonData_gridErosions" runat="server" />
                    <input type="hidden" id="hdnJsonUserPref_gridErosions" runat="server" />
                    <input type="hidden" id="hdnJsonAdditionalData_gridErosions" runat="server" />
                    <rma-grid id="gridErosions" name="gridErosions" multiselect="false" showselectioncheckbox="false"></rma-grid>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
