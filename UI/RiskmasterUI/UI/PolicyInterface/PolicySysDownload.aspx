﻿<%--**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 **********************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 * 06/04/2014 | 33371   | ajohari2   | Changes for TPA search changes 
 **********************************************************************************************--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicySysDownload.aspx.cs"
    Inherits="Riskmaster.UI.PolicyInterface.PolicySysDownload" %>

<%--<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>--%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javaScript" src="../../Scripts/search.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">    { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">    { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">    { var i; } </script>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link href="~/App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
    <!--Pradyumna 04032014 WWIG MITS#35885 - start -->
    <style type="text/css">
        .hide {
            display: none;
        }

        .td {
            display: inline-block;
            *display: inline; /* for IE7*/
            *zoom: 1; /* for IE7*/
            width: 24%;
            margin-top: 5px;
            vertical-align: middle;
            font-family: Tahoma, Arial, Helvetica, sans-serif;
        }

        .left {
            display: inline-block;
            *display: inline; /* for IE7*/
            *zoom: 1; /* for IE7*/
            width: 12%;
            margin-top: 5px;
            vertical-align: middle;
            font-family: Tahoma, Arial, Helvetica, sans-serif;
        }

        .right {
            display: inline-block;
            *display: inline; /* for IE7*/
            *zoom: 1; /* for IE7*/
            width: 37%;
            margin-top: 5px;
            vertical-align: middle;
            font-family: Tahoma, Arial, Helvetica, sans-serif;
        }
    </style>
    <!--Pradyumna 04032014 WWIG MITS#35885 - End -->
    <!-- tanwar2 WWIG - Requirement 1.4.1 - start -->
    <!--<script language="javascript" type="text/javascript">-->
    <script language="javascript" type="text/javascript">
        function DefaultFocus() {
            if (document.getElementById('dlSystemName') != null) {
                document.getElementById('dlSystemName').focus();
            }
        }
        $('document').ready(function () {
            if ($('#hdnPolicySrchSys').val().toUpperCase() === 'TRUE') {
                if ($('#hdnNoRequiredField') && !!($('#hdnNoRequiredField').val().toUpperCase() == "TRUE")) {

                    if ($('#txtLossDate')) {
                        $('#txtLossDate').removeAttr('readonly');
                    }

                    if ($('#btnLossDate')) {
                        $('#btnLossDate').removeAttr('disabled');
                    }
                }


                // Pradyumna 04032014 WWIG MITS#35885 - start
                $('.hide').remove();

                //Modify table structure: Replace tbody with div and td with span
                $('#tblCriteria').replaceWith($('#tblCriteria').html()
                   .replace(/<tbody/gi, "<div id='divCriteria'")
                   .replace(/<tr(.*)>/gi, '')
                   .replace(/<\/tr>/gi, '')
                   .replace(/<td/gi, "<span class=\"td\"")
                   .replace(/<\/td>/gi, "</span>")
                   .replace(/<\/tbody/gi, "<\/div")
                );

                $('select[style="display: none"]').closest('span').removeClass('td');
                //$('span br').closest('span').removeClass('td');
                $('span br').closest('span').remove();
                $('.td:even').removeClass('td').addClass('left');
                $('.td').removeClass('td').addClass('right');
                // Removing Empty Spaces between Spans
                jQuery.fn.cleanWhitespace = function () {
                    textNodes = this.contents().filter(
                    function () { return (this.nodeType == 3 && !/\S/.test(this.nodeValue)); })
                    .remove();
                    return this;
                }

                $('#divCriteria').cleanWhitespace();

                if ($('#hdnNoRequiredField') && !!($('#hdnNoRequiredField').val().toUpperCase() != "TRUE")) {
                    // PSHEKHAWAT 7/25/2014 : JIRA RMA-745 - Start
                    //if ($('#tdLineOfBusiness')) {
                    //    $('#tdLineOfBusiness').addClass('required');
                    //}
                    // PSHEKHAWAT 7/25/2014 : JIRA RMA-745 - Start
                    if ($('#txtLossDate')) {
                        $('#txtLossDate').attr('readonly');
                    }

                    if ($('#btnLossDate')) {
                        $('#btnLossDate').attr('disabled');
                    }
                }
            }
            // Pradyumna 04032014 WWIG MITS#35885 - End
        });
        // tanwar2 WWIG - Requirement 1.4.1 - end
        function ValidateSearch() {
            var sProduct = document.getElementById("hdPolicyProduct").value;
            if ((sProduct == "INTEGRAL") && (document.getElementById("txtIntegralPolicyLOB") != null) && (document.getElementById("txtIntegralPolicyLOB").value == "")) {
                alert("Please select Line of Business");
                return false;
            }
            // Pradyumna - MITS 36013 - Added and condition for "hdnNoRequiredField"
            if ((sProduct == "POINT") && (document.getElementById("txtPolicyLOB") != null) && (document.getElementById("txtPolicyLOB").value == "") && (document.getElementById("hdnNoRequiredField").value.toLowerCase() != "true")) {
                document.getElementById("txtPolicyLOB").focus();
                alert("Please select  Line of Business");
                return false;
            }
            pleaseWait.Show();

        }
        function LaunchAppOnLoad()
        {
            // Check if there is only one Policy System and that is Staging, then we do not need to show the interim Screen. We can show the app screen directly
            var objControl = document.getElementById("dlSystemName");

            if (objControl != null && objControl.value != "")
            {

                List = objControl.value.split("|");
                sPolicySystemType = List[4];

                if (sPolicySystemType == "STAGING" && objControl.options.length == 1)
                    onSystemNameChange();
            }
        }
        //MITS 35932 - Begin  
        // Control show/hide of fields and sections as per policy system type and client setting file
        function onSystemNameChange() {
            var objControl = document.getElementById("dlSystemName"),
                divPOINT = document.getElementById("divPOINTCriteria"),
                divIntegral = document.getElementById("divINTEGRALCriteria"),
                policyLobCode = document.getElementById('txtPolicyLOB').value || '',
                claimDate = document.getElementById('hdnClaimDate').value || '',
                sPolicySystemType = "", //RMA-11914
                selectedPolicySystemId=""; //RMA-11914

            if (objControl != null && objControl.value != "") {

                List = objControl.value.split("|");
                sPolicySystemType = List[4];
                selectedPolicySystemId = List[0];//RMA-11914
                document.getElementById("hdPolicyProduct").value = sPolicySystemType;

                if (sPolicySystemType == "INTEGRAL") {
                    divPOINT.style.display = "none";
                    divIntegral.style.display = "block";
                    tdSysName.style.width = "248";
                }
                else if (sPolicySystemType == "POINT") { //RMA-11493
                    divPOINT.style.display = "block";
                    divIntegral.style.display = "none";
                    GetClientFileSettings(objControl.value);
                    tdSysName.style.width = "243";
                } // Start RMA-11493
                else {
                    //policy app processing
                    var appURL = document.getElementById("hdPolicyAppURL").value;
                    if (appURL != "") {

                        appURL = appURL + "&policyLob=" + policyLobCode + "&dateOfLoss=" + claimDate + "&selectedPolicySystemId=" + selectedPolicySystemId;//RMA-11914

                        if (parent.document.getElementById('selectedScreenId') != null) {
                            appURL = appURL + "&source=PolicySystemDownload";
                            openPolicyApp(appURL);
                            //parent.closeScreen(parent.document.getElementById('selectedScreenId').value, false);
                        }
                        else {
                            appURL = appURL + "&source=Claim";
                            window.opener.openPolicyApp(appURL);
                            window.close();
                        }
                    }
                }// End RMA-11493
            }

        }
        //explicitly clear LOB and loss date on clear button click
        function onResetClear() {
            OnFormClear();
            //bram4 - clear button issue 
            document.getElementById("frmData").reset();
            if (document.getElementById("txtIntegralPolicyLOB") != null)
                document.getElementById("txtIntegralPolicyLOB").value = '';
            if (document.getElementById("txtIntegralLossDate") != null && document.getElementById("hdnClaimId").value == '')
                document.getElementById("txtIntegralLossDate").value = '';
            if (document.getElementById("txtPolicyLOB") != null)
                document.getElementById("txtPolicyLOB").value = '';
            if (document.getElementById("txtLossDate") != null && document.getElementById("hdnClaimId").value == '')
                document.getElementById("txtLossDate").value = '';

        }
        //MITS 35932 - End       
    </script>

    <title>Search</title>
    <style type="text/css">
        .auto-style1 {
            height: 22px;
        }
    </style>
</head>
<body onload="javascript:if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded(); LaunchAppOnLoad();">
    <form id="frmData" runat="server">       
        <div>
            <input type="hidden" id="txtIntegralPolicyLOB_cid" runat="server" />
            <uc1:errorcontrol id="ErrorControl" runat="server" />
            <%--MITS 35932 - Begin--%>
            <div id="divCommonHeader" runat="server" style="display: block">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr xmlns="">
                        <td colspan="7" class="ctrlgroup">Policy System Download Search
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" class="ctrlgroup">Policy Search Criteria
                        </td>
                    </tr>
                </table>
                <table id="tblCriteria" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr xmlns="">
                        <td runat="server" width="248px" id="tdSysName">System Name:
                        </td>
                        <td runat="server" id="tdSysNametxt">
                            <asp:DropDownList ID="dlSystemName" TabIndex="1" runat="server" onchange="onSystemNameChange();">
                            </asp:DropDownList>
                        </td>
                        <td runat="server" id="tdSysNameBr">
                            <br />
                        </td>
                        <%--   </tr>
            <tr xmlns="">--%>
                    </tr>
                </table>
            </div>
            <div id="divPOINTCriteria" runat="server" style="display: block">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                    <div id="dvPolicySearchCriteria" runat="server">
                        <tr>
                            <td>Search Policies Based on Event Date:</td>
                            <td>
                                <asp:RadioButton ID="PolicyType1" type="radio" TabIndex="2" Checked="true" value="1" rmxref="/Instance/Document/form/group/PolicyCvgType"
                                    onclick="setDataChanged(true);overridePolicySearchCriteria();" runat="server" GroupName="rbgPolicyCvgType" />
                            </td>
                            <td id="tdSearchPolicyBr" runat="server">
                                <br />
                            </td>
                            <td>Search Policies Based on Claim Date:</td>
                            <td>
                                <asp:RadioButton ID="PolicyType2" type="radio" TabIndex="25" value="0" rmxref="/Instance/Document/form/group/PolicyCvgType"
                                    onclick="setDataChanged(true);overridePolicySearchCriteria();" runat="server" GroupName="rbgPolicyCvgType" />
                            </td>
                        </tr>
                    </div>
                    <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                    <tr xmlns="">
                        <td id="tdPolicySymbol" runat="server" style="width: 243px;">Policy Symbol:
                        </td>
                        <td id="tdPolicySymboldl" class="hide" runat="server">
                            <select name="dlPolicySymbol" id="dlPolicySymbol" tabindex="3" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdPolicySymboltxt" runat="server">
                            <input name="txtPolicySymbol" type="text" id="txtPolicySymbol" tabindex="4" size="30" maxlength="3"
                                runat="server" />
                        </td>
                        <td id="tdPolicySymbolBr" runat="server">
                            <br />
                        </td>
                        <%-- </tr>
            <tr xmlns="">--%>
                        <td id="tdPolicyModule" runat="server">Module:
                        </td>
                        <td id="tdPolicyModuledl" class="hide" runat="server">
                            <select name="dlPolicyModule" id="dlPolicyModule" tabindex="26" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdPolicyModuletxt" runat="server">
                            <input name="txtPolicyModule" type="text" id="txtPolicyModule" tabindex="27" size="30" maxlength="2"
                                runat="server" />
                        </td>
                    </tr>
                    <tr xmlns="">
                        <!-- tanwar2 WWIG - Requirement 1.4.1 - start -->
                        <!--<td class="required">-->
                        <%--MITS:33574 START--%>
                        <%--  <td class="required">--%>
                        <td id="tdLineOfBusiness" runat="server">
                            <%--MITS:33574 END--%>
                            <!-- tanwar2 WWIG - Requirement 1.4.1 - start -->
                            <asp:Label ID="lbLineOfBusiness" Text="Line Of Business:" runat="server"></asp:Label>
                        </td>
                        <td id="tdLineOfBusinessdl" class="hide" runat="server">
                            <select name="dlPolicyLOB" id="dlPolicyLOB" tabindex="5" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                            </select>
                        </td>
                        <td id="tdLineOfBusinesstxt" runat="server">
                            <input name="txtPolicyLOB" type="text" id="txtPolicyLOB" tabindex="6" size="30" onchange="javascript:return datachanged('true');"
                                onkeydown="javascript:return onEnter('POLICY_CLAIM_LOB','txtPolicyLOB');" onblur="javascript:return onLostFocus('POLICY_CLAIM_LOB','txtPolicyLOB','Search');"
                                runat="server" />
                            <input type="button" class="EllipsisControl" id="Button1" onclick="CodeSelect('POLICY_CLAIM_LOB', 'txtPolicyLOB', 'Search', '')"
                                tabindex="7" />
                            <input type="hidden" id="txtPolicyLOB_cid" runat="server" />
                        </td>
                        <%-- </tr>
            <tr xmlns="">--%>
                        <td id="tdLineOfBusinessBr" runat="server">
                            <br />
                        </td>
                        <td id="tdPolicyNum" runat="server">Policy Number:
                        </td>
                        <td id="tdPolicyNumdl" class="hide" runat="server">
                            <select name="dlPolicyNumber" id="dlPolicyNumber" tabindex="28" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdPolicyNumtxt" runat="server">

                            <input name="txtPolicyNumber" type="text" id="txtPolicyNumber" tabindex="29" size="30" maxlength="15"
                                runat="server" />
                        </td>
                    </tr>
                    <tr xmlns="">
                        <td id="tdPolicyState" runat="server">Policy State:
                        </td>
                        <td id="tdPolicyStatedl" class="hide" runat="server">
                            <select name="dlState" id="dlState" tabindex="8" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                            </select>
                        </td>
                        <td id="tdPolicyStatetxt" runat="server">
                            <input name="txtState" type="text" id="txtState" tabindex="9" size="30" onchange="javascript:return datachanged('true');"
                                onkeydown="javascript:return onEnter('STATES','txtState');" onblur="javascript:return onLostFocus('STATES','txtState','Search');"
                                runat="server" />
                            <input type="button" class="EllipsisControl" id="btnState" onclick="CodeSelect('STATES', 'txtState', 'Search', '')"
                                tabindex="10" />
                            <input type="hidden" id="txtState_cid" runat="server" />
                        </td>
                        <%--</tr>
            <tr xmlns="">--%>
                        <td id="tdPolicyStateBr" runat="server">
                            <br />
                        </td>
                        <td id="tdLocCompany" runat="server">
                            <asp:Label ID="lbLocComapny" Text="Location Company:" runat="server"></asp:Label>
                        </td>
                        <td id="tdLocCompanydl" class="hide" runat="server">
                            <select name="dlLocCompany" id="dlLocCompany" tabindex="30" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdLocCompanytxt" runat="server">
                            <input name="txtLocCompany" type="text" id="txtLocCompany" tabindex="31" size="30" maxlength="2"
                                runat="server" />
                        </td>
                    </tr>

                    <tr xmlns="">
                        <td id="tdLossdate" runat="server">Date of Loss:
                        </td>
                        <td id="tdLossdatedl" class="hide" runat="server">
                            <select name="dlLossDate" id="dlLossDate" tabindex="11" onchange="return BetweenOption('date','dlLossDate');"
                                style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdLossdatetxt" runat="server">

                            <input name="txtLossDate" type="text" id="txtLossDate" tabindex="12" rmxref="" rmxtype="date" size="30"
                                onchange="datachanged('true')" onblur="dateLostFocusOld(this.id);" runat="server" />

                            <div id="dvPointDatePicker" runat="server">
                                <script type="text/javascript">
                                    $(function () {
                                        $("#txtLossDate").datepicker({
                                            showOn: "button",
                                            buttonImage: "../../Images/calendar.gif",
                                            //buttonImageOnly: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true,
                                            changeYear: true
                                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "13");
                                    });
                                </script>
                            </div>

                        </td>

                        <td id="tdLossdateBr" runat="server">
                            <br />
                        </td>

                        <td id="tdCustNum" runat="server">Customer Number:
                        </td>
                        <td id="tdCustNumdl" class="hide" runat="server">
                            <select name="dlCustomerNo" id="dlCustomerNo" tabindex="32" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdCustNumtxt" runat="server">
                            <input name="txtCustomerNo" type="text" id="txtCustomerNo" tabindex="33" size="30" maxlength="10"
                                runat="server" />
                        </td>
                    </tr>
                    <tr xmlns="" id="trTPAAccess" runat="server">
                        <td id="tdMasterCmpny" runat="server">
                            <%--MITS:33371 ajohari2: Start--%>
                            <asp:Label ID="lblMasterCompany" Text="Master Company:" runat="server"></asp:Label>
                            <%--MITS:33371 ajohari2: End--%>
                        </td>
                        <td id="tdMasterCmpnydl" class="hide" runat="server">
                            <select name="dlMasterCompany" id="dlMasterCompany" tabindex="13" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdMasterCmpnytxt" runat="server">
                            <input name="txtMasterCompany" type="text" id="txtMasterCompany" tabindex="14" size="30" maxlength="2"
                                runat="server" />
                        </td>
                        <td id="tdMasterCmpnyBr" runat="server">
                            <br />
                        </td>
                        <td id="tdAgentNum" runat="server">
                            <%--MITS:33371 ajohari2: Start--%>
                            <asp:Label ID="lblAgentNumber" Text="Agent Number:" runat="server"></asp:Label>
                            <%--MITS:33371 ajohari2:End--%>
                        </td>
                        <td id="tdAgentNumdl" class="hide" runat="server">
                            <select name="dlAgentName" id="dlAgentName" tabindex="34" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdAgentNumtxt" runat="server">
                            <input name="txtAgentName" type="text" id="txtAgentName" tabindex="35" size="32" runat="server" />
                        </td>


                    </tr>
                    <tr>
                        <td id="tdInsuredZip" runat="server">Insured Zip:
                        </td>
                        <td id="tdInsuredZipdl" class="hide" runat="server">
                            <select name="dlZip" id="dlZip" tabindex="15" runat="server" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdInsuredZiptxt" runat="server">
                            <input name="txtZip" type="text" id="txtZip" tabindex="16" size="30" runat="server" maxlength="10" />
                        </td>

                        <td id="tdInsuredZipBr" runat="server">
                            <br />
                        </td>

                        <td id="tdInsuredCity" runat="server">
                            <asp:Label runat="server" ID="lblInsuredCityAddr" Text="Insured City:"></asp:Label>
                        </td>
                        <td id="tdInsuredCitydl" class="hide" runat="server">
                            <select name="dlInsuredCityAddr" id="dlInsuredCityAddr" tabindex="36" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdInsuredCitytxt" runat="server">
                            <input name="txtInsuredCityAddr" type="text" id="txtInsuredCityAddr" tabindex="37" maxlength="28" runat="server" size="30" />
                        </td>
                    </tr>

                    <tr xmlns="" runat="server" id="trCFOnRow1">
                        <td id="tdInsuredSSN" runat="server">Insured SSN:
                        </td>
                        <td id="tdInsuredSSNdl" class="hide" runat="server">
                            <select name="dlInsuredSSN" id="dlInsuredSSN" tabindex="17" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td class="style3" id="tdInsuredSSNtxt" runat="server">
                            <input name="txtInsuredSSN" type="text" id="txtInsuredSSN" tabindex="18" size="30" maxlength="11"
                                runat="server" onblur="ssnLostFocus(this)" />
                        </td>
                        <td id="tdInsuredSSNBr" runat="server">
                            <br />
                        </td>
                        <td class="style4" id="tdGrpNum" runat="server">Group Number:
                        </td>
                        <td id="tdGrpNumdl" class="hide" runat="server">
                            <select name="dlGroupNo" id="dlGroupNo" tabindex="38" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdGrpNumtxt" runat="server">
                            <input name="txtGroupNo" type="text" id="txtGroupNo" tabindex="39" size="30" maxlength="10" runat="server" />
                        </td>
                    </tr>
                    <tr xmlns="" runat="server" id="trCFOnRow2">
                        <td id="tdInsuredLN" runat="server">Insured Last Name:
                        </td>
                        <td id="tdInsuredLNdl" class="hide" runat="server">
                            <select name="dlInsuredLastName" id="dlInsuredLastName" tabindex="19" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td class="style3" id="tdInsuredLNtxt" runat="server">
                            <input name="txtInsuredLastName" type="text" id="txtInsuredLastName" tabindex="20" maxlength="60"
                                size="30" runat="server" />
                        </td>
                        <td id="tdInsuredLNBr" runat="server">
                            <br />
                        </td>
                        <td class="style4" id="tdInsuredFN" runat="server">Insured First Name:
                        </td>
                        <td id="tdInsuredFNdl" class="hide" runat="server">
                            <select name="dlInsuredFirstName" id="dlInsuredFirstName" tabindex="40" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdInsuredFNtxt" runat="server">
                            <input name="txtInsuredFirstName" type="text" id="txtInsuredFirstName" tabindex="41" maxlength="40"
                                size="30" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="trCFOffRow1" xmlns="">
                        <td id="tdInsuredName" runat="server">
                            <asp:Label runat="server" ID="lblName" Text="Insured Name:"></asp:Label>
                        </td>
                        <td id="tdInsuredNamedl" class="hide" runat="server">
                            <select name="dlName" id="dlName" tabindex="21" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td id="tdInsuredNametxt" runat="server">
                            <input name="txtNm" type="text" id="txtNm" tabindex="22" size="30" runat="server" maxlength="30" />
                        </td>
                        <td colspan="4" id="tdInsuredNameBr" runat="server"></td>
                        <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                    </tr>
                    <div id="dvClaimDateReported" runat="server">
                        <tr xmlns="">
                            <td id="tdClaimRptDate" runat="server">Claim Date Reported:
                            </td>
                            <td id="tdClaimRptDate1" class="hide" runat="server">
                                <select name="dlClaimRptDate1" id="dlClaimRptDate1" tabindex="23" onchange="return BetweenOption('date','dlClaimRptDate1');"
                                    style='display: none;'>
                                    <option value="=">=</option>
                                    <option value="&lt;>">&lt;&gt;</option>
                                    <option value=">">&gt;</option>
                                    <option value="">=">&gt;=</option>
                                    <option value="&lt;">&lt;</option>
                                    <option value="&lt;=">&lt;=</option>
                                </select>
                            </td>
                            <td id="tdClaimDateRpttxt" runat="server">
                                <input name="txtClaimDateRpt" type="text" readonly="true" id="txtClaimDateRpt" tabindex="24" rmxref="" rmxtype="date" size="30"
                                    onchange="datachanged('true')" onblur="dateLostFocusOld(this.id);" runat="server" />

                            </td>
                        </tr>
                    </div>

                </table>


                <br />
                <table border="0">
                    <tr>
                        <td align="left" colspan="3">

                            <asp:Button ID="bSubmit" Text="Submit Query" class="button" TabIndex="42"
                                runat="server" OnClientClick="pleaseWait.Show();" OnClick="btnSubmit_Click" />

                            <input type="submit" name="btnClear" value="Clear" onclick="onResetClear(); return false;"
                                id="btnClear" tabindex="43" class="button" />
                        </td>
                    </tr>
                </table>
            </div>

            <div id="divINTEGRALCriteria" runat="server" style="display: block">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr xmlns="">
                        <td class="required" style="width: 243px;">
                            <asp:Label ID="Label2" Text="Line Of Business:" runat="server"></asp:Label>
                        </td>
                        <td>
                            <select name="dlPolicyLOB" id="Select3" tabindex="44" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                            </select>
                        </td>
                        <td>
                            <input name="txtIntegralPolicyLOB" type="text" id="txtIntegralPolicyLOB" tabindex="45" size="30" onchange="javascript:return datachanged('true');"
                                onkeydown="javascript:return onEnter('POLICY_CLAIM_LOB','txtIntegralPolicyLOB');" onblur="javascript:return onLostFocus('POLICY_CLAIM_LOB','txtIntegralPolicyLOB','Search');"
                                runat="server" />
                            <input type="button" class="EllipsisControl" id="Button3" onclick="CodeSelect('POLICY_CLAIM_LOB', 'txtIntegralPolicyLOB', 'Search', '')"
                                tabindex="46" />
                            <input type="hidden" id="Hidden1" runat="server" />
                        </td>
                        <td>&nbsp;</td>
                        <td>Policy Number:
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <input name="txtIntegralPolicyNumber" type="text" id="txtIntegralPolicyNumber" tabindex="57" size="30" maxlength="15"
                                runat="server" />
                        </td>
                    </tr>
                    <tr xmlns="">
                        <td class="auto-style1">Insured Last Name:
                        </td>
                        <td>
                            <select name="dlPolicySymbol" id="Select4" tabindex="47" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td class="auto-style1">
                            <input name="txtIntegralInsuredLastName" type="text" id="txtIntegralInsuredLastName" tabindex="48" maxlength="60"
                                size="30" runat="server" /></td>
                        <td class="auto-style1">
                            <br />
                        </td>
                        <td class="auto-style1">Insured First Name:
                        </td>
                        <td class="auto-style1"></td>
                        <td class="auto-style1">
                            <input name="txtIntegralInsuredFirstName" type="text" id="txtIntegralInsuredFirstName" tabindex="58" maxlength="40"
                                size="30" runat="server" /></td>
                    </tr>
                    <tr xmlns="">
                        <td>Agent Last Name:
                        </td>
                        <td>
                            <select name="dlPolicySymbol" id="Select6" tabindex="49" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td>
                            <input name="txtIntegralAgentLastName" type="text" id="txtIntegralAgentLastName" tabindex="50" size="30" runat="server" /></td>
                        <td>
                            <br />
                        </td>
                        <td>Agent First Name:
                        </td>
                        <td></td>
                        <td>
                            <input name="txtIntegralAgentFirstName" type="text" id="txtIntegralAgentFirstName" tabindex="59" size="30" runat="server" /></td>
                    </tr>
                    <tr xmlns="">
                        <td>Agent Number:</td>
                        <td>
                            <select name="dlPolicySymbol" id="Select7" tabindex="51" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td>
                            <input name="txtIntegralAgentNumber" type="text" id="txtIntegralAgentNumber" tabindex="52" size="30" runat="server" /></td>
                        <td>&nbsp;</td>
                        <td>Date of Loss:
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <input name="txtIntegralLossDate" type="text" id="txtIntegralLossDate" tabindex="60" rmxref="" rmxtype="date" size="30"
                                onblur="dateLostFocus(this.id);" runat="server" />
                            <div id="dvDatePicker" runat="server">
                                <script type="text/javascript">
                                    $(function () {
                                        $("#txtIntegralLossDate").datepicker({
                                            showOn: "button",
                                            buttonImage: "../../Images/calendar.gif",
                                            buttonImageOnly: true,
                                            showOtherMonths: true,
                                            selectOtherMonths: true,
                                            changeYear: true
                                        });
                                    });
                                </script>
                            </div>
                        </td>
                    </tr>
                    <tr xmlns="" > <!-- Removed style='display:none' by vkumar258(Vehicle Chasis Number Display Issue Fixed) -->
                        <td>Vehicle Chasis Number:
                        </td>
                        <td>
                            <select name="dlPolicySymbol" id="Select8" tabindex="53" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td class="style3">
                            <input name="txtIntegralVehicleChasisNumber" type="text" id="txtIntegralVehicleChasisNumber" tabindex="54" maxlength="60"
                                size="30" runat="server" /></td>
                        <td>
                            <br />
                        </td>
                        <td class="style4">Vehicle Engine Number:
                        </td>
                        <td>
                            <select name="dlInsuredFirstName" id="Select5" tabindex="61" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td>
                            <input name="txtIntegralVehicleEngineNumber" type="text" id="txtIntegralVehicleEngineNumber" tabindex="62" maxlength="40"
                                size="30" runat="server" /></td>
                    </tr>
                    <tr xmlns="" > <!-- Removed style='display:none' by vkumar258(Vehicle Registration Number Display Issue Fixed) -->
                        <td>Vehicle Registration Number:
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <input name="txtIntegralVehicleRegistrationNumber" type="text" id="txtIntegralVehicleRegistrationNumber" tabindex="55" maxlength="60"
                                size="30" runat="server" /></td>
                        <td>
                            <br />
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="dlPolicyNumber" id="Select2" tabindex="56" style='display: none;'>
                                <option value="=">=</option>
                                <option value="&lt;>">&lt;&gt;</option>
                                <option value=">">&gt;</option>
                                <option value="">=">&gt;=</option>
                                <option value="&lt;">&lt;</option>
                                <option value="&lt;=">&lt;=</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <br />
                <table border="0">
                    <tr>
                        <td align="left" colspan="3">
                            <asp:Button ID="bSubmitInt" Text="Submit Query" class="button" TabIndex="63"
                                runat="server" OnClientClick="return ValidateSearch();pleaseWait.Show();" OnClick="btnSubmit_Click" />
                            <input type="submit" name="btnClear" value="Clear" onclick="onResetClear(); return false;"
                                id="btnClearInt" tabindex="64" class="button" />
                        </td>
                    </tr>
                </table>
            </div>
            <%--MITS 35932 - End--%>
            <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
            <%--////Ashish Ahuja: Claims Made Jira 1342--%>
            <asp:HiddenField ID="hdnClaimEventSettings" Value="" runat="server" />
            <asp:HiddenField ID="hdnClaimRptDateType" Value="" runat="server" />
            <asp:HiddenField ID="hdnClaimDate" Value="" runat="server" />
            <asp:HiddenField ID="hdnEventDate" Value="" runat="server" />
            <asp:HiddenField ID="hdnClaimType" Value="" runat="server" />
            <asp:HiddenField ID="hdnClaimId" Value="" runat="server" />
            <!-- Pradyumna - WWIG - Gap16 - Start -->
            <asp:HiddenField ID="hdnPolicySrchSys" Value="" runat="server" />
            <asp:HiddenField ID="hdnRemoveEmtpyFields" runat="server" Value="" />
            <!-- Pradyumna - WWIG - Gap16 - end -->
            <!--tanwar2 - WWIG - Requirement 1.4.1 - start-->
            <input type="hidden" id="hdnNoRequiredField" runat="server" />
            <!--tanwar2 - WWIG - Requirement 1.4.1 - end-->
            <asp:HiddenField ID="hdPolicyProduct" Value="" runat="server" />
            <input type="hidden" id="hdnPolSysType" runat="server" />
            <asp:HiddenField ID="hdPolicyAppURL" Value="" runat="server" />
            <%--RMA-10039--%>
        </div>
    </form>

</body>
</html>
