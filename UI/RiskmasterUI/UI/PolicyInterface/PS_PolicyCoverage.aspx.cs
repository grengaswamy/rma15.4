﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Telerik.Web.UI;
using System.Text;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_PolicyCoverage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGridWithRecords();
            }

        }

        private void BindGridWithRecords()
        {
            XmlDocument resultDoc = null;
            DataTable resultDataSet = null;
            bool bInSuccess = false;
          //  int iPolicyId = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("PolicyID"), out bInSuccess);
            string sUnitId = AppHelper.GetQueryStringValue("recordID");
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry objEnReq = null;
            PolicyEnquiry objEnRes = null;
            string sParentPage = string.Empty;
                
            try
            {
                objHelper = new PolicyInterfaceBusinessHelper();
                objEnReq = new PolicyEnquiry();
                objEnRes = new PolicyEnquiry();
               
                objEnReq.PolicyIdentfier = AppHelper.GetQueryStringValue("PolicyId");
                objEnReq.PolicySymbol = AppHelper.GetQueryStringValue("PolicyName");
                sParentPage = AppHelper.GetQueryStringValue("ParentPage");
                resultDoc = new XmlDocument();
                //call the function to fetch the unit details
                if (string.Equals(sParentPage, "property", StringComparison.InvariantCultureIgnoreCase))
                {
                    objEnReq.PropertyPIN = sUnitId;
                    objEnRes = objHelper.GetPropertyCoverageDetailResult(objEnReq);
                    resultDoc.LoadXml(objEnRes.PropertyListAcordXML);
                }
                else
                {
                    objEnReq.AutoVIN = sUnitId;
                    objEnRes = objHelper.GetAutoCoverageDetailResult(objEnReq);
                    resultDoc.LoadXml(objEnRes.AutoListAcordXML);
                }
                
              
                //Call function here to fetch the xml
                if (resultDoc != null)
                {
                    resultDataSet = ConvertXmlDocToDataSet(resultDoc);

                    if (resultDataSet != null)
                    {
                        //if (resultDataSet.Tables.Count > 0)
                        //{
                            gvCoverage.DataSource = resultDataSet;
                            gvCoverage.DataBind();
                        //}
                    }
                }
                else
                {
                    throw new ApplicationException("Error occured while fetching policy unit coverages.");
                }

            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
            }
            finally
            {
                resultDataSet = null;
                resultDoc = null;
            }
        }
        private DataTable ConvertXmlDocToDataSet(XmlDocument resultDoc)
        {
            //DataSet dSet = null;
            string sAmt = string.Empty;
            DataTable dt = new DataTable();
            dt.Columns.Add("CoverageCd", typeof(string));
            dt.Columns.Add("CoverageLimit", typeof(string));
            dt.Columns.Add("PerPersonLimit", typeof(string));
            dt.Columns.Add("OccuranceLimit", typeof(string));
            dt.Columns.Add("Deductible", typeof(string));
            dt.Columns.Add("EffectiveDate", typeof(string));
            dt.Columns.Add("ExpirationDate", typeof(string));
            try
            {
                foreach (XmlNode node in resultDoc.SelectNodes("//Coverage"))
                {
                    DataRow dr = dt.NewRow();

                    dr["CoverageCd"] = node.SelectSingleNode("CoverageDesc").InnerText;
                    foreach (XmlNode innerNode in node.SelectNodes("Limit"))
                    {
                        if (innerNode.SelectSingleNode("LimitAppliesToCd").InnerText == "CoverageLimit")
                        {
                            sAmt = innerNode.SelectSingleNode("FormatCurrencyAmt/Amt").InnerText;
                            if (string.IsNullOrEmpty(sAmt))
                                sAmt ="0.00";
                            if (string.IsNullOrEmpty(innerNode.SelectSingleNode("FormatCurrencyAmt/CurCd").InnerText))
                                dr["CoverageLimit"] = "$" + sAmt;
                            else
                                dr["CoverageLimit"] = innerNode.SelectSingleNode("FormatCurrencyAmt/CurCd").InnerText + sAmt;
                        }
                        else if (innerNode.SelectSingleNode("LimitAppliesToCd").InnerText == "PerPerson")
                        {
                            sAmt = innerNode.SelectSingleNode("FormatCurrencyAmt/Amt").InnerText;
                            if (string.IsNullOrEmpty(sAmt))
                                sAmt = "0.00";
                            if (string.IsNullOrEmpty(innerNode.SelectSingleNode("FormatCurrencyAmt/CurCd").InnerText))
                                dr["PerPersonLimit"] = "$" + sAmt;
                            else
                                dr["PerPersonLimit"] = innerNode.SelectSingleNode("FormatCurrencyAmt/CurCd").InnerText + sAmt;
                        }
                        else if (innerNode.SelectSingleNode("LimitAppliesToCd").InnerText == "PerOccurrence")
                        {
                            sAmt = innerNode.SelectSingleNode("FormatCurrencyAmt/Amt").InnerText;
                            if (string.IsNullOrEmpty(sAmt))
                                sAmt = "0.00";
                            if (string.IsNullOrEmpty(innerNode.SelectSingleNode("FormatCurrencyAmt/CurCd").InnerText))
                                dr["OccuranceLimit"] = "$" + sAmt;
                            else
                                dr["OccuranceLimit"] = innerNode.SelectSingleNode("FormatCurrencyAmt/CurCd").InnerText+sAmt;
                        }
                    }
                    sAmt = node.SelectSingleNode("Deductible/FormatCurrencyAmt/Amt").InnerText;
                    if (string.IsNullOrEmpty(sAmt))
                        sAmt = "0.00";

                    if (string.IsNullOrEmpty(node.SelectSingleNode("Deductible/FormatCurrencyAmt/CurCd").InnerText))
                        dr["Deductible"] = "$" + sAmt;
                    else
                        dr["Deductible"] = node.SelectSingleNode("Deductible/FormatCurrencyAmt/CurCd").InnerText + sAmt;

                    dr["EffectiveDate"] = node.SelectSingleNode("EffectiveDt").InnerText;
                    dr["ExpirationDate"] = node.SelectSingleNode("ExpirationDt").InnerText;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            //try
            //{
            //    dSet = new DataSet();
            //    dSet.ReadXml(new XmlNodeReader(resultDoc));

            //    return dSet;
            //}
            //catch (Exception ex)
            //{
            //    throw new ApplicationException(ex.Message);
            //}
        }
    }
 }
