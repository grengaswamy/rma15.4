﻿/**********************************************************************************************
*   Date     |  MITS/JIRA   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 33574        | pgupta93   | Changes for Continuous Trigger and Policy Search functionality
* 06/04/2014 | 33371        | ajohari2    | Changes for hide some control BES ON
* 7/24/2014  | RMA-718      | ajohari2   | Changes for TPA Access ON/OFF
**********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Telerik.Web.UI;
using System.Text;
using System.Xml.Linq; //MITS:33371 ajohari2
using System.Configuration;
using System.Web.Configuration;

namespace Riskmaster.UI.PolicyInterface
{
    public partial class PolicySysDownload : NonFDMBasePageCWS //MITS:33371 ajohari2
    {

        private string strRptDate = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            // Added by Pradyumna MITS 33414 12/09/2013
            bool bReturnStatus = false;
            bool bSuccess = false;
            string sReturnValue = string.Empty;
            XElement xmlTemplate = null;
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode xmlNode = null;
            // Pradyumna Addition ends
            ErrorControl.Visible = false;

            if (!Page.IsPostBack)
            {

                PopulateControls();

                
                hdnClaimType.Value = AppHelper.GetQueryStringValue("ClaimType");
                hdnClaimId.Value = AppHelper.GetQueryStringValue("claimId");
                hdnEventDate.Value = AppHelper.GetQueryStringValue("EventDate");
                hdnClaimDate.Value = AppHelper.GetQueryStringValue("ClaimDate");
                //txtLossDate.Value = AppHelper.GetQueryStringValue("PolicyDateCriteria");//Ashish Ahuja: Claims Made Jira 1342
                txtLossDate.Value = AppHelper.GetQueryStringValue("EventDate");
                txtIntegralLossDate.Value = AppHelper.GetQueryStringValue("PolicyDateCriteria");
                txtPolicyLOB.Value = AppHelper.GetQueryStringValue("PolicyLobCode");
                txtPolicyLOB_cid.Value = AppHelper.GetQueryStringValue("PolicyLobId");
                txtIntegralPolicyLOB.Value = AppHelper.GetQueryStringValue("PolicyLobCode");
                txtClaimDateRpt.Value = AppHelper.GetQueryStringValue("ClaimDateReported");//Ashish Ahuja: Claims Made Jira 1342
                hdnClaimEventSettings.Value = AppHelper.GetQueryStringValue("ClaimEventSettings");////Ashish Ahuja: Claims Made Jira 1342 Part 2
                txtIntegralPolicyLOB_cid.Value = AppHelper.GetQueryStringValue("PolicyLobId");

                if (hdnClaimId.Value.Trim() != string.Empty) // if opening from claim screen, keep loss date read only; if opening from maintenance , keep loss date editable
                {
                    txtLossDate.Attributes.Add("ReadOnly", "ReadOnly");
                    dvPointDatePicker.Visible = false;
                    txtIntegralLossDate.Attributes.Add("ReadOnly", "ReadOnly");
                    dvDatePicker.Visible = false;
                }
                //Ashish Ahuja: Claims Made Jira 1342
                else
                {
                    dvPolicySearchCriteria.Visible = false;
                    dvClaimDateReported.Visible = false;
                }


                //MITS:33371 ajohari2: Start
                bool BESStaus = GetBESStatus();
                if (BESStaus)
                {
                    trTPAAccess.Style.Add("display", "none");//JIRA RMA-718 ajohari2
                }

                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);//vkumar258 ML changes
                }
                //MITS:33371 ajohari2: End

                xmlTemplate = GetMessageTemplate1();
                bReturnStatus = CallCWSFunction("PolicySystemInterfaceAdaptor.GetClaimReportedDateType", out sReturnValue, xmlTemplate);
                xmlDoc.LoadXml(sReturnValue);
                xmlNode = xmlDoc.SelectSingleNode("//Document/ClaimDateRptType");
                if (xmlNode != null)
                {
                    hdnClaimRptDateType.Value = xmlNode.InnerText.Trim();
                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            hdnClaimType.Value = AppHelper.GetQueryStringValue("ClaimType");
            string sQueryString = string.Empty;

            if (PolicyType1.Checked)
            {
                hdnClaimEventSettings.Value = "0";

            }
            else
            {
                hdnClaimEventSettings.Value = "1";
            }

            sQueryString = "?rptDate=" + hdnClaimRptDateType.Value;
            //JIRA 1342 ajohari2: Start
            int PolicySearchByDate = 0;

            if (PolicyType1.Checked)//Event
            {
                PolicySearchByDate = 0;
            }
            else if (PolicyType2.Checked)//Claim
            {
                PolicySearchByDate = 1;
            }

            //JIRA 1342 ajohari2: End

            if (dlSystemName.SelectedValue.Split('|')[4].Equals("INTEGRAL"))
            {
                sQueryString = "?PolicyDateCriteria=" + txtIntegralLossDate.Value;// +"policysearchsys=" + hdnPolicySrchSys.Value;
                Server.Transfer("IntegralPolicySysDownloadResults.aspx" + sQueryString, true);
            }

            else if (hdnClaimType.Value != "")
            {
                if (String.Compare(hdnPolicySrchSys.Value.ToLower(), "true", true) == 0)
                    sQueryString += "&policysearchsys=" + hdnPolicySrchSys.Value;

                Server.Transfer("PolicySysDownloadResults.aspx" + sQueryString + "&ClaimEventSettings=" + hdnClaimEventSettings.Value + "&ReportedDate=" + txtClaimDateRpt.Value + "&ClaimDate=" + hdnClaimDate.Value + "&EventDate=" + hdnEventDate.Value + "&PolicySearchByDate=" + PolicySearchByDate, true);

            }
            else
            {
                sQueryString += "&policysearchsys=" + hdnPolicySrchSys.Value + "&ClaimEventSettings=" + hdnClaimEventSettings.Value + "&DateOfLoss=" + txtLossDate.Value + "&ReportedDate=" + txtClaimDateRpt.Value + "&PolicySearchByDate=" + PolicySearchByDate;
                Server.Transfer("PolicySysDownloadResults.aspx" + sQueryString, true);
            }

        }

        private void PopulateControls()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            PolicySystemList oResponse = null;
            DataSet oDS = new DataSet();
            string[] sList;
            string sValue = string.Empty;
            string sClientFileSetting = string.Empty;
            string sLocCompny = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            string xpathStaging = "/PolicySystemList/option[contains(@cf_lc_value,'STAGING') and @default_policy_flag='-1']"; //RMA-10039
            string xpathCyberLife = "/PolicySystemList/option[contains(@cf_lc_value,'CLCSA') and @default_policy_flag='-1']"; //RMA-10039
            bool bAppIsDefault = false; //RMA-10039

            try
            {

                oResponse = objHelper.GetPolicySystemList();

                if (oResponse != null && !string.IsNullOrEmpty(oResponse.ResponseXML))
                {
                    //start: JIRA : RMA-10039 - PolicyApp integration 
                    xmlDoc.LoadXml(oResponse.ResponseXML);
                    if (xmlDoc.SelectSingleNode(xpathStaging) != null || xmlDoc.SelectSingleNode(xpathCyberLife) != null)
                    {
                        bAppIsDefault = true;
                    }
                    PreparePolicyAppURL();
                    //end JIRA RMA-10039

                    oDS.ReadXml(new StringReader(xmlDoc.InnerXml));
                    dlSystemName.DataSource = oDS.Tables[1];
                    dlSystemName.DataTextField = "option_text";
                    dlSystemName.DataValueField = "cf_lc_value";
                    dlSystemName.DataBind();
                    
                    if (!bAppIsDefault) //RMA-10039
                    {// POINT or INTEGRAL processing
                        if (AppHelper.GetQueryStringValue("SystemName") != "")
                        {
                            dlSystemName.SelectedValue = AppHelper.GetQueryStringValue("SystemName");
                        }
                        sList = dlSystemName.SelectedValue.Split('|');
                        if (sList.Length > 4)
                            hdPolicyProduct.Value = sList[4].ToString();  // get product name of default policy system
                        if (hdPolicyProduct.Value.ToUpper().Equals("INTEGRAL"))
                        { // Integral Policy Search processing
                            divINTEGRALCriteria.Style.Add("display", "block");
                            divPOINTCriteria.Style.Add("display", "none");

                        }
                        else
                        { // POINT Policy Search processing
                            divINTEGRALCriteria.Style.Add("display", "none");
                            divPOINTCriteria.Style.Add("display", "block");
                            sClientFileSetting = sList[1];

                            sLocCompny = sList[2];
                            if (sList[2].Trim() != string.Empty)
                                txtLocCompany.Value = sList[2];

                            if (sList[3].Trim() != string.Empty)
                                txtMasterCompany.Value = sList[3];

                            if (sList[4].Trim() != string.Empty)
                                hdnPolSysType.Value = sList[4];

                            if (sClientFileSetting == "1")
                            {

                                trCFOffRow1.Style.Add("display", "none");

                            }
                            else if (sClientFileSetting == "0")
                            {

                                trCFOnRow1.Style.Add("display", "none");
                                trCFOnRow2.Style.Add("display", "none");
                            }

                        }
                    }
                    else 
                    {// Start RMA-11493
					    //STAGING / CLCSA  -default selection is skipped and app is opened on policy system change
                        ListItem liSelect = new ListItem("Select...", "");
                        dlSystemName.Items.Insert(0, liSelect);
                        divINTEGRALCriteria.Style.Add("display", "none");
                        divPOINTCriteria.Style.Add("display", "none");
                    } // End RMA-11493
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                ErrorControl.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oResponse != null)
                    oResponse = null;
            }

        }
        ////Ashish Ahuja: Claims Made Jira 1342
        private XElement GetMessageTemplate1()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PolicySystemInterfaceAdaptor.GetClaimReportedDateType</Function></Call><Document><Document>");
            sXml = sXml.Append("</Document></Document></Message>");

            XElement oTemplate = XElement.Parse(Convert.ToString(sXml));
            return oTemplate;
        }
        // Added by Pradyumna 
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PolicySystemInterfaceAdaptor.GetPolicySearchSystem</Function></Call><Document><Document>");
            sXml = sXml.Append("</Document></Document></Message>");

            XElement oTemplate = XElement.Parse(Convert.ToString(sXml));
            return oTemplate;
        }
        //MITS:33371 ajohari2: Start
        /// <summary>
        /// This will return the BESStatus
        /// </summary>
        /// <returns></returns>
        public bool GetBESStatus()
        {
            //JIRA RMA-718 ajohari2: Start
            bool BESStatus = false;
            bool TPAAccessStatus = false;
            XmlDocument objXml = new XmlDocument();
            XmlElement xmlBESStatus = null;
            XmlElement xmlEnableTPAAccess = null;
            XElement XmlTemplate = null;
            try
            {
                XmlTemplate = GetMessageTemplateBESStatus();
                string sCWSresponse = string.Empty;
                CallCWS("SetUpPolicySystemAdaptor.GetBESStatus", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument oFDMPageDom = new XmlDocument();
                oFDMPageDom.LoadXml(sCWSresponse);
                xmlBESStatus = (XmlElement)oFDMPageDom.SelectSingleNode("/ResultMessage/Document/BESStatusInfo/BESStatus");
                xmlEnableTPAAccess = (XmlElement)oFDMPageDom.SelectSingleNode("/ResultMessage/Document/BESStatusInfo/EnableTPAAccess");
                if (xmlBESStatus != null)
                {
                    BESStatus = Convert.ToBoolean(xmlBESStatus.InnerText);
                }
                if (xmlEnableTPAAccess != null)
                {
                    TPAAccessStatus = Convert.ToBoolean(xmlEnableTPAAccess.InnerText);
                }
                if (BESStatus && TPAAccessStatus)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //JIRA RMA-718 ajohari2: End
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }

        }

        /// <summary>
        /// Reads Policy Download System Search settings 
        /// </summary>
        /// Pradyumna MITS 35885 04/02/2014 
        private void ReadConfigurationSettings()
        {
            XmlDocument objPolConfigSetting = null;
            XmlNode objNode = null;
            string sJsonConfigSetkting = string.Empty;
            try
            {
                objPolConfigSetting = new XmlDocument();
                objPolConfigSetting = GetPolicySearchConfigDetails();

                if (objPolConfigSetting != null)
                {
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/SysName");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/SysNameText");
                            if (objNode != null)
                            {
                                tdPolicySymbol.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdSysName.Attributes.Add("class", "hide");
                            tdSysNametxt.Attributes.Add("class", "hide");
                            tdSysNameBr.Attributes.Add("class", "hide");
                        }
                    }

                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/PolSym");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/PolSymText");
                            if (objNode != null)
                            {
                                tdPolicySymbol.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdPolicySymbol.Attributes.Add("class", "hide");
                            tdPolicySymboltxt.Attributes.Add("class", "hide");
                            tdPolicySymbolBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/Module");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/ModuleText");
                            if (objNode != null)
                            {
                                tdPolicyModule.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdPolicyModule.Attributes.Add("class", "hide");
                            tdPolicyModuletxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/Lob");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/LobText");
                            if (objNode != null)
                            {
                                lbLineOfBusiness.Text = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdLineOfBusiness.Attributes.Add("class", "hide");
                            tdLineOfBusinesstxt.Attributes.Add("class", "hide");
                            tdLineOfBusinessBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/PolNum");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/PolNumText");
                            if (objNode != null)
                            {
                                tdPolicyNum.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdPolicyNum.Attributes.Add("class", "hide");
                            tdPolicyNumtxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/PolState");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/PolStateText");
                            if (objNode != null)
                            {
                                tdPolicyState.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdPolicyState.Attributes.Add("class", "hide");
                            tdPolicyStatetxt.Attributes.Add("class", "hide");
                            tdPolicyStateBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/LocCmpny");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/LocCmpnyText");
                            if (objNode != null)
                            {
                                lbLocComapny.Text = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdLocCompany.Attributes.Add("class", "hide");
                            tdLocCompanytxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/LossDate");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/LossDateText");
                            if (objNode != null)
                            {
                                tdLossdate.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdLossdate.Attributes.Add("class", "hide");
                            tdLossdatetxt.Attributes.Add("class", "hide");
                            tdLossdateBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/AgntNum");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/AgntNumText");
                            if (objNode != null)
                            {
                                lblAgentNumber.Text = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdAgentNum.Attributes.Add("class", "hide");
                            tdAgentNumtxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdZip");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdZipText");
                            if (objNode != null)
                            {
                                tdInsuredZip.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdInsuredZip.Attributes.Add("class", "hide");
                            tdInsuredZiptxt.Attributes.Add("class", "hide");
                            tdInsuredZipBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/CustNum");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/CustNumText");
                            if (objNode != null)
                            {
                                tdCustNum.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdCustNum.Attributes.Add("class", "hide");
                            tdCustNumtxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/MstrCmpny");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/MstrCmpnyText");
                            if (objNode != null)
                            {
                                lblMasterCompany.Text = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdMasterCmpny.Attributes.Add("class", "hide");
                            tdMasterCmpnytxt.Attributes.Add("class", "hide");
                            tdMasterCmpnyBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdCity");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdCityText");
                            if (objNode != null)
                            {
                                lblInsuredCityAddr.Text = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdInsuredCity.Attributes.Add("class", "hide");
                            tdInsuredCitytxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdSSN");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdSSNText");
                            if (objNode != null)
                            {
                                tdInsuredSSN.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdInsuredSSN.Attributes.Add("class", "hide");
                            tdInsuredSSNtxt.Attributes.Add("class", "hide");
                            tdInsuredSSNBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/GrpNum");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/GrpNumText");
                            if (objNode != null)
                            {
                                tdGrpNum.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdGrpNum.Attributes.Add("class", "hide");
                            tdGrpNumtxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdLastName");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdLastNameText");
                            if (objNode != null)
                            {
                                tdInsuredLN.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdInsuredLN.Attributes.Add("class", "hide");
                            tdInsuredLNtxt.Attributes.Add("class", "hide");
                            tdInsuredLNBr.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdFirstName");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdFirstNameText");
                            if (objNode != null)
                            {
                                tdInsuredFN.InnerText = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdInsuredFN.Attributes.Add("class", "hide");
                            tdInsuredFNtxt.Attributes.Add("class", "hide");
                        }
                    }
                    objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdName");
                    if (objNode != null)
                    {
                        if (String.Compare(objNode.InnerText, "True", true) == 0)
                        {
                            objNode = objPolConfigSetting.SelectSingleNode("//PolicySearchConfig/InsrdNameText");
                            if (objNode != null)
                            {
                                lblName.Text = objNode.InnerText.Trim() + ":";
                            }
                        }
                        else
                        {
                            tdInsuredName.Attributes.Add("class", "hide");
                            tdInsuredNametxt.Attributes.Add("class", "hide");
                            tdInsuredNameBr.Attributes.Add("class", "hide");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// This used to generate a xml format
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplateBESStatus()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>SetUpPolicySystemAdaptor.GetBESStatus</Function></Call>");
            sXml = sXml.Append("<Document>");
            sXml = sXml.Append("<BESStatusInfo>");
            sXml = sXml.Append("<BESStatus>");
            sXml = sXml.Append("</BESStatus>");
            sXml = sXml.Append("</BESStatusInfo>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        //MITS:33371 ajohari2: End

        /// <summary>
        /// Get the Policy System Download Search configuration 
        /// </summary>
        /// <returns></returns>
        private XmlDocument GetPolicySearchConfigDetails()
        {
            XElement messageElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = GetConfigMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>PolicySearchConfigAdaptor.GetSavedPolicySrchConfig</Function>
              </Call>
              <Document>
                 <PolicySearchConfig>
                  </PolicySearchConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }     
        
        //start: JIRA : RMA-10039 
        private void PreparePolicyAppURL()
        {
            string session = AppHelper.GetSessionId();
            string userName = AppHelper.GetUserLoginName();
            string dnsName = AppHelper.ReadCookieValue("DsnName");
           
            string sAppLocation = "/RiskmasterUI/UX/policyApp";
            if (WebConfigurationManager.AppSettings["PolicyAppUrl"] != null)
                sAppLocation = WebConfigurationManager.AppSettings["PolicyAppUrl"].ToString();

            hdPolicyAppURL.Value = sAppLocation + "/index.html#/login?id=" + session + "&userName=" + userName + "&dsnName=" + dnsName + "&clientId=" + AppHelper.ClientId;
        }
        //RMA-10039 - end
    }
}