﻿<%--**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 **********************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 **********************************************************************************************--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_UnitListing.aspx.cs"
    Inherits="Riskmaster.UI.UI.PolicyInterface.PS_UnitListing" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Policy Unit Download</title>
    <style type="text/css">
        .SelectedItem
        {
            background: none repeat scroll 0 0 #6699FF !important;
        }
    </style>
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script type="text/JavaScript" src="../../../Scripts/Utilities.js"></script>
     <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">         { var i; }
    </script>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <!-- Pradyumna MITS#35296 03062014 - Start -->
    <script type="text/javascript">
        function chkBoxChange(id) {
            if (document.getElementById('hdnPolicySrchSys') != null && document.getElementById('hdnPolicySrchSys').value.toLowerCase() == "true") {
                if (document.getElementById('hdnMultiUnitDownload') != null && document.getElementById('hdnMultiUnitDownload').value == "false") {
                    document.getElementById("txtChkBoxSelected").value = "";
                    var items = document.getElementsByTagName('input');
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].type === "checkbox") {
                            items[i].checked = false;
                        }
                    }
                    document.getElementById(id).checked = true;
                }
            }
        }
    </script>
    <!-- Pradyumna MITS#35296 03062014 - End -->
</head>
<body onbeforeunload="doUnload();"  onmousedown="mouseclicked();">
    <form id="frmData" name="frmData" method="post" runat="server">
    <uc1:errorcontrol id="ErrorControl1" runat="server" />
    <div class="msgheader" id="formtitle">
        <asp:Label ID="lblMode" runat="server" Text="Download Units"/></div>
    <div id="SearchData" runat="server">
    <table>
    <tr>
    <td align="center">
    <table>
            <tr>
                <td>
                    Unit Number
                </td>
                <td>
                    <asp:TextBox ID="tbUnitNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Unit Description
                </td>
                <td>
                    <asp:TextBox ID="tbDesc" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Unit Type
                </td>
                <td>
                    <asp:TextBox ID="tbUnitType" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Status
                </td>
                <td>
                    <asp:TextBox ID="tbStatus" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle ID
                </td>
                <td>
                    <asp:TextBox ID="tbVin" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    City
                </td>
                <td>
                    <asp:TextBox ID="tbCity" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    State
                </td>
                <td>
                    <asp:TextBox ID="tbState" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSearch" Text="Search" runat="server" OnClientClick="return ShowSearch();"
                        onclick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </td>
    </tr>
    </table>
        
    </div>
    <telerik:RadScriptManager id="ScriptManager1" enablepagemethods="true" runat="server" />
        <telerik:RadStyleSheetmanager id="RadStyleSheetManager1" runat="server" />
         <telerik:RadAjaxManager id="RadAjaxManager2" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="gvDownloadItems">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="gvConfirmSaveList" />
                        <telerik:AjaxUpdatedControl ControlID="txtChkBoxSelected2" />
                         <telerik:AjaxUpdatedControl ControlID="gvDownloadItems" />
                        <telerik:AjaxUpdatedControl ControlID="txtChkBoxSelected" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    <div id="DataGrid" runat="server">
                
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <asp:TextBox ID="txtChkBoxSelected" runat="server" Style="display: none"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                            <td width="80%">
                                <div>
                                    <telerik:RadGrid runat="server" id="gvDownloadItems" autogeneratecolumns="false"
                                        onneeddatasource="gvDownloadItems_NeedDataSource" pagesize="25" onitemdatabound="gvDownloadItems_ItemDataBound"
                                        allowsorting="True" allowpaging="True" allowfilteringbycolumn="True" gridlines="None"
                                        skin="Office2007" onprerender="gvDownloadItems_PreRender">
                                        <SelectedItemStyle CssClass="SelectedItem" />
                                        <ClientSettings AllowKeyboardNavigation="true">
                                            <ClientEvents OnCommand="onCommand" />
                                            <Scrolling SaveScrollPosition="true" AllowScroll="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                        <MasterTableView Width="97.9%">
                                            <RowIndicatorColumn>
                                                <HeaderStyle Width="20px" />
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn>
                                                <HeaderStyle Width="20px" />
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridTemplateColumn AllowFiltering="false" ItemStyle-Width="1px" UniqueName="SelectColumn">
                                                    <ItemTemplate>
                                                    <%--Pradyumna MITS#35296 03062014 Added OnClick Event--%>
                                                        <asp:CheckBox ID="chkBox" runat="server" onclick="chkBoxChange(this.id);" />
                                                        <asp:HiddenField ID="SequenceNumber" runat="server" />
                                                        <asp:HiddenField ID="UnitNumber" runat="server" />
                                                       </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="StatUnitNo" HeaderText="Unit Number" FilterControlWidth="50px"
                                                    UniqueName="StatUnitNo" SortExpression="StatUnitNo">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="UnitType" FilterControlWidth="50px" HeaderText="Type Of Unit"
                                                    UniqueName="UnitType" SortExpression="UnitType">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Status" HeaderText="Status" FilterControlWidth="80%"
                                                    UniqueName="Status" SortExpression="Status">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Desc" HeaderText="Description/Address" UniqueName="Desc"
                                                    FilterControlWidth="50px" SortExpression="Desc">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Vin" HeaderText="Vin" UniqueName="Vin" FilterControlWidth="50px"
                                                    SortExpression="Vin">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="City" HeaderText="City" FilterControlWidth="70px"
                                                    UniqueName="City" SortExpression="City">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="State" HeaderText="State" FilterControlWidth="70px"
                                                    UniqueName="State" SortExpression="State">
                                                </telerik:GridBoundColumn>
                                                 <telerik:GridBoundColumn DataField="InsLineCd" HeaderText="Ins Line" FilterControlWidth="70px"
                                                    UniqueName="InsLineCd" SortExpression="InsLineCd">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                            <ItemStyle CssClass="datatd1" />
                                            <AlternatingItemStyle CssClass="datatd" />
                                            <HeaderStyle CssClass="msgheader" />
                                        </MasterTableView>
                                        <PagerStyle Mode="NextPrevAndNumeric" Position="Top" AlwaysVisible="true" />
                                        <FilterMenu EnableTheming="True">
                                            <CollapseAnimation Duration="200" Type="OutQuint" />
                                        </FilterMenu>
                                    </telerik:RadGrid>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="hdnCbId" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
           
                <td>
                    <asp:Button ID="btnNext" Text="Next" OnClientClick="return ValidateSelection();"
                        runat="server" OnClick="btnNext_Click" />
                </td>
                <td><input type="button" value="Cancel" id="btnCancelWithNext" onclick="onCancelClick();" title="This will cancel the Unit Download, whereas the Policy Download will still continue." />
                   <%-- <asp:Button ID="btnCancel" Text="Cancel" OnClientClick="return onCancelClick();"
                        runat="server" />--%>
                </td>
            </tr>
        </table>
    </div>
    <div id="Div1" runat="server">
        
       
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <asp:TextBox ID="txtChkBoxSelected2" runat="server" Style="display: none"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                            <td width="80%">
                                <div>
                                    <telerik:RadGrid runat="server" id="gvConfirmSaveList" autogeneratecolumns="false"
                                        onneeddatasource="gvConfirmSaveList_NeedDataSource" pagesize="25" onitemdatabound="gvConfirmSaveList_ItemDataBound"
                                        allowsorting="true" allowpaging="true" allowfilteringbycolumn="true" gridlines="None"
                                        skin="Office2007" onprerender="gvDownloadItems_PreRender">
                                        <SelectedItemStyle CssClass="SelectedItem" />
                                        <ClientSettings AllowKeyboardNavigation="true">
                                            <ClientEvents OnCommand="onCommand1" />
                                            <Scrolling SaveScrollPosition="true" AllowScroll="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                        <MasterTableView Width="97.9%">
                                            <RowIndicatorColumn>
                                                <HeaderStyle Width="20px" />
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn>
                                                <HeaderStyle Width="20px" />
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridTemplateColumn AllowFiltering="false" ItemStyle-Width="1px" UniqueName="SelectColumn">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkBox1" runat="server" />
                                                        <asp:HiddenField ID="SequenceNumber1" runat="server" />
                                                         <asp:HiddenField ID="UnitNumber1" runat="server" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="StatUnitNo" HeaderText="Unit Number" FilterControlWidth="50px"
                                                    UniqueName="StatUnitNo" SortExpression="StatUnitNo">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="UnitType" FilterControlWidth="50px" HeaderText="Type Of Unit"
                                                    UniqueName="UnitType" SortExpression="UnitType">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Status" HeaderText="Status" FilterControlWidth="80%"
                                                    UniqueName="Status" SortExpression="Status">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Desc" HeaderText="Description/Address" UniqueName="Desc"
                                                    FilterControlWidth="50px" SortExpression="Desc">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Vin" HeaderText="Vin" UniqueName="Vin" FilterControlWidth="50px"
                                                    SortExpression="Vin">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="City" HeaderText="City" FilterControlWidth="70px"
                                                    UniqueName="City" SortExpression="City">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="State" HeaderText="State" FilterControlWidth="70px"
                                                    UniqueName="State" SortExpression="State">
                                                </telerik:GridBoundColumn>
                                                 <telerik:GridBoundColumn DataField="InsLineCd" HeaderText="Ins Line" FilterControlWidth="70px"
                                                    UniqueName="InsLineCd" SortExpression="InsLineCd">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                            <ItemStyle CssClass="datatd1" />
                                            <AlternatingItemStyle CssClass="datatd" />
                                            <HeaderStyle CssClass="msgheader" />
                                        </MasterTableView>
                                        <PagerStyle Mode="NextPrevAndNumeric" Position="Top" AlwaysVisible="true" />
                                        <FilterMenu EnableTheming="True">
                                            <CollapseAnimation Duration="200" Type="OutQuint" />
                                        </FilterMenu>
                                    </telerik:RadGrid>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnSave" Text="Save" runat="server" OnClick="btnSave_Click" OnClientClick = "return FinalSelection();" />
                </td>
                <td><input type="button" value="Cancel" id="btnCancelWithConfirm" onclick="onCancelClick();" />
                   <%-- <asp:Button ID="btnCancel2" Text="Cancel" OnClientClick="return onCancelClick();"

                        runat="server" />--%>
                </td>
            </tr>
        </table>
    </div>
    <div id="NoRecords" style="display:none" runat="server"><b> No Records Found, please refine search criteria.</b></div>
    <asp:TextBox ID="txtMode" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSelectedValues" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtUpdateWith" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtPolicyId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtAddEntityAs" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtLossDate" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtPolicyFlag" runat="server" Style="display: none"></asp:TextBox><%--MITS:33574--%>
    <asp:TextBox ID="txtLoBCode" runat="server" Style="display: none"></asp:TextBox><%--MITS:33574--%>
        <asp:TextBox ID="hdnClaimDateReported" runat="server" Style="display: none"></asp:TextBox><%--//Ashish Ahuja: Claims Made Jira 1342--%>
      <asp:HiddenField runat="server" ID="hfException" />
      <%--Pradyumna 03142014- MITS# 35296 - Allow Single/Multiple unit Download - Start--%>
      <asp:HiddenField ID="hdnMultiUnitDownload" Value="" runat="server" />
      <%--Pradyumna 03142014- MITS# 35296 - Allow Single/Multiple unit Download - Ends--%>
      <%--tanwar2 - Download Policy from Staging - start--%>
      <asp:TextBox ID="txtStagingPolicyId" runat="server" Style="display: none"></asp:TextBox>
      <asp:HiddenField id="hdnPolicySrchSys"  value="" runat="server"/>
      <%--tanwar2 - Download Policy from Staging - end--%>

    <script type="text/javascript">
    <!--
        function onCommand(sender, e) {//debugger;
            if (e.get_commandName() == "Page" || e.get_commandName() == "Sort" || e.get_commandName() == "Filter") {
               
                UpdateSelection(sender.get_masterTableView(), document.getElementById("txtChkBoxSelected"));
            }
        }
        function onCommand1(sender, e) {//debugger;
            if (e.get_commandName() == "Page" || e.get_commandName() == "Sort" || e.get_commandName() == "Filter") {

                UpdateSelection(sender.get_masterTableView(), document.getElementById("txtSelectedValues"));
            }
        }
        function UpdateSelection(MasterTable, sSelectedValues)
        {
        
           // var MasterTable = gvDownloadItems.get_masterTableView();
            var selectedRows = MasterTable.get_dataItems();
            var rCount = selectedRows.length;
           // var sSelectedValue = "";


            for (var rowIdx = 0; rowIdx < rCount; rowIdx++) {
                var uniqueId;
                var row = selectedRows[rowIdx];
                var cell = MasterTable.getCellByColumnUniqueName(row, "SelectColumn")

                var chkBox = cell.children[0];
                uniqueId = cell.children[1];
                if (chkBox.checked) {


                    if (sSelectedValues.value == "")
                        sSelectedValues.value = "'" + uniqueId.value + "'";
                    else if (sSelectedValues.value.indexOf("'" + uniqueId.value + "'") == -1)
                        sSelectedValues.value = sSelectedValues.value + "," + "'" + uniqueId.value + "'";

                }
                else {
                    var flag = 0;

                    var str = sSelectedValues.value.split(',');

                    for (var i = 0 ; i < str.length ; i++) {

                        if (str[i] == "'" + uniqueId.value + "'") {
                            str[i] = 0;
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1) {
                        sSelectedValues.value = "";
                        for (var i = 0; i < str.length; i++) {

                            if (str[i] != 0)
                                if (sSelectedValues.value == "")
                                    sSelectedValues.value = str[i];
                                else
                                    sSelectedValues.value = sSelectedValues.value + "," + str[i];


                        }
                    }
                }


            }
        }

       
        function ShowSearch() {//debugger;
            
            var gvDownloadItems = $find("<%=gvDownloadItems.ClientID %>");
            var MasterTable = gvDownloadItems.get_masterTableView();
            var selectedRows = MasterTable.get_dataItems();
            var rCount = selectedRows.length;
            var sSelectedValue = document.getElementById("txtSelectedValues").value;
            var addEntityAs = "";

            for (var rowIdx = 0; rowIdx < rCount; rowIdx++) {

                var row = selectedRows[rowIdx];
                var cell = MasterTable.getCellByColumnUniqueName(row, "SelectColumn")

                var chkBox = cell.children[0];
                if (chkBox.checked) {
                    var uniqueId;

                    uniqueId = cell.children[1];
                    if (sSelectedValue == "")
                        sSelectedValue = "'" + uniqueId.value + "'";
                    else
                        sSelectedValue = sSelectedValue + "," + "'" + uniqueId.value + "'";

                }

            }
        }
        function FinalSelection() {
            //debugger;
            var gvDownloadItems = $find("<%=gvConfirmSaveList.ClientID %>");
            //var MasterTable = gvDownloadItems.get_masterTableView();
            //var selectedRows = MasterTable.get_dataItems();
            //var rCount = selectedRows.length;
            UpdateSelection(gvDownloadItems.get_masterTableView(), document.getElementById("txtSelectedValues"));
            //var sSelectedValue = document.getElementById("txtSelectedValues").value;
            //var addEntityAs = "";
            //sSelectedValue = "";
            //for (var rowIdx = 0; rowIdx < rCount; rowIdx++) {

            //    var row = selectedRows[rowIdx];
            //    var cell = MasterTable.getCellByColumnUniqueName(row, "SelectColumn")

            //    var chkBox = cell.children[0];
            //    if (chkBox.checked) {
            //        var uniqueId;

            //        uniqueId = cell.children[1];
            //        if (sSelectedValue == "")
            //            sSelectedValue = "'" + uniqueId.value + "'";
            //        else
            //            sSelectedValue = sSelectedValue + "," + "'" + uniqueId.value + "'";

            //    }

            //}
            //document.getElementById("txtSelectedValues").value = sSelectedValue;
            if (document.getElementById("txtSelectedValues").value == "") {
                alert("Please Select any " + document.getElementById("txtMode").value);
                return false;
            }
            pleaseWait.Show();
            return true;
          
        }

        function ValidateSelection() {
            var gvDownloadItems = $find("<%=gvDownloadItems.ClientID %>");
            UpdateSelection(gvDownloadItems.get_masterTableView(), document.getElementById("txtChkBoxSelected"));
            if (document.getElementById("txtChkBoxSelected").value == "") {
                alert("Please Select any " + document.getElementById("txtMode").value);
                return false;
            }
            pleaseWait.Show();
         }

        function showPolicySuccess() {

            alert("Policy Data successfully downloaded");

            try {
                //MITS:33574 START
                //window.opener.ValidatePolicy(document.getElementById("txtPolicyId").value);
                if (document.getElementById("txtPolicyFlag").value != '') {
                    window.opener.ValidatePolicyWithoutClaim(document.getElementById("txtPolicyId").value, document.getElementById("txtLoBCode").value, document.getElementById("hdnClaimDateReported").value);
                }
                else
                {
                 	// Pradyumna - Modified for GAP16 - WWIG 12/19/2013- Start
                	if (document.getElementById('hdnPolicySrchSys') != null && document.getElementById('hdnPolicySrchSys').value.toLowerCase() == "true") {
                    	window.opener.ValidatePolicy(document.getElementById("txtPolicyId").value, '1');
                	}
                	else {
                 window.opener.ValidatePolicy(document.getElementById("txtPolicyId").value);
                	}
                // Pradyumna - Modified for GAP16 - WWIG 12/19/2013- Ends
                }
                //MITS:33574 END
            }
            catch (e) { };

            window.close();
            return false;
        }

        function onCancelClick() {
             isClose = false;
            window.close();
            return false;
        }
      var isClose = false;
      var isSuccess = false;
      //this code will handle the F5 or Ctrl+F5 key Collapse | Copy Code//need to handle more cases like ctrl+R whose codes are not listed here
      document.onkeydown = checkKeycode;
      function checkKeycode(e) {
          var keycode;
          if (window.event)
              keycode = window.event.keyCode;
          else if (e)
              keycode = e.which;
          if (keycode == 116) {
              isClose = true;
          }
          if (event.altKey && keycode == 115) {
              isClose = false;
          }
      }
      function mouseclicked() {
      
                isClose = true;
            }
            function doUnload() {//debugger;
                
                var isException = document.getElementById("hfException");
                if (isException != null && isException.value == "") {
                    if (!isClose && !isSuccess) {
                        //                    if (document.getElementById("txtmode").value == "unit") {
                        isClose = true;
                        window.close();
                        //}
                        // window.opener.OpenPolicyPages(document.getElementById("txtmode").value, document.getElementById("txtPolicyId").value);
                        showPolicySuccess();
                    }
                }
            }

            //tanwar2 - Policy Download from staging - start
            function NoUnits() {
                isClose = false;
                isSuccess = false;
                window.close();
            }
            //tanwar2 - Policy Download from staging - end

//-->
    </script>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading"  />
    </form>
</body>
</html>
