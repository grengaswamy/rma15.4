<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_PolicyCoverage.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_PolicyCoverage" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Policy Coverage</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
    </script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
    </script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
  </head>
  <body class="10pt" >
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Policy Coverage" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABScoverage" id="TABScoverage">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="coverage" id="LINKTABScoverage">Coverage Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPcoverage">
          <nbsp />
          <nbsp />
        </div>                
      </div>
      <div class="singletopborder" runat="server" name="FORMTABcoverage" id="FORMTABcoverage">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
                <asp:gridview id="gvCoverage" runat="server" Width="100%" AutoGenerateColumns="false" GridLines="None" HorizontalAlign="Left">
                <HeaderStyle CssClass="colheader6" ForeColor="White" HorizontalAlign="Left"/>
                <AlternatingRowStyle CssClass="data2" />
                <Columns>
                        <asp:BoundField HeaderText="Coverage Type" HeaderStyle-CssClass="headerlink2"  ItemStyle-CssClass="data" DataField="CoverageCd"/>
                        <asp:BoundField HeaderText="Policy Limit" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="CoverageLimit"/>
                        <asp:BoundField HeaderText="Per Person Limit" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="PerPersonLimit"/>
                        <asp:BoundField HeaderText="Occurance Limit" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data"  DataField="OccuranceLimit"/>
                         <asp:BoundField HeaderText="SIR/Deductible" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data"  DataField="Deductible"/>
                        <asp:BoundField HeaderText="Effective Date" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data"  DataField="EffectiveDate"/>
                        <asp:BoundField HeaderText="Expiration Date" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data"  DataField="ExpirationDate"/>
                    </Columns>  
                </asp:gridview>
		    </td>
          </tr>
          <tr>
            <td>
              </td></tr>
        </table>
      </div>   
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>
