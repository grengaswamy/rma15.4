<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_PolicyInsurer.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_PolicyInsurer" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Policy Insurer</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />   
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Policy Insurer" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSinsurerinfo" id="TABSinsurerinfo">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="insurerinfo" id="LINKTABSinsurerinfo">Insurer Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPinsurerinfo">
          <nbsp />
          <nbsp />
        </div>        
      </div>
      <div class="singletopborder" runat="server" name="FORMTABinsurerinfo" id="FORMTABinsurerinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdinsurerinfo" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="policyid" RMXRef="/Instance/PolicyXInsurer/PolicyId|/Instance/UI/FormVariables/SysExData/PolicyId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="inrowid" RMXRef="/Instance/PolicyXInsurer/InRowID" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="insentityid" RMXRef="/Instance/PolicyXInsurer/InsurerCode/@codeid|/Instance/PolicyXInsurer/InsurerEntity/EntityId" RMXType="id" />
              <div runat="server" class="half" id="div_inslastname" xmlns="">
                <asp:label runat="server" class="required" id="lbl_inslastname" Text="Insurer" />
                <span class="formw">
                  <asp:label runat="server" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/LastName" RMXType="entitylookup" id="inslastname" />                  
                </span>
              </div>
              <div runat="server" class="half" id="div_insphone1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insphone1" Text="Office Phone" />
                <span class="formw">
                  <asp:label runat="server" id="insphone1" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/Phone1" RMXType="phone" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_inscontact" xmlns="">
                <asp:label runat="server" class="label" id="lbl_inscontact" Text="Contact" />
                <span class="formw">
                  <asp:label runat="server" id="inscontact" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/Contact" RMXType="text" />
                </span>
              </div>
              <div runat="server" class="half" id="div_insphone2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insphone2" Text="Alt. Phone" />
                <span class="formw">
                  <asp:label runat="server" id="insphone2" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/Phone2" RMXType="phone" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_insaddr1" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insaddr1" Text="Address 1" />
                <span class="formw">
                  <asp:label runat="server" id="insaddr1" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/Addr1" RMXType="text" />
                </span>
              </div>
              <div runat="server" class="half" id="div_insfaxnumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insfaxnumber" Text="Fax" />
                <span class="formw">
                  <asp:label runat="server" id="insfaxnumber" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/FaxNumber" RMXType="phone" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_insaddr2" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insaddr2" Text="Address 2" />
                <span class="formw">
                  <asp:label runat="server" id="insaddr2" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/Addr2" RMXType="text" />
                </span>
              </div>
              <div runat="server" class="half" id="div_insemailaddress" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insemailaddress" Text="Email Address" />
                <span class="formw">
                  <asp:label runat="server" id="insemailaddress" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/EmailAddress" RMXType="text" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_inscity" xmlns="">
                <asp:label runat="server" class="label" id="lbl_inscity" Text="City" />
                <span class="formw">
                  <asp:label runat="server" id="inscity" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/City" RMXType="text" />
                </span>
              </div>
              <div runat="server" class="half" id="div_preamount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_preamount" Text="Premium" />
                <span class="formw">
                  <asp:label runat="server" id="preamount" RMXRef="/Instance/PolicyXInsurer/PreAmount" RMXType="currency" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_insstateid" xmlns="">
                <asp:label runat="server" class="label" id="lbl_insstateid" Text="State" />
                <span class="formw">                  
                  <asp:label runat="server" id="insstateid" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/StateId" RMXType="code" />
                </span>
              </div>
              <div runat="server" class="half" id="div_portionpartlayer" xmlns="">
                <asp:label runat="server" class="label" id="lbl_portionpartlayer" Text="Portion of Part of Layer" />
                <span class="formw">
                  <asp:label runat="server" id="portionpartlayer" RMXRef="/Instance/PolicyXInsurer/PortionPartLayer" RMXType="numeric" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_inszipcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_inszipcode" Text="Zip" />
                <span class="formw">
                  <asp:label runat="server" id="inszipcode" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/ZipCode" RMXType="zip" />
                </span>
              </div>
              <div runat="server" class="half" id="div_parttotallayer" xmlns="">
                <asp:label runat="server" class="label" id="lbl_parttotallayer" Text="Part of Total Layer" />
                <span class="formw">
                  <asp:label runat="server" id="parttotallayer" RMXRef="/Instance/PolicyXInsurer/PartTotalLayer" RMXType="numeric" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_inscountrycode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_inscountrycode" Text="Country" />
                <span class="formw">                  
                  <asp:label runat="server" id="inscountrycode" RMXRef="/Instance/PolicyXInsurer/InsurerEntity/CountryCode" RMXType="code" />
                </span>
              </div>
              <div runat="server" class="half" id="div_paymentamount" xmlns="">
                <asp:label runat="server" class="label" id="lbl_paymentamount" Text="Installment Payment" />
                <span class="formw">
                  <asp:label runat="server" id="paymentamount" RMXRef="/Instance/PolicyXInsurer/PaymentAmount" RMXType="currency" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_respercentage" xmlns="">
                <asp:label runat="server" class="label" id="lbl_respercentage" Text="Responsible Percentage" />
                <span class="formw">
                  <asp:label runat="server" id="respercentage" RMXRef="/Instance/PolicyXInsurer/ResPercentage" RMXType="numeric" />
                </span>
              </div>
              <div runat="server" class="half" id="div_schedulecode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_schedulecode" Text="Installment Schedule" />
                <span class="formw">                  
                  <asp:label runat="server" id="schedulecode" RMXRef="/Instance/PolicyXInsurer/ScheduleCode" RMXType="code" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_occurancelimit" xmlns="">
                <asp:label runat="server" class="label" id="lbl_occurancelimit" Text="Occurence Limit" />
                <span class="formw">
                  <asp:label runat="server" id="occurancelimit" RMXRef="/Instance/PolicyXInsurer/OccuranceLimit" RMXType="currency" />
                </span>
              </div>
              <div runat="server" class="half" id="div_specialcircularmemo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_specialcircularmemo" Text="Special Circumstances" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="specialcircularmemo" RMXRef="/Instance/PolicyXInsurer/SpecialCircularMemo" RMXType="memo" readonly="true" TextMode="MultiLine" Columns="30" rows="3" />                  
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_commentmemo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_commentmemo" Text="Comments" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="commentmemo" RMXRef="/Instance/PolicyXInsurer/CommentMemo" RMXType="memo" readonly="true" TextMode="MultiLine" Columns="30" rows="3" />                  
                </span>
              </div>
              <div runat="server" class="half" id="div_layernumcode" xmlns="">
                <asp:label runat="server" class="label" id="lbl_layernumcode" Text="Layer Number" />
                <span class="formw">                  
                  <asp:label runat="server" id="layernumcode" RMXRef="/Instance/PolicyXInsurer/LayerNumCode" RMXType="code" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_primaryinsurer" xmlns="">
                <asp:label runat="server" class="label" id="lbl_primaryinsurer" Text="Primary Insurer" />
                <span class="formw">
                  <asp:CheckBox runat="server" id="primaryinsurer" RMXRef="/Instance/PolicyXInsurer/PrimaryInsurer" RMXType="checkbox" Height="24px" Enabled="false" />
                </span>
              </div>
              <div runat="server" class="half" id="div_coveragedesc" xmlns="">
                <asp:label runat="server" class="label" id="lbl_coveragedesc" Text="Coverage Spec Desc for Reports" />
                <span class="formw">
                  <asp:label runat="server" id="coveragedesc" RMXRef="/Instance/PolicyXInsurer/CoverageDesc" RMXType="text" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>    
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:Button class="button" runat="Server" id="BackToParent" Text="Back to Policy" OnClientClick="if(!( XFormHandler('SysCmd=0','1','back')))return false;" PostBackUrl="?SysCmd=0" />
        </div>
        <div class="formButton" runat="server" id="div_btnReinsurer">
          <asp:Button class="button" runat="Server" id="btnReinsurer" Text="Reinsurers" RMXRef="Instance/PolicyXInsurer/PolicyXReInsurerList/@committedcount" OnClientClick="if(!( XFormHandler('SysFormName=policyreinsurer&SysCmd=1&SysEx=InRowID&SysExMapCtl=InRowID','','')))return false;" PostBackUrl="policyreinsurer.aspx?SysFormName=policyreinsurer&SysCmd=1&SysEx=InRowID&SysExMapCtl=InRowID" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="insentitytableid" RMXRef="Instance/PolicyXInsurer/InsurerEntity/EntityTableId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="policy" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="policyid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="policyinsurer" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="inrowid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="9000" />
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="PolicyXInsurer" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;PolicyXInsurer&gt;&lt;InsurerEntity&gt;&lt;/InsurerEntity&gt;&lt;Supplementals&gt;&lt;/Supplementals&gt;&lt;/PolicyXInsurer&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSinsurerinfo|TABSsuppgroup" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="policyinsurer" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="inslastname|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="inslastname|" />
      <asp:TextBox style="display:none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>
