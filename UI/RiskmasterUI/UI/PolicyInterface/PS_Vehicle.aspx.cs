﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Telerik.Web.UI;
using System.Text;


namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_Vehicle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadUnitDetails();
            }
        }

        private void LoadUnitDetails()
        {
            XmlDocument objUnitXml = null;
            bool bInSuccess = false;
            string sUnitId = AppHelper.GetQueryStringValue("recordID");
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry objEnReq = null;
            PolicyEnquiry objEnRes = null;
            try
            {
                objHelper = new PolicyInterfaceBusinessHelper();
                objEnReq = new PolicyEnquiry();
                objEnRes = new PolicyEnquiry();
                objEnReq.AutoVIN = sUnitId;
                objEnReq.PolicyIdentfier = AppHelper.GetQueryStringValue("PolicyId");
                objEnReq.PolicySymbol = AppHelper.GetQueryStringValue("PolicyName");

                //call the function to fetch the unit details
                objEnRes = objHelper.GetAutoUnitDetailResult(objEnReq);
                objUnitXml = new XmlDocument();
                objUnitXml.LoadXml(objEnRes.AutoListAcordXML);
            
                //call the function to fetch the unit details
                if (objUnitXml != null)
                {
                    vehiclemake.Text = objUnitXml.SelectSingleNode("//Manufacturer").InnerText;
                    vehiclemodel.Text = objUnitXml.SelectSingleNode("//Model").InnerText;
                    vehicleyear.Text = objUnitXml.SelectSingleNode("//ModelYear").InnerText;
                    vin.Text = objUnitXml.SelectSingleNode("//com.csc_VehicleIdentificationNumber").InnerText;
                    unittype.Text = objUnitXml.SelectSingleNode("//com.csc_VehicleTypeCd").InnerText;
                }
            }
            catch (Exception e)
            {
                ErrorHelper.logErrors(e);
                lblError.Text = e.Message;
            }
            finally
            {
                objUnitXml = null;
            }
        }
    }
}