﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LimitTracking.aspx.cs" Inherits="Riskmaster.UI.PolicyInterface.LimitTracking" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <title>Limit Tracking Setup</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/cul.js" type="text/javascript"></script>
    <script src="../../Scripts/form.js" type="text/javascript"></script>
     <script type="text/javascript">
         function Validate() {
             if (document.getElementById("chkPolicyLimit").checked) {
                 var sel = document.getElementById("lstPolLimitList");
                 var listLength = sel.options.length;
                 if (listLength == 0) {
                     //alert("Coverage occurence approver not set");
                     alert(LimitTrackingValidations.NoPolLimitApprover);
                     return false;
                 }
                 

             }

             return true;
         }
         </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
       <div class="msgheader" id="formtitle">
       <asp:label text="<%$ Resources:lblPolicyLimitSetup %>" id="lblPolicyLimitSetup" runat="server" ></asp:label></div>

        <table border="0" >        
            <tr>
                 <td colspan="2" align="left">
                  
                   <asp:ImageButton ID="btnSave" ImageUrl="../../Images/tb_save_active.png" class="bold" ToolTip="Save" runat="server" OnClick="btnSave_Click" OnClientClick="return Validate();" />
                 
                 </td>
            </tr>
           
            <tr>
                <td > 
                        <asp:label text="<%$ Resources:lblPolicyLimit %>" id="lblPolicyLimit" runat="server" ></asp:label>

              
               
            </tr>
            <tr>
              
                </tr>
            <tr>
                <td class="required" align="left">
                
                    <select multiple="true" id="lstPolLimitList" size="3" runat="server" style="width: 190px; height: 71px"></select>
                    <input type="button" class="CodeLookupControl" value="" id="cmdPolLimitAddCustomizedUser" onclick="AddCustomizedListUserGroup('limittracking', 'lstPolLimitList', 'PolLimitUserIdStr', 'PolLimitUserStr', 'hdnPolLimitUserStr', 'hdnPolLimitGroupStr', 'hdnPolLimitLstGroups', 'hdnPolLimitLstUsers')"/>        
                    <input type="button" class="BtnRemove" value="" id="cmdPolLimitDelCustomizedListUser" style="width:21px" onclick="DelCustomizedListUser('limittracking', 'lstPolLimitList', 'PolLimitUserIdStr', 'PolLimitUserStr', 'hdnPolLimitUserStr', 'hdnPolLimitGroupStr', 'hdnPolLimitLstGroups', 'hdnPolLimitLstUsers')"/>
                    <input type="hidden"  runat="server" name="" value="" id="PolLimitUserIdStr"/>
                    <input type="hidden"  runat="server" value="" id="PolLimitUserStr"/>
                    <input type="hidden"  runat="server" value="" id="hdnPolLimitUserStr"/>
                    <input type="hidden"  runat="server" value="" id="hdnPolLimitGroupStr" />
                    <input type="text" runat="server" style="display:none" id="hdnPolLimitLstGroups"/>
                    <input type="text" runat="server" style="display:none" id="hdnPolLimitLstUsers"/>
                </td>
            </tr>
     
           


             <tr id="">
                    <td>
                            <asp:label text="<%$ Resources:lblPolLimitExd %>" id="lblPolLimitExd" runat="server" ></asp:label>
                    </td>
                    <td>
                        <div title="" style="padding: 0px; margin: 0px">
                            <asp:CheckBox ID="chkPolicyLimit" type="checkbox" onclick="setDataChanged(true);"   runat="server" />
                        </div>
                    </td>
                </tr>

        </table>
    
    
    </form>
</body>
</html>
