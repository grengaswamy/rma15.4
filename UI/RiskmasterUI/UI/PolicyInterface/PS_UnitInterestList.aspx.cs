﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_UnitInterestList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    LoadDetails();
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void LoadDetails()
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry oEnquiryRequest = null;
            PolicyEnquiry oEnquiryResponse = null;
            string sValues;
            string[] sCollection;
            bool bInSuccess;
            string sUnitRowId = string.Empty;
            try
            {
                oEnquiryRequest = new PolicyEnquiry();
                objHelper = new PolicyInterfaceBusinessHelper();
                sUnitRowId = AppHelper.GetQueryStringValue("UnitRowId");
                if (sUnitRowId != string.Empty)
                {
                    oEnquiryRequest.UnitRowId = Conversion.CastToType<int>(sUnitRowId, out bInSuccess);
                
                }
                else
                {
                    sValues = AppHelper.GetQueryStringValue("val");
                    sCollection = sValues.Split('|');
                    
                    oEnquiryRequest.UnitNumber = sCollection[0];
                    oEnquiryRequest.InsLine = sCollection[1];
                    oEnquiryRequest.UnitRiskLocation = sCollection[2];
                    oEnquiryRequest.UnitSubRiskLocation = sCollection[3];
                    oEnquiryRequest.UnitState = sCollection[4];
                    oEnquiryRequest.Product = sCollection[5];
                    oEnquiryRequest.PolicyNumber = sCollection[7];
                    oEnquiryRequest.PolicySymbol = sCollection[8];
                    oEnquiryRequest.LOB = sCollection[10];
                    oEnquiryRequest.IssueCode = sCollection[11];
                    oEnquiryRequest.Location = sCollection[12];
                    oEnquiryRequest.MasterCompany = sCollection[13];
                    oEnquiryRequest.Module = sCollection[14];
                    oEnquiryRequest.PolicyIdentfier = sCollection[7];
                    oEnquiryRequest.PolicySystemId = Conversion.CastToType<int>(sCollection[17], out bInSuccess);
                    //dbisht6 jira rma-369
                    oEnquiryRequest.PolLossDt = AppHelper.GetQueryStringValue("lossdate");
                    // jira rma-369 end

                    //set hidden parameters
                    hdUnitNum.Value = sCollection[0];
                    hdPolicyNum.Value = sCollection[7];
                    hdLobCd.Value = sCollection[10];
                    hdIssueCd.Value = sCollection[11];
                    hdLocatnCompny.Value = sCollection[12];
                    hdMasterCompny.Value = sCollection[13];
                    hdModule.Value = sCollection[14];
                    hdPolicySystemID.Value = oEnquiryRequest.PolicySystemId.ToString();
                    oEnquiryResponse = objHelper.GetUnitInterestListResult(oEnquiryRequest);
                }
                   PopulateAdditionalInterestList(oEnquiryResponse.ResponseAcordXML, oEnquiryRequest);
                
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception e)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(e, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                oEnquiryRequest = null;
                oEnquiryResponse = null;
            }
        }

        /// <summary>
        /// populates the Additional policy tab 
        /// </summary>
        /// <param name="pResponseXML">represents the response XML which has 
        /// the data to be populated on the UI</param>
        private void PopulateAdditionalInterestList(string pResponseXML, PolicyEnquiry oEnquiryRequest)
        {
            IEnumerable<XElement> oElements = null;
            XElement oDocElement, oSubEle1, oSubEle2, oEleAddr1, oEleCity, oTemp;
            ListItem liData;
            string sToolTip, sVal, sLocationNum, sSeqNum;

            oDocElement = XElement.Parse(pResponseXML);

            oElements = oDocElement.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitInterestListRs/InsuredOrPrincipal");

            foreach (XElement oElement in oElements)
            {
                oSubEle2 = null;
                oSubEle1 = null;
                sToolTip = string.Empty; sLocationNum = string.Empty; sSeqNum = string.Empty;
                oSubEle1 = oElement.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                oSubEle2 = oElement.XPathSelectElement("./InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd");
                oEleAddr1 = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                oEleCity = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SeqNum']/OtherId");
                sSeqNum = oTemp.Value;
                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Location']/OtherId");
                sLocationNum = oTemp.Value;

                sToolTip = "Addr: " + oEleAddr1.Value + " City: " + oEleCity.Value;
                sVal = oEnquiryRequest.PolicyNumber + "|" + oEnquiryRequest.PolicySymbol + "|" + oEnquiryRequest.Module + "|" + oEnquiryRequest.Location + "|" +
                    oEnquiryRequest.MasterCompany + "|" + oSubEle2.Value + "|" + hdPolicySystemID.Value + "|" + sLocationNum + "|" + sSeqNum + "|" + oEnquiryRequest.LOB + "|" + oEnquiryRequest.UnitNumber + "|" + oEnquiryRequest.UnitState;
              //  if (oSubEle1 != null && oSubEle2 != null && !string.IsNullOrEmpty(oSubEle1.Value) && !string.IsNullOrEmpty(oSubEle2.Value))
                if (oSubEle1 != null && !string.IsNullOrEmpty(oSubEle1.Value))
                {
                    if (!string.IsNullOrEmpty(oSubEle2.Value))
                    {
                        liData = new ListItem(oSubEle1.Value + " : " + oSubEle2.Value, sVal);
                       
                    }
                    else
                    {
                        liData = new ListItem(oSubEle1.Value , sVal);
                    }
                    lbAdditionalInfo.Items.Add(liData);
                    liData.Attributes.Add("title", sToolTip);
                }
            }
            if (lbAdditionalInfo.Items.Count == 0)
            {
                lblAdditionalPolicyMsg.Visible = true;
                btnAdditionalInterestDetails.Enabled = false;
            }
        }

        /// <summary>
        /// populates the Additional policy tab for staging 
        /// </summary>
        /// <param name="pResponseXML">represents the response XML which has 
        /// the data to be populated on the UI</param>
        private void PopulateStagingAdditionalInterestList(string pResponseXML, PolicyEnquiry oEnquiryRequest)
        {
            IEnumerable<XElement> oElements = null;
            XElement oDocElement, oSubEle1, oSubEle2,oSubEle3,oSubRole, oEleAddr1,oEleAddr2, oEleCity, oTemp;
            ListItem liData;
            string sToolTip, sVal, sLocationNum, sSeqNum;
            string sEntityId = string.Empty;
            oDocElement = XElement.Parse(pResponseXML);

            oElements = oDocElement.XPathSelectElements("InterestList");

            foreach (XElement oElement in oElements)
            {
                oSubEle2 = null;
                oSubEle1 = null;
                oSubEle3 = null;
                sToolTip = string.Empty; sLocationNum = string.Empty; sSeqNum = string.Empty;
                oSubEle1 = oElement.XPathSelectElement("Lastname");
                oSubEle2 = oElement.XPathSelectElement("Firstname");
                oSubEle3 = oElement.XPathSelectElement("Middlename");
                oSubRole = oElement.XPathSelectElement("Role");
                oEleAddr1 = oElement.XPathSelectElement("Addr1");
                oEleAddr2 = oElement.XPathSelectElement("Addr2");
                oEleCity = oElement.XPathSelectElement("City");                
                oTemp = oElement.XPathSelectElement("EntityId");
                sEntityId = oTemp.Value;

                if (!string.IsNullOrEmpty(oEleAddr2.Value))
                {
                    sToolTip = "Addr: " + oEleAddr1.Value + " " + oEleAddr2.Value + " City: " + oEleCity.Value;
                }
                else
                {
                    sToolTip = "Addr: " + oEleAddr1.Value + " City: " + oEleCity.Value;
                }
                sVal = sEntityId;
               
                if (oSubEle1 != null && !string.IsNullOrEmpty(oSubEle1.Value))
                {
                    if (!string.IsNullOrEmpty(oSubEle2.Value))
                    {
                        if (!string.IsNullOrEmpty(oSubEle3.Value))
                        {
                            if (!string.IsNullOrEmpty(oSubRole.Value))
                            {
                                liData = new ListItem(oSubEle2.Value + " " + oSubEle3.Value + " " + oSubEle1.Value + " : " + oSubRole.Value, sVal);
                            }
                            else
                            {
                                liData = new ListItem(oSubEle2.Value + " " + oSubEle3.Value + " " + oSubEle1.Value, sVal);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(oSubRole.Value))
                            {
                                liData = new ListItem(oSubEle2.Value + " " + oSubEle1.Value + " : " + oSubRole.Value, sVal);
                            }
                            else
                            {
                                liData = new ListItem(oSubEle2.Value + " " + oSubEle1.Value, sVal);
                            }
                            
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(oSubEle3.Value))
                        {
                            if (!string.IsNullOrEmpty(oSubRole.Value))
                            {
                                liData = new ListItem(oSubEle3.Value + " " + oSubEle1.Value + " : " + oSubRole.Value, sVal);
                            }
                            else
                            {
                                liData = new ListItem(oSubEle3.Value + " " + oSubEle1.Value, sVal);
                            }
                            
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(oSubRole.Value))
                            {
                                liData = new ListItem(oSubEle1.Value + " : " + oSubRole.Value, sVal);
                            }
                            else
                            {
                                liData = new ListItem(oSubEle1.Value, sVal);
                            }
                            
                        }
                        
                    }
                    lbAdditionalInfo.Items.Add(liData);
                    liData.Attributes.Add("title", sToolTip);
                }
            }
            if (lbAdditionalInfo.Items.Count == 0)
            {
                lblAdditionalPolicyMsg.Visible = true;
                btnAdditionalInterestDetails.Enabled = false;
            }
        }

        private bool HasErrorOccured(string pResponseXML)
        {
            bool bResult = false;
            IEnumerable<XElement> oElements;
            XElement objDoc, oErrorEle;
            string sErrorText = string.Empty;

            objDoc = XElement.Parse(pResponseXML);
            oElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_ErrorList/com.csc_Error");
            if (oElements != null)
            {
                foreach (XElement oElement in oElements)
                {
                    oErrorEle = oElement.XPathSelectElement("./ERROR");
                    sErrorText = string.Concat(sErrorText, oErrorEle.Value);
                    bResult = true;
                }
            }
            if (bResult)
            {
                ErrorControl1.DisplayError(sErrorText);
                ErrorControl1.Visible = true;
            }

            return bResult;

        }
    }
}