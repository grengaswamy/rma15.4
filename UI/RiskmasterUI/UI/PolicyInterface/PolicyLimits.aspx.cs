﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PolicyLimits : NonFDMBasePageCWS
    {

        static string sSuccess = "Success";
        static string sPageName = "PolicyLimits.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);

                string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

                string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);


                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId(sPageName), "PolicyLimitsValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "PolicyLimitsValidations", sValidationResources, true);

                if (!IsPostBack)
                {
                    txtPolicyId.Text = AppHelper.GetQueryStringValue("policyid");
                    GetLimits();
                    GetEmptyErosionGrid("0","0");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        

        public  XElement GetPolicyLimitMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PolicySystemInterfaceAdaptor.GetPolicyLimits</Function></Call><Document>");
            sXml = sXml.Append("<PolicyLimits>");
            sXml = sXml.Append("<PolicyId>");
            sXml = sXml.Append(txtPolicyId.Text);
            sXml = sXml.Append("</PolicyId>");
           
            sXml = sXml.Append("<Data>");
            sXml = sXml.Append("</Data>");
            sXml = sXml.Append("<PageName>");
            sXml = sXml.Append(sPageName);
            sXml = sXml.Append("</PageName>");
            sXml = sXml.Append("<GridId>");
            sXml = sXml.Append("gridPolicyLimits");
            sXml = sXml.Append("</GridId>");
            sXml = sXml.Append("</PolicyLimits></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }

        public static  string GetErosionMessageTemplate(string sLimitId,string sPolicyId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>PolicySystemInterfaceAdaptor.GetErosionDetails</Function></Call><Document>");
            sXml = sXml.Append("<ErosionDetails>");
            sXml = sXml.Append("<LimitRowId>");
            sXml = sXml.Append(sLimitId);
            sXml = sXml.Append("</LimitRowId>");
            sXml = sXml.Append("<PolicyId>");
            sXml = sXml.Append(sPolicyId);
            sXml = sXml.Append("</PolicyId>");
            sXml = sXml.Append("<PageName>");
            sXml = sXml.Append(sPageName);
            sXml = sXml.Append("</PageName>");
            sXml = sXml.Append("<GridId>");
            sXml = sXml.Append("gridErosions");
            sXml = sXml.Append("</GridId>");
            sXml = sXml.Append("<Data>");
            sXml = sXml.Append("</Data>");

            sXml = sXml.Append("</ErosionDetails></Document></Message>");

            //XElement oTemplate = XElement.Parse(sXml.ToString());

            return sXml.ToString();
        }
        protected void PolicyLimits_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectGridRow('" + DataBinder.Eval(e.Row.DataItem, "limitid").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }
        public void GetLimits()
        {
            XmlDocument resultDoc = new XmlDocument();
            XElement messageElement;
            string sCWSresponse = "";
            XmlElement xmlIn;
            string AdditionData = string.Empty;
            DataSet objDataSet = new DataSet();
            try
            {

                messageElement = GetPolicyLimitMessageTemplate();
                CallCWS("PolicySystemInterfaceAdaptor.GetPolicyLimits", messageElement, out sCWSresponse, true, true);

                resultDoc.LoadXml(sCWSresponse);
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//PolicyLimits//Data");
                if (xmlIn != null)
                    hdnJsonData_gridPolicyLimits.Value = xmlIn.InnerText;
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//PolicyLimits//UserPref");
                if (xmlIn != null)
                    hdnJsonUserPref_gridPolicyLimits.Value = xmlIn.InnerText;
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//PolicyLimits//AdditionalData");
                if (xmlIn != null)
                    hdnJsonAdditionalData_gridPolicyLimits.Value = xmlIn.InnerText;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                resultDoc = null;
                messageElement = null; sCWSresponse = "";
                xmlIn = null;
            }

        }
        [System.Web.Services.WebMethod]
        public static string GetErosion(string LimitId,string PolicyId)
        {
             string sSuccess = "Success";
            XmlDocument resultDoc = new XmlDocument();
            StringBuilder sError = null;
            string sCWSresponse = "";
            XmlElement xmlIn;
            string AdditionData = string.Empty;
            
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>(); ;
            try
            {
                sError = new StringBuilder();
                string sReturn = AppHelper.CallCWSService(GetErosionMessageTemplate(LimitId,PolicyId));
                resultDoc.LoadXml(sReturn);
                if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    dicResponseData.Add("response", sSuccess);
                }
                else if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {                    
                    sError.Append(ErrorHelper.UpdateErrorMessage(resultDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//ErosionDetails//Data");
                if (xmlIn != null)
                    dicResponseData.Add("Data", xmlIn.InnerText);
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//ErosionDetails//UserPref");
                if (xmlIn != null)
                    dicResponseData.Add("UserPref", xmlIn.InnerText);
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//ErosionDetails//AdditionalData");
                if (xmlIn != null)
                    dicResponseData.Add("AdditionalData", xmlIn.InnerText);

                dicResponseData.Add("error", ErrorHelper.UpdateErrorMessage(sError.ToString()));                
                return JsonConvert.SerializeObject(dicResponseData);               
            }
            catch (Exception ex)
            {
                if (!dicResponseData.ContainsKey("error"))
                    dicResponseData.Add("error", ex.Message);
                return JsonConvert.SerializeObject(dicResponseData);
            }
            finally
            {
                resultDoc = null;
                xmlIn = null;
                dicResponseData = null;              
            }

        }

        public void GetEmptyErosionGrid(string LimitId,string PolicyId)
        {
            XmlDocument resultDoc = new XmlDocument();
            XElement messageElement;
            string sCWSresponse = "";
            XmlElement xmlIn;
            string AdditionData = string.Empty;
            DataSet objDataSet = new DataSet();
            try
            {

                string sReturn = AppHelper.CallCWSService(GetErosionMessageTemplate(LimitId,PolicyId));
                resultDoc.LoadXml(sReturn);
                                
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//ErosionDetails//Data");
                if (xmlIn != null)
                    hdnJsonData_gridErosions.Value = xmlIn.InnerText;
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//ErosionDetails//UserPref");
                if (xmlIn != null)
                    hdnJsonUserPref_gridErosions.Value = xmlIn.InnerText;
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//ErosionDetails//AdditionalData");
                if (xmlIn != null)
                    hdnJsonAdditionalData_gridErosions.Value = xmlIn.InnerText;

                divErosions.Style.Add("display", "none");
                //objDataSet = new DataSet();
                //objDataSet.ReadXml(new XmlNodeReader(resultDoc));


                //if ((objDataSet.Tables["Row"] != null) && (objDataSet.Tables["Row"].Rows.Count > 0))
                //{
                //    GridErosion.Visible = true;
                //    GridErosion.DataSource = objDataSet.Tables["Row"];
                //    GridErosion.DataBind();
                //    lblNoErosion.Visible = false;
                //}
                //else
                //{
                //    lblNoErosion.Visible = true;
                //    GridErosion.Visible = false;
                //}
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                resultDoc = null;
                messageElement = null; sCWSresponse = "";
                xmlIn = null;
            }

        }

        [System.Web.Services.WebMethod]
        public static string SavePreferences(string gridPreference, string gridId)
        {

            StringBuilder sbMessageTemplate = new StringBuilder();
            string sReturnResponse = "";
            Dictionary<string, string> dicResponse = new Dictionary<string, string>();
            try
            {
                gridPreference = AppHelper.HtmlEncodeString(gridPreference);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>GridPreferenceAdaptor.SaveUserHeaderAndPreference</Function>");
                sbMessageTemplate.Append("</Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<PolicyLimits>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(gridId);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>" + sPageName + "</PageName>");
                sbMessageTemplate.Append("<UserPref>" + gridPreference + "</UserPref>");
                sbMessageTemplate.Append("</PolicyLimits>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString()); //return bool.   
                XmlDocument objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(sReturn);

                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    dicResponse.Add("error", "true");
                    dicResponse.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    JsonSerializerSettings js = new JsonSerializerSettings();
                    Newtonsoft.Json.JsonSerializer o = new JsonSerializer();

                    sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
                }
                //else                    
                //    dicResponse.Add("response", sSuccess);                
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            catch (Exception ex)
            {
                dicResponse.Add("error", "true");
                dicResponse.Add("errorMessage", ex.Message);
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            finally
            {
                sbMessageTemplate = null;
                dicResponse = null;
            }
            return sReturnResponse;

        }

        /// <summary>
        /// this function is used to restore default values for the grid.
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string RestoreDefault(string inputData)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            XmlDocument objXmlDoc = new XmlDocument();
            Dictionary<string, string> dicInputData;
            string sResponseData = "";
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>();
            try
            {
                dicInputData = new Dictionary<string, string>();
                dicResponseData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(inputData, dicInputData);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>PolicySystemInterfaceAdaptor.RestoreDefaults</Function>");
                sbMessageTemplate.Append(" </Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<RestoreDefault>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(dicInputData["GridId"]);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>" + sPageName + "</PageName>");
                sbMessageTemplate.Append("</RestoreDefault>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString());
                objXmlDoc.LoadXml(sReturn);
                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    xmlIn = (XmlElement)objXmlDoc.SelectSingleNode("//UserPref");
                    if (xmlIn != null)
                    {
                        sResponseData = xmlIn.InnerText;
                    }
                }
                else
                {//error handling
                    dicResponseData.Add("error", "true");
                    dicResponseData.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
                }
            }
            catch (Exception ex)
            {
                dicResponseData.Add("error", "true");
                dicResponseData.Add("errorMessage", ex.Message);
                sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
            }

            finally
            {
                sbMessageTemplate = null;
                objXmlDoc = null;
                dicInputData = null;
                xmlIn = null;
                dicResponseData = null;
            }
            return sResponseData;
        }
    }
}