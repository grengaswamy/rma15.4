﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicySystemSetup.aspx.cs" Inherits="Riskmaster.UI.PolicyInterface.PolicySystemSetup"  EnableViewStateMac="false" ValidateRequest="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid" TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Policy System Web Settings</title>
    <link href="../../Content/zpcal/themes/system.css" rel="stylesheet" type="text/css" />
    <link href="../../App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
  <script src="../../Scripts/form.js" type="text/javascript"></script>
  
  <script src="../../Scripts/WaitDialog.js" type="text/javascript"></script> 
</head>
<body onload="parent.MDIScreenLoaded();"> 
      <form id="frmData" runat="server" method="post">
    <div>
     <table>
        <tr>
            <td colspan="2">
                <uc:ErrorControl ID="ErrorControl1" runat="server" />                
            </td>
        </tr>
    </table>
    </div>
    <div class="msgheader" id="formtitle">
        Policy System Web Settings</div>
    <div>
    <div class="completerow">
    <asp:Button ID="btnValidatePolSystem" runat="server" Text="Refresh Policy Systems" 
            onclick="btnValidatePolSystem_Click" />
    </div>
     <div class="completerow">
                        <dg:UserControlDataGrid runat="server" ID="PolicySystemGrid" GridName="PolicySystemGrid"
                            GridTitle="" Target="/Document/Document/PolicySystemList" Ref="/Instance/Document/form//control[@name='PolicySystemGrid']"
                            Unique_Id="policysystemid" ShowRadioButton="True" Width="99%" Height="100%"
                            HideNodes="|tag|policysystemid|" ShowHeader="True" LinkColumn="" PopupWidth="700"
                            PopupHeight="600;" HideButtons="" Type="GridAndButtons" AllowPaging="true" PageSize="10"/>
                        <asp:TextBox Style="display: none" runat="server" ID="PolicySystemGridSelectedId" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="PolicySystemGrid_RowDeletedFlag"
                            RMXType="id" Text="false" />
                        <asp:TextBox Style="display: none" runat="server" ID="PolicySystemGrid_Action" RMXType="id" />
                        <asp:TextBox Style="display: none" runat="server" ID="PolicySystemGrid_RowAddedFlag"
                            RMXType="id" Text="false" />
                    </div>

                     <asp:TextBox Style="display: none" runat="server" ID="HdnActionSave" Text="" />
        <asp:TextBox Style="display: none" runat="server" ID="HdnSelectedIdForDeletion" Text="" />
                <asp:TextBox Style="display: none" runat="server" ID="HdnListNameForDeletion" Text="" />
    </div>
    </form>
</body>
</html>
