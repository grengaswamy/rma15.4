﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Configuration;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_PolicyInterest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper oHelper;
            PolicyEnquiry oRequest, oResponse;
            string sValues;
            string[] sCollection;
            bool bInSuccess;
            if (!Page.IsPostBack)
            {
                try
                {
                    //tanwar2 - Policy Download from staging - start
                    if (ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"] != null)
                    {
                        hdnRemoveEmtpyFields.Value = ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"];
                    }
                    //tanwar2 - Policy Download from staging - end

                    sValues = AppHelper.GetQueryStringValue("val");
                    sCollection = sValues.Split('|');
                    oRequest = new PolicyEnquiry();

                    
                    oRequest.PolicyNumber = sCollection[0];
                    oRequest.PolicySymbol = sCollection[1];
                    oRequest.Module = sCollection[2];
                    oRequest.Location = sCollection[3];
                    oRequest.MasterCompany = sCollection[4];
                    oRequest.InterestTypeCode = sCollection[5];
                    oRequest.PolicyIdentfier = sCollection[0];
                    oRequest.PolicySystemId = Conversion.CastToType<int>(sCollection[6], out bInSuccess);
                    oRequest.UnitRiskLocation = sCollection[7];
                    oRequest.UnitSubRiskLocation = sCollection[8];
                    oRequest.LOB = sCollection[9];
                    if (sCollection.Length > 10)
                    {                        
                        oRequest.UnitNumber = sCollection[10];
                        oRequest.UnitState = sCollection[11];
                    }
                    
                    oHelper = new PolicyInterfaceBusinessHelper();
                    oResponse = oHelper.GetPolicyInterestDetailResult(oRequest);
                    if (!HasErrorOccured(oResponse.ResponseAcordXML))
                    {
                        PopulateDetails(oResponse.ResponseAcordXML);
                                                
                    }                    
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorControl1.Visible = true;
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
        }

        private void PopulateDetails(string pXML)
        {
            XElement oDoc, oTemp;

            oDoc = XElement.Parse(pXML);

            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd");
            lblCd.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/InsuredOrPrincipalInfo/InsuredOrPrincipalRoleDesc");
            lblDesc.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SequenceNum']/OtherId");
            lblIntersetSeq.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Location']/OtherId");
            lblInterestLoc.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
            lblNm.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1");
            lblOptional.Text = oTemp.Value;     // optional name binded with addr1
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2");
            lblAddr.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/Addr/City");
            lblCity.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd");
            lblState.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode");
            lblPostalCd.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ContractTerm/EffectiveDt");
            lblEffDt.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/ContractTerm/ExpirationDt");
            lblExpDt.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/com.csc_LoanAccNum");
            lblLoanNo.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='FEIN/SSN']/TaxId");
            lblFEIN.Text = oTemp.Value;
            oTemp = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='TaxId']/TaxId");
            lblTaxID.Text = oTemp.Value;
        }

        //Added by swati
        private void PopulateStagingDetails(string pXML)
        {

            XElement oDoc, oTemp, oTemp1,oTemp2, oElements;

            oDoc = XElement.Parse(pXML);
            oElements = oDoc.XPathSelectElement("AdditionalInterestList");
            oTemp = oElements.XPathSelectElement("InterestType");
            if (oTemp != null)
            {
                if (!string.IsNullOrEmpty(oTemp.Value))
                {
                    lblCd.Text = oTemp.Value;
                }
            }
            else
            {
                lblCd.Text = string.Empty;
            }
            

            lblDesc.Text = string.Empty;

            lblIntersetSeq.Text = string.Empty;
            oTemp = oElements.XPathSelectElement("County");
            if (oTemp != null)
            {
                if (!string.IsNullOrEmpty(oTemp.Value))
                {
                    lblInterestLoc.Text = oTemp.Value;
                }
            }
            else
            {
                lblInterestLoc.Text = string.Empty;
            }
            oTemp = oElements.XPathSelectElement("Lastname");
            oTemp1 = oElements.XPathSelectElement("Firstname");
            oTemp2 = oElements.XPathSelectElement("Middlename");
            if (oTemp != null)
            {
                if (oTemp1 != null)
                {
                    if (!string.IsNullOrEmpty(oTemp1.Value))
                    {
                        if (oTemp2 != null)
                        {
                            if (!string.IsNullOrEmpty(oTemp2.Value))
                            {
                                lblNm.Text = oTemp1.Value + " " + oTemp2.Value + " " + oTemp.Value;
                            }
                            else
                            {
                                lblNm.Text = oTemp1.Value + " " + oTemp.Value;
                            }
                        }
                    }
                    else
                    {
                        if (oTemp2 != null)
                        {
                            if (!string.IsNullOrEmpty(oTemp2.Value))
                            {
                                lblNm.Text = oTemp2.Value + " " + oTemp.Value;
                            }
                            else
                            {
                                lblNm.Text = oTemp.Value;
                            }
                        }
                    }
                }
            }

            oTemp = oElements.XPathSelectElement("Abbreviation");
            if (oTemp != null)
            {
                if (!string.IsNullOrEmpty(oTemp.Value))
                {
                    lblOptional.Text = oTemp.Value;
                }
            }
            else
            {
                lblOptional.Text = string.Empty;
            }
            
            oTemp = oElements.XPathSelectElement("Addr1");
            oTemp1 = oElements.XPathSelectElement("Addr2");
            if (oTemp != null)
            {
                if (oTemp1 != null)
                {
                    if (!string.IsNullOrEmpty(oTemp1.Value))
                    {
                        lblAddr.Text = oTemp.Value + ", " + oTemp1.Value;
                    }
                    else
                    {
                        lblAddr.Text = oTemp.Value;
                    }
                }
                
            }
            
            oTemp = oElements.XPathSelectElement("City");
            if (oTemp != null)
            {
                if (!string.IsNullOrEmpty(oTemp.Value))
                {
                    lblCity.Text = oTemp.Value;
                }
            }
            else
            {
                lblCity.Text = string.Empty;
            }
            oTemp = oElements.XPathSelectElement("State");
            if (oTemp != null)
            {
                if (!string.IsNullOrEmpty(oTemp.Value))
                {
                    lblState.Text = oTemp.Value;
                }
            }
            else
            {
                lblState.Text = string.Empty;
            }
            oTemp = oElements.XPathSelectElement("ZipCode");
            if (oTemp != null)
            {
                if (!string.IsNullOrEmpty(oTemp.Value))
                {
                    lblPostalCd.Text = oTemp.Value;
                }
            }
            else
            {
                lblPostalCd.Text = string.Empty;
            }

            lblEffDt.Text = string.Empty;
            lblExpDt.Text = string.Empty;
            lblLoanNo.Text = string.Empty;
            lblFEIN.Text = string.Empty;

            oTemp = oElements.XPathSelectElement("TaxId");
            if (oTemp != null)
            {
                if (!string.IsNullOrEmpty(oTemp.Value))
                {
                    lblTaxID.Text = oTemp.Value;
                }
            }
            else
            {
                lblTaxID.Text = string.Empty;
            }

        }
        //change end here by swati

        private bool HasErrorOccured(string pResponseXML)
        {
            bool bResult = false;
            IEnumerable<XElement> oElements;
            XElement objDoc, oErrorEle;
            string sErrorText = string.Empty;

            objDoc = XElement.Parse(pResponseXML);
            oElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_ErrorList/com.csc_Error");
            if (oElements != null)
            {
                foreach (XElement oElement in oElements)
                {
                    oErrorEle = oElement.XPathSelectElement("./ERROR");
                    sErrorText = string.Concat(sErrorText, oErrorEle.Value);
                    bResult = true;
                }
            }
            if (bResult)
            {
                ErrorControl1.DisplayError(sErrorText);
                ErrorControl1.Visible = true;
            }

            return bResult;

        }
    }
}