﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_PolicyInterest.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_PolicyInterest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Policy Interest Details</title>
    <%--tanwar2 - Policy Download from staging - start--%>
    <style type="text/css">
    .hide
    {
        display:none;
    }
    .td{
    display:inline-block;
    *display:inline; /* for IE7*/
    *zoom:1;/* for IE7*/
    width:24%;
    margin-top:5px;
    vertical-align:middle;
    font-family: Tahoma, Arial, Helvetica, sans-serif;
    }

    </style>
    <script src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $('document').ready(function () {
            if ($('#hdnRemoveEmtpyFields').val() === 'True') {
                //hide empty tds
                $('#dvManual table tbody tr td').each(function () {
                    if ($(this).children('span:empty()').length === $(this).children('span').length && $(this).children('span').length>0) {
                        $(this).addClass('hide');
                        $(this).prev('td').addClass('hide');
                    }
                });

                //mark trs with all empty tds
                $('#dvManual table tbody tr').each(function () {
                    if ($(this).children('td.hide').length == $(this).children('td').length) {
                        $(this).addClass('hide');
                    }
                });

                //Remove tr td having class hide
                $('.hide').remove();

                //Change all tr td to divs
                $('#dvManual').html($('#dvManual table tbody').html()
                    .replace(/<tr(.*)>/gi, '')
                    .replace(/<\/tr>/gi, '')
                    .replace(/<td/gi, '<div class="td"')
                    .replace(/<\/td/gi, '</div')
                    );

                $('.td:even').addClass('required');
            }
        });
    </script>
    <%--tanwar2 - Policy Download from staging - end--%>
</head>
<body>
    <form id="form1" runat="server" method="post">
    <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div>
    <div class="msgheader" id="formtitle">
        Policy Interest Details
    </div>
    <div id="dvManual">
    <table width="98%" border="0" cellpadding="4" cellspacing="8">
    <colgroup>
    <col width="25%" class="required" />
    <col width="25%" />
    <col width="25%" class="required" />
    <col width="25%"  />
    </colgroup>
    
    <tr>
    <td><asp:Label Text="Interest Type" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCd"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblDesc"></asp:Label></td>
    <td class='hide'></td>
    <td class='hide'></td>
    </tr>
    <tr>
    <td><asp:Label runat="server" Text="Interest Location"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblInterestLoc"></asp:Label></td>
    <td><asp:Label runat="server" Text="Interest Sequence"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblIntersetSeq"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Name" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblNm"></asp:Label></td>
    <td><asp:Label Text="Optional" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblOptional"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Address" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID ="lblAddr"></asp:Label></td>
    <td><asp:Label Text="City" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCity"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="State" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblState"></asp:Label></td>
    <td><asp:Label Text="Postal Code" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblPostalCd"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Effective Date" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblEffDt"></asp:Label></td>
    <td><asp:Label Text="Expiration Date" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblExpDt"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="FEIN" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblFEIN"></asp:Label></td>
    <td><asp:Label runat="server" Text="Loan Number"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblLoanNo"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label runat="server" Text="Tax ID"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblTaxID"></asp:Label></td>
    </tr>
    </table>
    </div>
    </div>
    <%--tanwar2 - Policy Download from staging - start--%>
    <asp:HiddenField ID="hdnRemoveEmtpyFields" runat="server" Value="" />
    <%--tanwar2 - Policy Download from staging - end--%>
    </form>
</body>
</html>
