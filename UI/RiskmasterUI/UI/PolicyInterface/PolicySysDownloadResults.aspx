﻿<%--**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 **********************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 **********************************************************************************************--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicySysDownloadResults.aspx.cs"
    Inherits="Riskmaster.UI.UI.PolicyInterface.PolicySysDownloadResults" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javaScript" src="../../Scripts/search.js" type="text/javascript"></script>
    <script language="javaScript" type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script language="javaScript" type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script language="javaScript" type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }    </script>
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js"></script>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link href="~/App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalBackground {
	    background-color:Gray;
	    filter:alpha(opacity=70);
	    opacity:0.7;
        }

        .modalPopup {
	    background-color:#ffffdd;
	    border-width:3px;
	    border-style:solid;
	    border-color:Gray;
	    padding:3px;
	    width:350px;
        left:20px;
        top:20px;
        }
        
    </style>
    <script language="javascript" type="text/javascript">
        var masterTable;
        var bShowModal = true;
        function SaveFailed() {
            alert("Policy was not  downloaded");
            return false;
        }
        function modalPopupShowingHandler(sender, ev) {
            if (bShowModal == false) {
                ev.set_cancel(true);
            }
        }
        function pageLoad(sender, args) {


            var test = $find("InsuredData");
            if (test != null) {
                test.add_showing(modalPopupShowingHandler);
            }
        }
        function BackButtonClicked() {

            var claimid = document.getElementById("hdnClaimId");
            var claimtype = document.getElementById("hdnClaimType");
            var sPolicyLobCode = document.getElementById("hdnPolicyLobCode");
            var sDate = document.getElementById("hdnPolicyLossDate");
            var sPolicyLobId = document.getElementById("hdnPolicyLobId");
            var sSystemName = document.getElementById("hdnPolicySysName"); //sanoopsharma 
            //Ashish Ahuja: Claims Made Jira 1342 Start
            var sClaimReportedDate = document.getElementById("hdnClaimReportedDate");
            var sClaimDate = document.getElementById("hdnClaimDate");
            var sEventDate = document.getElementById("hdnEventDate");
            //Ashish Ahuja: Claims Made Jira 1342 End
            window.location.href = "/RiskmasterUI/UI/PolicyInterface/PolicySysDownload.aspx?ClaimType=" + claimtype.value + "&claimId=" + claimid.value + '&PolicyLobCode=' + sPolicyLobCode.value + '&PolicyDateCriteria=' + sDate.value + '&PolicyLobId=' + sPolicyLobId.value + '&SystemName=' + sSystemName.value + '&ClaimDateReported=' + sClaimReportedDate.value + '&ClaimDate=' + sClaimDate.value + '&EventDate=' + sEventDate.value;

        }
        function SaveSucceeded() {
            var objClaimWindow = null;

            //            if (document.getElementById('hdMDIClaimId') != null && document.getElementById('hdMDIClaimId').value != "" && document.getElementById('hdnMDIEventId') != null && document.getElementById('hdnMDIEventId').value != "") {
            //                objClaimWindow = parent.GetWindowObjectByMDIId(unescape(document.getElementById('hdMDIClaimId').value));
            //                objClaimWindow.OpenPolicyPages("", document.getElementById("hdAddedPolicyId").value, document.getElementById('hdMDIClaimId').value, document.getElementById('hdnMDIEventId').value);
            //            }
            //            else {
            
            //Ashish Ahuja: Claims Made Jira 1342 : Added new parameter i.e. hdnClaimReportedDate
            try {
                window.opener.OpenPolicyPages("", document.getElementById("hdAddedPolicyId").value, document.getElementById("hdnClaimReportedDate").value, document.getElementById("hdnPolicyLossDate").value);
            }
            catch (e) {
                OpenPolicyPages("", document.getElementById("hdAddedPolicyId").value, document.getElementById("hdnClaimReportedDate").value, document.getElementById("hdnPolicyLossDate").value);
            }
            

            window.close();
            //}
            return false;
        }
        function CheckDuplicate() {
            var sTmp;
            if (document.forms[0].dupeoverride != null) {
                sTmp = document.forms[0].dupeoverride.value;
            }
            var sClaimIds = sTmp.substring(0, sTmp.indexOf(';'));
            sTmp = sTmp.substring(sTmp.indexOf(';') + 1);
            var spolname = sTmp.substring(0, sTmp.indexOf(';'));
            sTmp = sTmp.substring(sTmp.indexOf(';') + 1);
            var lossdt = sTmp.substring(0, sTmp.indexOf(';'));
            sTmp = sTmp.substring(sTmp.indexOf(';') + 1);
            var polnum = sTmp.substring(0, sTmp.indexOf(';'));
            sTmp = sTmp.substring(sTmp.indexOf(';') + 1);
            var symbol = sTmp.substring(0, sTmp.indexOf(';'));
            sTmp = sTmp.substring(sTmp.indexOf(';') + 1);
            var mastercompany = sTmp.substring(0, sTmp.indexOf(';'));
            sTmp = sTmp.substring(sTmp.indexOf(';') + 1);
            var locationcmpny = sTmp.substring(0, sTmp.indexOf(';'));
            sTmp = sTmp.substring(sTmp.indexOf(';') + 1);
            var module = sTmp.substring(sTmp.indexOf(';') + 1);
            var wndurl = "/RiskmasterUI/UI/SupportScreens/DupClaim.aspx?claimids=" + sClaimIds + "&Type=PolicyDownload&policyname=" + spolname + "&lossdate=" + lossdt + "&polnum=" + polnum + "&symbol=" + symbol + "&mastercompany=" + mastercompany + "&location=" + locationcmpny + "&module=" + module;
            var popUpWin = window.open(wndurl, 'Duplicate Claims', 'width=800,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 800) / 2 + ',resizable=yes,scrollbars=yes');
            popUpWin.focus();
        }
        function OnEnquireButtonClicked() {
          
            var vLossDt;
            if (document.getElementById("hdnPolicyLossDate") != null) {
                vLossDt = document.getElementById("hdnPolicyLossDate").value;
            }
            //dbisht6 jira rma-369

            if (document.getElementById("txtLossDate") != null && document.getElementById("txtLossDate").value.trim().length != 0)
                vLossDt = document.getElementById("txtLossDate").value;
            // jira rma-369
            var selectedRow = GetSelectedRows();
            if (selectedRow != null && selectedRow != undefined) {
                var policyNumber = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyNumber");
                //   var policyName = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyName");
                //   var policyId = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyId");
                var policyEffDate = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyEffDate");
                var policyStatus = masterTable.getCellByColumnUniqueName(selectedRow, "Status");
                var module = masterTable.getCellByColumnUniqueName(selectedRow, "Module");
                var mastercompany = masterTable.getCellByColumnUniqueName(selectedRow, "MasterCompany");
                var lob = masterTable.getCellByColumnUniqueName(selectedRow, "LOB");
                var symbol = masterTable.getCellByColumnUniqueName(selectedRow, "policyname");
                var loctn = masterTable.getCellByColumnUniqueName(selectedRow, "location");
                var policySystemId = document.getElementById("hdnSystemName").value;
                document.getElementById("hdSelectedPolicyNumber").value = policyNumber.innerText;
                //   document.getElementById("hdSelectedPolicyName").value = policyName.innerText;
                //   document.getElementById("hdSelectedPolicyHolders").value = policyHolders.innerText;
                document.getElementById("hdSelectedEffDate").value = policyEffDate.innerText;
                //   document.getElementById("hdSelectedExtPolId").value = policyId.innerText;
                document.getElementById("hdSelectedMasterCompany").value = mastercompany.innerText;
                document.getElementById("hdSelectedLOB").value = lob.innerText;
                document.getElementById("hdSelectedModule").value = module.innerText;
                document.getElementById("hfInsurerNm").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerNm").innerText;
                document.getElementById("hdnTaxId").value = masterTable.getCellByColumnUniqueName(selectedRow, "TaxId").innerText;
                document.getElementById("hdnClientSeqNo").value = masterTable.getCellByColumnUniqueName(selectedRow, "ClientSeqNo").innerText;
                document.getElementById("hdnAddressSeqNo").value = masterTable.getCellByColumnUniqueName(selectedRow, "AddressSeqNo").innerText;
                document.getElementById("hdnBirthDt").value = masterTable.getCellByColumnUniqueName(selectedRow, "BirthDt").innerText;
                document.getElementById("hfInsurerAddr1").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerAddr1").innerText;
                document.getElementById("hfInsurerCity").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerCity").innerText;
                document.getElementById("hfInsurerPostalCd").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerPostalCode").innerText;
                document.getElementById("hdnNameType").value = masterTable.getCellByColumnUniqueName(selectedRow, "NameType").innerText;
                //JIRA 1342 ajohari2: Start
                document.getElementById("hdnMinimumRetroDate").value = masterTable.getCellByColumnUniqueName(selectedRow, "MinimumRetroDate").innerText;
                document.getElementById("hdnMaximumExtendDate").value = masterTable.getCellByColumnUniqueName(selectedRow, "MaximumExtendDate").innerText;
                document.getElementById("hdnIsClaimsMade").value = masterTable.getCellByColumnUniqueName(selectedRow, "IsClaimsMade").innerText;
                //JIRA 1342 ajohari2: End
                //var val = document.getElementById("hfInsurerNm").value + "|" + document.getElementById("hfInsurerAddr1").value + "|" + document.getElementById("hfInsurerCity").value + "|" + document.getElementById("hfInsurerPostalCd").value;
                //window.open("PS_Policy.aspx?policysystemid=" + policySystemId + "&policyNumber=" + policyNumber.innerText + "&mastercompany=" + mastercompany.innerText + "&symbol=" + symbol.innerText + "&module=" + module.innerText + "&location=" + loctn.innerText + "&lob=" + lob.innerText+"&Val="+val, null, 'toolbar=0,titlebar=0,width=770,height=515,resizable=yes,scrollbars=yes');
                
                //tanwar2 - WWIG - changes for reading data from staging tables - start
                //window.open("PS_Policy.aspx?policysystemid=" + policySystemId + "&policyNumber=" + policyNumber.innerText + "&mastercompany=" + mastercompany.innerText + "&symbol=" + symbol.innerText + "&module=" + module.innerText + "&location=" + loctn.innerText + "&lob=" + lob.innerText + "&losdt=" + vLossDt, null, 'toolbar=0,titlebar=0,width=770,height=545,resizable=yes,scrollbars=yes');
                var policyStagingId = 0;
                var policyStagingIdNode = masterTable.getCellByColumnUniqueName(selectedRow, "StagingPolicyID");
                if (policyStagingId != null) {
                    //TODO: change policy staging id to 0 or -1
                    policyStagingId = policyStagingIdNode.innerText;
                }
                var sClaimDateReported = null;
                if (document.getElementById("txtClaimDateReported").value.trim().length != 0)
                {
                    sClaimDateReported = document.getElementById("txtClaimDateReported").value;
                }
                else
                {
                    sClaimDateReported = document.getElementById("hdnClaimReportedDate").value;
                }

                window.open("PS_Policy.aspx?policysystemid=" + policySystemId + "&policyStagingId=" + policyStagingId + "&policyNumber=" + policyNumber.innerText + "&mastercompany=" + mastercompany.innerText + "&symbol=" + symbol.innerText + "&module=" + module.innerText + "&location=" + loctn.innerText + "&lob=" + lob.innerText + "&losdt=" + vLossDt + "&ClaimReportedDate=" + sClaimDateReported, null, 'toolbar=0,titlebar=0,width=770,height=545,resizable=yes,scrollbars=yes');//Ashish Ahuja 
                //tanwar2 - WWIG - staging - end
            }
            else {
                alert("Please select policy record");
            }
            return false;
        }

        function OnDownloadButtonClicked() {
            var bValidationFailed;

            bValidationFailed = false;
            var selectedRow = GetSelectedRows();
            if (selectedRow != null && selectedRow != undefined) {
                var policyNumber = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyNumber");
                //                var policyName = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyName");
                //                var policyId = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyId");
                // var policyEffDate = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyEffDate");
                var policyLocation = masterTable.getCellByColumnUniqueName(selectedRow, "Location");
                var module = masterTable.getCellByColumnUniqueName(selectedRow, "Module");
                var mastercompany = masterTable.getCellByColumnUniqueName(selectedRow, "MasterCompany");
                // var lob = masterTable.getCellByColumnUniqueName(selectedRow, "LOB");
                var policysymbol = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyName");
                var lob = masterTable.getCellByColumnUniqueName(selectedRow, "LOB");
                var state = masterTable.getCellByColumnUniqueName(selectedRow, "State");
                var inceptionDt = masterTable.getCellByColumnUniqueName(selectedRow, "InceptionDate");
                // Added by Pradyumna - WWIG GAP16 MITS 33414
                var stagingpolicyid = masterTable.getCellByColumnUniqueName(selectedRow, "StagingPolicyID");
                // Addition Ends -WWIG GAP16 
                //MITS:33574 START
                //if (document.getElementById("hdnClaimType").value == '') {//JIRA 1342 ajohari2
                var effectivedate = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyEffDate").textContent.trim();//innerText; RMA-15689 msampathkuma
                    effectivedate = new Date(effectivedate);
                    var expirationdate = masterTable.getCellByColumnUniqueName(selectedRow, "PolicyExpDate").textContent.trim();//innerText; RMA-15689 msampathkuma
                   expirationdate = new Date(expirationdate);
                

                    //}//JIRA 1342 ajohari2
                //MITS:33574 END
                //RMA-15689-start
                   document.getElementById("hdSelectedPolicyNumber").value = policyNumber.textContent.trim();//innerText;
                //   document.getElementById("hdSelectedPolicyName").value = policyName.innerText;
                //                document.getElementById("hdSelectedPolicyHolders").value = policyHolders.innerText;
                //    document.getElementById("hdSelectedEffDate").value = policyEffDate.innerText;
                //                document.getElementById("hdSelectedExtPolId").value = policyId.innerText;
                if (mastercompany != null) {
                    document.getElementById("hdSelectedMasterCompany").value = mastercompany.textContent.trim();//innerText;
                }
                document.getElementById("hdSelectedLOB").value = lob.textContent.trim();//.innerText;
                document.getElementById("hdSelectedModule").value = module.textContent.trim();//.innerText;
                document.getElementById("hdnLocation").value = policyLocation.textContent.trim();//.innerText;
                document.getElementById("hdnState").value = state.textContent.trim();//.innerText;
                document.getElementById("hdnPolicySymbol").value = policysymbol.textContent.trim();//.innerText;

                document.getElementById("hfInsurerNm").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerNm").textContent.trim();//.innerText;
                document.getElementById("hdnTaxId").value = masterTable.getCellByColumnUniqueName(selectedRow, "TaxId").textContent.trim();//.innerText;
                document.getElementById("hdnClientSeqNo").value = masterTable.getCellByColumnUniqueName(selectedRow, "ClientSeqNo").textContent.trim();//.innerText;
                document.getElementById("hdnAddressSeqNo").value = masterTable.getCellByColumnUniqueName(selectedRow, "AddressSeqNo").textContent.trim();//.innerText;
                document.getElementById("hdnBirthDt").value = masterTable.getCellByColumnUniqueName(selectedRow, "BirthDt").textContent.trim();//.innerText;
                document.getElementById("hfInsurerAddr1").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerAddr1").textContent.trim();//.innerText;
                document.getElementById("hfInsurerCity").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerCity").textContent.trim();//.innerText;
                document.getElementById("hfInsurerPostalCd").value = masterTable.getCellByColumnUniqueName(selectedRow, "InsurerPostalCode").textContent.trim();//.innerText;
                document.getElementById("hdnNameType").value = masterTable.getCellByColumnUniqueName(selectedRow, "NameType").textContent.trim();//.innerText;
                
                //JIRA 1342 ajohari2: Start
                document.getElementById("hdnMinimumRetroDate").value = masterTable.getCellByColumnUniqueName(selectedRow, "MinimumRetroDate").textContent.trim();//.innerText;
                document.getElementById("hdnMaximumExtendDate").value = masterTable.getCellByColumnUniqueName(selectedRow, "MaximumExtendDate").textContent.trim();//.innerText;
                document.getElementById("hdnIsClaimsMade").value = masterTable.getCellByColumnUniqueName(selectedRow, "IsClaimsMade").textContent.trim();//.innerText;
                //JIRA 1342 ajohari2: End
                document.getElementById("hdnInceptionDt").value = inceptionDt.textContent.trim();//.innerText;
                // Added by Pradyumna - WWIG GAP16 MITS 33414
                document.getElementById("hdnStagingPolicyId").value = stagingpolicyid.textContent.trim();//.innerText;
                // Addition Ends - WWIG GAP16 
                //RMA-15689-end
                //MITS:33574 START
                if (document.getElementById("hdnClaimType").value == '') {
                    document.getElementById("PointClaimEventSetting").value = masterTable.getCellByColumnUniqueName(selectedRow, "PointClaimEventSetting").textContent.trim();//.innerText;
                    var LossDate = document.getElementById('txtLossDate').value;
                    if (LossDate != '') {
                        LossDate = new Date(LossDate);
                    }
                    if (LossDate == '') {
                        alert("Date Of Loss is required for download the policy");
                        bValidationFailed = true;
                        return false;
                    }
                    //JIRA 1342 ajohari2: Start
                    var IsClaimsMade = document.getElementById("hdnIsClaimsMade").value;

                    var MinimumRetroDate = document.getElementById("hdnMinimumRetroDate").value;
                    if (MinimumRetroDate == '' || MinimumRetroDate.trim() == '0')
                    {
                        MinimumRetroDate = effectivedate;
                    }
                    MinimumRetroDate = new Date(MinimumRetroDate);
                    var MaximumExtendDate = document.getElementById("hdnMaximumExtendDate").value;
                    if (MaximumExtendDate == '' || MaximumExtendDate.trim() == '0') {
                        MaximumExtendDate = expirationdate;
                    }
                    MaximumExtendDate = new Date(MaximumExtendDate);
                    if (IsClaimsMade != '' && IsClaimsMade.trim() == 'Y')
                    {
                        if ((LossDate >= MinimumRetroDate) && (LossDate <= MaximumExtendDate))
                        {
                        }
                        else
                        {
                            alert("Policy selection for entered loss date is invalid");
                            bValidationFailed = true;
                            return false;
                        }

                    }
                    else {

                        if ((LossDate >= effectivedate) && (LossDate <= expirationdate))
                        {
                        }
                        else
                        {
                            alert("Policy selection for entered loss date is invalid");
                            bValidationFailed = true;
                            return false;
                        }
                    }

                    //JIRA 1342 ajohari2: End
                }

            }
            else {
                alert("Please select policy record");
                bValidationFailed = true;
                return false;
            }

            var CFValue, objControl, pitype;
            objControl = document.getElementById("hdnClientFile");
            if (objControl != null) {
                CFValue = objControl.value;
            }
            objControl = document.getElementById("hdnPolSysType1");
            if (objControl != null) {
                pitype = objControl.value;
            }



            if (CFValue == 0 && pitype.toUpperCase() == "POINT" && bValidationFailed == false) {
                var testctrl = $find("InsuredData");
                if (testctrl != null)
                    testctrl.show();
                // return false;
            }
            else {
                pleaseWait.Show();
                return true;
            }


            return false;
        }

        //        function PolicyDownloaded() {
        //            
        //            window.opener.ValidatePolicy(document.getElementById("hdAddedPolicyId").value, "","");
        //        }
        function SetData() {

            var CFValue, objControl;

            var ctrlrbInd = document.getElementById("rbInd");
            var ctrlrbBus = document.getElementById("rbBus");
            if (ctrlrbBus != null && ctrlrbBus.checked == true) {
                document.getElementById("hdnNameType").value = "B";
            }
            if (ctrlrbInd != null && ctrlrbInd.checked == true) {
                document.getElementById("hdnNameType").value = "I";
            }


            pleaseWait.Show();
        }

    </script>
    <title>Search</title>
</head>
<body onload="javascript:if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div id="dvResults" runat="server">
        <table border="0" cellspacing="0" cellpadding="2" width="99.5%">
            <tr>
                <td class="ctrlgroup">
                    Search Results
                </td>
            </tr>
            <tr>
                <td align="center" width="100%">
                    <asp:Literal ID="ltNoResult" runat="server" Visible="false" />
                </td>
            </tr>
        </table>
        <table id="tblToolbar" border="0" cellspacing="0" cellpadding="0" align="center"
            runat="server" width="100%">
            <tr>
                <td>
                    <asp:ImageButton runat="server" OnClientClick="return OnDownloadButtonClicked();"
                        src="../../Images/tb_save_active.png" Width="28" Height="28" OnClick="SaveClicked"
                        border="0" ID="imgSave" AlternateText="Save Policy" onMouseOver="this.src='../../Images/tb_save_mo.png';"
                        onMouseOut="this.src='../../Images/tb_save_active.png';" />
                    <asp:ImageButton runat="server" OnClientClick="return OnEnquireButtonClicked();"
                        src="../../Images/tb_search_active.png" Width="28" Height="28" border="0" ID="imgEnquire"
                        AlternateText="Inquire Policy" onMouseOver="this.src='../../Images/tb_search_mo.png';"
                        onMouseOut="this.src='../../Images/tb_search_active.png';" />
                      <%--MITS:33574 START--%>
                      <asp:Label runat ="server"  Font-Bold="true"  ID = "lblDateOfLoss" Text ="Date of Loss:"></asp:Label>

                        <select name="dlLossDate" id="dlLossDate" runat ="server" tabindex="500" onchange="return BetweenOption('date','dlLossDate');"
                            style='display: none;'>
                            <option value="=">=</option>
                            <option value="&lt;>">&lt;&gt;</option>
                            <option value=">">&gt;</option>
                            <option value="">=">&gt;=</option>
                            <option value="&lt;">&lt;</option>
                            <option value="&lt;=">&lt;=</option>
                        </select>
                    <!--//Ashish Ahuja: Claims Made Jira 1342 added datelosscopy function-->
                        <input name="txtLossDate" type="text" id="txtLossDate" tabindex="14" rmxref="" rmxtype="date" size="30"
                            onchange="datachanged('true');dateLossCopy();" onblur="dateLostFocusOld(this.id);dateLossCopy();" runat="server" /><%--//JIRA 1342 ajohari2--%>
                        <input type="button" name="btnIssueDateStart" value="" id="btnLossDate" tabindex="15"
                            class="DateLookupControl" runat="server" />
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                            {
                                inputField: "txtLossDate",
                                ifFormat: "%m/%d/%Y",
                                button: "btnLossDate"
                            }
                            );
                        </script>
                        <%--MITS:33574 END--%>
                    <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                    <asp:Label runat ="server"  Font-Bold="False"  ID = "lblClaimDateReported" Text ="Claim Date Reported:"></asp:Label>

                        <select name="dlClaimDateReported" id="dlClaimDateReported" runat ="server" tabindex="500" onchange="return BetweenOption('date','dlClaimDateReported');"
                            style='display: none;'>
                            <option value="=">=</option>
                            <option value="&lt;>">&lt;&gt;</option>
                            <option value=">">&gt;</option>
                            <option value="">=">&gt;=</option>
                            <option value="&lt;">&lt;</option>
                            <option value="&lt;=">&lt;=</option>
                        </select>

                        <input name="txtClaimDateReported" type="text" id="txtClaimDateReported" tabindex="14" rmxref="" rmxtype="date" size="30"
                            onchange="datachanged('true')" onblur="dateLostFocusOld(this.id);" runat="server" /><%--//JIRA 1342 ajohari2--%>
                        <input type="button" name="btnIssueDateStart" value="" id="btnClaimDateReported" tabindex="15"
                            class="DateLookupControl" runat="server" />
                        <script type="text/javascript">
                            Zapatec.Calendar.setup(
                            {
                                inputField: "txtClaimDateReported",
                                ifFormat: "%m/%d/%Y",
                                button: "btnClaimDateReported"
                            }
                            );
                        </script>
                    <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
                    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
                    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
                        <script type="text/javascript">
                            function GetSelectedRows() {
                                masterTable = $find("<%=gvPolicyList.ClientID %>").get_masterTableView();
                                var selectedRows = masterTable.get_selectedItems();
                                if (selectedRows != null && selectedRows != undefined) {
                                    return selectedRows[0];
                                }
                            }
                        </script>
                    </telerik:RadCodeBlock>
                    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="gvPolicyList">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="gvPolicyList" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                        <telerik:RadGrid runat="server" ID="gvPolicyList"  AutoGenerateColumns="false"
                            OnNeedDataSource="gvPolicyList_NeedDataSource" AllowSorting="True" AllowFilteringByColumn="True"
                            GridLines="None" OnItemDataBound="gvPolicyList_ItemDataBound" Skin="Office2007"
                            OnPreRender="gvPolicyList_PreRender" Height="100%" Width="100%" HeaderStyle-CssClass="msgheader"
                            AllowPaging="true" PageSize="10">
                            <SelectedItemStyle CssClass="SelectedItem" />
                            <ClientSettings AllowKeyboardNavigation="true">
                                <ClientEvents OnRowSelected="" />
                                <Scrolling SaveScrollPosition="true" AllowScroll="false" />                           
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <MasterTableView Width="100%">
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridClientSelectColumn ItemStyle-Width="5%" UniqueName="GridClientSelectCheckbox"
                                        Visible="true" HeaderText="" />
                                    <telerik:GridTemplateColumn HeaderText="Status" FilterControlWidth="80%" AllowFiltering="true"
                                        ItemStyle-Wrap="true" UniqueName="Status" DataField="status" SortExpression="status"
                                        ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Symbol" FilterControlWidth="80%" AllowFiltering="true"
                                        ItemStyle-Wrap="false" DataField="policyname" SortExpression="policyname" ItemStyle-Width="10%"
                                        UniqueName="PolicyName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPolicyName" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn ItemStyle-CssClass="data" ItemStyle-Width="10%" FilterControlWidth="80%"
                                        ItemStyle-Wrap="false" DataField="policynumber" AllowFiltering="true" HeaderText="Policy Number"
                                        SortExpression="policynumber" UniqueName="PolicyNumber">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPolicyNumber" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn ItemStyle-CssClass="data" ItemStyle-Width="10%" FilterControlWidth="80%"
                                        ItemStyle-Wrap="false" DataField="module" AllowFiltering="true" HeaderText="Module"
                                        SortExpression="module" UniqueName="Module">
                                        <ItemTemplate>
                                            <asp:Label ID="lblModule" CssClass="data" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Insured" ItemStyle-Width="15%" FilterControlWidth="80%"
                                        AllowFiltering="true" DataField="commercialname" SortExpression="commercialname"
                                        ItemStyle-Wrap="true" UniqueName="CommercialName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCommercialName" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Last Name" ItemStyle-Width="15%" FilterControlWidth="80%"
                                        AllowFiltering="true" DataField="lastname" SortExpression="lastname" ItemStyle-Wrap="true"
                                        UniqueName="LastName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastName" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="First Name" ItemStyle-Width="15%" FilterControlWidth="80%"
                                        AllowFiltering="true" DataField="firstname" SortExpression="firstname" ItemStyle-Wrap="true"
                                        UniqueName="FirstName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFirstName" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Middle Name" ItemStyle-Width="15%" FilterControlWidth="80%"
                                        AllowFiltering="true" DataField="middlename" SortExpression="middlename" ItemStyle-Wrap="true"
                                        UniqueName="MiddleName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMiddleName" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Short Name" ItemStyle-Width="15%" FilterControlWidth="80%"
                                        AllowFiltering="true" DataField="sortname" SortExpression="sortname" ItemStyle-Wrap="true"
                                        UniqueName="SortName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSortName" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="City" FilterControlWidth="80%" AllowFiltering="true"
                                        ItemStyle-Wrap="false" UniqueName="City" DataField="city" SortExpression="city"
                                        ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="State" FilterControlWidth="80%" UniqueName="State"
                                        AllowFiltering="true" DataField="state" SortExpression="state" ItemStyle-Width="10%"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblState" runat="server" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Effective Date" ItemStyle-Width="10%" FilterControlWidth="80%"
                                        UniqueName="PolicyEffDate" AllowFiltering="true" DataField="effectivedate" SortExpression="effectivedate"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEffDate" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Expiration Date" ItemStyle-Width="10%" FilterControlWidth="80%"
                                        UniqueName="PolicyExpDate" AllowFiltering="true" DataField="expirationdate" SortExpression="expirationdate"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblExpDate" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Location" ItemStyle-Width="10%" FilterControlWidth="50%"
                                        UniqueName="Location" AllowFiltering="true" DataField="location" SortExpression="location"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLocation" runat="server" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Master Company" ItemStyle-Width="10%" FilterControlWidth="50%"
                                        UniqueName="MasterCompany" AllowFiltering="true" DataField="mastercompany" SortExpression="mastercompany"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMaster" runat="server" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Agency" ItemStyle-Width="10%" FilterControlWidth="50%"
                                        UniqueName="Agency" AllowFiltering="true" DataField="agency" SortExpression="agency"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAgency" runat="server" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Line of Business" ItemStyle-Width="10%" FilterControlWidth="50%"
                                        UniqueName="LOB" AllowFiltering="true" DataField="lob" SortExpression="lob" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLOB" runat="server" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="InsurerNm" DataField="InsurerName" Display="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInsurerNm" runat="server" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="InsurerAddr1" DataField="InsurerAddr1" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblInsurerAddr1" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="InsurerCity" DataField="InsurerCity" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblInsurerCity" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="InsurerPostalCode" UniqueName="InsurerPostalCode"
                                        Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblInsurerPostalCd" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="TaxId" UniqueName="TaxId" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTaxId" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="ClientSeqNo" UniqueName="ClientSeqNo" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientSeqNo" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="AddressSeqNo" UniqueName="AddressSeqNo" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAddressSeqNo" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="BirthDt" UniqueName="BirthDt" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblBirthDt" Text="" Width="5"></asp:Label>
                                        </ItemTemplate>
                                               </telerik:GridTemplateColumn>
                                        
                                    <telerik:GridTemplateColumn HeaderText="Inception Date" UniqueName="InceptionDate"
                                        AllowFiltering="false" Display="false" DataField="inceptiondate" SortExpression="inceptiondate"
                                        ItemStyle-Wrap="false" ItemStyle-CssClass="hiderowcol" HeaderStyle-CssClass="hiderowcol">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInceptionDate" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                      <telerik:GridTemplateColumn DataField="NameType" UniqueName="NameType" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblNameType" Text="" ></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="StagingPolicyID" UniqueName="StagingPolicyID" Display="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStagingPolicyID" Text="" ></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                     <%--MITS:33574 START--%>
                                        <telerik:GridTemplateColumn DataField="PointClaimEventSetting" UniqueName="PointClaimEventSetting" Display="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblPointClaimEventSetting" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>   
                                   <%--MITS:33574 END--%>
                                   <%--JIRA 1342 ajohari2: Start--%>
                                        <telerik:GridTemplateColumn DataField="MinimumRetroDate" UniqueName="MinimumRetroDate" Display="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblMinimumRetroDate" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>   
                                        <telerik:GridTemplateColumn DataField="MaximumExtendDate" UniqueName="MaximumExtendDate" Display="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblMaximumExtendDate" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>   
                                        <telerik:GridTemplateColumn DataField="IsClaimsMade" UniqueName="IsClaimsMade" Display="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblIsClaimsMade" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>   
                                   <%--JIRA 1342 ajohari2: End--%>
                                </Columns>
                                <ItemStyle CssClass="datatd1" />
                                <AlternatingItemStyle CssClass="datatd" />
                                <HeaderStyle CssClass="msgheader" />
                            </MasterTableView>
                            <%--  <HeaderStyle CssClass="msgheader" />--%>
                            <PagerStyle Mode="NextPrevAndNumeric" Position="Top" AlwaysVisible="true" />
                            <FilterMenu EnableTheming="True">
                                <CollapseAnimation Duration="200" Type="OutQuint" />
                            </FilterMenu>
                        </telerik:RadGrid>
                    </telerik:RadAjaxPanel>
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="2" width="99.5%">
            <tr>
                <td>
                    <asp:HiddenField ID="hdSelectedPolicyNumber" Value="" runat="server" />
                    <asp:HiddenField ID="hdSelectedPolicyName" Value="" runat="server" />
                    <asp:HiddenField ID="hdSelectedEffDate" Value="" runat="server" />
                    <asp:HiddenField ID="hdSelectedExpDate" Value="" runat="server" />
                    <asp:HiddenField ID="hdAddedPolicyId" Value="" runat="server" />
                    <asp:HiddenField ID="hdnClaimType" Value="" runat="server" />
                    <asp:HiddenField ID="hdSelectedMasterCompany" Value="" runat="server" />
                    <asp:HiddenField ID="hdSelectedLOB" Value="" runat="server" />
                    <asp:HiddenField ID="hdSelectedModule" Value="" runat="server" />
                    <asp:HiddenField ID="hdnSystemName" Value="" runat="server" />
                    <asp:HiddenField ID="hdnClaimId" Value="" runat="server" />
                    <asp:HiddenField ID="hdnPolicySymbol" Value="" runat="server" />
                    <asp:HiddenField ID="hdnLocation" Value="" runat="server" />
                    <asp:HiddenField ID="hdnPolicyLobCode" Value="" runat="server" />
                    <asp:HiddenField ID="hdnPolicyLobId" Value="" runat="server" />
                    <asp:HiddenField ID="hdnPolicyLossDate" Value="" runat="server" />
                    <asp:HiddenField ID="hdnClaimReportedDate" Value="" runat="server" /><!--//Ashish Ahuja: Claims Made Jira 1342-->
                    <asp:HiddenField ID="hdnClaimDate" Value="" runat="server" /><!--//Ashish Ahuja: Claims Made Jira 1342-->
                    <asp:HiddenField ID="hdnEventDate" Value="" runat="server" /><!--//Ashish Ahuja: Claims Made Jira 1342-->
                    <asp:HiddenField ID="hdnClaimBasedFlag" Value="" runat="server" /><!--//Ashish Ahuja: Claims Made Jira 1342-->
                    <asp:HiddenField ID="hdnPolicySearchByDate" Value="" runat="server" /><!--////JIRA 1342 ajohari2-->
                    <asp:HiddenField ID="hdnState" Value="" runat="server" />
                    <asp:HiddenField ID="hdnInceptionDt" Value="" runat="server" />
                    <asp:HiddenField ID="hfInsurerNm" runat="server" Value="" />
                    <asp:HiddenField ID="hdnTaxId" runat="server" Value="" />
                    <asp:HiddenField ID="hfInsurerAddr1" runat="server" Value="" />
                    <asp:HiddenField ID="hfInsurerCity" runat="server" Value="" />
                    <asp:HiddenField ID="hfInsurerPostalCd" runat="server" Value="" />
                    <asp:HiddenField ID="hdnClientSeqNo" runat="server" Value="" />
                    <asp:HiddenField ID="hdnAddressSeqNo" runat="server" Value="" />
                    <asp:HiddenField ID="hdnBirthDt" runat="server" Value="" />
                     <asp:HiddenField ID="hdnNameType" runat="server" Value="" />
                     <asp:HiddenField ID="PointClaimEventSetting" runat="server" Value="" /><%--MITS:33574--%>
                     <!-- Pradyumna: WWIG GAP16 MITS 33414 12/17/2013 - Start -->
                     <asp:HiddenField ID="hdnPolicySrchSys" runat="server" Value="" />
                     <asp:HiddenField ID="hdnStagingPolicyId" runat="server" Value="" />
                     <!-- Pradyumna: WWIG GAP16 MITS 33414 12/17/2013 - End -->
                    <asp:HiddenField ID ="hdnPolicySysName" runat ="server" Value ="" />
                    <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                    <asp:HiddenField ID ="hdnClaimRptDateType" runat ="server" Value ="" />
                    <input type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="PolicySysDownload" />
                    <%--//Ashish Ahuja: Claims Made Jira 1342--%>
                    <asp:HiddenField ID ="hdnClientFile" runat="server" Value="" />
                    <asp:HiddenField ID="hdnModalPopup" runat="server" />
                    <input type="hidden" runat="server" id="hdnPolSysType1" />
                    <%--JIRA 1342 ajohari2: Start--%>
                    <asp:HiddenField ID ="hdnMinimumRetroDate" runat ="server" Value ="" />
                    <asp:HiddenField ID ="hdnMaximumExtendDate" runat ="server" Value ="" />
                    <asp:HiddenField ID ="hdnIsClaimsMade" runat ="server" Value ="" />
                    <%--JIRA 1342 ajohari2: End--%>
                    <asp:HiddenField ID ="dupeoverride" runat ="server" Value ="" />
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" name="btnBack" value="Back" onclick="BackButtonClicked()" id="btnBack"
                        tabindex="500" class="button" />
                </td>
            </tr>
        </table>
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </div>
        <asp:Panel ID="pnl1" runat="server"  Style="display: none" CssClass="modalPopup" BackColor="White">
            <asp:Panel ID="Panel3" runat="server" Style="cursor: move;" >
                <div style="text-align:center" class="colheader3">
                    <p ><b>
                        <asp:Label ID="NewLabel" runat="server" Text="Insured Data Selection"></asp:Label></b></p>
                </div>
            </asp:Panel>
                <table>
                    <tr id="row1" runat="server" >
                        <td>
                            <asp:Label ID="Label1" Text ="Download Insured as " runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id ="row2" runat="server" >
                        <td>
                        <asp:RadioButton ID ="rbInd" value="I" GroupName="InsDownload" runat="server" Text="Individual" Checked="true" />
                        <asp:RadioButton ID ="rbBus" value="B" GroupName="InsDownload" runat="server"  Text="Business"/>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnOK" Text="OK" OnClientClick="SetData();" OnClick="SaveClicked" />
                            <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
                        </td>
                    </tr>
                </table>
        </asp:Panel>
        <asp:ModalPopupExtender ID="InsuredData" 
            runat="server"
            PopupControlID="pnl1"
            BackgroundCssClass="modalBackground"
            DropShadow="true" 
            PopupDragHandleControlID="Panel3" 
            TargetControlID="hdnModalPopup"
            CancelControlID ="btnCancel"
            OnCancelScript ="return false;" />
    </form>
</body>
</html>
