﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.PolicyInterface
{
    public partial class AddPolicyConfig : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("AddPolicyConfig.aspx"), "AddPolicyConfigValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "AddPolicyConfigValidations", sValidationResources, true);
        }
    }
}