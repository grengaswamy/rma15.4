﻿using System;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.RMXResourceManager;
//jira# RMA-6135.Multicurrency
using System.Threading;
using System.Globalization;
//jira# RMA-6135.Multicurrency
namespace Riskmaster.UI.UI.CoverageGroupMaintenance
{
    public partial class CoverageGroupMaintenance : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = string.Empty;
        string m_sCovGroupId = string.Empty;
        string m_sMode = string.Empty;
        string sSelected = string.Empty;
        XmlDocument xmlServiceDoc = null;
        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Start - Yukti-ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }

                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CoverageGroupMaintenance.aspx"), "CoverageGroupMaintenanceValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CoverageGroupMaintenanceValidations", sValidationResources, true);
                m_sCovGroupId = AppHelper.GetQueryStringValue("selectedid");
                m_sMode = AppHelper.GetQueryStringValue("mode");
                //jira# RMA-6135.Multicurrency
                if ((BaseCurrency != null) && (string.IsNullOrEmpty(BaseCurrency.Text)))
                {
                    CallCWS("RMUtilitiesAdaptor.GetBaseCurrency", GetMessageCurrTemplate(), out sCWSresponse, false, false);
                    if ((BaseCurrency != null) && (!string.IsNullOrEmpty(BaseCurrency.Text)))
                    {
                        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(BaseCurrency.Text);
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo(BaseCurrency.Text);
                    }
                }
                //jira# RMA-6135.Multicurrency
                if (!IsPostBack)
                {
                    //Changed by Nikhil on 09/22/14.Code Review changes
                   // if (m_sMode.ToLower() == "edit" || m_sMode.ToLower() == "new")


                  
                    if (string.Compare(m_sMode,"edit",true) == 0 || string.Compare(m_sMode,"new",true) == 0 )
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("CoverageGroupMaintenanceAdaptor.GetSelectedCoverageGroupInfo", XmlTemplate, out sCWSresponse, true, true);
                        /*Start: Neha Suresh Jain, 08/22/2013: To get the combo trigger_date selected value*/
                        xmlServiceDoc = new XmlDocument();
                        if (!string.IsNullOrEmpty(sCWSresponse))
                        {
                            xmlServiceDoc.LoadXml(sCWSresponse);
                            XmlNode xXMLNode = xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='TriggerEffDate']");
                            if (xXMLNode != null)
                            {
                                if (!string.IsNullOrEmpty(((XmlElement)xXMLNode).GetAttribute("value")))
                                {
                                    sSelected = ((XmlElement)xXMLNode).GetAttribute("value");
                                }

                                FillTriggerDateCombo(xmlServiceDoc.SelectSingleNode("ResultMessage/Document/form/group/displaycolumn/control[@name ='triggerDate']"));
                            }

                        }
                        //End: To get the combo trigger_date selected value
                    }
                    //To Set default value of Deductible Per Event flag to checked.
                     //Changed by Nikhil on 09/22/14.Code Review changes
               //    if (m_sMode.ToLower() == "new")
                   if (string.Compare(m_sMode.ToString(),"new",true) == 0)
                   {
                       chkUseSharedDeductible.Checked = true;
                     
                   }
                   //if (chkUseSharedDeductible.Checked)
                   //{
                   //    txtDedPerEvent.ReadOnly = false;
                   //}
                   //else
                   //{
                   //    txtDedPerEvent.ReadOnly = true;
                   //}
                   this.Page.ClientScript.RegisterStartupScript(typeof(string), "CheckDeductibleOption", "CheckDeductibleOption()", true);
                }

     
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// btnOk_Click .to save coverage maintainance changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("CoverageGroupMaintenanceAdaptor.Save", XmlTemplate, out sCWSresponse, true, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                //Changed by Nikhil on 09/22/14.Code Review changes
              //  if (sMsgStatus == "Success")
                if (string.Compare(sMsgStatus,"Success",true) == 0)

                {
                  //  ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.frmData.submit();window.close();</script>");
                    Server.Transfer("CoverageGroupsMaintenance.aspx", false);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// GetMessageTemplate
        /// </summary>
        
        private XElement GetMessageTemplate()
        {
            string sRowId = string.Empty;
            //Changed by Nikhil on 09/22/14.Code Review changes
            //if (m_sMode.ToLower() == "edit")
            if (string.Compare(m_sMode, "edit",true) == 0)
            {
                sRowId = AppHelper.GetQueryStringValue("selectedid");
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>"); 
            sXml = sXml.Append("<form name='CoverageGroupSetup' title='CoverageGroup'>");
            sXml = sXml.Append("<group name='CoverageGroupSetup' title='CoverageGroup'>");
            sXml = sXml.Append("<displaycolumn>");
            sXml = sXml.Append("<control name='CovGroupId' type='id'>");
            sXml = sXml.Append(sRowId);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='triggerDate' value='' type='combobox'>");
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='triggerDateSelected'>");
            sXml = sXml.Append(TriggerEffDate.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</displaycolumn>");
            sXml = sXml.Append("</group></form>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// btnCancel_Click .to redirect back to coverage maintainance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("CoverageGroupsMaintenance.aspx", false);
        }

        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date:08/22/2013
        /// Function to Fill trigger date combo
        /// </summary>
        /// <param name="xmlNode"></param>
        private void FillTriggerDateCombo(XmlNode xmlNode)
        {
            ListItem objListItem = null;
            TriggerEffDate.Items.Clear();
            if (xmlNode != null)
            {
                //Changed by Nikhil on 09/22/14.Code Review changes
              //  if (xmlNode.InnerXml != "0")
                if (string.Compare(xmlNode.InnerXml,"0") != 0 )
                {
                    foreach (XmlElement objElement in xmlNode)
                    {
                        objListItem = new ListItem(Server.HtmlDecode(objElement.InnerText), objElement.GetAttribute("value"));
                        TriggerEffDate.Items.Add(objListItem);
                        if (string.Compare(objListItem.Value, sSelected, true) == 0)
                        {
                            TriggerEffDate.SelectedValue = sSelected;
                        }
                    }
                }
            }
        }
        private XElement GetMessageCurrTemplate()
        {
            string sMesage = @"<Message><Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization><Call><Function /></Call><Document><BaseCurrencyType></BaseCurrencyType></Document></Message>";
            return XElement.Parse(sMesage);
        }
    }
}