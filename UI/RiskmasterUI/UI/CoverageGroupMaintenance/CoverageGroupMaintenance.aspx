<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CoverageGroupMaintenance.aspx.cs" Inherits="Riskmaster.UI.UI.CoverageGroupMaintenance.CoverageGroupMaintenance" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<script type = "text/javascript">
    function CheckDeductibleOption() {

        var chkDedShared = document.getElementById('chkUseSharedDeductible');
        if (chkDedShared.checked) {
            document.getElementById('txtDedPerEvent').disabled = false;
        }
        else {

            document.getElementById('txtDedPerEvent').disabled = true;
        }
      

    }

    function CovGroup_onSave()
    {
        var covGroup = document.getElementById('CovGroupCode').value.trim();
        var covGroupDesc = document.getElementById('CovGroupDesc').value.trim();
        if (covGroup == "" || covGroupDesc == "")
        {
            alert(CoverageGroupMaintenanceValidations.ValidRequiredFields);
            return false;
        }

        var objStartDate = document.getElementById("EffStartDate");
        var objEndDate = document.getElementById("EffEndDate");
        
        var dStartDate = new Date(objStartDate.value);
        var dEndDate = new Date(objEndDate.value);

        
        if (dStartDate > dEndDate)   
        {
            alert(CoverageGroupMaintenanceValidations.EffEndDateGrtThenStartDate);
            return false;
        }

        return true;



    }


</script>
    <title>Coverage Group Details</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    
    <script type="text/javascript" language="javascript" ></script>
    <%--<script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>


    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
    <form id="frmData" runat="server">
     <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td colspan="7">
                    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
                  <td colspan="7">
                <asp:TextBox Style="display: none" runat="server" ID="CovGroupId" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='CovGroupId']" rmxignoreset='true'/>
                            </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_save_active.png" 
                        class="bold" ToolTip="<%$ Resources:btnSaveTip %>"
                        runat="server" OnClick="btnOk_Click" OnClientClick="return CovGroup_onSave();" />
                    <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_reject_active.png" class="bold" ToolTip="<%$ Resources:btnCancelTip %>"
                        runat="server" OnClick="btnCancel_Click" />
                </td>
            </tr>
            <tr>
                <td><input type="text" name="" value="deductibleDetails" id="SysFormName"  style="display:none" /></td>
                <td><input type="text" name="" value="" id="SysPageDataChanged"  style="display:none" /></td>
            </tr>
            <tr>
                <td colspan="7" class="ctrlgroup">
                    <asp:Label ID="lblCoverageGroupHeader" runat="server" Text="<%$ Resources:lblCoverageGroupHeader %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            
            <tr>
                <td align="left" class="required">
                    <asp:Label ID="lblCovGroupCode" runat="server" Text="<%$ Resources:lblCovGroupCode %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="CovGroupCode" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name = 'CovGroupCode']" ></asp:TextBox>
                </td>
                </tr>

                 <tr>
                <td align="left" class="required">
                    <asp:Label ID="lblCovGroupDesc" runat="server" Text="<%$ Resources:lblCovGroupDesc %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="CovGroupDesc" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name = 'CovGroupDesc']" ></asp:TextBox>
                </td>
                </tr>
                  <tr>
                <td align="left" class="">
                    <asp:Label ID="lblTriggerEffDate" runat="server" Text="<%$ Resources:lblTriggerEffDate %>"></asp:Label>
                </td>
                <td>
                     <asp:DropDownList OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="TriggerEffDate" type="combobox" runat="server" 
                        rmxref="/Instance/Document/form/group/displaycolumn/control[@name='TriggerEffDate']/@value" 
                        ItemSetRef="/Instance/Document/form/group/displaycolumn/control[@name='TriggerEffDate']" />
                </td>
                </tr>
                  <tr>
                <td align="left" class="">
                    <asp:Label ID="lblEffStartDate" runat="server" Text="<%$ Resources:lblEffStartDate %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EffStartDate']" id="EffStartDate" runat="server" size="30"  ></asp:TextBox>
                   <%-- <input type="button"  class="DateLookupControl" value="..." name="datebtn" id ="EffStartDatebtn"/>
                     <script type="text/javascript">
                         Zapatec.Calendar.setup(
                    {
                        inputField: "EffStartDate",
                        ifFormat: "%m/%d/%Y",
                        button: "EffStartDatebtn"
                    }
                );
            </script>--%>

                     <script type="text/javascript">
                         $(function () {
                             $("#EffStartDate").datepicker({
                                 showOn: "button",
                                 buttonImage: "../../Images/calendar.gif",
                                 //buttonImageOnly: true,
                                 showOtherMonths: true,
                                 selectOtherMonths: true,
                                 changeYear: true
                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                         });
            </script>
                </td>
                </tr>
                     <tr>
                <td align="left" class="">
                    <asp:Label ID="lblEffEndDate" runat="server" Text="<%$ Resources:lblEffEndDate %>"></asp:Label>
                </td>
                <td>
                   <asp:TextBox OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='EffEndDate']" id="EffEndDate" runat="server" size="30"  ></asp:TextBox>
                    <%--<input type="button"  class="DateLookupControl" value="..." name="datebtn" id ="EffEndDatebtn"/>
                     <script type="text/javascript">
                         Zapatec.Calendar.setup(
                    {
                        inputField: "EffEndDate",
                        ifFormat: "%m/%d/%Y",
                        button: "EffEndDatebtn"
                    }
                );
            </script>--%>

                     <script type="text/javascript">
                         $(function () {
                             $("#EffEndDate").datepicker({
                                 showOn: "button",
                                 buttonImage: "../../Images/calendar.gif",
                                 //buttonImageOnly: true,
                                 showOtherMonths: true,
                                 selectOtherMonths: true,
                                 changeYear: true
                             }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                         });
            </script>
                </td>
                </tr>
                
                <tr>
                    <td align="left">
                        <asp:Label ID="lblUseSharedDedcutible" runat="server" Text="<%$ Resources:lblUseSharedDedcutible %>" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkUseSharedDeductible" onclick = "CheckDeductibleOption();" runat="server" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='UseSharedDeductible']" />
                    </td>
                </tr>
                <!-- Start -  Added by Nikhil on 10/13/14. -->
                <tr>
                     <td align="left" >
                    <asp:Label runat="server" ID="lblDedPerEvent" Text="<%$ Resources:lblDedPerEvent %>" />
                    
                </td>
                <td >
                    <mc:CurrencyTextbox id="txtDedPerEvent" OnChange="setDataChanged(true)" runat="server" size="25" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='DedPerEvent']"/>
                </td>
                
                </tr>
                <tr>
                
                    <td align="left" >
                    <asp:Label ID="lblAggLimit" runat="server" Text="<%$ Resources:lblDedLimit %>"></asp:Label>
                </td>
                <td >
                    <mc:CurrencyTextbox id="txtAggLimit" OnChange="setDataChanged(true)" runat="server" size="25" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='AggregateLimit']"/>
                </td>
                </tr>
                <tr>
                  <td align="left">
                    <asp:Label ID="lblExclExpensePmt" runat="server" Text="<%$ Resources:lblExclExpensePmt %>"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox runat="server" OnChange="setDataChanged(true)" id="chkExcludeExpense" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ExcludeExpenseFlag']"/>
                </td>
                
                </tr>
                <!-- End - Added by Nikhil on 10/13/14. -->

                <tr>
                    <td>
                        <asp:Label ID="lblApplicableState" runat="server" Text="<%$ Resources:lblApplicableState %>" />
                    </td>
                    <td>
                        <uc:CodeLookUp ID="ApplicableState"  CodeTable="STATES" OnChange="SetDataChanged(true);" runat="server" ControlName="ApplicableState" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='ApplicableState']" RMXType="code"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="lblApplicableReserveType" runat="server" Text="<%$ Resources:lblApplicableReserveType %>" />
                    </td>
                    <td>
                        <uc:MultiCode ID="lstApplicableReserveType"  CodeTable="RESERVE_TYPE" width="200px" runat="server" ControlName= "lstApplicableReserveType" RMXRef="/Instance/Document/form/group/displaycolumn/ApplicableReserveType" RMXType="codelist"/>
                    </td>
                </tr>
                               
             <tr><td>&nbsp;</td></tr>
            <%--//jira# RMA-6135.Multicurrency--%>
           <asp:TextBox runat="server" id="BaseCurrency" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/BaseCurrencyType"></asp:TextBox>
                   
        </table>
    </div>
    </form>
</body>
</html>
