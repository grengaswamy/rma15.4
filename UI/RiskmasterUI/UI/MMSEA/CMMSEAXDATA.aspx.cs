﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.UI.FDM;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using Riskmaster.AppHelpers;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.MMSEA
{
    public partial class CMMSEAXDATA : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        private string PageID = RMXResourceManager.RMXResourceProvider.PageId("CMMSEAXDATA.aspx");

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Vsharma205-Jira-RMA-19869-Starts
                if (AppHelper.GetQueryStringValue("Multicvg") == "True")
                {
                    ListItem removeItem = LobCode.Items.FindByValue("242"); ;
                    LobCode.Items.Remove(removeItem);

                    removeItem = LobCode.Items.FindByValue("-3"); ;
                    LobCode.Items.Remove(removeItem);

                    ListItem removeItem2 = SecondLobCode.Items.FindByValue("242");
                    SecondLobCode.Items.Remove(removeItem2);
                }
                //Vsharma205-Jira-RMA-19869-Ends
                //MITS 34102  - rkulavil
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, PageID, "CMMSEAXDATAValidations",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CMMSEAXDATAValidations", sValidationResources, true);
                //MITS 34102  - rkulavil
                if (!IsPostBack)
                {
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    EntityEID.Text = AppHelper.GetQueryStringValue("EntityId");
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);

                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("CMMSEAXDataAdaptor.GetMMSEAXData", XmlTemplate, out sCWSresponse, false, true);
                       

                    }
                }
            }
            catch (Exception ee)
            {

                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        public override void ModifyControl(XElement Xelement)
        {
            base.ModifyControl(Xelement);
            //Start rsushilaggar MITS 20818 Date 06/07/2011
            DropDownList LOBlist = (DropDownList)this.FindControl("LobCode");
            //Start: MITS 25350: SMISHRA54 06/30/2011
            if (!string.IsNullOrEmpty(Xelement.Value))
            {
            //End : SMISHRA54
                string sLobCode = Xelement.Descendants("LobCode").FirstOrDefault().Value;
                if (LOBlist != null)
                {
                    if (sLobCode == "-3")
                    {
                        DropDownList SecLOBlist = (DropDownList)this.FindControl("SecondLobCode");
                        if (SecLOBlist != null)
                            SecLOBlist.Enabled = false;
                    }

                }
                //End rsushilaggar
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("CMMSEAXDataAdaptor.SaveMMSEAXData", XmlTemplate, out sCWSresponse, true, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>if(window.opener.document.getElementById('setGridDataChanged')!=null){window.opener.document.getElementById('setGridDataChanged').value='true';}window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate()
        {
            string sRowId = string.Empty;
            if (mode.Text.ToLower() == "edit")
            {
                sRowId = AppHelper.GetQueryStringValue("selectedid");

            }
            else
            {
                sRowId = "-1";
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><MMSEAXDataDetails>");
            sXml = sXml.Append("<MMSEAXData>");
            sXml = sXml.Append("<EntMMSEARowID>");
            sXml = sXml.Append(sRowId);
            sXml = sXml.Append("</EntMMSEARowID>");
            sXml = sXml.Append("<EntityEID>");
            sXml = sXml.Append(EntityEID.Text);
            sXml = sXml.Append("</EntityEID>");
            sXml = sXml.Append("<LangCode>");
            sXml = sXml.Append(AppHelper.GetLanguageCode());
            sXml = sXml.Append("</LangCode>");
             sXml = sXml.Append("<PageId>");
             sXml = sXml.Append(PageID);
             sXml = sXml.Append("</PageId>");
            sXml = sXml.Append("</MMSEAXData>");
            sXml = sXml.Append("</MMSEAXDataDetails></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

    }
}