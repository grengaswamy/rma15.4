﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMSEAPartyData.aspx.cs" Inherits="Riskmaster.UI.UI.MMSEA.MMSEAPartyData" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MMSEA Party to Claim</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/utilities.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>
    <script type="text/javascript">
        function setValue(sFielMark)
    {
        
        document.getElementById(sFielMark + "lastfirstname_cid").value = document.getElementById(sFielMark + "entityid").value

    }
    
    </script>

</head>
<body onload="CopyGridRowDataToPopup();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="<%$ Resources:lblFormTitle %>" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:TextBox style="display:none" runat="server" id="MMSEAClaimantRowID" RMXRef="//MMSEAClaimantRowID" RMXType="id" rmxignoreset="true"/>
        <asp:TextBox style="display:none" runat="server" id="ClaimantRowID" RMXRef="//ClaimantRowID" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="DttmRcdAdded" RMXRef="//DttmRcdAdded" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="DttmRcdLastUpd" RMXRef="//DttmRcdLastUpd" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedid" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="mode" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="selectedrowposition" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="gridname" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="UniqueId" RMXRef="" RMXType="id" Text="MMSEAClaimantRowID" />
        <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr><td>&nbsp;</td></tr>
             <tr>
                <td>
                   <asp:label runat="server" id="lblRelationToBenificiary" Text="<%$ Resources:lblRelationToBenificiary %>" />
                </td>
                <td>
                    &nbsp;
                </td>
                 <td>
                    <uc:CodeLookUp runat="server" ID="RelationToBenificiary" CodeTable="MMSEA_CLMPRTY_CODE" ControlName="RelationToBenificiary"
                        RMXRef="//RelationToBenificiary" tabindex="1"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lbl_att1lastfirstname" Text="<%$ Resources:lblatt1lastfirstname %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                        <asp:Textbox runat="server" size="30" onchange="lookupTextChanged(this);" tabindex="2" onblur="lookupLostFocus(this);" RMXRef="//att1lastfirstname" id="att1lastfirstname" Enabled="false"/>
                        <input type="button" class="EllipsisControl" tabindex="3" id="att1lastfirstnamebtn"  value="..." onblur="setValue('att1');" runat="server" disabled="disabled"/>
                        <asp:Textbox runat="server" style="display:none"  id="att1entityid" />
                        <asp:Textbox runat="server" style="display:none" id="att1lastfirstname_cid" RMXRef="//att1lastfirstname/@codeid"/>
                </td>                
            </tr>            
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblTypeOfRepresentative" Text="<%$ Resources:lblTypeOfRepresentative %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                 <td>
                    <uc:CodeLookUp runat="server" ID="TypeOfRepresentative" CodeTable="MMSEA_CLPYREP_CODE" ControlName="TypeOfRepresentative"
                        RMXRef="//TypeOfRepresentative" tabindex="4" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lbl_attlastfirstname" Text="<%$ Resources:lblattlastfirstname %>"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                        <asp:Textbox runat="server" size="30" onchange="lookupTextChanged(this);" tabindex="5" onblur="lookupLostFocus(this);" RMXRef="//attlastfirstname" id="attlastfirstname" Enabled="false"/>
                        <input type="button" class="EllipsisControl" tabindex="6" id="attlastfirstnamebtn"  value="..." onblur="setValue('att');" runat="server" disabled="disabled"/>
                        <asp:Textbox runat="server" style="display:none"  id="attentityid" />
                        <asp:Textbox runat="server" style="display:none" id="attlastfirstname_cid" RMXRef="//attlastfirstname/@codeid"/>
                </td>                
            </tr>
            
        </tbody>
    </table>
    <div>
        <asp:TextBox style="display:none" runat="server" id="txtData" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="txtPostBack" RMXRef="" RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="PartyNameData"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="PartyName"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="Sender"  RMXType="id"/>
        <asp:TextBox style="display:none" runat="server" id="Representative"  RMXType="id"/>
        <asp:TextBox style="display:none" runat="server" id="Representative_cid"  RMXType="id"/>
        <asp:TextBox style="display:none" runat="server" id="Beneficiary"  RMXType="id"/>
        <asp:TextBox style="display:none" runat="server" id="Beneficiary_cid"  RMXType="id"/>
        <asp:TextBox style="display:none" runat="server" id="FormName"  RMXType="id" value="MMSEAPartyData"/>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnOk">
          <asp:button class="button" runat="server" id="btnOk" Text="<%$ Resources:btnOk %>" width="75px" onClientClick="return MMSEAPartyData_onOk();" onclick="btnOk_Click" />
        </div>
        <div class="formButton" runat="server" id="div_btnCancel">
          <asp:button class="button" runat="server" id="btnCancel"  Text="<%$ Resources:btnCancel %>" width="75px" onClientClick="return MMSEAPartyData_onCancel();" onclick="btnCancel_Click" />
        </div>
      </div>
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="<%$ Resources:PleaseWaitDialog1 %>" />
    </form>
  </body>
</html>