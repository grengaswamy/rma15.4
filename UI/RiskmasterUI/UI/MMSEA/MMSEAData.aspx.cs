﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared.Controls;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MMSEA
{
    public partial class MMSEAData : NonFDMBasePageCWS
    {
        int Counter = 0;
        string sClaimantEid = "-1";
        string sClaimId = "-1";
        string sPartNameData = string.Empty;
        //Added by asingh263 mits 32440
        string sClaimantsRepresentativeCode = string.Empty;
        string sType = string.Empty;
        string sRepresentative = string.Empty;
        //mits 32440 ends
        private string PageID = RMXResourceManager.RMXResourceProvider.PageId("MMSEAData.aspx");
        protected void Page_Load(object sender, EventArgs e)
        {
			//rkulavil - ML Changes - MITS 34107  -start
            langcodeid.Text = AppHelper.GetLanguageCode();
            pageinfoid.Text = PageID;
			//rkulavil - ML Changes - MITS 34107  -end
            if (Request.QueryString["ClaimantEid"] != null)
            {
                sClaimantEid = AppHelper.GetQueryStringValue("ClaimantEid");
            }

            if (Request.QueryString["ClaimId"] != null)
            {
                sClaimId = AppHelper.GetQueryStringValue("ClaimId");
            }
            ////MITS 34107 - rkulavil
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this,2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }

            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, PageID, "MMSEADataValidations",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "MMSEADataValidations", sValidationResources, true);
            //MITS 34107 - rkulavil
            if (!Page.IsPostBack)
            {
                GetMMSSEAData();
                SetEnableDisable();
            } 
            else
            {
                // akaushik5 Changed for MITS 24985 Starts
                //if (MMSEAPartyGrid_RowAddedFlag.Text == "true" || TPOCGrid_RowAddedFlag.Text == "true")
                if (MMSEAPartyGrid_RowAddedFlag.Text == "true" || TPOCGrid_RowAddedFlag.Text == "true" || Representative_AddedEditedFlag.Text.Equals("true"))
                // akaushik5 Changed for MITS 24985 Ends
                {
                    SetDateFileDeletedFlag();
                    NonFDMCWSPageLoad("ClaimantMMSEAAdapter.GetData");//Calling a  Function just to update the MessageTemplate for Grid Binding.
                    MMSEAPartyGrid_RowAddedFlag.Text = "false";
                    TPOCGrid_RowAddedFlag.Text = "false";
                    // akaushik5 Added for MITS 24985 Starts
                    Representative_AddedEditedFlag.Text = "false";
                    // akaushik5 Added for MITS 24985 Ends
                    SetEnableDisable();
                }
            }

            //mits 32440
            sClaimantsRepresentativeCode = ((TextBox)ClaimantsRepresentativeCode.FindControl("codelookup")).Text;
            if (!string.IsNullOrEmpty(sClaimantsRepresentativeCode))
            {
                sType = sClaimantsRepresentativeCode.Substring(0, 1);
                clmtreplastfirstname.Enabled = true;
                clmtreplastfirstnamebtn.Disabled = false;

                switch (sType.ToUpper())
                {
                    case "A":
                        clmtreplastfirstnamebtn.Attributes.Add("onclick", "lookupData('clmtreplastfirstname','ATTORNEYS','-1','clmtrep','1')");
                        sRepresentative = "A";
                        break;
                    case "G":
                        clmtreplastfirstnamebtn.Attributes.Add("onclick", "lookupData('clmtreplastfirstname','GUARDIAN_TYPE','-1','clmtrep','1')");
                        sRepresentative = "G";
                        break;
                    case "P":
                        clmtreplastfirstnamebtn.Attributes.Add("onclick", "lookupData('clmtreplastfirstname','POWOFATTORNEY_TYPE','-1','clmtrep','1')");
                        sRepresentative = "P";
                        break;
                    case "O":
                        clmtreplastfirstnamebtn.Attributes.Add("onclick", "lookupData('clmtreplastfirstname','OTHER_PEOPLE','-1','clmtrep','1')");
                        sRepresentative = "O";
                        break;
                    case "F":
                        clmtreplastfirstnamebtn.Attributes.Add("onclick", "lookupData('clmtreplastfirstname','ATTORNEY_FIRMS','-1','clmtrep','1')");
                        sRepresentative = "F";
                        break;
                }
            }
            // akaushik5 Added for MITS 24985 Starts
            if (ViewState["Representative"] != null)
            {
                if (ViewState["Representative"].ToString() != sRepresentative)
                {
                    clmtreplastfirstname.Text = "";
                }
                else
                {
                    if (clmtreplastfirstname.Text == "" && clmtreplastfirstname_cid.Text == "0")
                    {
                        clmtreplastfirstname.Text = Representative.Text;
                        clmtreplastfirstname_cid.Text = Representative_cid.Text;
                    }
                }
            }
            ViewState["Representative"] = sRepresentative;
            // akaushik5 Added for MITS 24985 Ends

            if (TPOCGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                string selectedRowId = TPOCSelectedId.Text;
                hdnTPOCDeletedValues.Text = hdnTPOCDeletedValues.Text + "," + selectedRowId;
                SelectedId.Text = selectedRowId + "|TPOC";
                if (hdnTPOCDeletedValues.Text.Length > 0)
                {
                    Counter = hdnTPOCDeletedValues.Text.IndexOf(",");
                    if (Counter == 0)
                        hdnTPOCDeletedValues.Text = hdnTPOCDeletedValues.Text.Remove(0, 1);
                }
                
            }

            if (MMSEAPartyGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                string selectedRowId = MMSEAPartySelectedId.Text;
                hdnPartyDeletedValues.Text = hdnPartyDeletedValues.Text + "," + selectedRowId;
                SelectedId.Text = selectedRowId + "|Party";

                if (hdnPartyDeletedValues.Text.Length > 0)
                {
                    Counter = hdnPartyDeletedValues.Text.IndexOf(",");
                    if (Counter == 0)
                        hdnPartyDeletedValues.Text = hdnPartyDeletedValues.Text.Remove(0, 1);

                }
            }
            if (MMSEAPartyGrid_RowDeletedFlag.Text.ToLower() == "true" || TPOCGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                SetDateFileDeletedFlag();
                NonFDMCWSPageLoad("ClaimantMMSEAAdapter.GetClaimantMMSEADummyData");
                MMSEAPartyGrid_RowDeletedFlag.Text = "false";
                TPOCGrid_RowDeletedFlag.Text = "false";
                SetEnableDisable();
            }
            UserControlDataGrid obj = (UserControlDataGrid)this.FindControl("MMSEAPartyGrid"); //Capturing PartyName
            GridView gvData = (GridView)obj.GridView;
            int iGridRowCount = gvData.Rows.Count;
            for (int i = 0; i < iGridRowCount - 1; i++)
            {
                if (sPartNameData == "" || sPartNameData == string.Empty)
                {
                    sPartNameData = gvData.Rows[i].Cells[5].Text; //Changed for Mits 17762:RelationToBeneficary is required before filling EntityName
                }
                else
                {
                    sPartNameData = sPartNameData + "|" + gvData.Rows[i].Cells[5].Text;
                }
            }
            PartyNameData.Text = sPartNameData;

            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //doing his as behaviour of code look/muliCode onchange not predictable
            if (setHidden.Text == "")
            {
                setHidden.Text = "0";
                //TextBox txtDiagnosisvalues = (TextBox)CSCDiagnosisList.FindControl("multicode_lst");
                TextBox txtDiagnosisvalues = CSCDiagnosisList_lst;
                txtHdnList.Text = txtDiagnosisvalues.Text;
		//MITS 32423: praveen ICD10 Changes start
                TextBox txtICD10Diagnosisvalues = CSCICD10DiagnosisList_lst;
                txtHdnListICD10.Text = txtICD10Diagnosisvalues.Text;
		//MITS 32423: praveen ICD10 Changes end
                hdnProductHarmText.Text = ProductHarmText.Text;
            }
           
        }

        private void GetMMSSEAData()
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XmlNode objNode = null;
            

            if (claimantrowid.Text == "")
            {
                XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                <Call>
                <Function>ClaimantMMSEAAdapter.GetClaimantRowIdIfExist</Function> 
                </Call>
                <Document>
            <PassToWebService>
            <MMSEA>
            <ClaimantEid>" + sClaimantEid + @"</ClaimantEid>
            <ClaimId>" + sClaimId + @"</ClaimId>
            </MMSEA>
            </PassToWebService>
            </Document>
            </Message>");

                using (XmlReader reader = oTemplate.CreateReader())
                {
                    xmlNodeDoc.Load(reader);
                }

                string strCWSSrvcOutput = AppHelper.CallCWSService(xmlNodeDoc.InnerXml);
                XmlDocument xmlOutDoc = new XmlDocument();
                xmlOutDoc.LoadXml(strCWSSrvcOutput);

                objNode = xmlOutDoc.SelectSingleNode("ResultMessage/Document/ClaimantMMSEA/ClaimantRowID");
                claimantrowid.Text = objNode.InnerXml;

                //objNode = xmlOutDoc.SelectSingleNode("ResultMessage/Document/ClaimantMMSEA/IsNew");//Commented for Mits 19213-Variable is no more being used.
                
                NonFDMCWSPageLoad("ClaimantMMSEAAdapter.GetClaimantMMSEAData");
                //IsNew.Text = "false";
                 
            }
            else
            {
                   NonFDMCWSPageLoad("ClaimantMMSEAAdapter.GetClaimantMMSEAData");
            }
            
           
        }

        private void SetDateFileDeletedFlag()
        {

            DropDownList ddList = (DropDownList)div2.FindControl("ddlDeleteRcd");
            if (ddList.SelectedItem.Text.ToUpper() == "YES")
            {
                DelMDCRERCDFlag.Text = "1";
            }
            else
            {
                DelMDCRERCDFlag.Text = "0";
            }
        }

        public override void ModifyXml(ref XElement Xelement)
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            ListBox lstvalues = null;
            TextBox tbnewvalues = null;
            ListBox lstvaluesICD10 = null;    		//MITS 32423: praveen ICD10 Changes
            TextBox tbnewvaluesICD10 = null; 		//MITS 32423: praveen ICD10 Changes
            string sNode = "";
            using (XmlReader reader = Xelement.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }
            XmlNode objXNode = xmlNodeDoc.SelectSingleNode("//CSCDiagnosisList");

            if (objXNode != null)
            {
           
                //lstvalues = (ListBox)CSCDiagnosisList.FindControl("multicode");
                //tbnewvalues = (TextBox)CSCDiagnosisList.FindControl("multicode_lst");
                lstvalues = CSCDiagnosisList;
                tbnewvalues = CSCDiagnosisList_lst;
                dochange(lstvalues, tbnewvalues);
                if (lstvalues != null)
                {
                    sNode = "";

                    for (int i = 0; i < lstvalues.Items.Count; i++)
                    {
                        sNode = sNode + "<Item value='" + lstvalues.Items[i].Value + "' codeid='" + lstvalues.Items[i].Value + "'>" + lstvalues.Items[i].Text + "</Item>";
                    }
                    sNode = "<DiagnosisList  type='codelist'>" + sNode + "</DiagnosisList>";
                    if (lstvalues.Items.Count > 0)
                    {
                        objXNode.InnerXml = sNode;
                    }

                    Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
                }
            }
            //prashbiharis ICD10 changes (MITS 32423) start

            XmlNode objXNode1 = xmlNodeDoc.SelectSingleNode("//CSCICD10DiagnosisList");
            if (objXNode1 != null)
            {
                lstvaluesICD10 = CSCICD10DiagnosisList;
                tbnewvaluesICD10 = CSCICD10DiagnosisList_lst;
                dochange(lstvaluesICD10, tbnewvaluesICD10);
                if (lstvaluesICD10 != null)
                {
                    sNode = "";

                    for (int i = 0; i < lstvaluesICD10.Items.Count; i++)
                    {
                        sNode = sNode + "<Item value='" + lstvaluesICD10.Items[i].Value + "' codeid='" + lstvaluesICD10.Items[i].Value + "'>" + lstvaluesICD10.Items[i].Text + "</Item>";
                    }
                    sNode = "<DiagnosisList  type='codelist'>" + sNode + "</DiagnosisList>";
                    if (lstvaluesICD10.Items.Count > 0)
                    {
                        objXNode1.InnerXml = sNode;
                    }

                    Xelement = XElement.Parse(xmlNodeDoc.InnerXml);
                }
            }
            
            //prashbiharis ICD10 changes (MITS 32423) End
        }


        private void dochange(ListBox lbold, TextBox tbnew)
        {
            string [] arrCodes = new string[1];
            lbold.Items.Clear();
            ListItem lsitem = null;
            if (tbnew.Text != "")
            {
                if (tbnew.Text.IndexOf(" ") > 0)
                    arrCodes = tbnew.Text.Split(' ');
                else
                    arrCodes[0] = tbnew.Text;

                for (int i = 0; i < arrCodes.Length; i++)
                {
                    lsitem = new ListItem(arrCodes[i], arrCodes[i]);
                    lbold.Items.Add(lsitem);
                }
            }
           
        }



        private void SetEnableDisable()
        {
           
            ConfirmedDate.ReadOnly = true;
            ConfirmedDate.Style.Add("background-color", "#F2F2F2");
            //ConfirmedDatebtn.Enabled = false;

            LastExtractDate.ReadOnly = true;
            LastExtractDate.Style.Add("background-color", "#F2F2F2");
            //LastExtractDatebtn.Enabled = false;

            MSPEffectiveDate.ReadOnly = true;
            MSPEffectiveDate.Style.Add("background-color", "#F2F2F2");
            //MSPEffectiveDatebtn.Enabled = false;

            MSPTerminationDate.ReadOnly = true;
            MSPTerminationDate.Style.Add("background-color", "#F2F2F2");
            //MSPTerminationDatebtn.Enabled = false;

            MSPTypeIndicator.Enabled = false;

            DispositionCode.Enabled = false;

            if (DelMDCRERCDFlag.Text == "1")
            {
                DropDownList ddList = (DropDownList)div2.FindControl("ddlDeleteRcd");
                ddList.SelectedIndex = 0;
            }
            else
            {
                DropDownList ddList = (DropDownList)div2.FindControl("ddlDeleteRcd");
                ddList.SelectedIndex = 1;
            }

            //ListBox lstvalues = (ListBox)CSCDiagnosisList.FindControl("multicode");
            //TextBox tbnewvalues = (TextBox)CSCDiagnosisList.FindControl("multicode_lst");
            ListBox lstvalues = CSCDiagnosisList;
            TextBox tbnewvalues = CSCDiagnosisList_lst;
            tbnewvalues.Text = "";
            for (int i = 0; i < lstvalues.Items.Count; i++)
            {
                if (i == 0)
                {
                    tbnewvalues.Text = lstvalues.Items[i].Value;
                }
                else
                {
                    tbnewvalues.Text = tbnewvalues.Text + " " + lstvalues.Items[i].Value;
                }
            }
            //prashbiharis ICD10 changes (MITS 32423) start
            ListBox lstvaluesICD10 = CSCICD10DiagnosisList;
            TextBox tbnewvaluesICD10 = CSCICD10DiagnosisList_lst;
            tbnewvaluesICD10.Text = "";
            for (int i = 0; i < lstvaluesICD10.Items.Count; i++)
            {
                if (i == 0)
                {
                    tbnewvaluesICD10.Text = lstvaluesICD10.Items[i].Value;
                }
                else
                {
                    tbnewvaluesICD10.Text = tbnewvaluesICD10.Text + " " + lstvaluesICD10.Items[i].Value;
                }
            }
            //prashbiharis ICD10 changes (MITS 32423)End
        }

        protected void VerifyChangeinLookup()
        {
            try
            {
                //TextBox objDiagvalues = (TextBox)CSCDiagnosisList.FindControl("multicode_lst");
                TextBox objDiagvalues = CSCDiagnosisList_lst;
                if (txtHdnList.Text != objDiagvalues.Text)
                {
                    MMSEAEditedFlag.Text = "1";
                }
		//MITS 32423: praveen ICD10 Changes start
                TextBox objICD10Diagvalues = CSCICD10DiagnosisList_lst;
                if (txtHdnListICD10.Text != objICD10Diagvalues.Text)
                {
                    MMSEAEditedFlag.Text = "1";
                }
		//MITS 32423: praveen ICD10 Changes end
                string sHarmText = HttpContext.Current.Request.Form["ProductHarmText"];
                if (sHarmText != hdnProductHarmText.Text)
                {
                    MMSEAEditedFlag.Text = "1";
                }

                setHidden.Text = "";
            }
            catch(Exception ex){};
        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSresponse = "";
            SetDateFileDeletedFlag();

            VerifyChangeinLookup();

            bool bResult = CallCWS("ClaimantMMSEAAdapter.SaveClaimantMMSEAData", null, out sCWSresponse, true, false);

            ErrorControl1.errorDom = sCWSresponse;

            XmlDocument xmlServiceDoc = new XmlDocument();
            xmlServiceDoc.LoadXml(sCWSresponse);
             if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
             {

                 //IsNew.Text = "false";//Commented for Mits 19213-Variable is no more being used.
                NonFDMCWSPageLoad("ClaimantMMSEAAdapter.GetClaimantMMSEAData");
                SetEnableDisable();
            }
             SysPageDataChanged.Text = "";
             hdnTPOCDeletedValues.Text = "";  

        }
        public override void ModifyControl(XElement objElement)
        {
            //Added Rakhi for Mits 20074:The Disposition Code field on Claimant MMSEA Data screen is too small
            XElement objDispCode = objElement.XPathSelectElement("//DispositionCode");
            TextBox txtDispCode = (TextBox)DispositionCode.FindControl("codelookup");
            if (objDispCode != null && txtDispCode != null) 
            {
                if (objDispCode.Value != string.Empty && objDispCode.Value.Length > 30)
                    txtDispCode.Width = Unit.Percentage(40);
                else
                    txtDispCode.Width = Unit.Percentage(22.5);
            }
            //Added Rakhi for Mits 20074:The Disposition Code field on Claimant MMSEA Data screen is too small
        }
    }
}
