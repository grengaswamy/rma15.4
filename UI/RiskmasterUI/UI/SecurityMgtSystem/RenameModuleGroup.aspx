﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RenameModuleGroup.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.RenameModuleGroup" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Rename Module Group</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script type="text/javascript" src="../../Scripts/drift.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript" src="../../Scripts/security.js"></script>
   
</head>
<body onload="javascript:CloseIfSuccess();">
    <form id="frmData" runat="server" name="DsnWizard" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:Label ID="errormessage" CssClass="errtext" runat="server"></asp:Label>
    
    <table>
    <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
    <tr>
     <td>
      <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
       <tr>
        <td class="ctrlgroup">Rename Module Group</td>
       </tr>
      </table>
      <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
       <tr>
        <td></td>
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='entitytype']"
                        Style="display: none" ID="entitytype"/></td>        
        <td></td>
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='selectedentityid']"
                        Style="display: none" ID="selectedentityid"/></td>        
        <td>Enter new group name</td>
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='groupname']"
                        ID="groupname" size="30" onchange="setDataChanged(true);"/></td>     
        <td></td>
        <td><asp:TextBox runat="server" Style="display: none" ID="hdAction"/></td>        
        <td></td>
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdNewDBId']"
                        Style="display: none" ID="hdNewDBId"/></td>        
        <td></td>
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdUserLoginName']"
                        Style="display: none" ID="hdUserLoginName"/></td> 
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/@postback" Style="display: none" ID="hdpostback"/></td> 
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/@iserror" Style="display: none" ID="hdiserror"/></td> 
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/@closeonpostbackifnoerr" Style="display: none" ID="hdcloseonpostbackifnoerr"/></td> 
        <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/@message" Style="display: none" ID="hdmessage"/></td> 
      </tr>
      </table>
     </td>
    </tr>
    <table border="0" width="100%" class1="singleborder" cellspacing="0" cellpadding="0">
     <tr>
      <td align="center"><asp:button runat="server" class="button" id="btnOk" Text=" OK " OnClientClick="return SetWizardAction(this,'RenameModuleGroupEntity',event)" style=""/><asp:button runat="server" class="button" id="btnCancel" Text=" Cancel " OnClientClick="return OnCancel()" style=""/></td>
     </tr>
    </table>
    </table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
