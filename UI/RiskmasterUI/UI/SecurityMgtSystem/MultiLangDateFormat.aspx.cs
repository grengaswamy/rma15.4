﻿/***************************************************************************************************
 *   Date     |  MITS/JIRA  | Programmer | Description                                            
 ***************************************************************************************************
 * 24 Dec 2014  | RMA-6033     | ttumula2   | UI option to set date format for languages
 ***************************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Common;
using System.Text;
using Riskmaster.Models;
using Riskmaster.RMXResourceManager;
using System.Globalization;

namespace Riskmaster.UI.UI.SecurityMgtSystem
{
    public partial class MultiLangDateFormat : NonFDMBasePageCWS
    {
        private XElement XmlTemplate = null;
        private bool bReturnStatus = false;
        private string sreturnValue = string.Empty;
        private XmlDocument XmlDoc = new XmlDocument();
        private string sNode = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtLangCode.Text = AppHelper.GetLanguageCode();
                    txtPageId.Text = RMXResourceProvider.PageId("MultiLangDateFormat.aspx");
                    bReturnStatus = CallCWS("MultiLanguageAdaptor.GetLanguages", XmlTemplate, out sreturnValue, true, true);
                    if (bReturnStatus)
                    {
                        ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                        XmlDoc.LoadXml(sreturnValue);
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}

