﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;//abansal: MITS 14677
using System.Xml.Linq;
using System.Text;
using System.Xml;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class UserPrivileges : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (lstLOB.Items.Count > 0)
                {
                    txtLOB.Text = lstLOB.SelectedValue.ToString();
                }
                if (!Page.IsPostBack)
                {
                    GetData();
                    hTabName.Text = "ReserveLimits";
                }
                else
                {
                    bool bReturnStatus = false;
                    switch (actionSelectedOnScreen.Text.ToLower())
                    {
                        case "addlimit":
                        case "deletelimit":
                        case "checkboxselected": bReturnStatus = CallCWSFunction("UserPrivilegesAdaptor.Save");
                            if (bReturnStatus)
                                GetData();
                            break;

                        default: GetData();
                            break;  
                    }

                    SetScreenLayout();
                }
                //abansal: MITS 14667 Starts
                DatabindingHelper.UpdateTabFocus(this);
                //abansal: MITS 14667 Ends
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "UserPre.ClientScript", "tabChange('" +  m_tabName +  "');", true);
        }

        /// <summary>
        /// Set the controls to hidden or visible
        /// </summary>
        private void SetScreenLayout()
        {
            int iSelectedUserId = 0;
            int iSelectedGroupId = 0;
            int.TryParse(hdSelectedUser.Text, out iSelectedUserId);
            int.TryParse(hdSelectedGroup.Text, out iSelectedGroupId);

            if (iSelectedUserId > 0)
            {
                rdUser.Checked = true;
                rdGroup.Checked = false;
                foreach (ListItem oItem in lbUserList.Items)
                {
                    if (oItem.Value == iSelectedUserId.ToString())
                        oItem.Selected = true;
                }
                lbUserList.Style["display"] = string.Empty;
                lbGroupList.Style["display"] = "none";
            }
            else if (iSelectedGroupId > 0)
            {
                rdUser.Checked = false;
                rdGroup.Checked = true;
                foreach (ListItem oItem in lbGroupList.Items)
                {
                    if (oItem.Value == iSelectedGroupId.ToString())
                        oItem.Selected = true;
                }
                lbUserList.Style["display"] = "none";
                lbGroupList.Style["display"] = string.Empty;
            }
            else
            {
                rdUser.Checked = true;
                rdGroup.Checked = false;
                lbUserList.Style["display"] = string.Empty;
                lbGroupList.Style["display"] = "none";
            }

        }

        private void GetData()
        {
            NonFDMCWSPageLoad("UserPrivilegesAdaptor.Get");
            rdUser.Checked = true;
            rdGroup.Checked = false;
            //PrintCheckLimits_Radio_User.Checked = true;
            //PrintCheckLimits_Radio_Group.Checked = false;
            ////MITS : 14708 : abansal23 Starts
            //PaymentLimits_Radio_User.Checked = true;
            //PaymentLimits_Radio_Group.Checked = false;
            ////MITS : 14708 : abansal23 Ends
            //PayDetailLimits_Radio_User.Checked = true;
            //PayDetailLimits_Radio_Group.Checked = false;
            //PayDetailLimits_Radio_User.Checked = true;
            //PayDetailLimits_Radio_Group.Checked = false;
            //ClaimLimits_Radio_User.Checked = true;
            //ClaimLimits_Radio_Group.Checked = false;
            //EventTypeLimits_Radio_User.Checked = true;
            //EventTypeLimits_Radio_Group.Checked = false;
            //PerClaimPayLimits_Radio_User.Checked = true;
            //PerClaimPayLimits_Radio_Group.Checked = false;
            //MITS: 14676 abansal23 Starts
            ReserveLimits_ReserveType.SelectedIndex = 0;
            ReserveMax.Text = "";
            PrintCheckMax.Text = "";
            PaymentMax.Text = "";
            PayDetailLimits_ReserveType.SelectedIndex = 0;
            PayDetailMax.Text = "";
            ClaimLimits_ClaimType.Text = "";
            ClaimLimits_ClaimType_cid.Text = "";
            ClaimLimits_ClaimStatus.Text = "";
            ClaimLimits_ClaimStatus_cid.Text = "";
            ClaimLimits_AllowAccess.Checked = false;
            ClaimLimits_AllowCreate.Checked = false;
            EventTypeLimits_EventType.Text = "";
            EventTypeLimits_EventType_cid.Text = "";
            EventTypeLimits_AllowAccess.Checked = false;
            EventTypeLimits_AllowCreate.Checked = false;
            PerClaimPayLimits_ClaimType.Text = "";
            PerClaimPayLimits_ClaimType_cid.Text = "";
            PerClaimPayLimitsMaxAmount.Text = "";
            PerClaimPayLimits_ReserveType.SelectedIndex =0;
            HasOverRideAuthority.Checked = false;   //Changes done by Gaurav for MITS 30226.
            PreventPayAboveLmt.Checked = false;     //Changes done by Gaurav for MITS 30226.
            //MITS: 14676 abansal23 Stops
            ClaimsTotalIncurredLimitsMaxAmount.Text = "";//sharishkumar Jira 6385            

            //Added by amitosh for R8 enhancement of Supervisory Approval
            TextBox ocontrol = (TextBox)((this.Form.FindControl("ReserveLimitsClaimType")).FindControl("codelookup"));
            if (ocontrol != null)
                ocontrol.Text = "";
            ocontrol = (TextBox)((this.Form.FindControl("PrintCheckLimitsClaimType")).FindControl("codelookup"));
            if (ocontrol != null)
                ocontrol.Text = "";
            ocontrol = (TextBox)((this.Form.FindControl("PaymentLimitsClaimType")).FindControl("codelookup"));
            if (ocontrol != null)
                ocontrol.Text = "";
            //sharishkumar Jira 6385 starts
            ocontrol = (TextBox)((this.Form.FindControl("ClaimsTotalIncurredLimitsClaimType")).FindControl("codelookup"));
            if (ocontrol != null)
                ocontrol.Text = "";
            //sharishkumar Jira 6385 ends
            ocontrol = (TextBox)((this.Form.FindControl("PayDetailLimitsClaimType")).FindControl("codelookup"));
            if (ocontrol != null)
                ocontrol.Text = "";
            ocontrol = (TextBox)(this.Form.FindControl("ReserveLimitsClaimType")).FindControl("codelookup_cid");
            ocontrol.Text = "";
            ocontrol = (TextBox)(this.Form.FindControl("PrintCheckLimitsClaimType")).FindControl("codelookup_cid");
            ocontrol.Text = "";
            ocontrol = (TextBox)(this.Form.FindControl("PaymentLimitsClaimType")).FindControl("codelookup_cid");
            ocontrol.Text = "";
            //sharishkumar Jira 6385 starts
            ocontrol = (TextBox)(this.Form.FindControl("ClaimsTotalIncurredLimitsClaimType")).FindControl("codelookup_cid");
            ocontrol.Text = "";
            //sharishkumar Jira 6385 ends
            ocontrol = (TextBox)(this.Form.FindControl("PayDetailLimitsClaimType")).FindControl("codelookup_cid");
            ocontrol.Text = "";
            //end Amitosh


            //MITS: 14677 abansal23 Starts
            actionSelectedOnScreen.Text = "";
            //MITS: 14677 abansal23 Starts
            //ArrayList arlistReserveType = new ArrayList(ReserveLimits_ReserveType.Items.Count);
            //foreach (ListItem item in ReserveLimits_ReserveType.Items)
           // {
           //     arlistReserveType.Add(item);
           // }
           // PayDetailLimits_ReserveType.DataSource = arlistReserveType;
           // PayDetailLimits_ReserveType.DataBind();
           // PerClaimPayLimits_ReserveType.DataSource = arlistReserveType;
           // PerClaimPayLimits_ReserveType.DataBind();
            
            //Mgaba2: R8:SuperVisory Approval
			//In Case Carrier Claims is ON,Per Claim Pay Limits caption should be changed to Incurred Limits
            //if (hdnAdvancedClaims.Text == "0")
            //{
                hdnPerClaimPayLimitTabname.Value = "Per Claim Pay Limits";
                PerClaimPayLimits_EnableLimits.Text = "Enable Per Claim Pay Limits";
                PerClaimPayLimits_EnableLimits.Attributes["onclick"] = "return SaveEnableDiableLimits('PerClaimPayLimits_EnableLimits','Are you sure you wish to change the Enable Per Claim Payment Limits Option?')";
            //MITS 30937 Raman: Following claim types should only be visible when carrier is turned on
                if (hdnAdvancedClaims.Text == "0")
                {

                    //Added by amitosh for R8 enhancement of Supervisory Approval
                    ReserveLimitsClaimType.Visible = false;
                    lbReserveLimits_ClaimType.Visible = false;
                    PrintCheckLimitsClaimType.Visible = false;
                    lbPrintCheckLimits_ClaimType.Visible = false;
                    lbPaymentLimits_ClaimType.Visible = false;
                    PaymentLimitsClaimType.Visible = false;
                    lbPayDetailLimits_ClaimType.Visible = false;
                    PayDetailLimitsClaimType.Visible = false;
                    ClaimsTotalIncurredLimits_ClaimType.Visible = false;//sharishkumar Jira 6385
                    ClaimsTotalIncurredLimitsClaimType.Visible = false;//sharishkumar Jira 6385
                    //end Amitosh
                }
            //}
            //else  //MITS 30625 mcapps2 We do not change the name of Per Claim Pay Limits it does not matter if Carrier Claims is turned on or not.
            //{
                                
            //    hdnPerClaimPayLimitTabname.Value = "Incurred Limits";
            //    PerClaimPayLimits_EnableLimits.Text = "Enable Incurred Limits";
            //    PerClaimPayLimits_EnableLimits.Attributes["onclick"] = "return SaveEnableDiableLimits('PerClaimPayLimits_EnableLimits','Are you sure you wish to change the Enable Incurred Limits Option?')";
                
            //}

            //Asharma326 MITS 30874
                this.CheckUncheckRestrictedUsers();
            //Asharma326 MITS 30874
        }

        private void CheckUncheckRestrictedUsers()
        {
            //Asharma326 MITS 30874 Starts
            ReserveLimitsUsers_EnableLimits.Enabled = ReserveLimits_EnableLimits.Checked ? true : false;
            PrintCheckLimitsUsers_EnableLimits.Enabled = PrintCheckLimits_EnableLimits.Checked ? true : false;
            PaymentLimitsUsers_EnableLimits.Enabled = PaymentLimits_EnableLimits.Checked ? true : false;
            PayDetailLimitsUsers_EnableLimits.Enabled = PayDetailLimits_EnableLimits.Checked ? true : false;
            ClaimLimitsUsers_EnableLimits.Enabled = ClaimLimits_EnableLimits.Checked ? true : false;
            EventTypeLimitsUsers_EnableLimits.Enabled = EventTypeLimits_EnableLimits.Checked ? true : false;
            PerClaimPayLimitsUsers_EnableLimits.Enabled = PerClaimPayLimits_EnableLimits.Checked ? true : false;
            //Asharma326 MITS 30874 Ends
            ClaimsTotalIncurredLimitsUsers_EnableLimits.Enabled = ClaimsTotalIncurredLimits_EnableLimits.Checked ? true : false;//sharishkumar Jira 6385
        }
        /// <summary>
        /// To modify the controls for R8 enhancement of Supervisory Approval if Advanced Claim is On 
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="Xelement"></param>
        public override void ModifyControl(XElement Xelement)
        {

            base.ModifyControl(Xelement);
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(Xelement.ToString());
            //skhare7 R8 supervisory approval
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//ReserveLimitsListRowData") != null)
            {
                hdnReserveLimitsList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//ReserveLimitsListRowData").InnerText;
            }
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PrintCheckLimitsListRowData") != null)
            {
                hdnPrintCheckLimitsList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PrintCheckLimitsListRowData").InnerText;
            }
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PaymentLimitsListRowData") != null)
            {
                hdnPaymentLimitsList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PaymentLimitsListRowData").InnerText;
            }
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PayDetailLimitsListRowData") != null)
            {
                hdnPayDetailLimitsList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PayDetailLimitsListRowData").InnerText;
            }
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//ClaimLimitsListRowData") != null)
            {
                hdnClaimLimitsList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//ClaimLimitsListRowData").InnerText;
            }
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//EventTypeListRowData") != null)
            {
                hdnEventTypeList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//EventTypeListRowData").InnerText;
            }
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PerClaimPayLimitsListRowData") != null)
            {
                hdnPerClaimPayLimitsList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//PerClaimPayLimitsListRowData").InnerText;
            }

            //sharishkumar jira 6385 starts
            if (xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//ClaimsTotalIncurredLimitsListRowData") != null)
            {
                hdnClaimsTotalIncurredLimitsList.Value = xDoc.SelectSingleNode("//Document//Data//form//displaycolumn//ClaimsTotalIncurredLimitsListRowData").InnerText;
            }
            //sharishkumar jira 6385 ends

            //skhare7 R8 supervisory approval end
            if (xDoc.SelectSingleNode("//Document//Data//control [@name=\"hdnAdvancedClaims\"]").InnerText == "-1")
            {
                //Added by amitosh for R8 enhancement of Supervisory Approval

                if (txtLOB.Text == "")
                {
                    ClaimLimits_ClaimTypebtn.OnClientClick = "return selectCode('CLAIM_TYPE','ClaimLimits_ClaimType',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    PerClaimPayLimits_ClaimTypebtn.OnClientClick = "return selectCode('CLAIM_TYPE','PerClaimPayLimits_ClaimType',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";//Deb MITS 26308
                    WebControl octrl = (Button)(this.Form.FindControl("ReserveLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','ReserveLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PrintCheckLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PrintCheckLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PaymentLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PaymentLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PayDetailLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PayDetailLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    //sharishkumar Jira 6385 starts
                    octrl = (Button)(this.Form.FindControl("ClaimsTotalIncurredLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','ClaimsTotalIncurredLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    //sharishkumar Jira 6385 ends
                }

                else if (xDoc.SelectSingleNode("//Document//Data//control [@name=\"LOB\"]").Attributes["value"].Value == "241")
                {
                    ClaimLimits_ClaimTypebtn.OnClientClick = "return selectCode('CLAIM_TYPE','ClaimLimits_ClaimType',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    PerClaimPayLimits_ClaimTypebtn.OnClientClick = "return selectCode('CLAIM_TYPE','PerClaimPayLimits_ClaimType',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";//Deb MITS 26308

                    WebControl octrl = (Button)(this.Form.FindControl("ReserveLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','ReserveLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PrintCheckLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PrintCheckLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PaymentLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PaymentLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PayDetailLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PayDetailLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    //sharishkumar Jira 6385 starts
                    octrl = (Button)(this.Form.FindControl("ClaimsTotalIncurredLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','ClaimsTotalIncurredLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 241 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    //sharishkumar Jira 6385 ends

                }
                else if (xDoc.SelectSingleNode("//Document//Data//control [@name=\"LOB\"]").Attributes["value"].Value == "243")
                {
                    WebControl octrl = (Button)(this.Form.FindControl("ReserveLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','ReserveLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 243 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";

                    octrl = (Button)(this.Form.FindControl("PrintCheckLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PrintCheckLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 243 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PaymentLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PaymentLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 243 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    octrl = (Button)(this.Form.FindControl("PayDetailLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','PayDetailLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 243 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";

                    ClaimLimits_ClaimTypebtn.OnClientClick = "return selectCode('CLAIM_TYPE','ClaimLimits_ClaimType',null,null,'(CODES.LINE_OF_BUS_CODE = 243 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    PerClaimPayLimits_ClaimTypebtn.OnClientClick = "return selectCode('CLAIM_TYPE','PerClaimPayLimits_ClaimType',null,null,'(CODES.LINE_OF_BUS_CODE = 243 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";//Deb MITS 26308

                    //sharishkumar Jira 6385 starts
                    octrl = (Button)(this.Form.FindControl("ClaimsTotalIncurredLimitsClaimType")).FindControl("codelookupbtn");
                    octrl.Attributes["onclick"] = "return selectCode('CLAIM_TYPE','ClaimsTotalIncurredLimitsClaimType$codelookup',null,null,'(CODES.LINE_OF_BUS_CODE = 243 OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)');";
                    //sharishkumar Jira 6385 ends
                }
            }
            else
            {
                btnLimit.Visible = false;
                if (string.IsNullOrEmpty(ReserveLimitsGrid.HideNodes))
                {
                    ReserveLimitsGrid.HideNodes = "ClaimType";
                }
                else
                {
                    ReserveLimitsGrid.HideNodes = "|" + "ClaimType";
                }

                if (string.IsNullOrEmpty(PrintCheckLimitsGrid.HideNodes))
                {
                    PrintCheckLimitsGrid.HideNodes = "ClaimType";
                }

                else
                {
                    PrintCheckLimitsGrid.HideNodes = "|" + "ClaimType";
                }

                if (string.IsNullOrEmpty(PaymentLimitsGrid.HideNodes))
                {
                    PaymentLimitsGrid.HideNodes = "ClaimType";
                }
                else
                {
                    PaymentLimitsGrid.HideNodes = "|" + "ClaimType";
                }

                //sharishkumar Jira 6385 starts
                if (string.IsNullOrEmpty(ClaimsTotalIncurredLimitsGrid.HideNodes))
                {
                    ClaimsTotalIncurredLimitsGrid.HideNodes = "ClaimType";
                }
                else
                {
                    ClaimsTotalIncurredLimitsGrid.HideNodes = "|" + "ClaimType";
                }
                //sharishkumar Jira 6385 ends
                if (string.IsNullOrEmpty(PayDetailLimitsGrid.HideNodes))
                {
                    PayDetailLimitsGrid.HideNodes = "ClaimType";
                }
                else
                {
                    PayDetailLimitsGrid.HideNodes = "|" + "ClaimType";
                }

            }
        }


    }
}
