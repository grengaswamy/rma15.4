﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;

namespace Riskmaster.UI.SecurityMgtSystem
{
    public partial class DomainAuthentication : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdUserLoginName.Text = AppHelper.GetUserLoginName();
                if (!IsPostBack)
                {
                    NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");
                }
                else
                {

                    bool bReturnStatus = false;
                    string response;
                    switch (hdAction.Text.ToLower())
                    {
                        case "ok": bReturnStatus = CallCWSFunction("RMWinSecurityAdaptor.SaveDomainData", out response);
                            if ((bReturnStatus == true) && (sequenceid.Text != ""))
                            {                                             
                                NonFDMCWSPageLoad("SecurityAdaptor.GetXMLData");
                            }
                            break;

                        default: break;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
