﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PwdPolUnlockUserAccnt.aspx.cs"
    Inherits="Riskmaster.UI.SecurityMgtSystem.PwdPolUnlockUserAccnt" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js"></script>

    <script type="text/javascript" src="../../Scripts/rmx-common-ref.js"></script>

</head>
<body class="10pt">
    <form id="frmData" runat="server" name="DsnWizard" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    
    
    <table width="100%">
        <tr>
            <td class="msgheader" bgcolor="#D5CDA4">
                List of Users with Account Status:
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%">
        <tr>
            <td class="ctrlgroup">
                Select User(s) to Lock/Unlock Account and click on appropriate button
            </td>
        </tr>
    </table>
    <div id="divForms" class="divScroll" style="margin-left: 2 px; position: relative;
        left: 0; top: 0; width: 100%; height: 315px; overflow: auto; valign: top">
        <table cellspacing="0" width="100%">
            <tr style="width: 100%">
                <td colspan="4" class="grayline2" style="width: 100%">
                    <asp:GridView ID="grdusers" runat="server" CellPadding="4" EnableViewState="false"
                        AutoGenerateColumns="False" PageSize="15" Width="1218px" GridLines="None"  
                        HeaderStyle-BackColor="DarkGray">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="20px">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="MasterCheckBox" Text="All" runat="server" onclick="SelectAllUsersUnderModifications(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="SingleCheckbox" runat="server" />
                                    <asp:TextBox type="hidden" ID="userid" runat="server" Style="display: none" Text='<%# Bind("userid") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Literal ID="ltrUserName" runat="server" Text='<%# Bind("UserFullName") %>'></asp:Literal>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Login Name" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:Literal ID="ltrLoginName" runat="server" Text='<%# Bind("loginname") %>'></asp:Literal>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Locked" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:Literal ID="ltrAccountStatus" runat="server" Text='<%# Bind("status") %>'></asp:Literal>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <%--<EditRowStyle BackColor="#999999" />--%>
                        
<HeaderStyle BackColor="DarkGray"></HeaderStyle>

                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table cellspacing="0">
        <tr>
            <td nowrap="">
                <asp:Button runat="server" name="$action^setvalue%26node-ids%269%26content%26Unlock"
                    ID="btnUnlock" Text="Unlock Selected User(s)" class="button" OnClientClick="return LockUnlockSelUsers();"
                    OnClick="btnUnlock_Click" />
                <asp:Button runat="server" name="$action^setvalue%26node-ids%269%26content%26Lock"
                    Text="Lock Selected User(s)" class="button" ID="btnLock" OnClientClick="return LockUnlockSelUsers();"
                    OnClick="btnLock_Click" />
            </td>
            <td>
                <asp:TextBox runat="server" type="hidden" Style="display: none" name="$node^1111"
                    value="" ID="SelectedUserIds"></asp:TextBox>
            </td>
        </tr>
    </table>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
