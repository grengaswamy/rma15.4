﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserPerm.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.UserPerm" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Information</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="javascript" src="../../Scripts/security.js"></script>

    <script type="text/javascript" language="javascript" src="../../Scripts/SecurityEntities.js"></script>

    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <%--vkumar258 - RMA_6037- Starts--%>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
</head>
<body onload="pageLoaded();SetTreePostedByValue('UpdateUserPermEntity');document.getElementById('hdPostback').value='';" style="margin-left:0px">
    <form id="frmData" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdUserLoginName']"
        Style="display: none" ID="hdUserLoginName" />
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/@name" Style="display: none"
        ID="formname" Text="useraccessperm" />
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdDsnIdForCurrentlyLoadedMGrps']"
        Style="display: none" ID="hdDsnIdForCurrentlyLoadedMGrps" />
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdSelectedModuleGrpId']"
        Style="display: none" ID="hdSelectedModuleGrpId" />
    <%
        if (hdPostback.Text == "true")
        {
    %>

    <script type="text/javascript" language="javascript">        PostbackHandlerForUpdates(null, "<%=dbid.Text %>");</script>

    <%
        }
        
    %>
    <div>
        <table border="0">
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td class="ctrlgroup" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td />
                        </tr>
                        <tr>
                            <td class="ctrlgroup" colspan="2">
                                User Access Permissions for selected Data Source
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Login Name
                            </td>
                            <td>
                                <asp:TextBox runat="server" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='loginname']"
                                    ID="loginname" onchange="setDataChanged(true)" onkeyup="SupressLength(this)" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password
                            </td>
                            <td>
                                <asp:TextBox TextMode="Password" type="password" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='password']"
                                    ID="password" size="30" onchange="setDataChanged(true)" onkeyup="SupressLength(this)"
                                    MaxLength="25" ReadOnly="false" onClick="SelectAll(this.id);" onkeydown="return keyDown(event);" onfocus="SelectAll(this.id);" autocomplete="off"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divPassPolicy" runat="server" visible="false">
                                    <table border="0">
                                        <tr>
                                            <td>
                                                <%--<script language="JavaScript" src="">{var i;}</script>--%>
                                                <asp:Button class="button" runat="server" ID="btnUpdate" OnClientClick="return ShowPasswordScreen('UserPerm')"
                                                    Text="Update password" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Force user to change password on next login
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'forcepwdchange' ]"
                                                    ID="forcepwdchange" onclick="setDataChanged(true)" />
                                                <%-- <input type="text" name="$node^55" value="" style="display: none"
                                        cancelledvalue="" id="forcepwdchange_mirror">--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Permission Expires
                            </td>
                            <td>
                                <asp:CheckBox type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='permexpires']"
                                    ID="permexpires" onclick="setDataChanged(true);PermissionExpires();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Expiration Date
                            </td>
                            <td>
                                <asp:TextBox runat="server" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='expdate']"
                                    ID="expdate" onchange="setDataChanged(true)" onblur="DateLostFocus(this.id);"
                                    disabled="" />
                                    <%--vkumar258 - RMA_6037- Starts--%>
                                    <%--<asp:Button runat="server" class="button" ID="expdatebtn" Text="..." disabled="true" /><script
                                    type="text/javascript">
                                                                                                                           Zapatec.Calendar.setup(
											{
											    inputField: "expdate",
											    ifFormat: "%m/%d/%Y",
											    button: "expdatebtn"
											}
											);
                                </script>--%>
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#expdate").datepicker({
                                                showOn: "button",
                                                buttonImage: "../../Images/calendar.gif",
                                                //buttonImageOnly: true,
                                                showOtherMonths: true,
                                                selectOtherMonths: true,
                                                changeYear: true
                                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                        });
                                    </script>
                                    <%--vkumar258 - RMA_6037- End--%>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%-- Text replaced by label control so that visibiltiy can be controlled.--%>
                                <asp:Label ID="lbFilePath" runat="server" Text="Use Document Path, overrides Data Source Document Path"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" size="30" MaxLength="255" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='filepath']"
                                    ID="filepath" onchange="setDataChanged(true)" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ctrlgroup" colspan="2">
                                Assign User To Group
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ModuleGroups
                            </td>
                            <td>
                                <asp:DropDownList runat="server" onchange="SetModuleGroupId();" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='lstModuleGroups']/@value"
                                    ID="lstModuleGroups" itemsetref="/Instance/Document/form/group/displaycolumn/control[@name='lstModuleGroups']" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ctrlgroup" colspan="2">
                                Allow access in these time intervals
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="50%">
                        <tr onclick="return AllowAccess(this);">
                            <td width="8%">
                                <asp:CheckBox ID="Mon" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Mon']"
                                    onchange="ApplyBool(this);" onclick="setDataChanged(true)" Checked="true" controltype="timeline" />
                            </td>
                            <td width="10%">
                                Mon
                            </td>
                            <td width="15%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Mon']/@value1"
                                    onchange="setDataChanged(true)" ID="ip1Mon" onblur="TimeLostFocus(this.id)" />
                            </td>
                            <td width="7%">
                                To
                            </td>
                            <td width="30%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Mon']/@value2"
                                    onchange="setDataChanged(true)" ID="ip2Mon" onblur="TimeLostFocus(this.id)" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="50%">
                        <tr onclick="return AllowAccess(this);">
                            <td width="8%">
                                <asp:CheckBox ID="Tue" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Tue']"
                                    onchange="ApplyBool(this);" onclick="setDataChanged(true)" Checked="true" controltype="timeline" />
                            </td>
                            <td width="10%">
                                Tue
                            </td>
                            <td width="15%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Tue']/@value1"
                                    onchange="setDataChanged(true)" ID="ip1Tue" onblur="TimeLostFocus(this.id)" />
                            </td>
                            <td width="7%">
                                To
                            </td>
                            <td width="30%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Tue']/@value2"
                                    onchange="setDataChanged(true)" ID="ip2Tue" onblur="TimeLostFocus(this.id)" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="50%">
                        <tr onclick="return AllowAccess(this);">
                            <td width="8%">
                                <asp:CheckBox ID="Wed" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Wed']"
                                    onchange="ApplyBool(this);" onclick="setDataChanged(true)" Checked="true" controltype="timeline" />
                            </td>
                            <td width="10%">
                                Wed
                            </td>
                            <td width="15%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Wed']/@value1"
                                    onchange="setDataChanged(true)" ID="ip1Wed" onblur="TimeLostFocus(this.id)" />
                            </td>
                            <td width="7%">
                                To
                            </td>
                            <td width="30%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Wed']/@value2"
                                    onchange="setDataChanged(true)" ID="ip2Wed" onblur="TimeLostFocus(this.id)" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="50%">
                        <tr onclick="return AllowAccess(this);">
                            <td width="8%">
                                <asp:CheckBox ID="Thu" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Thu']"
                                    onchange="ApplyBool(this);" onclick="setDataChanged(true)" Checked="true" controltype="timeline" />
                            </td>
                            <td width="10%">
                                Thu
                            </td>
                            <td width="15%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Thu']/@value1"
                                    onchange="setDataChanged(true)" ID="ip1Thu" onblur="TimeLostFocus(this.id)" />
                            </td>
                            <td width="7%">
                                To
                            </td>
                            <td width="30%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Thu']/@value2"
                                    onchange="setDataChanged(true)" ID="ip2Thu" onblur="TimeLostFocus(this.id)" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="50%">
                        <tr onclick="return AllowAccess(this);">
                            <td width="8%">
                                <asp:CheckBox ID="Fri" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Fri']"
                                    onchange="ApplyBool(this);" onclick="setDataChanged(true)" Checked="true" controltype="timeline" />
                            </td>
                            <td width="10%">
                                Fri
                            </td>
                            <td width="15%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Fri']/@value1"
                                    onchange="setDataChanged(true)" ID="ip1Fri" onblur="TimeLostFocus(this.id)" />
                            </td>
                            <td width="7%">
                                To
                            </td>
                            <td width="30%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Fri']/@value2"
                                    onchange="setDataChanged(true)" ID="ip2Fri" onblur="TimeLostFocus(this.id)" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="50%">
                        <tr onclick="return AllowAccess(this);">
                            <td width="8%">
                                <asp:CheckBox ID="Sat" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sat']"
                                    onchange="ApplyBool(this);" onclick="setDataChanged(true)" Checked="true" controltype="timeline" />
                            </td>
                            <td width="10%">
                                Sat
                            </td>
                            <td width="15%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sat']/@value1"
                                    onchange="setDataChanged(true)" ID="ip1Sat" onblur="TimeLostFocus(this.id)" />
                            </td>
                            <td width="7%">
                                To
                            </td>
                            <td width="30%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sat']/@value2"
                                    onchange="setDataChanged(true)" ID="ip2Sat" onblur="TimeLostFocus(this.id)" />
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="50%">
                        <tr onclick="return AllowAccess(this);">
                            <td width="8%">
                                <asp:CheckBox ID="Sun" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sun']"
                                    onchange="ApplyBool(this);" onclick="setDataChanged(true)" Checked="true" controltype="timeline" />
                            </td>
                            <td width="10%">
                                Sun
                            </td>
                            <td width="15%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sun']/@value1"
                                    onchange="setDataChanged(true)" ID="ip1Sun" onblur="TimeLostFocus(this.id)" />
                            </td>
                            <td width="7%">
                                To
                            </td>
                            <td width="30%">
                                <asp:TextBox runat="server" Text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sun']/@value2"
                                    onchange="setDataChanged(true)" ID="ip2Sun" onblur="TimeLostFocus(this.id)" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='dbid']"
                                        Style="display: none" ID="dbid" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='dsnid']"
                                        Style="display: none" ID="dsnid" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='userid']"
                                        Style="display: none" ID="userid" />
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdPostback']"
                                        Text="false" Style="display: none" ID="hdPostback" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdPwdFrmUserScreen']"
                                        Style="display: none" ID="hdPwdFrmUserScreen" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdSave']"
                                        Style="display: none" ID="hdSave" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='expdate']/@disabled"
                                        Style="display: none" ID="expdatedisable" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Mon']/@controltype"
                                        Style="display: none" ID="hdctrl1" Text="timeline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Tue']/@controltype"
                                        Style="display: none" ID="hdctrl2" Text="timeline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Wed']/@controltype"
                                        Style="display: none" ID="hdctrl3" Text="timeline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Thu']/@controltype"
                                        Style="display: none" ID="hdctrl4" Text="timeline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Fri']/@controltype"
                                        Style="display: none" ID="hdctrl5" Text="timeline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Sat']/@controltype"
                                        Style="display: none" ID="hdctrl6" Text="timeline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='Sun']/@controltype"
                                        Style="display: none" ID="hdctrl7" Text="timeline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='password']/@readonly"
                                        Style="display: none" ID="pwdreadonly" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='IsPasswordPolicyEnable']"
                                        Style="display: none" ID="tbIsPasswordPolicyEnable" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td valign="top">
                </td>
            </tr>
        </table>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
