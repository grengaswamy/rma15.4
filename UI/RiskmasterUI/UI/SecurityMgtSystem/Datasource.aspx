﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Datasource.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.Datasource" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script type="text/javascript" src="../../Scripts/drift.js"></script>

    <script type="text/javascript" src="../../Scripts/security.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <script language="javascript" type="text/javascript">
        function setpwdchanged() {
            var pwdchanged = document.getElementById('systempwdchanged');
            pwdchanged.value = true;
        }
    </script>
</head>
<body class="10pt" onload="pageLoaded();">
    <form id="frmData" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="errtextheader">
    </div>
    <table border="0">
        <tbody>
            <tr>
              <td class="ctrlgroup" colspan="2">
                                Data Source Settings
              </td>
            </tr>
            <tr>
                <td>
                    System Login Name
                </td>
                <td>
                    <asp:TextBox runat="server" name="$node^26" size="30" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'systemloginname' ]"
                        ID="systemloginname" onchange="setDataChanged(true);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    System Password
                </td>
                <td>
                    <asp:TextBox TextMode="Password" type="password" name="$node^32" runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'systempassword' ]"
                        size="30" onchange="setDataChanged(true);setpwdchanged();" ID="systempassword" MaxLength="16" onClick="SelectAll(this.id);" onkeydown="return keyDown(event);" onfocus="SelectAll(this.id);" autocomplete="off"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnReset" runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'btnReset' ]/@value"
                        name="btnReset" Style="display: none" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox name="$node^43" runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'hdBypassDupDsnNameChk' ]"
                        Style="display: none" ID="hdBypassDupDsnNameChk"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" name="$node^47" Style="display: none" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'orgsecflag' ]"
                        ID="orgsecflag"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup" colspan="2">
                    Document Path for this Data Source
                </td>
                <td>
                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/@name" Style="display: none"
                        ID="formname" Text="datasourceinfo">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:RadioButton GroupName="optservers" Text="File Server" value="0" ID="optservers1"
                        onclick="return OnSelectServersOptions(this)" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'optservers' ]/@value"
                        runat="server" />
                    <asp:RadioButton onclick="return OnSelectServersOptions(this)" GroupName="optservers"
                        Text="Db server" value="1" ID="optservers2" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'optservers' ]/@value"
                        runat="server" />
                    <asp:RadioButton GroupName="optservers" Text="S3 Bucket" value="2" ID="optS3server"
                        onclick="return OnSelectServersOptions(this)" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'optservers' ]/@value"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Path/Data Source
                </td>
                <td>
                    <asp:TextBox name="$node^71" runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'docpath' ]"
                        size="30" MaxLength="255" onchange="setDataChanged(true);" ID="ipdocpath"></asp:TextBox>
                    <asp:TextBox name="$node^71" ID="disbaledocpath" runat="server" Visible="false" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'docpath' ]/@disabled"></asp:TextBox>
                    <asp:Button runat="server" class="button" ID="btdocpath" OnClientClick="return OpenDBWizardSetDocPath()"
                        Text="..." />
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup" colspan="2">
                    License Information
                </td>
            </tr>
            <tr>
                <td>
                    # Workstations
                </td>
                <td>
                    <asp:TextBox name="$node^82" size="30" ID="workstations" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'workstations' ]"
                        onchange="setDataChanged(true);" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox name="$node^89" Style="display: none" runat="server" ID="licupdatedate"
                        RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'licupdatedate' ]"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Module Access Security Enabled
                </td>
                <td>
                    <asp:CheckBox name="$node^95" runat="server" value="True" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'chkModuleSec' ]"
                        ID="chkModuleSec" />
                    
                </td>
            </tr>
            <tr>
                <asp:TextBox runat="server" name="$node^100" value="" Style="display: none" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'hdFormName' ]"
                    ID="hdFormName">
                </asp:TextBox>
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdAction']"
                        Text="SecurityAdaptor.GetXMLData" Style="display: none" ID="hdAction">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdSave']"
                        Style="display: none" ID="hdSave">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <asp:TextBox runat="server" Style="display: none" name="$node^114" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'dsnid' ]"
                    ID="dsnid">
                </asp:TextBox>
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'dsname' ]"
                        name="$node^119" Style="display: none" ID="dsname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" name="$node^123" Style="display: none" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'servername' ]"
                        ID="servername">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" name="$node^127" Style="display: none" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'drivername' ]"
                        ID="drivername"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" Style="display: none" name="$node^131" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'password' ]"
                        ID="password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" name="$node^135" Style="display: none" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'loginname' ]"
                        ID="loginname">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" name="$node^139" Style="display: none" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'dbname' ]"
                        ID="dbname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox runat="server" name="$node^143" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'connectionstring' ]"
                        Style="display: none" ID="connectionstring"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <%--  <asp:TextBox Style="display: none" runat="server" name="$node^147" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'hdUserLoginName' ]"
                                        ID="hdUserLoginName"></asp:TextBox>--%>
                </td>
            </tr>
        </tbody>
    </table>
    <table>
        <tbody>
            <tr>
                <td>
                    <asp:Button name="btnUpdate" runat="server" Text="Update License info" OnClientClick="return UpdateLicenses()" />
                    <asp:Button runat="server" name="btnEdit" Text="Edit this Datasource" OnClientClick="return OpenDBWizardForEdit()" />
                </td>
            </tr>
        </tbody>
    </table>
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
        <asp:TextBox runat="server" Style="display: none" name="$node^131" RMXref="/Instance/Document/form/group/displaycolumn/control[ @name = 'systempwdchanged' ]"
                        ID="systempwdchanged"></asp:TextBox>
         <asp:TextBox runat="server" style="display: none" Text="" name="s3bucketname" ID="txts3bucketname"></asp:TextBox>
    </form>
</body>
</html>
