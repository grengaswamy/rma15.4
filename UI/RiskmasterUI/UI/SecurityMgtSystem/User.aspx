﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Riskmaster.UI.SecurityMgtSystem.User" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">


    <title>User Information</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
 
    <script type="text/javascript" language="javascript" src="../../Scripts/security.js"></script>
     <script type="text/javascript" language="javascript" src="../../Scripts/SecurityEntities.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/WaitDialog.js"></script>
    <%--vkumar258 - RMA_6037- Starts--%>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>
</head>
<body onload="pageLoaded();" style="margin-left:0px">
    <form id="frmData" runat="server" method="post">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdUserLoginName']" style="display:none" id="hdUserLoginName"/>
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/@name" style="display:none" id="formname" Text="userinfo"/>
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdDsnIdForCurrentlyLoadedMGrps']" style="display:none" id="hdDsnIdForCurrentlyLoadedMGrps"/>
    <asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdSelectedModuleGrpId']" style="display:none" id="hdSelectedModuleGrpId"/>
   
    <%
        if (hdPostback.Text == "true")
        {
            %>
            <script type="text/javascript" language="javascript">PostbackHandlerForUpdates(null,"<%=dbid.Text %>");</script>
            
            <%
        }
        if (dsnid.Text == "" || dsnid.Text == "0")
        {
       %>
    <div>
        <table border="0">
    
     <tr>
      <td>
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colspan="2"></td>
         </tr>
         <tr>
          <td></td>
          <td>
          
          </td>
         </tr>
         <tr>
          <td class="ctrlgroup" colspan="2">User Settings</td>
         </tr>
         <tr>
          <td>Last Name</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='lastname']" size="30" id="lastname" onchange="setDataChanged(true);"/></td>
         </tr>
         <tr>
          <td>First Name</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='firstname']" size="30" id="firstname" onchange="setDataChanged(true);"/></td>
         </tr>
         <tr>
          <td>Title</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='title']" size="30" id="title" onchange="setDataChanged(true);"/></td>
         </tr>
         <tr>
          <td>Language</td>
          <td><asp:DropDownList runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='language']/@value" id="language" onchange="setDataChanged(true);" itemsetref="/Instance/Document/form/group/displaycolumn/control[ @name = 'language' ]"/>
            </td>
         </tr>
         <tr>
          <td>Company</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='company']" size="30" id="company" onchange="setDataChanged(true);"/></td>
         </tr>
         <tr>
          <td>Address</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='address1']" size="30" id="address1" onchange="setDataChanged(true);"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='address2']" size="30" id="address2" onchange="setDataChanged(true);"/></td>
         </tr>
         <tr>
          <td>City</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='city']" size="30" id="city" onchange="setDataChanged(true);"/></td>
         </tr>
          <tr>
          <td>State</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='state']" size="30" id="TextBox1" MaxLength="30" onchange="setDataChanged(true);" /></td>
          
         </tr>
         <tr>
          <td>Zip</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='zip']" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);" id="zip"/></td>
         </tr>
         <tr>
          <td>Country</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='country']" size="30" id="country" onchange="setDataChanged(true);"/></td>
         </tr>
         <tr>
          <td>Office Phone</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='officephone']" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="officephone"/></td>
         </tr>
         <tr>
          <td>Home Phone</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='homephone']" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" id="homephone"/></td>
         </tr>
         <tr>
          <td>E-Mail</td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='email']" size="30" id="email" onchange="setDataChanged(true);" onblur="emailLostFocus(this)"/></td>
         </tr>
         <tr>
          <td>Manager</td>
          <td><asp:DropDownList runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='manager']/@value" id="manager" onchange="setDataChanged(true);" itemsetref="/Instance/Document/form/group/displaycolumn/control[ @name = 'manager' ]"/>
          </td>
         </tr>
         <tr>
          <td></td>
          <td><input type="reset" name="btnReset" style="display:none"/></td>
         </tr>
         <tr>
          <td>Permit to access Security Mgt. System</td>
          <td><asp:checkbox type="checkbox" runat="server" appearance="full" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='SMSAccess']" id="SMSAccess" onchange="ApplyBool(this);" onclick="IsAdminUser()"/></td>
         </tr>
           <%--skhare7 :MITS 22374--%>
         <tr>
          <td>Permit to access User Privileges Setup</td>
          <td><asp:checkbox type="checkbox" runat="server" appearance="full" 
                  rmxref="/Instance/Document/form/group/displaycolumn/control[@name='UserPrAccess']" 
                  id="UserPrAccess" onchange="ApplyBool(this);" onclick="" /></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='hdIsSMSAdminUser']" style="display:none" id="hdIsSMSAdminUser"/></td>
         </tr>
         <tr>
          <td class="ctrlgroup" colspan="2">Allow User To Login To</td>
         </tr>
         <tr>
          <td>Datasources</td>
          <td><asp:DropDownList runat="server" onchange="SetDsnId();" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='lstDatasources']/@value" id="lstDatasources" itemsetref="/Instance/Document/form/group/displaycolumn/control[@name='lstDatasources']"/>
         </td>
         </tr>
         <tr>
          <td class="ctrlgroup" colspan="2">User Already Allowed To Login To</td>
         </tr>
         <tr>
          <td></td>
          <td>
              <%--akaushik5 Added Enabled = "false" property for RMA-8231--%> 
              <asp:ListBox id="lstShowDatasources" runat="server" size="5" style="background-color: silver;" readonly="true" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='lstShowDatasources']/@value" itemsetref="/Instance/Document/form/group/displaycolumn/control[@name='lstShowDatasources']" Enabled="false"/>
            </td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdFormName']" style="display:none" id="hdFormName"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdAction']" Text="SecurityAdaptor.GetXMLData" style="display:none" id="hdAction"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='userpermid']" style="display:none" id="userpermid"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='userid']" style="display:none" id="userid"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdSave']" style="display:none" id="hdSave"/></td>
         </tr>
         
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='dsnid']" style="display:none" id="dsnid"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='dsname']" style="display:none" id="dsname"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdNLSCode']" style="display:none" id="hdNLSCode"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdPostback']" Text="false" style="display:none" id="hdPostback"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='dbid']" style="display:none" id="dbid"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdPwdFrmUserScreen']" style="display:none" id="hdPwdFrmUserScreen"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdEnhSecurity']" style="display:none" id="hdEnhSecurity"/></td>
         </tr>
         <tr>
          <td></td>
          <td><asp:TextBox runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdIsDomainAuthenticationEnabled']" style="display:none" id="hdIsDomainAuthenticationEnabled"/></td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>
          <td></td>
         </tr>
        </tbody>
       </table>
      </td>
      <td valign="top"></td>
     </tr>
    
   </table>
    </div>
    <%
        }
        else
        {
            %>
            <div>
                 <table border="0">
    
     <tr>
      <td>
       <table border="0">
    
         <tr>
          <td class="ctrlgroup" colspan="2"></td>
         </tr>
         <tr>
          <td></td>
          <td/>
         </tr>
         <tr>
          <td class="ctrlgroup" colspan="2">User Access Permissions for selected Data Source</td>
         </tr>
         <tr>
          <td>Login Name</td>
          <td><asp:TextBox runat="server" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='loginname']" id="loginname" onchange="setDataChanged(true)" onkeyup="SupressLength(this)"/></td>
         </tr>
         <tr>
          <td>Password</td>
          <td><asp:TextBox TextMode="Password" type="password" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='password']" id="password" size="30" onchange="setDataChanged(true)" onkeyup="SupressLength(this)" maxlength="25" autocomplete="off" /></td>
         </tr>
         <tr>
          <td>Permission Expires</td>
          <td>
          <asp:checkbox type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='permexpires']" id="permexpires" onchange="ApplyBool(this);" onclick="PermissionExpires();"/></td>
         </tr>
         <tr>
          <td>Expiration Date</td>
          <td>
          <asp:TextBox runat="server" size="30" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='expdate']" id="expdate" onchange="setDataChanged(true)" onblur="DateLostFocus(this.id);" />
                                    <%--vkumar258 - RMA_6037- Starts--%>
                                    <%-- <asp:button runat="server" class="button" id="expdatebtn" Text="..." disabled="true"/>
             <script type="text/javascript">
											Zapatec.Calendar.setup(
											{
												inputField : "expdate",
												ifFormat : "%m/%d/%Y",
												button : "expdatebtn"
											}
											);
										</script>--%>
                                    <script type="text/javascript">
                                        $(function () {
                                            $("#expdate").datepicker({
                                                showOn: "button",
                                                buttonImage: "../../Images/calendar.gif",
                                                //buttonImageOnly: true,
                                                showOtherMonths: true,
                                                selectOtherMonths: true,
                                                changeYear: true
                                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                                        });
                                    </script>
                                    <%--vkumar258 - RMA_6037- End--%>
                                </td>
                            </tr>
                            <tr>
                                <td>Use Document Path, overrides Data Source Document Path</td>
                                <td>
                                    <asp:textbox runat="server" size="30" maxlength="255" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='filepath']" id="filepath" onchange="setDataChanged(true)" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ctrlgroup" colspan="2">Assign User To Group</td>
                            </tr>
                            <tr>
                                <td>ModuleGroups</td>
                                <td>
                                    <asp:dropdownlist runat="server" onchange="SetModuleGroupId();" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='lstModuleGroups']/@value" id="lstModuleGroups" itemsetref="/Instance/Document/form/group/displaycolumn/control[@name='lstModuleGroups']" disabled="true" />
                                    <%--<option value="" onclick="" id="">Please select the Module Group.</option></select>--%></td>
                            </tr>
                            <tr>
                                <td class="ctrlgroup" colspan="2">Allow access in these time intervals</td>
                            </tr>
                            <table border="0" width="50%">
                                <tr onclick="return AllowAccess(this);">
                                    <td width="8%">
                                        <asp:checkbox id="Mon" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Mon']" onchange="ApplyBool(this);" onclick="setDataChanged(true)" checked="true" />
                                    </td>
                                    <td width="10%">Mon</td>
                                    <td width="15%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Mon']/@value1" onchange="setDataChanged(true)" id="ip1Mon" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                    <td width="7%">To</td>
                                    <td width="30%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Mon']/@value2" onchange="setDataChanged(true)" id="ip2Mon" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" width="50%">
                                <tr onclick="return AllowAccess(this);">
                                    <td width="8%">
                                        <asp:checkbox id="Tue" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Tue']" onchange="ApplyBool(this);" onclick="setDataChanged(true)" checked="true" />
                                    </td>
                                    <td width="10%">Tue</td>
                                    <td width="15%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Tue']/@value1" onchange="setDataChanged(true)" id="ip1Tue" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                    <td width="7%">To</td>
                                    <td width="30%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Tue']/@value2" onchange="setDataChanged(true)" id="ip2Tue" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" width="50%">
                                <tr onclick="return AllowAccess(this);">
                                    <td width="8%">
                                        <asp:checkbox id="Wed" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Wed']" onchange="ApplyBool(this);" onclick="setDataChanged(true)" checked="true" />
                                    </td>
                                    <td width="10%">Wed</td>
                                    <td width="15%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Wed']/@value1" onchange="setDataChanged(true)" id="ip1Wed" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                    <td width="7%">To</td>
                                    <td width="30%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Wed']/@value2" onchange="setDataChanged(true)" id="ip2Wed" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" width="50%">
                                <tr onclick="return AllowAccess(this);">
                                    <td width="8%">
                                        <asp:checkbox id="Thu" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Thu']" onchange="ApplyBool(this);" onclick="setDataChanged(true)" checked="true" />
                                    </td>
                                    <td width="10%">Thu</td>
                                    <td width="15%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Thu']/@value1" onchange="setDataChanged(true)" id="ip1Thu" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                    <td width="7%">To</td>
                                    <td width="30%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Thu']/@value2" onchange="setDataChanged(true)" id="ip2Thu" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" width="50%">
                                <tr onclick="return AllowAccess(this);">
                                    <td width="8%">
                                        <asp:checkbox id="Fri" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Fri']" onchange="ApplyBool(this);" onclick="setDataChanged(true)" checked="true" />
                                    </td>
                                    <td width="10%">Fri</td>
                                    <td width="15%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Fri']/@value1" onchange="setDataChanged(true)" id="ip1Fri" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                    <td width="7%">To</td>
                                    <td width="30%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Fri']/@value2" onchange="setDataChanged(true)" id="ip2Fri" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                </tr>
                            </table>

                            <table border="0" width="50%">
                                <tr onclick="return AllowAccess(this);">
                                    <td width="8%">
                                        <asp:checkbox id="Sat" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sat']" onchange="ApplyBool(this);" onclick="setDataChanged(true)" checked="true" />
                                    </td>
                                    <td width="10%">Sat</td>
                                    <td width="15%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sat']/@value1" onchange="setDataChanged(true)" id="ip1Sat" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                    <td width="7%">To</td>
                                    <td width="30%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sat']/@value2" onchange="setDataChanged(true)" id="ip2Sat" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                </tr>
                            </table>
                            <table border="0" width="50%">
                                <tr onclick="return AllowAccess(this);">
                                    <td width="8%">
                                        <asp:checkbox id="Sun" type="checkbox" runat="server" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sun']" onchange="ApplyBool(this);" onclick="setDataChanged(true)" checked="true" />
                                    </td>
                                    <td width="10%">Sun</td>
                                    <td width="15%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sun']/@value1" onchange="setDataChanged(true)" id="ip1Sun" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                    <td width="7%">To</td>
                                    <td width="30%">
                                        <asp:textbox runat="server" text="12:00 AM" size="10" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Sun']/@value2" onchange="setDataChanged(true)" id="ip2Sun" onblur="TimeLostFocus(this.id)" />
                                    </td>
                                </tr>
                            </table>

                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td valign="top"></td>
                </tr>

            </table>
        </div>
        <%
        }
        %>
        <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="Loading" />

    </form>
</body>
</html>
