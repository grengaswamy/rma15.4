﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcordForms.aspx.cs" Inherits="Riskmaster.UI.UI.FROI.AcordForms" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>FROI Forms</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" src="../../Scripts/FROI.js" language="javascript"></script>
</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div id="divScroll" style="overflow:auto;height:100%">
        <table width="100%">
						<tr>
							<td class="msgheader" bgcolor="#D5CDA4">ACORD Forms</td>
						</tr>
						<tr>
						    <%if (hdFormCount.Text != "0")
            {%>
									<td class="bold1">Following ACORD Forms are available:</td><%}
            else
            {%>
								
								<td class="bold1"><BR></BR>There are no ACORD forms available.<BR></BR></td><%} %>
							
						</tr>
					</table>
					<div id="divForms" class="divScroll">
						<table>
							<%int i = 0; foreach (XElement item in result)
         {%>
			
								<tr>
									<td>
										<% 
                                            lnkFrm.ID = "lnkFrm" + i;
                                            lnkFrm.Text = item.Attribute("FormNameAndTitle").Value;
                                            lnkFrm.OnClientClick = "return ShowAcordPdf('" + item.Attribute("FormId").Value + "'," + "'" + lnkFrm.Text + "'" + ");";
                                            // rrachev JIRA RMA-1063 Begin
                                            lnkFrm.Attributes.Add("href", "#");
                                            lnkFrm.Attributes.Add("oncontextmenu", "return false;");
                                            // rrachev JIRA RMA-1063 End
              %>
              <asp:LinkButton runat="server" id="lnkFrm"></asp:LinkButton> 
										
										
									</td>
								</tr>
								<%} %>
							
						</table>
					</div>
				<br />
					<table>
						<tbody>
							<tr>					
								
								<td>
							    	<%  string sLOB = Request.QueryString["lob"];
                btnBackToClaimTop.OnClientClick = "return Back(document.getElementById('ClaimId').value ," + sLOB + ");";
							    	     %>
							    	<asp:button runat="server" CssClass="button" id="btnBackToClaimTop" text="Back To Claim"/>
							    	
								</td>
							</tr>
						</tbody> 
					</table>
					
					<!--End Updated by Tanuj -->
					<asp:textbox runat="server" rmxref="/Instance/Document/ACORDForms/ClaimId" id="ClaimId" style="display:none"/>										
					<asp:textbox runat="server" rmxref="/Instance/Document/ACORDForms/ClaimNumber" id="ClaimNumber" style="display:none"/>					
					<asp:textbox runat="server" rmxref="/Instance/Document/ACORDForms/FormId" id="FormId" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/ACORDForms/Name" id="Name" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/ACORDForms/Title" id="Title" style="display:none"/>
					
					<asp:textbox runat="server" rmxref="/Instance/Document/ACORDForms/FormCount" id="hdFormCount" style="display:none"/>
					
    </div>
    </form>
</body>
</html>