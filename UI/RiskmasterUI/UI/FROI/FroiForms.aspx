﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FroiForms.aspx.cs" Inherits="Riskmaster.UI.UI.FROI.FroiForms" EnableViewStateMac="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FROI Forms</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" src="../../Scripts/FROI.js" language="javascript"></script>
</head>
<body>
    <form id="frmData" name="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div id="divScroll" style="overflow:auto;height:100%">
        <table width="100%">
						<tr>
							<td class="msgheader" bgcolor="#D5CDA4">FROI Forms</td>
						</tr>
						<tr>
						    <%if (hdFormCount.Text != "0")
            {%>
									<td class="bold1">Following FROI forms are available:</td><%}
            else
            {%>
								
								<td class="bold1"><BR></BR>There are no FROI forms available.<BR></BR></td><%} %>
							
						</tr>
					</table>
					<div id="divForms" class="divScroll">
						<table>
							<%int i = 0; foreach (XElement item in result)
         {%>
			
								<tr>
									<td>
										<% 
                                            lnkFrm.ID = "lnkFrm" + i;
                                            lnkFrm.Text = item.Attribute("FormName").Value;
                                            lnkFrm.OnClientClick = "return ShowFroiPdf('" + item.Attribute("FormId").Value + "');";
                                            // rrachev JIRA RMA-1063 Begin
                                            lnkFrm.Attributes.Add("href", "#");
                                            lnkFrm.Attributes.Add("oncontextmenu", "return false;");
                                            // rrachev JIRA RMA-1063 End
                                            
              %>
              <asp:LinkButton runat="server" id="lnkFrm"></asp:LinkButton> 
										
										
									</td>
								</tr>
								<%} %>
							
						</table>
					</div>
					<br>
					</br>
					<table>
						<tbody>
							<tr>
							<td>
							    	<asp:button runat="server" CssClass="button" id="btnBack" OnClientClick="history.back();return false;" text="Back To Preparer Information"/>
							    	
								</td>
								
								<td>
							    	<asp:Button runat="server" Text="Back To Claim" CssClass="button" id="btnBackToClaimTop" onclick="btnBackToClaimTop_Click" />
							    	
								</td>
							</tr>
						</tbody>
					</table>
					
					<!--End Updated by Tanuj -->
					<asp:textbox runat="server" rmxref="/Instance/Document/FROIForms/ClaimId" id="ClaimId" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/FROIForms/FormId" id="FormId" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/FROIForms/AttachForm" id="AttachForm" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/FROIForms/Name" id="Name" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/FROIForms/Title" id="Title" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/FROIForms/Phone" id="Phone" style="display:none"/>
					<asp:textbox runat="server" rmxref="/Instance/Document/FROIForms/FormCount" id="hdFormCount" style="display:none"/>
					
    </div>
    </form>
</body>
</html>
