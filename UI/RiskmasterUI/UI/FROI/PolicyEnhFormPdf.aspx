﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyEnhFormPdf.aspx.cs" Inherits="Riskmaster.UI.UI.FROI.PolicyEnhFormPdf" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <link href= "../../Content/rmnet.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" language="JavaScript" src="../../Scripts/FROI.js"></script>
    
</head>
<body>
    <form id="form1" runat="server">
       <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                    <asp:HiddenField id="PolicyId" runat="server"/>
					<asp:HiddenField id="FormId" runat="server"/>
					<asp:HiddenField id="StorageId" runat="server"/>
					<asp:HiddenField id="InsuredId" runat="server"/>
					<asp:HiddenField id="Name" runat="server"/>
					<asp:HiddenField id="Title" runat="server"/>
					<asp:HiddenField id="Phone" runat="server"/>
					<asp:HiddenField ID="RequestHost" runat="server" />
					
    </form>
   
</body>
</html>

