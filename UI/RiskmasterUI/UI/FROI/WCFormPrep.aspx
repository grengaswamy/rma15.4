﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WCFormPrep.aspx.cs" Inherits="Riskmaster.UI.UI.FROI.WCFormPrep" EnableViewStateMac="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Juris Preparer Information</title>
    <link href= "../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript" src="../../Scripts/FROI.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
</head>
<body onload="CheckForSkip('WCForm');">
    <form id="frmData" name="frmData" runat="server">
    <div>
         <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <br/>
				<br/>
				<p align="center">
					<h3 align="center">Preparer Information</h3>
				</p>
    				<table border="0" align="center">
						<tr>
							<td>Preparer Name:</td>
							<td>
								<asp:TextBox rmxref="/Instance/Document/JurisPreparer/preparerName" id="Name" size="30" runat="server" />
							</td>
						</tr>
						<tr>
							<td>Preparer Title:</td>
							<td>
								<asp:TextBox rmxref="/Instance/Document/JurisPreparer/preparerTitle" id="Title" size="30" runat="server" />
							</td>
						</tr>
						<tr>
							<td>Preparer Phone:</td>
							<td>
								<asp:TextBox rmxref="/Instance/Document/JurisPreparer/preparerPhone" id="Phone" size="30" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);" runat="server" />
							</td>
						</tr>
						<%--<tr>
							<td>Attach Form to Claim:</td>
							<td>
								<asp:CheckBox runat="server" rmxref="/Instance/Document/PreparerInfo/AttachForm" ID="AttachForm_chk" rmxignoreset="true"/>
							</td>
						</tr>--%>
						<tr>
							<td colspan="2" align="center">
								<br/>
								<asp:Button ID="btnViewForm" runat="server" Text="View  Form" CssClass="button" OnClientClick="return OpenWCForms();"/>
								
						        <asp:Button runat="server" Text="Back To Claim" CssClass="button" OnClientClick="return Back(document.getElementById('ClaimId').value,'claimwc');" id="btnBackToClaimTop" />
						    </td>
						</tr>
					</table>
					<asp:TextBox runat="server" style="display:none" ID="ClaimId" rmxignoreset="true" rmxignoreget="true"></asp:TextBox>
					<%--<asp:TextBox runat="server" rmxref="/Instance/Document/PreparerInfo/SkipJurisPreparerPrompt" style="display:none" ID="SkipJurisPreparerPrompt" rmxignoreset="true"></asp:TextBox>--%>
					<asp:TextBox runat="server" style="display:none" ID="OrgHierarchy" rmxignoreget="true" rmxignoreset="true" />
					<asp:TextBox runat="server" style="display:none" ID="Folder" rmxignoreget="true" rmxignoreset="true" />
					<asp:TextBox runat="server" style="display:none" ID="skipprompt" rmxref="/Instance/Document/JurisPreparer/rememberInfo" />
					<asp:HiddenField runat="server" ID="Action" />
					
  
    </div>
    </form>
</body>
</html>
