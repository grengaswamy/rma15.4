﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PdfSave.aspx.cs" Inherits="Riskmaster.UI.UI.FROI.PdfSave" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link href= "../../Content/rmnet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
        <asp:Button ID="btnClose" runat="server" OnClientClick="window.close();return false;" text="Close" Visible="false"/>
    </div>
    </form>
</body>
</html>
