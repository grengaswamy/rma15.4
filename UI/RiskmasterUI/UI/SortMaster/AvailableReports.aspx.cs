﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.ServiceModel;
using System.Configuration;
using System.Data;
using System.IO;

namespace Riskmaster.UI.SortMaster
{
    public partial class AvailableReports : System.Web.UI.Page
    {
        //Add by kuladeep
        // hidden field used to maintain this data for both report list and load of single report
        //string sMenuRequestType = string.Empty; 
        protected void Page_Load(object sender, EventArgs e)
        {
            string sReturn = string.Empty;
            try
            {
                if (Request.QueryString["smpage"] != null)
                {   
                    // hidden field used to maintain this data for both report list and load of single report
                    //sMenuRequestType = Request.QueryString["smpage"];
                    hdnMenuRequestType.Value = Request.QueryString["smpage"];
                }
                if (Request.QueryString["IsDownload"] != null && Request.QueryString["ReportId"] != null)
                {
                    sReturn = AppHelper.CallCWSService(GetMessageTemplate(Request.QueryString["ReportId"].ToString()).ToString());
                    sReturn = sReturn.Replace("&lt;", "<");
                    sReturn = sReturn.Replace("&gt;", ">");
                    sReturn = sReturn.Replace('\"', '"');
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sReturn);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.UTF8);
                    doc.SelectSingleNode("//report").WriteTo(writer);
                    writer.Flush();
                    Response.Clear();
                    byte[] byteArray = stream.ToArray();
                    Response.AppendHeader("Content-Disposition", "attachment;filename=OshaReport.xml");
                    Response.AppendHeader("Content-Length", byteArray.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.BinaryWrite(byteArray);
                    writer.Close();
                    Response.Flush();
                }
                if (!IsPostBack)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    string strReturn = string.Empty; 
                    XElement oMessageElement = LoadTemplate();
                    CallService(oMessageElement, ref oFDMPageDom, ref strReturn);

                    if (oFDMPageDom.SelectSingleNode("//ReportDetails") != null)
                    {
                        DataSet aDataSet = new DataSet();
                        aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("ReportDetails")[0].OuterXml));
                        RptrReport.DataSource = aDataSet.Tables["Report"];
                        RptrReport.DataBind();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }
        private XElement GetMessageTemplate(String ReportValue)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.GetXMLByReportID</Function></Call>");
            sXml = sXml.Append("<Document><ParmaList><ReportID>" + ReportValue + "</ReportID><MenuRequestType>" + hdnMenuRequestType.Value + "</MenuRequestType></ParmaList></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        private XElement LoadTemplate()
        {
//            XElement oTemplate = XElement.Parse(@"
//                 <Message>
//                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
//                     <Call>
//                      <Function>SortMasterAdaptor.GetReportDetails</Function> 
//                      </Call>
//                      <Document>
//                        <ReportName></ReportName>
//                        <ReportDesc></ReportDesc>
//                        <ReportId></ReportId>
//                      </Document>
//                </Message>
//
//
//            ");
//            return oTemplate;

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SortMasterAdaptor.GetReportDetails</Function></Call>");
            sXml = sXml.Append("<Document><ParmaList><ReportName></ReportName>");
            sXml = sXml.Append("<ReportDesc></ReportDesc>");
            sXml = sXml.Append("<ReportId></ReportId>");
            sXml = sXml.Append("<MenuRequestType>" + hdnMenuRequestType.Value + "</MenuRequestType>");
            sXml = sXml.Append("</ParmaList></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void RptrReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label reportLbl = ((Label)e.Item.FindControl("lblReprtDesc"));
                reportLbl.Text = reportLbl.Text + "�";

                if (hdnMenuRequestType.Value != string.Empty)
                {
                    if (hdnMenuRequestType.Value == "smreportsel")
                    {
                        HyperLink HypLnkReportName = ((HyperLink)e.Item.FindControl("HypLnkReprtName"));
                        HypLnkReportName.NavigateUrl = HypLnkReportName.NavigateUrl + "&smpage=" + hdnMenuRequestType.Value;
                        hdnhyperRprt.Value = "true";
                    }
                    else
                    {
                        ((HyperLink)e.Item.FindControl("HypLnkReprtName")).Visible = false;
                    }
                }
            }
        }
    }
}