﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailJob.aspx.cs" Inherits="Riskmaster.UI.SortMaster.EmailJob" %>
<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
     <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
      <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">          { var i; }  </script>
        <script type="text/javascript">
            function pageLoaded() {
                var i;
                for (i = 0; i < document.frmData.length; i++) {

                    if ((document.frmData.item(i).type == "text") || (document.frmData.item(i).type == "select-one") || (document.frmData.item(i).type == "textarea")) {
                        document.frmData.item(i).focus();
                        break;
                    }
                }
            }

            function Validate() {
                var SendTo = false;
                
                if (($('#lstUsers :selected').val() == undefined) && ($("#txtEmailTo").val() == "")) {
                    alert("Please select the E-Mail Recipients from the list or enter them manually.");
                    return false;
                }
            }

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2">
                    <uc1:errorcontrol id="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <table class="singleborder" align="center" style="height:500px">
            <tr><td class="ctrlgroup">
                <asp:Label ID="lblEmailReport" runat="server" Text="E-Mail Report Jobs"></asp:Label></td></tr>
	        <tr>
		        <td nowrap="" class="datatd">
                    <asp:Label ID="lblSelectUser" runat="server" Text="Please select the Users to E-Mail to:" Font-Bold="true"></asp:Label>
		        </td>
	        </tr>
		    <tr>
			    <td class="datatd" align="center"><p align="center">
                <asp:ListBox ID="lstUsers" runat="server" SelectionMode="Multiple" Width="60%" style="vertical-align:central" rmxref="/Instance/Document/UserDetails/User" ItemSetRef="/Instance/Document/UserDetails/User"></asp:ListBox>
			    <span class="smallnote">
                    <asp:Label ID="lblSelectMultipleUser" runat="server" Text="(Press the <b>Ctrl</b> key to select multiple recipients)"></asp:Label>
			    </span>
                </p></td>
		    </tr>
	        <tr>
		        <td class="datatd"><asp:Label ID="lblAlsoEmailTo" runat="server" Text="Also E-Mail to:"></asp:Label><br />
                    <asp:TextBox ID="txtEmailTo" runat="server" rmxref="/Instance/Document/UserDetails/EmailCC" ItemSetRef="/Instance/Document/UserDetails/EmailCC"></asp:TextBox>
		        <br/>
		        <span class="smallnote"><asp:Label ID="lblEmailSemiColon" runat="server" Text="(Separate multiple E-Mail addresses with the semicolon)"></asp:Label></span>
		        </td>
	        </tr>
	        <tr>
		        <td class="datatd"><asp:Label ID="lblMessage" runat="server" Text="Message for the Recipients:"></asp:Label><br />
                    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" rmxref="/Instance/Document/UserDetails/Message" ItemSetRef="/Instance/Document/UserDetails/Message"></asp:TextBox>
		        </td>
	        </tr>
	        <tr>
		        <td align="center" nowrap="">
                    <asp:Button ID="btnEmail" runat="server" Text="  E-Mail  " OnClick="btnEmail_Click" OnClientClick="return Validate()" />
                    <asp:Button ID="btnCancel" runat="server" Text=" Cancel " OnClick="btnCancel_Click" />
		        </td>
	        </tr>
        </table>
    </div>
    </form>
</body>
</html>
