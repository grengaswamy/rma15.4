﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
namespace Riskmaster.UI.SortMaster
{
    public partial class SM_Redirect : System.Web.UI.Page
    {
        string SMMessage = "<Message> " +
         "<Authorization>f9cad182-2c64-4035-92fb-93dc2d013a8b</Authorization>" +
          " <Call>" +
             "<Function>SortMasterAdaptor.GetLoginDetails</Function>" +
           "</Call>" +
           "<Document><SortMaster></SortMaster> </Document>"+
             "</Message>";
        private XmlDocument Model = null;
        private string GetPagePath(string mapname)
        {
            string sPath="";
            switch (mapname)
            {
                case "rptfields":
                    sPath = "/wizardsm/rptfields.asp?MODE=0";
                    break;
                case "openreport":
                    sPath = "/wizardsm/smopenreport.asp?1=1";
                    break; 
                case "postreport":
                    sPath = "/wizardsm/postreport.asp?1=1";
                    break; 
                  case "smpostreport":
                    sPath = "/wizardsm/smpostreport.asp?1=1";
                    break;
                  case "smreportsel":
                    sPath = "/wizardsm/smreportsel.asp?1=1";
                    break;
                  case "smrepqueue":
                    sPath = "/wizardsm/smrepqueue.asp?1=1";
                    break;
                  case "smreportdelete":
                    sPath = "/wizardsm/smreportdelete.asp?1=1";
                    break;
                  case "schedulereport":
                    sPath = "/wizardsm/schedulereport.asp?1=1";
                    break;
                  case "schedulelist":
                    sPath = "/wizardsm/schedulelist.asp?1=1";
                    break;
                  case "postosha300":
                    sPath = "/wizardsm/prefabpostreport.asp?MODE=0&reporttype=1";
                    break;
                  case "postosha300A":
                    sPath = "/wizardsm/prefabpostreport.asp?MODE=0&reporttype=3";
                    break;
                  case "postosha301":
                    sPath = "/wizardsm/prefabpostreport.asp?MODE=0&reporttype=4";
                    break;
                  case "postoshasharps":
                    sPath = "/wizardsm/prefabpostreport.asp?MODE=0&reporttype=5";
                    break;
                  case "adminreportqueue":
                    sPath = "/wizardsm/adminreportqueue.asp?MODE=0";
                    break;
                  case "adminschedulelist":
                    sPath = "/wizardsm/adminschedulelist.asp?MODE=0";
                    break;
                    //Added for MITS-16085:View All Available Reports Functionality
                  case "adminavailablereports": 
                    sPath = "/wizardsm/smreportfetch.asp?MODE=0";
                    break;
                  //Added for MITS-16085:View All Available Reports Functionality
                default:
                   break; 
            }
            return sPath;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           string SMUrl = "";
            try
            {
                string sReturn = AppHelper.CallCWSService(SMMessage.ToString());
                Model = new XmlDocument();
                Model.LoadXml(sReturn);
                if (Model.SelectSingleNode("//UserLoginDetails/SMNetServer") != null)
                {
                    if (Model.SelectSingleNode("//UserLoginDetails/SMNetServer").InnerText.IndexOf("http") == -1)
                    {
                        SMUrl = "http://" + Model.SelectSingleNode("//UserLoginDetails/SMNetServer").InnerText;
                    }
                    else
                    {
                        SMUrl = Model.SelectSingleNode("//UserLoginDetails/SMNetServer").InnerText; ;
                    }
                    SMUrl = SMUrl + GetPagePath(AppHelper.GetQueryStringValue("smpage"));
                    SMUrl = SMUrl+AppendExtraQueryString();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
            
            }
            if (SMUrl != "")
            {

                //Raman Bhatia: We do need to close MDI screen loaded sign hence we have to redirect at client and not server
                sUrl.Value = SMUrl;
                //   Response.Redirect(SMUrl);
            }
        }

        private string AppendExtraQueryString()
        {
            string sQueryString = "";
            //Change by kuladeep for SortMaster mits:32377 Start
            //string sParams = "SMKeyId|UserId|DSNId|ConnectionString|DocStorageType|DocStoragePath|GroupId|DSNName|FirstName|LastName|LoginName|Email|AdminRights|DBStatus";
            string sParams = "SMKeyId";
            //Change by kuladeep for SortMaster mits:32377 End
            foreach (string param in sParams.Split("|".ToCharArray()[0]))
            {
                sQueryString = sQueryString + "&" + param + "=" + Server.UrlEncode(AppHelper.GetValueFromDOM(Model, param));
            }
            string strSessionID = AppHelper.ReadCookieValue("SessionId");
            //return sQueryString + "&strSessionID=" + Server.UrlEncode(strSessionID) ;
            //Change by kuladeep for SortMaster mits:32377
            //return sQueryString + "&strID=" + Server.UrlEncode(strSessionID);//Deb Pen Testing
            return sQueryString;

        }


    }
    
}
