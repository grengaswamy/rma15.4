﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScheduleSelect.aspx.cs" Inherits="Riskmaster.UI.UI.SortMaster.ScheduleSelect" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script type="text/javascript">
        function onScheduleSelect() {
            var iTaskType = document.forms[0].hdnOshaTaskTypeId.value;
            var sSubTaskNameText = document.forms[0].ddlOSHAReportName.options[document.forms[0].ddlOSHAReportName.selectedIndex].text;
            var iSubTaskValue = document.forms[0].ddlOSHAReportName.options[document.forms[0].ddlOSHAReportName.selectedIndex].value;
            if (document.forms[0].lstScheduleOSHAReport.options[document.forms[0].lstScheduleOSHAReport.selectedIndex].value != "0") {
                iScheduleType = document.forms[0].lstScheduleOSHAReport.options[document.forms[0].lstScheduleOSHAReport.selectedIndex].value;
                switch (iScheduleType) {
                    case '7':
                        //rkulavil :RMA-12898 starts
                        //window.location = "/RiskmasterUI/UI/SortMaster/ScheduleReport.aspx?TaskType=" + iTaskType + "&amp;TaskTypetxt=OSHA Reports&amp;ScheduleTypetxt=Daily&amp;sSubTaskNameText=" + sSubTaskNameText + "&amp;sSubTaskValue=" + iSubTaskValue + "&amp;ScheduleType=" + iScheduleType;
                        window.location = "/RiskmasterUI/UI/SortMaster/ScheduleReport.aspx?TaskType=" + iTaskType + "&TaskTypetxt=OSHA Reports&ScheduleTypetxt=Daily&sSubTaskNameText=" + sSubTaskNameText + "&sSubTaskValue=" + iSubTaskValue + "&ScheduleType=" + iScheduleType;
                        //rkulavil :RMA-12898 ends
                        break;
                    case '8':
                        //rkulavil :RMA-12898 starts
                        //window.location = "/RiskmasterUI/UI/SortMaster/ScheduleReport.aspx?TaskType=" + iTaskType + "&amp;TaskTypetxt=OSHA Reports&amp;ScheduleTypetxt=Monthly&amp;sSubTaskNameText=" + sSubTaskNameText + "&amp;sSubTaskValue=" + iSubTaskValue + "&amp;ScheduleType=" + iScheduleType;
                        window.location = "/RiskmasterUI/UI/SortMaster/ScheduleReport.aspx?TaskType=" + iTaskType + "&TaskTypetxt=OSHA Reports&ScheduleTypetxt=Monthly&sSubTaskNameText=" + sSubTaskNameText + "&sSubTaskValue=" + iSubTaskValue + "&ScheduleType=" + iScheduleType;
                        //rkulavil :RMA-12898 ends
                        break;
                    default:
                        break;
                }
            }
        }
        function onScheduleTypeChange()
        {
            $('#lblScheduleType').hide();
            $('#lstScheduleOSHAReport').hide();
            $('#lblReportName').show();
            $('#ddlOSHAReportName').show();
            $('#lblSchlType').show();
            $('#lblSchlTypeText').show();
            document.getElementById('lblSchlTypeText').innerHTML = "Run Report " + document.forms[0].lstScheduleOSHAReport.options[document.forms[0].lstScheduleOSHAReport.selectedIndex].text;
        }
    </script>
     <script type="text/javascript">
         $(document).ready(function () {
             $('#lblReportName').hide();
             $('#ddlOSHAReportName').hide();
             $('#lblSchlType').hide();
             $('#lblSchlTypeText').hide();
         });
    </script>
</head>
<body onload="parent.pleaseWait('stop');">
    <form id="form1" runat="server">
    <div>
        <table cellspacing="0" cellpadding="0" border="0" width="50%">
            <tr>
                    <td colspan="2"><uc3:ErrorControl ID="ErrorControl1" runat="server" /></td>
            </tr>
        </table>
        <table width="100%">
			<tr>
				<td class="msgheader">Schedule a Report</td>
			</tr>
		</table>
        <table>
         <tr>
            <td align="left">
                <asp:Label ID="lblScheduleType" runat="server" Text="Please select schedule type you would like to create:"></asp:Label>
                <asp:DropDownList ID="lstScheduleOSHAReport" runat="server" onchange="onScheduleTypeChange()">
                    <asp:ListItem value ="" Text=""></asp:ListItem>
                    <asp:ListItem value ="7" Text="Daily"></asp:ListItem>
                    <asp:ListItem value ="8" Text="Monthly"></asp:ListItem>
                </asp:DropDownList>
            </td>
             <td />
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblSchlType" runat="server" Text="Schedule Type:"></asp:Label>
                <asp:Label ID="lblSchlTypeText" runat="server" Font-Bold="true"></asp:Label>
            </td>
            <td />
        </tr>
        <tr height="10">
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblReportName" runat="server" Text="Select Report that you would like to Schedule:" ></asp:Label>
               <asp:DropDownList ID="ddlOSHAReportName" runat="server" DataTextField="ReportName" DataValueField="ReportId" onchange="onScheduleSelect()">
               </asp:DropDownList>         
            </td>
            <td />
        </tr>
        </table>
        
        <asp:HiddenField ID="hdnOshaTaskTypeId" runat="server" />
    </div>
    </form>
</body>
</html>
