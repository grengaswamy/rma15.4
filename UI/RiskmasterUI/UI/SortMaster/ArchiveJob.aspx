﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArchiveJob.aspx.cs" Inherits="Riskmaster.UI.SortMaster.ArchiveJob" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script type="text/javascript">
        function Validate()
        {
            var boolRet = false;
            if (document.forms[0].lstfolders.selectedIndex < 0)
                alert("Please select the Folder.");
            else
                boolRet = true;

            return boolRet;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2">
                    <uc1:errorcontrol id="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <table class="singleborder" align="center">
            <tr>
                <td class="ctrlgroup">Archive Report Jobs</td>
            </tr>
            <tr>
		        <td nowrap="" class="datatd"><b>Please select the folder to archive job(s) to:</b></td>
	        </tr>
            <tr>
		        <td class="datatd">
                    <p align="center">
                        <asp:ListBox ID="lstfolders" runat="server" Size="15" Height="250px" Width="100px" rmxref="/Instance/Document/FolderDetails/Folder" ItemSetRef="/Instance/Document/FolderDetails/Folder">
                          <%--  <asp:ListItem>Recycle Bin</asp:ListItem>--%>
                        </asp:ListBox>
                        <%--<select name="lstFolders" size="15"><%=objDocHandler.getFoldersAsOption(0)%></select>--%>
                    </p>
		        </td>
	        </tr>
            <tr>
		        <td align="center" nowrap="">
                    <asp:Button ID="btnarchive" runat="server" Text="  Archive  " class="button" OnClientClick="return Validate()" OnClick="btnArchive_Click"/>
                    <asp:Button ID="btncancel" runat="server" Text=" Cancel " class="button" PostBackUrl="~/UI/SortMaster/JobQueue.aspx"/>
		        </td>
	        </tr>
        </table>
    </div>
    </form>
</body>
</html>
