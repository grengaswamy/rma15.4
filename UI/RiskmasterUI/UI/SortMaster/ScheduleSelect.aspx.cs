﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.IO;

namespace Riskmaster.UI.UI.SortMaster
{
    public partial class ScheduleSelect : System.Web.UI.Page
    {
        string sMenuRequestType = string.Empty;//Add by kualdeep
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["smpage"] != null)
                {
                    sMenuRequestType = Request.QueryString["smpage"];
                }

                string bReturnStatus = string.Empty;
                XElement XmlTemplate = null;
                string sReturn = string.Empty;
                XmlDocument oFDMPageDom = new XmlDocument();
                string strReturn = string.Empty;

                XmlTemplate = GetMessageTemplate();
                CallService(XmlTemplate, ref oFDMPageDom, ref strReturn);
                if (oFDMPageDom.SelectSingleNode("//ReportDetails") != null)
                {
                    DataSet aDataSet = new DataSet();
                    aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("ReportDetails")[0].OuterXml));
                    ddlOSHAReportName.DataSource = aDataSet.Tables["Report"];
                    ddlOSHAReportName.DataBind();
                    ddlOSHAReportName.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                    hdnOshaTaskTypeId.Value = oFDMPageDom.SelectSingleNode("//ReportDetails/OSHATaskTypeID").InnerText;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.GetReportDetails</Function></Call><Document><ParmaList><TaskType/><TaskTypeText /><SubTaskType/><SubTaskTypeText/><ScheduleType /><ScheduleTypeText /> ");
            sXml = sXml.Append("<MenuRequestType>" + sMenuRequestType + "</MenuRequestType>");
            sXml = sXml.Append("</ParmaList></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}