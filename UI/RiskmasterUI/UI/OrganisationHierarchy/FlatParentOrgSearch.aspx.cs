﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using System.Collections;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.OrganisationHierarchy
{
    public partial class FlatParentOrgSearch : System.Web.UI.Page
    {
        
        private string sReturn = "";
        public IEnumerable result = null;
        public XElement rootElement = null;  
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    hdEntityTableId.Value = AppHelper.GetQueryStringValue("entitytableid");
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

                }
            }
        }

        protected void btnSearch_click(object sender, EventArgs e)
        {
            XmlDocument oResponse = null;
            try
            {

                oResponse = new XmlDocument();

                //Preparing XML to send to service
                XElement oMessageElement = GetMessageTemplate();

                XElement oElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/OrgXml/child_eid");
                if (oElement != null)
                {
                    oElement.Value = hdEntityTableId.Value;
                }
                oElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/OrgXml/strSearch");
                if (oElement != null)
                {
                    oElement.Value = strSearch.Text;
                }
                //Calling Service to get all PreBinded Data 
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                oResponse.LoadXml(sReturn);
                XmlNode oInstanceNode = oResponse.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                oResponse.LoadXml(oInstanceNode.OuterXml);

                rootElement = XElement.Parse(oResponse.OuterXml);
                result = from c in rootElement.XPathSelectElements("//ReturnXml/OutputResult/xml/option")
                         select c;
                hdCountItems.Value = rootElement.XPathSelectElements("//ReturnXml/OutputResult/xml/option").Count().ToString();
                if (hdCountItems.Value == "1")
                {
                    XElement oEle = rootElement.XPathSelectElement("//ReturnXml/OutputResult/xml/option");
                    selectedname.Value = oEle.Value;
                    selectedvalue.Value = oEle.Attribute("value").Value;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>32187afd-8341-426a-9fa8-1e88fd92f8a8</Authorization> 
             <Call>
              <Function>OrgHierarchyAdaptor.GetParentList</Function> 
              </Call>
              <Document>
                  <OrgHierarchy>
                    <OrgXml>
                      <child_eid></child_eid>
                      <strSearch></strSearch>
                    </OrgXml>
                  </OrgHierarchy>
                </Document>
              </Message>


            ");

            return oTemplate;
        }
    }
}
