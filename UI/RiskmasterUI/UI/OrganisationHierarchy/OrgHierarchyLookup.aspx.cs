﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Views.OrganisationHierarchy
{
    public partial class OrgHierarchyLookup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Removing the "orgcaption" from Org Tree as it normally holds the Org Level which is not displayed on this page
            Control oControl = OT.FindControl("orgcaption");
            if(oControl != null)
            {
                oControl.Visible = false;
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            //Populating the "Search Selected Level" dropdownlist
            Control oControl = OT.FindControl("cmb_dlevel");
            if (oControl != null)
            {
                Control oSearchControl = OrgSearch.FindControl("cmb_slevel");
                if (((DropDownList)oSearchControl).Items.Count == 0)
                {
                    foreach (ListItem iItem in ((DropDownList)oControl).Items)
                    {
                        ((DropDownList)oSearchControl).Items.Add(iItem);
                    }
                }
                else
                {
                    //DETERMINE IF SEARCH LEVEL IS SET FROM WEBSERVICE THEN SET IT ACCORDINGLY
                    
                    oControl = OT.FindControl("hd_slevel");
                    if (oControl != null && ((HiddenField)oControl).Value != String.Empty)
                    {
                        ((DropDownList)oSearchControl).SelectedValue = ((HiddenField)oControl).Value;
                    }
                    else
                    {
                        ((DropDownList)oSearchControl).SelectedIndex = ((DropDownList)oControl).SelectedIndex;
                    }
                    
                }
            }
           
        }
    }
}
