﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlatParentOrgSearch.aspx.cs" Inherits="Riskmaster.UI.UI.OrganisationHierarchy.FlatParentOrgSearch" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Quick Lookup Results</title>
    <script type="text/javascript" language="javascript" src="../../Scripts/org.js"></script>
    <script type="text/javascript" language="javascript">
        function handleLoad() {
            if (document.getElementById('hdCountItems').value == "1") {
                FormSubmit(document.getElementById('selectedvalue').value, document.getElementById('selectedname').value);
            }
        }
             function FormSubmit(p_sId , p_sName)
              {
                window.opener.document.forms[0].parentoperation.value = p_sName;
                window.opener.document.forms[0].parenteid.value = p_sId;
                window.opener.setDataChanged(true);
                window.close();
              }
              //gdass2 MITS 23964 Searching for a new parent entity returns Object Reference error
              function ResultsKeyDown(e) 
              {
                  var key;

                  if (window.event)
                      key = window.event.keyCode;     //IE
                  else
                      key = e.which;     //firefox

                  if (key == 13) {
                    
                      var btn = document.getElementById('btnSearch');
                      if (btn != null) { 
                          btn.click();
                          event.keyCode = 0
                      }
                  }
               //gdass2 
//              if(event.keyCode==13)
//              {
//              FormSubmit();
//              }
              return true;
              }
    </script>
</head>
<body onload="handleLoad();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div>
    <table align="center" width="100%" border="0">
                <tr>
                  <td width="100%" valign="middle" align="center">
                    <b>String to search</b>
                   <asp:TextBox runat="server" ID="strSearch" onKeyPress="ResultsKeyDown(event)"></asp:TextBox>
                   <asp:Button runat="server" ID="btnSearch" Text="Search" class="button" OnClick="btnSearch_click"/>
                  </td>
                </tr>
                <% if (Page.IsPostBack == true)
                   {
                       if (hdCountItems.Value == "0")
                       {
                       %>
                    <tr>
                      <td width="100%" valign="middle" align="center">
                        <font face="Verdana, Arial" size="4">
                          <b>Zero records match the criteria</b>
                        </font>
                      </td>
                    </tr>
                    <%}
                       else
                       {%>
                  
                    <tr>
                      <td width="100%" valign="middle" align="center">
                        <font face="Verdana, Arial" size="2">
                          <b>
                            Record(s) matching the criteria
                              <%if (strSearch.Text != "")
                                {%>
                                '<%=strSearch.Text%>'
                              <%} %> 
                              
                         </b>
                        </font> 
                      </td>
                      </tr>
                    <tr>
                      <td>
                        &#160;
                      </td>
                    </tr>
                      
                    
						   <%int i = 0; foreach (XElement item in result)
                            {
                                string rowclass = "";
                                if ((i % 2) == 1) rowclass = "datatd1";
                                else rowclass = "datatd";
                                i++;
                                %>
                                
                          <tr>                          
                                 <td class="<%=rowclass%>"><%     
                                    lnkEntity.ID = "lnkEntity" + i;
                                    lnkEntity.Text = item.Value;
                                    lnkEntity.OnClientClick = "return FormSubmit(" + item.Attribute("value").Value + ",'" + item.Value + "');";%>
                              
                             <asp:LinkButton runat="server" id="lnkEntity"></asp:LinkButton>  
									
								</td>
								                                        
                          </tr>                          
                            <% }%>    
                                     
                 
                    <tr>
                      <td>
                        &#160;
                      </td>
                    </tr>
                  <%}
                   }%>
                  
             </table>
             
     
    
        <asp:HiddenField runat="server" ID="hdCountItems" />
        <asp:HiddenField runat="server" ID="hdEntityTableId" />
        <asp:HiddenField runat="server" ID="selectedvalue" />
        <asp:HiddenField runat="server" ID="selectedname" />
        
    </div>
    </form>
</body>
</html>
