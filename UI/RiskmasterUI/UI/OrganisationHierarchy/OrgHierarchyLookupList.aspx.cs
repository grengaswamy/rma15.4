﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.Shared.Controls;
//using Riskmaster.UI.CodesService;


namespace Riskmaster.UI.OrganisationHierarchy
{
    public partial class OrgHierarchyLookupList : System.Web.UI.Page
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        DropDownList oDropDownControl = null;
        //MITS 16771- Pankaj 5/29/09
        TextBox otxtsorg = null;
        TextBox otxtcity = null;
        TextBox otxtstate_codelookup_cid = null;
        CodeLookUp otxtstate = null;
        TextBox otxtzip = null;
        protected string m_sInitialEntityLevel = "";//abansal23 on 05/14/2009 for 
        string m_sInitialSearchText = "";
        string m_sInitialCityText = "";
        string m_sInitialStateText = "";
        string m_sInitialStateDesc = "";
        string m_sInitialZipText = "";
        //static bool m_queryconsidered;//abansal23: MITS 16165 on 05/06/2009
        

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                OrgSearch.btnListHandler += new Riskmaster.UI.Shared.Controls.OrgSearchCriteria.OnListButtonClick(OrgSearch_btnListHandler);
                OrgSearch.btnResetHandler += new Riskmaster.UI.Shared.Controls.OrgSearchCriteria.OnResetButtonClick(OrgSearch_btnResetHandler);
                OrgSearch.btnTreeHandler += new Riskmaster.UI.Shared.Controls.OrgSearchCriteria.OnTreeButtonClick(OrgSearch_btnTreeHandler);
                oDropDownControl = (DropDownList)OrgSearch.FindControl("cmb_slevel");
                otxtsorg = (TextBox)OrgSearch.FindControl("txtsorg");
                //MITS 16771- Pankaj 5/29/09
                otxtcity = (TextBox)OrgSearch.FindControl("txtcity");
                otxtstate = (CodeLookUp)OrgSearch.FindControl("txtstate");
                otxtzip = (TextBox)OrgSearch.FindControl("txtzip");
                oFDMPageDom = new XmlDocument();
                string sReturn = "";
                OrgSearch.FindControl("chkTreeNode").Visible = false;
                OrgSearch.FindControl("lblSelectedBranch").Visible = false;

                if (!Page.IsPostBack)
                {
                    //m_queryconsidered = false;//abansal23: MITS 16165 on 05/06/2009
                    //Preparing XML to send to service
                    XElement oMessageElement = GetMessageTemplate();

                    //Modify XML if needed
                    ModifyTemplate(oMessageElement, "", "", "", "", "", "");

                    //Calling Service to get all PreBinded Data 
                    CommonFunctions.CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                    //Binding Error Control
                    ErrorControl.errorDom = sReturn;

                    //Update DataList Bindings to populate data
                    UpdateListBindings();

                    UpdateListStyle();


                    //Update dropdownlist for search level

                    ListItem lstItem = new ListItem("Client", "1005");
                    oDropDownControl.Items.Add(lstItem);
                    lstItem = new ListItem("Company", "1006");
                    oDropDownControl.Items.Add(lstItem);
                    lstItem = new ListItem("Operation", "1007");
                    oDropDownControl.Items.Add(lstItem);
                    lstItem = new ListItem("Region", "1008");
                    oDropDownControl.Items.Add(lstItem);
                    lstItem = new ListItem("Division", "1009");
                    oDropDownControl.Items.Add(lstItem);
                    lstItem = new ListItem("Location", "1010");
                    oDropDownControl.Items.Add(lstItem);
                    lstItem = new ListItem("Facility", "1011");
                    oDropDownControl.Items.Add(lstItem);
                    lstItem = new ListItem("Department", "1012");
                    oDropDownControl.Items.Add(lstItem);

                    oDropDownControl.SelectedValue = m_sInitialEntityLevel;
                    otxtsorg.Text = m_sInitialSearchText;
                    //MITS 16771- Pankaj 5/29/09
                    otxtcity.Text = m_sInitialCityText;
                    otxtstate.CodeId = m_sInitialStateText;
                    otxtstate.CodeText = m_sInitialStateDesc;
                    otxtzip.Text = m_sInitialZipText;
                }
              
     

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        /// <summary>
        /// Handles Tree View button click for the search criteria user control
        /// </summary>
        /// <param name="strValue"></param>
        void OrgSearch_btnTreeHandler(string strValue)
        {
            //MITS 16771- Pankaj 5/29/09
            string txtcity = ((TextBox)OrgSearch.FindControl("txtcity")).Text;
            string txtzip = ((TextBox)OrgSearch.FindControl("txtzip")).Text;
            string txtstate = ((CodeLookUp)OrgSearch.FindControl("txtstate")).CodeId;
            string txtstatetext = ((CodeLookUp)OrgSearch.FindControl("txtstate")).CodeText;

            if (txtcity != "")
                txtcity = txtcity.Replace("&", "%26");
            if (txtzip != "")
                txtzip = txtzip.Replace("&", "%26");

            if(ViewState["lob"] == null || String.IsNullOrEmpty(ViewState["lob"].ToString()))
            {
                Response.Redirect("OrgHierarchyMaintenance.aspx?level=" + CommonFunctions.GetCatFromLevel(Conversion.ConvertStrToLong(((DropDownList)OrgSearch.FindControl("cmb_slevel")).SelectedValue)) +
                    "&txtorg=" + ((TextBox)OrgSearch.FindControl("txtsorg")).Text.Replace("&", "%26") +
                    "&txtcity=" + txtcity +
                    "&txtstate=" + txtstate +
                    "&txtstatetext=" + txtstatetext +
                    "&txtzip=" + txtzip);
            }
            else
            {
                Response.Redirect("OrgHierarchyLookup.aspx?lob=" + ViewState["lob"].ToString() + "&level=" + CommonFunctions.GetCatFromLevel(Conversion.ConvertStrToLong(((DropDownList)OrgSearch.FindControl("cmb_slevel")).SelectedValue)) +
                    "&txtorg=" + ((TextBox)OrgSearch.FindControl("txtsorg")).Text.Replace("&", "%26") +
                    "&txtcity=" + txtcity +
                    "&txtstate=" + txtstate +
                    "&txtstatetext=" + txtstatetext +
                    "&txtzip=" + txtzip);
            }
        }

        
        /// <summary>
        /// Handles Reset button click for the search criteria user control
        /// </summary>
        /// <param name="strValue"></param>
        void OrgSearch_btnResetHandler(string strValue)
        {
            if (ViewState["lob"] == null || String.IsNullOrEmpty(ViewState["lob"].ToString()))
            {
                Response.Redirect("OrgHierarchyMaintenance.aspx");
            }
            else
            {
                Response.Redirect("OrgHierarchyLookup.aspx?lob=" + ViewState["lob"].ToString());
            }
        }
        /// <summary>
        /// Handles List View button click for the search criteria user control
        /// </summary>
        /// <param name="strValue"></param>
        void OrgSearch_btnListHandler(string strValue)
        {
            //Preparing XML to send to service
            XElement oMessageElement = GetMessageTemplate();

            //Modify XML if needed
            ModifyTemplate(oMessageElement, oDropDownControl.SelectedValue, (otxtsorg.Text), otxtcity.Text, otxtstate.CodeId.ToString(), otxtstate.CodeTextValue, otxtzip.Text);//abansal23: MITS 16165 on 05/05/2009

            //Calling Service to get all PreBinded Data 
            CommonFunctions.CallService(oMessageElement, ref oFDMPageDom);

            //Update DataList Bindings to populate data
            UpdateListBindings();

            UpdateListStyle();

            
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                  <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
                  <Call>
                    <Function>OrgHierarchyAdaptor.GetEnitySearch</Function> 
                  </Call>
                  <Document>
                   <OrgHierarchy>
                   <EntityId>1012</EntityId> 
                   <Lookup></Lookup> 
                    <City/>
                    <State/>
                    <Zip/>
                    </OrgHierarchy>
                  </Document>
                 </Message>

            ");
            return oTemplate;
        }
        /// <summary>
        /// This function would be used to modify the template as and when necessary
        /// </summary>
        /// <param name="oMessageElement"></param>
        private void ModifyTemplate(XElement oMessageElement, string p_sEntityLevel, string p_sLookup, string p_sCity, string p_lState, string p_sStateText, string p_sZip)
        {
            if (String.IsNullOrEmpty(p_sEntityLevel))
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["EntityId"]))
                {
                    string sEntityLevel = HttpContext.Current.Request.QueryString["EntityId"].ToString();
                    XElement oEntityLevelElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/EntityId");
                    if (oEntityLevelElement != null)
                    {
                        oEntityLevelElement.Value = sEntityLevel;
                    }
                    m_sInitialEntityLevel = sEntityLevel;
                    
                }
            }
            else
            {
                XElement oEntityLevelElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/EntityId");
                if (oEntityLevelElement != null)
                {
                    oEntityLevelElement.Value = p_sEntityLevel;
                }
                m_sInitialEntityLevel = p_sEntityLevel;
            }


            if ((String.IsNullOrEmpty(p_sLookup)) && (!Page.IsPostBack))//(!m_queryconsidered))//abansal23: MITS 16165 on 05/06/2009
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["Lookup"]))
                {
                    string sLookup = HttpContext.Current.Request.QueryString["Lookup"].ToString();
                    XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Lookup");

                    //Ijha: MITS 28258 : Passing special characters 
                    if (sLookup != null)
                    {
                        sLookup = sLookup.Replace("%26", "&");
                    } // ijha : end
                    if (oLookupElement != null)
                    {
                        oLookupElement.Value = sLookup;// +'%';
                    }
                    m_sInitialSearchText = sLookup;
                    //m_queryconsidered = true;//abansal23: MITS 16165 on 05/06/2009
                }
            }
            else
            {
                XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Lookup");
                if (oLookupElement != null)
                {
                    oLookupElement.Value = p_sLookup;
                }
                m_sInitialSearchText = p_sLookup;
            }

            //MITS 16771- Pankaj 5/29/09
            //City
            if ((String.IsNullOrEmpty(p_sCity)) && (!Page.IsPostBack)) //(!m_queryconsidered))
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["City"]))
                {
                    string sCity = HttpContext.Current.Request.QueryString["City"].ToString();
                    XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/City");
                    if (oLookupElement != null)
                    {
                        oLookupElement.Value = sCity;// +'%';
                    }
                    m_sInitialCityText = sCity;
                    //m_queryconsidered = true;
                }
            }
            else
            {
                XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/City");
                if (oLookupElement != null)
                {
                    oLookupElement.Value = p_sCity;
                }
                m_sInitialCityText = p_sCity;
            }

            //State Code
            if ((String.IsNullOrEmpty(p_lState)) && (!Page.IsPostBack)) //(!m_queryconsidered))
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["State"]))
                {
                    string sState = HttpContext.Current.Request.QueryString["State"].ToString();
                    XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/State");
                    if (oLookupElement != null)
                    {
                        oLookupElement.Value = sState;// +'%';
                    }
                    m_sInitialStateText = sState;
                    //m_queryconsidered = true;
                }
            }
            else
            {
                XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/State");
                if (oLookupElement != null)
                {
                    oLookupElement.Value = p_lState;
                }
                m_sInitialStateText = p_lState;
            }

            //State Text
            if ((String.IsNullOrEmpty(p_sStateText)) && (!Page.IsPostBack))//(!m_queryconsidered))
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["StateText"]))
                {
                    string sStateText = HttpContext.Current.Request.QueryString["StateText"].ToString();
                    XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/StateText");
                    if (oLookupElement != null)
                    {
                        oLookupElement.Value = sStateText;// +'%';
                    }
                    m_sInitialStateDesc = sStateText;
                    //m_queryconsidered = true;
                }
            }
            else
            {
                XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/StateText");
                if (oLookupElement != null)
                {
                    oLookupElement.Value = p_sStateText;
                }
                m_sInitialStateDesc = p_sStateText;
            }

            //Zip
            if ((String.IsNullOrEmpty(p_sZip)) && (!Page.IsPostBack))//(!m_queryconsidered))//abansal23: MITS 16165 on 05/06/2009
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["Zip"]))
                {
                    string sZip = HttpContext.Current.Request.QueryString["Zip"].ToString();
                    XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Zip");
                    if (oLookupElement != null)
                    {
                        oLookupElement.Value = sZip;
                    }
                    m_sInitialZipText = sZip;
                    //m_queryconsidered = true;
                }
            }
            else
            {
                XElement oLookupElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/Zip");
                if (oLookupElement != null)
                {
                    oLookupElement.Value = p_sZip;
                }
                m_sInitialZipText = p_sZip;
            }

            
            if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["Lob"]))
            {
                ViewState["lob"] = HttpContext.Current.Request.QueryString["Lob"].ToString();
            }
            if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["MaintainanceScreenFlag"]))
            {
                hd_flgMaintScreen.Value = HttpContext.Current.Request.QueryString["MaintainanceScreenFlag"].ToString();
            }
        }
        /// <summary>
        /// Update All Data List Bindings to display data
        /// </summary>
        private void UpdateListBindings()
        {
            XElement oEle = XElement.Parse(oFDMPageDom.SelectSingleNode("//NewDataSet").OuterXml);
            if (oEle.XPathSelectElement("./Table") != null)
            {
                DataSet ds = new DataSet();
                XmlTextReader oTextReader = new XmlTextReader(new StringReader(oFDMPageDom.SelectSingleNode("//NewDataSet").OuterXml));
                ds.ReadXml(oTextReader);
                //abansal23 on 05/14/2009 for GroupAssociation Starts
                if (ds.Tables[0].Columns["FACILITY_EID"] == null)
                {
                    ds.Tables[0].Columns.Add("FACILITY_EID");
                    ds.Tables[0].Columns.Add("LOCATION_EID");
                    ds.Tables[0].Columns.Add("DIVISION_EID");
                    ds.Tables[0].Columns.Add("REGION_EID");
                    ds.Tables[0].Columns.Add("OPERATION_EID");
                    ds.Tables[0].Columns.Add("COMPANY_EID");
                    ds.Tables[0].Columns.Add("CLIENT_EID");
                }
                //abansal23 on 05/14/2009 for GroupAssociation Ends
                ItemsList.DataSource = ds;
                ItemsList.DataBind();

                tbData.Visible = true;
                tbNoData.Visible = false;
            }
            else
            {
                tbNoData.Visible = true;
                tbData.Visible = false;
            }

        }
        
        /// <summary>
        /// Disables the links as per requirement
        /// </summary>
        private void UpdateListStyle()
        {
            //PJS:MITS 16546 - If lob="all" then enable all
            if (ViewState["lob"] != null && !String.IsNullOrEmpty(ViewState["lob"].ToString()) && ViewState["lob"].ToString()!= "all")
            {
                if (CommonFunctions.GetEntityLevel(Conversion.ConvertStrToInteger(ViewState["lob"].ToString())) != Conversion.ConvertStrToInteger(m_sInitialEntityLevel))
                {
                    foreach (Control oControl in ItemsList.Controls)
                    {
                        Control oLnkControl = oControl.FindControl("lstItem");
                        if (oLnkControl != null)
                        {
                            ((LinkButton)oLnkControl).OnClientClick = null;
                            ((LinkButton)oLnkControl).Enabled = false;
                        }
                    }
                }
            }
        }
        
    }
}
