<%@ page language="C#" autoeventwireup="true" codebehind="Status.aspx.cs" inherits="Riskmaster.UI.Home.Status"
    masterpagefile="~/App_Master/RMXMain.Master" title="RISKMASTER Database and View Selection" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<asp:content id="cHeader" contentplaceholderid="cphHeader" runat="server">
    <div id="banner" class="banner" runat="server">
    </div>
</asp:content>
<asp:content id="Content3" contentplaceholderid="cphMainBody" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="center-login-control">
        <table>
            <tr>
                <td class="statusLabel">
                    <asp:label id="lblLoginName" runat="server">Login Name:</asp:label>
                </td>
                <td align="left">
                    <asp:loginname id="Loginname1" runat="server" />
                    <asp:Label ID="tmploginname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="statusLabel">
                    <asp:label id="lblDSNName" runat="server">Data Source Name:</asp:label>
                </td>
                <td align="left">
                    <asp:dropdownlist id="ddlDataSources" Width="150px" runat="server" autopostback="True" onselectedindexchanged="ddlDataSources_SelectedIndexChanged" />
                </td>
            </tr>
            <tr>
                <td class="statusLabel">
                    <asp:label id="lblViews" runat="server">RISKMASTER View Name:</asp:label>    
                </td>
                <td align="left">
                    <asp:dropdownlist id="ddlViews" Width="150px" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:button id="btnLogin" runat="server" text="Continue" onclick="btnLogin_Click" />        
                </td>
            </tr>
        </table>
    </div>
</asp:content>
