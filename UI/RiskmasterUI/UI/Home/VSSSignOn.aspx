﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VSSSignOn.aspx.cs" Inherits="Riskmaster.UI.UI.Home.VSSSignOn" EnableViewState ="false" EnableViewStateMac="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VSS Login</title>
     <script language="javascript" type="text/javascript">

         function SingleSignon() {
             try {
                 //Submit the form to itself
                 var VSSUrl = document.getElementById('VSSUrl').value;
                 document.forms[0].action = VSSUrl;
                 document.forms[0].method = "post";
                 document.forms[0].submit();
             }
             catch (e) { }
         }

    </script>
</head>
<body onload="SingleSignon();">
    <form id="frmData" enableviewstate="false">       
        <input ID="VSSUrl" runat="server" enableviewstate="false" type="hidden"  />
        <input ID="loginName" runat="server" enableviewstate="false" type="hidden" />
        <input ID="passwd" runat="server" enableviewstate="false" type="hidden" />
        <input ID="envId" runat="server" enableviewstate="false" type="hidden" />
     </form>
</body>
</html>
