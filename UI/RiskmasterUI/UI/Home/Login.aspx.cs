﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/03/2014 | RMA-88  | achouhan3   | CSC SSO Implementation
 **********************************************************************************************/
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.AppHelpers;
using Riskmaster.Common;
//using Riskmaster.UI.RMAuthentication;
using Riskmaster.Security.Encryption;
//asharma326 MITS 27586
using Riskmaster.Models;
using System.IO;
using Riskmaster.Cache;
using System.Configuration;

namespace Riskmaster.UI.Home
{
    public partial class Login : Page
    {
        private string m_sWarningMessage = String.Empty;
        private string[] m_sMessageArray;
        private bool m_bSMSAccessOnly = false;
        private static string[] sDataSeparator = { "|^^|" };
        //CSC SSO RMA-88 Starts
        private bool m_bUseSSO = false;
        //CSC SSO RMA-88 Ends
        private const string DEFAULT_REDIRECT_URL = "Status.aspx";

        //private RMAuthentication.AuthenticationServiceClient authService = new RMAuthentication.AuthenticationServiceClient();
        private delegate bool GetUserAuthentication(ref string strUserName);


        /// <summary>
        /// Event which initializes the page with the Default Theme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = "RMX_Default";
        } // method: Page_PreInit

        /// <summary>
        /// Handles the Page Load event for the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //asharma326 MITS 27586 Starts for focusing login button
            Button lbButton = Login1.FindControl("Button1") as Button;
            ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("cphMainBody");
            contentPlaceHolder.Page.Form.DefaultButton = lbButton.UniqueID;
            //asharma326 MITS 27586 Ends  for focusing login button
            //RMA -88 SSO implementation
            Boolean.TryParse(ConfigurationManager.AppSettings["UseSSO"] ?? "false", out m_bUseSSO);
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["src"] != null)
                {
                    LoadRMALogo();
                    return;
                }
                try
                {
                    // npadhy JIRA RMACloud-3031 In case of Form Authentication, when you access a page which requires authentication, 
                    // ASP.NET will redirect you to the login page, passing in the ReturnUrl as a parameter so you can be returned to the page you came from post-login.
                    // When we Click on Logout, ReturnUrl is appended in the Querystring, and application thinks that is the Client ifd and tries to decrypt it and throws the error.
                    if (HttpContext.Current.Request.QueryString["ReturnUrl"] == null)
                    {
                        string sClientid = RMCryptography.DecryptString(HttpContext.Current.Request.QueryString[0]);
                        AppHelper.ClientId = Convert.ToInt32(sClientid);
                    }
                }
                catch (Exception ee)
                {
                    AppHelper.ClientId = 0;
                }
                string strUserName = string.Empty;
                //RMA -88 SSO implementation Starts
                if (m_bUseSSO)
                {
                    strUserName = (string)Context.Items["HTTP_USER"];
                    AppHelper.CreateFormsAuthenticationTicket(strUserName, false);
                    Response.Redirect(DEFAULT_REDIRECT_URL);
                    return;
                }
                //RMA -88 SSO implementation Ends

                if (IsUserAuthenticated(ref strUserName))
                {
                    AppHelper.CreateFormsAuthenticationTicket(strUserName, false);
                    Response.Redirect(DEFAULT_REDIRECT_URL);
                    return;
                }//if
                //Commented by Amsitosh for R8 enhancement
                //Determine which client browser version the user is running to access RMX
                //if (! PageHelper.IsSupportedBrowser(PageHelper.GetClientBrowserType()))
                // {
                //     StringBuilder strBrowserMessage = new StringBuilder();
                //     NameValueCollection nvSupportedBrowsers = PageHelper.GetSupportedBrowserTypes();


                //     //TODO: Migrate this content to a resource file
                //     strBrowserMessage.Append("<br />");
                //     strBrowserMessage.Append("This browser is currently not a supported browser. <br />");
                //     strBrowserMessage.Append("Currently supported browser versions are the following: <br />");


                //     foreach (string strBrowserName in nvSupportedBrowsers.AllKeys)
                //     {
                //         strBrowserMessage.AppendFormat("{0}<br/>", nvSupportedBrowsers[strBrowserName]);
                //     }//foreach

                //     strBrowserMessage.Append("If you have any questions regarding supported platforms, please contact RISKMASTER Support.");

                //     throw new ApplicationException(strBrowserMessage.ToString());
                // } // if  


                string sBackgroundColor = string.Empty;
                string sForgotPasswordColor = string.Empty; //zmohammad MITs 34734

                sBackgroundColor = RMXPortalHelper.GetBannerColorAsHTML();
                //zmohammad MITs 34734
                sForgotPasswordColor = RMXPortalHelper.GetForgotPasswordColorAsHTML();
                banner.Style.Add("background-color", sBackgroundColor);
                Login1.BackColor = RMXPortalHelper.GetGDIColorFromHtml(sBackgroundColor);
                Login1.ForeColor = RMXPortalHelper.GetLoginLabelFontColorAsObj();

                //zmohammad MITs 34734
                System.Web.UI.HtmlControls.HtmlAnchor lnkFrgtPwd = Login1.FindControl("lnkFrgtPwd") as System.Web.UI.HtmlControls.HtmlAnchor;
                if (!string.IsNullOrEmpty(sForgotPasswordColor))
                {
                    lnkFrgtPwd.Style.Add("color", sForgotPasswordColor);
                }
                // Populate homepage
                ifHomepage.Attributes["src"] = RMXPortalHelper.PortalHomePage;
                ifHomepage.Attributes["scrolling"] = "auto";
                banner.Style.Add("background-image", "Login.aspx?src=rmx_logo.gif");
                
				//mkaran2 - Start
                System.Data.DataSet dsADProvider = null;
                ADDataSource objADS = new ADDataSource();
                try
                {                    
                    dsADProvider = objADS.SelectADRecord();
                    if (dsADProvider != null && dsADProvider.Tables.Count > 0 && dsADProvider.Tables[0].Rows.Count > 0)
                    {
                        if (dsADProvider.Tables[0].Rows[0]["isEnabled"] != null)
                        {
                            if (dsADProvider.Tables[0].Rows[0]["isEnabled"].Equals(true))
                            {
                                lnkFrgtPwd.Style.Add("display", "none");
                            }
                        }
                    }
                }
                finally
                {
                    if (dsADProvider!=null)
                    {
                        dsADProvider.Dispose();
                    }                   
                }                
 				//mkaran2 - End
            } // if
            //Raman 05/03/2010
            //SMS Direct Access Implementation

            bool bSuccess;
            if (ViewState["SMSAccessOnly"] == null)
            {
                m_bSMSAccessOnly = Conversion.CastToType<bool>(AppHelper.GetQueryStringValue("smsaccessonly"), out bSuccess);
                ViewState["SMSAccessOnly"] = m_bSMSAccessOnly;
            }
            else
            {
                m_bSMSAccessOnly = Conversion.CastToType<bool>(ViewState["SMSAccessOnly"].ToString(), out bSuccess);
            }
        }//event: Page_Load();

        /// <summary>
        /// Load Image
        /// </summary>
        private void LoadRMALogo()
        {
            try
            {
                byte[] filecontent = RMXPortalHelper.PortalHomeImage;
                if (filecontent == null)
                {
                    //banner.Style.Add("background-image", "../../Images/rmx_logo.gif");
                    string cacheimageKey = "RMXPortalImage";
                    FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\Images\rmx_logo.gif", FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    filecontent = br.ReadBytes((int)fs.Length);
                    CacheCommonFunctions.UpdateValue2Cache<byte[]>(cacheimageKey, AppHelper.ClientId, filecontent);
                }
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "images/jpeg";
                Response.AddHeader("content-disposition", "attachment;filename=imagelogo");
                Response.BinaryWrite(filecontent);
                Response.Flush();
                Response.End();
            }
            catch
            {
            }
        }


        /// <summary>
        /// Authenticates the user using the specified credentials
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnAuthenticate(object sender, AuthenticateEventArgs e)
        {
            e.Authenticated = false;
            bool blnIsAuthenticated = false;
            AuthData oAuthData = null;
            string sOutResult = string.Empty;
            //RMA-88 CSC SSO Implementation Starts
            string SSOUserId;
            Boolean.TryParse(ConfigurationManager.AppSettings["UseSSO"] ?? "false", out m_bUseSSO);
            //RMA-88 CSC SSO Implementation Ends
            try
            {
                oAuthData = new AuthData();
                //RMA-88 CSC SSO Implementation Starts
                if (m_bUseSSO)
                {
                    SSOUserId = (string)Context.Items["HTTP_USER"];
                    oAuthData.UserName = AppHelper.HtmlEncodeString(SSOUserId.Trim());
                    oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                }
                //RMA-88 CSC SSO Implementation Ends
                else
                {
                    oAuthData.UserName = AppHelper.HtmlEncodeString(Login1.UserName.Trim());
                    oAuthData.Password = AppHelper.HtmlEncodeString(Login1.Password);
                    oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                }
                //Raman 05/03/2010
                //SMS Direct Access Implementation
                if (m_bSMSAccessOnly)
                {
                    //blnIsAuthenticated = authService.AuthenticateSMSUser(AppHelper.HtmlEncodeString(Login1.UserName.Trim()), AppHelper.HtmlEncodeString(Login1.Password), out m_sWarningMessage);
                    sOutResult = AppHelper.GetResponse<string>("RMService/Authenticate/loginadmin", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                }
                //RMA-88 CSC SSO Implementation Starts
                else if (m_bUseSSO)
                {
                   // blnIsAuthenticated = authService.SingleSignOnUser(AppHelper.HtmlEncodeString(SSOUserId.Trim()));
                    sOutResult = AppHelper.GetResponse<string>("RMService/Authenticate/sso", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                }
                //RMA-88 CSC SSO Implementation Ends
                else
                {
                    //blnIsAuthenticated = authService.AuthenticateUser(AppHelper.HtmlEncodeString(Login1.UserName.Trim()), AppHelper.HtmlEncodeString(Login1.Password), out m_sWarningMessage);
                    sOutResult = AppHelper.GetResponse<string>("RMService/Authenticate/login", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                }
                blnIsAuthenticated = Convert.ToBoolean(sOutResult.Split(sDataSeparator, StringSplitOptions.None)[0]);
                m_sWarningMessage = sOutResult.Split(sDataSeparator, StringSplitOptions.None)[1];
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message, ex);
            }
            finally
            {
                oAuthData = null;
            }
            e.Authenticated = blnIsAuthenticated;
        } // event: OnAuthenticate
        /// <summary>
        /// Handles the event after the user has successfully logged into the system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            //Asharma326 MITS 27586 Starts
            AuthData oAuthData = new AuthData();
            oAuthData.UserName = AppHelper.HtmlEncodeString(Login1.UserName.Trim());
            oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
            //if (authService.IsPasswordReset(Login1.UserName.Trim(),AppHelper.ClientId))
            // akaushik5 Commneted for RMA-16615 Starts
            //if (AppHelper.GetResponse<bool>("RMService/Authenticate/ispasswordreset", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData))
            //{
            //    Response.Redirect("../SecurityMgtSystem/ChangePassword.aspx?forgotpassword=1");
            //}
            // akaushik5 Commneted for RMA-16615 Ends
            //Asharma326 MITS 27586 Ends
            // RMA-88 CSC SSO Implementation Starts
            string strUserName = string.Empty;
            if (m_bUseSSO)
            {
                strUserName = (string)Context.Items["HTTP_USER"];
                AppHelper.CreateFormsAuthenticationTicket(strUserName.Trim(), Login1.RememberMeSet);
                HttpContext.Current.Items.Add("loginName", strUserName);
            }
            else
            {
                AppHelper.CreateFormsAuthenticationTicket(Login1.UserName.Trim(), Login1.RememberMeSet);
                HttpContext.Current.Items.Add("loginName", Login1.UserName.Trim());
            }
            // RMA-88 CSC SSO Implementation Ends
            // akaushik5 Added for RMA-16615 Starts
            if (AppHelper.GetResponse<bool>("RMService/Authenticate/ispasswordreset", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData))
            {
                Response.Redirect("../SecurityMgtSystem/ChangePassword.aspx?forgotpassword=1");
            }
            // akaushik5 Added for RMA-16615 Ends
            //Raman 05/03/2010
            //SMS Direct Access Implementation
            if (m_bSMSAccessOnly)
            {
                Response.Redirect("../SecurityMgtSystem/Default.html");
                return;
            }
            //Raman Bhatia: 11/03/2009
            //Enhanced Security Changes
            //Warning Message would contain the reason for the warning.. This would handle cases for Locked Account , Pwd Expiration,
            //Forced Password Expiration and also password expiring notifications..
            string strRedirectUrl = DEFAULT_REDIRECT_URL;

            if (string.IsNullOrEmpty(m_sWarningMessage))
            {
                Response.Redirect(DEFAULT_REDIRECT_URL);
            }//if
            else
            {
                m_sMessageArray = m_sWarningMessage.Split('|');
                switch (Convert.ToInt32(m_sMessageArray[0]))
                {
                    case (int)ACCOUNT_STATUS.UserPasswordExpired: //user password expired, redirect to password expire screen
                        strRedirectUrl = "../SecurityMgtSystem/PasswordExpired.aspx";
                        break;
                    case (int)ACCOUNT_STATUS.PasswordExpiringSoon: //Display password expiring notification
                        //Display password expiring notification
                        strRedirectUrl = String.Format("../SecurityMgtSystem/PasswordExpiringSoon.aspx?Diff={0}", m_sMessageArray[1]);
                        break;
                    default: //Redirect to the Default Url (currently set as the Status page)
                        strRedirectUrl = DEFAULT_REDIRECT_URL;
                        break;
                }//switch

                //Raman : R8 performance improvement : By using server.transfer we can save on a roundtrip
                //changed on 3/19/2012
                // RMAA-88 CSC SSO Implementation commented 
                //HttpContext.Current.Items.Add("loginName", Login1.UserName.Trim());
                //authService.Close();

                //Raman : There is a rare scenario where user closes the window without logging out and then logs in with a different
                //username.. in this scenario its always better to go to client again to get the correct user context.. hence we use response.redirect
                if (!m_bUseSSO && !String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) && String.Compare(HttpContext.Current.User.Identity.Name, Login1.UserName.Trim(), true) != 0)
                {
                    Response.Redirect(strRedirectUrl);
                }
                else //in normal scenario we can save on a roundtrip here
                {
                    Server.Transfer(strRedirectUrl);
                }
                //Response.Redirect(strRedirectUrl);

            }//else

            oAuthData = null;
        }// event: Login1_LoggedIn


        /// <summary>
        /// Handles the event if ther user has entered invalid credentials
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Login1_LoginError(object sender, EventArgs e)
        {
            //Raman Bhatia: 11/03/2009
            //Enhanced Security Changes
            //Warning Message would contain the reason for the warning.. This would handle cases for Locked Account , Pwd Expiration,
            //Forced Password Expiration and also password expiring notifications..
            m_sMessageArray = m_sWarningMessage.Split('|');
            if (!String.IsNullOrEmpty(m_sMessageArray[0]))
            {
                switch (Convert.ToInt32(m_sMessageArray[0]))
                {
                    case (int)ACCOUNT_STATUS.Locked: //Account is locked, Display appropriate message
                        Login1.FailureText = "Rma-" + m_sMessageArray[1] + ": Your Account is locked; Please contact your system Administrator."; //kkaur8 added for MITS 33671
                        break;
                    case (int)ACCOUNT_STATUS.ForcedPasswordExpired: //Password is expired, display appropriate message
                        Login1.FailureText = m_sMessageArray[1];
                        break;
                    default:
                        Login1.FailureText = "Your login attempt was not successful. Please try again.";
                        break;
                }
            }

        }//event: Login1_LoginError

        /// <summary>
        /// Handles the Unload event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {
            //Ensure that the call to the WCF service is closed upon termination of the page
            //authService.Close();
        }

        /// Determines if a user has passed authentication
        /// through various available authentication mechanisms
        /// </summary>
        /// <param name="strUserName">reference handle to the string containing the user name</param>
        /// <returns>boolean indicating whether or not the specified credential exists</returns>
        private bool IsUserAuthenticated(ref string strUserName)
        {
            bool blnIsAuthenticated = false, blnIsVerified = false;

            //Use a delegate to define mappings to methods with common signatures
            GetUserAuthentication[] delServerVarAuth = 
            {
                IsRequestParameter
            };

            //Loop through each of the delegate operations
            foreach (var delOperation in delServerVarAuth)
            {
                //Check each operation to verify authentication
                blnIsAuthenticated = delOperation(ref strUserName);

                blnIsVerified = VerifyCredentials(blnIsAuthenticated, strUserName);

                //Determine if the user has been successfully authenticated
                if (blnIsVerified)
                {
                    return blnIsVerified;
                }//if
            }//foreach

            //return the final value if the user did not pass either of the existing credential checks
            return blnIsVerified;
        }//method: IsUserAuthenticated


        /// <summary>
        /// Verifies that the credentials exists and are considered valid
        /// </summary>
        /// <param name="blnIsAuthenticated">boolean indicating whether or not the user was 
        /// successfully authenticated</param>
        /// <param name="strUserName">containing the user name</param>
        /// <returns>boolean indicating whether or not the specified credential exists</returns>
        private bool VerifyCredentials(bool blnIsAuthenticated, string strUserName)
        {
            if (blnIsAuthenticated && !string.IsNullOrEmpty(strUserName))
            {
                return true;
            }//if
            else
            {
                return false;
            }//else
        }//method: VerifyCredentials();

        #region Delegate Methods
        /// <summary>
        /// Determines if the username and password
        /// have been passed in through the QueryString variable or hidden fields
        /// collection
        /// </summary>
        /// <param name="strUserName">reference handle to the string containing the user name</param>
        /// <returns>boolean indicating whether or not the specified credential exists</returns>
        private bool IsRequestParameter(ref string strUserName)
        {
            string strPassword = string.Empty, strUserMessage = string.Empty;
            bool blnIsUserCred = false;

            strUserName = Request.QueryString["loginname"];
            strPassword = Request.QueryString["password"];

            //If not in the QUeryString, try to get from Form hidden fields
            if (string.IsNullOrEmpty(strUserName) && string.IsNullOrEmpty(strPassword))
            {
                strUserName = Request.Form["loginname"];
                strPassword = Request.Form["password"];
            }

            if (!string.IsNullOrEmpty(strUserName) && !string.IsNullOrEmpty(strPassword))
            {
                //blnIsUserCred = authService.AuthenticateUser(AppHelper.HtmlEncodeString(strUserName), AppHelper.HtmlEncodeString(strPassword), out strUserMessage);
                AuthData oAuthData = new AuthData();
                oAuthData.UserName = AppHelper.HtmlEncodeString(Login1.UserName.Trim());
                oAuthData.Password = AppHelper.HtmlEncodeString(Login1.Password);
                oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string sOutResult = AppHelper.GetResponse<string>("RMService/Authenticate/login", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                blnIsUserCred = Convert.ToBoolean(sOutResult.Split(sDataSeparator, StringSplitOptions.None));
                oAuthData = null;
                //Close the authentication service
                //authService.Close();
            }//if

            return blnIsUserCred;
        }//method: IsQueryStringParameter()
        #endregion

        //Asharma326 MITS 27586 Starts Forgot Password
        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                ForgotPasswordGenericResponse objForgotPasswordGenericResponse = null;
                Label lb = (Label)this.Login1.FindControl("lblError");
                AjaxControlToolkit.ModalPopupExtender AjaxModalPopup = (AjaxControlToolkit.ModalPopupExtender)this.Login1.FindControl("ForgotPasswordModalPopup");
                if (AjaxModalPopup != null)
                {
                    AjaxModalPopup.Show();
                }
                TextBox tb = (TextBox)this.Login1.FindControl("txtUserid");
                if (tb != null)
                {
                    //Call WCF Service
                    //objForgotPasswordGenericResponse = authService.ForgotPasswordSendMail(tb.Text.ToString());
                    AuthData oAuthData = new AuthData();
                    oAuthData.UserName = AppHelper.HtmlEncodeString(tb.Text.ToString());
                    oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    objForgotPasswordGenericResponse = AppHelper.GetResponse<ForgotPasswordGenericResponse>("RMService/Authenticate/password", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
                    oAuthData = null;
                }
                if (!string.IsNullOrEmpty(objForgotPasswordGenericResponse.Status))
                {
                    if (lb != null)
                    {
                        if (objForgotPasswordGenericResponse.Status == "Error")
                        {
                            lb.Text = objForgotPasswordGenericResponse.Message;
                            lb.ForeColor = System.Drawing.Color.Red;
                        }
                        if (objForgotPasswordGenericResponse.Status == "Success")
                        {
                            lb.Text = objForgotPasswordGenericResponse.Message;
                            lb.ForeColor = System.Drawing.Color.Green;
                        }
                    }
                }
            }
            catch
            { }
        }
        protected void btnhide_Click(object sender, EventArgs e)
        {
            AjaxControlToolkit.ModalPopupExtender AjaxModalPopup = (AjaxControlToolkit.ModalPopupExtender)this.Login1.FindControl("ForgotPasswordModalPopup");
            if (AjaxModalPopup != null)
            {
                AjaxModalPopup.Hide();
            }
        }
        //Asharma326 MITS 27586 Ends Forgot Password"

    }
}
