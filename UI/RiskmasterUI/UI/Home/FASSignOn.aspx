﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FASSignOn.aspx.cs" Inherits="Riskmaster.UI.UI.Home.FASSignOn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FAS Login</title>
</head>
<body>
    <form id="form1" runat="server" enableviewstate="false">
        <div>
            <asp:TextBox Style="display: none" ID="FASUrl" rmxref="/Instance/Document/FAS/FASUrl"
            runat="server" ></asp:TextBox>
            <asp:TextBox Style="display: none" ID="loginName" rmxref="/Instance/Document/FAS/loginName"
            runat="server" ></asp:TextBox>
            <asp:TextBox Style="display: none" ID="passwd" rmxref="/Instance/Document/FAS/passwd"
            runat="server" ></asp:TextBox>
        </div>
    </form>
</body>
</html>
