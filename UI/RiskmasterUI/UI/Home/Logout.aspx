﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/RMXMain.Master" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="Riskmaster.UI.UI.Home.Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .logout {
            display: block;
            -webkit-margin-before: 1.33em;
            -webkit-margin-after: 1.33em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
            font-weight: bold;
            font-size:18px;
        }

        .logoutcontainer {
            background-color: #EEEFF3;
            font-family: Arial, Verdana, Helvetica, sans-serif;
            text-align: center;
            background-repeat: repeat-x;
            width: 95%;
            margin-top: 0px;
            margin-right: auto;
            margin-bottom: 0px;
            margin-left: auto;
            height: auto;
            padding-bottom: 10px;
            
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
    <div id="banner" class="banner" runat="server">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainBody" runat="server">
    <div class="center-login-control" style="left:22% !important">
        <div style="clear: both;"></div>
        <div class="logout">
            You have been logged out. For security reasons, it is recommended to Close(X) your current internet browser
        </div>
    </div>
</asp:Content>
