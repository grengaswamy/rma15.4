﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Text;


namespace Riskmaster.UI.UI.Home
{
    public partial class VSSSignOn : System.Web.UI.Page
    {
        XElement m_objMessageElement = null;
        XElement objTempElement = null;
        XmlDocument objXml = new XmlDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                m_objMessageElement = GetMessageTemplate();

                if (!string.IsNullOrEmpty(AppHelper.GetSessionId()))
                {
                    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                    objTempElement.Value = AppHelper.GetSessionId();
                }

                //Call WCF wrapper for cws
                string sReturn = AppHelper.CallCWSService(m_objMessageElement.ToString());

                XmlDocument oXmlOut = new XmlDocument();
                oXmlOut.LoadXml(sReturn);
                XmlNode oInstanceNode = oXmlOut.SelectSingleNode("/ResultMessage/Document");

                objXml.LoadXml(oInstanceNode.InnerXml);

                this.VSSUrl.Value = objXml.SelectSingleNode("//VSSUrl").InnerText;
                this.loginName.Value = objXml.SelectSingleNode("//loginName").InnerText;
                this.passwd.Value = objXml.SelectSingleNode("//passwd").InnerText;
                this.envId.Value = "1";
                this.EnableViewState = false;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SingleSignonAdaptor.SingleSignonVSSXML</Function></Call><Document>");
            sXml = sXml.Append("<form>");          
            sXml = sXml.Append("<VSSUrl />");
            sXml = sXml.Append("<loginName />");
            sXml = sXml.Append("<passwd />");
            sXml = sXml.Append("<envId />");
            sXml = sXml.Append("</form></Document></Message>");

            XElement oTemplateEOB = XElement.Parse(sXml.ToString());

            return oTemplateEOB;
        }
        }
    }