﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 03/14/2014 | 35039           | pgupta93   | Changes req for tabing in IE
 * 11/08/2014 | 34257/ RMA-429  | ajohari2   | Default Claim Type by Policy LOB (if only single claim type is there for that LOB)
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.CodesService;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.Web.UI;  
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml.XPath;
using Riskmaster.Models;//MITS:34257 - JIRA RMA-429 ajohari2

namespace Riskmaster.UI.Codes
{
    public partial class CodesList : System.Web.UI.Page
    {
        private string m_SortDirection = "";
        private string m_strSortExp = "";
        protected CodeListType objList=null;
        protected List<CodeType> codes=null;
        protected string sCodeTable ="";

        string sDeptEid = String.Empty;
        string sCodetype = String.Empty;
        string sLOB = String.Empty;
        string sFilter = String.Empty;
        string sSessionlob = String.Empty;
        string sFormname = String.Empty;
        string sTriggerDate = String.Empty;
        string sEventdate = String.Empty;
        string sTitle = String.Empty;
        string sSessionClaimId = String.Empty;
        string sOrgLevel = String.Empty;
        string sTypeLimits = String.Empty;
        string sSessionDsnId = String.Empty;
        string sInsuredEid = String.Empty; //PJS MITS# 15220
        string sParentCodeid = String.Empty;//declare variable for the parent reserve type code id--stara 05/18/09
        string sEventId = string.Empty;
        string sShowCheckBox = string.Empty;
        string sPolicyId = string.Empty;
        string sCovTypeCodeId = string.Empty;
        string sLossTypeCodeId = string.Empty;      //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
        string sTransId = string.Empty;
        string sClaimantEid = string.Empty;
        //start:rupal for first & final payment
        string IsFirstFinal = string.Empty;
        string PayCol = string.Empty;//AA
        string sPolCovgRowId = string.Empty;
        string sPolUnitRowId = string.Empty;
        //end:rupal for first & final payment
        string sRcRowId = string.Empty;
        string sPolicyLOB = string.Empty;
        //rupal:start, policy system interface, CovgSeqNum
        string sCovgSeqNum = string.Empty;
        string sTransSeqNum = string.Empty;
        string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
        string sSortExpression = string.Empty;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        string sSortOrder = string.Empty; //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        string sField = string.Empty; //igupta3: Ensuring field is supp or not
		string sJurisdiction = string.Empty;    //MITS#36929
       
        protected void Page_Load(object sender, EventArgs e)
        {
            // CodesBusinessHelper cb = new CodesBusinessHelper();       //commented by bsharma33 : MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
           
            try
            {
                //mudabbir added below if condition 36624
                if (IsPostBack)
                {
                  string strmulticodes= StoreCheckID(dgCodes,hdnMultiselect.Value,pagenumbercopy.Value);
                  hdnMultiselect.Value = strmulticodes;
                }
                //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                if(!IsPostBack)
                {
                    hdSortColumn.Value = "1";
                    hdSortOrder.Value = "Asc";
                    pageaction.Value = Request.RawUrl;
                    hdnMultiselect.Value = "";
                }
                GetCode();
			}
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }   
        }

        //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
        private void GetCode()
        {
            CodesBusinessHelper cb = new CodesBusinessHelper();

                sCodeTable = AppHelper.GetQueryStringValue("codetype");

                sFilter = AppHelper.GetQueryStringValue("filter");
                sLOB = AppHelper.GetQueryStringValue("lob");
                sDeptEid = AppHelper.GetQueryStringValue("orgeid");
                sFormname = AppHelper.GetQueryStringValue("formname");
                sTriggerDate = AppHelper.GetQueryStringValue("triggerdate");
                sEventdate = AppHelper.GetQueryStringValue("eventdate");
                sSessionlob = AppHelper.GetQueryStringValue("sessionlob");
                sTitle = AppHelper.GetQueryStringValue("Title");
                sSessionClaimId = AppHelper.GetQueryStringValue("sessionclaimid");
                sOrgLevel = AppHelper.GetQueryStringValue("orgLevel");   
                sTypeLimits = AppHelper.GetQueryStringValue("typelimit");  
                sSessionDsnId =AppHelper.GetQueryStringValue("sessiondsnid");
                sInsuredEid = AppHelper.GetQueryStringValue("insuredeid"); //PJS MITS # 15220
                sParentCodeid = AppHelper.GetQueryStringValue("parentcodeid");// -- stara Mits 16667 05/18/09
                sEventId = AppHelper.GetQueryStringValue("eventid");
                sShowCheckBox = AppHelper.GetQueryStringValue("showcheckbox");//rsushilaggar Handled  paging for checkbox functionality  MITS 21600
                sPolicyId = AppHelper.GetQueryStringValue("PolicyId");
                sPolUnitRowId = AppHelper.GetQueryStringValue("PolUnitRowId");
                sCovTypeCodeId = AppHelper.GetQueryStringValue("CovCodeId");
                sLossTypeCodeId = AppHelper.GetQueryStringValue("LossCodeId");      //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                sTransId = AppHelper.GetQueryStringValue("TransId");
                sClaimantEid = AppHelper.GetQueryStringValue("sClaimantEntId");
                //rupal:start First & Final Payment
                //to get coverage_type, need to first identify if the payment is first & final. 
                IsFirstFinal = AppHelper.GetQueryStringValue("IsFirstFinal");
                PayCol = AppHelper.GetQueryStringValue("paycol"); 

                //rupal:end for first & final payment
                sRcRowId = AppHelper.GetQueryStringValue("RcRowId");
                sPolicyLOB = AppHelper.GetQueryStringValue("policyLOB");
                sCovgSeqNum  = AppHelper.GetQueryStringValue("CovgSeqNum");//rupal:added for point policy system interface 
                sTransSeqNum = AppHelper.GetQueryStringValue("TransSeqNum");
                sCoverageKey = AppHelper.GetQueryStringValue("CoverageKey");        //Ankit Start : Worked on MITS - 34297
                //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 sorting logic

             	sSortExpression = AppHelper.GetQueryStringValue("sortColumn");
             	sSortOrder = AppHelper.GetQueryStringValue("SortOrder");
                sField = AppHelper.GetQueryStringValue("FieldName"); //igupta3 : MITS 36575 
				//added by swati for AIC GAp 7 MITS # 36929
                sJurisdiction = AppHelper.GetQueryStringValue("jurisdiction");
                //change end here

	            if (string.IsNullOrEmpty(sSortExpression) == false)
    	        {
        	        hdSortColumn.Value = sSortExpression;
            	    if (string.IsNullOrEmpty(sSortOrder) == false)
                	{
                	   	hdSortOrder.Value = sSortOrder;
                	}
            	}
                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                //sJurisdiction added by swati
                //objList = cb.GetCode(sDeptEid, sCodeTable, sLOB, sTypeLimits, sFormname, sTriggerDate, sEventdate, sSessionDsnId, sSessionlob, sSessionClaimId, sTitle,
                objList = cb.GetCode(sDeptEid, sCodeTable, sLOB, sJurisdiction, sTypeLimits, sFormname, sTriggerDate, sEventdate, sSessionDsnId, sSessionlob, sSessionClaimId, sTitle,
                                    pagenumber.Value.ToString(), sInsuredEid, sEventId, sParentCodeid, sFilter, sShowCheckBox, sPolicyId, sCovTypeCodeId, sTransId, sClaimantEid, IsFirstFinal, sPolUnitRowId, sRcRowId, sPolicyLOB, sCovgSeqNum, AppHelper.GetQueryStringValue("RsvStatusParent"), sTransSeqNum, hdSortColumn.Value, hdSortOrder.Value, sField, sCoverageKey, sLossTypeCodeId, PayCol);    //Mona:Adding codefilter like showing codes whith parent "Open":R6       //Ankit Start : Worked on MITS - 34297
                //rupal: added sIsFFPmt to identify the payment as first & final, 

                // -- stara end Mits 16667 05/18/09
                if (objList != null)
                {
                    displayname.Value = objList.Displayname.ToString();

                    //apeykov JIRA RMA-5689 / MITS 37429 + bpaskova fix start 
                    if (sCodeTable == "PRIMARY_PAY_CODE" && (sField == "secondpaycode_codelookup" || sField == "patsecondpaycode_codelookup"))
                        displayname.Value = "Secondary Pay Code";
                    //apeykov JIRA RMA-5689 / MITS 37429 + bpaskova fix end

                    //bkumar33:cosmetic change
                    if (displayname.Value == "Cause")
                        displayname.Value = "Cause Code";
                    else if (displayname.Value == "Positions")
                        displayname.Value = "Position Code";
                    else if (displayname.Value == "Savings Type1")
                        displayname.Value = "Savings Type";

                    if (string.IsNullOrEmpty(objList.ThisPage))
                    {
                        pagenumber.Value = "0";
                    }
                    else
                        pagenumber.Value = objList.ThisPage;
                    if (string.IsNullOrEmpty(objList.PageCount))
                    {
                        pagecount.Value = "0";
                    }
                    else
                        pagecount.Value = objList.PageCount;
                    codes = objList.Codes.ToList();
                }
                else 
                {
                    displayname.Value = ""; 
                    pagenumber.Value = "0";
                    pagecount.Value = "0";
                }
                 
                //PJS : No sorting and client side paging is done as of now
                //codes = BindData("");
                dgCodes.DataSource = codes;
                dgCodes.DataBind();
            }
            
        protected void dgCodes_RowDataBound( object sender, GridViewRowEventArgs e)
        {
         if (e.Row.RowType == DataControlRowType.DataRow)
            {
                    HyperLink rowHyperLink = null;
                    rowHyperLink = new HyperLink();
                    rowHyperLink.NavigateUrl = "#"; 
                    String sShortCode ="";
                    String sDesc = "";
                    String sId = "";
                    String sParentCode = "";
                    String sParentDesc = "";
                    String sResstatus = string.Empty;//skhare7 27920
                    string sCovgSeqNum = string.Empty;//rupal:policy system interface
                    string sTransSeqNum = string.Empty;
                    string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
                    if (DataBinder.Eval(e.Row.DataItem, "ShortCode") == null)
                    {
                        sShortCode = "";
                    }
                    else
                    {
                        sShortCode = DataBinder.Eval(e.Row.DataItem, "ShortCode").ToString();
                    }
                    if (DataBinder.Eval(e.Row.DataItem, "Desc") == null)
                    {
                        sDesc = "";
                    }
                    else
                    {
                        sDesc = DataBinder.Eval(e.Row.DataItem, "Desc").ToString();
                    } 
                    if (DataBinder.Eval(e.Row.DataItem, "Id") == null)
                    {
                        sId  = "";
                    }
                    else
                    {
                        sId = DataBinder.Eval(e.Row.DataItem, "Id").ToString();
                    }
                    if (DataBinder.Eval(e.Row.DataItem, "ParentCode") == null)
                    {
                        sParentCode = "";
                    }
                    else
                    {
                        sParentCode = DataBinder.Eval(e.Row.DataItem, "ParentCode").ToString();
                    }
                    if (DataBinder.Eval(e.Row.DataItem, "parentDesc") == null)
                    {
                        sParentDesc = "";
                    }
                    else
                    {
                        sParentDesc = DataBinder.Eval(e.Row.DataItem, "parentDesc").ToString();
                    }
                    if ((string.Compare(sCodeTable, "TRANS_TYPES", true) == 0))//skhare7 27920
                    {
                        if (DataBinder.Eval(e.Row.DataItem, "resstatus") == null)
                        {
                            sResstatus = "";
                        }
                        else
                        {
                            sResstatus = DataBinder.Eval(e.Row.DataItem, "resstatus").ToString();
                        }
                    }
                           
             //rupal:start,policy system interface
                    if ((string.Compare(sCodeTable, "COVERAGE_TYPE", true) == 0))
                    {
                        if (DataBinder.Eval(e.Row.DataItem, "CovgSeqNum") == null)
                        {
                            sCovgSeqNum = "";
                        }
                        else
                        {
                            sCovgSeqNum = DataBinder.Eval(e.Row.DataItem, "CovgSeqNum").ToString();
                        }

                        if (DataBinder.Eval(e.Row.DataItem, "TransSeqNum") == null)
                        {
                            sTransSeqNum = "";
                        }
                        else
                        {
                            sTransSeqNum = DataBinder.Eval(e.Row.DataItem, "TransSeqNum").ToString();
                        }
                        //Ankit Start : Worked on MITS - 34297
                        if (DataBinder.Eval(e.Row.DataItem, "CoverageKey") == null)
                        {
                            sCoverageKey = "";
                        }
                        else
                        {
                            sCoverageKey = DataBinder.Eval(e.Row.DataItem, "CoverageKey").ToString();
                        }
                        //Ankit End
                    }
             //rupal:end
                    if ((string.Compare(sCodeTable, "ENTITY", true) == 0) || (string.Compare(sCodeTable, "states", true) == 0))
                    {
                        rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "')");
                        rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "')}");//MITS:35039
                    }
                    else if ((string.Compare(sCodeTable, "TRANS_TYPES", true) == 0))//skhare7 27920
                    {
                        rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc)+"_"+escapeStrings(sResstatus) + "')");
                        rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "_" + escapeStrings(sResstatus) + "')}");//MITS:35039
                    
                    }
                    //rupal:start, policy system interface
                    
                    else if ((string.Compare(sCodeTable, "COVERAGE_TYPE", true) == 0))
                    {
                        rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "','','" + escapeStrings(sCovgSeqNum) + "','" + escapeStrings(sTransSeqNum) + "','" + escapeStrings(sCoverageKey) + "')");       //Ankit Start : Worked on MITS - 34297
                        rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "','','" + escapeStrings(sCovgSeqNum) + "','" + escapeStrings(sTransSeqNum) + "','" + escapeStrings(sCoverageKey) + "')}");//MITS:35039        //Ankit Start : Worked on MITS - 34297
                    }//rupal:end
                    //avipinsrivas start : Worked for Jira-340
                    //else if (string.Equals(sCodeTable.ToUpper(), Globalization.BUSINESS_ENTITY_ROLE.ToUpper()) || string.Equals(sCodeTable.ToUpper(), Globalization.PEOPLE_ENTITY_ROLE.ToUpper()))
                    //{
                    //    rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "')");
                    //    rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "')}");//MITS:35039
                    //}
                    //avipinsrivas End
                    else
                    {
                        rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "')");
                        rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "')}");//MITS:35039

                    }
                    // Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
                    Control cntrl = e.Row.FindControl("chkId_hidden");
                    Control cntrlID = e.Row.FindControl("chkcodeId_hidden");//mudabbir 36624
                    if (cntrl != null && cntrlID != null)
                    {
                        HiddenField field = (HiddenField)cntrl;
                        HiddenField codeID = (HiddenField)cntrlID;//mudabbir 36624
                        if ((string.Compare(sCodeTable, "ENTITY", true) == 0) || (string.Compare(sCodeTable, "states", true) == 0))
                        {
                            //rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "')");
                            field.Value = escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "," + sId;
                            codeID.Value = sId;//mudabbir 36624
                        }
                        else if ((string.Compare(sCodeTable, "TRANS_TYPES", true) == 0))//skhare7 27920
                        {
                            rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "_" + escapeStrings(sResstatus) + "')");
                            rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "_" + escapeStrings(sResstatus) + "')}");//MITS:35039
                            //MGaba2:MITS 28749
                            field.Value = escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "," + sId + "," + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "_" + escapeStrings(sResstatus);
                            codeID.Value = sId;//mmudabbir 36624
                        }
                        //rupal:start, policy system interface
                        else if ((string.Compare(sCodeTable, "COVERAGE_TYPE", true) == 0))
                        {
                            rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "','','" + escapeStrings(sCovgSeqNum) + "','" + escapeStrings(sTransSeqNum) + "','" + escapeStrings(sCoverageKey) + "')");//MITS:35039        //Ankit Start : Worked on MITS - 34297
                            rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "','','" + escapeStrings(sCovgSeqNum) + "','" + escapeStrings(sTransSeqNum) + "','" + escapeStrings(sCoverageKey) + "')}");//MITS:35039        //Ankit Start : Worked on MITS - 34297
                        }//rupal:end
                        //avipinsrivas start : Worked for Jira-340
                        //else if (string.Equals(sCodeTable.ToUpper(), Globalization.BUSINESS_ENTITY_ROLE.ToUpper()) || string.Equals(sCodeTable.ToUpper(), Globalization.PEOPLE_ENTITY_ROLE.ToUpper()))
                        //{
                        //    field.Value = escapeStrings(sDesc) + "," + sId + "," + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc);
                        //}
                        //avipinsrivas End
                        else
                        {
                            rowHyperLink.Attributes.Add("onclick", "javascript:selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "')");
                            rowHyperLink.Attributes.Add("onkeyup", "javascript:if(event.keyCode == 13) { selCode('" + escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "','" + sId + "','" + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc) + "')}");//MITS:35039
                            field.Value = escapeStrings(sShortCode) + " " + escapeStrings(sDesc) + "," + sId + "," + escapeStrings(sParentCode) + " " + escapeStrings(sParentDesc);
                            codeID.Value = sId;//mudabbir 36624
                        }

                    }
                    // End: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
                 rowHyperLink.Text = sDesc;
                 rowHyperLink.CssClass = "HeaderNavy";
                 // Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
                 e.Row.Cells[2].Controls.Add(rowHyperLink);
                 e.Row.Cells[2].CssClass = "Bold2";
            }
        }
        protected void dgCodes_PreRender(object sender, EventArgs e)
        {
            dgCodes.Columns[4].Visible = false;//skhare7 MITS 27920
            dgCodes.Columns[5].Visible = false;//rupal:policy system interface
            dgCodes.Columns[6].Visible = false;
            // Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 
            if (string.Compare(sCodeTable, "ENTITY", true) == 0)
            {
                dgCodes.Columns[1].Visible = false;
                dgCodes.Columns[3].Visible = false;
            }
            if (string.Compare(sCodeTable, "states", true) == 0)
            {
                dgCodes.Columns[3].Visible = false;
            }
            if(dgCodes.Rows.Count>0)
                dgCodes.Rows[dgCodes.Rows.Count-1].Visible = false;  //done to show grid if no codes are there in code table
                //mmudabbir start : mits 36624
                List<string> lst = new List<string>();
                string strtemp = string.Empty;
                strtemp = hdnMultiselect.Value;
                char[] delimiterChars = { '$' };
                if (!string.IsNullOrEmpty(strtemp))
                {
                    string[] rowvalues = strtemp.Split(delimiterChars);
                    string rowindex = string.Empty;
                    string pgNumber = string.Empty;
                    foreach (GridViewRow grdrow in dgCodes.Rows)
                    {
                        CheckBox chk = (CheckBox)grdrow.FindControl("chkId");
                        if (chk != null)
                        {
                            IDataItemContainer container = (IDataItemContainer)chk.NamingContainer;
                            var selectedKey = container.DataItemIndex;


                            foreach (string rowitem in rowvalues)
                            {
                                if (!string.IsNullOrEmpty(rowitem))
                                {
                                    char[] delimiterChars1 = { '|' };
                                    string[] items = rowitem.Split(delimiterChars1);
                                    pgNumber = items[0].ToString();
                                    rowindex = items[1].ToString();
                                    if (String.Compare(pgNumber, pagenumber.Value) == 0)
                                    {
                                        if (string.Compare(rowindex, selectedKey.ToString()) == 0)
                                        {
                                            chk.Checked = true;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                //Mudabbir ends
        }
        
        protected string escapeStrings( string sCode)
        {

            sCode = sCode.Replace("'", "\\'"); 
            return sCode;
        
        }
        //PJS : No sorting and client side paging is done as of now
        //protected void grdView_OnSorting(object sender, GridViewSortEventArgs e)
        //{
        //    m_strSortExp = e.SortExpression;
        //    dgCodes.DataSource = BindData(e.SortExpression);
        //    dgCodes.DataBind();  
        //}
        //PJS : No sorting and client side paging is done as of now
        //protected void grdView_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        //{
        //    CodesBusinessHelper cb = new CodesBusinessHelper();
        //    dgCodes.PageIndex = e.NewPageIndex;
        //    //List<CodeType> codes = cb.GetCode(Request.QueryString["codetype"]);
        //    dgCodes.DataSource = BindData(""); 
        //    dgCodes.DataBind();
        //}
        //private List<CodeType> BindData(string exp)
        //{
        //    CodeListType objList = new CodeListType();
        //    List<CodeType> codes = new List<CodeType>();
        //    CodesBusinessHelper cb = new CodesBusinessHelper();
        //    try
        //    {
        //        if (exp == hdnSortExpression.Value)
        //        {
        //            exp = (3 + Conversion.ConvertObjToInt(exp)).ToString();
        //        }

        //        if (exp != "")
        //            hdnSortExpression.Value = exp;
        //        else
        //            exp = hdnSortExpression.Value;


        //        if (Conversion.ConvertStrToInteger(exp) >= 1 && Conversion.ConvertStrToInteger(exp) <= 3)
        //        {
        //            m_SortDirection = "Ascending";
        //        }
        //        else
        //        {
        //            m_SortDirection = "Descending";
        //        }

        //        if (Page.PreviousPage != null)
        //        {
        //            exp = AppHelper.GetFormValue("hdnSortExpression");
        //            hdnSortExpression.Value = exp;
        //        }
        //        m_strSortExp = exp;

               
        //     objList = cb.GetCode(Request.QueryString["codetype"],pagenumber.Value.ToString() );
        //     codes = objList.Codes.ToList();   
        //    }

        //    catch (FaultException<RMException> ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        // ErrorControl1.errorDom = ee.Detail.Errors;

        //    }
        //    catch (Exception ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        //        // ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

        //    }
        //    return codes;
        //}
        //protected void GridView_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        if (String.Empty != m_strSortExp)
        //        {
        //            AddSortImage(e.Row);
        //        }
        //    }
        //}
        //void AddSortImage(GridViewRow headerRow)
        //{
        //    if ((Conversion.ConvertStrToInteger(m_strSortExp) >= 4))
        //        m_strSortExp = ((Conversion.ConvertStrToInteger(m_strSortExp) - 3)).ToString();
        //    Int32 iCol = GetSortColumnIndex(m_strSortExp);
        //    if (-1 == iCol)
        //    {
        //        return;
        //    }
        //    // Create the sorting image based on the sort direction.
        //    Image sortImage = new Image();
        //    if ("Ascending" == m_SortDirection)
        //    {
        //        sortImage.ImageUrl = "~/Images/arrow_down_white.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "~/Images/arrow_up_white.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }

        //    // Add the image to the appropriate header cell.
        //    headerRow.Cells[iCol].Controls.Add(sortImage);

        //}
        //private int GetSortColumnIndex(String strCol)
        //{
        //    foreach (DataControlField field in dgCodes.Columns)
        //    {
        //        if (field.SortExpression == strCol)
        //        {
        //            return dgCodes.Columns.IndexOf(field);
        //        }
        //    }

        //    return -1;
        //}
        //PJS : No sorting and client side paging is done as of now

        /// <summary>
        /// rsushilaggar 
        /// Modified the code to fix an issue in the Check Stub Text 
        /// MITS 21600 
        /// Date 23-Jul-2010
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCodes_DataBound(object sender, EventArgs e)
        {
            if (sShowCheckBox.ToLower() != "true")
            {
                dgCodes.Columns[0].HeaderStyle.CssClass = "hiderowcol";
                dgCodes.Columns[0].ItemStyle.CssClass = "hiderowcol";
                ////avipinsrivas start : Worked for Jira-340
                //if (string.Equals(sCodeTable.ToLower(), Globalization.BUSINESS_ENTITY_ROLE.ToLower()) || string.Equals(sCodeTable.ToLower(), Globalization.PEOPLE_ENTITY_ROLE.ToLower()))
                //{
                //    //****************Hide Short Code
                //    dgCodes.Columns[1].HeaderStyle.CssClass = "hiderowcol";
                //    dgCodes.Columns[1].ItemStyle.CssClass = "hiderowcol";
                //    //****************Hide Parent Code
                //    dgCodes.Columns[3].HeaderStyle.CssClass = "hiderowcol";
                //    dgCodes.Columns[3].ItemStyle.CssClass = "hiderowcol"; 
                //}
                ////avipinsrivas End
                btnOk.Visible = false;
                btnOk2.Visible = false;
            }
            else
            {
                //tanwar2 - JIRA: RMA-17976. Added if condition
                if (dgCodes.HeaderRow!=null)
                {
                    Control ctrl = dgCodes.HeaderRow.FindControl("chkAll");
                    if (ctrl != null)
                    {
                        CheckBox chkBox = (CheckBox)ctrl;
                        chkBox.Attributes.Add("onclick", "return CheckAllclicked('dgCodes_chkAll');");
                    }
                }
            }
        }

        //MITS:34257 - JIRA RMA-429 ajohari2 Start
        /// <summary>
        /// Function use to get claim type
        /// </summary>
        /// <param name="sPolicyLOB"></param>
        /// <param name="sFormName"></param>
        /// <param name="sFieldName"></param>
        /// <param name="sSessionLOB"></param>
        /// <param name="langCode"></param>
        /// <param name="sPolicyLobCode"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string GetClaimTypeByPolicyLOB(string sPolicyLOB, string sFormName, string sFieldName, string sSessionLOB, string langCode, string sPolicyLobCode)
        {

            CodesBusinessHelper objCb = new CodesBusinessHelper();
            CodeListType objListToGetClaimsType = null;
            string sLOB = sSessionLOB;
            string sSessionlob = sSessionLOB;
            string sFormname = sFormName;
            string code = "";
            string sJurisdiction = string.Empty;    //MITS#36929

            XElement oMessageElement = null;
            XElement oClaimTypeCode = null;
            XElement oClaimTypeId = null;
            XElement oPolicyLobId = null;
            XElement oPolicyLobCode = null;

            try
            {
                oMessageElement = GetMessageTemplateDataForClaimType();

				//sJurisdiction added by swati for MITS # 36929
                //objListToGetClaimsType = objCb.GetCode(string.Empty, "CLAIM_TYPE", sLOB, string.Empty, sFormname, string.Empty, string.Empty, string.Empty, sSessionlob, string.Empty, string.Empty, "", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, sPolicyLOB, string.Empty, AppHelper.GetQueryStringValue("RsvStatusParent"), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);    //Get Claim Type
				objListToGetClaimsType = objCb.GetCode(string.Empty, "CLAIM_TYPE", sLOB, sJurisdiction, string.Empty, sFormname, string.Empty, string.Empty, string.Empty, sSessionlob, string.Empty, string.Empty, "", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, sPolicyLOB, string.Empty, AppHelper.GetQueryStringValue("RsvStatusParent"), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,string.Empty);    //Get Claim Type

                if (objListToGetClaimsType != null)
                {
                    if (objListToGetClaimsType.RecordCount == "1")
                    {
                        code = objListToGetClaimsType.Codes[0].ShortCode + " " + objListToGetClaimsType.Codes[0].Desc;
                        oClaimTypeCode = oMessageElement.XPathSelectElement("./Document/ClaimType/Code");
                        oClaimTypeCode.Value = code;

                        oClaimTypeId = oMessageElement.XPathSelectElement("./Document/ClaimType/Id");
                        oClaimTypeId.Value = Convert.ToString(objListToGetClaimsType.Codes[0].Id);
                    }
                }

                oPolicyLobId = oMessageElement.XPathSelectElement("./Document/ClaimType/PolicyLobId");
                oPolicyLobId.Value = sPolicyLOB;

                oPolicyLobCode = oMessageElement.XPathSelectElement("./Document/ClaimType/PolicyLobCode");
                oPolicyLobCode.Value = sPolicyLobCode;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
            }

            return oMessageElement.ToString();

        }

        /// <summary>
        /// Message template
        /// </summary>
        /// <returns></returns>
        private static XElement GetMessageTemplateDataForClaimType()
        {
            XElement oTemplate = XElement.Parse(@"
                <Message>
                    <Document>
                        <ClaimType>
                            <Code></Code> 
                            <Id></Id>
                            <PolicyLobCode></PolicyLobCode>
                            <PolicyLobId></PolicyLobId>
                         </ClaimType>
                    </Document>
                </Message>
            ");

            return oTemplate;
        }
        //MITS:34257 - JIRA RMA-429 ajohari2 End


        // Mudabbir added for  mits 36624 
        public string StoreCheckID(GridView dg_codes,string hdnvalues,string page_number)
        {
            try
            {
                string strmulti = string.Empty;
                string sID = string.Empty;
                bool bvalue = false;
                if (!string.IsNullOrEmpty(hdnvalues))
                {
                    strmulti = hdnvalues;
                }
                
                char[] delimiterChars = { '$'};
                
                
                    string[] rowvalues = strmulti.Split(delimiterChars);
                    foreach (GridViewRow row in dg_codes.Rows)
                    {
                        CheckBox chk = (CheckBox)row.FindControl("chkId");
                        IDataItemContainer container = (IDataItemContainer)chk.NamingContainer;

                        var selectedKey = container.DataItemIndex;

                            HiddenField hdn_chdIdcntrl = (HiddenField)row.FindControl("chkId_hidden");
                            HiddenField hdn_chdcodeIdcntrl = (HiddenField)row.FindControl("chkcodeId_hidden");
                            if (hdn_chdIdcntrl != null && hdn_chdcodeIdcntrl != null)
                            {
                                if (rowvalues.Length > 0)
                                {
                                    foreach (string s in rowvalues)
                                    {
                                        if (!string.IsNullOrEmpty(s))
                                        {
                                            char[] delimiterChars1 = { ',' };
                                            string[] csv = s.Split(delimiterChars1);
                                            if (chk.Checked)
                                            {
                                                if (!csv[1].Contains(hdn_chdcodeIdcntrl.Value))
                                                {
                                                    bvalue = true;
                                                }
                                                else
                                                {
                                                    bvalue = false;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (csv[1].Contains(hdn_chdcodeIdcntrl.Value))
                                                {
                                                    string temp = string.Empty;
                                                    temp = page_number + "|" + selectedKey.ToString() + "|" + hdn_chdIdcntrl.Value + "$";
                                                    strmulti =  strmulti.Replace(temp, "");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (chk.Checked)
                                                bvalue = true;
                                        }
                                    }
                                }
                                else
                                {
                                    bvalue = false;
                                }

                                if (bvalue)
                                {
                                    strmulti = strmulti + page_number + "|" + selectedKey.ToString() + "|" + hdn_chdIdcntrl.Value + "$";
                                    bvalue = false;
                                }
                            }
                    }
                
                return strmulti;
            }
               
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                return string.Empty;
            }         
        }
    }

}


