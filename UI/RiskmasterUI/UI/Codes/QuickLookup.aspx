<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuickLookup.aspx.cs" Inherits="Riskmaster.UI.Codes.QuickLookup" %>
<%@ OutputCache CacheProfile="RMXCodeCacheProfile" %>

<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc3" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Quick Lookup Results</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/getcode.js"></script>
    <script type="text/javascript" language="javascript">
        //srajindersin MITS 26940 dt 04/23/2012
        function getNextPage(lPageNumber) {
            var pageurl = document.getElementById('pageaction').value;
            var pagenumber = document.getElementById('hdPageNumber').value;
            var totalpages = document.getElementById('hdTotalNumberOfPages').value;

            if (lPageNumber == "FIRST")
                document.forms[0].action = pageurl; 
            else if (lPageNumber == "NEXT") {
                pagenumber = parseInt(pagenumber) + 1;
                document.forms[0].action = pageurl + "&PageNumber=" + pagenumber;
            }
            else if (lPageNumber == "PREV") {
                pagenumber = parseInt(pagenumber) - 1;
                document.forms[0].action = pageurl + "&PageNumber=" + pagenumber;
            }
            else if (lPageNumber == "LAST") {
                document.forms[0].action = pageurl + "&PageNumber=" + totalpages;
            }
        }
        //END srajindersin MITS 26940 dt 04/23/2012
        //RMA-6871
        function AddNewItem() {
            var IsERSysEnabled = document.getElementById('hdnIsEntityRoleEnabled').value;
            var encodedLookupstring = document.getElementById('lookupstring').value.replace(/&#39;/g, '\'').replace(/&quot;/g, '"');    //tkatsarski: 09/29/15 - RMA-10824
            if (IsERSysEnabled != "true") {
                var rdSelected = document.getElementsByName('rdtypelist2');
                if (rdSelected.length == 0) rdSelected = document.getElementsByName('rdtypelist1');
                var objItem = 'P';
                if (!rdSelected[0].checked) objItem = 'E';
                window.opener.AddNewItem(encodedLookupstring, IsERSysEnabled, objItem);
            } else { window.opener.AddNewItem(encodedLookupstring, IsERSysEnabled); }
        }
        function onSelect() {
            if (document.forms[0].cmdSubmit != null) {
                document.forms[0].cmdSubmit.click();
            }
            else if (document.forms[0].cmdSubmit1 != null) {
                document.forms[0].cmdSubmit1.click();
            } else if (document.forms[0].cmdSubmit2 != null) {
                document.forms[0].cmdSubmit2.click();
            } else if (document.forms[0].cmdSubmit3 != null) {
                document.forms[0].cmdSubmit3.click();
            }
        }
        //RMA-6871
    </script>
</head>
<body onload="formOnload();isEditable();RestorePrevValue();" onunload="handleUnload();">
    <form id="frmData" onsubmit="return FormSubmit(event);" runat="server" >
        <asp:HiddenField ID ="pageaction" runat ="server" /> <!--srajindersin MITS 26940 dt 04/23/2012 -->
        <input type="hidden" id="AddNewSetting"/>
        <input type="hidden" id="EditSetting"/> <!-- pmittal5 Mits 18137 - Auto fill initials for patient in case of "Edit"-->
		<input type="hidden" id="lookuptype" value="<%=Request.QueryString["codetype"]%>"/>
		<input type="hidden" id="resultid1" value="<%=Request.QueryString["lob"]%>"/>
		<input type="hidden" id="resulttext" runat ="server"/>
        <!-- zmohammad JIRa 6284 : Fix for Policy lookups -->
        <input type="hidden" id="hdnTransSeqNum" runat ="server"/>
        <input type="hidden" id="hdnCovgKey" runat ="server"/>
        <input type="hidden" id="hdnCovSeqNum" runat ="server"/>
		<table width="95%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <%if ((iCount > 1) || (iCount == 1 && Request.QueryString["creatable"] == "1" && AppHelper.GetQueryStringValue("codetype") != "employee") || (iCount == 1 && AppHelper.GetQueryStringValue("codetype") == "employee" && bNotExactMatch))  //pmittal5 Mits 15308// smishra25:MITS 15343
        {%>	
            <table align="center" width="100%" border="0">
	            <tr>
		            <td width="100%" valign="middle" align="center">
			            <font face="Verdana, Arial" size="2">
			            <b><asp:Label ID="lblCriteria" runat="server" Text=" <%$ Resources:lblCriteria %>"></asp:Label> '<%=AppHelper.HTMLCustomEncode(Request.QueryString["lookupstring"])%>'</b>
			            </font>
		            </td>
	            </tr>
                <tr>
            <td align="left" colspan="2">
            <!--srajindersin MITS 26940 dt 04/23/2012 getNextPage() methond added for paging & caching-->
                <asp:LinkButton ID="lnkFirst" Text="<%$ Resources:lnkFirst %>" runat="server" OnClick="lnkFirst_Click" OnClientClick="getNextPage('FIRST');"></asp:LinkButton>
                <asp:LinkButton ID="lnkPrev" Text="<%$ Resources:lnkPrev %>" runat="server" OnClick="lnkPrev_Click" OnClientClick="getNextPage('PREV');"></asp:LinkButton>
                <asp:LinkButton ID="lnkNext" Text="<%$ Resources:lnkNext %>" runat="server" OnClick="lnkNext_Click" OnClientClick="getNextPage('NEXT');"></asp:LinkButton>
                <asp:LinkButton ID="lnkLast" Text="<%$ Resources:lnkLast %>" runat="server" OnClick="lnkLast_Click" OnClientClick="getNextPage('LAST');"></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b><asp:Label ID="lblPageDetails" runat="server"></asp:Label></b>
            </td>
        </tr>
	            <tr>
		            <td width="100%" valign="middle" align="center">
        	            
    	                    
		                        <select id="optResults" size="10" style="width: 100%" onkeydown="ResultsKeyDown();" ondblclick="onSelect()">
		                         <% for (int i = 0; i < codes.Count;i++ )
                                 { %>
                                     <%if(codetype.Value =="code.COVERAGE_TYPE") 
                                    { %>
		                                <option value="<%=codes[i].Id %>_<%=codes[i].parentText%>_<%=codes[i].CovgSeqNum%>_<%=codes[i].TransSeqNum%>_<%=codes[i].CoverageKey%>"><%=codes[i].Desc%></option>
		                            <%}%>
                                    <%else 
                                      {%>
                                        <option value="<%=codes[i].Id %>_<%=codes[i].parentText%>_<%=codes[i].ErRowID%>"><%=codes[i].Desc%></option>
    		                        <%}%>
		                        <%}%>
		                        </select>
			                    <br/>
			                    <br />
			                    &nbsp;<%if (Request.QueryString["creatable"] == "1")
                         {
                             //npadhy MITS 14527 EditCommandColumn Option in Employee QuickLookup
                             //RMA-6871
                             if (Request.QueryString["sysFormName"] == "funds" || Request.QueryString["sysFormName"] == "autoclaimchecks" || Request.QueryString["sysFormName"] == "thirdpartypayments")
                          {
                              if (hdnIsEntityRoleEnabled.Value != "true")
                              {%>
                    <asp:RadioButtonList class="radiolabel" ID="rdtypelist1" runat="server" rmxref="/Instance/Document/type" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P" Selected="True" Text="<%$ Resources:lblPeople %>"></asp:ListItem>
                        <asp:ListItem Value="E" Text="<%$ Resources:lblEntity %>"></asp:ListItem>
                    </asp:RadioButtonList><%}%>
			                    <input type="submit" class="button" runat="server" id="cmdSubmit1" value="<%$ Resources:btncmdSubmit %>"/>
                                <input class="button" type="button" runat="server" id="cmdKeep1" name="cmdKeep1" rmxref="/Instance/Document/action" value=" <%$ Resources:btncmdAddNew %> " onclick="AddNewItem(); window.close();" />
                    <%}
                          else
                          {%>
                        <input type="submit" class="button" id="cmdSubmit2" runat="server" value="<%$ Resources:btncmdSubmit %>"/>
                        <%if (Request.QueryString["isEditableFlag"] == "1")
                                        {
                                            %><input class="button" type="button" name="cmdEdit1" runat="server" id="cmdEdit1" value="<%$ Resources:btncmdEdit %>" onclick="setEditSetting(); window.opener.RequestRestore(window.document.forms[0].resulttext.value); window.close();" />
                    <%}  
                    if(Request.QueryString["isEditableFlag"] == "0" || Request.QueryString["isRestrictedAddNew"] == "false")
                     { %>
                    <input class="button" type="button" runat="server" id="cmdKeep2" name="cmdKeep2" rmxref="/Instance/Document/action" value=" <%$ Resources:btncmdAddNew %> " onclick="setAddNewValue(); window.opener.RequestRestore(window.document.forms[0].resulttext.value); ClearAutoFilled(); window.close();" />
                    <%}}
                      }else{ %>
                        <input type="submit" class="button" id="cmdSubmit3" name="cmdSubmit3" runat="server" value="<%$ Resources:btncmdSubmit %>"/><%} //RMA-6871 %>
                    <input class="button" type="button" id="cmdCancel" name="cmdCancel3" runat="server" value="<%$ Resources:btncmdCancel %>" onclick="window.opener.RequestCancel(); self.close();" />
                </td>
                </tr>
            </table>
            <%} %>
            <%if (iCount == 0)
            {%>
        <!--Mits 31267/35240  Ashish Sharma : Starts-->
        <script type="text/javascript">
            if (window.opener.document.getElementById('pye_lastname') != undefined) {
                if (window.opener.document.getElementById('pye_lastname').cancelledvalue != undefined && window.opener.document.getElementById('pye_lastname').cancelledvalue != "") {
                    window.opener.document.getElementById('pye_lastname').value = window.opener.document.getElementById('pye_lastname').cancelledvalue;
                }
                else if (window.opener.document.getElementById('pye_lastname').defaultValue != undefined && window.opener.document.getElementById('pye_lastname').defaultValue != "") {
                    window.opener.document.getElementById('pye_lastname').value = window.opener.document.getElementById('pye_lastname').defaultValue;
                }
                else if (window.opener.document.getElementById('pye_lastname').cancelledvalue == "")
                    window.opener.document.getElementById('pye_lastname').value = "";
            }
        </script>
        <!--Mits 31267 Ashish Sharma : Ends-->
        <input type="hidden" id="noresult" value="1" />
        <br />
        <br />
            <table align="center" width="100%" border="0">
			    <tr>
				    <td width="100%" valign="middle" align="center">
                        <font face="Verdana, Arial" size="4">
						    <b><asp:Label ID="lblZeroRecord" runat="server" Text=" <%$ Resources:lblZeroRecord %>"></asp:Label> '<%=AppHelper.HTMLCustomEncode(Request.QueryString["lookupstring"])%>' </b>   
					    </font>
				    </td>
			    </tr>
			    <tr>
                    <td width="100%" valign="middle" align="center">
                    <%if (Request.QueryString["creatable"] == "1")
                      {
                          //npadhy MITS 14527 EditCommandColumn Option in Employee QuickLookup
                          //RMA-6871
                           if (Request.QueryString["sysFormName"] == "funds" || Request.QueryString["sysFormName"] == "autoclaimchecks" || Request.QueryString["sysFormName"] == "thirdpartypayments")
                          {
                              if (hdnIsEntityRoleEnabled.Value != "true")
                              {%>
                    <asp:RadioButtonList class="radiolabel" ID="rdtypelist2" runat="server" rmxref="/Instance/Document/type" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P" Selected="True" Text="<%$ Resources:lblPeople %>"></asp:ListItem>
                        <asp:ListItem Value="E" Text="<%$ Resources:lblEntity %>"></asp:ListItem>
                    </asp:RadioButtonList>
                    <%}%>                        
                    <input class="button" type="button" name="cmdKeep3" runat="server" id="cmdKeep3" rmxref="/Instance/Document/action"  value=" <%$ Resources:btncmdAddNew %> " onclick="AddNewItem(); window.close();" />
                    <%}
                          else
                          {
                         if (Request.QueryString["isEditableFlag"] == "1")
						{ %>
                    <input class="button" type="button" name="cmdEdit2" runat="server" id="cmdEdit2" value="<%$ Resources:btncmdEdit %>" onclick="setEditSetting(); window.opener.RequestRestore(window.document.forms[0].resulttext.value); window.close();" />
                    <%}
                        if(Request.QueryString["isEditableFlag"] == "0" || Request.QueryString["isRestrictedAddNew"] == "false")
                            {
                            %>
                    <input class="button" type="button" name="cmdKeep4" runat="server" id="cmdKeep4" rmxref="/Instance/Document/action" value=" <%$ Resources:btncmdAddNew %> " onclick="setAddNewValue(); window.opener.RequestRestore(window.document.forms[0].resulttext.value); ClearAutoFilled(); window.close();" />
                    <%}}
                      } //RMA-6871%>
                    <!--Geeta : Code was not able to reset the window opener field to blank if record does not match-->
                    <input class="button" type="button" id="cmdClose" runat="server"  name="cmdClose" onclick="window.opener.RequestCancel(); handleUnload(); window.close();" value="<%$ Resources:btncmdClose %>" />
                </td>
			    </tr>
		    </table> 					
	       <%} %>
	       <%if ((iCount == 1 && Request.QueryString["creatable"] != "1" && AppHelper.GetQueryStringValue("codetype") != "employee") || (iCount == 1 && AppHelper.GetQueryStringValue("codetype") == "employee" && !bNotExactMatch))  //pmittal5 Mits 15308// smishra25:MITS 15343
           {%>	
            <div>  
     	        <br />
		        <br />
		        <br />
		        <br />
		        <table align="center" width="100%" border="0">
			        <tr>
				        <td valign="center" align="center">
					        <img src="../../Images/loading1.gif"/>
				        </td>
				        <%--<td width="100%" valign="middle" align="center">
					        <font face="Arial, Helvetica" size="7">
						        <b>Please wait...</b>
					        </font>
				        </td>--%>
			        </tr>
		        </table>
			        <input type="hidden" id="resultid" value='<%=codes[0].Id%>_<%=codes[0].parentText%>' />

			        
	        </div>		
		    <%}%>
                        <input type="hidden" id="hdTotalNumberOfPages" runat="server" />
        <input type="hidden" id="hdPageNumber" runat="server" />
        <input type="hidden" id="codetype" runat="server" />
        <input type="hidden" id="lookupstring" runat="server" />
        <input type="hidden" id="descSearch" runat="server" />
        <input type="hidden" id="filter" runat="server" />
        <input type="hidden" id="lob" runat="server" />
        <input type="hidden" id="orgeid" runat="server" />
        <input type="hidden" id="formname" runat="server" />
        <input type="hidden" id="triggerdate" runat="server" />
        <input type="hidden" id="eventdate" runat="server" />
        <input type="hidden" id="sessionlob" runat="server" />
        <input type="hidden" id="Title" runat="server" />
        <input type="hidden" id="sessionclaimid" runat="server" />
        <input type="hidden" id="orgLevel" runat="server" />
        <input type="hidden" id="insuredeid" runat="server" />
        <input type="hidden" id="parentcodeid" runat="server" />
        <input type="hidden" id="eventid" runat="server" />
        <input type="hidden" id="policyid" runat="server" />
        <input type="hidden" id="covtypecodeid" runat="server" />
        <input type="hidden" id="losstypecodeid" runat="server" />      <!--Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)-->
        <input type="hidden" id="transid" runat="server" />
        <input type="hidden" id="claimanteid" runat="server" />
        <!--rupal:start, r8 first & final pmt-->
        <input type="hidden" id="IsFirstFinal" runat="server" />
        <!--rupal:end, r8 first & final pmt-->
        <input type="hidden" id="PayCol" runat="server" />
        <input type="hidden" id="PolicyLOB" runat="server" />
        <input type="hidden" id="CovgSeqNum" runat="server" />
          <input type="hidden" id="RsvStatusParent" runat="server" />
          <input type="hidden" id="TransSeqNum" runat="server" />
          <input type="hidden" id="CoverageKey" runat="server" />
           <input type="hidden" id="FieldName" runat="server" /> <!-- igupta3 MITS 36575  -->
		   <!--added by swati MITS # 36929-->
        <input type="hidden" id="jurisdiction" runat="server" />
        <input type="hidden" id="hdnIsEntityRoleEnabled" runat="server" />
    </form>
</body>

    	
		
</html>
