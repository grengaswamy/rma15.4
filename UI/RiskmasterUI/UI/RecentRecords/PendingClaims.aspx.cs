﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.Script.Services;
using Riskmaster.Common.Extensions;
using Riskmaster.BusinessHelpers;
using Riskmaster.RMXResourceManager;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Text;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.Web;
using Riskmaster.Common;

namespace Riskmaster.UI.UI.RecentRecords
{
    public partial class PendingClaims : NonFDMBasePageCWS
    {
        //set default user name and user id
        public string m_UserName = "";
        int m_UserId = -1;
        bool m_showAll = false;
        static string sSuccess = "Success";
        bool bAjaxFlag = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Browser != null && String.Compare(HttpContext.Current.Request.Browser.Browser, "IE", true) == 0
                                   && HttpContext.Current.Request.Browser.MajorVersion < 10)
            {

                lblErrors.Text = RMXResourceProvider.GetSpecificObject("lblBrowsercheckError", RMXResourceProvider.PageId("PendingClaims.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                lblErrors.Visible = true;
                Browsercheck.Attributes.Add("style", "display:none");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "DisbaleWaitDialog", "DisbaleWaitDialog()", true);
                return;
            }
            lblErrors.Visible = false;

            GetPendingClaimData();
            //Browsercheck.Attributes.Add("style", "display:");


            if (!IsPostBack)
            {

                string strValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PendingClaims.aspx"), "CreatePendingClaimsLabel", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CreatePendingClaimsLabel", strValidationResources, true);
                string strToolTipResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PendingClaims.aspx"), "CreatePendingClaimsTT", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CreatePendingClaimsTT", strToolTipResources, true);

                string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);

                string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

                string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);


            }

        }
        
       // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        private void GetPendingClaimData()
        {
            XmlDocument resultdoc = new XmlDocument();

            XElement messageElement;
            string sCWSresponse = "";
            XmlElement xmlIn;

            XmlDocument ddDataDoc = BindDropDown();
            xmlIn = (XmlElement)ddDataDoc.SelectSingleNode("//displaycolumn//control");
            if (xmlIn != null)
                hdndropdownData.Value = xmlIn.InnerText;


            //Bind Data with hidden fields
           string sReturn = AppHelper.CallCWSService(GetMessageTemplate("-1", false).ToString());
            if (!string.IsNullOrEmpty(sReturn))
            {
                resultdoc = new XmlDocument();
                resultdoc.LoadXml(sReturn);
            }

            xmlIn = (XmlElement)resultdoc.SelectSingleNode("//pendingclaims//Data");
            if (xmlIn != null)
                hdnJsonData.Value = xmlIn.InnerText;
            xmlIn = (XmlElement)resultdoc.SelectSingleNode("//pendingclaims//UserPref");
            if (xmlIn != null)
                hdnJsonUserPref.Value = xmlIn.InnerText;
            xmlIn = (XmlElement)resultdoc.SelectSingleNode("//pendingclaims//AdditionalData");
            if (xmlIn != null)
                hdnJsonAdditionalData.Value = xmlIn.InnerText;

        }

        /// <summary>
        /// Returns list of reportess in xml format
        /// </summary>
        /// <returns></returns>
        private XmlDocument BindDropDown()
        {
            string oMessageElement = string.Empty;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;
            DataSet resultDataSet = null;
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sreturnValue = string.Empty;

            try
            {
                oMessageElement = GetDropDownTemplate().ToString();
                sReturn = AppHelper.CallCWSService(oMessageElement);
                if (!string.IsNullOrEmpty(sReturn))
                {
                    resultDoc = new XmlDocument();
                    resultDoc.LoadXml(sReturn);

                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            return resultDoc;
        }

        /// <summary>
        /// provides template for Drop down list
        /// </summary>
        /// <returns></returns>
        private XElement GetDropDownTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>PendingClaimsAdaptor.GetDDItems</Function> 
                    </Call>
                    <Document>
                        <BindDDList>true</BindDDList>
                    </Document>
            </Message>
            ");
            return oTemplate;
        }

        private static XElement GetMessageTemplate(string userId, bool showAll)
        {
            string inxml = @"<Message>
                            <Authorization>
                                </Authorization>
                            <Call>
                                    <Function>PendingClaimsAdaptor.GetPendingClaims</Function>
                                </Call>
                            <Document>
                                    <elements>
                                        <showAll>" + showAll + @"</showAll>
                                        <pagenumber>1</pagenumber>                
                                        <rowsperpage>1</rowsperpage>                
                                        <userId>" + userId + @"</userId>
                                    </elements>
                                </Document>
                            </Message>";
            XElement oTemplate = XElement.Parse(inxml);
            return oTemplate;
        }


        [System.Web.Services.WebMethod]
        public static string SavePreferences(string gridPreference, string gridId)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            string sReturnResponse = "";
            Dictionary<string, string> dicResponse = new Dictionary<string, string>();
            try
            {   //remove hyper link cell template from columns while save 
                Dictionary<string, object> l_dict = new Dictionary<string, object>();
                JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(gridPreference)), l_dict);
                List<Dictionary<string, object>> l_dict2 = new List<Dictionary<string, object>>();
                JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(l_dict["colDef"].ToString())), l_dict2);
                foreach (var item in l_dict2)
                {
                    item["cellTemplate"] = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD }}</span></div>";
                }
                l_dict["colDef"] = l_dict2;
                gridPreference = JsonConvert.SerializeObject(l_dict);

                gridPreference = AppHelper.HtmlEncodeString(gridPreference);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>GridPreferenceAdaptor.SaveUserHeaderAndPreference</Function>");
                sbMessageTemplate.Append("</Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<PendingClaims>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(gridId);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>PendingClaims.aspx</PageName>");
                sbMessageTemplate.Append("<UserPref>" + gridPreference + "</UserPref>");
                sbMessageTemplate.Append("</PendingClaims>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString()); //return bool.   
                XmlDocument objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(sReturn);


                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    dicResponse.Add("error", "true");
                    dicResponse.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
                }
                //else                    
                //    dicResponse.Add("response", sSuccess);                
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            catch (Exception ex)
            {
                dicResponse.Add("error", "true");
                dicResponse.Add("errorMessage", ex.Message);
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            finally
            {
                sbMessageTemplate = null;
            }
            return sReturnResponse;

        }

        [System.Web.Services.WebMethod]
        public static string RestoreDefault(string inputData)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            XmlDocument objXmlDoc = new XmlDocument();
            Dictionary<string, string> dicInputData;
            string sResponseData = "";
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData=new Dictionary<string,string>();
            try
            {
                dicInputData = new Dictionary<string, string>();
                dicResponseData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(inputData, dicInputData);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>PendingClaimsAdaptor.RestoreDefaults</Function>");
                sbMessageTemplate.Append(" </Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<RestoreDefault>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(dicInputData["GridId"]);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>PendingClaims.aspx</PageName>");
                sbMessageTemplate.Append("</RestoreDefault>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString());
                objXmlDoc.LoadXml(sReturn);
                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    xmlIn = (XmlElement)objXmlDoc.SelectSingleNode("//UserPref");
                    if (xmlIn != null)
                    {
                        sResponseData = xmlIn.InnerText;
                    }
                }
                else
                {//error handling
                    dicResponseData.Add("error", "true");
                    dicResponseData.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
                }
            }
            catch (Exception ex)
            {
                dicResponseData.Add("error", "true");
                dicResponseData.Add("errorMessage", ex.Message);
                sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
            }

            finally
            {
                sbMessageTemplate = null;
                objXmlDoc = null;
                dicInputData = null;
                xmlIn = null;
                dicResponseData = null;
            }
            return sResponseData;
        }

        /// <summary>
        /// this is a sample function which should be used to bind the grid on post backs
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string BindDataToGrid(string userId, string showAll)
        {


            XmlDocument resultDoc = new XmlDocument();
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData;
            Dictionary<string, string> dicInputData;
            XElement messageElement;
            string sCWSresponse = "";

            try
            {
                dicResponseData = new Dictionary<string, string>();
                dicInputData = new Dictionary<string, string>();

                messageElement = GetMessageTemplate(userId, Convert.ToBoolean(showAll));
                sCWSresponse = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc.LoadXml(sCWSresponse);
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//pendingclaims//Data");
                resultDoc.LoadXml(sCWSresponse);
                if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    dicResponseData.Add("response", sSuccess);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//pendingclaims//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("data", xmlIn.InnerText);
                }

                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//pendingclaims//UserPref");
                if (xmlIn != null)
                {
                    Dictionary<string, object> l_dict = new Dictionary<string, object>();
                    JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(xmlIn.InnerText)), l_dict);
                    Dictionary<string, object> l_dict1 = new Dictionary<string, object>();
                    JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(l_dict["AdditionalUserPref"].ToString())), l_dict1);
                    l_dict1["showAll"] = Convert.ToBoolean(showAll);
                    //string userPrefxml1 = JsonConvert.SerializeObject(l_dict1);
                    l_dict["AdditionalUserPref"] = l_dict1;
                    //l_dict["AdditionalUserPref"] = "{\"showAll\":" + showAll + "}";
                    //l_dict["AdditionalUserPref"] =(Newtonsoft.Json.Linq.JObject)({  "showAll": false});
                  
                  ////////  l_dict["AdditionalUserPref"] = (Newtonsoft.Json.Linq.JValue)("{\"showAll\":" + showAll + "}");
                string userPrefxml=  JsonConvert.SerializeObject(l_dict);
                   // dicResponseData.Add("userPref", xmlIn.InnerText);
                     dicResponseData.Add("userPref",userPrefxml);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//pendingclaims//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("additionalData", xmlIn.InnerText);
                }
                return JsonConvert.SerializeObject(dicResponseData);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                resultDoc = null;
                xmlIn = null;
                dicResponseData = null;
                dicInputData = null;
                messageElement = null;
            }

        }

    }
}
