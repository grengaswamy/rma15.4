﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.UI.RecentRecords
{
    public partial class RecentRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindGridWithRecentRecords();
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void grdRecentEvents_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Text = "<a href='#'>" + e.Row.Cells[0].Text + "</a>";
                e.Row.Cells[0].Attributes.Add("onclick", "parent.MDIShowScreen('" + DataBinder.Eval(e.Row.DataItem, "event_id").ToString() + "','event');return false;");
            }
        }

        private void BindGridWithRecentRecords()
        {
            string oMessageElement = string.Empty;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;
            DataSet resultDataSet = null;

            try
            {
                oMessageElement = GetMessageTemplate().ToString();
                sReturn = AppHelper.CallCWSService(oMessageElement);

                if (sReturn != string.Empty)
                {
                    resultDoc = new XmlDocument();
                    resultDoc.LoadXml(sReturn);

                    resultDataSet = ConvertXmlDocToDataSet(resultDoc);

                    if (resultDataSet != null)
                    {
                        if (resultDataSet.Tables.Count >= 3)
                        {
                            grdRecentEvents.DataSource = resultDataSet.Tables[3];
                            grdRecentEvents.DataBind();
                        }
                    }
                }
                else
                {
                    throw new ApplicationException("Error occured while getting Recent Events");
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>RecentRecordsAdaptor.GetRecentSavedRecords</Function> 
                    </Call>
                    <Document>
                        <recordtype>event</recordtype>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }

        private DataSet ConvertXmlDocToDataSet(XmlDocument resultDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(resultDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindGridWithRecentRecords();
        }
    }
}
