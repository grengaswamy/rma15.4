﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Text;
using System.Collections;



namespace Riskmaster.UI.UI.RecentRecords
{
    public partial class Weblinknavigate : NonFDMBasePageCWS
    {
           
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            string sRowId = null;
            string sScript = null;
            string[] sFormname;
            string[] sRecordId;
            string sScreenInfo = null;
            char GroupSeparator = (char)29;
            char UnitSeparator = (char)31;
            XmlDocument objXml = null;


            try
            {
                if (IsPostBack)
                {
                    sRowId = AppHelper.GetQueryStringValue("RowId");

                    TextBox txtScreeninfo = (TextBox)this.Form.FindControl("tbScreenInfo");
                    if (txtScreeninfo != null)
                    {
                        sScreenInfo = txtScreeninfo.Text;
                    }

                    TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
                    if (txtRowId != null)
                    {
                        txtRowId.Text = sRowId;
                    }

                    sFormname = sScreenInfo.Split(GroupSeparator);
                    sRecordId = sFormname[0].Split(UnitSeparator);
                    sFormname = sFormname[1].Split(UnitSeparator);
               
                    if (sFormname.Length > 0)
                        tbFormName.Text = sFormname[0];

                    tbRecordId.Text = sRecordId[sRecordId.Length - 1];


                    bReturnStatus = CallCWSFunctionBind("WebLinkAdaptor.LoadUrl", out sreturnValue);
                    objXml = new XmlDocument();
                    objXml.LoadXml(sreturnValue);


                    if (bReturnStatus && !(objXml.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error"))
                    {
                        hdnDoRedirect.Value = "true";
                        hdnDoPostBack.Value = "false";
                      
                    }
                    else
                    {
                        hdnDoPostBack.Value = "false";
                        hdnDoRedirect.Value = "false";
                    }
                }
               
            }
            catch (Exception ee)
            {
                hdnDoPostBack.Value = "false";
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            
        }

}
}