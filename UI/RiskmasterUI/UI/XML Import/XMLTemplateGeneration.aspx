﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="XMLTemplateGeneration.aspx.cs" Inherits="Riskmaster.UI.UI.XML_Import.XMLTemplateGeneration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="dg" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Import XML Template Generation</title>
    <link href="../../../../Content/zpcal/themes/system.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/cul.js" type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 330px;
        }
    </style>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="msgheader">
        Import XML Template Generation</div>
    <table>
    <tr>
        <td>
             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                function OnTreeClick(Check_Event) {
                    var objElement;
                    try {
                        // Get the element which fired the event.
                        objElement = window.event.srcElement;
                    }
                    catch (Error) {
                        //srcElement is failing, objElement is null.
                    }

                    if (objElement != null) {
                        // If the element is a checkbox do postback.
                        if (objElement.tagName == "INPUT" && objElement.type == "checkbox") {
                            __doPostBack("TreeView1", "");
                        }
                        
                    }
                    else {
                        //    If the srcElement is failing due to browser incompatibility determine
                        // whether the element is and HTML input element and do postback.
                        if (Check_Event != null) {
                            if (Check_Event.target.toString() == "[object HTMLInputElement]") {
                                __doPostBack("TreeView1", "");
                            }
                        }
                    }
                    return false;
                }

                
            </script>
            
            <div>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server" Text="Please select the type of records to be imported:"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton GroupName="RecordType" runat="server" ID="rdDataEntry" Text="Events and Claims" OnCheckedChanged="Radio_CheckedChanged" AutoPostBack="true"/>
                            <asp:RadioButton GroupName="RecordType" runat="server" ID="rdAttachments" Text="Attachments" OnCheckedChanged="Radio_CheckedChanged" AutoPostBack="true"/>
                            <asp:RadioButton GroupName="RecordType" runat="server" ID="rdWPADiaries" Text="WPA Diaries" OnCheckedChanged="Radio_CheckedChanged" AutoPostBack="true"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height:400px;width:400px;overflow:auto;" class="divScroll" >
                <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="all" ShowLines="true"
                    OnSelectedNodeChanged="TreeView1_SelectedNodeChanged" ShowExpandCollapse="true"
                    SelectedNodeStyle-Font-Bold="true" SelectedNodeStyle-BackColor="AliceBlue" OnTreeNodeCheckChanged="TreeView1_TreeNodeCheckChanged">
                    <Nodes>
                        <asp:TreeNode Text="Import" Value="1" SelectAction="None" Checked="true">
                        </asp:TreeNode>
                    </Nodes>
                </asp:TreeView>
            </div>
            <br />
            <br />
            <div>
                <asp:Label ID="lbl2" Text="Enter the number of " runat="server"></asp:Label><asp:Label
                    ID="lblSelectedNodeType" runat="server"></asp:Label><asp:Label ID="lbl3" Text=" for this "
                        runat="server" /><asp:Label runat="server" ID="lblSelectedNodeParentType"></asp:Label><asp:Label
                            ID="lbl9" Text=" :" runat="server" /><asp:TextBox runat="server" ID="txtSelectedNodeCount"></asp:TextBox>
                <asp:Button ID="btnRefresh" OnClick="btnRefresh_OnClick" Text="Refresh Tree" runat="server" />
                 <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Enter between 1 & 10"
                    ControlToValidate="txtSelectedNodeCount" Type="Integer" MinimumValue="1" MaximumValue="10"></asp:RangeValidator>
            </div>
          
        </ContentTemplate>
    </asp:UpdatePanel>
        </td>
        </tr>
        <tr>
    <td>
        <asp:Button id="btnXML" Text="Generate Template" runat="server" OnClick="btnXML_OnClick"/>
        </td>
    </tr>
    </table>
    
    </form>
</body>
</html>
