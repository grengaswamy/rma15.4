﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="XMLImport.aspx.cs" Inherits="Riskmaster.UI.UI.XML_Import.XMLImport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="dg" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Import XML</title>
    <link href="../../../../Content/zpcal/themes/system.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/cul.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Check() {            
            var filename = document.getElementById("uploadFile");
            if (filename.value == "") {
                alert("Please Provide the xml file to be uploaded");
                return false;
            }
        }
       
    </script>

</head>
<body onload="parent.MDIScreenLoaded();">

   <asp:Label ID="lbSuccess" runat="server" Visible="false" Text="The XML Files have been successfully updated" Style="font-weight: bold;
                        color: blue"></asp:Label>
                    <dg:errorcontrol id="ErrorControl1" runat="server" />
    <form id="frmData" runat="server">
     <div class="msgheader">
      Import XML</div>
    
    <div>
        <asp:HiddenField ID="hdnFilePath" runat="server" />
 
            

             
                
                    <br />
                    <br />
                           <br />
                    <br />
                
                   <table id="Table1" border="0" runat="server" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td >
                    Select the XML Files to be uploaded.
                </td>
                <td>
                   <asp:FileUpload ID="uploadFile" runat="server"  />
                </td>
            </tr>
            <tr>
                <td >
                    <br />
                    <br />
                </td>
                
            </tr>
            <tr>
                <td >
                    Click to upload the selected XMLs
                </td>
                <td>
                    <asp:Button ID="btnUpload" runat="server" Text="Upload Files" OnClick="btnUpload_Click" OnClientClick = "return Check()" />
                </td>
            </tr>
                        <tr>
                <td >
                    
                </td>
                <td><br /><!--btnDownLoadLog Added for MITS 29711 by bsharma33-->
                    <asp:Button ID="btnDownLoadLog" runat="server" Text="Download Log" OnClick="btnDownLoadLog_Click" Visible="false" />
                </td>
            </tr>
        </table>
        
      </div>
    </form>
</body>
</html>

