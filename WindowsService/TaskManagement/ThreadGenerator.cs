using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Threading;
using System.Diagnostics;
using System.IO;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.Service.TaskManagement
{
    //************************************************************** 
    //* $File				: ThreadGenerator.cs 
    //* $Revision			: 1.0.0.0 
    //* $Creation Date		: 12-07-2007
    //* $Author			    : Aashish Bhateja
    //***************************************************************	

    /// <summary>
    /// Generates and handles the threads which are used to run the processes.
    /// </summary>
    class ThreadGenerator
    {
        /// <summary>
        /// Holds the process collection.
        /// </summary>
        public ArrayList m_ColThread = new ArrayList();

        BusinessAdaptorErrors systemErrors = null;     // Collection for code to use to store system errors that need to be thrown back.
        object objSync = new object();

        /// <summary>
        /// Adds a thread to the "Thread Collection".
        /// </summary>
        /// <param name="p_sName"></param>
        public void Add(string p_sName, int iClientId)
        {

            try
            {
                lock (objSync)
                {
                    Thread objThread = new Thread(new ParameterizedThreadStart(Start));
                    objThread.Name = p_sName;

                    m_ColThread.Add(objThread);
                }
            }

            catch (Exception p_objEx)
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);
                //EventLog.WriteEntry("ThreadGenerator.Add", p_objEx.Message);
                systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.ThreadGenerator.Add.Error", iClientId), BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("ThreadGenerator.Add", null, false, systemErrors, iClientId);
            }
            finally
            {
            }
        }

        /// <summary>
        /// This is a "Start" method for the thread which in turn starts the process containing the job.
        /// </summary>
        /// <param name="p_objData">This is a "ThreadParams" parameter which represents the "Process" and the job id.</param>
        public void Start(object p_objData)
        {
            int iClientId = 0;
            try
            {
                lock (objSync)
                {
                    if (p_objData is ThreadParams)
                    {
                        ThreadParams objTP = (ThreadParams)p_objData;
                      
                        try
                        {
                            objTP.objProcess.Start();
                            iClientId = objTP.iClientId;
                        }
                        catch(Exception p_objEx)
                        {
                            TaskManager objTaskManager = new TaskManager(objTP.iClientId);
                            //objTaskManager.ErrorOnLaunch(objTP.iJobId);
                            objTaskManager.ArchiveJob(objTP.iJobId, 7, objTP.iClientId);//Add & change by kuladeep for Cloud
                            // Commented the line below.No need to grab the output from the running job as we are
                            // not required to show it on the UI anymore.
                            if (!objTP.bIsDetailedStatusDisabled)
                            {
                                objTaskManager.LogStatus(objTP.iJobId, "754^*^*^" + p_objEx.Message, objTP.iClientId);//Add & change by kuladeep for Cloud
                            }
                            throw p_objEx;
                        }

                        // Commented the line below.No need to grab the output from the running job as we are
                        // not required to show it on the UI anymore.
                        if (!objTP.bIsDetailedStatusDisabled)
                        {
                            objTP.objProcess.BeginOutputReadLine();
                        }

                        if (ProcessManager.m_StaticColProcess.Contains(objTP.objProcess.Id))
                        {
                            ProcessManager.m_StaticColProcess.Remove(objTP.objProcess.Id);
                        }

                        ProcessManager.m_StaticColProcess.Add(objTP.objProcess.Id, objTP.iJobId);    
                   
                        //Change by kuladeep for Cloud-Start                       
                        //if (ProcessManager.m_StaticColProcessByJobId.Contains(objTP.iJobId))
                        //{
                        //    ProcessManager.m_StaticColProcessByJobId.Remove(objTP.iJobId);
                        //}
                        //ProcessManager.m_StaticColProcessByJobId.Add(objTP.iJobId, objTP.objProcess.Id);

                        if (ProcessManager.m_StaticColProcessByJobId.Contains(objTP.ClientJobId))
                        {
                            ProcessManager.m_StaticColProcessByJobId.Remove(objTP.ClientJobId);
                        }
                        ProcessManager.m_StaticColProcessByJobId.Add(objTP.ClientJobId, objTP.objProcess.Id);
                        //Change by kuladeep for Cloud-End

                        // Make call to WaitForExit() in the Exit Handler.
                        //objTP.objProcess.WaitForExit();
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);
                //EventLog.WriteEntry("Start : RMAppException", p_objEx.Message, EventLogEntryType.Error);
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("ThreadGenerator.Start", null, false, systemErrors, iClientId);
            }
            catch (Exception p_objEx)
            {
                systemErrors = new BusinessAdaptorErrors(iClientId);
                //EventLog.WriteEntry("Start : Exception", p_objEx.Message, EventLogEntryType.Error);
                systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.ThreadGenerator.Start.Error", iClientId), BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("ThreadGenerator.Start", null, false, systemErrors, iClientId);
            }
        }


        /// <summary>
        /// Returns process count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.m_ColThread.Count;
            }
        }

        /// <summary>
        /// Gets the specific Thread from the thead collection
        /// </summary>
        /// <param name="keyValue">Key</param>
        /// <returns>Thread</returns>
        public Thread Get(int keyValue)
        {
            try
            {
                return (Thread)this.m_ColThread[keyValue];
            }
            catch(Exception ex)
            {
                
                EventLog.WriteEntry("ThreadGenerator.Get", ex.Message);
                return (Thread)this.m_ColThread[keyValue];
            }
        }

    }

    /// <summary>
    /// Creates the Thread Parameters which are used by the Thread Generator as start values for a thread.
    /// </summary>
    class ThreadParams
    {
        public Process objProcess;
        public int iJobId = 0;
        public bool bIsDetailedStatusDisabled = false;
        public int iClientId = 0;//Add & change by kuladeep for Cloud
        public string ClientJobId = string.Empty;
        //public ThreadParams(Process p_objProcess, int p_iJobId, bool p_bIsDetailedStatusDisabled)
        //{
        //    objProcess = p_objProcess;
        //    iJobId = p_iJobId;
        //    bIsDetailedStatusDisabled = p_bIsDetailedStatusDisabled;
        //}
        //Add by kuladeep for Cloud.
        public ThreadParams(Process p_objProcess, int p_iJobId, bool p_bIsDetailedStatusDisabled, int p_iClientId)
        {
            objProcess = p_objProcess;
            iJobId = p_iJobId;
            bIsDetailedStatusDisabled = p_bIsDetailedStatusDisabled;
            iClientId = p_iClientId;
            ClientJobId = p_iClientId + "_" + p_iJobId;
        }
    } 

}
