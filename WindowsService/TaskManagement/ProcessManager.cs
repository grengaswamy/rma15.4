using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;


namespace Riskmaster.Service.TaskManagement
{
    //************************************************************** 
    //* $File				: Job.cs 
    //* $Revision			: 1.0.0.0 
    //* $Creation Date		: 12-07-2007
    //* $Author			    : Aashish Bhateja
    //***************************************************************	

    /// <summary>
    /// This class manages the process handling.Also responsible for managing the process life time like handling the
    /// output returned by different processes and determines the process exit.
    /// </summary>
    class ProcessManager
    {
        /// <summary>
        /// Holds the process collection.
        /// </summary>
        public Hashtable m_ColProcess = new Hashtable();
        /// <summary>
        /// Holds the Collection of process ids
        /// </summary>
       public static Hashtable m_StaticColProcess = new Hashtable();
       /// <summary>
        /// Holds the collection of job ids
        /// </summary>
        public static Hashtable m_StaticColProcessByJobId = new Hashtable();
        /// <summary>
        /// Collection of process ids for which error has been logged.
        /// </summary>
        public static ArrayList m_StaticColErrorCode = new ArrayList();
        /// <summary>
        /// Collection for code to use to store system errors that need to be thrown back.
        /// </summary>
        //public static BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors();  
        public static BusinessAdaptorErrors systemErrors = null;

        object objSync = new object();
        int m_iClientId=0;
        /// <summary>
        /// Adds a process to the "Process Collection"
        /// </summary>
        /// <param name="p_iKey">This is Job Id,which acts as a key for the process collection.</param>
        /// <param name="p_sFileName"></param>
        public void Add(string p_iKey, string p_sFileName, string p_sArgs, bool p_bIsDetailedStatusDisabled, int p_iClientId)//Add & change by kuladeep for Cloud
        {
            Process p = null;  
            try
            {
                p = new Process();
                systemErrors = new BusinessAdaptorErrors(p_iClientId);  
                p.StartInfo.FileName = p_sFileName; // @"C:\Riskmaster\RMX\R4\New Processing Service\Code\Service\ConsoleApplication1\bin\Debug\ConsoleApplication1.exe";
                p.StartInfo.Arguments = p_sArgs;

                p.StartInfo.RedirectStandardOutput = true;
                p.EnableRaisingEvents = true;
                p.StartInfo.UseShellExecute = false;
                //p.StartInfo.CreateNoWindow = true;
                m_iClientId = p_iClientId;//For Cloud changes.

                // Commented the line below.No need to grab the output from the running job as we are
                // not required to show it on the UI anymore.
                if (!p_bIsDetailedStatusDisabled)
                {
                    p.OutputDataReceived += new DataReceivedEventHandler(CaptureConsoleOutput);
                }
                p.Exited += new System.EventHandler(ExitHandler);

                m_ColProcess.Add(p_iKey, p);
            }

            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("ProcessManager.Add", p_objEx.Message);
                systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.ProcessManager.Add.Error", p_iClientId), BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("ProcessManager.captureStdout", null, false, systemErrors, p_iClientId);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Returns process count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.m_ColProcess.Count;
            }
        }

        /// <summary>
        /// Get the process by providing the Job Id. 
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public Process Get(string keyValue)
        {
            try
            {
                return (Process)this.m_ColProcess[keyValue];
            }
            catch(Exception p_objEx)
            {

                EventLog.WriteEntry("ProcessManager.Get", p_objEx.Message);
                return (Process)this.m_ColProcess[keyValue];
            }
        }

        // For testing purpose
        //static int iStatic = 0;

        /// <summary>
        /// This is called by the Process.Exited event handler to archive the job once a process is completed or killed.
        /// </summary>
        /// <param name="sender">This is used to determine the process parameters like Id, ExitCode etc.</param>
        /// <param name="e"></param>       
        private void ExitHandler(object sender, EventArgs e)
        {
            Process ps = (Process)sender;
            TaskManager objTaskManager = null;

            try
            {
                //int i = ps.Id;
                //StreamReader myStreamReader = ps.StandardOutput;
                ////Read the standard output of the spawned process.
                //myString = myStreamReader.ReadToEnd();

                // For testing purpose, -START- 
                //iStatic = iStatic + 1;
                //string sLogFile = @"C:\Riskmaster\RMX\R4\New Processing Service\Code\Service\ConsoleApplication1\stdoutlog.txt";
                //System.IO.StreamWriter stm1 = new StreamWriter(sLogFile, true);
                ////stm1.WriteLine("Job Id : " + m_StaticColProcess[ps.Id] + " Count : " + m_StaticColProcess.Count);
                //stm1.WriteLine("Count : " + iStatic + " " + DateTime.Now.ToString() + " - " + "Job Id : Process Id " + m_StaticColProcess[ps.Id] + " : " + ps.Id + " $$Exit Code : " + ps.ExitCode);
                //stm1.Flush();
                //stm1.Close();
                // For testing purpose, -END- 

                // Achive the Job once it is completed.
                systemErrors = new BusinessAdaptorErrors(m_iClientId);  
                objTaskManager = new TaskManager(m_iClientId);
                
                int iJobId = Conversion.ConvertObjToInt(m_StaticColProcess[ps.Id],m_iClientId);

                if (ps.HasExited)
                {
                    if (iJobId != 0)
                    {
                        // A Data Integrator job
                        if (objTaskManager.IsDetailedStatusDisabled(objTaskManager.GetTaskTypeId(iJobId, m_iClientId), m_iClientId)) //Add & change by kuladeep for Cloud
                        {
                            objTaskManager.ArchiveJob(iJobId, ps.ExitCode, m_iClientId);//Add & change by kuladeep for Cloud
                            //switch (ps.ExitCode)
                            //{
                            //    case 0: //Completed
                            //        objTaskManager.ArchiveJob(iJobId, 3);
                            //        break;
                            //    case 1: //Completed With Error
                            //        objTaskManager.ArchiveJob(iJobId, 4);
                            //        break;
                            //    case 2: //Completed With Validation Errors
                            //        objTaskManager.ArchiveJob(iJobId, 8);
                            //        break;
                            //    default:
                            //        break;
                            //}
                        }
                        else
                        {
                            if (ps.ExitCode == 0) // Successful exit.
                            {
                                if (m_StaticColErrorCode.Contains(ps.Id))
                                {
                                    objTaskManager.ArchiveJob(iJobId, 4, m_iClientId);//Completed with error //Add & change by kuladeep for Cloud
                                }
                                else
                                {
                                    objTaskManager.ArchiveJob(iJobId, 3, m_iClientId);//Completed //Add & change by kuladeep for Cloud
                                }
                            }
                            else // Abnormal exit.
                            {
                                objTaskManager.ArchiveJob(iJobId, 6, m_iClientId);//Killed //Add & change by kuladeep for Cloud
                            }
                        }
                    }
                }

                if(!ps.HasExited)
                    ps.Kill();
                //ps.Close();

                // This call is moved from ThreadGenerator.Start() function.
                ps.WaitForExit();
                m_StaticColProcess.Remove(ps.Id);
                //m_StaticColProcessByJobId.Remove(iJobId);//Change by kuladeep 
                m_StaticColProcessByJobId.Remove(m_iClientId+"_"+iJobId);
                m_StaticColErrorCode.Remove(ps.Id);
            }
            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("TaskManager", p_objEx.Message, EventLogEntryType.Error);
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("ProcessManager.ExitHandler", null, false, systemErrors, m_iClientId);
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("ProcessManager.captureStdout", p_objEx.Message);
                systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.ProcessManager.captureStdout.Error", m_iClientId), BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("ProcessManager.ExitHandler", null, false, systemErrors, m_iClientId);
            }
        }

        /// <summary>
        /// This is called by Process.OutputDataReceived event handler to handle the stdout emitted by the running 
        /// process.This output is then logged to the TM database for logging purpose.
        /// </summary>
        /// <param name="sendingProcess"></param>
        /// <param name="outLine"></param>
        private void CaptureConsoleOutput(object sendingProcess, DataReceivedEventArgs outLine)
        {
            TaskManager objTaskManager = null;
           
            try
            {
                systemErrors = new BusinessAdaptorErrors(m_iClientId);  
                object objSync = new object();
                objTaskManager = new TaskManager(m_iClientId);
                string[] sArrSeparator = {"^*^*^"};

                lock (objSync)
                {
                    // Collect the sort command output.
                    if (!String.IsNullOrEmpty(outLine.Data))
                    {
                        //string sLogFile = "log" + ((Process)sendingProcess).Id + ".txt";
                        //WriteToFile(sLogFile, ((Process)sendingProcess).Id, outLine.Data);
                        objTaskManager.LogStatus(Conversion.ConvertObjToInt(m_StaticColProcess[((Process)sendingProcess).Id],m_iClientId), outLine.Data, m_iClientId);//Add & change by kuladeep for Cloud
                        if (Conversion.ConvertStrToLong(((string[])outLine.Data.Split(sArrSeparator, StringSplitOptions.None))[0]) > 0)
                        {
                            m_StaticColErrorCode.Add(((Process)sendingProcess).Id);
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("OutputHandler", p_objEx.Message, EventLogEntryType.Error);
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("CaptureConsoleOutput", null, false, systemErrors, m_iClientId);
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("ProcessManager.OutputHandler", p_objEx.Message);
                systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.ProcessManager.OutputHandler.Error", m_iClientId), BusinessAdaptorErrorType.Error);
                TaskManager.logErrors("CaptureConsoleOutput", null, false, systemErrors, m_iClientId);
            }
        }

        /// <summary>
        /// Temporary method used while development.
        /// </summary>
        /// <param name="p_sFileName"></param>
        /// <param name="p_iPID"></param>
        /// <param name="p_sData"></param>
        private static void WriteToFile(string p_sFileName, int p_iPID, string p_sData)
        {
            string sLogFile = @"C:\Riskmaster\RMX\R4\New Processing Service\Code\Service\ConsoleApplication1\" + p_sFileName;
            System.IO.StreamWriter stm = new StreamWriter(sLogFile, true);
            stm.WriteLine(p_iPID + " - " + p_sData);
            stm.Flush();
            stm.Close();
        }

    }

}
